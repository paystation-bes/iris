##-------------------------------------------------------------------------------------------------------------------
##	Class		: EMS6DataAccess
##	Purpose		: This class extracts the data from EMS6 Database. 
##	Important	: The calls to the procedure in the class are implicitly made by EMS6InitialMigration.py.
##	Authori		: Vijay Ramberg.
##-------------------------------------------------------------------------------------------------------------------

class EMS6DataAccess:
	def __init__(self, EMS6Connection, EMS6ConnectionCerberus, verbose):
		self.__verbose = verbose

		self.__EMS6Connection = EMS6Connection
		self.__EMS6Cursor = EMS6Connection.cursor()
		self.__EMS6ConnectionCerberus = EMS6ConnectionCerberus
		self.__EMS6CursorCerberus = EMS6ConnectionCerberus.cursor()

	def getCustomers(self):
		# By Default all the Customers are assigned a CHILD status(CustomerType=3)
		self.__EMS6Cursor.execute("SELECT 3,IF(AccountStatus='ENABLED',1,0),Name, version, now(),1,Id FROM Customer")
		return self.__EMS6Cursor.fetchall()	   
		
	def getEMS6CustomerProperties(self):
		self.__EMS6Cursor.execute("select CustomerId,MaxUserAccounts,CCOfflineRetry,MaxOfflineRetry,TimeZone,SMSWarningPeriod from CustomerProperties")
		return self.__EMS6Cursor.fetchall()

	def getEMS6CustomerSubscription(self):
                self.__EMS6CursorCerberus.execute("select c.EmsCustomerId, r.ServiceId from Customer_Service_Relation r left join Customer c on (c.Id=r.CustomerId)")
                return self.__EMS6CursorCerberus.fetchall()

	def getEMS6LotSettings(self):
		self.__EMS6Cursor.execute("select l.version,Customer.Id,l.Name,l.UniqueId,l.PaystationType,l.ActivationDate,l.TimeZone,l.FileLocation,l.UploadDate,l.Id from LotSetting l left join Customer on (l.CustomerId=Customer.Id) where Customer.id is not NULL")
		return self.__EMS6Cursor.fetchall()

 	def getEMS6Paystation(self):
		self.__EMS6Cursor.execute("select p.Id,p.PaystationType,CASE m.Type WHEN 'GSM' THEN 4 WHEN 'Ethernet' THEN 3 ELSE 0 END as Type ,p.CommAddress,version,Name,ProvisionDate,CustomerId,RegionId,LotSettingId from Paystation  p left join ModemSetting m on (p.Id=m.PaystationId)")
		return self.__EMS6Cursor.fetchall()

	def getEMS6Locations(self):
                self.__EMS6Cursor.execute("select Region.version,Region.AlarmState,Customer.Id,Region.Name,Region.Description,Region.ParentRegion,Region.IsDefault,Region.Id from Region,Customer where Customer.Id=Region.CustomerId and Region.CustomerId is not null")
		return self.__EMS6Cursor.fetchall()

	def getEMS6Coupons(self):
		self.__EMS6Cursor.execute("select Customer.Id,Coupon.Id,Coupon.PercentDiscount,Coupon.StartDate,Coupon.EndDate,Coupon.NumUses,Coupon.RegionId,Coupon.StallRange,Coupon.Description,Coupon.IsPndEnabled,Coupon.IsPbsEnabled from Coupon,Customer where Coupon.CustomerId=Customer.Id and Coupon.CustomerId is not null")	
		return self.__EMS6Cursor.fetchall()

	def getEMS6ParkingPermissionType(self):
		self.__EMS6Cursor.execute("select Id,Name from EMSParkingPermissionType")
		return self.__EMS6Cursor.fetchall()

	def getParkingPermission(self):
		self.__EMS6Cursor.execute("select Name,StartTimeHourLocal,StartTimeMinuteLocal,EndTimeHourLocal,EndTimeMinuteLocal,RegionId,PermissionTypeId,PermissionStatus,SpecialParkingPermissionId,MaxDurationMinutes,CreationDate,IsActive,Id from EMSParkingPermission")
		return self.__EMS6Cursor.fetchall()

	def getParkingPermissionDayOfWeek(self):
		self.__EMS6Cursor.execute("select EMSPermissionId,DayOfWeek from EMSParkingPermissionDayOfWeek")
		return self.__EMS6Cursor.fetchall()

	def getEMS6ExtensibleRatesDayOfWeek(self):
                self.__EMS6Cursor.execute("select EMSRateId,DayOfWeek from EMSRateDayOfWeek")
		return self.__EMS6Cursor.fetchall()

	def getEMS6RestAccount(self):
                self.__EMS6Cursor.execute("select r.AccountName,r.CustomerId,r.TypeId,r.SecretKey,r.VirtualPS from RESTAccount r,Customer c where r.CustomerId=c.Id and r.CustomerId is not null")
		return self.__EMS6Cursor.fetchall()

	def getEMS6LicencePlate(self, PaystationId):
		self.__EMS6Cursor.execute("select distinct(PlateNumber) from Transaction where PaystationId=%s and PlateNumber<>''",(PaystationId))
		return self.__EMS6Cursor.fetchall()	

	def getEMS6MobileNumberFromSMSAlert(self):
		self.__EMS6Cursor.execute("select distinct(MobileNumber) from SMSAlert where MobileNumber<>''")
		return self.__EMS6Cursor.fetchall()

	def getEMS6MobileNumberFromEMSExtensiblePermit(self):
		self.__EMS6Cursor.execute("select distinct(MobileNumber) from EMSExtensiblePermit where MobileNumber<>''")
		return self.__EMS6Cursor.fetchall()

	def getEMS6MobileNumberFromSMSTransactionLog(self):
		self.__EMS6Cursor.execute("select distinct(MobileNumber) from SMSTransactionLog where MobileNumber<>''")
		return self.__EMS6Cursor.fetchall()

	def getEMS6ExtensiblePermit(self):
		self.__EMS6Cursor.execute("select MobileNumber,CardData,Last4Digital,PurchasedDate,LatestExpiryDate,IsRFID from EMSExtensiblePermit")
		return self.__EMS6Cursor.fetchall()
	
	def getEMS6SMSTransactionLog(self):
		self.__EMS6Cursor.execute("select SMSMessageTypeId,MobileNumber,PaystationId,PurchasedDate,TicketNumber,ExpiryDate,UserResponse,TimeStamp from SMSTransactionLog")
		return self.__EMS6Cursor.fetchall()

	def getEMS6SMSAlert(self):
		self.__EMS6Cursor.execute("select MobileNumber,ExpiryDate,PaystationId,PurchasedDate,TicketNumber,PlateNumber,StallNumber,RegionId,NumOfRetry,IsAlerted,IsLocked,IsAutoExtended from SMSAlert")
		return self.__EMS6Cursor.fetchall()

	def getEMS6SMSFailedResponse(self):		
		self.__EMS6Cursor.execute("select PaystationId,PurchasedDate,TicketNumber,SMSMessageTypeId,IsOk,TrackingId,Number,ConvertedNumber,DeferUntilOccursInThePast,IsMessageEmpty,IsTooManyMessages,IsInvalidCountryCode,IsBlocked,BlockedReason,IsBalanceZero,IsInvalidCarrierCode from SMSFailedResponse")
		return self.__EMS6Cursor.fetchall()

	def getEMS6ModemSettings(self):
		self.__EMS6Cursor.execute("select PaystationId, Type, CCID, Carrier, APN from ModemSetting")
		return self.__EMS6Cursor.fetchall()

	def getEMS6LotSettingContent(self):
		self.__EMS6Cursor.execute("select Id, FileContent from LotSettingFileContent")
		return self.__EMS6Cursor.fetchall()

	def getEMS6CryptoKey(self):
		self.__EMS6Cursor.execute("select Type,KeyIndex,Hash,Expiry,Signature,Info,NextHash,Status,CreateDate,Comment from CryptoKey")
		return self.__EMS6Cursor.fetchall()

	def getEMS6RestLogs(self):	
		self.__EMS6Cursor.execute("select RestAccountName, EndpointName, LoggingDate, IsError, CustomerId, TotalCalls from RESTLog")
		return self.__EMS6Cursor.fetchall()

	def getEMS6RestSessionToken(self):	
		self.__EMS6Cursor.execute("select AccountName,SessionToken,CreationDate,ExpiryDate from RESTSessionToken")
		return self.__EMS6Cursor.fetchall()

	def getEMS6RestLogTotalCall(self):	
		self.__EMS6Cursor.execute("select RESTAccountName,LoggingDate,TotalCalls from RESTLogTotalCall")
		return self.__EMS6Cursor.fetchall()

	def getEMS6CustomerWsCal(self):	
		self.__EMS6Cursor.execute("select CustomerId,EndPointId,CalInUse,CalPurchase,Description,Id from CustomerWsCal")
		return self.__EMS6Cursor.fetchall()

        def getEMS6CustomerWsToken(self):
                self.__EMS6Cursor.execute("select CustomerId,Token,EndPointId,WsInUse,Id from CustomerWsToken")
                return self.__EMS6Cursor.fetchall()

	def getEMS6CustomerAlert(self):
		self.__EMS6Cursor.execute("select PaystationGroupId,CustomerId,AlertName,AlertTypeId,Threshold,Email,IsEnabled,IsDeleted,RegionId,IsPaystationBased from Alert")
		return self.__EMS6Cursor.fetchall()

	def getEMS6CustomerWebServiceCal(self):
		self.__EMS6Cursor.execute("select CustomerId,EndPointId,CalInUse,CalPurchase,Description from CustomerWsCal")
		return self.__EMS6Cursor.fetchall()

	def getEMS6CustomerWsToken(self):
		self.__EMS6Cursor.execute("select CustomerId,Token, EndPointId, WsInUse from CustomerWsToken")
		return self.__EMS6Cursor.fetchall()

	def getEMS6Rates(self):
		self.__EMS6Cursor.execute("select distinct CustomerId,RateName from Rates")
		return self.__EMS6Cursor.fetchall()

	def getEMS6EMSRates(self):
		self.__EMS6Cursor.execute("select distinct r.CustomerId,er.Name from EMSRate er, Region r where er.RegionId=r.Id")
		return self.__EMS6Cursor.fetchall()

	def getEMS6ExtensibleRate(self):
		self.__EMS6Cursor.execute("select Name,RateTypeId,SpecialRateId,StartTimeHourLocal,StartTimeMinuteLocal,EndTimeHourLocal,EndTimeMinuteLocal,RegionId,RateAmountCent,ServiceFeeCent,MinExtensionMinutes,CreationDate,IsActive,Id from EMSRate")
		return self.__EMS6Cursor.fetchall()
	
	def getEMS6Replenish(self):
		self.__EMS6Cursor.execute("select PaystationId, Date, Number, TypeId, Tube1Type, Tube1ChangedCount, Tube1CurrentCount, Tube2Type, Tube2ChangedCount, Tube2CurrentCount, Tube3Type, Tube3ChangedCount, Tube3CurrentCount, Tube4Type, Tube4ChangedCount, Tube4CurrentCount, CoinBag005AddedCount, CoinBag010AddedCount, CoinBag025AddedCount, CoinBag100AddedCount, CoinBag200AddedCount from Replenish")
		return self.__EMS6Cursor.fetchall()

	def getEMS6CardRetryTransaction(self):
		self.__EMS6Cursor.execute("select PaystationId,PurchasedDate,TicketNumber,LastRetryDate,NumRetries,CardHash,TypeId,CardData,CardExpiry,Amount,CardType,Last4DigitsOfCardNumber,CreationDate,BadCardHash,IgnoreBadCard,LastResponseCode,IsRFID from CardRetryTransaction")
		return self.__EMS6Cursor.fetchall()

	def getEMS6CardType(self):
		self.__EMS6Cursor.execute("select CustomerId,version,Name,Track2RegEx,CheckDigitAlg,Description,AuthorizationMethod from CardType")
		return self.__EMS6Cursor.fetchall()

	def getEMS6PaystationGroup(self):
		self.__EMS6Cursor.execute("select CustomerId,Name from PaystationGroup")
		return self.__EMS6Cursor.fetchall()
	
	def getEMS6CollectionType(self):
		self.__EMS6Cursor.execute("select Id,Name,IsInactive from CollectionType")
		return self.__EMS6Cursor.fetchall()

	def getEMS6ValueSmartCardType(self):
		self.__EMS6Cursor.execute("select CustomerId, RegionId, PaystationId, CustomCardData, SmartCardData from Transaction limit 2000000")
		return self.__EMS6Cursor.fetchall()

	def getDistinctPaystationForBatteryInfo(self):
		self.__EMS6Cursor.execute("select distinct(PaystationId) from BatteryInfo")
		return self.__EMS6Cursor.fetchall()

	def getBatteryInfo(self, PaystationId):
		self.__EMS6Cursor.execute("select PaystationId,DateField,SystemLoad,InputCurrent,Battery from BatteryInfo where PaystationId=%s",(PaystationId))
		return self.__EMS6Cursor.fetchall()

	def getDistinctPaystationId(self):
		self.__EMS6Cursor.execute("select distinct(PaystationId) from Transaction")
		return self.__EMS6Cursor.fetchall()

	def getEMS6Transaction(self, PaystationId):
		self.__EMS6Cursor.execute("select tr.CustomerId,tr.PaystationId,tr.PurchasedDate,tr.TicketNumber,tr.TypeId,tr.PaymentTypeId,tr.RegionId,tr.RateId,tr.CouponNumber,tr.LotNumber,tr.OriginalAmount,tr.ChargedAmount,tr.ChangeDispensed,tr.ExcessPayment,tr.CashPaid,tr.CardPaid,tr.IsOffline,tr.IsRefundSlip,tr.CreationDate, tc.CoinDollars,tc.BillDollars,tc.CoinCount,tc.BillCount,rt.RateValue,rt.Revenue,rt.RateName,rt.CustomerId,tr.StallNumber,tr.PlateNumber, tr.AddTimeNum,tr.ExpiryDate from Transaction tr left join TransactionCollection tc using (PaystationId,PurchasedDate,TicketNumber) left join Rates rt using (PaystationId,PurchasedDate,TicketNumber) where PaystationId=%s",(PaystationId))
		return self.__EMS6Cursor.fetchall()

	def getCustomerEmail(self):
		self.__EMS6Cursor.execute("select CustomerId, AlertTypeId, Email, IsDeleted from Alert")
		return self.__EMS6Cursor.fetchall()

	def getProcessorProperties(self):
		self.__EMS6Cursor.execute("select Processor,Name,Value from ProcessorProperties")
		return self.__EMS6Cursor.fetchall()

	def getMerchantAccount(self):
		self.__EMS6Cursor.execute("select Id,version,CustomerId,Name,Field1,Field2,Field3,IsDeleted,ProcessorName,ReferenceCounter,Field4,Field5,Field6,IsValid from MerchantAccount")
		return self.__EMS6Cursor.fetchall()

	def getPreAuth(self):
		self.__EMS6Cursor.execute("select Id, ResponseCode, ProcessorTransactionId, AuthorizationNumber, ReferenceNumber, MerchantAccountId,Amount,Last4DigitsOfCardNumber,CardType,Approved,PreAuthDate,CardData,PaystationId,ReferenceId,Expired,CardHash,ExtraData,PsRefId,IsRFID,CardExpiry from PreAuth")
		return self.__EMS6Cursor.fetchall()
		
	def getPreAuthHolding(self):
		self.__EMS6Cursor.execute("select Id, ResponseCode, ProcessorTransactionId, AuthorizationNumber, ReferenceNumber, MerchantAccountId,Amount,Last4DigitsOfCardNumber,CardType,Approved,PreAuthDate,CardData,PaystationId,ReferenceId,Expired,CardHash,ExtraData,PsRefId,IsRFID,CardExpiry from PreAuthHolding")
		return self.__EMS6Cursor.fetchall()

	def getProcessorTransaction(self, PaystationId):
		self.__EMS6Cursor.execute("select TypeId, PaystationId, MerchantAccountId, TicketNumber, Amount, CardType, Last4DigitsOfCardNumber, CardChecksum, PurchasedDate, ProcessingDate, ProcessorTransactionId, AuthorizationNumber, ReferenceNumber, Approved, CardHash, IsUploadedFromBoss, IsRFID from ProcessorTransaction where PaystationId=%s",(PaystationId))
		return self.__EMS6Cursor.fetchall()

	def getDistinctPaystationIdFromProcessorTransaction(self):
		self.__EMS6Cursor.execute("select distinct(PaystationId) from ProcessorTransaction")
		return self.__EMS6Cursor.fetchall()

	def getEMS6TransactionForPermit(self, PaystationId):
		self.__EMS6Cursor.execute("select tr.PaystationId,tr.TicketNumber,tr.RegionId,tr.TypeId,tr.PlateNumber,tr.StallNumber,tr.AddTimeNum,tr.PurchasedDate,tr.ExpiryDate,ep.MobileNumber from Transaction tr left join EMSExtensiblePermit ep using (PurchasedDate) where tr.PaystationId=%s",(PaystationId))
		return self.__EMS6Cursor.fetchall()

	def getEMS6TransactionForPayment(self, PaystationId):
		self.__EMS6Cursor.execute("select t.PaystationId,t.TicketNumber,t.PurchasedDate,t.PaymentTypeId,pt.CardType,pt.TypeId,pt.MerchantAccountId,pt.Amount,pt.Last4DigitsOfCardNumber,pt.ProcessingDate,pt.IsUploadedFromBoss,pt.IsRFID,pt.Approved from Transaction t join ProcessorTransaction pt using (PaystationId,PurchasedDate,TicketNumber) where t.PaystationId=%s",(PaystationId))
		return self.__EMS6Cursor.fetchall() 

	def getEMS6UserAccount(self):
		self.__EMS6Cursor.execute("select u.version,u.CustomerId,u.Name,u.Password,u.RoleId,u.AccountStatus, concat(u.Name,'@',c.Name) as UserAccount,Id from UserAccount u left join Customer c on(c.Id=u.CustomerId)")
		return self.__EMS6Cursor.fetchall()

	def getDistinctPaystationIdForEventLogNew(self):
		self.__EMS6Cursor.execute("select distinct(PaystationId) from EventLogNew")
		return self.__EMS6Cursor.fetchall()

	def getEventLogNew(self, PaystationId):
		self.__EMS6Cursor.execute("select PaystationId,DeviceId,TypeId,ActionId,DateField,Information,ClearUserAccountId,ClearDateField from EventLogNew where PaystationId=%s",PaystationId)
		return self.__EMS6Cursor.fetchall()
	
	def getEMS6CustomerAlert(self):
		self.__EMS6Cursor.execute("select Id, PaystationGroupId, CustomerId, AlertName, AlertTypeId, Threshold, Email, IsEnabled, IsDeleted, RegionId, IsPaystationBased from Alert")
		return self.__EMS6Cursor.fetchall()

	def getEMS6CerberusTrialCustomer(self):
		self.__EMS6Cursor.execute("select EmsCustomerId,Status,Expiry from cerberus_db.Customer left join ems_db.Customer on cerberus_db.Customer.EmsCustomerId=ems_db.Customer.Id where Status=1 and ServerId=1")
		return self.__EMS6Cursor.fetchall()

	def getEMS6RegionPaystationLog(self):
		self.__EMS6Cursor.execute("select Id, RegionId, PaystationId, CreationDate from RegionPaystationLog")
		return self.__EMS6Cursor.fetchall()
	
	def getEMS6PaystationBalance(self):
		self.__EMS6Cursor.execute("select PaystationId, CashDollars, CoinCount, CoinDollars, BillCount, BillDollars, UnsettledCreditCardCount, UnsettledCreditCardDollars, TotalDollars, LastCashCollectionGMT, LastCoinCollectionGMT, LastBillCollectionGMT, LastCardCollectionGMT, LastRecalcGMT, CollectionRecalcGMT, CollectionRecalcTransactions, ClusterId, HasRecentCollection, IsRecalcable, LastCollectionTypeId, CollectionLockId, PrevCollectionLockId, PrevPrevCollectionLockId, NextRecalcGMT from PaystationBalance")
		return self.__EMS6Cursor.fetchall()

	def getEMS6ClusterMember(self):
		self.__EMS6Cursor.execute("select Id,Name,Hostname,LocalPort from ClusterMember")
		return self.__EMS6Cursor.fetchall()

	def getEMS6CollectionLock(self):
		self.__EMS6Cursor.execute("select ClusterId, LockGMT, BatchSize, LockableCount, LockedCount, RecalcedCount, IsReissuedLock, RecalcMode, RecalcEndGMT from CollectionLock")
		return self.__EMS6Cursor.fetchall()

	def getEMS6PaystationHeartBeat(self):
		self.__EMS6Cursor.execute("select PaystationId,LastHeartBeatGMT from PaystationHeartBeat")
		return self.__EMS6Cursor.fetchall()

	def getEMS6ServiceState(self):
		self.__EMS6Cursor.execute("select PaystationId,version,LastHeartBeat,IsDoorOpen,IsDoorUpperOpen,IsDoorLowerOpen,IsInServiceMode,Battery1Voltage,Battery2Voltage,IsBillAcceptor,IsBillAcceptorJam,IsBillAcceptorUnableToStack,IsBillStacker,BillStackerSize,BillStackerCount,IsCardReader,IsCoinAcceptor,CoinBagCount,IsCoinHopper1,CoinHopper1Level,CoinHopper1DispensedCount,IsCoinHopper2,CoinHopper2Level,CoinHopper2DispensedCount,IsPrinter,IsPrinterCutterError,IsPrinterHeadError,IsPrinterLeverDisengaged,PrinterPaperLevel,IsPrinterTemperatureError,IsPrinterVoltageError,Battery1Level,Battery2Level,IsLowPowerShutdownOn,IsShockAlarmOn,WirelessSignalStrength,IsCoinAcceptorJam,BillStackerLevel,TotalSinceLastAudit,CoinChangerLevel,IsCoinChanger,IsCoinChangerJam,LotNumber,MachineNumber,BBSerialNumber,PrimaryVersion,SecondaryVersion,UpgradeDate,AlarmState,IsNewLotSetting,LastLotSettingUpload,IsNewTicketFooter,IsNewPublicKey,AmbientTemperature,ControllerTemperature,InputCurrent,SystemLoad,RelativeHumidity,IsCoinCanister,IsCoinCanisterRemoved,IsBillCanisterRemoved,IsMaintenanceDoorOpen,IsCashVaultDoorOpen,IsCoinEscrow,IsCoinEscrowJam,IsCommunicationAlerted,IsCollectionAlerted,UserDefinedAlarmState from ServiceState")
		return self.__EMS6Cursor.fetchall()

	def getEMS6PaystationAlert(self):
		self.__EMS6Cursor.execute("select AlertId,PaystationId,IsAlerted from PaystationAlert")
		return self.__EMS6Cursor.fetchall()

	def getActivePOSAlertForUserDefined(self):
		self.__EMS6Cursor.execute("select CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId, max(AlertGMT),AlertInfo,IsActive,IsSentEmail,SentEmailGMT,ClearedGMT,ClearedByUserId,CreatedGMT from POSAlert where EventSeverityTypeId<>0 and EventDeviceTypeId=20 group by PointOfSaleId")
		return self.__EMS6Cursor.fetchall()

	def getActivePOSAlertForPaystation(self):
		self.__EMS6Cursor.execute("select CustomerAlertTypeId,PointOfSaleId,EventDeviceTypeId,EventStatusTypeId,EventActionTypeId,EventSeverityTypeId,max(AlertGMT),AlertInfo,IsActive,IsSentEmail,SentEmailGMT,ClearedGMT,ClearedByUserId,CreatedGMT from POSAlert where EventSeverityTypeId<>0 and EventDeviceTypeId!=20 group by PointOfSaleId")
		return self.__EMS6Cursor.fetchall()

# Get the paystation  list from cerberus and fileter it by Customer.ServerId=1, must have that.
	def getEMS6BillingReport(self, PaystationId):
		self.__EMS6Cursor.execute("SELECT  H.idPaystation, H.ChangeGMT, H.idBillingStatusType FROM cerberus_db.BillingStatusHistory H, Paystation P WHERE P.id = %s AND P.id = H.idPaystation AND H.ChangeGMT >= '2010-01-01 00:00:00' AND H.ChangeGMT < ADDDATE('2012-01-01 00:00:00',INTERVAL 1 DAY) AND H.ChangeGMT = (SELECT  MAX(H2.ChangeGMT) FROM cerberus_db.BillingStatusHistory H2 WHERE H2.idPaystation = H.idPaystation AND H2.ChangeGMT >= '2010-01-01 00:00:00' AND H2.ChangeGMT < ADDDATE(now(),INTERVAL 1 DAY))GROUP BY P.id", (PaystationId))
		return self.__EMS6Cursor.fetchall()

	def getEMS6AuditAccess(self, PaystationId):
		self.__EMS6Cursor.execute("SELECT  P.Id, A.ActivityDate AS ChangeGMT, 37-A.EventTypeId AS NowActivated FROM ems_db.AuditAccess A, Paystation P WHERE   A.EventTypeId IN (35,36) AND A.Description LIKE 'PaystationId:' AND P.id = %s AND P.id = CAST(SUBSTRING(A.Description,Locate('PaystationId: ',A.Description)+14) AS UNSIGNED) AND A.ActivityDate = (SELECT  MAX(A2.ActivityDate) FROM ems_db.AuditAccess A2 WHERE A2.EventTypeId IN (35,36) AND CAST(SUBSTRING(A2.Description,Locate('PaystationId: ',A2.Description)+14) AS UNSIGNED) = CAST(SUBSTRING(A.Description,Locate('PaystationId: ',A.Description)+14) AS UNSIGNED))", (PaystationId))
		return self.__EMS6Cursor.fetchall()

	def getEMS6PaystationForPOSDate(self):
		self.__EMS6Cursor.execute("select Id,version,Name,CommAddress,ContactURL,CustomerID,IsActive,RegionId,TimeZone,LockState,ProvisionDate,MerchantAccountId,DeleteFlag,CustomerCardMerchantAccountID,PaystationType,IsVerrus,QueryStallsBy,LotSettingId")
		return self.__EMS6Cursor.fetchall()

	def getEMS6BadValueCard(self):
		self.__EMS6Cursor.execute("select CustomerId,AccountNumber,version,CardType from BadCard")
		return self.__EMS6Cursor.fetchall()

	def getEMS6BadCreditCard(self):
		self.__EMS6Cursor.execute("select CustomerId,CardHash,CardData,CardExpiry,AddedDate,Comment from BadCreditCard")
		return self.__EMS6Cursor.fetchall()

	def getEMS6BadSmartCard(self):
		self.__EMS6Cursor.execute("select CustomerId,CardNumber,AddedDate,Comment from BadCard")
		return self.__EMS6Cursor.fetchall()

