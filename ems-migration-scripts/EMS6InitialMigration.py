##-------------------------------------------------------------------------------------------------------------------
##	Class		: EMS6InitialMigration
##	Purpose		: This class instruments the data extraction from EMS6 database and data Transformation/Loading into EMS7 Database. The procedures in the class are called by the Main controller class, "migration.py". 
##	Important	: The calls to the procedure in the class are implicit.
##	Authori		: Vijay Ramberg.
##-------------------------------------------------------------------------------------------------------------------

from datetime import date
class EMS6InitialMigration:
	def __init__(self, EMS6Connection, EMS6ConnectionCerberus, EMS7Connection, EMS6DataAccess, EMS7DataAccess, verbose):
		self.__verbose = verbose
		self.__EMS6Connection = EMS6Connection
		self.__EMS7Connection = EMS7Connection
		self.__EMS6ConnectionCerberus = EMS6ConnectionCerberus
		self.__EMS7DataAccess = EMS7DataAccess
		self.__EMS6DataAccess = EMS6DataAccess
		self.__EMS6Cursor = EMS6Connection.cursor()
		self.__EMS7Cursor = EMS7Connection.cursor()
		self.__EMS6CursorCerberus = EMS6ConnectionCerberus.cursor()

	def migrateCustomers(self):
		input = raw_input("Import Customers --> y/n    :   ") 
		Action='Insert'	
		if (input=='y') :
			for curCustomer in self.__EMS6DataAccess.getCustomers():
				self.__EMS7DataAccess.addCustomer(curCustomer, Action)
		else:
			print ("Aborting Migration!!")

	def migrateCustomerProperties(self):
		input = raw_input("Import CustomerProperties --> y/n     :    ")
		if (input=='y'):
			Action='Insert'
			for curCustomerProperty in self.__EMS6DataAccess.getEMS6CustomerProperties():
				self.__EMS7DataAccess.addCustomerPropertyToEMS7(curCustomerProperty,Action)
		else:
			print ("Aborting migration for CustomerProperty!!")
			
	def migrateLotSettings(self):
		input = raw_input("Import LotSetting --> y/n      :   ")
		if(input=='y'):
			for curLotSetting in self.__EMS6DataAccess.getEMS6LotSettings():
				self.__EMS7DataAccess.addLotSettingToEMS7(curLotSetting)

	def migratePaystations(self):
		input = raw_input("Import Paystation --> y/n      :   ")
		Action='Insert'
		if(input=='y'):
			for curPaystation in self.__EMS6DataAccess.getEMS6Paystation():
				self.__EMS7DataAccess.addPaystationToEMS7(curPaystation,Action)
		else:
			print("Aborting Paystation Migration!!")

	def migrateLocations(self):
		input = raw_input("Import Location --> y/n      :   ")
		if(input=='y'):
			for curLocation in self.__EMS6DataAccess.getEMS6Locations():
				self.__EMS7DataAccess.addLocationToEMS7(curLocation)
		else:
			print ("Aborting Migration")

	def migrateCoupons(self):
		input = raw_input("Import Coupons --> y/n     :     ")
		if(input=='y'):
			for curCoupon in self.__EMS6DataAccess.getEMS6Coupons():
				self.__EMS7DataAccess.addCouponToEMS7(curCoupon)
		else:
			print ("Aborting Migration for Coupon")
	

	def migrateParkingPermissionType(self):
		input = raw_input("Importing EMSParkingPermissionType --> y/n  :")
		if(input=='y'):
			for curParkingPermissionType in self.__EMS6DataAccess.getEMS6ParkingPermissionType():
				self.__EMS7DataAccess.addParkingPermissionTypeToEMS7(curParkingPermissionType)
		else:
			print ("Aborting Migration")
		
	def migrateParkingPermission(self):
		input = raw_input("Import Parking Permission --> y/n     :")
		if(input=='y'):
			for curParkingPermission in self.__EMS6DataAccess.getParkingPermission():
				self.__EMS7DataAccess.addParkingPermissionToEMS7(curParkingPermission)
		else:
			print "Aborting Parking Permission Migration"

	def migrateParkingPermissionDayOfWeek(self):			
		input = raw_input("Import EMSParkingParmissionDayOfweek --> y/n     :")	
		if(input=='y'):
			for curParkingPermissionDayOfWeek in self.__EMS6DataAccess.getParkingPermissionDayOfWeek():
				self.__EMS7DataAccess.addParkingPermissionDayOfWeek(curParkingPermissionDayOfWeek)
		else:
			print "Aborting Migration"

	def migrateUnifiedRate(self):
		input = raw_input("Import Unified Rate --> y/n ")
		if(input=='y'):
			for curRates in self.__EMS6DataAccess.getEMS6Rates():
				self.__EMS7DataAccess.addRatesToEMS7(curRates)
		else:
			print "Aborting Migration"


	def migrateEMSRateIntoUnifiedRate(self):
		input = raw_input("Import EMS Rates into UnifiedRate table --> y/n ")
		if(input=='y'):
			for curEMSRate in self.__EMS6DataAccess.getEMS6EMSRates():
				self.__EMS7DataAccess.addEMSRatesToEMS7(curEMSRate)
		else:
			print "Aborting Migration"

#Should import Unified Rate first before migrating Extensible Rate
	def migrateExtensibleRateDayOfWeek(self):
		input = raw_input("Import Exensible Rate Day Of Week --> y/n     :")
		if(input=='y'):
			for curExtensibleRateDOW in self.__EMS6DataAccess.getEMS6ExtensibleRatesDayOfWeek():
				self.__EMS7DataAccess.addExtensibleRateDayOfWeekToEMS7(curExtensibleRateDOW)
		else:
			print "Aborting migration"

	def migrateRatesToEMS7ExtensibleRate(self):
		input = raw_input("Import EMSRate into EMS7 Extensible Rate --> y/n     :")
		if(input=='y'):
			for curExtensibleRate in self.__EMS6DataAccess.getEMS6ExtensibleRate():
				self.__EMS7DataAccess.addExtensibleRatesToEMS7(curExtensibleRate)
			else:
				print "Aborting migration"

	def migrateRestAccount(self):
		input = raw_input("Import RESTAccount --> y/n     :     ")
		if(input=='y'):
			for curRestAccount in self.__EMS6DataAccess.getEMS6RestAccount():
				self.__EMS7DataAccess.addRestAccountToEMS7(curRestAccount)
		else:
			print"Aborting Migration"
			
	def migrateLicencePlate(self):
		input = raw_input("import Licence Plate --> y/n    : ")
		if(input=='y'):
			for distinctPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				for curLicencePlate in self.__EMS6DataAccess.getEMS6LicencePlate(distinctPaystationId):
					self.__EMS7DataAccess.addLicencePlateToEMS7(curLicencePlate)

	def migrateMobileNumber(self):
		input = raw_input("import MobileNumber --> y/n")
		if(input=='y'):
			for curSMSAlertMobileNumber in self.__EMS6DataAccess.getEMS6MobileNumberFromSMSAlert():
				self.__EMS7DataAccess.addMobileNumberToEMS7(curSMSAlertMobileNumber)
			for curExtensiblePermitMobileNumber in self.__EMS6DataAccess.getEMS6MobileNumberFromEMSExtensiblePermit():
				self.__EMS7DataAccess.addMobileNumberToEMS7(curExtensiblePermitMobileNumber)
			for curSMSTransactionLog in self.__EMS6DataAccess.getEMS6MobileNumberFromSMSTransactionLog():
				self.__EMS7DataAccess.addMobileNumberToEMS7(curSMSTransactionLog)


	def migrateExtensiblePermit(self):
		input = raw_input("import Extensible Permit --> y/n        :")
		if(input=='y'):
			for curExtensiblePermit in self.__EMS6DataAccess.getEMS6ExtensiblePermit():
				self.__EMS7DataAccess.addExtensiblePermitToEMS7(curExtensiblePermit)

	def migrateSMSTransactionLog(self):
		input = raw_input("Import SMSTransactionLog --> y/n   : ")
		if(input=='y'):
			for curSMSTransactionLog in self.__EMS6DataAccess.getEMS6SMSTransactionLog():
				self.__EMS7DataAccess.addSMSTransactionLogToEMS7(curSMSTransactionLog)

	def migrateSMSAlert(self):
		input = raw_input("Import SMSAlert --> y/n ")
		if(input=='y'):
			for curSMSAlert in self.__EMS6DataAccess.getEMS6SMSAlert():
				self.__EMS7DataAccess.addSMSAlertToEMS7(curSMSAlert)
		else:
			print "Aborting Migration for SMSAlert" 

	def migrateSMSFailedResponse(self):
		input = raw_input("import SMSFailedResponse --> y/n")
		if(input=='y'):
			for curSMSFailedReponse in self.__EMS6DataAccess.getEMS6SMSFailedResponse():
				self.__EMS7DataAccess.addEMS6SMSFailedResponseToEMS7(curSMSFailedReponse)
		else:
			print "Aborting Migration"				
		
	def migrateModemSettings(self):
		input = raw_input("import ModemSetting --> y/n ")
		if (input=='y'):
			print " the answer is yes" 
			for curModemSetting in self.__EMS6DataAccess.getEMS6ModemSettings():
				self.__EMS7DataAccess.addModemSettingToEMS7(curModemSetting)
		else:
			print ("Aborting Migration")

	def migrateLotSettingContent(self):
		input = raw_input("Import Lot Setting File Content --> y/n")
		if (input=='y'):
			for curLotSettingContent in self.__EMS6DataAccess.getEMS6LotSettingContent():
				self.__EMS7DataAccess.addEMS6LotSettingContentToEMS7(curLotSettingContent)

	def migrateCryptoKey(self):
		input = raw_input("Import CryptoKey --> y/n      :")
		if(input=='y'):
			for curCryptoKey in self.__EMS6DataAccess.getEMS6CryptoKey():
				self.__EMS7DataAccess.addCryptoKeyToEMS7(curCryptoKey)
		else:
			print "Aborting Migration for CryptoKey"

	def migrateRestLog(self):
		input = raw_input("import RestLog table --> y/n  ")
		if(input=='y'):
			for curRestLog in self.__EMS6DataAccess.getEMS6RestLogs():
				self.__EMS7DataAccess.addRestLogToEMS7(curRestLog)
		else:
			print "Aborting Migration"

	def migrateRestSessionToken(self):
		input = raw_input(" import RestSessionToken --> y/n  ")
		if (input=='y') :
			for curRestSessionToken in self.__EMS6DataAccess.getEMS6RestSessionToken():
				self.__EMS7DataAccess.addRestSessionToken(curRestSessionToken)
		else:
			print ("Aborting Migration!")

			self.__EMS7Connection.commit()
		
	def migrateRestLogTotalCall(self):
		input = raw_input(" import RestLogTotalCall --> y/n  ")
		if (input=='y') :
			for curRestLogTotalCall in self.__EMS6DataAccess.getEMS6RestLogTotalCall():
				self.__EMS7DataAccess.addRestLogTotalCall(curRestLogTotalCall)
		else:
			print ("Aborting Migration!")

	def migrateCustomerWsCal(self):
		input = raw_input(" import CustomerWsCal --> y/n  ")
		if (input=='y') :
			for curCustomerWsCal in self.__EMS6DataAccess.getEMS6CustomerWsCal():
				self.__EMS7DataAccess.addCustomerWsCal(curCustomerWsCal)
		else:
			print ("Aborting Migration!")


	def migrateCustomerWsToken(self):
                input = raw_input(" import CustomerWsToken --> y/n  ")
                if (input=='y') :
                        for curCustomerWsToken in self.__EMS6DataAccess.getEMS6CustomerWsToken():
                                self.__EMS7DataAccess.addCustomerWsTokenToEMS7(curCustomerWsToken)
                else:
                        print ("Aborting Migration!")

# I have left this section as is because this section is referencing the EMS6 and EMS7Cursor within the same module. It looks like this table is not going to change much from initial migration.
	def migrateCustomerAlert(self):
		input = raw_input("import Customer Alert --> y/n")		
		if(input=='y'):
			for curCustomerAlert in self.__EMS6DataAccess.getEMS6CustomerAlert():
				self.__EMS7DataAccess.addCustomerAlertToEMS7(curCustomerAlert)
		else:
			print ("Aborting Migration for CustomerAlert")
	
	def migrateCustomerSubscription(self):
		input = raw_input("Import Customer Subscription --> y/n    :")
		if (input == 'y'):
			Action='Insert'	
			for curCustomerSubscription in self.__EMS6DataAccess.getEMS6CustomerSubscription():
				self.__EMS7DataAccess.addCustomerSubscriptionToEMS7(curCustomerSubscription,Action)
			self.__EMS7DataAccess.CustomerSubscriptionDisable()

	def migrateCustomerWebServiceCal(self):
		input = raw_input("Import Customer Web Service Cal --> y/n    :")
		if (input == 'y'):
			for curCustomerWebServiceCal in self.__EMS6DataAccess.getEMS6CustomerWebServiceCal():
				self.__EMS7DataAccess.addCustomerWebServiceCalToEMS7(curCustomerWebServiceCal)

	def migrateCustomer(self,CustomerWsToken):
		input = raw_input("Import Customer WebService Token --> y/n    :")
		if (input == 'y'):
			for curCustomerWsToken in self.__EMS6DataAccess.getEMS6CustomerWsToken():
				self.__EMS7DataAccess.addCustomerWsTokenToEMS7(curCustomerWsToken)

	def migrateRates(self):
		input  = raw_input("Import Rates --> y/n      :")
		if (input =='y'):
			for curRates in self.__EMS6DataAccess.getEMS6Rates():
				self.__EMS7DataAccess.addRatesToEMS7(curRates)

	def migrateReplenish(self):
		input = raw_input(" import Replenish --> y/n   :")
		if (input == 'y'):
			for curReplenish in self.__EMS6DataAccess.getEMS6Replenish():
				self.__EMS7DataAccess.addReplenishToEMS7(curReplenish)

	def migrateCardRetryTransaction(self):
		input= raw_input("Import Card Retry Transaction --> y/n   :")
		if (input == 'y'):
			for curCardRetryTransaction in self.__EMS6DataAccess.getEMS6CardRetryTransaction():
				self.__EMS7DataAccess.addCardRetryTransactionToEMS7(curCardRetryTransaction)

	def migrateCustomerCardType(self):
		input  = raw_input(" import Customer Card Type --> y/n   :")
		if (input == 'y'):
			for curCustomerCardType in self.__EMS6DataAccess.getEMS6CardType():
				self.__EMS7DataAccess.addCustomerCardTypeToEMS7(curCustomerCardType)

	def migratePaystationGroup(self):
		input = raw_input("import PaystationGroup --> y/n   :")
		if(input=='y'):
			for curPaystationGroup in self.__EMS6DataAccess.getEMS6PaystationGroup():
				self.__EMS7DataAccess.addPaystationGroupToEMS7(curPaystationGroup)

	def migrateCollectionType(self):
		input = raw_input("import CollectionType --> y/n   :")
		if(input=='y'):
			for curCollectionType in self.__EMS6DataAccess.getEMS6CollectionType():
				self.__EMS7DataAccess.addCollectionTypeToEMS7(curCollectionType)

	def migrateBatteryInfo(self):
		input = raw_input("Import BatteryInfo --> y/n    :")
		if(input=='y'):
			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationForBatteryInfo():
				for curBatteryInfo in self.__EMS6DataAccess.getBatteryInfo(curPaystationId):
					self.__EMS7DataAccess.addBatteryInfoToEMS7(curBatteryInfo)

	def migrateValueSmartCardData(self):
		input = raw_input ("import Value Card and Smart Card Data --> y/n    :")
		if(input=='y'):
			for curVSCardData in self.__EMS6DataAccess.getEMS6ValueSmartCardType():
				self.__EMS7DataAccess.addValueCardSmartCardToEMS7(curVSCardData)

 	def migratePurchase(self):
		input = raw_input ("import Purchase --> y/n    :")
		if (input=='y'):
			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				for curTransaction in self.__EMS6DataAccess.getEMS6Transaction(curPaystationId):
					self.__EMS7DataAccess.addPurchaseToEMS7(curTransaction)

	def migrateCustomerEmail(self):
		input = raw_input ("import CustomerAlertEmail --> y/n   :")
		if(input=='y'):
			for curCustomerEmail in self.__EMS6DataAccess.getCustomerEmail():
				self.__EMS7DataAccess.addCustomerEmail(curCustomerEmail)

	def migrateProcessorProperties(self):
		input = raw_input ("import ProcessorProperties --> y/n    :")
		if(input=='y'):
			for curProcessorProperties in self.__EMS6DataAccess.getProcessorProperties():
				self.__EMS7DataAccess.addProcessorProperties(curProcessorProperties)

	def migrateMerchantAccount(self):
		input = raw_input ("import MerchantAccount --> y/n    :")
		if(input=='y'):
			for curMerchantAccount in self.__EMS6DataAccess.getMerchantAccount():
				self.__EMS7DataAccess.addMerchantAccount(curMerchantAccount)

	def migratePreAuth(self):
		input = raw_input ("import PreAuth --> y/n     :")
		if(input=='y'):
			for curPreAuth in self.__EMS6DataAccess.getPreAuth():
				self.__EMS7DataAccess.addPreAuth(curPreAuth)

	def migratePreAuthHolding(self):
		input = raw_input ("import PreAuthHolding --> y/n    :")
		if(input=='y'):
			for curPreAuthHolding in self.__EMS6DataAccess.getPreAuthHolding():
				self.__EMS7DataAccess.addPreAuthHolding(curPreAuthHolding)

	def migrateProcessorTransaction(self):
		input = raw_input ("import ProcessorTransaction --> y/n   :")
		if(input=='y'):
			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationIdFromProcessorTransaction():
				for curProcessorTransaction in self.__EMS6DataAccess.getProcessorTransaction(curPaystationId):
					self.__EMS7DataAccess.addProcessorTransaction(curProcessorTransaction)
 	
	def migratePermit(self):
		input = raw_input ("import Permit --> y/n   :")
		if (input=='y'):
			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				for curTransaction in self.__EMS6DataAccess.getEMS6TransactionForPermit(curPaystationId):
					self.__EMS7DataAccess.addPermitToEMS7(curTransaction)

	def migratePaymentCard(self):
		input = raw_input ("import Payment -->  y/n :")
		if (input=='y'):
			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				for curTransaction in self.__EMS6DataAccess.getEMS6TransactionForPayment(curPaystationId):
					self.__EMS7DataAccess.addPaymentCardToEMS7(curTransaction)

	def migrateUserAccount(self):
		input = raw_input ("import UserAccount --> y/n  :")
		if (input=='y'):
			for curUserAccount in self.__EMS6DataAccess.getEMS6UserAccount():
				self.__EMS7DataAccess.addUserAccountToEMS7(curUserAccount)

	def migrateEventLogNewIntoPOSAlert(self):
		input = raw_input ("import EventNew Log into POS Alert --> y/n :")
		if (input == 'y'):
			for curPaystationId in  self.__EMS6DataAccess.getDistinctPaystationIdForEventLogNew():
				for curPOSAlert in self.__EMS6DataAccess.getEventLogNew(curPaystationId):
					self.__EMS7DataAccess.addPOSAlertToEMS7(curPOSAlert)
	
	def migrateTrialCustomer(self):
		Action='Insert'
		input = raw_input ("import Trial Customer  --> y/n :")
		if(input == 'y'):
			for curTrialCustomer in self.__EMS6DataAccess.getEMS6CerberusTrialCustomer():
				self.__EMS7DataAccess.addTrialCustomerToEMS7(curTrialCustomer, Action)

	def migrateRegionPaystationLog(self):
		Action='Insert'
		input = raw_input ("import Region Paystation Log --> y/n :")
		if(input == 'y'):
			for curRegionPaystationLog in self.__EMS6DataAccess.getEMS6RegionPaystationLog():
				self.__EMS7DataAccess.addLocationPOSLogToEMS7(curRegionPaystationLog, Action)

	def migrateCollectionLock(self):
		Action='Insert'
		input = raw_input ("import Collection Lock --> y/n :")
		if(input == 'y'):
			for curCollectionLock in self.__EMS6DataAccess.getEMS6CollectionLock():
				self.__EMS7DataAccess.addCollectionLockToEMS7(curCollectionLock, Action)

	def migratePaystationBalance(self):
		Action='Insert'
		input = raw_input("import PaystationBalance --> y/n  :")
		if(input == 'y'):
			for curPaystationBalance in self.__EMS6DataAccess.getEMS6PaystationBalance():
				self.__EMS7DataAccess.addPOSBalanceToEMS7(curPaystationBalance, Action)
	
	def migrateClusterMember(self):
		Action='Insert'
		input = raw_input("import ClusterMember  --> y/n :")
		if(input == 'y'):
			for curClusterMember in self.__EMS6DataAccess.getEMS6ClusterMember():
				self.__EMS7DataAccess.addClusterToEMS7(curClusterMember, Action)

	def migratePOSHeartBeat(self):
		Action='Insert'
		input = raw_input("import  Paystation Heart Beat --> y/n :")
		if(input == 'y'):
			for curPOSHeartBeat in self.__EMS6DataAccess.getEMS6PaystationHeartBeat():
				self.__EMS7DataAccess.addPOSHeartBeatToEMS7(curPOSHeartBeat, Action)

	def migratePOSServiceState(self):
		Action='Insert'
		input = raw_input("import  Service State --> y/n :")
		if(input == 'y'):
			for curPOSServiceState in self.__EMS6DataAccess.getEMS6ServiceState():
				self.__EMS7DataAccess.addPOSServiceStateToEMS7(curPOSServiceState, Action)

	def migrateActivePOSAlert(self):
		Action='Insert'
		input = raw_input ("import ActivePOSAlert  --> y/n :")
		if(input == 'y'):
			for curActivePOSAlert in self.__EMS6DataAccess.getEMS6PaystationAlert():
				self.__EMS7DataAccess.addActivePOSAlertToEMS7(curActivePOSAlert, Action)
			self.__EMS7DataAccess.addActivePOSAlertForPaytatationAlert()
			self.__EMS7DataAccess.addActivePOSAlertForUserDefinedAlert()

	def migratePOSDateiBilling(self):
		input = raw_input ("import POSDate Billing -->  y/n :")
		if (input=='y'):
			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				for curBillingReport in self.__EMS6DataAccess.getEMS6BillingReport(curPaystationId):
					self.__EMS7DataAccess.addPOSDateBillingReportToEMS7(curBillingReport)

	def migratePOSDateAuditAccess(self):
		input = raw_input ("import POSDate AuditAccess -->  y/n :")
		if (input=='y'):
			for curPaystationId in self.__EMS6DataAccess.getDistinctPaystationId():
				for curAuditAccess in self.__EMS6DataAccess.getEMS6AuditAccess(curPaystationId):
					self.__EMS7DataAccess.addPOSDateAuditAccessToEMS7(curAuditAccess)

	def migratePaystation(self):
		input = raw_input ("import Paystation Properties into POSDate --> y/n :")
		if (input=='y'):
			for curPaystation in self.__EMS6DataAccess.getEMS6PaystationForPOSDate():
				self.__EMS7DataAccess.addPOSDateAuditAccessToEMS7(curPaystation)

	def migrateBadValueCard(self):
		input = raw_input ("import Bad Value Card --> y/n :")
		if (input=='y'):
			for curBadValueCard in self.__EMS6DataAccess.getEMS6BadValueCard():
				self.__EMS7DataAccess.addBadValueCardToEMS7(curBadValueCard)

	def migrateBadCreditCard(self):
		input = raw_input ("import Bad Credit Card --> y/n :")
		if (input=='y'):
			for curBadCreditCard in self.__EMS6DataAccess.getEMS6BadCreditCard():
				self.__EMS7DataAccess.addBadCreditCardToEMS7(curBadCreditCard)

	def migrateBadSmartCard(self):
		input = raw_input ("import Bad Smart Card --> y/n :")
		if (input=='y'):
			for curBadSmartCard in self.__EMS6DataAccess.getEMS6BadSmartCard():
				self.__EMS7DataAccess.addBadSmartCardToEMS7(curBadSmartCard)

	def migrateParentRegionIdToLocation(self):
		Action='Insert'
		input = raw_input ("Add Parent Region to Location  --> y/n :")
		if (input=='y'):
			for curParentRegion in self.__EMS6DataAccess.getEMS6Locations():
				self.__EMS7DataAccess.addParentRegionIdToLocation(curParentRegion, Action)

