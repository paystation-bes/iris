package com.digitalpaytech.service;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.digitalpaytech.domain.FlexLocationFacility;
import com.digitalpaytech.domain.FlexWSUserAccount;
import com.digitalpaytech.dto.PermitInfo;
import com.digitalpaytech.dto.customeradmin.TransactionDetail;
import com.digitalpaytech.dto.customeradmin.TransactionReceiptDetails;
import com.digitalpaytech.dto.dataset.CitationMapQueryDataSet;
import com.digitalpaytech.dto.dataset.CitationRawQueryDataSet;
import com.digitalpaytech.dto.dataset.CitationWidgetQueryDataSet;
import com.digitalpaytech.dto.dataset.ContraventionTypeQueryDataSet;
import com.digitalpaytech.dto.dataset.ContraventionTypeQueryDataSet.ViolationFlexRecord;
import com.digitalpaytech.dto.dataset.FacilityQueryDataSet;
import com.digitalpaytech.dto.dataset.FacilityQueryDataSet.FacilityFlexRecord;
import com.digitalpaytech.dto.dataset.PermitQueryDataSet;
import com.digitalpaytech.dto.dataset.PropertyQueryDataSet;
import com.digitalpaytech.dto.dataset.PropertyQueryDataSet.PropertyFlexRecord;
import com.digitalpaytech.exception.T2FlexException;
import com.digitalpaytech.util.RandomKeyMapping;

public interface T2FlexWsService {
    FacilityQueryDataSet getFacilities(Date startTime, Date endTime, FlexWSUserAccount flexAccount) throws T2FlexException;
    
    Map<Integer, FacilityFlexRecord> getFacilitiesMap(Date startTime, Date endTime, FlexWSUserAccount flexAccount) throws T2FlexException;
    
    PropertyQueryDataSet getProperties(Date startTime, Date endTime, FlexWSUserAccount flexAccount) throws T2FlexException;
    
    Map<Integer, PropertyFlexRecord> getPropertiesMap(Date startTime, Date endTime, FlexWSUserAccount flexAccount) throws T2FlexException;
    
    ContraventionTypeQueryDataSet getCitationTypes(Date startTime, Date endTime, FlexWSUserAccount flexAccount) throws T2FlexException;
    
    Map<Integer, ViolationFlexRecord> getCitationTypeMap(Date startTime, Date endTime, FlexWSUserAccount flexAccount) throws T2FlexException;
    
    PermitQueryDataSet getPermits(PermitInfo permitInfo, List<FlexLocationFacility> flfList) throws T2FlexException;
    
    List<TransactionDetail> getPermits(PermitInfo permitInfo, DateFormat format, RandomKeyMapping keyMapping, List<FlexLocationFacility> flfList)
        throws T2FlexException;
    
    TransactionReceiptDetails populateTransactionReceiptDetails(TransactionDetail transactionDetail, RandomKeyMapping keyMapping);
    
    boolean isFlexAvailable(FlexWSUserAccount flexAccount) throws T2FlexException;
    
    CitationWidgetQueryDataSet getCitationData(Date startTime, Date endTime, FlexWSUserAccount flexAccount) throws T2FlexException;
    
    CitationMapQueryDataSet getCitationMapData(Date startTime, Date endTime, FlexWSUserAccount flexAccount) throws T2FlexException;
    
    CitationRawQueryDataSet getCitationRawData(Date startTime, Date endTime, Integer conUid, Integer rowCount, FlexWSUserAccount flexAccount) throws T2FlexException;
}
