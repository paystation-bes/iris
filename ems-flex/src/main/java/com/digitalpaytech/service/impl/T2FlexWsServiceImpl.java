package com.digitalpaytech.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.FlexDataWSCallStatus;
import com.digitalpaytech.domain.FlexETLDataType;
import com.digitalpaytech.domain.FlexLocationFacility;
import com.digitalpaytech.domain.FlexWSUserAccount;
import com.digitalpaytech.dto.PermitInfo;
import com.digitalpaytech.dto.customeradmin.TransactionDetail;
import com.digitalpaytech.dto.customeradmin.TransactionReceiptDetails;
import com.digitalpaytech.dto.dataset.CitationMapQueryDataSet;
import com.digitalpaytech.dto.dataset.CitationMapQueryDataSet.CitationMapData;
import com.digitalpaytech.dto.dataset.CitationRawQueryDataSet;
import com.digitalpaytech.dto.dataset.CitationRawQueryDataSet.CitationRawData;
import com.digitalpaytech.dto.dataset.CitationWidgetQueryDataSet;
import com.digitalpaytech.dto.dataset.CitationWidgetQueryDataSet.CitationWidgetFlexRecord;
import com.digitalpaytech.dto.dataset.ContraventionTypeQueryDataSet;
import com.digitalpaytech.dto.dataset.ContraventionTypeQueryDataSet.ViolationFlexRecord;
import com.digitalpaytech.dto.dataset.FacilityQueryDataSet;
import com.digitalpaytech.dto.dataset.FacilityQueryDataSet.FacilityFlexRecord;
import com.digitalpaytech.dto.dataset.FlexInfoQueryDataSet;
import com.digitalpaytech.dto.dataset.PermitQueryDataSet;
import com.digitalpaytech.dto.dataset.PermitQueryDataSet.PermitFlexRecord;
import com.digitalpaytech.dto.dataset.PropertyQueryDataSet;
import com.digitalpaytech.dto.dataset.PropertyQueryDataSet.PropertyFlexRecord;
import com.digitalpaytech.exception.T2FlexException;
import com.digitalpaytech.helper.QueryDataSetConverter;
import com.digitalpaytech.service.FlexWSService;
import com.digitalpaytech.service.T2FlexWsService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.FlexConstants;
import com.digitalpaytech.util.LabelConstants;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.PooledResource;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.T2FlexUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.t2systems.ArrayOfQueryParameter;
import com.t2systems.QueryParameter;
import com.t2systems.T2FlexMiscSoap;

@Service("t2FlexWsService")
public class T2FlexWsServiceImpl implements T2FlexWsService {
    
    private static Logger log = Logger.getLogger(T2FlexWsServiceImpl.class);
    
    @Autowired
    private QueryDataSetConverter queryDataSetConverter;
    
    @Autowired
    private FlexWSService flexWSService;
    
    @Autowired
    private MessageHelper messages;
    
    @Override
    // This method needs to return an empty FacilityQueryDataSet no matter what happens
    public final FacilityQueryDataSet getFacilities(final Date startTime, final Date endTime, final FlexWSUserAccount flexAccount)
        throws T2FlexException {
        final FlexDataWSCallStatus callStatus = new FlexDataWSCallStatus();
        callStatus.setCustomer(flexAccount.getCustomer());
        callStatus.setFlexETLDataType(new FlexETLDataType(FlexConstants.WS_TYPE_FACILITY));
        
        callStatus.setRequestedFromDateGmt(startTime);
        callStatus.setRequestedToDateGmt(endTime);
        
        final PooledResource<DateFormat> dateFmt = DateUtil.takeDateFormat(DateUtil.UI_FORMAT, DateUtil.GMT);
        
        final List<QueryParameter> parameters = new ArrayList<QueryParameter>();
        parameters.add(param(FlexConstants.PARAMETER_START_DATE, dateFmt.get().format(startTime.getTime())));
        parameters.add(param(FlexConstants.PARAMETER_END_DATE, dateFmt.get().format(endTime.getTime())));
        
        final String responseXml = executeQueryOnFlex(FlexConstants.QUERY_FACILITY, parameters, flexAccount, callStatus);
        
        final FacilityQueryDataSet facilities;
        final boolean resultDataSetFlag = T2FlexUtil.hasResultDataSetXml(responseXml);
        if (resultDataSetFlag) {
            facilities = this.queryDataSetConverter.convertFacility(responseXml);
        } else {
            facilities = new FacilityQueryDataSet();
            facilities.setFacilityFlexRecords(new ArrayList<FacilityFlexRecord>());
        }
        
        facilities.setCallStatusId(callStatus.getId());
        return facilities;
    }
    
    @Override
    public final Map<Integer, FacilityFlexRecord> getFacilitiesMap(final Date startTime, final Date endTime, final FlexWSUserAccount flexAccount)
        throws T2FlexException {
        final FacilityQueryDataSet facilityQueryDataSet = this.getFacilities(startTime, endTime, flexAccount);
        final List<FacilityFlexRecord> facList = facilityQueryDataSet.getFacilityFlexRecords();
        final Map<Integer, FacilityFlexRecord> facMap = new HashMap<Integer, FacilityFlexRecord>();
        for (FacilityFlexRecord fqds : facList) {
            facMap.put(fqds.getUid(), fqds);
        }
        
        return facMap;
    }
    
    @Override
    // This method needs to return an empty FacilityQueryDataSet no matter what happens
    @SuppressWarnings("checkstyle:illegalcatch")
    public final PropertyQueryDataSet getProperties(final Date startTime, final Date endTime, final FlexWSUserAccount flexAccount)
        throws T2FlexException {
        final FlexDataWSCallStatus callStatus = new FlexDataWSCallStatus();
        callStatus.setCustomer(flexAccount.getCustomer());
        callStatus.setFlexETLDataType(new FlexETLDataType(FlexConstants.WS_TYPE_PROPERTY));
        
        callStatus.setRequestedFromDateGmt(startTime);
        callStatus.setRequestedToDateGmt(endTime);
        
        final PooledResource<DateFormat> dateFmt = DateUtil.takeDateFormat(DateUtil.UI_FORMAT, DateUtil.GMT);
        
        final List<QueryParameter> parameters = new ArrayList<QueryParameter>();
        parameters.add(param(FlexConstants.PARAMETER_START_DATE, dateFmt.get().format(startTime.getTime())));
        parameters.add(param(FlexConstants.PARAMETER_END_DATE, dateFmt.get().format(endTime.getTime())));
        
        final String responseXml = executeQueryOnFlex(FlexConstants.QUERY_PROPERTY, parameters, flexAccount, callStatus);
        
        final PropertyQueryDataSet properties;
        final boolean resultDataSetFlag = T2FlexUtil.hasResultDataSetXml(responseXml);
        if (resultDataSetFlag) {
            properties = this.queryDataSetConverter.convertProperty(responseXml);
        } else {
            properties = new PropertyQueryDataSet();
            properties.setPropertyFlexRecords(new ArrayList<PropertyFlexRecord>());
        }
        
        properties.setCallStatusId(callStatus.getId());
        return properties;
    }
    
    @Override
    public final Map<Integer, PropertyFlexRecord> getPropertiesMap(final Date startTime, final Date endTime, final FlexWSUserAccount flexAccount)
        throws T2FlexException {
        final PropertyQueryDataSet propertyQueryDataSet = this.getProperties(startTime, endTime, flexAccount);
        final List<PropertyFlexRecord> propList = propertyQueryDataSet.getPropertyFlexRecords();
        final Map<Integer, PropertyFlexRecord> propMap = new HashMap<Integer, PropertyFlexRecord>();
        for (PropertyFlexRecord pfr : propList) {
            propMap.put(pfr.getUid(), pfr);
        }
        
        return propMap;
    }
    
    @Override
    // This method needs to return an empty FacilityQueryDataSet no matter what happens
    @SuppressWarnings("checkstyle:illegalcatch")
    public final ContraventionTypeQueryDataSet getCitationTypes(final Date startTime, final Date endTime, final FlexWSUserAccount flexAccount)
        throws T2FlexException {
        final FlexDataWSCallStatus callStatus = new FlexDataWSCallStatus();
        callStatus.setCustomer(flexAccount.getCustomer());
        callStatus.setFlexETLDataType(new FlexETLDataType(FlexConstants.WS_TYPE_CITATION_TYPE));
        
        callStatus.setRequestedFromDateGmt(startTime);
        callStatus.setRequestedToDateGmt(endTime);
        
        final PooledResource<DateFormat> dateFmt = DateUtil.takeDateFormat(DateUtil.UI_FORMAT, DateUtil.GMT);
        
        final List<QueryParameter> parameters = new ArrayList<QueryParameter>();
        parameters.add(param(FlexConstants.PARAMETER_START_DATE, dateFmt.get().format(startTime.getTime())));
        parameters.add(param(FlexConstants.PARAMETER_END_DATE, dateFmt.get().format(endTime.getTime())));
        
        final String responseXml = executeQueryOnFlex(FlexConstants.QUERY_CONTRAVENTION_TYPE, parameters, flexAccount, callStatus);
        
        final ContraventionTypeQueryDataSet citationTypes;
        final boolean resultDataSetFlag = T2FlexUtil.hasResultDataSetXml(responseXml);
        if (resultDataSetFlag) {
            citationTypes = this.queryDataSetConverter.convertContraventionType(responseXml);
        } else {
            citationTypes = new ContraventionTypeQueryDataSet();
            citationTypes.setViolationFlexRecords(new ArrayList<ViolationFlexRecord>());
        }
        
        citationTypes.setCallStatusId(callStatus.getId());
        return citationTypes;
    }
    
    @Override
    public final Map<Integer, ViolationFlexRecord> getCitationTypeMap(final Date startTime, final Date endTime, final FlexWSUserAccount flexAccount)
        throws T2FlexException {
        final ContraventionTypeQueryDataSet citationTypeQueryDataSet = this.getCitationTypes(startTime, endTime, flexAccount);
        final List<ViolationFlexRecord> citationTypeList = citationTypeQueryDataSet.getViolationFlexRecords();
        final Map<Integer, ViolationFlexRecord> citationTypeMap = new HashMap<Integer, ViolationFlexRecord>();
        for (ViolationFlexRecord pfr : citationTypeList) {
            citationTypeMap.put(pfr.getViolationUid(), pfr);
        }
        return citationTypeMap;
    }
    
    @Override
    public final PermitQueryDataSet getPermits(final PermitInfo permitInfo, final List<FlexLocationFacility> flfList) throws T2FlexException {
        
        // ----------- For debugging performance only ------------
        long startTime = 0;
        if (log.isDebugEnabled()) {
            startTime = System.currentTimeMillis();
        }
        // ----------- For debugging performance only ------------
        
        String flexPermitQueryName = FlexConstants.QUERY_PERMITS;
        
        final List<QueryParameter> listQueryParameters = new ArrayList<QueryParameter>();
        // Start Date
        listQueryParameters.add(createQueryParameter(FlexConstants.PARAMETER_START_DATE, formatDate(permitInfo.getStartDate())));
        // End Date
        listQueryParameters.add(createQueryParameter(FlexConstants.PARAMETER_END_DATE, formatDate(permitInfo.getEndDate())));
        // Max Last Updated Timestamp
        listQueryParameters.add(createQueryParameter(FlexConstants.PARAMETER_MAX_LAST_UPDATED_TIME, formatDate(permitInfo.getMaxLastUpdatedTime())));
        // Licence Plate
        if (StringUtils.isBlank(permitInfo.getLicencePlate())) {
            permitInfo.setLicencePlate(WebCoreConstants.PERCENT_SIGN);
        }
        listQueryParameters.add(createQueryParameter(FlexConstants.PARAMETER_LICENCE_PLATE, permitInfo.getLicencePlate()));
        // Start Row
        listQueryParameters.add(createQueryParameter(FlexConstants.PARAMETER_START_ROW, String.valueOf(permitInfo.getStartRow())));
        // End Row
        listQueryParameters.add(createQueryParameter(FlexConstants.PARAMETER_END_ROW,
                                                     String.valueOf(permitInfo.getEndRow() + FlexConstants.PERMIT_RESULT_SET_OFFSET)));
        
        String facUids = "";
        for (FlexLocationFacility flf : flfList) {
            facUids += "," + flf.getFacUid();
        }
        if (facUids.length() > 0) {
            flexPermitQueryName = FlexConstants.QUERY_PERMITS_IN_FACILITIES;
            facUids = facUids.substring(1);
            listQueryParameters.add(createQueryParameter(FlexConstants.PARAMETER_FAC_ID, facUids));
        }
        
        final FlexWSUserAccount flexWSData = this.flexWSService.findFlexWSUserAccountByCustomerId(permitInfo.getCustomerId());
        
        String responseXml = null;
        responseXml = executeQueryOnFlex(flexPermitQueryName, listQueryParameters, flexWSData, null);
        final PermitQueryDataSet permits;
        final boolean resultDataSetFlag = T2FlexUtil.hasResultDataSetXml(responseXml);
        if (resultDataSetFlag) {
            permits = this.queryDataSetConverter.convertPermit(responseXml);
        } else {
            permits = new PermitQueryDataSet();
            permits.setPermitFlexRecords(new ArrayList<PermitFlexRecord>());
        }
        
        // ----------- For debugging performance only ------------
        if (log.isDebugEnabled()) {
            final long processingTime = (System.currentTimeMillis() - startTime) / StandardConstants.SECONDS_IN_MILLIS_1;
            final StringBuilder bdr = new StringBuilder();
            bdr.append("\r\n\r\nReturn from calling T2 Flex WS '").append(flexPermitQueryName).append("' (");
            bdr.append(permits.getPermitFlexRecords().size()).append(" records) after ").append(processingTime).append(" seconds. \r\n\r\n");
            log.debug(bdr.toString());
        }
        // ----------- For debugging performance only ------------
        
        return permits;
    }
    
    @Override
    public final List<TransactionDetail> getPermits(final PermitInfo permitInfo, final DateFormat format, final RandomKeyMapping keyMapping,
        final List<FlexLocationFacility> flfList) throws T2FlexException {
        
        // ----------- For debugging performance only ------------
        long startTime = 0;
        if (log.isDebugEnabled()) {
            startTime = System.currentTimeMillis();
        }
        // ----------- For debugging performance only ------------        
        
        final PermitQueryDataSet permitQueryDataSet = getPermits(permitInfo, flfList);
        if (permitQueryDataSet.getPermitFlexRecords().isEmpty()) {
            return new ArrayList<TransactionDetail>();
        }
        
        final List<TransactionDetail> list = new ArrayList<TransactionDetail>(permitQueryDataSet.getPermitFlexRecords().size());
        PermitFlexRecord rec = null;
        final Iterator<PermitFlexRecord> iter = permitQueryDataSet.getPermitFlexRecords().iterator();
        while (iter.hasNext()) {
            rec = iter.next();
            final TransactionDetail detail = new TransactionDetail();
            detail.setPurchaseRandomId(keyMapping.getRandomString(PermitFlexRecord.class, rec.getPermitId()));
            detail.setPermitId(Long.valueOf(rec.getPermitId()));
            detail.setLicencePlateNumber(rec.getVehiclePlateLicense());
            detail.setPurchaseDate(format.format(rec.getPermitEffectiveStartDate()));
            detail.setPermitExpiryDate(format.format(rec.getPermitEffectiveEndDate()));
            detail.setExpiryDate(rec.getPermitEffectiveEndDate());
            detail.setPermitNumber(rec.getPermitNumber());
            detail.setFacilityName(rec.getFacilityName());
            detail.setIsActive(rec.isActive());
            detail.setIsFlex(Boolean.TRUE);
            detail.setIsDestroyed(rec.isDestroyed());
            detail.setIsLost(rec.isLostStolen());
            detail.setIsCustody(rec.isMisscust());
            detail.setIsReturned(rec.isReturned());
            detail.setIsTerminated(rec.isTerminated());
            detail.setAccountName(rec.getAccountName());
            detail.setPaymentType(rec.getPaymentType());
            detail.setAmount(rec.getAmount() == null ? Float.valueOf(0) : rec.getAmount() * StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
            list.add(detail);
        }
        
        // ----------- For debugging performance only ------------
        if (log.isDebugEnabled()) {
            final long processingTime = (System.currentTimeMillis() - startTime) / StandardConstants.SECONDS_IN_MILLIS_1;
            final StringBuilder bdr = new StringBuilder();
            bdr.append("\r\n\r\nReturn from calling T2 Flex WS (").append(permitQueryDataSet.getPermitFlexRecords().size()).append(" records) ");
            bdr.append("and populating TransactionDetail objects after ").append(processingTime).append(" seconds.\r\n\r\n");
            log.debug(bdr.toString());
        }
        // ----------- For debugging performance only ------------
        
        return list;
    }
    
    @Override
    public final TransactionReceiptDetails populateTransactionReceiptDetails(final TransactionDetail transactionDetail,
        final RandomKeyMapping keyMapping) {
        final TransactionReceiptDetails receiptDetails = new TransactionReceiptDetails();
        receiptDetails.setPurchaseRandomId(keyMapping.getRandomString(PermitFlexRecord.class, transactionDetail.getPermitId()));
        receiptDetails.setLicensePlateNumber(transactionDetail.getLicencePlateNumber());
        
        final SimpleDateFormat format = new SimpleDateFormat(WebCoreConstants.DATE_FORMAT);
        final SimpleDateFormat formatNoTime = new SimpleDateFormat(WebCoreConstants.DATE_NO_TIME_FORMAT);
        final SimpleDateFormat formatOnlyTime = new SimpleDateFormat(WebCoreConstants.TIME_UI_FORMAT_TODAY);
        Date purchaseDate;
        try {
            purchaseDate = format.parse(transactionDetail.getPurchaseDate());
            receiptDetails.setPurchasedDate(formatNoTime.format(purchaseDate));
            receiptDetails.setPurchasedTime(formatOnlyTime.format(purchaseDate));
            
        } catch (ParseException pe) {
            log.error(pe);
            receiptDetails.setPurchasedDate(WebCoreConstants.EMPTY_STRING);
            receiptDetails.setPurchasedTime(WebCoreConstants.EMPTY_STRING);
        }
        Date permitExpiryDate;
        try {
            permitExpiryDate = format.parse(transactionDetail.getPermitExpiryDate());
            final Calendar cal = Calendar.getInstance();
            cal.setTime(permitExpiryDate);
            final int year = cal.get(Calendar.YEAR);
            if (year == FlexConstants.PERMIT_NO_EXPIRY_YEAR) {
                receiptDetails.setExpiryDate(this.messages.getMessage(LabelConstants.LABEL_REPORTING_TRANSACTIONSEARCH_NOEXPIRYDATE));
                receiptDetails.setExpiryTime(WebCoreConstants.EMPTY_STRING);
            } else {
                receiptDetails.setExpiryDate(formatNoTime.format(permitExpiryDate));
                receiptDetails.setExpiryTime(formatOnlyTime.format(permitExpiryDate));
            }
        } catch (ParseException pe) {
            log.error(pe);
            receiptDetails.setExpiryDate(WebCoreConstants.EMPTY_STRING);
            receiptDetails.setExpiryTime(WebCoreConstants.EMPTY_STRING);
        }
        receiptDetails.setIsFlex(transactionDetail.getIsFlex());
        receiptDetails.setPermitNumber(transactionDetail.getPermitNumber());
        receiptDetails.setFacilityName(transactionDetail.getFacilityName());
        receiptDetails.setIsActive(transactionDetail.getIsActive());
        receiptDetails.setIsDestroyed(transactionDetail.getIsDestroyed());
        receiptDetails.setIsLost(transactionDetail.getIsLost());
        receiptDetails.setIsCustody(transactionDetail.getIsCustody());
        receiptDetails.setIsReturned(transactionDetail.getIsReturned());
        receiptDetails.setIsTerminated(transactionDetail.getIsTerminated());
        receiptDetails.setAccountName(transactionDetail.getAccountName());
        receiptDetails.setPaymentType(transactionDetail.getPaymentType());
        receiptDetails.setOriginalAmount(transactionDetail.getAmount());
        return receiptDetails;
    }
    
    @SuppressWarnings("checkstyle:illegalcatch")
    @Override
    public final boolean isFlexAvailable(final FlexWSUserAccount flexAccount) throws T2FlexException {
        boolean error = false;
        
        String responseXml = "";
        if (flexAccount != null) {
            try {
                responseXml = executeQueryOnFlex(FlexConstants.QUERY_AVAILABILITY, flexAccount, (FlexDataWSCallStatus) null);
                error = !T2FlexUtil.hasQueryData(responseXml);
                if (!error) {
                    final FlexInfoQueryDataSet flexInfoSet = this.queryDataSetConverter.convertFlexInfo(responseXml);
                    if ((flexInfoSet != null) && (flexInfoSet.getFlexInfoRecords() != null) && (!flexInfoSet.getFlexInfoRecords().isEmpty())) {
                        flexAccount.setTimeZone(flexInfoSet.getFlexInfoRecords().get(0).getTimezone());
                    }
                }
            } catch (Exception e) {
                error = true;
                log.error(e.getMessage());
            }
        }
        return !error;
    }
    
    @Override
    public final CitationWidgetQueryDataSet getCitationData(final Date startTime, final Date endTime, final FlexWSUserAccount flexAccount)
        throws T2FlexException {
        CitationWidgetQueryDataSet convertedData = null;
        
        final FlexDataWSCallStatus callStatus = new FlexDataWSCallStatus();
        callStatus.setCustomer(flexAccount.getCustomer());
        callStatus.setFlexETLDataType(new FlexETLDataType(FlexConstants.WS_TYPE_CITATION_WIDGET));
        
        callStatus.setRequestedFromDateGmt(startTime);
        callStatus.setRequestedToDateGmt(endTime);
        
        final PooledResource<DateFormat> dateFmt = DateUtil.takeDateFormat(DateUtil.UI_FORMAT, DateUtil.GMT);
        
        final List<QueryParameter> parameters = new ArrayList<QueryParameter>();
        parameters.add(param(FlexConstants.PARAMETER_START_DATE, dateFmt.get().format(startTime)));
        parameters.add(param(FlexConstants.PARAMETER_END_DATE, dateFmt.get().format(endTime)));
        
        final String response = executeQueryOnFlex(FlexConstants.QUERY_CITATION_WIDGET, parameters, flexAccount, callStatus);
        if (T2FlexUtil.hasQueryData(response)) {
            convertedData = this.queryDataSetConverter.convertCitationWidget(response);
        } else {
            convertedData = new CitationWidgetQueryDataSet();
            convertedData.setCitationWidgetFlexRecords(new ArrayList<CitationWidgetFlexRecord>(0));
        }
        
        dateFmt.close();
        
        convertedData.setCallStatusId(callStatus.getId());
        return convertedData;
    }
    
    @Override
    public final CitationMapQueryDataSet getCitationMapData(final Date startTime, final Date endTime, final FlexWSUserAccount flexAccount)
        throws T2FlexException {
        CitationMapQueryDataSet convertedData = null;
        
        final FlexDataWSCallStatus callStatus = new FlexDataWSCallStatus();
        callStatus.setCustomer(flexAccount.getCustomer());
        callStatus.setFlexETLDataType(new FlexETLDataType(FlexConstants.WS_TYPE_CITATION_MAP));
        
        callStatus.setRequestedFromDateGmt(startTime);
        callStatus.setRequestedToDateGmt(endTime);
        
        final PooledResource<DateFormat> dateFmt = DateUtil.takeDateFormat(DateUtil.UI_FORMAT, DateUtil.GMT);
        
        final List<QueryParameter> parameters = new ArrayList<QueryParameter>();
        parameters.add(param(FlexConstants.PARAMETER_START_DATE, dateFmt.get().format(startTime)));
        // parameters.add(param(PARAMETER_START_DATE, new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(startTime.getTime())));
        parameters.add(param(FlexConstants.PARAMETER_END_DATE, dateFmt.get().format(endTime)));
        // parameters.add(param(PARAMETER_END_DATE, new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(endTime.getTime())));
        
        final String response = executeQueryOnFlex(FlexConstants.QUERY_CITATION_MAP, parameters, flexAccount, callStatus);
        if (T2FlexUtil.hasQueryData(response)) {
            convertedData = this.queryDataSetConverter.convertCitationMapData(response);
        } else {
            convertedData = new CitationMapQueryDataSet();
            convertedData.setCitationMapDataList(new ArrayList<CitationMapData>(0));
        }
        
        dateFmt.close();
        
        convertedData.setCallStatusId(callStatus.getId());
        return convertedData;
    }
    
    private QueryParameter param(final String field, final String value) {
        final QueryParameter result = new QueryParameter();
        result.setField(field);
        result.setValue(value);
        
        return result;
    }
    
    private String executeQueryOnFlex(final String queryName, final FlexWSUserAccount flexWSUserAccount, final FlexDataWSCallStatus callStatus)
        throws T2FlexException {
        return executeQueryOnFlex(queryName, new ArrayList<QueryParameter>(0), flexWSUserAccount, callStatus);
    }
    
    @SuppressWarnings("checkstyle:illegalcatch")
    private String executeQueryOnFlex(final String queryName, final List<QueryParameter> parameters, final FlexWSUserAccount flexWSUserAccount,
        final FlexDataWSCallStatus callStatus) throws T2FlexException {
        final JaxWsProxyFactoryBean factory = getWSFactory(flexWSUserAccount);
        if (factory == null) {
            return WebCoreConstants.EMPTY_STRING;
        }
        final T2FlexMiscSoap client = (T2FlexMiscSoap) factory.create();
        
        final ArrayOfQueryParameter parametersArr = new ArrayOfQueryParameter();
        parametersArr.setQueryParameter(parameters);
        
        String error = null;
        String result = null;
        
        if (callStatus != null) {
            callStatus.setStatus((byte) 0);
            callStatus.setWscallBeginGmt(new Date());
        }
        
        try {
            result = client.executeQueryByName(FlexConstants.FLEX_VERSION, flexWSUserAccount.getUsername(), flexWSUserAccount.getPassword(),
                                               queryName, parametersArr);
            
            error = T2FlexUtil.retrieveError(result);
        } finally {
            if (callStatus != null) {
                callStatus.setWscallEndGmt(new Date());
                if (error == null) {
                    callStatus.setStatus((byte) 1);
                }
                this.flexWSService.saveFlexDataWSCallStatus(callStatus);
            }
        }
        
        if (error != null) {
            throw new T2FlexException(error);
        }
        
        return result;
    }
    
    private QueryParameter createQueryParameter(final String field, final String value) {
        final QueryParameter param = new QueryParameter();
        param.setField(field);
        param.setValue(value);
        return param;
    }
    
    private String formatDate(final Date date) {
        String result = null;
        try (PooledResource<DateFormat> dateFormat = DateUtil.takeDateFormat(DateUtil.UI_FORMAT, DateUtil.GMT)) {
            result = dateFormat.get().format(date);
        }
        
        return result;
    }
    
    private JaxWsProxyFactoryBean getWSFactory(final FlexWSUserAccount flexWSUserAccount) {
        final JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(T2FlexMiscSoap.class);
        if (flexWSUserAccount == null) {
            return null;
        }
        factory.setAddress(flexWSUserAccount.getUrl());
        
        return factory;
    }
    
    public final void setQueryDataSetConverter(final QueryDataSetConverter queryDataSetConverter) {
        this.queryDataSetConverter = queryDataSetConverter;
    }
    
    public final void setFlexWSService(final FlexWSService flexWSService) {
        this.flexWSService = flexWSService;
    }
    
    @Override
    public final CitationRawQueryDataSet getCitationRawData(final Date startTime, final Date endTime, final Integer conUid, final Integer rowCount,
        final FlexWSUserAccount flexAccount) throws T2FlexException {
        CitationRawQueryDataSet convertedData = null;
        
        final FlexDataWSCallStatus callStatus = new FlexDataWSCallStatus();
        callStatus.setCustomer(flexAccount.getCustomer());
        callStatus.setFlexETLDataType(new FlexETLDataType(FlexConstants.WS_TYPE_CITATION));
        
        callStatus.setRequestedFromDateGmt(startTime);
        callStatus.setRequestedToDateGmt(endTime);
        
        final PooledResource<DateFormat> dateFmt = DateUtil.takeDateFormat(DateUtil.UI_FORMAT, DateUtil.GMT);
        
        final List<QueryParameter> parameters = new ArrayList<QueryParameter>();
        parameters.add(param(FlexConstants.PARAMETER_START_DATE, dateFmt.get().format(startTime.getTime())));
        parameters.add(param(FlexConstants.PARAMETER_END_DATE, dateFmt.get().format(endTime.getTime())));
        parameters.add(param(FlexConstants.PARAMETER_CON_ID, conUid.toString()));
        parameters.add(param(FlexConstants.PARAMETER_ROW_COUNT, rowCount.toString()));
        final String response = executeQueryOnFlex(FlexConstants.QUERY_CITATIONS, parameters, flexAccount, callStatus);
        if (T2FlexUtil.hasQueryData(response)) {
            convertedData = this.queryDataSetConverter.convertCitationRawData(response);
        } else {
            convertedData = new CitationRawQueryDataSet();
            convertedData.setCitationRawDataList(new ArrayList<CitationRawData>(0));
        }
        
        dateFmt.close();
        
        convertedData.setCallStatusId(callStatus.getId());
        return convertedData;
    }
}
