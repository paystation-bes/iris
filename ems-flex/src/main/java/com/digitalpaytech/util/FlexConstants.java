package com.digitalpaytech.util;

public final class FlexConstants {
    public static final String PARAMETER_START_DATE = "startDate";
    public static final String PARAMETER_END_DATE = "endDate";
    public static final String PARAMETER_MAX_LAST_UPDATED_TIME = "maxLastUpdatedTime";
    public static final String PARAMETER_LICENCE_PLATE = "licencePlate";
    public static final String PARAMETER_START_ROW = "startRow";
    public static final String PARAMETER_END_ROW = "endRow";
    public static final String PARAMETER_FAC_ID = "facId";
    public static final String PARAMETER_CON_ID = "conUIDMIN";
    public static final String PARAMETER_ROW_COUNT = "rowCount";
    
    public static final String PERMIT_ID_LOOK_UP_NAME = "permitId";
    public static final int PERMIT_NO_EXPIRY_YEAR = 9999;
    public static final int PERMIT_RESULT_SET_OFFSET = 100;
    
    public static final String FLEX_VERSION = "1.0";
    
    public static final String CITATION_QUERY_START_TIME_START = "01/01/";
    public static final String CITATION_QUERY_START_TIME_END = " 00:00:00";
    
    public static final String QUERY_FACILITY = "IRISWS-Facilities";
    public static final String QUERY_PROPERTY = "IRISWS-Properties";
    public static final String QUERY_CONTRAVENTION_TYPE = "IRISWS-CitationTypes";
    public static final String QUERY_CITATION_MAP = "IRISWS-CitationsMap";
    public static final String QUERY_CITATION_WIDGET = "IRISWS-CitationsCountByType";
    public static final String QUERY_PERMITS = "IRISWS-Permits";
    public static final String QUERY_PERMITS_IN_FACILITIES = "IRISWS-PermitsInFacilities";
    public static final String QUERY_AVAILABILITY = "IRISWS-Availability";
    public static final String QUERY_CITATIONS = "IRISWS-Citations";
    
    public static final Byte WS_TYPE_CITATION_WIDGET = 1;
    public static final Byte WS_TYPE_CITATION_MAP = 2;
    public static final Byte WS_TYPE_PERMIT = 3;
    public static final Byte WS_TYPE_CITATION_TYPE = 4;
    public static final Byte WS_TYPE_FACILITY = 5;
    public static final Byte WS_TYPE_PROPERTY = 6;
    public static final Byte WS_TYPE_CITATION = 7;
    
    public static final Byte WIDGET_TABLE_TYPE_HOUR = 1;
    public static final Byte WIDGET_TABLE_TYPE_DAY = 2;
    public static final Byte WIDGET_TABLE_TYPE_MONTH = 3;
    public static final Byte WIDGET_TABLE_TYPE_MAP = 4;
    
    public static final Integer NUMBER_OF_DAYS_HOUR = 2;
    public static final Integer NUMBER_OF_DAYS_DAY = 63;
    public static final Integer NUMBER_OF_DAYS_MAP = 8;
    
    private FlexConstants() {
    }
}
