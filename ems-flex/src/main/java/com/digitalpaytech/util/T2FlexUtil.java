package com.digitalpaytech.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.digitalpaytech.exception.T2FlexException;

public final class T2FlexUtil {
    private static final int REGEX_GROUP_ERROR_MESSAGE = 2;
    
    private static Pattern regexError = Pattern.compile("<\\s*ErrorDescription(\\s+[^\\/]*)*>(.*)<\\/\\s*ErrorDescription(\\s+[^\\/]*)*>",
                                                        Pattern.CASE_INSENSITIVE);
    private static Pattern regexCheckData = Pattern.compile("<\\s*QUERY_DATASET(\\s+[^\\/]*)*>", Pattern.CASE_INSENSITIVE);
    
    private T2FlexUtil() {
    }
    
    /**
     * hasResultDataSetXml performs the following check:
     * - has data (…<QUERY_DATASET><RECORD>…</RECORD><RECORD>…</RECORD></QUERY_DATA>), returns 'true'
     * - has no data (…<QUERY_DATASET />), T2FlexUtil.hasResultDataSetXml returns 'false'
     * - error (…<T2ErrorList><Error>… <ErrorDescription>…</ ErrorDescription></ERROR></T2ErrorList>), throws a T2FlexException with error description.
     * 
     * @param xml
     *            Result from accessing T2 Flex Web Services
     * @return boolean 'true' if has data otherwise returns 'false'.
     * @throws T2FlexException
     *             When T2 Flex returns error response with description.
     */
    public static boolean hasResultDataSetXml(final String xml) throws T2FlexException {
        final String error = retrieveError(xml);
        if (error != null) {
            throw new T2FlexException(error);
        }
        
        return hasQueryData(xml);
    }
    
    public static boolean hasQueryData(final String xml) {
        return (xml == null) ? false : regexCheckData.matcher(xml).find();
    }
    
    public static String retrieveError(final String xml) throws T2FlexException {
        String result = null;
        
        final Matcher m = regexError.matcher(xml);
        if (m.find()) {
            result = m.group(REGEX_GROUP_ERROR_MESSAGE);
        }
        
        return result;
    }
}
