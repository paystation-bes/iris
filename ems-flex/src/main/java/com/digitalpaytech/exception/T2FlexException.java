package com.digitalpaytech.exception;

public class T2FlexException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = -6462199356766711143L;
    
    public T2FlexException(final String msg) {
        super(msg);
    }
    
    public T2FlexException(final String msg, final Throwable t) {
        super(msg, t);
    }
    
    public T2FlexException(final Throwable t) {
        super(t);
    }
}
