package com.digitalpaytech.helper.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.digitalpaytech.dto.dataset.CitationMapQueryDataSet;
import com.digitalpaytech.dto.dataset.CitationRawQueryDataSet;
import com.digitalpaytech.dto.dataset.CitationWidgetQueryDataSet;
import com.digitalpaytech.dto.dataset.ContraventionTypeQueryDataSet;
import com.digitalpaytech.dto.dataset.FacilityQueryDataSet;
import com.digitalpaytech.dto.dataset.FlexInfoQueryDataSet;
import com.digitalpaytech.dto.dataset.PermitDetailQueryDataSet;
import com.digitalpaytech.dto.dataset.PermitQueryDataSet;
import com.digitalpaytech.dto.dataset.PropertyQueryDataSet;
import com.digitalpaytech.helper.QueryDataSetConverter;
import com.digitalpaytech.util.WebCoreConstants;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.BooleanConverter;
import com.thoughtworks.xstream.converters.basic.DateConverter;

/**
 * From XStream Annotations & Performance:
 * In auto-detection mode XStream will have to examine any unknown class type for annotations. This will slow down the marshaling process until
 * all processed types have been examined once.
 * 
 * Please note, that any call to XStream.processAnnotations will turn off the auto-detection mode.
 */

@Service("queryDataSetConverter")
public class QueryDataSetConverterImpl implements QueryDataSetConverter {
    
    private Map<Integer, XStream> xstreamMap;
    
    @PostConstruct
    public final void createAndLoadXStream() {
        this.xstreamMap = new HashMap<Integer, XStream>();
        createRegisterAndProcessAnnotations();
    }
    
    private void createRegisterAndProcessAnnotations() {
        
        // -------------- Contravention Type ----------------------
        XStream contraventionTypeXs = new XStream();
        contraventionTypeXs = register(contraventionTypeXs);
        contraventionTypeXs.processAnnotations(ContraventionTypeQueryDataSet.class);
        this.xstreamMap.put(WebCoreConstants.XSTREAM_CONTRAVENTION_TYPE, contraventionTypeXs);
        
        // -------------- Facility ----------------------
        XStream facilityXs = new XStream();
        facilityXs = register(facilityXs);
        facilityXs.processAnnotations(FacilityQueryDataSet.class);
        this.xstreamMap.put(WebCoreConstants.XSTREAM_FACILITY, facilityXs);
        
        // -------------- Citation Map Type ----------------------
        XStream citationMapXs = new XStream();
        citationMapXs = register(citationMapXs);
        citationMapXs.processAnnotations(CitationMapQueryDataSet.class);
        this.xstreamMap.put(WebCoreConstants.XSTREAM_CITATION_WIDGET_MAP_TYPE, citationMapXs);

        // -------------- Citation Map Type ----------------------
        XStream citationRawXs = new XStream();
        citationRawXs = register(citationRawXs);
        citationRawXs.processAnnotations(CitationRawQueryDataSet.class);
        this.xstreamMap.put(WebCoreConstants.XSTREAM_CITATION_WIDGET_RAW_TYPE, citationRawXs);

        // -------------- Property ----------------------
        XStream propertyXs = new XStream();
        propertyXs = register(propertyXs);
        propertyXs.processAnnotations(PropertyQueryDataSet.class);
        this.xstreamMap.put(WebCoreConstants.XSTREAM_PROPERTY, propertyXs);
        
        // -------------- Permit ----------------------
        XStream permitXs = new XStream();
        permitXs = register(permitXs);
        permitXs.processAnnotations(PermitQueryDataSet.class);
        this.xstreamMap.put(WebCoreConstants.XSTREAM_PERMIT, permitXs);
        
        // -------------- Permit Detail ----------------------
        XStream permitDetailXs = new XStream();
        permitDetailXs = register(permitDetailXs);
        permitDetailXs.processAnnotations(PermitDetailQueryDataSet.class);
        this.xstreamMap.put(WebCoreConstants.XSTREAM_PERMIT_DETAIL, permitDetailXs);
        
        // -------------- CitationWidget ------------------
        XStream citationWidgetXs = new XStream();
        citationWidgetXs = register(citationWidgetXs);
        citationWidgetXs.processAnnotations(CitationWidgetQueryDataSet.class);
        this.xstreamMap.put(WebCoreConstants.XSTREAM_CITATION_WIDGET_TYPE, citationWidgetXs);
        
        // -------------- Flex Info ------------------
        XStream flexInfoXs = new XStream();
        flexInfoXs = register(flexInfoXs);
        flexInfoXs.processAnnotations(FlexInfoQueryDataSet.class);
        this.xstreamMap.put(WebCoreConstants.XSTREAM_FLEX_INFO, flexInfoXs);
        
    }
    
    private XStream register(final XStream xstream) {
        xstream.registerConverter(new DateConverter(WebCoreConstants.DATE_FORMAT_WITH_TIMEZONE_XSTREAM, new String[] {}));
        xstream.registerConverter(new BooleanConverter("1", "0", true));
        return xstream;
    }
    
    @Override
    public final FacilityQueryDataSet convertFacility(final String xml) {
        final FacilityQueryDataSet dataSet = (FacilityQueryDataSet) this.xstreamMap.get(WebCoreConstants.XSTREAM_FACILITY).fromXML(xml);
        return dataSet;
    }
    
    @Override
    public final PropertyQueryDataSet convertProperty(final String xml) {
        final PropertyQueryDataSet dataSet = (PropertyQueryDataSet) this.xstreamMap.get(WebCoreConstants.XSTREAM_PROPERTY).fromXML(xml);
        return dataSet;
    }
    
    @Override
    public final ContraventionTypeQueryDataSet convertContraventionType(final String xml) {
        final ContraventionTypeQueryDataSet dataSet = (ContraventionTypeQueryDataSet) this.xstreamMap
                .get(WebCoreConstants.XSTREAM_CONTRAVENTION_TYPE).fromXML(xml);
        return dataSet;
    }
    
    @Override
    public final PermitQueryDataSet convertPermit(final String xml) {
        final PermitQueryDataSet dataSet = (PermitQueryDataSet) this.xstreamMap.get(WebCoreConstants.XSTREAM_PERMIT).fromXML(xml);
        return dataSet;
    }
    
    @Override
    public final CitationWidgetQueryDataSet convertCitationWidget(final String xml) {
        final CitationWidgetQueryDataSet dataSet = (CitationWidgetQueryDataSet) this.xstreamMap.get(WebCoreConstants.XSTREAM_CITATION_WIDGET_TYPE)
                .fromXML(xml);
        return dataSet;
    }
    
    @Override
    public final CitationMapQueryDataSet convertCitationMapData(final String xml) {
        final CitationMapQueryDataSet dataSet = (CitationMapQueryDataSet) this.xstreamMap.get(WebCoreConstants.XSTREAM_CITATION_WIDGET_MAP_TYPE)
                .fromXML(xml);
        return dataSet;
    }
    
    @Override
    public final PermitDetailQueryDataSet convertPermitDetail(final String xml) {
        final PermitDetailQueryDataSet dataSet = (PermitDetailQueryDataSet) this.xstreamMap.get(WebCoreConstants.XSTREAM_PERMIT_DETAIL).fromXML(xml);
        return dataSet;
    }
    
    @Override
    public final FlexInfoQueryDataSet convertFlexInfo(final String xml) {
        final FlexInfoQueryDataSet dataSet = (FlexInfoQueryDataSet) this.xstreamMap.get(WebCoreConstants.XSTREAM_FLEX_INFO).fromXML(xml);
        return dataSet;
    }

    @Override
    public final CitationRawQueryDataSet convertCitationRawData(final String xml) {
        final CitationRawQueryDataSet dataSet = (CitationRawQueryDataSet) this.xstreamMap.get(WebCoreConstants.XSTREAM_CITATION_WIDGET_RAW_TYPE)
                .fromXML(xml);
        return dataSet;
    }
    

    public final Map<Integer, XStream> getXstreamMap() {
        return this.xstreamMap;
    }
    
    public final void setXstreamMap(final Map<Integer, XStream> xstreamMap) {
        this.xstreamMap = xstreamMap;
    }

}
