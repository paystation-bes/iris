package com.digitalpaytech.helper;

import com.digitalpaytech.dto.dataset.CitationMapQueryDataSet;
import com.digitalpaytech.dto.dataset.CitationRawQueryDataSet;
import com.digitalpaytech.dto.dataset.CitationWidgetQueryDataSet;
import com.digitalpaytech.dto.dataset.ContraventionTypeQueryDataSet;
import com.digitalpaytech.dto.dataset.FacilityQueryDataSet;
import com.digitalpaytech.dto.dataset.FlexInfoQueryDataSet;
import com.digitalpaytech.dto.dataset.PermitDetailQueryDataSet;
import com.digitalpaytech.dto.dataset.PermitQueryDataSet;
import com.digitalpaytech.dto.dataset.PropertyQueryDataSet;

public interface QueryDataSetConverter {
    ContraventionTypeQueryDataSet convertContraventionType(String xml);       
    
    FacilityQueryDataSet convertFacility(final String xml);
    
    PropertyQueryDataSet convertProperty(final String xml);
    
    CitationWidgetQueryDataSet convertCitationWidget(String xml);
    
    CitationMapQueryDataSet convertCitationMapData(String xml);

    CitationRawQueryDataSet convertCitationRawData(String xml);

    PermitQueryDataSet convertPermit(String xml);
    
    PermitDetailQueryDataSet convertPermitDetail(String xml);
    
    FlexInfoQueryDataSet convertFlexInfo(String xml);
}
