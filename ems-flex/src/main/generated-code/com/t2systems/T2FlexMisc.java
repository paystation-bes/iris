package com.t2systems;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;

/**
 * Web methods relating
 * to miscellaneous operations that can be performed with PowerPark
 * Flex.
 * 
 * 
 * This class was generated by Apache CXF 2.7.0
 * 2014-10-03T11:36:00.700-07:00
 * Generated source version: 2.7.0
 * 
 */
@WebServiceClient(name = "T2_Flex_Misc", wsdlLocation = "classpath:wsdl/T2_Flex_Misc.asmx.wsdl", targetNamespace = "http://www.t2systems.com/")
public class T2FlexMisc extends Service {
    
    public final static URL WSDL_LOCATION;
    
    public final static QName SERVICE = new QName("http://www.t2systems.com/", "T2_Flex_Misc");
    public final static QName T2FlexMiscSoap = new QName("http://www.t2systems.com/", "T2_Flex_MiscSoap");
    static {
        URL url = null;
        try {
            url = new URL("classpath:wsdl/T2_Flex_Misc.asmx.wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(T2FlexMisc.class.getName()).log(java.util.logging.Level.INFO,
                                                                               "Can not initialize the default wsdl from {0}",
                                                                               "classpath:wsdl/T2_Flex_Misc.asmx.wsdl");
        }
        WSDL_LOCATION = url;
    }
    
    public T2FlexMisc(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }
    
    public T2FlexMisc(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }
    
    public T2FlexMisc() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public T2FlexMisc(WebServiceFeature... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }
    
    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public T2FlexMisc(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SERVICE, features);
    }
    
    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public T2FlexMisc(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }
    
    /**
     * 
     * @return
     *         returns T2FlexMiscSoap
     */
    @WebEndpoint(name = "T2_Flex_MiscSoap")
    public T2FlexMiscSoap getT2FlexMiscSoap() {
        return super.getPort(T2FlexMiscSoap, T2FlexMiscSoap.class);
    }
    
    /**
     * 
     * @param features
     *            A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy. Supported features not in the <code>features</code> parameter will
     *            have their default values.
     * @return
     *         returns T2FlexMiscSoap
     */
    @WebEndpoint(name = "T2_Flex_MiscSoap")
    public T2FlexMiscSoap getT2FlexMiscSoap(WebServiceFeature... features) {
        return super.getPort(T2FlexMiscSoap, T2FlexMiscSoap.class, features);
    }
    
}
