
package com.t2systems;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ExecuteQueryByNameResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "executeQueryByNameResult"
})
@XmlRootElement(name = "ExecuteQueryByNameResponse")
public class ExecuteQueryByNameResponse {

    @XmlElement(name = "ExecuteQueryByNameResult")
    protected String executeQueryByNameResult;

    /**
     * Gets the value of the executeQueryByNameResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExecuteQueryByNameResult() {
        return executeQueryByNameResult;
    }

    /**
     * Sets the value of the executeQueryByNameResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExecuteQueryByNameResult(String value) {
        this.executeQueryByNameResult = value;
    }

}
