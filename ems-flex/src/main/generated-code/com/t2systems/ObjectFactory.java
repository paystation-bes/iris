
package com.t2systems;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.t2systems package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _String_QNAME = new QName("http://www.t2systems.com/", "string");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.t2systems
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ExecuteQueryByNameResponse }
     * 
     */
    public ExecuteQueryByNameResponse createExecuteQueryByNameResponse() {
        return new ExecuteQueryByNameResponse();
    }

    /**
     * Create an instance of {@link ExecuteQuery }
     * 
     */
    public ExecuteQuery createExecuteQuery() {
        return new ExecuteQuery();
    }

    /**
     * Create an instance of {@link ArrayOfQueryParameter }
     * 
     */
    public ArrayOfQueryParameter createArrayOfQueryParameter() {
        return new ArrayOfQueryParameter();
    }

    /**
     * Create an instance of {@link ExecuteQueryResponse }
     * 
     */
    public ExecuteQueryResponse createExecuteQueryResponse() {
        return new ExecuteQueryResponse();
    }

    /**
     * Create an instance of {@link ExecuteQueryByName }
     * 
     */
    public ExecuteQueryByName createExecuteQueryByName() {
        return new ExecuteQueryByName();
    }

    /**
     * Create an instance of {@link QueryParameter }
     * 
     */
    public QueryParameter createQueryParameter() {
        return new QueryParameter();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.t2systems.com/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

}
