package com.digitalpaytech.helper.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import com.digitalpaytech.dto.dataset.CitationWidgetQueryDataSet;
import com.digitalpaytech.dto.dataset.ContraventionTypeQueryDataSet;
import com.digitalpaytech.dto.dataset.PermitDetailQueryDataSet;
import com.digitalpaytech.dto.dataset.PermitDetailQueryDataSet.PermitDetailFlexRecord;
import com.digitalpaytech.dto.dataset.PermitQueryDataSet;
import com.digitalpaytech.dto.dataset.PermitQueryDataSet.PermitFlexRecord;

public class QueryDataSetConverterImplTest {
    private static Logger log = Logger.getLogger(QueryDataSetConverterImplTest.class);
    private QueryDataSetConverterImpl converter;
    
    @Before
    public final void before() {
        this.converter = new QueryDataSetConverterImpl();
        this.converter.createAndLoadXStream();
    }
    
    // --------------------------------------------------------------------------
    
    @Test
    public final void convertPermit() {
        final String xml = "<QUERY_DATASET><RECORD><PERMITID>6011577</PERMITID><EFFECTIVESTARTDATE>2012-12-31T00:00:00-05:00</EFFECTIVESTARTDATE>"
                           + "<EFFECTIVEENDDATE>9999-12-31T23:59:59-05:00</EFFECTIVEENDDATE><PERMITNUMBER>HOB3372</PERMITNUMBER>"
                           + "<FACILITYNAME>HOBBY CENTER</FACILITYNAME><LICENSEPLATE>BH9C999</LICENSEPLATE><ISDEACTIVATED>0</ISDEACTIVATED>"
                           + "<ISDESTROYED>1</ISDESTROYED><ISLOST>0</ISLOST><ISCUSTODY>0</ISCUSTODY><ISRETURNED>0</ISRETURNED><ISTERMINATED>0</ISTERMINATED>"
                           + "</RECORD><RECORD><PERMITID>6011712</PERMITID><EFFECTIVESTARTDATE>2012-12-31T00:00:00-05:00</EFFECTIVESTARTDATE>"
                           + "<EFFECTIVEENDDATE>9999-12-31T23:59:59-05:00</EFFECTIVEENDDATE><PERMITNUMBER>HOB3507</PERMITNUMBER>"
                           + "<FACILITYNAME>HOBBY CENTER</FACILITYNAME><LICENSEPLATE>GGG503</LICENSEPLATE><ISDEACTIVATED>0</ISDEACTIVATED>"
                           + "<ISDESTROYED>1</ISDESTROYED><ISLOST>0</ISLOST>" + "<ISCUSTODY>0</ISCUSTODY><ISRETURNED>0</ISRETURNED>"
                           + "<ISTERMINATED>0</ISTERMINATED></RECORD>"
                           + "<RECORD><PERMITID>6009932</PERMITID><EFFECTIVESTARTDATE>2012-12-31T00:00:00-05:00</EFFECTIVESTARTDATE>"
                           + "<EFFECTIVEENDDATE>9999-12-31T23:59:59-05:00</EFFECTIVEENDDATE><PERMITNUMBER>HOB1727</PERMITNUMBER>"
                           + "<FACILITYNAME>HOBBY CENTER</FACILITYNAME><LICENSEPLATE>47CSF6</LICENSEPLATE><ISDEACTIVATED>0</ISDEACTIVATED>"
                           + "<ISDESTROYED>1</ISDESTROYED><ISLOST>0</ISLOST><ISCUSTODY>0</ISCUSTODY>" + "<ISRETURNED>0</ISRETURNED>"
                           + "<ISTERMINATED>0</ISTERMINATED></RECORD><RECORD>"
                           + "<PERMITID>6009455</PERMITID><EFFECTIVESTARTDATE>2012-12-31T00:00:00-05:00</EFFECTIVESTARTDATE>"
                           + "<EFFECTIVEENDDATE>9999-12-31T23:59:59-05:00</EFFECTIVEENDDATE><PERMITNUMBER>HOB1250</PERMITNUMBER>"
                           + "<FACILITYNAME>HOBBY CENTER</FACILITYNAME><LICENSEPLATE>485XVP</LICENSEPLATE><ISDEACTIVATED>0</ISDEACTIVATED>"
                           + "<ISDESTROYED>1</ISDESTROYED><ISLOST>0</ISLOST><ISCUSTODY>0</ISCUSTODY><ISRETURNED>0</ISRETURNED>"
                           + "<ISTERMINATED>0</ISTERMINATED></RECORD></QUERY_DATASET>";
        final PermitQueryDataSet resp = this.converter.convertPermit(xml);
        log.debug(resp);
        assertNotNull(resp);
        
        final List<PermitFlexRecord> list = resp.getPermitFlexRecords();
        assertNotNull(list);
        log.debug("size1: " + list.size());
        
        PermitFlexRecord rec;
        final Iterator<PermitFlexRecord> iter = list.iterator();
        while (iter.hasNext()) {
            rec = iter.next();
            assertNotNull(rec);
            log.debug("isDestroyed: " + rec.isDestroyed() + ", isLost: " + rec.isLostStolen() + ", isMisscust: " + rec.isMisscust()
                      + ", isReturned: " + rec.isReturned() + ", isTerminated: " + rec.isTerminated());
            
            assertTrue(rec.isDestroyed());
            /*
             * log.debug("effective date: " + rec.getPermitEffectiveStartDate() + ", end date: " + rec.getPermitEffectiveEndDate()
             * + ", facility description: " + rec.getFacilityDescription());
             */
            
        }
    }
    
    @Test
    public final void convertContraventionType() {
        final String xml = "<QUERY_DATASET><RECORD><VIC_UID>0</VIC_UID><VIC_DESCRIPTION xml:space=\"preserve\"> </VIC_DESCRIPTION></RECORD>"
                           + "<RECORD><VIC_UID>2000</VIC_UID><VIC_DESCRIPTION>PK01- Parking meter expired</VIC_DESCRIPTION></RECORD>"
                           + "<RECORD><VIC_UID>2001</VIC_UID><VIC_DESCRIPTION>PK02- Overtime parking</VIC_DESCRIPTION></RECORD>"
                           + "<RECORD><VIC_UID>2002</VIC_UID><VIC_DESCRIPTION>PK03- Not wholly within space</VIC_DESCRIPTION></RECORD>"
                           + "<RECORD><VIC_UID>2003</VIC_UID><VIC_DESCRIPTION>PK04- Parked on wrong side of St</VIC_DESCRIPTION></RECORD>"
                           + "<RECORD><VIC_UID>2004</VIC_UID><VIC_DESCRIPTION>PK05- Parked more than 24 hrs</VIC_DESCRIPTION></RECORD>"
                           + "<RECORD><VIC_UID>2005</VIC_UID><VIC_DESCRIPTION>PK06-Noncommercial in truck zone</VIC_DESCRIPTION></RECORD>"
                           + "<RECORD><VIC_UID>2006</VIC_UID><VIC_DESCRIPTION>PK07- Parked in bus zone</VIC_DESCRIPTION></RECORD>"
                           + "<RECORD><VIC_UID>2007</VIC_UID><VIC_DESCRIPTION>PK08- Parked in reserved zone</VIC_DESCRIPTION></RECORD></QUERY_DATASET>";
        
        final ContraventionTypeQueryDataSet resp = this.converter.convertContraventionType(xml);
        log.debug(resp);
        assertNotNull(resp);
        
        final int size = resp.getViolationFlexRecords().size();
        if (size == 0) {
            fail("size should not be zero");
        }
    }
    
    /*
    @Test
    public final void convertContraventionTypeWithAttribute() {
        final String xml = "<QUERY_DATASET><RECORD><VIC_UID>0</VIC_UID><VIC_DESCRIPTION xml:space=\"preserve\"> </VIC_DESCRIPTION></RECORD></QUERY_DATASET>";
        final ContraventionTypeQueryDataSet resp = this.converter.convertContraventionType(xml);
        assertNotNull(resp);
        
        ViolationFlexRecord rec = null;
        final Iterator<ViolationFlexRecord> iter = resp.getViolationFlexRecords().iterator();
        while (iter.hasNext()) {
            rec = iter.next();
            assertNotNull(rec.getViolationDescription().getSpace());
            log.debug("attttribute ViolationDescription: " + rec.getViolationDescription() + ", space: " + rec.getViolationDescription().getSpace());
        }
    }
    */
    
    @Test
    public final void convertCitationWidget() {
        final String xml = "<QUERY_DATASET><RECORD><COUNT>6</COUNT><CITATIONTYPEID>2003</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>16</COUNT><CITATIONTYPEID>2004</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>2</COUNT><CITATIONTYPEID>2006</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>7</COUNT><CITATIONTYPEID>2007</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>6</COUNT><CITATIONTYPEID>2008</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>4</COUNT><CITATIONTYPEID>2010</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>11</COUNT><CITATIONTYPEID>2011</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>8</COUNT><CITATIONTYPEID>2012</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>9</COUNT><CITATIONTYPEID>2013</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>32</COUNT><CITATIONTYPEID>2015</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>32</COUNT><CITATIONTYPEID>2016</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>10</COUNT><CITATIONTYPEID>2017</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>21</COUNT><CITATIONTYPEID>2018</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>2</COUNT><CITATIONTYPEID>2019</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>22</COUNT><CITATIONTYPEID>2020</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>3</COUNT><CITATIONTYPEID>2021</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>1</COUNT><CITATIONTYPEID>2022</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>4</COUNT><CITATIONTYPEID>2027</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>1</COUNT><CITATIONTYPEID>2037</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>3327</COUNT><CITATIONTYPEID>2042</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>706</COUNT><CITATIONTYPEID>2043</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>17</COUNT><CITATIONTYPEID>2044</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>66</COUNT><CITATIONTYPEID>2045</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>655</COUNT><CITATIONTYPEID>2046</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>55</COUNT><CITATIONTYPEID>2047</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>21</COUNT><CITATIONTYPEID>2048</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>234</COUNT><CITATIONTYPEID>2049</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>42</COUNT><CITATIONTYPEID>2050</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>2</COUNT><CITATIONTYPEID>2051</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>53</COUNT><CITATIONTYPEID>2052</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>891</COUNT><CITATIONTYPEID>2053</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>1533</COUNT><CITATIONTYPEID>2054</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>1017</COUNT><CITATIONTYPEID>2055</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>3595</COUNT><CITATIONTYPEID>2056</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>4</COUNT><CITATIONTYPEID>2057</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>830</COUNT><CITATIONTYPEID>2058</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>142</COUNT><CITATIONTYPEID>2059</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>1095</COUNT><CITATIONTYPEID>2060</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>1123</COUNT><CITATIONTYPEID>2061</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>1171</COUNT><CITATIONTYPEID>2062</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>2</COUNT><CITATIONTYPEID>2063</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>154</COUNT><CITATIONTYPEID>2064</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>18</COUNT><CITATIONTYPEID>2065</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>2</COUNT><CITATIONTYPEID>2066</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>9</COUNT><CITATIONTYPEID>2067</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>5</COUNT><CITATIONTYPEID>2068</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>46</COUNT><CITATIONTYPEID>2069</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>14</COUNT><CITATIONTYPEID>2070</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>261</COUNT><CITATIONTYPEID>2072</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>22</COUNT><CITATIONTYPEID>2073</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>10</COUNT><CITATIONTYPEID>2075</CITATIONTYPEID></RECORD>"
                           + "<RECORD><COUNT>10</COUNT><CITATIONTYPEID>2076</CITATIONTYPEID></RECORD></QUERY_DATASET>";
        
        final CitationWidgetQueryDataSet resp = this.converter.convertCitationWidget(xml);
        log.debug(resp);
        assertNotNull(resp);
    }
    
    @Test
    public final void convertPermitDetail() {
        final String xml = "<QUERY_DATASET><RECORD><PERMITID>727675</PERMITID>"
                           + "<PERMITEFFECTIVESTARTINMINS>2012-01-01T00:00:00-05:00</PERMITEFFECTIVESTARTINMINS>"
                           + "<PERMITEFFECTIVEENDINMINS>2012-12-31T23:59:00-05:00</PERMITEFFECTIVEENDINMINS><PERMITNUMBER>RPD05309</PERMITNUMBER>"
                           + "<FACILITYNAME>RES California</FACILITYNAME><ENTITYNAME>TERRY MOORE</ENTITYNAME>"
                           + "<LICENSEPLATE>BT9G246</LICENSEPLATE><ISDEACTIVATED>1</ISDEACTIVATED><ISDESTROYED>1</ISDESTROYED>"
                           + "<ISLOST>1</ISLOST><ISRETURNED>1</ISRETURNED><ISTERMINATED>1</ISTERMINATED>"
                           + "<AMOUNT>-23.09</AMOUNT><MINOFCREATEDATE>2011-11-29T10:52:00-05:00</MINOFCREATEDATE><PAYMENTTYPE>8</PAYMENTTYPE>"
                           + "</RECORD></QUERY_DATASET>";
        
        final PermitDetailQueryDataSet resp = this.converter.convertPermitDetail(xml);
        log.debug(resp);
        assertNotNull(resp);
        
        final List<PermitDetailFlexRecord> list = resp.getPermitDetailFlexRecords();
        assertNotNull(list);
        log.debug("size: " + list.size());
        
        PermitDetailFlexRecord rec;
        final Iterator<PermitDetailFlexRecord> iter = list.iterator();
        while (iter.hasNext()) {
            rec = iter.next();
            assertNotNull(rec);
            assertTrue(rec.isDeactivated());
            assertTrue(rec.isDestroyed());
            assertTrue(rec.isLostStolen());
            assertTrue(rec.isReturned());
            assertTrue(rec.isTerminated());
        }
    }
}
