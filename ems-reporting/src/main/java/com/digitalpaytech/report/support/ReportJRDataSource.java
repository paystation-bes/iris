package com.digitalpaytech.report.support;

import java.sql.ResultSet;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;

public class ReportJRDataSource extends JRResultSetDataSource {
    // private ReportsSessionObject sessionObj;
    JRDataSourceEventHandler dataSourceEventHandler;
    
    public ReportJRDataSource(final ResultSet rs, final JRDataSourceEventHandler dataSourceEventHandler) {
        super(rs);
        this.dataSourceEventHandler = dataSourceEventHandler;
        if (dataSourceEventHandler != null) {
            dataSourceEventHandler.onInitialize(rs);
        }
    }
    
    @Override
    public final boolean next() throws JRException {
        final boolean moreRows = super.next();
        if (moreRows && this.dataSourceEventHandler != null) {
            this.dataSourceEventHandler.onNextRow();
        }
        return moreRows;
    }
}
