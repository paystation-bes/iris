package com.digitalpaytech.report.support.dto;

import com.digitalpaytech.util.StandardConstants;

public class ArchiveReportProperties {
    
    private int months;
    private int bufferDays;
    
    public int getMonths() {
        return this.months;
    }
    
    public void setMonths(final int months) {
        this.months = months;
    }
    
    public int getBufferDays() {
        return this.bufferDays;
    }
    
    public void setBufferDays(final int bufferDays) {
        this.bufferDays = bufferDays;
    }
    
    public int getLimit() {
        return Math.round(this.months * StandardConstants.AVG_DAYS_IN_MONTH - this.bufferDays);
    }
    
}
