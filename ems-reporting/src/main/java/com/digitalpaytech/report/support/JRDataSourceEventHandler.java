package com.digitalpaytech.report.support;

import java.sql.ResultSet;

public interface JRDataSourceEventHandler {
    void onInitialize(ResultSet rs);
    
    void onNextRow();
}
