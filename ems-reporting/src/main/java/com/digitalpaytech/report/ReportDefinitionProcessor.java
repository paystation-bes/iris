package com.digitalpaytech.report;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.ReportDefinitionService;
import com.digitalpaytech.util.StandardConstants;

/**
 * This class checks ReportDefinition for online and scheduled reports that are
 * ready to run and creates an entry in the ReportQueue.
 * 
 * @author Murat Erozlu
 * 
 */
@Component("reportDefinitionProcessor")
public class ReportDefinitionProcessor {
    
    private static final Logger LOGGER = Logger.getLogger(ReportDefinitionProcessor.class);
                
    @Autowired
    private ReportDefinitionService reportDefinitionService;
    
    @Autowired
    private ClusterMemberService clusterMemberService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;      
        
    /**
     * Check ReportDefinition table for ready reports. Create an entry for all
     * ready reports in ReportQueue
     * An atomic transaction is required to lock the reportDefinition table and prevent 2 different
     * servers from creating the same item in the queue therefore some business logic is transported
     * to the service layer.
     * 
     */
    //	@Scheduled(fixedDelay = 30000)
    public void checkAndProcessReadyReports() {
		LOGGER.error("checkAndProcessReadyReports running on this server");

    	
    	if (!getClusterName().equals(getReportServerName())) {
            LOGGER.debug("Report processing aborted on " + getClusterName() 
                         + ". Should run on " + getReportServerName() + StandardConstants.STRING_DOT);
            return;
        }                 
		LOGGER.error("checkAndProcessReadyReports before lookup");
        
        while (this.reportDefinitionService.findReadyReportsWithLock()) {
            ;
        }
		LOGGER.error("checkAndProcessReadyReports after lookup");

        
    }
    
    public final void setReportDefinitionService(final ReportDefinitionService reportDefinitionService) {
        this.reportDefinitionService = reportDefinitionService;
    }
    
    private String getClusterName() {
        return this.clusterMemberService.getClusterName();
    }    
    
    private String getReportServerName() {
        return this.emsPropertiesService.getPropertyValue(EmsPropertiesService.REPORTS_BACKGROUND_SERVER);
    }    
}
