package com.digitalpaytech.report.scriptlets;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import net.sf.jasperreports.engine.JRDefaultScriptlet;
import net.sf.jasperreports.engine.JRScriptletException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.impl.ReportingUtilImpl;

@Component("reportScriptlet")
public class ReportsScriptlet extends JRDefaultScriptlet {
    
    private static final Logger LOGGER = Logger.getLogger(ReportsScriptlet.class);
    
    private static final String STR_TO_W_SPACES = " To ";
    private static final String STR_STRING_W_COMMA_COLON = ", str: ";
    private static final String STR_DATE_FORMAT_COLON = ", dateFormat.format(date2Time): ";
    private static final String STR_TO_W_LINE_END = " To\n";
    
    private static final int RADIX_10 = 10;
    
    private static final String[] MONTH_NAMES = { StandardConstants.STRING_JANUARY, StandardConstants.STRING_FEBRUARY, StandardConstants.STRING_MARCH,
        StandardConstants.STRING_APRIL, StandardConstants.STRING_MAY, StandardConstants.STRING_JUNE, StandardConstants.STRING_JULY,
        StandardConstants.STRING_AUGUST, StandardConstants.STRING_SEPTEMBER, StandardConstants.STRING_OCTOBER, StandardConstants.STRING_NOVEMBER,
        StandardConstants.STRING_DECEMBER, };
    
    public final String formatDate(final String timeZone, final Date date) throws JRScriptletException {
        if (timeZone == null) {
            return DateUtil.createDateString(date);
        } else if (date == null) {
            return ReportingConstants.REPORT_STRINGS_NA;
        }
        return DateUtil.createDateString(timeZone, date);
    }
    
    public final String formatShortDate(final String timeZone, final Date date) throws JRScriptletException {
        if (timeZone == null) {
            return DateUtil.createDateString(DateUtil.getShortDateFormat(), date);
        } else if (date == null) {
            return ReportingConstants.REPORT_STRINGS_NA;
        }
        return DateUtil.createDateString(DateUtil.getShortDateFormat(), timeZone, date);
    }
    
    private static void appendSubscription(final List<String> activeSubscriptions, final boolean flag, final String subscriptionName) {
        if (flag) {
            activeSubscriptions.add(subscriptionName + ReportingConstants.SUBSCRIPTIONS_NEW_LINE);
        }
    }
    
    public final String formatSubscriptions(final boolean standardReports, final boolean alerts, final boolean realTimeCreditCardProcessing,
        final boolean batchCreditCardProcessing, final boolean realTimeCampusCardProcessing, final boolean coupons, final boolean passcards,
        final boolean smartCards, final boolean extendByPhone, final boolean digitalAPIRead, final boolean digitalAPIWrite,
        final boolean thirdPartyPayByCellIntegration, final boolean mobileDigitalCollect, final boolean mobileDigitalMaintain,
        final boolean mobileDigitalPatrol, final boolean flexIntegration, final boolean autoCountIntegration, final boolean digitalApiXChange,
        final boolean onlineRateConfiguration, final boolean onlinePSConfiguration, final int column) {
        final StringBuilder subscriptionListBuilder = new StringBuilder();
        
        if (column > ReportingConstants.SUBSCRIPTIONS_NUMBEROFALLOWEDCOLUMNS || column <= 0) {
            return subscriptionListBuilder.toString();
        }
        
        final List<String> activeSubscriptions = new ArrayList<String>();
        appendSubscription(activeSubscriptions, standardReports, ReportingConstants.SUBSCRIPTIONS_STANDARDREPORTS);
        appendSubscription(activeSubscriptions, alerts, ReportingConstants.SUBSCRIPTIONS_ALERTS);
        appendSubscription(activeSubscriptions, realTimeCreditCardProcessing, ReportingConstants.SUBSCRIPTIONS_REALTIMECREDITCARDPROCESSING);
        appendSubscription(activeSubscriptions, batchCreditCardProcessing, ReportingConstants.SUBSCRIPTIONS_BATCHCREDITCARDPROCESSING);
        appendSubscription(activeSubscriptions, realTimeCampusCardProcessing, ReportingConstants.SUBSCRIPTIONS_REALTIMECAMPUSCARDPROCESSING);
        appendSubscription(activeSubscriptions, coupons, ReportingConstants.SUBSCRIPTIONS_COUPONS);
        appendSubscription(activeSubscriptions, passcards, ReportingConstants.SUBSCRIPTIONS_PASSCARDS);
        appendSubscription(activeSubscriptions, smartCards, ReportingConstants.SUBSCRIPTIONS_SMARTCARDS);
        appendSubscription(activeSubscriptions, extendByPhone, ReportingConstants.SUBSCRIPTIONS_EXTENDBYPHONE);
        appendSubscription(activeSubscriptions, digitalAPIRead, ReportingConstants.SUBSCRIPTIONS_DIGITALAPIREAD);
        appendSubscription(activeSubscriptions, digitalAPIWrite, ReportingConstants.SUBSCRIPTIONS_DIGITALAPIWRITE);
        appendSubscription(activeSubscriptions, thirdPartyPayByCellIntegration, ReportingConstants.SUBSCRIPTIONS_THIRDPARTYPAYBYCELLINTEGRATION);
        appendSubscription(activeSubscriptions, mobileDigitalCollect, ReportingConstants.SUBSCRIPTIONS_MOBILEDIGITALCOLLECT);
        appendSubscription(activeSubscriptions, mobileDigitalMaintain, ReportingConstants.SUBSCRIPTIONS_MOBILEDIGITALMAINTAIN);
        appendSubscription(activeSubscriptions, mobileDigitalPatrol, ReportingConstants.SUBSCRIPTIONS_MOBILEDIGITALPATROL);
        appendSubscription(activeSubscriptions, flexIntegration, ReportingConstants.SUBSCRIPTIONS_FLEXINTEGRATION);
        appendSubscription(activeSubscriptions, autoCountIntegration, ReportingConstants.SUBSCRIPTIONS_AUTOCOUNTINTEGRATION);
        appendSubscription(activeSubscriptions, digitalApiXChange, ReportingConstants.SUBSCRIPTIONS_DIGITALAPIXCHANGE);
        appendSubscription(activeSubscriptions, onlineRateConfiguration, ReportingConstants.SUBSCRIPTIONS_ONLINERATECONFIGURATION);
        appendSubscription(activeSubscriptions, onlinePSConfiguration, ReportingConstants.SUBSCRIPTIONS_ONLINEPSCONFIGURATION);
        
        final int startIndex = column - 1;
        final int endIndex = activeSubscriptions.size() - 1;
        
        if (activeSubscriptions.isEmpty() || endIndex < startIndex) {
            if (column == ReportingConstants.SUBSCRIPTIONS_COLUMN_1) {
                // Not removing NewLine if "None" is displayed because that leads to unalignment between subscription label and column value
                subscriptionListBuilder.append(ReportingConstants.SUBSCRIPTIONS_NONE).append(ReportingConstants.SUBSCRIPTIONS_NEW_LINE);
            }
            return subscriptionListBuilder.toString();
        }
        
        int subscriptionCount = 0;
        for (int i = startIndex; i <= endIndex; i += ReportingConstants.SUBSCRIPTIONS_NUMBEROFALLOWEDCOLUMNS) {
            subscriptionListBuilder.append(activeSubscriptions.get(i));
            subscriptionCount++;
        }
        
        String subscriptionList = subscriptionListBuilder.toString();
        // Not removing NewLine if subscription count is 1 because that leads to unalignment between subscription label and column value
        if (subscriptionList.endsWith(ReportingConstants.SUBSCRIPTIONS_NEW_LINE) && subscriptionCount > 1) {
            subscriptionList = subscriptionList.substring(0, subscriptionList.lastIndexOf(ReportingConstants.SUBSCRIPTIONS_NEW_LINE));
        }
        
        return subscriptionList;
    }
    
    /**
     * Use this function when the time is meat to be "user timezone", and is stored
     * in database not time zone adjusted (not system time zone). In this case the
     * time zone offset needs to be DEDUCTED from the Date object before formatting.
     * 
     * @param timeZone
     * @param date
     * @return
     */
    public final String convertTimeZoneAndFormatDate(final String timeZone, final Date date) {
        if (timeZone == null) {
            return DateUtil.createDateString(date);
        } else if (date == null) {
            return ReportingConstants.REPORT_STRINGS_NA;
        }
        return DateUtil.convertTimeZoneAndCreateDateString(timeZone, date);
    }
    
    public final String formatLocalDate(final String custTimeZone, final Date date) {
        if (custTimeZone == null) {
            return DateUtil.createDateString(date);
        } else if (date == null) {
            return ReportingConstants.REPORT_STRINGS_NA;
        } else {
            final TimeZone timeZone = TimeZone.getTimeZone(custTimeZone);
            long dateMS = date.getTime();
            dateMS -= timeZone.getOffset(dateMS);
            final Date dateGMT = new Date(dateMS);
            return DateUtil.createDateString(custTimeZone, dateGMT);
        }
    }
    
    public final String getDateInfo(final String startDate, final String endDate, final String startMonth, final String endMonth,
        final String startYear, final String endYear, final int dateOption) {
        final StringBuffer stb = new StringBuffer();
        if (dateOption == ReportingConstants.REPORT_FILTER_DATE_MONTH) {
            int monthInt = Integer.parseInt(startMonth, RADIX_10);
            stb.append(MONTH_NAMES[monthInt - 1]);
            stb.append(StandardConstants.STRING_EMPTY_SPACE + startYear);
            monthInt = Integer.parseInt(endMonth, RADIX_10);
            stb.append(STR_TO_W_SPACES + MONTH_NAMES[monthInt - 1]);
            stb.append(StandardConstants.STRING_EMPTY_SPACE + endYear);
        }
        
        if (dateOption == ReportingConstants.REPORT_FILTER_DATE_DATE_TIME) {
            try {
                final DateFormat dateFormat = new SimpleDateFormat(DateUtil.REPORTS_FORMAT);
                final Date start = dateFormat.parse(startDate);
                final Date end = dateFormat.parse(endDate);
                stb.append(DateUtil.createDateString(start) + STR_TO_W_LINE_END);
                stb.append(DateUtil.createDateString(end));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return stb.toString();
    }
    
    public final String getDateInfo(final String startDate, final String endDate, final String startMonth, final String endMonth,
        final String startYear, final String endYear, final int dateOption, final TimeZone tz) {
        final StringBuffer stb = new StringBuffer();
        
        if (dateOption == ReportingConstants.REPORT_FILTER_DATE_MONTH) {
            int monthInt = (new Integer(startMonth)).intValue();
            stb.append(MONTH_NAMES[monthInt - 1]);
            stb.append(StandardConstants.STRING_EMPTY_SPACE + startYear);
            monthInt = (new Integer(endMonth)).intValue();
            stb.append(STR_TO_W_SPACES + MONTH_NAMES[monthInt - 1]);
            stb.append(StandardConstants.STRING_EMPTY_SPACE + endYear);
        }
        
        if (dateOption == ReportingConstants.REPORT_FILTER_DATE_DATE_TIME) {
            try {
                final DateFormat dateFormat = new SimpleDateFormat(DateUtil.REPORTS_FORMAT);
                
                final Date start = dateFormat.parse(startDate);
                final Date end = dateFormat.parse(endDate);
                stb.append(dateFormat.format(start) + STR_TO_W_LINE_END);
                stb.append(dateFormat.format(end));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return stb.toString();
    }
    
    public final String convertArrayOfStringsToList(final Object arrayOfStrings) {
        final int[] types = (int[]) arrayOfStrings;
        final StringBuffer stb = new StringBuffer();
        
        for (int i = 0; i < types.length; i++) {
            if (types[i] == ReportingConstants.REPORT_FILTER_TRANSACTION_ALL) {
                // TODO
                return ReportingUtilImpl.findFilterStringStatic(ReportingConstants.REPORT_FILTER_TRANSACTION_ALL);
            }
            
            stb.append(ReportingUtilImpl.findFilterStringStatic(types[i]));
            if (i < (types.length) - 1) {
                stb.append(", ");
            }
        }
        
        return stb.toString();
    }
    
    public final String getCardNumberPadded(final String cardNumString, final int expectedLength) {
        if ((cardNumString == null) || (cardNumString.length() <= 0)) {
            return ReportingConstants.REPORT_STRINGS_NA;
        }
        
        final int len = cardNumString.length();
        
        if (len >= expectedLength) {
            if (cardNumString.startsWith("B")) {
                return cardNumString;
            } else {
                return cardNumString.substring(len - expectedLength, len);
            }
        }
        
        final StringBuffer stb = new StringBuffer();
        for (int i = 0; i < (expectedLength - len); i++) {
            stb.append("0");
        }
        stb.append(cardNumString);
        
        return stb.toString();
    }
    
    public final String getGroupExpression(final String groupBy, final String groupFieldOn, final Date startDate, final String timeZone) {
        String groupFieldValue = groupFieldOn;
        if (ReportingUtilImpl.findFilterStringStatic(ReportingConstants.REPORT_FILTER_GROUP_BY_DAY).equals(groupBy)) {
            if (startDate == null) {
                return ReportingConstants.REPORT_STRINGS_NA;
            }
            final SimpleDateFormat dateFormat = new SimpleDateFormat(DateUtil.DATE_ONLY_FORMAT);
            dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
            return dateFormat.format(startDate);
        } else if (ReportingUtilImpl.findFilterStringStatic(ReportingConstants.REPORT_FILTER_GROUP_BY_MONTH).equals(groupBy)) {
            if (startDate == null) {
                return ReportingConstants.REPORT_STRINGS_NA;
            }
            final SimpleDateFormat dateFormat = new SimpleDateFormat(DateUtil.MONTH_ONLY_FORMAT);
            dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
            return dateFormat.format(startDate);
        }
        if (groupFieldValue == null
            && ReportingUtilImpl.findFilterStringStatic(ReportingConstants.REPORT_FILTER_GROUP_BY_ACCOUNT).equalsIgnoreCase(groupBy)) {
            groupFieldValue = ReportingConstants.REPORT_STRINGS_UNASSIGNED;
        }
        return groupFieldValue;
    }
    
    public final String getGroupName(final String groupBy, final String groupFieldValue, final Date startDate, final String timeZone) {
        final StringBuffer groupName = new StringBuffer(groupBy);
        
        groupName.append(":  ");
        
        if (ReportingUtilImpl.findFilterStringStatic(ReportingConstants.REPORT_FILTER_GROUP_BY_DAY).equals(groupBy)
            || ReportingUtilImpl.findFilterStringStatic(ReportingConstants.REPORT_FILTER_GROUP_BY_LAST_SEEN_TIME).equals(groupBy)) {
            if (startDate == null) {
                groupName.append(ReportingConstants.REPORT_STRINGS_NA);
            } else {
                final SimpleDateFormat dateFormat = new SimpleDateFormat(DateUtil.DATE_ONLY_FORMAT);
                dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
                groupName.append(dateFormat.format(startDate));
            }
        } else if (ReportingUtilImpl.findFilterStringStatic(ReportingConstants.REPORT_FILTER_GROUP_BY_MONTH).equals(groupBy)) {
            if (startDate == null) {
                groupName.append(ReportingConstants.REPORT_STRINGS_NA);
            } else {
                final SimpleDateFormat dateFormat = new SimpleDateFormat(DateUtil.MONTH_ONLY_FORMAT);
                dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
                groupName.append(dateFormat.format(startDate));
            }
        } else if (ReportingUtilImpl.findFilterStringStatic(ReportingConstants.REPORT_FILTER_LOCATION_ROUTE_TREE).equals(groupBy)
                   && groupFieldValue == null) {
            groupName.append("(Not in Route)");
        } else {
            if (groupFieldValue != null) {
                groupName.append(groupFieldValue);
            } else {
                groupName.append(ReportingConstants.REPORT_STRINGS_UNASSIGNED);
            }
        }
        
        return groupName.toString();
    }
    
    public final String getStartTime(final String groupBy, final String groupFieldValue, final Date startDate, final String startDateFromUser,
        final String timeZone) {
        String str = "";
        String userInputStartDate = startDateFromUser;
        
        if (StringUtils.isBlank(userInputStartDate) || userInputStartDate.equalsIgnoreCase(StandardConstants.STRING_NULL)) {
            userInputStartDate = DateUtil.createDateString(new SimpleDateFormat(DateUtil.REPORTS_FORMAT),
                                                           TimeZone.getTimeZone(ReportingConstants.SERVER_REPORTS_SCRIPTLET_TIMEZONE_UTC),
                                                           WebCoreConstants.NO_START_DATE);
        }
        
        if (ReportingUtilImpl.findFilterStringStatic(ReportingConstants.REPORT_FILTER_GROUP_BY_DAY).equals(groupBy)) {
            final SimpleDateFormat sdf = new SimpleDateFormat(DateUtil.REPORTS_FORMAT);
            sdf.setTimeZone(TimeZone.getTimeZone(ReportingConstants.SERVER_REPORTS_SCRIPTLET_TIMEZONE_UTC));
            final Date date2Time = sdf.parse(userInputStartDate, new ParsePosition(0));
            SimpleDateFormat dateFormat = new SimpleDateFormat(DateUtil.DATE_ONLY_FORMAT);
            dateFormat.setTimeZone(TimeZone.getTimeZone(ReportingConstants.SERVER_REPORTS_SCRIPTLET_TIMEZONE_UTC));
            
            if (LOGGER.isDebugEnabled()) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append("++++++ dateFormat.format(startDate): ").append(dateFormat.format(startDate));
                bdr.append(STR_DATE_FORMAT_COLON).append(dateFormat.format(date2Time));
                LOGGER.debug(bdr.toString());
            }
            if (dateFormat.format(startDate).equals(dateFormat.format(date2Time))) {
                dateFormat = new SimpleDateFormat(DateUtil.DATABASE_FORMAT);
                dateFormat.setTimeZone(TimeZone.getTimeZone(ReportingConstants.SERVER_REPORTS_SCRIPTLET_TIMEZONE_UTC));
                str = dateFormat.format(date2Time);
            } else {
                str = dateFormat.format(startDate);
                str = str + " 00:00:00";
            }
        } else {
            final SimpleDateFormat sdf = new SimpleDateFormat(DateUtil.REPORTS_FORMAT);
            sdf.setTimeZone(TimeZone.getTimeZone(ReportingConstants.SERVER_REPORTS_SCRIPTLET_TIMEZONE_UTC));
            final Date date2Time = sdf.parse(userInputStartDate, new ParsePosition(0));
            SimpleDateFormat dateFormat = new SimpleDateFormat(DateUtil.MONTH_ONLY_FORMAT);
            dateFormat.setTimeZone(TimeZone.getTimeZone(ReportingConstants.SERVER_REPORTS_SCRIPTLET_TIMEZONE_UTC));
            if (dateFormat.format(startDate).equals(dateFormat.format(date2Time))) {
                dateFormat = new SimpleDateFormat(DateUtil.DATABASE_FORMAT);
                dateFormat.setTimeZone(TimeZone.getTimeZone(ReportingConstants.SERVER_REPORTS_SCRIPTLET_TIMEZONE_UTC));
                str = dateFormat.format(date2Time);
            } else {
                str = dateFormat.format(startDate);
                str = str + "-01 00:00:00";
            }
        }
        
        if (LOGGER.isDebugEnabled()) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("++++ startDate: ").append(startDate).append(", UserInputStartDate: ").append(userInputStartDate);
            bdr.append(STR_STRING_W_COMMA_COLON).append(str);
            LOGGER.debug(bdr.toString());
        }
        return str;
    }
    
    public final String getEndTime(final String groupBy, final String groupFieldValue, final Date startDate, final String endDateFromUser,
        final String timeZone) {
        String str = "";
        String userInputEndDate = endDateFromUser;
        
        if (StringUtils.isBlank(userInputEndDate) || userInputEndDate.equalsIgnoreCase(StandardConstants.STRING_NULL)) {
            userInputEndDate = DateUtil.createDateString(new SimpleDateFormat(DateUtil.REPORTS_FORMAT),
                                                         TimeZone.getTimeZone(ReportingConstants.SERVER_REPORTS_SCRIPTLET_TIMEZONE_UTC),
                                                         WebCoreConstants.NO_END_DATE);
        }
        
        if (ReportingUtilImpl.findFilterStringStatic(ReportingConstants.REPORT_FILTER_GROUP_BY_DAY).equals(groupBy)) {
            final SimpleDateFormat sdf = new SimpleDateFormat(DateUtil.REPORTS_FORMAT);
            sdf.setTimeZone(TimeZone.getTimeZone(ReportingConstants.SERVER_REPORTS_SCRIPTLET_TIMEZONE_UTC));
            final Date date2Time = sdf.parse(userInputEndDate, new ParsePosition(0));
            SimpleDateFormat dateFormat = new SimpleDateFormat(DateUtil.DATE_ONLY_FORMAT);
            dateFormat.setTimeZone(TimeZone.getTimeZone(ReportingConstants.SERVER_REPORTS_SCRIPTLET_TIMEZONE_UTC));
            
            if (LOGGER.isDebugEnabled()) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append("++++++ dateFormat.format(endDate): ").append(dateFormat.format(startDate));
                bdr.append(STR_DATE_FORMAT_COLON).append(dateFormat.format(date2Time));
                LOGGER.debug(bdr.toString());
            }
            if (dateFormat.format(startDate).equals(dateFormat.format(date2Time))) {
                dateFormat = new SimpleDateFormat(DateUtil.DATABASE_FORMAT);
                dateFormat.setTimeZone(TimeZone.getTimeZone(ReportingConstants.SERVER_REPORTS_SCRIPTLET_TIMEZONE_UTC));
                str = dateFormat.format(date2Time);
            } else {
                str = dateFormat.format(startDate);
                str = str + " 23:59:59";
            }
        } else {
            final SimpleDateFormat sdf = new SimpleDateFormat(DateUtil.REPORTS_FORMAT);
            sdf.setTimeZone(TimeZone.getTimeZone(ReportingConstants.SERVER_REPORTS_SCRIPTLET_TIMEZONE_UTC));
            final Date date2Time = sdf.parse(userInputEndDate, new ParsePosition(0));
            SimpleDateFormat dateFormat = new SimpleDateFormat(DateUtil.MONTH_ONLY_FORMAT);
            dateFormat.setTimeZone(TimeZone.getTimeZone(ReportingConstants.SERVER_REPORTS_SCRIPTLET_TIMEZONE_UTC));
            if (dateFormat.format(startDate).equals(dateFormat.format(date2Time))) {
                dateFormat = new SimpleDateFormat(DateUtil.DATABASE_FORMAT);
                dateFormat.setTimeZone(TimeZone.getTimeZone(ReportingConstants.SERVER_REPORTS_SCRIPTLET_TIMEZONE_UTC));
                str = dateFormat.format(date2Time);
            } else {
                str = dateFormat.format(startDate);
                str = str + "-31 23:59:59";
            }
        }
        
        if (LOGGER.isDebugEnabled()) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("++++ endDate: ").append(startDate).append(", UserInputEndDate: ").append(userInputEndDate);
            bdr.append(STR_STRING_W_COMMA_COLON).append(str);
            LOGGER.debug(bdr.toString());
        }
        return str;
    }
    
    /**
     * find out if the record is duplicated or not
     * 
     * @param paystationId
     * @param paystationGroupId
     * @param duplicatedPaystationRouteMap
     * @return true if the record is duplicated, false otherwise
     */
    public final boolean isRecordDuplicated(final Integer paystationId, final Integer paystationRouteId,
        final Map<Integer, List<Integer>> duplicatedPaystationRouteMap) {
        if (paystationRouteId == null) {
            return false;
        }
        
        final List<Integer> duplicatedPaystationRouteIds = duplicatedPaystationRouteMap.get(paystationId);
        
        if (duplicatedPaystationRouteIds != null && duplicatedPaystationRouteIds.contains(paystationRouteId)) {
            return true;
        }
        
        return false;
    }
    
    public final String getUniqueRateName(final String rateName, final Long rateId, final Integer rateValue,
        final Map<String, Map<String, Integer>> nameMap) {
        final StringBuffer key = new StringBuffer();
        key.append(Long.toString(rateId));
        
        Map<String, Integer> indexMap = nameMap.get(rateName);
        if (indexMap == null) {
            indexMap = new HashMap<String, Integer>();
            indexMap.put(key.toString(), 0);
            nameMap.put(rateName, indexMap);
            return rateName;
        } else {
            final Integer index = indexMap.get(key.toString());
            if (index != null) {
                return rateName + (index.intValue() != 0
                        ? StandardConstants.STRING_OPEN_PARENTHESIS + index + StandardConstants.STRING_CLOSE_PARENTHESIS : "");
            } else {
                final int nextIndex = indexMap.size();
                indexMap.put(key.toString(), nextIndex);
                return rateName + "(" + nextIndex + ")";
            }
        }
    }
    
    /**
     * This method returns the processor name by getting it from the processorMap
     * which is constructed when the ReportingUtilImpl is loaded (via
     * the @postContruct)
     * 
     * @param processorName
     *            Key in message.properties
     * @return value for processor name in message.properties
     */
    public final String getProcessorName(final String processorName) {
        if (processorName == null || "".equals(processorName)) {
            return "";
        } else {
            return ReportingUtilImpl.getProcessorPropertyName(processorName);
        }
    }
    
    /**
     * This method applies URLdecoding on the parameter string
     * 
     * @param text
     *            Key in message.properties
     * @return UrlEncoded text
     */
    public final String applyURLEncoding(final String text) {
        String returnText = null;
        try {
            returnText = URLDecoder.decode(text, WebSecurityConstants.URL_ENCODING_LATIN1);
        } catch (UnsupportedEncodingException uee) {
            returnText = text;
        }
        
        return returnText;
    }
    
}
