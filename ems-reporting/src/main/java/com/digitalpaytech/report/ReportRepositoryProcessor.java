package com.digitalpaytech.report;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.ReportRepositoryService;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.StandardConstants;

/**
 * This class checks ReportDefinition for online and scheduled reports that are ready to run and creates an entry in the ReportQueue.
 * 
 * @author Murat Erozlu
 * 
 */
@Component("reportRepositoryProcessor")
public class ReportRepositoryProcessor {
    private static final Logger LOGGER = Logger.getLogger(ReportRepositoryProcessor.class);
    
    private int maxSize = ReportingConstants.REPORTS_DEFAULT_SIZE_LIMIT_KB;
    private int maxDays = ReportingConstants.REPORTS_DEFAULT_TIME_LIMIT_DAYS;
    private int toBeDeletedSize = ReportingConstants.REPORTS_DEFAULT_WARNING_SIZE_LIMIT_KB;
    private int toBeDeletedDays = ReportingConstants.REPORTS_DEFAULT_WARNING_TIME_LIMIT_DAYS;
    private int systemAdminMaxDays = ReportingConstants.REPORTS_DEFAULT_SYSTEM_ADMIN_TIME_LIMIT_DAYS;
    
    @Autowired
    private ReportRepositoryService reportRepositoryService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private ClusterMemberService clusterMemberService;
    
    public final void setReportRepositoryService(final ReportRepositoryService reportRepositoryService) {
        this.reportRepositoryService = reportRepositoryService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final void setClusterMemberService(final ClusterMemberService clusterMemberService) {
        this.clusterMemberService = clusterMemberService;
    }
    
    @PostConstruct
    private void initializeLimits() {
        try {
            this.maxSize = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.REPORTS_SIZE_LIMIT_KB,
                ReportingConstants.REPORTS_DEFAULT_SIZE_LIMIT_KB);
            this.maxDays = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.REPORTS_TIME_LIMIT_DAYS,
                ReportingConstants.REPORTS_DEFAULT_TIME_LIMIT_DAYS);
            this.toBeDeletedSize = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.REPORTS_SIZE_WARNING_KB,
                ReportingConstants.REPORTS_DEFAULT_WARNING_SIZE_LIMIT_KB);
            this.toBeDeletedDays = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.REPORTS_TIME_WARNING_DAYS,
                ReportingConstants.REPORTS_DEFAULT_WARNING_TIME_LIMIT_DAYS);
            this.systemAdminMaxDays = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.REPORTS_SYSTEM_ADMIN_TIME_LIMIT_DAYS,
                ReportingConstants.REPORTS_DEFAULT_SYSTEM_ADMIN_TIME_LIMIT_DAYS);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        checkAndProcessOldReportsBySize();
    }
    
    /**
     * Check repository for systemAdmin files older than 24 hours and and delete them
     */
    //    @Scheduled(cron = "${check.stored.files.days.schedule.cron.expression}")
    public void checkAndProcessOldSystemAdminReportsByHour() {
        if (!getClusterName().equals(getReportServerName())) {
            LOGGER.debug(getWrongServerLogString());
            return;
        }         
    
        final Calendar limitDate = Calendar.getInstance();
        limitDate.add(Calendar.DAY_OF_YEAR, -this.systemAdminMaxDays);
        
        this.reportRepositoryService.deleteOldSystemAdminReports(limitDate.getTime());
    }
    
    /**
     * Check repository for files older than 45 days and delete them
     */
    //    @Scheduled(cron = "${check.stored.files.days.schedule.cron.expression}")
    public void checkAndProcessOldReportsByDay() {
        if (!getClusterName().equals(getReportServerName())) {
            LOGGER.debug(getWrongServerLogString());
            return;
        }        
        
        final Calendar limitDate = Calendar.getInstance();
        limitDate.set(Calendar.HOUR_OF_DAY, 0);
        limitDate.set(Calendar.MINUTE, 0);
        limitDate.set(Calendar.SECOND, 0);
        limitDate.add(Calendar.DAY_OF_YEAR, -this.maxDays);
        
        final Calendar limitWarningDate = Calendar.getInstance();
        limitWarningDate.set(Calendar.HOUR_OF_DAY, 0);
        limitWarningDate.set(Calendar.MINUTE, 0);
        limitWarningDate.set(Calendar.SECOND, 0);
        limitWarningDate.add(Calendar.DAY_OF_YEAR, -this.toBeDeletedDays);
        
        this.reportRepositoryService.deleteOldReportsByDay(limitDate.getTime());
        this.reportRepositoryService.addWarningToOldReportsByDay(limitWarningDate.getTime());        
    }
    
    //    @Scheduled(cron = "${check.stored.files.size.schedule.cron.expression}")
    public final void checkAndProcessOldReportsBySize() {
        if (!getClusterName().equals(getReportServerName())) {
            LOGGER.debug(getWrongServerLogString());
            return;
        }        

        final List<Object[]> userTotalList = this.reportRepositoryService.getUserAccountFileSizeTotals();
        
        for (Object[] userTotal : userTotalList) {
            final int totalSize = ((BigDecimal) userTotal[1]).intValue();
            if (totalSize > this.maxSize) {
                this.reportRepositoryService.deleteOldestReports((Integer) userTotal[0], this.maxSize, totalSize);
            }
        }
        
        final List<Object[]> userUnWarnedTotalList = this.reportRepositoryService.getUserAccountUnWarnedFileSizeTotals();
        
        for (Object[] userTotal : userUnWarnedTotalList) {
            final int totalSize = ((BigDecimal) userTotal[1]).intValue();
            if (totalSize > this.toBeDeletedSize) {
                this.reportRepositoryService.addWarningToOldestReports((Integer) userTotal[0], this.toBeDeletedSize, totalSize);
            }
        }
    }
    
    public final void cleanupOldAdhocReport() {
        if (!getClusterName().equals(getReportServerName())) {
            LOGGER.debug(getWrongServerLogString());
            return;
        }
        
        int cleanupMins = EmsPropertiesService.DEFAULT_REPORTS_ADHOC_CLEANUP_MINS;
        try {
            cleanupMins = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.REPORTS_ADHOC_CLEANUP_MINS,
                EmsPropertiesService.DEFAULT_REPORTS_ADHOC_CLEANUP_MINS);
        } catch (Exception e) {
            Log.error("Could not resolve \"" + EmsPropertiesService.REPORTS_ADHOC_CLEANUP_MINS + "\", the default value is used: "
                      + EmsPropertiesService.DEFAULT_REPORTS_ADHOC_CLEANUP_MINS, e);
        }
        
        final Date targetTime = new Date(System.currentTimeMillis() - (cleanupMins * 60000L));
        try {
            final int deleted = this.reportRepositoryService.deleteOlderAdhocReport(targetTime);
            Log.info("Cleaned up " + deleted + " ReportRepository(s) which are older than " + targetTime);
        } catch (Exception e) {
            Log.error("Failed to cleanup adhoc reports !", e);
        }        
    }
    
    private String getClusterName() {
        return this.clusterMemberService.getClusterName();
    }    
    
    private String getReportServerName() {
        return this.emsPropertiesService.getPropertyValue(EmsPropertiesService.REPORTS_BACKGROUND_SERVER);
    }
    
    private String getWrongServerLogString() {
        return "Report processing aborted on " + getClusterName() 
                + ". Should run on " + getReportServerName() + StandardConstants.STRING_DOT;
    }    
}
