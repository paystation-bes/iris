package com.digitalpaytech.report;

import java.util.Calendar;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.task.TaskExecutor;
import org.springframework.core.task.TaskRejectedException;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.report.handler.AuditThread;
import com.digitalpaytech.report.handler.CollectionSummaryThread;
import com.digitalpaytech.report.handler.CouponUsageSummaryThread;
import com.digitalpaytech.report.handler.InventoryThread;
import com.digitalpaytech.report.handler.MaintenanceSummaryThread;
import com.digitalpaytech.report.handler.PayStationSummaryThread;
import com.digitalpaytech.report.handler.RateSummaryThread;
import com.digitalpaytech.report.handler.RateThread;
import com.digitalpaytech.report.handler.ReplenishThread;
import com.digitalpaytech.report.handler.ReportBaseThread;
import com.digitalpaytech.report.handler.StallReportThread;
import com.digitalpaytech.report.handler.TaxesThread;
import com.digitalpaytech.report.handler.TransactionAllThread;
import com.digitalpaytech.report.handler.TransactionCCProcessingThread;
import com.digitalpaytech.report.handler.TransactionCCRefundThread;
import com.digitalpaytech.report.handler.TransactionCCRetryThread;
import com.digitalpaytech.report.handler.TransactionCashThread;
import com.digitalpaytech.report.handler.TransactionCreditCardThread;
import com.digitalpaytech.report.handler.TransactionDelayThread;
import com.digitalpaytech.report.handler.TransactionPatrollerCardThread;
import com.digitalpaytech.report.handler.TransactionSmartCardThread;
import com.digitalpaytech.report.handler.TransactionValueCardThread;
import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.ReportDefinitionService;
import com.digitalpaytech.service.ReportQueueService;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.StandardConstants;

/**
 * This class checks ReportDefinition for online and scheduled reports that are
 * ready to run and creates an entry in the ReportQueue.
 *
 * @author Murat Erozlu
 *
 */
@Component("reportQueueProcessor")
public class ReportQueueProcessor implements ApplicationContextAware {
    private static final Logger LOGGER = Logger.getLogger(ReportQueueProcessor.class);

    private int idleTimeMinutes = ReportingConstants.REPORTS_DEFAULT_QUEUE_IDLE_TIME_LIMIT_MINUTES;

    private ApplicationContext applicationContext;

    @Autowired
    private TaskExecutor taskExecutor;

    @Autowired
    private ReportDefinitionService reportDefinitionService;

    @Autowired
    private ReportQueueService reportQueueService;

    @Autowired
    private ClusterMemberService clusterMemberService;

    @Autowired
    private EmsPropertiesService emsPropertiesService;

    public final void setTaskExecutor(final TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }

    public final void setReportDefinitionService(final ReportDefinitionService reportDefinitionService) {
        this.reportDefinitionService = reportDefinitionService;
    }

    public final void setReportQueueService(final ReportQueueService reportQueueService) {
        this.reportQueueService = reportQueueService;
    }

    public final void setClusterMemberService(final ClusterMemberService clusterMemberService) {
        this.clusterMemberService = clusterMemberService;
    }

    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }

    @PostConstruct
    private void initializeLimits() {
        try {
            this.idleTimeMinutes = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.REPORTS_QUEUE_IDLE_TIME_LIMIT_MINUTES,
                                                                                   ReportingConstants.REPORTS_DEFAULT_QUEUE_IDLE_TIME_LIMIT_MINUTES);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Check ReportDefinition table for ready reports. Create an entry for all
     * ready reports in ReportQueue
     */
    //    @Scheduled(fixedDelay = 30000)
    public void checkAndProcessQueuedReports() {

        LOGGER.info("checkAndProcessQueuedReports starting");

        if (!getClusterName().equals(getReportServerName())) {
            LOGGER.debug(getWrongServerLogString());
            return;
        }

        LOGGER.info("checkAndProcessQueuedReports running on this server");

        ReportQueue reportQueue = null;
        final int maxActiveThreads = ((ThreadPoolTaskExecutor) this.taskExecutor).getMaxPoolSize();

        LOGGER.info("checkAndProcessQueuedReports maxPoolSize:" + maxActiveThreads);

        boolean isThreadPoolFull = false;
        do {
            try {
                LOGGER.info("checkAndProcessQueuedReports trying to run the query");
                reportQueue = (ReportQueue) this.reportQueueService
                        .findQueuedReportWithLock(this.clusterMemberService.getClusterName());
                LOGGER.info("checkAndProcessQueuedReports query completed ");
                if (reportQueue != null) {
                    LOGGER.info("checkAndProcessQueuedReports returned reportQueue is " + reportQueue.getId());
                    try {
                        LOGGER.info("checkAndProcessQueuedReports starting Thread to run report");
                        this.taskExecutor.execute(getReportThread(reportQueue));
                        LOGGER.info("checkAndProcessQueuedReports Thread completed ");
                    } catch (TaskRejectedException tre) {
                        LOGGER.info("checkAndProcessQueuedReports TaskRejected exception", tre);
                        try {
                            while (((ThreadPoolTaskExecutor) this.taskExecutor).getActiveCount() == maxActiveThreads) {
                                Thread.sleep(1000);
                            }
                            this.taskExecutor.execute(getReportThread(reportQueue));
                        } catch (Exception e) {

                        }
                    }
                    if (((ThreadPoolTaskExecutor) this.taskExecutor).getActiveCount() == maxActiveThreads) {
                        isThreadPoolFull = true;
                        break;
                    }
                }
            } catch (Exception e) {
                LOGGER.info("checkAndProcessQueuedReports Exception ", e);
            } finally {
                LOGGER.info("checkAndProcessQueuedReports Finished iteration ");
            }
        } while (reportQueue != null && !isThreadPoolFull);
    }

    //    @Scheduled(fixedDelay = 900000)
    public final void cleanupStalledReports() {
        if (!getClusterName().equals(getReportServerName())) {
            LOGGER.debug(getWrongServerLogString());
            return;
        }
        final Calendar idleTime = Calendar.getInstance();
        idleTime.add(Calendar.MINUTE, -this.idleTimeMinutes);

        final String stalledReportList = this.reportQueueService.fixStalledReports(this.clusterMemberService.getClusterName(), idleTime.getTime());
        if (stalledReportList != null) {
            LOGGER.warn("Fixed stalled reports in queue: " + stalledReportList);
        }
    }

    private ReportBaseThread getReportThread(final ReportQueue reportQueue) {
        final ReportDefinition reportDefinition = this.reportDefinitionService.getReportDefinition(reportQueue.getReportDefinition().getId());

        ReportBaseThread newThread = null;

        switch (reportDefinition.getReportType().getId()) {
            case ReportingConstants.REPORT_TYPE_TRANSACTION_ALL:
                newThread = new TransactionAllThread(reportDefinition, reportQueue, this.applicationContext);
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_CASH:
                newThread = new TransactionCashThread(reportDefinition, reportQueue, this.applicationContext);
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_CREDIT_CARD:
                newThread = new TransactionCreditCardThread(reportDefinition, reportQueue, this.applicationContext);
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_CC_REFUND:
                newThread = new TransactionCCRefundThread(reportDefinition, reportQueue, this.applicationContext);
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_CC_PROCESSING:
                newThread = new TransactionCCProcessingThread(reportDefinition, reportQueue, this.applicationContext);
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_CC_RETRY:
                newThread = new TransactionCCRetryThread(reportDefinition, reportQueue, this.applicationContext);
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_PATROLLER_CARD:
                newThread = new TransactionPatrollerCardThread(reportDefinition, reportQueue, this.applicationContext);
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_RATE:
                newThread = new RateThread(reportDefinition, reportQueue, this.applicationContext);
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_RATE_SUMMARY:
                newThread = new RateSummaryThread(reportDefinition, reportQueue, this.applicationContext);
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_SMART_CARD:
                newThread = new TransactionSmartCardThread(reportDefinition, reportQueue, this.applicationContext);
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_VALUE_CARD:
                newThread = new TransactionValueCardThread(reportDefinition, reportQueue, this.applicationContext);
                break;
            case ReportingConstants.REPORT_TYPE_STALL_REPORTS:
                newThread = new StallReportThread(reportDefinition, reportQueue, this.applicationContext);
                break;
            case ReportingConstants.REPORT_TYPE_COUPON_USAGE_SUMMARY:
                newThread = new CouponUsageSummaryThread(reportDefinition, reportQueue, this.applicationContext);
                break;
            case ReportingConstants.REPORT_TYPE_AUDIT_REPORTS:
                newThread = new AuditThread(reportDefinition, reportQueue, this.applicationContext);
                break;
            case ReportingConstants.REPORT_TYPE_REPLENISH_REPORTS:
                newThread = new ReplenishThread(reportDefinition, reportQueue, this.applicationContext);
                break;
            case ReportingConstants.REPORT_TYPE_PAY_STATION_SUMMARY:
                newThread = new PayStationSummaryThread(reportDefinition, reportQueue, this.applicationContext);
                break;
            case ReportingConstants.REPORT_TYPE_TAX_REPORT:
                newThread = new TaxesThread(reportDefinition, reportQueue, this.applicationContext);
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_DELAY:
                newThread = new TransactionDelayThread(reportDefinition, reportQueue, this.applicationContext);
                break;
            case ReportingConstants.REPORT_TYPE_INVENTORY:
            case ReportingConstants.REPORT_TYPE_PAYSTATION_INVENTORY:
                newThread = new InventoryThread(reportDefinition, reportQueue, this.applicationContext);
                break;
            case ReportingConstants.REPORT_TYPE_MAINTENANCE_SUMMARY:
                newThread = new MaintenanceSummaryThread(reportDefinition, reportQueue, this.applicationContext);
                break;
            case ReportingConstants.REPORT_TYPE_COLLECTION_SUMMARY:
                newThread = new CollectionSummaryThread(reportDefinition, reportQueue, this.applicationContext);
                break;
            default:
                break;
        }
        return newThread;
    }

    @Override
    public final void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;

    }

    private String getClusterName() {
        return this.clusterMemberService.getClusterName();
    }

    private String getReportServerName() {
        return this.emsPropertiesService.getPropertyValue(EmsPropertiesService.REPORTS_BACKGROUND_SERVER);
    }

    private String getWrongServerLogString() {
        return "Report processing aborted on " + getClusterName()
        + ". Should run on " + getReportServerName() + StandardConstants.STRING_DOT;
    }
}
