package com.digitalpaytech.report.handler;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.ReportingConstants;

public class TransactionCCRetryThread extends TransactionBaseThread {
    private static final Logger LOGGER = Logger.getLogger(TransactionCCRetryThread.class);
    
    public TransactionCCRetryThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue,
            final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext);
    }
    
    @Override
    protected final String generateSQLString() throws Exception {
        final boolean isSummary = this.reportDefinition.isIsDetailsOrSummary();
        
        final StringBuilder sqlQuery = new StringBuilder("SELECT ");
        
        // Fields
        getFieldsCreditCardRetry(sqlQuery, isSummary, isSummary);
        super.getFieldsRateName(sqlQuery, isSummary);
        super.getFieldsLocationName(sqlQuery, isSummary, true);
        
        // Tables & Where
        getTablesCardRetry(sqlQuery, true, isSummary);
        getWhereStandardRetryTransaction(sqlQuery, true);
        
        // Credit Card Last 4 Digits
        if (ReportingConstants.REPORT_FILTER_CARD_NUMBER_ALL != super.getFilterTypeId(this.reportDefinition.getCardNumberFilterTypeId())) {
            sqlQuery.append(" AND retrytrans.Last4DigitsOfCardNumber=" + this.reportDefinition.getCardNumberSpecificFilter());
        }
        
        if (isSummary && ReportingConstants.REPORT_FILTER_GROUP_BY_NONE != super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId())) {
            sqlQuery.append(" UNION ").append(generateCreditCardRetryReportGroupSummaryQuery());
        }
        
        // Order By
        getOrderByCreditCardRetry(sqlQuery, isSummary);
        return sqlQuery.toString();
    }
    
    @Override
    protected final String createSqlRecordCountQuery() throws Exception {
        final boolean isSummary = this.reportDefinition.isIsDetailsOrSummary();
        final StringBuilder sqlQuery = new StringBuilder("SELECT ");
        
        // Fields
        sqlQuery.append(" COUNT(*) ");
        
        // Tables & Where
        getTablesCardRetry(sqlQuery, false, isSummary);
        getWhereStandardRetryTransaction(sqlQuery, false);
        
        // Credit Card Last 4 Digits
        if (ReportingConstants.REPORT_FILTER_CARD_NUMBER_ALL != super.getFilterTypeId(this.reportDefinition.getCardNumberFilterTypeId())) {
            sqlQuery.append(" AND retrytrans.Last4DigitsOfCardNumber=" + this.reportDefinition.getCardNumberSpecificFilter());
        }
        
        return sqlQuery.toString();
    }
    
    private String generateCreditCardRetryReportGroupSummaryQuery() throws Exception {
        final StringBuilder sqlQuery = new StringBuilder("(SELECT ");
        
        // Fields
        getFieldsCreditCardRetry(sqlQuery, true, false);
        
        // Tables & Where
        getTablesCardRetry(sqlQuery, true, false);
        getWhereStandardRetryTransaction(sqlQuery, true);
        
        if (ReportingConstants.REPORT_FILTER_CARD_NUMBER_ALL != super.getFilterTypeId(this.reportDefinition.getCardNumberFilterTypeId())) {
            sqlQuery.append(" AND retrytrans.Last4DigitsOfCardNumber=" + this.reportDefinition.getCardNumberSpecificFilter());
        }
        
        sqlQuery.append(" GROUP BY GroupName)");
        
        return sqlQuery.toString();
    }
    
    private void getTablesCardRetry(final StringBuilder query, final boolean generateReport, final boolean isOverallSummary) {
        query.append(" FROM CardRetryTransaction retrytrans ");
        // getTableJoinToTransactionFromProcessorTransaction(reportsForm, query,
        // generateReport);
        query.append("INNER JOIN PointOfSale pos ON retrytrans.PointOfSaleId = pos.Id ");
        
        query.append("INNER JOIN CardRetryTransactionType retrytranstype ON retrytrans.CardRetryTransactionTypeId=retrytranstype.Id ");
        query.append("LEFT JOIN Location l ON pos.LocationId = l.Id ");
        
        final int groupFilterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (!isOverallSummary && ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == groupFilterTypeId
            && this.reportDefinition.isIsCsvOrPdf()) {
            query.append("LEFT JOIN RoutePOS rpos ON pos.Id=rpos.PointOfSaleId ");
            query.append("LEFT JOIN Route r ON rpos.RouteId=r.Id ");
        }
        
        if (!isOverallSummary) {
            query.append("LEFT JOIN Purchase pur ON (retrytrans.PointOfSaleId = pur.PointOfSaleId AND retrytrans.PurchasedDate = pur.PurchaseGMT AND retrytrans.TicketNumber = pur.PurchaseNumber) ");
            query.append("LEFT JOIN UnifiedRate ur ON (pur.UnifiedRateId = ur.Id) ");
            
            if (ReportingConstants.REPORT_FILTER_GROUP_BY_MERCHANT_ACCOUNT == groupFilterTypeId) {
                query.append("LEFT JOIN ProcessorTransaction proctrans ON ");
                query.append("(pos.Id=proctrans.PointOfSaleId AND retrytrans.PurchasedDate = proctrans.PurchasedDate AND retrytrans.TicketNumber = proctrans.TicketNumber) ");
                query.append("LEFT JOIN MerchantAccount ma ON proctrans.MerchantAccountId = ma.Id");
            }
        }
        
        // Filter for hidden POS
        getFromHiddenPointOfSales(query, "pos");
        
    }
    
    private void getFieldsCreditCardRetry(final StringBuilder query, final boolean isSummary, final boolean isOverallSummary) {
        final int groupingType = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        
        if (isSummary) {
            if (!isOverallSummary && ReportingConstants.REPORT_FILTER_GROUP_BY_CARD_TYPE == groupingType) {
                query.append("0 AS IsOverallSummary");
                query.append(", CONCAT('" + reportingUtil.findFilterString(groupingType) + ":  ', retrytrans.CardType) AS GroupName");
                query.append(", retrytrans.CardType AS GroupOrderValue");
            } else {
                super.getFieldsGroupField(query, isSummary, isOverallSummary);
            }
            query.append(", SUM(IF(retrytrans.NumRetries != 32766 AND retrytrans.NumRetries != 32767 AND retrytrans.CardRetryTransactionTypeId = 0, 1, 0)) AS BatchedRetryN");
            query.append(", SUM(IF(retrytrans.NumRetries != ").append(CardProcessingConstants.CC_RETRY_LOST_STOLEN)
                    .append(" AND retrytrans.NumRetries != ").append(CardProcessingConstants.CC_RETRY_MAX_EXCEEDED)
                    .append(" AND retrytrans.CardRetryTransactionTypeId = 0, retrytrans.Amount, 0)) AS BatchedRetry");
            query.append(", SUM(IF(retrytrans.NumRetries != ").append(CardProcessingConstants.CC_RETRY_LOST_STOLEN)
                    .append(" AND retrytrans.NumRetries != ").append(CardProcessingConstants.CC_RETRY_MAX_EXCEEDED)
                    .append(" AND retrytrans.CardRetryTransactionTypeId = 1, 1, 0)) AS StoreFwdRetryN");
            query.append(", SUM(IF(retrytrans.NumRetries != ").append(CardProcessingConstants.CC_RETRY_LOST_STOLEN)
                    .append(" AND retrytrans.NumRetries != ").append(CardProcessingConstants.CC_RETRY_MAX_EXCEEDED)
                    .append(" AND retrytrans.CardRetryTransactionTypeId = 1, retrytrans.Amount, 0)) AS StoreFwdRetry");
            query.append(", SUM(IF((retrytrans.NumRetries = ")
                    .append(CardProcessingConstants.CC_RETRY_LOST_STOLEN)
                    .append(" OR retrytrans.NumRetries = ")
                    .append(CardProcessingConstants.CC_RETRY_MAX_EXCEEDED)
                    .append(
                        ") AND (retrytrans.CardRetryTransactionTypeId = 0 OR retrytrans.CardRetryTransactionTypeId = 1), 1, 0)) AS ExceededRetryN");
            query.append(", SUM(IF((retrytrans.NumRetries = ")
                    .append(CardProcessingConstants.CC_RETRY_LOST_STOLEN)
                    .append(" OR retrytrans.NumRetries = ")
                    .append(CardProcessingConstants.CC_RETRY_MAX_EXCEEDED)
                    .append(
                        ") AND (retrytrans.CardRetryTransactionTypeId = 0 OR retrytrans.CardRetryTransactionTypeId = 1), retrytrans.Amount, 0)) AS ExceededRetry");
        } else {
            if (ReportingConstants.REPORT_FILTER_GROUP_BY_CARD_TYPE == groupingType) {
                query.append("retrytrans.CardType AS GroupField");
            } else {
                super.getFieldsGroupField(query, false, false);
            }
            if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == groupingType && this.reportDefinition.isIsCsvOrPdf()) {
                query.append(", pos.Id as PaystationId, r.Id as PaystationRouteId");
            } else {
                query.append(", 0 as PaystationId, 0 as PaystationRouteId");
            }
            
            query.append(", pos.Name AS PosName, '");
            query.append(this.timeZone);
            query.append("' AS TimeZone, pos.Name AS MachineNumber, retrytrans.TicketNumber, retrytrans.PurchasedDate,");
            query.append("retrytrans.CardType, retrytrans.Last4DigitsOfCardNumber AS CardNumber, ");
            query.append("retrytrans.NumRetries AS RetryNumber, ");
            query.append("retrytrans.Amount AS Amount, ");
            query.append("IF(retrytrans.NumRetries=0 AND retrytrans.LastRetryDate=retrytrans.CreationDate, NULL, retrytrans.LastRetryDate) AS RetryDate, ");
            query.append("retrytranstype.TypeName AS RetryType, retrytrans.CardRetryTransactionTypeId AS RetryTypeId ");
        }
    }
    
    private void getWhereStandardRetryTransaction(final StringBuilder query, final boolean generateReport) throws Exception {
        query.append(" WHERE ");
        
        // Machine Name (aka. PS ID)
        super.getWherePointOfSale(query, "pos", false);
        
        // Purchased Date & Time =
        super.getWherePurchaseGMT(query, "retrytrans");
        
        // Ticket Number
        super.getWhereTicketNumber(query, "retrytrans");
        
        // Processing Date/Time
        if (this.reportQueue.getPrimaryDateRangeBeginGmt() != null && this.reportQueue.getPrimaryDateRangeEndGmt() != null) {
            query.append(" AND retrytrans.LastRetryDate BETWEEN '");
            query.append(this.reportQueue.getPrimaryDateRangeBeginGmt());
            query.append("' AND '");
            query.append(this.reportQueue.getPrimaryDateRangeEndGmt());
            query.append("' ");
        }
        
        // Card Type
        getWhereCardType(query, "retrytrans");
        
        // Retry Transaction Type
        getWhereRetryTransactionType(query);
        
        // Filter for hidden POS
        super.getWhereHiddenPointOfSales(query);
        
    }
    
    private void getWhereRetryTransactionType(final StringBuilder query) {
        final int transType = super.getFilterTypeId(this.reportDefinition.getOtherParametersTypeFilterTypeId());
        // Offline
        if (ReportingConstants.REPORT_FILTER_RETRY_BATCHED == transType) {
            query.append(" AND retrytrans.CardRetryTransactionTypeId=0");
            // Store Forward
        } else if (ReportingConstants.REPORT_FILTER_RETRY_STORE_FORWARD == transType) {
            query.append(" AND retrytrans.CardRetryTransactionTypeId=1 ");
        }
    }
    
    private void getOrderByCreditCardRetry(final StringBuilder query, final boolean isSummary) {
        final int groupingType = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        
        if (isSummary) {
            if (ReportingConstants.REPORT_FILTER_GROUP_BY_NONE != groupingType) {
                query.append(" ORDER BY GroupOrderValue ");
            }
            return;
        }
        query.append(" ORDER BY ");
        
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_DAY == groupingType || ReportingConstants.REPORT_FILTER_GROUP_BY_MONTH == groupingType
            || ReportingConstants.REPORT_FILTER_GROUP_BY_NONE == groupingType) {
            query.append("RetryDate ");
        } else {
            query.append("GroupField, RetryDate ");
        }
    }
    
}
