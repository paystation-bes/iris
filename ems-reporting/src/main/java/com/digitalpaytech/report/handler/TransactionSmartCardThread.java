package com.digitalpaytech.report.handler;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.util.ReportingConstants;

public class TransactionSmartCardThread extends TransactionBaseThread {
    private static final Logger LOGGER = Logger.getLogger(TransactionSmartCardThread.class);
    
    public TransactionSmartCardThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue,
            final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext);
    }
    
    @Override
    protected final String generateSQLString() throws Exception {
        final boolean isSummary = this.reportDefinition.isIsDetailsOrSummary();
        
        StringBuilder sqlQuery = null;
        
        final int otherParamFilterTypeId = super.getFilterTypeId(this.reportDefinition.getOtherParametersTypeFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_TRANSACTION_ALL == otherParamFilterTypeId) {
            sqlQuery = new StringBuilder(800);
            sqlQuery.append("(");
            
            // Part 1
            sqlQuery.append(getQuerySmartCard(true, 1, isSummary, isSummary));
            sqlQuery.append(") UNION (");
            
            // Part2
            sqlQuery.append(getQuerySmartCard(true, 2, isSummary, isSummary));
            sqlQuery.append(")");
        } else {
            sqlQuery = getQuerySmartCard(true, 0, isSummary, isSummary);
        }
        
        final int groupByFilterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (isSummary && ReportingConstants.REPORT_FILTER_GROUP_BY_NONE == groupByFilterTypeId) {
            sqlQuery.append(" UNION ").append(generateSmartCardReportGroupSummaryQuery());
        }
        
        // Order By
        getOrderByStandard(sqlQuery, isSummary);
        return sqlQuery.toString();
    }
    
    @Override
    protected final String createSqlRecordCountQuery() throws Exception {
        final boolean isSummary = this.reportDefinition.isIsDetailsOrSummary();
        StringBuilder sqlQuery = null;
        
        final int otherParamFilterTypeId = super.getFilterTypeId(this.reportDefinition.getOtherParametersTypeFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_TRANSACTION_ALL == otherParamFilterTypeId) {
            sqlQuery = new StringBuilder(800);
            sqlQuery.append("(");
            
            // Part 1
            sqlQuery.append(getQuerySmartCard(false, 1, isSummary, isSummary));
            
            sqlQuery.append(") UNION (");
            
            // Part2
            sqlQuery.append(getQuerySmartCard(false, 2, isSummary, isSummary));
            
            sqlQuery.append(")");
        } else {
            sqlQuery = getQuerySmartCard(false, 0, isSummary, isSummary);
        }
        return sqlQuery.toString();
    }
    
    private StringBuilder getQuerySmartCard(final boolean generateReport, final int queryModifier, final boolean isSummary,
        final boolean isOverallSummary) throws Exception {
        final StringBuilder sqlQuery = new StringBuilder("SELECT ");
        
        // Fields
        if (generateReport) {
            super.getFieldsGroupField(sqlQuery, isSummary, isOverallSummary);
            if (!isSummary) {
                super.getFieldsStandard(sqlQuery);
            }
            super.getFieldsCash(sqlQuery, isSummary);
            super.getFieldsCreditCardTransactions(sqlQuery, isSummary);
            super.getFieldsSmartCard(sqlQuery, isSummary);
            super.getFieldsRateName(sqlQuery, isSummary);
            super.getSummaryFieldTotalTransaction(sqlQuery, isSummary);
        } else {
            sqlQuery.append(" COUNT(*) ");
        }
        
        // Tables & Where
        super.getTables(sqlQuery, generateReport, isOverallSummary);
        super.getWhereStandardTransaction(sqlQuery, queryModifier, generateReport, isOverallSummary);
        
        // Card Number
        final int cardNumFilterId = super.getFilterTypeId(this.reportDefinition.getCardNumberFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_CARD_NUMBER_SPECIFIC == cardNumFilterId) {
            sqlQuery.append(" AND (Right(pscard.SmartCardData, 8)='");
            sqlQuery.append(this.reportDefinition.getCardNumberSpecificFilter());
            sqlQuery.append("')");
        }
        
        if (isSummary && !isOverallSummary) {
            // that means building a group summary query
            sqlQuery.append(" GROUP BY GroupName");
        }
        
        return sqlQuery;
    }
    
    private String generateSmartCardReportGroupSummaryQuery() throws Exception {
        final StringBuilder sqlQuery = new StringBuilder(800);
        
        // If TransactionType = ALL then must do a union query
        final int otherParamFilterType = super.getFilterTypeId(this.reportDefinition.getOtherParametersTypeFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_TRANSACTION_ALL == otherParamFilterType) {
            sqlQuery.append("(");
            
            // Part 1
            sqlQuery.append(getQuerySmartCard(true, 1, true, false));
            sqlQuery.append(") UNION (");
            
            // Part2
            sqlQuery.append(getQuerySmartCard(true, 2, true, false));
            sqlQuery.append(")");
        } else {
            sqlQuery.append("(");
            sqlQuery.append(getQuerySmartCard(true, 0, true, false));
            sqlQuery.append(")");
        }
        
        return sqlQuery.toString();
    }
}
