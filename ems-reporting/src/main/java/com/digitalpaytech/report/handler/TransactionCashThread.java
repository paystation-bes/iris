package com.digitalpaytech.report.handler;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.util.ReportingConstants;

public class TransactionCashThread extends TransactionBaseThread {
    private static final Logger LOGGER = Logger.getLogger(TransactionCashThread.class);
    
    public TransactionCashThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue, final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext);
    }
    
    @Override
    protected final String generateSQLString() throws Exception {
        final boolean isSummary = this.reportDefinition.isIsDetailsOrSummary();
        
        final StringBuilder sqlQuery = new StringBuilder("SELECT ");
        
        // Fields
        super.getFieldsGroupField(sqlQuery, isSummary, isSummary);
        
        if (!isSummary) {
            super.getFieldsStandard(sqlQuery);
        }
        super.getFieldsCash(sqlQuery, isSummary);
        super.getFieldsRateName(sqlQuery, isSummary);
        super.getSummaryFieldTotalTransaction(sqlQuery, isSummary);
        
        // Tables & Where
        super.getTables(sqlQuery, true, isSummary);
        super.getWhereStandardTransaction(sqlQuery, true, isSummary);
        
        final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (isSummary && ReportingConstants.REPORT_FILTER_GROUP_BY_NONE != filterTypeId) {
            sqlQuery.append(" UNION ").append(generateCashReportGroupSummaryQuery());
        }
        
        // Order By
        super.getOrderByStandard(sqlQuery, isSummary);
        return sqlQuery.toString();
    }
    
    @Override
    protected final String createSqlRecordCountQuery() throws Exception {
        final boolean isSummary = this.reportDefinition.isIsDetailsOrSummary();
        final StringBuilder sqlQuery = new StringBuilder("SELECT ");
        
        // Fields
        sqlQuery.append(" COUNT(*) ");
        
        // Tables & Where
        super.getTables(sqlQuery, false, isSummary);
        super.getWhereStandardTransaction(sqlQuery, false, isSummary);
        
        return sqlQuery.toString();
    }
    
    private String generateCashReportGroupSummaryQuery() throws Exception {
        final StringBuilder sqlQuery = new StringBuilder("(SELECT ");
        
        // Fields
        super.getFieldsGroupField(sqlQuery, true, false);
        super.getFieldsCash(sqlQuery, true);
        super.getSummaryFieldTotalTransaction(sqlQuery, true);
        
        // Tables & Where
        super.getTables(sqlQuery, true, false);
        super.getWhereStandardTransaction(sqlQuery, true, false);
        sqlQuery.append(" GROUP BY GroupName)");
        
        return sqlQuery.toString();
    }
}
