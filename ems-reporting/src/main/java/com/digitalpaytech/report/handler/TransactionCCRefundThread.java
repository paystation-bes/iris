package com.digitalpaytech.report.handler;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.util.ReportingConstants;

public class TransactionCCRefundThread extends TransactionBaseThread {
    private static final Logger LOGGER = Logger.getLogger(TransactionCCRefundThread.class);
    
    public TransactionCCRefundThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue,
            final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext);
    }
    
    @Override
    protected final String generateSQLString() throws Exception {
        final boolean isSummary = this.reportDefinition.isIsDetailsOrSummary();
        
        final StringBuilder sqlQuery = new StringBuilder("SELECT ");
        
        // Fields
        if (!isSummary) {
            final int filterId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
            if (ReportingConstants.REPORT_FILTER_GROUP_BY_CARD_TYPE == filterId) {
                sqlQuery.append("proctrans.CardType AS GroupField ");
            } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_MERCHANT_ACCOUNT == filterId) {
                sqlQuery.append(" IF(m.TerminalName IS NULL OR m.TerminalName = '', m.Name, m.TerminalName) AS GroupField ");
            } else {
                super.getFieldsGroupField(sqlQuery, isSummary, isSummary);
            }
        } else {
            super.getFieldsGroupField(sqlQuery, isSummary, isSummary);
        }
        getFieldsCreditCardRefundsReport(sqlQuery, isSummary);
        super.getFieldsRateName(sqlQuery, isSummary);
        super.getFieldsLocationName(sqlQuery, this.reportDefinition.isIsCsvOrPdf());
        
        // Tables & Where
        super.getTablesCardProcessingAndRefunds(sqlQuery, true, isSummary);
        super.getWhereStandardProcessorTransaction(sqlQuery, true);
        
        // Credit Card Last 4 Digits
        if (ReportingConstants.REPORT_FILTER_CARD_NUMBER_ALL != super.getFilterTypeId(this.reportDefinition.getCardNumberFilterTypeId())) {
            sqlQuery.append(" AND proctrans.Last4DigitsOfCardNumber=" + this.reportDefinition.getCardNumberSpecificFilter());
        }
        
        if (isSummary && ReportingConstants.REPORT_FILTER_GROUP_BY_NONE != super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId())) {
            sqlQuery.append(" UNION ").append(generateCreditCardRefundReportGroupSummaryQuery());
        }
        
        // Order By
        super.getOrderByStandard(sqlQuery, isSummary);
        return sqlQuery.toString();
    }
    
    @Override
    protected final String createSqlRecordCountQuery() throws Exception {
        final boolean isSummary = this.reportDefinition.isIsDetailsOrSummary();
        final StringBuilder sqlQuery = new StringBuilder("SELECT ");
        
        // Fields
        sqlQuery.append(" COUNT(*) ");
        
        // Tables & Where
        super.getTablesCardProcessingAndRefunds(sqlQuery, false, isSummary);
        super.getWhereStandardProcessorTransaction(sqlQuery, false);
        
        // Credit Card Last 4 Digits
        if (ReportingConstants.REPORT_FILTER_CARD_NUMBER_ALL != super.getFilterTypeId(this.reportDefinition.getCardNumberFilterTypeId())) {
            sqlQuery.append(" AND proctrans.Last4DigitsOfCardNumber=" + this.reportDefinition.getCardNumberSpecificFilter());
        }
        
        return sqlQuery.toString();
    }
    
    private String generateCreditCardRefundReportGroupSummaryQuery() throws Exception {
        final StringBuilder sqlQuery = new StringBuilder("(SELECT ");
        
        // Fields
        final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_CARD_TYPE == filterTypeId
            || ReportingConstants.REPORT_FILTER_GROUP_BY_MERCHANT_ACCOUNT == filterTypeId) {
            sqlQuery.append("0 AS IsOverallSummary");
            sqlQuery.append(", CONCAT('");
            sqlQuery.append(reportingUtil.findFilterString(filterTypeId));
            if (ReportingConstants.REPORT_FILTER_GROUP_BY_CARD_TYPE == filterTypeId) {
                sqlQuery.append(":  ', proctrans.CardType) AS GroupName");
                sqlQuery.append(", proctrans.CardType AS GroupOrderValue");
            } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_MERCHANT_ACCOUNT == filterTypeId) {
                sqlQuery.append(":  ', (SELECT IF(TerminalName IS NULL OR TerminalName = '', Name, TerminalName) AS Name FROM MerchantAccount ");
                sqlQuery.append(" WHERE Id = proctrans.MerchantAccountId)) AS GroupName");
                sqlQuery.append(", (SELECT IF(TerminalName IS NULL OR TerminalName = '', Name, TerminalName) AS Name FROM MerchantAccount ");
                sqlQuery.append(" WHERE Id = proctrans.MerchantAccountId) AS GroupOrderValue");
            }
        } else {
            super.getFieldsGroupField(sqlQuery, true, false);
        }
        
        getFieldsCreditCardRefundsReport(sqlQuery, true);
        
        // Tables & Where
        super.getTablesCardProcessingAndRefunds(sqlQuery, true, false);
        super.getWhereStandardProcessorTransaction(sqlQuery, true);
        
        final int cardNumFilterTypeId = super.getFilterTypeId(this.reportDefinition.getCardNumberFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_CARD_NUMBER_ALL != cardNumFilterTypeId) {
            sqlQuery.append(" AND proctrans.Last4DigitsOfCardNumber=" + this.reportDefinition.getCardNumberSpecificFilter());
        }
        
        sqlQuery.append(" GROUP BY GroupName)");
        
        return sqlQuery.toString();
    }
    
    private void getFieldsCreditCardRefundsReport(final StringBuilder query, final boolean isSummary) {
        if (isSummary) {
            if ("SELECT".equalsIgnoreCase(query.toString().trim())) {
                query.append("SUM(IF(pur.PaymentTypeId IN (2,5,11,12,13,14,15,16,17), pur.CardPaidAmount, 0)) AS OrigAmount");
            } else {
                query.append(", SUM(IF(pur.PaymentTypeId IN (2,5,11,12,13,14,15,16,17), pur.CardPaidAmount, 0)) AS OrigAmount");
            }
            query.append(", SUM(proctrans.Amount) AS RefundAmount");
            query.append(", COUNT(*) AS RefundN");
        } else {
            final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
            if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == filterTypeId && this.reportDefinition.isIsCsvOrPdf()) {
                query.append(", pos.Id as PaystationId, r.Id as PaystationRouteId");
            } else {
                query.append(", 0 as PaystationId, 0 as PaystationRouteId");
            }
            // Orig Trans Details
            query.append(", l.Name AS LocationName, '");
            query.append(this.timeZone);
            query.append("' AS TimeZone");
            /*
             * // dropped if (EMSConstants.MACHINE_NUMBER.equals(reportsForm.
             * getSelectedLocationType())) {
             * query.append(", s.MachineNumber AS Name"); } else {
             * query.append(", p.Name"); }
             */
            query.append(", pos.Name AS PosName");
            query.append(", pt.Name AS PaymentTypeName, tt.Name AS TransactionTypeName ");
            query.append(", pos.Name AS MachineNumber, per.SpaceNumber, lp.Number AS PlateNumber, proctrans.TicketNumber ");
            query.append(", proctrans.CardType, proctrans.Last4DigitsOfCardNumber AS CardNumber ");
            query.append(", proctrans.PurchasedDate, proctrans.AuthorizationNumber AS AuthNumber ");
            query.append(", IF(pur.PaymentTypeId IN (2,5,11,12,13,14,15,16,17), pur.CardPaidAmount, 0) AS OrigAmount ");
            query.append(", proctrans.ProcessingDate AS RefundDate, proctrans.Amount AS RefundAmount ");
            query.append(", pur.TransactionTypeId ");
        }
    }
    
}
