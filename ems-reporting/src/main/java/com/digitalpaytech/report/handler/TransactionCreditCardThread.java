package com.digitalpaytech.report.handler;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.util.ReportingConstants;

public class TransactionCreditCardThread extends TransactionBaseThread {
    private static final Logger LOGGER = Logger.getLogger(TransactionCreditCardThread.class);
    
    public TransactionCreditCardThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue,
            final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext);
    }
    
    @Override
    protected final String generateSQLString() throws Exception {
        final boolean isSummary = this.reportDefinition.isIsDetailsOrSummary();
        
        final StringBuilder sqlQuery = new StringBuilder("SELECT ");
        
        // Fields
        if (!isSummary) {
            final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
            if (ReportingConstants.REPORT_FILTER_GROUP_BY_CARD_TYPE == filterTypeId) {
                sqlQuery.append("IF(ISNULL(proctrans.CardType), 'Credit Card', proctrans.CardType) AS GroupField ");
            } else {
                super.getFieldsGroupField(sqlQuery, isSummary, isSummary);
            }
            super.getFieldsStandard(sqlQuery);
        } else {
            super.getFieldsGroupField(sqlQuery, isSummary, isSummary);
        }
        super.getFieldsCreditCardTransactions(sqlQuery, isSummary);
        super.getFieldsRateName(sqlQuery, isSummary);
        super.getFieldsLocationName(sqlQuery, this.reportDefinition.isIsCsvOrPdf());
        super.getSummaryFieldTotalTransaction(sqlQuery, isSummary);
        
        // Tables & Where
        super.getTables(sqlQuery, true, isSummary);
        super.getWhereStandardTransaction(sqlQuery, true, isSummary);
        
        // Credit Card Last 4 Digits
        final int cardFilterTypeId = super.getFilterTypeId(this.reportDefinition.getCardNumberFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_CARD_NUMBER_ALL != cardFilterTypeId) {
            sqlQuery.append(" AND proctrans.Last4DigitsOfCardNumber=" + this.reportDefinition.getCardNumberSpecificFilter());
        }
        
        sqlQuery.append(" AND pur.TransactionTypeId NOT IN (" + ReportingConstants.TRANSACTION_TYPE_REFUND_3RD_PARTY + ") ");
        
        final int groupFitlerId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (isSummary && ReportingConstants.REPORT_FILTER_GROUP_BY_NONE != groupFitlerId) {
            sqlQuery.append(" UNION ").append(generateCreditCardReportGroupSummaryQuery());
        }
        
        // Order By
        super.getOrderByStandard(sqlQuery, isSummary);
        return sqlQuery.toString();
    }
    
    @Override
    protected final String createSqlRecordCountQuery() throws Exception {
        final boolean isSummary = this.reportDefinition.isIsDetailsOrSummary();
        final StringBuilder sqlQuery = new StringBuilder("SELECT ");
        
        // Fields
        sqlQuery.append(" COUNT(*) ");
        
        // Tables & Where
        super.getTables(sqlQuery, false, isSummary);
        super.getWhereStandardTransaction(sqlQuery, false, isSummary);
        
        // Credit Card Last 4 Digits
        final int cardNumFilterId = super.getFilterTypeId(this.reportDefinition.getCardNumberFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_CARD_NUMBER_ALL != cardNumFilterId) {
            sqlQuery.append(" AND proctrans.Last4DigitsOfCardNumber=" + this.reportDefinition.getCardNumberSpecificFilter());
        }
        
        sqlQuery.append(" AND pur.TransactionTypeId NOT IN (" + ReportingConstants.TRANSACTION_TYPE_REFUND_3RD_PARTY + ") ");
        
        return sqlQuery.toString();
    }
    
    private String generateCreditCardReportGroupSummaryQuery() throws Exception {
        final StringBuilder sqlQuery = new StringBuilder("(SELECT ");
        
        // Fields
        final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_CARD_TYPE == filterTypeId) {
            sqlQuery.append("0 AS IsOverallSummary");
            sqlQuery.append(", CONCAT('");
            sqlQuery.append(reportingUtil.findFilterString(filterTypeId));
            sqlQuery.append(":  ', IF(ISNULL(proctrans.CardType), 'Credit Card', proctrans.CardType)) AS GroupName");
            sqlQuery.append(", IF(ISNULL(proctrans.CardType), 'Credit Card', proctrans.CardType) AS GroupOrderValue");
        } else {
            super.getFieldsGroupField(sqlQuery, true, false);
        }
        
        super.getFieldsCreditCardTransactions(sqlQuery, true);
        super.getSummaryFieldTotalTransaction(sqlQuery, true);
        
        // Tables & Where
        super.getTables(sqlQuery, true, false);
        super.getWhereStandardTransaction(sqlQuery, true, false);
        
        final int cardNumFilterId = super.getFilterTypeId(this.reportDefinition.getCardNumberFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_CARD_NUMBER_ALL != cardNumFilterId) {
            sqlQuery.append(" AND proctrans.Last4DigitsOfCardNumber=" + this.reportDefinition.getCardNumberSpecificFilter());
        }
        
        sqlQuery.append(" AND pur.TransactionTypeId NOT IN (" + ReportingConstants.TRANSACTION_TYPE_REFUND_3RD_PARTY + ") ");
        sqlQuery.append(" GROUP BY GroupName)");
        
        return sqlQuery.toString();
    }
}
