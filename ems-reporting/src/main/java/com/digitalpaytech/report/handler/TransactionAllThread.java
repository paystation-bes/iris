package com.digitalpaytech.report.handler;

import org.springframework.context.ApplicationContext;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.StandardConstants;

public class TransactionAllThread extends TransactionBaseThread {
    private static final String SELECT = "SELECT ";

    public TransactionAllThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue, final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext);
    }
    
    @Override
    protected final String generateSQLString() throws Exception {
        final boolean isSummary = this.reportDefinition.isIsDetailsOrSummary();
        
        final StringBuilder sqlQuery = new StringBuilder(SELECT);
        
        // Fields
        super.getFieldsGroupField(sqlQuery, isSummary, isSummary);
        
        if (!isSummary) {
            super.getFieldsStandard(sqlQuery);
        }
        
        super.getFieldsCash(sqlQuery, isSummary);
        super.getFieldsCreditCardTransactions(sqlQuery, isSummary);
        super.getFieldsValueCard(sqlQuery, isSummary);
        super.getFieldsSmartCard(sqlQuery, isSummary);
        super.getFieldsPatrollerCard(sqlQuery, isSummary);
        getFieldsAllTotals(sqlQuery, isSummary);
        super.getFieldsRateName(sqlQuery, isSummary);
        
        // Tables & Where
        super.getTables(sqlQuery, true, isSummary);
        super.getWhereStandardTransaction(sqlQuery, true, isSummary);
        
        final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (isSummary && ReportingConstants.REPORT_FILTER_GROUP_BY_NONE != filterTypeId) {
            sqlQuery.append(" UNION ").append(generateAllReportGroupSummaryQuery());
        }
        
        // Order By
        super.getOrderByStandard(sqlQuery, isSummary);
        return sqlQuery.toString();
    }
    
    @Override
    protected final String createSqlRecordCountQuery() throws Exception {
        final boolean isSummary = this.reportDefinition.isIsDetailsOrSummary();
        final StringBuilder sqlQuery = new StringBuilder(SELECT);
        
        // Fields
        sqlQuery.append(" COUNT(*) ");
        
        // Tables & Where
        super.getTables(sqlQuery, false, isSummary);
        super.getWhereStandardTransaction(sqlQuery, false, isSummary);
        
        return sqlQuery.toString();
    }
    
    private String generateAllReportGroupSummaryQuery() throws Exception {
        final StringBuilder sqlQuery = new StringBuilder("(SELECT ");
        
        // Fields
        super.getFieldsGroupField(sqlQuery, true, false);
        super.getFieldsCash(sqlQuery, true);
        super.getFieldsCreditCardTransactions(sqlQuery, true);
        super.getFieldsValueCard(sqlQuery, true);
        super.getFieldsSmartCard(sqlQuery, true);
        super.getFieldsPatrollerCard(sqlQuery, true);
        getFieldsAllTotals(sqlQuery, true);
        
        // Tables & Where
        super.getTables(sqlQuery, true, false);
        super.getWhereStandardTransaction(sqlQuery, true, false);
        sqlQuery.append(" GROUP BY GroupName)");
        
        return sqlQuery.toString();
    }
    
    private void getFieldsAllTotals(final StringBuilder query, final boolean isSummary) {
        if (isSummary) {
            if ("SELECT".equalsIgnoreCase(query.toString().trim())) {
                query.append("SUM(IF(pur.PaymentTypeId=3 AND pur.TransactionTypeId=6, 0, 1)) AS TransactionN");
            } else {
                query.append(", SUM(IF(pur.PaymentTypeId=3 AND pur.TransactionTypeId=6, 0, 1)) AS TransactionN");
            }
            query.append(", SUM(IF((IF(pur.CashPaidAmount > 0, pur.CashPaidAmount, 0) + IF(pur.PaymentTypeId IN (2, 5, 11, 12, 13, 14, 15, 16, 17), pur.CardPaidAmount, 0) + IF(pur.PaymentTypeId=7 OR pur.PaymentTypeId=9, pur.CardPaidAmount, 0) - IF(pur.TransactionTypeId=11 OR pur.TransactionTypeId=13 OR pur.TransactionTypeId=14, pur.CashPaidAmount, 0)) > 0, 1, 0)) AS CollectionsN")
           .append(", SUM(IF(pur.CashPaidAmount > 0, pur.CashPaidAmount, 0) + IF(pur.PaymentTypeId IN (2, 5, 11, 12, 13, 14, 15, 16, 17), pur.CardPaidAmount, 0) + IF(pur.PaymentTypeId=7 OR pur.PaymentTypeId=9, pur.CardPaidAmount, 0) - IF(pur.TransactionTypeId=11 OR pur.TransactionTypeId=13 OR pur.TransactionTypeId=14, pur.CashPaidAmount, 0)) AS Collections")
           .append(", SUM(IF(IF(pur.TransactionTypeId IN(8, 11, 13, 14), 0, pur.CashPaidAmount) - IF(pur.PaymentTypeId=1 AND pur.TransactionTypeId=4, pur.ChargedAmount, 0) - IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip, (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChargedAmount), pur.ChangeDispensedAmount) + IF(pur.PaymentTypeId IN(2, 5, 7, 9, 11, 12, 13, 14, 15, 16, 17), pur.CardPaidAmount, 0) - IF(pur.PaymentTypeId IN (2, 11, 13) AND pur.TransactionTypeId=4, pur.ChargedAmount, 0) + IF(IF(pur.PaymentTypeId=4 OR pur.PaymentTypeId=6, pur.CardPaidAmount, 0) > 0, IF(pur.PaymentTypeId=4 OR pur.PaymentTypeId=6, pur.CardPaidAmount, 0), 0) + IF(pur.PaymentTypeId=3 AND pur.TransactionTypeId<>6, pur.CardPaidAmount, 0) + IF(pur.transactionTypeId = 6 AND pur.OriginalAmount = 0, 1, 0) > 0 , 1, 0)) AS RevenueN")
           .append(", SUM(IF(pur.TransactionTypeId IN(8, 11, 13, 14), 0, pur.CashPaidAmount) - IF(pur.PaymentTypeId=1 AND pur.TransactionTypeId=4, pur.ChargedAmount, 0) - IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip, (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChargedAmount), pur.ChangeDispensedAmount) + IF(pur.PaymentTypeId IN(2, 5, 7, 9, 11, 12, 13, 14, 15, 16, 17), pur.CardPaidAmount, 0) - IF(pur.PaymentTypeId IN (2, 11, 13) AND pur.TransactionTypeId=4, pur.ChargedAmount, 0) + IF(IF(pur.PaymentTypeId=4 OR pur.PaymentTypeId=6, pur.CardPaidAmount, 0) > 0, IF(pur.PaymentTypeId=4 OR pur.PaymentTypeId=6, pur.CardPaidAmount, 0), 0) + IF(pur.PaymentTypeId=3 AND pur.TransactionTypeId<>6, pur.CardPaidAmount, 0)) AS Revenue");
        }
    }
    
}
