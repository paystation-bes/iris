package com.digitalpaytech.report.handler;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.util.ReportingConstants;

public class RateSummaryThread extends ReportBaseThread {
    private static final Logger LOGGER = Logger.getLogger(RateSummaryThread.class);
    
    public RateSummaryThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue, final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext);
        this.jasperFileName = ReportingConstants.REPORT_JASPER_FILE_RATE_SUMMARY;
    }
    
    @Override
    protected final String generateSQLString() throws Exception {
        final StringBuilder sqlQuery = new StringBuilder("SELECT ");
        
        getFieldString(sqlQuery);
        getTableString(sqlQuery);
        getWhereString(sqlQuery, false);
        getOrderByString(sqlQuery);
        return sqlQuery.toString();
    }
    
    @Override
    protected final String createSqlRecordCountQuery() throws Exception {
        final StringBuilder countQuery = new StringBuilder("SELECT COUNT(*) ");
        countQuery.append(" FROM Purchase pur");
        getWhereString(countQuery, true);
        
        final String query = countQuery.toString();
        LOGGER.info("COUNT QUERY : " + query);
        return query;
    }
    
    @Override
    protected final int getMaxRecordCount(final Errors errors) throws Exception {
        return ReportingConstants.REPORTS_DEFAULT_MAX_RECORDS;
    }
    
    @Override
    protected final String createFileName() {
        final StringBuilder sb = new StringBuilder();
        sb.append(reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_RATE_SUMMARY));
        return super.createFileName(sb);
    }
    
    private void getFieldString(final StringBuilder query) {
        
        query.append("pur.UnifiedRateId AS RateId");
        query.append(", urt.Name AS RateName, pur.RateAmount AS Rate");
        final int offset = getTimeZoneOffset();
        for (int i = 0; i < 24; i++) {
            query.append(", ");
            query.append(getFieldExpressionForHour(i, offset));
        }
        query.append(", COUNT(1) AS CountTotal, SUM(pur.RateRevenueAmount) AS AmountTotal");
    }
    
    private void getTableString(final StringBuilder query) {
        query.append(" FROM Purchase pur");
        query.append(" INNER JOIN UnifiedRate urt ON pur.UnifiedRateId = urt.Id");
        
        // TODO dropped???
        /*
         * if (EMSConstants.MACHINE_NUMBER.equals(reportsForm.getSelectedLocationType()) && !EMSConstants.ALL.equals(reportsForm.getSelectedPs1MachineNumber()))
         * {
         * query.append(" INNER JOIN ServiceState s ON t.PaystationId = s.PaystationId");
         * }
         */
        
        // Filter for hidden POS
        getFromHiddenPointOfSales(query, "pur");
        
    }
    
    private void getWhereString(final StringBuilder query, final boolean isSummary) {
        query.append(" WHERE ");
        super.getWherePointOfSale(query, "pur", true);
        
        // Purchased Date & Time
        super.getWherePurchaseGMT(query, "pur");
        
        // Transaction Type
        getWhereTransactionType(query);
        
        // Filter for hidden POS
        super.getWhereHiddenPointOfSales(query);
        
    }
    
    private void getOrderByString(final StringBuilder query) {
        query.append(" GROUP BY pur.UnifiedRateId, pur.RateAmount");
    }
    
    protected final Map<String, Object> createReportParameterList(final String query, final int customerId) {
        final Map<String, Object> parameters = super.createReportParameterList(query, customerId);
        
        parameters.put(
            "ReportTitle",
            super.getTitle(reportingUtil.getPropertyEN("label.siteName") + " "
                           + reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_RATE_SUMMARY)));
        
        final int priDateFilterId = super.getFilterTypeId(this.reportDefinition.getPrimaryDateFilterTypeId());
        if (ReportingConstants.REPORT_SELECTION_ALL == priDateFilterId) {
            parameters.put("DateString", this.reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL));
        } else {
            parameters.put("DateString",
                createReportParameterDateString(this.reportQueue.getPrimaryDateRangeBeginGmt(), this.reportQueue.getPrimaryDateRangeEndGmt()));
            
        }
        
        // Location Parameters
        parameters.put("LotType", "Setting");
        
        parameters.put("LocationType", reportingUtil.findFilterString(this.reportDefinition.getLocationFilterTypeId()));
        parameters.put("MachineNameOrNumber", convertArrayToDelimiterString(this.locationValueNameList, ','));
        
        parameters.put("Grouping", reportingUtil.findFilterString(this.reportDefinition.getGroupByFilterTypeId()));
        parameters.put("TransactionType", reportingUtil.findFilterString(this.reportDefinition.getOtherParametersTypeFilterTypeId()));
        
        parameters.put("IsCsv", !this.reportDefinition.isIsCsvOrPdf());
        parameters.put("IsPdf", this.reportDefinition.isIsCsvOrPdf());
        
        parameters.put("UniqueRateNameMap", new HashMap<String, Map<String, Integer>>());
        
        parameters.put("ZeroFloat", new Float(0.00));
        parameters.put("ZeroInteger", new Integer(0));
        
        return parameters;
    }
    
    private void getWhereTransactionType(final StringBuilder query) {
        final int transType = super.getFilterTypeId(this.reportDefinition.getOtherParametersTypeFilterTypeId());
        
        switch (transType) {
            case ReportingConstants.REPORT_FILTER_TRANSACTION_ALL:
                query.append(" AND pur.TransactionTypeId NOT IN (").append(ReportingConstants.TRANSACTION_TYPE_EXTEND_BY_PHONE_ADDTIME_VALUE)
                        .append(") ");
                break;
            case ReportingConstants.REPORT_FILTER_TRANSACTION_REGULAR:
                query.append(" AND pur.TransactionTypeId=").append(ReportingConstants.TRANSACTION_TYPE_REGULAR);
                break;
            case ReportingConstants.REPORT_FILTER_TRANSACTION_ADD_TIME:
                query.append(" AND pur.TransactionTypeId=").append(ReportingConstants.TRANSACTION_TYPE_ADDTIME);
                break;
            case ReportingConstants.REPORT_FILTER_TRANSACTION_CANCELLED:
                query.append(" AND pur.TransactionTypeId=").append(ReportingConstants.TRANSACTION_TYPE_CANCELLED);
                break;
            default:
                break;
        }
    }
    
    private String getFieldExpressionForHour(final int hourNumber, final int offset) {
        int timezoneAdjustedHour = hourNumber;
        
        timezoneAdjustedHour = timezoneAdjustedHour - offset;
        if (timezoneAdjustedHour < 0) {
            timezoneAdjustedHour = timezoneAdjustedHour + 24;
        } else if (timezoneAdjustedHour > 23) {
            timezoneAdjustedHour = timezoneAdjustedHour - 24;
        }
        
        final StringBuilder expression = new StringBuilder();
        expression.append("COUNT(IF(HOUR(pur.PurchaseGMT) = ");
        expression.append(timezoneAdjustedHour);
        expression.append(" ,1 , NULL)) AS Count");
        expression.append(hourNumber);
        
        expression.append(", ");
        
        expression.append("SUM(IF(HOUR(pur.PurchaseGMT) = ");
        expression.append(timezoneAdjustedHour);
        expression.append(" ,pur.RateRevenueAmount , 0)) AS Amount");
        expression.append(hourNumber);
        
        return expression.toString();
    }
    
}
