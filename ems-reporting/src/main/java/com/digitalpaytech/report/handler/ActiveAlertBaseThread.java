package com.digitalpaytech.report.handler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.service.AlertTypeService;
import com.digitalpaytech.service.EventDeviceTypeService;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.WebCoreConstants;

public abstract class ActiveAlertBaseThread extends ReportBaseThread {
    
    protected EventDeviceTypeService eventDeviceTypeService;
    protected AlertTypeService alertTypeService;
    
    protected String reportKey;
    
    public ActiveAlertBaseThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue, final ApplicationContext applicationContext,
            final String reportKey) {
        super(reportDefinition, reportQueue, applicationContext);
        
        this.eventDeviceTypeService = applicationContext.getBean(EventDeviceTypeService.class);
        this.alertTypeService = applicationContext.getBean(AlertTypeService.class);
        
        this.reportKey = reportKey;
    }
    
    protected abstract void appendSpecificConditions(StringBuilder query);
    
    @Override
    protected final String generateSQLString() throws Exception {
        final StringBuilder query = new StringBuilder();
        appendSelection(query);
        appendGroupSelection(query);
        appendTable(query);
        appendConditions(query);
        appendGroupAndOrder(query);
        
        return query.toString();
    }
    
    @Override
    protected final String createSqlRecordCountQuery() throws Exception {
        return null;
    }
    
    @Override
    protected final String createFileName() {
        return this.reportingUtil.getPropertyEN(this.reportKey) + (this.reportDefinition.isIsCsvOrPdf() ? ".pdf" : ".csv");
    }
    
    @Override
    protected final int getMaxRecordCount(final Errors errors) throws Exception {
        return ReportingConstants.REPORTS_DEFAULT_MAX_RECORDS;
    }
    
    @Override
    protected final Map<String, Object> createReportParameterList(final String query, final int customerId) {
        final Map<String, Object> parameters = super.createReportParameterList(query, customerId);
        
        parameters.put("ReportingUtil", this.reportingUtil);
        
        parameters.put("ReportTitle",
                       super.getTitle(reportingUtil.getPropertyEN("label.siteName") + " " + this.reportingUtil.getPropertyEN(this.reportKey)));
        
        parameters.put("Location", convertArrayToDelimiterString(this.locationValueNameList, ','));
        parameters.put("LocationType", this.reportingUtil.findFilterString(this.reportDefinition.getLocationFilterTypeId()));
        
        // If 'All' device type is selected, database table ReportDefinition- EventDeviceTypeId is null.
        if (this.reportDefinition.getEventDeviceTypeId() == null) {
            parameters.put("DeviceType", this.reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL));
        } else {
            parameters.put("DeviceType", this.eventDeviceTypeService.getText(this.reportDefinition.getEventDeviceTypeId()));
        }
        
        if ((this.reportDefinition.getEventSeverityTypeId() == null)
            || (this.reportDefinition.getEventSeverityTypeId() == ReportingConstants.REPORT_SELECTION_ALL)) {
            parameters.put("SeverityType", this.reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL));
        } else {
            final EventSeverityType sev = this.alertTypeService.findEventSeverityTypeById(this.reportDefinition.getEventSeverityTypeId());
            parameters.put("SeverityType", sev.getName());
        }
        
        final List<Integer> thresholdTypeIds = resolveThresholdTypes(this.reportDefinition);
        if (thresholdTypeIds.size() <= 0) {
            parameters.put("ThresholdType", this.reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL));
        } else {
            parameters.put("ThresholdType", StringUtils.join(this.alertTypeService.findAlertThresholdTypeText(thresholdTypeIds), ','));
        }
        
        final Integer groupByFilterTypeId = this.reportDefinition.getGroupByFilterTypeId();
        final boolean isGroupByPayStation = (groupByFilterTypeId != null)
                                            && (groupByFilterTypeId == ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION);
        
        parameters.put("Grouping", this.reportingUtil.findFilterString(groupByFilterTypeId));
        parameters.put("IsGroupByPayStation", isGroupByPayStation);
        
        parameters.put("IsCsv", !this.reportDefinition.isIsCsvOrPdf());
        parameters.put("IsPdf", this.reportDefinition.isIsCsvOrPdf());
        
        return parameters;
    }
    
    protected final void appendSelection(final StringBuilder query) {
        query.append(" SELECT pos.Id AS POSId ");
        query.append("  , pos.Name AS POSName ");
        query.append("  , loc.Name AS LocationName ");
        query.append("  , parentLoc.Name AS ParentLocationName ");
        query.append("  , IFNULL(att.Id, 12) AS AlertThresholdTypeId ");
        query.append("  , CASE WHEN cat.AlertThresholdTypeId ");
        query.append("         IS NULL THEN et.Description ");
        query.append("         ELSE cat.Name ");
        query.append("    END AS AlertName ");
        query.append("  , CASE WHEN pec.EventActionTypeId ");
        query.append("         IS NULL THEN '' ");
        query.append("         ELSE eat.Name ");
        query.append("    END AS ActionName ");
        query.append("  , MAX(pec.AlertGMT) AS AlertGMT ");
        query.append("  , '");
        query.append(this.timeZone);
        query.append("'  AS TimeZone");
    }
    
    protected final void appendGroupSelection(final StringBuilder query) {
        final int groupByType = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        switch (groupByType) {
            case ReportingConstants.REPORT_FILTER_GROUP_BY_LOCATION:
                query.append("  , loc.Name AS GroupingField ");
                query.append("  , loc.Id AS GroupingId ");
                query.append("  , parentLoc.Name AS ParentGroupingField ");
                query.append("  , parentLoc.Id AS ParentGroupingId ");
                break;
            case ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE:
                query.append("  , rt.Name AS GroupingField ");
                query.append("  , rt.Id AS GroupingId ");
                query.append("  , NULL AS ParentGroupingField ");
                query.append("  , NULL AS ParentGroupingId ");
                break;
            case ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION:
                query.append("  , pos.Name AS GroupingField ");
                query.append("  , pos.Id AS GroupingId ");
                query.append("  , NULL AS ParentGroupingField ");
                query.append("  , NULL AS ParentGroupingId ");
                break;
            default:
                query.append("  , NULL AS GroupingField ");
                query.append("  , NULL AS GroupingId ");
                query.append("  , NULL AS ParentGroupingField ");
                query.append("  , NULL AS ParentGroupingId ");
        }
    }
    
    protected final void appendTable(final StringBuilder query) {
        query.append(" FROM POSEventCurrent pec ");
        query.append("  JOIN EventType et ON (et.id = pec.EventTypeId) ");
        query.append("  LEFT JOIN CustomerAlertType cat ON(pec.CustomerAlertTypeId = cat.Id) ");
        query.append("  LEFT JOIN AlertThresholdType att ON(cat.AlertThresholdTypeId = att.Id) ");
        query.append("  LEFT JOIN EventActionType eat ON (pec.EventActionTypeId = eat.Id) ");
        query.append("  JOIN PointOfSale pos ON(pec.PointOfSaleId = pos.Id) ");
        query.append("  JOIN POSStatus posStat ON(pos.Id = posStat.PointOfSaleId) ");
        query.append("  JOIN Location loc ON(pos.LocationId = loc.Id) ");
        query.append("  LEFT JOIN Location parentLoc ON(loc.ParentLocationId = parentLoc.Id) ");
        
        final Integer locTypeId = this.reportDefinition.getLocationFilterTypeId();
        if (locTypeId == ReportingConstants.REPORT_FILTER_LOCATION_ROUTE_TREE) {
            query.append("  LEFT JOIN RoutePOS rPOS ON(pos.Id = rPOS.PointOfSaleId) ");
            query.append("  LEFT JOIN Route rt ON(rPOS.RouteId = rt.Id) ");
        }
    }
    
    protected final void appendConditions(final StringBuilder query) {
        query.append(" WHERE (pec.IsActive = 1) ");
        query.append("  AND ");
        
        // Filter for location/route/pay station
        super.getWherePointOfSale(query, "pos", false);
        // Filter for hidden POS
        super.getWhereHiddenPointOfSales(query, "posStat");
        
        appendSpecificConditions(query);
    }
    
    protected final void appendGroupAndOrder(final StringBuilder query) {
        query.append(" GROUP BY ParentGroupingId, GroupingId, POSId, ParentGroupingField, GroupingField, ParentLocationName, LocationName, POSName, AlertThresholdTypeId, AlertName ");
        query.append(" ORDER BY ParentGroupingField, GroupingField, POSName, AlertName, AlertGMT ");
    }
    
    protected final List<Integer> resolveThresholdTypes(final ReportDefinition reportDefinition) {
        final ArrayList<Integer> alertThresholdTypeIds = new ArrayList<Integer>();
        if (reportDefinition.getReportCollectionFilterType() != null) {
            final short collectionFilter = reportDefinition.getReportCollectionFilterType().getId();
            if ((collectionFilter & ReportingConstants.COLLECTION_FILTER_BITMASK_RUNNING_TOTAL_DOLLAR) == ReportingConstants.COLLECTION_FILTER_BITMASK_RUNNING_TOTAL_DOLLAR) {
                alertThresholdTypeIds.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_RUNNING_TOTAL_DOLLAR);
            }
            if ((collectionFilter & ReportingConstants.COLLECTION_FILTER_BITMASK_COIN_CANISTER_COUNT) == ReportingConstants.COLLECTION_FILTER_BITMASK_COIN_CANISTER_COUNT) {
                alertThresholdTypeIds.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_COUNT);
            }
            if ((collectionFilter & ReportingConstants.COLLECTION_FILTER_BITMASK_COIN_CANISTER_DOLLARS) == ReportingConstants.COLLECTION_FILTER_BITMASK_COIN_CANISTER_DOLLARS) {
                alertThresholdTypeIds.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_DOLLARS);
            }
            if ((collectionFilter & ReportingConstants.COLLECTION_FILTER_BITMASK_BILL_STACKER_COUNT) == ReportingConstants.COLLECTION_FILTER_BITMASK_BILL_STACKER_COUNT) {
                alertThresholdTypeIds.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_COUNT);
            }
            if ((collectionFilter & ReportingConstants.COLLECTION_FILTER_BITMASK_BILL_STACKER_DOLLARS) == ReportingConstants.COLLECTION_FILTER_BITMASK_BILL_STACKER_DOLLARS) {
                alertThresholdTypeIds.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_DOLLARS);
            }
            if ((collectionFilter & ReportingConstants.COLLECTION_FILTER_BITMASK_UNSETTLED_CREDIT_CARD_COUNT) == ReportingConstants.COLLECTION_FILTER_BITMASK_UNSETTLED_CREDIT_CARD_COUNT) {
                alertThresholdTypeIds.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_COUNT);
            }
            if ((collectionFilter & ReportingConstants.COLLECTION_FILTER_BITMASK_UNSETTLED_CREDIT_CARD_DOLLARS) == ReportingConstants.COLLECTION_FILTER_BITMASK_UNSETTLED_CREDIT_CARD_DOLLARS) {
                alertThresholdTypeIds.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_DOLLARS);
            }
            if ((collectionFilter & ReportingConstants.COLLECTION_FILTER_BITMASK_OVERDUE_COLLECTION) == ReportingConstants.COLLECTION_FILTER_BITMASK_OVERDUE_COLLECTION) {
                alertThresholdTypeIds.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_OVERDUE_COLLECTION);
            }
        }
        return alertThresholdTypeIds;
    }
}
