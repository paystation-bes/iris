package com.digitalpaytech.report.handler;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRScriptletException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.type.WhenNoDataTypeEnum;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRSwapFile;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.validation.Errors;
import org.springframework.web.context.support.XmlWebApplicationContext;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PaystationType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportHistory;
import com.digitalpaytech.domain.ReportLocationValue;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.domain.ReportStatusType;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.RoutePOS;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.report.scriptlets.ReportsScriptlet;
import com.digitalpaytech.report.support.JRDataSourceEventHandler;
import com.digitalpaytech.report.support.ReportJRDataSource;
import com.digitalpaytech.report.support.dto.ArchiveReportProperties;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.ReplenishTypeService;
import com.digitalpaytech.service.ReportDefinitionService;
import com.digitalpaytech.service.ReportHistoryService;
import com.digitalpaytech.service.ReportLocationValueService;
import com.digitalpaytech.service.ReportQueueService;
import com.digitalpaytech.service.ReportRepositoryService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.LabelConstants;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.ReportingUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.mchange.v2.c3p0.ComboPooledDataSource;

public abstract class ReportBaseThread extends Thread {
    
    protected static String tempReportsPath;
    protected static String jasperFilePath;
    
    private static final String PLUS_QUOTE_CLOSE_PARENTHESES_SPACE = "+') ";
    private static final String AND_PUR_COUPON_ID_IS_NOT_NULL = " AND pur.CouponId IS NOT NULL ";
    private static final String QUOTE_SPACE_AND_SPACE_QUOTE = "' AND '";
    private static final String SPACE_BETWEEN_SPACE = " BETWEEN '";
    private static final String SPACE_AND_SPACE = " AND ";
    private static final String SPACE_MILLISECONDS = " milliseconds";
    private static final String SPACE_DASH_SPACE = " - ";
    private static final String POINT_OF_SALE_ID_IN_OPEN_PARENTHESES = ".PointOfSaleId IN (";
    private static final String POS_ID_IN_OPEN_PARENTHESES = "pos.Id IN (";
    private static final String AND_SPACE = "AND ";
    private static final String CLOSE_PARENTHESES_SPACE = ") ";
    private static final String SPACE_AND_SPACE_OPEN_PERENTHESES = " AND (";
    
    private static final Logger LOGGER = Logger.getLogger(ReportBaseThread.class);
    private static final int DEFAULT = -1;
    private static final int STRING_BUFFER_SIZE = 500;
    private static final int MAX_VIRTUALIZER = 50;
    private static final int BUFFER_SIZE = 500;
    private static final int COLUMN_TOTAL_SIZE = 24;
    private static final int FETCH_SIZE = 10000;
    private static final int MAX_JRV_OBJ = 10;
    
    private static final String POS = "pos";
    private static final String UNABLE_TO_LOAD = "Unable to load report";
    private static final String ERROR_GEN_REP = "Error generating report";
    private static final String FIVE_ZEROS = "00000";
    private static final String SWAP_PATH = "swapPath";
    private static final String RECORD_COUNT = "Record Count: ";
    private static final String IS_GREAT_MAX_REC = " is > max allowed records: ";
    private static final String MILLISECONDS = " milliseconds.";
    private static final String JASPERTHREAD = "JasperThread, cannot close SQL connection, ";
    private static final String LESS_THAN_MAX_ALLOWED_RECS = " is <= max allowed records: ";
    private static final String SERIAL_NUMBER = "SerialNumber";
    private static final String COIN_TOTAL_AMT = "CoinTotalAmount";
    private static final String BILL_TOTAL_AMT = "BillTotalAmount";
    private static final String CARD_TOTAL_AMT = "CardTotalAmount";
    private static final String OVER_FILL_AMT = "OverfillAmount";
    private static final String SMART_CARD_RECHARGE_AMT = "SmartCardRechargeAmount";
    private static final String SMART_CARD_AMT = "SmartCardAmount";
    private static final String ATTENDANT_TICKETS_AMT = "AttendantTicketsAmount";
    private static final String ATTENDANT_DEPOSIT_AMT = "AttendantDepositAmount";
    private static final String REFUND_ISSUED_AMT = "RefundIssuedAmount";
    private static final String CHANGE_ISSUED_AMT = "ChangeIssuedAmount";
    private static final String TUBE_1_TYPE = "Tube1Type";
    private static final String TUBE_2_TYPE = "Tube2Type";
    private static final String TUBE_3_TYPE = "Tube3Type";
    private static final String TUBE_4_TYPE = "Tube4Type";
    private static final String NAME = "Name";
    private static final String ACCEPTED_FLOAT_AMT = "AcceptedFloatAmount";
    private static final String PURCHASED_DATE = "PurchasedDate";
    private static final String SPACE_NUMBER = "SpaceNumber";
    private static final String EXPIRY_DATE = "ExpiryDate";
    private static final String NULL_STRING = "null";
    
    private static final String COLLECTION_USER_NAME = "CollectionUserName";
    private static final String JRSWAPVIRT_INIT = "JRSwapFileVirtualizer initiated.";
    private static final String JREXCEPT_GEN_REPT = "JREXception Error generating report";
    private static final String JRSWAP_CLEAN = "JRSwapFileVirtualizer has cleaned up the swap file.";
    private static final String FILL_REPT_PROC = "Fill report process took ";
    
    private static final String TUBE1_CHANGED_CNT = "Tube1ChangedCount";
    private static final String TUBE2_CHANGED_CNT = "Tube2ChangedCount";
    private static final String TUBE3_CHANGED_CNT = "Tube3ChangedCount";
    private static final String TUBE4_CHANGED_CNT = "Tube4ChangedCount";
    private static final String TUBE1_CURRENT_CNT = "Tube1CurrentCount";
    private static final String TUBE2_CURRENT_CNT = "Tube2CurrentCount";
    private static final String TUBE3_CURRENT_CNT = "Tube3CurrentCount";
    private static final String TUBE4_CURRENT_CNT = "Tube4CurrentCount";
    
    private static final String PS2_TRANSACTION_DELAY_REPORT_TITLE_ROW =
            "PS Name, Serial Number, Location, Space Number, Transaction Number, Purchase Date,"
                                                                         + " Expiry Date, Purchase Created Date, Delta, Licence Plate\n";
    
    private static final String PS2_AUDIT_REPORT_TITLE_ROW =
            "Report Number, Start Date, End Date, " + "PS Name, PS Serial Number, Transaction#/End Ticket Number, "
                                                             + "Ticket Count, # Patroller Tickets, Tube 1 Type, Tube 1 Amount, Tube 2 Type, "
                                                             + "Tube 2 Amount, Tube 3 Type, Tube 3 Amount, "
                                                             + "Tube 4 Type, Tube 4 Amount, Tube Total Amount, Changer Replenished, "
                                                             + "Changer OverFill, Changer Accepted Float, "
                                                             + "Changer Dispensed, Changer Test Dispensed, Hopper 1 Type, "
                                                             + "Hopper 1 Amount, Hopper 2 Type, Hopper 2 Amount, "
                                                             + "Hopper Total Amount, Hopper 1 Replenished, Hopper 2 Replenished, "
                                                             + "Hopper 1 Dispensed, Hopper 2 Dispensed, "
                                                             + "Hopper 1 Test Dispensed, Hopper 2 Test Dispensed, Coin Bag Amount, "
                                                             + "Stacker Ones, Stacker Twos, Stacker Fives, Stacker Tens, "
                                                             + "Stacker Twenties, Stacker Fifties, Stacker Total, Visa, "
                                                             + "MC, Amex, Discover, Diner's, Other, Swipe Card Total, "
                                                             + "SmartCard Charge, SmartCard Recharges, "
                                                             //TODO To be added when Citations are implemented
                                                             // + "# Citations Paid, Citation Amount, "
                                                             + "# Attendant Tickets, Attendant Tickets Amount, "
                                                             + "Attendant Deposit Amount, OverPayment, Collections, "
                                                             + "Change Issued, Refund Tickets Issued, " + "Revenue, Collection Done By\n";
                                                             
    private static final String PS2_REPLENISH_REPORT_TITLE_ROW =
            "Date, PS Name, Report Number, Type, " + "Tube 1 Type, Tube 1 Added Count, Tube 1 Added Amount, "
                                                                 + "Tube 1 Current Count, Tube 1 Current Amount, "
                                                                 + "Tube 2 Type, Tube 2 Added Count, Tube 2 Added Amount, "
                                                                 + "Tube 2 Current Count, Tube 2 Current Amount, "
                                                                 + "Tube 3 Type, Tube 3 Added Count, Tube 3 Added Amount, "
                                                                 + "Tube 3 Current Count, Tube 3 Current Amount, "
                                                                 + "Tube 4 Type, Tube 4 Added Count, Tube 4 Added Amount, "
                                                                 + "Tube 4 Current Count, Tube 4 Current Amount, "
                                                                 + "Added Tube Total, Current Tube Total, "
                                                                 + "Coin Bag Added: $0.05, Coin Bag Added: $0.10, "
                                                                 + "Coin Bag Added $0.25, Coin Bag Added $1.00, "
                                                                 + "Coin Bag Added $2.00, Coin Bag Total, " + "Hopper 1 Type, Hopper 1 Added Count, "
                                                                 + "Hopper 1 Added Amount, Hopper 1 Current Count, Hopper 1 Current Amount, "
                                                                 + "Hopper 2 Type, Hopper 2 Added Count, "
                                                                 + "Hopper 2 Added Amount, Hopper 2 Current Count, Hopper 2 Current Amount, "
                                                                 + "Added Hopper Total, Current Hopper Total\n";
    
    protected ReportDefinition reportDefinition;
    protected ReportQueue reportQueue;
    protected ReportHistory reportHistory;
    protected Customer customer;
    protected String[] locationValueNameList;
    protected String[] organizationNameList;
    protected Date sqlExecutionBeginGMT;
    protected Date sqlExecutionEndGMT;
    protected String fileName;
    protected String timeZone;
    protected String statusExplanation;
    protected String jasperFileName;
    protected ComboPooledDataSource archivedDataSource;
    protected ComboPooledDataSource currentDataSource;
    protected ReportingUtil reportingUtil;
    protected EntityService entityService;
    
    protected EmsPropertiesService emsPropertiesService;
    protected CustomerAdminService customerAdminService;
    protected LocationService locationService;
    protected PointOfSaleService pointOfSaleService;
    protected RouteService routeService;
    protected ReportLocationValueService reportLocationValueService;
    protected ReportDefinitionService reportDefinitionService;
    protected ReportQueueService reportQueueService;
    protected ReportHistoryService reportHistoryService;
    protected ReportRepositoryService reportRepositoryService;
    protected ReplenishTypeService replenishTypeService;
    protected MailerService mailerService;
    private ArchiveReportProperties archiveReportProperties;
    
    public ReportBaseThread() {
    }
    
    public ReportBaseThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue, final ApplicationContext applicationContext) {
        this.reportDefinition = reportDefinition;
        this.reportQueue = reportQueue;
        
        if (tempReportsPath == null) {
            tempReportsPath = ((XmlWebApplicationContext) applicationContext).getServletContext().getRealPath(ReportingConstants.REPORTS_TEMP_PATH);
            jasperFilePath = ((XmlWebApplicationContext) applicationContext).getServletContext().getRealPath(ReportingConstants.JASPER_BASE_DIR);
        }
        
        this.currentDataSource = (ComboPooledDataSource) applicationContext.getBean("reportQueryDataSource");
        this.archivedDataSource = (ComboPooledDataSource) applicationContext.getBean("archiveReportQueryDataSource");
        
        this.reportingUtil = (ReportingUtil) applicationContext.getBean("reportingUtil");
        
        this.entityService = (EntityService) applicationContext.getBean("entityService");
        
        this.emsPropertiesService = (EmsPropertiesService) applicationContext.getBean("emsPropertiesService");
        this.customerAdminService = (CustomerAdminService) applicationContext.getBean("customerAdminService");
        this.locationService = (LocationService) applicationContext.getBean("locationService");
        this.pointOfSaleService = (PointOfSaleService) applicationContext.getBean("pointOfSaleService");
        this.routeService = (RouteService) applicationContext.getBean("routeService");
        this.reportLocationValueService = (ReportLocationValueService) applicationContext.getBean("reportLocationValueService");
        this.reportDefinitionService = (ReportDefinitionService) applicationContext.getBean("reportDefinitionService");
        this.reportQueueService = (ReportQueueService) applicationContext.getBean("reportQueueService");
        this.reportHistoryService = (ReportHistoryService) applicationContext.getBean("reportHistoryService");
        this.reportRepositoryService = (ReportRepositoryService) applicationContext.getBean("reportRepositoryService");
        this.replenishTypeService = (ReplenishTypeService) applicationContext.getBean("replenishTypeService");
        this.mailerService = (MailerService) applicationContext.getBean("mailerService");
        
        this.customer = this.customerAdminService.findCustomerByCustomerId(this.reportDefinition.getCustomer().getId());
        this.timeZone = getTimeZone();
        
        this.archiveReportProperties = (ArchiveReportProperties) applicationContext.getBean("archiveReportProperties");
    }
    
    @Override
    public final void run() {
        
        updateReportQueueStatusLaunched();
        try {
            final String sqlString = generateSQLString();
            LOGGER.debug(sqlString);
            
            if (this.jasperFileName.equals(ReportingConstants.REPORT_JASPER_FILE_RATE_SUMMARY)) {
                doReports(sqlString.toString(), new RateSummaryReportDataInfo());
            } else {
                doReports(sqlString.toString());
            }
            if (this.reportHistory == null) {
                this.statusExplanation = this.reportingUtil.getPropertyEN(LabelConstants.REPORT_ERROR_PROPERTY);
                createHistory(null, false);
            }
        } catch (Exception e) {
            // To catch problem with stalled reports, if we get an exception here it will not be caught and the reportQueue record might not be deleted
            try {
                e.printStackTrace();
                if (this.reportHistory == null || this.reportHistory.getId() == null) {
                    this.statusExplanation = this.reportingUtil.getPropertyEN(LabelConstants.REPORT_ERROR_PROPERTY);
                    createHistory(null, false);
                }
            } catch (Exception ex) {
                LOGGER.error("STALLED REPORT POSIBILITY!!!!!!!!!!!!!!!!!!!!!!!!!");
                LOGGER.error("STALLED REPORT CAUSE: " + ex.getMessage());
                ex.printStackTrace();
            }
        }
        
    }
    
    protected abstract String generateSQLString() throws Exception;
    
    protected abstract String createSqlRecordCountQuery() throws Exception;
    
    protected abstract int getMaxRecordCount(Errors errors) throws Exception;
    
    protected abstract String createFileName();
    
    public final String createFileName(final StringBuilder newFileName) {
        String timestampFormat = ReportingConstants.REPORTS_DEFAULT_FILE_NAME_TIME_FORMAT;
        try {
            timestampFormat = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.REPORTS_FILE_NAME_TIME_FORMAT,
                                                                         ReportingConstants.REPORTS_DEFAULT_FILE_NAME_TIME_FORMAT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        newFileName.append("-" + DateUtil.getFileNameTimestamp(this.reportQueue.getExecutionBeginGmt(), timestampFormat, this.timeZone));
        newFileName.append(StandardConstants.STRING_DOT
                           + (this.reportDefinition.isIsCsvOrPdf() ? ReportingConstants.REPORTS_FORMAT_PDF : ReportingConstants.REPORTS_FORMAT_CSV));
        return newFileName.toString();
    }
    
    protected final void updateReportQueueStatusLaunched() {
        final ReportStatusType reportStatusType = new ReportStatusType();
        reportStatusType.setId(ReportingConstants.REPORT_STATUS_LAUNCHED);
        this.reportQueue = (ReportQueue) this.entityService.merge(this.reportQueue);
        this.reportQueue.setReportStatusType(reportStatusType);
        this.reportQueue.setExecutionBeginGmt(new Date());
        this.reportQueueService.updateReportQueue(this.reportQueue);
    }
    
    private String getTimeZone() {
        final CustomerProperty timeZoneProperty = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(this.customer.getId(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        final String timeZoneTemp = timeZoneProperty.getPropertyValue();
        return timeZoneTemp;
    }
    
    /**
     * Generate query for All paystation from one customer
     * 
     * @param tableName
     * @param query
     */
    protected final void getCustomerPointOfSaleWhere(final String tableName, final StringBuilder query) {
        // customerId should come from session buffer
        final int customerId = this.reportDefinition.getCustomer().getId();
        
        query.append(" (");
        // Paystation table name
        query.append(tableName);
        query.append(".id > 0)");
        query.append(SPACE_AND_SPACE_OPEN_PERENTHESES);
        // Paystation table name
        query.append(tableName);
        query.append(".CustomerId=");
        query.append(customerId);
        query.append(CLOSE_PARENTHESES_SPACE);
    }
    
    /**
     * Check if input 'customerIds' contains one id only and use it for searching Route. Otherwise return input Customer Id.
     * 
     * @return int Customer Id from input customerIds or input Customer parameter.
     */
    private int findChildCustomerId(final Customer customerTemp, final String childCustomerIds) {
        // Check if 'childCustomerIds' contains only one id.
        if (childCustomerIds != null && childCustomerIds.indexOf(",") == -1) {
            return Integer.parseInt(childCustomerIds);
        }
        return customerTemp.getId();
    }
    
    protected final String[] getRoutePointOfSaleWhere(final List<ReportLocationValue> selectedList, final StringBuilder query,
        final boolean isAllSelected, final String childCustomerIds, final boolean includeVirtualPS) {
        final List<String> selectedRouteNameList = new ArrayList<String>();
        final List<String> posIdList = new ArrayList<String>();
        
        // "All Routes" selected
        if (isAllSelected) {
            final int custId = findChildCustomerId(this.reportDefinition.getCustomer(), childCustomerIds);
            final Collection<Route> routeList = this.routeService.findRoutesByCustomerId(custId);
            if (routeList != null) {
                for (Route route : routeList) {
                    final List<PointOfSale> posList =
                            this.routeService.findPointOfSalesByRouteId(route.getId(), !this.reportDefinition.isIsOnlyVisible());
                    appendAllInList(posList, posIdList, includeVirtualPS);
                }
            }
            for (ReportLocationValue selected : selectedList) {
                if (ReportingConstants.REPORT_SELECTION_ALL == selected.getObjectId()) {
                    selectedRouteNameList.add(this.reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL_ROUTES));
                    break;
                }
            }
        } else {
            for (ReportLocationValue selected : selectedList) {
                // Route selected
                if (selected.getObjectType().equals(Route.class.getName())) {
                    final int routeId = selected.getObjectId();
                    final List<PointOfSale> posList = this.routeService.findPointOfSalesByRouteId(routeId, !this.reportDefinition.isIsOnlyVisible());
                    appendAllInList(posList, posIdList, includeVirtualPS);
                    
                    final Route route = this.routeService.findRouteById(routeId);
                    selectedRouteNameList.add(route.getName());
                }
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Route posIds: " + posIdList.toString());
        }
        
        if (posIdList.size() > 0) {
            final String[] posIds = new String[posIdList.size()];
            query.append(convertArrayToDelimiterString(posIdList.toArray(posIds), ','));
        } else {
            // Garbage one
            query.append(FIVE_ZEROS);
        }
        
        final String[] selectedRouteNames = new String[selectedRouteNameList.size()];
        selectedRouteNameList.toArray(selectedRouteNames);
        return selectedRouteNames;
    }
    
    protected final String[] getPointOfSaleWhere(final List<ReportLocationValue> selectedList, final StringBuilder query, final String tableName) {
        final List<String> selectedRouteNameList = new ArrayList<String>();
        final List<String> posIdList = new ArrayList<String>();
        for (ReportLocationValue selected : selectedList) {
            // PayStation selected
            if (selected.getObjectType().equals(PointOfSale.class.getName())) {
                final int pointOfSaleId = selected.getObjectId();
                final PointOfSale pos = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
                
                posIdList.add(String.valueOf(pointOfSaleId));
                selectedRouteNameList.add(pos.getName());
            }
        }
        LOGGER.debug("PointOfSale posIds: " + posIdList.toString());
        
        if (posIdList.size() > 0) {
            query.append(AND_SPACE);
            if (POS.equals(tableName)) {
                query.append(POS_ID_IN_OPEN_PARENTHESES);
            } else {
                query.append(tableName);
                query.append(POINT_OF_SALE_ID_IN_OPEN_PARENTHESES);
            }
            final String[] posIds = new String[posIdList.size()];
            query.append(convertArrayToDelimiterString(posIdList.toArray(posIds), ','));
            query.append(CLOSE_PARENTHESES_SPACE);
        }
        
        final String[] selectedRouteNames = new String[selectedRouteNameList.size()];
        selectedRouteNameList.toArray(selectedRouteNames);
        return selectedRouteNames;
    }
    
    protected final String[] getLocationPointOfSaleWhere(final List<ReportLocationValue> selectedList, final StringBuilder query,
        final boolean includeVirtualPS) {
        final List<String> selectedLocationPaystationNameList = new ArrayList<String>();
        final List<String> posIdList = new ArrayList<String>();
        
        for (ReportLocationValue selected : selectedList) {
            // Location Selected
            if (selected.getObjectType().equals(Location.class.getName())) {
                final int locationId = selected.getObjectId();
                
                final Location location = this.locationService.findLocationById(locationId);
                final List<PointOfSale> posList = this.pointOfSaleService.findPointOfSalesByLocationId(locationId);
                appendAllInList(posList, posIdList, includeVirtualPS);
                selectedLocationPaystationNameList.add(location.getName());
            }
            
            // PointOfSale Selected
            if (selected.getObjectType().equals(PointOfSale.class.getName())) {
                final int posId = selected.getObjectId();
                final PointOfSale pos = this.pointOfSaleService.findPointOfSaleById(posId);
                if (!posIdList.contains(Integer.toString(posId))) {
                    posIdList.add(Integer.toString(posId));
                }
                selectedLocationPaystationNameList.add(pos.getName());
            }
        }
        LOGGER.debug("Location/Paystation posIds: " + posIdList.toString());
        
        if (posIdList.size() > 0) {
            final String[] posIds = new String[posIdList.size()];
            query.append(convertArrayToDelimiterString(posIdList.toArray(posIds), ','));
        } else {
            // Garbage one
            query.append(FIVE_ZEROS);
        }
        
        final String[] selectedLocationPaystationNames = new String[selectedLocationPaystationNameList.size()];
        selectedLocationPaystationNameList.toArray(selectedLocationPaystationNames);
        return selectedLocationPaystationNames;
    }
    
    private String[] getLocationPointOfSaleWhere(final List<ReportLocationValue> selectedList, final StringBuilder query, final String tableName) {
        final List<String> selectedLocationPaystationNameList = new ArrayList<String>();
        final List<String> lIdList = new ArrayList<String>();
        
        for (ReportLocationValue selected : selectedList) {
            if (selected.getObjectType().equals(Location.class.getName())) {
                final int locationId = selected.getObjectId();
                
                final Location location = this.locationService.findLocationById(locationId);
                
                selectedLocationPaystationNameList.add(location.getName());
                
                if (location.getIsParent()) {
                    final List<Location> childLocations = this.locationService.getLocationsByParentLocationId(location.getId());
                    for (Location childLocation : childLocations) {
                        if (!lIdList.contains(Integer.toString(childLocation.getId()))) {
                            lIdList.add(Integer.toString(childLocation.getId()));
                        }
                    }
                } else {
                    if (!lIdList.contains(Integer.toString(locationId))) {
                        lIdList.add(Integer.toString(locationId));
                    }
                }
            }
        }
        
        LOGGER.debug("Region rgIds: " + lIdList.toString());
        
        query.append(AND_SPACE);
        query.append(tableName);
        query.append(".LocationId");
        query.append(" IN (");
        final String[] lIds = new String[lIdList.size()];
        query.append(convertArrayToDelimiterString(lIdList.toArray(lIds), ','));
        // Close 'pur.LocationId IN'
        query.append(CLOSE_PARENTHESES_SPACE);
        
        final String[] selectedLocationPaystationNames = new String[selectedLocationPaystationNameList.size()];
        selectedLocationPaystationNameList.toArray(selectedLocationPaystationNames);
        return selectedLocationPaystationNames;
    }
    
    protected final void getWherePointOfSale(final StringBuilder query, final String tableName, final boolean includeVirtualPS) {
        
        final List<ReportLocationValue> locationList =
                this.reportLocationValueService.findReportLocationValuesByReportDefinition(this.reportDefinition.getId());
        String customerIds = null;
        
        if (this.customer.isIsParent()) {
            final List<ReportLocationValue> customerValueList = new ArrayList<ReportLocationValue>();
            for (int i = 0; i < locationList.size(); i++) {
                final ReportLocationValue location = locationList.get(i);
                if (location.getObjectType().equals(Customer.class.getName())) {
                    customerValueList.add(locationList.remove(i));
                    i--;
                }
            }
            
            final List<String> customerIdList = new ArrayList<String>();
            final List<String> customerNameList = new ArrayList<String>();
            if (customerValueList.size() == 1 && customerValueList.get(0).getObjectId() == ReportingConstants.REPORT_SELECTION_ALL) {
                final List<Customer> childCustomerList = this.customerAdminService.findAllChildCustomers(this.customer.getId());
                
                for (Customer childCustomer : childCustomerList) {
                    customerIdList.add(childCustomer.getId().toString());
                }
                customerNameList.add(this.reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL_ORGANIZATIONS));
                
            } else {
                for (ReportLocationValue customerValue : customerValueList) {
                    final Customer childCustomer = this.customerAdminService.findCustomerByCustomerId(customerValue.getObjectId());
                    customerIdList.add(childCustomer.getId().toString());
                    customerNameList.add(childCustomer.getName());
                }
            }
            
            customerIds = convertArrayToDelimiterString(customerIdList.toArray(new String[customerNameList.size()]), ',');
            this.organizationNameList = customerNameList.toArray(new String[customerNameList.size()]);
            query.append(tableName);
            query.append(".CustomerId IN (");
            query.append(customerIds);
            query.append(CLOSE_PARENTHESES_SPACE);
        } else {
            query.append(tableName);
            query.append(".CustomerId = ");
            query.append(this.customer.getId());
            query.append(StandardConstants.STRING_EMPTY_SPACE);
        }
        
        final int locationType = getFilterTypeId(this.reportDefinition.getLocationFilterTypeId());
        
        final boolean allSelected = isAllSelected(locationList);
        
        if (ReportingConstants.REPORT_FILTER_LOCATION_LOCATION_TREE == locationType) {
            if (allSelected) {
                this.locationValueNameList = new String[] { this.reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL_LOCATIONS) };
            } else {
                this.locationValueNameList = getLocationPointOfSaleWhere(locationList, query, tableName);
            }
        } else if (ReportingConstants.REPORT_FILTER_LOCATION_ROUTE_TREE == locationType) {
            if (POS.equals(tableName)) {
                query.append("AND pos.Id IN (");
            } else {
                query.append(AND_SPACE);
                query.append(tableName);
                query.append(POINT_OF_SALE_ID_IN_OPEN_PARENTHESES);
            }
            this.locationValueNameList = getRoutePointOfSaleWhere(locationList, query, allSelected, customerIds, includeVirtualPS);
            query.append(CLOSE_PARENTHESES_SPACE);
            
        } else if (ReportingConstants.REPORT_FILTER_LOCATION_PAY_STATION_TREE == locationType) {
            if (allSelected) {
                this.locationValueNameList = new String[] { this.reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL_PAY_STATIONS) };
            } else {
                this.locationValueNameList = getPointOfSaleWhere(locationList, query, tableName);
            }
        } else {
            query.append(AND_SPACE);
            if (POS.equals(tableName)) {
                query.append(POS_ID_IN_OPEN_PARENTHESES);
            } else {
                query.append(tableName);
                query.append(POINT_OF_SALE_ID_IN_OPEN_PARENTHESES);
                
            }
            // Garbage one
            query.append(FIVE_ZEROS);
            query.append(CLOSE_PARENTHESES_SPACE);
        }
    }
    
    protected final String convertArrayToDelimiterString(final String[] valueArray, final char delimiter) {
        if (valueArray == null) {
            return null;
        }
        
        final StringBuilder sb = new StringBuilder();
        for (String value : valueArray) {
            sb.append(value);
            sb.append(delimiter);
        }
        // delete the last ", "
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        
        return sb.toString();
    }
    
    protected final void appendAllInList(final List<PointOfSale> posList, final List<String> posIdList, final boolean includeVirtualPS) {
        if (posList != null) {
            final Iterator<PointOfSale> it = posList.iterator();
            while (it.hasNext()) {
                final PointOfSale pos = (PointOfSale) it.next();
                final String posId = Integer.toString(pos.getId());
                
                if (!posIdList.contains(posId)) {
                    if (includeVirtualPS) {
                        posIdList.add(posId);
                    } else {
                        // Was changed from pointOfSaleType.POINT_OF_SALE_TYPE_DIGITAL_API
                        final PaystationType paystationType = this.pointOfSaleService.findPayStationTypeByPointOfSaleId(pos.getId());
                        if (paystationType.getId() != WebCoreConstants.PAY_STATION_TYPE_TEST_VIRTUAL) {
                            posIdList.add(posId);
                        }
                    }
                }
            }
        }
    }
    
    protected final boolean isAllSelected(final List<ReportLocationValue> locationList) {
        if (locationList == null) {
            return true;
        }
        for (ReportLocationValue selected : locationList) {
            if (ReportingConstants.REPORT_SELECTION_ALL == selected.getObjectId()) {
                return true;
            }
        }
        return false;
    }
    
    protected final void doReports(final String sqlQuery) throws Exception {
        doReports(sqlQuery, null);
    }
    
    protected final void doReports(final String sqlQuery, final JRDataSourceEventHandler dataSourceEventHandler) throws Exception {
        byte[] fileContent = null;
        JasperReport jasperReport = null;
        final Integer customerId = this.reportDefinition.getCustomer().getId();
        
        // Get the jasper reports class file
        if (this.reportDefinition.getReportType().getId() != ReportingConstants.REPORT_TYPE_TRANSACTION_DELAY) {
            File jasperFile = null;
            InputStream stream = null;
            try {
                jasperFile = new File(ReportBaseThread.jasperFilePath + this.jasperFileName);
                stream = new FileInputStream(jasperFile);
                final File tempPath = new File(tempReportsPath);
                if (!tempPath.exists()) {
                    if (!tempPath.mkdirs()) {
                        LOGGER.error("Cannot create temp reports directory , " + tempReportsPath);
                        this.statusExplanation = this.reportingUtil.getPropertyEN("error.report.status.temp.directory");
                        throw new ApplicationException("Cannot create temp reports directory");
                    }
                }
                
                try {
                    jasperReport = (JasperReport) JRLoader.loadObject(stream);
                    jasperReport.setWhenNoDataType(WhenNoDataTypeEnum.ALL_SECTIONS_NO_DETAIL);
                } catch (JRException e) {
                    LOGGER.error(UNABLE_TO_LOAD, e);
                    this.statusExplanation = this.reportingUtil.getPropertyEN("error.report.status.jasper");
                    throw new ApplicationException(UNABLE_TO_LOAD);
                }
                
            } finally {
                if (stream != null) {
                    stream.close();
                }
            }
        }
        LOGGER.info("QUERY: " + sqlQuery);
        ComboPooledDataSource dataSource = getDataSource(this.reportDefinition);
        LOGGER.info("++++ DataSource info for report queries: " + dataSource.toString() + " ++++");
        
        final Connection connection = dataSource.getConnection();
        
        final String countQuery = createSqlRecordCountQuery();
        final int maxRecords = getMaxRecordCount(null);
        byte[] fileContentZipped = null;
        
        final int maxRecordUsingVirtualizer = Integer.parseInt(this.emsPropertiesService
                .getPropertyValue(EmsPropertiesService.REPORTS_MAX_RECORDS_USING_VIRTUALIZER,
                                  String.valueOf(ReportingConstants.REPORTS_DEFAULT_MAX_RECORDS_USING_VIRTUALIZER), false));
        
        if (this.reportDefinition.isIsCsvOrPdf()) {
            final Map<String, Object> parameters = createReportParameterList(sqlQuery, customerId);
            if (dataSourceEventHandler != null) {
                parameters.put("dataSourceEventHandler", dataSourceEventHandler);
            }
            
            if (!this.reportDefinition.isIsDetailsOrSummary()) {
                fileContent = createJasperReport(jasperReport, parameters, connection, this.reportDefinition.isIsCsvOrPdf(), tempReportsPath,
                                                 sqlQuery, countQuery, maxRecords, maxRecordUsingVirtualizer, dataSourceEventHandler);
            } else {
                fileContent = createJasperReport(jasperReport, parameters, connection, this.reportDefinition.isIsCsvOrPdf(), tempReportsPath,
                                                 sqlQuery, countQuery, maxRecords, 0, dataSourceEventHandler);
            }
        } else {
            if (this.jasperFileName.equals(ReportingConstants.REPORT_JASPER_FILE_AUDITS)
                || this.jasperFileName.equals(ReportingConstants.REPORT_JASPER_FILE_REPLENISH)
                || this.jasperFileName.equals(ReportingConstants.REPORT_JASPER_FILE_TRANSACTION_DELAY)) {
                fileContent = createCSVReport(sqlQuery, connection, this.reportDefinition.isIsCsvOrPdf(), tempReportsPath, this.timeZone, countQuery,
                                              maxRecords);
                fileContentZipped = this.zipFileContent(fileContent);
            } else {
                final Map<String, Object> parameters = createReportParameterList(sqlQuery, customerId);
                
                if (!this.reportDefinition.isIsDetailsOrSummary()) {
                    fileContentZipped = createJasperReportForCSV(jasperReport, parameters, connection, tempReportsPath, sqlQuery, countQuery,
                                                                 maxRecords, maxRecordUsingVirtualizer, dataSourceEventHandler);
                } else {
                    fileContentZipped = createJasperReportForCSV(jasperReport, parameters, connection, tempReportsPath, sqlQuery, countQuery,
                                                                 maxRecords, 0, dataSourceEventHandler);
                }
            }
        }
        
        final List<String> emailList = this.reportDefinitionService.findEmailsByReportDefinitionId(this.reportDefinition.getId());
        boolean isWithEmail = false;
        if (emailList != null && !emailList.isEmpty()) {
            isWithEmail = true;
        }
        
        // this this oddly named column is set to true, then the report is a pdf report
        if (this.reportDefinition.isIsCsvOrPdf()) {
            this.reportHistory = createHistory(fileContent, isWithEmail);
        } else {
            this.reportHistory = createHistory(fileContentZipped, isWithEmail);
        }
        
        if (isWithEmail) {
            for (String email : emailList) {
                if (this.reportDefinition.isIsCsvOrPdf()) {
                    this.mailerService
                            .sendMessage(URLDecoder.decode(email, WebSecurityConstants.URL_ENCODING_LATIN1),
                                         this.reportingUtil.getPropertyEN(LabelConstants.REP_EMAIL_SUBJECT) + SPACE_DASH_SPACE
                                                                                                             + this.reportDefinition.getTitle(),
                                         this.reportingUtil.getPropertyEN(LabelConstants.REP_EMAIL_CONTENT), null, fileContent, createFileName());
                } else {
                    final ReportContent content = decompress(fileContentZipped);
                    if (content.isZipped()) {
                        this.mailerService
                                .sendMessage(URLDecoder.decode(email, WebSecurityConstants.URL_ENCODING_LATIN1),
                                             this.reportingUtil.getPropertyEN(LabelConstants.REP_EMAIL_SUBJECT) + " - "
                                                                                                                 + this.reportDefinition.getTitle(),
                                             this.reportingUtil.getPropertyEN(LabelConstants.REP_EMAIL_CONTENT), null, fileContentZipped,
                                             createFileName() + ".zip");
                    } else {
                        this.mailerService
                                .sendMessage(URLDecoder.decode(email, WebSecurityConstants.URL_ENCODING_LATIN1),
                                             this.reportingUtil.getPropertyEN(LabelConstants.REP_EMAIL_SUBJECT) + " - "
                                                                                                                 + this.reportDefinition.getTitle(),
                                             this.reportingUtil.getPropertyEN(LabelConstants.REP_EMAIL_CONTENT), null, content.getData(),
                                             createFileName());
                    }
                }
            }
        }
    }
    
    protected ComboPooledDataSource getDataSource(final ReportDefinition reportDef) {
        if (useArchiveReportDB(reportDef)) {
            return this.archivedDataSource;
        } else {
            return this.currentDataSource;
        }
    }
    
    private boolean useArchiveReportDB(final ReportDefinition reportDef) {
        return isOlderThanArchiveYear(reportDef.getPrimaryDateRangeBeginGmt()) || isOlderThanArchiveYear(reportDef.getSecondaryDateRangeBeginGmt());
    }
    
    private boolean isOlderThanArchiveYear(final Date date) {
        final int currentReportDays = this.archiveReportProperties.getLimit();
        return date != null && date.toInstant().isBefore(Instant.now().minus(Duration.of(currentReportDays, ChronoUnit.DAYS)));
    }
    
    private byte[] zipFileContent(final byte[] fileContent) {
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        final ZipOutputStream out = new ZipOutputStream(baos);
        out.setMethod(ZipOutputStream.DEFLATED);
        out.setLevel(Deflater.BEST_COMPRESSION);
        boolean ioExceptionOccured = false;
        try {
            out.putNextEntry(new ZipEntry(createFileName()));
            out.write(fileContent);
        } catch (IOException e) {
            LOGGER.warn("Error zipping report: ", e);
            ioExceptionOccured = true;
        } finally {
            try {
                baos.close();
                out.closeEntry();
                out.close();
            } catch (IOException e) {
            }
        }
        return baos != null && baos.toByteArray().length != 0 && !ioExceptionOccured ? baos.toByteArray() : null;
    }
    
    /**
     * @param compressed
     *            byte arrary
     * @return ReportContent with isZipped set to true if fileContent > 9MB
     */
    public final ReportContent decompress(final byte[] data) {
        final ReportContent fileContent = new ReportContent();
        if (data == null || data.length == 0) {
            fileContent.setData(data);
            fileContent.setZipped(true);
            return fileContent;
        }
        final ZipInputStream zi = new ZipInputStream(new ByteArrayInputStream(data));
        try {
            if (zi.available() > 0) {
                zi.getNextEntry();
            }
        } catch (IOException e1) {
            LOGGER.error("ZipInputStream error: ", e1);
        }
        if (zi != null) {
            try (final ByteArrayOutputStream out = new ByteArrayOutputStream(data.length)) {
                final byte[] buffer = new byte[1024];
                int readCnt = 0;
                final int limit = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.REPORTS_ZIPPED_LIMIT_MB,
                                                                                  EmsPropertiesService.DEFAULT_REPORTS_ZIPPED_LIMIT_MB)
                                  * 1024 * 1024;
                int i = 0;
                while ((readCnt = zi.read(buffer)) > 0) {
                    out.write(buffer, 0, readCnt);
                    i++;
                    final int moretThan9MB = readCnt * i;
                    if (moretThan9MB > limit) {
                        fileContent.setData(data);
                        fileContent.setZipped(true);
                        return fileContent;
                    }
                }
                fileContent.setData(out.toByteArray());
                fileContent.setZipped(false);
                
            } catch (final IOException e) {
                LOGGER.error("Writing into byte array error: ", e);
            }
        }
        return fileContent;
    }
    
    private byte[] createJasperReportForCSV(final JasperReport jasperReport, final Map<String, Object> parameters, final Connection connection,
        final String tempReportsPathTemp, final String sqlQuery, final String countQuery, final int maxRecordCount, final int maxRecordForVirtualizer,
        final JRDataSourceEventHandler dataSourceEventHandler) {
        byte[] fileContent = null;
        Statement statement = null;
        
        try {
            
            final long start = System.currentTimeMillis();
            int recordCount = 0;
            boolean isTooManyRecords = false;
            
            statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            
            if (maxRecordCount > ReportingConstants.REPORTS_DEFAULT_MAX_RECORDS && countQuery != null && !"".equals(countQuery)) {
                final ResultSet countResultSets = statement.executeQuery(countQuery);
                if (countResultSets.next()) {
                    recordCount = countResultSets.getInt(1);
                    // TODO Failed Report
                    if (recordCount > maxRecordCount) {
                        // reports_session.setError(true);
                        // reports_session.setErrorCode("error.common.tooManyRecords");
                        LOGGER.info(RECORD_COUNT + recordCount + IS_GREAT_MAX_REC + maxRecordCount);
                        this.statusExplanation =
                                this.reportingUtil.getPropertyEN(LabelConstants.REPORT_ERROR_TOO_LARGE,
                                                                 new String[] { String.valueOf(recordCount), String.valueOf(maxRecordCount), });
                        isTooManyRecords = true;
                    } else {
                        LOGGER.debug(RECORD_COUNT + recordCount + LESS_THAN_MAX_ALLOWED_RECS + maxRecordCount);
                    }
                }
                countResultSets.close();
            }
            
            if (!isTooManyRecords) {
                // if (!reports_session.isError())
                ResultSet resultSet = null;
                
                if (sqlQuery != null && !isTooManyRecords) {
                    this.sqlExecutionBeginGMT = new Date();
                    statement.close();
                    statement = connection.prepareStatement(sqlQuery, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
                    statement.setFetchSize(Integer.MIN_VALUE);
                    resultSet = statement.executeQuery(sqlQuery);
                    resultSet.setFetchSize(FETCH_SIZE);
                    this.sqlExecutionEndGMT = new Date();
                }
                
                fileContent = this.createReport(jasperReport, parameters, resultSet, recordCount, tempReportsPathTemp, maxRecordForVirtualizer, start,
                                                dataSourceEventHandler);
            }
            
        } catch (Exception e) {
            LOGGER.warn(ERROR_GEN_REP, e);
            e.printStackTrace();
            this.statusExplanation = this.reportingUtil.getPropertyEN(LabelConstants.REPORT_ERROR_PROPERTY);
            
        } finally {
            
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException sqle) {
                    LOGGER.error(JASPERTHREAD, sqle);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException sqle) {
                    LOGGER.error(JASPERTHREAD, sqle);
                }
            }
        }
        
        return fileContent;
    }
    
    private byte[] createReport(final JasperReport jasperReport, final Map<String, Object> parameters, final ResultSet resultSet, int recordCount,
        final String tempReportsPathTemp, final int maxRecordForVirtualizer, final long start,
        final JRDataSourceEventHandler dataSourceEventHandler) {
        byte[] fileContent = null;
        ByteArrayOutputStream baos = null;
        ZipOutputStream out = null;
        
        try {
            
            JasperPrint jasperPrint = null;
            JRSwapFileVirtualizer virtualizer = null;
            
            if (maxRecordForVirtualizer != 0 && recordCount > maxRecordForVirtualizer) {
                final String swapPath = tempReportsPathTemp + File.separator + SWAP_PATH;
                final File tempDir = new File(swapPath);
                if (!tempDir.exists()) {
                    tempDir.mkdirs();
                }
                final JRSwapFile swapFile = new JRSwapFile(swapPath, StandardConstants.ONE_KILOBYTE, StandardConstants.ONE_KILOBYTE);
                // creating the virtualizer
                virtualizer = new JRSwapFileVirtualizer(MAX_VIRTUALIZER, swapFile, true);
                
                parameters.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
                
                parameters.put(JRParameter.IS_IGNORE_PAGINATION, false);
                
                LOGGER.info(JRSWAPVIRT_INIT);
            }
            
            final ReportJRDataSource ds = new ReportJRDataSource(resultSet, dataSourceEventHandler);
            jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, ds);
            
            if (maxRecordForVirtualizer != 0 && recordCount > maxRecordForVirtualizer) {
                /* This should be set after fillReport. */
                virtualizer.setReadOnly(true);
            }
            if (resultSet != null) {
                resultSet.close();
            }
            LOGGER.info(FILL_REPT_PROC + (System.currentTimeMillis() - start) + SPACE_MILLISECONDS);
            
            final String fullPath = tempReportsPathTemp + File.separator + SWAP_PATH;
            final File tempDir = new File(fullPath);
            if (!tempDir.exists()) {
                tempDir.mkdirs();
            }
            
            baos = new ByteArrayOutputStream();
            out = new ZipOutputStream(baos);
            out.setMethod(ZipOutputStream.DEFLATED);
            out.setLevel(Deflater.BEST_COMPRESSION);
            out.putNextEntry(new ZipEntry(createFileName()));
            
            final JRExporter exporter = new JRCsvExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
            exporter.exportReport();
            
            /* Clean up the swap file. */
            if (maxRecordForVirtualizer != 0 && recordCount > maxRecordForVirtualizer) {
                virtualizer.cleanup();
                LOGGER.info(JRSWAP_CLEAN);
            }
            LOGGER.info("Exporting to CVS took " + (System.currentTimeMillis() - start) + MILLISECONDS);
            
        } catch (JRException jre) {
            LOGGER.warn(JREXCEPT_GEN_REPT, jre);
            jre.printStackTrace();
            this.statusExplanation = this.reportingUtil.getPropertyEN(LabelConstants.REPORT_ERROR_PROPERTY);
        } catch (Exception e) {
            LOGGER.warn(ERROR_GEN_REP, e);
            e.printStackTrace();
            this.statusExplanation = this.reportingUtil.getPropertyEN(LabelConstants.REPORT_ERROR_PROPERTY);
            // reports_session.setError(true);
        } finally {
            
            try {
                if (baos != null) {
                    baos.close();
                }
                if (out != null) {
                    out.closeEntry();
                    out.close();
                }
            } catch (IOException e) {
            }
        }
        
        fileContent = (baos != null) ? baos.toByteArray() : null;
        return fileContent;
    }
    
    private byte[] createJasperReport(final JasperReport jasperReport, final Map<String, Object> parameters, final Connection connection,
        final boolean isPDF, final String tempReportsPathTemp, final String sqlQuery, final String countQuery, final int maxRecordCount,
        final int maxRecordForVirtualizer, final JRDataSourceEventHandler dataSourceEventHandler) {
        byte[] fileContent = null;
        Statement statement = null;
        try {
            
            JasperPrint jasperPrint = null;
            JRSwapFileVirtualizer virtualizer = null;
            long start = System.currentTimeMillis();
            int recordCount = 0;
            boolean isTooManyRecords = false;
            
            statement = connection.createStatement();
            if (maxRecordCount > ReportingConstants.REPORTS_DEFAULT_MAX_RECORDS && countQuery != null && !"".equals(countQuery)) {
                final ResultSet countResultSets = statement.executeQuery(countQuery);
                if (countResultSets.next()) {
                    recordCount = countResultSets.getInt(1);
                    // TODO Failed Report
                    if (recordCount > maxRecordCount) {
                        // reports_session.setError(true);
                        // reports_session.setErrorCode("error.common.tooManyRecords");
                        LOGGER.info(RECORD_COUNT + recordCount + IS_GREAT_MAX_REC + maxRecordCount);
                        this.statusExplanation =
                                this.reportingUtil.getPropertyEN(LabelConstants.REPORT_ERROR_TOO_LARGE,
                                                                 new String[] { String.valueOf(recordCount), String.valueOf(maxRecordCount) });
                        isTooManyRecords = true;
                    } else {
                        LOGGER.debug(RECORD_COUNT + recordCount + LESS_THAN_MAX_ALLOWED_RECS + maxRecordCount);
                    }
                }
                countResultSets.close();
            }
            
            if (!isTooManyRecords) {
                // if (!reports_session.isError())
                ResultSet resultSet = null;
                
                if (sqlQuery != null && !isTooManyRecords) {
                    this.sqlExecutionBeginGMT = new Date();
                    resultSet = statement.executeQuery(sqlQuery);
                    this.sqlExecutionEndGMT = new Date();
                }
                
                if (maxRecordForVirtualizer != 0 && recordCount > maxRecordForVirtualizer) {
                    final String swapPath = tempReportsPathTemp + File.separator + SWAP_PATH;
                    final File tempDir = new File(swapPath);
                    if (!tempDir.exists()) {
                        tempDir.mkdirs();
                    }
                    final JRSwapFile swapFile = new JRSwapFile(swapPath, 1024, 1024);
                    // creating the virtualizer
                    virtualizer = new JRSwapFileVirtualizer(MAX_JRV_OBJ, swapFile, true);
                    
                    parameters.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
                    parameters.put(JRParameter.IS_IGNORE_PAGINATION, false);
                    
                    LOGGER.info(JRSWAPVIRT_INIT);
                }
                
                final ReportJRDataSource ds = new ReportJRDataSource(resultSet, dataSourceEventHandler);
                jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, ds);
                
                if (maxRecordForVirtualizer != 0 && recordCount > maxRecordForVirtualizer) {
                    /* This should be set after fillReport. */
                    virtualizer.setReadOnly(true);
                }
                if (resultSet != null) {
                    resultSet.close();
                }
                LOGGER.info(FILL_REPT_PROC + (System.currentTimeMillis() - start) + SPACE_MILLISECONDS);
                
                final String fullPath = tempReportsPathTemp + File.separator + SWAP_PATH;
                final File tempDir = new File(fullPath);
                if (!tempDir.exists()) {
                    tempDir.mkdirs();
                }
                
                // Map imagesMap = new HashMap();
                // session_.setAttribute("IMAGES_MAP", images_map);
                // Export report
                start = System.currentTimeMillis();
                
                if (isPDF) {
                    fileContent = JasperExportManager.exportReportToPdf(jasperPrint);
                } else if (!isPDF) {
                    final StringBuffer buffer = new StringBuffer();
                    final JRExporter exporter = new JRCsvExporter();
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STRING_BUFFER, buffer);
                    exporter.exportReport();
                    fileContent = buffer.toString().getBytes();
                }
                
                /* Clean up the swap file. */
                if (maxRecordForVirtualizer != 0 && recordCount > maxRecordForVirtualizer) {
                    virtualizer.cleanup();
                    LOGGER.info(JRSWAP_CLEAN);
                }
                LOGGER.info("Exporting to " + (isPDF ? "PDF" : "CVS") + " took " + (System.currentTimeMillis() - start) + MILLISECONDS);
            }
        } catch (JRException jre) {
            LOGGER.warn(JREXCEPT_GEN_REPT, jre);
            jre.printStackTrace();
            this.statusExplanation = this.reportingUtil.getPropertyEN(LabelConstants.REPORT_ERROR_PROPERTY);
        } catch (Exception e) {
            LOGGER.warn(ERROR_GEN_REP, e);
            e.printStackTrace();
            this.statusExplanation = this.reportingUtil.getPropertyEN(LabelConstants.REPORT_ERROR_PROPERTY);
            // reports_session.setError(true);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException sqle) {
                    LOGGER.error(JASPERTHREAD, sqle);
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException sqle) {
                    LOGGER.error(JASPERTHREAD, sqle);
                }
            }
        }
        
        return fileContent;
    }
    
    /*
     * private void cleanReportRepository(ReportHistory reportHistory) {
     * int maxFileTotal = ReportingConstants.REPORTS_DEFAULT_SIZE_LIMIT_KB;
     * try {
     * maxFileTotal = emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.REPORTS_SIZE_LIMIT_KB, ReportingConstants.REPORTS_DEFAULT_SIZE_LIMIT_KB);
     * } catch (Exception e) {
     * e.printStackTrace();
     * }
     * reportRepositoryService.cleanReportRepository(reportHistory, 8);
     * }
     */
    
    protected final ReportHistory createHistory(byte[] fileContent, final boolean isWithEmail) {
        // First Check if the file is too big
        int maxSize = ReportingConstants.REPORTS_DEFAULT_SIZE_LIMIT_KB;
        try {
            maxSize = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.REPORTS_SIZE_LIMIT_KB,
                                                                      ReportingConstants.REPORTS_DEFAULT_SIZE_LIMIT_KB);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (fileContent != null) {
            if ((fileContent.length / StandardConstants.ONE_KILOBYTE) > maxSize) {
                fileContent = null;
                this.statusExplanation =
                        this.reportingUtil.getPropertyEN("error.report.status.file.too.large", new String[] { String.valueOf(maxSize) });
            }
        }
        
        // TODO Email Customer
        final ReportHistory reportHistoryTemp = new ReportHistory();
        reportHistoryTemp.setReportDefinition(this.reportDefinition);
        final ReportStatusType reportStatusType = new ReportStatusType();
        if (fileContent == null) {
            reportStatusType.setId(ReportingConstants.REPORT_STATUS_FAILED);
        } else {
            reportStatusType.setId(ReportingConstants.REPORT_STATUS_COMPLETED);
            reportHistoryTemp.setFileSizeKb(fileContent.length / StandardConstants.ONE_KILOBYTE);
        }
        reportHistoryTemp.setReportStatusType(reportStatusType);
        reportHistoryTemp.setCustomer(this.reportDefinition.getCustomer());
        reportHistoryTemp.setUserAccount(this.reportDefinition.getUserAccount());
        reportHistoryTemp.setReportDateRangeBeginGmt(this.reportQueue.getPrimaryDateRangeBeginGmt());
        reportHistoryTemp.setReportDateRangeEndGmt(this.reportQueue.getPrimaryDateRangeEndGmt());
        reportHistoryTemp.setReportRepeatType(this.reportDefinition.getReportRepeatType());
        reportHistoryTemp.setScheduledBeginGmt(this.reportQueue.getScheduledBeginGmt());
        reportHistoryTemp.setQueuedGmt(this.reportQueue.getQueuedGmt());
        reportHistoryTemp.setExecutionBeginGmt(this.reportQueue.getExecutionBeginGmt());
        reportHistoryTemp.setSqlBeginGmt(this.sqlExecutionBeginGMT);
        reportHistoryTemp.setSqlEndGmt(this.sqlExecutionEndGMT);
        reportHistoryTemp.setStatusExplanation(this.statusExplanation);
        
        this.reportHistoryService.saveReportHistory(reportHistoryTemp, this.reportQueue, fileContent, createFileName(), isWithEmail);
        return reportHistoryTemp;
    }
    
    protected Map<String, Object> createReportParameterList(final String sqlQuery, final int customerId) {
        final Map<String, Object> parameters = new HashMap<String, Object>();
        
        final Date currentDate = new Date();
        parameters.put("SQLQuery", sqlQuery);
        parameters.put("BaseDir", new File("/WEB-INF/classes/"));
        
        // report date on footer
        final String reportedDateOnFooter = this.formatDate(new SimpleDateFormat(DateUtil.REPORTS_DATE_FORMAT_ON_FOOTER), this.timeZone, currentDate);
        parameters.put("ReportedDateOnFooter", reportedDateOnFooter);
        parameters.put("PrintedDate", this.formatDate(new SimpleDateFormat(DateUtil.REPORTS_OUTPUT_FORMAT), this.timeZone, currentDate));
        
        parameters.put("TimeZone", this.timeZone);
        
        return parameters;
    }
    
    protected final String formatDate(final DateFormat dateFormat, final String timeZoneTemp, final Date date) {
        if (date == null) {
            return WebCoreConstants.N_A_STRING;
        }
        return DateUtil.createDateString(dateFormat, timeZoneTemp, date);
    }
    
    protected final void getWherePurchaseGMT(final StringBuilder query, final String tableName) {
        final int reportType = this.reportDefinition.getReportType().getId();
        
        Date beginDate = null;
        Date endDate = null;
        Integer dateFilter = 0;
        
        if (ReportingConstants.REPORT_TYPE_TRANSACTION_CC_PROCESSING == reportType
            || ReportingConstants.REPORT_TYPE_TRANSACTION_CC_RETRY == reportType) {
            beginDate = this.reportQueue.getSecondaryDateRangeBeginGmt();
            endDate = this.reportQueue.getSecondaryDateRangeEndGmt();
            dateFilter = this.reportDefinition.getSecondaryDateFilterTypeId();
        } else {
            beginDate = this.reportQueue.getPrimaryDateRangeBeginGmt();
            endDate = this.reportQueue.getPrimaryDateRangeEndGmt();
            dateFilter = this.reportDefinition.getPrimaryDateFilterTypeId();
        }
        
        if (dateFilter.intValue() != ReportingConstants.REPORT_SELECTION_ALL) {
            String columnName = "";
            if (ReportingConstants.REPORT_TYPE_TRANSACTION_CC_REFUND == reportType) {
                columnName = "ProcessingDate";
            } else {
                if ("pur".equals(tableName)) {
                    columnName = "PurchaseGMT";
                } else {
                    columnName = PURCHASED_DATE;
                }
            }
            
            query.append(SPACE_AND_SPACE);
            query.append(tableName);
            query.append(StandardConstants.STRING_DOT);
            query.append(columnName);
            query.append(SPACE_BETWEEN_SPACE);
            query.append(beginDate);
            query.append(QUOTE_SPACE_AND_SPACE_QUOTE);
            query.append(endDate);
            query.append("' ");
        }
    }
    
    protected final void getWhereDate(final StringBuilder query, final String tableName, final String columnName, final boolean primary) {
        Date beginDate = null;
        Date endDate = null;
        
        if (primary) {
            if (this.reportQueue.getPrimaryDateRangeBeginGmt() != null && this.reportQueue.getPrimaryDateRangeEndGmt() != null) {
                beginDate = this.reportQueue.getPrimaryDateRangeBeginGmt();
                endDate = this.reportQueue.getPrimaryDateRangeEndGmt();
            }
        } else {
            if (this.reportQueue.getSecondaryDateRangeBeginGmt() != null && this.reportQueue.getSecondaryDateRangeEndGmt() != null) {
                beginDate = this.reportQueue.getSecondaryDateRangeBeginGmt();
                endDate = this.reportQueue.getSecondaryDateRangeEndGmt();
            }
        }
        
        if (beginDate != null && endDate != null) {
            query.append(SPACE_AND_SPACE_OPEN_PERENTHESES);
            query.append(tableName);
            query.append(StandardConstants.STRING_DOT);
            query.append(columnName);
            query.append(SPACE_BETWEEN_SPACE);
            query.append(beginDate);
            query.append(QUOTE_SPACE_AND_SPACE_QUOTE);
            query.append(endDate);
            query.append("') ");
        }
    }
    
    protected final void getWhereCouponNumber(final StringBuilder query) {
        final int couponNumberType = getFilterTypeId(this.reportDefinition.getCouponNumberFilterTypeId());
        switch (couponNumberType) {
            case 0:
            case ReportingConstants.REPORT_FILTER_COUPON_NUMBER_NA:
                break;
            case ReportingConstants.REPORT_FILTER_COUPON_NUMBER_ALL_COUPONS:
                query.append(AND_PUR_COUPON_ID_IS_NOT_NULL);
                break;
            case ReportingConstants.REPORT_FILTER_COUPON_NUMBER_NO_COUPONS:
                query.append(" AND pur.CouponId IS NULL ");
                break;
            case ReportingConstants.REPORT_FILTER_COUPON_NUMBER_SPECIFIC:
                final String couponNumber = this.reportDefinition.getCouponNumberSpecificFilter();
                String couponId = null;
                if ("*".equals(couponNumber)) {
                    query.append(AND_PUR_COUPON_ID_IS_NOT_NULL);
                    break;
                } else if (couponNumber.indexOf('*') >= 0) {
                    couponId = convertWildCards(couponNumber);
                    query.append(" AND (c.Coupon LIKE '" + couponId + "' OR c.Coupon LIKE 'B" + couponId + PLUS_QUOTE_CLOSE_PARENTHESES_SPACE);
                    break;
                } else {
                    query.append(" AND (c.Coupon = '" + couponNumber + "' OR c.Coupon = 'B" + couponNumber + PLUS_QUOTE_CLOSE_PARENTHESES_SPACE);
                }
                break;
            default:
                break;
        }
    }
    
    private String convertWildCards(String value) {
        if (value != null && value.length() > 0) {
            final StringBuffer sb = new StringBuffer(value);
            // add escape character for MySQL wildcard characters % and _
            addEscapeCharacter(ReportingConstants.MYSQL_MULTIPLE_WILD_CARD, sb, 0);
            addEscapeCharacter(ReportingConstants.MYSQL_SINGLE_WILD_CARD, sb, 0);
            substituteWildCard(ReportingConstants.EMS_MULTIPLE_WILD_CARD, ReportingConstants.MYSQL_MULTIPLE_WILD_CARD, sb, 0);
            substituteWildCard(ReportingConstants.EMS_SINGLE_WILD_CARD, ReportingConstants.MYSQL_SINGLE_WILD_CARD, sb, 0);
            value = sb.toString();
        }
        return value;
    }
    
    private void addEscapeCharacter(final String searchValue, final StringBuffer sb, final int startIndex) {
        final int index = sb.indexOf(searchValue, startIndex);
        if (index >= 0) {
            sb.insert(index, ReportingConstants.ESCAPE_CHARACTER);
            
            // calculate new start position
            final int newIndex = index + searchValue.length() + 1;
            if (newIndex < sb.length()) {
                addEscapeCharacter(searchValue, sb, newIndex);
            }
        }
    }
    
    private void substituteWildCard(final String wildCardValue, final String replaceWildCardValue, final StringBuffer sb, final int startIndex) {
        final int index = sb.indexOf(wildCardValue, startIndex);
        if (index >= 0) {
            sb.replace(index, index + wildCardValue.length(), replaceWildCardValue);
            
            // calculate new start position
            final int newIndex = index + replaceWildCardValue.length();
            if (newIndex < sb.length()) {
                substituteWildCard(wildCardValue, replaceWildCardValue, sb, newIndex);
            }
        }
    }
    
    protected final String createReportParameterDateString(final Date startDate, final Date endDate) {
        final StringBuffer buffer = new StringBuffer();
        
        buffer.append(DateUtil.createDateString(new SimpleDateFormat(DateUtil.REPORTS_OUTPUT_FORMAT_NO_TIMEZONE), this.timeZone, startDate));
        buffer.append(" to ");
        buffer.append(DateUtil.createDateString(new SimpleDateFormat(DateUtil.REPORTS_OUTPUT_FORMAT), this.timeZone, endDate));
        return buffer.toString();
    }
    
    public final Map<Integer, List<Integer>> getAllDuplicatedPointOfSaleRouteMap(final int customerId, final String[] selectedPointOfSaleRouteIds)
        throws DataAccessException {
        final Map<Integer, List<Integer>> resultMap = new HashMap<Integer, List<Integer>>();
        
        final List<PointOfSale> posList = this.pointOfSaleService.findPointOfSalesByCustomerId(customerId);
        
        for (PointOfSale pos : posList) {
            final List<RoutePOS> routePOSList = this.routeService.findRoutePOSesByPointOfSaleId(pos.getId());
            
            // if the paystation belongs to more than one group
            if (routePOSList.size() > 1) {
                // remove the first group
                routePOSList.remove(0);
                // create map for the rest groups
                final List<Integer> routeIds = new ArrayList<Integer>();
                for (RoutePOS routePOS : routePOSList) {
                    routeIds.add(routePOS.getRoute().getId());
                }
                
                resultMap.put(pos.getId(), routeIds);
            }
        }
        return resultMap;
    }
    
    protected final void getFromHiddenPointOfSales(final StringBuilder query, final String tableName) {
        if (this.reportDefinition.isIsOnlyVisible()) {
            query.append(" INNER JOIN POSStatus ps ON ");
            query.append(tableName);
            if (POS.equals(tableName)) {
                query.append(".Id");
            } else {
                query.append(".PointOfSaleId");
            }
            query.append("= ps.PointOfSaleId");
        }
    }
    
    protected final void getWhereHiddenPointOfSales(final StringBuilder query) {
        getWhereHiddenPointOfSales(query, "ps");
    }
    
    protected final void getWhereHiddenPointOfSales(final StringBuilder query, final String tableName) {
        if (this.reportDefinition.isIsOnlyVisible()) {
            query.append(SPACE_AND_SPACE);
            query.append(tableName);
            query.append(".IsVisible=1");
            
            if (this.reportDefinition.isIsAdhoc()) {
                query.append(SPACE_AND_SPACE);
                query.append(tableName);
                query.append(".IsActivated=1");
            }
        }
    }
    
    protected final String[] getRawSelectedGroupIds(final List<ReportLocationValue> selectedList) {
        // find out all the selected group ids
        final List<String> rIdList = new ArrayList<String>();
        for (ReportLocationValue selected : selectedList) {
            // Group selected
            if (selected.getObjectType().equals(Route.class.getName())) {
                final int routeId = selected.getObjectId();
                if (!rIdList.contains(Integer.toString(routeId))) {
                    rIdList.add(Integer.toString(routeId));
                }
            }
        }
        final String[] rIds = new String[rIdList.size()];
        return rIdList.toArray(rIds);
    }
    
    protected final int getTimeZoneOffset() {
        TimeZone timezone = TimeZone.getTimeZone(this.timeZone);
        if (timezone == null) {
            timezone = TimeZone.getDefault();
        }
        
        Date datetime = null;
        if (this.reportQueue.getPrimaryDateRangeBeginGmt() != null) {
            datetime = this.reportQueue.getPrimaryDateRangeBeginGmt();
        } else {
            datetime = this.reportQueue.getExecutionBeginGmt();
        }
        
        final int offsetHour = timezone.getOffset(datetime.getTime()) / StandardConstants.SECONDS_IN_MILLIS_1 / StandardConstants.MINUTES_IN_SECS_1
                               / StandardConstants.HOURS_IN_MINS_1;
        return offsetHour;
    }
    
    // CSV Report processing for Audit and Replenish 
    
    private byte[] createCSVReport(final String sqlQuery, final Connection connection, final boolean isPDF, final String tempReportsPathTemp,
        final String timeZoneTemp, final String countQuery, final int maxRecordCount) {
        ByteArrayOutputStream baos = null;
        Statement statement = null;
        try {
            
            final long start = System.currentTimeMillis();
            statement = connection.createStatement();
            boolean isTooManyRecords = false;
            
            if (maxRecordCount > ReportingConstants.REPORTS_DEFAULT_MAX_RECORDS && countQuery != null && !"".equals(countQuery)) {
                final ResultSet countResultSets = statement.executeQuery(countQuery);
                if (countResultSets.next()) {
                    final int recordCount = countResultSets.getInt(1);
                    if (recordCount > maxRecordCount) {
                        LOGGER.info(RECORD_COUNT + recordCount + IS_GREAT_MAX_REC + maxRecordCount);
                        this.statusExplanation =
                                this.reportingUtil.getPropertyEN(LabelConstants.REPORT_ERROR_TOO_LARGE,
                                                                 new String[] { String.valueOf(recordCount), String.valueOf(maxRecordCount) });
                        isTooManyRecords = true;
                    } else {
                        LOGGER.debug(RECORD_COUNT + recordCount + LESS_THAN_MAX_ALLOWED_RECS + maxRecordCount);
                    }
                }
                countResultSets.close();
            }
            
            if (!isTooManyRecords) {
                baos = new ByteArrayOutputStream();
                
                // * Export report
                exportReport(this.reportDefinition.getReportType().getId(), sqlQuery, baos, connection);
                LOGGER.info("CSV export took " + (System.currentTimeMillis() - start) + MILLISECONDS);
            }
        } catch (Exception e) {
            LOGGER.warn(ERROR_GEN_REP, e);
            this.statusExplanation = this.reportingUtil.getPropertyEN(LabelConstants.REPORT_ERROR_PROPERTY);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException sqle) {
                    LOGGER.error("CSVReport, cannot close SQL connection, ", sqle);
                }
            }
            try {
                connection.close();
            } catch (SQLException e) {
                LOGGER.warn("Unable to close connection while executing audit report csv query", e);
            }
        }
        return (baos != null) ? baos.toByteArray() : null;
    }
    
    private void exportReport(final int reportType, final String query, final ByteArrayOutputStream baos, final Connection connection)
        throws Exception {
        // * Execute the query
        final Statement statement = connection.createStatement();
        //        statement.execute(query);
        //        ResultSet rs = statement.getResultSet();
        this.sqlExecutionBeginGMT = new Date();
        final ResultSet resultSet = statement.executeQuery(query);
        this.sqlExecutionEndGMT = new Date();
        
        switch (reportType) {
            case ReportingConstants.REPORT_TYPE_AUDIT_REPORTS:
                exportPs2AuditReport(resultSet, baos);
                return;
            case ReportingConstants.REPORT_TYPE_REPLENISH_REPORTS:
                exportPs2ReplenishReport(resultSet, baos);
                return;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_DELAY:
                exportPs2TransactionDelayReport(resultSet, baos);
                return;
            default:
                exportUnknownReport(resultSet, baos);
                return;
        }
        
    }
    
    private void exportPs2TransactionDelayReport(final ResultSet rs, final ByteArrayOutputStream baos)
        throws SQLException, IOException, JRScriptletException {
        baos.write(PS2_TRANSACTION_DELAY_REPORT_TITLE_ROW.getBytes());
        final ReportsScriptlet scriptlet = new ReportsScriptlet();
        // Write records to file
        while (rs.next()) {
            final StringBuffer buffer = new StringBuffer(ReportBaseThread.STRING_BUFFER_SIZE);
            buffer.append(StandardConstants.STRING_DOUBLE_QUOTES);
            buffer.append(rs.getString("PayStation"));
            buffer.append(StandardConstants.STRING_DOUBLE_QUOTES);
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(rs.getString(SERIAL_NUMBER));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(StandardConstants.STRING_DOUBLE_QUOTES);
            buffer.append(rs.getString("Location"));
            buffer.append(StandardConstants.STRING_DOUBLE_QUOTES);
            buffer.append(ReportingConstants.CSV_COMMA);
            
            int spaceNumber = -1;
            spaceNumber = rs.getInt(SPACE_NUMBER);
            if (spaceNumber >= 0) {
                buffer.append(spaceNumber);
            }
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(rs.getInt("TransactionNumber"));
            buffer.append(ReportingConstants.CSV_COMMA);
            
            final Timestamp purchaseDate = rs.getTimestamp("PurchaseDate");
            if (purchaseDate != null) {
                buffer.append(scriptlet.formatDate(this.timeZone, purchaseDate));
            }
            buffer.append(ReportingConstants.CSV_COMMA);
            
            final Timestamp expiryDate = rs.getTimestamp(EXPIRY_DATE);
            if (expiryDate != null) {
                buffer.append(scriptlet.formatDate(this.timeZone, expiryDate));
            }
            buffer.append(ReportingConstants.CSV_COMMA);
            
            final Timestamp purchaseCreatedGMT = rs.getTimestamp("PurchaseCreatedGMT");
            if (purchaseCreatedGMT != null) {
                buffer.append(scriptlet.formatDate(this.timeZone, purchaseCreatedGMT));
            }
            buffer.append(ReportingConstants.CSV_COMMA);
            
            buffer.append(rs.getInt("Delta"));
            buffer.append(ReportingConstants.CSV_COMMA);
            final String licencePlate = rs.getString("LicencePlate");
            if (licencePlate != null && !licencePlate.equals(NULL_STRING)) {
                buffer.append(licencePlate);
            }
            buffer.append('\n');
            
            // Write to file
            baos.write(buffer.toString().getBytes());
        }
        baos.close();
        rs.close();
    }
    
    private void exportPs2AuditReport(final ResultSet rs, final ByteArrayOutputStream baos) throws Exception {
        // Write title row to file
        baos.write(PS2_AUDIT_REPORT_TITLE_ROW.getBytes());
        final ReportsScriptlet scriptlet = new ReportsScriptlet();
        
        // Write records to file
        while (rs.next()) {
            final int paystationType = rs.getInt("PaystationType");
            // Calculate extra fields
            float collections;
            // PS1
            if (paystationType == 0) {
                collections = rs.getFloat(COIN_TOTAL_AMT) + rs.getFloat(BILL_TOTAL_AMT) + rs.getFloat(CARD_TOTAL_AMT);
            } else {
                collections = rs.getInt(ACCEPTED_FLOAT_AMT) + rs.getFloat(COIN_TOTAL_AMT) + rs.getFloat(BILL_TOTAL_AMT) + rs.getFloat(CARD_TOTAL_AMT)
                              - rs.getInt(OVER_FILL_AMT);
            }
            float revenue;
            if (paystationType == 0) {
                revenue = collections - rs.getFloat(SMART_CARD_RECHARGE_AMT) + rs.getFloat(SMART_CARD_AMT) + rs.getFloat(ATTENDANT_TICKETS_AMT)
                          - rs.getFloat(ATTENDANT_DEPOSIT_AMT) - rs.getFloat(REFUND_ISSUED_AMT) - rs.getFloat(CHANGE_ISSUED_AMT);
            } else {
                revenue = collections - rs.getFloat(SMART_CARD_RECHARGE_AMT) + rs.getFloat(SMART_CARD_AMT) - rs.getFloat(REFUND_ISSUED_AMT)
                          - rs.getFloat(CHANGE_ISSUED_AMT);
            }
            
            // Create report record
            final StringBuffer buffer = new StringBuffer(BUFFER_SIZE);
            buffer.append(rs.getInt("ReportNumber"));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(scriptlet.formatDate(this.timeZone, rs.getTimestamp("StartGMT")));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(scriptlet.formatDate(this.timeZone, rs.getTimestamp("EndGMT")));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(StandardConstants.STRING_DOUBLE_QUOTES);
            buffer.append(rs.getString(NAME));
            buffer.append(StandardConstants.STRING_DOUBLE_QUOTES);
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(rs.getString(SERIAL_NUMBER));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(rs.getInt("CalculatedEndTicketNumber"));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(rs.getInt("TicketsSold"));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(rs.getInt("PatrollerTicketsSold"));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT.format(rs.getInt(TUBE_1_TYPE) / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT.format(rs.getInt("Tube1Amount") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT.format(rs.getInt(TUBE_2_TYPE) / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT.format(rs.getInt("Tube2Amount") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT.format(rs.getInt(TUBE_3_TYPE) / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT.format(rs.getInt("Tube3Amount") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT.format(rs.getInt(TUBE_4_TYPE) / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT.format(rs.getInt("Tube4Amount") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getInt("TubeTotalAmount") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getInt("ReplenishedAmount") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT.format(rs.getInt(OVER_FILL_AMT) / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getInt(ACCEPTED_FLOAT_AMT) / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getInt("ChangerDispensed") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getInt("TestDispensedChanger") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT.format(rs.getInt("Hopper1Type") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getInt("Hopper1Current") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT.format(rs.getInt("Hopper2Type") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getInt("Hopper2Current") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getInt("HopperTotalCurrent") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getInt("Hopper1Replenished") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getInt("Hopper2Replenished") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getInt("Hopper1Dispensed") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getInt("Hopper2Dispensed") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getInt("TestDispensedHopper1") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getInt("TestDispensedHopper2") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getFloat(COIN_TOTAL_AMT) / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getFloat("BillAmount1") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getFloat("BillAmount2") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getFloat("BillAmount5") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getFloat("BillAmount10") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getFloat("BillAmount20") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getFloat("BillAmount50") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getFloat(BILL_TOTAL_AMT) / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT.format(rs.getFloat("VisaAmount") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getFloat("MasterCardAmount") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT.format(rs.getFloat("AmexAmount") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getFloat("DiscoverAmount") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getFloat("DinersAmount") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getFloat("ValueCardAmount") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getFloat(CARD_TOTAL_AMT) / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getFloat(SMART_CARD_AMT) / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getFloat(SMART_CARD_RECHARGE_AMT) / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            //TODO To be added when Citations are implemented
            //            buffer.append(rs.getFloat("CitationsPaid"));
            //            buffer.append(ReportingConstants.CSV_COMMA);
            //            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT.format(rs.getFloat("CitationAmount") 
            //                                                          / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            //            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(rs.getFloat("AttendantTicketsSold"));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getFloat(ATTENDANT_TICKETS_AMT) / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getFloat(ATTENDANT_DEPOSIT_AMT) / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getFloat("ExcessPaymentAmount") / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT.format(collections / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getFloat(CHANGE_ISSUED_AMT) / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT
                    .format(rs.getFloat(REFUND_ISSUED_AMT) / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(ReportingConstants.CSV_DECIMAL_FORMAT.format(revenue / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1));
            buffer.append(ReportingConstants.CSV_COMMA);
            
            String userName = rs.getString(COLLECTION_USER_NAME);
            if (userName != null) {
                try {
                    userName = URLDecoder.decode(rs.getString(COLLECTION_USER_NAME), WebSecurityConstants.URL_ENCODING_LATIN1);
                } catch (UnsupportedEncodingException uee) {
                    //Do nothing userName already has undecoded username
                }
            }
            
            buffer.append(userName);
            buffer.append('\n');
            
            // Write to file
            baos.write(buffer.toString().getBytes());
        }
        baos.close();
        rs.close();
    }
    
    private void exportPs2ReplenishReport(final ResultSet rs, final ByteArrayOutputStream baos) throws Exception {
        // Write title row to file
        baos.write(PS2_REPLENISH_REPORT_TITLE_ROW.getBytes());
        final ReportsScriptlet scriptlet = new ReportsScriptlet();
        
        while (rs.next()) {
            final int typeId = rs.getInt("ReplenishTypeId");
            
            // Create report record
            final StringBuffer buffer = new StringBuffer(BUFFER_SIZE);
            buffer.append(scriptlet.formatDate(this.timeZone, rs.getTimestamp("ReplenishGMT")));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(StandardConstants.STRING_DOUBLE_QUOTES);
            buffer.append(rs.getString(NAME));
            buffer.append(StandardConstants.STRING_DOUBLE_QUOTES);
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(rs.getInt("Number"));
            buffer.append(ReportingConstants.CSV_COMMA);
            buffer.append(this.replenishTypeService.getText(typeId));
            switch (typeId) {
                case ReportingConstants.REPLENISH_TYPE_CHARGER_ADD:
                    addCoinChangerReplenishFields(rs, buffer);
                    break;
                case ReportingConstants.REPLENISH_TYPE_HOPPER_ADD:
                    addCoinHopperReplenishFields(rs, buffer);
                    break;
                case ReportingConstants.REPLENISH_TYPE_CHARGER_TEST:
                    addCoinChangerReplenishFields(rs, buffer);
                    break;
                case ReportingConstants.REPLENISH_TYPE_HOPPER_TEST:
                    addCoinHopperReplenishFields(rs, buffer);
                    break;
                case ReportingConstants.REPLENISH_TYPE_HOPPER_REMOVAL:
                    break;
                case ReportingConstants.REPLENISH_TYPE_CHARGER_PRE_AUTH_SET:
                    addCoinChangerReplenishFields(rs, buffer);
                    break;
                case ReportingConstants.REPLENISH_TYPE_CHARGER_AUTO_SET:
                    addCoinChangerReplenishFields(rs, buffer);
                    break;
                default:
                    break;
            }
            buffer.append('\n');
            
            // Write to file
            baos.write(buffer.toString().getBytes());
        }
        baos.close();
        rs.close();
    }
    
    private void addCoinChangerReplenishFields(final ResultSet rs, final StringBuffer buffer) throws SQLException {
        // Calculate extra changer fields
        final int tube1AddedAmount = rs.getInt(TUBE_1_TYPE) * rs.getInt(TUBE1_CHANGED_CNT);
        final int tube2AddedAmount = rs.getInt(TUBE_2_TYPE) * rs.getInt(TUBE2_CHANGED_CNT);
        final int tube3AddedAmount = rs.getInt(TUBE_3_TYPE) * rs.getInt(TUBE3_CHANGED_CNT);
        final int tube4AddedAmount = rs.getInt(TUBE_4_TYPE) * rs.getInt(TUBE4_CHANGED_CNT);
        final int tubeTotalAddedAmount = tube1AddedAmount + tube2AddedAmount + tube3AddedAmount + tube4AddedAmount;
        final int tube1CurrentAmount = rs.getInt(TUBE_1_TYPE) * rs.getInt(TUBE1_CURRENT_CNT);
        final int tube2CurrentAmount = rs.getInt(TUBE_2_TYPE) * rs.getInt(TUBE2_CURRENT_CNT);
        final int tube3CurrentAmount = rs.getInt(TUBE_3_TYPE) * rs.getInt(TUBE3_CURRENT_CNT);
        final int tube4CurrentAmount = rs.getInt(TUBE_4_TYPE) * rs.getInt(TUBE4_CURRENT_CNT);
        final int tubeTotalCurrentAmount = tube1CurrentAmount + tube2CurrentAmount + tube3CurrentAmount + tube4CurrentAmount;
        final float coinbag005AddedAmount = 0.05f * rs.getInt("CoinBag005AddedCount");
        final float coinbag010AddedAmount = 0.10f * rs.getInt("CoinBag010AddedCount");
        final float coinbag025AddedAmount = 0.25f * rs.getInt("CoinBag025AddedCount");
        final int coinbag100AddedAmount = 1 * rs.getInt("CoinBag100AddedCount");
        final int coinbag200AddedAmount = 2 * rs.getInt("CoinBag200AddedCount");
        final float coinbagTotalAddedAmount =
                coinbag005AddedAmount + coinbag010AddedAmount + coinbag025AddedAmount + coinbag100AddedAmount + coinbag200AddedAmount;
        
        // Changer Fields
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(rs.getInt(TUBE_1_TYPE) / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(rs.getInt(TUBE1_CHANGED_CNT));
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(tube1AddedAmount / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(rs.getInt(TUBE1_CURRENT_CNT));
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(tube1CurrentAmount / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(rs.getInt(TUBE_2_TYPE) / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(rs.getInt(TUBE2_CHANGED_CNT));
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(tube2AddedAmount / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(rs.getInt(TUBE2_CURRENT_CNT));
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(tube2CurrentAmount / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(rs.getInt(TUBE_3_TYPE) / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(rs.getInt(TUBE3_CHANGED_CNT));
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(tube3AddedAmount / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(rs.getInt(TUBE3_CURRENT_CNT));
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(tube3CurrentAmount / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(rs.getInt(TUBE_4_TYPE) / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(rs.getInt(TUBE4_CHANGED_CNT));
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(tube4AddedAmount / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(rs.getInt(TUBE4_CURRENT_CNT));
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(tube4CurrentAmount / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(tubeTotalAddedAmount / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(tubeTotalCurrentAmount / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(coinbag005AddedAmount);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(coinbag010AddedAmount);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(coinbag025AddedAmount);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(coinbag100AddedAmount);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(coinbag200AddedAmount);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(coinbagTotalAddedAmount);
        
        // Hopper Fields
        buffer.append(",0,0,0,0,0,0,0,0,0,0,0,0");
    }
    
    private void addCoinHopperReplenishFields(final ResultSet rs, final StringBuffer buffer) throws SQLException {
        // Changer Fields
        buffer.append(",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0");
        
        // Hopper Fields
        // Calculate extra hopper fields (tube1 = hopper1, tube2 = hopper2)
        final int hopper1AddedAmount = rs.getInt(TUBE_1_TYPE) * rs.getInt(TUBE1_CHANGED_CNT);
        final int hopper2AddedAmount = rs.getInt(TUBE_2_TYPE) * rs.getInt(TUBE2_CHANGED_CNT);
        final int hopperTotalAddedAmount = hopper1AddedAmount + hopper2AddedAmount;
        final int hopper1CurrentAmount = rs.getInt(TUBE_1_TYPE) * rs.getInt(TUBE1_CURRENT_CNT);
        final int hopper2CurrentAmount = rs.getInt(TUBE_2_TYPE) * rs.getInt(TUBE2_CURRENT_CNT);
        final int hopperTotalCurrentAmount = hopper1CurrentAmount + hopper2CurrentAmount;
        
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(rs.getInt(TUBE_1_TYPE) / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(rs.getInt(TUBE1_CHANGED_CNT));
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(hopper1AddedAmount / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(rs.getInt(TUBE1_CURRENT_CNT));
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(hopper1CurrentAmount / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(rs.getInt(TUBE_2_TYPE) / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(rs.getInt(TUBE2_CHANGED_CNT));
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(hopper2AddedAmount / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(rs.getInt(TUBE2_CURRENT_CNT));
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(hopper2CurrentAmount / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(hopperTotalAddedAmount / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
        buffer.append(ReportingConstants.CSV_COMMA);
        buffer.append(hopperTotalCurrentAmount / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
    }
    
    private void exportUnknownReport(final ResultSet rs, final ByteArrayOutputStream baos) throws Exception {
        StringBuffer buffer = null;
        
        // Write title row to file
        final ResultSetMetaData rsmd = rs.getMetaData();
        for (int i = 1; i <= rsmd.getColumnCount(); i++) {
            buffer = new StringBuffer(BUFFER_SIZE);
            buffer.append(rsmd.getColumnName(i));
            if (i + 1 < rsmd.getColumnCount()) {
                buffer.append(ReportingConstants.CSV_COMMA);
            } else {
                buffer.append('\n');
            }
        }
        baos.write(buffer.toString().getBytes());
        
        // Write records to file
        while (rs.next()) {
            for (int i = 0; i < rsmd.getColumnCount(); i++) {
                // Create report record
                buffer = new StringBuffer(BUFFER_SIZE);
                buffer.append(rsmd.getColumnName(i));
                if (i + 1 < rsmd.getColumnCount()) {
                    buffer.append(ReportingConstants.CSV_COMMA);
                } else {
                    buffer.append('\n');
                }
            }
            // Write to file
            baos.write(buffer.toString().getBytes());
        }
        baos.close();
    }
    
    protected final String getTitle(final String title) {
        return title + " " + DateUtil.createDateString(new SimpleDateFormat(DateUtil.REPORTS_DATE_FORMAT_ON_FOOTER), this.timeZone,
                                                       this.reportQueue.getScheduledBeginGmt());
    }
    
    /**
     * @param typeId
     *            Return '-1' if typeId is null
     * @return int Return '-1' if typeId is null
     */
    protected final int getFilterTypeId(final Integer typeId) {
        int filterTypeId = DEFAULT;
        if (typeId != null) {
            filterTypeId = typeId.intValue();
        }
        return filterTypeId;
    }
    
    public class RateSummaryReportDataInfo implements JRDataSourceEventHandler {
        // index of the max count for each row
        private int[] maxCountForRows;
        private int maxRowTotalCount = -1;
        private int maxColumnTotalCount = -1;
        private int maxRowTotalAmount = -1;
        private int maxColumnTotalAmount = -1;
        private int currRow = -1;
        
        public RateSummaryReportDataInfo() {
            
        }
        
        public final void onInitialize(final ResultSet rs) {
            if (rs != null) {
                try {
                    rs.last();
                    final int size = rs.getRow();
                    
                    if (size > 0) {
                        final int[] columnTotalCounts = new int[COLUMN_TOTAL_SIZE];
                        final int[] columnTotalAmounts = new int[COLUMN_TOTAL_SIZE];
                        
                        rs.beforeFirst();
                        this.maxCountForRows = new int[size];
                        
                        for (int i = 0; i < COLUMN_TOTAL_SIZE; i++) {
                            columnTotalCounts[i] = 0;
                            columnTotalAmounts[i] = 0;
                        }
                        
                        for (int i = 0; i < size; i++) {
                            this.maxCountForRows[i] = -1;
                        }
                        
                        int rowNum = -1;
                        while (rs.next()) {
                            rowNum = rowNum + 1;
                            int rowCount = 0;
                            int rowAmount = 0;
                            for (int i = 0; i < COLUMN_TOTAL_SIZE; i++) {
                                final String countColumnName = "Count" + i;
                                final String amountColumnName = "Amount" + i;
                                final int count = rs.getInt(countColumnName);
                                final int amount = rs.getInt(amountColumnName);
                                rowCount = rowCount + count;
                                rowAmount = rowAmount + amount;
                                columnTotalCounts[i] = columnTotalCounts[i] + count;
                                columnTotalAmounts[i] = columnTotalAmounts[i] + amount;
                                if (count > this.maxCountForRows[rowNum]) {
                                    this.maxCountForRows[rowNum] = count;
                                }
                            }
                            if (rowCount > this.maxRowTotalCount) {
                                this.maxRowTotalCount = rowCount;
                            }
                            if (rowAmount > this.maxRowTotalAmount) {
                                this.maxRowTotalAmount = rowAmount;
                            }
                        }
                        
                        for (int i = 0; i < COLUMN_TOTAL_SIZE; i++) {
                            if (columnTotalCounts[i] > this.maxColumnTotalCount) {
                                this.maxColumnTotalCount = columnTotalCounts[i];
                            }
                            if (columnTotalAmounts[i] > this.maxColumnTotalAmount) {
                                this.maxColumnTotalAmount = columnTotalAmounts[i];
                            }
                        }
                    }
                    rs.beforeFirst();
                } catch (SQLException e) {
                    LOGGER.error("Error generating rate summary report", e);
                }
            }
        }
        
        public final void onNextRow() {
            this.currRow++;
        }
        
        public final boolean isMaxCountForCurrRow(final int count) {
            return count >= this.maxCountForRows[this.currRow];
        }
        
        public final boolean isMaxRowTotalCount(final int rowTotalCount) {
            return rowTotalCount >= this.maxRowTotalCount;
        }
        
        public final boolean isMaxRowTotalAmount(final int rowTotalAmount) {
            return rowTotalAmount >= this.maxRowTotalAmount;
        }
        
        public final boolean isMaxTotalCountForHour(final int totalCountForHour) {
            return totalCountForHour >= this.maxColumnTotalCount;
        }
        
        public final boolean isMaxTotalAmountForHour(final int totalAmountForHour) {
            return totalAmountForHour >= this.maxColumnTotalAmount;
        }
    }
    
}