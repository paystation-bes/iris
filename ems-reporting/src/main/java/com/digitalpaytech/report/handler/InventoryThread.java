package com.digitalpaytech.report.handler;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

/**
 * @author Amanda Chong
 */

public class InventoryThread extends ReportBaseThread {
    
    private static final Logger LOGGER = Logger.getLogger(InventoryThread.class);
    private static final String ORGANIZATION = "OrganizationName";
    
    private static final String SQL_SELECT = "SELECT c.Name AS OrganizationName," + " pos.Name AS PsName, pos.SerialNumber as PsSerial,"
                                             + " pss.PaystationSettingName AS ConfigurationSettingName, l.Name AS LocationName,"
                                             + " phb.LastHeartbeatGMT AS LastSeenTime,"
                                             + " pss.PrimaryVersion AS SoftwareVersion, pss.SecondaryVersion AS FirmwareVersion,"
                                             + " csStdReports.IsEnabled AS StandardReports," + " csAlerts.IsEnabled AS Alerts,"
                                             + " csCCProcess.IsEnabled AS RealTimeCreditCardProcessing,"
                                             + " csBatchCCProcess.IsEnabled AS BatchCreditCardProcessing,"
                                             + " csCampusProcess.IsEnabled AS RealTimeCampusCardProcessing," + " csCoupons.IsEnabled AS Coupons,"
                                             + " csPasscards.IsEnabled AS Passcards," + " csSmartcards.IsEnabled AS SmartCards,"
                                             + " csExtendByPhone.IsEnabled AS ExtendByPhone," + " csDigitalRead.IsEnabled AS DigitalAPIRead,"
                                             + " csDigitalWrite.IsEnabled AS DigitalAPIWrite,"
                                             + " csPayByCell.IsEnabled AS 3rdPartyPayByCellIntegration,"
                                             + " csMobileDigitalCollect.IsEnabled AS MobileDigitalCollect,"
                                             + " csOnlinePSConfig.IsEnabled AS OnlinePSConfiguration,"
                                             + " csFlexIntegration.IsEnabled AS FlexIntegration,"
                                             + " csAutoCountIntegration.IsEnabled AS AutoCountIntegration,"
                                             + " csDigitalApiXChange.IsEnabled AS DigitalApiXChange,"
                                             + " csOnlineRateConfig.IsEnabled AS OnlineRateConfiguration," 
                                             + " IF(ma.TerminalName IS NULL OR ma.TerminalName = '', ma.Name, ma.TerminalName) AS MerchantAccount, "
                                             + " pro.Name as MerchantProcessor," + " posss.UpgradeGMT AS UpgradeDate";
    
    private static final String SQL_SELECT_MODEM_SETTINGS =
            ", modemType.Name AS ModemType," + " accessPoint.Name AS AccessPointName," + " modemSet.CCID AS CCID," + " modemSet.MEID AS MEID,"
                                                            + " modemSet.HardwareManufacturer AS HardwareManufacturer,"
                                                            + " modemSet.HardwareModel AS HardwareModel,"
                                                            + " modemSet.HardwareFirmware AS HardwareFirmware";
    
    private static final String SQL_SELECT_EMV_TELEMETRY_DATA =
            ", EMVMA.Field1 As TerminalId," + "TelModel.Value AS TelemetryModel," + "TelSerialNumber.Value As TelemetrySerialNumber,"
                                                                + "TelFirmVer.Value As TelemetryFirmwareVersion,"
                                                                + "TelViolationSts.Value AS TelemetryTamperStatus,"
                                                                + "TelPairingStatus.Value AS TelemetryPairingStatus,"
                                                                + "CLTelModel.Value AS CLTelemetryModel,"
                                                                + "CLTelSerialNumber.Value As CLTelemetrySerialNumber,"
                                                                + "CLTelFirmVer.Value As CLTelemetryFirmwareVersion,"
                                                                + "CLTelViolationSts.Value AS CLTelemetryTamperStatus,"
                                                                + "CLTelPairingStatus.Value AS CLTelemetryPairingStatus";
    
    // For Pay station inventory report.
    private static final String SQL_SELECT_ROUTE_CSV =
            ", (SELECT GROUP_CONCAT(DISTINCT r2.name ORDER BY r2.Name)" + " FROM RoutePOS rpos2, Route r2, PointOfSale pos2"
                                                       + " WHERE r2.Id = rpos2.RouteId AND rpos2.PointOfSaleId = pos2.Id AND pos2.Id IN (pos.Id)"
                                                       + " GROUP BY pos2.Id) AS Route";
    
    private static final String SQL_SELECT_NOGROUP = ", 'NoGroup' AS GroupField";
    
    //TODO Hard to maintain with new subscriptiontypes
    private static final String SQL_FROM =
            " FROM PointOfSale pos INNER JOIN Location l ON pos.LocationId = l.Id" + " INNER JOIN Customer c ON pos.CustomerId = c.Id"
                                           + " INNER JOIN POSServiceState pss ON pos.Id = pss.PointOfSaleId"
                                           + " INNER JOIN POSHeartbeat phb ON pos.Id = phb.PointOfSaleId"
                                           + " INNER JOIN POSStatus ps ON ps.PointOfSaleId = pos.Id"
                                           + " INNER JOIN CustomerSubscription csStdReports "
                                           + "ON(csStdReports.CustomerId = c.Id AND csStdReports.SubscriptionTypeId = "
                                           + WebCoreConstants.SUBSCRIPTION_TYPE_STANDARD_REPORT + StandardConstants.STRING_CLOSE_PARENTHESIS
                                           + " INNER JOIN CustomerSubscription csAlerts ON(csAlerts.CustomerId = c.Id AND csAlerts.SubscriptionTypeId = "
                                           + WebCoreConstants.SUBSCRIPTION_TYPE_ALERTS + StandardConstants.STRING_CLOSE_PARENTHESIS
                                           + " INNER JOIN CustomerSubscription csCCProcess "
                                           + "ON(csCCProcess.CustomerId = c.Id AND csCCProcess.SubscriptionTypeId = "
                                           + WebCoreConstants.SUBSCRIPTION_TYPE_REALTIME_CC_PROCESSING + StandardConstants.STRING_CLOSE_PARENTHESIS
                                           + " INNER JOIN CustomerSubscription csBatchCCProcess "
                                           + "ON(csBatchCCProcess.CustomerId = c.Id AND csBatchCCProcess.SubscriptionTypeId = "
                                           + WebCoreConstants.SUBSCRIPTION_TYPE_BATCH_CC_PROCESSING + StandardConstants.STRING_CLOSE_PARENTHESIS
                                           + " INNER JOIN CustomerSubscription csCampusProcess "
                                           + "ON(csCampusProcess.CustomerId = c.Id AND csCampusProcess.SubscriptionTypeId = "
                                           + WebCoreConstants.SUBSCRIPTION_TYPE_REALTIME_CAMPUS_CARD_PROCESSING
                                           + StandardConstants.STRING_CLOSE_PARENTHESIS + " INNER JOIN CustomerSubscription csCoupons "
                                           + "ON(csCoupons.CustomerId = c.Id AND csCoupons.SubscriptionTypeId = "
                                           + WebCoreConstants.SUBSCRIPTION_TYPE_COUPONS + StandardConstants.STRING_CLOSE_PARENTHESIS
                                           + " INNER JOIN CustomerSubscription csPasscards "
                                           + "ON(csPasscards.CustomerId = c.Id AND csPasscards.SubscriptionTypeId = "
                                           + WebCoreConstants.SUBSCRIPTION_TYPE_PASSCARDS + StandardConstants.STRING_CLOSE_PARENTHESIS
                                           + " INNER JOIN CustomerSubscription csSmartcards "
                                           + "ON(csSmartcards.CustomerId = c.Id AND csSmartcards.SubscriptionTypeId = "
                                           + WebCoreConstants.SUBSCRIPTION_TYPE_SMART_CARDS + StandardConstants.STRING_CLOSE_PARENTHESIS
                                           + " INNER JOIN CustomerSubscription csExtendByPhone "
                                           + "ON(csExtendByPhone.CustomerId = c.Id AND csExtendByPhone.SubscriptionTypeId = "
                                           + WebCoreConstants.SUBSCRIPTION_TYPE_EXTEND_BY_PHONE + StandardConstants.STRING_CLOSE_PARENTHESIS
                                           + " INNER JOIN CustomerSubscription csDigitalRead "
                                           + "ON(csDigitalRead.CustomerId = c.Id AND csDigitalRead.SubscriptionTypeId = "
                                           + WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_API_READ + StandardConstants.STRING_CLOSE_PARENTHESIS
                                           + " INNER JOIN CustomerSubscription csDigitalWrite "
                                           + "ON(csDigitalWrite.CustomerId = c.Id AND csDigitalWrite.SubscriptionTypeId = "
                                           + WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_API_WRITE + StandardConstants.STRING_CLOSE_PARENTHESIS
                                           + " INNER JOIN CustomerSubscription csPayByCell "
                                           + "ON(csPayByCell.CustomerId = c.Id AND csPayByCell.SubscriptionTypeId = "
                                           + WebCoreConstants.SUBSCRIPTION_TYPE_3RD_PARTY_PAY_BY_CELL + StandardConstants.STRING_CLOSE_PARENTHESIS
                                           + " INNER JOIN CustomerSubscription csMobileDigitalCollect "
                                           + "ON(csMobileDigitalCollect.CustomerId = c.Id AND csMobileDigitalCollect.SubscriptionTypeId = "
                                           + WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_COLLECT + StandardConstants.STRING_CLOSE_PARENTHESIS
                                           + " INNER JOIN CustomerSubscription csOnlinePSConfig "
                                           + "ON(csOnlinePSConfig.CustomerId = c.Id AND csOnlinePSConfig.SubscriptionTypeId = "
                                           + WebCoreConstants.SUBSCRIPTION_TYPE_ONLINE_PAYSTATION_CONFIGURATION
                                           + StandardConstants.STRING_CLOSE_PARENTHESIS + " INNER JOIN CustomerSubscription csFlexIntegration "
                                           + "ON(csFlexIntegration.CustomerId = c.Id AND csFlexIntegration.SubscriptionTypeId = "
                                           + WebCoreConstants.SUBSCRIPTION_TYPE_FLEX_INTEGRATION + StandardConstants.STRING_CLOSE_PARENTHESIS
                                           + " INNER JOIN CustomerSubscription csAutoCountIntegration "
                                           + "ON(csAutoCountIntegration.CustomerId = c.Id AND csAutoCountIntegration.SubscriptionTypeId = "
                                           + WebCoreConstants.SUBSCRIPTION_TYPE_CASE_INTEGRATION + StandardConstants.STRING_CLOSE_PARENTHESIS
                                           + " INNER JOIN CustomerSubscription csDigitalApiXChange "
                                           + "ON(csDigitalApiXChange.CustomerId = c.Id AND csDigitalApiXChange.SubscriptionTypeId = "
                                           + WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_API_XCHANGE + StandardConstants.STRING_CLOSE_PARENTHESIS
                                           + " INNER JOIN CustomerSubscription csOnlineRateConfig "
                                           + "ON(csOnlineRateConfig.CustomerId = c.Id AND csOnlineRateConfig.SubscriptionTypeId = "
                                           + WebCoreConstants.SUBSCRIPTION_TYPE_ONLINE_RATE_CONFIGURATION + StandardConstants.STRING_CLOSE_PARENTHESIS
                                           + " LEFT JOIN MerchantPOS mpos ON (pos.Id = mpos.PointOfSaleId AND mpos.IsDeleted = 0)"
                                           + " LEFT JOIN MerchantAccount ma ON mpos.MerchantAccountId = ma.Id"
                                           + " LEFT JOIN Processor pro ON ma.ProcessorId = pro.Id"
                                           + " INNER JOIN POSServiceState posss ON pos.Id = posss.PointOfSaleId ";
    
    private static final String SQL_FROM_MODEM_DATA = " LEFT JOIN ModemSetting modemSet ON modemSet.PointOfSaleId  = pos.Id"
                                                      + " LEFT JOIN ModemType modemType ON modemSet.ModemTypeId = modemType.Id"
                                                      + " LEFT JOIN AccessPoint accessPoint ON modemSet.AccessPointId = accessPoint.Id";
    
    @SuppressWarnings("checkstyle:linelength")
    private static final String SQL_FROM_EMV_TELEMETRY_DATA =
            " LEFT JOIN POSTelemetry TelSerialNumber ON (TelSerialNumber.PointofSaleId = pos.Id AND TelSerialNumber.TelemetryTypeId = (SELECT Id FROM TelemetryType WHERE `Name` = 'SerialNumber') AND TelSerialNumber.IsHistory = 0)"
                                                              + " LEFT JOIN POSTelemetry TelModel ON (TelModel.PointofSaleId = pos.Id AND TelModel.TelemetryTypeId = (SELECT Id FROM TelemetryType WHERE `Name` = 'Model') AND TelModel.IsHistory = 0)"
                                                              + " LEFT JOIN POSTelemetry TelFirmVer ON (TelFirmVer.PointofSaleId = pos.Id AND TelFirmVer.TelemetryTypeId = (SELECT Id FROM TelemetryType WHERE `Name` = 'FirmwareVersion') AND TelFirmVer.IsHistory = 0)"
                                                              + " LEFT JOIN POSTelemetry TelViolationSts ON (TelViolationSts.PointofSaleId = pos.Id AND TelViolationSts.TelemetryTypeId = (SELECT Id FROM TelemetryType WHERE `Name` = 'ViolationStatus') AND TelViolationSts.IsHistory = 0)"
                                                              + " LEFT JOIN POSTelemetry TelPairingStatus ON (TelPairingStatus.PointofSaleId = pos.Id AND TelPairingStatus.TelemetryTypeId = (SELECT Id FROM TelemetryType WHERE `Name` = 'Pairing Status') AND TelPairingStatus.IsHistory = 0)"
                                                              + " LEFT JOIN POSTelemetry CLTelSerialNumber ON (CLTelSerialNumber.PointofSaleId = pos.Id AND CLTelSerialNumber.TelemetryTypeId = (SELECT Id FROM TelemetryType WHERE `Name` = 'Contactless Serial') AND CLTelSerialNumber.IsHistory = 0)"
                                                              + " LEFT JOIN POSTelemetry CLTelModel ON (CLTelModel.PointofSaleId = pos.Id AND CLTelModel.TelemetryTypeId = (SELECT Id FROM TelemetryType WHERE `Name` = 'Contactless Model') AND CLTelModel.IsHistory = 0)"
                                                              + " LEFT JOIN POSTelemetry CLTelFirmVer ON (CLTelFirmVer.PointofSaleId = pos.Id AND CLTelFirmVer.TelemetryTypeId = (SELECT Id FROM TelemetryType WHERE `Name` = 'Contactless Firmware') AND CLTelFirmVer.IsHistory = 0)"
                                                              + " LEFT JOIN POSTelemetry CLTelViolationSts ON (CLTelViolationSts.PointofSaleId = pos.Id AND CLTelViolationSts.TelemetryTypeId = (SELECT Id FROM TelemetryType WHERE `Name` = 'Contactless Violation Status') AND CLTelViolationSts.IsHistory = 0)"
                                                              + " LEFT JOIN POSTelemetry CLTelPairingStatus ON (CLTelPairingStatus.PointofSaleId = pos.Id AND CLTelPairingStatus.TelemetryTypeId = (SELECT Id FROM TelemetryType WHERE `Name` = 'Contactless Pairing Status') AND CLTelPairingStatus.IsHistory = 0)"
                                                              + " LEFT JOIN MerchantAccount EMVMA ON (mpos.MerchantAccountId = EMVMA.Id AND EMVMA.ProcessorId = pro.Id AND pro.IsEMV = 1) ";
    
    private static final String SQL_WHERE = " WHERE ";
    private static final String SQL_WHERE_SYSTEMADMIN = " c.IsParent = 0";
    private static final String SQL_WHERE_ISPARENT = " AND c.IsParent = 0";
    private static final String SQL_ORDER_BY_PDF = " ORDER BY GroupField, pos.Name, pos.SerialNumber";
    private static final String SQL_ORDER_BY_PDF_IF_LASTSEEN = " ORDER BY GroupField DESC, l.Name, pos.Name, pos.SerialNumber";
    private static final String SQL_ORDER_BY_CSV = " ORDER BY pos.Name, pos.SerialNumber";
    private static final String SQL_ORDER_BY_CUSTOMER_ID_PDF = " ORDER BY pos.customerId, GroupField, pos.Name, pos.SerialNumber";
    private static final String SQL_ORDER_BY_CUSTOMER_ID_PDF_IF_LASTSEEN =
            " ORDER BY pos.customerId, GroupField DESC, l.Name, pos.Name, pos.SerialNumber";
    private static final String SQL_ORDER_BY_CUSTOMER_ID_CSV = " ORDER BY pos.customerId, pos.Name, pos.SerialNumber";
    
    public InventoryThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue, final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext);
        this.jasperFileName = ReportingConstants.REPORT_JASPER_FILE_INVENTORY;
    }
    
    @Override
    protected final String generateSQLString() {
        final StringBuilder query = new StringBuilder();
        if (this.reportDefinition.getReportType().getId() == ReportingConstants.REPORT_TYPE_PAYSTATION_INVENTORY) {
            query.append(SQL_SELECT).append(SQL_SELECT_ROUTE_CSV).append(SQL_SELECT_NOGROUP).append(SQL_SELECT_MODEM_SETTINGS)
                    .append(SQL_SELECT_EMV_TELEMETRY_DATA).append(SQL_FROM).append(SQL_FROM_MODEM_DATA).append(SQL_FROM_EMV_TELEMETRY_DATA)
                    .append(SQL_WHERE).append(SQL_WHERE_SYSTEMADMIN);
            super.getWhereHiddenPointOfSales(query);
            query.append(SQL_ORDER_BY_CSV);
        } else if (this.reportDefinition.getReportType().getId() == ReportingConstants.REPORT_TYPE_INVENTORY) {
            getSelectString(query);
            query.append(SQL_SELECT_EMV_TELEMETRY_DATA);
            getFieldGroupByString(query);
            getFromString(query);
            query.append(SQL_FROM_MODEM_DATA).append(SQL_FROM_EMV_TELEMETRY_DATA);
            getWhereString(query);
            getOrderByString(query);
        }
        return query.toString();
    }
    
    private void getSelectString(final StringBuilder query) {
        query.append(SQL_SELECT);
        query.append(SQL_SELECT_MODEM_SETTINGS);
        if (!this.reportDefinition.isIsCsvOrPdf()) {
            query.append(SQL_SELECT_ROUTE_CSV);
        }
    }
    
    private void getFieldGroupByString(final StringBuilder query) {
        
        if (this.reportDefinition.isIsCsvOrPdf()) {
            int groupByType = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
            if (groupByType == WebCoreConstants.INT_RECORD_NOT_FOUND) {
                groupByType = ReportingConstants.REPORT_FILTER_GROUP_BY_LOCATION;
            }
            switch (groupByType) {
                case ReportingConstants.REPORT_FILTER_GROUP_BY_LOCATION:
                    query.append(", l.Name AS GroupField");
                    break;
                case ReportingConstants.REPORT_FILTER_GROUP_BY_ORGANIZATION:
                    query.append(", c.Name AS GroupField");
                    break;
                case ReportingConstants.REPORT_FILTER_GROUP_BY_LAST_SEEN_TIME:
                    query.append(", DATE(phb.LastHeartbeatGMT) AS GroupField");
                    break;
                case ReportingConstants.REPORT_FILTER_GROUP_BY_NONE:
                    query.append(SQL_SELECT_NOGROUP);
                    break;
                default:
                    break;
            }
        } else {
            query.append(SQL_SELECT_NOGROUP);
        }
    }
    
    private void getFromString(final StringBuilder query) {
        query.append(SQL_FROM);
    }
    
    //    private void getWhereString(final StringBuilder query, 
    //    final int locationFilterTypeId) {
    private void getWhereString(final StringBuilder query) {
        query.append(SQL_WHERE);
        super.getWherePointOfSale(query, "pos", false);
        super.getWhereHiddenPointOfSales(query);
        query.append(SQL_WHERE_ISPARENT);
    }
    
    private void getOrderByString(final StringBuilder query) {
        if (this.reportDefinition.isIsCsvOrPdf()) {
            if (this.customer.isIsParent()) {
                if (ReportingConstants.REPORT_FILTER_GROUP_BY_LAST_SEEN_TIME == super.getFilterTypeId(this.reportDefinition
                        .getGroupByFilterTypeId())) {
                    query.append(SQL_ORDER_BY_CUSTOMER_ID_PDF_IF_LASTSEEN);
                    
                } else {
                    query.append(SQL_ORDER_BY_CUSTOMER_ID_PDF);
                }
            } else {
                if (ReportingConstants.REPORT_FILTER_GROUP_BY_LAST_SEEN_TIME == super.getFilterTypeId(this.reportDefinition
                        .getGroupByFilterTypeId())) {
                    query.append(SQL_ORDER_BY_PDF_IF_LASTSEEN);
                } else {
                    query.append(SQL_ORDER_BY_PDF);
                }
            }
        } else if (this.customer.isIsParent()) {
            query.append(SQL_ORDER_BY_CUSTOMER_ID_CSV);
        } else {
            query.append(SQL_ORDER_BY_CSV);
        }
    }
    
    @Override
    protected final String createSqlRecordCountQuery() {
        final StringBuilder countQuery = new StringBuilder("SELECT COUNT(*) ");
        countQuery.append(SQL_FROM);
        final String query = countQuery.toString();
        
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("COUNT QUERY : " + query);
        }
        
        return query;
    }
    
    @Override
    protected final int getMaxRecordCount(final Errors errors) {
        return ReportingConstants.REPORTS_DEFAULT_MAX_RECORDS;
    }
    
    @Override
    protected final String createFileName() {
        final StringBuilder sb = new StringBuilder();
        sb.append(reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_INVENTORY));
        return super.createFileName(sb);
    }
    
    protected final Map<String, Object> createReportParameterList(final String query, final int customerId) {
        final Map<String, Object> parameters = super.createReportParameterList(query, customerId);
        parameters.put("ReportTitle", super.getTitle(reportingUtil.getPropertyEN("label.siteName") + " "
                                                     + reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_INVENTORY)));
        if (this.customer.isIsParent()) {
            parameters.put(ORGANIZATION, convertArrayToDelimiterString(this.organizationNameList, ','));
        } else {
            parameters.put(ORGANIZATION, customer.getName());
        }
        parameters.put("LocationType", reportingUtil.findFilterString(this.reportDefinition.getLocationFilterTypeId()));
        parameters.put("MachineNameOrNumber", convertArrayToDelimiterString(this.locationValueNameList, ','));
        parameters.put("Grouping", reportingUtil.findFilterString(this.reportDefinition.getGroupByFilterTypeId()));
        parameters.put("IsCsv", !this.reportDefinition.isIsCsvOrPdf());
        parameters.put("IsPdf", this.reportDefinition.isIsCsvOrPdf());
        parameters.put("IsGrouping",
                       ReportingConstants.REPORT_FILTER_GROUP_BY_NONE != super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId()));
        parameters.put("IsPdfGroupingLocation",
                       this.reportDefinition.isIsCsvOrPdf()
                                                && ReportingConstants.REPORT_FILTER_GROUP_BY_LOCATION == super.getFilterTypeId(this.reportDefinition
                                                        .getGroupByFilterTypeId()));
        parameters.put("isPayStationInventoryReport",
                       this.reportDefinition.getReportType().getId() == ReportingConstants.REPORT_TYPE_PAYSTATION_INVENTORY);
        
        return parameters;
    }
}
