package com.digitalpaytech.report.handler;

import org.springframework.context.ApplicationContext;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.WebCoreConstants;

public class MaintenanceSummaryThread extends ActiveAlertBaseThread {
	public MaintenanceSummaryThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue, final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext, ReportingConstants.REPORT_TYPE_NAME_MAINTENANCE_SUMMARY);
        this.jasperFileName = ReportingConstants.REPORT_JASPER_FILE_MAINTENANCE_SUMMARY;
    }
	
	@Override
    protected final void appendSpecificConditions(final StringBuilder query) {
		query.append("  AND (pec.CustomerAlertTypeId IS NULL OR cat.AlertThresholdTypeId = ");
		query.append(WebCoreConstants.ALERT_THRESHOLD_TYPE_LAST_SEEN_INTERVAL_HOUR);
		query.append("  ) ");
		
		// If 'All' device type is selected, database table ReportDefinition- EventDeviceTypeId is null.
		if (this.reportDefinition.getEventDeviceTypeId() != null) {
			query.append("  AND (et.EventDeviceTypeId = ");
			query.append(this.reportDefinition.getEventDeviceTypeId());
			query.append("  ) ");
		}
		
		if((this.reportDefinition.getEventSeverityTypeId() != null) && (this.reportDefinition.getEventSeverityTypeId() != ReportingConstants.REPORT_SELECTION_ALL)) {
			query.append("  AND (pec.EventSeverityTypeId = ");
			query.append(this.reportDefinition.getEventSeverityTypeId());
			query.append("  ) ");
		}
		else {
			query.append("  AND (pec.EventSeverityTypeId > 0) ");
		}
	}
}
