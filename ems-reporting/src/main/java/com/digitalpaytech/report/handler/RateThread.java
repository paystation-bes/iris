package com.digitalpaytech.report.handler;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.util.ReportingConstants;

public class RateThread extends ReportBaseThread {
    private static final Logger LOGGER = Logger.getLogger(RateThread.class);
    
    public RateThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue, final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext);
        this.jasperFileName = ReportingConstants.REPORT_JASPER_FILE_RATE;
    }
    
    @Override
    protected final String generateSQLString() throws Exception {
        final StringBuilder sqlQuery = new StringBuilder("SELECT ");
        
        getFieldGroupByString(sqlQuery, getTimeZoneOffset());
        getFieldString(sqlQuery);
        getTableString(sqlQuery);
        getWhereString(sqlQuery, false);
        getOrderByString(sqlQuery);
        return sqlQuery.toString();
    }
    
    @Override
    protected final String createSqlRecordCountQuery() throws Exception {
        final StringBuilder countQuery = new StringBuilder("SELECT COUNT(*) ");
        countQuery.append(" FROM Purchase pur");
        getWhereString(countQuery, true);
        
        final String query = countQuery.toString();
        LOGGER.info("COUNT QUERY : " + query);
        return query;
    }
    
    @Override
    protected final int getMaxRecordCount(final Errors errors) throws Exception {
        return ReportingConstants.REPORTS_DEFAULT_MAX_RECORDS;
    }
    
    @Override
    protected final String createFileName() {
        final StringBuilder sb = new StringBuilder();
        sb.append(reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_RATE));
        return super.createFileName(sb);
    }
    
    private void getFieldGroupByString(final StringBuilder query, final int offsetHour) {
        final int groupByType = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        
        switch (groupByType) {
            case ReportingConstants.REPORT_FILTER_GROUP_BY_DAY:
                query.append("DATE(ADDDATE(pur.PurchaseGMT, INTERVAL ");
                query.append(offsetHour);
                query.append(" HOUR)) as GroupField");
                break;
            case ReportingConstants.REPORT_FILTER_GROUP_BY_DAY_OF_WEEK:
                query.append("DAYNAME(ADDDATE(pur.PurchaseGMT, INTERVAL ");
                query.append(offsetHour);
                query.append(" HOUR)) as GroupField, DAYOFWEEK(ADDDATE(pur.PurchaseGMT, INTERVAL ");
                query.append(offsetHour);
                query.append(" HOUR)) as DayOfWeek");
                break;
            case ReportingConstants.REPORT_FILTER_GROUP_BY_MONTH:
                query.append("MONTHNAME(ADDDATE(pur.PurchaseGMT, INTERVAL ");
                query.append(offsetHour);
                query.append(" HOUR)) as GroupField, MONTH(ADDDATE(pur.PurchaseGMT, INTERVAL ");
                query.append(offsetHour);
                query.append(" HOUR)) as month");
                break;
            case ReportingConstants.REPORT_FILTER_GROUP_BY_LOCATION:
                query.append("l.Name AS GroupField, pur.LocationId AS LocationId");
                break;
            case ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION:
                query.append("pos.Name AS GroupField, pur.PointOfSaleId AS PaystationId");
                break;
            case ReportingConstants.REPORT_FILTER_GROUP_BY_TRANS_TYPE:
                query.append("tt.Name AS GroupField");
                break;
            case ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE:
                if (this.reportDefinition.isIsCsvOrPdf()) {
                    query.append("r.Name AS GroupField, r.Id AS PaystationRouteId");
                } else {
                    query.append("'NoGroup' AS GroupField");
                }
                break;
            default:
                query.append("'NoGroup' AS GroupField");
        }
    }
    
    private void getFieldString(final StringBuilder query) {
        query.append(", pur.UnifiedRateId AS RateId");
        query.append(", urt.Name AS RateName, pur.RateAmount AS Rate");
        query.append(", COUNT(1) AS TransactionCount");
        query.append(", COUNT(IF(pur.CouponId = '', NULL, pur.CouponId)) AS CouponCount");
        query.append(", COUNT(1) AS TotalCollectedCount");
        query.append(", SUM(pur.CardPaidAmount + pur.CashPaidAmount - IF(pur.TransactionTypeId=11 OR pur.TransactionTypeId=13 OR pur.TransactionTypeId=14, pur.CashPaidAmount, 0)) AS TotalCollectedAmount");
        query.append(", COUNT(IF(pur.ExcessPaymentAmount > 0, 1, NULL)) AS ExcessPaymentCount");
        query.append(", SUM(pur.ExcessPaymentAmount) AS ExcessPaymentAmount");
        query.append(", COUNT(IF(pur.ChangeDispensedAmount > 0, 1, NULL)) AS ChangeIssuedCount");
        query.append(", SUM(pur.ChangeDispensedAmount) AS ChangeIssuedAmount");
        query.append(", COUNT(IF(IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip, (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChangeDispensedAmount - pur.ChargedAmount), 0) > 0, 1, NULL)) AS RefundTicketCount");
        query.append(", SUM(IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip, (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChangeDispensedAmount - pur.ChargedAmount), 0)) AS RefundTicketAmount");
        query.append(", SUM(pur.RateRevenueAmount) AS Revenue");
        
    }
    
    private void getTableString(final StringBuilder query) {
        final int groupByType = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        
        query.append(" FROM Purchase pur");
        query.append(" INNER JOIN UnifiedRate urt ON pur.UnifiedRateId = urt.Id");
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == groupByType && this.reportDefinition.isIsCsvOrPdf()) {
            query.append(" LEFT JOIN RoutePOS rpos ON pur.PointOfSaleId=rpos.PointOfSaleId ");
            query.append(" LEFT JOIN Route r ON rpos.RouteId=r.Id ");
        } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_LOCATION == groupByType) {
            query.append(" LEFT JOIN Location l ON pur.LocationId = l.Id");
        } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_TRANS_TYPE == groupByType) {
            query.append(" INNER JOIN TransactionType tt ON pur.TransactionTypeId = tt.Id");
        } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION == groupByType) {
            query.append(" INNER JOIN PointOfSale pos ON pur.PointOfSaleId = pos.Id");
        }
        
        // Filter for hidden POS
        getFromHiddenPointOfSales(query, "pur");
        
        // TODO dropped???
        /*
         * if (EMSConstants.MACHINE_NUMBER.equals(reportsForm.getSelectedLocationType()) && !EMSConstants.ALL.equals(reportsForm.getSelectedPs1MachineNumber()))
         * {
         * sqlQuery.append(" INNER JOIN ServiceState s ON t.PaystationId = s.PaystationId");
         * }
         */
        
    }
    
    private void getWhereString(final StringBuilder query, final boolean isSummary) {
        query.append(" WHERE ");
        super.getWherePointOfSale(query, "pur", true);
        
        //        if (!isSummary) {
        //            getWherePointOfSaleRouteId(query, true);
        //        }
        
        // Purchased Date & Time
        getWherePurchaseGMT(query, "pur");
        
        // Transaction Type
        getWhereTransactionType(query);
        
        // Filter for hidden POS
        super.getWhereHiddenPointOfSales(query);
        
    }
    
    private void getOrderByString(final StringBuilder query) {
        final int groupByType = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        
        query.append(" GROUP BY GroupField, pur.UnifiedRateId");
        
        switch (groupByType) {
            case ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE:
                if (this.reportDefinition.isIsCsvOrPdf()) {
                    query.append(", r.Id");
                    query.append(" ORDER BY r.Id");
                } else {
                    query.append(" ORDER BY GroupField");
                }
                break;
            case ReportingConstants.REPORT_FILTER_GROUP_BY_LOCATION:
                query.append(" ORDER BY pur.LocationId");
                break;
            case ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION:
                query.append(" ORDER BY pur.PointOfSaleId");
                break;
            case ReportingConstants.REPORT_FILTER_GROUP_BY_TRANS_TYPE:
                query.append(" ORDER BY pur.TransactionTypeId");
                break;
            case ReportingConstants.REPORT_FILTER_GROUP_BY_DAY_OF_WEEK:
                query.append(" ORDER BY DayOfWeek");
                break;
            case ReportingConstants.REPORT_FILTER_GROUP_BY_DAY:
                query.append(" ORDER BY GroupField");
                break;
            case ReportingConstants.REPORT_FILTER_GROUP_BY_MONTH:
                query.append(" ORDER BY Month");
                break;
            default:
                query.append(" ORDER BY GroupField");
                break;
        }
        query.append(", urt.Name");
    }
    
    protected final Map<String, Object> createReportParameterList(final String query, final int customerId) {
        final Map<String, Object> parameters = super.createReportParameterList(query, customerId);
        
        parameters.put(
            "ReportTitle",
            super.getTitle(reportingUtil.getPropertyEN("label.siteName") + " "
                           + reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_RATE)));
        final int priDateFilterId = super.getFilterTypeId(this.reportDefinition.getPrimaryDateFilterTypeId());
        if (ReportingConstants.REPORT_SELECTION_ALL == priDateFilterId) {
            parameters.put("DateString", reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL));
        } else {
            parameters.put("DateString",
                createReportParameterDateString(this.reportQueue.getPrimaryDateRangeBeginGmt(), this.reportQueue.getPrimaryDateRangeEndGmt()));
            
        }
        
        // Location Parameters
        parameters.put("LotType", "Setting");
        
        parameters.put("LocationType", reportingUtil.findFilterString(this.reportDefinition.getLocationFilterTypeId()));
        parameters.put("MachineNameOrNumber", convertArrayToDelimiterString(this.locationValueNameList, ','));
        
        parameters.put("Grouping", reportingUtil.findFilterString(this.reportDefinition.getGroupByFilterTypeId()));
        parameters.put("TransactionType", reportingUtil.findFilterString(this.reportDefinition.getOtherParametersTypeFilterTypeId()));
        
        parameters.put("IsCsv", !this.reportDefinition.isIsCsvOrPdf());
        parameters.put("IsPdf", this.reportDefinition.isIsCsvOrPdf());
        
        final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        parameters.put("IsPdfGrouping", reportDefinition.isIsCsvOrPdf() && ReportingConstants.REPORT_FILTER_GROUP_BY_NONE != filterTypeId);
        final Boolean isGroupByPosRoute = ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == filterTypeId
                                          && reportDefinition.isIsCsvOrPdf();
        
        parameters.put("IsGroupByPsRoute", isGroupByPosRoute);
        parameters.put("NotGroupByPsRoute", !isGroupByPosRoute);
        parameters.put("UniqueRateNameMap", new HashMap<String, Map<String, Integer>>());
        
        parameters.put("ZeroFloat", new Float(0.00));
        parameters.put("ZeroInteger", new Integer(0));
        
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION == filterTypeId && reportDefinition.isIsCsvOrPdf()) {
            final String overallSummaryQuery = buildOverallSummaryQuery();
            LOGGER.info("RATE SUMMARY QUERY: " + overallSummaryQuery);
            parameters.put("OverallSummaryQuery", overallSummaryQuery);
            
            JasperReport subreport = null;
            try {
                
                final File jasperFile = new File(this.jasperFilePath + ReportingConstants.REPORT_JASPER_FILE_RATE_DETAIL_SUMMARY);
                final InputStream stream = new FileInputStream(jasperFile);
                
                parameters.put("Connection", super.getDataSource(this.reportDefinition).getConnection());
                final File tempPath = new File(this.tempReportsPath);
                if (!tempPath.exists()) {
                    if (!tempPath.mkdirs()) {
                        LOGGER.error("Cannot create temp reports directory , " + tempReportsPath);
                        throw new ApplicationException("Cannot create temp reports directory");
                    }
                }
                subreport = (JasperReport) JRLoader.loadObject(stream);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            parameters.put("OverallRateSummaryReport", subreport);
        } else {
            parameters.put("Connection", null);
            parameters.put("OverallSummaryQuery", null);
            parameters.put("OverallRateSummaryReport", null);
        }
        
        return parameters;
    }
    
    private String buildOverallSummaryQuery() {
        final StringBuilder summaryQuery = new StringBuilder("SELECT ");
        
        // Fields
        getSummaryFieldsString(summaryQuery);
        
        // Tables
        getSummaryTablesString(summaryQuery);
        
        // Where
        getWhereString(summaryQuery, true);
        
        return summaryQuery.toString();
    }
    
    private void getSummaryFieldsString(final StringBuilder query) {
        query.append("COUNT(1) AS TransactionCount");
        query.append(", COUNT(IF(pur.CouponId = '', NULL, pur.CouponId)) AS CouponCount");
        query.append(", COUNT(IF(pur.CardPaidAmount + pur.CashPaidAmount - IF(pur.TransactionTypeId=11 OR pur.TransactionTypeId=13 OR pur.TransactionTypeId=14, pur.CashPaidAmount, 0) > 0, 1, NULL)) AS TotalCollectedCount");
        query.append(", SUM(pur.CardPaidAmount + pur.CashPaidAmount - IF(pur.TransactionTypeId=11 OR pur.TransactionTypeId=13 OR pur.TransactionTypeId=14, pur.CashPaidAmount, 0)) AS TotalCollectedAmount");
        query.append(", COUNT(IF(pur.ExcessPaymentAmount > 0, 1, NULL)) AS ExcessPaymentCount");
        query.append(", SUM(pur.ExcessPaymentAmount) AS ExcessPaymentAmount");
        query.append(", COUNT(IF(pur.ChangeDispensedAmount > 0, 1, NULL)) AS ChangeIssuedCount");
        query.append(", SUM(pur.ChangeDispensedAmount) AS ChangeIssuedAmount");
        query.append(", COUNT(IF(IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip, (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChangeDispensedAmount - pur.ChargedAmount), 0) > 0, 1, NULL)) AS RefundTicketCount");
        query.append(", SUM(IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip, (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChangeDispensedAmount - pur.ChargedAmount), 0)) AS RefundTicketAmount");
        query.append(", SUM(pur.RateRevenueAmount) AS Revenue");
    }
    
    protected final void getSummaryTablesString(final StringBuilder query) {
        query.append(" FROM Purchase pur INNER JOIN UnifiedRate urt ON pur.UnifiedRateId = urt.Id");
    }
    
    private void getWhereTransactionType(final StringBuilder query) {
        final int transType = super.getFilterTypeId(this.reportDefinition.getOtherParametersTypeFilterTypeId());
        
        switch (transType) {
            case ReportingConstants.REPORT_FILTER_TRANSACTION_ALL:
                query.append(" AND pur.TransactionTypeId NOT IN (" + ReportingConstants.TRANSACTION_TYPE_REFUND_3RD_PARTY + ", "
                             + ReportingConstants.TRANSACTION_TYPE_EXTEND_BY_PHONE_ADDTIME_VALUE + ") ");
                break;
            case ReportingConstants.REPORT_FILTER_TRANSACTION_ADD_TIME:
                query.append(" AND pur.TransactionTypeId=" + ReportingConstants.TRANSACTION_TYPE_ADDTIME);
                break;
            case ReportingConstants.REPORT_FILTER_TRANSACTION_CANCELLED:
                query.append(" AND pur.TransactionTypeId=" + ReportingConstants.TRANSACTION_TYPE_CANCELLED);
                break;
            default:
                break;
        }
    }
    
}
