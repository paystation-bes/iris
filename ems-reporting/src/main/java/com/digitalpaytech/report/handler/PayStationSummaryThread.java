package com.digitalpaytech.report.handler;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.util.ReportingConstants;

public class PayStationSummaryThread extends ReportBaseThread {
    private static final Logger LOGGER = Logger.getLogger(PayStationSummaryThread.class);
    
    public PayStationSummaryThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue, final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext);
        this.jasperFileName = ReportingConstants.REPORT_JASPER_FILE_PS_STATUS;
    }
    
    @Override
    protected final String generateSQLString() throws Exception {
        final StringBuilder sqlQuery = new StringBuilder("SELECT");
        getFieldString(sqlQuery);
        getTableString(sqlQuery);
        getWhereString(sqlQuery);
        getOrderByString(sqlQuery);
        return sqlQuery.toString();
    }
    
    @Override
    protected final String createSqlRecordCountQuery() throws Exception {
        return null;
    }
    
    @Override
    protected final int getMaxRecordCount(final Errors errors) throws Exception {
        return ReportingConstants.REPORTS_DEFAULT_MAX_RECORDS;
    }
    
    @Override
    protected final String createFileName() {
        final StringBuilder sb = new StringBuilder();
        sb.append(reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_PAY_STATION_SUMMARY));
        return super.createFileName(sb);
    }
    
    private void getFieldString(final StringBuilder query) {
        query.append(" l.Name AS LocationName, pos.Name as PsName, phb.LastHeartBeatGMT AS LastActivity,");
        query.append(" ss.Battery1Voltage, ss.Battery2Voltage,  pb.CoinCount AS CoinCount, pb.BillCount as BillCount,");
        query.append(" pb.UnsettledCreditCardCount as UnsettledCreditCardCount, ROUND(pb.UnsettledCreditCardAmount / 100, 2) as UnsettledCreditCardAmount, ROUND(pb.TotalAmount / 100, 2)  AS RunningTotalAmount, '");
        query.append(this.timeZone);
        query.append("' AS TimeZone, CASE WHEN ss.PrinterStatus = 4 THEN 'JAM' WHEN ss.PrinterStatus <> 4 && ss.PrinterPaperLevel = 0 THEN 'EMPTY' WHEN ss.PrinterStatus <> 4 && ss.PrinterPaperLevel = 1 THEN 'LOW' ELSE 'GOOD' END AS PaperStatus");
    }
    
    private void getTableString(final StringBuilder query) {
        query.append(" FROM PointOfSale pos");
        query.append(" INNER JOIN Location l ON pos.LocationId=l.Id");
        query.append(" INNER JOIN POSSensorState ss ON pos.Id=ss.PointOfSaleId");
        query.append(" INNER JOIN POSBalance pb ON pos.Id= pb.PointOfSaleId");
        query.append(" INNER JOIN POSHeartbeat phb ON pos.Id= phb.PointOfSaleId");
        getFromHiddenPointOfSales(query, "pos");
    }
    
    private void getWhereString(final StringBuilder query) {
        query.append(" WHERE ");
        
        super.getWherePointOfSale(query, "pos", false);
        // Filter for hidden POS
        super.getWhereHiddenPointOfSales(query);
    }
    
    private void getOrderByString(final StringBuilder query) {
        query.append(" ORDER BY ");
        
        final int sortingOption = super.getFilterTypeId(this.reportDefinition.getSortByFilterTypeId());
        
        switch (sortingOption) {
            case ReportingConstants.REPORT_FILTER_SORT_BY_PAY_STATION_NAME:
                query.append(" pos.Name ");
                break;
            case ReportingConstants.REPORT_FILTER_SORT_BY_VOLTAGE:
                query.append(" ss.Battery1Voltage ");
                break;
            case ReportingConstants.REPORT_FILTER_SORT_BY_COIN_COUNT:
                query.append(" pb.CoinCount DESC ");
                break;
            case ReportingConstants.REPORT_FILTER_SORT_BY_BILL_COUNT:
                query.append(" pb.BillCount DESC ");
                break;
            case ReportingConstants.REPORT_FILTER_SORT_BY_PAPER_STATUS:
                query.append(" CASE PaperStatus");
                query.append(" WHEN 'EMPTY' THEN 1");
                query.append(" WHEN 'LOW' THEN 2");
                query.append(" WHEN 'GOOD' THEN 3");
                query.append(" END");
                break;
            case ReportingConstants.REPORT_FILTER_SORT_BY_RUNNING_TOTAL:
                query.append(" pb.TotalAmount ");
                break;
            case ReportingConstants.REPORT_FILTER_SORT_BY_UNSETTLED_CREDIT_CARD_COUNT:
                query.append(" pb.UnsettledCreditCardCount");
                break;
            case ReportingConstants.REPORT_FILTER_SORT_BY_UNSETTLED_CREDIT_CARD_AMOUNT:
                query.append(" pb.UnsettledCreditCardAmount ");
                break;
            default:
        }
    }
    
    protected final Map<String, Object> createReportParameterList(final String query, final int customerId) {
        final Map<String, Object> parameters = super.createReportParameterList(query, customerId);
        
        parameters.put(
            "ReportTitle",
            super.getTitle(reportingUtil.getPropertyEN("label.siteName") + " "
                           + reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_PAY_STATION_SUMMARY)));
        
        parameters.put("Location", convertArrayToDelimiterString(this.locationValueNameList, ','));
        parameters.put("LocationType", reportingUtil.findFilterString(this.reportDefinition.getLocationFilterTypeId()) + ":");
        
        parameters.put("Sorting", reportingUtil.findFilterString(this.reportDefinition.getSortByFilterTypeId()));
        
        parameters.put("IsCsv", !this.reportDefinition.isIsCsvOrPdf());
        parameters.put("IsPdf", this.reportDefinition.isIsCsvOrPdf());
        
        return parameters;
    }
    
}
