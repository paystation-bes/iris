package com.digitalpaytech.report.handler;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.context.ApplicationContext;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportLocationValue;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.service.CreditCardTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

public abstract class TransactionBaseThread extends ReportBaseThread {
    private static final String EXISTS_SELECT_PROCESSOR_TRANSACTION_TYPE_OPEN =
            " AND EXISTS ( SELECT PTT.Id FROM ProcessorTransactionType PTT WHERE PTT.Id ";
    private static final String EXISTS_SELECT_PROCESSOR_TRANSACTION_TYPE_CLOSE = " AND PTT.Id = proctrans.ProcessorTransactionTypeId ) ";
    private static final String SELECT_LOCATION_NAME = "l.Name AS LocationName";
    
    protected MerchantAccountService merchantAccountService;
    protected CreditCardTypeService creditCardTypeService;
    
    public TransactionBaseThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue,
            final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext);
        this.merchantAccountService = (MerchantAccountService) applicationContext.getBean("merchantAccountService");
        this.creditCardTypeService = (CreditCardTypeService) applicationContext.getBean("creditCardTypeService");
        this.jasperFileName = getJasperFileName();
    }
    
    @Override
    protected final int getMaxRecordCount(final Errors errors) throws Exception {
        int maxRecords = ReportingConstants.REPORTS_DEFAULT_MAX_RECORDS;
        if (this.reportDefinition.isIsDetailsOrSummary()) {
            maxRecords = ReportingConstants.REPORTS_DEFAULT_MAX_RECORDS;
        } else if (!this.reportDefinition.isIsCsvOrPdf()) {
            maxRecords = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.REPORTS_CSV_TRANSACTION_DETAILS_MAX_RECORDS,
                                                                         ReportingConstants.REPORTS_DEFAULT_CSV_TRANSACTION_DETAILS_MAX_RECORDS);
        } else if (this.reportDefinition.isIsCsvOrPdf()) {
            maxRecords = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.REPORTS_PDF_TRANSACTION_DETAILS_MAX_RECORDS,
                                                                         ReportingConstants.REPORTS_DEFAULT_PDF_TRANSACTION_DETAILS_MAX_RECORDS);
        }
        return maxRecords;
    }
    
    protected final void getFieldsGroupField(final StringBuilder query, final boolean isSummary, final boolean isOverallSummary) {
        final int groupingType = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        
        final int reportType = this.reportDefinition.getReportType().getId();
        
        if (isSummary) {
            if (isOverallSummary) {
                query.append("1 AS IsOverallSummary");
                query.append(", 'OverallSummary' AS GroupName");
                query.append(", 'OverallSummary' AS GroupOrderValue");
            } else {
                final String timeZoneOffsetStr = getTimeZoneOffsetString();
                query.append("0 AS IsOverallSummary");
                if (ReportingConstants.REPORT_FILTER_GROUP_BY_DAY == groupingType) {
                    if (ReportingConstants.REPORT_TYPE_TRANSACTION_CC_PROCESSING == reportType) {
                        query.append(", DATE_FORMAT(CONVERT_TZ(proctrans.ProcessingDate,'+00:00','" + timeZoneOffsetStr + "'), '"
                                     + reportingUtil.findFilterString(ReportingConstants.REPORT_FILTER_GROUP_BY_DAY) + ":  %m/%d/%Y') AS GroupName");
                        query.append(", DATE_FORMAT(CONVERT_TZ(proctrans.ProcessingDate,'+00:00','" + timeZoneOffsetStr
                                     + "'), '%Y/%m/%d') AS GroupOrderValue");
                    } else if (ReportingConstants.REPORT_TYPE_TRANSACTION_CC_RETRY == reportType) {
                        query.append(", DATE_FORMAT(CONVERT_TZ(IF(retrytrans.NumRetries=0 AND retrytrans.LastRetryDate=retrytrans.CreationDate, NULL, retrytrans.LastRetryDate),'+00:00','"
                                     + timeZoneOffsetStr + "'), '" + reportingUtil.findFilterString(ReportingConstants.REPORT_FILTER_GROUP_BY_DAY)
                                     + ":  %m/%d/%Y') AS GroupName");
                        query.append(", DATE_FORMAT(CONVERT_TZ(IF(retrytrans.NumRetries=0 AND retrytrans.LastRetryDate=retrytrans.CreationDate, NULL, retrytrans.LastRetryDate),'+00:00','"
                                     + timeZoneOffsetStr + "'), '%Y/%m/%d') AS GroupOrderValue");
                    } else {
                        query.append(", DATE_FORMAT(CONVERT_TZ(pur.PurchaseGMT,'+00:00','" + timeZoneOffsetStr + "'), '"
                                     + reportingUtil.findFilterString(ReportingConstants.REPORT_FILTER_GROUP_BY_DAY) + ":  %m/%d/%Y') AS GroupName");
                        query.append(", DATE_FORMAT(CONVERT_TZ(pur.PurchaseGMT,'+00:00','" + timeZoneOffsetStr
                                     + "'), '%Y/%m/%d') AS GroupOrderValue");
                    }
                } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_MONTH == groupingType) {
                    if (ReportingConstants.REPORT_TYPE_TRANSACTION_CC_PROCESSING == reportType) {
                        query.append(", DATE_FORMAT(CONVERT_TZ(proctrans.ProcessingDate,'+00:00','" + timeZoneOffsetStr + "'), '"
                                     + reportingUtil.findFilterString(ReportingConstants.REPORT_FILTER_GROUP_BY_MONTH) + ":  %m/%Y') AS GroupName");
                        query.append(", DATE_FORMAT(CONVERT_TZ(proctrans.ProcessingDate,'+00:00','" + timeZoneOffsetStr
                                     + "'), '%Y/%m') AS GroupOrderValue");
                    } else if (ReportingConstants.REPORT_TYPE_TRANSACTION_CC_RETRY == reportType) {
                        query.append(", DATE_FORMAT(CONVERT_TZ(IF(retrytrans.NumRetries=0 AND retrytrans.LastRetryDate=retrytrans.CreationDate, NULL, retrytrans.LastRetryDate),'+00:00','"
                                     + timeZoneOffsetStr + "'), '" + reportingUtil.findFilterString(ReportingConstants.REPORT_FILTER_GROUP_BY_MONTH)
                                     + ":  %m/%Y') AS GroupName");
                        query.append(", DATE_FORMAT(CONVERT_TZ(IF(retrytrans.NumRetries=0 AND retrytrans.LastRetryDate=retrytrans.CreationDate, NULL, retrytrans.LastRetryDate),'+00:00','"
                                     + timeZoneOffsetStr + "'), '%Y/%m') AS GroupOrderValue");
                    } else {
                        query.append(", DATE_FORMAT(CONVERT_TZ(pur.PurchaseGMT,'+00:00','" + timeZoneOffsetStr + "'), '"
                                     + reportingUtil.findFilterString(ReportingConstants.REPORT_FILTER_GROUP_BY_MONTH) + ":  %m/%Y') AS GroupName");
                        query.append(", DATE_FORMAT(CONVERT_TZ(pur.PurchaseGMT,'+00:00','" + timeZoneOffsetStr + "'), '%Y/%m') AS GroupOrderValue");
                    }
                } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_LOCATION == groupingType) {
                    if (ReportingConstants.REPORT_TYPE_TRANSACTION_CC_REFUND == reportType
                        || ReportingConstants.REPORT_TYPE_TRANSACTION_CC_PROCESSING == reportType) {
                        query.append(", CONCAT('" + reportingUtil.findFilterString(ReportingConstants.REPORT_FILTER_GROUP_BY_LOCATION)
                                     + ":  ', IF(l.Name IS NULL, l2.Name, l.Name)) AS GroupName");
                        query.append(", IF(l.Name IS NULL, l2.Name, l.Name) AS GroupOrderValue");
                    } else {
                        query.append(", CONCAT('" + reportingUtil.findFilterString(ReportingConstants.REPORT_FILTER_GROUP_BY_LOCATION)
                                     + ":  ', l.Name) AS GroupName");
                        query.append(", l.Name AS GroupOrderValue");
                    }
                } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION == groupingType) {
                    query.append(", CONCAT('" + reportingUtil.findFilterString(ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION)
                                 + ":  ', pos.Name) AS GroupName");
                    query.append(", pos.Name AS GroupOrderValue");
                } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAYMENT_TYPE == groupingType) {
                    query.append(", CONCAT('" + reportingUtil.findFilterString(ReportingConstants.REPORT_FILTER_GROUP_BY_PAYMENT_TYPE)
                                 + ":  ', pt.Name) AS GroupName");
                    query.append(", pt.Name AS GroupOrderValue");
                } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_TRANS_TYPE == groupingType) {
                    query.append(", CONCAT('" + reportingUtil.findFilterString(ReportingConstants.REPORT_FILTER_GROUP_BY_TRANS_TYPE)
                                 + ":  ', tt.Name) AS GroupName");
                    query.append(", tt.Name AS GroupOrderValue");
                } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_CARD_TYPE == groupingType) {
                    if ((ReportingConstants.REPORT_TYPE_TRANSACTION_CREDIT_CARD == reportType)
                        || (ReportingConstants.REPORT_TYPE_TRANSACTION_CC_REFUND == reportType)
                        || (ReportingConstants.REPORT_TYPE_TRANSACTION_CC_PROCESSING == reportType)) {
                        query.append(", CONCAT('" + reportingUtil.findFilterString(ReportingConstants.REPORT_FILTER_GROUP_BY_CARD_TYPE)
                                     + ":  ', IF(ISNULL(proctrans.CardType), 'Credit Card', proctrans.CardType)) AS GroupName");
                        query.append(", IF(ISNULL(proctrans.CardType), 'Credit Card', proctrans.CardType) AS GroupOrderValue");
                    } else if (ReportingConstants.REPORT_TYPE_TRANSACTION_VALUE_CARD == reportType) {
                        query.append(", CONCAT('" + reportingUtil.findFilterString(ReportingConstants.REPORT_FILTER_GROUP_BY_CARD_TYPE)
                                     + ":  ', cct.Name) AS GroupName");
                        query.append(", cct.Name AS GroupOrderValue");
                    } else {
                        query.append(", 'NoGroup' AS GroupName");
                        query.append(", 'NoGroup' AS GroupOrderValue");
                    }
                } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_CARD_NUMBER == groupingType) {
                    if (ReportingConstants.REPORT_TYPE_TRANSACTION_PATROLLER_CARD == reportType) {
                        query.append(", CONCAT('" + reportingUtil.findFilterString(ReportingConstants.REPORT_FILTER_GROUP_BY_CARD_NUMBER)
                                     + ":  ', cc.CardNumber, '') AS GroupName");
                        query.append(", CONCAT(cc.CardNumber,'') AS GroupOrderValue");
                    } else {
                        query.append(", CONCAT('" + reportingUtil.findFilterString(ReportingConstants.REPORT_FILTER_GROUP_BY_CARD_NUMBER)
                                     + ":  ', Right(cc.CardNumber,8)) AS GroupName");
                        query.append(", Right(cc.CardNumber,8) AS GroupOrderValue");
                    }
                } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == groupingType && this.reportDefinition.isIsCsvOrPdf()) {
                    query.append(", CONCAT('" + reportingUtil.findFilterString(ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE)
                                 + ":  ' , r.Name) AS GroupName");
                    query.append(", r.Name AS GroupOrderValue");
                    
                } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_MERCHANT_ACCOUNT == groupingType) {
                    query.append(", CONCAT('").append(reportingUtil.findFilterString(ReportingConstants.REPORT_FILTER_GROUP_BY_MERCHANT_ACCOUNT));
                    query.append(":  ', IFNULL((SELECT IF(TerminalName IS NULL OR TerminalName = '', Name, TerminalName) AS Name FROM MerchantAccount ");
                    query.append(" WHERE id = proctrans.MerchantAccountId), 'N/A')) AS GroupName");
                    query.append(", IFNULL((SELECT IF(TerminalName IS NULL OR TerminalName = '', Name, TerminalName) AS Name FROM MerchantAccount ");
                    query.append(" WHERE id = proctrans.MerchantAccountId), 'N/A') AS GroupOrderValue");
                } else {
                    query.append(", 'NoGroup' AS GroupName");
                    query.append(", 'NoGroup' AS GroupOrderValue");
                }
            }
            return;
        }
        
        // not group summary
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_DAY == groupingType) {
            query.append("'" + reportingUtil.findFilterString(ReportingConstants.REPORT_FILTER_GROUP_BY_DAY) + "' as GroupField");
        } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_MONTH == groupingType) {
            query.append("'" + reportingUtil.findFilterString(ReportingConstants.REPORT_FILTER_GROUP_BY_MONTH) + "' as GroupField");
        } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_LOCATION == groupingType) {
            if (ReportingConstants.REPORT_TYPE_TRANSACTION_CC_REFUND == reportType
                || ReportingConstants.REPORT_TYPE_TRANSACTION_CC_PROCESSING == reportType) {
                // If 'location l LEFT JOIN transaction regionId' is null,
                // replaces with 'region l2 LEFT JOINT paystation regionId'.
                query.append("IF(l.Name IS NULL, l2.Name, l.Name) AS GroupField");
            } else {
                query.append("l.Name AS GroupField");
            }
        } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION == groupingType) {
            query.append("pos.Name AS GroupField");
        } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAYMENT_TYPE == groupingType) {
            query.append("pt.Name AS GroupField");
        } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_TRANS_TYPE == groupingType) {
            query.append("tt.Name AS GroupField");
        } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_CARD_TYPE == groupingType) {
            if ((ReportingConstants.REPORT_TYPE_TRANSACTION_CREDIT_CARD == reportType)
                || (ReportingConstants.REPORT_TYPE_TRANSACTION_CC_REFUND == reportType)
                || (ReportingConstants.REPORT_TYPE_TRANSACTION_CC_PROCESSING == reportType)) {
                query.append("IF(ISNULL(proctrans.CardType), 'Credit Card', proctrans.CardType) AS GroupField");
            } else if (ReportingConstants.REPORT_TYPE_TRANSACTION_VALUE_CARD == reportType) {
                query.append("cct.Name AS GroupField");
            } else {
                query.append("'NoGroup' AS GroupField");
            }
        } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_CARD_NUMBER == groupingType) {
            if (ReportingConstants.REPORT_TYPE_TRANSACTION_PATROLLER_CARD == reportType) {
                query.append("CONCAT(cc.CardNumber,'') AS GroupField");
            } else {
                query.append("Right(cc.CardNumber,8) AS GroupField");
            }
        } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_MERCHANT_ACCOUNT == groupingType) {
            query.append("IFNULL(ma.Name, 'N/A') AS GroupField");
        } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == groupingType && this.reportDefinition.isIsCsvOrPdf()) {
            query.append("r.Name AS GroupField");
        } else {
            query.append("'NoGroup' AS GroupField");
        }
    }
    
    protected final void getFieldsStandard(final StringBuilder query) {
        final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == filterTypeId && this.reportDefinition.isIsCsvOrPdf()) {
            query.append(", pos.Id as PaystationId, r.Id as PaystationRouteId");
        } else {
            query.append(", 0 as PaystationId, 0 as PaystationRouteId");
        }
        
        // Location/Paystation Fields
        query.append(", pos.Name AS PosName");
        
        // PaymentType/TransactionType Fields
        query.append(", pt.Name AS PaymentTypeName, tt.Name AS TransactionTypeName");
        query.append(", ").append(SELECT_LOCATION_NAME).append(", '");
        query.append(this.timeZone);
        query.append("' AS TimeZone");
        
        // Miscellaneous Fields
        query.append(", pos.Name AS MachineNumber, per.SpaceNumber, lp.Number AS PlateNumber, pur.PurchaseNumber AS TicketNumber");
        query.append(", pur.PurchaseGMT AS PurchasedDate, per.PermitOriginalExpireGMT AS ExpiryDate");
        // TODO CitationNumber removed
        query.append(", IF(ISNULL(c.Coupon), '', c.Coupon) AS CouponNumber");
        query.append(", pur.TransactionTypeId");
    }
    
    protected final void getFieldsCash(final StringBuilder query, final boolean isSummary) {
        final int reportType = this.reportDefinition.getReportType().getId();
        if (isSummary) {
            if (ReportingConstants.REPORT_TYPE_TRANSACTION_PATROLLER_CARD == reportType) {
                if ("SELECT".equalsIgnoreCase(query.toString().trim())) {
                    query.append("SUM(IF(pur.CashPaidAmount > 0, 1, 0)) AS CashCollectionsN");
                } else {
                    query.append(", SUM(IF(pur.CashPaidAmount > 0, 1, 0)) AS CashCollectionsN");
                }
                query.append(", SUM(IF(pur.CashPaidAmount > 0, pur.CashPaidAmount,0)) AS CashCollections");
                query.append(", SUM(IF(pur.TransactionTypeId=8 AND pur.CashPaidAmount > 0, 1, 0)) AS CashAttendantDepositN");
                query.append(", SUM(IF(pur.TransactionTypeId=8 AND pur.CashPaidAmount > 0, pur.CashPaidAmount, 0)) AS CashAttendantDeposit");
            } else if (ReportingConstants.REPORT_TYPE_TRANSACTION_CASH == reportType) {
                if ("SELECT".equalsIgnoreCase(query.toString().trim())) {
                    query.append("SUM(IF(pur.CashPaidAmount > 0, 1, 0)) AS CashCollectionsN");
                } else {
                    query.append(", SUM(IF(pur.CashPaidAmount > 0, 1, 0)) AS CashCollectionsN");
                }
                query.append(", SUM(pur.CashPaidAmount) AS CashCollections");
                query.append(", SUM(IF(pur.ChangeDispensedAmount > 0, 1, 0)) AS CashChangeDispensedN");
                query.append(", SUM(pur.ChangeDispensedAmount) AS CashChangeDispensed");
                query.append(", SUM(IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip AND (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChangeDispensedAmount - pur.ChargedAmount) > 0, 1, 0)) AS CashRefundTicketN");
                query.append(", SUM(IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip AND (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChangeDispensedAmount - pur.ChargedAmount) > 0, (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChangeDispensedAmount - pur.ChargedAmount), 0)) AS CashRefundTicket");
                query.append(", SUM((IF((pur.CashPaidAmount > 0 AND pur.IsRefundSlip AND (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChargedAmount) > 0) OR (pur.ChangeDispensedAmount > 0), 1, 0))) AS CashTotalRefundsN");
                query.append(", SUM(IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip, (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChargedAmount), pur.ChangeDispensedAmount)) AS CashTotalRefunds");
                query.append(", SUM(IF((pur.CashPaidAmount - IF(pur.TransactionTypeId=4 AND pur.PaymentTypeId=1 AND pur.ChargedAmount>0, pur.ChargedAmount, 0) - IF(pur.TransactionTypeId=8 AND pur.CashPaidAmount > 0, pur.CashPaidAmount, 0) - IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip, (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChargedAmount), pur.ChangeDispensedAmount))  + IF(pur.transactionTypeId = 6 AND pur.OriginalAmount = 0, 1, 0) + IF(pur.transactionTypeId = 6 AND pur.OriginalAmount = 0, 1, 0)  > 0, 1, 0))  AS CashRevenueN");
                query.append(", SUM(pur.CashPaidAmount - IF(pur.TransactionTypeId=4 AND pur.PaymentTypeId=1 AND pur.ChargedAmount>0, pur.ChargedAmount, 0) - IF(pur.TransactionTypeId=8 AND pur.CashPaidAmount > 0, pur.CashPaidAmount, 0) - IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip, (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChargedAmount), pur.ChangeDispensedAmount)) AS CashRevenue");
                query.append(", SUM(IF(pur.ExcessPaymentAmount > 0, 1, 0)) AS CashExcessPaymentN");
                query.append(", SUM(pur.ExcessPaymentAmount) AS CashExcessPayment");
                query.append(", SUM(IF(pur.TransactionTypeId=8 AND pur.CashPaidAmount > 0, 1, 0)) AS CashAttendantDepositN");
                query.append(", SUM(IF(pur.TransactionTypeId=8, pur.CashPaidAmount, 0)) AS CashAttendantDeposit");
                query.append(", SUM(IF(pur.TransactionTypeId=4 AND pur.PaymentTypeId=1 AND pur.ChargedAmount>0, 1, 0)) AS CashScRechargeAmountN");
                query.append(", SUM(IF(pur.TransactionTypeId=4 AND pur.PaymentTypeId=1, pur.ChargedAmount, 0)) AS CashScRechargeAmount");
            } else if (ReportingConstants.REPORT_TYPE_TRANSACTION_SMART_CARD == reportType) {
                if ("SELECT".equalsIgnoreCase(query.toString().trim())) {
                    query.append("SUM(IF(pur.CashPaidAmount > 0, 1, 0)) AS CashCollectionsN");
                } else {
                    query.append(", SUM(IF(pur.CashPaidAmount > 0, 1, 0)) AS CashCollectionsN");
                }
                query.append(", SUM(pur.CashPaidAmount) AS CashCollections");
                query.append(", SUM(IF((pur.CashPaidAmount - IF(pur.TransactionTypeId=4 AND pur.PaymentTypeId=1, pur.ChargedAmount, 0) - IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip, (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChargedAmount), pur.ChangeDispensedAmount)) + IF(pur.transactionTypeId = 6 AND pur.OriginalAmount = 0, 1, 0) > 0, 1, 0)) AS CashRevenueN");
                query.append(", SUM(pur.CashPaidAmount - IF(pur.TransactionTypeId=4 AND pur.PaymentTypeId=1, pur.ChargedAmount, 0) - IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip, (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChargedAmount), pur.ChangeDispensedAmount)) AS CashRevenue");
                query.append(", SUM(IF(pur.TransactionTypeId=4 AND pur.PaymentTypeId=1 AND pur.ChargedAmount>0, 1, 0)) AS CashScRechargeAmountN");
                query.append(", SUM(IF(pur.TransactionTypeId=4 AND pur.PaymentTypeId=1, pur.ChargedAmount, 0)) AS CashScRechargeAmount");
            } else if (ReportingConstants.REPORT_TYPE_TRANSACTION_ALL == reportType) {
                if ("SELECT".equalsIgnoreCase(query.toString().trim())) {
                    query.append("SUM(IF((pur.CashPaidAmount - IF(pur.TransactionTypeId=11 OR pur.TransactionTypeId=13 OR pur.TransactionTypeId=14, pur.CashPaidAmount, 0)) > 0, 1, 0)) AS CashCollectionsN");
                } else {
                    query.append(", SUM(IF((pur.CashPaidAmount - IF(pur.TransactionTypeId=11 OR pur.TransactionTypeId=13 OR pur.TransactionTypeId=14, pur.CashPaidAmount, 0)) > 0, 1, 0)) AS CashCollectionsN");
                }
                query.append(", SUM(pur.CashPaidAmount - IF(pur.TransactionTypeId=11 OR pur.TransactionTypeId=13 OR pur.TransactionTypeId=14, pur.CashPaidAmount, 0)) AS CashCollections");
                query.append(", SUM(IF((pur.CashPaidAmount - IF(pur.PaymentTypeId=1 AND pur.TransactionTypeId=4, pur.ChargedAmount, 0) - IF(pur.TransactionTypeId=8, pur.CashPaidAmount, 0) - IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip, (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChargedAmount), pur.ChangeDispensedAmount) - IF(pur.TransactionTypeId=11 OR pur.TransactionTypeId=13 OR pur.TransactionTypeId=14, pur.CashPaidAmount, 0)) + IF(pur.transactionTypeId = 6 AND pur.OriginalAmount = 0, 1, 0) > 0, 1, 0)) AS CashRevenueN");
                query.append(", SUM(pur.CashPaidAmount - IF(pur.TransactionTypeId=4 AND pur.PaymentTypeId=1, pur.ChargedAmount, 0) - IF(pur.TransactionTypeId=8 AND pur.CashPaidAmount > 0, pur.CashPaidAmount, 0) - IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip, (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChargedAmount), pur.ChangeDispensedAmount) - IF(pur.TransactionTypeId=11 OR pur.TransactionTypeId=13 OR pur.TransactionTypeId=14, pur.CashPaidAmount, 0)) AS CashRevenue");
                query.append(", SUM(IF(pur.ChangeDispensedAmount > 0, 1, 0)) AS CashChangeDispensedN");
                query.append(", SUM(pur.ChangeDispensedAmount) AS CashChangeDispensed");
                query.append(", SUM(IF(IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip AND (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChangeDispensedAmount - pur.ChargedAmount) > 0, (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChangeDispensedAmount - pur.ChargedAmount), 0) > 0, 1, 0)) AS CashRefundTicketN");
                query.append(", SUM(IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip AND (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChangeDispensedAmount - pur.ChargedAmount) > 0, (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChangeDispensedAmount - pur.ChargedAmount), 0)) AS CashRefundTicket");
                query.append(", SUM(IF((pur.CashPaidAmount > 0 AND pur.IsRefundSlip AND (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChargedAmount)) OR (pur.ChangeDispensedAmount > 0), 1, 0)) AS CashTotalRefundsN");
                query.append(", SUM(IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip, (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChargedAmount), pur.ChangeDispensedAmount)) AS CashTotalRefunds");
                query.append(", SUM(IF(pur.ExcessPaymentAmount > 0, 1, 0)) AS CashExcessPaymentN");
                query.append(", SUM(pur.ExcessPaymentAmount) AS CashExcessPayment");
                query.append(", SUM(IF(pur.TransactionTypeId=8, 1, 0)) AS CashAttendantDepositN");
                query.append(", SUM(IF(pur.TransactionTypeId=8, pur.CashPaidAmount, 0)) AS CashAttendantDeposit");
                query.append(", SUM(IF(pur.TransactionTypeId=4 AND pur.PaymentTypeId=1 AND pur.ChargedAmount>0, 1, 0)) AS CashScRechargeAmountN");
                query.append(", SUM(IF(pur.TransactionTypeId=4 AND pur.PaymentTypeId=1, pur.ChargedAmount, 0)) AS CashScRechargeAmount");
            }
        } else {
            // Cash Fields
            query.append(", pur.ExcessPaymentAmount AS CashExcessPayment, pur.ChangeDispensedAmount AS CashChangeDispensed");
            // CashRefundTicket = CashPaid - ChargedAmount - ChangeDispensed
            query.append(", IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip AND (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChangeDispensedAmount - pur.ChargedAmount) > 0, (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChangeDispensedAmount - pur.ChargedAmount), 0) AS CashRefundTicket");
            query.append(", IF(pur.TransactionTypeId=4 AND pur.PaymentTypeId=1, pur.ChargedAmount, 0) AS CashScRechargeAmount");
            query.append(", IF(pur.TransactionTypeId=8, pur.CashPaidAmount, 0) AS CashAttendantDeposit");
            // CashTotalRefunds = CashRefundTicket + ChangeDispensed = CashPaid
            // - ChargedAmount
            query.append(", IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip, (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChargedAmount), pur.ChangeDispensedAmount) AS CashTotalRefunds");
            query.append(", pur.CashPaidAmount AS CashCollections, IF(pur.TransactionTypeId=11 OR pur.TransactionTypeId=13 OR pur.TransactionTypeId=14, pur.CashPaidAmount, 0) AS ReplenishAmount");
        }
    }
    
    protected final void getFieldsCreditCardTransactions(final StringBuilder query, final boolean isSummary) {
        final int reportType = this.reportDefinition.getReportType().getId();
        if (isSummary) {
            if ("SELECT".equalsIgnoreCase(query.toString().trim())) {
                query.append("SUM(IF(pur.PaymentTypeId IN (2, 5, 11, 12, 13, 14, 15, 16, 17), pur.CardPaidAmount, 0)) AS CcCollections");
            } else {
                query.append(", SUM(IF(pur.PaymentTypeId IN (2, 5, 11, 12, 13, 14, 15, 16, 17), pur.CardPaidAmount, 0)) AS CcCollections");
            }
            query.append(", SUM(IF((pur.PaymentTypeId IN (2, 5, 11, 12, 13, 14, 15, 16, 17)) AND pur.CardPaidAmount>0, 1, 0)) AS CcCollectionsN");
            query.append(", SUM(IF(pur.TransactionTypeId=4 AND pur.PaymentTypeId IN (2, 11, 13), pur.ChargedAmount, 0)) AS CcScRechargeAmount");
            query.append(", SUM(IF(pur.TransactionTypeId=4 AND pur.PaymentTypeId IN (2, 11, 13) AND pur.ChargedAmount>0, 1, 0)) AS CcScRechargeAmountN");
            query.append(", SUM(IF(pur.TransactionTypeId=4 AND pur.PaymentTypeId IN (2, 11, 13), 0 ,IF((pur.PaymentTypeId IN (2, 5, 11, 12, 13, 14, 15, 16, 17)), pur.CardPaidAmount, 0))) AS CcRevenue");
            query.append(", SUM(IF(pur.TransactionTypeId=4 AND pur.PaymentTypeId IN (2, 11, 13), 0 ,IF(((pur.PaymentTypeId IN (2, 5, 11, 12, 13, 14, 15, 16, 17)) AND pur.CardPaidAmount>0), 1, 0))) AS CcRevenueN");
        } else {
            if (ReportingConstants.REPORT_TYPE_TRANSACTION_CREDIT_CARD == reportType) {
                query.append(", IF(ISNULL(proctrans.AuthorizationNumber), 'N/A',");
                query.append(" SUBSTRING_INDEX(proctrans.AuthorizationNumber, ':', 1)) AS AuthorizationNumber");
                query.append(", IF(ISNULL(proctrans.CardType), 'Credit Card', proctrans.CardType) AS CardType");
                query.append(", IF(proctrans.Last4DigitsOfCardNumber=0, 0, proctrans.Last4DigitsOfCardNumber) AS CardNumber");
                query.append(", IF(etd.TransactionSettlementStatusTypeId=" + CardProcessingConstants.TRANSACTION_SETTLEMENT_STATUS_TYPE_POSTAUTH_ID
                             + StandardConstants.STRING_COMMA);
                query.append(" tst.name,IF(ISNULL(proctranstype.StatusName), 'Offline',");
                query.append(" IF(proctranstype.StatusName = 'Extend by Phone', 'SMS', proctranstype.StatusName))) AS Status");
            }
            query.append(", IF(pur.TransactionTypeId=4 AND pur.PaymentTypeId IN (2, 11, 13), pur.ChargedAmount, 0) AS CcScRechargeAmount");
            query.append(", IF(pur.PaymentTypeId IN (2, 5, 11, 12, 13, 14, 15, 16, 17), pur.CardPaidAmount, 0) AS CcCollections");
        }
    }
    
    protected final void getFieldsSmartCard(final StringBuilder query, final boolean isSummary) {
        final int reportType = this.reportDefinition.getReportType().getId();
        if (isSummary) {
            query.append(", SUM(IF(pur.PaymentTypeId=4 OR pur.PaymentTypeId=6, pur.CardPaidAmount, 0)) AS ScRevenue");
            query.append(", SUM(IF((pur.PaymentTypeId=4 OR pur.PaymentTypeId=6) AND pur.CardPaidAmount>0, 1, 0)) AS ScRevenueN");
            if (ReportingConstants.REPORT_TYPE_TRANSACTION_SMART_CARD == reportType) {
                query.append(", SUM(pur.CashPaidAmount + IF(pur.PaymentTypeId IN (2, 5, 11, 12, 13, 14, 15, 16, 17), pur.CardPaidAmount, 0)) AS Collections");
                query.append(", SUM(IF(pur.CashPaidAmount + IF(pur.PaymentTypeId IN (2, 5, 11, 12, 13, 14, 15, 16, 17), pur.CardPaidAmount, 0) > 0, 1, 0)) AS CollectionsN");
                query.append(", SUM(IF(pur.TransactionTypeId=4 AND pur.PaymentTypeId IN (2, 11, 13), 0 ,IF((pur.PaymentTypeId IN (2, 5, 11, 12, 13, 14, 15, 16, 17)), pur.CardPaidAmount, 0))");
                query.append("+ pur.CashPaidAmount - IF(pur.TransactionTypeId=4 AND pur.PaymentTypeId=1, pur.ChargedAmount, 0) - IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip, (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChargedAmount), pur.ChangeDispensedAmount)");
                query.append("+ IF(pur.PaymentTypeId=4 OR pur.PaymentTypeId=6, pur.CardPaidAmount, 0)) AS Revenue");
                query.append(", SUM(IF(IF(pur.TransactionTypeId=4 AND pur.PaymentTypeId IN (2, 11, 13), 0 ,IF((pur.PaymentTypeId IN (2, 5, 11, 12, 13, 14, 15, 16, 17)), pur.CardPaidAmount, 0))");
                query.append("+ pur.CashPaidAmount - IF(pur.TransactionTypeId=4 AND pur.PaymentTypeId=1, pur.ChargedAmount, 0) - IF(pur.CashPaidAmount > 0 AND pur.IsRefundSlip, (pur.CashPaidAmount + pur.CardPaidAmount - pur.ChargedAmount), pur.ChangeDispensedAmount)");
                query.append("+ IF(pur.PaymentTypeId=4 OR pur.PaymentTypeId=6, pur.CardPaidAmount, 0) > 0, 1, 0)) AS RevenueN");
            }
        } else {
            if (ReportingConstants.REPORT_TYPE_TRANSACTION_SMART_CARD == reportType) {
                query.append(", pscard.SmartCardData AS ScNumber");
            }
            query.append(", IF(pur.PaymentTypeId=4 OR pur.PaymentTypeId=6, pur.CardPaidAmount, 0) AS ScRevenue");
        }
    }
    
    protected final void getFieldsPatrollerCard(final StringBuilder query, final boolean isSummary) {
        if (isSummary) {
            if ("SELECT".equalsIgnoreCase(query.toString().trim())) {
                query.append("SUM(IF(pur.PaymentTypeId=3 AND pur.TransactionTypeId<>6 AND pur.CardPaidAmount>0, 1, 0)) AS PcRevenueN");
            } else {
                query.append(", SUM(IF(pur.PaymentTypeId=3 AND pur.TransactionTypeId<>6 AND pur.CardPaidAmount>0, 1, 0)) AS PcRevenueN");
            }
            query.append(", SUM(IF(pur.PaymentTypeId=3 AND pur.TransactionTypeId<>6, pur.CardPaidAmount, 0)) AS PcRevenue");
            query.append(", SUM(IF(pur.PaymentTypeId=3 AND pur.TransactionTypeId=6 AND pur.CardPaidAmount>0, 1, 0)) AS PcTestAmountN");
            query.append(", SUM(IF(pur.PaymentTypeId=3 AND pur.TransactionTypeId=6, pur.CardPaidAmount, 0)) AS PcTestAmount");
            if (ReportingConstants.REPORT_TYPE_TRANSACTION_PATROLLER_CARD == this.reportDefinition.getReportType().getId()) {
                query.append(", SUM(IF(pur.PaymentTypeId=3 AND pur.TransactionTypeId=6 AND pur.CardPaidAmount>0, 0, 1)) AS Count");
            }
        } else {
            if (ReportingConstants.REPORT_TYPE_TRANSACTION_PATROLLER_CARD == this.reportDefinition.getReportType().getId()) {
                query.append(", cct.Name AS CustomCardType, cc.CardNumber AS CardNumber");
            }
            query.append(", IF(pur.PaymentTypeId=3 and pur.TransactionTypeId<>6, pur.CardPaidAmount, 0) AS PcRevenue");
            query.append(", IF(pur.PaymentTypeId=3 and pur.TransactionTypeId=6, pur.CardPaidAmount, 0) AS PcTestAmount");
        }
    }
    
    protected final void getTables(final StringBuilder query, final boolean generateReport, final boolean isOverallSummary) {
        final int reportType = this.reportDefinition.getReportType().getId();
        final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        query.append(" FROM Purchase pur ");
        query.append("LEFT JOIN Permit per ON per.PurchaseId = pur.Id ");
        
        getTableJoinToProcessorTransactionFromTransaction(query, generateReport, filterTypeId);
        query.append("LEFT JOIN PointOfSale pos ON pur.PointOfSaleId = pos.Id ");
        
        query.append("INNER JOIN TransactionType tt ON pur.TransactionTypeId = tt.Id ");
        query.append("INNER JOIN PaymentType pt ON pur.PaymentTypeId = pt.Id ");
        query.append("LEFT JOIN Location l ON pur.LocationId = l.Id ");
        query.append("LEFT JOIN Coupon c ON pur.CouponId = c.Id ");
        query.append("LEFT JOIN LicencePlate lp ON per.LicencePlateId = lp.Id ");
        query.append("LEFT JOIN PaymentCard pcard ON pur.Id=pcard.PurchaseId ");
        query.append("LEFT JOIN CustomerCard cc ON pcard.CustomerCardId=cc.Id ");
        query.append("LEFT JOIN CustomerCardType cct ON cc.CustomerCardTypeId=cct.Id ");
        if (ReportingConstants.REPORT_TYPE_TRANSACTION_SMART_CARD == reportType) {
            query.append("LEFT JOIN PaymentSmartCard pscard ON pur.Id=pscard.PurchaseId ");
        }
        if (ReportingConstants.REPORT_TYPE_TRANSACTION_CREDIT_CARD == reportType
            || ReportingConstants.REPORT_TYPE_TRANSACTION_VALUE_CARD == reportType) {
            query.append("LEFT JOIN ProcessorTransactionType proctranstype ON proctrans.ProcessorTransactionTypeId=proctranstype.Id ");
        }
        
        if (!isOverallSummary && ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == filterTypeId
            && this.reportDefinition.isIsCsvOrPdf()) {
            query.append("LEFT JOIN RoutePOS rpos ON pos.Id=rpos.PointOfSaleId ");
            query.append("LEFT JOIN Route r ON rpos.RouteId=r.Id ");
        }
        
        if (!isOverallSummary) {
            query.append("LEFT JOIN UnifiedRate ur ON ur.CustomerId = pos.CustomerId AND pur.UnifiedRateId = ur.Id ");
        }
        
        // Filter for hidden POS
        getFromHiddenPointOfSales(query, "pos");
        
    }
    
    private void getTableJoinToProcessorTransactionFromTransaction(final StringBuilder query, final boolean generateReport, final int filterTypeId) {
        final int reportType = this.reportDefinition.getReportType().getId();
        
        final int cardNumberFilterTypeId = super.getFilterTypeId(this.reportDefinition.getCardNumberFilterTypeId());
        final int cardMerchantAccountId = super.getFilterTypeId(this.reportDefinition.getCardMerchantAccountId());
        final int cardTypeId = super.getFilterTypeId(this.reportDefinition.getCardTypeId());
        int statusFilterTypeId = super.getFilterTypeId(this.reportDefinition.getApprovalStatusFilterTypeId());
        
        if (ReportingConstants.REPORT_TYPE_TRANSACTION_CREDIT_CARD == reportType
            && (!(ReportingConstants.REPORT_FILTER_CARD_NUMBER_ALL == cardNumberFilterTypeId
                  && ReportingConstants.REPORT_SELECTION_ALL == cardMerchantAccountId && ReportingConstants.REPORT_SELECTION_ALL == cardTypeId
                  && ReportingConstants.REPORT_FILTER_APPROVAL_STATUS_ALL == statusFilterTypeId))) {
            statusFilterTypeId = super.getFilterTypeId(this.reportDefinition.getApprovalStatusFilterTypeId());
            if (ReportingConstants.REPORT_FILTER_APPROVAL_STATUS_OFFLINE == statusFilterTypeId) {
                query.append("LEFT JOIN ");
            } else {
                query.append("INNER JOIN ");
            }
            query.append("ProcessorTransaction proctrans ");
            query.append("ON pur.PointOfSaleId=proctrans.PointOfSaleId ");
            query.append("AND pur.PurchaseGMT=proctrans.PurchasedDate AND pur.PurchaseNumber=proctrans.TicketNumber  ");
            query.append("AND (proctrans.IsApproved=1) ");
            if (ReportingConstants.REPORT_FILTER_APPROVAL_STATUS_TO_BE_SETTLED == statusFilterTypeId) {
                query.append(" INNER JOIN ElavonTransactionDetail etd ON etd.ProcessorTransactionId=proctrans.Id ");
            }
            getWhereProcTransTypeOrTransSettleStatusType(query);
            if (ReportingConstants.REPORT_FILTER_APPROVAL_STATUS_TO_BE_SETTLED != statusFilterTypeId) {
                query.append(" LEFT JOIN ElavonTransactionDetail etd ON etd.ProcessorTransactionId=proctrans.Id ");
                query.append(" LEFT JOIN TransactionSettlementStatusType tst on tst.id = etd.TransactionSettlementStatusTypeId ");
            } else {
                query.append(" INNER JOIN TransactionSettlementStatusType tst on tst.id = etd.TransactionSettlementStatusTypeId ");
            }
        } else if (ReportingConstants.REPORT_TYPE_TRANSACTION_VALUE_CARD == reportType
                   && (!(ReportingConstants.REPORT_SELECTION_ALL == cardMerchantAccountId
                         && ReportingConstants.REPORT_FILTER_APPROVAL_STATUS_ALL == statusFilterTypeId))) {
            
            query.append("INNER JOIN ProcessorTransaction proctrans ");
            query.append("ON pur.PointOfSaleId=proctrans.PointOfSaleId ");
            query.append("AND pur.PurchaseGMT=proctrans.PurchasedDate AND pur.PurchaseNumber=proctrans.TicketNumber  ");
            query.append("AND (proctrans.IsApproved=1) ");
            getWhereProcTransTypeOrTransSettleStatusType(query);
        } else if (ReportingConstants.REPORT_TYPE_TRANSACTION_CREDIT_CARD == reportType
                   || ReportingConstants.REPORT_TYPE_TRANSACTION_VALUE_CARD == reportType) {
            query.append("LEFT JOIN ProcessorTransaction proctrans ");
            query.append("ON pur.PointOfSaleId=proctrans.PointOfSaleId ");
            query.append("AND pur.PurchaseGMT=proctrans.PurchasedDate AND pur.PurchaseNumber=proctrans.TicketNumber  ");
            query.append("AND (proctrans.IsApproved=1) ");
            getWhereProcTransTypeOrTransSettleStatusType(query);
            query.append(" LEFT JOIN ElavonTransactionDetail etd ON etd.ProcessorTransactionId=proctrans.Id ");
            query.append(" LEFT JOIN TransactionSettlementStatusType tst on tst.id = etd.TransactionSettlementStatusTypeId ");
        }
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_MERCHANT_ACCOUNT == filterTypeId) {
            query.append("LEFT JOIN MerchantAccount ma ON proctrans.MerchantAccountId = ma.Id ");
        }
    }
    
    private void getWhereProcTransTypeOrTransSettleStatusType(final StringBuilder query) {
        final int transType = super.getFilterTypeId(this.reportDefinition.getOtherParametersTypeFilterTypeId());
        final int reportType = this.reportDefinition.getReportType().getId();
        final int approvalType = super.getFilterTypeId(this.reportDefinition.getApprovalStatusFilterTypeId());
        
        final StringBuilder bdr = new StringBuilder();
        bdr.append(EXISTS_SELECT_PROCESSOR_TRANSACTION_TYPE_OPEN);
        
        if (ReportingConstants.REPORT_TYPE_TRANSACTION_CC_REFUND == reportType) {
            if (ReportingConstants.REPORT_FILTER_TRANSACTION_REFUND == transType) {
                bdr.append("IN (4,15,18,22) ");
            } else if (ReportingConstants.REPORT_FILTER_TRANSACTION_EXTEND_BY_PHONE == transType) {
                bdr.append("IN (18) ");
            }
        } else if (ReportingConstants.REPORT_TYPE_TRANSACTION_CC_PROCESSING == reportType) {
            // All
            if (ReportingConstants.REPORT_FILTER_TRANSACTION_ALL == transType) {
                bdr.append("IN (2,3,4,6,7,10,11,12,13,17,18,19,21,22,23) ");
            } else if (ReportingConstants.REPORT_FILTER_TRANSACTION_NON_REFUND == transType) {
                bdr.append("IN (2,3,6,7,10,11,12,13,17,19,21,23) ");
            } else if (ReportingConstants.REPORT_FILTER_TRANSACTION_EXTEND_BY_PHONE == transType) {
                bdr.append("IN (17,18,19) ");
            } else {
                bdr.append("IN (4,18,22) ");
            }
        } else {
            if (ReportingConstants.REPORT_FILTER_APPROVAL_STATUS_ALL == approvalType) {
                bdr.append("IN (1,2,3,6,7,8,9,10,11,12,13,14,16,17,19,20,21,22,23) ");
            } else if (ReportingConstants.REPORT_FILTER_APPROVAL_STATUS_REAL_TIME == approvalType) {
                bdr.append("IN (2,6,17,19,21,23) ");
            } else if (ReportingConstants.REPORT_FILTER_APPROVAL_STATUS_PRE_AUTH == approvalType) {
                bdr.append("=1 ");
            } else if (ReportingConstants.REPORT_FILTER_APPROVAL_STATUS_RECOVERABLE == approvalType) {
                bdr.append("=20 ");
            } else if (ReportingConstants.REPORT_FILTER_APPROVAL_STATUS_UNCLOSEABLE == approvalType) {
                bdr.append("=8 ");
            } else if (ReportingConstants.REPORT_FILTER_APPROVAL_STATUS_BATCHED == approvalType) {
                bdr.append("IN (3,7,9,10,11,12,13) ");
            } else if (ReportingConstants.REPORT_FILTER_APPROVAL_STATUS_ENCRYPTION_ERROR == approvalType) {
                bdr.append("=14 ");
            } else if (ReportingConstants.REPORT_FILTER_APPROVAL_STATUS_TO_BE_SETTLED == approvalType) {
                query.append(" AND etd.TransactionSettlementStatusTypeId = ");
                query.append(String.valueOf(CardProcessingConstants.TRANSACTION_SETTLEMENT_STATUS_TYPE_POSTAUTH_ID) + " ");
            }
        }
        bdr.append(EXISTS_SELECT_PROCESSOR_TRANSACTION_TYPE_CLOSE);
        
        // If status is NOT 'To Be Settled' (Elavon), append the EXISTS query to the main query.
        if (ReportingConstants.REPORT_FILTER_APPROVAL_STATUS_TO_BE_SETTLED != approvalType) {
            query.append(bdr);
        }
    }
    
    protected final void getWhereStandardTransaction(final StringBuilder query, final boolean generateReport, final boolean isOverallSummary)
        throws Exception {
        getWhereStandardTransaction(query, 0, generateReport, isOverallSummary);
    }
    
    protected final void getWhereStandardTransaction(final StringBuilder query, final int queryModifier, final boolean generateReport,
        final boolean isOverallSummary) throws Exception {
        query.append(" WHERE ");
        
        // Machine Name (aka. PS ID)
        getWherePointOfSale(query, "pur", true);
        
        //        // paystationGroup ids only for Group by paystation group
        //        if (!isOverallSummary) {
        //            // overall summary does not need paystation group
        //            getWherePointOfSaleRouteId(query, generateReport);
        //        }
        
        // Purchased Date & Time
        super.getWherePurchaseGMT(query, "pur");
        
        // License Plate Number
        getWhereLicensePlateNumber(query);
        
        // Ticket Number
        getWhereTicketNumber(query, "pur");
        
        // Payment Type
        getWherePaymentType(query, queryModifier);
        
        // Stall Number
        getWhereSpaceNumber(query);
        
        // Coupon Number
        super.getWhereCouponNumber(query);
        
        // Transaction Type
        getWhereTransactionType(query, queryModifier);
        
        if (ReportingConstants.REPORT_TYPE_TRANSACTION_CREDIT_CARD == this.reportDefinition.getReportType().getId()
            || ReportingConstants.REPORT_TYPE_TRANSACTION_VALUE_CARD == this.reportDefinition.getReportType().getId()) {
            // Merchant Account
            getWhereCardMerchantAccount(query);
            
            // Card Type
            if (this.reportDefinition.getCardTypeId() != null
                && this.reportDefinition.getCardTypeId().intValue() != ReportingConstants.REPORT_SELECTION_ALL) {
                if (ReportingConstants.REPORT_TYPE_TRANSACTION_CREDIT_CARD == this.reportDefinition.getReportType().getId()) {
                    getWhereCardType(query, "proctrans");
                } else {
                    getWherePassCardType(query);
                }
            }
            
            // Processor Transaction Type
            final int statusFilterTypeId = super.getFilterTypeId(this.reportDefinition.getApprovalStatusFilterTypeId());
            if (ReportingConstants.REPORT_FILTER_APPROVAL_STATUS_OFFLINE == statusFilterTypeId) {
                query.append(" AND ISNULL(proctrans.ProcessorTransactionTypeId) ");
            }
        }
        
        // Filter for hidden POS
        super.getWhereHiddenPointOfSales(query);
        
        // Offline xml transactions
        // getWhereOfflineXmlTransactions(reportsForm, query, "t");
    }
    
    private void getWhereLicensePlateNumber(final StringBuilder query) {
        final String plateNumber = this.reportDefinition.getLicencePlateFilter();
        if (plateNumber != null && !plateNumber.isEmpty()) {
            query.append(" AND lp.Number='");
            query.append(plateNumber).append("'");
        }
    }
    
    protected final void getWhereTicketNumber(final StringBuilder query, final String tableName) {
        final int ticketType = super.getFilterTypeId(this.reportDefinition.getTicketNumberFilterTypeId());
        
        if (ReportingConstants.REPORT_FILTER_TICKET_NUMBER_ALL != ticketType) {
            if ("pur".equals(tableName)) {
                query.append(" AND pur.PurchaseNumber");
            } else {
                query.append(" AND ");
                query.append(tableName);
                query.append(".TicketNumber");
            }
            if (ReportingConstants.REPORT_FILTER_TICKET_NUMBER_SPECIFIC == ticketType) {
                query.append("=");
                query.append(this.reportDefinition.getTicketNumberBeginFilter());
            } else if (ReportingConstants.REPORT_FILTER_TICKET_NUMBER_BETWEEN == ticketType) {
                query.append(" BETWEEN ");
                query.append(this.reportDefinition.getTicketNumberBeginFilter());
                query.append(" AND ");
                query.append(this.reportDefinition.getTicketNumberEndFilter());
            }
        }
    }
    
    private void getWherePaymentType(final StringBuilder query, final int queryModifier) {
        
        final int reportType = this.reportDefinition.getReportType().getId();
        
        // Non-union query
        if (queryModifier == 0) {
            switch (reportType) {
                case ReportingConstants.REPORT_TYPE_TRANSACTION_ALL:
                    break;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_CASH:
                    query.append(" AND (pur.PaymentTypeId IN(");
                    query.append(WebCoreConstants.PAYMENT_TYPE_CASH);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_SWIPE);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CL);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CH);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CASH_SMART);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CASH_VALUE);
                    query.append("))");
                    break;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_CREDIT_CARD:
                    query.append(" AND (pur.PaymentTypeId IN (");
                    query.append(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_SWIPE);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CHIP);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_EXTERNAL);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_SWIPE);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CL);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CH);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_FSWIPE);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CARD_FSWIPE);                    
                    
                    query.append(")) ");
                    break;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_CC_REFUND:
                    query.append(" AND (pur.PaymentTypeId IN (");
                    query.append(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_SWIPE);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CHIP);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_EXTERNAL);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_SWIPE);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CL);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CH);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_FSWIPE);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CARD_FSWIPE);                    
                    query.append(")) ");
                    break;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_CC_PROCESSING:
                    query.append(" AND (pur.PaymentTypeId IN (");
                    query.append(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_SWIPE);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CHIP);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_EXTERNAL);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_SWIPE);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CL);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CH);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_VALUE_CARD);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CASH_VALUE);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_FSWIPE);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CARD_FSWIPE);                    
                    query.append(")) ");
                    break;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_VALUE_CARD:
                    query.append(" AND (pur.PaymentTypeId IN (");
                    query.append(WebCoreConstants.PAYMENT_TYPE_VALUE_CARD);
                    query.append(StandardConstants.STRING_COMMA);
                    query.append(WebCoreConstants.PAYMENT_TYPE_CASH_VALUE);
                    query.append("))");
                    break;
                default:
                    break;
            }
            // Union Query - 1st Part
        } else if (queryModifier == 1) {
            if (ReportingConstants.REPORT_TYPE_TRANSACTION_SMART_CARD == reportType) {
                query.append(" AND (pur.PaymentTypeId IN (");
                query.append(WebCoreConstants.PAYMENT_TYPE_SMART_CARD);
                query.append(StandardConstants.STRING_COMMA);
                query.append(WebCoreConstants.PAYMENT_TYPE_CASH_SMART);
                query.append("))");
            } else if (ReportingConstants.REPORT_TYPE_TRANSACTION_PATROLLER_CARD == reportType) {
                query.append(" AND (pur.PaymentTypeId=");
                query.append(WebCoreConstants.PAYMENT_TYPE_PATROLLER_VALUE);
                query.append(")");
            }
            // Union Query - 2nd Part
        } else if (queryModifier == 2) {
            if (ReportingConstants.REPORT_TYPE_TRANSACTION_PATROLLER_CARD == reportType) {
                query.append(" AND (pur.PaymentTypeId=");
                query.append(WebCoreConstants.PAYMENT_TYPE_CASH);
                query.append(")");
            }
        }
    }
    
    private void getWhereSpaceNumber(final StringBuilder query) {
        final int spaceNumberType = super.getFilterTypeId(this.reportDefinition.getSpaceNumberFilterTypeId());
        switch (spaceNumberType) {
            case 0:
            case ReportingConstants.REPORT_FILTER_SPACE_NUMBER_NA:
                break;
            case ReportingConstants.REPORT_FILTER_SPACE_NUMBER_NO_STALL:
                query.append(" AND per.SpaceNumber=0 ");
                break;
            case ReportingConstants.REPORT_FILTER_SPACE_NUMBER_ALL:
                query.append(" AND per.SpaceNumber>0 ");
                break;
            case ReportingConstants.REPORT_FILTER_SPACE_NUMBER_EQUAL:
                query.append(" AND per.SpaceNumber=");
                query.append(this.reportDefinition.getSpaceNumberBeginFilter());
                break;
            case ReportingConstants.REPORT_FILTER_SPACE_NUMBER_GREATER:
                query.append(" AND per.SpaceNumber>=");
                query.append(this.reportDefinition.getSpaceNumberBeginFilter());
                break;
            case ReportingConstants.REPORT_FILTER_SPACE_NUMBER_LESS:
                query.append(" AND (per.SpaceNumber BETWEEN 1 AND ");
                query.append(this.reportDefinition.getSpaceNumberBeginFilter());
                query.append(")");
                break;
            case ReportingConstants.REPORT_FILTER_SPACE_NUMBER_BETWEEN:
                query.append(" AND (per.SpaceNumber BETWEEN ");
                query.append(this.reportDefinition.getSpaceNumberBeginFilter());
                query.append(" AND ");
                query.append(this.reportDefinition.getSpaceNumberEndFilter());
                query.append(")");
                break;
            default:
                break;
        }
    }
    
    private void getWhereTransactionType(final StringBuilder query, final int queryModifier) {
        final int transType = super.getFilterTypeId(this.reportDefinition.getOtherParametersTypeFilterTypeId());
        final int reportType = this.reportDefinition.getReportType().getId();
        
        // Non-union query
        if (queryModifier == 0) {
            switch (transType) {
                case ReportingConstants.REPORT_FILTER_TRANSACTION_ALL:
                    break;
                case ReportingConstants.REPORT_FILTER_TRANSACTION_REGULAR:
                    query.append(" AND pur.TransactionTypeId IN (" + ReportingConstants.TRANSACTION_TYPE_REGULAR);
                    query.append(", " + ReportingConstants.TRANSACTION_TYPE_EXTEND_BY_PHONE_REGULAR_VALUE + ")");
                    if (ReportingConstants.REPORT_TYPE_TRANSACTION_SMART_CARD == reportType) {
                        query.append(" AND (pur.PaymentTypeId IN (");
                        query.append(WebCoreConstants.PAYMENT_TYPE_SMART_CARD);
                        query.append(StandardConstants.STRING_COMMA);
                        query.append(WebCoreConstants.PAYMENT_TYPE_CASH_SMART);
                        query.append("))");
                    } else if (ReportingConstants.REPORT_TYPE_TRANSACTION_PATROLLER_CARD == reportType) {
                        query.append(" AND (pur.PaymentTypeId=");
                        query.append(WebCoreConstants.PAYMENT_TYPE_PATROLLER_VALUE);
                        query.append(")");
                    }
                    break;
                case ReportingConstants.REPORT_FILTER_TRANSACTION_ADD_TIME:
                    query.append(" AND pur.TransactionTypeId IN (" + ReportingConstants.TRANSACTION_TYPE_ADDTIME);
                    query.append(", " + ReportingConstants.TRANSACTION_TYPE_EXTEND_BY_PHONE_ADDTIME_VALUE + ")");
                    if (ReportingConstants.REPORT_TYPE_TRANSACTION_SMART_CARD == reportType) {
                        query.append(" AND (pur.PaymentTypeId IN (");
                        query.append(WebCoreConstants.PAYMENT_TYPE_SMART_CARD);
                        query.append(StandardConstants.STRING_COMMA);
                        query.append(WebCoreConstants.PAYMENT_TYPE_CASH_SMART);
                        query.append("))");
                    } else if (ReportingConstants.REPORT_TYPE_TRANSACTION_PATROLLER_CARD == reportType) {
                        query.append(" AND (pur.PaymentTypeId=");
                        query.append(WebCoreConstants.PAYMENT_TYPE_PATROLLER_VALUE);
                        query.append(")");
                    }
                    break;
                case ReportingConstants.REPORT_FILTER_TRANSACTION_SC_RECHARGE:
                    query.append(" AND pur.TransactionTypeId=" + ReportingConstants.TRANSACTION_TYPE_SC_RECHARGE);
                    break;
                //TODO To be added when Citations are implemented
                //                case ReportingConstants.REPORT_FILTER_TRANSACTION_CITATION:
                //                    query.append(" AND pur.TransactionTypeId=" + ReportingConstants.TRANSACTION_TYPE_CITATION);
                //                    break;
                case ReportingConstants.REPORT_FILTER_TRANSACTION_TEST:
                    query.append(" AND pur.TransactionTypeId=" + ReportingConstants.TRANSACTION_TYPE_TEST);
                    break;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_PATROLLER_CARD:
                    query.append(" AND (pur.PaymentTypeId=");
                    query.append(WebCoreConstants.PAYMENT_TYPE_PATROLLER_VALUE);
                    query.append(")");
                    break;
                case ReportingConstants.REPORT_FILTER_TRANSACTION_DEPOSIT:
                    query.append(" AND pur.TransactionTypeId=" + ReportingConstants.TRANSACTION_TYPE_DEPOSIT);
                    if (ReportingConstants.REPORT_TYPE_TRANSACTION_PATROLLER_CARD == reportType) {
                        query.append(" AND (pur.PaymentTypeId=");
                        query.append(WebCoreConstants.PAYMENT_TYPE_PATROLLER_VALUE);
                        query.append(")");
                    }
                    break;
                case ReportingConstants.REPORT_FILTER_TRANSACTION_MONTHLY:
                    query.append(" AND pur.TransactionTypeId=" + ReportingConstants.TRANSACTION_TYPE_MONTHLY);
                    if (ReportingConstants.REPORT_TYPE_TRANSACTION_SMART_CARD == reportType) {
                        query.append(" AND (pur.PaymentTypeId IN (");
                        query.append(WebCoreConstants.PAYMENT_TYPE_SMART_CARD);
                        query.append(StandardConstants.STRING_COMMA);
                        query.append(WebCoreConstants.PAYMENT_TYPE_CASH_SMART);
                        query.append("))");
                    } else if (ReportingConstants.REPORT_TYPE_TRANSACTION_PATROLLER_CARD == reportType) {
                        query.append(" AND (pur.PaymentTypeId=");
                        query.append(WebCoreConstants.PAYMENT_TYPE_PATROLLER_VALUE);
                        query.append(")");
                    }
                    break;
                case ReportingConstants.REPORT_FILTER_TRANSACTION_REPLENISH:
                    query.append(" AND pur.TransactionTypeId IN (");
                    query.append(ReportingConstants.TRANSACTION_TYPE_REPLENISH);
                    query.append(", ");
                    query.append(ReportingConstants.TRANSACTION_TYPE_HOPPER1ADD);
                    query.append(", ");
                    query.append(ReportingConstants.TRANSACTION_TYPE_HOPPER2ADD);
                    query.append(") ");
                    break;
                case ReportingConstants.REPORT_FILTER_TRANSACTION_EXTEND_BY_PHONE:
                    query.append(" AND pur.TransactionTypeId IN (");
                    query.append(ReportingConstants.TRANSACTION_TYPE_EXTEND_BY_PHONE_ADDTIME_VALUE);
                    query.append(", ");
                    query.append(ReportingConstants.TRANSACTION_TYPE_EXTEND_BY_PHONE_REGULAR_VALUE);
                    query.append(") ");
                    break;
                default:
                    break;
            }
            // Union Query - 1st Part
        } else if (queryModifier == 1) {
            // Do nothing
            // Union Query - 2nd Part
        } else if (queryModifier == 2) {
            if (ReportingConstants.REPORT_TYPE_TRANSACTION_SMART_CARD == reportType) {
                query.append(" AND (pur.TransactionTypeId=");
                query.append(ReportingConstants.TRANSACTION_TYPE_SC_RECHARGE);
                query.append(")");
            } else if (ReportingConstants.REPORT_TYPE_TRANSACTION_PATROLLER_CARD == reportType) {
                query.append(" AND (pur.TransactionTypeId=");
                query.append(ReportingConstants.TRANSACTION_TYPE_DEPOSIT);
                query.append(")");
            }
        }
    }
    
    private void getWhereCardMerchantAccount(final StringBuilder query) {
        final int merchantAccountId = super.getFilterTypeId(this.reportDefinition.getCardMerchantAccountId());
        if (ReportingConstants.REPORT_SELECTION_ALL == merchantAccountId) {
            // do nothing
        } else {
            query.append(" AND proctrans.MerchantAccountId=" + merchantAccountId);
        }
    }
    
    protected final void getWhereCardType(final StringBuilder query, final String tableName) {
        final int cardType = super.getFilterTypeId(this.reportDefinition.getCardTypeId());
        
        switch (cardType) {
            case ReportingConstants.REPORT_SELECTION_ALL:
                break;
            case ReportingConstants.CREDIT_CARD_TYPE_AMEX:
            case ReportingConstants.CREDIT_CARD_TYPE_DISCOVER:
            case ReportingConstants.CREDIT_CARD_TYPE_MASTERCARD:
            case ReportingConstants.CREDIT_CARD_TYPE_VISA:
            case ReportingConstants.CREDIT_CARD_TYPE_DINERS:
                query.append(" AND ");
                query.append(tableName);
                query.append(".CardType='" + this.creditCardTypeService.getText(cardType) + "'");
                break;
            default:
                query.append(" AND pcard.CustomerCardId='");
                query.append(cardType);
                query.append("'");
                break;
        }
    }
    
    protected final void getWherePassCardType(final StringBuilder query) {
        final int cardType = super.getFilterTypeId(this.reportDefinition.getCardTypeId());
        
        query.append(" AND (cct.Id='");
        query.append(cardType);
        query.append("' OR cct.ParentCustomerCardTypeId='");
        query.append(cardType);
        query.append("')");
    }
    
    private void getWhereCardMerchantAccountProcessorTransaction(final StringBuilder query) throws Exception {
        final int merchantAccountId = super.getFilterTypeId(this.reportDefinition.getCardMerchantAccountId());
        if (ReportingConstants.REPORT_SELECTION_ALL == merchantAccountId) {
            final List<MerchantAccount> merchantAccounts =
                    this.merchantAccountService.findAllMerchantAccountsForReportsByCustomerId(this.reportDefinition.getCustomer().getId());
            
            if (merchantAccounts == null || merchantAccounts.isEmpty()) {
                // no merchant accounts, search for < 0 to make query faster
                // query.append(" AND proctrans.MerchantAccountId<0 "); // This is comment to allow parent customer to generate report
            } else {
                query.append(" AND proctrans.MerchantAccountId IN(");
                boolean prependComma = false;
                for (MerchantAccount ma : merchantAccounts) {
                    if (prependComma) {
                        query.append(StandardConstants.STRING_COMMA);
                    } else {
                        prependComma = true;
                    }
                    query.append(ma.getId());
                }
                query.append(") ");
            }
        } else {
            query.append(" AND proctrans.MerchantAccountId=" + merchantAccountId);
        }
    }
    
    private String getTimeZoneOffsetString() {
        TimeZone timezone = TimeZone.getTimeZone(this.timeZone);
        if (timezone == null) {
            timezone = TimeZone.getDefault();
        }
        
        Date datetime = null;
        if (this.reportQueue.getPrimaryDateRangeBeginGmt() != null) {
            datetime = this.reportQueue.getPrimaryDateRangeBeginGmt();
        } else {
            datetime = this.reportQueue.getExecutionBeginGmt();
        }
        
        final int offsetHour = timezone.getOffset(datetime.getTime()) / 1000 / 60 / 60;
        final int absOffsetHour = Math.abs(offsetHour);
        final StringBuffer sb = new StringBuffer();
        if (offsetHour < 0) {
            sb.append("-");
        } else {
            sb.append("+");
        }
        if (absOffsetHour < 10) {
            sb.append("0");
        }
        sb.append(Integer.toString(absOffsetHour)).append(":00");
        return sb.toString();
    }
    
    protected final void getOrderByStandard(final StringBuilder query, final boolean isSummary) {
        final int groupingType = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        
        if (isSummary) {
            if (ReportingConstants.REPORT_FILTER_GROUP_BY_NONE != groupingType) {
                query.append(" ORDER BY GroupOrderValue ");
            }
            return;
        }
        
        query.append(" ORDER BY ");
        
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_DAY == groupingType || ReportingConstants.REPORT_FILTER_GROUP_BY_MONTH == groupingType
            || ReportingConstants.REPORT_FILTER_GROUP_BY_NONE == groupingType) {
            query.append("PurchasedDate ");
        } else {
            query.append("GroupField, PurchasedDate ");
        }
    }
    
    protected final void getTablesCardProcessingAndRefunds(final StringBuilder query, final boolean generateReport, final boolean isOverallSummary) {
        final int groupType = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        final int reportType = this.reportDefinition.getReportType().getId();
        query.append(" FROM ProcessorTransaction proctrans ");
        
        // Add 'use index (idx_processortransaction_processingdate)' ONLY for Card Processing.
        if (ReportingConstants.REPORT_TYPE_TRANSACTION_CC_PROCESSING == reportType) {
            query.append(ReportingConstants.SQL_USE_IDX_PROCESSOR_TRANSACTION_PROCESSING_DATE);
        }
        
        getTableJoinToTransactionFromProcessorTransaction(query, generateReport);
        query.append("INNER JOIN PointOfSale pos ON proctrans.PointOfSaleId = pos.Id ");
        
        query.append("INNER JOIN ProcessorTransactionType proctranstype ON proctrans.ProcessorTransactionTypeId=proctranstype.Id ");
        query.append("LEFT JOIN PaymentType pt ON pur.PaymentTypeId=pt.Id ");
        query.append("LEFT JOIN Location l ON pur.LocationId = l.Id ");
        query.append("LEFT JOIN Coupon c ON pur.CouponId = c.Id ");
        query.append("LEFT JOIN LicencePlate lp ON per.LicencePlateId = lp.Id ");
        
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_LOCATION == groupType) {
            query.append("LEFT JOIN Location l2 ON pos.LocationId = l2.Id ");
        }
        
        query.append("LEFT JOIN MerchantAccount m ON proctrans.MerchantAccountId=m.id ");
        
        if (ReportingConstants.REPORT_TYPE_TRANSACTION_CC_REFUND == reportType) {
            query.append("LEFT JOIN TransactionType tt ON pur.TransactionTypeId=tt.Id ");
        }
        
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == groupType) {
            query.append("LEFT JOIN RoutePOS rpos ON pos.Id=rpos.PointOfSaleId ");
            query.append("LEFT JOIN Route r ON rpos.RouteId=r.Id ");
        }
        
        if (!isOverallSummary) {
            query.append("LEFT JOIN UnifiedRate ur ON ur.CustomerId = pos.CustomerId AND pur.UnifiedRateId = ur.Id ");
        }
        
        // Filter for hidden POS
        getFromHiddenPointOfSales(query, "pos");
        
    }
    
    private void getTableJoinToTransactionFromProcessorTransaction(final StringBuilder query, final boolean generateReport) {
        final int couponNumFilterId = super.getFilterTypeId(this.reportDefinition.getCouponNumberFilterTypeId());
        final int spaceNumFilterId = super.getFilterTypeId(this.reportDefinition.getSpaceNumberFilterTypeId());
        
        if (!(ReportingConstants.REPORT_FILTER_COUPON_NUMBER_NA == couponNumFilterId
              && ReportingConstants.REPORT_FILTER_SPACE_NUMBER_NA == spaceNumFilterId)) {
            query.append("INNER JOIN Purchase pur ON proctrans.PointOfSaleId=pur.PointOfSaleId AND ");
        } else {
            query.append("LEFT JOIN Purchase pur ON proctrans.PointOfSaleId=pur.PointOfSaleId AND ");
        }
        query.append("proctrans.PurchasedDate=pur.PurchaseGMT AND proctrans.TicketNumber=pur.PurchaseNumber ");
        query.append("LEFT JOIN Permit per ON pur.Id = per.PurchaseId ");
        // Offline xml transactions
        // getWhereOfflineXmlTransactions(reportsForm, query, "t");
    }
    
    protected final void getWhereStandardProcessorTransaction(final StringBuilder query, final boolean generateReport) throws Exception {
        query.append(" WHERE ");
        
        // Machine Name (aka. PS ID)
        super.getWherePointOfSale(query, "pos", true);
        
        // Purchased Date & Time
        super.getWherePurchaseGMT(query, "proctrans");
        
        // License Plate Number
        this.getWhereLicensePlateNumber(query);
        
        // Ticket Number
        getWhereTicketNumber(query, "proctrans");
        
        // We only want approved transactions
        query.append(" AND (proctrans.IsApproved=1)");
        
        // Processing Status
        getWhereProcTransTypeOrTransSettleStatusType(query);
        
        if (this.reportQueue.getPrimaryDateRangeBeginGmt() != null && this.reportQueue.getPrimaryDateRangeEndGmt() != null) {
            // Processing Date/Time
            query.append(" AND proctrans.ProcessingDate BETWEEN '");
            query.append(this.reportQueue.getPrimaryDateRangeBeginGmt());
            query.append("' AND '");
            query.append(this.reportQueue.getPrimaryDateRangeEndGmt());
            query.append("' ");
        }
        // Stall Number
        getWhereSpaceNumber(query);
        
        // Coupon Number
        getWhereCouponNumber(query);
        
        // Merchant Account
        getWhereCardMerchantAccountProcessorTransaction(query);
        
        // Card Type
        getWhereCardType(query, "proctrans");
        
        // Filter for hidden POS
        super.getWhereHiddenPointOfSales(query);
        
        // offline xml transactions
        // getWhereOfflineXmlTransactions(reportsForm, query, "t");
    }
    
    protected final void getFieldsValueCard(final StringBuilder query, final boolean isSummary) {
        if (ReportingConstants.REPORT_TYPE_TRANSACTION_VALUE_CARD == this.reportDefinition.getReportType().getId()) {
            if (!isSummary) {
                query.append(", IF(ISNULL(proctrans.AuthorizationNumber), 'N/A', SUBSTRING_INDEX(proctrans.AuthorizationNumber, ':', 1)) AS AuthorizationNumber");
                
                query.append(", cct.Name AS CustomCardType, cc.CardNumber AS CustomCardData");
            }
        }
        if (isSummary) {
            if ("SELECT".equalsIgnoreCase(query.toString().trim())) {
                query.append("SUM(IF((pur.PaymentTypeId=7 OR pur.PaymentTypeId=9) AND pur.CardPaidAmount > 0, 1, 0)) AS CustomCardCollectionsN");
            } else {
                query.append(", SUM(IF((pur.PaymentTypeId=7 OR pur.PaymentTypeId=9) AND pur.CardPaidAmount > 0, 1, 0)) AS CustomCardCollectionsN");
            }
            query.append(", SUM(IF(pur.PaymentTypeId=7 OR pur.PaymentTypeId=9, pur.CardPaidAmount, 0)) AS CustomCardCollections");
            query.append(", SUM(IF((pur.PaymentTypeId=7 OR pur.PaymentTypeId=9) AND pur.CardPaidAmount > 0, 1, 0)) AS CustomCardRevenueN");
            query.append(", SUM(IF(pur.PaymentTypeId=7 OR pur.PaymentTypeId=9, pur.CardPaidAmount, 0)) AS CustomCardRevenue");
        } else {
            query.append(", IF(pur.PaymentTypeId=7 OR pur.PaymentTypeId=9, pur.CardPaidAmount, 0) AS CustomCardCollections");
            query.append(", IF(pur.PaymentTypeId=7 OR pur.PaymentTypeId=9, pur.CardPaidAmount, 0) AS CustomCardRevenue");
        }
    }
    
    protected final void getSummaryFieldTotalTransaction(final StringBuilder query, final boolean isSummary) {
        if (isSummary) {
            query.append(", COUNT(*) AS TransactionN");
        }
    }
    
    protected final String createReportName() {
        final StringBuilder sb = new StringBuilder();
        
        switch (this.reportDefinition.getReportType().getId()) {
            case ReportingConstants.REPORT_TYPE_TRANSACTION_ALL:
                sb.append(reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_TRANSACTION_ALL));
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_CASH:
                sb.append(reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_TRANSACTION_CASH));
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_CC_PROCESSING:
                sb.append(reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_TRANSACTION_CC_PROCCESSING));
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_CREDIT_CARD:
                sb.append(reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_TRANSACTION_CREDIT_CARD));
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_CC_REFUND:
                sb.append(reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_TRANSACTION_CC_REFUND));
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_CC_RETRY:
                sb.append(reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_TRANSACTION_CC_RETRY));
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_PATROLLER_CARD:
                sb.append(reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_TRANSACTION_PATROLLER_CARD));
                break;
            /*
             * TODO case ReportingConstants.REPORT_TYPE_TRANSACTION_RATE:
             * sb.append(reportingUtil.getPropertyEN(ReportingConstants.
             * REPORT_TYPE_NAME_TRANSACTION_)); break; case
             * ReportingConstants.REPORT_TYPE_TRANSACTION_RATE_SUMMARY:
             * sb.append(reportingUtil.getPropertyEN(ReportingConstants.
             * REPORT_TYPE_NAME_TRANSACTION_)); break;
             */
            case ReportingConstants.REPORT_TYPE_TRANSACTION_SMART_CARD:
                sb.append(reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_TRANSACTION_SMART_CARD));
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_VALUE_CARD:
                sb.append(reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_TRANSACTION_VALUE_CARD));
                break;
            default:
                break;
        }
        
        if (this.reportDefinition.isIsDetailsOrSummary()) {
            sb.append(" ");
            sb.append(reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_ADD_SUMMARY));
        }
        return sb.toString();
    }
    
    @Override
    protected final String createFileName() {
        final StringBuilder sb = new StringBuilder();
        sb.append(createReportName());
        return super.createFileName(sb);
    }
    
    protected final String createReportParameterTitle() {
        final StringBuffer title = new StringBuffer(reportingUtil.getPropertyEN("label.siteName") + " ");
        title.append(createReportName());
        return title.toString();
    }
    
    protected final String getJasperFileName() {
        if (!this.reportDefinition.isIsDetailsOrSummary()) {
            switch (this.reportDefinition.getReportType().getId()) {
                case ReportingConstants.REPORT_TYPE_TRANSACTION_ALL:
                    return ReportingConstants.REPORT_JASPER_FILE_TRANSACTION_ALL;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_CASH:
                    return ReportingConstants.REPORT_JASPER_FILE_TRANSACTION_CASH;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_CREDIT_CARD:
                    return ReportingConstants.REPORT_JASPER_FILE_TRANSACTION_CREDIT_CARD;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_CC_PROCESSING:
                    return ReportingConstants.REPORT_JASPER_FILE_TRANSACTION_CC_PROCESSING;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_CC_REFUND:
                    return ReportingConstants.REPORT_JASPER_FILE_TRANSACTION_CC_REFUND;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_CC_RETRY:
                    return ReportingConstants.REPORT_JASPER_FILE_TRANSACTION_CC_RETRY;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_PATROLLER_CARD:
                    return ReportingConstants.REPORT_JASPER_FILE_TRANSACTION_PATROLLER_CARD;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_SMART_CARD:
                    return ReportingConstants.REPORT_JASPER_FILE_TRANSACTION_SMART_CARD;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_VALUE_CARD:
                    return ReportingConstants.REPORT_JASPER_FILE_TRANSACTION_VALUE_CARD;
                default:
                    break;
            }
        } else {
            switch (this.reportDefinition.getReportType().getId()) {
                case ReportingConstants.REPORT_TYPE_TRANSACTION_ALL:
                    return ReportingConstants.REPORT_JASPER_FILE_TRANSACTION_ALL_SUMMARY;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_CASH:
                    return ReportingConstants.REPORT_JASPER_FILE_TRANSACTION_CASH_SUMMARY;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_CREDIT_CARD:
                    return ReportingConstants.REPORT_JASPER_FILE_TRANSACTION_CREDIT_CARD_SUMMARY;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_CC_PROCESSING:
                    return ReportingConstants.REPORT_JASPER_FILE_TRANSACTION_CC_PROCESSING_SUMMARY;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_CC_REFUND:
                    return ReportingConstants.REPORT_JASPER_FILE_TRANSACTION_CC_REFUND_SUMMARY;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_CC_RETRY:
                    return ReportingConstants.REPORT_JASPER_FILE_TRANSACTION_CC_RETRY_SUMMARY;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_PATROLLER_CARD:
                    return ReportingConstants.REPORT_JASPER_FILE_TRANSACTION_PATROLLER_CARD_SUMMARY;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_SMART_CARD:
                    return ReportingConstants.REPORT_JASPER_FILE_TRANSACTION_SMART_CARD_SUMMARY;
                case ReportingConstants.REPORT_TYPE_TRANSACTION_VALUE_CARD:
                    return ReportingConstants.REPORT_JASPER_FILE_TRANSACTION_VALUE_CARD_SUMMARY;
                default:
                    break;
            }
        }
        return null;
    }
    
    protected final Map<String, Object> createReportParameterList(final String query, final int customerId) {
        final Map<String, Object> parameters = super.createReportParameterList(query, customerId);
        
        final int reportType = this.reportDefinition.getReportType().getId();
        
        parameters.put("ReportTitle", super.getTitle(createReportParameterTitle()));
        
        if (ReportingConstants.REPORT_TYPE_TRANSACTION_CC_PROCESSING == reportType
            || ReportingConstants.REPORT_TYPE_TRANSACTION_CC_RETRY == reportType) {
            final int priDateFilterId = super.getFilterTypeId(this.reportDefinition.getPrimaryDateFilterTypeId());
            if (ReportingConstants.REPORT_SELECTION_ALL == priDateFilterId) {
                parameters.put("ProcDateString", reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL));
            } else {
                parameters.put("ProcDateString", createReportParameterDateString(this.reportQueue.getPrimaryDateRangeBeginGmt(),
                                                                                 this.reportQueue.getPrimaryDateRangeEndGmt()));
                
            }
            
            final int secDateFilterId = super.getFilterTypeId(this.reportDefinition.getSecondaryDateFilterTypeId());
            if (ReportingConstants.REPORT_SELECTION_ALL == secDateFilterId) {
                parameters.put("DateString", reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL));
            } else {
                parameters.put("DateString", createReportParameterDateString(this.reportQueue.getSecondaryDateRangeBeginGmt(),
                                                                             this.reportQueue.getSecondaryDateRangeEndGmt()));
                
            }
        } else {
            final int priDateFilterId = super.getFilterTypeId(this.reportDefinition.getPrimaryDateFilterTypeId());
            if (ReportingConstants.REPORT_SELECTION_ALL == priDateFilterId) {
                parameters.put("DateString", reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL));
            } else {
                parameters.put("DateString", createReportParameterDateString(this.reportQueue.getPrimaryDateRangeBeginGmt(),
                                                                             this.reportQueue.getPrimaryDateRangeEndGmt()));
                
            }
        }
        
        // Location Parameters
        
        parameters.put("LocationType", reportingUtil.findFilterString(this.reportDefinition.getLocationFilterTypeId()));
        final Customer customer = this.customerAdminService.findCustomerByCustomerId(customerId);
        if (this.customer.isIsParent()) {
            parameters.put("OrganizationName", convertArrayToDelimiterString(this.organizationNameList, ','));
        } else {
            parameters.put("OrganizationName", customer.getName());
        }
        
        parameters.put("MachineNameOrNumber", convertArrayToDelimiterString(this.locationValueNameList, ','));
        
        if (ReportingConstants.REPORT_TYPE_TRANSACTION_CC_RETRY != reportType) {
            parameters.put("SpaceNumberType", reportingUtil.findFilterString(this.reportDefinition.getSpaceNumberFilterTypeId()));
            parameters.put("SpaceNumberLow", this.reportDefinition.getSpaceNumberBeginFilter() != null
                    ? this.reportDefinition.getSpaceNumberBeginFilter().toString() : "");
            parameters.put("SpaceNumberHigh",
                           this.reportDefinition.getSpaceNumberEndFilter() != null ? this.reportDefinition.getSpaceNumberEndFilter().toString() : "");
            
            parameters.put("CouponNumberType", reportingUtil.findFilterString(this.reportDefinition.getCouponNumberFilterTypeId()));
            parameters.put("CouponNumber", this.reportDefinition.getCouponNumberSpecificFilter());
            
            if (ReportingConstants.REPORT_TYPE_TRANSACTION_CREDIT_CARD == reportType
                || ReportingConstants.REPORT_TYPE_TRANSACTION_CC_REFUND == reportType
                || ReportingConstants.REPORT_TYPE_TRANSACTION_CC_PROCESSING == reportType
                || ReportingConstants.REPORT_TYPE_TRANSACTION_VALUE_CARD == reportType) {
                final int merchantAccountId = super.getFilterTypeId(this.reportDefinition.getCardMerchantAccountId());
                
                if (ReportingConstants.REPORT_SELECTION_ALL != merchantAccountId) {
                    final MerchantAccount merchantAccount = this.merchantAccountService.findById(merchantAccountId);
                    parameters.put("MerchantAccount", merchantAccount.getUIName());
                } else {
                    parameters.put("MerchantAccount", reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL));
                }
            }
            if (ReportingConstants.REPORT_TYPE_TRANSACTION_CREDIT_CARD == reportType
                || ReportingConstants.REPORT_TYPE_TRANSACTION_VALUE_CARD == reportType) {
                
                final int statusTypeId = super.getFilterTypeId(this.reportDefinition.getApprovalStatusFilterTypeId());
                parameters.put("AuthStatus",
                               (ReportingConstants.REPORT_SELECTION_ALL == statusTypeId)
                                       ? reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL)
                                       : reportingUtil.findFilterString(this.reportDefinition.getApprovalStatusFilterTypeId()));
            }
            parameters.put("LicensePlateNumber", this.reportDefinition.getLicencePlateFilter());
        }
        
        parameters.put("TicketNumberType", reportingUtil.findFilterString(this.reportDefinition.getTicketNumberFilterTypeId()));
        parameters
                .put("TicketNumberLow",
                     this.reportDefinition.getTicketNumberBeginFilter() != null ? this.reportDefinition.getTicketNumberBeginFilter().toString() : "");
        parameters.put("TicketNumberHigh",
                       this.reportDefinition.getTicketNumberEndFilter() != null ? this.reportDefinition.getTicketNumberEndFilter().toString() : "");
        
        parameters.put("Grouping", reportingUtil.findFilterString(this.reportDefinition.getGroupByFilterTypeId()));
        parameters.put("TransactionType", reportingUtil.findFilterString(this.reportDefinition.getOtherParametersTypeFilterTypeId()));
        
        if (ReportingConstants.REPORT_TYPE_TRANSACTION_VALUE_CARD == reportType) {
            final int customerCardId = super.getFilterTypeId(this.reportDefinition.getCardTypeId());
            if (ReportingConstants.REPORT_SELECTION_ALL != customerCardId) {
                final CustomerCardType customerCardType = this.customerAdminService.findCustomerCardTypeById(customerCardId);
                parameters.put("CardType", customerCardType.getName());
            } else {
                parameters.put("CardType", reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL));
            }
            
        } else if (ReportingConstants.REPORT_TYPE_TRANSACTION_CREDIT_CARD == reportType
                   || ReportingConstants.REPORT_TYPE_TRANSACTION_CC_REFUND == reportType
                   || ReportingConstants.REPORT_TYPE_TRANSACTION_CC_PROCESSING == reportType
                   || ReportingConstants.REPORT_TYPE_TRANSACTION_CC_RETRY == reportType) {
            
            final int cardTypeId = super.getFilterTypeId(this.reportDefinition.getCardTypeId());
            if (ReportingConstants.REPORT_SELECTION_ALL == cardTypeId) {
                parameters.put("CardType", reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL));
            } else {
                parameters.put("CardType", this.creditCardTypeService.getText(cardTypeId));
            }
        }
        parameters.put("CardNumberType", reportingUtil.findFilterString(this.reportDefinition.getCardNumberFilterTypeId()));
        parameters.put("CardNumber", this.reportDefinition.getCardNumberSpecificFilter() != null
                ? this.reportDefinition.getCardNumberSpecificFilter().toString() : "");
        
        parameters.put("IsCsv", !this.reportDefinition.isIsCsvOrPdf());
        parameters.put("IsPdf", this.reportDefinition.isIsCsvOrPdf());
        parameters.put("IsDetails", !this.reportDefinition.isIsDetailsOrSummary());
        parameters.put("IsCsvDetails", !this.reportDefinition.isIsCsvOrPdf() && !this.reportDefinition.isIsDetailsOrSummary());
        parameters.put("IsPdfDetails", this.reportDefinition.isIsCsvOrPdf() && !this.reportDefinition.isIsDetailsOrSummary());
        
        final int groupById = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        parameters.put("IsPdfGrouping", reportDefinition.isIsCsvOrPdf() && ReportingConstants.REPORT_FILTER_GROUP_BY_NONE != groupById);
        
        parameters.put("ZeroFloat", new Float(0.00));
        parameters.put("ZeroInteger", Integer.valueOf(0));
        
        final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (reportDefinition.isIsCsvOrPdf() && ReportingConstants.REPORT_FILTER_GROUP_BY_NONE == filterTypeId) {
            final List<ReportLocationValue> locationList =
                    this.reportLocationValueService.findReportLocationValuesByReportDefinition(this.reportDefinition.getId());
            final String[] rawSelectedGroupIds = getRawSelectedGroupIds(locationList);
            parameters.put("DuplicatedPaystationRouteMap", getAllDuplicatedPointOfSaleRouteMap(customerId, rawSelectedGroupIds));
        } else {
            parameters.put("DuplicatedPaystationRouteMap", new HashMap<Integer, List<Integer>>());
        }
        // CC retry flag
        parameters.put("LostOrStolen", CardProcessingConstants.CC_RETRY_LOST_STOLEN);
        parameters.put("ExceedMaxTry", CardProcessingConstants.CC_RETRY_MAX_EXCEEDED);
        return parameters;
        
    }
    
    // TODO Deprecated??
    // do not show offline xml transactions
    private void getWhereOfflineXmlTransactions(final StringBuilder query, final String tableName) {
        query.append(" AND ");
        query.append(tableName);
        query.append(".IsOffline = 0 ");
    }
    
    final void getFieldsRateName(final StringBuilder query, final boolean isSummary) {
        if (!isSummary) {
            appendCommaSpaceIfNeeded(query);
            query.append("ur.Name AS RateName ");
        }
    }
    
    final void getFieldsLocationName(final StringBuilder query, final boolean isPdf) {
        if (!isPdf && query.indexOf(SELECT_LOCATION_NAME) == StandardConstants.INT_MINUS_ONE) {
            appendCommaSpaceLocationName(query);
        }
    }
    
    final void getFieldsLocationName(final StringBuilder query, final boolean isSummary, final boolean isCCProcessingOrRetry) {
        if (!isSummary && isCCProcessingOrRetry && query.indexOf(SELECT_LOCATION_NAME) == StandardConstants.INT_MINUS_ONE) {
            appendCommaSpaceLocationName(query);
        }
    }
    
    private StringBuilder appendCommaSpaceLocationName(final StringBuilder query) {
        appendCommaSpaceIfNeeded(query);
        query.append(SELECT_LOCATION_NAME).append(StandardConstants.STRING_EMPTY_SPACE);
        return query;
    }
    
    private StringBuilder appendCommaSpaceIfNeeded(final StringBuilder query) {
        if (!query.toString().endsWith(StandardConstants.STRING_COMMA)) {
            query.append(", ");
        }
        return query;
    }
}
