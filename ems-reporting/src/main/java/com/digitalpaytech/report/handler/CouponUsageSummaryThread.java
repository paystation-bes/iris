package com.digitalpaytech.report.handler;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.WebCoreUtil;

public class CouponUsageSummaryThread extends ReportBaseThread {
    private static final Logger LOGGER = Logger.getLogger(CouponUsageSummaryThread.class);
    private static final String SQL_SELECT = "SELECT ";
    
    public CouponUsageSummaryThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue, final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext);
        this.jasperFileName = ReportingConstants.REPORT_JASPER_FILE_COUPON_USAGE_SUMMARY;
    }
    
    @Override
    protected final String generateSQLString() throws Exception {
        if (this.reportDefinition.getCouponTypeFilterTypeId() == null || !validCouponType(this.reportDefinition.getCouponTypeFilterTypeId())) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("No CouponTypeId in ReportDefinition ID: ").append(this.reportDefinition.getId());
            throw new RuntimeException(bdr.toString());
        }
        
        StringBuilder query = null;
        if (this.reportDefinition.getCouponTypeFilterTypeId() == ReportingConstants.REPORT_FILTER_COUPON_TYPE_ALL) {
            // Percent query
            final String percentQuery = generateSQLString(ReportingConstants.REPORT_FILTER_COUPON_TYPE_PERCENTAGE).toString();
            
            // Dollar query
            final String dollarQuery = generateSQLString(ReportingConstants.REPORT_FILTER_COUPON_TYPE_DOLLAR_BASED).toString();
            
            // 0 discount query
            final String zeroQuery = generateZeroDiscountSQLString().toString();
            
            final String offlineQuery = generateOfflineSQLString().toString();
            
            // Add 'union' keyword.
            query = new StringBuilder();
            query.append("SELECT * FROM ( ").append(dollarQuery).append(" ) A   UNION   ");
            query.append("SELECT * FROM ( ").append(percentQuery).append(" ) A   UNION   ");
            query.append("SELECT * FROM ( ").append(zeroQuery).append(" ) A UNION ");
            query.append("SELECT * FROM ( ").append(offlineQuery).append(" ) A; ");
            
        } else {
            query = generateSQLString(this.reportDefinition.getCouponTypeFilterTypeId());
        }
        return query.toString();
    }
    
    private StringBuilder generateZeroDiscountSQLString() {
        final StringBuilder query = new StringBuilder(SQL_SELECT);
        getFieldsStandardForPercentDiscountNoConcat(query);
        getTableString(query);
        getWhereString(query, false, false);
        addWhereStandardForZeroPercentAndDollarDiscount(query);
        getOrderByString(query);
        return query;
    }
    
    private StringBuilder generateOfflineSQLString() {
        final StringBuilder query = new StringBuilder(SQL_SELECT);
        getFieldsStandardForOffline(query);
        getTableString(query);
        getWhereString(query, false, true);
        getOrderByString(query);
        return query;
    }
    
    private StringBuilder generateSQLString(final int couponType) {
        final StringBuilder sqlQuery = new StringBuilder(SQL_SELECT);
        // Fields - dollar or percent (default)
        if (couponType == ReportingConstants.REPORT_FILTER_COUPON_TYPE_DOLLAR_BASED) {
            getFieldsStandardForDollarDiscount(sqlQuery);
        } else {
            getFieldsStandardForPercentDiscount(sqlQuery);
        }
        getTableString(sqlQuery);
        getWhereString(sqlQuery, false, false);
        
        // Where - dollar AND percent (default)
        if (couponType == ReportingConstants.REPORT_FILTER_COUPON_TYPE_DOLLAR_BASED) {
            addWhereStandardForDollarDiscount(sqlQuery);
        } else {
            addWhereStandardForPercentDiscount(sqlQuery);
        }
        
        getOrderByString(sqlQuery);
        return sqlQuery;
    }
    
    @Override
    protected final String createSqlRecordCountQuery() throws Exception {
        final StringBuilder countQuery = new StringBuilder("SELECT COUNT(*) ");
        
        getTableString(countQuery);
        getWhereString(countQuery, true, false);
        
        final String query = countQuery.toString();
        LOGGER.info("COUNT QUERY : " + query);
        return query;
    }
    
    @Override
    protected final int getMaxRecordCount(final Errors errors) throws Exception {
        return ReportingConstants.REPORTS_DEFAULT_MAX_RECORDS;
    }
    
    @Override
    protected final String createFileName() {
        final StringBuilder sb = new StringBuilder();
        sb.append(reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_COUPON_USAGE_SUMMARY));
        return super.createFileName(sb);
    }
    
    private void getLocationNameTimeZoneAccountCouponCode(final StringBuilder query) {
        query.append(" l.Name AS LocationName, '");
        query.append(this.timeZone);
        query.append("' AS TimeZone, ");
        query.append(" CONCAT(co.FirstName, ' ', co.LastName) AS Account, ");
        query.append(" CONCAT(co.FirstName, ' ', co.LastName) AS GroupField, ");
        query.append(" c.Coupon AS CouponCode, ");
    }
    
    private void getNumUsesRemainingStartEndDate(final StringBuilder query) {
        query.append(" c.IsDeleted AS IsCouponDeleted, ");
        query.append(" if(c.StartDateLocal='2000-01-01 00:00:00', 0, 1) as IsStartDate, ");
        query.append(" if(c.EndDateLocal='3000-01-01 23:59:59', 0, 1) as IsEndDate  ");
    }
    
    private void getNumOfUsesRevenueAmt(final StringBuilder query) {
        query.append(" c.StartDateLocal AS StartDate, c.EndDateLocal AS EndDate, COUNT(*) AS NumOfUses, c.NumberOfUsesRemaining AS NumUses, SUM(pur.ChargedAmount) / 100 AS RevenueAmount, ");
    }
    
    private void getFieldsStandardForDollarDiscount(final StringBuilder query) {
        getLocationNameTimeZoneAccountCouponCode(query);
        
        query.append(" concat('$',round(c.DollarDiscountAmount/100,2)) AS CouponDiscountValue, ");
        
        getNumOfUsesRevenueAmt(query);
        
        query.append(" ROUND(sum( if( pur.ChargedAmount = 0, pur.OriginalAmount, c.DollarDiscountAmount )/100),2) AS DiscountAmount, ");
        
        getNumUsesRemainingStartEndDate(query);
    }
    
    private void getFieldsStandardForPercentDiscount(final StringBuilder query) {
        getLocationNameTimeZoneAccountCouponCode(query);
        
        query.append(" concat(c.PercentDiscount,'%') AS CouponDiscountValue, ");
        
        getNumOfUsesRevenueAmt(query);
        
        query.append(" if(c.PercentDiscount > 0, (SUM(if(pur.OriginalAmount > 0, pur.OriginalAmount, (pur.ChargedAmount * 100)/(100 - c.PercentDiscount))) - SUM(pur.ChargedAmount)) /100,0) AS DiscountAmount, ");
        
        getNumUsesRemainingStartEndDate(query);
    }
    
    private void getFieldsStandardForPercentDiscountNoConcat(final StringBuilder query) {
        getLocationNameTimeZoneAccountCouponCode(query);
        
        query.append(" c.PercentDiscount AS CouponDiscountValue, ");
        
        getNumOfUsesRevenueAmt(query);
        
        query.append(" if(c.PercentDiscount > 0, (SUM(if(pur.OriginalAmount > 0, pur.OriginalAmount, (pur.ChargedAmount * 100)/(100 - c.PercentDiscount))) - SUM(pur.ChargedAmount)) /100,0) AS DiscountAmount, ");
        
        getNumUsesRemainingStartEndDate(query);
    }
    
    private void getFieldsStandardForOffline(final StringBuilder query) {
        getLocationNameTimeZoneAccountCouponCode(query);
        
        query.append(" concat(c.PercentDiscount,'%') AS CouponDiscountValue, ");
        
        getNumOfUsesRevenueAmt(query);
        
        query.append(" SUM(pur.OriginalAmount - pur.ChargedAmount) / 100 AS DiscountAmount, ");
        
        getNumUsesRemainingStartEndDate(query);
    }
    
    private void getTableString(final StringBuilder query) {
        query.append(" FROM Purchase pur");
        query.append(" INNER JOIN Coupon c ON pur.CouponId = c.Id");
        query.append(" LEFT JOIN Consumer co ON c.ConsumerId = co.Id");
        query.append(" LEFT JOIN Location l ON c.LocationId = l.Id");
        
        // Filter for hidden POS
        getFromHiddenPointOfSales(query, "pur");
        
        // TODO dropped???
        /*
         * if (EMSConstants.MACHINE_NUMBER.equals(reportsForm.getSelectedLocationType()) && !EMSConstants.ALL.equals(reportsForm.getSelectedPs1MachineNumber()))
         * {
         * query.append(" INNER JOIN ServiceState s ON t.PaystationId = s.PaystationId");
         * }
         */
        
    }
    
    private void getWhereString(final StringBuilder query, final boolean isSummary, final boolean isOffline) {
        query.append(" WHERE ");
        super.getWherePointOfSale(query, "pur", true);
        
        // Purchased Date & Time
        super.getWherePurchaseGMT(query, "pur");
        
        // Coupon
        super.getWhereCouponNumber(query);
        
        // Filter for hidden POS
        super.getWhereHiddenPointOfSales(query);
        
        // Filter for 'IsDeleted', 'IsOffline' and 'ArchiveDate'.
        if (!isSummary) {
            addWhereIsOffline(query, isOffline);
        }

        // Account (Consumer id)
        if (this.reportDefinition.getAccountFilterTypeId() != null
                && this.reportDefinition.getAccountFilterTypeId().intValue() > 0) {
            addWhereConsumerId(query, this.reportDefinition.getAccountFilterTypeId().intValue());
        }
    }
    
    private void addWhereConsumerId(final StringBuilder query, final int accountValue) {
        query.append(" AND c.ConsumerId = ").append(accountValue).append(" ");
    }
    
    private void addWhereIsOffline(final StringBuilder query, final boolean isOffline) {
        query.append(" AND c.IsOffline = ");
        query.append(isOffline ? "1" : "0");
    }
    
    private void addWhereStandardForDollarDiscount(final StringBuilder query) {
        query.append(" AND c.DollarDiscountAmount > 0 ");
    }
    
    private void addWhereStandardForPercentDiscount(final StringBuilder query) {
        query.append(" AND c.PercentDiscount > 0 ");
    }
    
    private void addWhereStandardForZeroPercentAndDollarDiscount(final StringBuilder query) {
        query.append(" AND c.PercentDiscount = 0 AND c.DollarDiscountAmount = 0 ");
    }
    
    private void getOrderByString(final StringBuilder query) {
        query.append(" GROUP BY c.ConsumerId, c.Coupon ORDER BY c.Coupon");
    }
    
    protected final Map<String, Object> createReportParameterList(final String query, final int customerId) {
        final Map<String, Object> parameters = super.createReportParameterList(query, customerId);
        
        parameters.put("ReportTitle",
                       super.getTitle(reportingUtil.getPropertyEN("label.siteName") + " "
                                      + reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_COUPON_USAGE_SUMMARY)));
        
        final int priDateFilterId = super.getFilterTypeId(this.reportDefinition.getPrimaryDateFilterTypeId());
        if (ReportingConstants.REPORT_SELECTION_ALL == priDateFilterId) {
            parameters.put("DateString", reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL));
        } else {
            parameters
                    .put("DateString",
                         createReportParameterDateString(this.reportQueue.getPrimaryDateRangeBeginGmt(), this.reportQueue.getPrimaryDateRangeEndGmt()));
            
        }

        // Location Parameters
        parameters.put("LotType", "Setting");
        
        parameters.put("LocationType", reportingUtil.findFilterString(this.reportDefinition.getLocationFilterTypeId()));
        parameters.put("MachineNameOrNumber", convertArrayToDelimiterString(this.locationValueNameList, ','));
        
        parameters.put("CouponNumberType", reportingUtil.findFilterString(this.reportDefinition.getCouponNumberFilterTypeId()));
        parameters.put("CouponCode", this.reportDefinition.getCouponNumberSpecificFilter());
        parameters.put("CouponType", reportingUtil.findFilterString(this.reportDefinition.getCouponTypeFilterTypeId()));
        parameters.put("AccountName", WebCoreUtil.returnEmptyIfBlank(this.reportDefinition.getAccountNameFilter()));
        
        parameters.put("IsCsv", !this.reportDefinition.isIsCsvOrPdf());
        parameters.put("IsPdf", this.reportDefinition.isIsCsvOrPdf());
        parameters.put("IsDetails", !this.reportDefinition.isIsDetailsOrSummary());
        parameters.put("IsCsvDetails", !this.reportDefinition.isIsCsvOrPdf() && !this.reportDefinition.isIsDetailsOrSummary());
        parameters.put("IsPdfDetails", this.reportDefinition.isIsCsvOrPdf() && !this.reportDefinition.isIsDetailsOrSummary());
        
        parameters.put("ZeroFloat", new Float(0.00));
        parameters.put("ZeroInteger", new Integer(0));
        
        // Grouping is 1318 (Account).
        parameters.put("Grouping", reportingUtil.findFilterString(this.reportDefinition.getGroupByFilterTypeId()));
        parameters.put("IsPdfGrouping", reportDefinition.isIsCsvOrPdf());

        return parameters;
    }
    
    private boolean validCouponType(final int couponTypeCode) {
        for (int type : ReportingConstants.REPORT_FILTER_COUPON_TYPES) {
            if (type == couponTypeCode) {
                return true;
            }
        }
        return false;
    }
}
