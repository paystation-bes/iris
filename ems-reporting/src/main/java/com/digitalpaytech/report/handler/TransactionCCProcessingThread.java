package com.digitalpaytech.report.handler;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.util.ReportingConstants;

public class TransactionCCProcessingThread extends TransactionBaseThread {
    private static final Logger LOGGER = Logger.getLogger(TransactionCCProcessingThread.class);
    
    public TransactionCCProcessingThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue,
            final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext);
    }
    
    @Override
    protected final String generateSQLString() throws Exception {
        final boolean isSummary = this.reportDefinition.isIsDetailsOrSummary();
        
        final StringBuilder sqlQuery = new StringBuilder("SELECT ");
        
        // Fields
        getFieldsCreditCardProcessing(sqlQuery, false, isSummary, isSummary);
        super.getFieldsRateName(sqlQuery, isSummary);
        super.getFieldsLocationName(sqlQuery, isSummary, true);
        
        // Tables & Where
        super.getTablesCardProcessingAndRefunds(sqlQuery, true, isSummary);
        super.getWhereStandardProcessorTransaction(sqlQuery, true);
        
        // Credit Card Last 4 Digits
        final int cardNumFilterTypeId = super.getFilterTypeId(this.reportDefinition.getCardNumberFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_CARD_NUMBER_ALL != cardNumFilterTypeId) {
            sqlQuery.append(" AND proctrans.Last4DigitsOfCardNumber=" + this.reportDefinition.getCardNumberSpecificFilter());
        }
        
        final int groupFilterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (isSummary && ReportingConstants.REPORT_FILTER_GROUP_BY_NONE != groupFilterTypeId) {
            sqlQuery.append(" UNION ").append(generateCreditCardProcessingReportGroupSummaryQuery());
        }
        
        // Order By
        getOrderByCreditCardProcessing(sqlQuery, isSummary);
        return sqlQuery.toString();
    }
    
    @Override
    protected final String createSqlRecordCountQuery() throws Exception {
        final boolean isSummary = this.reportDefinition.isIsDetailsOrSummary();
        final StringBuilder sqlQuery = new StringBuilder("SELECT ");
        
        // Fields
        sqlQuery.append(" COUNT(*) ");
        
        // Tables & Where
        super.getTablesCardProcessingAndRefunds(sqlQuery, true, isSummary);
        super.getWhereStandardProcessorTransaction(sqlQuery, true);
        
        // Credit Card Last 4 Digits
        final int cardNumFilterId = super.getFilterTypeId(this.reportDefinition.getCardNumberFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_CARD_NUMBER_ALL != cardNumFilterId) {
            sqlQuery.append(" AND proctrans.Last4DigitsOfCardNumber=" + this.reportDefinition.getCardNumberSpecificFilter());
        }
        
        return sqlQuery.toString();
    }
    
    private String generateCreditCardProcessingReportGroupSummaryQuery() throws Exception {
        final StringBuilder sqlQuery = new StringBuilder("(SELECT ");
        final boolean isSummary = this.reportDefinition.isIsDetailsOrSummary();
        
        // Fields
        getFieldsCreditCardProcessing(sqlQuery, false, true, false);
        
        // Tables & Where
        super.getTablesCardProcessingAndRefunds(sqlQuery, false, isSummary);
        super.getWhereStandardProcessorTransaction(sqlQuery, false);
        
        final int cardNumFilterId = super.getFilterTypeId(this.reportDefinition.getCardNumberFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_CARD_NUMBER_ALL != cardNumFilterId) {
            sqlQuery.append(" AND proctrans.Last4DigitsOfCardNumber=" + this.reportDefinition.getCardNumberSpecificFilter());
        }
        
        sqlQuery.append(" GROUP BY GroupName)");
        
        return sqlQuery.toString();
    }
    
    private void getFieldsCreditCardProcessing(final StringBuilder query, final boolean refund, final boolean isSummary,
        final boolean isOverallSummary) {
        final int groupingType = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (isSummary) {
            if (!isOverallSummary && ReportingConstants.REPORT_FILTER_GROUP_BY_CARD_TYPE == groupingType) {
                query.append("0 AS IsOverallSummary");
                query.append(", CONCAT('" + reportingUtil.findFilterString(groupingType) + ":  ', proctrans.CardType) AS GroupName");
                query.append(", proctrans.CardType AS GroupOrderValue");
            } else if (!isOverallSummary && ReportingConstants.REPORT_FILTER_GROUP_BY_MERCHANT_ACCOUNT == groupingType) {
                query.append("0 AS IsOverallSummary");
                query.append(", CONCAT('").append(reportingUtil.findFilterString(groupingType)).append(":  ");
                query.append("', (SELECT IF(TerminalName IS NULL OR TerminalName = '', Name, TerminalName) AS Name FROM MerchantAccount ");
                query.append(" WHERE Id = proctrans.MerchantAccountId)) AS GroupName");
                query.append(", (SELECT IF(TerminalName IS NULL OR TerminalName = '', Name, TerminalName) AS Name FROM MerchantAccount ");
                query.append(" WHERE Id = proctrans.MerchantAccountId) AS GroupOrderValue");
            } else {
                super.getFieldsGroupField(query, isSummary, isOverallSummary);
            }
            query.append(", SUM(IF(proctrans.ProcessorTransactionTypeId IN (4,18,22), proctrans.Amount, 0)) AS TotalRefund");
            query.append(", SUM(IF(proctrans.ProcessorTransactionTypeId IN (4,18,22) AND proctrans.Amount > 0, 1, 0)) AS TotalRefundCount");
            query.append(", SUM(IF(proctrans.ProcessorTransactionTypeId IN (2,6,17,19,21,23), proctrans.Amount, 0)) AS TotalRealTime");
            query.append(", SUM(IF(proctrans.ProcessorTransactionTypeId IN (2,6,17,19,21,23) AND proctrans.Amount > 0, 1, 0)) AS TotalRealTimeCount");
            query.append(", SUM(IF(proctrans.ProcessorTransactionTypeId IN (3,7,10,12), proctrans.Amount, 0)) AS TotalBatched");
            query.append(", SUM(IF(proctrans.ProcessorTransactionTypeId IN (3,7,10,12) AND proctrans.Amount > 0, 1, 0)) AS TotalBatchedCount");
            query.append(", SUM(IF(proctrans.ProcessorTransactionTypeId IN (11,13), proctrans.Amount, 0)) AS TotalSF");
            query.append(", SUM(IF(proctrans.ProcessorTransactionTypeId IN (11,13) AND proctrans.Amount > 0, 1, 0)) AS TotalSFCount");
            query.append(", SUM(IF(proctrans.ProcessorTransactionTypeId IN (2, 6, 17, 19, 3, 7, 10, 12, 11, 13, 21, 23), proctrans.Amount, 0)) AS TotalDeposit");
            query.append(", SUM(IF(proctrans.ProcessorTransactionTypeId IN (2, 6, 17, 19, 3, 7, 10, 12, 11, 13, 21, 23) AND proctrans.Amount > 0, 1, 0)) AS TotalDepositCount");
        } else {
            if (ReportingConstants.REPORT_FILTER_GROUP_BY_CARD_TYPE == groupingType) {
                query.append("proctrans.CardType AS GroupField");
            } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_MERCHANT_ACCOUNT == groupingType) {
                query.append(" IF(m.TerminalName IS NULL OR m.TerminalName = '', m.Name, m.TerminalName) AS GroupField");
            } else {
                super.getFieldsGroupField(query, false, false);
            }
            
            if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == groupingType && this.reportDefinition.isIsCsvOrPdf()) {
                query.append(", pos.Id as PaystationId, r.Id as PaystationRouteId");
            } else {
                query.append(", 0 as PaystationId, 0 as PaystationRouteId");
            }
            
            query.append(", pos.Name AS PosName, '");
            query.append(this.timeZone);
            query.append("' AS TimeZone, pos.Name AS MachineNumber, proctrans.TicketNumber, per.SpaceNumber, lp.Number AS PlateNumber, proctrans.PurchasedDate, ");
            query.append("IF(pur.PaymentTypeId IN (2,5,11,12,13,14,15,16,17), pur.CardPaidAmount, 0) AS OrigAmount, ");
            query.append("proctrans.CardType, proctrans.Last4DigitsOfCardNumber AS CardNumber, ");
            query.append("IF(m.TerminalName IS NULL OR m.TerminalName = '', m.Name, m.TerminalName) AS MerchantAccount, ");
            query.append("proctrans.AuthorizationNumber AS AuthNumber, proctrans.ReferenceNumber, ");
            query.append("proctrans.amount AS RequestAmount, proctrans.ProcessingDate AS RequestDate, ");
            query.append("proctranstype.Name AS RequestType, ");
            query.append("pt.Name AS PaymentTypeName, ");
            query.append("proctrans.ProcessorTransactionTypeId AS RequestTypeId ");
            
        }
    }
    
    private void getOrderByCreditCardProcessing(final StringBuilder query, final boolean isSummary) {
        final int groupingType = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (isSummary) {
            if (ReportingConstants.REPORT_FILTER_GROUP_BY_NONE != groupingType) {
                query.append(" ORDER BY GroupOrderValue ");
            }
            return;
        }
        query.append(" ORDER BY ");
        
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_DAY == groupingType || ReportingConstants.REPORT_FILTER_GROUP_BY_MONTH == groupingType
            || ReportingConstants.REPORT_FILTER_GROUP_BY_NONE == groupingType) {
            query.append("RequestDate ");
        } else {
            query.append("GroupField, RequestDate ");
        }
    }
    
}
