package com.digitalpaytech.report.handler;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationContext;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.WebCoreConstants;

public class CollectionSummaryThread extends ActiveAlertBaseThread {
    public CollectionSummaryThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue, final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext, ReportingConstants.REPORT_TYPE_NAME_COLLECTION_SUMMARY);
        this.jasperFileName = ReportingConstants.REPORT_JASPER_FILE_COLLECTION_SUMMARY;
    }
    
    @Override
    protected final void appendSpecificConditions(final StringBuilder query) {
        query.append("  AND (pec.EventSeverityTypeId >= 2) ");
        
        final List<Integer> alertThresholdTypeIds = resolveThresholdTypes(this.reportDefinition);
        if (alertThresholdTypeIds.size() > 0) {
            query.append("  AND (cat.AlertThresholdTypeId IN(");
            query.append(StringUtils.join(alertThresholdTypeIds, ','));
            query.append(")) ");
        } else {
            query.append("  AND (cat.AlertThresholdTypeId NOT IN(");
            query.append(WebCoreConstants.ALERT_THRESHOLD_TYPE_LAST_SEEN_INTERVAL_HOUR);
            query.append("   , ");
            query.append(WebCoreConstants.ALERT_THRESHOLD_TYPE_PAYSTATION);
            query.append("  )) ");
        }
    }
}
