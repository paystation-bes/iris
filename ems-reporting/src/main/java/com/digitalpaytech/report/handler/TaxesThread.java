package com.digitalpaytech.report.handler;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Map;

import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.ReportingConstants;

public class TaxesThread extends ReportBaseThread {
    private static final Logger LOGGER = Logger.getLogger(TaxesThread.class);
    
    private ThreadLocal<StringBuilder> summaryQuery = new ThreadLocal<StringBuilder>();
    private ThreadLocal<StringBuilder> groupSummaryQuery = new ThreadLocal<StringBuilder>();
    
    public TaxesThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue, final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext);
        this.jasperFileName = ReportingConstants.REPORT_JASPER_FILE_TAX;
    }
    
    @Override
    protected final String generateSQLString() throws Exception {
        final StringBuilder sqlQuery = new StringBuilder("SELECT ");
        this.summaryQuery.set(new StringBuilder());
        this.groupSummaryQuery.set(new StringBuilder());
        this.summaryQuery.get().append("SELECT ");
        this.groupSummaryQuery.get().append("SELECT ");
        
        getFieldGroupByString(sqlQuery);
        getFieldString(sqlQuery);
        getTableString(sqlQuery);
        getWhereString(sqlQuery, false);
        getGroupByString(sqlQuery, false);
        getOrderByString(sqlQuery);
        return sqlQuery.toString();
    }
    
    @Override
    protected final String createSqlRecordCountQuery() throws Exception {
        final StringBuilder countQuery = new StringBuilder("SELECT COUNT(*) ");
        getTablesForCount(countQuery);
        getWhereStandardForCount(countQuery);
        
        final String query = countQuery.toString();
        LOGGER.info("COUNT QUERY : " + query);
        return query;
    }
    
    @Override
    protected final int getMaxRecordCount(final Errors errors) throws Exception {
        int maxRecords = ReportingConstants.REPORTS_DEFAULT_MAX_RECORDS;
        if (this.reportDefinition.isIsDetailsOrSummary()) {
            maxRecords = ReportingConstants.REPORTS_DEFAULT_MAX_RECORDS;
        } else if (this.reportDefinition.isIsCsvOrPdf()) {
            maxRecords = emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.REPORTS_TAXES_MAX_RECORDS,
                                                                    ReportingConstants.REPORTS_DEFAULT_TAXES_MAX_RECORDS);
        }
        return maxRecords;
    }
    
    @Override
    protected final String createFileName() {
        final StringBuilder sb = new StringBuilder();
        sb.append(reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_TAX));
        return super.createFileName(sb);
    }
    
    protected final void getTablesForCount(final StringBuilder query) {
        query.append(" FROM Purchase pur INNER JOIN PurchaseTax purtax ON pur.Id=purtax.PurchaseId INNER JOIN Tax tax ON purtax.TaxId=tax.Id");
        query.append(" INNER JOIN PointOfSale pos ON pur.PointOfSaleId=pos.Id LEFT JOIN Location l ON pur.LocationId = l.Id ");
        
        final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == filterTypeId && this.reportDefinition.isIsCsvOrPdf()) {
            query.append("LEFT JOIN RoutePOS rpos ON pos.Id=rpos.PointOfSaleId ");
            query.append("LEFT JOIN Route r ON rpos.RouteId=r.Id ");
        }
        
        // Filter for hidden POS
        getFromHiddenPointOfSales(query, "pos");
        
    }
    
    private void getWhereStandardForCount(final StringBuilder query) {
        query.append(" WHERE ");
        
        // Machine Name (aka. PS ID)
        super.getWherePointOfSale(query, "pur", true);
        
        //        // paystationGroup ids only for Group by paystation group
        //        super.getWherePointOfSaleRouteId(query, true);
        
        // Purchased Date & Time
        super.getWherePurchaseGMT(query, "pur");
        
        // Filter for hidden POS
        super.getWhereHiddenPointOfSales(query);
        
    }
    
    private void getFieldGroupByString(final StringBuilder query) {
        final int groupByType = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        
        switch (groupByType) {
            case ReportingConstants.REPORT_FILTER_GROUP_BY_DAY:
            case ReportingConstants.REPORT_FILTER_GROUP_BY_MONTH:
                query.append("'");
                query.append(reportingUtil.findFilterString(groupByType));
                query.append("' as GroupField");
                this.groupSummaryQuery.get().append("'");
                this.groupSummaryQuery.get().append(reportingUtil.findFilterString(groupByType));
                this.groupSummaryQuery.get().append("' as GroupField");
                break;
            case ReportingConstants.REPORT_FILTER_GROUP_BY_LOCATION:
                query.append("l.Name AS GroupField");
                this.groupSummaryQuery.get().append("l.Name AS GroupField");
                break;
            case ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION:
                query.append("pos.Name AS GroupField");
                this.groupSummaryQuery.get().append("pos.Name AS GroupField");
                break;
            case ReportingConstants.REPORT_FILTER_GROUP_BY_TAX_NAME:
                query.append("tax.Name AS GroupField");
                this.groupSummaryQuery.get().append("tax.Name AS GroupField");
                break;
            case ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE:
                if (this.reportDefinition.isIsCsvOrPdf()) {
                    query.append("r.Name AS GroupField");
                    this.groupSummaryQuery.get().append("r.Name AS GroupField");
                } else {
                    query.append("'NoGroup' AS GroupField");
                    this.groupSummaryQuery.get().append("'NoGroup' AS GroupField");
                }
                break;
            default:
                query.append("'NoGroup' AS GroupField");
                this.groupSummaryQuery.get().append("'NoGroup' AS GroupField");
                break;
        }
    }
    
    private void getFieldString(final StringBuilder query) {
        
        query.append(", pos.Name as PaystationName, l.Name AS LocationName, tax.Name AS TaxName, ");
        query.append(" ROUND(purtax.TaxRate/100, 2) AS TaxRate, ROUND(sum(purtax.TaxAmount)/100, 2) AS SumCollected, ");
        query.append(" MAX(pur.PurchaseGMT) as PurchasedDate");
        
        final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == filterTypeId && this.reportDefinition.isIsCsvOrPdf()) {
            query.append(", pos.Id AS PaystationId, r.Id AS PaystationRouteId");
        } else {
            query.append(", 0 AS PaystationId, 0 AS PaystationRouteId");
        }
        
        this.summaryQuery.get().append(" tax.Name AS TaxName, ROUND(sum(purtax.TaxAmount)/100, 2) AS SumCollected ");
        this.groupSummaryQuery.get()
                .append(", pos.Name AS PaystationName, tax.Name AS TaxName, l.Name as LocationName, ROUND(sum(purtax.TaxAmount)/100, 2) AS GroupSumCollected ");
    }
    
    private void getTableString(final StringBuilder query) {
        query.append(" FROM Purchase pur INNER JOIN PurchaseTax purtax ON pur.Id=purtax.PurchaseId INNER JOIN Tax tax ON purtax.TaxId=tax.Id");
        query.append(" INNER JOIN PointOfSale pos ON pur.PointOfSaleId=pos.Id LEFT JOIN Location l ON pur.LocationId = l.Id ");
        
        this.summaryQuery.get()
                .append(" FROM Purchase pur INNER JOIN PurchaseTax purtax ON pur.Id=purtax.PurchaseId INNER JOIN Tax tax ON purtax.TaxId=tax.Id");
        this.summaryQuery.get().append(" INNER JOIN PointOfSale pos ON pur.PointOfSaleId=pos.Id LEFT JOIN Location l ON pur.LocationId = l.Id ");
        
        this.groupSummaryQuery.get()
                .append(" FROM Purchase pur INNER JOIN PurchaseTax purtax ON pur.Id=purtax.PurchaseId INNER JOIN Tax tax ON purtax.TaxId=tax.Id");
        this.groupSummaryQuery.get().append(" INNER JOIN PointOfSale pos ON pur.PointOfSaleId=pos.Id LEFT JOIN Location l ON pur.LocationId = l.Id ");
        
        final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == filterTypeId && this.reportDefinition.isIsCsvOrPdf()) {
            query.append("LEFT JOIN RoutePOS rpos ON pos.Id=rpos.PointOfSaleId ");
            query.append("LEFT JOIN Route r ON rpos.RouteId=r.Id ");
            
            this.summaryQuery.get().append("LEFT JOIN RoutePOS rpos ON pos.Id=rpos.PointOfSaleId ");
            this.summaryQuery.get().append("LEFT JOIN Route r ON rpos.RouteId=r.Id ");
            
            this.groupSummaryQuery.get().append("LEFT JOIN RoutePOS rpos ON pos.Id=rpos.PointOfSaleId ");
            this.groupSummaryQuery.get().append("LEFT JOIN Route r ON rpos.RouteId=r.Id ");
        }
    }
    
    private void getWhereString(final StringBuilder query, final boolean isSummary) {
        final int groupByType = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        
        query.append(" WHERE ");
        this.summaryQuery.get().append(" WHERE ");
        this.groupSummaryQuery.get().append(" WHERE ");
        
        super.getWherePointOfSale(query, "pur", true);
        super.getWherePointOfSale(this.summaryQuery.get(), "pur", true);
        super.getWherePointOfSale(this.groupSummaryQuery.get(), "pur", true);
        
        // Purchased Date & Time
        super.getWherePurchaseGMT(query, "pur");
        super.getWherePurchaseGMT(this.summaryQuery.get(), "pur");
        super.getWherePurchaseGMT(this.groupSummaryQuery.get(), "pur");
        
        switch (groupByType) {
            case ReportingConstants.REPORT_FILTER_GROUP_BY_DAY:
            case ReportingConstants.REPORT_FILTER_GROUP_BY_MONTH:
                this.groupSummaryQuery.get().append(" AND (pur.PurchaseGMT BETWEEN $P{StartTime} and $P{EndTime}) ");
                break;
            case ReportingConstants.REPORT_FILTER_GROUP_BY_TAX_NAME:
                this.groupSummaryQuery.get().append(" AND tax.Name = $P{TName} ");
                break;
            case ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION:
                this.groupSummaryQuery.get().append(" AND pos.Name = $P{PSName} ");
                break;
            case ReportingConstants.REPORT_FILTER_GROUP_BY_LOCATION:
                this.groupSummaryQuery.get().append(" AND l.Name = $P{RName} ");
                break;
            default:
                LOGGER.warn("There is no matching in where cause.");
                break;
            
        }
        
    }
    
    private void getGroupByString(final StringBuilder query, final boolean isSummary) {
        final int groupByType = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        query.append(" GROUP BY ");
        if (isSummary) {
            if (ReportingConstants.REPORT_FILTER_GROUP_BY_DAY == groupByType) {
                query.append(" CONCAT(Year(pur.PurchaseGMT),  \"-\", Month(pur.PurchaseGMT), \"-\", DAYOFMONTH(pur.PurchaseGMT)), ");
            } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_MONTH == groupByType) {
                query.append(" CONCAT(Year(pur.PurchaseGMT),  \"-\", Month(pur.PurchaseGMT)), ");
            }
        } else {
            if (ReportingConstants.REPORT_FILTER_GROUP_BY_DAY == groupByType) {
                query.append(" DATE(pur.PurchaseGMT), ");
            } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_MONTH == groupByType) {
                query.append(" CONCAT(Year(pur.PurchaseGMT),  \"-\", Month(pur.PurchaseGMT), \"-\", DAYOFMONTH(pur.PurchaseGMT)), ");
            }
        }
        query.append(" PaystationName, pur.LocationId, tax.Name, purtax.TaxRate ");
        this.summaryQuery.get().append(" group by tax.Name ");
        this.groupSummaryQuery.get().append(" group by tax.Name ");
    }
    
    private void getOrderByString(final StringBuilder query) {
        final int groupByType = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        query.append(" ORDER BY ");
        this.summaryQuery.get().append(" ORDER BY tax.Name ");
        this.groupSummaryQuery.get().append(" ORDER BY tax.Name ");
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_DAY == groupByType || ReportingConstants.REPORT_FILTER_GROUP_BY_MONTH == groupByType) {
            query.append("pur.PurchaseGMT ");
        } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_NONE == groupByType) {
            query.append("pur.PurchaseGMT");
        } else {
            query.append("GroupField, pur.PurchaseGMT ");
        }
    }
    
    protected final Map<String, Object> createReportParameterList(final String query, final int customerId) {
        final Map<String, Object> parameters = super.createReportParameterList(query, customerId);
        
        parameters.put("ReportTitle", super.getTitle(reportingUtil.getPropertyEN("label.siteName") + " "
                                                     + reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_TAX)));
        
        final int priDateFilterId = super.getFilterTypeId(this.reportDefinition.getPrimaryDateFilterTypeId());
        if (ReportingConstants.REPORT_SELECTION_ALL == priDateFilterId) {
            parameters.put("DateString", reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL));
        } else {
            parameters.put("DateString", createReportParameterDateString(this.reportQueue.getPrimaryDateRangeBeginGmt(),
                                                                         this.reportQueue.getPrimaryDateRangeEndGmt()));
            
        }
        
        // Location Parameters
        parameters.put("LocationType", reportingUtil.findFilterString(this.reportDefinition.getLocationFilterTypeId()));
        parameters.put("MachineNameOrNumber", convertArrayToDelimiterString(this.locationValueNameList, ','));
        
        parameters.put("Grouping", reportingUtil.findFilterString(this.reportDefinition.getGroupByFilterTypeId()));
        parameters.put("TransactionType", reportingUtil.findFilterString(this.reportDefinition.getOtherParametersTypeFilterTypeId()));
        
        parameters.put("IsCsv", !this.reportDefinition.isIsCsvOrPdf());
        parameters.put("IsPdf", this.reportDefinition.isIsCsvOrPdf());
        parameters.put("IsDetails", !this.reportDefinition.isIsDetailsOrSummary());
        parameters.put("IsGrouping",
                       ReportingConstants.REPORT_FILTER_GROUP_BY_NONE != super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId()));
        parameters.put("IsCsvDetails", !this.reportDefinition.isIsCsvOrPdf() && !this.reportDefinition.isIsDetailsOrSummary());
        parameters.put("IsPdfDetails", this.reportDefinition.isIsCsvOrPdf() && !this.reportDefinition.isIsDetailsOrSummary());
        parameters.put("IsPdfGrouping",
                       this.reportDefinition.isIsCsvOrPdf()
                                        && ReportingConstants.REPORT_FILTER_GROUP_BY_NONE != super.getFilterTypeId(this.reportDefinition
                                                .getGroupByFilterTypeId()));
        
        parameters.put("UserStartDate", DateUtil.createDateString(DateUtil.REPORTS_FORMAT, this.reportQueue.getPrimaryDateRangeBeginGmt()));
        parameters.put("UserEndDate", DateUtil.createDateString(DateUtil.REPORTS_FORMAT, this.reportQueue.getPrimaryDateRangeEndGmt()));
        
        JasperReport subreport = null;
        try {
            final File jasperFile = new File(this.jasperFilePath + ReportingConstants.REPORT_JASPER_FILE_TAX_GROUP_SUMMARY);
            InputStream stream = new FileInputStream(jasperFile);
            
            File tempPath = new File(this.tempReportsPath);
            if (!tempPath.exists()) {
                if (!tempPath.mkdirs()) {
                    LOGGER.error("Cannot create temp reports directory , " + this.tempReportsPath);
                    throw new ApplicationException("Cannot create temp reports directory");
                }
            }
            subreport = (JasperReport) JRLoader.loadObject(stream);
            parameters.put("TaxesGroupSummary", subreport);
            
            final File summaryJasperFile = new File(this.jasperFilePath + ReportingConstants.REPORT_JASPER_FILE_TAX_SUMMARY);
            stream = new FileInputStream(summaryJasperFile);
            tempPath = new File(this.tempReportsPath);
            if (!tempPath.exists()) {
                if (!tempPath.mkdirs()) {
                    LOGGER.error("Cannot create temp reports directory , " + this.tempReportsPath);
                    throw new ApplicationException("Cannot create temp reports directory");
                }
            }
            subreport = (JasperReport) JRLoader.loadObject(stream);
            parameters.put("TaxesSummary", subreport);
            parameters.put("connection", super.getDataSource(this.reportDefinition).getConnection());
            parameters.put("SummarySQLQuery", this.summaryQuery.get().toString());
            parameters.put("GroupSummarySQLQuery", this.groupSummaryQuery.get().toString());
        } catch (Exception e) {
            LOGGER.error("load tax subreport error : " + e.getMessage());
            e.printStackTrace();
        }
        return parameters;
    }
    
}
