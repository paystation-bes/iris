package com.digitalpaytech.report.handler;

import org.springframework.context.ApplicationContext;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.util.ReportingConstants;

public class TransactionDelayThread extends ReportBaseThread {
    
    private static final String SQL_SELECT = "SELECT pos.Name AS Paystation, pos.SerialNumber AS SerialNumber,"
                                             + " l.Name AS Location, per.SpaceNumber AS SpaceNumber,"
                                             + " pur.PurchaseNumber AS TransactionNumber, pur.PurchaseGMT AS PurchaseDate,"
                                             + " per.PermitOriginalExpireGMT AS ExpiryDate, pur.CreatedGMT AS PurchaseCreatedGMT,"
                                             + " timestampdiff(second, pur.PurchaseGMT, pur.CreatedGMT) AS Delta, lp.Number AS LicencePlate";
    
    private static final String SQL_FROM = " FROM Purchase pur LEFT JOIN Permit per ON per.purchaseid = pur.id"
                                           + " INNER JOIN PointOfSale pos ON pos.id = pur.PointOfSaleId INNER JOIN Location l ON pur.LocationId = l.id"
                                           + " INNER JOIN POSStatus ps ON ps.pointofsaleid = pos.id"
                                           + " LEFT JOIN LicencePlate lp ON lp.id = per.licencePlateId";
    
    private static final String SQL_ORDER_BY = " ORDER BY pur.CreatedGMT desc";
    
    public TransactionDelayThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue, final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext);
        this.jasperFileName = ReportingConstants.REPORT_JASPER_FILE_TRANSACTION_DELAY;
    }
    
    @Override
    protected final String generateSQLString() throws Exception {
        final StringBuilder query = new StringBuilder();
        query.append(SQL_SELECT);
        query.append(SQL_FROM);
        getWhereString(query, this.reportDefinition.getLocationFilterTypeId());
        query.append(SQL_ORDER_BY);
        return query.toString();
    }
    
    @Override
    protected final String createSqlRecordCountQuery() throws Exception {
        
        final StringBuilder query = new StringBuilder("SELECT ");
        query.append(" COUNT(*) ");
        query.append(SQL_FROM);
        super.getWherePurchaseGMT(query, "pur");
        
        return query.toString();
    }
    
    private void getWhereString(final StringBuilder query, final int locationFilterTypeId) {
        query.append(" WHERE pur.CreatedGMT IS NOT NULL ");
        super.getWhereHiddenPointOfSales(query);
        super.getWherePurchaseGMT(query, "pur");
        query.append(" AND ");
        super.getWherePointOfSale(query, "pur", true);
    }
    
    @Override
    protected final int getMaxRecordCount(final Errors errors) throws Exception {
        return ReportingConstants.REPORTS_DEFAULT_MAX_RECORDS;
    }
    
    @Override
    protected final String createFileName() {
        final StringBuilder sb = new StringBuilder();
        sb.append(reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_TRANSACTION_DELAY));
        return super.createFileName(sb);
    }
}
