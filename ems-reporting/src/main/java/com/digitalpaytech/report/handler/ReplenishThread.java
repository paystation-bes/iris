package com.digitalpaytech.report.handler;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.util.ReportingConstants;

public class ReplenishThread extends ReportBaseThread {
    private static final Logger LOGGER = Logger.getLogger(ReplenishThread.class);
    
    public ReplenishThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue, final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext);
        this.jasperFileName = ReportingConstants.REPORT_JASPER_FILE_REPLENISH;
    }
    
    @Override
    protected final String generateSQLString() throws Exception {
        final StringBuilder sqlQuery = new StringBuilder("SELECT ");
        
        getFieldGroupByString(sqlQuery);
        getFieldString(sqlQuery);
        getTableString(sqlQuery);
        getWhereString(sqlQuery, false);
        getOrderByString(sqlQuery);
        return sqlQuery.toString();
    }
    
    @Override
    protected final String createSqlRecordCountQuery() throws Exception {
        final StringBuilder countQuery = new StringBuilder("SELECT COUNT(*) ");
        getTableString(countQuery);
        getWhereString(countQuery, true);
        
        final String query = countQuery.toString();
        LOGGER.info("COUNT QUERY : " + query);
        return query;
    }
    
    @Override
    protected final int getMaxRecordCount(final Errors errors) throws Exception {
        return ReportingConstants.REPORTS_DEFAULT_MAX_RECORDS;
    }
    
    @Override
    protected final String createFileName() {
        final StringBuilder sb = new StringBuilder();
        sb.append(reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_REPLENISH));
        return super.createFileName(sb);
    }
    
    private void getFieldGroupByString(final StringBuilder query) {
        final int groupByType = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_MACHINE == groupByType) {
            query.append("pos.Name AS GroupField");
        } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == groupByType && this.reportDefinition.isIsCsvOrPdf()) {
            query.append("r.Name AS GroupField");
        } else {
            query.append("'NoGroup' AS GroupField");
        }
    }
    
    private void getFieldString(final StringBuilder query) {
        query.append(", '");
        query.append(this.timeZone);
        query.append("' AS TimeZone, ");
        query.append("pos.Name, pos.SerialNumber, rep.*, rt.Name AS Type ");
        
        final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == filterTypeId && this.reportDefinition.isIsCsvOrPdf()) {
            query.append(", pos.Id as PaystationId, r.Id as PaystationRouteId");
        } else {
            query.append(", 0 as PaystationId, 0 as PaystationRouteId");
        }
    }
    
    private void getTableString(final StringBuilder query) {
        //TODO AND PointOfSaleType = 1 removed from join
        query.append(" FROM Replenish rep INNER JOIN PointOfSale pos ON rep.PointOfSaleId=pos.Id");
        query.append(" INNER JOIN ReplenishType rt ON rep.ReplenishTypeId = rt.Id");
        
        final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == filterTypeId && this.reportDefinition.isIsCsvOrPdf()) {
            query.append("LEFT JOIN RoutePOS rpos ON pos.Id=rpos.PointOfSaleId ");
            query.append("LEFT JOIN Route r ON rpos.RouteId=r.Id ");
        }
        
        // Filter for hidden POS
        getFromHiddenPointOfSales(query, "pos");
        
    }
    
    private void getWhereString(final StringBuilder query, final boolean isSummary) {
        query.append(" WHERE ");
        
        super.getWherePointOfSale(query, "pos", true);
        
        //        super.getWherePointOfSaleRouteId(query, true);
        
        // Start Date & Time
        super.getWhereDate(query, "rep", "ReplenishGMT", true);
        
        // Report Type
        getWhereReportType(query);
        
        // Filter for hidden POS
        super.getWhereHiddenPointOfSales(query);
        
    }
    
    private void getOrderByString(final StringBuilder query) {
        query.append(" ORDER BY ");
        
        final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == filterTypeId && this.reportDefinition.isIsCsvOrPdf()) {
            query.append("GroupField, pos.Name, rep.ReplenishGMT, rep.Number");
        } else {
            query.append("pos.Name, rep.ReplenishGMT, rep.Number");
        }
    }
    
    private void getWhereReportType(final StringBuilder query) {
        final int reportNumberType = super.getFilterTypeId(this.reportDefinition.getTicketNumberFilterTypeId());
        
        switch (reportNumberType) {
            case ReportingConstants.REPORT_FILTER_REPORT_NUMBER_ALL:
                break;
            case ReportingConstants.REPORT_FILTER_REPORT_NUMBER_SPECIFIC:
                query.append(" AND (rep.Number=");
                query.append(this.reportDefinition.getTicketNumberBeginFilter());
                query.append(") ");
                break;
            case ReportingConstants.REPORT_FILTER_REPORT_NUMBER_LESS:
                query.append(" AND (rep.Number BETWEEN 0 AND ");
                query.append(this.reportDefinition.getTicketNumberBeginFilter());
                query.append(") ");
                break;
            case ReportingConstants.REPORT_FILTER_REPORT_NUMBER_GREATER:
                query.append(" AND (rep.Number>=");
                query.append(this.reportDefinition.getTicketNumberBeginFilter());
                query.append(") ");
                break;
            case ReportingConstants.REPORT_FILTER_REPORT_NUMBER_BETWEEN:
                query.append(" AND (rep.Number BETWEEN ");
                query.append(this.reportDefinition.getTicketNumberBeginFilter());
                query.append(" AND ");
                query.append(this.reportDefinition.getTicketNumberEndFilter());
                query.append(") ");
                break;
            default:
                break;
        }
        
        query.append(" AND rep.ReplenishTypeId > 0 ");
    }
    
    protected final Map<String, Object> createReportParameterList(final String query, final int customerId) {
        final Map<String, Object> parameters = super.createReportParameterList(query, customerId);
        
        parameters.put(
            "ReportTitle",
            super.getTitle(reportingUtil.getPropertyEN("label.siteName") + " "
                           + reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_REPLENISH)));
        final int priDateFilterId = super.getFilterTypeId(this.reportDefinition.getPrimaryDateFilterTypeId());
        if (ReportingConstants.REPORT_SELECTION_ALL == priDateFilterId) {
            parameters.put("DateString", reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL));
        } else {
            parameters.put("DateString",
                createReportParameterDateString(this.reportQueue.getPrimaryDateRangeBeginGmt(), this.reportQueue.getPrimaryDateRangeEndGmt()));
            
        }
        
        // Location Parameters
        parameters.put("LocationType", reportingUtil.findFilterString(this.reportDefinition.getLocationFilterTypeId()));
        parameters.put("MachineName", convertArrayToDelimiterString(this.locationValueNameList, ','));
        
        parameters.put("ReportNumberOption", reportingUtil.findFilterString(this.reportDefinition.getTicketNumberFilterTypeId()));
        parameters.put("ReportNumberLow", this.reportDefinition.getTicketNumberBeginFilter() != null ? this.reportDefinition
                .getTicketNumberBeginFilter().toString() : "");
        parameters.put("ReportNumberHigh", this.reportDefinition.getTicketNumberEndFilter() != null ? this.reportDefinition
                .getTicketNumberEndFilter().toString() : "");
        
        parameters.put("Grouping", reportingUtil.findFilterString(this.reportDefinition.getGroupByFilterTypeId()));
        parameters.put("IsDetails", !this.reportDefinition.isIsDetailsOrSummary());
        parameters.put("IsGrouping",
            ReportingConstants.REPORT_FILTER_GROUP_BY_NONE != super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId()));
        
        parameters.put("ZeroFloat", new Float(0.00));
        parameters.put("ZeroInteger", new Integer(0));
        
        return parameters;
    }
}
