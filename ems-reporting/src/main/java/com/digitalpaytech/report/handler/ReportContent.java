package com.digitalpaytech.report.handler;

public class ReportContent {
    private byte[] data;
    private boolean zipped;
    public final byte[] getData() {
        return this.data;
    }
    public final void setData(final byte[] data) {
        this.data = data;
    }
    public final boolean isZipped() {
        return this.zipped;
    }
    public final void setZipped(final boolean zipped) {
        this.zipped = zipped;
    }
}
