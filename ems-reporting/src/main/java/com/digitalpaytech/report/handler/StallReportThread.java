package com.digitalpaytech.report.handler;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportLocationValue;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.WebCoreConstants;

public class StallReportThread extends ReportBaseThread {
    private static final Logger LOGGER = Logger.getLogger(StallReportThread.class);
    
    public StallReportThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue, final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext);
        this.jasperFileName = ReportingConstants.REPORT_JASPER_FILE_STALL;
        
    }
    
    @Override
    protected final String generateSQLString() throws Exception {
        final StringBuilder sqlQuery = new StringBuilder("SELECT");
        getFieldString(sqlQuery);
        getTableString(sqlQuery);
        getWhereString(sqlQuery);
        getOrderByString(sqlQuery);
        return sqlQuery.toString();
    }
    
    @Override
    protected final String createSqlRecordCountQuery() throws Exception {
        return null;
    }
    
    @Override
    protected final int getMaxRecordCount(final Errors errors) throws Exception {
        return ReportingConstants.REPORTS_DEFAULT_MAX_RECORDS;
    }
    
    @Override
    protected final String createFileName() {
        final StringBuilder reportTitle = new StringBuilder();
        final int reportType = super.getFilterTypeId(this.reportDefinition.getOtherParametersTypeFilterTypeId());
        switch (reportType) {
            case ReportingConstants.REPORT_FILTER_REPORT_VALID:
                reportTitle.append("Valid ");
                break;
            case ReportingConstants.REPORT_FILTER_REPORT_EXPIRED:
                reportTitle.append("Expired ");
                break;
            case ReportingConstants.REPORT_FILTER_REPORT_ALL:
            default:
                reportTitle.append("All ");
                break;
        }
        
        reportTitle.append(reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_STALL));
        return super.createFileName(reportTitle);
    }
    
    protected final void getFieldString(final StringBuilder query) {
        
        final CustomerProperty timeZoneProperty = customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(this.reportDefinition
                .getCustomer().getId(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        final String timeZone = timeZoneProperty.getPropertyValue();
        
        query.append(" l.Name AS LocationName, '");
        query.append(timeZone);
        query.append("' AS TimeZone, ");
        query.append(" ap.SpaceNumber AS StallNumber, MAX(ap.PermitBeginGMT) AS PurchasedDate, ");
        query.append(" MAX(ap.PermitExpireGMT) AS ExpiryDate, MAX(ap.PermitExpireGMT) < UTC_Timestamp() AS IsExpired");
    }
    
    protected final void getTableString(final StringBuilder query) {
        query.append(" FROM ActivePermit ap ");
        query.append(" LEFT JOIN Location l ON ap.PermitLocationId = l.Id");
    }
    
    private void getWhereString(final StringBuilder query) {
        query.append(" WHERE ap.CustomerId = ");
        query.append(this.reportDefinition.getCustomer().getId());
        
        this.locationValueNameList = new String[1];
        
        final List<ReportLocationValue> locationList = this.reportLocationValueService
                .findReportLocationValuesByReportDefinition(this.reportDefinition.getId());
        if (locationList != null && !locationList.isEmpty() && locationList.size() == 1) {
            final ReportLocationValue locationValue = locationList.get(0);
            if (locationValue.getObjectType().equals(Location.class.getName())) {
                if (locationValue.getObjectId() != ReportingConstants.REPORT_SELECTION_ALL) {
                    query.append(" AND ap.PermitLocationId = ");
                    query.append(locationValue.getObjectId());
                    final Location location = this.locationService.findLocationById(locationValue.getObjectId());
                    this.locationValueNameList[0] = location.getName();
                } else {
                    this.locationValueNameList[0] = reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL_LOCATIONS);
                }
            }
        }
        
        final int spaceNumberType = super.getFilterTypeId(this.reportDefinition.getSpaceNumberFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_SPACE_NUMBER_ALL == spaceNumberType) {
            query.append(" AND ap.SpaceNumber > 0");
        } else {
            query.append(" AND ap.SpaceNumber BETWEEN ");
            query.append(this.reportDefinition.getSpaceNumberBeginFilter());
            query.append(" AND ");
            query.append(this.reportDefinition.getSpaceNumberEndFilter());
        }
    }
    
    private void getOrderByString(final StringBuilder query) {
        query.append(" GROUP BY ap.SpaceNumber");
        
        final int reportType = super.getFilterTypeId(this.reportDefinition.getOtherParametersTypeFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_REPORT_VALID == reportType) {
            query.append(" HAVING IsExpired=0");
        } else if (ReportingConstants.REPORT_FILTER_REPORT_EXPIRED == reportType) {
            query.append(" HAVING IsExpired=1");
        }
        query.append(" ORDER BY ap.SpaceNumber");
    }
    
    protected final Map<String, Object> createReportParameterList(final String query, final int customerId) {
        final Map<String, Object> parameters = super.createReportParameterList(query, customerId);
        
        final int reportType = super.getFilterTypeId(this.reportDefinition.getOtherParametersTypeFilterTypeId());
        final StringBuilder reportTitle = new StringBuilder(reportingUtil.getPropertyEN("label.siteName") + " ");
        switch (reportType) {
            case ReportingConstants.REPORT_FILTER_REPORT_VALID:
                reportTitle.append("Valid ");
                break;
            case ReportingConstants.REPORT_FILTER_REPORT_EXPIRED:
                reportTitle.append("Expired ");
                break;
            case ReportingConstants.REPORT_FILTER_REPORT_ALL:
            default:
                reportTitle.append("All ");
                break;
        }
        reportTitle.append(reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_STALL));
        parameters.put("ReportTitle", super.getTitle(reportTitle.toString()));
        
        parameters.put("LocationType", reportingUtil.findFilterString(this.reportDefinition.getLocationFilterTypeId()));
        parameters.put("Location", this.locationValueNameList[0]);
        
        final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getSpaceNumberFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_SPACE_NUMBER_ALL == filterTypeId) {
            parameters.put("StallRange", reportingUtil.findFilterString(this.reportDefinition.getSpaceNumberFilterTypeId()));
        } else {
            final StringBuilder stallRange = new StringBuilder();
            stallRange.append("Between ");
            stallRange.append(this.reportDefinition.getSpaceNumberBeginFilter());
            stallRange.append(" and ");
            stallRange.append(this.reportDefinition.getSpaceNumberEndFilter());
            parameters.put("StallRange", stallRange.toString());
        }
        return parameters;
    }
    
}
