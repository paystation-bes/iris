package com.digitalpaytech.report.handler;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.util.ReportingConstants;

public class TransactionPatrollerCardThread extends TransactionBaseThread {
    private static final Logger LOGGER = Logger.getLogger(TransactionPatrollerCardThread.class);
    
    public TransactionPatrollerCardThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue,
            final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext);
    }
    
    @Override
    protected final String generateSQLString() throws Exception {
        final boolean isSummary = this.reportDefinition.isIsDetailsOrSummary();
        
        StringBuilder sqlQuery = null;
        
        final int paramTypeFilterTypeId = super.getFilterTypeId(this.reportDefinition.getOtherParametersTypeFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_TRANSACTION_ALL == paramTypeFilterTypeId) {
            sqlQuery = new StringBuilder(800);
            sqlQuery.append("(");
            
            // Part 1
            sqlQuery.append(getQueryPatrollerCard(true, 1, isSummary, isSummary));
            sqlQuery.append(") UNION (");
            
            // Part2
            sqlQuery.append(getQueryPatrollerCard(true, 2, isSummary, isSummary));
            sqlQuery.append(")");
        } else {
            sqlQuery = getQueryPatrollerCard(true, 0, isSummary, isSummary);
        }
        
        final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (isSummary && ReportingConstants.REPORT_FILTER_GROUP_BY_NONE == filterTypeId) {
            sqlQuery.append(" UNION ").append(generatePatrollerCardReportGroupSummaryQuery());
        }
        
        // Order By
        getOrderByStandard(sqlQuery, isSummary);
        return sqlQuery.toString();
    }
    
    @Override
    protected final String createSqlRecordCountQuery() throws Exception {
        final boolean isSummary = this.reportDefinition.isIsDetailsOrSummary();
        StringBuilder sqlQuery = null;
        
        final int paramTypeFilterTypeId = super.getFilterTypeId(this.reportDefinition.getOtherParametersTypeFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_TRANSACTION_ALL == paramTypeFilterTypeId) {
            sqlQuery = new StringBuilder(800);
            sqlQuery.append("(");
            
            // Part 1
            sqlQuery.append(getQueryPatrollerCard(false, 1, isSummary, isSummary));
            
            sqlQuery.append(") UNION (");
            
            // Part2
            sqlQuery.append(getQueryPatrollerCard(false, 2, isSummary, isSummary));
            
            sqlQuery.append(")");
        } else {
            sqlQuery = getQueryPatrollerCard(false, 0, isSummary, isSummary);
        }
        return sqlQuery.toString();
    }
    
    private StringBuilder getQueryPatrollerCard(final boolean generateReport, final int queryModifier, final boolean isSummary,
        final boolean isOverallSummary) throws Exception {
        final StringBuilder sqlQuery = new StringBuilder("SELECT ");
        
        // Fields
        if (generateReport) {
            super.getFieldsGroupField(sqlQuery, isSummary, isOverallSummary);
            if (!isSummary) {
                super.getFieldsStandard(sqlQuery);
            }
            super.getFieldsCash(sqlQuery, isSummary);
            super.getFieldsPatrollerCard(sqlQuery, isSummary);
            super.getFieldsRateName(sqlQuery, isSummary);
        } else {
            sqlQuery.append(" COUNT(*) ");
        }
        
        // Tables & Where
        super.getTables(sqlQuery, generateReport, isOverallSummary);
        super.getWhereStandardTransaction(sqlQuery, queryModifier, generateReport, isOverallSummary);
        
        // Card Number
        final int cardNumFilterId = super.getFilterTypeId(this.reportDefinition.getCardNumberFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_CARD_NUMBER_SPECIFIC == cardNumFilterId) {
            sqlQuery.append(" AND (cc.CardNumber='");
            sqlQuery.append(this.reportDefinition.getCardNumberSpecificFilter());
            sqlQuery.append("')");
        }
        
        if (isSummary && !isOverallSummary) {
            // that means building a group summary query
            sqlQuery.append(" GROUP BY GroupName");
        }
        
        return sqlQuery;
    }
    
    private String generatePatrollerCardReportGroupSummaryQuery() throws Exception {
        final StringBuilder sqlQuery = new StringBuilder(800);
        
        // If TransactionType = ALL then must do a union query
        final int otherParamFilterTypeId = super.getFilterTypeId(this.reportDefinition.getOtherParametersTypeFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_TRANSACTION_ALL == otherParamFilterTypeId) {
            sqlQuery.append("(");
            
            // Part 1
            sqlQuery.append(getQueryPatrollerCard(true, 1, true, false));
            sqlQuery.append(") UNION (");
            
            // Part2
            sqlQuery.append(getQueryPatrollerCard(true, 2, true, false));
            sqlQuery.append(")");
        } else {
            sqlQuery.append("(");
            sqlQuery.append(getQueryPatrollerCard(true, 0, true, false));
            sqlQuery.append(")");
        }
        
        return sqlQuery.toString();
    }
}
