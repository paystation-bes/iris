package com.digitalpaytech.report.handler;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.util.ReportingConstants;

public class TransactionValueCardThread extends TransactionBaseThread {
    private static final Logger LOGGER = Logger.getLogger(TransactionValueCardThread.class);
    
    public TransactionValueCardThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue,
            final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext);
    }
    
    @Override
    protected final String generateSQLString() throws Exception {
        final boolean isSummary = this.reportDefinition.isIsDetailsOrSummary();
        
        final StringBuilder sqlQuery = new StringBuilder("SELECT ");
        
        // Fields
        super.getFieldsGroupField(sqlQuery, isSummary, isSummary);
        if (!isSummary) {
            super.getFieldsStandard(sqlQuery);
        }
        super.getFieldsValueCard(sqlQuery, isSummary);
        super.getFieldsRateName(sqlQuery, isSummary);
        super.getFieldsLocationName(sqlQuery, this.reportDefinition.isIsCsvOrPdf());
        super.getSummaryFieldTotalTransaction(sqlQuery, isSummary);
        
        // Tables & Where
        super.getTables(sqlQuery, true, isSummary);
        super.getWhereStandardTransaction(sqlQuery, true, isSummary);
        
        // Value Card Number
        final int cardNumFilterId = super.getFilterTypeId(this.reportDefinition.getCardNumberFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_CARD_NUMBER_SPECIFIC == cardNumFilterId) {
            sqlQuery.append(" AND (cc.CardNumber='");
            sqlQuery.append(this.reportDefinition.getCardNumberSpecificFilter());
            sqlQuery.append("')");
        }
        
        final int groupByFilterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (isSummary && ReportingConstants.REPORT_FILTER_GROUP_BY_NONE != groupByFilterTypeId) {
            sqlQuery.append(" UNION ").append(generateValueCardReportGroupSummaryQuery());
        }
        
        // Order By
        super.getOrderByStandard(sqlQuery, isSummary);
        return sqlQuery.toString();
    }
    
    @Override
    protected final String createSqlRecordCountQuery() throws Exception {
        final boolean isSummary = this.reportDefinition.isIsDetailsOrSummary();
        final StringBuilder sqlQuery = new StringBuilder("SELECT ");
        
        // Fields
        sqlQuery.append(" COUNT(*) ");
        
        // Tables & Where
        super.getTables(sqlQuery, false, isSummary);
        super.getWhereStandardTransaction(sqlQuery, false, isSummary);
        
        // Value Card Number
        final int cardNumFilterId = super.getFilterTypeId(this.reportDefinition.getCardNumberFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_CARD_NUMBER_SPECIFIC == cardNumFilterId) {
            sqlQuery.append(" AND (cc.CardNumber='");
            sqlQuery.append(this.reportDefinition.getCardNumberSpecificFilter());
            sqlQuery.append("')");
        }
        
        return sqlQuery.toString();
    }
    
    private String generateValueCardReportGroupSummaryQuery() throws Exception {
        final StringBuilder sqlQuery = new StringBuilder("(SELECT ");
        
        // Fields
        super.getFieldsGroupField(sqlQuery, true, false);
        super.getFieldsValueCard(sqlQuery, true);
        super.getSummaryFieldTotalTransaction(sqlQuery, true);
        
        // Tables & Where
        super.getTables(sqlQuery, true, false);
        super.getWhereStandardTransaction(sqlQuery, true, false);
        
        // Value Card Number
        final int cardNumFilterId = super.getFilterTypeId(this.reportDefinition.getCardNumberFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_CARD_NUMBER_SPECIFIC == cardNumFilterId) {
            sqlQuery.append(" AND (cc.CardNumber='");
            sqlQuery.append(this.reportDefinition.getCardNumberSpecificFilter());
            sqlQuery.append("')");
        }
        sqlQuery.append(" GROUP BY GroupName)");
        
        return sqlQuery.toString();
    }
}
