package com.digitalpaytech.report.handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportLocationValue;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.WebCoreConstants;

public class AuditThread extends ReportBaseThread {
    private static final Logger LOGGER = Logger.getLogger(AuditThread.class);
    
    public AuditThread(final ReportDefinition reportDefinition, final ReportQueue reportQueue, final ApplicationContext applicationContext) {
        super(reportDefinition, reportQueue, applicationContext);
        this.jasperFileName = ReportingConstants.REPORT_JASPER_FILE_AUDITS;
    }
    
    @Override
    protected final String generateSQLString() throws Exception {
        final StringBuilder sqlQuery = new StringBuilder("SELECT ");
        
        getFieldGroupByString(sqlQuery);
        getFieldString(sqlQuery);
        getTableString(sqlQuery);
        getWhereString(sqlQuery, false);
        getOrderByString(sqlQuery);
        return sqlQuery.toString();
    }
    
    @Override
    protected final String createSqlRecordCountQuery() throws Exception {
        final StringBuilder countQuery = new StringBuilder("SELECT COUNT(*) ");
        getTableString(countQuery);
        getWhereString(countQuery, true);
        
        final String query = countQuery.toString();
        LOGGER.info("COUNT QUERY : " + query);
        return query;
    }
    
    @Override
    protected final int getMaxRecordCount(final Errors errors) throws Exception {
        int maxRecords = ReportingConstants.REPORTS_DEFAULT_MAX_RECORDS;
        
        if (this.reportDefinition.isIsDetailsOrSummary()) {
            maxRecords = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.REPORTS_AUDIT_SUMMARY_MAX_RECORDS,
                                                                         ReportingConstants.REPORTS_DEFAULT_AUDIT_SUMMARY_MAX_RECORDS);
        } else if (!this.reportDefinition.isIsCsvOrPdf()) {
            maxRecords = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.REPORTS_CSV_AUDIT_DETAILS_MAX_RECORDS,
                                                                         ReportingConstants.REPORTS_DEFAULT_CSV_AUDIT_DETAILS_MAX_RECORDS);
        } else if (this.reportDefinition.isIsCsvOrPdf()) {
            maxRecords = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.REPORTS_PDF_AUDIT_DETAILS_MAX_RECORDS,
                                                                         ReportingConstants.REPORTS_DEFAULT_PDF_AUDIT_DETAILS_MAX_RECORDS);
        }
        
        return maxRecords;
    }
    
    @Override
    protected final String createFileName() {
        final StringBuilder sb = new StringBuilder();
        sb.append(reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_AUDIT));
        return super.createFileName(sb);
    }
    
    private void getFieldGroupByString(final StringBuilder query) {
        final int groupByType = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_MACHINE == groupByType) {
            query.append("pos.Name AS GroupField");
        } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == groupByType && this.reportDefinition.isIsCsvOrPdf()) {
            query.append("r.Name AS GroupField");
        } else {
            query.append("'NoGroup' AS GroupField");
        }
    }
    
    private void getFieldString(final StringBuilder query) {
        query.append(", '");
        query.append(this.timeZone);
        query.append("' AS TimeZone, posc.*, ");
        query.append(" pos.SerialNumber AS SerialNumber, ");
        //TODO PointOfSaleType changed to PayStationType
        query.append("pos.Name, p.PayStationTypeId AS PaystationType, pos.Name AS FullMachineName, ");
        query.append("IF(posc.NextTicketNumber=0, NULL, (posc.NextTicketNumber - 1)) as CalculatedEndTicketNumber, ");
        query.append("(posc.Tube1Amount + posc.Tube2Amount + posc.Tube3Amount + posc.Tube4Amount) AS TubeTotalAmount, ");
        query.append("(ROUND(   CAST(posc.ChangeIssuedAmount AS SIGNED)   ) - CAST(   (posc.Hopper1Dispensed + posc.Hopper2Dispensed) AS SIGNED)) AS ChangerDispensed, ");
        query.append("(posc.Hopper1Current + posc.Hopper2Current) AS HopperTotalCurrent, ");
        query.append("0 AS CitationsPaid, 0 AS CitationAmount, ");
        query.append("MAX(CONCAT(ua.FirstName,' ', ua.LastName)) AS CollectionUserName, ");
        // TODO Card Total might have to be re calculated
        // query.append("(posc.AmexAmount + posc.DinersAmount + posc.DiscoverAmount + posc.MasterCardAmount + posc.VisaAmount + posc.SmartCardAmount + posc.ValueCardAmount) AS CcAmount ");
        
        final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == filterTypeId && this.reportDefinition.isIsCsvOrPdf()) {
            query.append("pos.Id as PaystationId, r.Id as PaystationRouteId");
        } else {
            query.append("0 as PaystationId, 0 as PaystationRouteId");
        }
    }
    
    private void getTableString(final StringBuilder query) {
        query.append(" FROM POSCollection posc INNER JOIN PointOfSale pos ON posc.PointOfSaleId=pos.Id");
        query.append(" LEFT JOIN Paystation p ON pos.PayStationId = p.Id");
        query.append(" LEFT JOIN POSCollectionUser poscu ON posc.Id = poscu.POSCollectionId");
        query.append(" LEFT JOIN UserAccount ua ON poscu.UserAccountId = ua.Id");
        
        final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == filterTypeId && this.reportDefinition.isIsCsvOrPdf()) {
            query.append("LEFT JOIN RoutePOS rpos ON pos.Id=rpos.PointOfSaleId ");
            query.append("LEFT JOIN Route r ON rpos.RouteId=r.Id ");
        }
        // Filter for hidden POS
        getFromHiddenPointOfSales(query, "pos");
        
    }
    
    private void getWhereString(final StringBuilder query, final boolean isSummary) {
        query.append(" WHERE ");
        
        super.getWherePointOfSale(query, "pos", false);
        
        //        super.getWherePointOfSaleRouteId(query, true);
        
        // Start Date & Time
        super.getWhereDate(query, "posc", "StartGMT", false);
        
        // End Date & Time
        super.getWhereDate(query, "posc", "EndGMT", true);
        
        // Report Type
        getWhereReportType(query);
        
        // Filter for hidden POS
        super.getWhereHiddenPointOfSales(query);
        
    }
    
    private void getOrderByString(final StringBuilder query) {
        // Needed because of POSCollectionUsers can be more than one
        query.append(" GROUP BY posc.Id");

        query.append(" ORDER BY ");
        
        final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        if (ReportingConstants.REPORT_FILTER_GROUP_BY_MACHINE == filterTypeId) {
            query.append("GroupField, posc.StartGMT, posc.EndGMT");
            
        } else if (ReportingConstants.REPORT_FILTER_GROUP_BY_PAY_STATION_ROUTE == filterTypeId && this.reportDefinition.isIsCsvOrPdf()) {
            query.append("GroupField, posc.StartGMT, posc.EndGMT");
        } else {
            query.append("FullMachineName, posc.StartGMT, posc.EndGMT");
        }
    }
    
    private void getWhereReportType(final StringBuilder query) {
        final int reportNumberType = super.getFilterTypeId(this.reportDefinition.getTicketNumberFilterTypeId());
        
        switch (reportNumberType) {
            case ReportingConstants.REPORT_FILTER_REPORT_NUMBER_ALL:
                break;
            case ReportingConstants.REPORT_FILTER_REPORT_NUMBER_SPECIFIC:
                query.append(" AND (posc.ReportNumber=");
                query.append(this.reportDefinition.getTicketNumberBeginFilter());
                query.append(") ");
                break;
            case ReportingConstants.REPORT_FILTER_REPORT_NUMBER_LESS:
                query.append(" AND (posc.ReportNumber BETWEEN 0 AND ");
                query.append(this.reportDefinition.getTicketNumberBeginFilter());
                query.append(") ");
                break;
            case ReportingConstants.REPORT_FILTER_REPORT_NUMBER_GREATER:
                query.append(" AND (posc.ReportNumber>=");
                query.append(this.reportDefinition.getTicketNumberBeginFilter());
                query.append(") ");
                break;
            case ReportingConstants.REPORT_FILTER_REPORT_NUMBER_BETWEEN:
                query.append(" AND (posc.ReportNumber BETWEEN ");
                query.append(this.reportDefinition.getTicketNumberBeginFilter());
                query.append(" AND ");
                query.append(this.reportDefinition.getTicketNumberEndFilter());
                query.append(") ");
                break;
            default:
                break;
        }
        
        final int reportType = super.getFilterTypeId(this.reportDefinition.getOtherParametersTypeFilterTypeId());
        if (reportType != ReportingConstants.REPORT_FILTER_REPORT_ALL) {
            int collectionType = 0;
            switch (reportType) {
                case ReportingConstants.REPORT_FILTER_REPORT_COLLECTION_REPORT_BILL:
                    collectionType = WebCoreConstants.COLLECTION_TYPE_BILL;
                    break;
                case ReportingConstants.REPORT_FILTER_REPORT_COLLECTION_REPORT_COIN:
                    collectionType = WebCoreConstants.COLLECTION_TYPE_COIN;
                    break;
                case ReportingConstants.REPORT_FILTER_REPORT_COLLECTION_REPORT_CREDIT_CARD:
                    collectionType = WebCoreConstants.COLLECTION_TYPE_CARD;
                    break;
                case ReportingConstants.REPORT_FILTER_REPORT_COLLECTION_REPORT_ALL:
                    collectionType = WebCoreConstants.COLLECTION_TYPE_ALL;
                    break;
                default:
                    break;
            }
            
            query.append(" AND posc.CollectionTypeId=");
            query.append(collectionType);
            query.append(" ");
        }
    }
    
    protected final Map<String, Object> createReportParameterList(final String query, final int customerId) {
        final Map<String, Object> parameters = super.createReportParameterList(query, customerId);
        
        parameters.put("ReportTitle",
                       super.getTitle(reportingUtil.getPropertyEN("label.siteName") + " "
                                      + reportingUtil.getPropertyEN(ReportingConstants.REPORT_TYPE_NAME_AUDIT)));
        parameters.put("StartDateString", this.reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL));
        
        final int priDateFilterId = super.getFilterTypeId(this.reportDefinition.getPrimaryDateFilterTypeId());
        if (ReportingConstants.REPORT_SELECTION_ALL == priDateFilterId) {
            parameters.put("EndDateString", reportingUtil.getPropertyEN(ReportingConstants.REPORT_LABEL_ALL));
        } else {
            parameters
                    .put("EndDateString",
                         createReportParameterDateString(this.reportQueue.getPrimaryDateRangeBeginGmt(), this.reportQueue.getPrimaryDateRangeEndGmt()));
            
        }
        
        parameters.put("LocationType", reportingUtil.findFilterString(this.reportDefinition.getLocationFilterTypeId()));
        parameters.put("MachineNameOrNumber", convertArrayToDelimiterString(this.locationValueNameList, ','));
        
        parameters.put("ReportNumberOption", reportingUtil.findFilterString(this.reportDefinition.getTicketNumberFilterTypeId()));
        parameters.put("ReportNumberLow", this.reportDefinition.getTicketNumberBeginFilter() != null ? this.reportDefinition.getTicketNumberBeginFilter()
                .toString() : "");
        parameters.put("ReportNumberHigh", this.reportDefinition.getTicketNumberEndFilter() != null ? this.reportDefinition.getTicketNumberEndFilter()
                .toString() : "");
        
        parameters.put("Grouping", reportingUtil.findFilterString(this.reportDefinition.getGroupByFilterTypeId()));
        parameters.put("ReportType", reportingUtil.findFilterString(this.reportDefinition.getOtherParametersTypeFilterTypeId()));
        
        parameters.put("IsCsv", !this.reportDefinition.isIsCsvOrPdf());
        parameters.put("IsPdf", this.reportDefinition.isIsCsvOrPdf());
        parameters.put("IsDetails", !this.reportDefinition.isIsDetailsOrSummary());
        parameters.put("IsCsvDetails", !this.reportDefinition.isIsCsvOrPdf() && !this.reportDefinition.isIsDetailsOrSummary());
        parameters.put("IsPdfDetails", this.reportDefinition.isIsCsvOrPdf() && !this.reportDefinition.isIsDetailsOrSummary());
        
        final int filterTypeId = super.getFilterTypeId(this.reportDefinition.getGroupByFilterTypeId());
        parameters.put("IsPdfGrouping", reportDefinition.isIsCsvOrPdf() && ReportingConstants.REPORT_FILTER_GROUP_BY_NONE != filterTypeId);
        
        if (reportDefinition.isIsCsvOrPdf() && ReportingConstants.REPORT_FILTER_GROUP_BY_NONE == filterTypeId) {
            final List<ReportLocationValue> locationList = this.reportLocationValueService.findReportLocationValuesByReportDefinition(this.reportDefinition.getId());
            final String[] rawSelectedGroupIds = getRawSelectedGroupIds(locationList);
            parameters.put("DuplicatedPaystationRouteMap", getAllDuplicatedPointOfSaleRouteMap(customerId, rawSelectedGroupIds));
        } else {
            parameters.put("DuplicatedPaystationRouteMap", new HashMap<Integer, List<Integer>>());
        }
        
        return parameters;
    }
}
