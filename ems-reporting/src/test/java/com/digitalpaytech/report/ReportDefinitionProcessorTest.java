package com.digitalpaytech.report;

import org.junit.Test;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.report.scriptlets.ReportsScriptlet;
import com.digitalpaytech.util.ReportingConstants;

import junit.framework.Assert;

public class ReportDefinitionProcessorTest extends ReportDefinitionProcessor {
    
    private static final int SUBSCRIPTION_COLUMN3 = 3;
    private static final int SUBSCRIPTION_COLUMN2 = 2;
    private static final int SUBSCRIPTION_COLUMN1 = 1;
    
    @Test
    public final void isDateCalculatedCorrect() {
        final ReportDefinition reportDefinition = new ReportDefinition();
        // Methods copied to service layer
        /*
         * // initial date is 01/01/2012 12:30 Calendar initialDate =
         * Calendar.getInstance(); initialDate.set(Calendar.DAY_OF_YEAR, 1);
         * initialDate.set(Calendar.HOUR_OF_DAY, 12); initialDate.set(Calendar.MINUTE,
         * 30); initialDate.set(Calendar.SECOND, 0);
         * initialDate.set(Calendar.MILLISECOND, 0);
         * reportDefinition.setScheduledToRunGmt(((Calendar)
         * initialDate.clone()).getTime()); // TEST 1 daily at 13:30
         * reportDefinition.setReportRepeatType(new
         * ReportRepeatType(ReportingConstants.REPORT_REPEAT_TYPE_DAILY, "Daily", new
         * Date(), 0)); reportDefinition.setReportRepeatTimeOfDay("13:30"); // should be
         * 2 JAN 13:30 calculateNextReportDate(reportDefinition); Calendar compareDate =
         * (Calendar) initialDate.clone(); compareDate.set(Calendar.DAY_OF_YEAR, 2);
         * compareDate.set(Calendar.HOUR_OF_DAY, 13); compareDate.set(Calendar.MINUTE,
         * 30); System.out.println(reportDefinition.getScheduledToRunGmt().getTime());
         * System.out.println(compareDate.getTimeInMillis());
         * System.out.println(compareDate.getTime());
         * assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // TEST 2 weekly every Monday at 14:30
         * reportDefinition.setReportRepeatType(new
         * ReportRepeatType(ReportingConstants.REPORT_REPEAT_TYPE_WEEKLY, "Weekly", new
         * Date(), 1)); reportDefinition.setReportRepeatTimeOfDay("14:30");
         * reportDefinition.setReportRepeatFrequency((byte)1);
         * reportDefinition.setReportRepeatDays("2"); // should be 9 JAN 14:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_YEAR, 9);
         * compareDate.set(Calendar.HOUR_OF_DAY, 14); compareDate.set(Calendar.MINUTE,
         * 30); assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // TEST 3 weekly every Monday, Thuesday and
         * Wednesday at 11:30 reportDefinition.setReportRepeatTimeOfDay("13:30");
         * reportDefinition.setReportRepeatFrequency((byte)1);
         * reportDefinition.setReportRepeatDays("2,3,4"); // should be 10 JAN 13:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.HOUR_OF_DAY, 13); compareDate.set(Calendar.MINUTE,
         * 30); compareDate.set(Calendar.DAY_OF_YEAR, 10);
         * assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // should be 11 JAN 13:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_YEAR, 11);
         * assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // should be 16 JAN 13:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_YEAR, 16);
         * assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // TEST 4 bi-weekly every Thuesday and
         * Thursday at 11:30 reportDefinition.setReportRepeatFrequency((byte)2);
         * reportDefinition.setReportRepeatDays("3,5"); // should be 17 JAN 13:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_YEAR, 17);
         * assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // should be 19 JAN 13:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_YEAR, 19);
         * assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // should be 31 JAN 13:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_YEAR, 31);
         * assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // should be 2 FEB 13:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_MONTH, 2); compareDate.set(Calendar.MONTH,
         * 1); assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // TEST 5 monthly every 31th day of the month
         * at 11:30 reportDefinition.setReportRepeatType(new
         * ReportRepeatType(ReportingConstants.REPORT_REPEAT_TYPE_MONTHLY, "Monthly",
         * new Date(), 1)); reportDefinition.setReportRepeatFrequency((byte)1);
         * reportDefinition.setReportRepeatDays("31"); // should be 31 MAR 13:30 - 1 MAR
         * is skipped because normally changing the schedule would fix the date, the
         * calculater // assumes that the schedule has not changed since last report.
         * When the schedule is changed the date in the DB // should be set to the first
         * day of the new schedule calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_MONTH, 31); compareDate.set(Calendar.MONTH,
         * 2); assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // should be 1 MAY 13:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_MONTH, 1); compareDate.set(Calendar.MONTH,
         * 4); assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // should be 31 MAY 13:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_MONTH, 31); compareDate.set(Calendar.MONTH,
         * 4); assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // TEST 6 monthly every 15 and 30th day of
         * the month at 11:30 reportDefinition.setReportRepeatDays("15,31"); // should
         * be 15 JUN 13:30 calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_MONTH, 15); compareDate.set(Calendar.MONTH,
         * 5); assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // should be 1 JUL 13:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_MONTH, 1); compareDate.set(Calendar.MONTH,
         * 6); assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // should be 15 JUL 13:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_MONTH, 15); compareDate.set(Calendar.MONTH,
         * 6); assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // should be 31 JUL 13:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_MONTH, 31); compareDate.set(Calendar.MONTH,
         * 6); assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // TEST 7 monthly every 31th day of the month
         * at 11:30 - For testing February 2013 reportDefinition.setReportRepeatType(new
         * ReportRepeatType(ReportingConstants.REPORT_REPEAT_TYPE_MONTHLY, "Monthly",
         * new Date(), 1)); reportDefinition.setReportRepeatDays("31"); // should be 31
         * AUG 13:30 calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_MONTH, 31); compareDate.set(Calendar.MONTH,
         * 7); assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // should be 1 OCT 13:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_MONTH, 1); compareDate.set(Calendar.MONTH,
         * 9); assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // should be 31 OCT 13:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_MONTH, 31); compareDate.set(Calendar.MONTH,
         * 9); assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // should be 1 DEC 13:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_MONTH, 1); compareDate.set(Calendar.MONTH,
         * 11); assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // should be 31 DEC 13:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_MONTH, 31); compareDate.set(Calendar.MONTH,
         * 11); assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // TEST 8 monthly every 15 and 30th day of
         * the month at 11:30 reportDefinition.setReportRepeatDays("15,31"); // should
         * be 15 JAN 13:30 calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_MONTH, 15); compareDate.set(Calendar.MONTH,
         * 0); compareDate.set(Calendar.YEAR, 2013);
         * assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // should be 31 JAN 13:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_MONTH, 31); compareDate.set(Calendar.MONTH,
         * 0); assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // should be 15 FEB 13:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_MONTH, 15); compareDate.set(Calendar.MONTH,
         * 1); assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // should be 1 MAR 13:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_MONTH, 1); compareDate.set(Calendar.MONTH,
         * 2); assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // should be 15 MAR 13:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_MONTH, 15); compareDate.set(Calendar.MONTH,
         * 2); assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis()); // should be 31 MAR 13:30
         * calculateNextReportDate(reportDefinition);
         * compareDate.set(Calendar.DAY_OF_MONTH, 31); compareDate.set(Calendar.MONTH,
         * 2); assertTrue(reportDefinition.getScheduledToRunGmt().getTime() ==
         * compareDate.getTimeInMillis());
         */
    }
    
    @Test
    public final void isLayoutCorrectForSubscriptionsNegativeTests() {
        final ReportsScriptlet rs = new ReportsScriptlet();
        
        // enable all subscriptions but specify column number as less than 1 - column results should be empty
        String expectedResult = "";
        String actualResult = rs.formatSubscriptions(true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
                                                     true, true, true, true, 0);
        Assert.assertEquals(expectedResult, actualResult);
        
        // enable all subscriptions but specify column number as less than 0 - column results should be empty
        expectedResult = "";
        actualResult = rs.formatSubscriptions(true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
                                              true, true, true, -1);
        Assert.assertEquals(expectedResult, actualResult);
        
        // enable all subscriptions but specify column number as greater than Allowed number of columns - column results should be empty
        expectedResult = "";
        actualResult = rs.formatSubscriptions(true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
                                              true, true, true, ReportingConstants.SUBSCRIPTIONS_NUMBEROFALLOWEDCOLUMNS + 1);
        Assert.assertEquals(expectedResult, actualResult);
    }
    
    @Test
    public final void isLayoutCorrectForSubscriptionsPositiveTests() {
        final ReportsScriptlet rs = new ReportsScriptlet();
        
        // enable 0 subscriptions - first column show say none, rest should say nothing
        // col 1
        String expectedResult = ReportingConstants.SUBSCRIPTIONS_NONE + ReportingConstants.SUBSCRIPTIONS_NEW_LINE;
        String actualResult = rs.formatSubscriptions(false, false, false, false, false, false, false, false, false, false, false, false, false, false,
                                                     false, false, false, false, false, false, SUBSCRIPTION_COLUMN1);
        Assert.assertEquals(expectedResult, actualResult);
        
        // col 2
        expectedResult = "";
        actualResult = rs.formatSubscriptions(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
                                              false, false, false, false, false, SUBSCRIPTION_COLUMN2);
        Assert.assertEquals(expectedResult, actualResult);
        
        // col 3
        expectedResult = "";
        actualResult = rs.formatSubscriptions(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
                                              false, false, false, false, false, SUBSCRIPTION_COLUMN3);
        Assert.assertEquals(expectedResult, actualResult);
        
        // enable 1 subscription - first column will have the subscription name, rest of
        // the columns will be empty
        // col 1
        expectedResult = ReportingConstants.SUBSCRIPTIONS_FLEXINTEGRATION + ReportingConstants.SUBSCRIPTIONS_NEW_LINE;
        actualResult = rs.formatSubscriptions(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
                                              true, false, false, false, false, SUBSCRIPTION_COLUMN1);
        Assert.assertEquals(expectedResult, actualResult);
        
        // col 2
        expectedResult = "";
        actualResult = rs.formatSubscriptions(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
                                              true, false, false, false, false, SUBSCRIPTION_COLUMN2);
        Assert.assertEquals(expectedResult, actualResult);
        
        // col 3
        expectedResult = "";
        actualResult = rs.formatSubscriptions(false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
                                              true, false, false, false, false, SUBSCRIPTION_COLUMN3);
        Assert.assertEquals(expectedResult, actualResult);
        
        // enable 3 subscription - each column will return 1 subscription name
        // col 1
        expectedResult = ReportingConstants.SUBSCRIPTIONS_STANDARDREPORTS + ReportingConstants.SUBSCRIPTIONS_NEW_LINE;
        actualResult = rs.formatSubscriptions(true, false, false, false, false, false, true, false, false, false, false, false, false, false, false,
                                              true, false, false, false, false, SUBSCRIPTION_COLUMN1);
        Assert.assertEquals(expectedResult, actualResult);
        
        // col 2
        expectedResult = ReportingConstants.SUBSCRIPTIONS_PASSCARDS + ReportingConstants.SUBSCRIPTIONS_NEW_LINE;
        actualResult = rs.formatSubscriptions(true, false, false, false, false, false, true, false, false, false, false, false, false, false, false,
                                              true, false, false, false, false, SUBSCRIPTION_COLUMN2);
        Assert.assertEquals(expectedResult, actualResult);
        
        // col 3
        expectedResult = ReportingConstants.SUBSCRIPTIONS_FLEXINTEGRATION + ReportingConstants.SUBSCRIPTIONS_NEW_LINE;
        actualResult = rs.formatSubscriptions(true, false, false, false, false, false, true, false, false, false, false, false, false, false, false,
                                              true, false, false, false, false, SUBSCRIPTION_COLUMN3);
        Assert.assertEquals(expectedResult, actualResult);
        
        // enable 7 subscriptions - first column will have 3 subscription, second and
        // third columns will return 2 subscription each
        // col 1
        expectedResult = ReportingConstants.SUBSCRIPTIONS_STANDARDREPORTS + ReportingConstants.SUBSCRIPTIONS_NEW_LINE
                         + ReportingConstants.SUBSCRIPTIONS_COUPONS + ReportingConstants.SUBSCRIPTIONS_NEW_LINE
                         + ReportingConstants.SUBSCRIPTIONS_FLEXINTEGRATION;
        actualResult = rs.formatSubscriptions(true, true, false, true, false, true, true, false, false, false, false, false, true, false, false, true,
                                              false, false, false, false, SUBSCRIPTION_COLUMN1);
        Assert.assertEquals(expectedResult, actualResult);
        
        // col 2
        expectedResult =
                ReportingConstants.SUBSCRIPTIONS_ALERTS + ReportingConstants.SUBSCRIPTIONS_NEW_LINE + ReportingConstants.SUBSCRIPTIONS_PASSCARDS;
        actualResult = rs.formatSubscriptions(true, true, false, true, false, true, true, false, false, false, false, false, true, false, false, true,
                                              false, false, false, false, SUBSCRIPTION_COLUMN2);
        Assert.assertEquals(expectedResult, actualResult);
        
        // col 3
        expectedResult = ReportingConstants.SUBSCRIPTIONS_BATCHCREDITCARDPROCESSING + ReportingConstants.SUBSCRIPTIONS_NEW_LINE
                         + ReportingConstants.SUBSCRIPTIONS_MOBILEDIGITALCOLLECT;
        actualResult = rs.formatSubscriptions(true, true, false, true, false, true, true, false, false, false, false, false, true, false, false, true,
                                              false, false, false, false, SUBSCRIPTION_COLUMN3);
        Assert.assertEquals(expectedResult, actualResult);
        
        // enable all subscriptions
        // col 1
        expectedResult = ReportingConstants.SUBSCRIPTIONS_STANDARDREPORTS + ReportingConstants.SUBSCRIPTIONS_NEW_LINE
                         + ReportingConstants.SUBSCRIPTIONS_BATCHCREDITCARDPROCESSING + ReportingConstants.SUBSCRIPTIONS_NEW_LINE
                         + ReportingConstants.SUBSCRIPTIONS_PASSCARDS + ReportingConstants.SUBSCRIPTIONS_NEW_LINE
                         + ReportingConstants.SUBSCRIPTIONS_DIGITALAPIREAD + ReportingConstants.SUBSCRIPTIONS_NEW_LINE
                         + ReportingConstants.SUBSCRIPTIONS_MOBILEDIGITALCOLLECT + ReportingConstants.SUBSCRIPTIONS_NEW_LINE
                         + ReportingConstants.SUBSCRIPTIONS_FLEXINTEGRATION + ReportingConstants.SUBSCRIPTIONS_NEW_LINE
                         + ReportingConstants.SUBSCRIPTIONS_ONLINERATECONFIGURATION;
        actualResult = rs.formatSubscriptions(true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
                                              true, true, true, SUBSCRIPTION_COLUMN1);
        Assert.assertEquals(expectedResult, actualResult);
        
        // col 2
        expectedResult = ReportingConstants.SUBSCRIPTIONS_ALERTS + ReportingConstants.SUBSCRIPTIONS_NEW_LINE
                         + ReportingConstants.SUBSCRIPTIONS_REALTIMECAMPUSCARDPROCESSING + ReportingConstants.SUBSCRIPTIONS_NEW_LINE
                         + ReportingConstants.SUBSCRIPTIONS_SMARTCARDS + ReportingConstants.SUBSCRIPTIONS_NEW_LINE
                         + ReportingConstants.SUBSCRIPTIONS_DIGITALAPIWRITE + ReportingConstants.SUBSCRIPTIONS_NEW_LINE
                         + ReportingConstants.SUBSCRIPTIONS_MOBILEDIGITALMAINTAIN + ReportingConstants.SUBSCRIPTIONS_NEW_LINE
                         + ReportingConstants.SUBSCRIPTIONS_AUTOCOUNTINTEGRATION + ReportingConstants.SUBSCRIPTIONS_NEW_LINE
                         + ReportingConstants.SUBSCRIPTIONS_ONLINEPSCONFIGURATION;
        actualResult = rs.formatSubscriptions(true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
                                              true, true, true, SUBSCRIPTION_COLUMN2);
        Assert.assertEquals(expectedResult, actualResult);
        
        // col 3
        expectedResult = ReportingConstants.SUBSCRIPTIONS_REALTIMECREDITCARDPROCESSING + ReportingConstants.SUBSCRIPTIONS_NEW_LINE
                         + ReportingConstants.SUBSCRIPTIONS_COUPONS + ReportingConstants.SUBSCRIPTIONS_NEW_LINE
                         + ReportingConstants.SUBSCRIPTIONS_EXTENDBYPHONE + ReportingConstants.SUBSCRIPTIONS_NEW_LINE
                         + ReportingConstants.SUBSCRIPTIONS_THIRDPARTYPAYBYCELLINTEGRATION + ReportingConstants.SUBSCRIPTIONS_NEW_LINE
                         + ReportingConstants.SUBSCRIPTIONS_MOBILEDIGITALPATROL + ReportingConstants.SUBSCRIPTIONS_NEW_LINE
                         + ReportingConstants.SUBSCRIPTIONS_DIGITALAPIXCHANGE;
        
        actualResult = rs.formatSubscriptions(true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
                                              true, true, true, SUBSCRIPTION_COLUMN3);
        Assert.assertEquals(expectedResult, actualResult);
    }
}
