package com.digitalpaytech.scheduling.processor.service;

import com.digitalpaytech.exception.CardProcessorPausedException;

public interface ElavonViaConexPreAuthReversalService {

    void reversalProcessingManager();
    
    void processReversal(Long preAuthId) throws CardProcessorPausedException;
}
