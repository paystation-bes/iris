package com.digitalpaytech.scheduling.processor.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.cardprocessing.CardProcessingManager;
import com.digitalpaytech.cardprocessing.CardProcessingMaster;
import com.digitalpaytech.cardprocessing.CardProcessorFactory;
import com.digitalpaytech.cardprocessing.impl.ElavonViaConexProcessor;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.exception.CardProcessorPausedException;
import com.digitalpaytech.scheduling.processor.service.ElavonViaConexPreAuthReversalService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.PreAuthService;
import com.digitalpaytech.service.ServiceHelper;

@Service(ElavonViaConexPreAuthReversalServiceImpl.SERVICE_NAME)
@Transactional(propagation = Propagation.REQUIRED)
public class ElavonViaConexPreAuthReversalServiceImpl implements ElavonViaConexPreAuthReversalService {
    public static final String SERVICE_NAME = "elavonViaConexPreAuthReversalService";
    
    private static final Logger LOGGER = Logger.getLogger(ElavonViaConexPreAuthReversalServiceImpl.class);
    
    private static final double NANO_PER_SECOND = 1000000000.0;
    private static final int DEFAULT_ELAVON_PREAUTH_EXPIRY_TIME_IN_HRS = 24;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private PreAuthService preAuthService;
    
    @Autowired
    private CardProcessorFactory cardProcessorFactory;
    
    @Autowired
    private CardProcessingManager cardProcessingManager;
    
    @Autowired
    private CardProcessingMaster cardProcessingMaster;
    
    @Autowired
    private ServiceHelper serviceHelper;
    
    public final void setCardProcessingManager(final CardProcessingManager cardProcessingManager) {
        this.cardProcessingManager = cardProcessingManager;
    }
    public final void setCardProcessorFactory(final CardProcessorFactory cardProcessorFactory) {
        this.cardProcessorFactory = cardProcessorFactory;
    }    
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }

    @Override
    @SuppressWarnings("checkstyle:illegalcatch")
    @Transactional(propagation = Propagation.SUPPORTS)
    public final void reversalProcessingManager() {
        if (!this.cardProcessingMaster.isScheduledCardProcessingServer()) {
            LOGGER.debug("This tomcat is not the card processing server!");
            return;
        }
        
        final long startTime = System.nanoTime();
        
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("reversalProcessingManager started");
        }
        final int expiryTimeInHrs = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.ELAVON_PREAUTH_EXPIRY_TIME_IN_HRS,
                DEFAULT_ELAVON_PREAUTH_EXPIRY_TIME_IN_HRS);
        
        final int preAuthLimit = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.ELAVON_MAX_PREAUTH_REVERSAL_LIMIT,
                EmsPropertiesService.DEFAULT_ELAVON_MAX_PREAUTH_REVERSAL_LIMIT);
        
        final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.add(Calendar.HOUR_OF_DAY, -expiryTimeInHrs);
        final Date expiryDate = cal.getTime();
        
        final List<Long> preAuthList = this.preAuthService.findExpiredElavonPreAuths(expiryDate, preAuthLimit);
        if (preAuthList.isEmpty()) {
            LOGGER.info("No PreAuth to be processed");
            return;
        }
        
        for (Long pa : preAuthList) {
            try {
                self().processReversal(pa);
            } catch (Exception e) {
                // Catch every exception because if one of the pre-auth keep failing, it will prevent us from processing further pre-auth.
                LOGGER.error("Failed to process reversal for PreAuth ID: " + pa, e);
            }
        }
        
        if (LOGGER.isTraceEnabled()) {
            final long endTime = System.nanoTime();
            final long elapsedTime = endTime - startTime;
            final double seconds = (double) elapsedTime / NANO_PER_SECOND;
            LOGGER.debug("reversalProcessingManager ended");
            LOGGER.debug(showTimerMessage("reversalProcessingManager duration: ", seconds));
        }
    }
    
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public final void processReversal(final Long preAuthId) throws CardProcessorPausedException {
        final List<PreAuth> paList = this.preAuthService.findByIds(preAuthId);
        if (!paList.isEmpty()) {
            final PreAuth pa = paList.get(0);
            final ElavonViaConexProcessor ep = (ElavonViaConexProcessor) this.cardProcessorFactory.getCardProcessor(pa.getMerchantAccount(),
                                                                                                                    pa.getPointOfSale());
            final boolean success = ep.processElavonReversal(pa);
            if (success) {
                this.cardProcessingManager.processElavonReversalCleanup(paList);
            }
        }
    }
    
    private ElavonViaConexPreAuthReversalService self() {
        return this.serviceHelper.bean(SERVICE_NAME, ElavonViaConexPreAuthReversalService.class);
    }
    
    private String createExceptionMessage(final int processorId, final String merchantAccountName, final String exceptionMessage) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("Processor id: ").append(processorId);
        bdr.append("Merchant account name is: ").append(merchantAccountName);
        bdr.append("Exception message: ").append(exceptionMessage);
        return bdr.toString();
    }
    
    private String showTimerMessage(final String message, final double timer) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append(message).append(timer);
        return bdr.toString();
    }

}
