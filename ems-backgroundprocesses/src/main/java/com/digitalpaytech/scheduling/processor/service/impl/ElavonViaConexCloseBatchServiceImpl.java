package com.digitalpaytech.scheduling.processor.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.ShortType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.scheduling.annotation.Async;

import com.digitalpaytech.cardprocessing.CardProcessingMaster;
import com.digitalpaytech.cardprocessing.CardProcessorFactory;
import com.digitalpaytech.cardprocessing.impl.ElavonViaConexProcessor;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.data.ElavonBatchInfo;
import com.digitalpaytech.domain.ActiveBatchSummary;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.MerchantAccount;

import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.exception.CardProcessorPausedException;
import com.digitalpaytech.scheduling.processor.dto.ElavonBatchDTO;
import com.digitalpaytech.scheduling.processor.service.ElavonViaConexCloseBatchService;
import com.digitalpaytech.service.ActiveBatchSummaryService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.ServiceHelper;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.processor.ElavonViaConexService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Service("elavonViaConexCloseBatchService")
@Transactional(propagation = Propagation.REQUIRED)
@SuppressWarnings("PMD.ExcessiveImports")
public class ElavonViaConexCloseBatchServiceImpl implements ElavonViaConexCloseBatchService {
    private static final Logger LOGGER = Logger.getLogger(ElavonViaConexCloseBatchServiceImpl.class);
    
    private static final int DEFAULT_REAL_TIME_ELAVON_RETRY_ATTEMPTS = 3;
    private static final int DEFAULT_ELAVON_RETRY_WAITING_TIME = 1000;
    private static final int LENGTH_OF_FIRST_PART = 6;
    private static final String ELAVON_CLOSE_TIME_INTERVAL = "elavonCloseTimeInterval";
    private static final String GET_BATCH_FOR_PROCESSING_CORE_SQL = "SELECT abs.Id AS 'ActiveBatchSummaryId', abs.BatchNumber AS 'BatchNumber', "
                                                                    + "abs.MerchantAccountId AS 'MerchantAccountId', "
                                                                    + "abs.NoOfTransactions AS 'NoOfTransactions' "
                                                                    + "FROM ActiveBatchSummary abs "
                                                                    + "INNER JOIN MerchantAccount ma ON abs.MerchantAccountId = ma.Id ";

    private static final String GET_JOIN_TYPE_LEFT = "LEFT ";
    private static final String GET_BATCH_FOR_PROCESSING_JOIN_LASTBATCH_SQL = "JOIN (SELECT MAX(bs.BatchSettlementStartGMT) "
                                                                              + "AS 'lastBatchClosedByTimeTime', abs2.MerchantAccountId "
                                                                              + "FROM ActiveBatchSummary abs2 "
                                                                              + "INNER JOIN BatchSettlement bs ON abs2.id = bs.ActiveBatchSummaryId "
                                                                              + "WHERE abs2.BatchStatusTypeId = 2 AND bs.BatchSettlementTypeId = 2 "
                                                                              + "GROUP BY abs2.MerchantAccountId) X "
                                                                              + "ON X.MerchantAccountId = abs.MerchantAccountId ";
    
    private static final String GET_BATCH_FOR_PROCESSING_WHERE_CORE_SQL = "WHERE abs.BatchStatusTypeId IN (1,3,5) ";
    private static final String GET_BATCH_FOR_PROCESSING_WHERE_JOIN_MERCHANT_NULL_SQL = "AND (X.MerchantAccountId IS NULL OR X.lastBatchClosedByTimeTime "
            + "< TIMESTAMPADD(MINUTE, -:" + ELAVON_CLOSE_TIME_INTERVAL + ", UTC_TIMESTAMP())) ";


    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private CardProcessorFactory cardProcessorFactory;
    
    @Autowired
    private CardProcessingMaster cardProcessingMaster;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    @Autowired
    private ActiveBatchSummaryService activeBatchSummaryService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private ElavonViaConexService elavonViaConexService;
    
    @Autowired
    private ServiceHelper serviceHelper;
    
    @Autowired
    private MailerService mailerService;
    
    public final EntityDao getEntityDao() {
        return this.entityDao;
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final CardProcessorFactory getCardProcessorFactory() {
        return this.cardProcessorFactory;
    }
    
    public final void setCardProcessorFactory(final CardProcessorFactory cardProcessorFactory) {
        this.cardProcessorFactory = cardProcessorFactory;
    }
    
    public final MerchantAccountService getMerchantAccountService() {
        return this.merchantAccountService;
    }
    
    public final void setMerchantAccountService(final MerchantAccountService merchantAccountService) {
        this.merchantAccountService = merchantAccountService;
    }
    
    public final ActiveBatchSummaryService getActiveBatchSummaryService() {
        return this.activeBatchSummaryService;
    }
    
    public final CardProcessingMaster getCardProcessingMaster() {
        return this.cardProcessingMaster;
    }
    
    public final void setActiveBatchSummaryService(final ActiveBatchSummaryService activeBatchSummaryService) {
        this.activeBatchSummaryService = activeBatchSummaryService;
    }
    
    public final EmsPropertiesService getEmsPropertiesService() {
        return this.emsPropertiesService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final ElavonViaConexService getElavonViaConexService() {
        return this.elavonViaConexService;
    }
    
    public final void setElavonViaConexService(final ElavonViaConexService elavonViaConexService) {
        this.elavonViaConexService = elavonViaConexService;
    }
    
    public final void setServiceHelper(final ServiceHelper serviceHelper) {
        this.serviceHelper = serviceHelper;
    }
    
    public final void setMailerService(final MailerService mailerService) {
        this.mailerService = mailerService;
    }
    
    /*
     * Create a proxy to ElavonViaConexCloseBatchService interface.
     */
    private ElavonViaConexCloseBatchService self() {
        return this.serviceHelper.bean("elavonViaConexCloseBatchService", ElavonViaConexCloseBatchService.class);
    }
    
    private static String prefixStrings(final String...strings) {
        final StringBuilder bdr = new StringBuilder();
        for (int i = 0; i < strings.length; i++) {
            bdr.append(strings[i]);
        }
        return bdr.toString();
    }
    
    @SuppressWarnings("unchecked")
    private Collection<ElavonBatchInfo> getElavonBatchInfo(final String filterQuery) {
        final SQLQuery query = this.entityDao.createSQLQuery(GET_BATCH_FOR_PROCESSING_CORE_SQL + filterQuery);
        
        if (filterQuery.indexOf(prefixStrings(new String[] { StandardConstants.STRING_COLON, ELAVON_CLOSE_TIME_INTERVAL })) != -1) {
            query.setParameter(ELAVON_CLOSE_TIME_INTERVAL, 
                               this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.ELAVON_VIACONEX_CLOSE_TIME_INTERVAL_MINUTES,
                                                                               EmsPropertiesService.DEFAULT_ELAVON_VIACONEX_CLOSE_TIME_INTERVAL_MINUTES));
        }
        query.addScalar(HibernateConstants.COLUMN_ACTIVE_BATCH_SUMMARY_ID, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_BATCH_NUMBER, new ShortType());
        query.addScalar(HibernateConstants.COLUMN_MERCHANT_ACCOUNT_ID, new IntegerType());
        query.addScalar(HibernateConstants.COLUMN_NO_OF_TRANSACTIONS, new IntegerType());
        query.setResultTransformer(Transformers.aliasToBean(ElavonBatchInfo.class));
        query.setCacheable(false);
        return (List<ElavonBatchInfo>) query.list();
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public final void batchProcessingManager() {
        
        if (!this.cardProcessingMaster.isScheduledCardProcessingServer()) {
            LOGGER.debug("This tomcat is not the card processing server!");
            return;
        }
        
        final int noOfRetries = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.REAL_TIME_ELAVON_RETRY_ATTEMPTS,
                                                                                DEFAULT_REAL_TIME_ELAVON_RETRY_ATTEMPTS);
        final int waitInterval = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.ELAVON_RETRY_WAITING_TIME,
                                                                                 DEFAULT_ELAVON_RETRY_WAITING_TIME);
        final Set<ElavonBatchDTO> batchDTOs = new HashSet<ElavonBatchDTO>();
        
        // Close batch by predefined size and time.
        collectLimitPassedBatches(batchDTOs, noOfRetries, waitInterval);
        collectEndOfDayBatches(batchDTOs, noOfRetries, waitInterval);

        final Map<Integer, List<ElavonBatchDTO>> batchDtoListsByMerchAcct = new HashMap<Integer, List<ElavonBatchDTO>>();
        
        for (ElavonBatchDTO b : batchDTOs) {
            List<ElavonBatchDTO> elavonBatchDtoList = batchDtoListsByMerchAcct.get(b.getActiveBatchSummary().getMerchantAccount().getId());
            if (elavonBatchDtoList == null) {
                elavonBatchDtoList = new ArrayList<ElavonBatchDTO>();
                batchDtoListsByMerchAcct.put(b.getActiveBatchSummary().getMerchantAccount().getId(), elavonBatchDtoList);
            }
            elavonBatchDtoList.add(b);                                                       
        }                
        
        for (Map.Entry<Integer, List<ElavonBatchDTO>> me: batchDtoListsByMerchAcct.entrySet()) {                        
            LOGGER.info("Submitting " + me.getValue().size() + " batches to be closed for merchant account id: " + me.getKey());
            this.self().callProcessBatchCloseAsync(me.getKey(), me.getValue());
        }
    }
    
    @SuppressWarnings({ "PMD.UseConcurrentHashMap", "PMD.InsufficientStringBufferDeclaration", "PMD.AvoidInstantiatingObjectsInLoops" })
    private void collectEndOfDayBatches(final Set<ElavonBatchDTO> result, final int noOfRetries, final int waitInterval) {
        final List<CustomerProperty> customerPropertyList = this.merchantAccountService
                .findTimeZoneByProcessorId(CardProcessingConstants.PROCESSOR_ID_ELAVON_VIACONEX);
        if (customerPropertyList == null || customerPropertyList.isEmpty()) {
            return;
        }
        final Map<String, List<Integer>> customerMap = new HashMap<String, List<Integer>>();
        for (CustomerProperty customerProperty : customerPropertyList) {
            if (!customerMap.containsKey(customerProperty.getPropertyValue())) {
                customerMap.put(customerProperty.getPropertyValue(), new ArrayList<Integer>());
            }
            customerMap.get(customerProperty.getPropertyValue()).add(customerProperty.getCustomer().getId());
        }
        final StringBuilder timezoneQuery = new StringBuilder(" AND (");
        for (String timezone : customerMap.keySet()) {
            final int currentQuarterOfDay = DateUtil.getCurrentQuarterOfDayByTimezone(timezone);
            if (timezoneQuery.length() > LENGTH_OF_FIRST_PART) {
                timezoneQuery.append(" OR ");
            }
            timezoneQuery.append(StandardConstants.STRING_OPEN_PARENTHESIS);
            timezoneQuery.append("ma.CloseQuarterOfDay = ").append(currentQuarterOfDay).append(" AND ma.CustomerId IN (");
            final List<Integer> customerIdList = customerMap.get(timezone);
            for (int i = 0; i < customerIdList.size(); i++) {
                timezoneQuery.append(customerIdList.get(i));
                if (i < customerIdList.size() - 1) {
                    timezoneQuery.append(StandardConstants.STRING_COMMA);
                }
            }
            timezoneQuery.append(StandardConstants.STRING_CLOSE_PARENTHESIS);
            timezoneQuery.append(StandardConstants.STRING_CLOSE_PARENTHESIS);
        }
        timezoneQuery.append(StandardConstants.STRING_CLOSE_PARENTHESIS);
        final StringBuffer filterSQL = new StringBuffer(GET_JOIN_TYPE_LEFT).append(GET_BATCH_FOR_PROCESSING_JOIN_LASTBATCH_SQL)
                .append(GET_BATCH_FOR_PROCESSING_WHERE_CORE_SQL).append(GET_BATCH_FOR_PROCESSING_WHERE_JOIN_MERCHANT_NULL_SQL)
                .append(timezoneQuery.toString());
        collectBatch(result, noOfRetries, waitInterval, filterSQL.toString(), true);
    }
    
    private void collectLimitPassedBatches(final Set<ElavonBatchDTO> result, final int noOfRetries, final int waitInterval) {
        final StringBuffer filterSQL = new StringBuffer(GET_BATCH_FOR_PROCESSING_WHERE_CORE_SQL).append(" AND abs.NoOfTransactions >= ma.Field3");
        collectBatch(result, noOfRetries, waitInterval, filterSQL.toString(), false);
    }
    
    private void collectBatch(final Set<ElavonBatchDTO> result, final int noOfRetries, final int waitInterval, final String filterQuery,
        final boolean isEndOfDay) {
        final int expireBaselineInMilli = getExpireBatchBaselineEmsPropertiesValue();
        int totalExpireInMilli = 0;
        
        final List<ElavonBatchInfo> batchInfoList = (List<ElavonBatchInfo>) getElavonBatchInfo(filterQuery);
        for (ElavonBatchInfo batch : batchInfoList) {
            // get the current quarter of day 
            final ActiveBatchSummary abs = this.activeBatchSummaryService.findByBatchNoAndMerchantId(batch.getBatchNumber(),
                                                                                                     batch.getMerchantAccountId());
            /*
             * Get the Redis expire baseline from the database (e.g. 350 ms) times how many transactions for the total expire time.
             * e.g. Total transactions = 4500
             * Total Redis Expire time in milliseconds = 1575000 (26.25 mins)
             */
            totalExpireInMilli = expireBaselineInMilli * abs.getNoOfTransactions();
            
            final ElavonBatchDTO b = new ElavonBatchDTO(batch, abs);
            b.setTotalExpireInMilli(totalExpireInMilli);
            b.setWaitInterval(waitInterval);
            b.setNoOfRetries(noOfRetries);
            b.setEndOfDay(isEndOfDay);
            
            result.add(b);
        }
    }
    
    @Async
    @Override
    public final void callProcessBatchCloseAsync(final Integer merchantAccountId, final List<ElavonBatchDTO> batchList) {
        
        // doing select here because of hibernate session closing during lazy loading
        final MerchantAccount ma = this.merchantAccountService.findById(merchantAccountId);                
        final String tidmid = CardProcessingUtil.createRedisKey(ma);

        for (ElavonBatchDTO batch: batchList) {
            int batchCloseType = CardProcessingConstants.BATCH_SETTLEMENT_TYPE_TIME_OF_DAY;
            if (!batch.isEndOfDay()) {
                batchCloseType = CardProcessingConstants.BATCH_SETTLEMENT_TYPE_NO_OF_TRANZACTIONS;
            }
            
            for (int i = 0; i < batch.getNoOfRetries(); i++) {
                if (this.elavonViaConexService.getSemaphoreService().bookConnectionWithExpire(tidmid, batch.getTotalExpireInMilli())) {
                    final ActiveBatchSummary updatedActiveBatchSummary = this.self().updateImmediately(batch.getActiveBatchSummary().getId(), 
                                                                                                       CardProcessingConstants.BATCH_STATUS_TYPE_IN_PROCESS);
                    try {
                        this.self().processBatchClose(ma, updatedActiveBatchSummary, batchCloseType);
                    } catch (IOException | CardProcessorPausedException | NullPointerException e) {
                        if (updatedActiveBatchSummary.getBatchstatustype().getId() == CardProcessingConstants.BATCH_STATUS_TYPE_IN_PROCESS) {
                            this.self().updateImmediately(updatedActiveBatchSummary.getId(), CardProcessingConstants.BATCH_STATUS_TYPE_OPEN);
                        }
                    } finally {
                        this.elavonViaConexService.getSemaphoreService().releaseConnection(tidmid);
                        if (LOGGER.isDebugEnabled()) {
                            LOGGER.debug(createSemaphoreLogMessage(tidmid, "release semaphore"));
                        }
                        
                        try {
                            // take a little break to let any card processing get in
                            Thread.sleep(StandardConstants.SECONDS_IN_MILLIS_1);
                        } catch (InterruptedException ie) {
                            // Restore the interrupted status
                            Thread.currentThread().interrupt();
                        }                        
                    }
                    break;
                } else {
                    LOGGER.warn(createSemaphoreLogMessage(tidmid, "busy semaphore - transaction counter is more than 1 "));
                    try {
                        Thread.sleep(batch.getWaitInterval());
                    } catch (InterruptedException ie) {
                        // Restore the interrupted status
                        Thread.currentThread().interrupt();
                    }
                }
            }            
        }
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public final void processBatchClose(final MerchantAccount merchantAccount,
        final ActiveBatchSummary activeBatchSummary, final int batchCloseType) throws IOException, CardProcessorPausedException, NullPointerException {
        if (activeBatchSummary.getBatchstatustype().getId() != CardProcessingConstants.BATCH_STATUS_TYPE_IN_PROCESS) {
            LOGGER.warn("-=-=-=-= Current BatchStatusTypeId: " + activeBatchSummary.getBatchstatustype().getId() 
                        + " is not CardProcessingConstants.BATCH_STATUS_TYPE_IN_PROCESS (6), exit!");
            return;
        }
        String expMsg = null;
        try {
            // Not require to pass in PointOfSale for creating ElavonViaConexProcessor.
            final ElavonViaConexProcessor e = (ElavonViaConexProcessor) this.cardProcessorFactory.getCardProcessor(merchantAccount, null);
            final String result = e.processBatchClose(merchantAccount, activeBatchSummary, batchCloseType);
            updateActiveBatchSummaryStatus(activeBatchSummary, result);
        } catch (IOException ioe) {
            expMsg = createExceptionMessage(merchantAccount.getProcessor().getId(), merchantAccount.getName(), 1);
            LOGGER.error(expMsg, ioe);
            this.mailerService.sendAdminErrorAlert("ElavonViaConexCloseBatchService - IOException", expMsg, ioe);
            throw ioe;
        } catch (CardProcessorPausedException e1) {
            expMsg = createExceptionMessage(merchantAccount.getProcessor().getId(), merchantAccount.getName(), 0);
            LOGGER.error(expMsg, e1);
            this.mailerService.sendAdminErrorAlert("ElavonViaConexCloseBatchService - CardProcessorPausedException", expMsg, e1);
            throw e1;
        } catch (NullPointerException npe) {
            expMsg = createExceptionMessage(merchantAccount.getProcessor().getId(), merchantAccount.getName(), 2);
            LOGGER.error(expMsg, npe);
            this.mailerService.sendAdminErrorAlert("ElavonViaConexCloseBatchService - NullPointerException", expMsg, npe);
            throw npe;
        }
    }
    
    private void updateActiveBatchSummaryStatus(final ActiveBatchSummary activeBatchSummary, final String result) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("processBatchClose result: " + result);
        }
        switch (result) {
            case WebCoreConstants.EVENT_ACTION_EMPTY_STRING: 
            case WebCoreConstants.DUPLICATE_CLOSE_BATCH_TRANSACTION:
                // In case of an empty batch or a batch was opened manually, set status to close.
                this.self().updateImmediately(activeBatchSummary.getId(), CardProcessingConstants.BATCH_STATUS_TYPE_CLOSE);
                break;
            default:
                break;
        }
    }
    
    @SuppressWarnings("PMD.InsufficientStringBufferDeclaration")
    private String createExceptionMessage(final int processorId, final String merchantAccountName, final int exception) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("Processor id: ").append(processorId);
        if (exception == 0) {
            bdr.append(" is paused. ");
        } else if (exception == 1) {
            bdr.append(" has IOException. ");
        } else if (exception == 2) {
            bdr.append(" , unable to create ElavonViaConexProcessor.  ");
        }
        bdr.append("Merchant account name is: ").append(merchantAccountName);
        return bdr.toString();
    }
    
    @SuppressWarnings("PMD.InsufficientStringBufferDeclaration")
    private String createSemaphoreLogMessage(final String tidmid, final String operationType) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("TID_MID: ").append(tidmid).append(", operation type: ").append(operationType);
        return bdr.toString();
    }
    
    private int getExpireBatchBaselineEmsPropertiesValue() {
        return this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.ELAVON_VIACONEX_REDIS_SETTLE_EXPIRE_BASELINE,
                                                               EmsPropertiesService.DEFAULT_ELAVON_VIACONEX_REDIS_SETTLE_EXPIRE_MS_BASELINE);
    }
    
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public final ActiveBatchSummary updateImmediately(final Integer activeBatchSummaryId, final Integer batchStatusTypeId) {
        final ActiveBatchSummary activeBatchSummary = this.entityDao.get(ActiveBatchSummary.class, activeBatchSummaryId);
        this.entityDao.evict(activeBatchSummary);
        this.entityDao.evict(activeBatchSummary.getBatchstatustype());
        final Integer typeIdBefore = activeBatchSummary.getBatchstatustype().getId();
        
        activeBatchSummary.getBatchstatustype().setId(batchStatusTypeId);
        this.entityDao.getCurrentSession().update(activeBatchSummary);
        this.entityDao.getCurrentSession().flush();
        
        final ActiveBatchSummary abs = this.entityDao.get(ActiveBatchSummary.class, activeBatchSummary.getId());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("-=-=-=-= Updated ActiveBatchSummary id: " + activeBatchSummary.getId() + " batchStatusTypeId from: " 
                         + typeIdBefore + " to: " + batchStatusTypeId + ". Now batchStatusTypeId is: " + abs.getBatchstatustype().getId());
        }
        return abs;
    }
    
}
