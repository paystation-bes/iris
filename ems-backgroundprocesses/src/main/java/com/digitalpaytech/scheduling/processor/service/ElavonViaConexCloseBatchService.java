package com.digitalpaytech.scheduling.processor.service;

import java.io.IOException;
import java.util.List;

import com.digitalpaytech.domain.ActiveBatchSummary;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.exception.CardProcessorPausedException;
import com.digitalpaytech.scheduling.processor.dto.ElavonBatchDTO;

public interface ElavonViaConexCloseBatchService {
    
    void batchProcessingManager();
    
    void processBatchClose(MerchantAccount ma, ActiveBatchSummary abs, int batchCloseType) 
            throws IOException, CardProcessorPausedException, NullPointerException;
    
    void callProcessBatchCloseAsync(Integer merchantAccountId, List<ElavonBatchDTO> batchList);
    
    ActiveBatchSummary updateImmediately(Integer activeBatchSummaryId, Integer batchStatusTypeId);
}
