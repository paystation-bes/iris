package com.digitalpaytech.scheduling.processor.dto;

import java.io.Serializable;

import com.digitalpaytech.data.ElavonBatchInfo;
import com.digitalpaytech.domain.ActiveBatchSummary;

public final class ElavonBatchDTO implements Serializable {
    private static final long serialVersionUID = 7569862606564155529L;

    private ElavonBatchInfo batch;
    
    private ActiveBatchSummary activeBatchSummary;
    
    private boolean endOfDay;
    private int noOfRetries;
    private int waitInterval;
    private int totalExpireInMilli;
    
    public ElavonBatchDTO(final ElavonBatchInfo batch, final ActiveBatchSummary activeBatchSummary) {
        this.batch = batch;
        this.activeBatchSummary = activeBatchSummary;
    }

    @Override
    public boolean equals(final Object obj) {
        return (obj instanceof ElavonBatchDTO) 
                ?  this.batch.getActiveBatchSummaryId().equals(((ElavonBatchDTO) obj).getBatch().getActiveBatchSummaryId()) : false;
    }

    @Override
    public int hashCode() {
        return this.batch.getActiveBatchSummaryId().hashCode();
    }

    public ElavonBatchInfo getBatch() {
        return this.batch;
    }
    
    public ActiveBatchSummary getActiveBatchSummary() {
        return this.activeBatchSummary;
    }
    
    public boolean isEndOfDay() {
        return this.endOfDay;
    }

    public void setEndOfDay(final boolean endOfDay) {
        this.endOfDay = endOfDay;
    }

    public int getNoOfRetries() {
        return this.noOfRetries;
    }

    public void setNoOfRetries(final int noOfRetries) {
        this.noOfRetries = noOfRetries;
    }

    public int getWaitInterval() {
        return this.waitInterval;
    }

    public void setWaitInterval(final int waitInterval) {
        this.waitInterval = waitInterval;
    }

    public int getTotalExpireInMilli() {
        return this.totalExpireInMilli;
    }

    public void setTotalExpireInMilli(final int totalExpireInMilli) {
        this.totalExpireInMilli = totalExpireInMilli;
    }
}
