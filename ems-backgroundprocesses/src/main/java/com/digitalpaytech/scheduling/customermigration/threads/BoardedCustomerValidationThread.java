package com.digitalpaytech.scheduling.customermigration.threads;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.digitalpaytech.domain.ActivityLogin;
import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.domain.CustomerMigrationValidationStatus;
import com.digitalpaytech.service.ActivityLoginService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

//Catching exception is necessary because we want to log everything that can go wrong to be able to fix the issue 
@SuppressWarnings("checkstyle:illegalcatch")
public class BoardedCustomerValidationThread extends MigrationBaseThread {
    private static final Logger LOGGER = Logger.getLogger(BoardedCustomerValidationThread.class);
    private static Map<Integer, Boolean> isLogoutMap;
    
    private final ActivityLoginService activityLoginService;
    private final PointOfSaleService pointOfSaleService;
    
    static {
        isLogoutMap = new HashMap<Integer, Boolean>();
        isLogoutMap.put(WebSecurityConstants.LOGIN_RESULT_TYPE_LOGIN_SUCCESS, false);
        isLogoutMap.put(WebSecurityConstants.LOGIN_RESULT_TYPE_USER_LOGOUT, true);
        isLogoutMap.put(WebSecurityConstants.LOGIN_RESULT_TYPE_TIMEOUT_LOGOUT, true);
        isLogoutMap.put(WebSecurityConstants.LOGIN_RESULT_TYPE_USER_SWITCHED_IN, false);
        isLogoutMap.put(WebSecurityConstants.LOGIN_RESULT_TYPE_USER_SWITCHED_OUT, true);
    }
    
    public BoardedCustomerValidationThread(final CustomerMigration customerMigration, final ApplicationContext applicationContext) {
        super(customerMigration, applicationContext);
        
        this.activityLoginService = (ActivityLoginService) applicationContext.getBean("activityLoginService");
        this.pointOfSaleService = (PointOfSaleService) applicationContext.getBean("pointOfSaleService");
        
    }
    
    @Override
    public final void run() {
        try {
            LOGGER.info(new StringBuilder("Status update started for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                    .append(super.customer.getName()));
            super.customerMigration.setIsMigrationInProgress(true);
            super.customerMigrationService.updateCustomerMigration(super.customerMigration);
            
            LOGGER.info(new StringBuilder("Setting UsageMinutes for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                    .append(super.customer.getName()));
            
            super.customerMigration.setTotalUserLoggedMinutes(totalUsageInMinutes());
            
            LOGGER.info(new StringBuilder("Running validations for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                    .append(super.customer.getName()));
            
            super.customerMigration.setCustomerMigrationValidationStatusType(super.customerMigrationValidationStatusService
                    .runValidations(super.customer.getId(), true, false, false));
            
            if (WebCoreConstants.CUSTOMER_MIGRATION_VALIDATION_STATUS_TYPE_BLOCKED == super.customerMigration
                    .getCustomerMigrationValidationStatusType().getId()) {
                sendFailureEmails();
            }
            
            LOGGER.info(new StringBuilder("Updating total pay station count for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            
            super.customerMigration.setTotalPaystationCount(this.pointOfSaleService.findPointOfSalesByCustomerId(super.customer.getId()).size());
            
            final int timeDelayInMinutes = this.emsPropertiesService
                    .getPropertyValueAsInt(EmsPropertiesService.CUSTOMER_MIGRATION_CUSTOMER_VALIDATION_DELAY_IN_MINUTES,
                                           EmsPropertiesService.CUSTOMER_MIGRATION_CUSTOMER_VALIDATION_DELAY_IN_MINUTES_DEFAULT, true);
            
            super.customerMigration.setBackGroundJobNextTryGmt(addDelay(Calendar.MINUTE, timeDelayInMinutes));
            super.customerMigration.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
            super.customerMigration.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
            super.customerMigration.setIsMigrationInProgress(false);
            super.customerMigrationService.updateCustomerMigration(super.customerMigration);
            
            LOGGER.info(new StringBuilder("Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                    .append(super.customer.getName()).append(" logged minutes updated."));
            
        } catch (Exception e) {
            LOGGER.error(EXCEPTION_THROWN, e);
            final int timeDelayInMinutes = this.emsPropertiesService
                    .getPropertyValueAsInt(EmsPropertiesService.CUSTOMER_MIGRATION_CUSTOMER_VALIDATION_DELAY_IN_MINUTES,
                                           EmsPropertiesService.CUSTOMER_MIGRATION_CUSTOMER_VALIDATION_DELAY_IN_MINUTES_DEFAULT, true);
            
            super.customerMigration.setBackGroundJobNextTryGmt(addDelay(Calendar.MINUTE, timeDelayInMinutes));
            
            super.customerMigration.setIsMigrationInProgress(false);
            super.customerMigrationService.updateCustomerMigration(super.customerMigration);
            
            final String subject = super.reportingUtil.getPropertyEN("customermigration.email.admin.migrationvalidationcancelled.subject",
                                                                     new String[] { super.customer.getId().toString(), super.customer.getName(), });
            final String content = super.reportingUtil.getPropertyEN("customermigration.email.admin.migrationvalidationcancelled.content",
                                                                     new String[] { super.customer.getId().toString(), super.customer.getName(),
                                                                         String.valueOf(timeDelayInMinutes), });
            LOGGER.error(content);
            super.mailerService.sendMessage(super.itValidationEmailAddress, subject, content, null);
            
        }
    }
    
    @SuppressWarnings("PMD.NullAssignment")
    private int totalUsageInMinutes() {
        final List<ActivityLogin> activityLoginList = this.activityLoginService.findAllActivityForUsageCalculation(super.customer.getId());
        long totalUsage = 0L;
        boolean logoutFound = false;
        int latestUserAccountId = 0;
        Date latestLogout = null;
        Date latestLogin = null;
        for (ActivityLogin activityLogin : activityLoginList) {
            if (latestUserAccountId != activityLogin.getUserAccount().getId().intValue()) {
                latestUserAccountId = activityLogin.getUserAccount().getId().intValue();
                if (logoutFound && latestLogin != null) {
                    totalUsage += latestLogout.getTime() - latestLogin.getTime();
                    latestLogin = null;
                    latestLogout = activityLogin.getActivityGmt();
                }
                latestLogout = null;
                latestLogin = null;
                logoutFound = false;
            }
            if (isLogoutMap.get(activityLogin.getLoginResultType().getId())) {
                if (logoutFound && latestLogin != null) {
                    totalUsage += latestLogout.getTime() - latestLogin.getTime();
                    latestLogin = null;
                    latestLogout = activityLogin.getActivityGmt();
                } else {
                    latestLogout = activityLogin.getActivityGmt();
                    logoutFound = true;
                }
            } else {
                latestLogin = activityLogin.getActivityGmt();
            }
        }
        if (logoutFound && latestLogin != null) {
            totalUsage += latestLogout.getTime() - latestLogin.getTime();
        }
        return (int) (totalUsage / StandardConstants.MINUTES_IN_MILLIS_1);
    }
    
    public final ActivityLoginService getActivityLoginService() {
        return this.activityLoginService;
    }
    
    public final PointOfSaleService getPointOfSaleService() {
        return this.pointOfSaleService;
    }
    
    private void sendFailureEmails() {
        final List<CustomerMigrationValidationStatus> validationList = super.customerMigrationValidationStatusService.findSystemWideValidations();
        boolean isSystemWideFailed = false;
        final String subject = super.reportingUtil.getPropertyEN("customermigration.email.admin.validationfailed.subject");
        final StringBuilder content = new StringBuilder(super.reportingUtil.getPropertyEN("customermigration.email.admin.validationfailed.content"));
        
        for (CustomerMigrationValidationStatus validation : validationList) {
            if (validation.getCustomerMigrationValidationType().isIsBlocking() && validation.getCustomerMigrationValidationType().isIsITEmail()
                && !validation.isIsPassed()) {
                content.append(super.reportingUtil.getPropertyEN(validation.getCustomerMigrationValidationType().getName()))
                        .append(StandardConstants.STRING_NEXT_LINE);
                isSystemWideFailed = true;
            }
        }
        
        if (isSystemWideFailed) {
            LOGGER.error(content.toString());
            super.mailerService.sendMessage(super.itValidationEmailAddress, subject, content.toString(), null);
        }
    }
    
}
