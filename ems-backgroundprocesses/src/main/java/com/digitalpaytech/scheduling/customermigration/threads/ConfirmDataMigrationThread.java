package com.digitalpaytech.scheduling.customermigration.threads;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.domain.CustomerMigrationValidationStatus;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.LabelConstants;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

//Catching exception is necessary because we want to log everything that can go wrong to be able to fix the issue 
@SuppressWarnings("checkstyle:illegalcatch")
public class ConfirmDataMigrationThread extends MigrationBaseThread {
    private static final Logger LOGGER = Logger.getLogger(ConfirmDataMigrationThread.class);
    
    private static final int CURRENT_STEP = 3;
    
    public ConfirmDataMigrationThread(final CustomerMigration customerMigration, final ApplicationContext applicationContext) {
        super(customerMigration, applicationContext);
        
    }
    
    @Override
    public final void run() {
        try {
            LOGGER.info(new StringBuilder("Step ").append(CURRENT_STEP).append(" started for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            super.customerMigration.setIsMigrationInProgress(true);
            super.customerMigrationService.updateCustomerMigration(super.customerMigration);
            
            LOGGER.info(new StringBuilder("Validating customer data for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                    .append(super.customer.getName()));
            
            super.customerMigration.setCustomerMigrationValidationStatusType(super.customerMigrationValidationStatusService
                    .runValidations(super.customer.getId(), true, true, false));
            
            if (WebCoreConstants.CUSTOMER_MIGRATION_VALIDATION_STATUS_TYPE_BLOCKED == super.customerMigration
                    .getCustomerMigrationValidationStatusType().getId()) {
                
                LOGGER.info(new StringBuilder("Data migration not finished for Customer: ").append(super.customer.getId())
                        .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
                
                final int timeDelayInMinutes = this.emsPropertiesService
                        .getPropertyValueAsInt(EmsPropertiesService.DATA_VALIDATION_WAIT_TIME_IN_MINUTES,
                                               EmsPropertiesService.DATA_VALIDATION_WAIT_TIME_IN_MINUTES_DEFAULT, true);
                
                if (checkValidationExpiry(super.customerMigration.getEms6CustomerLockedGmt(), timeDelayInMinutes
                                                                                              * StandardConstants.MINUTES_IN_MILLIS_1 * 2)) {
                    super.customerMigration.setBackGroundJobNextTryGmt(addDelay(Calendar.MINUTE, 1));
                    if (WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_WAITING_DATA_MIGRATION != super.customerMigration
                            .getCustomerMigrationStatusType().getId()) {
                        super.customerMigration.setCustomerMigrationStatusType(super.customerMigrationStatusTypeService
                                .findCustomerMigrationStatusType(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_WAITING_DATA_MIGRATION));
                    }
                }
                super.customerMigration.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                super.customerMigration.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
                super.customerMigration.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                super.customerMigration.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
                super.customerMigration.setIsMigrationInProgress(false);
                super.customerMigrationService.updateCustomerMigration(super.customerMigration);
                
                return;
            } else {
                super.customerMigration.setCustomerMigrationStatusType(super.customerMigrationStatusTypeService
                        .findCustomerMigrationStatusType(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_DATA_MIGRATION_CONFIRMED));
                super.customerMigration.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                super.customerMigration.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
                super.customerMigration.setIsMigrationInProgress(false);
                super.customerMigration.setDataMigrationConfirmedGmt(DateUtil.getCurrentGmtDate());
                super.customerMigrationService.updateCustomerMigration(super.customerMigration);
                
                LOGGER.info(new StringBuilder("Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                        .append(super.customer.getName()).append(" data validation complete."));
            }
        } catch (Exception e) {
            LOGGER.error(EXCEPTION_THROWN, e);
            super.customerMigration.setCustomerMigrationStatusType(super.customerMigrationStatusTypeService
                    .findCustomerMigrationStatusType(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_CANCELLED));
            super.customerMigration.setIsMigrationInProgress(false);
            if (super.customerMigration.getCustomerMigrationFailureType() == null) {
                super.customerMigration.setCustomerMigrationFailureType(super.customerMigrationFailureTypeService
                        .findCustomerMigrationFailureType(WebCoreConstants.CUSTOMER_MIGRATION_FAILURE_TYPE_UNKNOWN));
            }
            super.customerMigration.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
            super.customerMigration.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
            super.customerMigrationService.updateCustomerMigration(super.customerMigration);
            
            final String subject = super.reportingUtil.getPropertyEN(LabelConstants.CM_EMAIL_ADMIN_MIGRATIONCANCEL_SUBJECT, new String[] {
                super.customer.getId().toString(), super.customer.getName(), });
            final String content = super.reportingUtil.getPropertyEN(LabelConstants.CM_EMAIL_ADMIN_MIGRATIONCANCEL_DV_CONTENT, new String[] {
                super.customer.getId().toString(), super.customer.getName(), });
            LOGGER.error(content);
            
            super.mailerService.sendMessage(super.itEmailAddress, subject, content, null);
        }
        
    }
    
    private boolean checkValidationExpiry(final Date fromDate, final long waitLimit) {
        
        final Date now = DateUtil.getCurrentGmtDate();
        final long timeDifference = now.getTime() - fromDate.getTime();
        
        // validation took twice as long as 
        if (timeDifference > waitLimit) {
            super.customerMigration.setCustomerMigrationStatusType(super.customerMigrationStatusTypeService
                    .findCustomerMigrationStatusType(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_CANCELLED));
            super.customerMigration.setIsMigrationInProgress(false);
            super.customerMigration.setCustomerMigrationFailureType(super.customerMigrationFailureTypeService
                    .findCustomerMigrationFailureType(WebCoreConstants.CUSTOMER_MIGRATION_FAILURE_TYPE_IN_MIGRATION_VALIDATIONS));
            
            final String subject = super.reportingUtil.getPropertyEN(LabelConstants.CM_EMAIL_ADMIN_MIGRATIONCANCEL_SUBJECT, new String[] {
                super.customer.getId().toString(), super.customer.getName(), });
            final StringBuilder content = new StringBuilder(
                    super.reportingUtil.getPropertyEN(LabelConstants.CM_EMAIL_ADMIN_MIGRATIONCANCEL_DV_CONTENT, new String[] {
                        super.customer.getId().toString(), super.customer.getName(), }));
            
            final List<CustomerMigrationValidationStatus> validationList = super.customerMigrationValidationStatusService.findSystemWideValidations();
            validationList.addAll(super.customerMigrationValidationStatusService.findInMigrationValidationsByCustomerId(customer.getId(), true));
            for (CustomerMigrationValidationStatus validation : validationList) {
                if (validation.getCustomerMigrationValidationType().isIsBlocking()
                    && validation.getCustomerMigrationValidationType().isIsDuringMigration() && !validation.isIsPassed()) {
                    content.append(validation.getCustomerMigrationValidationType().getName()).append(StandardConstants.STRING_NEXT_LINE);
                }
            }
            
            LOGGER.error(content.toString());
            
            super.mailerService.sendMessage(super.itEmailAddress, subject, content.toString(), null);
            return false;
        }
        return true;
    }
}
