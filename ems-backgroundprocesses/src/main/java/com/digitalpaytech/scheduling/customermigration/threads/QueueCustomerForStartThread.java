package com.digitalpaytech.scheduling.customermigration.threads;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.domain.CustomerMigrationValidationStatus;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

//Catching exception is necessary because we want to log everything that can go wrong to be able to fix the issue 
@SuppressWarnings("checkstyle:illegalcatch")
public class QueueCustomerForStartThread extends MigrationBaseThread {
    private static final Logger LOGGER = Logger.getLogger(QueueCustomerForStartThread.class);
    
    private static final int CURRENT_STEP = 1;
    
    public QueueCustomerForStartThread(final CustomerMigration customerMigration, final ApplicationContext applicationContext) {
        super(customerMigration, applicationContext);
    }
    
    @Override
    public final void run() {
        try {
            LOGGER.info(new StringBuilder("Step ").append(CURRENT_STEP).append(" started for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            super.customerMigration.setIsMigrationInProgress(true);
            super.customerMigration.setMigrationStartGmt(DateUtil.getCurrentGmtDate());
            super.customerMigration.setMigrationCancelGmt(null);
            super.customerMigration.setStartEmailSentGmt(null);
            super.customerMigration.setEms6CustomerLockedGmt(null);
            super.customerMigration.setDataMigrationConfirmedGmt(null);
            super.customerMigration.setEms7CustomerEnabledGmt(null);
            super.customerMigration.setHeartbeatsResetGmt(null);
            super.customerMigration.setCustomerMigrationFailureType(null);
            super.customerMigrationService.updateCustomerMigration(super.customerMigration);
            
            LOGGER.info(new StringBuilder("Running pre migration validations for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
                    
            super.customerMigration.setCustomerMigrationValidationStatusType(super.customerMigrationValidationStatusService
                    .runValidations(super.customer.getId(), true, false, true));
                    
            if (WebCoreConstants.CUSTOMER_MIGRATION_VALIDATION_STATUS_TYPE_BLOCKED == super.customerMigration
                    .getCustomerMigrationValidationStatusType().getId()) {
                    
                LOGGER.info(new StringBuilder("Pre-migration validations failed for Customer: ").append(super.customer.getId())
                        .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
                        
                super.customerMigration.setCustomerMigrationStatusType(super.customerMigrationStatusTypeService
                        .findCustomerMigrationStatusType(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SUSPENDED));
                super.customerMigrationService.updateCustomerMigration(super.customerMigration);
                
                LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                        .append(super.customer.getName()).append(" migration suspended."));
                        
                sendFailureEmails();
                
                LOGGER.info(new StringBuilder("Emails sent after migration start failed to Customer: ").append(super.customer.getId())
                        .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
                        
            } else {
                
                LOGGER.info(new StringBuilder("All validations passed for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                        .append(super.customer.getName()));
                        
                super.customerMigration.setCustomerMigrationStatusType(super.customerMigrationStatusTypeService
                        .findCustomerMigrationStatusType(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_QUEUED));
                super.customerMigrationService.updateCustomerMigration(super.customerMigration);
                
                LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                        .append(super.customer.getName()).append(" ready for migration."));
                        
                sendEmails();
                
                LOGGER.info(new StringBuilder("Emails sent at migration start to Customer: ").append(super.customer.getId())
                        .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            }
            
            super.customerMigration.setIsMigrationInProgress(false);
            super.customerMigrationService.updateCustomerMigration(super.customerMigration);
            
        } catch (Exception e) {
            LOGGER.error(EXCEPTION_THROWN, e);
            super.customerMigration.setCustomerMigrationStatusType(super.customerMigrationStatusTypeService
                    .findCustomerMigrationStatusType(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_CANCELLED));
            super.customerMigration.setIsMigrationInProgress(false);
            if (super.customerMigration.getCustomerMigrationFailureType() == null) {
                super.customerMigration.setCustomerMigrationFailureType(super.customerMigrationFailureTypeService
                        .findCustomerMigrationFailureType(WebCoreConstants.CUSTOMER_MIGRATION_FAILURE_TYPE_UNKNOWN));
            }
            super.customerMigrationService.updateCustomerMigration(super.customerMigration);
            
            final String subject = super.reportingUtil.getPropertyEN("customermigration.email.admin.migrationcancelled.subject",
                                                                     new String[] { super.customer.getId().toString(), super.customer.getName(), });
            final String content = super.reportingUtil.getPropertyEN("customermigration.email.admin.migrationcancelled.ems6customerlock.content",
                                                                     new String[] { super.customer.getId().toString(), super.customer.getName(), });
            LOGGER.error(content);
            super.mailerService.sendMessage(super.itEmailAddress, subject, content, null);
            
        }
    }
    
    private void sendEmails() {
        super.mailerService.sendMessage(this.customerEmailAddress,
                                        this.reportingUtil.getPropertyEN("customermigration.email.customer.migrationstarted.subject",
                                                                         new String[] { super.customer.getName() }),
                                        this.reportingUtil.getPropertyEN("customermigration.email.customer.migrationstarted.content"), null);
                                        
        final String itEmailAddress = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.CUSTOMER_MIGRATION_ALERT_EMAIL,
                                                                                 EmsPropertiesService.CUSTOMER_MIGRATION_ALERT_EMAIL_DEFAULT, true);
        final String subject = this.reportingUtil.getPropertyEN("customermigration.email.admin.migrationstarted.subject", new String[] {
            super.customerMigration.getCustomer().getId().toString(), super.customerMigration.getCustomer().getName(), });
        final String content = this.reportingUtil.getPropertyEN("customermigration.email.admin.migrationstarted.content", new String[] {
            super.customerMigration.getCustomer().getId().toString(), super.customerMigration.getCustomer().getName(), });
            
        super.mailerService.sendMessage(itEmailAddress, subject, content, null);
        
        super.customerMigration.setStartEmailSentGmt(DateUtil.getCurrentGmtDate());
    }
    
    private void sendFailureEmails() {
        
        final String itEmailAddress = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.CUSTOMER_MIGRATION_ALERT_EMAIL,
                                                                                 EmsPropertiesService.CUSTOMER_MIGRATION_ALERT_EMAIL_DEFAULT, true);
        final String subject = this.reportingUtil.getPropertyEN("customermigration.email.admin.migrationnotstarted.subject", new String[] {
            super.customerMigration.getCustomer().getId().toString(), super.customerMigration.getCustomer().getName(), });
        final StringBuilder content = new StringBuilder(this.reportingUtil.getPropertyEN("customermigration.email.admin.migrationnotstarted.content",
                                                                                         new String[] {
                                                                                             super.customerMigration.getCustomer().getId().toString(),
                                                                                             super.customerMigration.getCustomer().getName(), }));
                                                                                             
        final List<CustomerMigrationValidationStatus> validationList = super.customerMigrationValidationStatusService.findSystemWideValidations();
        validationList.addAll(super.customerMigrationValidationStatusService.findInMigrationValidationsByCustomerId(super.customer.getId(), false));
        for (CustomerMigrationValidationStatus validation : validationList) {
            if (validation.getCustomerMigrationValidationType().isIsBlocking() && !validation.isIsPassed()) {
                content.append(validation.getCustomerMigrationValidationType().getName()).append(StandardConstants.STRING_NEXT_LINE);
            }
        }
        super.mailerService.sendMessage(itEmailAddress, subject, content.toString(), null);
        
        super.customerMigration.setCustomerMigrationFailureType(super.customerMigrationFailureTypeService
                .findCustomerMigrationFailureType(WebCoreConstants.CUSTOMER_MIGRATION_FAILURE_TYPE_PRE_MIGRATION_VALIDATIONS));
    }
    
}
