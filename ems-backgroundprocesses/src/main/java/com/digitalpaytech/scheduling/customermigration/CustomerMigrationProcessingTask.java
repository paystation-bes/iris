package com.digitalpaytech.scheduling.customermigration;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.scheduling.customermigration.threads.BoardedCustomerValidationThread;
import com.digitalpaytech.scheduling.customermigration.threads.CheckNotBoardedCustomersThread;
import com.digitalpaytech.scheduling.customermigration.threads.CommunicationValidationThread;
import com.digitalpaytech.scheduling.customermigration.threads.ConfirmDataMigrationThread;
import com.digitalpaytech.scheduling.customermigration.threads.DisableEMS6CustomerThread;
import com.digitalpaytech.scheduling.customermigration.threads.EnableIRISCustomerThread;
import com.digitalpaytech.scheduling.customermigration.threads.MigrationBaseThread;
import com.digitalpaytech.scheduling.customermigration.threads.QueueCustomerForStartThread;
import com.digitalpaytech.scheduling.customermigration.threads.RollbackEMS6CustomerThread;
import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.CustomerMigrationService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.ReportingUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

/**
 * This class checks current external & internal key and processes generating next key.
 * checkAndProcessCryptoKey() is scheduled to check and process this every day. (8:00 GMT)
 * 
 * @author Brian Kim
 * 
 */
@Component("customerMigrationProcessingTask")
@SuppressWarnings("checkstyle:illegalcatch")
public class CustomerMigrationProcessingTask implements ApplicationContextAware {
    private static final Logger LOGGER = Logger.getLogger(CustomerMigrationProcessingTask.class);
    private static final String DEFAULT_PROCESS_SERVER = "EMSMain";
    
    private ApplicationContext applicationContext;
    
    private TaskExecutor migrationTaskExecutor;
    
    @Autowired
    private CustomerMigrationService customerMigrationService;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private ClusterMemberService clusterMemberService;
    
    @Autowired
    private ReportingUtil reportingUtil;
    
    public final void setMigrationTaskExecutor(final TaskExecutor migrationTaskExecutor) {
        this.migrationTaskExecutor = migrationTaskExecutor;
    }
    
    public final void setCustomerMigrationService(final CustomerMigrationService customerMigrationService) {
        this.customerMigrationService = customerMigrationService;
    }
    
    public final void setMailerService(final MailerService mailerService) {
        this.mailerService = mailerService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final void setReportingUtil(final ReportingUtil reportingUtil) {
        this.reportingUtil = reportingUtil;
    }
    
    public final void setClusterMemberService(final ClusterMemberService clusterMemberService) {
        this.clusterMemberService = clusterMemberService;
    }
    
    public final void processCustomerMigration() {
        if (!isCustomerMigrationServer(false)) {
            // not the configured server
            return;
        }
        
        final int maxActiveThreads = ((ThreadPoolTaskExecutor) this.migrationTaskExecutor).getMaxPoolSize();
        if (this.customerMigrationService.isMigrationInProgress()) {
            int i = 0;
            // The procedure is called once every minute. Each time it iterates 30 times with 1 second wait per iteration. 
            while (i < StandardConstants.LIMIT_30 && ((ThreadPoolTaskExecutor) this.migrationTaskExecutor).getActiveCount() != maxActiveThreads) {
                i++;
                final CustomerMigration customerMigration = this.customerMigrationService.findNextAvailableCustomer();
                if (customerMigration != null) {
                    try {
                        final MigrationBaseThread thread = getMigrationThread(customerMigration);
                        
                        this.migrationTaskExecutor.execute(thread);
                    } catch (Exception e) {
                        LOGGER.error(new StringBuilder("Exception trying to start migration thread for Customer: ")
                                .append(customerMigration.getCustomer().getId()).append(" - ").append(customerMigration.getCustomer().getName()));
                        LOGGER.error(new StringBuilder("Ignored exception: ").append(e.getClass().getName()).append(": ").append(e.getMessage()));
                    }
                }
                try {
                    Thread.sleep(StandardConstants.SECONDS_IN_MILLIS_1);
                } catch (InterruptedException ie) {
                    LOGGER.debug("CustomerMigration 1 second sleep interrupted.");
                }
            }
        } else {
            LOGGER.error("Migration already finished.");
            
        }
    }
    
    private MigrationBaseThread getMigrationThread(final CustomerMigration customerMigration) {
        switch (customerMigration.getCustomerMigrationStatusType().getId()) {
            case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_REQUESTED:
            case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_BOARDED:
            case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SUSPENDED:
                return new BoardedCustomerValidationThread(customerMigration, this.applicationContext);
            case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SCHEDULED:
                if (customerMigration.getMigrationScheduledGmt().before(DateUtil.getCurrentGmtDate())) {
                    return new QueueCustomerForStartThread(customerMigration, this.applicationContext);
                } else {
                    return new BoardedCustomerValidationThread(customerMigration, this.applicationContext);
                }
            case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_QUEUED:
                return new DisableEMS6CustomerThread(customerMigration, this.applicationContext);
            case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_CANCELLED:
                return new RollbackEMS6CustomerThread(customerMigration, this.applicationContext);
            case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_EMS6_DISABLED:
            case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_WAITING_DATA_MIGRATION:
                return new ConfirmDataMigrationThread(customerMigration, this.applicationContext);
            case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_DATA_MIGRATION_CONFIRMED:
                return new EnableIRISCustomerThread(customerMigration, this.applicationContext);
            case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMPLETED:
            case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMMUNICATION_CHECKED:
                return new CommunicationValidationThread(customerMigration, this.applicationContext);
            case WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_NOT_BOARDED:
                return new CheckNotBoardedCustomersThread(customerMigration, this.applicationContext);
            default:
                break;
        }
        return null;
    }
    
    private boolean isCustomerMigrationServer(final boolean checkRunMigration) {
        
        final boolean runMigration = this.emsPropertiesService.getPropertyValueAsBoolean(EmsPropertiesService.RUN_DAILY_CUSTOMER_MIGRATION,
                                                                                         EmsPropertiesService.RUN_DAILY_CUSTOMER_MIGRATION_DEFAULT,
                                                                                         true);
        if (checkRunMigration && !runMigration) {
            return false;
        }
        
        try {
            // get the cluster member name
            final String clusterName = this.clusterMemberService.getClusterName();
            if (clusterName == null || clusterName.isEmpty()) {
                throw new ApplicationException("Cluster member name is empty");
            }
            
            // get the configured auto crypto key processing server from
            // database
            final String processServer = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.CUSTOMER_MIGRATION_SERVER,
                                                                                    DEFAULT_PROCESS_SERVER, true);
            
            if (clusterName.equals(processServer)) {
                LOGGER.info(clusterName + " configured for customer migration tasks.");
                return true;
            } else {
                LOGGER.info(clusterName + " not configured for customer migration tasks.");
                return false;
            }
        } catch (ApplicationException e) {
            
            final String itEmailAddress = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.CUSTOMER_MIGRATION_ALERT_EMAIL,
                                                                                     EmsPropertiesService.CUSTOMER_MIGRATION_ALERT_EMAIL_DEFAULT,
                                                                                     true);
            
            String addr = null;
            try {
                addr = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e1) {
                LOGGER.error("Unknown host exception happened in CustomerMigrationServer check");
            }
            final String subject = this.reportingUtil.getPropertyEN("customermigration.email.admin.processmigration.subject");
            final String err = new StringBuilder("Unable to read member name from cluster.properties for cluster member ").append(addr).toString();
            LOGGER.error(err);
            
            this.mailerService.sendMessage(itEmailAddress, subject, err, null);
            
            return false;
        }
    }
    
    @Override
    public final void setApplicationContext(final ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
        
    }
    
}
