package com.digitalpaytech.scheduling.customermigration.threads;

import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.digitalpaytech.domain.ActivityLogin;
import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.service.ActivityLoginService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

//Catching exception is necessary because we want to log everything that can go wrong to be able to fix the issue 
@SuppressWarnings("checkstyle:illegalcatch")
public class CheckNotBoardedCustomersThread extends MigrationBaseThread {
    private static final Logger LOGGER = Logger.getLogger(CheckNotBoardedCustomersThread.class);
    
    private final transient ActivityLoginService activityLoginService;
    
    public CheckNotBoardedCustomersThread(final CustomerMigration customerMigration, final ApplicationContext applicationContext) {
        super(customerMigration, applicationContext);
        
        this.activityLoginService = (ActivityLoginService) applicationContext.getBean("activityLoginService");
        
    }
    
    @Override
    public final void run() {
        try {
            LOGGER.info(new StringBuilder("Check initial status for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                    .append(super.customer.getName()));
            super.customerMigration.setIsMigrationInProgress(true);
            super.customerMigrationService.updateCustomerMigration(super.customerMigration);
            
            LOGGER.info(new StringBuilder("Checking ActivityLogin table for existing logins for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            
            checkCustomersStatus();
            
            super.customerMigration.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
            super.customerMigration.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
            super.customerMigration.setIsMigrationInProgress(false);
            super.customerMigrationService.updateCustomerMigration(super.customerMigration);
            
            LOGGER.info(new StringBuilder("Status updated for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                    .append(super.customer.getName()));
            
        } catch (Exception e) {
            LOGGER.error(EXCEPTION_THROWN, e);
            final int timeDelayInMinutes = this.emsPropertiesService
                    .getPropertyValueAsInt(EmsPropertiesService.CUSTOMER_MIGRATION_CUSTOMER_VALIDATION_DELAY_IN_MINUTES,
                                           EmsPropertiesService.CUSTOMER_MIGRATION_CUSTOMER_VALIDATION_DELAY_IN_MINUTES_DEFAULT, true);
            
            super.customerMigration.setBackGroundJobNextTryGmt(addDelay(Calendar.MINUTE, timeDelayInMinutes));
            
            super.customerMigration.setIsMigrationInProgress(false);
            super.customerMigrationService.updateCustomerMigration(super.customerMigration);
            
            final String subject = super.reportingUtil.getPropertyEN("customermigration.email.admin.initialstatuscancelled.subject", new String[] {
                super.customer.getId().toString(), super.customer.getName(), });
            final String content = super.reportingUtil.getPropertyEN("customermigration.email.admin.initialstatuscancelled.content", new String[] {
                super.customer.getId().toString(), super.customer.getName(), String.valueOf(timeDelayInMinutes), });
            LOGGER.error(content);
            super.mailerService.sendMessage(super.itEmailAddress, subject, content, null);
            
        }
    }
    
    private void checkCustomersStatus() {
        final List<ActivityLogin> activityLoginList = this.activityLoginService.findAllActivityForUsageCalculation(super.customer.getId());
        super.customerMigration.setTotalPaystationCount(this.pointOfSaleService.findPointOfSalesByCustomerId(super.customer.getId()).size());
        if (activityLoginList == null || activityLoginList.isEmpty()) {
            super.customerMigration.setBackGroundJobNextTryGmt(null);
            super.customerMigration.setCustomerMigrationStatusType(super.customerMigrationStatusTypeService
                    .findCustomerMigrationStatusType(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_NOT_BOARDED));
        } else {
            super.customerMigration.setBackGroundJobNextTryGmt(DateUtil.getCurrentGmtDate());
            super.customerMigration.setCustomerMigrationStatusType(super.customerMigrationStatusTypeService
                    .findCustomerMigrationStatusType(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_BOARDED));
            super.customerMigration.setCustomerBoardedGmt(activityLoginList.get(0).getActivityGmt());
        }
    }
    
}
