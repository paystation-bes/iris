package com.digitalpaytech.scheduling.customermigration.threads;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.domain.CustomerMigrationEMS6ModifiedRecord;
import com.digitalpaytech.domain.CustomerMigrationOnlinePOS;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

// Catching exception is necessary because we want to log everything that can go wrong to be able to fix the issue  
@SuppressWarnings("checkstyle:illegalcatch")
public class RollbackEMS6CustomerThread extends MigrationBaseThread {
    private static final Logger LOGGER = Logger.getLogger(RollbackEMS6CustomerThread.class);
    
    public RollbackEMS6CustomerThread(final CustomerMigration customerMigration, final ApplicationContext applicationContext) {
        super(customerMigration, applicationContext);
        
    }
    
    @Override
    public final void run() {
        LOGGER.info(new StringBuilder("Cancellation Step started for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(super.customer.getName()));
        super.customerMigration.setIsMigrationInProgress(true);
        super.customerMigrationService.updateCustomerMigration(super.customerMigration);
        
        rollbackCustomer();
        
        super.customerMigration.setCustomerMigrationStatusType(super.customerMigrationStatusTypeService
                .findCustomerMigrationStatusType(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SUSPENDED));
        super.customerMigration.setIsMigrationInProgress(false);
        super.customerMigration.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
        super.customerMigration.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        super.customerMigration.setMigrationCancelGmt(DateUtil.getCurrentGmtDate());
        super.customerMigration.setFailureCount((byte) (super.customerMigration.getFailureCount() + 1));
        super.customerMigrationService.updateCustomerMigration(super.customerMigration);
        
        LOGGER.info(new StringBuilder("Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES).append(super.customer.getName())
                .append(" migration cancelletion complete."));
                
        String subject = null;
        String content = null;
        switch (super.customerMigration.getCustomerMigrationFailureType().getId()) {
            case WebCoreConstants.CUSTOMER_MIGRATION_FAILURE_TYPE_UNKNOWN:
                subject = super.reportingUtil.getPropertyEN("customermigration.email.customer.migrationcancelled.subject",
                                                            new String[] { super.customer.getName() });
                content = super.reportingUtil.getPropertyEN("customermigration.email.customer.migrationcancelled.content");
                break;
            case WebCoreConstants.CUSTOMER_MIGRATION_FAILURE_TYPE_UNPROCESSED_REVERSAL:
                subject = super.reportingUtil.getPropertyEN("customermigration.email.customer.migrationcancelled.reversal.subject",
                                                            new String[] { super.customer.getName() });
                content = super.reportingUtil.getPropertyEN("customermigration.email.customer.migrationcancelled.reversal.content");
                break;
            case WebCoreConstants.CUSTOMER_MIGRATION_FAILURE_TYPE_UNPROCESSED_CARD:
                subject = super.reportingUtil.getPropertyEN("customermigration.email.customer.migrationcancelled.cardtransaction.subject",
                                                            new String[] { super.customer.getName() });
                content = super.reportingUtil.getPropertyEN("customermigration.email.customer.migrationcancelled.cardtransaction.content");
                break;
            default:
                break;
        }
        this.mailerService.sendMessage(super.customerEmailAddress, subject, content, null);
        
        super.printCustomerReport();
        
    }
    
    private void rollbackCustomer() {
        
        LOGGER.info(new StringBuilder("Rolling back EMS6 changes for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(super.customer.getName()));
                
        final List<CustomerMigrationEMS6ModifiedRecord> modifiedRecordList =
                super.customerMigrationEMS6ModifiedRecordService.findModifiedRecordsByCustomerId(super.customer.getId());
                
        if (modifiedRecordList != null && !modifiedRecordList.isEmpty()) {
            LOGGER.info(new StringBuilder("Found ").append(modifiedRecordList.size()).append(" records to change back for Customer: ")
                    .append(super.customer.getId()).append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            for (CustomerMigrationEMS6ModifiedRecord modifiedRecord : modifiedRecordList) {
                super.customerMigrationEMS6Service.rollbackRecord(modifiedRecord, TABLENAME_CUSTOMERPROPERTIES.equals(modifiedRecord.getTableName()),
                                                                  TABLENAME_SERVICESTATE.equals(modifiedRecord.getTableName()));
                                                                  
                LOGGER.info(new StringBuilder("Rollback: Table:").append(modifiedRecord.getTableName()).append(" Id:")
                        .append(modifiedRecord.getTableId()).append(" Column:").append(modifiedRecord.getColumnName()).append("=")
                        .append(modifiedRecord.getValue()).append(" for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                        .append(super.customer.getName()));
                super.customerMigrationEMS6ModifiedRecordService.deleteCustomerMigrationEMS6ModifiedRecordService(modifiedRecord);
            }
            LOGGER.info(new StringBuilder("All changes rolled back for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                    .append(super.customer.getName()));
        } else {
            LOGGER.info(new StringBuilder("No changes found to roll back for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
        }
        
        final List<CustomerMigrationOnlinePOS> onlinePOSList =
                super.customerMigrationOnlinePOSService.findOnlinePOSByCustomerId(super.customer.getId());
                
        if (onlinePOSList != null && !onlinePOSList.isEmpty()) {
            LOGGER.info(new StringBuilder("Found ").append(onlinePOSList.size()).append(" online point of sales to delete for Customer: ")
                    .append(super.customer.getId()).append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            for (CustomerMigrationOnlinePOS onlinePOS : onlinePOSList) {
                
                LOGGER.info(new StringBuilder("Rollback: Point of sale:").append(onlinePOS.getPointOfSale().getSerialNumber()).append(" Id:")
                        .append(onlinePOS.getPointOfSale().getId()).append(" for Customer: ").append(super.customer.getId())
                        .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
                super.customerMigrationOnlinePOSService.deleteCustomerMigrationOnlinePOSService(onlinePOS);
            }
            LOGGER.info(new StringBuilder("All online point of sales deleted for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
        } else {
            LOGGER.info(new StringBuilder("No online point of sales found to delete for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
        }
        
    }
    
}
