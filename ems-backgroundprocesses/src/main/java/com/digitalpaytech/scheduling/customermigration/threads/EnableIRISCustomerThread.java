package com.digitalpaytech.scheduling.customermigration.threads;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.domain.CustomerMigrationEMS6ModifiedRecord;
import com.digitalpaytech.domain.CustomerMigrationOnlinePOS;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerPropertyType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.PosStatus;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.CustomerStatusTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.LabelConstants;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

//Catching exception is necessary because we want to log everything that can go wrong to be able to fix the issue 
//Instantiated objects are StringBuilder objects and adding a missing heartbeat is it is missing.
@SuppressWarnings({ "checkstyle:illegalcatch", "PMD.AvoidInstantiatingObjectsInLoops" })
public class EnableIRISCustomerThread extends MigrationBaseThread {
    private static final Logger LOGGER = Logger.getLogger(EnableIRISCustomerThread.class);
    
    private static final int CURRENT_STEP = 4;
    
    private static final int STEP_INITIAL = 0;
    private static final int STEP_CUSTOMER_PROPERTY_FIXED = 1;
    private static final int STEP_HEARTBEATS_SET = 2;
    private static final int STEP_CUSTOMER_ENABLED = 3;
    private static final int STEP_STARTING_IRIS_REENABLE = 4;
    private static final int STEP_FINISHED_IRIS_REENABLE = 5;
    private static final int STEP_STARTING_OFFLINE_PAYSTATION_FIX = 6;
    private static final int STEP_FINISHED_OFFLINE_PAYSTATION_FIX = 7;
    private static final int STEP_STARTING_EMS6_URL_RESET = 8;
    private static final int STEP_FINISHED_EMS6_URL_RESET = 9;
    private static final int STEP_EMAIL_SENT = 10;
    
    private static final String MIGRATION_STATUS_SET_TO_FALSE = " migration status set to false.";
    private static final String LAST_COMPLETED_STEP = "Last completed step: ";
    private static final String IT_WILL_BE_CREATED = " it will be created.";
    
    private final transient CustomerService customerService;
    private final transient CustomerStatusTypeService customerStatusTypeService;
    private final transient CustomerAdminService customerAdminService;
    private final transient PointOfSaleService pointOfSaleService;
    
    private transient int step;
    
    public EnableIRISCustomerThread(final CustomerMigration customerMigration, final ApplicationContext applicationContext) {
        super(customerMigration, applicationContext);
        
        this.customerService = (CustomerService) applicationContext.getBean("customerService");
        this.customerStatusTypeService = (CustomerStatusTypeService) applicationContext.getBean("customerStatusTypeService");
        this.customerAdminService = (CustomerAdminService) applicationContext.getBean("customerAdminService");
        this.pointOfSaleService = (PointOfSaleService) applicationContext.getBean("pointOfSaleService");
        
    }
    
    @Override
    public final void run() {
        this.step = STEP_INITIAL;
        try {
            LOGGER.info(new StringBuilder("Step ").append(CURRENT_STEP).append(" started for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            super.customerMigration.setIsMigrationInProgress(true);
            super.customerMigrationService.updateCustomerMigration(super.customerMigration);
            
            final Integer ems6CustomerId = super.customerMigrationIRISService.findEMS6CustomerId(customer.getId());
            
            setCardRetryProperties(ems6CustomerId);
            this.step = STEP_CUSTOMER_PROPERTY_FIXED;
            setHeartbeats();
            this.step = STEP_HEARTBEATS_SET;
            enableCustomer();
            this.step = STEP_CUSTOMER_ENABLED;
            resetEMS6URLRerouteFlag(ems6CustomerId);
            
            final int timeDelayInMinutes =
                    this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.CUSTOMER_MIGRATION_HEARTBEAT_DELAY_IN_MINUTES,
                                                                    EmsPropertiesService.CUSTOMER_MIGRATION_PAY_STATION_DELAY_IN_HOURS_DEFAULT, true);
                                                                    
            super.customerMigration.setBackGroundJobNextTryGmt(addDelay(Calendar.MINUTE, timeDelayInMinutes * 2));
            
            super.customerMigration.setCustomerMigrationStatusType(super.customerMigrationStatusTypeService
                    .findCustomerMigrationStatusType(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMPLETED));
            super.customerMigration.setIsMigrationInProgress(false);
            super.customerMigration.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
            super.customerMigration.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
            super.customerMigration.setMigrationEndGmt(DateUtil.getCurrentGmtDate());
            super.customerMigrationService.updateCustomerMigration(super.customerMigration);
            
            LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                    .append(super.customer.getName()).append(" migration Complete."));
                    
            final String subject = super.reportingUtil.getPropertyEN("customermigration.email.customer.migrationcompleted.subject",
                                                                     new String[] { super.customer.getName() });
            final String content = super.reportingUtil.getPropertyEN("customermigration.email.customer.migrationcompleted.content");
            this.mailerService.sendMessage(super.customerEmailAddress, subject, content, null);
            
            final String subjectIT = super.reportingUtil.getPropertyEN("customermigration.email.admin.migrationcompleted.subject",
                                                                       new String[] { super.customer.getId().toString(), super.customer.getName(), });
            final String contentIT = super.reportingUtil.getPropertyEN("customermigration.email.admin.migrationcompleted.content",
                                                                       new String[] { super.customer.getId().toString(), super.customer.getName(), });
                                                                       
            this.mailerService.sendMessage(super.itEmailAddress, subjectIT, contentIT, null);
            
            this.step = STEP_EMAIL_SENT;
            
            super.customerMigration.setCompletionEmailSentGmt(DateUtil.getCurrentGmtDate());
            super.customerMigrationService.updateCustomerMigration(super.customerMigration);
            
            LOGGER.info(new StringBuilder("Completion email send to Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                    .append(super.customer.getName()));
            super.printCustomerReport();
        } catch (Exception e) {
            handleException(e);
        }
        
    }
    
    // Complexity is created by the switch statement. Temporary code refacturing increases the risk of bugs
    @SuppressWarnings("checkstyle:cyclomaticcomplexity")
    private void handleException(final Exception e) {
        LOGGER.error(EXCEPTION_THROWN, e);
        if (this.step < STEP_FINISHED_EMS6_URL_RESET) {
            super.customerMigration.setCustomerMigrationStatusType(super.customerMigrationStatusTypeService
                    .findCustomerMigrationStatusType(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_CANCELLED));
            super.customerMigration.setIsMigrationInProgress(false);
            super.customerMigrationService.updateCustomerMigration(super.customerMigration);
            
            final String str1 =
                    new StringBuilder("Migration failed before pay station URL rerouted. Cancelling and rolling back changes for Customer: ")
                            .append(super.customer.getId()).append(STRING_DASH_WITH_SPACES).append(super.customer.getName()).toString();
            String stepName = null;
            switch (this.step) {
                case STEP_CUSTOMER_PROPERTY_FIXED:
                    stepName = "CustomerProperty MaxCCRetry fixed";
                    break;
                case STEP_HEARTBEATS_SET:
                    stepName = "Heartbeats set";
                    break;
                case STEP_CUSTOMER_ENABLED:
                case STEP_STARTING_IRIS_REENABLE:
                case STEP_FINISHED_IRIS_REENABLE:
                case STEP_STARTING_OFFLINE_PAYSTATION_FIX:
                case STEP_FINISHED_OFFLINE_PAYSTATION_FIX:
                    if (this.step == STEP_CUSTOMER_ENABLED) {
                        stepName = "Customer enabled";
                    } else if (this.step == STEP_STARTING_IRIS_REENABLE) {
                        stepName = "Starting Iris re-enable";
                    } else if (this.step == STEP_FINISHED_IRIS_REENABLE) {
                        stepName = "Finished Iris re-enable";
                    } else if (this.step == STEP_STARTING_OFFLINE_PAYSTATION_FIX) {
                        stepName = "Starting offline paystation fix";
                    } else if (this.step == STEP_FINISHED_OFFLINE_PAYSTATION_FIX) {
                        stepName = "Finished offline paystation fix";
                    }
                    super.customer = this.customerService.findCustomerNoCache(super.customer.getId());
                    super.customer.setIsMigrated(false);
                    super.customer.setMigratedDate(null);
                    if (super.customerMigration.getCustomerMigrationFailureType() == null) {
                        super.customerMigration.setCustomerMigrationFailureType(super.customerMigrationFailureTypeService
                                .findCustomerMigrationFailureType(WebCoreConstants.CUSTOMER_MIGRATION_FAILURE_TYPE_UNKNOWN));
                    }
                    super.customer.setCustomerStatusType(this.customerStatusTypeService
                            .findCustomerStatusType(WebCoreConstants.CUSTOMER_STATUS_TYPE_ENABLED));
                    this.customerService.update(super.customer);
                    LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                            .append(super.customer.getName()).append(MIGRATION_STATUS_SET_TO_FALSE));
                    break;
                case STEP_STARTING_EMS6_URL_RESET:
                    stepName = "Before starting EMS URL Reset";
                    super.customer = this.customerService.findCustomerNoCache(super.customer.getId());
                    super.customer.setIsMigrated(false);
                    super.customer.setMigratedDate(null);
                    if (super.customerMigration.getCustomerMigrationFailureType() == null) {
                        super.customerMigration.setCustomerMigrationFailureType(super.customerMigrationFailureTypeService
                                .findCustomerMigrationFailureType(WebCoreConstants.CUSTOMER_MIGRATION_FAILURE_TYPE_UNKNOWN));
                    }
                    super.customer.setCustomerStatusType(this.customerStatusTypeService
                            .findCustomerStatusType(WebCoreConstants.CUSTOMER_STATUS_TYPE_ENABLED));
                    this.customerService.update(super.customer);
                    LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                            .append(super.customer.getName()).append(MIGRATION_STATUS_SET_TO_FALSE));
                    break;
                default:
                    break;
            }
            final String str3 = new StringBuilder(LAST_COMPLETED_STEP).append(stepName).toString();
            
            LOGGER.error(str1);
            LOGGER.error(str3);
            
            final String subject = super.reportingUtil.getPropertyEN(LabelConstants.CM_EMAIL_ADMIN_MIGRATIONCANCEL_SUBJECT,
                                                                     new String[] { super.customer.getId().toString(), super.customer.getName(), });
            final String content =
                    super.reportingUtil.getPropertyEN("customermigration.email.admin.migrationcancelled.beforeurlreroute.content",
                                                      new String[] { super.customer.getId().toString(), super.customer.getName(), stepName, });
                                                      
            super.mailerService.sendMessage(super.itEmailAddress, subject, content, null);
            
        } else {
            final String str1 = new StringBuilder("Unexpected exception ").append(e.getClass().getName()).append(": '").append(e.getMessage())
                    .append("' for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES).append(super.customer.getName())
                    .toString();
            final String str2 = new StringBuilder("Manually finish Migration for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()).toString();
            String stepName = null;
            switch (this.step) {
                case STEP_FINISHED_EMS6_URL_RESET:
                    stepName = "EMS URL Reset flag set";
                    break;
                case STEP_EMAIL_SENT:
                    stepName = "Confirmation Email send";
                    break;
                default:
                    break;
            }
            final String str3 = new StringBuilder(LAST_COMPLETED_STEP).append(stepName).toString();
            LOGGER.error(str1);
            LOGGER.error(str2);
            LOGGER.error(str3);
            super.printCustomerReport();
            
            final String subject = super.reportingUtil.getPropertyEN(LabelConstants.CM_EMAIL_ADMIN_MIGRATIONCANCEL_SUBJECT,
                                                                     new String[] { super.customer.getId().toString(), super.customer.getName(), });
            final String content =
                    super.reportingUtil.getPropertyEN("customermigration.email.admin.migrationcancelled.afterurlreroute.content",
                                                      new String[] { super.customer.getId().toString(), super.customer.getName(), stepName, });
                                                      
            super.mailerService.sendMessage(super.itEmailAddress, subject, content, null);
            
        }
        
    }
    
    private void setCardRetryProperties(final Integer ems6CustomerId) {
        LOGGER.info(new StringBuilder("Setting card retry properties for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(super.customer.getName()));
        CustomerProperty ccRetry = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(super.customer.getId(),
                                                                        WebCoreConstants.CUSTOMER_PROPERTY_TYPE_CREDIT_CARD_OFFLINE_RETRY);
        CustomerProperty maxRetry = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(super.customer.getId(),
                                                                        WebCoreConstants.CUSTOMER_PROPERTY_TYPE_MAX_OFFLINE_RETRY);
                                                                        
        final CustomerMigrationEMS6ModifiedRecord originalCCRetry = super.customerMigrationEMS6ModifiedRecordService
                .findModifiedRecordIfExists(super.customer.getId(), ems6CustomerId, TABLENAME_CUSTOMERPROPERTIES, COLUMNNAME_CCOFFLINERETRY);
        final CustomerMigrationEMS6ModifiedRecord originalMaxRetry = super.customerMigrationEMS6ModifiedRecordService
                .findModifiedRecordIfExists(super.customer.getId(), ems6CustomerId, TABLENAME_CUSTOMERPROPERTIES, COLUMNNAME_MAXOFFLINERETRY);
                
        if (ccRetry == null) {
            LOGGER.info(new StringBuilder("CC Offline Retry property doesn't exist for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()).append(IT_WILL_BE_CREATED));
                    
            ccRetry = new CustomerProperty();
            final CustomerPropertyType ccRetryType = new CustomerPropertyType();
            ccRetryType.setId(WebCoreConstants.CUSTOMER_PROPERTY_TYPE_CREDIT_CARD_OFFLINE_RETRY);
            ccRetry.setCustomerPropertyType(ccRetryType);
            ccRetry.setCustomer(super.customer);
            ccRetry.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
            ccRetry.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        }
        ccRetry.setPropertyValue(originalCCRetry.getValue());
        
        if (maxRetry == null) {
            LOGGER.info(new StringBuilder("Max Offline Retry property doesn't exist for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()).append(IT_WILL_BE_CREATED));
                    
            maxRetry = new CustomerProperty();
            final CustomerPropertyType maxRetryType = new CustomerPropertyType();
            maxRetryType.setId(WebCoreConstants.CUSTOMER_PROPERTY_TYPE_MAX_OFFLINE_RETRY);
            maxRetry.setCustomerPropertyType(maxRetryType);
            maxRetry.setCustomer(super.customer);
            maxRetry.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
            maxRetry.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        }
        
        maxRetry.setPropertyValue(originalMaxRetry.getValue());
        this.customerAdminService.saveOrUpdateCustomerProperty(ccRetry);
        this.customerAdminService.saveOrUpdateCustomerProperty(maxRetry);
        
        LOGGER.info(new StringBuilder("CustomerProperty values: CCOfflineRetry - ").append(ccRetry.getPropertyValue()).append(", MaxOfflineRetry - ")
                .append(maxRetry.getPropertyValue()).append(" set for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(super.customer.getName()));
                
    }
    
    private void enableCustomer() {
        
        LOGGER.info(new StringBuilder("Enabling Iris customer for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(super.customer.getName()));
        super.customer = this.customerService.findCustomerNoCache(super.customer.getId());
        super.customer.setIsMigrated(true);
        super.customer.setMigratedDate(DateUtil.getCurrentGmtDate());
        super.customer.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        super.customer.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
        super.customer.setCustomerStatusType(this.customerStatusTypeService.findCustomerStatusType(WebCoreConstants.CUSTOMER_STATUS_TYPE_ENABLED));
        this.customerService.update(super.customer);
        
        super.customerMigration.setEms7CustomerEnabledGmt(DateUtil.getCurrentGmtDate());
        LOGGER.info(new StringBuilder("Iris Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(super.customer.getName()).append(" enabled and migrationStatus set to true"));
    }
    
    private void setHeartbeats() {
        
        final Calendar delayedTime = Calendar.getInstance();
        final int waitTime =
                super.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.CUSTOMER_MIGRATION_HEARTBEAT_DELAY_IN_MINUTES,
                                                                 EmsPropertiesService.CUSTOMER_MIGRATION_HEARTBEAT_DELAY_IN_MINUTES_DEFAULT);
        delayedTime.add(Calendar.MINUTE, waitTime);
        final Date heartbeatDelay = delayedTime.getTime();
        
        final List<CustomerMigrationOnlinePOS> onlinePOSList =
                this.customerMigrationOnlinePOSService.findOnlinePOSByCustomerId(super.customer.getId());
                
        LOGGER.info(new StringBuilder("Setting heartbeats of ").append(onlinePOSList.size()).append(" point of sales to ").append(heartbeatDelay)
                .append(STRING_FOR_CUSTOMER_LABEL).append(super.customer.getId()).append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
                
        for (CustomerMigrationOnlinePOS onlinePOS : onlinePOSList) {
            PosHeartbeat posHeartbeat = this.pointOfSaleService.findPosHeartbeatByPointOfSaleId(onlinePOS.getPointOfSale().getId());
            if (posHeartbeat == null) {
                LOGGER.error(new StringBuilder("Heartbeat doesn't exist for PointOfSale: ").append(onlinePOS.getPointOfSale().getId())
                        .append(StandardConstants.STRING_DASH).append(onlinePOS.getPointOfSale().getSerialNumber()).append(STRING_FOR_CUSTOMER_LABEL)
                        .append(super.customer.getId()).append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
                posHeartbeat = new PosHeartbeat();
                posHeartbeat.setLastHeartbeatGmt(heartbeatDelay);
                posHeartbeat.setPointOfSale(onlinePOS.getPointOfSale());
                this.pointOfSaleService.saveOrUpdatePointOfSaleHeartbeat(posHeartbeat);
                LOGGER.info(new StringBuilder("Heartbeat created and set for PointOfSale: ").append(onlinePOS.getPointOfSale().getId())
                        .append(StandardConstants.STRING_DASH).append(onlinePOS.getPointOfSale().getSerialNumber()).append(STRING_FOR_CUSTOMER_LABEL)
                        .append(super.customer.getId()).append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            } else {
                posHeartbeat.setLastHeartbeatGmt(heartbeatDelay);
                this.pointOfSaleService.saveOrUpdatePointOfSaleHeartbeat(posHeartbeat);
                
                LOGGER.info(new StringBuilder("Heartbeat set for PointOfSale: ").append(onlinePOS.getPointOfSale().getId())
                        .append(StandardConstants.STRING_DASH).append(onlinePOS.getPointOfSale().getSerialNumber()).append(STRING_FOR_CUSTOMER_LABEL)
                        .append(super.customer.getId()).append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            }
        }
        
        super.customerMigration.setHeartbeatsResetGmt(DateUtil.getCurrentGmtDate());
        LOGGER.info(new StringBuilder("Heartbeats set for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(super.customer.getName()));
                
    }
    
    // Temporary code refacturing increases the risk of bugs
    @SuppressWarnings("checkstyle:cyclomaticcomplexity")
    private void resetEMS6URLRerouteFlag(final Integer ems6CustomerId) {
        LOGGER.info(new StringBuilder("Checking URLRerouteId for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(super.customer.getName()));
                
        final int defaultURLRerouteId = super.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.DEFAULT_EMS6_URLREROUTE_ID,
                                                                                         EmsPropertiesService.DEFAULT_EMS6_URLREROUTE_ID_DEFAULT);
        LOGGER.info(new StringBuilder("Using ").append(defaultURLRerouteId).append(" as URLRerouteId for Customer: ").append(super.customer.getId())
                .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
                
        final List<CustomerMigrationEMS6ModifiedRecord> updatedPaystationList = super.customerMigrationEMS6ModifiedRecordService
                .findModifiedRecordsByCustomerIdAndTableType(super.customer.getId(), TABLENAME_PAYSTATION);
                
        if (updatedPaystationList == null || updatedPaystationList.isEmpty()) {
            LOGGER.info(new StringBuilder("Found no pay stations to reenable for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
        } else {
            LOGGER.info(new StringBuilder(STRING_FOUND_LABEL).append(updatedPaystationList.size()).append(" pay stations to reenable for Customer: ")
                    .append(super.customer.getId()).append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            this.step = STEP_STARTING_IRIS_REENABLE;
            for (CustomerMigrationEMS6ModifiedRecord paystation : updatedPaystationList) {
                final Integer ems6PayStationId = paystation.getTableId();
                final Integer irisPaystationId = super.customerMigrationIRISService.findIRISPaystationId(ems6PayStationId);
                final PointOfSale pointOfSale = super.pointOfSaleService.findPointOfSaleByPaystationId(irisPaystationId);
                final PosStatus posStatus = super.pointOfSaleService.findPointOfSaleStatusByPOSId(pointOfSale.getId(), false);
                posStatus.setIsActivated(true);
                this.customerAdminService.saveOrUpdatePointOfSaleStatus(posStatus);
                LOGGER.info(new StringBuilder(STRING_POINTOFSALE_LABEL).append(pointOfSale.getId()).append(StandardConstants.STRING_DASH)
                        .append(pointOfSale.getSerialNumber()).append(" reenabled for Customer: ").append(super.customer.getId())
                        .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            }
            this.step = STEP_FINISHED_IRIS_REENABLE;
        }
        
        final List<CustomerMigrationEMS6ModifiedRecord> updatedOfflinePaystationsList = super.customerMigrationEMS6ModifiedRecordService
                .findModifiedRecordsByCustomerIdAndTableType(super.customer.getId(), TABLENAME_SERVICESTATE);
                
        if (updatedOfflinePaystationsList == null || updatedOfflinePaystationsList.isEmpty()) {
            LOGGER.info(new StringBuilder("Found no offline pay stations to fix for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
        } else {
            LOGGER.info(new StringBuilder(STRING_FOUND_LABEL).append(updatedOfflinePaystationsList.size())
                    .append(" offline pay stations to fix for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                    .append(super.customer.getName()));
            this.step = STEP_STARTING_OFFLINE_PAYSTATION_FIX;
            for (CustomerMigrationEMS6ModifiedRecord serviceState : updatedOfflinePaystationsList) {
                final Integer ems6PayStationId = serviceState.getTableId();
                final Integer irisPaystationId = super.customerMigrationIRISService.findIRISPaystationId(ems6PayStationId);
                final PointOfSale pointOfSale = super.pointOfSaleService.findPointOfSaleByPaystationId(irisPaystationId);
                final PosServiceState posServiceState = super.pointOfSaleService.findPosServiceStateByPointOfSaleId(pointOfSale.getId());
                if (COLUMNNAME_PRIMARYVERSION.equals(serviceState.getColumnName())) {
                    posServiceState.setPrimaryVersion(serviceState.getValue().substring(1, serviceState.getValue().length() - 1));
                } else {
                    posServiceState.setSecondaryVersion(serviceState.getValue().substring(1, serviceState.getValue().length() - 1));
                }
                this.pointOfSaleService.updatePosServicState(posServiceState);
                LOGGER.info(new StringBuilder(STRING_POINTOFSALE_LABEL).append(pointOfSale.getId()).append(StandardConstants.STRING_DASH)
                        .append(pointOfSale.getSerialNumber()).append(" versions fixed for Customer: ").append(super.customer.getId())
                        .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            }
            this.step = STEP_FINISHED_OFFLINE_PAYSTATION_FIX;
        }
        
        final List<Object[]> paystationList = super.customerMigrationEMS6Service.findAllPaystationsByCustomer(ems6CustomerId);
        
        if (paystationList == null || paystationList.isEmpty()) {
            LOGGER.info(new StringBuilder("Found no pay stations to reroute for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
        } else {
            LOGGER.info(new StringBuilder(STRING_FOUND_LABEL).append(paystationList.size()).append(" pay stations to reroute for Customer: ")
                    .append(super.customer.getId()).append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            this.step = STEP_STARTING_EMS6_URL_RESET;
            super.customerMigrationEMS6Service.setPaystationURLRerouteIds(ems6CustomerId, (byte) defaultURLRerouteId);
            this.step = STEP_FINISHED_EMS6_URL_RESET;
            for (Object[] paystation : paystationList) {
                LOGGER.info(new StringBuilder("Paystation: ").append(paystation[0].toString()).append(StandardConstants.STRING_DASH)
                        .append(paystation[0].toString()).append(" rerouted for Customer: ").append(super.customer.getId())
                        .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
                super.recordModifiedRecord(ems6CustomerId, (Integer) paystation[0], TABLENAME_PAYSTATION, COLUMNNAME_EMSPAYSTATIONURLREROUTEID,
                                           String.valueOf(0));
            }
            
        }
        
        super.customerMigration.setEms6ForwardFlagSetGmt(DateUtil.getCurrentGmtDate());
        LOGGER.info(new StringBuilder("All pay stations rerouted for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(super.customer.getName()));
                
    }
    
}
