package com.digitalpaytech.scheduling.customermigration.threads;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.domain.CustomerMigrationEMS6ModifiedRecord;
import com.digitalpaytech.domain.CustomerMigrationEmail;
import com.digitalpaytech.service.CustomerMigrationEMS6ModifiedRecordService;
import com.digitalpaytech.service.CustomerMigrationFailureTypeService;
import com.digitalpaytech.service.CustomerMigrationOnlinePOSService;
import com.digitalpaytech.service.CustomerMigrationService;
import com.digitalpaytech.service.CustomerMigrationStatusTypeService;
import com.digitalpaytech.service.CustomerMigrationValidationStatusService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.customermigration.CustomerMigrationEMS6Service;
import com.digitalpaytech.service.customermigration.CustomerMigrationIRISService;
import com.digitalpaytech.util.ReportingUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebSecurityConstants;

//Catching exception is necessary because we want to log everything that can go wrong to be able to fix the issue
//Temporary class not refactured for simplicity
@SuppressWarnings({ "checkstyle:illegalcatch", "PMD.BeanMembersShouldSerialize", "PMD.TooManyFields" })
public abstract class MigrationBaseThread extends Thread {
    
    protected static final String TABLENAME_CUSTOMER = "Customer";
    protected static final String TABLENAME_PAYSTATION = "Paystation";
    protected static final String TABLENAME_USERACCOUNT = "UserAccount";
    protected static final String TABLENAME_ALERT = "Alert";
    protected static final String TABLENAME_CUSTOMERPROPERTIES = "CustomerProperties";
    protected static final String TABLENAME_SERVICESTATE = "ServiceState";
    
    protected static final String COLUMNNAME_ACCOUNTSTATUS = "AccountStatus";
    protected static final String COLUMNNAME_LOCKSTATE = "LockState";
    protected static final String COLUMNNAME_ISENABLED = "IsEnabled";
    protected static final String COLUMNNAME_ISDELETED = "IsDeleted";
    protected static final String COLUMNNAME_CCOFFLINERETRY = "CCOfflineRetry";
    protected static final String COLUMNNAME_MAXOFFLINERETRY = "MaxOfflineRetry";
    protected static final String COLUMNNAME_EMSPAYSTATIONURLREROUTEID = "EMSPaystationURLRerouteId";
    protected static final String COLUMNNAME_PRIMARYVERSION = "PrimaryVersion";
    protected static final String COLUMNNAME_SECONDARYVERSION = "SecondaryVersion";
    
    protected static final String ACCOUNTSTATUS_DISABLED = "\'DISABLED\'";
    protected static final String ACCOUNTSTATUS_ENABLED = "\'ENABLED\'";
    protected static final String LOCKSTATE_UNLOCKED = "\'UNLOCKED\'";
    protected static final String LOCKSTATE_LOCKED = "\'LOCKED\'";
    protected static final String VERSION_OFFLINE = "\'offline\'";
    protected static final String BOOLEAN_TRUE = "1";
    protected static final String BOOLEAN_FALSE = "0";
    
    protected static final String STRING_DASH_WITH_SPACES = " - ";
    protected static final String STRING_CUSTOMER_LABEL = "Customer: ";
    protected static final String STRING_POINTOFSALE_LABEL = "PointOfSale: ";
    protected static final String STRING_FOR_CUSTOMER_LABEL = " for Customer: ";
    protected static final String STRING_FOUND_LABEL = "Found ";
    
    protected static final String EXCEPTION_THROWN = "Exception thrown:";
    
    private static final Logger LOGGER = Logger.getLogger(MigrationBaseThread.class);
    
    protected Customer customer;
    protected CustomerMigration customerMigration;
    protected String customerEmailAddress;
    protected String itEmailAddress;
    protected String itValidationEmailAddress;
    
    protected CustomerMigrationService customerMigrationService;
    protected CustomerMigrationValidationStatusService customerMigrationValidationStatusService;
    protected CustomerMigrationIRISService customerMigrationIRISService;
    protected CustomerMigrationEMS6Service customerMigrationEMS6Service;
    protected CustomerMigrationStatusTypeService customerMigrationStatusTypeService;
    protected CustomerMigrationFailureTypeService customerMigrationFailureTypeService;
    protected CustomerMigrationEMS6ModifiedRecordService customerMigrationEMS6ModifiedRecordService;
    protected CustomerMigrationOnlinePOSService customerMigrationOnlinePOSService;
    
    protected EmsPropertiesService emsPropertiesService;
    
    protected PointOfSaleService pointOfSaleService;
    
    protected MailerService mailerService;
    
    protected ReportingUtil reportingUtil;
    
    public MigrationBaseThread(final CustomerMigration customerMigration, final ApplicationContext applicationContext) {
        super();
        this.customerMigration = customerMigration;
        this.customer = customerMigration.getCustomer();
        
        this.customerMigrationService = (CustomerMigrationService) applicationContext.getBean("customerMigrationService");
        this.customerMigrationValidationStatusService = (CustomerMigrationValidationStatusService) applicationContext
                .getBean("customerMigrationValidationStatusService");
        this.customerMigrationIRISService = (CustomerMigrationIRISService) applicationContext.getBean("customerMigrationIRISService");
        this.customerMigrationEMS6Service = (CustomerMigrationEMS6Service) applicationContext.getBean("customerMigrationEMS6Service");
        this.customerMigrationStatusTypeService = (CustomerMigrationStatusTypeService) applicationContext
                .getBean("customerMigrationStatusTypeService");
        this.customerMigrationFailureTypeService = (CustomerMigrationFailureTypeService) applicationContext
                .getBean("customerMigrationFailureTypeService");
        this.customerMigrationEMS6ModifiedRecordService = (CustomerMigrationEMS6ModifiedRecordService) applicationContext
                .getBean("customerMigrationEMS6ModifiedRecordService");
        this.customerMigrationOnlinePOSService = (CustomerMigrationOnlinePOSService) applicationContext.getBean("customerMigrationOnlinePOSService");
        
        this.emsPropertiesService = (EmsPropertiesService) applicationContext.getBean("emsPropertiesService");
        this.pointOfSaleService = (PointOfSaleService) applicationContext.getBean("pointOfSaleService");
        this.mailerService = (MailerService) applicationContext.getBean("mailerService");
        this.reportingUtil = (ReportingUtil) applicationContext.getBean("reportingUtil");
        
        this.itEmailAddress = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.CUSTOMER_MIGRATION_ALERT_EMAIL,
                                                                         EmsPropertiesService.CUSTOMER_MIGRATION_ALERT_EMAIL_DEFAULT, true);
        
        this.itValidationEmailAddress = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.CUSTOMER_MIGRATION_IT_EMAIL,
                                                                                   EmsPropertiesService.CUSTOMER_MIGRATION_IT_EMAIL_DEFAULT, true);
        
        try {
            this.customerEmailAddress = URLDecoder.decode(getCustomerEmailAddresses(), WebSecurityConstants.URL_ENCODING_LATIN1);
        } catch (UnsupportedEncodingException uee) {
            LOGGER.error(EXCEPTION_THROWN, uee);
            this.customerEmailAddress = getCustomerEmailAddresses();
        }
        
    }
    
    protected final void recordModifiedRecord(final Integer ems6CustomerId, final Integer tableId, final String tableName, final String columnName,
        final String value) {
        
        CustomerMigrationEMS6ModifiedRecord modifiedRecord = this.customerMigrationEMS6ModifiedRecordService.findModifiedRecordIfExists(this.customer
                .getId(), tableId, tableName, columnName);
        if (modifiedRecord == null) {
            modifiedRecord = new CustomerMigrationEMS6ModifiedRecord(this.customer, ems6CustomerId, tableId, tableName, columnName, value);
            this.customerMigrationEMS6ModifiedRecordService.saveCustomerMigrationEMS6ModifiedRecordService(modifiedRecord);
        } else {
            modifiedRecord.setValue(value);
            this.customerMigrationEMS6ModifiedRecordService.updateCustomerMigrationEMS6ModifiedRecordService(modifiedRecord);
        }
    }
    
    protected final void printCustomerReport() {
        LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(this.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(this.customer.getName()).append(" Status: ")
                .append(this.customerMigrationStatusTypeService.getText(this.customerMigration.getCustomerMigrationStatusType().getId())));
        if (this.customerMigration.getCustomerMigrationFailureType() != null) {
            LOGGER.info(new StringBuilder("Failure: ").append(this.customer.getId()).append(STRING_DASH_WITH_SPACES).append(this.customer.getName())
                    .append(" Failure: ")
                    .append(this.customerMigrationFailureTypeService.getText(this.customerMigration.getCustomerMigrationFailureType().getId())));
        }
        LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(this.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(this.customer.getName()).append(" CustomerBoardedGMT: ").append(this.customerMigration.getCustomerBoardedGmt()));
        LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(this.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(this.customer.getName()).append(" MigrationRequestedGMT: ").append(this.customerMigration.getMigrationRequestedGmt()));
        LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(this.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(this.customer.getName()).append(" MigrationScheduledGMT: ").append(this.customerMigration.getMigrationScheduledGmt()));
        LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(this.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(this.customer.getName()).append(" MigrationStartGMT: ").append(this.customerMigration.getMigrationStartGmt()));
        LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(this.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(this.customer.getName()).append(" MigrationEndGMT: ").append(this.customerMigration.getMigrationEndGmt()));
        LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(this.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(this.customer.getName()).append(" MigrationCancelGMT: ").append(this.customerMigration.getMigrationCancelGmt()));
        LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(this.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(this.customer.getName()).append(" StartEmailSentGMT: ").append(this.customerMigration.getStartEmailSentGmt()));
        LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(this.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(this.customer.getName()).append(" EMS6CustomerLockedGMT: ").append(this.customerMigration.getEms6CustomerLockedGmt()));
        LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(this.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(this.customer.getName()).append(" DataMigrationConfirmedGMT: ").append(this.customerMigration.getDataMigrationConfirmedGmt()));
        LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(this.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(this.customer.getName()).append(" IRISCustomerEnabledGMT: ").append(this.customerMigration.getEms7CustomerEnabledGmt()));
        LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(this.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(this.customer.getName()).append(" HeartbeatsResetGMT: ").append(this.customerMigration.getHeartbeatsResetGmt()));
        LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(this.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(this.customer.getName()).append(" EMS6ForwardFlagSetGMT: ").append(this.customerMigration.getEms6ForwardFlagSetGmt()));
        LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(this.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(this.customer.getName()).append(" CompletionEmailSentGMT: ").append(this.customerMigration.getCompletionEmailSentGmt()));
        LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(this.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(this.customer.getName()).append(" PaystationHeartbeatSetGMT: ").append(this.customerMigration.getPaystationHeartbeatSetGmt()));
        LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(this.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(this.customer.getName()).append(" CommunicationEmailSentGMT: ").append(this.customerMigration.getCommunicationEmailSentGmt()));
    }
    
    private String getCustomerEmailAddresses() {
        final List<CustomerMigrationEmail> emailList = this.customerMigrationService.findCustomerMigrationEmails(this.customerMigration.getId());
        final StringBuilder emailAddresses = new StringBuilder();
        
        for (CustomerMigrationEmail customerMigrationEmail : emailList) {
            emailAddresses.append(customerMigrationEmail.getCustomerEmail().getEmail());
            emailAddresses.append(StandardConstants.STRING_COMMA);
        }
        
        return emailAddresses.toString();
    }
    
    protected final Date addDelay(final int delayType, final int value) {
        final Calendar now = Calendar.getInstance();
        now.add(delayType, value);
        return now.getTime();
    }
}
