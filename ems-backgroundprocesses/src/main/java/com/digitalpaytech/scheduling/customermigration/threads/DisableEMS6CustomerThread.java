package com.digitalpaytech.scheduling.customermigration.threads;

import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

//Catching exception is necessary because we want to log everything that can go wrong to be able to fix the issue
//Instantiated objects are StringBuilder objects with log messages.
@SuppressWarnings({ "checkstyle:illegalcatch", "PMD.AvoidInstantiatingObjectsInLoops" })
public class DisableEMS6CustomerThread extends MigrationBaseThread {
    private static final Logger LOGGER = Logger.getLogger(DisableEMS6CustomerThread.class);
    private static final String STRING_FOUND = "Found ";
    private static final String STRING_PAY_STATION = "Pay station: ";
    private static final int CURRENT_STEP = 1;
    
    private static final int ALERT_INDEX_EMS6_ID = 0;
    private static final int ALERT_INDEX_IRIS_ID = 1;
    private static final int ALERT_INDEX_ISENABLED = 2;
    private static final int ALERT_INDEX_ISDELETED = 3;
    
    public DisableEMS6CustomerThread(final CustomerMigration customerMigration, final ApplicationContext applicationContext) {
        super(customerMigration, applicationContext);
        
    }
    
    @Override
    public final void run() {
        try {
            LOGGER.info(new StringBuilder("Step ").append(CURRENT_STEP).append(" started for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            super.customerMigration.setIsMigrationInProgress(true);
            
            super.customerMigrationService.updateCustomerMigration(super.customerMigration);
            LOGGER.info(new StringBuilder("Validating pay station versions for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
                    
            if (lockEMS6User()) {
                
                final int timeDelayInMinutes =
                        this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.DATA_VALIDATION_WAIT_TIME_IN_MINUTES,
                                                                        EmsPropertiesService.DATA_VALIDATION_WAIT_TIME_IN_MINUTES_DEFAULT, true);
                                                                        
                super.customerMigration.setBackGroundJobNextTryGmt(addDelay(Calendar.MINUTE, timeDelayInMinutes));
                super.customerMigration.setCustomerMigrationStatusType(super.customerMigrationStatusTypeService
                        .findCustomerMigrationStatusType(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_EMS6_DISABLED));
                super.customerMigration.setIsMigrationInProgress(false);
                super.customerMigration.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                super.customerMigration.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
                super.customerMigration.setEms6CustomerLockedGmt(DateUtil.getCurrentGmtDate());
                super.customerMigrationService.updateCustomerMigration(super.customerMigration);
                
                LOGGER.info(new StringBuilder(STRING_CUSTOMER_LABEL).append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                        .append(super.customer.getName()).append(" ready for data validation."));
                        
            } else {
                super.customerMigration.setCustomerMigrationStatusType(super.customerMigrationStatusTypeService
                        .findCustomerMigrationStatusType(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SUSPENDED));
                super.customerMigration.setIsMigrationInProgress(false);
                super.customerMigration.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
                super.customerMigration.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
                super.customerMigrationService.updateCustomerMigration(super.customerMigration);
                
                sendFailureEmails(true);
                
                return;
                
            }
        } catch (Exception e) {
            LOGGER.error(EXCEPTION_THROWN, e);
            super.customerMigration.setCustomerMigrationStatusType(super.customerMigrationStatusTypeService
                    .findCustomerMigrationStatusType(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_CANCELLED));
            super.customerMigration.setIsMigrationInProgress(false);
            if (super.customerMigration.getCustomerMigrationFailureType() == null) {
                super.customerMigration.setCustomerMigrationFailureType(super.customerMigrationFailureTypeService
                        .findCustomerMigrationFailureType(WebCoreConstants.CUSTOMER_MIGRATION_FAILURE_TYPE_UNKNOWN));
            }
            super.customerMigrationService.updateCustomerMigration(super.customerMigration);
            
            sendFailureEmails(false);
        }
    }
    
    private boolean lockEMS6User() {
        
        final Integer ems6CustomerId = super.customerMigrationIRISService.findEMS6CustomerId(super.customer.getId());
        
        if (ems6CustomerId == null) {
            LOGGER.error(new StringBuilder("EMS6 Customer cannot be found for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            return false;
        }
        LOGGER.info(new StringBuilder("EMS6 Customer found for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(super.customer.getName()).append(" - EMS6 CustomerId :").append(ems6CustomerId));
                
        super.customerMigrationEMS6Service.disableEMS6Customer(ems6CustomerId);
        super.recordModifiedRecord(ems6CustomerId, ems6CustomerId, TABLENAME_CUSTOMER, COLUMNNAME_ACCOUNTSTATUS, ACCOUNTSTATUS_ENABLED);
        
        LOGGER.info(new StringBuilder("EMS6 Customer Locked for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(super.customer.getName()));
                
        fixOfflinePaystations(ems6CustomerId);
        lockPaystations(ems6CustomerId);
        disableUserAccounts(ems6CustomerId);
        disableAlerts(ems6CustomerId);
        disableCardRetry(ems6CustomerId);
        return true;
    }
    
    private void fixOfflinePaystations(final Integer customerId) {
        LOGGER.info(new StringBuilder("Starting to fix versions for offline pay stations for Customer: ").append(super.customer.getId())
                .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
                
        final List<Object[]> paystationList = super.customerMigrationEMS6Service.findNAPaystations(customerId);
        if (paystationList == null || paystationList.isEmpty()) {
            LOGGER.info(new StringBuilder("No offline pay stations found to lock for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
        } else {
            LOGGER.info(new StringBuilder(STRING_FOUND).append(paystationList.size()).append(" offline pay stations for Customer: ")
                    .append(super.customer.getId()).append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            for (Object[] paystation : paystationList) {
                super.customerMigrationEMS6Service.fixNAPaystationById((Integer) paystation[0]);
                super.recordModifiedRecord(customerId, (Integer) paystation[0], TABLENAME_SERVICESTATE, COLUMNNAME_SECONDARYVERSION,
                                           StandardConstants.STRING_QUOTE + (String) paystation[2] + StandardConstants.STRING_QUOTE);
                LOGGER.info(new StringBuilder(STRING_PAY_STATION).append(paystation[0]).append(StandardConstants.STRING_DASH).append(paystation[1])
                        .append(" versions set to 'offline' (Original values ").append((String) paystation[2]).append(") for Customer: ")
                        .append(super.customer.getId()).append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            }
            LOGGER.info(new StringBuilder("All offline pay stations versions fixed for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
        }
    }
    
    private void lockPaystations(final Integer customerId) {
        LOGGER.info(new StringBuilder("Starting to lock pay stations for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(super.customer.getName()));
                
        final List<Object[]> paystationList = super.customerMigrationEMS6Service.findUnlockedPaystationsByCustomer(customerId);
        if (paystationList == null || paystationList.isEmpty()) {
            LOGGER.info(new StringBuilder("No pay stations found to lock for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
        } else {
            LOGGER.info(new StringBuilder(STRING_FOUND).append(paystationList.size()).append(" pay stations to lock for Customer: ")
                    .append(super.customer.getId()).append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            for (Object[] paystation : paystationList) {
                super.customerMigrationEMS6Service.lockPaystationById((Integer) paystation[0]);
                super.recordModifiedRecord(customerId, (Integer) paystation[0], TABLENAME_PAYSTATION, COLUMNNAME_LOCKSTATE, LOCKSTATE_UNLOCKED);
                LOGGER.info(new StringBuilder(STRING_PAY_STATION).append(paystation[0]).append(StandardConstants.STRING_DASH).append(paystation[1])
                        .append(" locked for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                        .append(super.customer.getName()));
            }
            LOGGER.info(new StringBuilder("All pay stations locked for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                    .append(super.customer.getName()));
        }
        
    }
    
    private void disableUserAccounts(final Integer customerId) {
        LOGGER.info(new StringBuilder("Starting to disable user accounts for Customer: ").append(super.customer.getId())
                .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
                
        final List<Object[]> userAccountList = super.customerMigrationEMS6Service.findEnabledUserAccountsByCustomer(customerId);
        if (userAccountList == null || userAccountList.isEmpty()) {
            LOGGER.info(new StringBuilder("No user accounts found to disable for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
        } else {
            LOGGER.info(new StringBuilder(STRING_FOUND).append(userAccountList.size()).append(" user accounts to disable for Customer: ")
                    .append(super.customer.getId()).append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            for (Object[] userAccount : userAccountList) {
                super.customerMigrationEMS6Service.disableUserAccountById((Integer) userAccount[0]);
                super.recordModifiedRecord(customerId, (Integer) userAccount[0], TABLENAME_USERACCOUNT, COLUMNNAME_ACCOUNTSTATUS,
                                           ACCOUNTSTATUS_ENABLED);
                LOGGER.info(new StringBuilder("User account: ").append(userAccount[0]).append(StandardConstants.STRING_DASH).append(userAccount[1])
                        .append(" disabled for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                        .append(super.customer.getName()));
            }
            LOGGER.info(new StringBuilder("All user accounts disabled for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                    .append(super.customer.getName()));
        }
        
    }
    
    private void disableAlerts(final Integer customerId) {
        LOGGER.info(new StringBuilder("Starting to disable and delete alerts for Customer: ").append(super.customer.getId())
                .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
                
        final List<Object[]> useralertList = super.customerMigrationEMS6Service.findActiveAlertsByCustomer(customerId);
        if (useralertList == null || useralertList.isEmpty()) {
            LOGGER.info(new StringBuilder("No alerts found to disable and delete for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
        } else {
            LOGGER.info(new StringBuilder(STRING_FOUND).append(useralertList.size()).append(" alerts to disable and delete for Customer: ")
                    .append(super.customer.getId()).append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            for (Object[] alert : useralertList) {
                super.customerMigrationEMS6Service.disableAndDeleteAlertById((Integer) alert[ALERT_INDEX_EMS6_ID]);
                super.recordModifiedRecord(customerId, (Integer) alert[ALERT_INDEX_EMS6_ID], TABLENAME_ALERT, COLUMNNAME_ISENABLED,
                                           ((Boolean) alert[ALERT_INDEX_ISENABLED]) ? BOOLEAN_TRUE : BOOLEAN_FALSE);
                super.recordModifiedRecord(customerId, (Integer) alert[ALERT_INDEX_EMS6_ID], TABLENAME_ALERT, COLUMNNAME_ISDELETED,
                                           ((Boolean) alert[ALERT_INDEX_ISDELETED]) ? BOOLEAN_TRUE : BOOLEAN_FALSE);
                                           
                LOGGER.info(new StringBuilder("Alert: ").append(alert[ALERT_INDEX_EMS6_ID]).append(StandardConstants.STRING_DASH)
                        .append(alert[ALERT_INDEX_IRIS_ID]).append(" disabled and deleted for Customer: ").append(super.customer.getId())
                        .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            }
            LOGGER.info(new StringBuilder("All alerts disabled and deleted for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
        }
        
    }
    
    private void disableCardRetry(final Integer customerId) {
        LOGGER.info(new StringBuilder("Starting to disable card retry for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES)
                .append(super.customer.getName()));
                
        final Object[] properties = super.customerMigrationEMS6Service.findCardRetryProperties(customerId);
        
        LOGGER.info(new StringBuilder("Original Values: CCOfflineRetry - ").append(properties[0]).append(", MaxOfflineRetry - ").append(properties[2])
                .append(" for Customer: ").append(super.customer.getId()).append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
                
        super.customerMigrationEMS6Service.disableCardRetryByCustomerId(customerId);
        super.recordModifiedRecord(customerId, customerId, TABLENAME_CUSTOMERPROPERTIES, COLUMNNAME_CCOFFLINERETRY,
                                   ((Short) properties[1]).toString());
        super.recordModifiedRecord(customerId, customerId, TABLENAME_CUSTOMERPROPERTIES, COLUMNNAME_MAXOFFLINERETRY,
                                   ((Short) properties[2]).toString());
                                   
        LOGGER.info(new StringBuilder("CustomerProperties for CardRetry set to 0 for Customer: ").append(super.customer.getId())
                .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
    }
    
    private void sendFailureEmails(final boolean sendCustomerEmail) {
        if (sendCustomerEmail) {
            final String subject = super.reportingUtil.getPropertyEN("customermigration.email.customer.migrationfailed.subject",
                                                                     new String[] { super.customer.getName() });
            final String content =
                    super.reportingUtil.getPropertyEN("customermigration.email.customer.migrationfailed.ems6customercannotbelocked.content");
                    
            super.mailerService.sendMessage(super.customerEmailAddress, subject, content, null);
        }
        final String subjectIT = super.reportingUtil.getPropertyEN("customermigration.email.admin.migrationcancelled.subject",
                                                                   new String[] { super.customer.getId().toString(), super.customer.getName(), });
        final String contentIT = super.reportingUtil.getPropertyEN("customermigration.email.admin.migrationcancelled.ems6customerlock.content",
                                                                   new String[] { super.customer.getId().toString(), super.customer.getName(), });
                                                                   
        super.mailerService.sendMessage(super.itEmailAddress, subjectIT, contentIT, null);
        
        LOGGER.error(new StringBuilder("EMS6 Customer cannot be locked in EMS6 for Customer: ").append(super.customer.getId())
                .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
        LOGGER.error(new StringBuilder("Status changed back to scheduled for Customer: ").append(super.customer.getId())
                .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
                
        LOGGER.error(contentIT);
        
    }
    
}
