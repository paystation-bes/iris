package com.digitalpaytech.scheduling.customermigration.threads;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.domain.CustomerMigrationOnlinePOS;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

//Catching exception is necessary because we want to log everything that can go wrong to be able to fix the issue 
//Instantiated objects are StringBuilder objects with log messages.
@SuppressWarnings({ "checkstyle:illegalcatch", "PMD.AvoidInstantiatingObjectsInLoops" })
public class CommunicationValidationThread extends MigrationBaseThread {
    private static final Logger LOGGER = Logger.getLogger(CommunicationValidationThread.class);
    private static final int CURRENT_STEP = 5;
    
    public CommunicationValidationThread(final CustomerMigration customerMigration, final ApplicationContext applicationContext) {
        super(customerMigration, applicationContext);
    }
    
    @Override
    public final void run() {
        
        LOGGER.info(new StringBuilder("Step ").append(CURRENT_STEP).append(" started for Customer: ").append(super.customer.getId())
                .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
        super.customerMigration.setIsMigrationInProgress(true);
        super.customerMigrationService.updateCustomerMigration(super.customerMigration);
        
        final List<String> serialNumbers = new ArrayList<String>();
        final List<String> names = new ArrayList<String>();
        
        final boolean allCommunicated = recalculateOnlinePaystations(serialNumbers, names);
        
        final String subject = super.reportingUtil.getPropertyEN("customermigration.email.customer.paystationcommunicationcheck.subject",
                                                                 new String[] { super.customer.getName() });
        final String subjectIT = super.reportingUtil.getPropertyEN("customermigration.email.admin.paystationcommunicationcheck.subject",
                                                                   new String[] { super.customer.getId().toString(), super.customer.getName() });
        String content = null;
        String contentIT = null;
        if (allCommunicated) {
            content = super.reportingUtil.getPropertyEN("customermigration.email.customer.paystationcommunicationcheck.content");
            contentIT = super.reportingUtil.getPropertyEN("customermigration.email.admin.paystationcommunicationcheck.content",
                                                          new String[] { super.customer.getId().toString(), super.customer.getName(), });
        } else {
            final StringBuilder itText = new StringBuilder();
            final StringBuilder customerText = new StringBuilder();
            for (int i = 0; i < serialNumbers.size(); i++) {
                itText.append(serialNumbers.get(i)).append(StandardConstants.STRING_NEXT_LINE);
                customerText.append(serialNumbers.get(i)).append(STRING_DASH_WITH_SPACES).append(names.get(i))
                        .append(StandardConstants.STRING_NEXT_LINE);
                        
            }
            
            content = super.reportingUtil.getPropertyEN("customermigration.email.customer.paystationcommunicationcheck.content.failure",
                                                        new String[] { customerText.toString() });
            contentIT = super.reportingUtil
                    .getPropertyEN("customermigration.email.admin.paystationcommunicationcheck.content.failure",
                                   new String[] { super.customer.getId().toString(), super.customer.getName(), itText.toString(), });
        }
        
        if (WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMPLETED == this.customerMigration.getCustomerMigrationStatusType().getId()) {
            this.mailerService.sendMessage(super.customerEmailAddress, subject, content, null);
            this.mailerService.sendMessage(super.itEmailAddress, subjectIT, contentIT, null);
            super.customerMigration.setCommunicationEmailSentGmt(DateUtil.getCurrentGmtDate());
            
        }
        
        final int timeDelayInHours =
                this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.CUSTOMER_MIGRATION_PAY_STATION_DELAY_IN_HOURS,
                                                                EmsPropertiesService.CUSTOMER_MIGRATION_PAY_STATION_DELAY_IN_HOURS_DEFAULT, true);
                                                                
        super.customerMigration.setBackGroundJobNextTryGmt(addDelay(Calendar.HOUR_OF_DAY, timeDelayInHours));
        super.customerMigration.setCustomerMigrationStatusType(super.customerMigrationStatusTypeService
                .findCustomerMigrationStatusType(allCommunicated ? WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_ALL_PAYSTATIONS_MIGRATED
                        : WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMMUNICATION_CHECKED));
        super.customerMigration.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
        super.customerMigration.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        super.customerMigration.setIsMigrationInProgress(false);
        super.customerMigrationService.updateCustomerMigration(super.customerMigration);
        
        LOGGER.info(new StringBuilder("Pay station communication check email send to Customer: ").append(super.customer.getId())
                .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
    }
    
    private boolean recalculateOnlinePaystations(final List<String> serialNumbers, final List<String> names) {
        final List<CustomerMigrationOnlinePOS> onlinePOSList =
                super.customerMigrationOnlinePOSService.findOnlinePOSByCustomerId(super.customer.getId());
                
        final Calendar initialHeartbeatSetTime = Calendar.getInstance();
        initialHeartbeatSetTime.setTime(super.customerMigration.getHeartbeatsResetGmt());
        
        final int heartbeatDelayMins =
                this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.CUSTOMER_MIGRATION_HEARTBEAT_DELAY_IN_MINUTES,
                                                                EmsPropertiesService.CUSTOMER_MIGRATION_HEARTBEAT_DELAY_IN_MINUTES_DEFAULT, true);
        initialHeartbeatSetTime.add(Calendar.MINUTE, heartbeatDelayMins);
        
        boolean allCommunicated = true;
        if (onlinePOSList == null || onlinePOSList.isEmpty()) {
            LOGGER.info(new StringBuilder("No online point of sales found for checking for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
        } else {
            LOGGER.info(new StringBuilder("Found ").append(onlinePOSList.size()).append(" online point of sales to validate for Customer: ")
                    .append(super.customer.getId()).append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
            if (super.customerMigration.getTotalOnlinePaystationCount() == 0) {
                super.customerMigration.setTotalOnlinePaystationCount(onlinePOSList.size());
            }
            int communicatedPaystationCount = 0;
            for (CustomerMigrationOnlinePOS onlinePOS : onlinePOSList) {
                final PosHeartbeat posHeartbeat = this.pointOfSaleService.findPosHeartbeatByPointOfSaleId(onlinePOS.getPointOfSale().getId());
                
                final StringBuilder loggedText = new StringBuilder("Point of sale:");
                loggedText.append(onlinePOS.getPointOfSale().getSerialNumber()).append(" Id:");
                if (posHeartbeat.getLastHeartbeatGmt().after(initialHeartbeatSetTime.getTime())) {
                    LOGGER.info(loggedText.append(onlinePOS.getPointOfSale().getId()).append(" communicated at ")
                            .append(posHeartbeat.getLastHeartbeatGmt()).append(" for Customer: ").append(super.customer.getId())
                            .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
                    communicatedPaystationCount++;
                } else {
                    LOGGER.info(loggedText.append(onlinePOS.getPointOfSale().getId()).append(" has not communicated for Customer: ")
                            .append(super.customer.getId()).append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
                            
                    serialNumbers.add(onlinePOS.getPointOfSale().getSerialNumber());
                    names.add(onlinePOS.getPointOfSale().getName());
                    allCommunicated = false;
                }
            }
            super.customerMigration.setCommunicatedPaystationCount(communicatedPaystationCount);
            LOGGER.info(new StringBuilder("All online point of sales checked for Customer: ").append(super.customer.getId())
                    .append(STRING_DASH_WITH_SPACES).append(super.customer.getName()));
        }
        return allCommunicated;
    }
}
