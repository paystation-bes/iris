package com.digitalpaytech.scheduling.sms.support;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class SmsDto {
    private String mobileNumber;
    private String messageBody;
    private String callBackUrl;
    private String callbackId;
    private Boolean endConversation;
    
    public SmsDto() {
    }
    
    public SmsDto(final String mobileNumber, final String messageBody, final String callBackUrl, final String callbackId, final Boolean endConversation) {
        this.mobileNumber = mobileNumber;
        this.messageBody = messageBody;
        this.callBackUrl = callBackUrl;
        this.callbackId = callbackId;
        this.endConversation = endConversation;
    }
    
    public final Boolean getEndConversation() {
        return endConversation;
    }

    public final void setEndConversation(final Boolean endConversation) {
        this.endConversation = endConversation;
    }
    public final String getMobileNumber() {
        return mobileNumber;
    }

    public final void setMobileNumber(final String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public final String getMessageBody() {
        return messageBody;
    }

    public final void setMessageBody(final String messageBody) {
        this.messageBody = messageBody;
    }

    public final String getCallBackUrl() {
        return callBackUrl;
    }

    public final void setCallBackUrl(final String callBackUrl) {
        this.callBackUrl = callBackUrl;
    }

    public final String getCallbackId() {
        return callbackId;
    }

    public final void setCallbackId(final String callbackId) {
        this.callbackId = callbackId;
    }
    
    
}
