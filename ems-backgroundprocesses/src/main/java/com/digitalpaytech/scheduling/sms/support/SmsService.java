package com.digitalpaytech.scheduling.sms.support;

@SuppressWarnings("PMD.SignatureDeclareThrowsException")
public interface SmsService {
    String sendSmsRequest(String smsJson) throws Exception;
    
    String sendReplyRequest(String callbackId, String smsJson) throws Exception;
    
    String endConversationRequest(String callbackId) throws Exception;
    
    @SuppressWarnings("PMD.UseObjectForClearerAPI")
    String createJson(String mobileNumber, String messageBody, String callbackUrl, String callbackId, Boolean endConversation);
}
