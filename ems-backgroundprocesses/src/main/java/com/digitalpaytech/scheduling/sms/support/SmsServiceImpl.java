package com.digitalpaytech.scheduling.sms.support;

import java.nio.charset.Charset;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.stereotype.Service;

import com.digitalpaytech.client.MessagingClient;
import com.digitalpaytech.ribbon.ClientFactory;
import com.digitalpaytech.util.impl.ManualRetry;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.hystrix.exception.HystrixRuntimeException;

@Service("smsService")
public class SmsServiceImpl implements SmsService {
    
    private static final Logger LOG = Logger.getLogger(SmsServiceImpl.class);
    
    @Autowired
    private ClientFactory clientFactory;
    
    @Autowired
    private ManualRetry manualRetry;
    
    @Override
    public String sendSmsRequest(final String smsJson) throws Exception {
        final MessagingClient messagingService = this.clientFactory.from(MessagingClient.class);
        return messagingService.sendMessageRequest(smsJson).execute().toString(Charset.defaultCharset());
    }
    
    @Override
    public String sendReplyRequest(final String callbackId, final String smsJson) throws Exception {
        final MessagingClient messagingService = this.clientFactory.from(MessagingClient.class);
        
        return this.manualRetry.getTemplate().execute(new RetryCallback<String, HystrixRuntimeException>() {
            
            @Override
            public String doWithRetry(final RetryContext context) throws HystrixRuntimeException {
                LOG.info("Retrying send sms reply. Retry count: " + context.getRetryCount());
                return messagingService.sendReplyRequest(callbackId, smsJson).execute().toString(Charset.defaultCharset());
            }
            
        });
        
    }
    
    @Override
    @SuppressWarnings("PMD.UseObjectForClearerAPI")
    public String createJson(final String mobileNumber, final String messageBody, final String callbackUrl, final String callbackId,
        final Boolean endConversation) {
        final SmsDto smsDto = new SmsDto();
        smsDto.setCallbackId(callbackId);
        smsDto.setMobileNumber(mobileNumber);
        smsDto.setMessageBody(messageBody);
        smsDto.setCallBackUrl(callbackUrl);
        smsDto.setEndConversation(endConversation);
        final ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(smsDto);
        } catch (JsonProcessingException e) {
            LOG.error("Unable to process JSON Mapping", e);
            return e.getMessage();
        }
    }
    
    @Override
    public String endConversationRequest(final String callbackId) {
        final MessagingClient messagingService = this.clientFactory.from(MessagingClient.class);
        return messagingService.endConversationRequest(callbackId).execute().toString(Charset.defaultCharset());
    }
    
}
