package com.digitalpaytech.scheduling.badcard.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.scheduling.badcard.LinuxBadCardListNotificationTask;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.LinkBadCardService;
import com.digitalpaytech.exception.SystemException;

@Service("linuxBadCardListNotificationTask")
@Transactional(propagation = Propagation.REQUIRED)
public class LinuxBadCardListNotificationTaskImpl implements LinuxBadCardListNotificationTask {
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private LinkBadCardService badCardService;

    @Autowired
    private MailerService mailerService;
    
    @Override
    public void checkBadCardList() {
        final Collection<Customer> customers = this.customerService.findAllChildCustomers();
        for (Customer customer : customers) {
            if (customer.isIsMigrated() && customer.getUnifiId() != null) {
                try {
                    this.badCardService.activateNotificationIfRequired(customer);
                } catch (SystemException se) {
                    this.mailerService.sendAdminErrorAlert("Link BadCardService is not available", 
                                                           "Cannot access BadCardService for customer id: " + customer.getId(), 
                                                           se);
                }
            }
        }
    }
}
