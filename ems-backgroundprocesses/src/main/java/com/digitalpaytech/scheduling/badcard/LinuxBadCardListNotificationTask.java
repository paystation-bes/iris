package com.digitalpaytech.scheduling.badcard;

public interface LinuxBadCardListNotificationTask {
    void checkBadCardList();
}
