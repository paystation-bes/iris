package com.digitalpaytech.scheduling.cps.impl;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.annotation.PostConstruct;

import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.apache.log4j.Logger;

import com.digitalpaytech.cardprocessing.CardProcessingMaster;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CPSReversalData;
import com.digitalpaytech.scheduling.cps.CPSReversalRetryService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.cps.CPSReversalDataService;
import com.digitalpaytech.service.cps.CoreCPSService;
import com.digitalpaytech.util.CardProcessingConstants;

@Service("cpsReversalRetryService")
@Transactional(propagation = Propagation.REQUIRED)
public class CPSReversalRetryServiceImpl implements CPSReversalRetryService {
    private static final Logger LOG = Logger.getLogger(CPSReversalRetryServiceImpl.class);
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private CardProcessingMaster cardProcessingMaster;
    
    @Autowired
    private CoreCPSService coreCPSService;
    
    @Autowired
    private CPSReversalDataService cpsReversalDataService;
    
    private int limit;
    private int maxRetryCount;
    
    @PostConstruct
    public final void init() {
        this.limit = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.CPS_REVERSAL_RETRY_SELECT_LIMIT,
                                                                     EmsPropertiesService.DEFAULT_CPS_REVERSAL_RETRY_SELECT_LIMIT);
        this.maxRetryCount = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.CPS_REVERSAL_RETRY_MAX_COUNT,
                                                                             EmsPropertiesService.DEFAULT_CPS_REVERSAL_RETRY_MAX_COUNT);
    }
    
    @Override
    public final void reversalRetry() {
        if (!this.cardProcessingMaster.isScheduledCardProcessingServer()) {
            return;
        }
        
        final Query query = this.entityDao.getNamedQuery("CPSReversalData.findByEarliest");
        query.setMaxResults(this.limit);
        @SuppressWarnings("unchecked")
        final List<CPSReversalData> list = query.list();
        
        int status = -1;
        final Date now = new GregorianCalendar().getTime();
        for (CPSReversalData rev : list) {
            status = this.coreCPSService.sendReversalsToCoreCPS(rev.getChargeToken(), rev.getRefundToken(), rev.getReversalType());
            if (status == CardProcessingConstants.STATUS_AUTHORIZED || this.isExpiredCPSReversals(rev)) {
                // Delete if Reversal Success or isExpired.
                LOG.info("Delete CPSReversalData record: " + rev);
                this.cpsReversalDataService.delete(rev);
            } else {
                // Update otherwise 
                rev.setCreatedGMT(now);
                rev.setRetryCount(rev.getRetryCount() + 1);
                this.cpsReversalDataService.update(rev);
            }
        }
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final void setCardProcessingMaster(final CardProcessingMaster cardProcessingMaster) {
        this.cardProcessingMaster = cardProcessingMaster;
    }
    
    public final void setCoreCPSService(final CoreCPSService coreCPSService) {
        this.coreCPSService = coreCPSService;
    }
    
    public final void setCPSReversalDataService(final CPSReversalDataService cpsReversalDataService) {
        this.cpsReversalDataService = cpsReversalDataService;
    }
    
    private boolean isExpiredCPSReversals(final CPSReversalData rev) {
        return rev != null && rev.getRetryCount() + 1 >= this.maxRetryCount;
    }
}
