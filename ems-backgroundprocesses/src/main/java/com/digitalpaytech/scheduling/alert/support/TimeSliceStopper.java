package com.digitalpaytech.scheduling.alert.support;

import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.JobListener;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.UnableToInterruptJobException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.digitalpaytech.util.StandardConstants;

@Service("TimeSliceStopper")
public class TimeSliceStopper {
    public static final String CTX_PARAM_SERVICE_CTX = "jobServiceContext";
    public static final String CTX_PARAM_SERVICE_TIME_SLICE = "timeSlice";
    
    public static final String CTX_PARAM_STOPPER = "stopper";
    public static final String CTX_PARAM_IS_INTERRUPTED = "isInterrupted";
    
    private static final Logger LOGGER = Logger.getLogger(TimeSliceStopper.class);
    
    @Autowired
    @Qualifier("actionServicesStopperScheduler")
    private Scheduler scheduler;
    
    public TimeSliceStopper() {
        
    }
    
    public final JobListener createListener(final String jobName, final int timeSlice) {
        return new StopperListener(jobName, timeSlice);
    }
    
    private void unscheduleStopperJob(final JobExecutionContext context) {
        final TriggerKey key = (TriggerKey) context.getMergedJobDataMap().get(CTX_PARAM_STOPPER);
        if (key == null) {
            LOGGER.warn("Couldn't locate Stopper's TriggerKey!");
        } else {
            try {
                this.scheduler.unscheduleJob(key);
            } catch (SchedulerException se) {
                LOGGER.warn("Unable to unschedule Job Stopper", se);
            }
        }
    }
    
    private class StopperListener implements JobListener {
        private String name;
        private int timeSlice;
        
        public StopperListener(final String jobName, final int timeSlice) {
            this.name = jobName + "_Listener";
            this.timeSlice = timeSlice;
        }
        
        @Override
        public String getName() {
            return this.name;
        }
        
        @Override
        public void jobToBeExecuted(final JobExecutionContext context) {
            final long timestamp = System.currentTimeMillis();
            
            final JobKey targetJob = context.getJobDetail().getKey();
            final String stopperJobName = targetJob.getName() + "_Stopper_" + timestamp;
            final String group = targetJob.getGroup();
            
            final JobDetail stopJobDtl = JobBuilder.newJob(JobStopper.class).withIdentity(stopperJobName, targetJob.getGroup()).build();
            final Trigger stopTrigger = TriggerBuilder.newTrigger().withIdentity(stopperJobName + "_Trigger", group)
                    .startAt(new Date(System.currentTimeMillis() + (this.timeSlice * StandardConstants.SECONDS_IN_MILLIS_1))).build();
            
            // Adding Scheduler's Context to Stopper's Context.
            stopTrigger.getJobDataMap().put(CTX_PARAM_SERVICE_CTX, context);
            try {
                scheduler.scheduleJob(stopJobDtl, stopTrigger);
                if (LOGGER.isTraceEnabled()) {
                    LOGGER.trace("Scheduled Job Stopper for job: " + targetJob.getGroup() + "." + targetJob.getName());
                }
            } catch (SchedulerException se) {
                LOGGER.warn("Unable to schedule Scheduler Stopper", se);
            }
            
            // Adding Stopper's Identity to Scheduler's Context.
            context.getMergedJobDataMap().put(CTX_PARAM_STOPPER, stopTrigger.getKey());
        }
        
        @Override
        public void jobExecutionVetoed(final JobExecutionContext context) {
            // TODO Auto-generated method stub
            if (LOGGER.isDebugEnabled()) {
                final JobKey jobKey = context.getJobDetail().getKey();
                LOGGER.debug("Job " + jobKey.getGroup() + "." + jobKey.getName() + " has been vetoed!");
            }
        }
        
        @Override
        public void jobWasExecuted(final JobExecutionContext context, final JobExecutionException jobException) {
            final boolean isInterrupted = context.getMergedJobDataMap().getBoolean(CTX_PARAM_IS_INTERRUPTED);
            final JobKey jobKey = context.getJobDetail().getKey();
            if (!isInterrupted) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Job " + jobKey.getGroup() + "." + jobKey.getName() + " has been completed, unscheduling its' Job Stopper...");
                }
                
                unscheduleStopperJob(context);
            }
        }
    }
    
    public static class JobStopper implements Job {
        public JobStopper() {
            
        }
        
        @Override
        public final void execute(final JobExecutionContext context) throws JobExecutionException {
            final JobExecutionContext targetContext = (JobExecutionContext) context.getMergedJobDataMap().get(CTX_PARAM_SERVICE_CTX);
            if (targetContext != null) {
                final JobKey jobKey = targetContext.getJobDetail().getKey();
                final String jobId = jobKey.getGroup() + "." + jobKey.getName();
                try {
                    if (LOGGER.isTraceEnabled()) {
                        LOGGER.trace("Trying to interupt Job: " + jobId);
                    }
                    
                    targetContext.getMergedJobDataMap().put(CTX_PARAM_IS_INTERRUPTED, true);
                    targetContext.getScheduler().interrupt(jobKey);
                } catch (UnableToInterruptJobException utij) {
                    LOGGER.warn("Could not stop job: " + jobId, utij);
                } catch (Exception e) {
                    LOGGER.error("Error while stopping job: " + jobId, e);
                }
            }
        }
        
    }
}
