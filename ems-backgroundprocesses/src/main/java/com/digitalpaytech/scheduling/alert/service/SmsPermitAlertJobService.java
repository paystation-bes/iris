package com.digitalpaytech.scheduling.alert.service;

import java.util.concurrent.ConcurrentLinkedQueue;

import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.dto.SmsAlertInfo;
import com.digitalpaytech.dto.SmsRateInfo;
import com.digitalpaytech.scheduling.alert.job.SmsPermitAlertJob;
import com.digitalpaytech.scheduling.sms.support.SmsService;

public interface SmsPermitAlertJobService extends ActionJobService {
       
    int AURA_REQ_4006 = 4006;
    int AURA_REQ_4010 = 4010;
    int AURA_REQ_4011 = 4011;
    int AURA_REQ_4015 = 4015;
    int AURA_REQ_4025 = 4025;
    int AURA_REQ_4030 = 4030;
    int AURA_REQ_4054 = 4054;
    int AURA_REQ_4057 = 4057;
    int AURA_REQ_4058 = 4058;
    int AURA_REQ_4072 = 4072;
    int AURA_REQ_4073 = 4073;
    int AURA_REQ_4074 = 4074;
    int AURA_REQ_4075 = 4075;
    int AURA_REQ_4082 = 4082;
    int AURA_REQ_4083 = 4083;
    
    /**
     * REQ. 4006, 4010
     */
    String AURA_SMS_REQ_4006_4010_CONTENT_TEMPLATE = "label.ebp.4006-4010.1";
    /**
     * REQ. 4006, 4010<br/>
     * 
     * 1) rate1 and rate2 are same rate <br/>
     * or <br/>
     * 2) rate1 and rate2 are merged
     */
    String AURA_SMS_REQ_4006_4010_CONTENT_TEMPLATE_2 = "label.ebp.4006-4010.2";
    
    /**
     * second rate free parking <br/>
     * 
     * Free Pkg (after startRate2Time)
     */
    String AURA_SMS_REQ_4006_4010_CONTENT_TEMPLATE_3 = "label.ebp.4006-4010.3";
    
    /**
     * REQ. 4006, 4010, 4011 <br/>
     * 
     * 1)single value + 2nd NO PARKING <br/>
     * 2)single value + free parking <br/>
     */
    String AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE = "label.ebp.4006-4010-4011.1";
    
    /**
     * 1)single value + 2nd NO PARKING <br/>
     * 2)single value + free parking <br/>
     * 
     * Free Pkg (after startRate2Time)
     * NO PARKING (after endRate1Time)
     */
    String AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE_2 = "label.ebp.4006-4010-4011.2";
    
    /**
     * REQ. 4006, 4010, 4011, 4030 <br/>
     * Free parking expires
     * 1)single value + 2nd NO PARKING <br/>
     * 2)single value + free parking <br/>
     */
    String AURA_SMS_REQ_4006_4010_4011_4030_CONTENT_TEMPLATE = "label.ebp.4006-4010-4011-4030.1";
    
    /**
     * REQ. 4006, 4010, 4011 -- single value + 1 rates
     */
    String AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE_3 = "label.ebp.4006-4010-4011.3";
    
    /**
     * REQ. 4006, 4010, 4011, 4030 -- single value + 1 rates + after free parking
     */
    String AURA_SMS_REQ_4006_4010_4011_4030_CONTENT_TEMPLATE_3 = "label.ebp.4006-4010-4011-4030.2";
    
    /**
     * REQ. 4015
     */
    String AURA_SMS_REQ_4015_CONTENT_TEMPLATE = "label.ebp.4015.1";
    
    String AURA_SMS_REQ_4015_CONTENT_TEMPLATE_2 = "label.ebp.4015.2";
    
    /**
     * REQ. 4025
     */
    String AURA_SMS_REQ_4025_CONTENT_TEMPLATE = "label.ebp.4025.1";
    
    /**
     * REQ. 4030, 4010<br/>
     * 
     * no rate2 message display
     */
    String AURA_SMS_REQ_4030_4010_CONTENT_TEMPLATE_2 = "label.ebp.4030-4010.2";
    
    /**
     * REQ. 4054
     */
    String AURA_SMS_REQ_4054_CONTENT_TEMPLATE = "label.ebp.4054.1";
    /**
     * REQ. 4057
     */
    String AURA_SMS_REQ_4057_CONTENT_TEMPLATE = "label.ebp.4057.1";
    /**
     * REQ. 4058
     */
    String AURA_SMS_REQ_4058_CONTENT_TEMPLATE = "label.ebp.4058.1";
    /**
     * REQ. 4072
     */
    String AURA_SMS_REQ_4072_CONTENT_TEMPLATE = "label.ebp.4072.1";
    /**
     * REQ. 4073
     */
    String AURA_SMS_REQ_4073_CONTENT_TEMPLATE = "label.ebp.4073.1";
    /**
     * REQ. 4074
     */
    String AURA_SMS_REQ_4074_CONTENT_TEMPLATE = "label.ebp.4074.1";
    /**
     * REQ. 4075
     */
    String AURA_SMS_REQ_4075_CONTENT_TEMPLATE = "label.ebp.4075.1";
    /**
     * REQ. 4082
     */
    String AURA_SMS_REQ_4082_CONTENT_TEMPLATE = "label.ebp.4082.1";
    
    /**
     * REQ. 4082 one time specified.
     */
    String AURA_SMS_REQ_4082_CONTENT_TEMPLATE1 = "label.ebp.4082.2";
    
    /**
     * REQ. 4030, 4010
     */
    String AURA_SMS_REQ_4030_4010_CONTENT_TEMPLATE = "label.ebp.4030-4010.1";
    
    /**
     * REQ. 4083
     */
    String AURA_SMS_REQ_4083_CONTENT_TEMPLATE = "label.ebp.4083.1";
    
    String AURA_SMS_MESSAGE_FREE_PKG = "label.ebp.free.Parking";
    
    int retrieveSMSAlertFromDB();
    
    void manageExtensiblePermit(SmsAlertInfo alertInfo);
    
    void sendSMS(SmsPermitAlertJob job);

    void sendSMS(SmsRateInfo rateInfo, SmsAlertInfo alertInfo, String smsMessage, Permit permit, SmsService client);

    void sendConfirmationSMS(SmsRateInfo smsRateInfo, SmsAlertInfo smsAlertInfo, Integer messageType, String smsMessage, String userReply,
        Permit permit, Boolean isFreeParkingExtended, SmsService client);
    
    void clearSMSAlertPushQueue();
    
    void clearExpiredAlertAndPermit();
    
    int getSleepTime();
    
    void notifyMoreDataComing();
    
    void notifyMoreDataNeeding();
    
    ConcurrentLinkedQueue<SmsAlertInfo> getSMSPermitAlertQueue();
    
    SmsAlertInfo getNextSMSPermitAlertFromQueue();

    boolean isNeedPause();
    
    String buildSMSMessageByRequest(SmsRateInfo rateInfo, SmsAlertInfo alertInfo, int requestType, int minutesToAdd);
    
    SmsAlertInfo getSmsAlertByParkerReply(String mobileNumber);
    
    void logSmsTransaction(SmsAlertInfo smsAlertInfo, int logType, String userResponse);
    
    void confirmExtensiblePermit(SmsAlertInfo smsAlertInfo, SmsRateInfo smsRateInfo, int messageType, String userReply, Permit originalPermit,
        Permit permit, ProcessorTransaction chargePt, PaymentCard paymentCard);
    
    void processPostConfirmation(int messageType, SmsAlertInfo smsAlertInfo, String userReply, Permit permit, boolean isFreeParkingExtended);

}
