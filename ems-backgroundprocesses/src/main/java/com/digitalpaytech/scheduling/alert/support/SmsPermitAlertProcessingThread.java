package com.digitalpaytech.scheduling.alert.support;

import org.apache.log4j.Logger;

import com.digitalpaytech.dto.SmsAlertInfo;
import com.digitalpaytech.scheduling.alert.service.SmsPermitAlertJobService;
import com.digitalpaytech.util.EMSThread;

public class SmsPermitAlertProcessingThread extends EMSThread {
    
    private static final Logger LOGGER = Logger.getLogger(SmsPermitAlertProcessingThread.class);
    private int threadId;
    private SmsPermitAlertJobService smsPermitAlertJobService;
    
    public SmsPermitAlertProcessingThread(final int threadId, final SmsPermitAlertJobService smsPermitAlertJobService) {
        super(SmsPermitAlertProcessingThread.class.getName());
        this.smsPermitAlertJobService = smsPermitAlertJobService;
        setPriority(Thread.NORM_PRIORITY);
        this.threadId = threadId;
    }
    
    public final void run() {
        //        logger.info("+++ SmsPermitAlertProcessingThread " + threadId + " startup +++");
        while (isRunning()) {
            //            logger.debug("+++  SmsPermitAlertProcessingThread " + threadId + " wakes up +++");
            try {
                if (this.smsPermitAlertJobService.isNeedPause()) {
                    //                    logger.info("+++ not the scheduled time for SmsPermitAlertProcessingThread " + this.threadId + ", wait +++");
                    synchronized (this) {
                        wait();
                    }
                } else {
                    //                    logger.info("+++ SmsPermitAlertProcessingThread " + this.threadId + " processing sms alert +++");
                    
                    if (!this.smsPermitAlertJobService.getSMSPermitAlertQueue().isEmpty()) {
                        final SmsAlertInfo alertInfo = this.smsPermitAlertJobService.getNextSMSPermitAlertFromQueue();
                        
                        if (alertInfo == null) {
                            synchronized (this) {
                                // logger.info("+++ SmsPermitAlertProcessingThread " + this.threadId + ": No sms alert in the queue, waiting +++");
                                wait();
                            }
                        } else {
                            // logger.info("+++ SmsPermitAlertProcessingThread " + this.threadId + " is trying to send SMS alert +++");
                            this.smsPermitAlertJobService.manageExtensiblePermit(alertInfo);
                        }
                    } else {
                        synchronized (this) {
                            // logger.info("+++ SmsPermitAlertProcessingThread " + this.threadId + ": No sms alert in the queue, waiting +++");
                            wait();
                        }
                    }
                }
            } catch (InterruptedException e) {
                LOGGER.warn("+++ SmsPermitAlertProcessingThread " + this.threadId + " interrupted.", e);
            } catch (Exception e) {
                LOGGER.error("++++ EXCEPTION in SmsPermitAlertProcessingThread ++++", e);
            }
        }
        LOGGER.warn("!!! SmsPermitAlertProcessingThread " + this.threadId + " stopped !!!");
    }
    
    public final void wakeup() {
        notify();
    }
    
    private boolean isRunning() {
        return !isShutdown();
    }
}
