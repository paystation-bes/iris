package com.digitalpaytech.scheduling.alert.support;

import java.util.Vector;

public class ActionService {
    private String type;
    private int dataLoadingRows;
    private int numOfThreads;
    private int sleepTime;
    private Vector<ServerInfo> servers;
    
    public ActionService() {
        this.servers = new Vector<ServerInfo>();
    }
    
    public final void setType(final String type) {
        this.type = type;
    }
    
    public final String getType() {
        return this.type;
    }
    
    public final void setDataLoadingRows(final String dataLoadingRows) {
        this.dataLoadingRows = Integer.parseInt(dataLoadingRows);
    }
    
    public final int getDataLoadingRows() {
        return this.dataLoadingRows;
    }
    
    public final void addServerInfo(final ServerInfo server) {
        this.servers.add(server);
    }
    
    public final Vector<ServerInfo> getServerInfo() {
        return this.servers;
    }
    
    public final void setServerInfo(final Vector<ServerInfo> servers) {
        this.servers = servers;
    }
    
    public final void setNumOfThreads(final String numOfThreads) {
        this.numOfThreads = Integer.parseInt(numOfThreads);
    }
    
    public final int getNumOfThreads() {
        return this.numOfThreads;
    }
    
    public final void setSleepTime(final String sleepTime) {
        this.sleepTime = Integer.parseInt(sleepTime);
    }
    
    public final int getSleepTime() {
        return this.sleepTime;
    }
    
    public final String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("{").append(this.type).append(".").append(this.dataLoadingRows).append(".").append(this.numOfThreads).append(".")
                .append(this.sleepTime).append(".");
        for (ServerInfo server : this.servers) {
            sb.append(server.toString());
        }
        sb.append(")");
        return sb.toString();
    }
}
