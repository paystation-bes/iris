package com.digitalpaytech.scheduling.alert.support;

public class ServerInfo {
    private String name;
    private String cronExpression;
    private int runningTime;
    
    public ServerInfo() {
    }
    
    public final void setName(final String serverName) {
        this.name = serverName;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final void setCronExpression(final String cronExpression) {
        this.cronExpression = cronExpression;
    }
    
    public final String getCronExpression() {
        return this.cronExpression;
    }
    
    public final void setRunningTime(final String runningTime) {
        this.runningTime = Integer.parseInt(runningTime);
    }
    
    public final int getRunningTime() {
        return this.runningTime;
    }
    
    public final String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("[").append(this.name).append(".").append(this.cronExpression).append(".").append(this.runningTime).append("]");
        return sb.toString();
    }
}
