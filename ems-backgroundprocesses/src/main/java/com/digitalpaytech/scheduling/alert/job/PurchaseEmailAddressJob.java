package com.digitalpaytech.scheduling.alert.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.digitalpaytech.scheduling.alert.service.ActionJobService;
import com.digitalpaytech.scheduling.alert.service.PurchaseEmailAddressJobService;
import com.digitalpaytech.util.StandardConstants;

public class PurchaseEmailAddressJob extends BaseInterruptableJob {
    
    private PurchaseEmailAddressJobService purchaseEmailAddressJobService;
    
    @Override
    public final void execute(final JobExecutionContext context) throws JobExecutionException {
        super.execute(context);
        this.purchaseEmailAddressJobService = (PurchaseEmailAddressJobService) context.getJobDetail().getJobDataMap()
                .get(ActionJobService.JOB_SERVICE_CLASS);
        while (!this.interrupted) {
            if (!this.purchaseEmailAddressJobService.isRunning()) {
                this.purchaseEmailAddressJobService.retrievePurchaseEmailAddressFromDB();
                this.purchaseEmailAddressJobService.sendReceipt(this);
            } else {
                try {
                    if (LOGGER.isTraceEnabled()) {
                        LOGGER.trace(new StringBuilder("+++ Receipt sending already runnning, sleep 1 second"));
                    }
                    
                    Thread.sleep(StandardConstants.SECONDS_IN_MILLIS_1);
                } catch (InterruptedException e) {
                }
            }
            if (this.interrupted) {
                this.purchaseEmailAddressJobService.setRunning(false);
                return;
            }
        }
    }
    
    @Override
    public final void clearData() {
        if (this.purchaseEmailAddressJobService != null) {
            this.purchaseEmailAddressJobService.clearReceiptPushQueue();
        }
    }
    
}
