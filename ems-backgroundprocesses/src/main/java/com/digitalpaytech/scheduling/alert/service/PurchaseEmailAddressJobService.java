package com.digitalpaytech.scheduling.alert.service;

import com.digitalpaytech.scheduling.alert.job.PurchaseEmailAddressJob;

public interface PurchaseEmailAddressJobService extends ActionJobService {
    
    void retrievePurchaseEmailAddressFromDB();
    
    void sendReceipt(PurchaseEmailAddressJob purchaseEmailAddressJob);
    
    void clearReceiptPushQueue();
    
}
