package com.digitalpaytech.scheduling.alert.support;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.UnableToInterruptJobException;

import com.digitalpaytech.util.StandardConstants;

public class ActionServiceStopper extends Thread {
    protected static final Logger LOGGER = Logger.getLogger(ActionServiceStopper.class);
    private JobExecutionContext context;
    private int timeInterval;
    
    public ActionServiceStopper(final JobExecutionContext context, final int timeInterval) {
        super();
        this.context = context;
        this.timeInterval = timeInterval;
    }
    
    public final void run() {
        try {
            sleep(this.timeInterval * StandardConstants.SECONDS_IN_MILLIS_1);
            this.context.getScheduler().interrupt(this.context.getJobDetail().getKey());
        } catch (UnableToInterruptJobException e) {
            LOGGER.warn("UnableToInterruptJobException: ", e);
        } catch (InterruptedException e) {
            // logger.warn("InterruptedException: ", e);
        }
    }
}
