package com.digitalpaytech.scheduling.alert.service;

public interface DuplicateSerialNumberJobService extends ActionJobService {
    
    void retrieveRecoveryAlertFromDB();
    
}
