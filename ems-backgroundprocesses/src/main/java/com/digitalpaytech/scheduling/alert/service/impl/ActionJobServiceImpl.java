package com.digitalpaytech.scheduling.alert.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.digester.Digester;
import org.apache.log4j.Logger;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerBuilder;
import org.quartz.impl.matchers.KeyMatcher;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.SAXException;

import com.digitalpaytech.scheduling.alert.service.ActionJobService;
import com.digitalpaytech.scheduling.alert.support.ActionService;
import com.digitalpaytech.scheduling.alert.support.ActionServices;
import com.digitalpaytech.scheduling.alert.support.ServerInfo;
import com.digitalpaytech.scheduling.alert.support.TimeSliceStopper;
import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.PurchaseEmailAddressService;
import com.digitalpaytech.service.queue.QueueCustomerAlertTypeService;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.xml.XMLRulesDriver;

@Service("actionJobService")
@Transactional(propagation = Propagation.SUPPORTS)
public class ActionJobServiceImpl implements ActionJobService, ApplicationContextAware, BeanNameAware {
    protected final Logger logger = Logger.getLogger(this.getClass());
    
    @Autowired
    protected XMLRulesDriver xmlRulesDriver;
    
    @Autowired
    protected ClusterMemberService clusterMemberService;
    
    @Autowired
    protected PurchaseEmailAddressService purchaseEmailAddressService;
    
    @Autowired
    @Qualifier("actionServicesConfiguration")
    protected Resource configuration;
    
    protected String actionServiceFile = "actionServices.xml";
    
    protected ActionServices actionServices;
    
    protected ActionService actionService;
    
    @Autowired
    @Qualifier("actionServicesScheduler")
    private Scheduler scheduler;
    
    @Autowired
    private TimeSliceStopper stopper;
    
    @Autowired
    @Qualifier("kafkaCustomerAlertTypeService")
    private QueueCustomerAlertTypeService queueCustomerAlertTypeService;
    
    private ApplicationContext context;
    private String beanName;
    
    private boolean isRunning;
    private boolean isRunningRecovery;
    
    public final void setClusterMemberService(final ClusterMemberService clusterMemberService) {
        this.clusterMemberService = clusterMemberService;
    }
    
    public final void setXmlRulesDriver(final XMLRulesDriver xmlRulesDriver) {
        this.xmlRulesDriver = xmlRulesDriver;
    }
    
    @PostConstruct
    private void init() {
        this.logger.info("++++++ Initialize ActionJobService ++++++");
        try {
            final Digester parser = this.xmlRulesDriver.getActionServicesParser();
            
            final InputStream configStream = getConfigStream();
            
            this.actionServices = (ActionServices) parser.parse(configStream);
            this.logger.debug("+++ actionservices size: " + this.actionServices.getServices().size());
            if (this.logger.isDebugEnabled()) {
                final Vector<ActionService> v = this.actionServices.getServices();
                for (ActionService service : v) {
                    this.logger.debug("+++ action service: " + service.toString());
                }
            }
            
            runService();
        } catch (IOException e) {
            this.logger.error("IOException: ", e);
        } catch (SAXException e) {
            this.logger.error("SAXException: ", e);
        }
    }
    
    private InputStream getConfigStream() {        
        InputStream configStream = null;
        
        try {
            
            if (this.configuration.exists()) {
                configStream = this.configuration.getInputStream();
                if (this.logger.isInfoEnabled()) {
                    if (configStream != null) {
                        this.logger.info("Load actionServices.xml has been loaded via spring injection from: " + this.configuration.getURL());
                    }
                }
            }
            
            if (configStream == null) {
                configStream = this.getClass().getClassLoader().getResourceAsStream(this.actionServiceFile);
                if (this.logger.isInfoEnabled()) {
                    if (configStream != null) {
                        this.logger.info("Load actionServices.xml manually from: "
                                         + this.getClass().getClassLoader().getResource(this.actionServiceFile));
                    }
                }
            }
            
        } catch (IOException e) {
            this.logger.error("IOException: ", e);
        }
        
        return configStream;
    }
    
    @PreDestroy
    private void destroy() {
        this.logger.info("++++++ Destroy ActionJobService ++++++");
    }
    
    protected final ActionService getActionService(final String serviceName) {
        ActionService as = null;
        final String serverName = this.clusterMemberService.getClusterName();
        Vector<ServerInfo> servers;
        for (ActionService service : this.actionServices.getServices()) {
            if (service.getType().trim().equals(serviceName)) {
                servers = new Vector<ServerInfo>();
                as = new ActionService();
                as.setType(service.getType());
                as.setDataLoadingRows(String.valueOf(service.getDataLoadingRows()));
                as.setNumOfThreads(String.valueOf(service.getNumOfThreads()));
                as.setSleepTime(String.valueOf(service.getSleepTime()));
                for (ServerInfo server : service.getServerInfo()) {
                    if (server.getName().trim().equals(serverName)) {
                        servers.add(server);
                    }
                }
                as.setServerInfo(servers);
                break;
            }
        }
        
        /*
         * if (this.logger.isDebugEnabled()) {
         * if (actionService != null) {
         * this.logger.debug("+++ action service: " + actionService.toString());
         * } else {
         * this.logger.debug("+++ action service(" + serviceName + ") is null +++");
         * }
         * }
         */
        
        return as;
    }
    
    protected final void scheduleJob(final Class<? extends Job> jobClass, final String jobId, final String group) throws SchedulerException {
        if (this.scheduler.isShutdown()) {
            this.logger.warn("!!!! Scheduler is shutdown, job cannot be executed !!!!!");
        } else {
            final ServerInfo serverInfo = this.actionService.getServerInfo().get(0);
            
            final JobDetail jobDtl = JobBuilder.newJob(jobClass).withIdentity(jobId, group).build();
            jobDtl.getJobDataMap().put(JOB_SERVICE_CLASS, this.context.getBean(this.beanName));
            
            final CronScheduleBuilder schedBuilder = CronScheduleBuilder.cronSchedule(serverInfo.getCronExpression());
            //            schedBuilder = schedBuilder.withMisfireHandlingInstructionDoNothing();
            
            final CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity(jobId + "_Trigger", group).withSchedule(schedBuilder).build();
            
            this.scheduler.scheduleJob(jobDtl, trigger);
            this.scheduler.getListenerManager().addJobListener(this.stopper.createListener(jobId, serverInfo.getRunningTime()),
                                                               KeyMatcher.keyEquals(jobDtl.getKey()));
            
            if (this.logger.isInfoEnabled()) {
                this.logger.info("Scheduled " + group + "." + jobId + " using Scheduler: " + this.scheduler.getSchedulerName() + "["
                                 + this.scheduler.getSchedulerInstanceId() + "]: " + serverInfo.getCronExpression());
            }
        }
    }
    
    protected final void scheduleCommunicationQueue(final Integer limit) {
        this.queueCustomerAlertTypeService.addCustomerAlertTypeEventWithLimit(WebCoreConstants.ALERT_THRESHOLD_TYPE_LAST_SEEN_INTERVAL_HOUR, limit);
    }
    
    public void runService() {
        
    }
    
    public void stopService() {
        
    }
    
    @Override
    public final void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
    
    @Override
    public final void setBeanName(final String name) {
        this.beanName = name;
    }
    
    @Override
    public final boolean isRunning() {
        return this.isRunning;
    }
    
    @Override
    public final void setRunning(final boolean running) {
        this.isRunning = running;
    }
    
    @Override
    public final boolean isRunningRecovery() {
        return this.isRunningRecovery;
    }
    
    @Override
    public final void setRunningRecovery(final boolean runningRecovery) {
        this.isRunningRecovery = runningRecovery;
    }
    
    protected final boolean isConfiguredServer(final String serviceName) {        
        if (this.actionService == null) {
            this.actionService = this.getActionService(serviceName); 
        }
        final Vector<ServerInfo> serverInfos = this.actionService.getServerInfo();
        final String clusterName = this.clusterMemberService.getClusterName();        
        for (ServerInfo serverInfo : serverInfos) {
            if (clusterName.equalsIgnoreCase(serverInfo.getName())) {
                return true;
            }
        }        
        return false;
    }    
    
}
