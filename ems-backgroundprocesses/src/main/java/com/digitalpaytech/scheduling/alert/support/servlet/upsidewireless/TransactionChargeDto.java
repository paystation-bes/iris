package com.digitalpaytech.scheduling.alert.support.servlet.upsidewireless;

import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.dto.SmsAlertInfo;
import com.digitalpaytech.dto.SmsRateInfo;

public class TransactionChargeDto {
    
    private String senderReply;
    private SmsAlertInfo smsAlertInfo;
    private SmsRateInfo rateInfo;
    private Permit originalPermit;
    private Purchase originalPurchase;
    private String senderMobileNumber;
    private String authorizationNumber;
    
    public TransactionChargeDto(final String senderReply, final SmsAlertInfo smsAlertInfo, final SmsRateInfo rateInfo, final Permit originalPermit,
            final Purchase originalPurchase, final String senderMobileNumber, final String authorizationNumber) {
        super();
        this.senderReply = senderReply;
        this.smsAlertInfo = smsAlertInfo;
        this.rateInfo = rateInfo;
        this.originalPermit = originalPermit;
        this.originalPurchase = originalPurchase;
        this.senderMobileNumber = senderMobileNumber;
        this.authorizationNumber = authorizationNumber;
    }
    
    public String getSenderReply() {
        return this.senderReply;
    }
    
    public void setSenderReply(final String senderReply) {
        this.senderReply = senderReply;
    }
    
    public SmsAlertInfo getSmsAlertInfo() {
        return this.smsAlertInfo;
    }
    
    public void setSmsAlertInfo(final SmsAlertInfo smsAlertInfo) {
        this.smsAlertInfo = smsAlertInfo;
    }
    
    public SmsRateInfo getRateInfo() {
        return this.rateInfo;
    }
    
    public void setRateInfo(final SmsRateInfo rateInfo) {
        this.rateInfo = rateInfo;
    }
    
    public Permit getOriginalPermit() {
        return this.originalPermit;
    }
    
    public void setOriginalPermit(final Permit originalPermit) {
        this.originalPermit = originalPermit;
    }
    
    public Purchase getOriginalPurchase() {
        return this.originalPurchase;
    }
    
    public void setOriginalPurchase(final Purchase originalPurchase) {
        this.originalPurchase = originalPurchase;
    }
    
    public String getSenderMobileNumber() {
        return this.senderMobileNumber;
    }
    
    public void setSenderMobileNumber(final String senderMobileNumber) {
        this.senderMobileNumber = senderMobileNumber;
    }
    
    public String getAuthorizationNumber() {
        return this.authorizationNumber;
    }
    
    public void setAuthorizationNumber(final String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }
    
}
