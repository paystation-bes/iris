package com.digitalpaytech.scheduling.alert.service.impl;

import org.quartz.SchedulerException;
import org.springframework.stereotype.Service;

import com.digitalpaytech.scheduling.alert.job.CommunicationAlertJob;
import com.digitalpaytech.scheduling.alert.service.CommunicationAlertJobService;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Service("communicationAlertJobService")
public class CommunicationAlertJobServiceImpl extends ActionJobServiceImpl implements CommunicationAlertJobService {
    
    private static final String FAIL_ALERT = "CommunicationFail";
    private static final String GROUP = "Communication";
    
    @Override
    public final void runService() {
        try {
            actionService = this.getActionService(WebCoreConstants.ACTION_SERVICE_COMMUNICATION);
            if ((actionService == null) || (actionService.getServerInfo().size() == 0)) {
                logger.info("+++ no actions in this server +++");
            } else {
                scheduleJob(CommunicationAlertJob.class, FAIL_ALERT, GROUP);
            }
        } catch (SchedulerException e) {
            logger.error("SchedulerException: ", e);
        }
    }
    
    public final void scheduleCommunicationQueue() {
        scheduleCommunicationQueue(actionService.getDataLoadingRows());
        try {
            Thread.sleep(StandardConstants.SECONDS_IN_MILLIS_1 * actionService.getSleepTime());
        } catch (InterruptedException ie) {
            //don't do anything
        }
    }
    
}
