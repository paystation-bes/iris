package com.digitalpaytech.scheduling.alert.service.impl;

import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.commons.lang.StringEscapeUtils;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.ActivePermit;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.SmsAlert;
import com.digitalpaytech.domain.SmsMessageType;
import com.digitalpaytech.domain.SmsTransactionLog;
import com.digitalpaytech.domain.ThirdPartyServiceAccount;
import com.digitalpaytech.dto.SmsAlertInfo;
import com.digitalpaytech.dto.SmsRateInfo;
import com.digitalpaytech.scheduling.alert.job.SmsPermitAlertJob;
import com.digitalpaytech.scheduling.alert.service.SmsPermitAlertJobService;
import com.digitalpaytech.scheduling.alert.support.SmsPermitAlertExtractionThread;
import com.digitalpaytech.scheduling.alert.support.SmsPermitAlertProcessingThread;
import com.digitalpaytech.scheduling.sms.support.SmsService;
import com.digitalpaytech.service.ActivePermitService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.ExtensiblePermitService;
import com.digitalpaytech.service.ExtensibleRateAndPermissionService;
import com.digitalpaytech.service.MobileNumberService;
import com.digitalpaytech.service.PaymentCardService;
import com.digitalpaytech.service.PermitService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.service.PurchaseService;
import com.digitalpaytech.service.SmsAlertService;
import com.digitalpaytech.service.SmsTransactionLogService;
import com.digitalpaytech.service.ThirdPartyServiceAccountService;
import com.digitalpaytech.service.kpi.KPIListenerService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Service("smsPermitAlertJobService")
@SuppressWarnings({ "checkstyle:illegalcatch", "PMD.ExcessiveImports", "PMD.GodClass", "PMD.TooManyFields", "PMD.TooManyMethods" })
public class SmsPermitAlertJobServiceImpl extends ActionJobServiceImpl implements SmsPermitAlertJobService, MessageSourceAware {
    private static final String SEND_SMS_METHOD_NAME = "sendSMS";
    private static final DecimalFormat DOLLAR_FORMATTER = new DecimalFormat("#0.00");
    private static final DecimalFormat HOUR_MINUTE_FORMATTER = new DecimalFormat("00");
    private static final String PATH_URL = "/receiveSMSPostCallback.htm";
    private static final String SMS_PERMIT_ALERT = "SmsPermitAlert";
    private static final String GROUP = "SmsPermit";
    private final ConcurrentLinkedQueue<SmsAlertInfo> smsPermitAlertQueue = new ConcurrentLinkedQueue<SmsAlertInfo>();
    
    private SmsPermitAlertProcessingThread[] smsPermitAlertProcessingThreads;
    private SmsPermitAlertExtractionThread smsPermitAlertExtractionThread;
    private SmsPermitAlertJob job;
    
    private MessageSourceAccessor messageAccessor;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private ExtensiblePermitService extensiblePermitService;
    
    @Autowired
    private ThirdPartyServiceAccountService thirdPartyServiceAccountService;
    
    @Autowired
    private SmsTransactionLogService smsTransactionLogService;
    
    @Autowired
    private SmsAlertService smsAlertService;
    
    @Autowired
    private PermitService permitService;
    
    @Autowired
    private ExtensibleRateAndPermissionService extensibleRateAndPermissionService;
    
    @Autowired
    private MobileNumberService mobileNumberService;
    
    @Autowired
    private PurchaseService purchaseService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private KPIListenerService kpiListenerService;
    
    @Autowired
    private ProcessorTransactionService processorTransactionService;
    
    @Autowired
    private ActivePermitService activePermitService;
    
    @Autowired
    private PaymentCardService paymentCardService;
    
    @Autowired
    private SmsService smsService;
    
    public final MessageSourceAccessor getMessageAccessor() {
        return this.messageAccessor;
    }
    
    public final void setMessageAccessor(final MessageSourceAccessor messageAccessor) {
        this.messageAccessor = messageAccessor;
    }
    
    @Override
    public final void setMessageSource(final MessageSource messageSource) {
        this.messageAccessor = new MessageSourceAccessor(messageSource);
    }
    
    @Override
    public final void runService() {
        try {
            actionService = this.getActionService(WebCoreConstants.ACTION_SERVICE_SMS_PERMIT);
            if (actionService == null || actionService.getServerInfo().size() == 0) {
                logger.info("+++ no actions in this server +++");
            } else {
                scheduleJob(SmsPermitAlertJob.class, SMS_PERMIT_ALERT, GROUP);
                initSMSPermitThread();
            }
        } catch (SchedulerException e) {
            logger.error("SchedulerException: ", e);
        }
    }
    
    public final void clearSMSAlertPushQueue() {
        logger.trace("+++ clearAlertPushQueue method called +++");
        this.smsPermitAlertQueue.clear();
    }
    
    public final void clearExpiredAlertAndPermit() {
        logger.trace("+++ clearExpiredAlertAndPermit method called +++");
        int count = 0;
        count = this.extensiblePermitService.deleteExpiredSmsAlert();
        if (logger.isInfoEnabled() && count > 0) {
            logger.info("+++ removed total expired sms alerts: " + count + " +++");
        }
        
        count = this.extensiblePermitService.deleteExpiredEMSExtensiblePermit();
        if (logger.isInfoEnabled() && count > 0) {
            logger.info("+++ removed total expired ems extensible permits: " + count + " +++");
        }
    }
    
    public final int retrieveSMSAlertFromDB() {
        if (logger.isTraceEnabled()) {
            logger.trace("+++ retrieveSMSAlertFromDB method called [loading " + actionService.getDataLoadingRows() + " rows] +++");
        }
        
        final Collection<SmsAlertInfo> alerts = this.extensiblePermitService.loadSmsAlertFromDB(this.actionService.getDataLoadingRows());
        addSMSAlertsToQueue(alerts);
        return alerts.size();
    }
    
    public final void manageExtensiblePermit(final SmsAlertInfo alertInfo) {
        String smsMessage = null;
        try {
            final ActivePermit activePermit = this.activePermitService.findActivePermitByOriginalPermitId(alertInfo.getOriginalPermitId());
            if (activePermit == null) {
                logger.info("SmsPermitAlertJobServiceImpl.manageExtensiblePermit(): No Active Permit found for Permit Id "
                            + alertInfo.getOriginalPermitId());
                return;
            }
            final Permit prevPermit = this.permitService.findPermitByIdForSms(alertInfo.getOriginalPermitId());
            if (!this.job.getIsInterrupted()) {
                final SmsRateInfo rateInfo = this.extensibleRateAndPermissionService.getSmsRateInfo(alertInfo);
                logger.info("++++ SmsRateInfo: " + rateInfo);
                // permit can extend
                if (rateInfo.isExtendable()) {
                    
                    if (rateInfo.isFreeParking() || alertInfo.getIsAutoExtended()) {
                        smsMessage = buildSMSMessageByRequest(rateInfo, alertInfo, AURA_REQ_4083, 0);
                        this.extensiblePermitService.deleteExtensiblePermitAndSMSAlert(alertInfo.getMobileNumber());
                        logSmsTransaction(prevPermit, WebCoreConstants.TIME_AUTO_EXTENDED);
                        logger.info("+++ parking permit (ID: " + prevPermit.getId() + ") in free parking zone, no alert need send +++");
                        
                        if (rateInfo.getEmsRate1() == null && rateInfo.getEmsRate2() == null) {
                            try {
                                final Method method = SmsPermitAlertJobServiceImpl.class.getDeclaredMethod(SEND_SMS_METHOD_NAME, new Class[] {
                                    SmsRateInfo.class, SmsAlertInfo.class, String.class, Permit.class, SmsService.class, });
                                this.kpiListenerService.invoke(this, method,
                                                               new Object[] { rateInfo, alertInfo, smsMessage, prevPermit, smsService });
                            } catch (Exception e) {
                                logger.error("Exception sending SMS free parking SMS, retrying without KPI listener: ", e);
                                this.sendSMS(rateInfo, alertInfo, smsMessage, prevPermit, smsService);
                            }
                        }
                        
                    } else {
                        if (rateInfo.getIsSecondPermissionNoParking() && !rateInfo.isSpecialExtentionForNextFreeParking()
                            && !rateInfo.isSpecialExtentionForNextNoParking() && !rateInfo.isSpecialExtentionForSingleRate()) {
                            smsMessage = buildSMSMessageByRequest(rateInfo, alertInfo, AURA_REQ_4015, 0);
                        } else {
                            // send SMS with option                           
                            smsMessage = buildSMSMessageByRequest(rateInfo, alertInfo, AURA_REQ_4006, 0);
                        }
                        
                        try {
                            final Method method = SmsPermitAlertJobServiceImpl.class.getDeclaredMethod(SEND_SMS_METHOD_NAME, new Class[] {
                                SmsRateInfo.class, SmsAlertInfo.class, String.class, Permit.class, SmsService.class, });
                            this.kpiListenerService.invoke(this, method, new Object[] { rateInfo, alertInfo, smsMessage, prevPermit, smsService });
                        } catch (Exception e) {
                            logger.error("Exception sending warning SMS, retrying without KPI listener: ", e);
                            this.sendSMS(rateInfo, alertInfo, smsMessage, prevPermit, smsService);
                        }
                        
                    }
                    // permit can not extend
                } else {
                    // send SMS with no option                  
                    if (rateInfo.isFreeParking() || alertInfo.getIsAutoExtended()) {
                        smsMessage = buildSMSMessageByRequest(rateInfo, alertInfo, AURA_REQ_4025, 0);
                    } else {
                        smsMessage = buildSMSMessageByRequest(rateInfo, alertInfo, AURA_REQ_4054, 0);
                    }
                    
                    try {
                        final Method method = SmsPermitAlertJobServiceImpl.class.getDeclaredMethod(SEND_SMS_METHOD_NAME, new Class[] {
                            SmsRateInfo.class, SmsAlertInfo.class, String.class, Permit.class, SmsService.class, });
                        this.kpiListenerService.invoke(this, method, new Object[] { rateInfo, alertInfo, smsMessage, prevPermit, smsService });
                    } catch (Exception e) {
                        logger.error("Exception sending no option SMS, retrying without KPI listener: ", e);
                        this.sendSMS(rateInfo, alertInfo, smsMessage, prevPermit, smsService);
                    }
                    
                }
            }
        } catch (Exception e) {
            logger.error("Exception managing extensible permit: ", e);
        }
    }
    
    public final void confirmExtensiblePermit(final SmsAlertInfo smsAlertInfo, final SmsRateInfo smsRateInfo, final int messageType,
        final String userReply, final Permit originalPermit, final Permit permit, final ProcessorTransaction chargePt,
        final PaymentCard paymentCard) {
        String smsMessage = null;
        boolean isFreeParkingExtended = false;
        
        try {
            if (messageType == WebCoreConstants.INVALID_USER_RESPONSE_RECEIVED) {
                this.logSmsTransaction(smsAlertInfo, WebCoreConstants.INVALID_USER_RESPONSE_RECEIVED, userReply);
                
                smsMessage = this.buildSMSMessageByRequest(smsRateInfo, smsAlertInfo, SmsPermitAlertJobService.AURA_REQ_4082, 0);
            } else if (messageType == WebCoreConstants.UNABLE_TO_PROCESS_CREDIT_CARD) {
                smsMessage =
                        this.buildSMSMessageByRequest(smsRateInfo, smsAlertInfo, SmsPermitAlertJobService.AURA_REQ_4057, Integer.parseInt(userReply));
            } else if (messageType == WebCoreConstants.CREDIT_CARD_DECLINED) {
                smsMessage =
                        this.buildSMSMessageByRequest(smsRateInfo, smsAlertInfo, SmsPermitAlertJobService.AURA_REQ_4058, Integer.parseInt(userReply));
            } else if (messageType == WebCoreConstants.EXTENSION_CONFIRMATION_SENT) {
                final Date expiryDateLocal = DateUtil.changeTimeZone(smsAlertInfo.getExpiryDate(), smsAlertInfo.getTimeZone());
                /* reset the latest purchased date and expiry date. */
                smsAlertInfo.setPurchasedDate(permit.getPermitBeginGmt());
                smsAlertInfo.setExpiryDate(permit.getPermitExpireGmt());
                
                if (smsRateInfo.isFreeParking()) {
                    if (smsAlertInfo.getPlateNumber() != null && !smsAlertInfo.getPlateNumber().isEmpty()) {
                        /* Pay by License free parking included. */
                        smsMessage = this.buildSMSMessageByRequest(smsRateInfo, smsAlertInfo, AURA_REQ_4073, Integer.parseInt(userReply));
                        isFreeParkingExtended = true;
                    } else {
                        /* Pay by Stall free parking included. */
                        smsMessage = this.buildSMSMessageByRequest(smsRateInfo, smsAlertInfo, AURA_REQ_4075, Integer.parseInt(userReply));
                        isFreeParkingExtended = true;
                    }
                    
                    logger.info("+++ Confirmation: parking permit (ID:" + permit.getId() + ") auto extended +++");
                } else {
                    if (smsRateInfo.isSecondRateFreeParking(expiryDateLocal) && !smsRateInfo.getIsSecondPermissionNoParking()) {
                        if (smsRateInfo.getMaxExtendedTimeUnderRate1() <= Integer.parseInt(userReply)) {
                            if (smsAlertInfo.getPlateNumber() != null && !smsAlertInfo.getPlateNumber().isEmpty()) {
                                /* Pay by License free parking included. */
                                smsMessage = this.buildSMSMessageByRequest(smsRateInfo, smsAlertInfo, AURA_REQ_4073, Integer.parseInt(userReply));
                                isFreeParkingExtended = true;
                            } else {
                                /* Pay by Stall free parking included. */
                                smsMessage = this.buildSMSMessageByRequest(smsRateInfo, smsAlertInfo, AURA_REQ_4075, Integer.parseInt(userReply));
                                isFreeParkingExtended = true;
                            }
                        } else {
                            if (smsAlertInfo.getPlateNumber() != null && !smsAlertInfo.getPlateNumber().isEmpty()) {
                                /* Pay by License parking. */
                                smsMessage = this.buildSMSMessageByRequest(smsRateInfo, smsAlertInfo, AURA_REQ_4072, Integer.parseInt(userReply));
                            } else {
                                /* Pay by Stall parking. */
                                smsMessage = this.buildSMSMessageByRequest(smsRateInfo, smsAlertInfo, AURA_REQ_4074, Integer.parseInt(userReply));
                            }
                        }
                    } else {
                        if (smsAlertInfo.getPlateNumber() != null && !smsAlertInfo.getPlateNumber().isEmpty()) {
                            /* Pay by License parking. */
                            smsMessage = this.buildSMSMessageByRequest(smsRateInfo, smsAlertInfo, AURA_REQ_4072, Integer.parseInt(userReply));
                        } else {
                            /* Pay by Stall parking. */
                            smsMessage = this.buildSMSMessageByRequest(smsRateInfo, smsAlertInfo, AURA_REQ_4074, Integer.parseInt(userReply));
                        }
                    }
                }
                
                this.permitService.saveExtendedPermit(originalPermit, permit);
                if (permit.getPurchase() != null) {
                    if (chargePt != null) {
                        chargePt.setPurchase(permit.getPurchase());
                        this.processorTransactionService.updateProcessorTransaction(chargePt);
                    }
                    if (paymentCard != null) {
                        paymentCard.setPurchase(permit.getPurchase());
                        this.paymentCardService.savePaymentCard(paymentCard);
                    }
                }
            }
            
            try {
                final Method method = SmsPermitAlertJobService.class.getDeclaredMethod("sendConfirmationSMS", new Class[] { SmsRateInfo.class,
                    SmsAlertInfo.class, Integer.class, String.class, String.class, Permit.class, Boolean.class, SmsService.class });
                this.kpiListenerService.invoke(this, method, new Object[] { smsRateInfo, smsAlertInfo, messageType, smsMessage, userReply, permit,
                    isFreeParkingExtended, this.smsService, });
            } catch (Throwable e) {
                logger.error("Exception sending confirmation SMS, retrying without KPI listener", e);
                this.sendConfirmationSMS(smsRateInfo, smsAlertInfo, messageType, smsMessage, userReply, permit, isFreeParkingExtended,
                                         this.smsService);
            }
            
        } catch (Exception e) {
            logger.error("Exception sending confirmation SMS: ", e);
        }
    }
    
    public final int getSleepTime() {
        return this.actionService.getSleepTime();
    }
    
    public final void notifyMoreDataComing() {
        for (int i = 0; i < this.smsPermitAlertProcessingThreads.length; i++) {
            synchronized (this.smsPermitAlertProcessingThreads[i]) {
                this.smsPermitAlertProcessingThreads[i].wakeup();
            }
        }
    }
    
    public final void notifyMoreDataNeeding() {
        synchronized (this.smsPermitAlertExtractionThread) {
            this.smsPermitAlertExtractionThread.wakeup();
        }
    }
    
    private void addSMSAlertsToQueue(final Collection<SmsAlertInfo> alerts) {
        for (SmsAlertInfo alertInfo : alerts) {
            if (!this.smsPermitAlertQueue.contains(alertInfo)) {
                this.smsPermitAlertQueue.offer(alertInfo);
            }
        }
    }
    
    @Override
    public final void sendSMS(final SmsPermitAlertJob smsJob) {
        if (this.job == null) {
            this.job = smsJob;
        } else {
            if (smsJob.isStartup()) {
                this.job = smsJob;
            }
        }
        if (this.job.isStartup()) {
            logger.debug("+++ scheduled time for sending SMS, invoking service +++");
            notifyMoreDataNeeding();
        } else {
            try {
                Thread.sleep(StandardConstants.SECONDS_IN_MILLIS_1 * super.actionService.getSleepTime());
            } catch (InterruptedException e) {
                logger.debug(e);
            }
            
        }
    }
    
    @Override
    public final void sendSMS(final SmsRateInfo rateInfo, final SmsAlertInfo alertInfo, final String smsMessage, final Permit permit,
        final SmsService client) {
        if (!this.emsPropertiesService.getPropertyValueAsBoolean(EmsPropertiesService.SMS_SIMULATED_ENV,
                                                                 EmsPropertiesService.SMS_SIMULATED_ENV_DEFAULT)) {
            final String serverUrl = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.LICENSE_SERVER_URL);
            final StringBuilder strBuilder = new StringBuilder(serverUrl);
            final String callbackUrl = strBuilder.append(PATH_URL).toString();
            final String smsJson = client.createJson(alertInfo.getMobileNumber(), smsMessage, callbackUrl, null, null);
            try {
                client.sendSmsRequest(smsJson);
                if (rateInfo.isExtendable()) {
                    logSmsTransaction(permit, WebCoreConstants.EXPIRY_ALERT_SENT_OPTION_TO_EXTEND);
                } else {
                    logSmsTransaction(permit, WebCoreConstants.EXPIRY_ALERT_SENT_NO_OPTION_TO_EXTEND);
                }
                this.extensiblePermitService.updateSmsAlert(true, 0, false, alertInfo);
                logger.info("+++ SMS alert " + alertInfo + " sent successfully +++");
                
            } catch (Exception e) {
                logger.error("sendSMS failure: ", e);
                logSmsTransaction(permit, WebCoreConstants.SMS_SEND_FAILURE);
                this.extensiblePermitService.updateSmsAlert(true, 2, false, alertInfo);
                logger.info("+++ SMS alert " + alertInfo + " sent failed +++");
            }
        } else {
            if (rateInfo.isExtendable()) {
                logSmsTransaction(permit, WebCoreConstants.EXPIRY_ALERT_SENT_OPTION_TO_EXTEND);
            } else {
                logSmsTransaction(permit, WebCoreConstants.EXPIRY_ALERT_SENT_NO_OPTION_TO_EXTEND);
            }
            this.extensiblePermitService.updateSmsAlert(true, 0, false, alertInfo);
            logger.info("!!! It is SMS simulated environment, SMS alert " + alertInfo + " sent successfully !!!");
        }
    }
    
    @Override
    public final void sendConfirmationSMS(final SmsRateInfo smsRateInfo, final SmsAlertInfo smsAlertInfo, final Integer messageType,
        final String smsMessage, final String userReply, final Permit permit, final Boolean isFreeParkingExtended, final SmsService client) {
        if (!this.emsPropertiesService.getPropertyValueAsBoolean(EmsPropertiesService.SMS_SIMULATED_ENV,
                                                                 EmsPropertiesService.SMS_SIMULATED_ENV_DEFAULT)) {
            
            final SmsAlert smsAlert = this.smsAlertService.findUnexpiredSmsAlertById(smsAlertInfo.getSmsAlertId());
            final String callbackId = smsAlert.getCallbackId();
            final Boolean endConversation;
            // Decide whether to end the convo:
            // The only time we don't want to end the convo is when messageType is the invalid response case
            if (messageType == WebCoreConstants.INVALID_USER_RESPONSE_RECEIVED) {
                endConversation = false;
            } else {
                endConversation = true;
            }
            
            final String smsJson = client.createJson(null, smsMessage, null, null, endConversation);
            
            try {
                client.sendReplyRequest(callbackId, smsJson);
                this.processPostConfirmation(messageType, smsAlertInfo, userReply, permit, isFreeParkingExtended);
            } catch (Exception e) {
                logger.error("sendConfirmationSMS failure: ", e);
                logSmsTransaction(smsAlertInfo, WebCoreConstants.SMS_SEND_FAILURE, userReply);
                this.extensiblePermitService.updateSmsAlert(false, 1, false, smsAlertInfo);
                
                logger.info("+++ SMS alert " + smsAlertInfo + " sent failed +++");
            }
        } else {
            this.processPostConfirmation(messageType, smsAlertInfo, userReply, permit, isFreeParkingExtended);
            logger.info("!!! It is SMS simulated environment, SMS alert " + smsAlertInfo + " sent successfully !!!");
        }
    }
    
    public final SmsAlertInfo getNextSMSPermitAlertFromQueue() {
        SmsAlertInfo smsAlertInfo = null;
        
        if (!this.smsPermitAlertQueue.isEmpty()) {
            smsAlertInfo = this.smsPermitAlertQueue.poll();
        }
        if (smsAlertInfo != null) {
            if (logger.isDebugEnabled()) {
                logger.debug("++++ retrieving SMS alert from push queue: mobileNumber: " + smsAlertInfo.getMobileNumber()
                             + ", originalPurchasedDate: " + smsAlertInfo.getPurchasedDate() + ", expiryDate: " + smsAlertInfo.getExpiryDate()
                             + ", locationId: " + smsAlertInfo.getLocationId().intValue() + " +++");
            }
        } else {
            logger.info("++++ retrieving SMS alert: null from push queue +++");
        }
        return smsAlertInfo;
    }
    
    public final ConcurrentLinkedQueue<SmsAlertInfo> getSMSPermitAlertQueue() {
        return this.smsPermitAlertQueue;
    }
    
    public final boolean isNeedPause() {
        if (this.job != null) {
            logger.debug("++ job is not null: " + this.job.getIsInterrupted());
            return this.job.getIsInterrupted();
        } else {
            logger.debug("++ job is null +++");
            return true;
        }
    }
    
    private void initSMSPermitThread() {
        final int numProcessingThreads = this.actionService.getNumOfThreads();
        this.smsPermitAlertExtractionThread = new SmsPermitAlertExtractionThread(this);
        this.smsPermitAlertProcessingThreads = new SmsPermitAlertProcessingThread[numProcessingThreads];
        /* retrieve upsidewireless account from DB. */
        int dptCustomerId = EmsPropertiesService.DEFAULT_DPT_CUSTOMER_ID;
        try {
            dptCustomerId = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.DPT_CUSTOMER_ID,
                                                                            EmsPropertiesService.DEFAULT_DPT_CUSTOMER_ID);
        } catch (Exception e) {
            logger.error("exception initializing SMS Permit Thread exception: ", e);
            //Don't need to do anything use default value
        }
        
        final ThirdPartyServiceAccount account = this.thirdPartyServiceAccountService
                .getThirdPartyServiceAccountByCustomerAndType(dptCustomerId, WebCoreConstants.THIRD_PARTY_ACCOUNT_TYPE_UPSIDEWIRELESS);
        
        for (int i = 0; i < numProcessingThreads; i++) {
            this.smsPermitAlertProcessingThreads[i] = new SmsPermitAlertProcessingThread(i, this);
        }
        this.smsPermitAlertExtractionThread.start();
        for (int i = 0; i < numProcessingThreads; i++) {
            this.smsPermitAlertProcessingThreads[i].start();
        }
        logger.info("+++  SMSPermitAlertExtractionThread and  SMSPermitAlertProcessingThreads initialized +++");
    }
    
    public final void processPostConfirmation(final int messageType, final SmsAlertInfo smsAlertInfo, final String userReply, final Permit permit,
        final boolean isFreeParkingExtended) {
        if (messageType == WebCoreConstants.INVALID_USER_RESPONSE_RECEIVED) {
            this.logSmsTransaction(smsAlertInfo, WebCoreConstants.FAILURE_MESSAGE_SENT_TO_USER, userReply);
            final SmsAlert smsAlert = this.smsAlertService.findSmsAlertById(smsAlertInfo.getSmsAlertId());
            this.extensiblePermitService.updateSmsAlert(true, smsAlert.getNumberOfRetries() + 1, false, smsAlertInfo);
        } else if (messageType == WebCoreConstants.UNABLE_TO_PROCESS_CREDIT_CARD) {
            this.logSmsTransaction(smsAlertInfo, WebCoreConstants.UNABLE_TO_PROCESS_CREDIT_CARD, userReply);
            this.extensiblePermitService.updateSmsAlert(true, 2, false, smsAlertInfo);
        } else if (messageType == WebCoreConstants.CREDIT_CARD_DECLINED) {
            this.logSmsTransaction(smsAlertInfo, WebCoreConstants.CREDIT_CARD_DECLINED, userReply);
            this.extensiblePermitService.updateSmsAlert(true, 2, false, smsAlertInfo);
        } else if (messageType == WebCoreConstants.EXTENSION_CONFIRMATION_SENT) {
            this.logSmsTransaction(smsAlertInfo, WebCoreConstants.EXTENSION_CONFIRMATION_SENT, userReply);
            this.extensiblePermitService.updateSmsAlertAndLatestExpiryDate(false, 0, false, permit.getPermitBeginGmt(), permit.getPermitExpireGmt(),
                                                                           isFreeParkingExtended, smsAlertInfo);
        }
    }
    
    public final String buildSMSMessageByRequest(final SmsRateInfo rateInfo, final SmsAlertInfo alertInfo, final int requestType,
        final int minutesToAdd) {
        String rate2Value = "";
        String rate2StartTime = "";
        String rate2EndTime = "";
        String content = "";
        
        if (rateInfo.getEmsRate2() == null) {
            // free parking for rate2
            if (rateInfo.getEmsRate1() != null) {
                rate2Value = this.messageAccessor.getMessage(AURA_SMS_MESSAGE_FREE_PKG);
                
                rate2StartTime = hourFormatter(rateInfo.getEmsRate1().getEndHourLocal(), false) + ":"
                                 + minuteFormatter(rateInfo.getEmsRate1().getEndMinuteLocal());
            }
        } else {
            rate2Value = "$" + dollarFormat(rateInfo.getEmsRate2().getRateAmount()) + "/hr";
            rate2StartTime = hourFormatter(rateInfo.getEmsRate2().getBeginHourLocal(), false) + ":"
                             + minuteFormatter(rateInfo.getEmsRate2().getBeginMinuteLocal());
        }
        
        final Date expiryDateLocal = DateUtil.changeTimeZone(alertInfo.getExpiryDate(), alertInfo.getTimeZone());
        final Date newExpiryDateLocal =
                new Date(expiryDateLocal.getTime() + (rateInfo.getMaxAllowedParkingTimeInMinutes() * StandardConstants.SECONDS_IN_MILLIS_1 * 60));
        final Calendar c = Calendar.getInstance();
        c.setTime(alertInfo.getExpiryDate());
        c.add(Calendar.MINUTE, rateInfo.getMaxAllowedParkingTimeInMinutes());
        final Date newExpiryDateGMT = c.getTime();
        final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        rate2EndTime = sdf.format(newExpiryDateLocal);
        if (rate2EndTime.equals("00:00")) {
            rate2EndTime = "24:00";
        }
        
        if (logger.isDebugEnabled()) {
            logger.debug("++++++ expiryDateLocal: " + expiryDateLocal);
            logger.debug("++++++ newExpiryDate(Local): " + newExpiryDateLocal + ", rateInfo.getMaxAllowedParkingTimeInMinutes()"
                         + rateInfo.getMaxAllowedParkingTimeInMinutes());
        }
        
        String servieFee = "";
        if (rateInfo.getEmsRate1() != null && rateInfo.getEmsRate1().getServiceFeeAmount() > 0) {
            servieFee = "(+$" + dollarFormat(rateInfo.getEmsRate1().getServiceFeeAmount()) + " fee)";
        }
        
        switch (requestType) {
            case AURA_REQ_4006:
                if (rateInfo.getIsRate2Need()) {
                    // req.4011
                    if (rateInfo.isSpecialExtentionForNextFreeParking()) {
                        //2)single value + free parking
                        if (rateInfo.getMaxAllowedTimeByPermission() != WebCoreConstants.DEFAULT_MAX_EXTENDABLE_TIME_IN_MINUTES) {
                            logger.info("++++++ AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE (SpecialExtentionForNextFreeParking) +++++");
                            content = this.messageAccessor
                                    .getMessage(AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE,
                                                new String[] { alertInfo.getExpiryDateDisplayTime(),
                                                    String.valueOf(rateInfo.getMaxExtendedTimeUnderRate1()),
                                                    dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(),
                                                    hourFormatter(rateInfo.getEmsRate1().getEndHourLocal(),
                                                                  true) + ":" + minuteFormatter(rateInfo.getEmsRate1().getEndMinuteLocal()),
                                                    rate2Value, rate2StartTime, rate2EndTime, servieFee });
                        } else {
                            logger.info("++++++ AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE_2 (SpecialExtentionForNextFreeParking) +++++");
                            content = this.messageAccessor
                                    .getMessage(AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE_2,
                                                new String[] { alertInfo.getExpiryDateDisplayTime(),
                                                    String.valueOf(rateInfo.getMaxExtendedTimeUnderRate1()),
                                                    dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(),
                                                    hourFormatter(rateInfo.getEmsRate1().getEndHourLocal(),
                                                                  true) + ":" + minuteFormatter(rateInfo.getEmsRate1().getEndMinuteLocal()),
                                                    rate2Value, rate2StartTime, servieFee, });
                        }
                        // req. 4011
                    } else if (rateInfo.isSpecialExtentionForNextNoParking()) {
                        //1)single value + 2nd NO PARKING
                        if (newExpiryDateLocal.before(rateInfo.getEmsRate1().getEndDateLocal())) {
                            if (!rateInfo.getIsNoParkingLongerThan24H()) {
                                logger.info("++++++ AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE (SpecialExtentionForNextNoParking) +++++");
                                content = this.messageAccessor.getMessage(AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE, new String[] {
                                    alertInfo.getExpiryDateDisplayTime(), String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()),
                                    dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(), rate2EndTime,
                                    "NO PARKING", rate2EndTime, rateInfo.getNoParkingLocalEndTimeDisplay(), servieFee, });
                            } else {
                                logger.info("++++++ AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE_2 (SpecialExtentionForNextNoParking) +++++");
                                content = this.messageAccessor
                                        .getMessage(AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE_2,
                                                    new String[] { alertInfo.getExpiryDateDisplayTime(),
                                                        String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()),
                                                        dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(),
                                                        rate2EndTime, "NO PARKING", rate2EndTime, servieFee, });
                            }
                        } else {
                            if (!rateInfo.getIsNoParkingLongerThan24H()) {
                                logger.info("++++++ AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE (SpecialExtentionForNextNoParking) +++++");
                                content = this.messageAccessor
                                        .getMessage(AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE,
                                                    new String[] { alertInfo.getExpiryDateDisplayTime(),
                                                        String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()),
                                                        dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(),
                                                        hourFormatter(rateInfo.getEmsRate1().getEndHourLocal(),
                                                                      true) + ":" + minuteFormatter(rateInfo.getEmsRate1().getEndMinuteLocal()),
                                                        "NO PARKING", rate2StartTime, rateInfo.getNoParkingLocalEndTimeDisplay(), servieFee, });
                            } else {
                                logger.info("++++++ AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE_2 (SpecialExtentionForNextNoParking) +++++");
                                content = this.messageAccessor
                                        .getMessage(AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE_2,
                                                    new String[] { alertInfo.getExpiryDateDisplayTime(),
                                                        String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()),
                                                        dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(),
                                                        hourFormatter(rateInfo.getEmsRate1().getEndHourLocal(),
                                                                      true) + ":" + minuteFormatter(rateInfo.getEmsRate1().getEndMinuteLocal()),
                                                        "NO PARKING", rate2StartTime, servieFee, });
                            }
                        }
                        // req. 4011
                    } else if (rateInfo.isSpecialExtentionForSingleRate()) {
                        //single value + 1 rates
                        logger.info("++++++ AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE_3 (SpecialExtentionForSingleRate) +++++");
                        content = this.messageAccessor.getMessage(AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE_3, new String[] {
                            alertInfo.getExpiryDateDisplayTime(), String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()),
                            dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(), rate2EndTime, servieFee, });
                        
                        // req.4006
                    } else {
                        if (rateInfo.isSecondRateFreeParking(expiryDateLocal)) {
                            if (rateInfo.getMaxAllowedTimeByPermission() != WebCoreConstants.DEFAULT_MAX_EXTENDABLE_TIME_IN_MINUTES) {
                                logger.info("++++++ AURA_SMS_REQ_4006_4010_CONTENT_TEMPLATE  +++++");
                                content = this.messageAccessor
                                        .getMessage(AURA_SMS_REQ_4006_4010_CONTENT_TEMPLATE,
                                                    new String[] { alertInfo.getExpiryDateDisplayTime(),
                                                        String.valueOf(rateInfo.getEmsRate1().getMinExtensionMinutes()),
                                                        String.valueOf(String.valueOf(rateInfo.getMaxExtensionTimeForFreeParking())),
                                                        dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(),
                                                        hourFormatter(rateInfo.getEmsRate1().getEndHourLocal(),
                                                                      true) + ":" + minuteFormatter(rateInfo.getEmsRate1().getEndMinuteLocal()),
                                                        rate2Value, rate2StartTime, rate2EndTime, servieFee, });
                            } else {
                                logger.info("++++++ AURA_SMS_REQ_4006_4010_CONTENT_TEMPLATE_3  +++++");
                                content = this.messageAccessor
                                        .getMessage(AURA_SMS_REQ_4006_4010_CONTENT_TEMPLATE_3,
                                                    new String[] { alertInfo.getExpiryDateDisplayTime(),
                                                        String.valueOf(rateInfo.getEmsRate1().getMinExtensionMinutes()),
                                                        String.valueOf(rateInfo.getMaxExtensionTimeForFreeParking()),
                                                        dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(),
                                                        hourFormatter(rateInfo.getEmsRate1().getEndHourLocal(),
                                                                      true) + ":" + minuteFormatter(rateInfo.getEmsRate1().getEndMinuteLocal()),
                                                        rate2Value, rate2StartTime, servieFee, });
                            }
                        } else {
                            logger.info("++++++ AURA_SMS_REQ_4006_4010_CONTENT_TEMPLATE  +++++");
                            content = this.messageAccessor
                                    .getMessage(AURA_SMS_REQ_4006_4010_CONTENT_TEMPLATE,
                                                new String[] { alertInfo.getExpiryDateDisplayTime(),
                                                    String.valueOf(rateInfo.getEmsRate1().getMinExtensionMinutes()),
                                                    String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()),
                                                    dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(),
                                                    hourFormatter(rateInfo.getEmsRate1().getEndHourLocal(),
                                                                  true) + ":" + minuteFormatter(rateInfo.getEmsRate1().getEndMinuteLocal()),
                                                    rate2Value, rate2StartTime, rate2EndTime, servieFee, });
                        }
                    }
                    //REQ. 4006, 4010 it comes here only when one rate is allowed for the rate calculation. 
                } else {
                    // from min to max
                    if (rateInfo.hasFirstRateFreeParking(expiryDateLocal)) {
                        if (rateInfo.getMaxAllowedTimeByPermission() != WebCoreConstants.DEFAULT_MAX_EXTENDABLE_TIME_IN_MINUTES) {
                            logger.info("++++++ AURA_SMS_REQ_4006_4010_CONTENT_TEMPLATE  +++++");
                            content = this.messageAccessor
                                    .getMessage(AURA_SMS_REQ_4006_4010_CONTENT_TEMPLATE,
                                                new String[] { alertInfo.getExpiryDateDisplayTime(),
                                                    String.valueOf(rateInfo.getEmsRate1().getMinExtensionMinutes()),
                                                    String.valueOf(rateInfo.getMaxExtensionTimeForFreeParking()),
                                                    dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(),
                                                    hourFormatter(rateInfo.getEmsRate1().getEndHourLocal(),
                                                                  true) + ":" + minuteFormatter(rateInfo.getEmsRate1().getEndMinuteLocal()),
                                                    rate2Value, rate2StartTime, rate2EndTime, servieFee, });
                        } else {
                            logger.info("++++++ AURA_SMS_REQ_4006_4010_CONTENT_TEMPLATE  +++++");
                            content = this.messageAccessor
                                    .getMessage(AURA_SMS_REQ_4006_4010_CONTENT_TEMPLATE,
                                                new String[] { alertInfo.getExpiryDateDisplayTime(),
                                                    String.valueOf(rateInfo.getEmsRate1().getMinExtensionMinutes()),
                                                    String.valueOf(rateInfo.getMaxExtensionTimeForFreeParking()),
                                                    dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(),
                                                    hourFormatter(rateInfo.getEmsRate1().getEndHourLocal(),
                                                                  true) + ":" + minuteFormatter(rateInfo.getEmsRate1().getEndMinuteLocal()),
                                                    rate2Value, rate2StartTime, rate2EndTime, servieFee, });
                        }
                    } else {
                        // req.4011
                        if (rateInfo.isSpecialExtentionForNextFreeParking()) {
                            // 2) single value + free parking
                            if (rateInfo.getMaxAllowedTimeByPermission() != WebCoreConstants.DEFAULT_MAX_EXTENDABLE_TIME_IN_MINUTES) {
                                logger.info("++++++ AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE (SpecialExtentionForNextFreeParking) +++++");
                                content = this.messageAccessor
                                        .getMessage(AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE,
                                                    new String[] { alertInfo.getExpiryDateDisplayTime(),
                                                        String.valueOf(rateInfo.getMaxExtendedTimeUnderRate1()),
                                                        dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(),
                                                        hourFormatter(rateInfo.getEmsRate1().getEndHourLocal(),
                                                                      true) + ":" + minuteFormatter(rateInfo.getEmsRate1().getEndMinuteLocal()),
                                                        rate2Value, rate2StartTime, rate2EndTime, servieFee, });
                            } else {
                                logger.info("++++++ AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE_2 (SpecialExtentionForNextFreeParking) +++++");
                                content = this.messageAccessor
                                        .getMessage(AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE_2,
                                                    new String[] { alertInfo.getExpiryDateDisplayTime(),
                                                        String.valueOf(rateInfo.getMaxExtendedTimeUnderRate1()),
                                                        dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(),
                                                        hourFormatter(rateInfo.getEmsRate1().getEndHourLocal(),
                                                                      true) + ":" + minuteFormatter(rateInfo.getEmsRate1().getEndMinuteLocal()),
                                                        rate2Value, rate2StartTime, servieFee, });
                            }
                            
                            // req. 4011
                        } else if (rateInfo.isSpecialExtentionForNextNoParking()) {
                            // 1) single value + 2nd NO PARKING                           
                            if (newExpiryDateLocal.before(rateInfo.getEmsRate1().getEndDateLocal())) {
                                if (!rateInfo.getIsNoParkingLongerThan24H()) {
                                    logger.info("++++++ AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE (SpecialExtentionForNextNoParking) +++++");
                                    content = this.messageAccessor.getMessage(AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE, new String[] {
                                        alertInfo.getExpiryDateDisplayTime(), String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()),
                                        dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(), rate2EndTime,
                                        "NO PARKING", rate2EndTime, rateInfo.getNoParkingLocalEndTimeDisplay(), servieFee, });
                                } else {
                                    logger.info("++++++ AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE_2 (SpecialExtentionForNextNoParking) +++++");
                                    content = this.messageAccessor.getMessage(AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE_2, new String[] {
                                        alertInfo.getExpiryDateDisplayTime(), String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()),
                                        dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(), rate2EndTime,
                                        "NO PARKING", rate2EndTime, servieFee, });
                                }
                            } else {
                                if (!rateInfo.getIsNoParkingLongerThan24H()) {
                                    logger.info("++++++ AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE (SpecialExtentionForNextNoParking) +++++");
                                    content = this.messageAccessor.getMessage(AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE, new String[] {
                                        alertInfo.getExpiryDateDisplayTime(), String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()),
                                        dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(),
                                        hourFormatter(rateInfo.getEmsRate1().getEndHourLocal(),
                                                      true) + ":" + minuteFormatter(rateInfo.getEmsRate1().getEndMinuteLocal()),
                                        "NO PARKING", rate2StartTime, rateInfo.getNoParkingLocalEndTimeDisplay(), servieFee, });
                                } else {
                                    logger.info("++++++ AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE_2 (SpecialExtentionForNextNoParking) +++++");
                                    content = this.messageAccessor.getMessage(AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE_2, new String[] {
                                        alertInfo.getExpiryDateDisplayTime(), String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()),
                                        dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(),
                                        hourFormatter(rateInfo.getEmsRate1().getEndHourLocal(),
                                                      true) + ":" + minuteFormatter(rateInfo.getEmsRate1().getEndMinuteLocal()),
                                        "NO PARKING", rate2StartTime, servieFee, });
                                }
                            }
                            // req. 4011
                        } else if (rateInfo.isSpecialExtentionForSingleRate()) {
                            // single value + 1 rates
                            logger.info("++++++ AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE_3 (SpecialExtentionForSingleRate) +++++");
                            content = this.messageAccessor.getMessage(AURA_SMS_REQ_4006_4010_4011_CONTENT_TEMPLATE_3,
                                                                      new String[] { alertInfo.getExpiryDateDisplayTime(),
                                                                          String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()),
                                                                          dollarFormat(rateInfo.getEmsRate1().getRateAmount()),
                                                                          alertInfo.getExpiryDateDisplayTime(), rate2EndTime, servieFee, });
                            
                        } else {
                            logger.info("++++++ AURA_SMS_REQ_4006_4010_CONTENT_TEMPLATE_2  +++++");
                            content = this.messageAccessor.getMessage(AURA_SMS_REQ_4006_4010_CONTENT_TEMPLATE_2, new String[] {
                                alertInfo.getExpiryDateDisplayTime(), String.valueOf(rateInfo.getEmsRate1().getMinExtensionMinutes()),
                                String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()), dollarFormat(rateInfo.getEmsRate1().getRateAmount()),
                                alertInfo.getExpiryDateDisplayTime(), alertInfo.getExpiryDateDisplayTime(newExpiryDateGMT), servieFee, });
                        }
                    }
                }
                break;
            case AURA_REQ_4015:
                String endTime = addMinutesInDateForDisplay(alertInfo.getExpiryDateLocal(), rateInfo.getMaxAllowedParkingTimeInMinutes());
                if (endTime.equals("00:00")) {
                    endTime = "24:00";
                }
                if (!rateInfo.getIsNoParkingLongerThan24H()) {
                    logger.info("++++++ AURA_SMS_REQ_4015_CONTENT_TEMPLATE  +++++");
                    content = this.messageAccessor.getMessage(AURA_SMS_REQ_4015_CONTENT_TEMPLATE, new String[] { alertInfo.getExpiryDateDisplayTime(),
                        String.valueOf(rateInfo.getEmsRate1().getMinExtensionMinutes()), String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()),
                        dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(), endTime,
                        addMinutesInDateForDisplay(alertInfo.getExpiryDateLocal(), rateInfo.getMaxAllowedParkingTimeInMinutes()),
                        rateInfo.getNoParkingLocalEndTimeDisplay(), servieFee, });
                } else {
                    logger.info("++++++ AURA_SMS_REQ_4015_CONTENT_TEMPLATE_2  +++++");
                    content = this.messageAccessor.getMessage(AURA_SMS_REQ_4015_CONTENT_TEMPLATE_2, new String[] {
                        alertInfo.getExpiryDateDisplayTime(), String.valueOf(rateInfo.getEmsRate1().getMinExtensionMinutes()),
                        String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()), dollarFormat(rateInfo.getEmsRate1().getRateAmount()),
                        alertInfo.getExpiryDateDisplayTime(), endTime,
                        addMinutesInDateForDisplay(alertInfo.getExpiryDateLocal(), rateInfo.getMaxAllowedParkingTimeInMinutes()), servieFee, });
                }
                break;
            case AURA_REQ_4025:
                logger.info("++++++ AURA_SMS_REQ_4025_CONTENT_TEMPLATE  +++++");
                content = this.messageAccessor.getMessage(AURA_SMS_REQ_4025_CONTENT_TEMPLATE, new String[] { alertInfo.getExpiryDateDisplayTime() });
                break;
            case AURA_REQ_4030:
                if (rateInfo.getIsRate2Need() && !(rateInfo.getEmsRate1() == null && rateInfo.getEmsRate2() == null)) {
                    // req.4011, 4030
                    if (rateInfo.isSpecialExtentionForNextFreeParking()) {
                        //2)single value + free parking
                        logger.info("++++++ AURA_SMS_REQ_4006_4010_4011_4030_CONTENT_TEMPLATE (SpecialExtentionForNextFreeParking) +++++");
                        content = this.messageAccessor
                                .getMessage(AURA_SMS_REQ_4006_4010_4011_4030_CONTENT_TEMPLATE,
                                            new String[] { alertInfo.getExpiryDateDisplayTime(),
                                                String.valueOf(rateInfo.getMaxExtendedTimeUnderRate1()),
                                                dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(),
                                                hourFormatter(rateInfo.getEmsRate1().getEndHourLocal(),
                                                              true) + ":" + minuteFormatter(rateInfo.getEmsRate1().getEndMinuteLocal()),
                                                rate2Value, rate2StartTime, rate2EndTime, servieFee, });
                        
                        // req. 4011, 4030
                    } else if (rateInfo.isSpecialExtentionForNextNoParking()) {
                        // 1) single value + 2nd NO PARKING
                        logger.info("++++++ AURA_SMS_REQ_4006_4010_4011_4030_CONTENT_TEMPLATE (SpecialExtentionForNextNoParking) +++++");
                        
                        if (newExpiryDateLocal.before(rateInfo.getEmsRate1().getEndDateLocal())) {
                            if (!rateInfo.getIsNoParkingLongerThan24H()) {
                                content = this.messageAccessor.getMessage(AURA_SMS_REQ_4006_4010_4011_4030_CONTENT_TEMPLATE, new String[] {
                                    alertInfo.getExpiryDateDisplayTime(), String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()),
                                    dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(), rate2EndTime,
                                    "NO PARKING", rate2EndTime, rateInfo.getNoParkingLocalEndTimeDisplay(), servieFee, });
                            }
                        } else {
                            content = this.messageAccessor
                                    .getMessage(AURA_SMS_REQ_4006_4010_4011_4030_CONTENT_TEMPLATE,
                                                new String[] { alertInfo.getExpiryDateDisplayTime(),
                                                    String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()),
                                                    dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(),
                                                    hourFormatter(rateInfo.getEmsRate1().getEndHourLocal(),
                                                                  true) + ":" + minuteFormatter(rateInfo.getEmsRate1().getEndMinuteLocal()),
                                                    "NO PARKING", rate2StartTime, rateInfo.getNoParkingLocalEndTimeDisplay(), servieFee, });
                        }
                        // req. 4011, 4030
                    } else if (rateInfo.isSpecialExtentionForSingleRate()) {
                        // single value + 1 rates
                        logger.info("++++++ AURA_SMS_REQ_4006_4010_4011_4030_CONTENT_TEMPLATE_3 (SpecialExtentionForSingleRate) +++++");
                        content = this.messageAccessor.getMessage(AURA_SMS_REQ_4006_4010_4011_4030_CONTENT_TEMPLATE_3, new String[] {
                            alertInfo.getExpiryDateDisplayTime(), String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()),
                            dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(), rate2EndTime, servieFee, });
                        
                        // req.4030
                    } else {
                        if (rateInfo.isSecondRateFreeParking(expiryDateLocal)) {
                            logger.info("++++++ AURA_SMS_REQ_4030_4010_CONTENT_TEMPLATE  +++++");
                            content = this.messageAccessor
                                    .getMessage(AURA_SMS_REQ_4030_4010_CONTENT_TEMPLATE,
                                                new String[] { alertInfo.getExpiryDateDisplayTime(),
                                                    String.valueOf(rateInfo.getEmsRate1().getMinExtensionMinutes()),
                                                    String.valueOf(rateInfo.getMaxExtensionTimeForFreeParking()),
                                                    dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(),
                                                    hourFormatter(rateInfo.getEmsRate1().getEndHourLocal(),
                                                                  true) + ":" + minuteFormatter(rateInfo.getEmsRate1().getEndMinuteLocal()),
                                                    rate2Value, rate2StartTime, rate2EndTime, servieFee, });
                        } else {
                            logger.info("++++++ AURA_SMS_REQ_4030_4010_CONTENT_TEMPLATE  +++++");
                            content = this.messageAccessor
                                    .getMessage(AURA_SMS_REQ_4030_4010_CONTENT_TEMPLATE,
                                                new String[] { alertInfo.getExpiryDateDisplayTime(),
                                                    String.valueOf(rateInfo.getEmsRate1().getMinExtensionMinutes()),
                                                    String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()),
                                                    dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(),
                                                    hourFormatter(rateInfo.getEmsRate1().getEndHourLocal(),
                                                                  true) + ":" + minuteFormatter(rateInfo.getEmsRate1().getEndMinuteLocal()),
                                                    rate2Value, rate2StartTime, rate2EndTime, servieFee, });
                        }
                    }
                    //REQ. 4030, 4010 it comes here only when one rate is allowed for the rate calculation. 
                } else {
                    // from min to max
                    if (rateInfo.hasFirstRateFreeParking(expiryDateLocal)) {
                        logger.info("++++++ AURA_SMS_REQ_4030_4010_CONTENT_TEMPLATE  +++++");
                        content = this.messageAccessor.getMessage(AURA_SMS_REQ_4030_4010_CONTENT_TEMPLATE, new String[] {
                            alertInfo.getExpiryDateDisplayTime(), String.valueOf(rateInfo.getEmsRate1().getMinExtensionMinutes()),
                            String.valueOf(rateInfo.getMaxExtensionTimeForFreeParking()), dollarFormat(rateInfo.getEmsRate1().getRateAmount()),
                            alertInfo.getExpiryDateDisplayTime(),
                            hourFormatter(rateInfo.getEmsRate1().getEndHourLocal(),
                                          true) + ":" + minuteFormatter(rateInfo.getEmsRate1().getEndMinuteLocal()),
                            rate2Value, rate2StartTime, rate2EndTime, servieFee, });
                    } else {
                        // req.4011, 4030
                        if (rateInfo.isSpecialExtentionForNextFreeParking()) {
                            // 2) single value + free parking
                            logger.info("++++++ AURA_SMS_REQ_4006_4010_4011_4030_CONTENT_TEMPLATE (SpecialExtentionForNextFreeParking) +++++");
                            content = this.messageAccessor
                                    .getMessage(AURA_SMS_REQ_4006_4010_4011_4030_CONTENT_TEMPLATE,
                                                new String[] { alertInfo.getExpiryDateDisplayTime(),
                                                    String.valueOf(rateInfo.getMaxExtendedTimeUnderRate1()),
                                                    dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(),
                                                    hourFormatter(rateInfo.getEmsRate1().getEndHourLocal(),
                                                                  true) + ":" + minuteFormatter(rateInfo.getEmsRate1().getEndMinuteLocal()),
                                                    rate2Value, rate2StartTime, rate2EndTime, servieFee, });
                            
                            // req. 4011, 4030
                        } else if (rateInfo.isSpecialExtentionForNextNoParking()) {
                            // 1) single value + 2nd NO PARKING
                            logger.info("++++++ AURA_SMS_REQ_4006_4010_4011_4030_CONTENT_TEMPLATE (SpecialExtentionForNextNoParking) +++++");
                            if (newExpiryDateLocal.before(rateInfo.getEmsRate1().getEndDateLocal())) {
                                content = this.messageAccessor.getMessage(AURA_SMS_REQ_4006_4010_4011_4030_CONTENT_TEMPLATE, new String[] {
                                    alertInfo.getExpiryDateDisplayTime(), String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()),
                                    dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(), rate2EndTime,
                                    "NO PARKING", rate2EndTime, rateInfo.getNoParkingLocalEndTimeDisplay(), servieFee, });
                            } else {
                                content = this.messageAccessor
                                        .getMessage(AURA_SMS_REQ_4006_4010_4011_4030_CONTENT_TEMPLATE,
                                                    new String[] { alertInfo.getExpiryDateDisplayTime(),
                                                        String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()),
                                                        dollarFormat(rateInfo.getEmsRate1().getRateAmount()), alertInfo.getExpiryDateDisplayTime(),
                                                        hourFormatter(rateInfo.getEmsRate1().getEndHourLocal(),
                                                                      true) + ":" + minuteFormatter(rateInfo.getEmsRate1().getEndMinuteLocal()),
                                                        "NO PARKING", rate2StartTime, rateInfo.getNoParkingLocalEndTimeDisplay(), servieFee, });
                            }
                            // req. 4011,4030
                        } else if (rateInfo.isSpecialExtentionForSingleRate()) {
                            // single value + 1 rates
                            logger.info("++++++ AURA_SMS_REQ_4006_4010_4011_4030_CONTENT_TEMPLATE_3 (SpecialExtentionForSingleRate) +++++");
                            content = this.messageAccessor.getMessage(AURA_SMS_REQ_4006_4010_4011_4030_CONTENT_TEMPLATE_3,
                                                                      new String[] { alertInfo.getExpiryDateDisplayTime(),
                                                                          String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()),
                                                                          dollarFormat(rateInfo.getEmsRate1().getRateAmount()),
                                                                          alertInfo.getExpiryDateDisplayTime(), rate2EndTime, servieFee, });
                            
                        } else {
                            logger.info("++++++ AURA_SMS_REQ_4030_4010_CONTENT_TEMPLATE_2  +++++");
                            content = this.messageAccessor.getMessage(AURA_SMS_REQ_4030_4010_CONTENT_TEMPLATE_2, new String[] {
                                alertInfo.getExpiryDateDisplayTime(), String.valueOf(rateInfo.getEmsRate1().getMinExtensionMinutes()),
                                String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()), dollarFormat(rateInfo.getEmsRate1().getRateAmount()),
                                alertInfo.getExpiryDateDisplayTime(), alertInfo.getExpiryDateDisplayTime(newExpiryDateGMT), servieFee, });
                        }
                    }
                }
                break;
            case AURA_REQ_4054:
                logger.info("++++++ AURA_SMS_REQ_4054_CONTENT_TEMPLATE  +++++");
                content = this.messageAccessor.getMessage(AURA_SMS_REQ_4054_CONTENT_TEMPLATE, new String[] { alertInfo.getExpiryDateDisplayTime() });
                break;
            case AURA_REQ_4057:
                logger.info("++++++ AURA_SMS_REQ_4057_CONTENT_TEMPLATE  +++++");
                content = this.messageAccessor.getMessage(AURA_SMS_REQ_4057_CONTENT_TEMPLATE, new String[] { alertInfo.getExpiryDateDisplayTime() });
                break;
            case AURA_REQ_4058:
                logger.info("++++++ AURA_SMS_REQ_4058_CONTENT_TEMPLATE  +++++");
                content = this.messageAccessor.getMessage(AURA_SMS_REQ_4058_CONTENT_TEMPLATE, new String[] { alertInfo.getExpiryDateDisplayTime() });
                break;
            case AURA_REQ_4072:
                logger.info("++++++ AURA_SMS_REQ_4072_CONTENT_TEMPLATE  +++++");
                content = this.messageAccessor.getMessage(AURA_SMS_REQ_4072_CONTENT_TEMPLATE,
                                                          new String[] { alertInfo.getExpiryDateDisplayTime(), alertInfo.getPlateNumber(),
                                                              dollarFormat(rateInfo.getTotalChargeCent(minutesToAdd)),
                                                              String.valueOf(alertInfo.getLast4Digit()), });
                break;
            case AURA_REQ_4073:
                logger.info("++++++ AURA_SMS_REQ_4073_CONTENT_TEMPLATE  +++++");
                content = this.messageAccessor.getMessage(AURA_SMS_REQ_4073_CONTENT_TEMPLATE,
                                                          new String[] { alertInfo.getExpiryDateDisplayTime(), alertInfo.getPlateNumber(),
                                                              dollarFormat(rateInfo.getTotalChargeCent(minutesToAdd)),
                                                              String.valueOf(alertInfo.getLast4Digit()), });
                break;
            case AURA_REQ_4074:
                logger.info("++++++ AURA_SMS_REQ_4074_CONTENT_TEMPLATE  +++++");
                content = this.messageAccessor
                        .getMessage(AURA_SMS_REQ_4074_CONTENT_TEMPLATE,
                                    new String[] { alertInfo.getExpiryDateDisplayTime(), String.valueOf(alertInfo.getStallNumber()),
                                        dollarFormat(rateInfo.getTotalChargeCent(minutesToAdd)), String.valueOf(alertInfo.getLast4Digit()), });
                break;
            case AURA_REQ_4075:
                logger.info("++++++ AURA_SMS_REQ_4075_CONTENT_TEMPLATE  +++++");
                content = this.messageAccessor
                        .getMessage(AURA_SMS_REQ_4075_CONTENT_TEMPLATE,
                                    new String[] { alertInfo.getExpiryDateDisplayTime(), String.valueOf(alertInfo.getStallNumber()),
                                        dollarFormat(rateInfo.getTotalChargeCent(minutesToAdd)), String.valueOf(alertInfo.getLast4Digit()), });
                break;
            
            case AURA_REQ_4082:
                logger.info("++++++ AURA_SMS_REQ_4082_CONTENT_TEMPLATE  +++++");
                final int maxMinutes;
                if (rateInfo.getIsRate2Need()) {
                    if (rateInfo.isSpecialExtentionForNextFreeParking()) {
                        maxMinutes = rateInfo.getMaxExtendedTimeUnderRate1();
                        
                    } else if (rateInfo.isSpecialExtentionForNextNoParking() || rateInfo.isSpecialExtentionForSingleRate()) {
                        maxMinutes = rateInfo.getMaxAllowedParkingTimeInMinutes();
                    } else {
                        if (rateInfo.isSecondRateFreeParking(expiryDateLocal)) {
                            maxMinutes = rateInfo.getMaxExtensionTimeForFreeParking();
                        } else {
                            maxMinutes = rateInfo.getMaxAllowedParkingTimeInMinutes();
                        }
                    }
                } else {
                    if (rateInfo.hasFirstRateFreeParking(expiryDateLocal)) {
                        maxMinutes = rateInfo.getMaxExtensionTimeForFreeParking();
                    } else {
                        if (rateInfo.isSpecialExtentionForNextFreeParking()) {
                            maxMinutes = rateInfo.getMaxExtendedTimeUnderRate1();
                        } else {
                            maxMinutes = rateInfo.getMaxAllowedParkingTimeInMinutes();
                        }
                    }
                }
                
                if (rateInfo.getEmsRate1() != null && rateInfo.getEmsRate1().getMinExtensionMinutes() < maxMinutes) {
                    content = this.messageAccessor.getMessage(AURA_SMS_REQ_4082_CONTENT_TEMPLATE, new String[] { alertInfo.getExpiryDateDisplayTime(),
                        String.valueOf(rateInfo.getEmsRate1().getMinExtensionMinutes()), String.valueOf(maxMinutes), });
                } else {
                    content = this.messageAccessor.getMessage(AURA_SMS_REQ_4082_CONTENT_TEMPLATE1,
                                                              new String[] { alertInfo.getExpiryDateDisplayTime(), String.valueOf(maxMinutes) });
                }
                break;
            
            case AURA_REQ_4083:
                final String endTimeOfParking =
                        addMinutesInDateForDisplay(alertInfo.getExpiryDateLocal(), rateInfo.getMaxAllowedParkingTimeInMinutes());
                logger.info("++++++ AURA_SMS_REQ_4083_CONTENT_TEMPLATE  +++++");
                content = this.messageAccessor.getMessage(AURA_SMS_REQ_4083_CONTENT_TEMPLATE, new String[] { alertInfo.getExpiryDateDisplayTime(),
                    String.valueOf(rateInfo.getMaxAllowedParkingTimeInMinutes()), alertInfo.getExpiryDateDisplayTime(), endTimeOfParking, });
            default:
                break;
        }
        
        final StringBuilder bdr = new StringBuilder();
        bdr.append("+++ SMS message for mobile number(").append(alertInfo.getMobileNumber()).append(") and ps(").append(alertInfo.getPointOfSaleId());
        bdr.append(")\n").append(content);
        logger.info(bdr.toString());
        
        return content;
    }
    
    private void logSmsTransaction(final Permit permit, final int logType) {
        final SmsTransactionLog smsTransactionLog = new SmsTransactionLog();
        final SmsMessageType smsMessageType = new SmsMessageType();
        smsMessageType.setId(logType);
        smsTransactionLog.setSmsMessageType(smsMessageType);
        smsTransactionLog.setMobileNumber(permit.getMobileNumber());
        final Purchase p = this.purchaseService.findPurchaseById(permit.getPurchase().getId(), false);
        smsTransactionLog.setPointOfSale(this.pointOfSaleService.findPointOfSaleById(p.getPointOfSale().getId()));
        smsTransactionLog.setPurchaseGmt(permit.getPermitBeginGmt());
        smsTransactionLog.setTicketNumber(permit.getPermitNumber());
        smsTransactionLog.setExpiryDateGmt(permit.getPermitExpireGmt());
        smsTransactionLog.setTimeStampGmt(new Date());
        
        this.smsTransactionLogService.saveSmsTransactionLog(smsTransactionLog);
    }
    
    private String dollarFormat(final int cents) {
        return DOLLAR_FORMATTER.format(cents / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
    }
    
    private String minuteFormatter(final int minute) {
        return HOUR_MINUTE_FORMATTER.format(minute);
    }
    
    private String hourFormatter(final int hour, final boolean isEndTime) {
        String ret = HOUR_MINUTE_FORMATTER.format(hour);
        if (isEndTime && ret.equals("00")) {
            ret = "24";
        }
        return ret;
    }
    
    private String addMinutesInDateForDisplay(final Date date, final int minutes) {
        final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, minutes);
        
        return sdf.format(cal.getTime());
    }
    
    public final SmsAlertInfo getSmsAlertByParkerReply(final String mobileNumber) {
        return this.extensiblePermitService.getSmsAlertByParkerReply(mobileNumber);
    }
    
    public final void logSmsTransaction(final SmsAlertInfo smsAlertInfo, final int logType, final String userResponse) {
        final SmsTransactionLog smsTransactionLog = new SmsTransactionLog();
        
        final PointOfSale pos = new PointOfSale();
        pos.setId(smsAlertInfo.getPointOfSaleId());
        smsTransactionLog.setPointOfSale(pos);
        
        final SmsMessageType smsMessageType = new SmsMessageType();
        smsMessageType.setId(logType);
        smsTransactionLog.setSmsMessageType(smsMessageType);
        
        smsTransactionLog.setPurchaseGmt(smsAlertInfo.getPurchasedDate());
        smsTransactionLog.setTicketNumber(smsAlertInfo.getTicketNumber());
        smsTransactionLog.setExpiryDateGmt(smsAlertInfo.getExpiryDate());
        smsTransactionLog.setMobileNumber(this.mobileNumberService.findMobileNumber(smsAlertInfo.getMobileNumber()));
        final String escapedResponse = StringEscapeUtils.escapeHtml(userResponse);
        if (escapedResponse.length() > StandardConstants.LIMIT_15) {
            smsTransactionLog.setConsumerResponse(escapedResponse.substring(0, StandardConstants.LIMIT_15));
        } else {
            smsTransactionLog.setConsumerResponse(escapedResponse);
        }
        
        smsTransactionLog.setTimeStampGmt(new Date());
        
        this.smsTransactionLogService.saveSmsTransactionLog(smsTransactionLog);
    }
    
    public final void setJob(final SmsPermitAlertJob job) {
        this.job = job;
    }
}
