package com.digitalpaytech.scheduling.alert.support;

import java.util.Vector;

public class ActionServices {
    private Vector<ActionService> services;
    
    public ActionServices() {
        this.services = new Vector<ActionService>();
    }
    
    public final void addService(final ActionService service) {
        this.services.add(service);
    }
    
    public final Vector<ActionService> getServices() {
        return this.services;
    }
}
