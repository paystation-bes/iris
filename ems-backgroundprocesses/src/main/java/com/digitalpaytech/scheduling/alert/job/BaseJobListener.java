package com.digitalpaytech.scheduling.alert.job;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;

import com.digitalpaytech.scheduling.alert.support.ActionServiceStopper;

public class BaseJobListener implements JobListener {
    protected static final Logger LOGGER = Logger.getLogger(BaseJobListener.class);
    
    protected String jobName;
    protected int runningTime;
    
    public BaseJobListener(final String jobName, final int timeInterval) {
        super();
        this.jobName = jobName;
        this.runningTime = timeInterval;
    }
    
    public final String getName() {
        return this.jobName;
    }

    public void jobExecutionVetoed(final JobExecutionContext context) {
        
    }
    
    public final void jobToBeExecuted(final JobExecutionContext context) {
        new ActionServiceStopper(context, this.runningTime).start();
    }
    
    public void jobWasExecuted(final JobExecutionContext context1, final JobExecutionException exception) {
    }
}
