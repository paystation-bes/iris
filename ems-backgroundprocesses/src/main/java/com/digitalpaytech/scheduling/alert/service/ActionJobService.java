package com.digitalpaytech.scheduling.alert.service;

public interface ActionJobService {
    String JOB_SERVICE_CLASS = "jobService";
    
    /**
     * Execute action service
     */
    void runService();
    
    /**
     * Stop action service
     */
    void stopService();
    
    boolean isRunning();
    
    void setRunning(boolean isRunning);
    
    boolean isRunningRecovery();
    
    void setRunningRecovery(boolean isRunningRecovery);
    
}
