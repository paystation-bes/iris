package com.digitalpaytech.scheduling.alert.service;

import com.digitalpaytech.scheduling.alert.job.CollectionAlertJob;
import com.digitalpaytech.scheduling.alert.job.CollectionRecoveryAlertJob;

public interface CollectionAlertJobService extends ActionJobService {
    void retrieveAlertFromDB();
    
    void sendAlert(CollectionAlertJob job);
    
    void retrieveRecoveryAlertFromDB();
    
    void sendRecoveryAlert(CollectionRecoveryAlertJob job);
    
    void clearAlertPushQueue();
    
    void clearRecoveryPushQueue();
}
