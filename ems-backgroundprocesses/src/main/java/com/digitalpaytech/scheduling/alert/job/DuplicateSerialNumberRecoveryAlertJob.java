package com.digitalpaytech.scheduling.alert.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.digitalpaytech.scheduling.alert.service.ActionJobService;
import com.digitalpaytech.scheduling.alert.service.DuplicateSerialNumberJobService;
import com.digitalpaytech.util.StandardConstants;

/**
 * Collection recovery alert for separate collection (credit card dollar value)
 * 
 * @author Brian Kim
 * 
 */
public class DuplicateSerialNumberRecoveryAlertJob extends BaseInterruptableJob {
    
    private DuplicateSerialNumberJobService duplicateSerialNumberJobService;
    
    @Override
    public final void execute(final JobExecutionContext context) throws JobExecutionException {
        //        LOGGER.info("+++ SeparateCollectionCreditCardDollarsRecoveryAlertJob started +++");
        super.execute(context);
        this.duplicateSerialNumberJobService = (DuplicateSerialNumberJobService) context.getJobDetail().getJobDataMap()
                .get(ActionJobService.JOB_SERVICE_CLASS);
        while (!this.interrupted) {
            if (!this.duplicateSerialNumberJobService.isRunningRecovery()) {
                this.duplicateSerialNumberJobService.retrieveRecoveryAlertFromDB();
            } else {
                try {
                    if (LOGGER.isTraceEnabled()) {
                        LOGGER.trace(new StringBuilder("+++ DuplicateSerialNumberRecoveryAlertJob already runnning, sleep 1 second"));
                    }
                    
                    Thread.sleep(StandardConstants.SECONDS_IN_MILLIS_1);
                } catch (InterruptedException e) {
                }
            }
            if (this.interrupted) {
                this.duplicateSerialNumberJobService.setRunningRecovery(false);
                return;
            }
        }
    }
    
    @Override
    public final void clearData() {
        // Don't do anything
    }
}
