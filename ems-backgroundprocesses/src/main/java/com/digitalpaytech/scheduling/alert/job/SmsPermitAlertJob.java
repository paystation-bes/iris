package com.digitalpaytech.scheduling.alert.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.digitalpaytech.scheduling.alert.service.ActionJobService;
import com.digitalpaytech.scheduling.alert.service.SmsPermitAlertJobService;

public class SmsPermitAlertJob extends BaseInterruptableJob {
    
    private SmsPermitAlertJobService smsPermitAlertJobService;
    private boolean isStartup;
    
    @Override
    public final void execute(final JobExecutionContext context) throws JobExecutionException {
        super.execute(context);
        //        logger.info("+++ SmsPermitAlertJob started +++");
        this.isStartup = true;
        this.smsPermitAlertJobService = (SmsPermitAlertJobService) context.getJobDetail().getJobDataMap().get(ActionJobService.JOB_SERVICE_CLASS);
        while (!this.interrupted) {
            this.smsPermitAlertJobService.sendSMS(this);
            this.isStartup = false;
            if (this.interrupted) {
                return;
            }
        }
    }
    
    @Override
    public final void clearData() {
        if (this.smsPermitAlertJobService != null) {
            this.smsPermitAlertJobService.clearSMSAlertPushQueue();
            this.smsPermitAlertJobService.clearExpiredAlertAndPermit();
        }
    }
    
    public final boolean isStartup() {
        return this.isStartup;
    }
}
