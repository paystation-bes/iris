package com.digitalpaytech.scheduling.alert.job;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.digitalpaytech.scheduling.alert.service.ActionJobService;
import com.digitalpaytech.scheduling.alert.service.RunningTotalJobService;

public class PeriodicRunningTotalUpdateJob extends BaseInterruptableJob {
    private static Logger log = Logger.getLogger(PeriodicRunningTotalUpdateJob.class);
    private RunningTotalJobService runningTotalJobService;
    private boolean callSetLockFlag;
    private boolean callCalcPayBalFlag;
    private int collectionLockId;
    
    @Override
    public final void execute(final JobExecutionContext context) throws JobExecutionException {
        log.info("=========== PeriodicRunningTotalUpdateJob started ===========");
        super.execute(context);
        this.runningTotalJobService = (RunningTotalJobService) context.getJobDetail().getJobDataMap().get(ActionJobService.JOB_SERVICE_CLASS);
        
        if (this.runningTotalJobService != null && this.runningTotalJobService.getClusterId() != 0) {
            while (!this.interrupted) {
                this.collectionLockId = 0;
                // public int setLock(int clusterId, int
                // numberOfPaystionToBeExecuted)
                
                this.collectionLockId = this.runningTotalJobService.setLock(this.runningTotalJobService.getClusterId(),
                    this.runningTotalJobService.getNumberOfPaystationsToBeExecuted());
                this.callSetLockFlag = true;
                
                if (this.collectionLockId > 0) {
                    // public void calculatePaystationBalance(int
                    // collectionLockId, int timeInterval, int preAuthDelayTime)
                    this.runningTotalJobService.calculatePaystationBalance(this.collectionLockId,
                        this.runningTotalJobService.getRunningTotalCalcTimeInterval(), this.runningTotalJobService.getPreAuthDelayTime());
                    this.callCalcPayBalFlag = true;
                } else if (this.collectionLockId == 0) {
                    // numRows is zero, complete the job.
                    log.info("setLock returns 0, PeriodicRunningTotalUpdateJob exits now");
                    return;
                }
                
                if (this.interrupted) {
                    log.warn("=========== execute method, after 'setLock', interrupted is called !! Exit out of PeriodicRunningTotalUpdateJob now. ===========");
                    return;
                }
            }
        }
        
    }
    
    @Override
    public final void clearData() {
        log.warn("=========== InterruptableJob interface, public void interrupt() is CALLED !!! ===========");
        log.warn(getObjectInfo());
    }
    
    private String getObjectInfo() {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("job name: ").append(jobName).append("\n");
        if (this.runningTotalJobService != null) {
            bdr.append("ClusterId: ").append(this.runningTotalJobService.getClusterId()).append(", ");
            bdr.append("CollectionLockId: ").append(this.collectionLockId).append(", ");
            bdr.append("NumberOfPaystationsToBeExecuted: ").append(this.runningTotalJobService.getNumberOfPaystationsToBeExecuted()).append(", ");
            bdr.append("PreAuthDelayTime: ").append(this.runningTotalJobService.getPreAuthDelayTime()).append(", ");
            bdr.append("RunningTotalCalcTimeInterval: ").append(this.runningTotalJobService.getRunningTotalCalcTimeInterval()).append(", ");
            bdr.append("is 'sp_CollectionSetLock' called? ").append(this.callSetLockFlag).append(", ");
            bdr.append("is 'sp_CollectionRecalcBalance' called? ").append(this.callCalcPayBalFlag);
        } else {
            bdr.append("runningTotalJobService is null !!");
        }
        return bdr.toString();
    }
    
    public static Logger getLog() {
        return log;
    }
    
    public static void setLog(final Logger log) {
        PeriodicRunningTotalUpdateJob.log = log;
    }
    
    public final boolean isCallSetLockFlag() {
        return this.callSetLockFlag;
    }
    
    public final void setCallSetLockFlag(final boolean callSetLockFlag) {
        this.callSetLockFlag = callSetLockFlag;
    }
    
    public final boolean isCallCalcPayBalFlag() {
        return this.callCalcPayBalFlag;
    }
    
    public final void setCallCalcPayBalFlag(final boolean callCalcPayBalFlag) {
        this.callCalcPayBalFlag = callCalcPayBalFlag;
    }
    
    public final int getCollectionLockId() {
        return this.collectionLockId;
    }
    
    public final void setCollectionLockId(final int collectionLockId) {
        this.collectionLockId = collectionLockId;
    }
}
