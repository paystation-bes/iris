package com.digitalpaytech.scheduling.alert.support;

import org.apache.log4j.Logger;

import com.digitalpaytech.scheduling.alert.service.SmsPermitAlertJobService;
import com.digitalpaytech.util.EMSThread;

public class SmsPermitAlertExtractionThread extends EMSThread {
    
    private static final Logger LOGGER = Logger.getLogger(SmsPermitAlertExtractionThread.class);
    
    private SmsPermitAlertJobService smsPermitAlertJobService;
    
    public SmsPermitAlertExtractionThread(final SmsPermitAlertJobService smsPermitAlertJobService) {
        super(SmsPermitAlertExtractionThread.class.getName());
        this.smsPermitAlertJobService = smsPermitAlertJobService;
        setPriority(Thread.NORM_PRIORITY);
    }
    
    public final void run() {
        //        LOGGER.info("+++ SmsPermitAlertExtractionThread startup +++");
        while (isRunning()) {
            //            LOGGER.debug("+++  SmsPermitAlertExtractionThread wakes up +++");
            
            try {
                if (this.smsPermitAlertJobService.isNeedPause()) {
                    LOGGER.trace("+++ not the scheduled time for SmsPermitAlertExtractionThread, wait +++ ");
                    synchronized (this) {
                        wait();
                    }
                } else {
                    LOGGER.trace("+++ Start loading SMSAlert from database +++");
                    
                    final int alerts = this.smsPermitAlertJobService.retrieveSMSAlertFromDB();
                    LOGGER.trace("+++ End loading SmsAlerts from database, size: " + alerts + " +++");
                    if (alerts > 0) {
                        //                        LOGGER.debug("Found " + alerts + " Sms Alert, notify Processing Thread(s) that more data is coming.");
                        this.smsPermitAlertJobService.notifyMoreDataComing();
                    }
                    LOGGER.trace("++++ finish loading SmsAlerts from  database, wait +++");
                    synchronized (this) {
                        wait();
                    }
                    
                    LOGGER.trace("+++ End loading SmsAlert from database +++");
                }
            } catch (InterruptedException e) {
                LOGGER.trace("+++ SmsPermitAlertExtractionThread wait interrupted: ", e);
            } catch (Exception e) {
                LOGGER.error("++++ EXCEPTION in SmsPermitAlertExtractionThread ++++", e);
            }
        }
        LOGGER.warn("!!! SmsPermitAlertExtractionThread stopped !!!");
    }
    
    public final void wakeup() {
        notify();
        
    }
    
    public final boolean isRunning() {
        return !isShutdown();
    }
}
