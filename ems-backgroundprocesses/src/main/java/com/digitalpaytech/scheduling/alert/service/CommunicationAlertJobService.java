package com.digitalpaytech.scheduling.alert.service;

public interface CommunicationAlertJobService extends ActionJobService {
    
    void scheduleCommunicationQueue();
}
