package com.digitalpaytech.scheduling.alert.service;

import com.digitalpaytech.scheduling.alert.job.PeriodicRunningTotalUpdateJob;

/**
 * This service will call the stored procedure in order to update running total
 * periodically.
 * 
 * @author Brian Kim
 * 
 */
public interface RunningTotalJobService extends ActionJobService {
    int setLock(int clusterId, int numberOfPaystionToBeExecuted);
    
    void calculatePaystationBalance(int collectionLockId, int timeInterval, int preAuthDelayTime);
    
    int getClusterId();
    
    int getRunningTotalCalcTimeInterval();
    
    int getNumberOfPaystationsToBeExecuted();
    
    int getPreAuthDelayTime();
    
    void processCollectionFlagUnsettledPreAuth();
    
    void processCollectionReleaseLock();
    
    void processDeleteCollectionLock();
    
    boolean calculatePaystationBalance(PeriodicRunningTotalUpdateJob job);
}
