package com.digitalpaytech.scheduling.alert.job;

import org.apache.log4j.Logger;
import org.quartz.InterruptableJob;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.UnableToInterruptJobException;
import org.quartz.impl.JobDetailImpl;

public abstract class BaseInterruptableJob implements InterruptableJob {
    protected static final Logger LOGGER = Logger.getLogger(BaseInterruptableJob.class);
    
    protected boolean interrupted;
    protected String jobName;
    protected String jobId;
    
    public final void interrupt() throws UnableToInterruptJobException {
        this.interrupted = true;
        clearData();
        
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("+++ " + this.jobName + " [" + this.jobId + "] stopped +++");
        }
    }
    
    public void execute(final JobExecutionContext context) throws JobExecutionException {
        this.interrupted = false;
        final JobDetail jobDtl = context.getJobDetail();
        this.jobName = ((JobDetailImpl) jobDtl).getName();
        this.jobId = jobDtl.getKey().getGroup() + "." + jobDtl.getKey().getName();
        
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("+++ " + this.jobName + " [" + this.jobId + "] started +++");
        }
    }
    
    public final boolean getIsInterrupted() {
        return this.interrupted;
    }
    
    public abstract void clearData();
}
