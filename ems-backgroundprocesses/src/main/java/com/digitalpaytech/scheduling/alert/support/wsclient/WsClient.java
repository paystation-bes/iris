package com.digitalpaytech.scheduling.alert.support.wsclient;

import com.upsidewireless.webservice.sms.SMSSendResult;

public interface WsClient {
    Object sendRequest(Object request) throws Exception;
    
    SMSSendResult sendRequest() throws Exception;
    
    void setMobileNumber(String mobileNumber);
    
    void setMessageBody(String messageBody);
}
