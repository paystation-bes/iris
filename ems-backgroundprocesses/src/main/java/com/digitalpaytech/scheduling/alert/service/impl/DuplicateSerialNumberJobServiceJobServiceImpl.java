package com.digitalpaytech.scheduling.alert.service.impl;

import java.util.Collection;

import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.scheduling.alert.job.DuplicateSerialNumberRecoveryAlertJob;
import com.digitalpaytech.scheduling.alert.service.DuplicateSerialNumberJobService;
import com.digitalpaytech.service.PosEventCurrentService;
import com.digitalpaytech.service.PosMacAddressService;
import com.digitalpaytech.service.queue.QueueEventService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

@Service("duplicateSerialNumberJobService")
public class DuplicateSerialNumberJobServiceJobServiceImpl extends ActionJobServiceImpl implements DuplicateSerialNumberJobService {
    
    private static final String RECOVERY_ALERT = "DuplicateSerialNumberRecovery";
    private static final String GROUP = "Pay Station Alert";
    
    @Autowired
    protected PosMacAddressService posMacAddressService;
    
    @Autowired
    protected PosEventCurrentService posEventCurrentService;
    
    @Autowired
    protected QueueEventService queueEventService;
    
    public final void setPosMacAddressService(final PosMacAddressService posMacAddressService) {
        this.posMacAddressService = posMacAddressService;
    }
    
    @Override
    public final void runService() {
        try {
            actionService = this.getActionService(WebCoreConstants.ACTION_SERVICE_DUPLICATE_SERIAL_NUMBER);
            if (actionService == null || actionService.getServerInfo().size() == 0) {
                logger.info("+++ no actions in this server +++");
            } else {
                scheduleJob(DuplicateSerialNumberRecoveryAlertJob.class, RECOVERY_ALERT, GROUP);
                
            }
        } catch (SchedulerException e) {
            logger.error("SchedulerException: ", e);
        }
    }
    
    //TEMPORARY Old code will be removed
    @SuppressWarnings("checkstyle:cyclomaticcomplexity")
    public final void retrieveRecoveryAlertFromDB() {
        setRunningRecovery(true);
        if (logger.isTraceEnabled()) {
            logger.trace("+++ retrieveRecoveryAlertFromDB method called [loading " + actionService.getDataLoadingRows() + " rows] +++");
        }
        
        //        logger.debug("actionService.getDataLoadingRows() : " + actionService.getDataLoadingRows());
        final Collection<PosEventCurrent> activePosEvents = this.posEventCurrentService.findNextRecoveryDuplicateSerialNumberData(actionService
                .getDataLoadingRows());
        
        if (activePosEvents == null || activePosEvents.isEmpty()) {
            logger.trace("---- retrieved 0 double serial number recovery data from db ----");
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("---- retrieved double serial number recovery data from db ----");
                for (PosEventCurrent posEventCurrent : activePosEvents) {
                    logger.debug("Double SerialNumber - PointOfSale Id:" + posEventCurrent.getPointOfSale().getId());
                    this.queueEventService
                            .createEventQueue(posEventCurrent.getPointOfSale().getId(), null, null, posEventCurrent.getEventType(),
                                              WebCoreConstants.SEVERITY_CLEAR, null, DateUtil.getCurrentGmtDate(), null, false, posEventCurrent
                                                      .getEventType().isIsNotification(), WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID,
                                              (byte) (WebCoreConstants.QUEUE_TYPE_RECENT_EVENT | WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT));
                }
            }
        }
        
        if (activePosEvents.isEmpty()) {
            setRunningRecovery(false);
            try {
                if (logger.isTraceEnabled()) {
                    logger.trace("+++ no pay station double serial number recovery alert, sleep " + super.actionService.getSleepTime()
                                 + " seconds +++");
                }
                
                Thread.sleep(StandardConstants.SECONDS_IN_MILLIS_1 * super.actionService.getSleepTime());
            } catch (InterruptedException e) {
                //                logger.warn(e);
            }
        }
    }
}
