package com.digitalpaytech.scheduling.alert.support.wsclient.upsidewireless;

import com.digitalpaytech.scheduling.alert.support.wsclient.WsClient;
import com.upsidewireless.webservice.sms.SMS;
import com.upsidewireless.webservice.sms.SMSSendResult;
import com.upsidewireless.webservice.sms.SMSSoap;
import com.upsidewireless.webservice.sms.SmsEncoding;

public class UpsidewirelessServiceClient implements WsClient {
    private SMS sms;
    private String token;
    private String signature;
    private String messageBody;
    private String mobileNumber;
    
    public UpsidewirelessServiceClient() {
        this.sms = new SMS();
    }
    
    public UpsidewirelessServiceClient(final String token, final String signature) {
        this();
        this.token = token;
        this.signature = signature;
    }
    
    public UpsidewirelessServiceClient(final String token, final String signature, final String messageBody, final String mobileNumber) {
        this();
        this.token = token;
        this.signature = signature;
        this.messageBody = messageBody;
        this.mobileNumber = mobileNumber;
    }
    
    public final SMSSendResult sendRequest() throws Exception {
        final SMSSoap soap = this.sms.getSMSSoap();
        final SMSSendResult result = soap.sendPlain(this.token, this.signature, this.mobileNumber, this.messageBody, SmsEncoding.SEVEN);
        
        return result;
    }
    
    public final Object sendRequest(final Object request) throws Exception {
        /* Not use this method. Instead, sendRequest() will be used. */
        return sendRequest();
    }
    
    public final String getToken() {
        return this.token;
    }
    
    public final void setToken(final String token) {
        this.token = token;
    }
    
    public final String getSignature() {
        return this.signature;
    }
    
    public final void setSignature(final String signature) {
        this.signature = signature;
    }
    
    public final String getMessageBody() {
        return this.messageBody;
    }
    
    public final void setMessageBody(final String messageBody) {
        this.messageBody = messageBody;
    }
    
    public final String getMobileNumber() {
        return this.mobileNumber;
    }
    
    public final void setMobileNumber(final String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
