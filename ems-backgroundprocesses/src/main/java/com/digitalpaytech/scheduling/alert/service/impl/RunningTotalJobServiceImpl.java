package com.digitalpaytech.scheduling.alert.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.scheduling.alert.job.PeriodicRunningTotalUpdateJob;
import com.digitalpaytech.scheduling.alert.service.RunningTotalJobService;
import com.digitalpaytech.service.AlertsService;
import com.digitalpaytech.service.CollectionTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.UserDefinedEventInfoService;
import com.digitalpaytech.service.queue.QueueCustomerAlertTypeService;
import com.digitalpaytech.util.WebCoreConstants;

/**
 * This service will call the stored procedure in order to update running total
 * periodically.
 * 
 * @author Brian Kim
 * 
 */
@Service("runningTotalJobService")
public class RunningTotalJobServiceImpl extends ActionJobServiceImpl implements RunningTotalJobService {
    private static final Logger LOG = Logger.getLogger(RunningTotalJobServiceImpl.class);
    private static final String PERIODIC_RUNNING_TOTAL_UPDATE = "PeriodicRunningTotalUpdate";
    private static final String GROUP = "Collection";
    
    @Autowired
    private CollectionTypeService collectionTypeService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private AlertsService alertsService;
    
    @Autowired
    private UserDefinedEventInfoService userDefinedEventInfoService;
    
    @Autowired
    private QueueCustomerAlertTypeService queueCustomerAlertTypeService;
    
    public final CollectionTypeService getCollectionTypeService() {
        return this.collectionTypeService;
    }
    
    public final void setCollectionTypeService(final CollectionTypeService collectionTypeService) {
        this.collectionTypeService = collectionTypeService;
    }
    
    public final EmsPropertiesService getEmsPropertiesService() {
        return this.emsPropertiesService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final AlertsService getAlertsService() {
        return this.alertsService;
    }
    
    public final void setAlertsService(final AlertsService alertsService) {
        this.alertsService = alertsService;
    }
    
    public final UserDefinedEventInfoService getUserDefinedEventInfoService() {
        return this.userDefinedEventInfoService;
    }
    
    public final void setUserDefinedEventInfoService(final UserDefinedEventInfoService userDefinedEventInfoService) {
        this.userDefinedEventInfoService = userDefinedEventInfoService;
    }
    
    public final QueueCustomerAlertTypeService getQueueCustomerAlertTypeService() {
        return this.queueCustomerAlertTypeService;
    }
    
    public final void setQueueCustomerAlertTypeService(final QueueCustomerAlertTypeService queueCustomerAlertTypeService) {
        this.queueCustomerAlertTypeService = queueCustomerAlertTypeService;
    }
    
    @Override
    public final void runService() {
        try {
            actionService = this.getActionService(WebCoreConstants.ACTION_SERVICE_PERIODIC_RUNNING_TOTAL_UPDATE);
            if (actionService == null || actionService.getServerInfo().size() == 0) {
                LOG.info("+++ no actions in this server +++");
            } else {
                scheduleJob(PeriodicRunningTotalUpdateJob.class, PERIODIC_RUNNING_TOTAL_UPDATE, GROUP);
                
                /*
                 * SchedulerFactory sf = new StdSchedulerFactory();
                 * Scheduler scheduler = sf.getScheduler();
                 * scheduler.start();
                 * ServerInfo serverInfo = actionService.getServerInfo().get(0);
                 * if (scheduler.isShutdown()) {
                 * logger.warn("!!!! Scheduler is shutdown, job cannot be executed !!!!!");
                 * } else {
                 * JobDetail runningTotalUpdateJobDetail = JobBuilder.newJob(PeriodicRunningTotalUpdateJob.class)
                 * .withIdentity(PERIODIC_RUNNING_TOTAL_UPDATE, GROUP).build();
                 * runningTotalUpdateJobDetail.getJobDataMap().put(JOB_SERVICE_CLASS, this);
                 * CronTrigger cronTrigger1 = TriggerBuilder.newTrigger().withIdentity(PERIODIC_RUNNING_TOTAL_UPDATE, GROUP)
                 * .withSchedule(CronScheduleBuilder.cronSchedule(serverInfo.getCronExpression())).build();
                 * BaseJobListener runningTotalUpdateJobListener = new BaseJobListener(PERIODIC_RUNNING_TOTAL_UPDATE, serverInfo.getRunningTime());
                 * scheduler.getListenerManager().addJobListener(runningTotalUpdateJobListener);
                 * scheduler.scheduleJob(runningTotalUpdateJobDetail, cronTrigger1);
                 * }
                 */
            }
        } catch (SchedulerException e) {
            LOG.error("SchedulerException: ", e);
        }
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public final boolean calculatePaystationBalance(final PeriodicRunningTotalUpdateJob job) {
        job.setCollectionLockId(setLock(getClusterId(), getNumberOfPaystationsToBeExecuted()));
        job.setCallSetLockFlag(true);
        if (job.getCollectionLockId() > 0) {
            calculatePaystationBalance(job.getCollectionLockId(), getRunningTotalCalcTimeInterval(), getPreAuthDelayTime());
            job.setCallCalcPayBalFlag(true);
        } else if (job.getCollectionLockId() == 0) {
            // numRows is zero, complete the job.
            return false;
        }
        return true;
    }
    
    public final int setLock(final int clusterId, final int numberOfPaystionToBeExecuted) {
        return this.collectionTypeService.setLock(clusterId, numberOfPaystionToBeExecuted);
    }
    
    public final void calculatePaystationBalance(final int collectionLockId, final int timeInterval, final int preAuthDelayTime) {
        final List<Integer> posIdList = this.collectionTypeService.calculatePaystationBalance(collectionLockId, timeInterval, preAuthDelayTime);
        
        if (posIdList != null && !posIdList.isEmpty()) {
            this.queueCustomerAlertTypeService.addCustomerAlertTypeEvent(null, null, posIdList);
        }
    }
    
    public final int getClusterId() {
        return this.clusterMemberService.getClusterId();
    }
    
    public final int getRunningTotalCalcTimeInterval() {
        return this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.RUNNING_TOTAL_CALC_TIME_INTERVAL,
                                                               EmsPropertiesService.DEFAULT_RUNNING_TOTAL_CALC_TIME_INTERVAL);
    }
    
    public final int getNumberOfPaystationsToBeExecuted() {
        return this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.NUMBER_OF_PAYSTATIONS_BE_EXECUTED,
                                                               EmsPropertiesService.DEFAULT_NUMBER_OF_PAYSTATIONS_BE_EXECUTED);
    }
    
    public final int getPreAuthDelayTime() {
        return this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.PREAUTH_DELAY_TIME,
                                                               EmsPropertiesService.DEFAULT_PREAUTH_DELAY_TIME);
    }
    
    /**
     * This method is scheduled to call by a cron job scheduler in
     * ems-backgroundprocesses.xml. Trigger: collectionFlagUnsettledPreAuthJobTrigger to
     * execute sp_CollectionFlagUnsettledPreAuth stored procedure.
     */
    public final void processCollectionFlagUnsettledPreAuth() {
        this.collectionTypeService.processCollectionFlagUnsettledPreAuth(getPreAuthDelayTime());
    }
    
    /**
     * This method is scheduled to call by a cron job scheduler in
     * ems-backgroundprocesses.xml. Trigger: collectionReleaseLockJobTrigger to execute
     * sp_CollectionReleaseLock stored procedure.
     * 
     * Run this process on the same tomcat as Running Totals
     */
    public final void processCollectionReleaseLock() {
        if (!isConfiguredServer(PERIODIC_RUNNING_TOTAL_UPDATE)) {
            printIfIsDebug();
            return;
        }
        final int clusterId = this.clusterMemberService.getClusterId();
        this.collectionTypeService.processCollectionReleaseLock(clusterId);
    }
    
    /**
     * This method is scheduled to call by cron job scheduler in ems-backgroundprocesses.xml.
     * Trigger: celeteCollectionLockJobTrigger to execute
     * sp_DeleteCollectionLock stored procedure.
     */
    public final void processDeleteCollectionLock() {        
        // run this process on the same tomcat as Running Totals
        if (!isConfiguredServer(PERIODIC_RUNNING_TOTAL_UPDATE)) {
            printIfIsDebug();
            return;
        }                
        this.collectionTypeService.processDeleteCollectionLock();
    }
    
    private void printIfIsDebug() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Method aborted! " + this.clusterMemberService.getClusterName() 
                + " is not the correct server for running total update");
        }
    }
}
