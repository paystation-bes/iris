package com.digitalpaytech.scheduling.alert.support.servlet.upsidewireless;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.digitalpaytech.cardprocessing.CardProcessingManager;
import com.digitalpaytech.domain.ActivePermit;
import com.digitalpaytech.domain.ExtensiblePermit;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PermitIssueType;
import com.digitalpaytech.domain.PermitType;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.SmsAlert;
import com.digitalpaytech.domain.TransactionType;
import com.digitalpaytech.dto.SmsAlertInfo;
import com.digitalpaytech.dto.SmsRateInfo;
import com.digitalpaytech.scheduling.alert.service.SmsPermitAlertJobService;
import com.digitalpaytech.scheduling.sms.support.SmsService;
import com.digitalpaytech.service.ActivePermitService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.ExtensiblePermitService;
import com.digitalpaytech.service.ExtensibleRateAndPermissionService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.PermitService;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.service.PurchaseService;
import com.digitalpaytech.service.SmsAlertService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

@SuppressWarnings({ "PMD.GodClass", "PMD.ExcessiveImports", "checkstyle:illegalcatch" })
public class SmsHttpPostCallbackServlet extends HttpServlet {
    
    private static final long serialVersionUID = 2105048767406984912L;
    private static final Logger LOGGER = Logger.getLogger(SmsHttpPostCallbackServlet.class);
    
    private static final String REQUEST_PARAM_NAME = "name";
    private static final String REQUEST_PARAM_SENDER = "sender";
    private static final String REQUEST_PARAM_DATA = "data";
    private static final String REQUEST_PARAM_AUTHNUM = "authorizationnumber";
    private static final String REQUEST_PARAM_CALLBACKID = "callbackId";
    
    private static final String END_PLUS_LOG_MSG = ") ++++";
    private static final String USER_REPLY_LOG_MSG = ", UserReply: ";
    private static final String SEND_END_CONVERSATION_FAILURE_LOG_MSG = "sendEndConversation failure: ";
    
    private SmsPermitAlertJobService smsPermitAlertJobService;
    private EmsPropertiesService emsPropertiesService;
    private ExtensiblePermitService extensiblePermitService;
    private SmsAlertService smsAlertService;
    private ExtensibleRateAndPermissionService extensibleRateAndPermissionService;
    private CardProcessingManager cardProcessingManager;
    private PermitService permitService;
    private PurchaseService purchaseService;
    private ActivePermitService activePermitService;
    private ProcessorTransactionService processorTransactionService;
    private SmsService client;
    private MerchantAccountService merchantAccountService;
    
    public final void init(final ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
        final WebApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
        this.smsPermitAlertJobService = (SmsPermitAlertJobService) ctx.getBean("smsPermitAlertJobService");
        this.emsPropertiesService = (EmsPropertiesService) ctx.getBean("emsPropertiesService");
        this.extensiblePermitService = (ExtensiblePermitService) ctx.getBean("extensiblePermitService");
        this.smsAlertService = (SmsAlertService) ctx.getBean("smsAlertService");
        this.extensibleRateAndPermissionService = (ExtensibleRateAndPermissionService) ctx.getBean("extensibleRateAndPermissionService");
        this.cardProcessingManager = (CardProcessingManager) ctx.getBean("cardProcessingManager");
        this.permitService = (PermitService) ctx.getBean("permitService");
        this.purchaseService = (PurchaseService) ctx.getBean("purchaseService");
        this.activePermitService = (ActivePermitService) ctx.getBean("activePermitService");
        this.processorTransactionService = (ProcessorTransactionService) ctx.getBean("processorTransactionService");
        this.client = (SmsService) ctx.getBean("smsService");
        this.merchantAccountService = ctx.getBean(MerchantAccountService.class);
    }
    
    public final void doPost(final HttpServletRequest request, final HttpServletResponse response) {
        final String remoteIP = request.getRemoteAddr();
        if (isValidIP(remoteIP)) {
            processRequest(request, response);
        } else {
            LOGGER.warn("++++ WARNING - Invalid IP Address: " + remoteIP + " ++++");
        }
    }
    
    private void processRequest(final HttpServletRequest request, final HttpServletResponse response) {
        // request parameters
        final String senderMobileNumber = request.getParameter(REQUEST_PARAM_SENDER).trim();
        final String senderOriginalReply = request.getParameter(REQUEST_PARAM_DATA);
        final String senderReply = this.getMatchedValue(senderOriginalReply, "\\d+");
        final String accountName = request.getParameter(REQUEST_PARAM_NAME);
        final String authorizationNumber = request.getParameter(REQUEST_PARAM_AUTHNUM);
        final String callbackId = request.getParameter(REQUEST_PARAM_CALLBACKID);
        
        LOGGER.info(new StringBuilder().append("++++ SMS Post CallBack arrived: (MobileNumber: ").append(senderMobileNumber)
                .append(USER_REPLY_LOG_MSG).append(senderOriginalReply).append(", AccountName: ").append(accountName).append(END_PLUS_LOG_MSG)
                .toString());
        
        if (senderMobileNumber != null && !senderMobileNumber.isEmpty()) {
            final SmsAlertInfo smsAlertInfo = this.smsPermitAlertJobService.getSmsAlertByParkerReply(senderMobileNumber);
            
            /*
             * Req.4100: If an SMS message is received from a phone number that is not associated with an active parking transaction,
             * it shall be silently ignored.
             */
            if (smsAlertInfo == null) {
                try {
                    this.client.endConversationRequest(callbackId);
                } catch (Exception e) {
                    LOGGER.error(SEND_END_CONVERSATION_FAILURE_LOG_MSG, e);
                }
                LOGGER.info("++++ NO active parking transaction found. (MobileNumber: " + senderMobileNumber + USER_REPLY_LOG_MSG + senderReply
                            + END_PLUS_LOG_MSG);
            } else {
                final ActivePermit activePermit = this.activePermitService.findActivePermitByOriginalPermitId(smsAlertInfo.getOriginalPermitId());
                if (activePermit == null) {
                    LOGGER.info("SmsHttpPostCallbackServlet.doPost(): No Active Permit found for Permit Id " + smsAlertInfo.getOriginalPermitId());
                    try {
                        response.sendError(HttpStatus.SC_NOT_FOUND);
                    } catch (IOException ioe) {
                        LOGGER.warn("Unable to send HTTP status " + HttpStatus.SC_NOT_FOUND);
                    }
                    return;
                }
                
                /* set the isLock to true. */
                final SmsAlert smsAlert = this.smsAlertService.findUnexpiredSmsAlertById(smsAlertInfo.getSmsAlertId());
                
                if (smsAlert == null) {
                    // If we don't find a matching smsAlert, tell the messaging server to end the conversation    
                    try {
                        this.client.endConversationRequest(callbackId);
                        this.extensiblePermitService.updateSmsAlert(false, 1, false, smsAlertInfo);
                    } catch (Exception e) {
                        LOGGER.error(SEND_END_CONVERSATION_FAILURE_LOG_MSG, e);
                        this.smsPermitAlertJobService.logSmsTransaction(smsAlertInfo, WebCoreConstants.END_CONVERSATION_FAILURE, senderReply);
                    }
                    LOGGER.info("++++ NO unexpired parking transaction found. (MobileNumber: " + senderMobileNumber + USER_REPLY_LOG_MSG + senderReply
                                + END_PLUS_LOG_MSG);
                } else {
                    
                    this.extensiblePermitService.updateSmsAlert(smsAlert.isIsAlerted(), smsAlert.getNumberOfRetries(), true, smsAlertInfo,
                                                                callbackId);
                    
                    /* DB log in SmsTransactionLog table. (4: User response received) */
                    this.smsPermitAlertJobService.logSmsTransaction(smsAlertInfo, WebCoreConstants.USER_RESPONSE_RECEIVED, senderOriginalReply);
                    
                    final SmsRateInfo rateInfo = this.extensibleRateAndPermissionService.getSmsRateInfo(smsAlertInfo);
                    LOGGER.info("++++ SmsRateInfo: " + rateInfo);
                    /*
                     * Req.4080: If a malformed message is received from the parker (empty message or message containing anything other than a valid
                     * number of minutes within the allowed range), an SMS message indicating that the parking session was not extended and
                     * reiterating the valid responses shall be sent.
                     */
                    if (senderReply.matches(WebCoreConstants.REGEX_INTEGER)) {
                        processValidParkerReply(senderMobileNumber, senderReply, authorizationNumber, callbackId, smsAlertInfo, smsAlert, rateInfo);
                    } else {
                        /*
                         * TODO Might need to add a check here if numOfRetries > 0 then don't send a message anymore.Instead end the
                         * conversation because right now it will send two failure msgs responding to 2 invalid responses prompting the parker to
                         * reply again a third time even though we have already ended the conversation on the second invalid response
                         */
                        this.smsPermitAlertJobService.confirmExtensiblePermit(smsAlertInfo, rateInfo, WebCoreConstants.INVALID_USER_RESPONSE_RECEIVED,
                                                                              senderOriginalReply, null, null, null, null);
                    }
                    
                }
                
            }
        }
    }
    
    private void processValidParkerReply(final String senderMobileNumber, final String senderReply, final String authorizationNumber,
        final String callbackId, final SmsAlertInfo smsAlertInfo, final SmsAlert smsAlert, final SmsRateInfo rateInfo) {
        final int repliedMinutes = Integer.parseInt(senderReply);
        if (rateInfo.isValidTime(repliedMinutes)) {
            final Permit originalPermit = this.permitService.findPermitByIdForSms(smsAlertInfo.getOriginalPermitId());
            final Purchase originalPurchase = this.purchaseService.findPurchaseById(originalPermit.getPurchase().getId(), false);
            
            if (originalPermit != null) {
                processChargeAndUpdatePermit(new TransactionChargeDto(senderReply, smsAlertInfo, rateInfo, originalPermit, originalPurchase,
                        senderMobileNumber, authorizationNumber));
            }
        } else {
            if (smsAlert.getNumberOfRetries() == 0) {
                // send the sms message as the input is out of range
                this.smsPermitAlertJobService.confirmExtensiblePermit(smsAlertInfo, rateInfo, WebCoreConstants.INVALID_USER_RESPONSE_RECEIVED,
                                                                      senderReply, null, null, null, null);
            } else {
                /*
                 * End the conversation in the messaging server without sending any sms message because the user has sent an reply that is a valid
                 * integer but not in a valid range and the number of retries is not 0
                 */
                
                try {
                    this.client.endConversationRequest(callbackId);
                } catch (Exception e) {
                    LOGGER.error(SEND_END_CONVERSATION_FAILURE_LOG_MSG, e);
                    this.smsPermitAlertJobService.logSmsTransaction(smsAlertInfo, WebCoreConstants.END_CONVERSATION_FAILURE, senderReply);
                    this.extensiblePermitService.updateSmsAlert(false, 1, false, smsAlertInfo);
                    
                }
                
                this.smsPermitAlertJobService.processPostConfirmation(WebCoreConstants.INVALID_USER_RESPONSE_RECEIVED, smsAlertInfo, senderReply,
                                                                      null, false);
                LOGGER.info("++++ Invalid User input more than 1 time, so gently ignored. (MobileNumber: " + senderMobileNumber + USER_REPLY_LOG_MSG
                            + senderReply + END_PLUS_LOG_MSG);
            }
        }
    }
    
    private void processChargeAndUpdatePermit(final TransactionChargeDto chargeDto) {
        
        final Purchase originalPurchase = chargeDto.getOriginalPurchase();
        final Permit originalPermit = chargeDto.getOriginalPermit();
        final SmsAlertInfo smsAlertInfo = chargeDto.getSmsAlertInfo();
        final SmsRateInfo rateInfo = chargeDto.getRateInfo();
        final String senderReply = chargeDto.getSenderReply();
        
        final ExtensiblePermit extensiblePermit = this.extensiblePermitService.findByMobileNumber(chargeDto.getSenderMobileNumber());
        
        final ProcessorTransaction chargeProcTrans = new ProcessorTransaction();
        chargeProcTrans.setAuthorizationNumber(chargeDto.getAuthorizationNumber());
        chargeProcTrans.setProcessorTransactionType(this.processorTransactionService
                .getProcessorTransactionType(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_PAY_BY_PHONE_CHARGE_WITH_NO_REFUNDS));
        chargeProcTrans.setAmount(originalPurchase.getCardPaidAmount());
        
        final PaymentCard paymentCard = new PaymentCard();
        
        final Permit newPermit = setUpNewPermit(originalPurchase, originalPermit, rateInfo, smsAlertInfo, senderReply);
        
        final ProcessorTransaction originalProcTrans = this.processorTransactionService
                .findProcTransByPurchaseIdTicketNoAndPurchasedDate(originalPurchase.getId(), originalPurchase.getPurchaseGmt(),
                                                                   originalPermit.getPermitNumber());
        
        final MerchantAccount ma =
                this.merchantAccountService.findByPOSAndCardTypeAndNotDeletedAndMerchantStatusEnabled(originalPurchase.getPointOfSale().getId(),
                                                                                                      CardProcessingConstants.CARD_TYPE_CREDIT_CARD);
        
        final int processResult;
        if (CardProcessingUtil.isNotNullAndIsLink(ma)) {
            processResult = this.cardProcessingManager.processLinkCPSTransactionCharge(extensiblePermit, originalProcTrans.getId(), chargeProcTrans,
                                                                                       ma, originalPurchase.getPointOfSale(), newPermit, paymentCard);
        } else {
            processResult = this.cardProcessingManager.processTransactionCharge(extensiblePermit.getCardData(), originalPurchase.getPointOfSale(),
                                                                                newPermit, extensiblePermit.getIsRfid(), chargeProcTrans, paymentCard,
                                                                                originalProcTrans.getId());
        }
        
        switch (processResult) {
            case CardProcessingConstants.AURA_CHARGE_SUCCEED:
                this.smsPermitAlertJobService.confirmExtensiblePermit(smsAlertInfo, rateInfo, WebCoreConstants.EXTENSION_CONFIRMATION_SENT,
                                                                      senderReply, originalPermit, newPermit, chargeProcTrans, paymentCard);
                break;
            case CardProcessingConstants.AURA_CHARGE_DECLINE:
                this.smsPermitAlertJobService.confirmExtensiblePermit(smsAlertInfo, rateInfo, WebCoreConstants.CREDIT_CARD_DECLINED, senderReply,
                                                                      null, null, chargeProcTrans, paymentCard);
                break;
            case CardProcessingConstants.AURA_CHARGE_UNKNOWN:
            case CardProcessingConstants.AURA_CHARGE_SEVER_SHUTDOWN:
            default:
                this.smsPermitAlertJobService.confirmExtensiblePermit(smsAlertInfo, rateInfo, WebCoreConstants.UNABLE_TO_PROCESS_CREDIT_CARD,
                                                                      senderReply, null, null, chargeProcTrans, paymentCard);
                break;
        }
    }
    
    private boolean isValidIP(final String ipAddress) {
        final boolean isValidate = this.emsPropertiesService.getPropertyValueAsBoolean(EmsPropertiesService.SMS_IS_SMS_CALLBACK_IP_VALIDATE,
                                                                                       EmsPropertiesService.SMS_IS_SMS_CALLBACK_IP_VALIDATE_DEFAULT);
        boolean result = false;
        
        if (isValidate) {
            final String ip = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.SMS_CALLBACK_IP_ADDRESS,
                                                                         EmsPropertiesService.SMS_CALLBACK_IP_ADDRESS_DEFAULT);
            final String[] ipArray = ip.split("-");
            
            final long ipAddressInt = Long.parseLong(ipAddress.replace(StandardConstants.STRING_DOT, WebCoreConstants.EMPTY_STRING));
            final long startIP = Long.parseLong(ipArray[0].replace(StandardConstants.STRING_DOT, WebCoreConstants.EMPTY_STRING));
            final long endIP = Long.parseLong(ipArray[1].replace(StandardConstants.STRING_DOT, WebCoreConstants.EMPTY_STRING));
            
            result = ipAddressInt >= startIP && ipAddressInt <= endIP;
        } else {
            result = true;
        }
        
        return result;
    }
    
    private Permit setUpNewPermit(final Purchase originalPurchase, final Permit originalPermit, final SmsRateInfo rateInfo,
        final SmsAlertInfo smsAlertInfo, final String userReply) {
        
        final Date newPurchasedDate = new Date();
        final int chargedAmount = rateInfo.getTotalChargeCent(Integer.parseInt(userReply));
        
        final Purchase newPurchase = new Purchase();
        newPurchase.setCustomer(originalPurchase.getCustomer());
        newPurchase.setPointOfSale(originalPurchase.getPointOfSale());
        newPurchase.setPurchaseGmt(newPurchasedDate);
        newPurchase.setPurchaseNumber(smsAlertInfo.getTicketNumber());
        
        final TransactionType transactionType = new TransactionType();
        transactionType.setId(WebCoreConstants.TRANSACTION_TYPE_EBP_ADDTIME);
        newPurchase.setTransactionType(transactionType);
        newPurchase.setPaymentType(originalPurchase.getPaymentType());
        newPurchase.setLocation(originalPurchase.getLocation());
        newPurchase.setPaystationSetting(originalPurchase.getPaystationSetting());
        
        if (rateInfo.getEmsRate1() == null) {
            newPurchase.setUnifiedRate(rateInfo.getEmsRate2().getUnifiedRate());
        } else {
            newPurchase.setUnifiedRate(rateInfo.getEmsRate1().getUnifiedRate());
        }
        
        newPurchase.setOriginalAmount(chargedAmount);
        newPurchase.setChargedAmount(chargedAmount);
        newPurchase.setCardPaidAmount(chargedAmount);
        newPurchase.setRateAmount(chargedAmount);
        newPurchase.setRateRevenueAmount(chargedAmount);
        newPurchase.setIsRefundSlip(false);
        
        newPurchase.setCreatedGmt(new Date());
        
        /* new Permit */
        final Permit permit = new Permit();
        permit.setPurchase(newPurchase);
        permit.setPermitNumber(smsAlertInfo.getTicketNumber());
        permit.setLocation(originalPurchase.getLocation());
        final PermitType permitType = new PermitType();
        permitType.setId(4);
        permit.setPermitType(permitType);
        final PermitIssueType permitIssueType = new PermitIssueType();
        if (!StringUtils.isBlank(smsAlertInfo.getPlateNumber())) {
            permitIssueType.setId(2);
        } else if (smsAlertInfo.getStallNumber() > 0) {
            permitIssueType.setId(3);
        } else {
            permitIssueType.setId(1);
        }
        permit.setPermitIssueType(permitIssueType);
        permit.setLicencePlate(originalPermit.getLicencePlate());
        permit.setMobileNumber(originalPermit.getMobileNumber());
        permit.setPermit(originalPermit);
        permit.setSpaceNumber(smsAlertInfo.getStallNumber());
        permit.setAddTimeNumber(originalPermit.getAddTimeNumber());
        permit.setPermitBeginGmt(newPurchasedDate);
        
        final Calendar c = Calendar.getInstance();
        c.setTime(smsAlertInfo.getExpiryDate());
        final Date expiryDateLocal = DateUtil.changeTimeZone(smsAlertInfo.getExpiryDate(), smsAlertInfo.getTimeZone());
        if (rateInfo.isSecondRateFreeParking(expiryDateLocal)) {
            if (rateInfo.getMaxExtensionTimeForFreeParking() > Integer.parseInt(userReply)) {
                c.add(Calendar.MINUTE, Integer.parseInt(userReply));
            } else {
                c.add(Calendar.MINUTE, rateInfo.getMaxAllowedParkingTimeInMinutes());
            }
        } else {
            c.add(Calendar.MINUTE, Integer.parseInt(userReply));
        }
        
        permit.setPermitOriginalExpireGmt(c.getTime());
        permit.setPermitExpireGmt(c.getTime());
        permit.setNumberOfExtensions(0);
        
        return permit;
    }
    
    private String getMatchedValue(final String value, final String exp) {
        final Pattern pattern = Pattern.compile(exp);
        final Matcher makeMatch = pattern.matcher(value);
        if (makeMatch.find()) {
            return makeMatch.group();
        } else {
            return "INVALID";
        }
    }
    
    public void setSmsPermitAlertJobService(final SmsPermitAlertJobService smsPermitAlertJobService) {
        this.smsPermitAlertJobService = smsPermitAlertJobService;
    }
    
    public void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public void setExtensiblePermitService(final ExtensiblePermitService extensiblePermitService) {
        this.extensiblePermitService = extensiblePermitService;
    }
    
    public void setSmsAlertService(final SmsAlertService smsAlertService) {
        this.smsAlertService = smsAlertService;
    }
    
    public void setPermitService(final PermitService permitService) {
        this.permitService = permitService;
    }
    
    public void setPurchaseService(final PurchaseService purchaseService) {
        this.purchaseService = purchaseService;
    }
    
    public void setCardProcessingManager(final CardProcessingManager cardProcessingManager) {
        this.cardProcessingManager = cardProcessingManager;
    }
    
    public void setExtensibleRateAndPermissionService(final ExtensibleRateAndPermissionService extensibleRateAndPermissionService) {
        this.extensibleRateAndPermissionService = extensibleRateAndPermissionService;
    }
    
    public void setActivePermitService(final ActivePermitService activePermitService) {
        this.activePermitService = activePermitService;
    }
    
    public void setSmsService(final SmsService smsService) {
        this.client = smsService;
    }
    
    public void setProcessorTransactionService(final ProcessorTransactionService processorTransactionService) {
        this.processorTransactionService = processorTransactionService;
    }
    
    public void setMerchantAccountService(final MerchantAccountService merchantAccountService) {
        this.merchantAccountService = merchantAccountService;
    }
}
