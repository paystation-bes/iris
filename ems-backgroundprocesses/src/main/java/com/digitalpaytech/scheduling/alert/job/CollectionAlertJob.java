package com.digitalpaytech.scheduling.alert.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.digitalpaytech.scheduling.alert.service.ActionJobService;
import com.digitalpaytech.scheduling.alert.service.CollectionAlertJobService;
import com.digitalpaytech.util.StandardConstants;

public class CollectionAlertJob extends BaseInterruptableJob {
    private CollectionAlertJobService collectionAlertJobService;
    
    @Override
    public final void execute(final JobExecutionContext context) throws JobExecutionException {
        //        LOGGER.info("+++ CollectionAlertJob started +++");
        super.execute(context);
        this.collectionAlertJobService = (CollectionAlertJobService) context.getJobDetail().getJobDataMap().get(ActionJobService.JOB_SERVICE_CLASS);
        while (!this.interrupted) {
            if (!this.collectionAlertJobService.isRunning()) {
                this.collectionAlertJobService.retrieveAlertFromDB();
                this.collectionAlertJobService.sendAlert(this);
            } else {
                try {
                    if (LOGGER.isTraceEnabled()) {
                        LOGGER.trace(new StringBuilder("+++ TotalCollectioDollar already runnning, sleep 1 second"));
                    }
                    
                    Thread.sleep(StandardConstants.SECONDS_IN_MILLIS_1);
                } catch (InterruptedException e) {
                }
            }
            if (this.interrupted) {
                this.collectionAlertJobService.setRunning(false);
                return;
            }
        }
    }
    
    @Override
    public final void clearData() {
        if (this.collectionAlertJobService != null) {
            this.collectionAlertJobService.clearAlertPushQueue();
        }
    }
}
