package com.digitalpaytech.scheduling.alert.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.digitalpaytech.scheduling.alert.service.ActionJobService;
import com.digitalpaytech.scheduling.alert.service.CommunicationAlertJobService;

public class CommunicationAlertJob extends BaseInterruptableJob {
    
    private CommunicationAlertJobService communicationAlertJobService;
    
    @Override
    public final void execute(final JobExecutionContext context) throws JobExecutionException {
        //        LOGGER.info("+++ CommunicationFailAlertJob started +++");
        super.execute(context);
        this.communicationAlertJobService = (CommunicationAlertJobService) context.getJobDetail().getJobDataMap()
                .get(ActionJobService.JOB_SERVICE_CLASS);
        
        this.communicationAlertJobService.scheduleCommunicationQueue();
    }
    
    @Override
    public final void clearData() {
        // Doesn't do anything
    }
}
