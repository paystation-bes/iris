package com.digitalpaytech.scheduling.alert.service.impl;

import java.util.Collection;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.PurchaseEmailAddress;
import com.digitalpaytech.scheduling.alert.job.PurchaseEmailAddressJob;
import com.digitalpaytech.scheduling.alert.service.PurchaseEmailAddressJobService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.PurchaseEmailAddressService;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Service("purchaseEmailAddressJobService")
public class PurchaseEmailAddressJobServiceImpl extends ActionJobServiceImpl implements PurchaseEmailAddressJobService {
    
    private static final String PURCHASE_EMAILADDRESS = "PurchaseEmailAddress";
    private static final String GROUP = "PurchaseEmailAddress";
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private PurchaseEmailAddressService purchaseEmailAddressService;
    
    private ConcurrentLinkedQueue<PurchaseEmailAddress> receiptPushQueue = new ConcurrentLinkedQueue<PurchaseEmailAddress>();
    
    @Override
    public final void runService() {
        try {
            actionService = this.getActionService(WebCoreConstants.ACTION_SERVICE_PURCHASE_EMAILADDRESS);
            if ((actionService == null) || (actionService.getServerInfo().size() == 0)) {
                logger.info("+++ no actions in this server +++");
            } else {
                scheduleJob(PurchaseEmailAddressJob.class, PURCHASE_EMAILADDRESS, GROUP);
            }
        } catch (SchedulerException e) {
            logger.error("SchedulerException: ", e);
        }
    }
    
    @Override
    public final void retrievePurchaseEmailAddressFromDB() {
        setRunning(true);
        if (logger.isTraceEnabled()) {
            logger.trace("+++ retrievePurchaseEmailAddressFromDB method called [loading " + actionService.getDataLoadingRows() + " rows] +++");
        }
        
        final Collection<PurchaseEmailAddress> purchaseEmailAddresses = this.purchaseEmailAddressService.findPurchaseEmailAddress(
            super.actionService.getDataLoadingRows(), false);
        
        for (PurchaseEmailAddress purchaseEmailAddress : purchaseEmailAddresses) {
            if (!this.receiptPushQueue.contains(purchaseEmailAddress)) {
                this.receiptPushQueue.offer(purchaseEmailAddress);
            }
        }
        
        if (this.receiptPushQueue.isEmpty()) {
            setRunning(false);
            try {
                if (logger.isTraceEnabled()) {
                    logger.trace(new StringBuilder("+++ no receipt to email, sleep ").append(super.actionService.getSleepTime())
                            .append(" seconds +++").toString());
                }
                
                Thread.sleep(StandardConstants.SECONDS_IN_MILLIS_1 * super.actionService.getSleepTime());
            } catch (InterruptedException e) {
            }
        }
        
        if (purchaseEmailAddresses.size() <= 0) {
            logger.trace("---- retrieved 0 purchase email receipt data from db ----");
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("---- retrieved purchase email receipt data from db ----");
                for (PurchaseEmailAddress purchaseEmailAddress : purchaseEmailAddresses) {
                    logger.debug(purchaseEmailAddress.toString());
                }
            }
        }
    }
    
    @Override
    public final void sendReceipt(final PurchaseEmailAddressJob purchaseEmailAddressJob) {
        logger.trace("+++ PurchaseEmailAddress sendReceipt method called +++");
        while (!this.receiptPushQueue.isEmpty() && !purchaseEmailAddressJob.getIsInterrupted()) {
            
            final PurchaseEmailAddress purchaseEmailAddress = this.receiptPushQueue.poll();
            
            final Customer customer = this.customerService.findCustomer(purchaseEmailAddress.getPurchase().getCustomer().getId());
            super.purchaseEmailAddressService.sendReceipt(purchaseEmailAddress, customer.isIsMigrated());
            
        }
        setRunning(false);
    }
    
    @Override
    public final void clearReceiptPushQueue() {
        logger.trace("+++ clearReceiptPushQueue method called +++");
        this.receiptPushQueue.clear();
    }
}
