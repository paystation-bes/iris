package com.digitalpaytech.scheduling.queue.alert.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.CustomerAlertEmail;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.EventActionType;
import com.digitalpaytech.domain.EventType;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.dto.queue.QueueEvent;
import com.digitalpaytech.scheduling.queue.alert.AlertProcessor;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerAlertTypeService;
import com.digitalpaytech.service.EventActionTypeService;
import com.digitalpaytech.service.EventSeverityTypeService;
import com.digitalpaytech.service.EventTypeService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.service.queue.QueueNotificationService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.LabelConstants;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;

@Component("alertProcessor")
@SuppressWarnings({ "PMD.ExcessiveImports", "PMD.GodClass" })
public class AlertProcessorImpl implements AlertProcessor, MessageSourceAware {
    
    private static final Logger LOGGER = Logger.getLogger(AlertProcessorImpl.class);
    private static final String CLEARED = " Cleared";
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private LocationService locationService;
    
    @Autowired
    private CustomerAlertTypeService customerAlertTypeService;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    @Autowired
    private EventTypeService eventTypeService;
    
    @Autowired
    private EventSeverityTypeService eventSeverityTypeService;
    
    @Autowired
    private EventActionTypeService eventActionTypeService;
    
    @Autowired
    private RouteService routeService;
    
    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    private QueueNotificationService queueNotificationService;
    
    public final PointOfSaleService getPointOfSaleService() {
        return this.pointOfSaleService;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final LocationService getLocationService() {
        return this.locationService;
    }
    
    public final void setLocationService(final LocationService locationService) {
        this.locationService = locationService;
    }
    
    public final CustomerAlertTypeService getCustomerAlertTypeService() {
        return this.customerAlertTypeService;
    }
    
    public final void setCustomerAlertTypeService(final CustomerAlertTypeService customerAlertTypeService) {
        this.customerAlertTypeService = customerAlertTypeService;
    }
    
    public final CustomerAdminService getCustomerAdminService() {
        return this.customerAdminService;
    }
    
    public final void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    public final EventTypeService getEventTypeService() {
        return this.eventTypeService;
    }
    
    public final void setEventTypeService(final EventTypeService eventTypeService) {
        this.eventTypeService = eventTypeService;
    }
    
    public final EventSeverityTypeService getEventSeverityTypeService() {
        return this.eventSeverityTypeService;
    }
    
    public final void setEventSeverityTypeService(final EventSeverityTypeService eventSeverityTypeService) {
        this.eventSeverityTypeService = eventSeverityTypeService;
    }
    
    public final EventActionTypeService getEventActionTypeService() {
        return this.eventActionTypeService;
    }
    
    public final void setEventActionTypeService(final EventActionTypeService eventActionTypeService) {
        this.eventActionTypeService = eventActionTypeService;
    }
    
    public final RouteService getRouteService() {
        return this.routeService;
    }
    
    public final void setRouteService(final RouteService routeService) {
        this.routeService = routeService;
    }
    
    public final MessageSource getMessageSource() {
        return this.messageSource;
    }
    
    @Override
    public final void setMessageSource(final MessageSource messageSource) {
        this.messageSource = messageSource;
    }
    
    public final QueueNotificationService getQueueNotificationService() {
        return this.queueNotificationService;
    }
    
    public final void setQueueNotificationService(final QueueNotificationService queueNotificationService) {
        this.queueNotificationService = queueNotificationService;
    }
    
    @Override
    public final void processAlert(final QueueEvent queueEvent) {
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(queueEvent.getPointOfSaleId());
        if (pointOfSale == null) {
            LOGGER.error("Cannot find PointOfSale from id: " + queueEvent.getPointOfSaleId() + ",\r\nqueueEvent: " + queueEvent);
            return;
        }
        
        if (queueEvent.getCustomerAlertTypeId() == null) {
            LOGGER.error("customerAlertTypeId is null \r\nqueueEvent: " + queueEvent);
            return;
        }
        
        final CustomerProperty customerProperty =
                this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(pointOfSale.getCustomer().getId(),
                                                                                                 WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        final String timeZone = customerProperty.getPropertyValue();
        
        final CustomerAlertType customerAlertType = this.customerAlertTypeService.findCustomerAlertTypeById(queueEvent.getCustomerAlertTypeId());
        if (customerAlertType.getAlertThresholdType().getId() == WebCoreConstants.ALERT_THRESHOLD_TYPE_PAYSTATION) {
            
            final List<CustomerAlertEmail> customerAlertEmailList =
                    this.customerAlertTypeService.findCustomerAlertEmailByCustomerAlertTypeId(customerAlertType.getId());
            
            if (!WebCoreUtil.isCollectionEmpty(customerAlertEmailList)) {
                sendPaystationEventAlert(pointOfSale, queueEvent, customerAlertEmailList, timeZone);
            }
            
        } else {
            final List<CustomerAlertEmail> customerAlertEmailList =
                    this.customerAlertTypeService.findCustomerAlertEmailByCustomerAlertTypeId(customerAlertType.getId());
            if (!WebCoreUtil.isCollectionEmpty(customerAlertEmailList)) {
                sendUserDefinedAlertEmail(customerAlertType, customerAlertEmailList, pointOfSale.getName(), queueEvent.getIsActive(),
                                          queueEvent.getTimestampGMT(), timeZone);
            }
        }
    }
    
    private void sendPaystationEventAlert(final PointOfSale pointOfSale, final QueueEvent queueEvent,
        final List<CustomerAlertEmail> customerAlertEmailList, final String timeZone) {
        
        final EventType eventType = this.eventTypeService.findEventTypeById(queueEvent.getEventTypeId());
        final Location location = this.locationService.findLocationById(pointOfSale.getLocation().getId());
        final PosServiceState posServiceState = this.pointOfSaleService.findPosServiceStateByPointOfSaleId(pointOfSale.getId());
        
        final String separator = System.getProperty(StandardConstants.STRING_LABEL_LINE_SEPARATOR);
        
        /* create email content. */
        final StringBuilder content = new StringBuilder();
        content.append("Type: ").append(eventType.getEventDeviceType().getNameNoSpace())
               .append(" => ").append(this.messageSource.getMessage(eventType.getDescription(), null, null));
        
        final StringBuilder trailingDescription = new StringBuilder(StandardConstants.STRING_EMPTY_STRING);
        if (queueEvent.getEventActionTypeId() == null) {
            trailingDescription.append(queueEvent.getIsActive() ? StandardConstants.STRING_EMPTY_STRING : CLEARED);
        } else {
            final EventActionType eventActionType = this.eventActionTypeService.findEventActionType(queueEvent.getEventActionTypeId());
            trailingDescription.append(StandardConstants.STRING_EMPTY_SPACE).append(eventActionType.getName());
            if ((eventActionType.getId() != WebCoreConstants.EVENT_ACTION_TYPE_CLEARED)
                    && (eventActionType.getId() != WebCoreConstants.EVENT_ACTION_TYPE_NORMAL)) {
                trailingDescription.append(queueEvent.getIsActive() ? StandardConstants.STRING_EMPTY_STRING : CLEARED);
            }
        }
        
        content.append(trailingDescription.toString())
               .append(separator);
        
        content.append("Location: ").append(pointOfSale.getLocation() == null ? StandardConstants.STRING_NA_PARANTHESIS : location.getName())
               .append(separator);
        
        content.append("Machine: ")
               .append(pointOfSale.getSerialNumber() == null ? StandardConstants.STRING_NA_PARANTHESIS : pointOfSale.getSerialNumber())
               .append(separator);
        
        content.append("Time: ").append(DateUtil.createDateString(timeZone, queueEvent.getTimestampGMT()))
               .append(separator);
        
        content.append("Lot: ")
               .append(posServiceState.getPaystationSettingName() == null ? StandardConstants.STRING_NA_PARANTHESIS : posServiceState
                       .getPaystationSettingName())
               .append(separator);
        
        content.append("PS Name: ").append(pointOfSale.getName())
               .append(separator);
        
        content.append("Severity: ")
               .append(this.eventSeverityTypeService.findEventSeverityType(queueEvent.getEventSeverityTypeId()).getName())
               .append(separator);
        
        content.append("Details: ")
               .append(StringUtils.isBlank(queueEvent.getAlertInfo()) ? StandardConstants.STRING_NA_PARANTHESIS : queueEvent.getAlertInfo())
               .append(separator);
        
        sendEmail(this.messageSource.getMessage(queueEvent.getIsActive() ? LabelConstants.ALERT_EMAIL_PAYSTATION_SUBJECT_TEMPLATE
                          : LabelConstants.ALERT_RECOVERY_EMAIL_PAYSTATION_SUBJECT_TEMPLATE, new String[] { pointOfSale.getName(), }, null),
                  content.toString(), customerAlertEmailList);
        
    }
    
    private void sendUserDefinedAlertEmail(final CustomerAlertType customerAlertType, final List<CustomerAlertEmail> customerAlertEmailList,
        final String posName, final boolean isAlert, final Date alertGmt, final String timeZone) {
        String subject = null;
        String content = null;
        
        if (isAlert) {
            if (customerAlertType.getAlertThresholdType().getId() == WebCoreConstants.ALERT_THRESHOLD_TYPE_LAST_SEEN_INTERVAL_HOUR) {
                subject = this.messageSource.getMessage("alert.email.communication.subject.template", new String[] { customerAlertType.getName() },
                                                        null);
                content = this.messageSource.getMessage("alert.email.communication.content.template",
                                                        new String[] { posName, String.valueOf(customerAlertType.getThreshold()) }, null);
            } else {
                if (customerAlertType.getAlertThresholdType().getId() == WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_COUNT
                    || customerAlertType.getAlertThresholdType().getId() == WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_COUNT
                    || customerAlertType.getAlertThresholdType().getId() == WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_COUNT) {
                    subject = this.messageSource.getMessage(LabelConstants.ALERT_EMAIL_COLLECTION_SUBJECT_TEMPLATE,
                                                            new String[] { customerAlertType.getName() }, null);
                    content = this.messageSource.getMessage("alert.email.collection.count.content.template",
                                                            new String[] { posName, String.valueOf(customerAlertType.getThreshold()) }, null);
                } else if (customerAlertType.getAlertThresholdType().getId() == WebCoreConstants.ALERT_THRESHOLD_TYPE_OVERDUE_COLLECTION) {
                    subject = this.messageSource.getMessage(LabelConstants.ALERT_EMAIL_COLLECTION_SUBJECT_TEMPLATE,
                                                            new String[] { customerAlertType.getName() }, null);
                    
                    content = this.messageSource.getMessage("alert.email.collection.overdue.content.template",
                                                            new String[] { posName,
                                                                String.valueOf(new Float(customerAlertType.getThreshold()).intValue()), }, null);
                } else {
                    subject = this.messageSource.getMessage(LabelConstants.ALERT_EMAIL_COLLECTION_SUBJECT_TEMPLATE,
                                                            new String[] { customerAlertType.getName() }, null);
                    content = this.messageSource.getMessage("alert.email.collection.content.template",
                                                            new String[] { posName, String.valueOf(customerAlertType.getThreshold()) }, null);
                }
            }
        } else {
            if (customerAlertType.getAlertThresholdType().getId() == WebCoreConstants.ALERT_THRESHOLD_TYPE_LAST_SEEN_INTERVAL_HOUR) {
                subject = this.messageSource.getMessage("alert.recovery.email.communication.subject.template",
                                                        new String[] { customerAlertType.getName() }, null);
                content = this.messageSource.getMessage("alert.recovery.email.communication.content.template",
                                                        new String[] { posName, DateUtil.createDateString(timeZone, alertGmt) }, null);
            } else {
                subject = this.messageSource.getMessage("alert.recovery.email.collection.subject.template",
                                                        new String[] { customerAlertType.getName() }, null);
                content = this.messageSource.getMessage("alert.recovery.email.collection.content.template",
                                                        new String[] { posName, DateUtil.createDateString(timeZone, alertGmt) }, null);
            }
        }
        
        sendEmail(subject, content, customerAlertEmailList);
    }
    
    private void sendEmail(final String subject, final String content, final List<CustomerAlertEmail> customerAlertEmailList) {
        
        final StringBuilder emailList = new StringBuilder();
        try {
            for (CustomerAlertEmail alertEmail : customerAlertEmailList) {
                emailList.append(URLDecoder.decode(alertEmail.getCustomerEmail().getEmail(), WebSecurityConstants.URL_ENCODING_LATIN1))
                        .append(StandardConstants.STRING_COMMA);
            }
        } catch (UnsupportedEncodingException uee) {
            LOGGER.error("AlertProcessor could not create emailList for CustomerAlertType = "
                         + customerAlertEmailList.get(0).getCustomerAlertType().getId(), uee);
        }
        
        this.queueNotificationService.createQueueNotificationEmail(subject, content, emailList.toString());
    }
    
}
