package com.digitalpaytech.scheduling.queue.alert;

import com.digitalpaytech.dto.queue.QueueEvent;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerAlertTypeService;
import com.digitalpaytech.service.EventSeverityTypeService;
import com.digitalpaytech.service.EventTypeService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.PosAlertStatusService;
import com.digitalpaytech.service.PosEventCurrentService;
import com.digitalpaytech.service.PosEventDelayedService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.service.queue.QueueEventService;

public interface PosEventCurrentProcessor {
    
    void processQueueEvent(QueueEvent queueEvent);
    
    void setEventTypeService(EventTypeService eventTypeService);
    
    void setEventSeverityTypeService(EventSeverityTypeService eventTypeService);
    
    void setQueueEventService(QueueEventService queueEventService);
    
    void setPosEventCurrentService(PosEventCurrentService posCurrentService);
    
    void setPointOfSaleService(PointOfSaleService pointOfSaleService);
    
    void setCustomerAlertTypeService(CustomerAlertTypeService customerAlertTypeService);
    
    void setPosAlertStatusService(PosAlertStatusService posAlertStatusService);
    
    void setPosEventDelayedService(PosEventDelayedService posEventDelayedService);
    
    void setCustomerAdminService(CustomerAdminService customerAdminService);
    
    void setRouteService(RouteService routeService);
    
}
