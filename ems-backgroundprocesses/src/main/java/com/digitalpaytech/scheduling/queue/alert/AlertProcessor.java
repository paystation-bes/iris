package com.digitalpaytech.scheduling.queue.alert;

import com.digitalpaytech.dto.queue.QueueEvent;

public interface AlertProcessor {
    
    void processAlert(QueueEvent queueEvent);
    
}
