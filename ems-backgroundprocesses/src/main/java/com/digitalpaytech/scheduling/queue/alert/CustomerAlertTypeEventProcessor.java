package com.digitalpaytech.scheduling.queue.alert;

import java.util.List;

import com.digitalpaytech.data.UserDefinedEventInfo;
import com.digitalpaytech.dto.queue.QueueCustomerAlertType;

public interface CustomerAlertTypeEventProcessor {
    void handle(QueueCustomerAlertType event);
    
    List<UserDefinedEventInfo> createPosEventCurrents(QueueCustomerAlertType event);
    
    List<QueueCustomerAlertType> removePosEventCurrents(QueueCustomerAlertType event);
}
