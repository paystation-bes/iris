package com.digitalpaytech.scheduling.queue.alert.delayed;

public interface DelayedEventNotificationService {
    
    void processDelayedAlerts();
    
}
