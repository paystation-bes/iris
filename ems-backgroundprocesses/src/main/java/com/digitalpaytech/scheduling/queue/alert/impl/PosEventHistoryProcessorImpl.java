package com.digitalpaytech.scheduling.queue.alert.impl;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.PosEventHistory;
import com.digitalpaytech.dto.queue.QueueEvent;
import com.digitalpaytech.scheduling.queue.alert.PosEventHistoryProcessor;
import com.digitalpaytech.service.CustomerAlertTypeService;
import com.digitalpaytech.service.EventActionTypeService;
import com.digitalpaytech.service.EventSeverityTypeService;
import com.digitalpaytech.service.EventTypeService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.PosEventHistoryService;

@Component("posEventHistoryProcessor")
public class PosEventHistoryProcessorImpl implements PosEventHistoryProcessor {
    
    private static final Logger LOGGER = Logger.getLogger(PosEventHistoryProcessorImpl.class);
    
    @Autowired
    private EventActionTypeService eventActionTypeService;
    
    @Autowired
    private CustomerAlertTypeService customerAlertTypeService;
    
    @Autowired
    private EventSeverityTypeService eventSeverityTypeService;
    
    @Autowired
    private EventTypeService eventTypeService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private PosEventHistoryService posEventHistoryService;
    
    public final EventActionTypeService getEventActionTypeService() {
        return this.eventActionTypeService;
    }
    
    public final void setEventActionTypeService(final EventActionTypeService eventActionTypeService) {
        this.eventActionTypeService = eventActionTypeService;
    }
    
    public final CustomerAlertTypeService getCustomerAlertTypeService() {
        return this.customerAlertTypeService;
    }
    
    public final void setCustomerAlertTypeService(final CustomerAlertTypeService customerAlertTypeService) {
        this.customerAlertTypeService = customerAlertTypeService;
    }
    
    public final EventSeverityTypeService getEventSeverityTypeService() {
        return this.eventSeverityTypeService;
    }
    
    public final void setEventSeverityTypeService(final EventSeverityTypeService eventSeverityTypeService) {
        this.eventSeverityTypeService = eventSeverityTypeService;
    }
    
    public final EventTypeService getEventTypeService() {
        return this.eventTypeService;
    }
    
    public final void setEventTypeService(final EventTypeService eventTypeService) {
        this.eventTypeService = eventTypeService;
    }
    
    public final PointOfSaleService getPointOfSaleService() {
        return this.pointOfSaleService;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final PosEventHistoryService getPosEventHistoryService() {
        return this.posEventHistoryService;
    }
    
    public final void setPosEventHistoryService(final PosEventHistoryService posEventHistoryService) {
        this.posEventHistoryService = posEventHistoryService;
    }
    
    @Override
    public final void savePosEventHistory(final QueueEvent queueEvent) {
        
        final PosEventHistory posEventHistory = new PosEventHistory();
        
        if (queueEvent.getCustomerAlertTypeId() == null) {
            posEventHistory.setCustomerAlertType(null);
        } else {
            posEventHistory.setCustomerAlertType(this.customerAlertTypeService.findCustomerAlertTypeById(queueEvent.getCustomerAlertTypeId()));
        }
        
        if (queueEvent.getEventActionTypeId() == null) {
            posEventHistory.setEventActionType(null);
        } else {
            posEventHistory.setEventActionType(this.eventActionTypeService.findEventActionType(queueEvent.getEventActionTypeId()));
        }
        
        posEventHistory.setEventSeverityType(this.eventSeverityTypeService.findEventSeverityType(queueEvent.getEventSeverityTypeId()));
        posEventHistory.setEventType(this.eventTypeService.findEventTypeById(queueEvent.getEventTypeId()));
        posEventHistory.setPointOfSale(this.pointOfSaleService.findPointOfSaleById(queueEvent.getPointOfSaleId()));
        posEventHistory.setIsActive(queueEvent.getIsActive());
        posEventHistory.setLastModifiedByUserId(queueEvent.getLastModifiedByUserId());
        // queueEvent.getCreatedGMT() is the current time.
        posEventHistory.setCreatedGmt(new Date());
        // queueEvent.getTimestampGMT() is when event happened.
        posEventHistory.setTimestampGmt(queueEvent.getTimestampGMT());
        posEventHistory.setAlertInfo(queueEvent.getAlertInfo());
        
        try {
            this.posEventHistoryService.savePosEventHistory(posEventHistory);
        } catch (DataIntegrityViolationException ex) {
            LOGGER.error(new StringBuilder("Potential duplicate alert. PosId:").append(posEventHistory.getPointOfSale().getId())
                                 .append(", eventTypeId:").append(posEventHistory.getEventType().getId()).append(", catId")
                                 .append(posEventHistory.getCustomerAlertType() == null ? "null" : posEventHistory.getCustomerAlertType().getId())
                                 .append(", isActive:").append(posEventHistory.isIsActive()).append(", timestamp:")
                                 .append(posEventHistory.getTimestampGmt()), ex);
        }
    }
}
