package com.digitalpaytech.scheduling.queue.alert.delayed;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosEventDelayed;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.PosEventDelayedService;
import com.digitalpaytech.service.queue.QueueEventService;
import com.digitalpaytech.util.WebCoreConstants;

@Service("delayedEventNotificationService")
@Transactional(propagation = Propagation.REQUIRED)
public class DelayedEventNotificationServiceImpl implements DelayedEventNotificationService {
    private static final Logger LOG = Logger.getLogger(DelayedEventNotificationServiceImpl.class);
    
    @Autowired
    private PosEventDelayedService posEventDelayedService;
    
    @Autowired
    private QueueEventService queueEventService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private ClusterMemberService clusterMemberService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Override
    public void processDelayedAlerts() {
        if (isDelayedEventsProcessingServer()) {
            final Collection<PosEventDelayed> events = this.posEventDelayedService.findByDelayedGmtPast();
            LOG.info(String.format("Processing %d delayed alerts", events.size()));
            
            events.forEach(event -> publishEventAndDelete(event));
        }
        
    }
    
    private void publishEventAndDelete(final PosEventDelayed event) {
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(event.getPointOfSale().getId());
        
        if (pointOfSale == null) {
            LOG.error(String.format("Null PointOfSale for POSEventDelayed %d", event.getId()));
            return;
        }
        
        final Paystation paystation = this.pointOfSaleService.findPayStationById(pointOfSale.getPaystation().getId());
        
        final Integer paystationTypeId;
        if (paystation == null || paystation.getPaystationType() == null) {
            paystationTypeId = WebCoreConstants.PAY_STATION_TYPE_NA;
        } else {
            paystationTypeId = Integer.valueOf(paystation.getPaystationType().getId());
        }
        
        this.queueEventService.createEventQueue(pointOfSale.getId(), paystationTypeId,
                                                event.getCustomerAlertType() == null ? null : event.getCustomerAlertType().getId(),
                                                event.getEventType(), event.getEventSeverityType().getId(),
                                                event.getEventActionType() == null ? null : event.getEventActionType().getId(),
                                                new Date(), event.getAlertInfo(), event.isIsActive(), true, 1,
                                                WebCoreConstants.QUEUE_TYPE_PROCESSING_EVENT);
        this.posEventDelayedService.delete(event);
    }
    
    private boolean isDelayedEventsProcessingServer() {
        try {
            final String clusterName = this.clusterMemberService.getClusterName();
            if (StringUtils.isBlank(clusterName)) {
                throw new ApplicationException("Cluster member name is empty");
            }
            final String processServer = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.DELAYED_EVENTS_PROCESSING_SERVER);
            
            if (StringUtils.isBlank(processServer)) {
                throw new ApplicationException("Could not get server name from EMSProperties to process Delayed Events");
            }
            
            if (clusterName.equals(processServer)) {
                LOG.info(clusterName + " configured for Delayed Event Processing");
                return true;
            } else {
                LOG.info(clusterName + " not configured for Delayed Event Processing");
                return false;
            }
        } catch (ApplicationException e) {
            try {
                LOG.error("Unable to read memeber name from cluster.properties for cluster member " + InetAddress.getLocalHost().getHostAddress(), e);
            } catch (UnknownHostException uhe) {
                LOG.error("UnExpected UnknownHost! This shouldn't happened", uhe);
            }
            
            return false;
        }
    }
}
