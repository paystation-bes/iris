package com.digitalpaytech.scheduling.queue.alert.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.hibernate.FlushMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.data.UserDefinedEventInfo;
import com.digitalpaytech.domain.AlertThresholdType;
import com.digitalpaytech.domain.AlertThresholdType1EventClearV;
import com.digitalpaytech.domain.AlertThresholdType1EventV;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.EventType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.dto.queue.QueueCustomerAlertType;
import com.digitalpaytech.scheduling.queue.alert.CustomerAlertTypeEventProcessor;
import com.digitalpaytech.service.AlertsService;
import com.digitalpaytech.service.CustomerAlertTypeService;
import com.digitalpaytech.service.PosAlertStatusService;
import com.digitalpaytech.service.PosEventCurrentService;
import com.digitalpaytech.service.ServiceHelper;
import com.digitalpaytech.service.UserDefinedEventInfoService;
import com.digitalpaytech.service.queue.QueueEventService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;

@Service("customerAlertTypeEventProcessor")
@SuppressWarnings({ "PMD.GodClass", "PMD.ExcessiveImports" })
public class CustomerAlertTypeEventProcessorImpl implements CustomerAlertTypeEventProcessor {
    private static final Logger LOG = Logger.getLogger(CustomerAlertTypeEventProcessorImpl.class);
    
    private static final int FLUSH_SIZE = StandardConstants.CONSTANT_1000;
    
    private static final EventType EVENT_TYPE_USER_DEFINED = new EventType((byte) WebCoreConstants.EVENT_TYPE_CUSTOMERDEFINED_ALERT);
    private static final EventSeverityType SEVERITY_CLEAR = new EventSeverityType(WebCoreConstants.SEVERITY_CLEAR);
    
    private static final String ALERT_QUERY_NAME_POINTOFSALE = "AlertThresholdType{0}EventV.findByPointOfSaleId";
    private static final String CLEAR_QUERY_NAME_POINTOFSALE = "AlertThresholdType{0}EventClearV.findByPointOfSaleId";
    private static final String ALERT_QUERY_NAME_POINTOFSALE_CUSTOMERALERTTYPE = "AlertThresholdType{0}EventV.findByPointOfSaleIdAndCustomerAlertTypeId";
    private static final String CLEAR_QUERY_NAME_POINTOFSALE_CUSTOMERALERTTYPE = "AlertThresholdType{0}EventClearV.findByPointOfSaleIdAndCustomerAlertTypeId";
    
    private static Map<Integer, String[]> alertQueryPointOfSaleList;
    private static Map<Integer, String[]> alertQueryPointOfSaleCustomerAlertTypeList;
    
    @Autowired
    private EntityDao entityDao;
    
    @Autowired
    private AlertsService alertsService;
    
    @Autowired
    private CustomerAlertTypeService customerAlertTypeService;
    
    @Autowired
    private QueueEventService queueEventService;
    
    @Autowired
    private UserDefinedEventInfoService userDefinedEventInfoService;
    
    @Autowired
    private PosEventCurrentService posEventCurrentService;
    
    @Autowired
    private PosAlertStatusService posAlertStatusService;
    
    @Autowired
    private ServiceHelper serviceHelper;
    
    public final EntityDao getEntityDao() {
        return this.entityDao;
    }
    
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    
    public final AlertsService getAlertsService() {
        return this.alertsService;
    }
    
    public final void setAlertsService(final AlertsService alertsService) {
        this.alertsService = alertsService;
    }
    
    public final CustomerAlertTypeService getCustomerAlertTypeService() {
        return this.customerAlertTypeService;
    }
    
    public final void setCustomerAlertTypeService(final CustomerAlertTypeService customerAlertTypeService) {
        this.customerAlertTypeService = customerAlertTypeService;
    }
    
    public final QueueEventService getQueueEventService() {
        return this.queueEventService;
    }
    
    public final void setQueueEventService(final QueueEventService queueEventService) {
        this.queueEventService = queueEventService;
    }
    
    public final UserDefinedEventInfoService getUserDefinedEventInfoService() {
        return this.userDefinedEventInfoService;
    }
    
    public final void setUserDefinedEventInfoService(final UserDefinedEventInfoService userDefinedEventInfoService) {
        this.userDefinedEventInfoService = userDefinedEventInfoService;
    }
    
    public final PosEventCurrentService getPosEventCurrentService() {
        return this.posEventCurrentService;
    }
    
    public final void setPosEventCurrentService(final PosEventCurrentService posEventCurrentService) {
        this.posEventCurrentService = posEventCurrentService;
    }
    
    public final PosAlertStatusService getPosAlertStatusService() {
        return this.posAlertStatusService;
    }
    
    public final void setPosAlertStatusService(final PosAlertStatusService posAlertStatusService) {
        this.posAlertStatusService = posAlertStatusService;
    }

    public static final void setAlertQueryPointOfSaleCustomerAlertTypeList(final Map<Integer, String[]> alertQueryPointOfSaleCustomerAlertTypeList) {
        CustomerAlertTypeEventProcessorImpl.alertQueryPointOfSaleCustomerAlertTypeList = alertQueryPointOfSaleCustomerAlertTypeList;
    }
    
    @PostConstruct
    public final void init() {
        // createQueryList
        final List<AlertThresholdType> alertThresholdTypeList = this.alertsService
                .findAlertThresholdTypeByAlertClassTypeId(WebCoreConstants.ALERT_CLASS_TYPE_COLLECTION);
        alertThresholdTypeList.addAll(this.alertsService.findAlertThresholdTypeByAlertClassTypeId(WebCoreConstants.ALERT_CLASS_TYPE_COMMUNICATION));
        alertQueryPointOfSaleList = new HashMap<Integer, String[]>();
        alertQueryPointOfSaleCustomerAlertTypeList = new HashMap<Integer, String[]>();
        for (AlertThresholdType alertThresholdType : alertThresholdTypeList) {
            // Only collection alerts should be included in this list
            if (alertThresholdType.getId() != WebCoreConstants.ALERT_THRESHOLD_TYPE_LAST_SEEN_INTERVAL_HOUR) {
                final String[] queriesWithPointOfSale = { MessageFormat.format(ALERT_QUERY_NAME_POINTOFSALE, alertThresholdType.getId()),
                    MessageFormat.format(CLEAR_QUERY_NAME_POINTOFSALE, alertThresholdType.getId()), };
                alertQueryPointOfSaleList.put(alertThresholdType.getId(), queriesWithPointOfSale);
            }
            
            final String[] queriesWithCustomerAlert = {
                MessageFormat.format(ALERT_QUERY_NAME_POINTOFSALE_CUSTOMERALERTTYPE, alertThresholdType.getId()),
                MessageFormat.format(CLEAR_QUERY_NAME_POINTOFSALE_CUSTOMERALERTTYPE, alertThresholdType.getId()), };
            
            alertQueryPointOfSaleCustomerAlertTypeList.put(alertThresholdType.getId(), queriesWithCustomerAlert);
        }
        
    }
    
    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    @SuppressWarnings("PMD.UseConcurrentHashMap")
    public final void handle(final QueueCustomerAlertType event) {
        switch (event.getAction()) {
            case ADDED:
                handleAddition(event);
                handleModification(event);
                break;
            case REMOVED:
                handleRemoval(event);
                break;
            case MODIFIED:
                handleModification(event);
                break;
            case COMMUNICATION:
                handleCommunication(event);
                break;
            default:
                LOG.warn("Unknown QueryCustomerAlertType.Type: " + event.getAction());
                break;
        }
    }
    
    private void handleAddition(final QueueCustomerAlertType event) {
        if (event.getCustomerAlertTypeIds() == null || event.getCustomerAlertTypeIds().size() <= 0) {
            LOG.warn("Encounter CustomerAlertType's addition without CutomerAlertTypeIds");
        } else if (event.getPointOfSaleIds() == null || event.getPointOfSaleIds().size() <= 0) {
            LOG.warn("Encounter CustomerAlertType's addition without PointOfSaleIds");
        } else {
            List<UserDefinedEventInfo> clearList = self().createPosEventCurrents(event);
            if (!WebCoreUtil.isCollectionEmpty(clearList)) {
                sendEvents(new ArrayList<UserDefinedEventInfo>(), clearList);
            }
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    public final List<UserDefinedEventInfo> createPosEventCurrents(final QueueCustomerAlertType event) {
        this.entityDao.getCurrentSession().setFlushMode(FlushMode.MANUAL);
        
        final List<CustomerAlertType> customerAlertTypeList = this.customerAlertTypeService.findCustomerAlertTypeByIdList(event.getCustomerAlertTypeIds());
        int processed = 0;
        for (Integer posId : event.getPointOfSaleIds()) {
            processed += createPosEventCurrent(new PointOfSale(posId), customerAlertTypeList);
            if (processed >= FLUSH_SIZE) {
                flushUpdates();
                processed = 0;
            }
        }
        
        flushUpdates();
        
        return checkForDefaultAlertsToDisable(event);
    }
    
    private void handleRemoval(final QueueCustomerAlertType event) {
        if (event.getCustomerAlertTypeIds() == null || event.getCustomerAlertTypeIds().size() <= 0) {
            LOG.warn("Encounter CustomerAlertType's removal without CutomerAlertTypeIds");
        } else if (event.getPointOfSaleIds() == null || event.getPointOfSaleIds().size() <= 0) {
            LOG.warn("Encounter CustomerAlertType's removal without PointOfSaleIds");
        } else {
            final List<QueueCustomerAlertType> defaultEvents = self().removePosEventCurrents(event);
            for (QueueCustomerAlertType newEvent : defaultEvents) {
                handleModification(newEvent);
            }
        }
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    public final List<QueueCustomerAlertType> removePosEventCurrents(final QueueCustomerAlertType event) {
        this.entityDao.getCurrentSession().setFlushMode(FlushMode.MANUAL);
        
        int processed = 0;
        final List<PosEventCurrent> pecList = scrollPosEventCurrents(event.getPointOfSaleIds(), event.getCustomerAlertTypeIds());
        for (PosEventCurrent pec : pecList) {
            // Send clear to historical if alert exists with severity higher than minor
            if (pec.isIsActive()) {
                if (pec.getEventSeverityType().getId() != WebCoreConstants.SEVERITY_MINOR) {
                    this.queueEventService.createEventQueue(pec.getPointOfSale().getId(), null, pec.getCustomerAlertType().getId(),
                                                            pec.getEventType(), WebCoreConstants.SEVERITY_CLEAR, null,
                                                            DateUtil.getCurrentGmtDate(), null, false, true,
                                                            WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID,
                                                            (byte) (WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT));
                }
                final PosEventCurrent pecClear = new PosEventCurrent(pec.getPointOfSale(), pec.getEventSeverityType(), pec.getEventActionType(),
                        pec.getCustomerAlertType(), pec.getEventType(), pec.getAlertInfo(), false, null, null);
                this.posAlertStatusService.updateAlertCount(pecClear, pec.getEventSeverityType().getId(), false);
            }
            
            this.entityDao.delete(pec);
            ++processed;
            if (processed >= FLUSH_SIZE) {
                flushUpdates();
                processed = 0;
            }
        }
        
        flushUpdates();
        
        return checkForDefaultAlertsToEnable(event);
    }
    
    private void handleModification(final QueueCustomerAlertType event) {
        
        if (event.getPointOfSaleIds() == null || event.getPointOfSaleIds().size() <= 0) {
            LOG.warn("Encounter modification without PointOfSaleIds");
        } else {
            if (event.getCustomerAlertTypeIds() == null) {
                final Collection<String[]> alertList = alertQueryPointOfSaleList.values();
                for (String[] queries : alertList) {
                    triggerEvents(event.getPointOfSaleIds(), queries[0], queries[1], null);
                }
            } else {
                for (Integer customerAlertTypeId : event.getCustomerAlertTypeIds()) {
                    final CustomerAlertType customerAlertType = this.customerAlertTypeService.findCustomerAlertTypeById(customerAlertTypeId);
                    final String[] queriesWithCustomerAlert = alertQueryPointOfSaleCustomerAlertTypeList.get(customerAlertType
                            .getAlertThresholdType().getId());
                    triggerEvents(event.getPointOfSaleIds(), queriesWithCustomerAlert[0], queriesWithCustomerAlert[1], customerAlertTypeId);
                }
            }
        }
    }
    
    private void handleCommunication(final QueueCustomerAlertType event) {
        
        final List<UserDefinedEventInfo> alertList = this.userDefinedEventInfoService.findEventsClassTypeAndLimit(AlertThresholdType1EventV.class,
                                                                                                                  event.getLimit());
        final List<UserDefinedEventInfo> clearList = this.userDefinedEventInfoService
                .findEventsClassTypeAndLimit(AlertThresholdType1EventClearV.class, event.getLimit());
        
        sendEvents(alertList, clearList);
        
    }
    
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    private List<UserDefinedEventInfo> checkForDefaultAlertsToDisable(final QueueCustomerAlertType event) {
        int processed = 0;
        
        final List<UserDefinedEventInfo> clearList = new ArrayList<UserDefinedEventInfo>();
        final List<Long> pecIdList = this.posEventCurrentService.findDefaultPosEventCurrentsToDisable(event.getPointOfSaleIds());
        if (!WebCoreUtil.isCollectionEmpty(pecIdList)) {
            final List<PosEventCurrent> defaultAlertList = this.posEventCurrentService.findPosEventCurrentsByIdList(pecIdList);
            if (!WebCoreUtil.isCollectionEmpty(defaultAlertList)) {
                for (PosEventCurrent posEventCurrent : defaultAlertList) {
                    posEventCurrent.setIsHidden(true);
                    if (posEventCurrent.isIsActive()) {
                        final UserDefinedEventInfo userDefinedEventInfo = new UserDefinedEventInfo();
                        userDefinedEventInfo.setCustomerAlertType(posEventCurrent.getCustomerAlertType());
                        userDefinedEventInfo.setPointOfSaleId(posEventCurrent.getPointOfSale().getId());
                        userDefinedEventInfo.setPosEventCurrent(posEventCurrent);
                        userDefinedEventInfo.setGapToCritical(0);
                        userDefinedEventInfo.setGapToMajor(0);
                        userDefinedEventInfo.setGapToMinor(0);
                        clearList.add(userDefinedEventInfo);
                    }
                    this.posEventCurrentService.updatePosEventCurrent(posEventCurrent);
                    ++processed;
                    if (processed >= FLUSH_SIZE) {
                        flushUpdates();
                        processed = 0;
                    }
                }
            }
        }
        
        flushUpdates();
        
        return clearList;
    }
    
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    private List<QueueCustomerAlertType> checkForDefaultAlertsToEnable(final QueueCustomerAlertType event) {
        int processed = 0;
        
        final List<QueueCustomerAlertType> defaultEvents = new ArrayList<QueueCustomerAlertType>();
        
        final List<Long> pecIdList = this.posEventCurrentService.findDefaultPosEventCurrentsToEnable(event.getPointOfSaleIds());
        if (!WebCoreUtil.isCollectionEmpty(pecIdList)) {
            final List<PosEventCurrent> defaultAlertList = this.posEventCurrentService.findPosEventCurrentsByIdList(pecIdList);
            if (!WebCoreUtil.isCollectionEmpty(defaultAlertList)) {
                final List<Integer> defaultCustomerAlertTypeIdList = new ArrayList<Integer>();
                for (PosEventCurrent posEventCurrent : defaultAlertList) {
                    posEventCurrent.setIsHidden(false);
                    defaultCustomerAlertTypeIdList.add(posEventCurrent.getCustomerAlertType().getId());
                    this.posEventCurrentService.updatePosEventCurrent(posEventCurrent);
                    ++processed;
                    if (processed >= FLUSH_SIZE) {
                        flushUpdates();
                        processed = 0;
                    }
                }
                if (!WebCoreUtil.isCollectionEmpty(defaultCustomerAlertTypeIdList)) {
                    final QueueCustomerAlertType defaultTriggerEvent = new QueueCustomerAlertType(QueueCustomerAlertType.Type.MODIFIED,
                            event.getPointOfSaleIds(), defaultCustomerAlertTypeIdList);
                    defaultEvents.add(defaultTriggerEvent);
                }
            }
        }
        
        flushUpdates();
        
        return defaultEvents;
    }
    
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    private int createPosEventCurrent(final PointOfSale pos, final List<CustomerAlertType> catsList) {
        int processed = 0;
        for (CustomerAlertType cat : catsList) {
            final PosEventCurrent posEventCurrent = new PosEventCurrent();
            posEventCurrent.setPointOfSale(pos);
            posEventCurrent.setCustomerAlertType(cat);
            posEventCurrent.setEventSeverityType(SEVERITY_CLEAR);
            posEventCurrent.setEventType(EVENT_TYPE_USER_DEFINED);
            
            this.posEventCurrentService.savePosEventCurrent(posEventCurrent);
            ++processed;
        }
        
        return processed;
    }
    
    private void triggerEvents(final List<Integer> pointOfSaleIdList, final String alertQuery, final String clearQuery,
        final Integer customerAlertTypeId) {
        final List<UserDefinedEventInfo> alertList = new ArrayList<UserDefinedEventInfo>();
        final List<UserDefinedEventInfo> clearList = new ArrayList<UserDefinedEventInfo>();
        
        if (customerAlertTypeId == null) {
            alertList.addAll(this.userDefinedEventInfoService.findEventsByNamedQueryAndPointOfSaleIdList(alertQuery, pointOfSaleIdList));
            clearList.addAll(this.userDefinedEventInfoService.findEventsByNamedQueryAndPointOfSaleIdList(clearQuery, pointOfSaleIdList));
        } else {
            alertList.addAll(this.userDefinedEventInfoService.findEventsByNamedQueryAndPointOfSaleIdListAndCustomerAlertTypeId(alertQuery,
                                                                                                                               pointOfSaleIdList,
                                                                                                                               customerAlertTypeId));
            clearList.addAll(this.userDefinedEventInfoService.findEventsByNamedQueryAndPointOfSaleIdListAndCustomerAlertTypeId(clearQuery,
                                                                                                                               pointOfSaleIdList,
                                                                                                                               customerAlertTypeId));
        }
        sendEvents(alertList, clearList);
    }
    
    private void sendEvents(final List<UserDefinedEventInfo> alertList, final List<UserDefinedEventInfo> clearList) {
        for (UserDefinedEventInfo userDefinedEventInfo : alertList) {
            final int severity = userDefinedEventInfo.calculateSeverity();
            //No history event for triggering Minor user defined alerts
            final byte addToHistory = severity == WebCoreConstants.SEVERITY_MINOR ? 0 : Byte.MAX_VALUE;
            final byte queueList = (byte) (WebCoreConstants.QUEUE_TYPE_RECENT_EVENT | (addToHistory & WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT));
            this.queueEventService.createEventQueue(userDefinedEventInfo.getPointOfSaleId(), null, userDefinedEventInfo.getCustomerAlertType()
                                                            .getId(), userDefinedEventInfo.getPosEventCurrent().getEventType(), severity, null,
                                                    DateUtil.getCurrentGmtDate(), null, true,
                                                    addToHistory == Byte.MAX_VALUE, WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID, queueList);
        }
        for (UserDefinedEventInfo userDefinedEventInfo : clearList) {
            //No history event for clearing Minor user defined alerts
            final int severity = userDefinedEventInfo.calculateSeverity();
            final int previousSeverity = userDefinedEventInfo.getPosEventCurrent().getEventSeverityType().getId();
            final byte addToHistory = previousSeverity == WebCoreConstants.SEVERITY_MINOR ? 0 : Byte.MAX_VALUE;
            final byte queueList = (byte) (WebCoreConstants.QUEUE_TYPE_RECENT_EVENT | (addToHistory & WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT));
            this.queueEventService.createEventQueue(userDefinedEventInfo.getPointOfSaleId(), null, userDefinedEventInfo.getCustomerAlertType()
                                                            .getId(), userDefinedEventInfo.getPosEventCurrent().getEventType(), severity, null,
                                                    DateUtil.getCurrentGmtDate(), null,
                                                    severity > 0, addToHistory == Byte.MAX_VALUE, WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID,
                                                    queueList);
        }
    }
    
    private List<PosEventCurrent> scrollPosEventCurrents(final Collection<Integer> pointOfSaleIds, final Collection<Integer> collectionAlertTypeIds) {
        Collection<Integer> catIds = collectionAlertTypeIds;
        if (catIds == null) {
            catIds = new ArrayList<Integer>(1);
            /*
             * PosEventCurrent.findUserDefinedPosEventCurrentByPointOfSaleIdsAndCustomerAlertTypeIds uses IN (...)
             * in HQL and must have a number in the brackets.
             */
            catIds.add(new Integer(0));
        }
        
        return this.entityDao.findByNamedQueryAndNamedParam("PosEventCurrent.findUserDefinedPosEventCurrentByPointOfSaleIdsAndCustomerAlertTypeIds",
                                                            new String[] { HibernateConstants.POINTOFSALE_IDS,
                                                                HibernateConstants.CUSTOMER_ALERT_TYPE_IDS, },
                                                            new Object[] { pointOfSaleIds, catIds, });
        
    }
    
    private void flushUpdates() {
        this.entityDao.getCurrentSession().flush();
        this.entityDao.getCurrentSession().clear();
    }
    
    private CustomerAlertTypeEventProcessor self() {
        return this.serviceHelper.bean("customerAlertTypeEventProcessor", CustomerAlertTypeEventProcessor.class);
    }
    
}
