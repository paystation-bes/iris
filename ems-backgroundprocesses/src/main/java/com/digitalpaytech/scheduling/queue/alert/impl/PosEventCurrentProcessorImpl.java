package com.digitalpaytech.scheduling.queue.alert.impl;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.EventActionType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.EventType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.domain.PosEventDelayed;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.dto.queue.QueueEvent;
import com.digitalpaytech.scheduling.queue.alert.PosEventCurrentProcessor;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerAlertTypeService;
import com.digitalpaytech.service.EventActionTypeService;
import com.digitalpaytech.service.EventSeverityTypeService;
import com.digitalpaytech.service.EventTypeService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.PosAlertStatusService;
import com.digitalpaytech.service.PosEventCurrentService;
import com.digitalpaytech.service.PosEventDelayedService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.service.queue.QueueEventService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;

@Service("posEventCurrentProcessor")
@SuppressWarnings("PMD.GodClass")
public class PosEventCurrentProcessorImpl implements PosEventCurrentProcessor {
    
    @Autowired
    private PosEventCurrentService posEventCurrentService;
    
    @Autowired
    private QueueEventService queueEventService;
    
    @Autowired
    private EventSeverityTypeService eventSeverityTypeService;
    
    @Autowired
    private EventActionTypeService eventActionTypeService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private CustomerAlertTypeService customerAlertTypeService;
    
    @Autowired
    private EventTypeService eventTypeService;
    
    @Autowired
    private PosAlertStatusService posAlertStatusService;
    
    @Autowired
    private PosEventDelayedService posEventDelayedService;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    @Autowired
    private RouteService routeService;
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    @SuppressWarnings("checkstyle:cyclomaticcomplexity")
    public final void processQueueEvent(final QueueEvent queueEvent) {
        
        PosEventCurrent posEventCurrent = this.posEventCurrentService
                .findPosEventCurrentByPointOfSaleIdAndEventTypeIdAndCustomerAlertTypeId(queueEvent.getPointOfSaleId(), queueEvent.getEventTypeId(),
                                                                                        queueEvent.getCustomerAlertTypeId());
        
        final boolean isActive = queueEvent.getIsActive();
        final EventSeverityType eventSeverityType = this.eventSeverityTypeService.findEventSeverityType(queueEvent.getEventSeverityTypeId());
        EventActionType eventActionType = null;
        if (queueEvent.getEventActionTypeId() != null) {
            eventActionType = this.eventActionTypeService.findEventActionType(queueEvent.getEventActionTypeId());
        }
        
        final CustomerAlertType customerAlertType = queueEvent.getCustomerAlertTypeId() == null ? null
                : this.customerAlertTypeService.findCustomerAlertTypeById(queueEvent.getCustomerAlertTypeId());
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(queueEvent.getPointOfSaleId());
        final EventType eventType = this.eventTypeService.findEventTypeById(queueEvent.getEventTypeId());
        int originalSeverity = 0;
        boolean originalIsActive = false;
        if (posEventCurrent == null || posEventCurrent.getId() == null) {
            
            final String alertInfo = queueEvent.getAlertInfo();
            posEventCurrent = new PosEventCurrent(pointOfSale, eventSeverityType, customerAlertType, eventType, alertInfo, isActive, false);
            posEventCurrent.setEventActionType(eventActionType);
            if (isActive) {
                posEventCurrent.setAlertGmt(queueEvent.getTimestampGMT());
            } else {
                posEventCurrent.setClearedGmt(queueEvent.getTimestampGMT());
            }
            this.posEventCurrentService.savePosEventCurrent(posEventCurrent);
            
            if (!isActive) {
                return;
            }
        } else {
            //Update existing event
            posEventCurrent.setEventActionType(eventActionType);
            originalSeverity = posEventCurrent.getEventSeverityType().getId();
            originalIsActive = posEventCurrent.isIsActive();
            if (!updateCurrentEventStatus(posEventCurrent, queueEvent, eventSeverityType, eventActionType)) {
                return;
            }
        }
        
        this.posAlertStatusService.updateAlertCount(posEventCurrent, originalSeverity, originalIsActive && posEventCurrent.isIsActive());
        
        if (queueEvent.getIsNotification()) {
            // This means the severity is reduced form Critical to Minor and the actual alert is cleared.
            if (originalIsActive && posEventCurrent.isIsActive() && queueEvent.getCustomerAlertTypeId() != null
                && originalSeverity > posEventCurrent.getEventSeverityType().getId()) {
                queueEvent.setEventSeverityTypeId(WebCoreConstants.SEVERITY_CLEAR);
                queueEvent.setIsActive(false);
            }
            
            // get CustomerAlertType and process further
            if (customerAlertType == null) {
                findCustomerAlertAndProcess(pointOfSale, queueEvent, posEventCurrent, eventSeverityType, eventActionType, eventType);
            } else {
                this.queueEventService.addItemToQueue(queueEvent, WebCoreConstants.QUEUE_TYPE_PROCESSING_EVENT);
            }
        }
    }
    
    private void findCustomerAlertAndProcess(final PointOfSale pointOfSale, final QueueEvent queueEvent, final PosEventCurrent posEventCurrent,
        final EventSeverityType eventSeverityType, final EventActionType eventActionType, final EventType eventType) {
        final Collection<CustomerAlertType> customerAlertTypes = this.customerAlertTypeService
                .findCustomerAlertTypesByCustomerIdAlertThresholdTypeIdAndIsActive(pointOfSale.getCustomer().getId(),
                                                                                   WebCoreConstants.ALERT_THRESHOLD_TYPE_PAYSTATION, true);
        if (!WebCoreUtil.isCollectionEmpty(customerAlertTypes)) {
            final List<Route> routeList = this.routeService.findRoutesByPointOfSaleId(queueEvent.getPointOfSaleId());
            final Set<Integer> routeIds = new HashSet<Integer>();
            if (!WebCoreUtil.isCollectionEmpty(routeList)) {
                for (Route route : routeList) {
                    routeIds.add(route.getId());
                }
            }
            for (CustomerAlertType customerAlertType : customerAlertTypes) {
                boolean sendEmail = false;
                if (customerAlertType.getRoute() == null || routeIds.contains(customerAlertType.getRoute().getId())) {
                    sendEmail = true;
                }
                queueEvent.setCustomerAlertTypeId(customerAlertType.getId());
                
                if (sendEmail) {
                    if (posEventCurrent.isIsActive()) {
                        if (eventType.isDelayable() && customerAlertType.getIsDelayed() && customerAlertType.getDelayedByMinutes() > 0
                            && new Date().before(DateUtil.addMinutesToDate(queueEvent.getTimestampGMT(), customerAlertType.getDelayedByMinutes()))) {
                            // Add event to POSEventDelayed
                            final PosEventDelayed posEventDelayed = new PosEventDelayed(pointOfSale, eventSeverityType, eventActionType,
                                    customerAlertType, eventType, queueEvent.getAlertInfo(), posEventCurrent.isIsActive(),
                                    posEventCurrent.isIsHidden(), queueEvent.getTimestampGMT(), null,
                                    DateUtil.addMinutesToDate(queueEvent.getTimestampGMT(), customerAlertType.getDelayedByMinutes()));
                            this.posEventDelayedService.save(posEventDelayed);
                        } else {
                            this.queueEventService.addItemToQueue(createNewQueueEvent(queueEvent, customerAlertType.getId()),
                                                                  WebCoreConstants.QUEUE_TYPE_PROCESSING_EVENT);
                        }
                    } else {
                        if (customerAlertType.getIsDelayed()) {
                            // Clear event from POSEventDelayed
                            final Collection<PosEventDelayed> delayedEvents = this.posEventDelayedService
                                    .findByCustomerAlertTypeAndPointOfSaleAndEventTypeId(customerAlertType, pointOfSale, queueEvent.getEventTypeId());
                            if (delayedEvents != null && !delayedEvents.isEmpty()) {
                                // remove it from POSEventDelayed. Don't add clear event to AlertProcessing Queue. 
                                delayedEvents.forEach(this.posEventDelayedService::delete);
                            } else {
                                this.queueEventService.addItemToQueue(createNewQueueEvent(queueEvent, customerAlertType.getId()),
                                                                      WebCoreConstants.QUEUE_TYPE_PROCESSING_EVENT);
                            }
                        } else {
                            this.queueEventService.addItemToQueue(createNewQueueEvent(queueEvent, customerAlertType.getId()), 
                                                                  WebCoreConstants.QUEUE_TYPE_PROCESSING_EVENT);
                        }
                    }
                }
            }
        }
    }

    /*
     * Since QueueEvent object was created from a caller (e.g. RecentEventsConsumerTask.java) the same object reference will be used when
     * passing to 'this.queueEventService.addItemToQueue'. This method creates a new QueueEvent object to avoid data being overwritten.
     * queueEventProcessing.setCustomerAlertTypeId(customerAlertTypeId);
     * Since in a multi threaded environment, before we clone the queueEvent instance, the customerAlertTypeId in it could have been 
     * changed. Hence by setting it explicitly in the next line using the method local (thread-safe) variable, we are ensuring queueEvent 
     * has the correct customerAlertTypeId.
     */
    private QueueEvent createNewQueueEvent(final QueueEvent queueEvent, final Integer customerAlertTypeId) {
        final QueueEvent queueEventProcessing = QueueEvent.newInstance(queueEvent);
        queueEventProcessing.setCustomerAlertTypeId(customerAlertTypeId);
        return queueEventProcessing;
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    private boolean updateCurrentEventStatus(final PosEventCurrent currentEvent, final QueueEvent queueEvent,
        final EventSeverityType eventSeverityType, final EventActionType eventActionType) {
        
        boolean saveEventStatus = false;
        boolean returnValue = false;
        final boolean isAlertHappenedBefore =
                currentEvent.isIsActive() && happenedBefore(currentEvent.getAlertGmt(), queueEvent.getTimestampGMT(), !queueEvent.getIsActive());
        final boolean isClearHappenedBefore =
                !currentEvent.isIsActive() && happenedBefore(currentEvent.getClearedGmt(), queueEvent.getTimestampGMT(), false);
        final boolean isSameEvent = isSameEvent(currentEvent, queueEvent);
        if (queueEvent.getIsActive()) {
            if (isAlertHappenedBefore) {
                currentEvent.setAlertGmt(queueEvent.getTimestampGMT());
                saveEventStatus = true;
                if (isSameEvent) {
                    returnValue = false;
                } else {
                    returnValue = true;
                    currentEvent.setEventSeverityType(eventSeverityType);
                    currentEvent.setEventActionType(eventActionType);
                }
            } else if (isClearHappenedBefore) {
                currentEvent.setIsActive(true);
                currentEvent.setClearedGmt(null);
                currentEvent.setAlertGmt(queueEvent.getTimestampGMT());
                currentEvent.setEventSeverityType(eventSeverityType);
                currentEvent.setEventActionType(eventActionType);
                saveEventStatus = true;
                returnValue = true;
            } else {
                returnValue = false;
            }
        } else {
            if (isAlertHappenedBefore) {
                currentEvent.setIsActive(false);
                currentEvent.setClearedGmt(queueEvent.getTimestampGMT());
                currentEvent.setEventSeverityType(eventSeverityType);
                currentEvent.setEventActionType(eventActionType);
                saveEventStatus = true;
                returnValue = true;
            } else if (isClearHappenedBefore) {
                currentEvent.setClearedGmt(queueEvent.getTimestampGMT());
                saveEventStatus = true;
                returnValue = false;
            } else {
                returnValue = false;
            }
        }
        
        if (saveEventStatus) {
            this.posEventCurrentService.updatePosEventCurrent(currentEvent);
        }
        
        return returnValue;
    }
    
    private boolean isSameEvent(final PosEventCurrent currentEvent, final QueueEvent queueEvent) {
        return currentEvent.getEventSeverityType().getId() == queueEvent.getEventSeverityTypeId()
               && (currentEvent.getEventActionType() == null && queueEvent.getEventActionTypeId() == null
                   || currentEvent.getEventActionType() != null && queueEvent.getEventActionTypeId() != null
                      && currentEvent.getEventActionType().getId() == queueEvent.getEventActionTypeId().intValue());
    }
    
    //PMD.UselessParentheses : False positive
    @SuppressWarnings("PMD.UselessParentheses")
    private boolean happenedBefore(final Date currentStatusGMT, final Date newEventGMT, final boolean compareAlertVsCleared) {
        // If alert and clear are send at the same time clear is considered more current
        return currentStatusGMT == null || (compareAlertVsCleared ? !currentStatusGMT.after(newEventGMT) : currentStatusGMT.before(newEventGMT));
    }
    
    public final PosEventCurrentService getPosEventCurrentService() {
        return this.posEventCurrentService;
    }
    
    public final void setPosEventCurrentService(final PosEventCurrentService posEventCurrentService) {
        this.posEventCurrentService = posEventCurrentService;
    }
    
    public final QueueEventService getQueueEventService() {
        return this.queueEventService;
    }
    
    public final void setQueueEventService(final QueueEventService queueEventService) {
        this.queueEventService = queueEventService;
    }
    
    public final PointOfSaleService getPointOfSaleService() {
        return this.pointOfSaleService;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final CustomerAlertTypeService getCustomerAlertTypeService() {
        return this.customerAlertTypeService;
    }
    
    public final void setCustomerAlertTypeService(final CustomerAlertTypeService customerAlertTypeService) {
        this.customerAlertTypeService = customerAlertTypeService;
    }
    
    public final EventTypeService getEventTypeService() {
        return this.eventTypeService;
    }
    
    public final void setEventTypeService(final EventTypeService eventTypeService) {
        this.eventTypeService = eventTypeService;
    }
    
    public final EventSeverityTypeService getEventSeverityTypeService() {
        return this.eventSeverityTypeService;
    }
    
    public final void setEventSeverityTypeService(final EventSeverityTypeService eventSeverityTypeService) {
        this.eventSeverityTypeService = eventSeverityTypeService;
    }
    
    public final EventActionTypeService getEventActionTypeService() {
        return this.eventActionTypeService;
    }
    
    public final void setEventActionTypeService(final EventActionTypeService eventActionTypeService) {
        this.eventActionTypeService = eventActionTypeService;
    }
    
    public final PosAlertStatusService getPosAlertStatusService() {
        return this.posAlertStatusService;
    }
    
    public final void setPosAlertStatusService(final PosAlertStatusService posAlertStatusService) {
        this.posAlertStatusService = posAlertStatusService;
    }
    
    public PosEventDelayedService getPosEventDelayedService() {
        return this.posEventDelayedService;
    }
    
    public void setPosEventDelayedService(final PosEventDelayedService posEventDelayedService) {
        this.posEventDelayedService = posEventDelayedService;
    }
    
    public final CustomerAdminService getCustomerAdminService() {
        return this.customerAdminService;
    }
    
    public final void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    public final RouteService getRouteService() {
        return this.routeService;
    }
    
    public final void setRouteService(final RouteService routeService) {
        this.routeService = routeService;
    }
    
}
