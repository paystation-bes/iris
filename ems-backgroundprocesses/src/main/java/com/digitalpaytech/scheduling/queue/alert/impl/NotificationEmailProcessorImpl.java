package com.digitalpaytech.scheduling.queue.alert.impl;

import javax.mail.MessagingException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.dto.queue.QueueNotificationEmail;
import com.digitalpaytech.scheduling.queue.alert.NotificationEmailProcessor;
import com.digitalpaytech.service.EmailService;
import com.digitalpaytech.util.dto.Email;

@Component("notificationEmailProcessor")
public class NotificationEmailProcessorImpl implements NotificationEmailProcessor {
    
    private static final Logger LOGGER = Logger.getLogger(NotificationEmailProcessorImpl.class);
    
    @Autowired
    private EmailService emailService;
    
    public final EmailService getEMailService() {
        return this.emailService;
    }
    
    public final void setEMailService(final EmailService emailService) {
        this.emailService = emailService;
    }
    
    @Override
    public final void sendNotificationEmail(final QueueNotificationEmail queueNotificationEmail) {
        try {
            this.emailService.sendNow(new Email().to(queueNotificationEmail.getToAddress()).subject(queueNotificationEmail.getSubject())
                    .content(queueNotificationEmail.getContent()));
            
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("EMAIL LOG - Address: " + queueNotificationEmail.getToAddress() + ", subject:" + queueNotificationEmail.getSubject()
                             + ", content:" + queueNotificationEmail.getContent());
            }
            
        } catch (MessagingException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("EMAIL FAILED LOG - Address: " + queueNotificationEmail.getToAddress() + ", subject:" + queueNotificationEmail.getSubject()
                             + ", content:" + queueNotificationEmail.getContent());
            }

            throw new RuntimeException("Failed to send notification email", e);
        }
    }
}
