package com.digitalpaytech.scheduling.queue.alert;

import com.digitalpaytech.dto.queue.QueueEvent;

public interface PosEventHistoryProcessor {
    
    void savePosEventHistory(QueueEvent queueEvent);
}
