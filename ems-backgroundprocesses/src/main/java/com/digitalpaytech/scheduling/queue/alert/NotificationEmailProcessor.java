package com.digitalpaytech.scheduling.queue.alert;

import com.digitalpaytech.dto.queue.QueueNotificationEmail;

public interface NotificationEmailProcessor {
    
    void sendNotificationEmail(QueueNotificationEmail queueNotificationEmail);
}
