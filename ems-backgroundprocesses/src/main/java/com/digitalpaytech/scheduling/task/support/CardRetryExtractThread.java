package com.digitalpaytech.scheduling.task.support;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Date;

import org.apache.log4j.Logger;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.dto.ProcessorInfo;
import com.digitalpaytech.scheduling.task.NightlyTransactionRetryProcessingTask;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

public class CardRetryExtractThread extends Thread {
    private static final String FMT_THREAD_NAME = "CardRetryExtract_{0}_{2}";
    
    private static Logger logger = Logger.getLogger(CardRetryExtractThread.class);
    
    private int threadId;
    private ProcessorInfo processorInfo;
    private Collection<Customer> customers;
    private CustomerAdminService customerAdminService;
    private MerchantAccountService merchantAccountService;
    private Date chargeRetryStartTime;
    private NightlyTransactionRetryProcessingTask nightlyTransactionRetryProcessor;
    
    public CardRetryExtractThread(final int threadId, final ProcessorInfo processorInfo, final Collection<Customer> customers,
        final CustomerAdminService customerAdminService, final MerchantAccountService merchantAccountService, final Date chargeRetryStartTime,
        final NightlyTransactionRetryProcessingTask nightlyTransactionRetryProcessor) {
        super(MessageFormat.format(FMT_THREAD_NAME, processorInfo.getId(), processorInfo.getName(), threadId));
        
        this.threadId = threadId;
        this.processorInfo = processorInfo;
        this.customers = customers;
        this.customerAdminService = customerAdminService;
        this.merchantAccountService = merchantAccountService;
        this.chargeRetryStartTime = chargeRetryStartTime;
        this.nightlyTransactionRetryProcessor = nightlyTransactionRetryProcessor;
    }
    
    public final void run() {
        logger.info("Start extracting card retry transactions for " + this.processorInfo.getName() + " processor.");
        for (Customer customer : this.customers) {
            if (customer.isIsMigrated()) {
                try {
                    final CustomerProperty custPerm = this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(
                        customer.getId(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_MAX_OFFLINE_RETRY);
                    final Collection<MerchantAccount> merchantAccounts = this.merchantAccountService.getValidMerchantAccountsForProcessor(
                        customer.getId(), this.processorInfo.getId());
                    
                    // iterate through all merchant accounts for this customer
                    for (final MerchantAccount acct : merchantAccounts) {
                        try {
                            this.nightlyTransactionRetryProcessor.processMerchantAccount(acct, this.processorInfo,
                                Integer.parseInt(custPerm.getPropertyValue(), StandardConstants.LIMIT_10), this.chargeRetryStartTime);
                        } catch (Exception e) {
                            logger.error("Unable to extract card retry transactions for merchant account " + acct.getId(), e);
                        }
                    }
                } catch (Exception e) {
                    logger.error("Unable to extract card retry transactions for customer " + customer.getId(), e);
                }
            }
        }
        
        logger.info("Finish extracting card retry transactions for " + this.processorInfo.getName() + " processor.");
        this.nightlyTransactionRetryProcessor.cardRetryExtractThreadFinished(this.threadId);
    }
}
