package com.digitalpaytech.scheduling.task.kafka.consumer;

import java.time.Duration;
import java.util.Date;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.KafkaException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.SettingsFile;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.scheduling.task.support.DeviceNotification;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.PaystationSettingService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;
import com.digitalpaytech.util.kafka.MessageConsumerFactory;

@Component(value = "fmsSettingsNotificationConsumerTask")
public class FMSSettingsNotificationConsumerTask extends ConsumerTask {
    
    private static final Logger LOG = Logger.getLogger(FMSSettingsNotificationConsumerTask.class);
    
    @Resource(name = KafkaKeyConstants.IRIS_PROPERTIES)
    private Properties irisKafka;
    
    @Resource(name = KafkaKeyConstants.TOPIC_NAMES_PROPERTIES)
    private Properties topicNames;
    
    @Autowired
    private PosServiceStateService posServiceStateService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private MessageConsumerFactory messageConsumerFactory;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private PaystationSettingService paystationSettingService;
    
    private KafkaConsumer<String, String> consumer;
    
    @PostConstruct
    public void init() {
        this.consumer = super.buildConsumer(this.topicNames.getProperty(KafkaKeyConstants.FMS_KAFKA_UPGRADE_TOPIC_NAME),
                                            this.topicNames.getProperty(KafkaKeyConstants.FMS_KAFKA_UPGRADE_CONSUMER_GROUP), this.irisKafka,
                                            this.topicNames.getProperty(KafkaKeyConstants.DELIMETER));
    }
    
    @PreDestroy
    protected void close() {
        super.closeConsumer(this.consumer);
    }
    
    @Scheduled(fixedDelayString = "${consumer.millisecond.delay}")
    public void processOnSchedule() {
        process();
    }
    
    @Async("kafkaConsumerExecutor")
    public void process() {
        super.traceLog(LOG, this.topicNames, KafkaKeyConstants.FMS_KAFKA_UPGRADE_TOPIC_NAME, KafkaKeyConstants.FMS_KAFKA_UPGRADE_CONSUMER_GROUP);
        final ConsumerRecords<String, String> consumerRecords = this.consumer.poll(Duration.ofMillis(super.getDefaultConsumerPollTimeout()));
        for (ConsumerRecord<String, String> record : consumerRecords) {
            
            try {
                final DeviceNotification deviceNotification = this.messageConsumerFactory.deserialize(record.value(), DeviceNotification.class);
                final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleBySerialNumber(deviceNotification.getDeviceId());
                if (verifyPos(pointOfSale, deviceNotification)) {
                    final PosServiceState posServiceState = this.posServiceStateService.findPosServiceStateById(pointOfSale.getId(), false);
                    if (posServiceState != null) {
                        posServiceState.setIsNewPaystationSetting(true);
                        final SettingsFile settingsFile = this.paystationSettingService
                                .findSettingsFileByCustomerIdAndName(pointOfSale.getCustomer().getId(), deviceNotification.getGroupName());
                        posServiceState.setSettingsFile(settingsFile);
                        final Date nowGmt = DateUtil.getCurrentGmtDate();
                        posServiceState.setLastPaystationSettingUploadGmt(nowGmt);
                        posServiceState.setLastModifiedGmt(nowGmt);
                        this.posServiceStateService.updatePosServiceState(posServiceState);
                    } else {
                        LOG.error("Update PosServiceState.IsNewPaystationSetting but PosServiceState not found for PointOfSaleId: "
                                  + pointOfSale.getId() + " with message: " + record.value());
                    }
                }
            } catch (JsonException e) {
                LOG.error("Invalid JSON recieved from Kafka by paystationSettingsFileConsumerTask: " + record.value(), e);
            }
        }
        try {
            this.consumer.commitSync();
        } catch (KafkaException kex) {
            LOG.error(kex);
        }
    }
    
    private boolean verifyPos(final PointOfSale pointOfSale, final DeviceNotification deviceNotification) {
        
        boolean verified = true;
        if (pointOfSale != null) {
            final int customerId = pointOfSale.getCustomer().getId();
            final Customer customer = this.customerService.findCustomer(customerId);
            final String messageCustomerId = deviceNotification.getCustomerId();
            try {
                if (customer.getId() != Integer.parseInt(messageCustomerId)) {
                    LOG.warn(printError(customer.getId(), messageCustomerId));
                    verified = false;
                }
            } catch (NumberFormatException nfe) {
                LOG.error(nfe);
                LOG.warn(printError(customer.getId(), messageCustomerId));
                verified = false;
            }
        } else {
            LOG.error("Update PosServiceState.IsNewPaystationSetting but PointOfSale not found for deviceId: "
                      + deviceNotification.getDeviceId() + " with message [" + deviceNotification + "]");
            verified = false;
        }
        
        return verified;
        
    }
    
    private String printError(final Integer customreId, final String messageCustomerId) {
        return "customerId in Iris [" + customreId + "] does not match customerId recieved from FMS [" + messageCustomerId + "]";
    }
    
}
