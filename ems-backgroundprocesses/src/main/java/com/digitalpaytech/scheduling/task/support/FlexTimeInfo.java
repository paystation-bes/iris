package com.digitalpaytech.scheduling.task.support;

import java.util.Date;
import java.util.TimeZone;

/**
 * This class is not threadsafe!
 * @author wittawatv
 *
 */
public class FlexTimeInfo {
    private static final long MILLISECS_IN_WEEK = 604800000L;
    
    private TimeZone customerTimeZone;
    private TimeZone flexTimeZone;
    
    private int weeksToInterpolate;
    private long millisecsToInterpolate;
    
    public FlexTimeInfo() {
        this(0);
    }
    
    public FlexTimeInfo(final int weeksToInterpolate) {
        this.weeksToInterpolate = weeksToInterpolate;
        this.millisecsToInterpolate = this.weeksToInterpolate * MILLISECS_IN_WEEK;
    }
    
    public final Date interpolate(final Date date) {
        return new Date(interpolate(date.getTime()));
    }
    
    public final long interpolate(final long timestamp) {
        long result = timestamp;
        if (this.weeksToInterpolate != 0) {
            result -= this.millisecsToInterpolate;
        }
        
        return result;
    }
    
    public final Date deInterpolate(final Date date) {
        return new Date(deInterpolate(date.getTime()));
    }
    
    public final long deInterpolate(final long timestamp) {
        long result = timestamp;
        if (this.weeksToInterpolate != 0) {
            result += this.millisecsToInterpolate;
        }
        
        return result;
    }
    
    public final TimeZone getCustomerTimeZone() {
        return this.customerTimeZone;
    }
    
    public final void setCustomerTimeZone(final TimeZone customerTimeZone) {
        this.customerTimeZone = customerTimeZone;
    }
    
    public final TimeZone getFlexTimeZone() {
        return this.flexTimeZone;
    }
    
    public final void setFlexTimeZone(final TimeZone flexTimeZone) {
        this.flexTimeZone = flexTimeZone;
    }
}
