package com.digitalpaytech.scheduling.task.kafka.consumer;

import java.io.UnsupportedEncodingException;
import java.time.Duration;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.KafkaException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.domain.CPSRefundData;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.cps.CPSRefundDataService;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;

@Component(value = "offlineRefundConsumerTask")
@Transactional(propagation = Propagation.REQUIRED)
public class OfflineRefundConsumerTask extends ConsumerTask {
    
    private static final Logger LOG = Logger.getLogger(OfflineRefundConsumerTask.class);
    
    @Resource(name = KafkaKeyConstants.PCI_LINK_PROPERTIES)
    private Properties pciLinkKafka;
    
    @Resource(name = KafkaKeyConstants.PCI_LINK_TOPIC_NAMES_PROPERTIES)
    private Properties pciLinkTopicNames;
    
    @Autowired
    private CPSRefundDataService cpsRefundDataService;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    private KafkaConsumer<String, String> consumer;
    
    @PostConstruct
    public void init() {
        this.consumer = super.buildConsumer(this.pciLinkTopicNames.getProperty(KafkaKeyConstants.OFFLINE_REFUND_RESULT_TOPIC_NAME_KEY),
                                            this.pciLinkTopicNames.getProperty(KafkaKeyConstants.OFFLINE_REFUND_RESULT_TOPIC_CONSUMER_GROUP),
                                            this.pciLinkKafka, this.pciLinkTopicNames.getProperty(KafkaKeyConstants.DELIMETER));
    }
    
    @PreDestroy
    public void close() {
        super.closeConsumer(this.consumer);
    }
    
    @Scheduled(fixedDelayString = "${consumer.millisecond.delay}")
    public void processOnSchedule() {
        process();
    }
    
    @Async("kafkaConsumerExecutor")
    public void process() {
        super.traceLog(LOG, this.pciLinkTopicNames, KafkaKeyConstants.OFFLINE_REFUND_RESULT_TOPIC_NAME_KEY,
                       KafkaKeyConstants.OFFLINE_REFUND_RESULT_TOPIC_CONSUMER_GROUP);
        
        final ConsumerRecords<String, String> consumerRecords = this.consumer.poll(Duration.ofMillis(super.getDefaultConsumerPollTimeout()));
        
        for (ConsumerRecord<String, String> rec : consumerRecords) {
            try {
                final CPSRefundData cpsRefundData =
                        super.getMessageConsumerFactory().deserializeMessageWithEmbeddedHeaders(rec.value(), CPSRefundData.class);
                
                if (LOG.isDebugEnabled()) {
                    LOG.debug(String.format("Processing message in %s topic [%s]",
                                            super.getTopicName(KafkaKeyConstants.OFFLINE_REFUND_RESULT_TOPIC_NAME_KEY, this.pciLinkTopicNames),
                                            cpsRefundData.toString()));
                }
                
                if (this.merchantAccountService.findByTerminalTokenAndIsLink(cpsRefundData.getTerminalToken()) == null) {
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Ignoring unrecognized SnFChargeResultMessage.terminalToken" + rec.value());
                    }
                } else {
                    this.cpsRefundDataService.manageCPSRefundData(cpsRefundData);
                }
            } catch (UnsupportedEncodingException | JsonException e) {
                LOG.error(String.format("Unable to process message in %s topic [%s]",
                                        super.getTopicName(KafkaKeyConstants.OFFLINE_REFUND_RESULT_TOPIC_NAME_KEY, this.pciLinkTopicNames),
                                        rec.value()));
            }
        }
        
        try {
            this.consumer.commitSync();
        } catch (KafkaException kex) {
            LOG.error(kex);
        }
    }
}
