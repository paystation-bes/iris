package com.digitalpaytech.scheduling.task.support;

public class ConfigurationFileInfo {
    
    private String fileId;
    private String hash;
    private long size;
    private String fileName;
    
    public ConfigurationFileInfo() {
        super();
    }
    
    public final String getFileId() {
        return this.fileId;
    }
    
    public final void setFileId(final String fileId) {
        this.fileId = fileId;
    }
    
    public final String getHash() {
        return this.hash;
    }
    
    public final void setHash(final String hash) {
        this.hash = hash;
    }
    
    public final long getSize() {
        return this.size;
    }
    
    public final void setSize(final long size) {
        this.size = size;
    }
    
    public final String getFileName() {
        return this.fileName;
    }
    
    public final void setFileName(final String fileName) {
        this.fileName = fileName;
    }
    
}
