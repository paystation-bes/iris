package com.digitalpaytech.scheduling.task.kafka.consumer;

import java.util.Arrays;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.kafka.MessageConsumerFactory;

@SuppressWarnings("PMD.AbstractClassWithoutAbstractMethod")
public abstract class ConsumerTask {
    static final String UNSUPPORTED_EXCEPTION_MSG = "Invalid raw message with embedded headers received from Kafka: ";
    
    @Autowired
    private MessageConsumerFactory messageConsumerFactory;
    
    @Value("${consumer.poll.timeout.millisecond:20}")
    private int defaultConsumerPollTimeoutMs;
    
    protected final KafkaConsumer<String, String> buildConsumer(final String topicName, final String consumerGroup, final Properties kafkaProperties,
        final String delimeter) {
        validateConsumerProperties(topicName, consumerGroup, delimeter);
        // Since input variable "kafkaProperties" is a singleton, need to make a copy of it for uniqueness. 
        final Properties consumerKafkaProperties = (Properties) kafkaProperties.clone();
        return this.messageConsumerFactory.buildConsumer(consumerKafkaProperties, Arrays.asList(topicName), consumerGroup);
    }
    
    protected final void closeConsumer(final KafkaConsumer<String, String> consumer) {
        if (consumer != null) {
            consumer.close();
        }
    }
    
    protected final String getConsumerRecordData(final ConsumerRecord<String, String> rec) {
        final StringBuilder bdr = new StringBuilder(145);
        bdr.append("Cannot deserialize: ").append(rec.value()).append(StandardConstants.STRING_NEXT_LINE).append("CustomerRecord data - key: ")
                .append(rec.key()).append(", value: ").append(rec.value()).append(", offset: ").append(rec.offset()).append(", partition: ")
                .append(rec.partition()).append(", serializedKeySize: ").append(rec.serializedKeySize()).append(", serializedValueSize: ")
                .append(rec.serializedValueSize()).append(", timestamp: ").append(rec.timestamp()).append(", topic: ").append(rec.topic());
        return bdr.toString();
        
    }
    
    protected final String getTopicName(final String topicNameKey, final Properties topicProperties) {
        return topicProperties == null ? StandardConstants.STRING_EMPTY_STRING : topicProperties.getProperty(topicNameKey);
    }
    
    protected final void traceLog(final Logger log, final Properties topicProperties, final String topicKey, final String groupKey) {
        if (log.isTraceEnabled() && topicProperties != null) {
            final String topicName = topicProperties.getProperty(topicKey);
            final String groupName = topicProperties.getProperty(groupKey);
            log.info("Kafka Consumer in group " + (groupName == null ? StandardConstants.STRING_EMPTY_STRING : groupName) + " polling on topic: "
                      + (topicName == null ? StandardConstants.STRING_EMPTY_STRING : topicName));
        }
    }
    
    protected final long getDefaultConsumerPollTimeout() {
        return this.defaultConsumerPollTimeoutMs;
    }
    
    protected final void validateConsumerProperties(final String topic, final String consumerGroup, final String delimiter) {
        
        if (StringUtils.isBlank(delimiter)) {
            throw new IllegalArgumentException("delimiter must be provided " + String.valueOf(delimiter));
        }
        
        final String regex = delimiter + ".*" + delimiter;
        
        if (StringUtils.isBlank(consumerGroup) || Pattern.compile(regex).matcher(consumerGroup).matches()) {
            throw new IllegalArgumentException("consumerGroupId must be provided " + String.valueOf(consumerGroup));
        }
        
        if (StringUtils.isBlank(topic) || Pattern.compile(regex).matcher(consumerGroup).matches()) {
            throw new IllegalArgumentException("topics must be provided " + String.valueOf(topic));
        }
    }
    
    public final MessageConsumerFactory getMessageConsumerFactory() {
        return this.messageConsumerFactory;
    }
}
