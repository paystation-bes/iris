package com.digitalpaytech.scheduling.task.t2flex;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.FlexDataWSCallStatus;
import com.digitalpaytech.domain.FlexWSUserAccount;
import com.digitalpaytech.dto.dataset.ContraventionTypeQueryDataSet;
import com.digitalpaytech.dto.dataset.FacilityQueryDataSet;
import com.digitalpaytech.dto.dataset.PropertyQueryDataSet;
import com.digitalpaytech.scheduling.task.support.FlexTimeInfo;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.CustomerSubscriptionService;
import com.digitalpaytech.service.FlexWSService;
import com.digitalpaytech.service.T2FlexWsService;
import com.digitalpaytech.util.FlexConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Component(SynchronizeT2FlexDataTask.INSTANCE_NAME)
public class SynchronizeT2FlexDataTask {
    public static final String INSTANCE_NAME = "synchronizeT2FlexDataTask";
    
    private static final Logger LOGGER = Logger.getLogger(SynchronizeT2FlexDataTask.class);
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private CustomerSubscriptionService customerSubscriptionService;
    
    @Autowired
    private FlexWSService flexWSService;
    
    @Autowired
    private T2FlexWsService t2FlexWsService;
    
    @Autowired
    private BeanFactory beanFactory;
    
    public final void setFlexWSService(final FlexWSService flexWSService) {
        this.flexWSService = flexWSService;
    }
    
    public final void setT2FlexWsService(final T2FlexWsService t2FlexWsService) {
        this.t2FlexWsService = t2FlexWsService;
    }
    
    public final CustomerAdminService getCustomerAdminService() {
        return this.customerAdminService;
    }
    
    public final CustomerSubscriptionService getCustomerSubscriptionService() {
        return this.customerSubscriptionService;
    }
    
    public final FlexWSService getFlexWSService() {
        return this.flexWSService;
    }
    
    public final T2FlexWsService getT2FlexWsService() {
        return this.t2FlexWsService;
    }
    
    public final void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    public final void setCustomerSubscriptionService(final CustomerSubscriptionService customerSubscriptionService) {
        this.customerSubscriptionService = customerSubscriptionService;
    }
    
    private SynchronizeT2FlexDataTask self() {
        return this.beanFactory.getBean(INSTANCE_NAME, SynchronizeT2FlexDataTask.class);
    }
    
    @SuppressWarnings({ "checkstyle:designforextension" })
    @Transactional(propagation = Propagation.SUPPORTS)
    public void syncT2Data() {
        if (!this.flexWSService.isFlexDataExtractionServer()) {
            return;
        }
        
        final List<Future<Boolean>> results = new ArrayList<Future<Boolean>>();
        final SynchronizeT2FlexDataTask self = self();
        
        final List<Customer> customerList = this.customerService.findCustomerBySubscriptionTypeId(WebCoreConstants.SUBSCRIPTION_TYPE_FLEX_INTEGRATION);
        final FlexTimeInfo timeInfo = new FlexTimeInfo();
        
        for (Customer customer : customerList) {
            final FlexWSUserAccount flexAccount = this.flexWSService.findFlexWSUserAccountByCustomerId(customer.getId());
            if (flexAccount != null) {
                final CustomerProperty customerProperty = this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(customer
                        .getId(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
                
                timeInfo.setCustomerTimeZone(TimeZone.getTimeZone(customerProperty.getPropertyValue()));
                timeInfo.setFlexTimeZone(TimeZone.getTimeZone(flexAccount.getTimeZone()));
                
                results.add(self.syncLocationFacilities(flexAccount, timeInfo));
                results.add(self.syncLocationProperties(flexAccount, timeInfo));
                results.add(self.syncCitationTypes(flexAccount, timeInfo));
            }
        }
        
        // Ensure that there are no overlap schedules.
        waitForExecutions(results);
    }
    
    @SuppressWarnings({ "checkstyle:designforextension", "checkstyle:illegalcatch" })
    @Async
    @Transactional(propagation = Propagation.SUPPORTS)
    public Future<Boolean> syncLocationFacilities(final FlexWSUserAccount flexAccount, final FlexTimeInfo timeInfo) {
        boolean wasSuccessful = false;
        try {
            final Date lastSync = lastSync(flexAccount, FlexConstants.WS_TYPE_FACILITY, timeInfo);
            final Date now = new Date();
            
            final FacilityQueryDataSet modifiedFacilities = this.t2FlexWsService.getFacilities(lastSync, now, flexAccount);
            this.flexWSService.syncFacilities(modifiedFacilities.getFacilityFlexRecords(), flexAccount.getCustomer(),
                                              modifiedFacilities.getCallStatusId());
            
            wasSuccessful = true;
        } catch (Exception e) {
            LOGGER.error("Failed to synchronize Facilities for Customer: " + flexAccount.getCustomer().getId(), e);
        }
        
        return new AsyncResult<Boolean>(wasSuccessful);
    }
    
    @SuppressWarnings({ "checkstyle:designforextension", "checkstyle:illegalcatch" })
    @Async
    @Transactional(propagation = Propagation.SUPPORTS)
    public Future<Boolean> syncLocationProperties(final FlexWSUserAccount flexAccount, final FlexTimeInfo timeInfo) {
        boolean wasSuccessful = false;
        try {
            final Date lastSync = lastSync(flexAccount, FlexConstants.WS_TYPE_PROPERTY, timeInfo);
            final Date now = new Date();
            
            final PropertyQueryDataSet modifiedProperties = this.t2FlexWsService.getProperties(lastSync, now, flexAccount);
            this.flexWSService.syncProperties(modifiedProperties.getPropertyFlexRecords(), flexAccount.getCustomer(),
                                              modifiedProperties.getCallStatusId());
            
            wasSuccessful = true;
        } catch (Exception e) {
            LOGGER.error("Failed to synchronize Properties for Customer: " + flexAccount.getCustomer().getId(), e);
        }
        
        return new AsyncResult<Boolean>(wasSuccessful);
    }
    
    @SuppressWarnings({ "checkstyle:designforextension", "checkstyle:illegalcatch" })
    @Async
    @Transactional(propagation = Propagation.SUPPORTS)
    public Future<Boolean> syncCitationTypes(final FlexWSUserAccount flexAccount, final FlexTimeInfo timeInfo) {
        boolean wasSuccessful = false;
        try {
            final Date lastSync = lastSync(flexAccount, FlexConstants.WS_TYPE_CITATION_TYPE, timeInfo);
            final Date now = new Date();
            
            final ContraventionTypeQueryDataSet modifiedCitationTypes = this.t2FlexWsService.getCitationTypes(lastSync, now, flexAccount);
            this.flexWSService.syncCitationTypes(modifiedCitationTypes.getViolationFlexRecords(), flexAccount.getCustomer(),
                                                 modifiedCitationTypes.getCallStatusId());
            
            wasSuccessful = true;
        } catch (Exception e) {
            LOGGER.error("Failed to synchronize CitationTypes for Customer: " + flexAccount.getCustomer().getId(), e);
        }
        
        return new AsyncResult<Boolean>(wasSuccessful);
    }
    
    private Date lastSync(final FlexWSUserAccount flexAccount, final Byte callType, final FlexTimeInfo timeInfo) {
        Date lastSync = null;
        
        final FlexDataWSCallStatus prevStatus = this.flexWSService.findLastCallByCustomerIdAndTypeId(flexAccount.getCustomer().getId(), callType);
        if (prevStatus == null) {
            lastSync = new Date(0);
        } else {
            lastSync = prevStatus.getRequestedToDateGmt();
        }
        
        return lastSync;
    }
    
    // Call this to wait for all the "results" to complete before continue the main thread.
    private void waitForExecutions(final List<Future<Boolean>> results) {
        for (Future<?> r : results) {
            try {
                r.get();
            } catch (ExecutionException | InterruptedException e) {
                LOGGER.error(e);
            }
        }
    }
}
