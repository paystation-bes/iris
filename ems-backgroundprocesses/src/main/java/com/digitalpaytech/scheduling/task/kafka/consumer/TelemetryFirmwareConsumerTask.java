package com.digitalpaytech.scheduling.task.kafka.consumer;

import java.time.Duration;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.KafkaException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.scheduling.task.support.TelemetryFirmwareNotification;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.ServiceHelper;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;
import com.digitalpaytech.util.kafka.MessageConsumerFactory;


@Component(value = "telemetryFirmwareConsumerTask")
public class TelemetryFirmwareConsumerTask extends ConsumerTask {
    
    private static final Logger LOG = Logger.getLogger(TelemetryFirmwareConsumerTask.class);
    
    @Resource(name = KafkaKeyConstants.IRIS_PROPERTIES)
    private Properties irisKafka;
    
    @Resource(name = KafkaKeyConstants.TOPIC_NAMES_PROPERTIES)
    private Properties topicNames;
    
    @Autowired
    private PosServiceStateService posServiceStateService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private MessageConsumerFactory messageConsumerFactory;
    
    @Autowired
    private ServiceHelper serviceHelper;
    
    private KafkaConsumer<String, String> consumer;
    
    private TelemetryFirmwareConsumerTask self() {
        return this.serviceHelper.bean(TelemetryFirmwareConsumerTask.class);
    }
    
    @PostConstruct
    public final void init() {
        
        this.consumer = super.buildConsumer(this.topicNames.getProperty(KafkaKeyConstants.OTA_DEVICE_NOTIFICATION_TOPIC_NAME),
                                            this.topicNames.getProperty(KafkaKeyConstants.OTA_DEVICE_NOTIFICATION_CONSUMER_GROUP), this.irisKafka,
                                            this.topicNames.getProperty(KafkaKeyConstants.DELIMETER));
    }
    
    @PreDestroy
    public void close() {
        super.closeConsumer(this.consumer);
    }
    
    @Scheduled(fixedDelayString = "${consumer.millisecond.delay}")
    public void processOnSchedule() {
        self().process();
    }
    
    @Async("OTAkafkaExecutor")
    public void process() {
        super.traceLog(LOG, this.topicNames, KafkaKeyConstants.OTA_DEVICE_NOTIFICATION_TOPIC_NAME,
                       KafkaKeyConstants.OTA_DEVICE_NOTIFICATION_CONSUMER_GROUP);
        final ConsumerRecords<String, String> consumerRecords = this.consumer.poll(Duration.ofMillis(super.getDefaultConsumerPollTimeout()));

        if (LOG.isDebugEnabled()) {
            LOG.debug("Count of Consumer Records polled from " + KafkaKeyConstants.OTA_DEVICE_NOTIFICATION_TOPIC_NAME + " is "
                      + (consumerRecords == null ? 0 : consumerRecords.count()));
        }
        
        for (ConsumerRecord<String, String> record : consumerRecords) {
            
            try {
                final TelemetryFirmwareNotification firmwareNotification =
                        this.messageConsumerFactory.deserialize(record.value(), TelemetryFirmwareNotification.class);
                final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleBySerialNumber(firmwareNotification.getDeviceSerialNumber());
                if (pointOfSale != null) {
                    final PosServiceState posServiceState = this.posServiceStateService.findPosServiceStateById(pointOfSale.getId(), false);
                    if (posServiceState != null) {
                        posServiceState.setIsNewOTAFirmwareUpdate(firmwareNotification.getUpgradeAvailable());
                        this.posServiceStateService.updatePosServiceState(posServiceState);
                    } else {
                        LOG.error("Update PosServiceState.IsNewPaystationSetting but PosServiceState not found for PointOfSaleId: "
                                  + pointOfSale.getId() + " with message [" + record.value() + "]");
                    }
                } else {
                    LOG.error("There was no record found for the deviceSerialNumber provided from telemetry: "
                              + firmwareNotification.getDeviceSerialNumber() + " with message [" + record.value() + "]");
                }
                
            } catch (JsonException e) {
                LOG.error("Invalid JSON recieved from Kafka by TelemetryFirmwareConsumerTask: " + record.value(), e);
            }
        }
        try {
            this.consumer.commitSync();
        } catch (KafkaException kex) {
            LOG.error(kex);
        }
    }
    
}
