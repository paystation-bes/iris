package com.digitalpaytech.scheduling.task.kafka.consumer;

import java.time.Duration;
import java.util.Date;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.KafkaException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.LinuxConfigurationFile;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.scheduling.task.support.ConfigurationFileInfo;
import com.digitalpaytech.scheduling.task.support.ConfigurationNotification;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.paystation.LinuxConfigurationFileService;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.kafka.MessageConsumerFactory;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;

@Component(value = "paystationConfigurationNotificationConsumerTask")
public class PaystationConfigurationNotificationConsumerTask extends ConsumerTask {
    
    private static final Logger LOG = Logger.getLogger(PaystationConfigurationNotificationConsumerTask.class);
    
    @Resource(name = KafkaKeyConstants.LINK_PROPERTIES)
    private Properties linkKafka;
    
    @Resource(name = KafkaKeyConstants.TOPIC_NAMES_PROPERTIES)
    private Properties topicNames;
    
    @Autowired
    private PosServiceStateService posServiceStateService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private MessageConsumerFactory messageConsumerFactory;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private LinuxConfigurationFileService linuxConfigurationFileService;
    
    private String topic;
    
    private KafkaConsumer<String, String> consumer;
    
    @PostConstruct
    public final void init() {
        this.consumer = super.buildConsumer(this.topicNames.getProperty(KafkaKeyConstants.LCDMS_KAFKA_DEVICE_NOTIFY_TOPIC_NAME),
                                            this.topicNames.getProperty(KafkaKeyConstants.LCDMS_KAFKA_DEVICE_NOTIFY_CONSUMER_GROUP), this.linkKafka,
                                            this.topicNames.getProperty(KafkaKeyConstants.DELIMETER));
    }
    
    @PreDestroy
    public void close() {
        super.closeConsumer(this.consumer);
    }
    
    @Scheduled(fixedDelayString = "${consumer.millisecond.delay}")
    public void processOnSchedule() {
        process();
    }
    
    @Async("kafkaConsumerExecutor")
    public void process() {
        super.traceLog(LOG, this.topicNames, KafkaKeyConstants.LCDMS_KAFKA_DEVICE_NOTIFY_TOPIC_NAME,
                       KafkaKeyConstants.LCDMS_KAFKA_DEVICE_NOTIFY_CONSUMER_GROUP);        
        
        
        final ConsumerRecords<String, String> consumerRecords = this.consumer.poll(Duration.ofMillis(super.getDefaultConsumerPollTimeout()));

        if (LOG.isDebugEnabled()) {
            LOG.debug("Count of Consumer Records polled from  " + KafkaKeyConstants.LCDMS_KAFKA_DEVICE_NOTIFY_TOPIC_NAME + " is "
                      + (consumerRecords == null ? 0 : consumerRecords.count()));
        }
        
        for (ConsumerRecord<String, String> record : consumerRecords) {
            try {
                final ConfigurationNotification configurationNotification =
                        this.messageConsumerFactory.deserialize(record.value(), ConfigurationNotification.class);
                final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleBySerialNumber(configurationNotification.getDeviceSerialNumber());
                if (verifyPos(pointOfSale, configurationNotification)) {
                    // delete all existing data assigned to paystation
                	final int removed = this.linuxConfigurationFileService.deleteByPointOfSaleId(pointOfSale.getId());
                	LOG.info("Removed " + removed + " LinuxConfigurationFile entries for PointOfSale " + pointOfSale.getId()); 
                    saveLinuxConfigFiles(configurationNotification, pointOfSale.getId());
                    final PosServiceState posServiceState = this.posServiceStateService.findPosServiceStateById(pointOfSale.getId(), false);
                    if (posServiceState != null) {
                        final Date nowGmt = DateUtil.getCurrentGmtDate();
                        posServiceState.setLastModifiedGmt(nowGmt);
                        posServiceState.setNewConfigurationId(configurationNotification.getSnapShotId());
                        this.posServiceStateService.updatePosServiceState(posServiceState);
                    }
                } else {
                    LOG.error("Update PosServiceState.setNewConfiguration");
                }
            } catch (JsonException e) {
                LOG.error("Invalid JSON recieved from Kafka by paystationConfigurationNotificationConsumerTask: " + record.value(), e);
            }
        }
        try {
            /*
             * It is important to remember that commitSync() will commit the latest offset returned by poll(), so make sure
             * you call commitSync() after you are done processing all the records in the collection.
             */
            this.consumer.commitSync();
        } catch (KafkaException kex) {
            LOG.error(kex);
        }
    }
    
    private boolean verifyPos(final PointOfSale pointOfSale, final ConfigurationNotification configurationNotification) {
        boolean verified = true;
        if (pointOfSale != null) {
            final int customerId = pointOfSale.getCustomer().getId();
            final Customer customer = this.customerService.findCustomer(customerId);
            if (customer.getUnifiId() == null) {
                LOG.warn("UnifiId in Iris Customer ID: [" + customer.getId() + "] is empty");
                verified = false;
            }
            
        } else {
            LOG.error("Update PosServiceState.IsNewPaystationSetting but PointOfSale not found for deviceId: "
                      + configurationNotification.getDeviceSerialNumber() + " with message [" + configurationNotification + "]");
            verified = false;
        }
        return verified;
    }
    
    private void saveLinuxConfigFiles(final ConfigurationNotification notification, final int pointOfSaleId) {
        for (ConfigurationFileInfo info : notification.getFileInfos()) {
            final LinuxConfigurationFile newFile = new LinuxConfigurationFile(pointOfSaleId, info.getFileId(), notification.getSnapShotId(),
                    info.getFileName(), info.getHash(), info.getSize());
            this.linuxConfigurationFileService.save(newFile);
        }
    }
}
