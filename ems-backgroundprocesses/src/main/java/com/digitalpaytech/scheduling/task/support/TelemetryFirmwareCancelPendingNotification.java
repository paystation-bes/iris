package com.digitalpaytech.scheduling.task.support;

import java.io.Serializable;

public class TelemetryFirmwareCancelPendingNotification implements Serializable {
    
    private static final long serialVersionUID = 4928136747235148712L;

    private String deviceSerialNumber;
    private String creationDate;
    private boolean upgradeCancelPending;
    
    public String getDeviceSerialNumber() {
        return this.deviceSerialNumber;
    }
    
    public void setDeviceSerialNumber(final String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }
    
    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public boolean getUpgradeCancelPending() {
        return upgradeCancelPending;
    }

    public void setUpgradeCancelPending(boolean upgradeCancelPending) {
        this.upgradeCancelPending = upgradeCancelPending;
    }

    @Override
    public String toString() {
        return creationDate + "_" + deviceSerialNumber + "_" + upgradeCancelPending;
    }    
}
