package com.digitalpaytech.scheduling.task;

import java.net.InetAddress;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.SingleTransactionCardDataService;

/**
 * This task clean old data in SingleTransactionCardData table.
 * 
 * @author Brian Kim
 * 
 */
@Component("oldDataCleanUpTask")
public class OldDataCleanUpTask {
    
    private static final Logger LOGGER = Logger.getLogger(OldDataCleanUpTask.class);
    
    @Autowired
    private ClusterMemberService clusterMemberService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private SingleTransactionCardDataService singleTransactionCardDataService;
    
    public final void cleanupSingleTransactionCardData() {
        if (!isArchiveServer()) {
            // not the configured server
            return;
        }
        
        int maxDays;
        try {
            maxDays = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.SINGLE_TRANSACTION_CARD_DATA_STORAGE_DURATION,
                EmsPropertiesService.DEFAULT_SINGLE_TRANSACTION_CARD_DATA_STORAGE_DURATION);
        } catch (Exception e) {
            LOGGER.error("+++ Failed to query max single transacton card data storaga days from emsproperties table. +++ ");
            maxDays = EmsPropertiesService.DEFAULT_SINGLE_TRANSACTION_CARD_DATA_STORAGE_DURATION;
        }
        
        LOGGER.info("+++ Start remove " + maxDays + " days old SingleTransactionCardData from database. +++ ");
        this.singleTransactionCardDataService.cleanupOldData(maxDays);
        LOGGER.info("+++ End remove " + maxDays + " days old SingleTransactionCardData from database. +++ ");
    }
    
    private boolean isArchiveServer() {
        try {
            // get the cluster member name
            final String clusterName = this.clusterMemberService.getClusterName();
            if (clusterName == null || clusterName.isEmpty()) {
                throw new ApplicationException("Cluster member name is empty");
            }
            
            final String archiveServer = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.ARCHIVE_SERVER_NAME,
                EmsPropertiesService.DEFAULT_ARCHIVE_SERVER_NAME);
            
            if (clusterName.equals(archiveServer)) {
                LOGGER.info("Server " + clusterName + " configured for archiving data tasks.");
                return true;
            } else {
                LOGGER.info("Server " + clusterName + " not configured for archiving data tasks.");
                return false;
            }
        } catch (Exception e) {
            String addr = null;
            try {
                addr = InetAddress.getLocalHost().getHostAddress();
            } catch (Exception e1) {
                // shouldn't happen
            }
            final String err = "Unable to read memeber name from cluster.properties for cluster member " + addr;
            LOGGER.error(err);
            this.mailerService.sendAdminErrorAlert("Archive data", err, e);
            
            return false;
        }
    }
}
