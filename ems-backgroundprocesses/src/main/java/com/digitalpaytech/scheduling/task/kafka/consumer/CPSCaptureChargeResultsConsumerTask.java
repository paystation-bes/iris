package com.digitalpaytech.scheduling.task.kafka.consumer;

import java.io.UnsupportedEncodingException;
import java.time.Duration;
import java.util.Date;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.KafkaException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.digitalpaytech.client.dto.corecps.Authorization;
import com.digitalpaytech.domain.CPSProcessData;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.service.cps.CPSProcessDataService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;

@Component(value = "cpsCaptureChargeResultsConsumerTask")
public class CPSCaptureChargeResultsConsumerTask extends ConsumerTask {
    private static final Logger LOG = Logger.getLogger(CPSCaptureChargeResultsConsumerTask.class);
    
    @Resource(name = KafkaKeyConstants.PCI_LINK_PROPERTIES)
    private Properties pciLinkKafka;
    
    @Resource(name = KafkaKeyConstants.PCI_LINK_TOPIC_NAMES_PROPERTIES)
    private Properties pciLinkTopicNames;
    
    @Autowired
    private CPSProcessDataService cpsProcessDataService;
    
    private KafkaConsumer<String, String> consumer;
    
    @PostConstruct
    public void init() {
        this.consumer = super.buildConsumer(this.pciLinkTopicNames.getProperty(KafkaKeyConstants.CORE_CPS_CAPTURE_CHARGE_RESULTS_TOPIC_NAME),
                                            this.pciLinkTopicNames.getProperty(KafkaKeyConstants.CORE_CPS_CAPTURE_CHARGE_RESULTS_CONSUMER_GROUP),
                                            this.pciLinkKafka, this.pciLinkTopicNames.getProperty(KafkaKeyConstants.DELIMETER));
        
    }
    
    @PreDestroy
    public void close() {
        super.closeConsumer(this.consumer);
    }
    
    @Scheduled(fixedDelayString = "${consumer.millisecond.delay}")
    public void processOnSchedule() {
        process();
    }
    
    @Async("kafkaConsumerExecutor")
    public void process() {
        
        super.traceLog(LOG, this.pciLinkTopicNames, KafkaKeyConstants.CORE_CPS_CAPTURE_CHARGE_RESULTS_TOPIC_NAME,
                       KafkaKeyConstants.CORE_CPS_CAPTURE_CHARGE_RESULTS_CONSUMER_GROUP);
        final ConsumerRecords<String, String> consumerRecords = this.consumer.poll(Duration.ofMillis(super.getDefaultConsumerPollTimeout()));
        
        for (ConsumerRecord<String, String> rec : consumerRecords) {
            try {
                final Authorization capChargeResultMessage =
                        super.getMessageConsumerFactory().deserializeMessageWithEmbeddedHeaders(rec.value(), Authorization.class);
                
                if (LOG.isDebugEnabled()) {
                    LOG.debug("ChargeResultMessage object in Kafka consumer poll: " + capChargeResultMessage.toString());
                }
                
                // In response message, deviceSerialNumber and amount will be null.
                final CPSProcessData cpsProcessData = new CPSProcessData(null, capChargeResultMessage.getTerminalToken().toString(),
                        capChargeResultMessage.getChargeToken().toString(), 0, capChargeResultMessage.getAuthorizationNumber(),
                        capChargeResultMessage.getCardType(), Short.parseShort(capChargeResultMessage.getLast4Digits()),
                        capChargeResultMessage.getReferenceNumber(), capChargeResultMessage.getProcessorTransactionId(),
                        capChargeResultMessage.getProcessedDate() == null ? null : Date.from(capChargeResultMessage.getProcessedDate()),
                        DateUtil.getCurrentGmtDate());
                
                this.cpsProcessDataService.captureChargeResults(capChargeResultMessage.isApproved(), cpsProcessData);
                
            } catch (UnsupportedEncodingException | JsonException e) {
                LOG.error(ConsumerTask.UNSUPPORTED_EXCEPTION_MSG + rec.value(), e);
            }
        }
        try {
            /*
             * It is important to remember that commitSync() will commit the latest offset returned by poll(), so make sure
             * you call commitSync() after you are done processing all the records in the collection.
             */
            this.consumer.commitSync();
        } catch (KafkaException kex) {
            LOG.error(kex);
        }
    }

    public final void setCpsProcessDataService(final CPSProcessDataService cpsProcessDataService) {
        this.cpsProcessDataService = cpsProcessDataService;
    }
}
