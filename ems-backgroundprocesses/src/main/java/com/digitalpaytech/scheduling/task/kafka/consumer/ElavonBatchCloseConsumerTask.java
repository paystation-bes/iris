package com.digitalpaytech.scheduling.task.kafka.consumer;

import java.io.UnsupportedEncodingException;
import java.time.Duration;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.KafkaException;
import org.apache.log4j.Logger;
import org.codehaus.plexus.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.digitalpaytech.dto.cps.TransactionClosedMessage;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.service.cps.CPSTransactionClosedService;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;
import com.digitalpaytech.util.kafka.MessageConsumerFactory;

@Component(value = "elavonBatchCloseConsumer")
public class ElavonBatchCloseConsumerTask extends ConsumerTask {
    private static final Logger LOG = Logger.getLogger(ElavonBatchCloseConsumerTask.class);
    
    @Resource(name = KafkaKeyConstants.PCI_LINK_PROPERTIES)
    private Properties pciLinkKafka;
    
    @Resource(name = KafkaKeyConstants.PCI_LINK_TOPIC_NAMES_PROPERTIES)
    private Properties pciLinkTopicNames;
    
    @Autowired
    private MessageConsumerFactory messageConsumerFactory;
    
    @Autowired
    private CPSTransactionClosedService cpsTransactionClosedService;
    
    private KafkaConsumer<String, String> consumer;
    
    @PostConstruct
    public void init() {
        this.consumer = super.buildConsumer(this.pciLinkTopicNames.getProperty(KafkaKeyConstants.ELAVON_BATCH_CLOSE_TOPIC_NAME),
                                            this.pciLinkTopicNames.getProperty(KafkaKeyConstants.ELAVON_BATCH_CLOSE_CONSUMER_GROUP),
                                            this.pciLinkKafka, this.pciLinkTopicNames.getProperty(KafkaKeyConstants.DELIMETER));
    }
    
    @PreDestroy
    protected void close() {
        super.closeConsumer(this.consumer);
    }
    
    @Scheduled(fixedDelayString = "${consumer.millisecond.delay}")
    public void processOnSchedule() {
        process();
    }
    
    /**
     * CPS PublishBatchesForClose.java, publishClosedBatchesForIris method sends individual TransactionClosedMessage from a List
     * of TransactionClosedMessages to Kafka.
     */
    @Async("kafkaConsumerExecutor")
    public void process() {
        super.traceLog(LOG, this.pciLinkTopicNames, KafkaKeyConstants.ELAVON_BATCH_CLOSE_TOPIC_NAME,
                       KafkaKeyConstants.ELAVON_BATCH_CLOSE_CONSUMER_GROUP);
        final ConsumerRecords<String, String> consumerRecords = this.consumer.poll(Duration.ofMillis(super.getDefaultConsumerPollTimeout()));
        try {
            for (ConsumerRecord<String, String> record : consumerRecords) {
                try {
                    final TransactionClosedMessage tcMsg =
                            this.messageConsumerFactory.deserializeMessageWithEmbeddedHeaders(record.value(), TransactionClosedMessage.class);
                    this.cpsTransactionClosedService.saveElavonTransactionDetail(tcMsg);
                    
                } catch (JsonException je) {
                    LOG.error("Invalid JSON received from Link Kafka - Elavon Batch Close producer: " + removeHeaders(record.value()), je);
                }
            }
        } catch (UnsupportedEncodingException ue) {
            LOG.error(ue);
        }
        
        try {
            this.consumer.commitSync();
        } catch (KafkaException kex) {
            LOG.error(kex);
        }
    }
    
    private String removeHeaders(final String value) {
        if (StringUtils.isNotBlank(value) && value.contains(StandardConstants.STRING_CURLY_BRACE_OPEN)
            && value.contains(StandardConstants.STRING_CURLY_BRACE_CLOSE)) {
            return value.substring(value.lastIndexOf(StandardConstants.STRING_CURLY_BRACE_OPEN),
                                   value.lastIndexOf(StandardConstants.STRING_CURLY_BRACE_CLOSE));
        }
        return value;
    }
    
    public void setCpsTransactionClosedService(final CPSTransactionClosedService cpsTransactionClosedService) {
        this.cpsTransactionClosedService = cpsTransactionClosedService;
    }
}
