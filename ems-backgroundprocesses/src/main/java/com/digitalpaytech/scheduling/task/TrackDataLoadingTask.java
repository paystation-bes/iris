package com.digitalpaytech.scheduling.task;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.cardprocessing.CardProcessingManager;
import com.digitalpaytech.cardprocessing.CardProcessingMaster;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;

/**
 * This class represents the functionality to load the credit card track data
 * from files and put the data in the memory transaction queue.<br/>
 * The encrypted track data is decrypted and processed in this class.
 * 
 * @author Brian Kim
 * 
 */
@Component("trackDataLoadingTask")
public class TrackDataLoadingTask {
    
    private static final Logger LOGGER = Logger.getLogger(TrackDataLoadingTask.class);
    
    @Autowired
    private CardProcessingMaster cardProcessingMaster;
    
    @Autowired
    private CardProcessingManager cardProcessingManager;
    
    @Autowired
    private CryptoService cryptoService;
    
    public final CardProcessingMaster getCardProcessingMaster() {
        return this.cardProcessingMaster;
    }
    
    public final void setCardProcessingMaster(final CardProcessingMaster cardProcessingMaster) {
        this.cardProcessingMaster = cardProcessingMaster;
    }
    
    public final void setCardProcessingManager(final CardProcessingManager cardProcessingManager) {
        this.cardProcessingManager = cardProcessingManager;
    }
    
    public final CryptoService getCryptoService() {
        return this.cryptoService;
    }
    
    public final void setCryptoService(final CryptoService cryptoService) {
        this.cryptoService = cryptoService;
    }
    
    /**
     * Loads the credit card track data from file and put them in the store forward queue.
     */
    @SuppressWarnings("unchecked")
    public final void loadTrackDataAndAddToQueue() {
        LOGGER.info("STARTING loadTrackDataAndAddToQueue() to load track data and add them to queue.");
        
        /* File directory name. */
        final String dirPath = WebCoreConstants.DEFAULT_TRACKDATAFILE_LOCATION;
        ByteArrayInputStream bais = null;
        ObjectInputStream ois = null;
        try {
            final File[] trackDataFiles = WebCoreUtil.getAllFiles(dirPath);
            
            if (trackDataFiles != null && trackDataFiles.length > 0) {
                for (File trackDataFile : trackDataFiles) {
                    /* convert file to byte array. */
                    final byte[] bytes = WebCoreUtil.convertFileToBinary(trackDataFile);
                    
                    bais = new ByteArrayInputStream(bytes);
                    ois = new ObjectInputStream(bais);
                    
                    List<CardRetryTransaction> trackDataList = (CopyOnWriteArrayList<CardRetryTransaction>) ois.readObject();
                    
                    /** TODO: Replace with original logic AFTER migration is completed . */
                    trackDataList = this.cardProcessingManager.findCardRetryTransactionsForMigratedCustomers(trackDataList);
                    
                    for (CardRetryTransaction crt : trackDataList) {
                        //                        LOGGER.debug("load cc: " + crt);
                        /* Decrypt the track data and credit card data. */
                        final String decryptedCarddata = this.cryptoService.decryptData(crt.getCardData());
                        crt.setCardData(decryptedCarddata);
                        
                        /* Put data in the store forward queue. */
                        this.cardProcessingMaster.addTransactionToStoreForwardQueue(crt);
                    }
                    
                    /* Delete file from directory. */
                    WebCoreUtil.secureDelete(trackDataFile);
                }
            }
        } catch (IOException e) {
            LOGGER.error("Failed to load track data file or covert to CardRetryTransaction object.", e);
            return;
        } catch (ClassNotFoundException e) {
            LOGGER.error("Failed to find the CardRetryTransaction class.", e);
            return;
        } catch (CryptoException e) {
            LOGGER.error("Failed to decrypt the card data.", e);
            return;
        } finally {
            if (bais != null) {
                try {
                    bais.close();
                } catch (IOException ioe) {
                    LOGGER.error("Failed to close ByteArrayInputStream, ", ioe);
                }
            }
            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException ioe) {
                    LOGGER.error("Failed to close ObjectInputStream, ", ioe);
                }
            }
        }
        LOGGER.info("FINISHING loadTrackDataAndAddToQueue() to load track data and add them to queue.");
    }
}
