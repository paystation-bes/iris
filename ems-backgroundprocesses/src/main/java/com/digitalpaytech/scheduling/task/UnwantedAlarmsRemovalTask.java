package com.digitalpaytech.scheduling.task;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.service.PosEventCurrentService;
import com.digitalpaytech.service.queue.QueueEventService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

/**
 * This task removes low power shout down and shock alarms older than 24 hrs.
 * It will be triggered from background scheduler.
 * 
 * @author Brian Kim
 * 
 */
@Component("unwantedAlarmsRemovalTask")
public class UnwantedAlarmsRemovalTask {
    
    @Autowired
    private PosEventCurrentService posEventCurrentService;
    
    @Autowired
    private QueueEventService queueEventService;
    
    public final PosEventCurrentService getPosEventCurrentService() {
        return this.posEventCurrentService;
    }
    
    public final void setPosEventCurrentService(final PosEventCurrentService posEventCurrentService) {
        this.posEventCurrentService = posEventCurrentService;
    }
    
    public final QueueEventService getQueueEventService() {
        return this.queueEventService;
    }
    
    public final void setQueueEventService(final QueueEventService queueEventService) {
        this.queueEventService = queueEventService;
    }
    
    /**
     * This method removes low power shout down and shock alarms older than 24 hrs.
     * It is triggered from a background scheduler. (removeUnwantedAlarmsJobTrigger)
     */
    public final void removeUnwantedAlarms() {
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -1);
        final Date yesterday = calendar.getTime();
        
        removeUnwantedEvents(yesterday);
    }
    
    private void removeUnwantedEvents(final Date yesterday) {
        
        final List<PosEventCurrent> activePosEventCurrentList = this.posEventCurrentService.findActivePosEventCurrentByTimestamp(yesterday);
        for (PosEventCurrent posEventCurrent : activePosEventCurrentList) {
            
            this.queueEventService.createEventQueue(posEventCurrent.getPointOfSale().getId(), null, null, posEventCurrent.getEventType(),
                                                    WebCoreConstants.SEVERITY_CLEAR, WebCoreConstants.EVENT_ACTION_TYPE_CLEAR,
                                                    DateUtil.getCurrentGmtDate(), null, false, posEventCurrent.getEventType().isIsNotification(),
                                                    WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID,
                                                    (byte) (WebCoreConstants.QUEUE_TYPE_RECENT_EVENT | WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT));
        }
        
    }
}
