package com.digitalpaytech.scheduling.task.t2flex;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.data.CitationWidgetBase;
import com.digitalpaytech.domain.CitationMap;
import com.digitalpaytech.domain.CitationTotalDay;
import com.digitalpaytech.domain.CitationTotalHour;
import com.digitalpaytech.domain.CitationTotalMonth;
import com.digitalpaytech.domain.CitationType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.FlexDataWSCallStatus;
import com.digitalpaytech.domain.FlexWSUserAccount;
import com.digitalpaytech.domain.Time;
import com.digitalpaytech.dto.dataset.CitationRawQueryDataSet;
import com.digitalpaytech.dto.dataset.CitationRawQueryDataSet.CitationRawData;
import com.digitalpaytech.exception.IncompleteDataException;
import com.digitalpaytech.scheduling.task.support.FlexTimeInfo;
import com.digitalpaytech.service.CitationTypeService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.FlexWSService;
import com.digitalpaytech.service.T2FlexWsService;
import com.digitalpaytech.service.TimeService;
import com.digitalpaytech.util.FlexConstants;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Component(SynchronizeT2FlexWidgetDataTask.INSTANCE_NAME)
//Class to complex should be refactured later
@SuppressWarnings({ "PMD.ExcessiveImports", "PMD.GodClass" })
public class SynchronizeT2FlexWidgetDataTask {
    public static final String INSTANCE_NAME = "syncFlexWidgetDataTask";
    
    private static final Logger LOGGER = Logger.getLogger(SynchronizeT2FlexWidgetDataTask.class);
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private FlexWSService flexWSService;
    
    @Autowired
    private CitationTypeService citationTypeService;
    
    @Autowired
    private T2FlexWsService t2FlexWsService;
    
    @Autowired
    private TimeService timeService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private BeanFactory beanFactory;
    
    public final CustomerAdminService getCustomerAdminService() {
        return this.customerAdminService;
    }
    
    public final void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    public final CustomerService getCustomerService() {
        return this.customerService;
    }
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final FlexWSService getFlexWSService() {
        return this.flexWSService;
    }
    
    public final void setFlexWSService(final FlexWSService flexWSService) {
        this.flexWSService = flexWSService;
    }
    
    public final CitationTypeService getCitationTypeService() {
        return this.citationTypeService;
    }
    
    public final void setCitationTypeService(final CitationTypeService citationTypeService) {
        this.citationTypeService = citationTypeService;
    }
    
    public final T2FlexWsService getT2FlexWsService() {
        return this.t2FlexWsService;
    }
    
    public final void setT2FlexWsService(final T2FlexWsService t2FlexWsService) {
        this.t2FlexWsService = t2FlexWsService;
    }
    
    public final TimeService getTimeService() {
        return this.timeService;
    }
    
    public final void setTimeService(final TimeService timeService) {
        this.timeService = timeService;
    }
    
    public final EmsPropertiesService getEmsPropertiesService() {
        return this.emsPropertiesService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final BeanFactory getBeanFactory() {
        return this.beanFactory;
    }
    
    public final void setBeanFactory(final BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }
    
    private SynchronizeT2FlexWidgetDataTask self() {
        return this.beanFactory.getBean(INSTANCE_NAME, SynchronizeT2FlexWidgetDataTask.class);
    }
    
    private CitationMap convertToCitationMapData(final CitationRawData citationRawData, final Customer customer, final FlexTimeInfo timeInfo)
        throws IncompleteDataException {
        
        final int typeId = Integer.parseInt(citationRawData.getCitationType());
        final CitationType type = this.flexWSService.findCitationTypeByCustomerIdAndFlexCitationTypeId(customer.getId(), typeId);
        if (type == null) {
            throw new IncompleteDataException(CitationType.class, typeId);
        }
        
        final CitationMap citationMap = new CitationMap();
        citationMap.setCitationType(type);
        citationMap.setTicketId(citationRawData.getTicketId());
        citationMap.setCitationId(Integer.parseInt(citationRawData.getCitationId()));
        citationMap.setCustomer(customer);
        citationMap.setLatitude(new BigDecimal(citationRawData.getLatitude()));
        citationMap.setLongitude(new BigDecimal(citationRawData.getLongitude()));
        citationMap.setIssueDateGmt(new Date(timeInfo.deInterpolate(citationRawData.getIssueDate().getTime())));
        
        return citationMap;
    }
    
    // This method cannot be final, otherwise spring will have trouble locate all the auto-wired objects.
    @SuppressWarnings({ "checkstyle:designforextension" })
    public void getCitationRawData() {
        if (!this.flexWSService.isFlexDataExtractionServer()) {
            return;
        }
        
        final Date now = new Date();
        final FlexTimeInfo timeInfo = new FlexTimeInfo(
                this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.FLEX_CITATION_WEEK_INTERPOLATION,
                                                                EmsPropertiesService.DEFAULT_FLEX_CITATION_WEEK_INTERPOLATION));
        
        final List<Future<Boolean>> results = new ArrayList<Future<Boolean>>();
        final SynchronizeT2FlexWidgetDataTask self = self();
        
        final List<Customer> customerList = this.customerService
                .findCustomerBySubscriptionTypeId(WebCoreConstants.SUBSCRIPTION_TYPE_FLEX_INTEGRATION);
        
        for (Customer customer : customerList) {
            final FlexWSUserAccount flexAccount = this.flexWSService.findFlexWSUserAccountByCustomerId(customer.getId());
            if (flexAccount != null && !flexAccount.isBlocked()) {
                results.add(self.getCitationRawData(flexAccount, timeInfo, now));
            }
        }
        
        // Ensure that there are no overlap schedules.
        waitForExecutions(results);
    }
    
    // Returning Future to allow the main thread to wait for the end of executions.
    // Illegal catch is suppressed because for async task it is more useful to log every exceptions manually.
    // 
    @SuppressWarnings({ "checkstyle:illegalcatch", "checkstyle:designforextension" })
    @Async
    @Transactional(propagation = Propagation.SUPPORTS)
    public Future<Boolean> getCitationRawData(final FlexWSUserAccount flexAccount, final FlexTimeInfo timeInfo, final Date fetchTime) {
        boolean success = false;
        try {
            final CustomerProperty customerProperty = this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(flexAccount
                    .getCustomer().getId(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
            
            timeInfo.setCustomerTimeZone(TimeZone.getTimeZone(customerProperty.getPropertyValue()));
            timeInfo.setFlexTimeZone(TimeZone.getTimeZone(flexAccount.getTimeZone()));
            
            int returnedListSize;
            int numberOfCalls = 0;
            final Calendar startTime = Calendar.getInstance();
            startTime.add(Calendar.YEAR, -1);
            startTime.set(Calendar.MONTH, 0);
            startTime.set(Calendar.DAY_OF_MONTH, 1);
            startTime.set(Calendar.HOUR_OF_DAY, 0);
            startTime.set(Calendar.MINUTE, 0);
            startTime.set(Calendar.SECOND, 0);
            startTime.set(Calendar.MILLISECOND, 0);
            final int citationOffsetMinutes = this.emsPropertiesService
                    .getPropertyValueAsInt(EmsPropertiesService.FLEX_CITATION_OFFSET_MINUTE, EmsPropertiesService.DEFAULT_FLEX_CITATION_OFFSET_MINUTE);
            
            final int citationMaxRowCount = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.FLEX_CITATION_MAX_ROW_COUNT,
                                                                                            EmsPropertiesService.DEFAULT_FLEX_CITATION_MAX_ROW_COUNT);
            
            final int citationMaxTryLimit = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.FLEX_CITATION_MAX_TRY_LIMIT,
                                                                                            EmsPropertiesService.DEFAULT_FLEX_CITATION_MAX_TRY_LIMIT);
            
            do {
                
                final FlexDataWSCallStatus callStatus = this.flexWSService.findLastCallByCustomerIdAndTypeId(flexAccount.getCustomer().getId(),
                                                                                                             FlexConstants.WS_TYPE_CITATION);
                final Calendar endTime = Calendar.getInstance();
                
                // offset added to currentTime to include slightly out of time handheld devices. 
                // If the issue date is even more off the citation will be ignored.
                endTime.add(Calendar.MINUTE, citationOffsetMinutes);
                
                int conUid = 0;
                if (callStatus != null) {
                    conUid = callStatus.getLastUniqueIdentifier();
                }
                
                returnedListSize = 0;
                final CitationRawQueryDataSet convertedData = this.t2FlexWsService.getCitationRawData(timeInfo.interpolate(startTime.getTime()),
                                                                                                      timeInfo.interpolate(endTime.getTime()),
                                                                                                      conUid, citationMaxRowCount, flexAccount);
                numberOfCalls++;
                returnedListSize = convertedData.getCitationRawDataList().size();
                if (convertedData.getCitationRawDataList() != null && !convertedData.getCitationRawDataList().isEmpty()) {
                    self().createWidgetData(flexAccount, convertedData, timeInfo, conUid);
                }
            } while (returnedListSize == citationMaxRowCount && numberOfCalls < citationMaxTryLimit);
            success = true;
        } catch (IncompleteDataException ide) {
            blockFlexAccount(flexAccount.getId(), ide);
            LOGGER.error("Failed to retrive Citations from Flex for Customer: " + flexAccount.getCustomer().getId()
                         + StandardConstants.STRING_AT_SIGN + fetchTime + " CitationType doesn't exist. ", ide);
        } catch (Exception e) {
            
            LOGGER.error("Failed to retrive Citations from Flex for Customer: " + flexAccount.getCustomer().getId()
                         + StandardConstants.STRING_AT_SIGN + fetchTime, e);
        }
        
        return new AsyncResult<Boolean>(success);
    }
    
    // ConcurrentHashMap not necessary
    @SuppressWarnings({ "PMD.UseConcurrentHashMap" })
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void createWidgetData(final FlexWSUserAccount flexAccount, final CitationRawQueryDataSet convertedData, final FlexTimeInfo timeInfo,
        final Integer lastUniqueId) throws IncompleteDataException {
        final List<Serializable> widgetDataList = new ArrayList<Serializable>();
        final Map<String, CitationWidgetBase> widgetDataMap = new HashMap<String, CitationWidgetBase>();
        
        final Calendar now = Calendar.getInstance();
        now.setTimeZone(WebCoreConstants.SYSTEM_ADMIN_TIMEZONE);
        int lastId = lastUniqueId;
        for (CitationRawData citationRawData : convertedData.getCitationRawDataList()) {
            // Some citations have multiple locations it is very expensive to eliminate them using flex query.
            if (Integer.parseInt(citationRawData.getCitationId()) == lastId) {
                continue;
            } else {
                lastId = Integer.valueOf(citationRawData.getCitationId());
            }
            final Calendar issueDate = Calendar.getInstance();
            issueDate.setTime(timeInfo.deInterpolate(citationRawData.getIssueDate()));
            issueDate.setTimeZone(timeInfo.getCustomerTimeZone());
            
            final int flexCitationType = Integer.valueOf(citationRawData.getCitationType());
            final CitationType citationType = this.citationTypeService.findCitationTypeByCustomerIdAndFlexCitationTypeId(flexAccount.getCustomer()
                    .getId(), flexCitationType);
            if (citationType == null) {
                throw new IncompleteDataException(CitationType.class, flexCitationType);
            }
            
            // Add Citation Map data
            if (issueDate.getTimeInMillis() + StandardConstants.DAYS_IN_MILLIS_1 * FlexConstants.NUMBER_OF_DAYS_MAP > now.getTimeInMillis()
                && citationRawData.getLatitude() != null && citationRawData.getLongitude() != null) {
                widgetDataList.add(convertToCitationMapData(citationRawData, flexAccount.getCustomer(), timeInfo));
            }
            
            // Add hourly data
            if (issueDate.getTimeInMillis() + StandardConstants.DAYS_IN_MILLIS_1 * FlexConstants.NUMBER_OF_DAYS_HOUR > now.getTimeInMillis()) {
                processCitationHour(widgetDataMap, citationRawData, flexAccount.getCustomer(), citationType, issueDate);
            }
            
            // Add daily data
            if (issueDate.getTimeInMillis() + StandardConstants.DAYS_IN_MILLIS_1 * FlexConstants.NUMBER_OF_DAYS_DAY > now.getTimeInMillis()) {
                processCitationDay(widgetDataMap, citationRawData, flexAccount.getCustomer(), citationType, issueDate);
            }
            
            // Add monthly data
            processCitationMonth(widgetDataMap, citationRawData, flexAccount.getCustomer(), citationType, issueDate);
        }
        
        widgetDataList.addAll(widgetDataMap.values());
        this.flexWSService.saveWidgetData(widgetDataList, lastId, convertedData.getCallStatusId());
        
    }
    
    private void processCitationHour(final Map<String, CitationWidgetBase> widgetDataMap, final CitationRawData citationRawData,
        final Customer customer, final CitationType citationType, final Calendar issueDate) {
        final Time timeCustomer = this.timeService.findBucket(issueDate);
        final Calendar issueDateGmt = (Calendar) issueDate.clone();
        issueDateGmt.setTimeZone(TimeZone.getTimeZone(WebCoreConstants.GMT));
        final Time timeGmt = this.timeService.findBucket(issueDateGmt);
        
        final String key = getMapKey(customer.getId(), citationRawData.getCitationType(), FlexConstants.WIDGET_TABLE_TYPE_HOUR, timeCustomer.getId());
        CitationWidgetBase citation = widgetDataMap.get(key);
        if (citation == null) {
            citation = this.flexWSService.findCitationWidgetData(CitationTotalHour.class.getSimpleName(), customer.getId(),
                                                                 Integer.valueOf(citationRawData.getCitationType()), timeCustomer.getId());
            if (citation == null) {
                citation = new CitationTotalHour(citationType, timeCustomer, customer, timeGmt, 0);
            }
            widgetDataMap.put(key, citation);
        }
        widgetDataMap.get(key).setTotalCitation(widgetDataMap.get(key).getTotalCitation() + 1);
    }
    
    private void processCitationDay(final Map<String, CitationWidgetBase> widgetDataMap, final CitationRawData citationRawData,
        final Customer customer, final CitationType citationType, final Calendar issueDate) {
        issueDate.set(Calendar.HOUR_OF_DAY, 0);
        issueDate.set(Calendar.MINUTE, 0);
        issueDate.set(Calendar.SECOND, 0);
        final Time timeCustomer = this.timeService.findBucket(issueDate);
        final String key = getMapKey(customer.getId(), citationRawData.getCitationType(), FlexConstants.WIDGET_TABLE_TYPE_DAY, timeCustomer.getId());
        CitationWidgetBase citation = widgetDataMap.get(key);
        if (citation == null) {
            citation = this.flexWSService.findCitationWidgetData(CitationTotalDay.class.getSimpleName(), customer.getId(),
                                                                 Integer.valueOf(citationRawData.getCitationType()), timeCustomer.getId());
            if (citation == null) {
                citation = new CitationTotalDay(citationType, timeCustomer, customer, 0);
            }
            widgetDataMap.put(key, citation);
        }
        widgetDataMap.get(key).setTotalCitation(widgetDataMap.get(key).getTotalCitation() + 1);
    }
    
    private void processCitationMonth(final Map<String, CitationWidgetBase> widgetDataMap, final CitationRawData citationRawData,
        final Customer customer, final CitationType citationType, final Calendar issueDate) {
        issueDate.set(Calendar.HOUR_OF_DAY, 0);
        issueDate.set(Calendar.MINUTE, 0);
        issueDate.set(Calendar.SECOND, 0);
        issueDate.set(Calendar.DAY_OF_MONTH, 1);
        final Time timeCustomer = this.timeService.findBucket(issueDate);
        final String key = getMapKey(customer.getId(), citationRawData.getCitationType(), FlexConstants.WIDGET_TABLE_TYPE_MONTH, timeCustomer.getId());
        CitationWidgetBase citation = widgetDataMap.get(key);
        if (citation == null) {
            citation = this.flexWSService.findCitationWidgetData(CitationTotalMonth.class.getSimpleName(), customer.getId(),
                                                                 Integer.valueOf(citationRawData.getCitationType()), timeCustomer.getId());
            if (citation == null) {
                citation = new CitationTotalMonth(citationType, timeCustomer, customer, 0);
            }
            widgetDataMap.put(key, citation);
        }
        widgetDataMap.get(key).setTotalCitation(widgetDataMap.get(key).getTotalCitation() + 1);
    }
    
    private String getMapKey(final Integer customerId, final String flexCitationTypeId, final byte tableType, final Integer timeId) {
        return new StringBuffer().append(customerId).append(StandardConstants.STRING_UNDERSCORE).append(flexCitationTypeId)
                .append(StandardConstants.STRING_UNDERSCORE).append(tableType).append(StandardConstants.STRING_UNDERSCORE).append(timeId).toString();
    }
    
    // Call this to wait for all the "results" to complete before continue the main thread.
    private void waitForExecutions(final List<Future<Boolean>> results) {
        for (Future<?> r : results) {
            try {
                r.get();
            } catch (ExecutionException | InterruptedException e) {
                LOGGER.error(e);
            }
        }
    }
    
    private void blockFlexAccount(final Integer flexAccountId, final IncompleteDataException error) {
        FlexWSUserAccount.Blocker blocker = null;
        if (CitationType.class.isAssignableFrom(error.getType())) {
            blocker = FlexWSUserAccount.Blocker.CITATION_TYPE_MISSING;
        }
        
        if (blocker != null) {
            this.flexWSService.blockFlexAccount(flexAccountId, blocker);
        }
    }
    
}
