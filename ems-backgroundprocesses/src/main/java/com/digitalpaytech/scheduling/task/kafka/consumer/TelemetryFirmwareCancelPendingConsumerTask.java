package com.digitalpaytech.scheduling.task.kafka.consumer;

import java.time.Duration;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.KafkaException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.scheduling.task.support.TelemetryFirmwareCancelPendingNotification;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.ServiceHelper;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.util.kafka.AbstractMessageProducer;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;
import com.digitalpaytech.util.kafka.MessageConsumerFactory;

@Component(value = "telemetryFirmwareCancelPendingConsumerTask")
public class TelemetryFirmwareCancelPendingConsumerTask extends ConsumerTask {
    
    private static final Logger LOG = Logger.getLogger(TelemetryFirmwareCancelPendingConsumerTask.class);

    @Resource(name = KafkaKeyConstants.IRIS_PROPERTIES)
    private Properties irisKafka;
    
    @Resource(name = KafkaKeyConstants.TOPIC_NAMES_PROPERTIES)
    private Properties topicNames;
    
    @Autowired
    private PosServiceStateService posServiceStateService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    @Qualifier("irisMessageProducer")
    private AbstractMessageProducer otaMessageProducer;

    @Autowired
    private MessageConsumerFactory messageConsumerFactory;
    
    @Autowired
    private ServiceHelper serviceHelper;
    
    private KafkaConsumer<String, String> consumer;
    
    private TelemetryFirmwareCancelPendingConsumerTask self() {
        return this.serviceHelper.bean(TelemetryFirmwareCancelPendingConsumerTask.class);
    }
    
    @PostConstruct
    public final void init() {        
        this.consumer = super.buildConsumer(this.topicNames.getProperty(KafkaKeyConstants.OTA_DEVICE_CANCEL_PENDING_NOTIFICATION_TOPIC_NAME),
                                            this.topicNames.getProperty(KafkaKeyConstants.OTA_DEVICE_CANCEL_PENDING_NOTIFICATION_CONSUMER_GROUP), this.irisKafka,
                                            this.topicNames.getProperty(KafkaKeyConstants.DELIMETER));
    }
    
    @PreDestroy
    public void close() {
        super.closeConsumer(this.consumer);
    }
    
    @Scheduled(fixedDelayString = "${consumer.millisecond.delay}")
    public void processOnSchedule() {
        self().process();
    }
    
    public final void setMessageProducer(final AbstractMessageProducer producer) {
        this.otaMessageProducer = producer;
    }

    @Async("OTAkafkaExecutor")
    public void process() {
        super.traceLog(LOG, this.topicNames, KafkaKeyConstants.OTA_DEVICE_CANCEL_PENDING_NOTIFICATION_TOPIC_NAME,
                       KafkaKeyConstants.OTA_DEVICE_CANCEL_PENDING_NOTIFICATION_CONSUMER_GROUP);
        ConsumerRecords<String, String> consumerRecords = this.consumer.poll(Duration.ofMillis(super.getDefaultConsumerPollTimeout()));

        if (LOG.isDebugEnabled()) {
            LOG.debug("Count of Consumer Records polled from " + KafkaKeyConstants.OTA_DEVICE_CANCEL_PENDING_NOTIFICATION_TOPIC_NAME + " is "
                      + (consumerRecords == null ? 0 : consumerRecords.count()));
        }
        
        if (consumerRecords != null) {
            for (ConsumerRecord<String, String> record : consumerRecords) {
                try {
                    final TelemetryFirmwareCancelPendingNotification firmwareCancelPendingNotification =
                            this.messageConsumerFactory.deserialize(record.value(), TelemetryFirmwareCancelPendingNotification.class);
                    final PointOfSale pointOfSale =
                            this.pointOfSaleService.findPointOfSaleBySerialNumber(firmwareCancelPendingNotification.getDeviceSerialNumber());
                    if (pointOfSale != null) {
                        final PosServiceState posServiceState = this.posServiceStateService.findPosServiceStateById(pointOfSale.getId(), false);
                        if (posServiceState != null) {
                            posServiceState.setIsNewOTAFirmwareUpdateCancelPending(firmwareCancelPendingNotification.getUpgradeCancelPending());
                            this.posServiceStateService.updatePosServiceState(posServiceState);
                        } else {
                            LOG.error("Update PosServiceState.IsNewPaystationSetting but PosServiceState not found for PointOfSaleId: "
                                      + pointOfSale.getId() + " with message [" + record.value() + "]");
                        }
                    } else {
                        LOG.error("There was no record found for the deviceSerialNumber provided from telemetry: "
                                  + firmwareCancelPendingNotification.getDeviceSerialNumber() + " with message [" + record.value() + "]");
                    }
                } catch (JsonException e) {
                    LOG.error("Invalid JSON received from Kafka by TelemetryFirmwareCancelPendingConsumerTask: " + record.value(), e);
                }
            }
        }
        try {
            this.consumer.commitSync();
        } catch (KafkaException kex) {
            LOG.error(kex);
        }
    }    
}
