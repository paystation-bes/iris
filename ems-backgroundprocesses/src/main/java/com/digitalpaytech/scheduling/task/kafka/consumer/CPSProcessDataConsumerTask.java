package com.digitalpaytech.scheduling.task.kafka.consumer;

import java.io.UnsupportedEncodingException;
import java.time.Duration;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.KafkaException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.CPSProcessData;
import com.digitalpaytech.dto.cps.SnFChargeResultMessage;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.cps.CPSProcessDataService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;

@Component(value = "cpsProcessDataConsumerTask")
public class CPSProcessDataConsumerTask extends ConsumerTask {
    private static final Logger LOG = Logger.getLogger(CPSProcessDataConsumerTask.class);
    
    @Resource(name = KafkaKeyConstants.PCI_LINK_PROPERTIES)
    private Properties pciLinkKafka;
    
    @Resource(name = KafkaKeyConstants.PCI_LINK_TOPIC_NAMES_PROPERTIES)
    private Properties pciLinkTopicNames;
    
    @Autowired
    private CPSProcessDataService cpsProcessDataService;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    private KafkaConsumer<String, String> consumer;
    
    @PostConstruct
    public void init() {
        
        this.consumer = super.buildConsumer(this.pciLinkTopicNames.getProperty(KafkaKeyConstants.CORE_CPS_SNF_CHARGE_RESULTS_TOPIC_NAME),
                                            this.pciLinkTopicNames.getProperty(KafkaKeyConstants.CORE_CPS_SNF_CHARGE_RESULTS_CONSUMER_GROUP),
                                            this.pciLinkKafka, this.pciLinkTopicNames.getProperty(KafkaKeyConstants.DELIMETER));
    }
    
    @PreDestroy
    public void close() {
        super.closeConsumer(this.consumer);
    }
    
    @Scheduled(fixedDelayString = "${consumer.millisecond.delay}")
    public void processOnSchedule() {
        process();
    }
    
    @Async("kafkaConsumerExecutor")
    public void process() {
        
        super.traceLog(LOG, this.pciLinkTopicNames, KafkaKeyConstants.CORE_CPS_SNF_CHARGE_RESULTS_TOPIC_NAME,
                       KafkaKeyConstants.CORE_CPS_SNF_CHARGE_RESULTS_CONSUMER_GROUP);
        
        final ConsumerRecords<String, String> consumerRecords = this.consumer.poll(Duration.ofMillis(super.getDefaultConsumerPollTimeout()));
        
        for (ConsumerRecord<String, String> rec : consumerRecords) {
            try {
                final SnFChargeResultMessage snfChargeResultMessage =
                        super.getMessageConsumerFactory().deserializeMessageWithEmbeddedHeaders(rec.value(), SnFChargeResultMessage.class);
                
                if (LOG.isDebugEnabled()) {
                    LOG.debug("SnFChargeResultMessage object in Kafka consumer poll: " + snfChargeResultMessage.toString());
                }
                
                if (this.merchantAccountService.findByTerminalTokenAndIsLink(snfChargeResultMessage.getTerminalToken()) == null) {
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Ignoring unrecognized SnFChargeResultMessage.terminalToken" + rec.value());
                    }
                } else {
                    final CPSProcessData cpsProcessData = createCPSProcessData(snfChargeResultMessage);
                    this.cpsProcessDataService.manageCPSProcessData(cpsProcessData);
                }
            } catch (UnsupportedEncodingException | JsonException e) {
                LOG.error(ConsumerTask.UNSUPPORTED_EXCEPTION_MSG + rec.value(), e);
            }
        }
        try {
            this.consumer.commitSync();
        } catch (KafkaException kex) {
            LOG.error(kex);
        }
    }
    
    private CPSProcessData createCPSProcessData(final SnFChargeResultMessage snfChargeResultMessage) {
        return new CPSProcessData(snfChargeResultMessage.getDeviceSerialNumber(), snfChargeResultMessage.getTerminalToken(),
                snfChargeResultMessage.getChargeToken(), Integer.parseInt(snfChargeResultMessage.getAmount()),
                snfChargeResultMessage.getAuthorizationNumber(), snfChargeResultMessage.getCardType(),
                Short.parseShort(snfChargeResultMessage.getLast4Digits()), snfChargeResultMessage.getReferenceNumber(),
                snfChargeResultMessage.getProcessorTransactionId(), DateUtil.convertUTCToGMT(snfChargeResultMessage.getProcessedDate()),
                DateUtil.getCurrentGmtDate());
    }
    
    public final void setCpsProcessDataService(final CPSProcessDataService cpsProcessDataService) {
        this.cpsProcessDataService = cpsProcessDataService;
    }
}
