package com.digitalpaytech.scheduling.task.kafka.consumer;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import java.util.Collection;
import java.util.Properties;
import java.io.UnsupportedEncodingException;
import java.time.Duration;
import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.KafkaException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;
import com.digitalpaytech.scheduling.task.support.BINRangeNotification;
import com.digitalpaytech.exception.JsonException;

@Component(value = "binRangeNotificationConsumerTask")
public class BINRangeNotificationConsumerTask extends ConsumerTask {
    private static final Logger LOG = Logger.getLogger(BINRangeNotificationConsumerTask.class);
    
    @Resource(name = KafkaKeyConstants.PCI_LINK_PROPERTIES)
    private Properties pciLinkKafka;
    
    @Resource(name = KafkaKeyConstants.PCI_LINK_TOPIC_NAMES_PROPERTIES)
    private Properties pciLinkTopicNames;
    
    @Autowired
    private PosServiceStateService posServiceStateService;
    
    private KafkaConsumer<String, String> consumer;
    
    @PostConstruct
    public final void init() {
        this.consumer = super.buildConsumer(this.pciLinkTopicNames.getProperty(KafkaKeyConstants.BIN_RANGE_NOTIFICATION_TOPIC_NAME_KEY),
                                            this.pciLinkTopicNames.getProperty(KafkaKeyConstants.BIN_RANGE_NOTIFICATION_TOPIC_CONSUMER_GROUP),
                                            this.pciLinkKafka, this.pciLinkTopicNames.getProperty(KafkaKeyConstants.DELIMETER));
    }
    
    @PreDestroy
    public void close() {
        super.closeConsumer(this.consumer);
    }
    
    @Scheduled(fixedDelayString = "${consumer.millisecond.delay}")
    public void processOnSchedule() {
        process();
    }
    
    @Async("kafkaConsumerExecutor")
    public void process() {
        super.traceLog(LOG, this.pciLinkTopicNames, KafkaKeyConstants.BIN_RANGE_NOTIFICATION_TOPIC_NAME_KEY,
                       KafkaKeyConstants.BIN_RANGE_NOTIFICATION_TOPIC_CONSUMER_GROUP);
        
        final ConsumerRecords<String, String> consumerRecords = this.consumer.poll(Duration.ofMillis(super.getDefaultConsumerPollTimeout()));

        if (LOG.isDebugEnabled()) {
            LOG.debug("Count of Consumer Records polled from " + KafkaKeyConstants.BIN_RANGE_NOTIFICATION_TOPIC_NAME_KEY + " is "
                      + (consumerRecords == null ? 0 : consumerRecords.count()));
        }
        
        for (ConsumerRecord<String, String> rec : consumerRecords) {
            try {
                final BINRangeNotification binRangeNote 
                    = super.getMessageConsumerFactory().deserializeMessageWithEmbeddedHeaders(rec.value(), BINRangeNotification.class);
                setNotification(binRangeNote);
                
            } catch (UnsupportedEncodingException | JsonException e) {
                LOG.error(super.getConsumerRecordData(rec));
                LOG.error(ConsumerTask.UNSUPPORTED_EXCEPTION_MSG + rec.value(), e);
            }
        }
        try {
            this.consumer.commitSync();
        } catch (KafkaException kex) {
            LOG.error(kex);
        }
    }
    
    /**
     * 1st - if BINRangeNotification has isForAllDevices = true, look up all Linux pay station POSServiceStates
     * 2nd - if BINRangeNotification has customerId and isForAllDevices = false, look up POSServiceState by customerId and set isNewBINRange to true.
     * - If BINRangeNotification has 1 or more deviceIds, look up POSServiceState by deviceIds (serialNumbers) and set isNewBINRange to true.
     * 
     * 
     * and set isNewBINRange to true.
     * 
     * @param note
     *            BINRangeNotification object represents the JSON document.
     */
    /*
     * TODO: The logic in "findLinuxPosServiceStateBySerialNumber" method doesn't check if PointOfSale table IsLinux = 1. 
     * final PosServiceState pss = this.posServiceStateService.findLinuxPosServiceStateBySerialNumber(note.getDeviceSerialNumber());
     */
    private void setNotification(final BINRangeNotification note) {
        final Collection<PosServiceState> col;
        if (note.isForAllDevices()) {
            col = this.posServiceStateService.findAllLinuxPosServiceStates();
            
        } else if (note.getCustomerId() != null) {
            col = this.posServiceStateService.findLinuxPosServiceStatesByLinkCustomerId(note.getCustomerId());
            
        } else if (StringUtils.isNotBlank(note.getDeviceSerialNumber())) {
            final PosServiceState pss = this.posServiceStateService.findLinuxPosServiceStateBySerialNumber(note.getDeviceSerialNumber());
            if (pss == null) {
                col = null;
            } else {
                col = new ArrayList<PosServiceState>(1);
                col.add(pss);
            }
        } else {
            col = null;
        }
        updatePosServiceStates(col);
    }
    
    private void updatePosServiceStates(final Collection<PosServiceState> posServiceStates) {
        if (posServiceStates == null || posServiceStates.isEmpty()) {
            return;
        }
        posServiceStates.forEach(pss -> {
                pss.setIsNewBINRange(true);
                this.posServiceStateService.updatePosServiceState(pss);
            });
    }
    
}
