package com.digitalpaytech.scheduling.task.support;

import java.io.Serializable;

public class DeviceNotification implements Serializable {
    private static final long serialVersionUID = -4433942037931907038L;
    private String deviceId;
    private String fileId;
    private String groupId;
    private String customerId;
    private String groupName;
    
    public final String getDeviceId() {
        return this.deviceId;
    }
    
    public final void setDeviceId(final String deviceId) {
        this.deviceId = deviceId;
    }
    
    public final String getFileId() {
        return this.fileId;
    }
    
    public final void setFileId(final String fileId) {
        this.fileId = fileId;
    }
    
    public final String getGroupId() {
        return this.groupId;
    }
    
    public final void setGroupId(final String groupId) {
        this.groupId = groupId;
    }
    
    public final String getCustomerId() {
        return this.customerId;
    }
    
    public final void setCustomerId(final String customerId) {
        this.customerId = customerId;
    }
    
    public final String getGroupName() {
        return this.groupName;
    }
    
    public final void setGroupName(final String groupName) {
        this.groupName = groupName;
    }
}
