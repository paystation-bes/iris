package com.digitalpaytech.scheduling.task.support;

import java.io.Serializable;

public class BINRangeNotification implements Serializable {
    private static final long serialVersionUID = 786690857040232605L;
    private boolean forAllDevices;
    private Integer customerId;
    private String deviceSerialNumber;
    
    public BINRangeNotification() {
    }

    public BINRangeNotification(final boolean forAllDevices, final Integer customerId, final String deviceSerialNumber) {
        this.forAllDevices = forAllDevices;
        this.customerId = customerId;
        this.deviceSerialNumber = deviceSerialNumber;
    }
    
    public Integer getCustomerId() {
        return this.customerId;
    }
    public void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }
    public String getDeviceSerialNumber() {
        return this.deviceSerialNumber;
    }
    public void setDeviceSerialNumber(final String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }
    public boolean isForAllDevices() {
        return this.forAllDevices;
    }
    public void setForAllDevices(final boolean forAllDevices) {
        this.forAllDevices = forAllDevices;
    }
}
