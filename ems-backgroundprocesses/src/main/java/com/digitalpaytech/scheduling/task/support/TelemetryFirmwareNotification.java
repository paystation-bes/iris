package com.digitalpaytech.scheduling.task.support;

import java.io.Serializable;

public class TelemetryFirmwareNotification implements Serializable {
    
    private static final long serialVersionUID = -4433942037931907038L;
    
    private String deviceSerialNumber;
    private boolean upgradeAvailable;
    
    public String getDeviceSerialNumber() {
        return this.deviceSerialNumber;
    }
    
    public void setDeviceSerialNumber(final String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }
    
    public final Boolean getUpgradeAvailable() {
        return this.upgradeAvailable;
    }
    
    public final void setUpgradeAvailable(final Boolean isNewOTAFirmwareUpdate) {
        this.upgradeAvailable = isNewOTAFirmwareUpdate;
    }
}
