package com.digitalpaytech.scheduling.task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.cardprocessing.CardProcessingManager;
import com.digitalpaytech.cardprocessing.CardProcessingMaster;
import com.digitalpaytech.cardprocessing.CardProcessingThread;
import com.digitalpaytech.cardprocessing.CardProcessorFactory;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantPOS;
import com.digitalpaytech.dto.ProcessorInfo;
import com.digitalpaytech.scheduling.task.support.CardRetryExtractThread;
import com.digitalpaytech.scheduling.task.support.CardRetryProcessingThread;
import com.digitalpaytech.service.CardRetryTransactionService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.StandardConstants;

/**
 * This class processes nightly retry transaction and triggers card retry threads.
 * 
 * @author Brian Kim
 * 
 */
@Component("nightlyTransactionRetryProcessingTask")
public class NightlyTransactionRetryProcessingTask {
    
    private static final Logger LOGGER = Logger.getLogger(CardProcessingMaster.class);
        
    private static final int FIFTEEN_MINUTES_MS = 1000 * 60 * 15;
    private static final int CHARGE_RETRY_MAX_RECORDS_IN_QUEUE = 300;
    
    @Autowired
    private CardProcessingMaster cardProcessingMaster;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    @Autowired
    private CardRetryTransactionService cardRetryTransactionService;
    
    @Autowired
    private CardProcessingManager cardProcessingManager;
    
    @Autowired
    private CardProcessorFactory cardProcessorFactory;
    
    private boolean[] cardRetryExtractThreadStatus;
    
    /* Card processor to queue map */
    private Map<Integer, ConcurrentLinkedQueue<CardRetryTransaction>> processorCardRetryQueueMap;
    private Map<Integer, CardRetryProcessingThread> cardRetryProcessingThreadMap;
    
    public final CardProcessingMaster getCardProcessingMaster() {
        return this.cardProcessingMaster;
    }
    
    public final void setCardProcessingMaster(final CardProcessingMaster cardProcessingMaster) {
        this.cardProcessingMaster = cardProcessingMaster;
    }
    
    public final void setCardProcessorFactory(final CardProcessorFactory cardProcessorFactory) {
        this.cardProcessorFactory = cardProcessorFactory;
    }
    
    /**
     * This method is invoked at 7:00 am GMT every day to process nightly retry transaction.
     * Cannot set 'final' because of test cases.
     */
    public void processNightlyTransactionRetries() {
        
        if (!this.cardProcessingMaster.isScheduledCardProcessingServer()) {
            return;
        }
        
        /*
         * Check if retry already running needs to be synchronized, but entire
         * method does not.
         */
        synchronized (this) {
            /* Check if charge retry already running, if so, do not start again */
            if (this.cardProcessingMaster.getIsChargeRetryRunning()) {
                LOGGER.info("Attempt to process outstanding transaction charge retries while it is already running, exiting");
                return;
            }
            
            /* Indicate that the charge retry is running */
            LOGGER.info("Processing outstanding transaction charge retries");
            this.cardProcessingMaster.setChargeRetryRunning(true);
        }
        
        try {
            /*
             * Adjust start_time backwards by fifteen minutes
             * To ensure any recent S&F are not process twice
             */
            this.cardProcessingMaster.setChargeRetryStartTime(new Date(System.currentTimeMillis() - FIFTEEN_MINUTES_MS));
            
            /* create processing threads and queues */
            createNightlyChargeThreads();
            
            /* Get all customers */
            final Collection<Customer> customers = this.customerService.findAllChildCustomers();
            
            /* create one thread for each processor to fill their own queue */
            final Set<ProcessorInfo> ccProcessors = this.cardProcessorFactory.getCreditCardProcessorInfo();
            final CardRetryExtractThread[] extractThreads = new CardRetryExtractThread[ccProcessors.size()];
            
            int eThreadId = 0;
            this.cardRetryExtractThreadStatus = new boolean[ccProcessors.size()];
            for (ProcessorInfo processor : ccProcessors) {
                if (!this.cardProcessorFactory.isProcessorPaused(processor.getId())) {
                    final CardRetryExtractThread eThread = new CardRetryExtractThread(eThreadId, processor, customers, this.customerAdminService,
                            this.merchantAccountService, this.cardProcessingMaster.getChargeRetryStartTime(), this);
                    extractThreads[eThreadId] = eThread;
                    this.cardRetryExtractThreadStatus[eThreadId] = true;
                    eThread.start();
                    eThreadId++;
                }
            }
        } catch (Exception e) {
            final String msg = "Unexpected exception occurred when processing nightly charge retries";
            LOGGER.warn(msg, e);
            this.cardProcessingMaster.sendAdminErrorAlert(msg, e);
        }
    }
    
    /**
     * Called by CardRetryExtractThread to fill the queue of a processor type.
     * 
     * @param ma
     * @param maxRetries
     * @param retriedBefore
     */
    public final void processMerchantAccount(final MerchantAccount ma, final ProcessorInfo ccProcessorType, final int maxRetries,
        final Date retriedBefore) {
        
        final Collection<MerchantPOS> merchantPoses = this.merchantAccountService
                .findMerchantPosesByMerchantAccountIdAndCardTypeId(ma.getId(), CardProcessingConstants.CARD_TYPE_CREDIT_CARD);
        
        /* If no PS for this merchant account, go to next merchant account */
        if (merchantPoses == null || merchantPoses.size() == 0) {
            return;
        }
        
        final Collection<Integer> posIds = new ArrayList<Integer>();
        for (MerchantPOS merchantPos : merchantPoses) {
            posIds.add(merchantPos.getPointOfSale().getId());
        }
        
        /* process transactions for this merchant account */
        boolean keepProcessing = true;
        while (keepProcessing) {
            keepProcessing = processTransactionsForMerchantAccount(posIds, maxRetries, retriedBefore, ccProcessorType);
        }
    }
    
    public final boolean isCardRetryProcessingRunning(final ProcessorInfo cardProcessorType) {
        final ConcurrentLinkedQueue<CardRetryTransaction> queue = this.processorCardRetryQueueMap.get(cardProcessorType.getId());
        return this.cardProcessingMaster.getIsChargeRetryRunning() || (queue != null && !queue.isEmpty());
    }
    
    /**
     * Used for nightly charge retry.<br>
     * Called by CardRetryProcessingThread to get a Card Retry Transaction from
     * their own queue.
     * 
     * @param cardProcessorType
     * @return
     */
    public final Object getNextCardRetryTransactionForCardProcessor(final ProcessorInfo cardProcessorType) {
        /*
         * get the queue for that processor type
         * there should only be one queue for each processor type
         */
        final ConcurrentLinkedQueue<CardRetryTransaction> queue = this.processorCardRetryQueueMap.get(cardProcessorType.getId());
        
        if (queue == null) {
            throw new IllegalArgumentException("Unknown Credit Card Processor Type: " + cardProcessorType);
        }
        
        /* Stop get transaction from queue */
        if (this.cardProcessingMaster.getNeedStopProcessing()) {
            return null;
        }
        
        /* get a transaction from the queue */
        CardRetryTransaction cardRetryTran = null;
        boolean alreadyProcessing = true;
        
        synchronized (this.cardProcessingMaster.getCurrentlyProcessingTrans()) {
            while (!queue.isEmpty() && alreadyProcessing) {
                cardRetryTran = queue.remove();
                this.cardProcessingMaster.adjustNumTransInQueue(CardProcessingMaster.TYPE_CHARGE_RETRY, -1);
                
                alreadyProcessing = !this.cardProcessingMaster.getCurrentlyProcessingTrans().add(cardRetryTran);
            }
        }
        
        if (alreadyProcessing) {
            // EMS-1693
            // that implies the last entry from the queue is retrieved
            // but the transaction is being processed
            return null;
        } else {
            LOGGER.debug("Retrieved card retry transaction: " + cardRetryTran);
            return cardRetryTran;
        }
    }
    
    /* Cannot set 'final' because of test cases. */
   public Map<Integer, Integer> getAllCardRetryQueueSize() {
        final Map<Integer, Integer> currQueueSizeMap = new HashMap<Integer, Integer>();
        final Set<ProcessorInfo> ccProcessors = this.cardProcessorFactory.getCreditCardProcessorInfo();
        final List<ProcessorInfo> emvProcessorInfos = new ArrayList<ProcessorInfo>(this.cardProcessorFactory.getEMVProcessorInfo().values());
        
        final Set<ProcessorInfo> processorInfos = new HashSet<ProcessorInfo>(ccProcessors);
        processorInfos.addAll(emvProcessorInfos);
        
        for (ProcessorInfo ccProcessorType : processorInfos) {
            if (this.processorCardRetryQueueMap != null) {
                final ConcurrentLinkedQueue<CardRetryTransaction> procTransQueue = this.processorCardRetryQueueMap.get(ccProcessorType.getId());
                currQueueSizeMap.put(ccProcessorType.getId(), procTransQueue == null ? 0 : procTransQueue.size());
            } else {
                currQueueSizeMap.put(ccProcessorType.getId(), 0);
            }
        }
        return currQueueSizeMap;
    }
    
    public final void cardRetryExtractThreadFinished(final int threadId) {
        this.cardRetryExtractThreadStatus[threadId] = false;
        updateCardRetryStatus();
    }
    
    private boolean processTransactionsForMerchantAccount(final Collection<Integer> pointOfSaleIds, final int maxRetries, final Date retriedBefore,
        final ProcessorInfo cardProcessorType) {
        final ConcurrentLinkedQueue<CardRetryTransaction> procTransQueue = this.processorCardRetryQueueMap.get(cardProcessorType.getId());
        final CardRetryProcessingThread processingThread = this.cardRetryProcessingThreadMap.get(cardProcessorType.getId());
        
        // check if we are below minimum queue level
        // EMS-1694.
        // It is possible that while EMS is processing the last transaction in
        // queue
        // (queue is empty, but processing function has not returned yet)
        // the extraction thread wakes up and get the card retry record that
        // is being processed, put that into the queue again. Refer to the
        // query eventually called by loadCardRetryTransaction.
        // calling processingThread.isProcessing() makes sure that no
        // transaction is being processed before we load another batch.
        if (procTransQueue.isEmpty() && !processingThread.isProcessing()) {
            // load more transactions
            final boolean isRemainingTrans = loadCardRetryTransactions(pointOfSaleIds, maxRetries, retriedBefore, cardProcessorType);
            
            return isRemainingTrans;
        }
        
        /* We are above minimum queue level so sleep 1 second and check again */
        final String queueSize = " queue size";
        try {
            LOGGER.info("Sleeping for one second before checking " + cardProcessorType.getName() + queueSize);
            Thread.sleep(StandardConstants.SECONDS_IN_MILLIS_1);
        } catch (InterruptedException e) {
            /* Should never happen */
            final String msg = "Interrupted while sleeping before checking " + cardProcessorType.getName() + queueSize;
            LOGGER.warn(msg, e);
            this.cardProcessingMaster.sendAdminErrorAlert(msg, e);
        }
        return true;
    }
    
    /**
     * Creates one queue for each processor type.<br>
     * Also creates one producer thread and one consumer thread for each
     * processor type.
     * 
     */
    private void createNightlyChargeThreads() {
        final Set<ProcessorInfo> ccProcessors = this.cardProcessorFactory.getCreditCardProcessorInfo();
        final CardProcessingThread[] retryThreads = new CardProcessingThread[ccProcessors.size()];
        
        /* initialize queues and queue map */
        if (this.processorCardRetryQueueMap == null) {
            this.processorCardRetryQueueMap = new HashMap<Integer, ConcurrentLinkedQueue<CardRetryTransaction>>(ccProcessors.size() * 2);
            for (ProcessorInfo processor : ccProcessors) {
                this.processorCardRetryQueueMap.put(processor.getId(), new ConcurrentLinkedQueue<CardRetryTransaction>());
            }
        }
        
        /* initialize and start thread for each card processor type */
        if (this.cardRetryProcessingThreadMap == null) {
            this.cardRetryProcessingThreadMap = new HashMap<Integer, CardRetryProcessingThread>(ccProcessors.size() * 2);
        }
        this.cardRetryProcessingThreadMap.clear();
        int i = 0;
        for (ProcessorInfo processor : ccProcessors) {
            if (!this.cardProcessorFactory.isProcessorPaused(processor.getId())) {
                final CardRetryProcessingThread crtThread = new CardRetryProcessingThread(i, this.cardProcessingManager, this.cardProcessingMaster,
                        this, processor);
                this.cardRetryProcessingThreadMap.put(processor.getId(), crtThread);
                retryThreads[i] = crtThread;
                retryThreads[i].start();
                i++;
            }
        }
    }
    
    private synchronized void updateCardRetryStatus() {
        for (int i = 0; i < this.cardRetryExtractThreadStatus.length; i++) {
            if (this.cardRetryExtractThreadStatus[i]) {
                this.cardProcessingMaster.setChargeRetryRunning(true);
                return;
            }
        }
        
        /* if reach here, all threads has stopped */
        this.cardProcessingMaster.setChargeRetryRunning(false);
    }
    
    /**
     * @return - true if there could be more records to load for current
     *         merchant account
     */
    private boolean loadCardRetryTransactions(final Collection<Integer> pointOfSaleIds, final int maxRetryCount, final Date retriedBefore,
        final ProcessorInfo cardProcessorType) {
        final ConcurrentLinkedQueue<CardRetryTransaction> procTransQueue = this.processorCardRetryQueueMap.get(cardProcessorType.getId());
        final int numRecordsToLoad = CHARGE_RETRY_MAX_RECORDS_IN_QUEUE - procTransQueue.size();
        
        /* Get records & add to queue */
        final Collection<CardRetryTransaction> crts = this.cardRetryTransactionService.findCardRetryTransactions(pointOfSaleIds, maxRetryCount,
                                                                                                                 numRecordsToLoad, retriedBefore);
        
        int size = 0;
        if (crts != null && !crts.isEmpty()) {
            /* add transaction to the corresponding queue */
            size = crts.size();
            try {
                procTransQueue.addAll(crts);
                LOGGER.debug("Adding " + crts.size() + " transaction to " + cardProcessorType.getId() + " card retry queue.");
                this.cardProcessingMaster.adjustNumTransInQueue(CardProcessingMaster.TYPE_CHARGE_RETRY, crts.size());
                
                /* notify processing thread */
                this.cardRetryProcessingThreadMap.get(cardProcessorType.getId()).wakeup();
            } catch (NullPointerException e) {
                final String msg = "Cannot find card retry processing queue for processor type " + cardProcessorType.getId();
                LOGGER.error(msg, e);
            }
        }
        
        return size >= numRecordsToLoad;
    }
}
