package com.digitalpaytech.scheduling.task.kafka.consumer;

import java.io.UnsupportedEncodingException;
import java.time.Duration;
import java.util.Date;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.KafkaException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantStatusType;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.dto.cps.TerminalMessage;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.ProcessorService;
import com.digitalpaytech.service.systemadmin.SystemAdminService;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;

@Component(value = "merchantAccountUpdateTask")
@SuppressWarnings("PMD.ExcessiveImports")
public class MerchantAccountUpdateTask extends ConsumerTask {
    private static final Logger LOG = Logger.getLogger(MerchantAccountUpdateTask.class);
    
    @Resource(name = KafkaKeyConstants.PCI_LINK_PROPERTIES)
    private Properties pciLinkKafka;
    
    @Resource(name = KafkaKeyConstants.PCI_LINK_TOPIC_NAMES_PROPERTIES)
    private Properties pciLinkTopicNames;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private ProcessorService processorService;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    @Autowired
    private MessageHelper messageHelper;
    
    @Autowired
    private SystemAdminService systemAdminService;
    
    @Autowired
    private MailerService mailerService;
    
    private KafkaConsumer<String, String> consumer;
    
    @PostConstruct
    public void init() {
        this.consumer = super.buildConsumer(this.pciLinkTopicNames.getProperty(KafkaKeyConstants.MERCHANT_SERVICE_UPDATE_TOPIC_NAME_KEY),
                                            this.pciLinkTopicNames.getProperty(KafkaKeyConstants.MERCHANT_SERVICE_UPDATE_CONSUMER_GROUP),
                                            this.pciLinkKafka, this.pciLinkTopicNames.getProperty(KafkaKeyConstants.DELIMETER));
    }
    
    @PreDestroy
    public void close() {
        super.closeConsumer(this.consumer);
    }
    
    @Scheduled(fixedDelayString = "${consumer.millisecond.delay}")
    public void processOnSchedule() {
        process();
    }
    
    @Async("kafkaConsumerExecutor")
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    public void process() {
        super.traceLog(LOG, this.pciLinkTopicNames, KafkaKeyConstants.MERCHANT_SERVICE_UPDATE_TOPIC_NAME_KEY,
                       KafkaKeyConstants.MERCHANT_SERVICE_UPDATE_CONSUMER_GROUP);
        
        final ConsumerRecords<String, String> consumerRecords = this.consumer.poll(Duration.ofMillis(super.getDefaultConsumerPollTimeout()));
        
        for (ConsumerRecord<String, String> rec : consumerRecords) {
            try {
                final TerminalMessage terminalMessage =
                        super.getMessageConsumerFactory().deserializeMessageWithEmbeddedHeaders(rec.value(), TerminalMessage.class);
                
                LOG.info(terminalMessage);
                
                final MerchantAccount targetMerchantAccount =
                        this.merchantAccountService.findByTerminalTokenAndIsLink(terminalMessage.getTerminalToken());
                
                if (targetMerchantAccount == null) {
                    createMerchantAccount(terminalMessage);
                } else if (terminalMessage.getIsDeleted() != null && terminalMessage.getIsDeleted()) {
                    deleteMerchantAccount(terminalMessage, targetMerchantAccount);
                } else if (CardProcessingUtil.isNotNullAndIsLink(targetMerchantAccount)) {
                    updateMerchantAccount(terminalMessage, targetMerchantAccount);
                } else {
                    LOG.warn(String.format("MerchantAccount update aborted for terminalToken=%s update to non-link MerchantAccount",
                                           terminalMessage.getTerminalToken()));
                }
            } catch (UnsupportedEncodingException | JsonException e) {
                LOG.error(String.format("Unable to process message in %s topic [%s]",
                                        super.getTopicName(KafkaKeyConstants.MERCHANT_SERVICE_UPDATE_TOPIC_NAME_KEY, this.pciLinkTopicNames),
                                        rec.value()));
            }
        }
        
        try {
            this.consumer.commitSync();
        } catch (KafkaException kex) {
            LOG.error(kex);
        }
    }
    
    private void createMerchantAccount(final TerminalMessage terminalMessage) {
        upsertMerchantAccount(terminalMessage, new MerchantAccount());
    }
    
    private void updateMerchantAccount(final TerminalMessage terminalMessage, final MerchantAccount targetMerchantAccount) {
        final Customer customer = this.customerService.findCustomer(targetMerchantAccount.getCustomer().getId());
        if (customer.getUnifiId().equals(terminalMessage.getCustomerId())) {
            upsertMerchantAccount(terminalMessage, targetMerchantAccount);
        } else {
            LOG.warn(String.format("MerchantAccount update aborted for terminalToken=%s unifiId=%s customerId=%s", terminalMessage.getTerminalToken(),
                                   customer.getUnifiId(), terminalMessage.getCustomerId()));
        }
    }
    
    private void deleteMerchantAccount(final TerminalMessage tm, final MerchantAccount ma) {
        final Customer customer = this.customerService.findCustomer(ma.getCustomer().getId());
        if (tm.getCustomerId() == null || tm.getCustomerId().equals(customer.getUnifiId())) {
            final MerchantStatusType merchantStatusType = new MerchantStatusType();
            merchantStatusType.setId(WebCoreConstants.MERCHANT_STATUS_TYPE_DELETED_ID);
            ma.setMerchantStatusType(merchantStatusType);
            ma.setLastModifiedGmt(new Date());
            ma.setLastModifiedByUserId(1);
            
            if (LOG.isDebugEnabled()) {
                LOG.debug(String.format("Deleted MerchantAccount with TerminalToken [%s]", tm.getTerminalToken()));
            }
            this.merchantAccountService.saveOrUpdateMerchantAccount(ma);
        } else {
            LOG.warn(String.format("Delete with wrong customerId for MerchantAccount with TerminalToken [%s]", tm.getTerminalToken()));
        }
    }
    
    private void upsertMerchantAccount(final TerminalMessage tm, final MerchantAccount ma) {
        
        final Customer customer = this.customerService.findCustomerByUnifiId(tm.getCustomerId());
        if (customer == null) {
            LOG.warn(String.format("Updating MerchantAccount failed, Customer not found for UnifiId %s", tm.getCustomerId()));
        } else {
            ma.setCustomer(customer);
            
            final MerchantStatusType merchantStatusType = new MerchantStatusType();
            merchantStatusType.setId(chooseStatusTypeId(tm));
            ma.setMerchantStatusType(merchantStatusType);
            ma.setTerminalToken(tm.getTerminalToken());
            ma.setLastModifiedGmt(new Date());
            ma.setLastModifiedByUserId(1);
            ma.setIsLink(true);
            ma.setName(tm.getMerchantName());
            ma.setTerminalName(StringUtils.left(tm.getTerminalName(), HibernateConstants.VARCHAR_LENGTH_30));
            ma.setTimeZone(tm.getTimeZone());
            
            if (!StringUtils.isBlank(tm.getProcessorType())) {
                ma.setProcessor(this.processorService
                        .findByName(tm.getProcessorType() + this.messageHelper.getMessageWithDefault(WebCoreConstants.LINK_PROCESSOR_NAME_SUFFIX_KEY, 
                                                                                                     WebCoreConstants.LINK_PROCESSOR_NAME_SUFFIX)));
            }
            
            if (tm.getIsValidated() != null) {
                ma.setIsValidated(tm.getIsValidated());
            }
            
            if (!StringUtils.isBlank(tm.getQuarterOfDay())) {
                try {
                    ma.setCloseQuarterOfDay(Byte.parseByte(tm.getQuarterOfDay()));
                } catch (NumberFormatException nfe) {
                    LOG.error(nfe);
                }
            }
            
            if (LOG.isDebugEnabled()) {
                LOG.debug(String.format("Updated MerchantAccount with TerminalToken [%s]", tm.getTerminalToken()));
            }
            
            /*
             * Check if migrated merchant account is saved this moment.
             */
            final MerchantAccount targetMerchantAccount = this.merchantAccountService.findByTerminalTokenAndIsLink(tm.getTerminalToken());
            if (targetMerchantAccount != null) {
                ma.setId(targetMerchantAccount.getId());
            }
            this.merchantAccountService.saveOrUpdateMerchantAccount(ma);
            
            /*
             * If MerchantAccountService.migrateMerchantAccount completed first, search original MerchantAccount record by terminalToken
             * and isLink = false first. Then find MerchantPOS by MerchantAccount Id.
             */
            final MerchantAccount oriMerchantAcct = this.merchantAccountService.findByTerminalTokenAndIsLink(tm.getTerminalToken(), false);
            if (oriMerchantAcct != null) {
                this.merchantAccountService.findMerchantPosesByMerchantAccountId(oriMerchantAcct.getId()).forEach(mpos -> {
                    mpos.setMerchantAccount(ma);
                    mpos.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
                    this.systemAdminService.saveOrUpdateMerchantPos(mpos);
                });
            } else {
                final String err =
                        "Cannot update MerchantPOS with migrated MerchantAccount Id due to not able to find the original MerchantAccount by TerminalToken: "
                                   + tm.getTerminalToken();
                LOG.error(err);
                this.mailerService.sendAdminErrorAlert("MerchantAccount TerminalToken: " + tm.getTerminalToken(), err, null);
            }
        }
    }
    
    private int chooseStatusTypeId(final TerminalMessage tm) {
        if (tm.getIsDeleted() != null && tm.getIsDeleted()) {
            return WebCoreConstants.MERCHANT_STATUS_TYPE_DELETED_ID;
        } else if (tm.getIsEnabled() != null && tm.getIsEnabled()) {
            return WebCoreConstants.MERCHANT_STATUS_TYPE_ENABLED_ID;
        } else {
            return WebCoreConstants.MERCHANT_STATUS_TYPE_DISABLED_ID;
        }
    }
}
