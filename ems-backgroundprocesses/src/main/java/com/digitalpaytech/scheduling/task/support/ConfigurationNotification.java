package com.digitalpaytech.scheduling.task.support;

import java.util.List;

public class ConfigurationNotification {
    private String deviceSerialNumber;
    private String customerId;
    private String snapShotId;
    private List<ConfigurationFileInfo> fileInfos;
    
    public final String getDeviceSerialNumber() {
        return this.deviceSerialNumber;
    }
    
    public final void setDeviceSerialNumber(final String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }
    
    public final String getCustomerId() {
        return this.customerId;
    }
    
    public final void setCustomerId(final String customerId) {
        this.customerId = customerId;
    }
    
    public final String getSnapShotId() {
        return this.snapShotId;
    }
    
    public final void setSnapShotId(final String snapShotId) {
        this.snapShotId = snapShotId;
    }
    
    public final List<ConfigurationFileInfo> getFileInfos() {
        return this.fileInfos;
    }   
    
    public final void setFileInfos(final List<ConfigurationFileInfo> fileInfos) {
        this.fileInfos = fileInfos;
    }
}
