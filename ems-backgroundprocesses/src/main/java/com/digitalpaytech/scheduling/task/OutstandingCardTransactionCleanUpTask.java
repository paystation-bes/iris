package com.digitalpaytech.scheduling.task;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.TimeZone;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.cardprocessing.CardProcessingManager;
import com.digitalpaytech.cardprocessing.CardProcessingMaster;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.ReversalService;
import com.digitalpaytech.service.cps.CPSProcessDataService;
import com.digitalpaytech.util.DateUtil;

/**
 * This class processes outstanding reversal, card settlement and cleans up card authorization.
 * 
 * @author Brian Kim
 * 
 */
@Component("outstandingCardTransactionCleanUpTask")
public class OutstandingCardTransactionCleanUpTask {
    
    private static final Logger LOGGER = Logger.getLogger(OutstandingCardTransactionCleanUpTask.class);
    private static final int TEN_MINUTES = 10;
    private static final int FIFTEEN_MINUTES = 15;
    
    @Autowired
    private CardProcessingMaster cardProcessingMaster;
    
    @Autowired
    private CardProcessingManager cardProcessingManager;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private CPSProcessDataService cpsProcessDataService;
    
    @Autowired
    private ReversalService reversalService;
    
    /**
     * This method processes outstanding reversal transaction.
     * It is triggered from a scheduler defined in ems-backgroundprocesses.xml (cardReversalJobTrigger)
     */
    public final void processOutstandingTransactionReversals() {
        if (!this.cardProcessingMaster.isScheduledCardProcessingServer()) {
            return;
        }
        
        LOGGER.info("Processing outstanding transaction reversals");
        
        if (this.cardProcessingMaster.getNeedStopProcessing()) {
            LOGGER.info("Digital Iris is shutting down, stop processing outstanding transaction reversals");
            return;
        }
        
        Collection<Reversal> transactionList = null;
        
        /* Get incomplete reversals */
        try {
            final Date processedBefore = DateUtils.addMinutes(new Date(), -TEN_MINUTES);
            
            transactionList = this.reversalService.getIncompleteReversalsForReversal(processedBefore);
        } catch (Exception e) {
            final String msg = "Unable to retrieve incomplete reversals for batch reversal";
            LOGGER.error(msg, e);
            this.mailerService.sendAdminErrorAlert("Card Processing", msg, e);
        }
        
        if (transactionList == null || transactionList.size() == 0) {
            LOGGER.info("No reversals for batch reversal");
            return;
        }
        
        final Iterator<Reversal> it = transactionList.iterator();
        while (it.hasNext()) {
            final Reversal reversal = it.next();
            this.cardProcessingMaster.addReversalToReversalQueue(reversal);
        }
    }
    
    /**
     * This method processes outstanding unsettled transaction and cleans up overdue card data.
     * It is triggered from a scheduler defined in ems-backgroundprocesses.xml (cardSettlementJobTrigger)
     */
    public final void processOutstandingTransactionSettlementsAndCleanupCardData() {
        if (!this.cardProcessingMaster.isScheduledCardProcessingServer()) {
            return;
        }
        
        /* Get Authorized, but unsettled transactions -> processorTransactionType.id = 1 (Auth PlaceHolder) */
        final Collection<ProcessorTransaction> transactionList
            = this.cardProcessingManager.getAuthorizedProcessorTransactionsForSettlement(DateUtils.addMinutes(new Date(), -FIFTEEN_MINUTES));
        
        /* Add transactions to settlement queue */
        if (transactionList == null || transactionList.size() == 0) {
            LOGGER.info("No transactions to batch settle");
        } else {
            final Iterator<ProcessorTransaction> it = transactionList.iterator();
            
            while (it.hasNext()) {
                final ProcessorTransaction ptd = it.next();
                ptd.setLoaded(false);                
                this.cardProcessingMaster.addTransactionToSettlementQueue(ptd);
            }
        }
        
        /* Clean up overdue card data and create PreAuthHolding record(s). */
        this.cardProcessingManager.cleanupOverdueCardData();
    }
    
    
    public final void processCaptureChargeResults() {
        if (!this.cardProcessingMaster.isScheduledCardProcessingServer()) {
            return;
        }
        /* Get Authorized, but unsettled transactions -> processorTransactionType.id = 1 (Auth PlaceHolder) */
        final Collection<ProcessorTransaction> transactionList
            = this.cpsProcessDataService.findAuthorizedTransactionsForCaptureChargeResults(DateUtils.addMinutes(new Date(), -TEN_MINUTES));
        
        /* Add transactions to settlement queue */
        if (transactionList != null) {
            final Iterator<ProcessorTransaction> it = transactionList.iterator();
            while (it.hasNext()) {
                final ProcessorTransaction ptd = it.next();
                ptd.setLoaded(false);                
                this.cardProcessingMaster.addTransactionToSettlementQueue(ptd);
            }
        }
    }
    
    /**
     * This method cleans up overdue "Recoverable" PreAuth from PreAuthHolding table and updates ProcessorTransaction records to "Uncloseable".
     * It is triggered from a scheduler defined in ems-backgroundprocesses.xml (cardAuthorizationBackupCleanupJobTrigger)
     */
    public final void processCardAuthorizationBackupCleanup() {
        if (!this.cardProcessingMaster.isScheduledCardProcessingServer()) {
            return;
        }
        
        LOGGER.debug("Processing PreAuthHolding data clean-up");
        
        int maxHoldingDays = 0;
        try {
            maxHoldingDays = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.CARD_AUTHORIZATION_BACK_UP_MAX_HOLDING_DAYS,
                EmsPropertiesService.DEFAULT_CARD_AUTHORIZATION_BACK_UP_MAX_HOLDING_DAYS);
        } catch (Exception e) {
            LOGGER.warn("getCardAuthorizationBackUpHoldingDays is not an integer");
            maxHoldingDays = EmsPropertiesService.DEFAULT_CARD_AUTHORIZATION_BACK_UP_MAX_HOLDING_DAYS;
        }
        
        final Date maxHoldingDay = DateUtils.addDays(DateUtil.getDatetimeInGMT(new Date(), TimeZone.getDefault().getDisplayName()), -maxHoldingDays);
        
        /* update ProcessorTransaction type to 'Uncloseable'. */
        this.cardProcessingManager.updateProcessorTransactionToUncloseable(maxHoldingDay);
        
        /* clean up PreAuthHolding table. */
        this.cardProcessingManager.cleanupCardAuthorizationBackup(maxHoldingDay);
        LOGGER.debug("Finished PreAuthHolding data clean-up");
    }

    public final void setCPSProcessDataService(final CPSProcessDataService cpsProcessDataService) {
        this.cpsProcessDataService = cpsProcessDataService;
    }
}
