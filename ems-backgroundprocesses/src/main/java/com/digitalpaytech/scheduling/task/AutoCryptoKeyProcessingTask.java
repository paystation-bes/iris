package com.digitalpaytech.scheduling.task;

import java.net.InetAddress;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.cardprocessing.CardProcessingManager;
import com.digitalpaytech.cardprocessing.CardProcessingMaster;
import com.digitalpaytech.dto.crypto.HashAlgorithmData;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.scheduling.task.support.CardReencryptThread;
import com.digitalpaytech.service.CardRetryTransactionService;
import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.CustomerBadCardService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.HashAlgorithmTypeService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.util.KeyValuePair;

/**
 * This class checks current external & internal key and processes generating
 * next key. checkAndProcessCryptoKey() is scheduled to check and process this
 * every day. (8:00 GMT)
 * 
 * @author Brian Kim
 * 
 */
@Component("autoCryptoKeyProcessingTask")
public class AutoCryptoKeyProcessingTask {
    private static final Logger LOGGER = Logger.getLogger(AutoCryptoKeyProcessingTask.class);
    private static final String DEFAULT_PROCESS_SERVER = "EmsMain";
    private static final String BADCC_PROCESSING_ADMIN_ALERT_NAME = "Create Reencrypto Bad Credit Card Processing - ";
    
    @Autowired
    private CustomerBadCardService customerBadCardService;
    
    @Autowired
    private CardProcessingManager cardProcessingManager;
    
    @Autowired
    private CryptoService cryptoService;
    
    @Autowired
    private CardRetryTransactionService cardRetryTransactionService;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private ClusterMemberService clusterMemberService;
    
    @Autowired
    private CardProcessingMaster cardProcessingMaster;
    
    @Autowired
    private HashAlgorithmTypeService hashAlgorithmTypeService;
    
    private CardReencryptThread cardReencryptThread;
    
    public final void setCustomerBadCardService(final CustomerBadCardService customerBadCardService) {
        this.customerBadCardService = customerBadCardService;
    }
    
    public final void setCardProcessingManager(final CardProcessingManager cardProcessingManager) {
        this.cardProcessingManager = cardProcessingManager;
    }
    
    public final void setCryptoService(final CryptoService cryptoService) {
        this.cryptoService = cryptoService;
    }
    
    public final void setCardRetryTransactionService(final CardRetryTransactionService cardRetryTransactionService) {
        this.cardRetryTransactionService = cardRetryTransactionService;
    }
    
    public final void setMailerService(final MailerService mailerService) {
        this.mailerService = mailerService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final void setClusterMemberService(final ClusterMemberService clusterMemberService) {
        this.clusterMemberService = clusterMemberService;
    }
    
    public final void setHashAlgorithmTypeService(final HashAlgorithmTypeService hashAlgorithmTypeService) {
        this.hashAlgorithmTypeService = hashAlgorithmTypeService;
    }
    
    /**
     * Is this server configured for auto crypto key process
     * 
     * @return
     */
    private boolean isCryptoKeyGenerateServer() {
        try {
            // get the cluster member name
            final String clusterName = this.clusterMemberService.getClusterName();
            if (clusterName == null || clusterName.isEmpty()) {
                throw new ApplicationException("Cluster member name is empty");
            }
            
            // get the configured auto crypto key processing server from
            // database
            final String processServer = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.AUTO_CRYPTO_KEY_PROCESS_SERVER,
                DEFAULT_PROCESS_SERVER, true);
            
            if (clusterName.equals(processServer)) {
                LOGGER.info("Server " + clusterName + " configured for anto crypto key processing tasks.");
                return true;
            } else {
                LOGGER.info("Server " + clusterName + " not configured for anto crypto key processing tasks.");
                return false;
            }
        } catch (Exception e) {
            String addr = null;
            try {
                addr = InetAddress.getLocalHost().getHostAddress();
            } catch (Exception e1) {
                // shouldn't happen
            }
            final String err = new StringBuilder("Unable to read memeber name from cluster.properties for cluster member ").append(addr).toString();
            LOGGER.error(err);
            this.mailerService.sendAdminErrorAlert("Auto cryto key processor", err, e);
            
            return false;
        }
    }
    
    private void checkAndUpdateSigningKeyStore() {
        // if (!isCryptoKeyGenerateServer()) {
        // // not the configured server
        // return;
        // }
        LOGGER.info("Cron Job schedules checkAndUpdateSigningKeyStore method");
        final List<String> activeSignAlgorithms = this.hashAlgorithmTypeService.findActiveSigningHashAlgorithmNames();
        LOGGER.info("Active signing algorithms retrieved");
        this.cryptoService.checkAndUpdateSigningKeyStore(activeSignAlgorithms);
        
    }
    
    /**
     * 1. Check and process external key - 6 months new key, 12 months remove
     * deprecated key 2. Check and process internal key - same remove and create
     * interval as external key reencrypt cc data BadCreditCard, PreAuth,
     * PreAuthHoldings, Reserval, CardRetryTransaction
     */
    public final void checkAndProcessCryptoKey() {
        
        // all servers check if next internal and next external key available in
        // key store
        this.cryptoService.isKeyStoreUpToDate();
        
        if (!isCryptoKeyGenerateServer()) {
            // not the configured server
            return;
        }
        
        final boolean rotateKeyInIris = this.emsPropertiesService.getPropertyValueAsBoolean(EmsPropertiesService.ROTATE_KEYS_IN_IRIS, false);
        if (rotateKeyInIris) {
            
            // External Key
            final Map<Integer, HashAlgorithmData> map = this.hashAlgorithmTypeService.findHashAlgorithmNotSigningMap();
            this.cryptoService.checkAndProcessExternalCryptoKey(map);
            this.cryptoService.checkAndRemoveDeprecatedExternalCryptoKey();
            
            // Internal Key
            final Map<Integer, HashAlgorithmData> internalMap = this.hashAlgorithmTypeService.findForInternalKeyRotation();
            this.cryptoService.checkAndProcessInternalCryptoKey(internalMap);
        }
        
        String prevKeyInfo = null;
        try {
            final KeyValuePair<Integer, Integer> currKeyInfo = this.cryptoService.splitKeyInfo(this.cryptoService.currentInternalKey());
            if (currKeyInfo.getValue() > 1) {
                prevKeyInfo = this.cryptoService.createKeyInfo(currKeyInfo.getKey(), currKeyInfo.getValue() - 1);
            }
        } catch (CryptoException ce) {
            LOGGER.error("Failed to retrieve current key", ce);
        }
        
        // check Reencrypt thread is running, otherwise start it
        if (this.cardReencryptThread == null) {
            try {
                
                if (haveCreditCardDataNeedEncrypt(prevKeyInfo)) {
                    this.cardReencryptThread = new CardReencryptThread(prevKeyInfo, this.cryptoService, this.customerBadCardService, this.emsPropertiesService,
                            this.cardRetryTransactionService, this.cardProcessingMaster, this.mailerService, this.cardProcessingManager);
                    this.cardReencryptThread.start();
                }
            } catch (Exception e) {
                final String msg = "Unable to start reencrypt thread";
                LOGGER.error(msg, e);
                sendBadCreditCardProcessingAdminErrorAlert("", msg, e);
            }
        } else {
            if (this.cardReencryptThread.isReencryptComplete()) {
                this.cardReencryptThread = null; // Try to release this thread
            }
        }
        
        if (rotateKeyInIris) {
            // check all credit card is been updated before call remove function
            if (!haveCreditCardDataNeedEncrypt(prevKeyInfo)) {
                this.cryptoService.checkAndRemoveInternalCryptoKey();
            }
        }
    }
    
    public final boolean haveCreditCardDataNeedEncrypt(final String prevKeyInfo) {
        if (prevKeyInfo == null) {
            return true;
        }
        
        try {
            
            
            int totalData = this.customerBadCardService.getTotalBadCreditCard(prevKeyInfo + "%");
            if (totalData > 0) {
                return true;
            }
            
            totalData = this.cardRetryTransactionService.getMatchingTransactionsCountForCardData(prevKeyInfo + "%");
            if (totalData > 0) {
                return true;
            }
            
            totalData = this.cardProcessingManager.findTotalReversalsByCardData(prevKeyInfo + "%");
            if (totalData > 0) {
                return true;
            }
            
            totalData = this.cardProcessingManager.findTotalPreAuthsByCardData(prevKeyInfo + "%");
            if (totalData > 0) {
                return true;
            }
            
            totalData = this.cardProcessingManager.findTotalPreAuthHoldingsByCardData(prevKeyInfo + "%");
            if (totalData > 0) {
                return true;
            }
        } catch (Exception e) {
            final String msg = "Unable to count credit card need re-encrypt";
            LOGGER.error(msg, e);
            sendBadCreditCardProcessingAdminErrorAlert("", msg, e);
            return true;
        }
        return false;
    }
    
    private void sendBadCreditCardProcessingAdminErrorAlert(final String subject, final String message, final Exception e) {
        this.mailerService.sendAdminErrorAlert(BADCC_PROCESSING_ADMIN_ALERT_NAME + subject, message, e);
    }
}
