package com.digitalpaytech.scheduling.task.support;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;

import com.digitalpaytech.cardprocessing.CardProcessingManager;
import com.digitalpaytech.cardprocessing.CardProcessingMaster;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.CustomerBadCard;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.PreAuthHolding;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.service.CardRetryTransactionService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.CustomerBadCardService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MailerService;

public class CardReencryptThread extends Thread {
    private static final Logger LOGGER = Logger.getLogger(CardReencryptThread.class);
    
    private static final int DEFAULT_NUMBER_BACTCH_IN_PEAKTIME = 2;
    private static final int DEFAULT_NUMBER_BACTCH_OFF_PEAKTIME = 45;
    private static final int DEFAULT_SLEEP_IN_PEAKTIME = 1000;
    private static final int DEFAULT_SLEEP_IN_OFFPEAKTIME = 200;
    private static final int SLEEP_WAITING_FOR_RETRY = 1000 * 60 * 5;
    // GMT -> 6 PST
    private static final String DEFAULT_START_PEAKTIME = "14:00:00";
    // GMT -> 23 PST
    private static final String DEFAULT_END_PEAKTIME = "07:00:00";
    private static final String BADCC_PROCESSING_ADMIN_ALERT_NAME = "Reencrypto Bad Credit Card Processing - ";
    
    private CryptoService cryptoService;
    private CustomerBadCardService customerBadCardService;
    private CardProcessingMaster cardProcessingMaster;
    private CardRetryTransactionService cardRetryTransactionService;
    private MailerService mailerService;
    private CardProcessingManager cardProcessingManager;
    
    private boolean isComplete;
    private String prevKeyInfo;
    private int numberOfBatchInPeakTime = DEFAULT_NUMBER_BACTCH_IN_PEAKTIME;
    private int numberOfBatchOffPeakTime = DEFAULT_NUMBER_BACTCH_OFF_PEAKTIME;
    // 1 second;
    private long sleepInPeakTime = DEFAULT_SLEEP_IN_PEAKTIME;
    // 0.2 second;
    private long sleepInOffPeakTime = DEFAULT_SLEEP_IN_OFFPEAKTIME;
    private String startPeakTime = DEFAULT_START_PEAKTIME;
    private String endPeakTime = DEFAULT_END_PEAKTIME;
    
    public CardReencryptThread(final String prevKeyInfo, final CryptoService cryptoService, final CustomerBadCardService customerBadCardService,
        final EmsPropertiesService emsPropertiesService, final CardRetryTransactionService cardRetryTransactionService,
        final CardProcessingMaster cardProcessingMaster, final MailerService mailerService, final CardProcessingManager cardProcessingManager)
        throws ApplicationException {
        
        super(CardReencryptThread.class.getName());
        
        this.prevKeyInfo = prevKeyInfo;
        
        // set lower priority
        this.setPriority(Thread.MIN_PRIORITY);
        
        this.setDaemon(true);
        
        this.cryptoService = cryptoService;
        
        this.customerBadCardService = customerBadCardService;
        
        this.cardRetryTransactionService = cardRetryTransactionService;
        
        this.cardProcessingMaster = cardProcessingMaster;
        
        this.mailerService = mailerService;
        
        this.cardProcessingManager = cardProcessingManager;
        
        try {
            this.numberOfBatchInPeakTime = emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.NUMBER_OF_BATCH_REENCRYPT_PEAKTIME,
                DEFAULT_NUMBER_BACTCH_IN_PEAKTIME);
            
            this.numberOfBatchOffPeakTime = emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.NUMBER_OF_BATCH_REENCRYPT,
                DEFAULT_NUMBER_BACTCH_OFF_PEAKTIME);
            
            this.sleepInPeakTime = emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.SLEEP_PEAKTIME, DEFAULT_SLEEP_IN_PEAKTIME);
            
            this.sleepInOffPeakTime = emsPropertiesService
                    .getPropertyValueAsInt(EmsPropertiesService.SLEEP_OFFPEAKTIME, DEFAULT_SLEEP_IN_OFFPEAKTIME);
            
            this.startPeakTime = emsPropertiesService.getPropertyValue(EmsPropertiesService.START_PEAKTIME, DEFAULT_START_PEAKTIME);
            
            this.endPeakTime = emsPropertiesService.getPropertyValue(EmsPropertiesService.END_PEAKTIME, DEFAULT_END_PEAKTIME);
        } catch (Exception ex) {
            throw new ApplicationException();
        }
    }
    
    /*
     * also keep this in mind for anything else that is really wrong that should
     * never go wrong but that you are checking for
     */
    
    public final void run() {
        LOGGER.info("Starting CardReencryptThread ");
        this.isComplete = false;
        int numEncrypt;
        
        while (!this.isComplete) {
            try {
                // Peak Hour
                if (isPeakTime(new Date(System.currentTimeMillis()))) {
                    // Processing BadCredit Card Data
                    numEncrypt = encryptBadCreditCard(this.numberOfBatchInPeakTime);
                    
                    if (numEncrypt == 0) {
                        // Card Retry Process doesn't run at peak hour,
                        // Processing Encrypt CardRetry Transaction
                        numEncrypt = encryptCardRetryTransaction(this.numberOfBatchInPeakTime);
                        
                        if (numEncrypt == 0) {
                            numEncrypt = encryptReversal();
                            
                            // re-encrypt PreAuth
                            if (numEncrypt == 0) {
                                numEncrypt = encryptPreAuth();
                                
                                // re-encrypt PreAuthHolding
                                if (numEncrypt == 0) {
                                    numEncrypt = encryptPreAuthHolding();
                                }
                            }
                            
                            if (numEncrypt == 0) {
                                this.isComplete = true;
                            }
                        }
                    }
                    
                    // Sleep some time
                    synchronized (this) {
                        wait(this.sleepInPeakTime);
                    }
                    // Non-peak hours
                } else {
                    // If card processing running, don't re-encrypt
                    // wait 5 minutes and check again
                    if (this.cardProcessingMaster.isProcessingRunning(CardProcessingMaster.TYPE_CHARGE_RETRY)) {
                        synchronized (this) {
                            wait(SLEEP_WAITING_FOR_RETRY);
                        }
                    } else {
                        // Processing BadCredit Card Data
                        numEncrypt = encryptBadCreditCard(this.numberOfBatchOffPeakTime);
                        
                        if (numEncrypt == 0) {
                            // Processing Encrypt CardRetry Transaction
                            numEncrypt = encryptCardRetryTransaction(this.numberOfBatchOffPeakTime);
                            if (numEncrypt == 0) {
                                numEncrypt = encryptReversal();
                                
                                // re-encrypt PreAuth
                                if (numEncrypt == 0) {
                                    numEncrypt = encryptPreAuth();
                                    
                                    // re-encrypt PreAuthHolding
                                    if (numEncrypt == 0) {
                                        numEncrypt = encryptPreAuthHolding();
                                    }
                                }
                                
                                if (numEncrypt == 0) {
                                    this.isComplete = true;
                                }
                            }
                        }
                        
                        // Sleep some time
                        synchronized (this) {
                            wait(this.sleepInOffPeakTime);
                        }
                    }
                }
            } catch (Exception e) {
                // if keys not working or other things broken admin alert emails
                // need to get sent
                this.isComplete = true; // if any exception, jump out
                final String msg = "Unable to reencrypt credit card data";
                LOGGER.error(msg, e);
                sendBadCreditCardProcessingAdminErrorAlert("", msg, e);
            }
        }
    }
    
    public final boolean isReencryptComplete() {
        if (!isAlive()) {
            // if thread die, return true, let parent to release this thread;
            return true;
        }
        
        return this.isComplete;
    }
    
    // need to change defaults to GMT(ie. 14:00 to 07:00, aka 06:00->23:00 PST)
    // - need to change calculation
    // for peak hours
    private boolean isPeakTime(final Date curDate) {
        final SimpleDateFormat dtFormat = new SimpleDateFormat("HH:mm:ss");
        final String curTime = dtFormat.format(curDate);
        if (this.startPeakTime.compareTo(this.endPeakTime) < 0) { // no cross midnight
            if ((curTime.compareTo(this.startPeakTime) >= 0) && (curTime.compareTo(this.endPeakTime) <= 0)) {
                return true;
            }
        } else {
            if ((curTime.compareTo(this.startPeakTime) >= 0) && (curTime.compareTo("23:59:59") <= 0)) {
                return true;
            }
            if ((curTime.compareTo("00:00:00") >= 0) && (curTime.compareTo(this.endPeakTime) <= 0)) {
                return true;
            }
        }
        return false;
    }
    
    private int encryptBadCreditCard(final int numberOfBatch) throws DataAccessException, CryptoException {
        final Collection<CustomerBadCard> badCCList = this.customerBadCardService.getBadCreditCardByCardData(this.prevKeyInfo + "%", numberOfBatch,
            true);
        
        if (badCCList.size() > 0) {
            final Iterator<CustomerBadCard> it = badCCList.iterator();
            CustomerBadCard badCCard;
            String cardData;
            
            while (it.hasNext()) {
                badCCard = it.next();
                cardData = badCCard.getCardData();
                if (cardData != null) {
                    cardData = this.cryptoService.decryptData(cardData);
                    cardData = this.cryptoService.encryptData(cardData);
                    badCCard.setCardData(cardData);
                }
            }
            this.customerBadCardService.updateCustomerBadCards(badCCList);
        }
        return badCCList.size();
    }
    
    private int encryptCardRetryTransaction(final int numberOfBatch) throws DataAccessException, CryptoException {
        final Collection<CardRetryTransaction> badCCList = this.cardRetryTransactionService.getCardRetryTransactionByCardData(this.prevKeyInfo + "%",
            numberOfBatch, true);
        if (badCCList.size() > 0) {
            final Iterator<CardRetryTransaction> it = badCCList.iterator();
            CardRetryTransaction cardRetry;
            String cardData;
            
            while (it.hasNext()) {
                cardRetry = it.next();
                cardData = cardRetry.getCardData();
                if (cardData != null) {
                    cardData = this.cryptoService.decryptData(cardData);
                    cardData = this.cryptoService.encryptData(cardData);
                    cardRetry.setCardData(cardData);
                }
                this.cardRetryTransactionService.updateCardRetryTransaction(cardRetry);
            }
            
        }
        return badCCList.size();
    }
    
    private int encryptReversal() throws DataAccessException, CryptoException {
        final Collection<Reversal> reversalList = this.cardProcessingManager.findReversalsByCardData(this.prevKeyInfo + "%", true);
        
        if (reversalList.size() > 0) {
            final Iterator<Reversal> itr = reversalList.iterator();
            Reversal reversal;
            String cardNumber;
            while (itr.hasNext()) {
                reversal = itr.next();
                cardNumber = reversal.getCardData();
                if (cardNumber != null) {
                    cardNumber = this.cryptoService.decryptData(cardNumber);
                    cardNumber = this.cryptoService.encryptData(cardNumber);
                    reversal.setCardData(cardNumber);
                    this.cardProcessingManager.updateReversal(reversal);
                }
            }
        }
        return reversalList.size();
    }
    
    private int encryptPreAuth() throws CryptoException {
        final Collection<PreAuth> preAuthList = this.cardProcessingManager.findPreAuthsByCardData(this.prevKeyInfo + "%", true);
        
        if (preAuthList.size() > 0) {
            final Iterator<PreAuth> itr = preAuthList.iterator();
            PreAuth preAuth;
            String cardNumber;
            while (itr.hasNext()) {
                preAuth = itr.next();
                cardNumber = preAuth.getCardData();
                if (cardNumber != null) {
                    cardNumber = this.cryptoService.decryptData(cardNumber);
                    cardNumber = this.cryptoService.encryptData(cardNumber);
                    preAuth.setCardData(cardNumber);
                    this.cardProcessingManager.updatePreAuth(preAuth);
                }
            }
        }
        return preAuthList.size();
    }
    
    private int encryptPreAuthHolding() throws CryptoException {
        final Collection<PreAuthHolding> holdingList = this.cardProcessingManager.findPreAuthHoldingsByCardData(this.prevKeyInfo + "%", true);
        
        if (holdingList.size() > 0) {
            final Iterator<PreAuthHolding> itr = holdingList.iterator();
            PreAuthHolding holding;
            String cardNumber;
            while (itr.hasNext()) {
                holding = itr.next();
                cardNumber = holding.getCardData();
                if (cardNumber != null) {
                    cardNumber = this.cryptoService.decryptData(cardNumber);
                    cardNumber = this.cryptoService.encryptData(cardNumber);
                    holding.setCardData(cardNumber);
                    this.cardProcessingManager.updatePreAuthHolding(holding);
                }
            }
        }
        return holdingList.size();
    }
    
    private void sendBadCreditCardProcessingAdminErrorAlert(final String subject, final String message, final Exception e) {
        this.mailerService.sendAdminErrorAlert(BADCC_PROCESSING_ADMIN_ALERT_NAME + subject, message, e);
    }
    
}
