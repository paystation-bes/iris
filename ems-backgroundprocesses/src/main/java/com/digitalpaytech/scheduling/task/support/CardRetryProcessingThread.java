package com.digitalpaytech.scheduling.task.support;

import org.apache.log4j.Logger;

import com.digitalpaytech.cardprocessing.CardProcessingManager;
import com.digitalpaytech.cardprocessing.CardProcessingMaster;
import com.digitalpaytech.cardprocessing.CardProcessingThread;
import com.digitalpaytech.dto.ProcessorInfo;
import com.digitalpaytech.scheduling.task.NightlyTransactionRetryProcessingTask;

public class CardRetryProcessingThread extends CardProcessingThread {
    private static final Logger LOGGER = Logger.getLogger(CardRetryProcessingThread.class);
    private ProcessorInfo cardProcessorType;
    private NightlyTransactionRetryProcessingTask nightlyTransactionRetryProcessor;
    
    public CardRetryProcessingThread(final int threadId, final CardProcessingManager cardProcessingManager, final CardProcessingMaster master,
            final NightlyTransactionRetryProcessingTask nightlyTransactionRetryProcessor, final ProcessorInfo cardProcessorType) {
        super(threadId, CardProcessingMaster.TYPE_CHARGE_RETRY, cardProcessingManager, master);
        this.cardProcessorType = cardProcessorType;
        this.nightlyTransactionRetryProcessor = nightlyTransactionRetryProcessor;
    }
    
    @Override
    protected final Object getNextTransactionFromQueue() {
        return this.nightlyTransactionRetryProcessor.getNextCardRetryTransactionForCardProcessor(this.cardProcessorType);
    }
    
    @Override
    protected final boolean keepRunning() {
        return this.nightlyTransactionRetryProcessor.isCardRetryProcessingRunning(this.cardProcessorType);
    }
    
    @Override
    protected final void logStart() {
        LOGGER.info(STARTING + getProcessingTypeName() + THREAD + threadId + " for " + this.cardProcessorType + " processor.");
    }
    
    @Override
    protected final void logStop() {
        LOGGER.info(STOPPING + getProcessingTypeName() + THREAD + threadId + " for " + this.cardProcessorType + " processor.");
    }
}
