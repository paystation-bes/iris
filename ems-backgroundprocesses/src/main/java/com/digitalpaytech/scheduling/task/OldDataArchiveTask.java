package com.digitalpaytech.scheduling.task;

import java.net.InetAddress;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.PosBatteryInfoService;
import com.digitalpaytech.service.PosSensorInfoService;
import com.digitalpaytech.service.ReversalService;

/**
 * This task archives old data in Reversal, POSSensorInfo, and POSBatteryInfo table.
 * 
 * @author Brian Kim
 * 
 */
@Component("oldDataArchiveTask")
public class OldDataArchiveTask {
    
    private static final Logger LOGGER = Logger.getLogger(OldDataArchiveTask.class);
    
    @Autowired
    private ClusterMemberService clusterMemberService;
    
    @Autowired
    private ReversalService reversalService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private PosSensorInfoService posSensorInfoService;
    
    @Autowired
    private PosBatteryInfoService posBatteryInfoService;
    
    /**
     * This method archives old reversal data in ReversalArchive table.
     */
    public final void archiveOldReversalData() {
        if (!isArchiveServer()) {
            // not the configured server
            return;
        }
        this.reversalService.archiveReversalData();
    }
    
    public final void archiveOldPosSensorInfoData() {
        if (!isArchiveServer()) {
            // not the configured server
            return;
        }
        
        int maxDays = 0;
        int archiveBatchSize = 0;
        try {
            maxDays = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.ARCHIVE_MAX_DAYS_FOR_POS_SENSOR_INFO_AND_POS_BATTERY_INFO,
                EmsPropertiesService.DEFAULT_ARCHIVE_MAX_DAYS_FOR_POS_SENSOR_INFO_AND_POS_BATTERY_INFO);
        } catch (Exception e) {
            LOGGER.error("+++ Failed to query max SensorLog archiving days from emsproperties table. +++ ");
            maxDays = EmsPropertiesService.DEFAULT_ARCHIVE_MAX_DAYS_FOR_POS_SENSOR_INFO_AND_POS_BATTERY_INFO;
        }
        
        try {
            archiveBatchSize = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.ARCHIVE_POS_SENSOR_INFO_BATTERY_INFO_BATCH_SIZE,
                EmsPropertiesService.DEFAULT_ARCHIVE_POS_SENSOR_INFO_BATTERY_INFO_BATCH_SIZE);
        } catch (Exception e) {
            LOGGER.error("+++ Failed to query max SensorLog archiving days from emsproperties table. +++ ");
            archiveBatchSize = EmsPropertiesService.DEFAULT_ARCHIVE_POS_SENSOR_INFO_BATTERY_INFO_BATCH_SIZE;
        }
        
        LOGGER.info("+++ Start remove " + maxDays + " days old SensorLog from database. +++ ");
        this.posSensorInfoService.archiveOldPosSensorInfoData(maxDays, archiveBatchSize, this.clusterMemberService.getClusterName());
        LOGGER.info("+++ End remove " + maxDays + " days old SensorLog from database. +++ ");
    }
    
    public final void archiveOldPosBatteryInfoData() {
        if (!isArchiveServer()) {
            // not the configured server
            return;
        }
        
        int maxDays = 0;
        int archiveBatchSize = 0;
        try {
            maxDays = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.ARCHIVE_MAX_DAYS_FOR_POS_SENSOR_INFO_AND_POS_BATTERY_INFO,
                EmsPropertiesService.DEFAULT_ARCHIVE_MAX_DAYS_FOR_POS_SENSOR_INFO_AND_POS_BATTERY_INFO);
        } catch (Exception e) {
            LOGGER.error("+++ Failed to query max BatteryInfo archiving days from emsproperties table. +++ ");
            maxDays = EmsPropertiesService.DEFAULT_ARCHIVE_MAX_DAYS_FOR_POS_SENSOR_INFO_AND_POS_BATTERY_INFO;
        }
        
        try {
            archiveBatchSize = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.ARCHIVE_POS_SENSOR_INFO_BATTERY_INFO_BATCH_SIZE,
                EmsPropertiesService.DEFAULT_ARCHIVE_POS_SENSOR_INFO_BATTERY_INFO_BATCH_SIZE);
        } catch (Exception e) {
            LOGGER.error("+++ Failed to query max SensorLog archiving days from emsproperties table. +++ ");
            archiveBatchSize = EmsPropertiesService.DEFAULT_ARCHIVE_POS_SENSOR_INFO_BATTERY_INFO_BATCH_SIZE;
        }
        
        LOGGER.info("+++ Start remove " + maxDays + " days old BatteryInfo from database. +++ ");
        this.posBatteryInfoService.archiveOldPosBatteryInfoData(maxDays, archiveBatchSize, this.clusterMemberService.getClusterName());
        LOGGER.info("+++ End remove " + maxDays + " days old BatteryInfo from database. +++ ");
    }
    
    private boolean isArchiveServer() {
        try {
            // get the cluster member name
            final String clusterName = this.clusterMemberService.getClusterName();
            if (clusterName == null || clusterName.isEmpty()) {
                throw new ApplicationException("Cluster member name is empty");
            }
            
            final String archiveServer = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.ARCHIVE_SERVER_NAME,
                EmsPropertiesService.DEFAULT_ARCHIVE_SERVER_NAME);
            
            if (clusterName.equals(archiveServer)) {
                LOGGER.info("Server " + clusterName + " configured for archiving data tasks.");
                return true;
            } else {
                LOGGER.info("Server " + clusterName + " not configured for archiving data tasks.");
                return false;
            }
        } catch (Exception e) {
            String addr = null;
            try {
                addr = InetAddress.getLocalHost().getHostAddress();
            } catch (Exception e1) {
                // shouldn't happen
            }
            final String err = "Unable to read memeber name from cluster.properties for cluster member " + addr;
            LOGGER.error(err);
            this.mailerService.sendAdminErrorAlert("Archive data", err, e);
            
            return false;
        }
    }
}
