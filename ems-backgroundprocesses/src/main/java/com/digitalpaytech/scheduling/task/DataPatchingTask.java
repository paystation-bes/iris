package com.digitalpaytech.scheduling.task;

import java.util.List;

import org.apache.cxf.common.util.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.cardprocessing.CardProcessingMaster;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.service.CardRetryTransactionService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.crypto.CryptoAlgorithmFactory;
import com.digitalpaytech.util.crypto.CryptoConstants;

@Component("dataPatchingTask")
public class DataPatchingTask {
    private static final Logger LOG = Logger.getLogger(DataPatchingTask.class);
    
    @Autowired
    private EmsPropertiesService emsProperties;
    
    @Autowired
    private CardProcessingMaster cardProcessingMaster;
    
    @Autowired
    private CardRetryTransactionService cardRetryTransactionService;
    
    @Autowired
    private CryptoService cryptoService;
    
    @Autowired
    private CryptoAlgorithmFactory cryptoAlgorithmFactory;
    
    public DataPatchingTask() {
        
    }
    
    @SuppressWarnings({ "checkstyle:illegalcatch", "checkstyle:designforextension" })
    // I suppressed illegalcatch because this is a backgroun-process that is supposed to log every exceptions.
    public void process() {
        if (!this.emsProperties.getPropertyValueAsBoolean(EmsPropertiesService.EXECUTE_PATCH, false, true)
            || !this.cardProcessingMaster.isScheduledCardProcessingServer()) {
            return;
        }
        
        final List<Object[]> emptyHashes = this.cardRetryTransactionService.findEmptyBadCardHash();
        for (Object[] eh : emptyHashes) {
            final String cardHash = (String) eh[0];
            String cardData = null;
            try {
                if (!StringUtils.isEmpty(cardHash)) {
                    cardData = this.cryptoService.decryptData((String) eh[1]);
                }
                
                if (!StringUtils.isEmpty(cardData)) {
                    final int separatorIdx = cardData.indexOf("=");
                    if (separatorIdx > 0) {
                        cardData = cardData.substring(0, separatorIdx);
                    }
                }
                
                if (StringUtils.isEmpty(cardData)) {
                    LOG.error("Failed to rehash badCardHash because there are some record(s) with empty cardHash or cardData");
                } else {
                    final String badCardHash = this.cryptoAlgorithmFactory.getSha1Hash(cardData, CryptoConstants.HASH_BAD_CREDIT_CARD);
                    this.cardRetryTransactionService.updateBadCardHash((String) eh[0], badCardHash);
                }
            } catch (CryptoException ce) {
                LOG.error("Error decrypting/hashing card: " + cardHash, ce);
            } catch (Exception e) {
                LOG.error("Unexpecting exception for card: " + cardHash, e);
            }
        }
        
        this.emsProperties.updatePatchExecution(false);
    }
}
