package com.digitalpaytech.scheduling.task.kafka.consumer;

import java.io.UnsupportedEncodingException;
import java.time.Duration;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.KafkaException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.digitalpaytech.dto.queue.QueueNotificationEmail;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.scheduling.queue.alert.NotificationEmailProcessor;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;
import com.digitalpaytech.util.kafka.MessageConsumerFactory;

@Component(value = "notificationEmailConsumerTask")
public class NotificationEmailConsumerTask extends ConsumerTask {
    private static final Logger LOG = Logger.getLogger(NotificationEmailConsumerTask.class);
    
    @Resource(name = KafkaKeyConstants.IRIS_PROPERTIES)
    private Properties irisKafka;
    
    @Resource(name = KafkaKeyConstants.TOPIC_NAMES_PROPERTIES)
    private Properties topicNames;
    
    @Autowired
    private NotificationEmailProcessor notificationEmailProcessor;
    
    @Autowired
    private MessageConsumerFactory messageConsumerFactory;
    
    private KafkaConsumer<String, String> consumer;
    
    @PostConstruct
    public void init() {
        this.consumer = super.buildConsumer(this.topicNames.getProperty(KafkaKeyConstants.ALERT_PROCESSING_NOTIFICATION_EVENTS_TOPIC_NAME_KEY),
                                            this.topicNames.getProperty(KafkaKeyConstants.ALERT_PROCESSING_NOTIFICATION_EVENTS_CONSUMER_GROUP),
                                            this.irisKafka, this.topicNames.getProperty(KafkaKeyConstants.DELIMETER));
    }
    
    @PreDestroy
    protected void close() {
        super.closeConsumer(this.consumer);
    }
    
    @Scheduled(fixedDelayString = "${consumer.millisecond.delay}")
    public void processOnSchedule() {
        process();
    }
    
    @Async("kafkaConsumerExecutor")
    public void process() {
        super.traceLog(LOG, this.topicNames, KafkaKeyConstants.ALERT_PROCESSING_NOTIFICATION_EVENTS_TOPIC_NAME_KEY,
                       KafkaKeyConstants.ALERT_PROCESSING_NOTIFICATION_EVENTS_CONSUMER_GROUP);
        
        final ConsumerRecords<String, String> consumerRecords = this.consumer.poll(Duration.ofMillis(super.getDefaultConsumerPollTimeout()));
        for (ConsumerRecord<String, String> rec : consumerRecords) {
            try {
                final QueueNotificationEmail qne =
                        this.messageConsumerFactory.deserializeMessageWithEmbeddedHeaders(rec.value(), QueueNotificationEmail.class);
                this.notificationEmailProcessor.sendNotificationEmail(qne);
                
            } catch (JsonException | UnsupportedEncodingException e) {
                LOG.error(String.format("Unable to send Notification Email in %s topic [%s]",
                                        super.getTopicName(KafkaKeyConstants.ALERT_PROCESSING_NOTIFICATION_EVENTS_TOPIC_NAME_KEY, this.topicNames),
                                        rec.value()),
                          e);
            }
        }
        try {
            this.consumer.commitSync();
        } catch (KafkaException ke) {
            LOG.error(ke);
        }
    }
    
    public final void setNotificationEmailProcessor(final NotificationEmailProcessor notificationEmailProcessor) {
        this.notificationEmailProcessor = notificationEmailProcessor;
    }
}
