package com.digitalpaytech.scheduling.emvrefund.impl;

import java.util.List;

import com.digitalpaytech.cardprocessing.CardProcessingMaster;
import com.digitalpaytech.scheduling.emvrefund.EMVRefundRetryService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.cardprocessing.cps.EMVRefundService;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.emvrefund.EMVRefund;
import com.digitalpaytech.domain.emvrefund.EMVRefundResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.hibernate.Query;

@Service("emvRefundRetryService")
@Transactional(propagation = Propagation.REQUIRED)
public class EMVRefundRetryServiceImpl implements EMVRefundRetryService {
    private static final Logger LOG = Logger.getLogger(EMVRefundRetryServiceImpl.class);
    
    @Autowired
    private EntityDao entityDao;
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    @Autowired
    private EMVRefundService emvRefundService;
    @Autowired
    private CardProcessingMaster cardProcessingMaster;
    

    @Override
    public final void refundRetry() {
        if (!this.cardProcessingMaster.isScheduledCardProcessingServer()) {
            return;
        }
        
        
        final int maxRetryCount = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.EMV_REFUND_RETRY_MAX_COUNT,
                                                                                  EmsPropertiesService.DEFAULT_EMV_REFUND_RETRY_MAX_COUNT);
        
        final int selectLimit = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.EMV_REFUND_RETRY_SELECT_LIMIT,
                                                                                EmsPropertiesService.DEFAULT_EMV_REFUND_RETRY_SELECT_LIMIT);

        
        final int sizeBefore = this.emvRefundService.count().intValue();        
        
        final Query query = this.entityDao.getNamedQuery("EMVRefund.findByEarliestLastRetryDate");
        query.setParameter("maxNumRetries", maxRetryCount).setMaxResults(selectLimit);
        final List<EMVRefund> list = query.list();        
        
        if (LOG.isDebugEnabled()) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("maxRetryCount: ").append(maxRetryCount).append(", selectLimit: ").append(selectLimit);
            bdr.append(", query result size: ").append(sizeBefore);
            LOG.debug(bdr.toString());
        }

        for (EMVRefund emvr : list) {
            final EMVRefundResponse emvRefundResponse = this.emvRefundService.callRefundsRealTime(emvr);
            deleteOrUpdate(emvr.getId(), emvRefundResponse); 
        }
    }
    
    
    private void deleteOrUpdate(final int emvRefundId, final EMVRefundResponse emvRefundResponse) {
        final EMVRefund emvr = (EMVRefund) this.entityDao.get(EMVRefund.class, emvRefundId);
        if (emvr != null && emvRefundResponse.getCode().equalsIgnoreCase(StandardConstants.STRING_STATUS_CODE_ACCEPTED)) {
            this.entityDao.delete(emvr);
            
        } else if (emvr != null) {
            emvr.setNumRetries(emvr.getNumRetries() + 1);
            emvr.setLastRetryDate(DateUtil.getCurrentGmtDate());
            emvr.setLastResponse(emvRefundResponse.getMessage());
            this.entityDao.update(emvr);
        
        } else {
            LOG.warn("EMVRefund record doesn't exist! ID: " + emvRefundId);
        }
    }
    
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    public final void setEMVRefundService(final EMVRefundService emvrefundService) {
        this.emvRefundService = emvrefundService;
    }
    public final void setEntityDao(final EntityDao entityDao) {
        this.entityDao = entityDao;
    }
    public final void setCardProcessingMaster(final CardProcessingMaster cardProcessingMaster) {
        this.cardProcessingMaster = cardProcessingMaster;
    }
}
