package com.digitalpaytech.scheduling.emvrefund;

public interface EMVRefundRetryService {
    void refundRetry();
}
