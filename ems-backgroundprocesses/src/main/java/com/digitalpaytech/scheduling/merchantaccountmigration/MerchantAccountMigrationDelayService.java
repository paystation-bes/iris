package com.digitalpaytech.scheduling.merchantaccountmigration;

public interface MerchantAccountMigrationDelayService {
    
    void processMerchantAccountMigration();
    
}
