package com.digitalpaytech.scheduling.merchantaccountmigration;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantAccountMigration;
import com.digitalpaytech.domain.MerchantAccountMigrationStatus;
import com.digitalpaytech.domain.MerchantStatusType;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MerchantAccountMigrationService;
import com.digitalpaytech.service.MerchantAccountMigrationStatusService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.util.WebCoreConstants;

@Service("merchantAccountMigrationDelayService")
@Transactional(propagation = Propagation.REQUIRED)
public class MerchantAccountMigrationDelayServiceImpl implements MerchantAccountMigrationDelayService {
    
    private static final String FAIL_UNIFIID_NULL = "Failed migration because UnifiId is null";
    private static final String FAIL_STILL_UNSETTLED_PRE_AUTH =
            "Failed migration because there are still unsettled PreAuths for this Merchant Account";
    private static final String FAIL_ACTIVE_REVERSAL = "Failed migration because there is an active Reversal for this Merchant Account";
    private static final String FAIL_UNSETTLED_POST_AUTH = "Failed migration because there is an unsettled PostAuth for this Merchant Account";
    
    private static final Logger LOG = Logger.getLogger(MerchantAccountMigrationDelayServiceImpl.class);
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    @Autowired
    private MerchantAccountMigrationService merchantAccountMigrationService;
    
    @Autowired
    private MerchantAccountMigrationStatusService merchantAccountMigrationStatusService;
    
    @Autowired
    private ClusterMemberService clusterMemberService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Override
    public void processMerchantAccountMigration() {
        
        if (isMerchantAccountMigrationServer()) {
            final Collection<MerchantAccountMigration> migrations = this.merchantAccountMigrationService.findByDelayUntilGMTPastAndPending();
            
            LOG.debug(String.format("Processing %d MerchantAccountMigrationRecords", migrations.size()));
            
            if (migrations != null && !migrations.isEmpty()) {
                migrations.forEach(m -> process(m));
            }
        }
    }
    
    private void process(final MerchantAccountMigration merchantAccountMigration) {
        final MerchantAccount ma = merchantAccountMigration.getMerchantAccount();
        
        if (this.merchantAccountMigrationService.preAuthBlockMigration(ma)) {
            rollbackMigrationProcess(merchantAccountMigration, FAIL_STILL_UNSETTLED_PRE_AUTH);
        } else if (this.merchantAccountMigrationService.postAuthBlockMigration(ma)) {
            rollbackMigrationProcess(merchantAccountMigration, FAIL_UNSETTLED_POST_AUTH);
        } else if (this.merchantAccountMigrationService.reversalsBlockMigraion(ma)) {
            rollbackMigrationProcess(merchantAccountMigration, FAIL_ACTIVE_REVERSAL);
        } else {
            
            final Integer unifiId = merchantAccountMigration.getMerchantAccount().getCustomer().getUnifiId();
            if (unifiId == null) {
                LOG.error(FAIL_UNIFIID_NULL + " Merchant Account Id: " + merchantAccountMigration.getMerchantAccount().getId());
                rollbackMigrationProcess(merchantAccountMigration, FAIL_UNIFIID_NULL);
                return;
            }
            
            try {
                final MerchantAccount migratedMerchantAccount =
                        this.merchantAccountService.migrateMerchantAccount(unifiId, merchantAccountMigration.getMerchantAccount());
                if (!compareMerchantAccounts(merchantAccountMigration.getMerchantAccount(), migratedMerchantAccount)
                    && migratedMerchantAccount.getMerchantStatusType().getId() == WebCoreConstants.MERCHANT_STATUS_TYPE_ENABLED_ID) {
                    
                    setSuccessMigrationProcess(merchantAccountMigration);
                }
            } catch (NumberFormatException | InvalidDataException exception) {
                LOG.error(exception);
                rollbackMigrationProcess(merchantAccountMigration, exception.getMessage());
            }
        }
    }
    
    private boolean compareMerchantAccounts(final MerchantAccount oldMerchantAccount, final MerchantAccount newMerchantAccount) {
        return oldMerchantAccount.getId().intValue() == newMerchantAccount.getId().intValue();
    }
    
    private void setSuccessMigrationProcess(final MerchantAccountMigration merchantAccountMigration) {
        
        merchantAccountMigration
                .setMerchantAccountMigrationStatus(this.merchantAccountMigrationStatusService.find(MerchantAccountMigrationStatus.SUCCESS));
        this.merchantAccountMigrationService.save(merchantAccountMigration);
    }
    
    private void rollbackMigrationProcess(final MerchantAccountMigration merchantAccountMigration, final String failureCause) {
        merchantAccountMigration
                .setMerchantAccountMigrationStatus(this.merchantAccountMigrationStatusService.find(MerchantAccountMigrationStatus.FAILED));
        merchantAccountMigration.setFailureCause(failureCause.getBytes());
        this.merchantAccountMigrationService.save(merchantAccountMigration);
        
        //Re-enabling Merchant Account        
        final MerchantAccount merchantAccount = this.merchantAccountService.findById(merchantAccountMigration.getMerchantAccount().getId());
        final MerchantStatusType merchantStatusType = new MerchantStatusType();
        merchantStatusType.setId(WebCoreConstants.MERCHANT_STATUS_TYPE_ENABLED_ID);
        merchantAccount.setMerchantStatusType(merchantStatusType);
        
        this.merchantAccountService.saveOrUpdateMerchantAccount(merchantAccount);
    }
    
    private boolean isMerchantAccountMigrationServer() {
        try {
            final String clusterName = this.clusterMemberService.getClusterName();
            if (StringUtils.isBlank(clusterName)) {
                throw new ApplicationException("Cluster member name is empty");
            }
            final String processServer =
                    this.emsPropertiesService.getPropertyValue(EmsPropertiesService.MERCHANT_ACCOUNT_MIGRATION_PROCESSING_SERVER);
            
            if (StringUtils.isBlank(processServer)) {
                throw new ApplicationException("Could not get server name from EMSProperties to process merchant account migration");
            }
            
            if (clusterName.equals(processServer)) {
                LOG.info(clusterName + " configured for merchant account migration");
                return true;
            } else {
                LOG.info(clusterName + " not configured for merchant account migration");
                return false;
            }
        } catch (ApplicationException e) {
            try {
                LOG.error("Unable to read memeber name from cluster.properties for cluster member " + InetAddress.getLocalHost().getHostAddress(), e);
            } catch (UnknownHostException uhe) {
                LOG.error("UnExpected UnknownHost! This shouldn't happened", uhe);
            }
            
            return false;
        }
    }
}
