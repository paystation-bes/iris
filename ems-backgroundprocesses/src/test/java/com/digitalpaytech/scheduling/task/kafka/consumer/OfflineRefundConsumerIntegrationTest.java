package com.digitalpaytech.scheduling.task.kafka.consumer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.services.CardRetryTransactionTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.ProcessorTransactionServiceMockImpl;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.data.TransactionData;
import com.digitalpaytech.domain.CPSData;
import com.digitalpaytech.domain.CPSProcessData;
import com.digitalpaytech.domain.CPSRefundData;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerStatusType;
import com.digitalpaytech.domain.ExtensiblePermit;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantStatusType;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.PaymentType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.ProcessorTransactionType;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.SmsAlert;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.cps.CPSResponse;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.service.CardRetryTransactionTypeService;
import com.digitalpaytech.service.ProcessorTransactionTypeService;
import com.digitalpaytech.service.cps.impl.CPSDataServiceImpl;
import com.digitalpaytech.service.cps.impl.CPSProcessDataServiceImpl;
import com.digitalpaytech.service.cps.impl.CPSRefundDataServiceImpl;
import com.digitalpaytech.service.impl.CardTypeServiceImpl;
import com.digitalpaytech.service.impl.CreditCardTypeServiceImpl;
import com.digitalpaytech.service.impl.CustomerServiceImpl;
import com.digitalpaytech.service.impl.EmsPropertiesServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.PaymentCardServiceImpl;
import com.digitalpaytech.service.impl.PaystationSettingServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.ProcessorServiceImpl;
import com.digitalpaytech.service.impl.ProcessorTransactionServiceImpl;
import com.digitalpaytech.service.impl.PurchaseServiceImpl;
import com.digitalpaytech.service.impl.ThirdPartyServiceAccountServiceImpl;
import com.digitalpaytech.service.impl.TransactionServiceImpl;
import com.digitalpaytech.service.paystation.impl.PosServiceStateServiceImpl;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.util.json.JSONConfigurationBean;
import com.digitalpaytech.util.kafka.impl.MessageConsumerFactoryImpl;
import com.digitalpaytech.util.kafka.impl.MessageHeadersServiceImpl;

public class OfflineRefundConsumerIntegrationTest {
    
    private static final String EXPIRY = "2012";
    private static final short LAST_4 = (short) 8965;
    private static final String PROC_TRANS_ID = "1254554";
    private static final String REFERENCE_NUMBER = "00021454754";
    private static final int AMOUNT = 100;
    private static final String AUTH_NUMBER = "0512";
    private static final String CARD_TYPE = "VISA";
    private static final String SERIAL_NUMBER = "500000050010";
    private static final String TERMINAL_TOKEN = UUID.randomUUID().toString();
    private static final String CHARGE_TOKEN = UUID.randomUUID().toString();
    private static final String REFUND_ID = UUID.randomUUID().toString();
    private static final String TOPIC = "snf_refund_results";
    
    private EntityDao entityDao = new EntityDaoTest();
    
    private final JSON json = new JSON(new JSONConfigurationBean(true, false, false));
    
    private Customer customer;
    
    private final TestContext ctx = TestContext.getInstance();
    
    @Mock
    private KafkaConsumer<String, String> mockConsumer;
    
    @Spy
    private MessageConsumerFactoryImpl messageConsumerFactory;
    
    @Mock
    private MessageHelper messageHelper;
    
    @InjectMocks
    private EmsPropertiesServiceImpl emsPropertiesService;
    
    @InjectMocks
    private PaystationSettingServiceImpl paystationSettingService;
    
    @InjectMocks
    private CustomerServiceImpl customerService;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    private PosServiceStateServiceImpl posServiceStateService;
    
    @InjectMocks
    private MerchantAccountUpdateTask merchantAccountUpdateTask;
    
    @InjectMocks
    private CPSProcessDataConsumerTask cpsProcessDataConsumerTask;
    
    @InjectMocks
    private MessageHeadersServiceImpl messageHeadersService;
    
    @InjectMocks
    private MerchantAccountServiceImpl merchantAccountService;
    
    @InjectMocks
    private ProcessorServiceImpl processorService;
    
    @InjectMocks
    private OfflineRefundConsumerTask offlineRefundConsumerTask;
    
    @InjectMocks
    private CPSDataServiceImpl cpsDataService;
    
    @Mock
    private ProcessorTransactionTypeService processorTransactionTypeService;
    
    @InjectMocks
    private CPSRefundDataServiceImpl cpsRefundDataService;
    
    @InjectMocks
    private ProcessorTransactionServiceImpl processorTransactionService;
    
    @InjectMocks
    private CPSProcessDataServiceImpl cpsProcessDataServiceImpl;
    
    @InjectMocks
    private PaymentCardServiceImpl paymentCardService;
    
    @InjectMocks
    private PurchaseServiceImpl purchaseService;
    
    @InjectMocks
    private CreditCardTypeServiceImpl creditCardTypeService;
    
    @InjectMocks
    private CardTypeServiceImpl cardTypeService;
    
    @InjectMocks
    private EntityServiceImpl entityService;
    
    @InjectMocks
    private TransactionServiceImpl transactionService;
    
    @InjectMocks
    private ThirdPartyServiceAccountServiceImpl thirdPartyServiceAccountService;
    
    @Mock
    private CardRetryTransactionTypeService cardRetryTransactionTypeService;
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.ctx.autowiresFromTestObject(this);
        this.messageConsumerFactory.setJSON(this.json);
        
        MockitoAnnotations.initMocks(this);
        
        Mockito.doReturn(this.mockConsumer).when(this.messageConsumerFactory).buildConsumer(Mockito.any(), Mockito.anyCollection(),
                                                                                            Mockito.anyString());
        
        Mockito.when(this.messageHelper.getMessageWithDefault(Mockito.anyString(), Mockito.anyString())).thenAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                return invocation.getArgumentAt(1, String.class);
            }
            
        });
        
        Mockito.when(this.processorTransactionTypeService.findProcessorTransactionType(Mockito.anyInt()))
                .thenAnswer(ProcessorTransactionServiceMockImpl.getProcessorTransactionType());
        
        Mockito.when(this.cardRetryTransactionTypeService.findCardRetryTransactionType(Mockito.anyInt()))
                .thenAnswer(CardRetryTransactionTypeServiceMockImpl.findCardRetryTransactionType());
        
        this.customer = new Customer();
        this.customer.setName("Test Customer");
        this.customer.setId(1);
        this.customer.setIsMigrated(true);
        this.customer.setUnifiId(1);
        this.customer.setCustomerStatusType(new CustomerStatusType(WebCoreConstants.CUSTOMER_STATUS_TYPE_ENABLED,
                WebCoreConstants.CUSTOMER_STATUS_TYPE_ENABLED_LABEL, new Date(), 1));
        this.entityDao.save(this.customer);
        
        final UserAccount userAccount = new UserAccount();
        userAccount.setId(1);
        userAccount.setCustomer(this.customer);
        
    }
    
    @After
    public void tearDown() throws Exception {
        this.ctx.getDatabase().clear();
    }
    
    @Test
    public void testRefundSnfPs() throws Exception {
        
        buildMerchantAccount(TERMINAL_TOKEN);
        
        final PointOfSale pos = buildPos();
        
        final ProcessorTransaction procTrans =
                setUpProcessorTransaction(pos, true, CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS, AUTH_NUMBER);
        
        final CPSRefundData data = new CPSRefundData(null, REFUND_ID, AMOUNT, EXPIRY, CHARGE_TOKEN, String.valueOf(LAST_4), AUTH_NUMBER,
                REFERENCE_NUMBER, PROC_TRANS_ID, new Date(), TERMINAL_TOKEN);
        
        refund(data);
        
        final CPSRefundData cpsRefundData = this.cpsRefundDataService.findByChargeTokenAndRefundToken(CHARGE_TOKEN, REFUND_ID);
        assertNotNull(cpsRefundData);
        
        snf(pos);
        
        ps(pos, procTrans);
        
        final ProcessorTransaction refundRes = this.processorTransactionService
                .findRefundProcessorTransaction(pos.getId(), procTrans.getPurchasedDate(), procTrans.getTicketNumber());
        assertEquals(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_REFUND, refundRes.getProcessorTransactionType().getId());
        
        final CPSRefundData cpsRefundDataComplete = this.cpsRefundDataService.findByChargeTokenAndRefundToken(CHARGE_TOKEN, REFUND_ID);
        assertNull(cpsRefundDataComplete);
    }
    
    @Test
    public void testRefundPsSnf() throws Exception {
        
        buildMerchantAccount(TERMINAL_TOKEN);
        
        final PointOfSale pos = buildPos();
        
        final ProcessorTransaction procTrans =
                setUpProcessorTransaction(pos, true, CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS, AUTH_NUMBER);
        
        final CPSRefundData data = new CPSRefundData(null, REFUND_ID, AMOUNT, EXPIRY, CHARGE_TOKEN, String.valueOf(LAST_4), AUTH_NUMBER,
                REFERENCE_NUMBER, PROC_TRANS_ID, new Date(), TERMINAL_TOKEN);
        
        refund(data);
        
        final CPSRefundData cpsRefundData = this.cpsRefundDataService.findByChargeTokenAndRefundToken(CHARGE_TOKEN, REFUND_ID);
        assertNotNull(cpsRefundData);
        
        ps(pos, procTrans);
        
        snf(pos);
        
        final ProcessorTransaction refundRes = this.processorTransactionService
                .findRefundProcessorTransaction(pos.getId(), procTrans.getPurchasedDate(), procTrans.getTicketNumber());
        assertEquals(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_REFUND, refundRes.getProcessorTransactionType().getId());
        
        final CPSRefundData cpsRefundDataComplete = this.cpsRefundDataService.findByChargeTokenAndRefundToken(CHARGE_TOKEN, REFUND_ID);
        assertNull(cpsRefundDataComplete);
    }
    
    @Test
    public void testPsRefundSnf() throws Exception {
        
        buildMerchantAccount(TERMINAL_TOKEN);
        
        final PointOfSale pos = buildPos();
        
        final ProcessorTransaction procTrans =
                setUpProcessorTransaction(pos, true, CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS, AUTH_NUMBER);
        
        ps(pos, procTrans);
        
        final CPSRefundData data = new CPSRefundData(null, REFUND_ID, AMOUNT, EXPIRY, CHARGE_TOKEN, String.valueOf(LAST_4), AUTH_NUMBER,
                REFERENCE_NUMBER, PROC_TRANS_ID, new Date(), TERMINAL_TOKEN);
        
        refund(data);
        
        final CPSRefundData cpsRefundData = this.cpsRefundDataService.findByChargeTokenAndRefundToken(CHARGE_TOKEN, REFUND_ID);
        assertNotNull(cpsRefundData);
        
        snf(pos);
        
        final ProcessorTransaction refundRes = this.processorTransactionService
                .findRefundProcessorTransaction(pos.getId(), procTrans.getPurchasedDate(), procTrans.getTicketNumber());
        assertEquals(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_REFUND, refundRes.getProcessorTransactionType().getId());
        
        final CPSRefundData cpsRefundDataComplete = this.cpsRefundDataService.findByChargeTokenAndRefundToken(CHARGE_TOKEN, REFUND_ID);
        assertNull(cpsRefundDataComplete);
    }
    
    @Test
    public void testSnfRefundPs() throws Exception {
        
        buildMerchantAccount(TERMINAL_TOKEN);
        
        final PointOfSale pos = buildPos();
        
        final ProcessorTransaction procTrans =
                setUpProcessorTransaction(pos, true, CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS, AUTH_NUMBER);
        
        snf(pos);
        
        final CPSRefundData data = new CPSRefundData(null, REFUND_ID, AMOUNT, EXPIRY, CHARGE_TOKEN, String.valueOf(LAST_4), AUTH_NUMBER,
                REFERENCE_NUMBER, PROC_TRANS_ID, new Date(), TERMINAL_TOKEN);
        
        refund(data);
        
        final CPSRefundData cpsRefundData = this.cpsRefundDataService.findByChargeTokenAndRefundToken(CHARGE_TOKEN, REFUND_ID);
        assertNotNull(cpsRefundData);
        
        ps(pos, procTrans);
        
        final ProcessorTransaction refundRes = this.processorTransactionService
                .findRefundProcessorTransaction(pos.getId(), procTrans.getPurchasedDate(), procTrans.getTicketNumber());
        assertEquals(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_REFUND, refundRes.getProcessorTransactionType().getId());
        
        final CPSRefundData cpsRefundDataComplete = this.cpsRefundDataService.findByChargeTokenAndRefundToken(CHARGE_TOKEN, REFUND_ID);
        assertNull(cpsRefundDataComplete);
    }
    
    @Test
    public void testPsSNFRefund() throws Exception {
        
        final MerchantAccount merchantAccount = buildMerchantAccount(TERMINAL_TOKEN);
        
        final PointOfSale pos = buildPos();
        
        final ProcessorTransaction procTrans =
                setUpProcessorTransaction(pos, true, CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS, AUTH_NUMBER);
        
        procTrans.setMerchantAccount(merchantAccount);
        
        final CPSData cpsData = new CPSData(procTrans, CHARGE_TOKEN);
        this.entityDao.save(cpsData);
        
        final CPSRefundData data = new CPSRefundData(null, REFUND_ID, AMOUNT, EXPIRY, CHARGE_TOKEN, String.valueOf(LAST_4), AUTH_NUMBER,
                REFERENCE_NUMBER, PROC_TRANS_ID, new Date(), TERMINAL_TOKEN);
        
        refund(data);
        
        final CPSRefundData cpsRefundData = this.cpsRefundDataService.findByChargeTokenAndRefundToken(CHARGE_TOKEN, REFUND_ID);
        assertNull(cpsRefundData);
        
        final ProcessorTransaction result = this.processorTransactionService.findProcessorTransactionById(procTrans.getId(), false);
        assertNotNull(result);
        assertEquals(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_REFUNDS, result.getProcessorTransactionType().getId());
        
        final ProcessorTransaction refundRes = this.processorTransactionService
                .findRefundProcessorTransaction(pos.getId(), procTrans.getPurchasedDate(), procTrans.getTicketNumber());
        assertEquals(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_REFUND, refundRes.getProcessorTransactionType().getId());
    }
    
    @Test
    public void testSnFNotComplete() throws Exception {
        
        buildMerchantAccount(TERMINAL_TOKEN);
        final PointOfSale pos = buildPos();
        
        final ProcessorTransaction procTrans =
                setUpProcessorTransaction(pos, false, CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_NO_REFUNDS, "");
        
        final CPSData cpsData = new CPSData(procTrans, CHARGE_TOKEN);
        this.entityDao.save(cpsData);
        
        final CPSRefundData data = new CPSRefundData(null, REFUND_ID, AMOUNT, EXPIRY, CHARGE_TOKEN, String.valueOf(LAST_4), AUTH_NUMBER,
                REFERENCE_NUMBER, PROC_TRANS_ID, new Date(), TERMINAL_TOKEN);
        
        refund(data);
        
        final CPSRefundData cpsRefundData = this.cpsRefundDataService.findByChargeTokenAndRefundToken(CHARGE_TOKEN, REFUND_ID);
        assertNotNull(cpsRefundData);
        
        final ProcessorTransaction result = this.processorTransactionService.findProcessorTransactionById(procTrans.getId(), false);
        assertNotNull(result);
        assertEquals(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_NO_REFUNDS, result.getProcessorTransactionType().getId());
        
        final ProcessorTransaction refundRes = this.processorTransactionService
                .findRefundProcessorTransaction(pos.getId(), procTrans.getPurchasedDate(), procTrans.getTicketNumber());
        
        assertNull(refundRes);
        
    }
    
    @Test
    public void testExpired() throws Exception {
        
        buildMerchantAccount(TERMINAL_TOKEN);
        final PointOfSale pos = buildPos();
        final ProcessorTransaction procTrans =
                setUpProcessorTransaction(pos, true, CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS, AUTH_NUMBER);
        
        final CPSData cpsData = new CPSData(procTrans, CHARGE_TOKEN);
        this.entityDao.save(cpsData);
        
        final CPSRefundData data = new CPSRefundData(null, REFUND_ID, AMOUNT, EXPIRY, CHARGE_TOKEN, String.valueOf(LAST_4), null, null, null,
                new Date(), TERMINAL_TOKEN);
        
        refund(data);
        
        final CPSRefundData cpsRefundData = this.cpsRefundDataService.findByChargeTokenAndRefundToken(CHARGE_TOKEN, REFUND_ID);
        assertNull(cpsRefundData);
        
        final ProcessorTransaction result = this.processorTransactionService.findProcessorTransactionById(procTrans.getId(), false);
        assertNotNull(result);
        assertEquals(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_REFUNDS, result.getProcessorTransactionType().getId());
        
        final List<ProcessorTransaction> procTransList = this.entityDao.loadWithLimit(ProcessorTransaction.class, 2, true);
        
        final List<ProcessorTransaction> refundRes = procTransList.stream()
                .filter(r -> r.getProcessorTransactionType().getId() == CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_EXPIRED_REFUND)
                .collect(Collectors.toList());
        assertEquals(1, refundRes.size());
        assertFalse(refundRes.get(0).isIsApproved());
        
    }
    
    @Test
    public void duplicateRecordConsumed() throws Exception {
        
        buildMerchantAccount(TERMINAL_TOKEN);
        
        final PointOfSale pos = buildPos();
        
        final ProcessorTransaction procTrans =
                setUpProcessorTransaction(pos, true, CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS, AUTH_NUMBER);
        
        final CPSData cpsData = new CPSData(procTrans, CHARGE_TOKEN);
        this.entityDao.save(cpsData);
        
        final List<CPSRefundData> data = new ArrayList<>();
        
        final CPSRefundData d1 = new CPSRefundData(null, REFUND_ID, AMOUNT, EXPIRY, CHARGE_TOKEN, String.valueOf(LAST_4), AUTH_NUMBER,
                REFERENCE_NUMBER, PROC_TRANS_ID, new Date(), TERMINAL_TOKEN);
        
        final CPSRefundData d2 = new CPSRefundData(null, REFUND_ID, AMOUNT, EXPIRY, CHARGE_TOKEN, String.valueOf(LAST_4), AUTH_NUMBER,
                REFERENCE_NUMBER, PROC_TRANS_ID, new Date(), TERMINAL_TOKEN);
        
        data.add(d1);
        data.add(d2);
        
        processRecords(data);
        
        final CPSRefundData cpsRefundData = this.cpsRefundDataService.findByChargeTokenAndRefundToken(CHARGE_TOKEN, REFUND_ID);
        assertNull(cpsRefundData);
    }
    
    @Test
    public void merchantAccountDNE() throws Exception {
        final CPSRefundData data = new CPSRefundData(null, REFUND_ID, AMOUNT, EXPIRY, CHARGE_TOKEN, String.valueOf(LAST_4), AUTH_NUMBER,
                REFERENCE_NUMBER, PROC_TRANS_ID, new Date(), TERMINAL_TOKEN);
        
        refund(data);
        
        final CPSRefundData cpsRefundData = this.cpsRefundDataService.findByChargeTokenAndRefundToken(CHARGE_TOKEN, REFUND_ID);
        assertNull(cpsRefundData);
    }
    
    private MerchantAccount buildMerchantAccount(final String terminalToken) {
        final MerchantAccount merchantAccount = new MerchantAccount();
        merchantAccount.setIsLink(true);
        merchantAccount.setIsValidated(true);
        merchantAccount.setMerchantStatusType(new MerchantStatusType(1, "Enabled", new Date(), 1));
        merchantAccount.setCustomer(this.customer);
        merchantAccount.setTerminalToken(TERMINAL_TOKEN);
        final Processor processor = new Processor();
        processor.setId(1);
        merchantAccount.setProcessor(processor);
        this.entityDao.save(merchantAccount);
        return merchantAccount;
    }
    
    private void refund(final CPSRefundData data) throws JsonException {
        processRecords(Arrays.asList(data));
        
    }
    
    private void processRecords(final List<CPSRefundData> data) throws JsonException {
        
        final List<ConsumerRecord<String, String>> recList = new ArrayList<>();
        
        for (int i = 0; i < data.size(); i++) {
            recList.add(new ConsumerRecord<String, String>(TOPIC, i, i, data.get(i).getChargeToken(), this.json.serialize(data.get(i))));
        }
        
        @SuppressWarnings("PMD.UseConcurrentHashMap")
        final Map<TopicPartition, List<ConsumerRecord<String, String>>> recordMap = new HashMap<>();
        final TopicPartition topicPartition = new TopicPartition(TOPIC, 0);
        
        recordMap.put(topicPartition, recList);
        
        final ConsumerRecords<String, String> records = new ConsumerRecords<String, String>(recordMap);
        Mockito.when(this.mockConsumer.poll(Mockito.any(Duration.class))).thenReturn(records);
        
        this.offlineRefundConsumerTask.process();
    }
    
    private void processSNFRecords(final List<CPSProcessData> data) throws JsonException {
        
        final List<ConsumerRecord<String, String>> recList = new ArrayList<>();
        
        for (int i = 0; i < data.size(); i++) {
            recList.add(new ConsumerRecord<String, String>(TOPIC, i, i, data.get(i).getChargeToken(), this.json.serialize(data.get(i))));
        }
        
        @SuppressWarnings("PMD.UseConcurrentHashMap")
        final Map<TopicPartition, List<ConsumerRecord<String, String>>> recordMap = new HashMap<>();
        final TopicPartition topicPartition = new TopicPartition(TOPIC, 0);
        
        recordMap.put(topicPartition, recList);
        
        final ConsumerRecords<String, String> records = new ConsumerRecords<String, String>(recordMap);
        Mockito.when(this.mockConsumer.poll(Mockito.any(Duration.class))).thenReturn(records);
        
        this.cpsProcessDataConsumerTask.process();
    }
    
    private ProcessorTransaction setUpProcessorTransaction(final PointOfSale pos, final boolean approved, final int processorTransactionType,
        final String authNumber) {
        
        final ProcessorTransaction procTrans = new ProcessorTransaction();
        procTrans.setPointOfSale(pos);
        procTrans.setId(1L);
        procTrans.setAmount(AMOUNT);
        procTrans.setIsApproved(approved);
        procTrans.setAuthorizationNumber(authNumber);
        procTrans.setLast4digitsOfCardNumber(LAST_4);
        procTrans.setCardType(CARD_TYPE);
        procTrans.setReferenceNumber(REFERENCE_NUMBER);
        procTrans.setAmount(AMOUNT);
        procTrans.setPurchasedDate(new Date());
        procTrans.setProcessorTransactionType(new ProcessorTransactionType(processorTransactionType));
        
        final Purchase purchase = new Purchase();
        purchase.setId(1L);
        final PaymentType paymentType = new PaymentType();
        paymentType.setId((byte) WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_SWIPE);
        purchase.setPaymentType(paymentType);
        purchase.setCardPaidAmount(AMOUNT);
        procTrans.setPurchase(purchase);
        
        this.entityDao.save(purchase);
        
        this.entityDao.save(procTrans);
        
        final PaymentCard paymentCard = new PaymentCard();
        paymentCard.setPurchase(purchase);
        paymentCard.setProcessorTransaction(procTrans);
        this.entityDao.save(paymentCard);
        
        return procTrans;
    }
    
    private PointOfSale buildPos() {
        final PointOfSale pos = new PointOfSale();
        pos.setCustomer(this.customer);
        pos.setSerialNumber(SERIAL_NUMBER);
        pos.setId(1);
        
        final PosServiceState pss = new PosServiceState();
        this.entityDao.save(pos);
        
        pss.setPointOfSale(pos);
        pss.setPointOfSaleId(pos.getId());
        this.entityDao.save(pss);
        
        pos.setPosServiceState(pss);
        this.entityDao.update(pos);
        return pos;
    }
    
    private void snf(final PointOfSale pos) throws JsonException {
        final CPSProcessData snf = new CPSProcessData();
        snf.setAmount(AMOUNT);
        snf.setChargeToken(CHARGE_TOKEN);
        snf.setAuthorizationNumber(AUTH_NUMBER);
        snf.setLast4Digits(LAST_4);
        snf.setProcessedGMT(new Date());
        snf.setReferenceNumber(REFERENCE_NUMBER);
        snf.setProcessorTransactionId(PROC_TRANS_ID);
        snf.setSerialNumber(pos.getSerialNumber());
        snf.setTerminalToken(TERMINAL_TOKEN);
        snf.setCardType(CARD_TYPE);
        
        processSNFRecords(Arrays.asList(snf));
    }
    
    private void ps(final PointOfSale pos, final ProcessorTransaction procTrans) throws CryptoException {
        final CPSResponse embeddedTxObject = new CPSResponse();
        embeddedTxObject.setTerminalToken(TERMINAL_TOKEN);
        embeddedTxObject.setChargeToken(CHARGE_TOKEN);
        
        final TransactionData transactionData =
                new TransactionData(pos, procTrans.getPurchase(), procTrans, new SmsAlert(), new ExtensiblePermit(), new PosServiceState(), "0");
        transactionData.setCancelledTransaction(true);
        transactionData.setCoreCPS(true);
        this.transactionService.processTransaction(transactionData, false, null, embeddedTxObject);
    }
}
