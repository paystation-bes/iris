package com.digitalpaytech.scheduling.task.kafka.consumer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.services.ProcessorTransactionServiceMockImpl;
import com.digitalpaytech.bdd.util.DBHelper;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.CPSData;
import com.digitalpaytech.domain.CPSProcessData;
import com.digitalpaytech.domain.CPSRefundData;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantStatusType;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.ProcessorTransactionType;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.service.ProcessorTransactionTypeService;
import com.digitalpaytech.service.cps.impl.CPSDataServiceImpl;
import com.digitalpaytech.service.cps.impl.CPSProcessDataServiceImpl;
import com.digitalpaytech.service.cps.impl.CPSRefundDataServiceImpl;
import com.digitalpaytech.service.impl.CardTypeServiceImpl;
import com.digitalpaytech.service.impl.CreditCardTypeServiceImpl;
import com.digitalpaytech.service.impl.CustomerServiceImpl;
import com.digitalpaytech.service.impl.EmsPropertiesServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.PaymentCardServiceImpl;
import com.digitalpaytech.service.impl.PaystationSettingServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.ProcessorServiceImpl;
import com.digitalpaytech.service.impl.ProcessorTransactionServiceImpl;
import com.digitalpaytech.service.impl.PurchaseServiceImpl;
import com.digitalpaytech.service.paystation.impl.PosServiceStateServiceImpl;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.util.json.JSONConfigurationBean;
import com.digitalpaytech.util.kafka.impl.MessageConsumerFactoryImpl;
import com.digitalpaytech.util.kafka.impl.MessageHeadersServiceImpl;

public class CPSProcessDataConsumerIntegrationTest {
    
    private static final short LAST_4 = (short) 8965;
    private static final String PROC_TRANS_ID = "1254554";
    private static final String REFERENCE_NUMBER = "00021454754";
    private static final int AMOUNT = 100;
    private static final String AUTH_NUMBER = "0512";
    private static final String CARD_TYPE = "VISA";
    private static final String SERIAL_NUMBER = "500000050010";
    private static final String TERMINAL_TOKEN = UUID.randomUUID().toString();
    private static final String CHARGE_TOKEN = UUID.randomUUID().toString();
    private static final String TOPIC = "snf_charge_results";
    
    private EntityDao entityDao = new EntityDaoTest();
    
    private final JSON json = new JSON(new JSONConfigurationBean(true, false, false));
    
    private Customer customer;
    
    private final TestContext ctx = TestContext.getInstance();
    
    @Mock
    private KafkaConsumer<String, String> mockConsumer;
    
    @Spy
    private MessageConsumerFactoryImpl messageConsumerFactory;
    
    @Mock
    private MessageHelper messageHelper;
    
    @InjectMocks
    private EmsPropertiesServiceImpl emsPropertiesService;
    
    @InjectMocks
    private PaystationSettingServiceImpl paystationSettingService;
    
    @InjectMocks
    private CustomerServiceImpl customerService;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    private PosServiceStateServiceImpl posServiceStateService;
    
    @InjectMocks
    private MerchantAccountUpdateTask merchantAccountUpdateTask;
    
    @InjectMocks
    private MessageHeadersServiceImpl messageHeadersService;
    
    @InjectMocks
    private MerchantAccountServiceImpl merchantAccountService;
    
    @InjectMocks
    private ProcessorServiceImpl processorService;
    
    @InjectMocks
    private CPSProcessDataConsumerTask cpsProcessDataConsumerTask;
    
    @InjectMocks
    private CPSDataServiceImpl cpsDataService;
    
    @Mock
    private ProcessorTransactionTypeService processorTransactionTypeService;
    
    @InjectMocks
    private CPSRefundDataServiceImpl cpsRefundDataService;
    
    @InjectMocks
    private ProcessorTransactionServiceImpl processorTransactionService;
    
    @InjectMocks
    private CPSProcessDataServiceImpl cpsProcessDataServiceImpl;
    
    @InjectMocks
    private PaymentCardServiceImpl paymentCardService;
    
    @InjectMocks
    private PurchaseServiceImpl purchaseService;
    
    @InjectMocks
    private CreditCardTypeServiceImpl creditCardTypeService;
    
    @InjectMocks
    private CardTypeServiceImpl cardTypeService;
    
    @InjectMocks
    private EntityServiceImpl entityService;
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.ctx.autowiresFromTestObject(this);
        this.messageConsumerFactory.setJSON(this.json);
        
        MockitoAnnotations.initMocks(this);
        
        Mockito.doReturn(this.mockConsumer).when(this.messageConsumerFactory).buildConsumer(Mockito.any(), Mockito.anyCollection(),
                                                                                            Mockito.anyString());
        
        Mockito.when(this.messageHelper.getMessageWithDefault(Mockito.anyString(), Mockito.anyString())).thenAnswer(new Answer() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                return invocation.getArgumentAt(1, String.class);
            }
            
        });
        
        Mockito.when(this.processorTransactionTypeService.findProcessorTransactionType(Mockito.anyInt()))
                .thenAnswer(ProcessorTransactionServiceMockImpl.getProcessorTransactionType());
        
        this.customer = new Customer();
        this.customer.setName("Test Customer");
        this.customer.setId(1);
        this.customer.setIsMigrated(true);
        this.customer.setUnifiId(1);
        this.entityDao.save(this.customer);
        
        final UserAccount userAccount = new UserAccount();
        userAccount.setId(1);
        userAccount.setCustomer(this.customer);
        
    }
    
    @After
    public void tearDown() throws Exception {
        this.ctx.getDatabase().clear();
    }
    
    @Test
    public void testCPSArrivesFirst() throws Exception {
        
        buildMerchantAccount(TERMINAL_TOKEN);
        
        final CPSProcessData data = new CPSProcessData(SERIAL_NUMBER, TERMINAL_TOKEN, CHARGE_TOKEN, AMOUNT, AUTH_NUMBER, CARD_TYPE, LAST_4,
                REFERENCE_NUMBER, PROC_TRANS_ID, new Date(), new Date());
        
        processRecords(data);
        
        final CPSProcessData cpsProcessData = this.cpsProcessDataServiceImpl.find(TERMINAL_TOKEN, CHARGE_TOKEN);
        assertNotNull(cpsProcessData);
    }
    
    @Test
    public void testCPSArrivesSecond() throws Exception {
        
        buildMerchantAccount(TERMINAL_TOKEN);
        
        final ProcessorTransaction procTrans = new ProcessorTransaction();
        procTrans.setId(1L);
        procTrans.setAmount(AMOUNT);
        this.entityDao.save(procTrans);
        
        final CPSData cpsData = new CPSData(procTrans, CHARGE_TOKEN);
        this.entityDao.save(cpsData);
        
        final CPSProcessData data = new CPSProcessData(SERIAL_NUMBER, TERMINAL_TOKEN, CHARGE_TOKEN, AMOUNT, AUTH_NUMBER, CARD_TYPE, LAST_4,
                REFERENCE_NUMBER, PROC_TRANS_ID, new Date(), new Date());
        
        processRecords(data);
        
        final CPSProcessData cpsProcessData = this.cpsProcessDataServiceImpl.find(TERMINAL_TOKEN, CHARGE_TOKEN);
        assertNull(cpsProcessData);
        
        final ProcessorTransaction result = this.processorTransactionService.findProcessorTransactionById(procTrans.getId(), false);
        assertNotNull(result);
        
        assertTrue(result.isIsApproved());
        assertEquals(AUTH_NUMBER, result.getAuthorizationNumber());
        assertEquals(LAST_4, result.getLast4digitsOfCardNumber());
        assertEquals(PROC_TRANS_ID, result.getProcessorTransactionId());
        assertEquals(CARD_TYPE, result.getCardType());
        assertEquals(REFERENCE_NUMBER, result.getReferenceNumber());
        assertEquals(AMOUNT, result.getAmount());
    }
    
    @Test
    public void testRefundWaiting() throws Exception {
        
        final CPSRefundData cpsRefund = new CPSRefundData(1, UUID.randomUUID().toString(), AMOUNT, "2012", CHARGE_TOKEN, "LAST_4", AUTH_NUMBER,
                REFERENCE_NUMBER, PROC_TRANS_ID, new Date(), TERMINAL_TOKEN);
        
        this.entityDao.save(cpsRefund);
        
        buildMerchantAccount(TERMINAL_TOKEN);
        
        final PointOfSale pos = new PointOfSale();
        pos.setCustomer(this.customer);
        pos.setSerialNumber(SERIAL_NUMBER);
        pos.setId(1);
        
        this.entityDao.save(pos);
        
        final ProcessorTransaction procTrans = new ProcessorTransaction();
        procTrans.setId(1L);
        procTrans.setAmount(AMOUNT);
        procTrans.setTicketNumber(1);
        procTrans.setPointOfSale(pos);
        procTrans.setPurchasedDate(new Date());
        procTrans.setProcessorTransactionType(new ProcessorTransactionType(
                CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_NO_REFUNDS));
        
        final Purchase purchase = new Purchase();
        purchase.setId(1L);
        procTrans.setPurchase(purchase);
        this.entityDao.save(purchase);
        
        this.entityDao.save(procTrans);
        
        final CPSData cpsData = new CPSData(procTrans, CHARGE_TOKEN);
        this.entityDao.save(cpsData);
        
        final CPSProcessData data = new CPSProcessData(SERIAL_NUMBER, TERMINAL_TOKEN, CHARGE_TOKEN, AMOUNT, AUTH_NUMBER, CARD_TYPE, LAST_4,
                REFERENCE_NUMBER, PROC_TRANS_ID, new Date(), new Date());
        
        processRecords(data);
        
        final CPSProcessData cpsProcessData = this.cpsProcessDataServiceImpl.find(TERMINAL_TOKEN, CHARGE_TOKEN);
        assertNull(cpsProcessData);
        
        final ProcessorTransaction result = this.processorTransactionService.findProcessorTransactionById(procTrans.getId(), false);
        assertNotNull(result);
        
        assertTrue(result.isIsApproved());
        assertEquals(AUTH_NUMBER, result.getAuthorizationNumber());
        assertEquals(LAST_4, result.getLast4digitsOfCardNumber());
        assertEquals(PROC_TRANS_ID, result.getProcessorTransactionId());
        assertEquals(CARD_TYPE, result.getCardType());
        assertEquals(REFERENCE_NUMBER, result.getReferenceNumber());
        assertEquals(AMOUNT, result.getAmount());
        assertEquals(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_REFUNDS, result.getProcessorTransactionType().getId());
        
        final ProcessorTransaction refundRes = this.processorTransactionService
                .findRefundProcessorTransaction(pos.getId(), procTrans.getPurchasedDate(), procTrans.getTicketNumber());
        assertNotNull(refundRes);
        assertEquals(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_REFUND, refundRes.getProcessorTransactionType().getId());
    }
    
    @Test
    public void testTwoRefundWaiting() throws Exception {
        
        final CPSRefundData cpsRefund1 = new CPSRefundData(1, UUID.randomUUID().toString(), AMOUNT, "2012", CHARGE_TOKEN, "LAST_4", AUTH_NUMBER,
                REFERENCE_NUMBER, PROC_TRANS_ID, new Date(), TERMINAL_TOKEN);
        
        final CPSRefundData cpsRefund2 = new CPSRefundData(2, UUID.randomUUID().toString(), AMOUNT, "2012", CHARGE_TOKEN, "LAST_4", AUTH_NUMBER,
                REFERENCE_NUMBER, PROC_TRANS_ID, new Date(), TERMINAL_TOKEN);
        
        this.entityDao.save(cpsRefund1);
        this.entityDao.save(cpsRefund2);
        
        buildMerchantAccount(TERMINAL_TOKEN);
        
        final PointOfSale pos = new PointOfSale();
        pos.setCustomer(this.customer);
        pos.setSerialNumber(SERIAL_NUMBER);
        pos.setId(1);
        
        this.entityDao.save(pos);
        
        final ProcessorTransaction procTrans = new ProcessorTransaction();
        procTrans.setAmount(AMOUNT);
        procTrans.setTicketNumber(1);
        procTrans.setPointOfSale(pos);
        procTrans.setPurchasedDate(new Date());
        procTrans.setProcessorTransactionType(new ProcessorTransactionType(
                CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_NO_REFUNDS));
        
        final Purchase purchase = new Purchase();
        purchase.setId(1L);
        procTrans.setPurchase(purchase);
        this.entityDao.save(purchase);
        
        this.entityDao.save(procTrans);
        
        final CPSData cpsData = new CPSData(procTrans, CHARGE_TOKEN);
        this.entityDao.save(cpsData);
        
        final CPSProcessData data = new CPSProcessData(SERIAL_NUMBER, TERMINAL_TOKEN, CHARGE_TOKEN, AMOUNT, AUTH_NUMBER, CARD_TYPE, LAST_4,
                REFERENCE_NUMBER, PROC_TRANS_ID, new Date(), new Date());
        
        processRecords(data);
        
        final CPSProcessData cpsProcessData = this.cpsProcessDataServiceImpl.find(TERMINAL_TOKEN, CHARGE_TOKEN);
        assertNull(cpsProcessData);
        
        final ProcessorTransaction result = this.processorTransactionService.findProcessorTransactionById(procTrans.getId(), false);
        assertNotNull(result);
        
        assertTrue(result.isIsApproved());
        assertEquals(AUTH_NUMBER, result.getAuthorizationNumber());
        assertEquals(LAST_4, result.getLast4digitsOfCardNumber());
        assertEquals(PROC_TRANS_ID, result.getProcessorTransactionId());
        assertEquals(CARD_TYPE, result.getCardType());
        assertEquals(REFERENCE_NUMBER, result.getReferenceNumber());
        assertEquals(AMOUNT, result.getAmount());
        assertEquals(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_REFUNDS, result.getProcessorTransactionType().getId());
        
        @SuppressWarnings("unchecked")
        final List<ProcessorTransaction> refundRes =
                this.entityDao.findByNamedQueryAndNamedParam("ProcessorTransaction.findRefundProcessorTransaction",
                                                             new String[] { "pointOfSaleId", "purchasedDate", "ticketNumber" }, new Object[] {
                                                                 pos.getId(), procTrans.getPurchasedDate(), procTrans.getTicketNumber() });
        assertEquals(2, refundRes.size());
        
        refundRes.forEach(r -> assertEquals(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_REFUND,
                                            r.getProcessorTransactionType().getId()));
    }
    
    @Test
    public void testExpired() throws Exception {
        
        buildMerchantAccount(TERMINAL_TOKEN);
        
        final ProcessorTransaction procTrans = new ProcessorTransaction();
        procTrans.setId(1L);
        procTrans.setAmount(AMOUNT);
        procTrans.setProcessorTransactionType(new ProcessorTransactionType(
                CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_NO_REFUNDS));
        
        final Purchase purchase = new Purchase();
        purchase.setId(1L);
        procTrans.setPurchase(purchase);
        this.entityDao.save(purchase);
        
        this.entityDao.save(procTrans);
        
        final PaymentCard paymentCard = new PaymentCard();
        paymentCard.setPurchase(purchase);
        paymentCard.setProcessorTransaction(procTrans);
        paymentCard.setProcessorTransactionType(procTrans.getProcessorTransactionType());
        paymentCard.setCardType(new CardType(CardProcessingConstants.CARD_TYPE_CREDIT_CARD));
        this.entityDao.save(paymentCard);
        
        final CPSData cpsData = new CPSData(procTrans, CHARGE_TOKEN);
        this.entityDao.save(cpsData);
        
        final CPSProcessData data =
                new CPSProcessData(SERIAL_NUMBER, TERMINAL_TOKEN, CHARGE_TOKEN, AMOUNT, null, CARD_TYPE, LAST_4, null, null, new Date(), new Date());
        
        processRecords(data);
        
        final CPSProcessData cpsProcessData = this.cpsProcessDataServiceImpl.find(TERMINAL_TOKEN, CHARGE_TOKEN);
        assertNull(cpsProcessData);
        
        final ProcessorTransaction result = this.processorTransactionService.findProcessorTransactionById(procTrans.getId(), false);
        assertNotNull(result);
        
        assertFalse(result.isIsApproved());
        assertEquals(LAST_4, result.getLast4digitsOfCardNumber());
        assertEquals(CARD_TYPE, result.getCardType());
        assertEquals(AMOUNT, result.getAmount());
        
        final PaymentCard pcRes = this.paymentCardService.findPaymentCardByPurchaseIdAndProcessorTransactionId(purchase.getId(), procTrans.getId());
        assertEquals(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_UNCLOSEABLE, pcRes.getProcessorTransactionType().getId());
        assertFalse(pcRes.isIsApproved());
    }
    
    @Test
    public void duplicateRecordConsumed() throws Exception {
        
        buildMerchantAccount(TERMINAL_TOKEN);
        
        final List<CPSProcessData> data = new ArrayList<>();
        
        final CPSProcessData d1 = new CPSProcessData(SERIAL_NUMBER, TERMINAL_TOKEN, CHARGE_TOKEN, AMOUNT, AUTH_NUMBER, CARD_TYPE, LAST_4,
                REFERENCE_NUMBER, PROC_TRANS_ID, new Date(), new Date());
        
        final CPSProcessData d2 = new CPSProcessData(SERIAL_NUMBER, TERMINAL_TOKEN, CHARGE_TOKEN, AMOUNT, AUTH_NUMBER, CARD_TYPE, LAST_4,
                REFERENCE_NUMBER, PROC_TRANS_ID, new Date(), new Date());
        
        data.add(d1);
        data.add(d2);
        
        processRecords(data);
        
        final CPSProcessData cpsProcessData = this.cpsProcessDataServiceImpl.find(TERMINAL_TOKEN, CHARGE_TOKEN);
        assertNotNull(cpsProcessData);
        assertEquals(1, DBHelper.list(CPSProcessData.class).size());
    }
    
    @Test
    public void duplicateRecordNullSerialNumber() throws Exception {
        
        buildMerchantAccount(TERMINAL_TOKEN);
        
        final List<CPSProcessData> data = new ArrayList<>();
        
        final CPSProcessData d1 = new CPSProcessData(null, TERMINAL_TOKEN, CHARGE_TOKEN, AMOUNT, AUTH_NUMBER, CARD_TYPE, LAST_4, REFERENCE_NUMBER,
                PROC_TRANS_ID, new Date(), new Date());
        
        final CPSProcessData d2 = new CPSProcessData(null, TERMINAL_TOKEN, CHARGE_TOKEN, AMOUNT, AUTH_NUMBER, CARD_TYPE, LAST_4, REFERENCE_NUMBER,
                PROC_TRANS_ID, new Date(), new Date());
        
        data.add(d1);
        data.add(d2);
        
        processRecords(data);
        
        final CPSProcessData cpsProcessData = this.cpsProcessDataServiceImpl.find(TERMINAL_TOKEN, CHARGE_TOKEN);
        assertNotNull(cpsProcessData);
        assertEquals(1, DBHelper.list(CPSProcessData.class).size());
    }
    
    @Test
    public void merchantAccountDNE() throws Exception {
        final CPSProcessData data = new CPSProcessData(SERIAL_NUMBER, TERMINAL_TOKEN, CHARGE_TOKEN, AMOUNT, AUTH_NUMBER, CARD_TYPE, LAST_4,
                REFERENCE_NUMBER, PROC_TRANS_ID, new Date(), new Date());
        
        processRecords(data);
        
        final CPSProcessData cpsProcessData = this.cpsProcessDataServiceImpl.find(TERMINAL_TOKEN, CHARGE_TOKEN);
        assertNull(cpsProcessData);
    }
    
    private void buildMerchantAccount(final String terminalToken) {
        final MerchantAccount merchantAccount = new MerchantAccount();
        merchantAccount.setIsLink(true);
        merchantAccount.setIsValidated(true);
        merchantAccount.setMerchantStatusType(new MerchantStatusType(1, "Enabled", new Date(), 1));
        merchantAccount.setCustomer(this.customer);
        merchantAccount.setTerminalToken(TERMINAL_TOKEN);
        final Processor processor = new Processor();
        processor.setId(1);
        merchantAccount.setProcessor(processor);
        this.entityDao.save(merchantAccount);
    }
    
    private void processRecords(final CPSProcessData data) throws JsonException {
        processRecords(Arrays.asList(data));
        
    }
    
    private void processRecords(final List<CPSProcessData> data) throws JsonException {
        
        final List<ConsumerRecord<String, String>> recList = new ArrayList<>();
        
        for (int i = 0; i < data.size(); i++) {
            recList.add(new ConsumerRecord<String, String>(TOPIC, i, i, data.get(i).getChargeToken(), this.json.serialize(data.get(i))));
        }
        
        @SuppressWarnings("PMD.UseConcurrentHashMap")
        final Map<TopicPartition, List<ConsumerRecord<String, String>>> recordMap = new HashMap<>();
        final TopicPartition topicPartition = new TopicPartition(TOPIC, 0);
        
        recordMap.put(topicPartition, recList);
        
        final ConsumerRecords<String, String> records = new ConsumerRecords<String, String>(recordMap);
        Mockito.when(this.mockConsumer.poll(Mockito.any(Duration.class))).thenReturn(records);
        
        this.cpsProcessDataConsumerTask.process();
    }
}
