package com.digitalpaytech.scheduling.queue.alert.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyByte;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import org.hibernate.Session;
import org.springframework.context.MessageSource;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.digitalpaytech.bdd.services.AlertsServiceMockImpl;
import com.digitalpaytech.bdd.services.CustomerAdminServiceMockImpl;
import com.digitalpaytech.bdd.services.CustomerAlertTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.EntityDaoServiceMockImpl;
import com.digitalpaytech.bdd.services.EventActionTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.EventSeverityTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.EventTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.LocationServiceMockImpl;
import com.digitalpaytech.bdd.services.PointOfSaleServiceMockImpl;
import com.digitalpaytech.bdd.services.PosEventCurrentServiceMockImpl;
import com.digitalpaytech.bdd.services.QueueEventPublisherMockImpl;
import com.digitalpaytech.bdd.services.RouteServiceMockImpl;
import com.digitalpaytech.bdd.services.UserDefinedEventInfoServiceMockImpl;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.dto.queue.QueueCustomerAlertType;
import com.digitalpaytech.dto.queue.QueueEvent;
import com.digitalpaytech.scheduling.queue.alert.CustomerAlertTypeEventProcessor;
import com.digitalpaytech.scheduling.queue.alert.impl.AlertProcessorImpl;
import com.digitalpaytech.service.AlertsService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerAlertTypeService;
import com.digitalpaytech.service.EventActionTypeService;
import com.digitalpaytech.service.EventSeverityTypeService;
import com.digitalpaytech.service.EventTypeService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.PosAlertStatusService;
import com.digitalpaytech.service.PosEventCurrentService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.service.ServiceHelper;
import com.digitalpaytech.service.UserDefinedEventInfoService;
import com.digitalpaytech.service.queue.QueueNotificationService;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.EntityMap;
import com.digitalpaytech.service.queue.QueueEventService;

// for replacing HandlingCollectionAndCommunicationAlertStatusChangesStories.java

public class HandlingCollectionAndCommunicationAlertStatusChangesIntegrationTest {
    @InjectMocks
    private CustomerAlertTypeEventProcessorImpl customerAlertTypeEventProcessor;

    @Mock
    private AlertsService alertsService;
    @Mock
    private CustomerAlertTypeService customerAlertTypeService;
    @Mock
    private UserDefinedEventInfoService userDefinedEventInfoService;
    @Mock
    private PosEventCurrentService posEventCurrentService;
    @Mock
    private PosAlertStatusService posAlertStatusService;
    @Mock
    private QueueEventService queueEventService;
    @Mock
    private ServiceHelper serviceHelper;
    @Mock
    private EntityDao entityDao;

    
    
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        final Session session = Mockito.mock(Session.class);
        Mockito.when(this.entityDao.getCurrentSession()).thenReturn(session);
        
    }

    @After
    public void tearDown() {
        
    }

    
    @Test
    public void callHandleAdded() {
        // Call CustomerAlertTypeEventProcessor, handle method, ADDED
        final QueueCustomerAlertType q = createQueueCustomerAlertType(QueueCustomerAlertType.Type.ADDED);
        initMockitoObjects(q);
        this.customerAlertTypeEventProcessor.handle(q);
    }

    @Test
    public void callHandleRemoved() {
        // Call CustomerAlertTypeEventProcessor, handle method, REMOVED
        final QueueCustomerAlertType q = createQueueCustomerAlertType(QueueCustomerAlertType.Type.REMOVED);
        initMockitoObjects(q);
        this.customerAlertTypeEventProcessor.handle(q);
    }

    @Test
    public void callHandleModified() {
        // Call CustomerAlertTypeEventProcessor, handle method, MODIFIED
        final QueueCustomerAlertType q = createQueueCustomerAlertType(QueueCustomerAlertType.Type.MODIFIED);
        initMockitoObjects(q);
        this.customerAlertTypeEventProcessor.handle(q);
    }

    @Test
    public void callHandleCommunication() {
        // Call CustomerAlertTypeEventProcessor, handle method, COMMUNICATION
        final QueueCustomerAlertType q = createQueueCustomerAlertType(QueueCustomerAlertType.Type.COMMUNICATION);
        initMockitoObjects(q);
        this.customerAlertTypeEventProcessor.handle(q);
    }
    
    private void initMockitoObjects(final QueueCustomerAlertType qCustAlertType) {
        Mockito.when(this.posEventCurrentService.findActiveDefaultPosEventCurrentByPosIdListAndAttIdList(Mockito.eq(qCustAlertType
                     .getPointOfSaleIds()), Mockito.anyList()))
                     .thenAnswer(PosEventCurrentServiceMockImpl.findActivePosEventCurrentByPosIdListAndAttIdListMock());
        Mockito.when(this.alertsService.findAlertThresholdTypeByAlertClassTypeId(Mockito.anyByte()))
            .then(AlertsServiceMockImpl.findAlertThresholdTypeByAlertClassTypeId());

        Mockito.when(this.customerAlertTypeService.findCustomerAlertTypeByIdList(Mockito.anyList()))
            .then(CustomerAlertTypeServiceMockImpl.findCustomerAlertTypeByIdListMock());
        Mockito.when(this.customerAlertTypeService.findCustomerAlertTypeById(Mockito.anyInt())).thenAnswer(CustomerAlertTypeServiceMockImpl
                                                                                                           .findCustomerAlertTypeByIdMock());
        
        Mockito.when(this.userDefinedEventInfoService.findEventsByNamedQueryAndPointOfSaleIdListAndCustomerAlertTypeId(Mockito.anyString(),
                                                                                                               Mockito.anyList(),
                                                                                                               Mockito.anyInt()))
            .thenAnswer(UserDefinedEventInfoServiceMockImpl.findEventsByNamedQueryAndPointOfSaleIdListAndCustomerAlertTypeIdMock());
        Mockito.when(this.userDefinedEventInfoService.findEventsByNamedQueryAndPointOfSaleIdList(Mockito.anyString(), Mockito.anyList()))
            .thenAnswer(UserDefinedEventInfoServiceMockImpl.findEventsByNamedQueryAndPointOfSaleIdListMock());
        Mockito.doAnswer(PosEventCurrentServiceMockImpl.savePosEventCurrentMock()).when(this.posEventCurrentService)
            .savePosEventCurrent(Mockito.any(PosEventCurrent.class));
        
        Mockito.doAnswer(PosEventCurrentServiceMockImpl.findPosEventCurrentsToDisableMock()).when(this.posEventCurrentService)
            .findDefaultPosEventCurrentsToDisable(Mockito.anyList());
        Mockito.doAnswer(PosEventCurrentServiceMockImpl.findPosEventCurrentsToEnableMock()).when(this.posEventCurrentService)
            .findDefaultPosEventCurrentsToEnable(Mockito.anyList());
        Mockito.doAnswer(PosEventCurrentServiceMockImpl.findPosEventCurrentsByIdListMock()).when(this.posEventCurrentService)
            .findPosEventCurrentsByIdList(Mockito.anyList());

        Mockito.when(this.serviceHelper.bean(Mockito.anyString(), Mockito.eq(CustomerAlertTypeEventProcessor.class)))
            .thenReturn(this.customerAlertTypeEventProcessor);

        final Map<Integer, String[]> map = new HashMap<Integer, String[]>();
        map.put(5, new String[] { "alertQuery", "clearQuery" });
        CustomerAlertTypeEventProcessorImpl.setAlertQueryPointOfSaleCustomerAlertTypeList(map);        
    }
    
    private QueueCustomerAlertType createQueueCustomerAlertType(final QueueCustomerAlertType.Type type) {
        final List<CustomerAlertType> custAlertTypes = EntityMap.INSTANCE.findDefaultCustomerAlertTypes();
        final List<Integer> posIds = new ArrayList<>(2);
        posIds.add(1);
        posIds.add(2);
        
        final List<Integer> catIds = new ArrayList<>(custAlertTypes.size());
        for (CustomerAlertType cat : custAlertTypes) {
            catIds.add(cat.getId());
        }
        final QueueCustomerAlertType qca = new QueueCustomerAlertType();
        qca.setAction(type);
        qca.setCustomerAlertTypeIds(catIds);
        qca.setPointOfSaleIds(posIds);
        qca.setLimit(500);
        return qca;
    }
    
    
}
