package com.digitalpaytech.scheduling.queue.alert.impl;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.when;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import com.digitalpaytech.bdd.services.CollectionTypeServiceMockImpl;
import com.digitalpaytech.scheduling.alert.service.impl.RunningTotalJobServiceImpl;
import com.digitalpaytech.service.AlertsService;
import com.digitalpaytech.service.CollectionTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.UserDefinedEventInfoService;
import com.digitalpaytech.service.queue.QueueCustomerAlertTypeService;

// For replacing CalculatePaystationBalanceStories.java

public class CalculatePaystationBalanceIntegrationTest {

    @InjectMocks
    private RunningTotalJobServiceImpl runningTotalJobServiceImpl;

    @Mock
    private QueueCustomerAlertTypeService queueCustomerAlertTypeService;
    @Mock
    private CollectionTypeService collectionTypeService;
    @Mock
    private EmsPropertiesService emsPropertiesService;
    @Mock
    private AlertsService alertsService;
    @Mock
    private UserDefinedEventInfoService userDefinedEventInfoService;

    
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.runningTotalJobServiceImpl.setQueueCustomerAlertTypeService(this.queueCustomerAlertTypeService);
        when(this.collectionTypeService.calculatePaystationBalance(anyInt(), anyInt(), anyInt())).thenAnswer(CollectionTypeServiceMockImpl
                                                                                                             .calculatePaystationBalanceMock());
        
        this.queueCustomerAlertTypeService.addCustomerAlertTypeEvent(anyList(), anyList(), anyList());
        this.queueCustomerAlertTypeService.addCustomerAlertTypeEvent(anyList(), anyList(), anyList(), anyInt());
        this.queueCustomerAlertTypeService.addCustomerAlertTypeEvent(anyList(), anyList(), anyList(), anyList());
        this.runningTotalJobServiceImpl.calculatePaystationBalance(0, 0, 0);
    }

    @After
    public void tearDown() {
    }
    
    @Test
    public void calculatePaystationBalance() {
        this.runningTotalJobServiceImpl.calculatePaystationBalance(0, 0, 0);        
    }
}
