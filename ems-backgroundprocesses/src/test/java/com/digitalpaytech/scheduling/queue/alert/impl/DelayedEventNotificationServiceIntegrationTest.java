package com.digitalpaytech.scheduling.queue.alert.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.EventActionType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.EventType;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PaystationType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosEventDelayed;
import com.digitalpaytech.scheduling.queue.alert.delayed.DelayedEventNotificationServiceImpl;
import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.PosEventDelayedServiceImpl;
import com.digitalpaytech.service.queue.QueueEventService;

@SuppressWarnings("checkstyle:magicnumber")
public class DelayedEventNotificationServiceIntegrationTest {
    private static final String TEST_SERVER = "testServer";
    
    private final TestContext ctx = TestContext.getInstance();
    
    private final EntityDaoTest entityDao = new EntityDaoTest();
    
    @Mock
    private ClusterMemberService clusterMemberService;
    
    @Mock
    private EmsPropertiesService emsPropertiesService;
    
    @Mock
    private QueueEventService queueEventService;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    @SuppressWarnings("PMD.UnusedPrivateField")
    private PosEventDelayedServiceImpl posEventDelayedService;
    
    @InjectMocks
    private DelayedEventNotificationServiceImpl delayedEventNotificationServiceImpl;
    
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.ctx.autowiresFromTestObject(this);
        Mockito.when(this.clusterMemberService.getClusterName()).thenReturn(TEST_SERVER);
        Mockito.when(this.emsPropertiesService.getPropertyValue(EmsPropertiesService.DELAYED_EVENTS_PROCESSING_SERVER)).thenReturn(TEST_SERVER);
    }
    
    @After
    public void tearDown() throws Exception {
        this.ctx.getDatabase().clear();
    }
    
    @Test
    public void testDeleteByDelayedGMT() {
        
        this.entityDao.save(buildPosEventDelayed(DateUtils.addDays(new Date(), -2), DateUtils.addDays(new Date(), -1)));
        this.entityDao.save(buildPosEventDelayed(new Date(), DateUtils.addDays(new Date(), 1)));
        this.delayedEventNotificationServiceImpl.processDelayedAlerts();
        Mockito.verify(this.queueEventService).createEventQueue(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt(), Mockito.any(), Mockito.anyInt(),
                                                                Mockito.anyInt(), Mockito.any(), Mockito.any(), Mockito.anyBoolean(),
                                                                Mockito.anyBoolean(), Mockito.anyInt(), Mockito.anyByte());
        final List<PosEventDelayed> events = this.entityDao.loadAll(PosEventDelayed.class);
        Assert.assertEquals(1, events.size());
    }
    
    @Test
    public void testEmptyList() {
        this.delayedEventNotificationServiceImpl.processDelayedAlerts();
        Mockito.verify(this.queueEventService, Mockito.times(0))
                .createEventQueue(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt(), Mockito.any(), Mockito.anyInt(), Mockito.anyInt(),
                                  Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.anyBoolean(), Mockito.anyInt(), Mockito.anyByte());
        final List<PosEventDelayed> events = this.entityDao.loadAll(PosEventDelayed.class);
        Assert.assertEquals(0, events.size());
    }
    
    @Test
    public void testAllNotReady() {
        this.entityDao.save(buildPosEventDelayed(DateUtils.addDays(new Date(), 1), DateUtils.addDays(new Date(), 1)));
        this.entityDao.save(buildPosEventDelayed(new Date(), DateUtils.addDays(new Date(), 1)));
        this.delayedEventNotificationServiceImpl.processDelayedAlerts();
        Mockito.verify(this.queueEventService, Mockito.times(0))
                .createEventQueue(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt(), Mockito.any(), Mockito.anyInt(), Mockito.anyInt(),
                                  Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.anyBoolean(), Mockito.anyInt(), Mockito.anyByte());
        final List<PosEventDelayed> events = this.entityDao.loadAll(PosEventDelayed.class);
        Assert.assertEquals(2, events.size());
    }
    
    @Test
    public void testAllReady() {
        this.entityDao.save(buildPosEventDelayed(DateUtils.addDays(new Date(), -1), DateUtils.addDays(new Date(), -1)));
        this.entityDao.save(buildPosEventDelayed(DateUtils.addDays(new Date(), -1), DateUtils.addDays(new Date(), -1)));
        this.delayedEventNotificationServiceImpl.processDelayedAlerts();
        Mockito.verify(this.queueEventService, Mockito.times(2))
                .createEventQueue(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt(), Mockito.any(), Mockito.anyInt(), Mockito.anyInt(),
                                  Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.anyBoolean(), Mockito.anyInt(), Mockito.anyByte());
        final List<PosEventDelayed> events = this.entityDao.loadAll(PosEventDelayed.class);
        Assert.assertEquals(0, events.size());
    }
    
    @Test
    public void testWrongServer() {
        Mockito.when(this.emsPropertiesService.getPropertyValue(EmsPropertiesService.DELAYED_EVENTS_PROCESSING_SERVER)).thenReturn("WrongServer");
        this.entityDao.save(buildPosEventDelayed(DateUtils.addDays(new Date(), -1), DateUtils.addDays(new Date(), -1)));
        this.entityDao.save(buildPosEventDelayed(DateUtils.addDays(new Date(), -1), DateUtils.addDays(new Date(), -1)));
        this.delayedEventNotificationServiceImpl.processDelayedAlerts();
        Mockito.verify(this.queueEventService, Mockito.times(0))
                .createEventQueue(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt(), Mockito.any(), Mockito.anyInt(), Mockito.anyInt(),
                                  Mockito.any(), Mockito.any(), Mockito.anyBoolean(), Mockito.anyBoolean(), Mockito.anyInt(), Mockito.anyByte());
        final List<PosEventDelayed> events = this.entityDao.loadAll(PosEventDelayed.class);
        Assert.assertEquals(2, events.size());
    }
    
    private PosEventDelayed buildPosEventDelayed(final Date alertGmt, final Date delayedGmt) {
        final PosEventDelayed event = new PosEventDelayed();
        
        final PointOfSale pos = new PointOfSale();
        this.entityDao.save(pos);
        
        final Paystation paystation = new Paystation();
        paystation.setId(1);
        PaystationType paystationType = new PaystationType();
        paystationType.setId(5);
        paystation.setPaystationType(paystationType);
        
        this.entityDao.save(paystation);
        pos.setPaystation(paystation);
        event.setPointOfSale(pos);
        
        final EventSeverityType est = new EventSeverityType(1);
        event.setEventSeverityType(est);
        
        final EventActionType eat = new EventActionType(1);
        event.setEventActionType(eat);
        
        final CustomerAlertType cat = new CustomerAlertType();
        cat.setId(1);
        
        final EventType et = new EventType((byte) 33);
        event.setEventType(et);
        
        event.setIsActive(true);
        event.setIsHidden(false);
        
        event.setAlertGmt(alertGmt);
        event.setDelayedGmt(delayedGmt);
        return event;
    }
}
