package com.digitalpaytech.scheduling.queue.alert.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyByte;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.context.MessageSource;

import com.digitalpaytech.bdd.services.CustomerAdminServiceMockImpl;
import com.digitalpaytech.bdd.services.CustomerAlertTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.EventActionTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.EventSeverityTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.EventTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.LocationServiceMockImpl;
import com.digitalpaytech.bdd.services.PointOfSaleServiceMockImpl;
import com.digitalpaytech.bdd.services.RouteServiceMockImpl;
import com.digitalpaytech.dto.queue.QueueEvent;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerAlertTypeService;
import com.digitalpaytech.service.EventActionTypeService;
import com.digitalpaytech.service.EventSeverityTypeService;
import com.digitalpaytech.service.EventTypeService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.service.queue.QueueNotificationService;

// For replacing AlertProcessingStories.java

public class AlertProcessorImplIntegrationTest {
    @InjectMocks
    private AlertProcessorImpl alertProcessor;
    @Mock
    private QueueNotificationService queueNotificationService;
    @Mock
    private PointOfSaleService pointOfSaleService;
    @Mock
    private LocationService locationService;
    @Mock
    private CustomerAlertTypeService customerAlertTypeService;
    @Mock
    private CustomerAdminService customerAdminService;
    @Mock
    private EventTypeService eventTypeService;
    @Mock
    private EventSeverityTypeService eventSeverityTypeService;
    @Mock
    private EventActionTypeService eventActionTypeService;
    @Mock
    private MessageSource messageSource;
    @Mock
    private RouteService routeService;
    
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        
        when(this.pointOfSaleService.findPointOfSaleById(anyInt())).thenAnswer(PointOfSaleServiceMockImpl.findPointOfSaleByIdMock());
        when(this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(anyInt(), anyInt()))
                .thenAnswer(CustomerAdminServiceMockImpl.createDummyCustomerProperty());
        
        when(this.customerAlertTypeService.findCustomerAlertTypesByCustomerIdAlertThresholdTypeIdAndIsActive(anyInt(), anyInt(), anyBoolean()))
                .thenAnswer(CustomerAlertTypeServiceMockImpl.findCustomerAlertTypesByCustomerIdAlertThresholdTypeIdAndIsActiveMock());
        
        when(this.customerAlertTypeService.findCustomerAlertEmailByCustomerAlertTypeId(anyInt()))
                .thenAnswer(CustomerAlertTypeServiceMockImpl.findCustomerAlertEmailByCustomerAlertTypeIdMock());
        when(this.customerAlertTypeService.findCustomerAlertTypeById(anyInt()))
                .thenAnswer(CustomerAlertTypeServiceMockImpl.findCustomerAlertTypeByIdMock());
        when(this.routeService.findRoutesByPointOfSaleId(anyInt())).thenAnswer(RouteServiceMockImpl.findRoutesByPointOfSaleIdMock());
        when(this.eventTypeService.findEventTypeById(anyByte())).thenAnswer(EventTypeServiceMockImpl.findEventTypeByIdMock());
        when(this.locationService.findLocationById(anyInt())).thenAnswer(LocationServiceMockImpl.findLocationByIdMock());
        when(this.pointOfSaleService.findPosServiceStateByPointOfSaleId(anyInt()))
                .thenAnswer(PointOfSaleServiceMockImpl.findPosServiceStateByPointOfSaleIdMock());
        when(this.eventSeverityTypeService.findEventSeverityType(anyInt())).thenAnswer(EventSeverityTypeServiceMockImpl.findEventSeverityTypeMock());
        when(this.eventActionTypeService.findEventActionType(anyInt())).thenAnswer(EventActionTypeServiceMockImpl.findEventActionTypeMock());
        when(this.messageSource.getMessage(anyString(), any(Object[].class), any(Locale.class))).thenAnswer(new Answer<String>() {
            @Override
            public String answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                return (String) args[0];
            }
        });
        this.alertProcessor.setQueueNotificationService(this.queueNotificationService);
        this.queueNotificationService.createQueueNotificationEmail(anyString(), anyString(), anyString());
    }
    
    @After
    public void tearDown() {
    }
    

    @Test
    public void processAlertCustomerAlertTypeId() {
        final List<QueueEvent> list = createQueueEventsWithCustomerAlertTypeIds();
        for (QueueEvent q : list) {
            this.alertProcessor.processAlert(q);
        }
    }
    
    
    private Date createCal(final int y, final int m, final int d, final int h, final int s) {
        return new GregorianCalendar(y, m, d, h, s).getTime();
    }
    
    private QueueEvent createQueueEvent(final String alertInfo, 
                                        final Integer customerAlertTypeId, 
                                        final Integer eventActionTypeId, 
                                        final int eventSeverityTypeId, 
                                        final byte eventTypeId, 
                                        final boolean isActive, 
                                        final boolean isNotification, 
                                        final Integer pointOfSaleId) {
        final QueueEvent q = new QueueEvent();
        q.setAlertInfo(alertInfo);
        q.setCustomerAlertTypeId(customerAlertTypeId);
        q.setEventActionTypeId(eventActionTypeId);
        q.setEventSeverityTypeId(eventSeverityTypeId);
        q.setEventTypeId(eventTypeId);
        q.setIsActive(isActive);
        q.setIsNotification(isNotification);
        q.setPointOfSaleId(pointOfSaleId);
        q.setCreatedGMT(createCal(2018, 5, 14, 16, 30));
        q.setTimestampGMT(createCal(2018, 5, 14, 16, 30));
        q.setLastModifiedByUserId(1);
        return q;
    }
    
    private final List<QueueEvent> createQueueEventsNoCustomerAlertTypeId() {
        final QueueEvent qe1 = createQueueEvent("Maintenance", null, null, 1, (byte) 38, true, false, 5);
        final QueueEvent qe2 = createQueueEvent("PowerDown", null, null, 0, (byte) 21, false, false, 7);
        final QueueEvent qe3 = createQueueEvent("", null, 4, 0, (byte) 57, false, false, 8);
        final List<QueueEvent> list = new ArrayList<>();
        list.add(qe1);
        list.add(qe2);
        list.add(qe3);
        return list;
    }

    private final List<QueueEvent> createQueueEventsWithCustomerAlertTypeIds() {
        final QueueEvent qe1 = createQueueEvent("Maintenance", 1, null, 1, (byte) 38, true, false, 5);
        final QueueEvent qe2 = createQueueEvent("PowerDown", 2, null, 0, (byte) 21, false, false, 7);
        final QueueEvent qe3 = createQueueEvent("", 3, 4, 0, (byte) 57, false, false, 8);
        final List<QueueEvent> list = new ArrayList<>();
        list.add(qe1);
        list.add(qe2);
        list.add(qe3);
        return list;        
    }
}
