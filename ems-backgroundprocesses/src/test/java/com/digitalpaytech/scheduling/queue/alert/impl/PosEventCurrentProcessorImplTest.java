package com.digitalpaytech.scheduling.queue.alert.impl;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.digitalpaytech.domain.AlertThresholdType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.dto.queue.QueueEvent;
import com.digitalpaytech.scheduling.queue.alert.PosEventCurrentProcessor;
import com.digitalpaytech.service.EventTypeService;
import com.digitalpaytech.service.PosEventCurrentService;
import com.digitalpaytech.service.queue.impl.KafkaEventServiceImpl;
import com.digitalpaytech.testing.services.impl.CustomerAlertTypeServiceTestImpl;
import com.digitalpaytech.testing.services.impl.EventSeverityTypeServiceTestImpl;
import com.digitalpaytech.testing.services.impl.EventTypeServiceTestImpl;
import com.digitalpaytech.testing.services.impl.PointOfSaleServiceTestImpl;
import com.digitalpaytech.testing.services.impl.PosAlertStatusServiceTestImpl;
import com.digitalpaytech.testing.services.impl.PosEventCurrentServiceTestImpl;
import com.digitalpaytech.util.DateUtil;

public class PosEventCurrentProcessorImplTest {
    
    private QueueEvent qEvent = new QueueEvent();
    
    private PosEventCurrentProcessor posEventCurrentProcessor;
    private PosEventCurrentService posEventCurrentService;
    private EventTypeService eventTypeService;
    
    @Before
    public final void setUp() {
        final int pointOfSaleId = 1;
        final Integer customerAlertTypeId = 1;
        final byte eventTypeId = 1;
        final int eventSeverityTypeId = 1;
        final Date date = DateUtil.getCurrentGmtDate();
        final Date timestampGMT = date;
        
        //        final boolean isActive = true;
        final boolean isActive = false;
        
        final Date createdGMT = date;
        final int lastModifiedByUserId = 1;
        
        this.qEvent.setCreatedGMT(createdGMT);
        this.qEvent.setCustomerAlertTypeId(customerAlertTypeId);
        this.qEvent.setEventSeverityTypeId(eventSeverityTypeId);
        this.qEvent.setEventTypeId(eventTypeId);
        this.qEvent.setIsActive(isActive);
        this.qEvent.setLastModifiedByUserId(lastModifiedByUserId);
        this.qEvent.setPointOfSaleId(pointOfSaleId);
        this.qEvent.setTimestampGMT(timestampGMT);
        
        this.posEventCurrentService = new PosEventCurrentServiceTestImpl();
        this.eventTypeService = new EventTypeServiceTestImpl();
        
        this.posEventCurrentProcessor = new PosEventCurrentProcessorImpl() {
            
        };
        
        this.posEventCurrentProcessor.setPosEventCurrentService(this.posEventCurrentService);
        
        this.posEventCurrentProcessor.setCustomerAlertTypeService(new CustomerAlertTypeServiceTestImpl() {
            
            @Override
            public CustomerAlertType findCustomerAlertTypeById(final Integer id) {
                final Date date = DateUtil.getCurrentGmtDate();
                final AlertThresholdType aThresholdType = new AlertThresholdType((short) 1);
                final Customer customer = new Customer(1);
                
                //isDefault = true
                final CustomerAlertType cat = new CustomerAlertType(aThresholdType, customer, "TEST", (float) 10, true, false, true, date, 1);
                
                return cat;
            }
            
        });
        
        this.posEventCurrentProcessor.setPosAlertStatusService(new PosAlertStatusServiceTestImpl() {
            
            @Override
            public void updateAlertCount(final PosEventCurrent posEventCurrent, final int originalSeverity, final boolean isSeverityRecalc) {
                // TODO Auto-generated method stub
                
            }
        });
        
        this.posEventCurrentProcessor.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            
            @Override
            public PointOfSale findPointOfSaleById(final Integer id) {
                return new PointOfSale(1);
            }
            
        });
        
        this.posEventCurrentProcessor.setQueueEventService(new KafkaEventServiceImpl());
        
        this.posEventCurrentProcessor.setEventTypeService(this.eventTypeService);
        
        this.posEventCurrentProcessor.setEventSeverityTypeService(new EventSeverityTypeServiceTestImpl() {
            
            public EventSeverityType findEventSeverityType(final Integer id) {
                return new EventSeverityType(1);
            }
        });
        
    }
    
    @Test
    public final void testProcessQueueEvent() {
        
        this.posEventCurrentProcessor.processQueueEvent(this.qEvent);
        
    }
    
}
