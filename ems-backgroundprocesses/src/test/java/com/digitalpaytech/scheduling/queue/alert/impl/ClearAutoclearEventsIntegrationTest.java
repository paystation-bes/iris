package com.digitalpaytech.scheduling.queue.alert.impl;

import org.junit.Test;
import org.junit.Before;
import java.util.Date;
import java.util.List;
import java.util.GregorianCalendar;
import java.util.Calendar;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.digitalpaytech.bdd.services.EventExceptionServiceMockImpl;
import com.digitalpaytech.bdd.services.PosEventCurrentServiceMockImpl;
import com.digitalpaytech.bdd.services.QueueEventPublisherMockImpl;
import com.digitalpaytech.dto.queue.QueueEvent;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.scheduling.task.UnwantedAlarmsRemovalTask;
import com.digitalpaytech.service.EventExceptionService;
import com.digitalpaytech.service.PosEventCurrentService;
import com.digitalpaytech.service.impl.RouteServiceImpl;
import com.digitalpaytech.service.queue.QueueEventService;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.EntityMap;

public class ClearAutoclearEventsIntegrationTest {
    @InjectMocks
    private RouteServiceImpl routeService;
    @InjectMocks
    private UnwantedAlarmsRemovalTask unwantedAlarmsRemovalTask;

    @Mock
    private PosEventCurrentService posEventCurrentService;
    @Mock
    private QueueEventService queueEventService;
    @Mock
    private EventExceptionService eventExceptionService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(this.posEventCurrentService.findActivePosEventCurrentByTimestamp((Date) Mockito.anyObject()))
            .thenAnswer(PosEventCurrentServiceMockImpl.findActivePosEventCurrentByTimestampMock());
    
        Mockito.when(this.eventExceptionService.isException(Mockito.anyInt(), Mockito.anyInt()))
            .thenAnswer(EventExceptionServiceMockImpl.isException());
    }
    
    @Test
    public void removeUnwantedEvents() {
  
        this.queueEventService.createEventQueue(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyInt(), Mockito.any(),  
                                                Mockito.anyInt(), Mockito.anyInt(), Mockito.any(), Mockito.any(), 
                                                Mockito.anyBoolean(), Mockito.anyBoolean(), Mockito.anyInt(), Mockito.anyByte());

        this.unwantedAlarmsRemovalTask.removeUnwantedAlarms();
    }

/*    
    private final QueueEvent createQueueEvent() {
        final List<CustomerAlertType> list = EntityMap.INSTANCE.findDefaultCustomerAlertTypes();
        final CustomerAlertType custType = list.get(0);
        custType.setName("Pay Station Battery Depleted");
        final GregorianCalendar cal = new GregorianCalendar();
        cal.add(Calendar.HOUR, -25);
        
        final QueueEvent q = new QueueEvent();
        q.setAlertInfo("alertInfo");
        q.setCreatedGMT(cal.getTime());
        q.setCustomerAlertTypeId(custType.getId());
        q.setEventActionTypeId(1);
        q.setEventSeverityTypeId(2);
        q.setIsActive(true);
        q.setIsNotification(true);
        q.setPointOfSaleId(1);

        return q;
    }
*/    
}
