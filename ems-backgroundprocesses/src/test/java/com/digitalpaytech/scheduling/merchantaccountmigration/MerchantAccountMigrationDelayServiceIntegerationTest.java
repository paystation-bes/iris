package com.digitalpaytech.scheduling.merchantaccountmigration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantAccountMigration;
import com.digitalpaytech.domain.MerchantAccountMigrationStatus;
import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.impl.MerchantAccountMigrationServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountMigrationStatusServiceImpl;
import com.digitalpaytech.service.impl.PreAuthServiceImpl;

public class MerchantAccountMigrationDelayServiceIntegerationTest {
    private static final String TEST_SERVER = "testServer";
    
    private EntityDao entityDao = new EntityDaoTest();
    
    private final TestContext ctx = TestContext.getInstance();
    
    @InjectMocks
    private MerchantAccountMigrationServiceImpl merchantAccountMigrationService;
    
    @InjectMocks
    private MerchantAccountMigrationStatusServiceImpl merchantAccountMigrationStatusService;
    
    @InjectMocks
    private MerchantAccountMigrationDelayServiceImpl service;
    
    @InjectMocks
    private PreAuthServiceImpl preAuthService;
    
    @Mock
    private EmsPropertiesService emsPropertiesService;
    
    @Mock
    private ClusterMemberService clusterMemberService;
    
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.ctx.autowiresFromTestObject(this);
        Mockito.when(this.clusterMemberService.getClusterName()).thenReturn(TEST_SERVER);
        Mockito.when(this.emsPropertiesService.getPropertyValue(EmsPropertiesService.DELAYED_EVENTS_PROCESSING_SERVER)).thenReturn(TEST_SERVER);
    }
    
    @Test
    public void testStillWaiting() {
        
        final MerchantAccountMigration mam =
                buildMerchantAccountMigration(MerchantAccountMigrationStatus.PENDING, Date.from(Instant.now().plus(Duration.ofHours(1))));
        this.service.processMerchantAccountMigration();
        
        final MerchantAccountMigration mam1Db = this.merchantAccountMigrationService.findById(mam.getId());
        assertNotNull(mam1Db);
        assertEquals(Integer.valueOf(MerchantAccountMigrationStatus.PENDING), mam1Db.getMerchantAccountMigrationStatus().getId());
        
    }
    
    @Test
    public void testFailed() {
        
        final MerchantAccountMigration mam = buildMerchantAccountMigration(MerchantAccountMigrationStatus.FAILED, new Date());
        this.service.processMerchantAccountMigration();
        
        final MerchantAccountMigration mam1Db = this.merchantAccountMigrationService.findById(mam.getId());
        assertNotNull(mam1Db);
        assertEquals(Integer.valueOf(MerchantAccountMigrationStatus.FAILED), mam1Db.getMerchantAccountMigrationStatus().getId());
        
    }
    
    @Test
    public void emptyMigrations() {
        
        this.service.processMerchantAccountMigration();
        
    }
    
    @Test
    public void testSucceed() {
        
        final MerchantAccountMigration mam = buildMerchantAccountMigration(MerchantAccountMigrationStatus.SUCCESS, new Date());
        this.service.processMerchantAccountMigration();
        
        final MerchantAccountMigration mam1Db = this.merchantAccountMigrationService.findById(mam.getId());
        assertNotNull(mam1Db);
        assertEquals(Integer.valueOf(MerchantAccountMigrationStatus.SUCCESS), mam1Db.getMerchantAccountMigrationStatus().getId());
        
    }
    
    private MerchantAccountMigration buildMerchantAccountMigration(final int status, final Date delayedUntil) {
        final MerchantAccountMigration mam = new MerchantAccountMigration();
        mam.setCreatedGMT(new Date());
        mam.setDelayUntilGMT(delayedUntil);
        final MerchantAccountMigrationStatus mams = new MerchantAccountMigrationStatus();
        mams.setId(status);
        mam.setMerchantAccountMigrationStatus(mams);
        
        final MerchantAccount ma = new MerchantAccount();
        this.entityDao.save(ma);
        
        mam.setMerchantAccount(ma);
        this.entityDao.save(mam);
        return mam;
    }
    
}
