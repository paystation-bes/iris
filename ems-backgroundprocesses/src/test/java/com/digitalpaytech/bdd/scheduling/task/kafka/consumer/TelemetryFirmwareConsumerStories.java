package com.digitalpaytech.bdd.scheduling.task.kafka.consumer;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class TelemetryFirmwareConsumerStories extends AbstractStories {
    
    public TelemetryFirmwareConsumerStories() {
        super();
        this.testHandlers.registerTestHandler("ota.device.notificationtopicpoll", TestTelemetryFirmwareConsumer.class);
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers));
    }
}
