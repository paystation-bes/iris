package com.digitalpaytech.bdd.scheduling.queue.alert.impl;

import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Iterator;

import org.junit.Assert;

import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.bdd.util.expression.StoryObjectBuilder;
import com.digitalpaytech.domain.PosEventCurrent;

@SuppressWarnings("unchecked")
public class TestPosEventCurrentTable {
    
    public final void assertInsert(final String dataToAssert) {
        
        final Collection<PosEventCurrent> posEventCurrentCollection =
                (Collection<PosEventCurrent>) TestDBMap.getTypeListFromMemory(PosEventCurrent.class);
                
        TestLookupTables.getExpressionParser().assertExisted("Expected POSEventCurrent was not found", posEventCurrentCollection, dataToAssert);
    }
    
    public final void assertNoInsert(final String data) {
        final StoryObjectBuilder objBuilder = TestLookupTables.getObjectBuilder();
        final PosEventCurrent expectedPec = (PosEventCurrent) objBuilder.instantiate(PosEventCurrent.ALIAS);
        TestLookupTables.getExpressionParser().parse(data, expectedPec);
        final Collection<PosEventCurrent> pecList = (Collection<PosEventCurrent>) TestDBMap.getTypeListFromMemory(PosEventCurrent.class);
        if (pecList != null) {
            final Boolean found = findPecInList(expectedPec, pecList);
            assertTrue(!found);
        }
    }
    
    public final void assertDelete(final String data) {
        final StoryObjectBuilder objBuilder = TestLookupTables.getObjectBuilder();
        final PosEventCurrent expectedPec = (PosEventCurrent) objBuilder.instantiate(PosEventCurrent.ALIAS);
        TestLookupTables.getExpressionParser().parse(data, expectedPec);
        final Collection<PosEventCurrent> pecList = (Collection<PosEventCurrent>) TestDBMap.getTypeListFromMemory(PosEventCurrent.class);
        final Boolean found = findPecInList(expectedPec, pecList);
        Assert.assertFalse(found);
        
    }
    
    public final void testPosEventCurrentUpdate(final String data) {
        assertInsert(data);
    }
    
    private Boolean findPecInList(final PosEventCurrent expectedPec, final Collection<PosEventCurrent> pecList) {
        Boolean found = false;
        final Iterator<PosEventCurrent> pecItr = pecList.iterator();
        while (pecItr.hasNext()) {
            final PosEventCurrent pec = pecItr.next();
            final Boolean posMatch = pec.getPointOfSale().getId() == expectedPec.getPointOfSale().getId();
            final Boolean catMatch = pec.getCustomerAlertType() == expectedPec.getCustomerAlertType();
            found = posMatch && catMatch;
            if (found) {
                break;
            }
        }
        return found;
    }
    
}
