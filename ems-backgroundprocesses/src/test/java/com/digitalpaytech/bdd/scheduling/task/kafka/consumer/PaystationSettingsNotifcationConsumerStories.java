package com.digitalpaytech.bdd.scheduling.task.kafka.consumer;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.Ignore;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

/**
 * TODO - Need to determine why PaystationSettingsNotifcationConsumerStories would fail in mvn verify, 
 *        need to fix paystation_settings_notification_consumer_stories.story.
 *         
 * https://t2systems.atlassian.net/browse/IRIS-4195
 */

//@RunWith(JUnitReportingRunner.class)
public class PaystationSettingsNotifcationConsumerStories {
//public class PaystationSettingsNotifcationConsumerStories extends AbstractStories {
//
//    
//    public PaystationSettingsNotifcationConsumerStories() {
//        super();
//        this.testHandlers.registerTestHandler("fms.device.upgradetopicpoll", TestPayStationSettingsNotificationConsumer.class);
//        JUnitReportingRunner.recommendedControls(configuredEmbedder());
//    }
//    
//    @Override
//    public final InjectableStepsFactory stepsFactory() {
//        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers));
//    }
}
