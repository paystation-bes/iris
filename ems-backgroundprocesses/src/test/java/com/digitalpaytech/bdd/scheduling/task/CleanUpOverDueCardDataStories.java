package com.digitalpaytech.bdd.scheduling.task;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.AlertSteps;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class CleanUpOverDueCardDataStories extends AbstractStories {
    
    public CleanUpOverDueCardDataStories() {
        super();
        this.testHandlers.registerTestHandler("calculateoutstandingcarddata", TestProcessPreAuths.class);
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }

    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new AlertSteps(this.testHandlers));
    }
    
}
