package com.digitalpaytech.bdd.scheduling.queue.alert.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyByte;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Locale;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.context.MessageSource;

import com.digitalpaytech.bdd.services.CustomerAdminServiceMockImpl;
import com.digitalpaytech.bdd.services.CustomerAlertTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.EventActionTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.EventSeverityTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.EventTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.LocationServiceMockImpl;
import com.digitalpaytech.bdd.services.PointOfSaleServiceMockImpl;
import com.digitalpaytech.bdd.services.QueueEventPublisherMockImpl;
import com.digitalpaytech.bdd.services.RouteServiceMockImpl;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.dto.queue.QueueEvent;
import com.digitalpaytech.dto.queue.QueueNotificationEmail;
import com.digitalpaytech.scheduling.queue.alert.impl.AlertProcessorImpl;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerAlertTypeService;
import com.digitalpaytech.service.EventActionTypeService;
import com.digitalpaytech.service.EventSeverityTypeService;
import com.digitalpaytech.service.EventTypeService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.service.impl.RouteServiceImpl;
import com.digitalpaytech.service.queue.impl.KafkaNotificationServiceImpl;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.queue.QueuePublisher;

@SuppressWarnings({ "PMD.BeanMembersShouldSerialize", "PMD.ExcessiveImports", "PMD.JUnit4TestShouldUseTestAnnotation", "PMD.TooManyStaticImports" })
public class TestAlertProcessor implements JBehaveTestHandler {
    
    @Mock
    private PointOfSaleService pointOfSaleService;
    
    @Mock
    private LocationService locationService;
    
    @Mock
    private CustomerAlertTypeService customerAlertTypeService;
    
    @Mock
    private CustomerAdminService customerAdminService;
    
    @Mock
    private EventTypeService eventTypeService;
    
    @Mock
    private EventSeverityTypeService eventSeverityTypeService;
    
    @Mock
    private EventActionTypeService eventActionTypeService;
    
    @Mock
    private MessageSource messageSource;
    
    @Mock
    private RouteService routeService;
    
    @InjectMocks
    private AlertProcessorImpl alertProcessor;
    
    @Mock
    private QueuePublisher<QueueNotificationEmail> notificationEmailQueuePublisher;
    
    @InjectMocks
    private KafkaNotificationServiceImpl queueNotificationService;
    
    
    public final void testReceiveQueueEvent() {
        MockitoAnnotations.initMocks(this);
        initalizeMocks();
        QueueEvent queueEvent = null;
        do {
            queueEvent = (QueueEvent) TestDBMap.getTestEventQueueFromMem().pollQueue(WebCoreConstants.QUEUE_TYPE_PROCESSING_EVENT);
            
            if (queueEvent != null) {
                this.alertProcessor.processAlert(queueEvent);
            }
        } while (queueEvent != null);
    }
    
    private void initalizeMocks() {
        when(this.pointOfSaleService.findPointOfSaleById(anyInt())).thenAnswer(PointOfSaleServiceMockImpl.findPointOfSaleById());
        when(this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(anyInt(), anyInt()))
                .thenAnswer(CustomerAdminServiceMockImpl.createDummyCustomerProperty());
        
        when(this.customerAlertTypeService.findCustomerAlertTypesByCustomerIdAlertThresholdTypeIdAndIsActive(anyInt(), anyInt(), anyBoolean()))
                .thenAnswer(CustomerAlertTypeServiceMockImpl.findCustomerAlertTypesByCustomerIdAlertThresholdTypeIdAndIsActive());
        
        when(this.customerAlertTypeService.findCustomerAlertEmailByCustomerAlertTypeId(anyInt()))
                .thenAnswer(CustomerAlertTypeServiceMockImpl.findCustomerAlertEmailByCustomerAlertTypeId());
        when(this.customerAlertTypeService.findCustomerAlertTypeById(anyInt()))
                .thenAnswer(CustomerAlertTypeServiceMockImpl.findCustomerAlertTypeById());
        when(this.routeService.findRoutesByPointOfSaleId(anyInt())).thenAnswer(RouteServiceMockImpl.findRoutesByPointOfSaleId());
        when(this.eventTypeService.findEventTypeById(anyByte())).thenAnswer(EventTypeServiceMockImpl.findEventTypeById());
        when(this.locationService.findLocationById(anyInt())).thenAnswer(LocationServiceMockImpl.findLocationById());
        when(this.pointOfSaleService.findPosServiceStateByPointOfSaleId(anyInt()))
                .thenAnswer(PointOfSaleServiceMockImpl.findPosServiceStateByPointOfSaleId());
        when(this.eventSeverityTypeService.findEventSeverityType(anyInt())).thenAnswer(EventSeverityTypeServiceMockImpl.findEventSeverityType());
        when(this.eventActionTypeService.findEventActionType(anyInt())).thenAnswer(EventActionTypeServiceMockImpl.findEventActionType());
        when(this.messageSource.getMessage(anyString(), any(Object[].class), any(Locale.class))).thenAnswer(new Answer<String>() {
            @Override
            public String answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                return (String) args[0];
            }
        });
        
        Mockito.doAnswer(QueueEventPublisherMockImpl.offer(WebCoreConstants.QUEUE_TYPE_NOTIFICATION_EMAIL)).when(this.notificationEmailQueuePublisher)
                .offer(Mockito.any(QueueNotificationEmail.class));
        
        this.alertProcessor.setQueueNotificationService(this.queueNotificationService);
    }
    
    @Override
    public Object performAction(Object... objects) {
        testReceiveQueueEvent();
        return null;
    }
    
    @Override
    public Object assertSuccess(Object... objects) {
        return null;
    }
    
    @Override
    public Object assertFailure(Object... objects) {
        return null;
    }
    
}
