package com.digitalpaytech.bdd.scheduling.alert.service.impl;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.AlertSteps;
import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

//Because we're using Kafka for events, these test stories wouldn't work anymore.
@org.junit.Ignore
@RunWith(JUnitReportingRunner.class)
public class CalculatePaystationBalanceStories extends AbstractStories {
    public CalculatePaystationBalanceStories() {
        super();
        this.testHandlers.registerTestHandler("calculateaddcustomeralerttypeevent", TestCalculatePaystationBalance.class);
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new AlertSteps(this.testHandlers));
    }
}
