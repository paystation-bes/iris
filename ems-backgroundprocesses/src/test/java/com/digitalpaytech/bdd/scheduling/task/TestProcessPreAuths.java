package com.digitalpaytech.bdd.scheduling.task;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.BeanFactory;

import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.cardprocessing.CardProcessingManager;
import com.digitalpaytech.cardprocessing.impl.CardProcessingManagerImpl;
import com.digitalpaytech.client.CardTypeClient;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.dao.impl.OpenSessionDaoTest;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.ribbon.mock.MockClientFactory;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.impl.CardRetryTransactionServiceImpl;
import com.digitalpaytech.service.impl.CardTypeServiceImpl;
import com.digitalpaytech.service.impl.CreditCardTypeServiceImpl;
import com.digitalpaytech.service.impl.CustomerCardTypeServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.LinkCardTypeServiceImpl;
import com.digitalpaytech.service.impl.PreAuthServiceImpl;
import com.digitalpaytech.service.impl.ProcessorTransactionServiceImpl;
import com.digitalpaytech.service.impl.ReversalArchiveServiceImpl;
import com.digitalpaytech.util.impl.TransactionFacadeImpl;

public class TestProcessPreAuths implements JBehaveTestHandler {
    
    @InjectMocks
    private CardProcessingManagerImpl cardProcessingManager;
    
    @InjectMocks
    private PreAuthServiceImpl preAuthService;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private CardRetryTransactionServiceImpl cardRetryTransactionService;
    
    @InjectMocks
    private ReversalArchiveServiceImpl reversalArchiveService;
    
    @InjectMocks
    private CreditCardTypeServiceImpl creditCardTypeService;
    
    @InjectMocks
    private TransactionFacadeImpl transactionFacade;
    
    @InjectMocks
    private CustomerCardTypeServiceImpl customerCardTypeService;
    
    @InjectMocks
    private ProcessorTransactionServiceImpl processorTransactionService;
    
    @InjectMocks
    private OpenSessionDaoTest openSessionDao;
    
    @Mock
    private BeanFactory beanFactory;
    
    @Mock
    private CryptoService cryptoService;
    
    @InjectMocks
    private MockClientFactory client;
    
    @InjectMocks
    private LinkCardTypeServiceImpl linkCardTypeService;
    
    @Mock
    private MailerService mailerService;
    
    @InjectMocks
    private CardTypeServiceImpl cardTypeService;
    
    @InjectMocks
    private EntityServiceImpl entityService;
    
    public TestProcessPreAuths() {
        MockitoAnnotations.initMocks(this);
        this.client.prepareForSuccessRequest(CardTypeClient.class, "{}".getBytes());
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Mockito.when(this.beanFactory.getBean(anyString(), Mockito.eq(CardProcessingManager.class))).thenReturn(this.cardProcessingManager);
        try {
            when(this.cryptoService.decryptData(Mockito.anyString(), Mockito.anyInt(), Mockito.any(Date.class))).thenAnswer(new Answer<String>() {
                
                @Override
                public String answer(final InvocationOnMock invocation) throws Throwable {
                    final String cardData = (String) invocation.getArguments()[0];
                    return cardData.replace("X", "");
                }
            });
        } catch (CryptoException e) {
            e.printStackTrace();
        }
        this.customerCardTypeService.init();
        
    }
    
    private void processOutstandingCardData() {
        this.cardProcessingManager.cleanupOverdueCardData();
    }
    
    @Override
    public final Object assertFailure(final Object... arg0) {
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... arg0) {
        return null;
    }
    
    @Override
    public final Object performAction(final Object... arg0) {
        processOutstandingCardData();
        return null;
    }
    
}
