package com.digitalpaytech.bdd.scheduling.alert.support.servlet.upsidewireless;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import com.digitalpaytech.bdd.services.CryptoServiceMockImpl;
import com.digitalpaytech.bdd.services.EmsPropertiesServiceMockImpl;
import com.digitalpaytech.bdd.services.ExtensiblePermitServiceMockImpl;
import com.digitalpaytech.bdd.services.ExtensibleRateAndPermissionServiceMockImpl;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.DBHelper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestConstants;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.cardprocessing.cps.CPSProcessor;
import com.digitalpaytech.cardprocessing.impl.CardProcessingManagerImpl;
import com.digitalpaytech.cardprocessing.impl.CardProcessorFactoryImpl;
import com.digitalpaytech.client.MessagingClient;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.ActivePermit;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.ThirdPartyServiceAccount;
import com.digitalpaytech.dto.SmsAlertInfo;
import com.digitalpaytech.dto.cps.CPSTransactionDto;
import com.digitalpaytech.ribbon.mock.MockClientFactory;
import com.digitalpaytech.scheduling.alert.service.impl.SmsPermitAlertJobServiceImpl;
import com.digitalpaytech.scheduling.alert.support.servlet.upsidewireless.SmsHttpPostCallbackServlet;
import com.digitalpaytech.scheduling.sms.support.SmsServiceImpl;
import com.digitalpaytech.service.ActivePermitService;
import com.digitalpaytech.service.CcFailLogService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.ExtensiblePermitService;
import com.digitalpaytech.service.ExtensibleRateAndPermissionService;
import com.digitalpaytech.service.SmsTransactionLogService;
import com.digitalpaytech.service.cps.impl.CPSDataServiceImpl;
import com.digitalpaytech.service.cps.impl.CoreCPSServiceImpl;
import com.digitalpaytech.service.impl.CardTypeServiceImpl;
import com.digitalpaytech.service.impl.CreditCardTypeServiceImpl;
import com.digitalpaytech.service.impl.CustomerServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.MobileNumberServiceImpl;
import com.digitalpaytech.service.impl.PaymentCardServiceImpl;
import com.digitalpaytech.service.impl.PaymentTypeServiceImpl;
import com.digitalpaytech.service.impl.PermitServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.ProcessorTransactionServiceImpl;
import com.digitalpaytech.service.impl.PurchaseServiceImpl;
import com.digitalpaytech.service.impl.SmsAlertServiceImpl;
import com.digitalpaytech.service.kpi.KPIListenerService;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.impl.TransactionFacadeImpl;
import com.digitalpaytech.util.json.CardTransactionResponseGenerator;
import com.digitalpaytech.util.json.Status;

import junit.framework.Assert;

@SuppressWarnings({ "PMD.ExcessiveImports", "PMD.TooManyStaticImports", "PMD.UnusedPrivateField", "PMD.TooManyFields" })
public class TestDoPost implements JBehaveTestHandler {
    
    @SuppressWarnings("PMD.ImmutableField")
    private MockClientFactory client = MockClientFactory.getInstance();
    
    @Mock
    private KPIListenerService kpiListenerService;
    
    @Mock
    private CcFailLogService ccFailLogService;
    
    @Mock
    private SmsTransactionLogService smsTransactionLogService;
    
    @Mock
    private CryptoService cryptoService;
    
    @Mock
    private SmsServiceImpl smsService;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private SmsAlertServiceImpl smsAlertService;
    
    @InjectMocks
    private EntityServiceImpl entityService;
    
    @InjectMocks
    private CardTypeServiceImpl cardTypeService;
    
    @InjectMocks
    private CardProcessingManagerImpl cardProcessingManager;
    
    @InjectMocks
    private CPSDataServiceImpl cpsDataService;
    
    @InjectMocks
    private MerchantAccountServiceImpl merchantAccountService;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    private CardProcessorFactoryImpl cardProcessorFactory;
    
    @Mock
    private EmsPropertiesService emsPropertiesService;
    
    @InjectMocks
    private PaymentTypeServiceImpl paymentTypeService;
    
    @InjectMocks
    private CoreCPSServiceImpl coreCPSService;
    
    @InjectMocks
    private PurchaseServiceImpl purchaseService;
    
    @Mock
    private ProcessorTransactionServiceImpl processorTransactionService;
    
    @InjectMocks
    private PermitServiceImpl permitService;
    
    @Mock
    private ExtensiblePermitService extensiblePermitService;
    
    @Mock
    private ActivePermitService activePermitService;
    
    @InjectMocks
    private SmsHttpPostCallbackServlet smsHttpPostCallbackServlet;
    
    @InjectMocks
    private SmsPermitAlertJobServiceImpl smsPermitAlertJobService;
    
    @InjectMocks
    private CustomerServiceImpl customerService;
    
    @InjectMocks
    private TransactionFacadeImpl transactionFacade;
    
    @InjectMocks
    private PaymentCardServiceImpl paymentCardService;
    
    @InjectMocks
    private MobileNumberServiceImpl mobileNumberService;
    
    @InjectMocks
    private CreditCardTypeServiceImpl creditCardTypeService;
    
    @Mock
    private CPSProcessor cpsProcessor;
    
    @Mock
    private MessageSourceAccessor messageAccessor;
    
    @Mock
    private ExtensibleRateAndPermissionService extensibleRateAndPermissionService;
    
    @SuppressWarnings({ "PMD.AvoidUsingHardCodedIP", "PMD.SignatureDeclareThrowsException", "unchecked" })
    private void doPost() throws Exception {
        MockitoAnnotations.initMocks(this);
        TestContext.getInstance().autowiresFromTestObject(this);
        this.client.prepareForSuccessRequest(MessagingClient.class, "{}".getBytes());
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final Map<String, Object> dataMap = new ConcurrentHashMap<String, Object>();
        controllerFields.setExpectedResult(dataMap);
        final MockHttpServletRequest request = controllerFields.getRequest();
        final MockHttpServletResponse response = controllerFields.getResponse();
        request.setRemoteAddr("127.0.0.1");
        final ThirdPartyServiceAccount tpsa = new ThirdPartyServiceAccount();
        tpsa.setId(1);
        tpsa.setUserName("joeblow");
        tpsa.setPassword("heynow");
        TestDBMap.addToMemory(TestConstants.THIRD_PARTY_SERVICE_ACCT, tpsa);
        
        when(this.emsPropertiesService.getPropertyValueAsInt(anyString(), anyInt())).thenAnswer(EmsPropertiesServiceMockImpl.getPropertyValueAsInt());
        when(this.extensiblePermitService.getSmsAlertByParkerReply(Mockito.anyString()))
                .thenAnswer(ExtensiblePermitServiceMockImpl.getSmsAlertByParkerReply());
        when(this.extensiblePermitService.findByMobileNumber(anyString())).thenAnswer(ExtensiblePermitServiceMockImpl.findByMobileNumber());
        when(this.cryptoService.decryptData(anyString())).thenAnswer(CryptoServiceMockImpl.decryptData());
        when(this.extensibleRateAndPermissionService.getSmsRateInfo((SmsAlertInfo) anyObject()))
                .thenAnswer(ExtensibleRateAndPermissionServiceMockImpl.getSmsRateInfo());
        when(this.processorTransactionService.findProcessorTransactionById(Mockito.anyLong(), Mockito.anyBoolean())).thenCallRealMethod();
        Mockito.doCallRealMethod().when(this.processorTransactionService).saveProcessorTransaction(Mockito.any());
        
        when(this.processorTransactionService.findProcTransByPurchaseIdTicketNoAndPurchasedDate(Mockito.anyLong(), Mockito.any(), Mockito.anyInt()))
                .then(new Answer<ProcessorTransaction>() {
                    
                    @Override
                    public ProcessorTransaction answer(final InvocationOnMock invocation) throws Throwable {
                        final List<ProcessorTransaction> transactions = DBHelper.list(ProcessorTransaction.class);
                        for (ProcessorTransaction procTrans : transactions) {
                            if (invocation.getArgumentAt(0, long.class).equals(procTrans.getPurchase().getId())
                                && invocation.getArgumentAt(2, int.class).equals(procTrans.getTicketNumber())) {
                                return procTrans;
                            }
                        }
                        return null;
                    }
                    
                });
        
        Mockito.doAnswer(new Answer<ActivePermit>() {
            
            @Override
            public ActivePermit answer(final InvocationOnMock invocation) throws Throwable {
                final List<ActivePermit> permits = DBHelper.list(ActivePermit.class);
                if (permits != null && !permits.isEmpty()) {
                    return permits.get(0);
                } else {
                    return null;
                }
            }
            
        }).when(this.activePermitService).findActivePermitByOriginalPermitId(Mockito.anyLong());
        
        final CardTransactionResponseGenerator cpsResponse = new CardTransactionResponseGenerator();
        cpsResponse.setStatus(new Status(StandardConstants.STRING_STATUS_CODE_ACCEPTED));
        
        when(this.smsService.sendReplyRequest(Mockito.anyString(), Mockito.anyString())).thenReturn("");
        when(this.cpsProcessor.processAddOn((CPSTransactionDto) anyObject())).thenReturn(cpsResponse);
        when(this.kpiListenerService.invoke(Mockito.any(), Mockito.any(), Mockito.any())).thenThrow(NoSuchMethodException.class);
        when(this.messageAccessor.getMessage(Mockito.anyString())).thenReturn("");
        
        this.smsHttpPostCallbackServlet.setSmsAlertService(this.smsAlertService);
        this.smsHttpPostCallbackServlet.setExtensiblePermitService(this.extensiblePermitService);
        this.smsHttpPostCallbackServlet.setProcessorTransactionService(this.processorTransactionService);
        this.smsHttpPostCallbackServlet.setEmsPropertiesService(this.emsPropertiesService);
        this.smsHttpPostCallbackServlet.setSmsPermitAlertJobService(this.smsPermitAlertJobService);
        this.smsHttpPostCallbackServlet.setExtensibleRateAndPermissionService(this.extensibleRateAndPermissionService);
        this.smsHttpPostCallbackServlet.setCardProcessingManager(this.cardProcessingManager);
        this.smsHttpPostCallbackServlet.setPermitService(this.permitService);
        this.smsHttpPostCallbackServlet.setPurchaseService(this.purchaseService);
        this.smsHttpPostCallbackServlet.setActivePermitService(this.activePermitService);
        this.smsHttpPostCallbackServlet.setSmsService(this.smsService);
        this.smsHttpPostCallbackServlet.setMerchantAccountService(this.merchantAccountService);
        this.smsHttpPostCallbackServlet.doPost(request, response);
        
    }
    
    private void assertParkingSessionExtended() {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final String mobileNumAsString = (String) controllerFields.getRequest().getParameter(TestConstants.SMS_REQ_SENDER_PARAM_NAME);
        final SmsAlertInfo smsAlertInfo = DBHelper.findOne(SmsAlertInfo.class, SmsAlertInfo.ALIAS_MOBILE_NUMBER, mobileNumAsString.trim());
        
        if (smsAlertInfo == null) {
            Assert.fail();
        } else {
            final List<ActivePermit> listAp = DBHelper.list(ActivePermit.class);
            
            if (listAp == null || listAp.isEmpty()) {
                Assert.fail();
            } else if (listAp.get(0) == null) {
                Assert.fail();
            } else {
                Assert.assertTrue(smsAlertInfo.getExpiryDate().after(new Date()));
                verifyUpdateCalled(1);
            }
        }
    }
    
    private void assertParkingSessionNotExtended() {
        verifyUpdateCalled(0);
    }
    
    // This method is called when the charge is successful 
    private void verifyUpdateCalled(final int times) {
        Mockito.verify(this.extensiblePermitService, Mockito.times(times))
                .updateSmsAlertAndLatestExpiryDate(Mockito.anyBoolean(), Mockito.anyInt(), Mockito.anyBoolean(), Mockito.any(), Mockito.any(),
                                                   Mockito.anyBoolean(), Mockito.any());
    }
    
    @Override
    @SuppressWarnings({ "PMD.AvoidPrintStackTrace", "checkstyle:illegalcatch" })
    public final Object performAction(final Object... objects) {
        try {
            doPost();
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... objects) {
        assertParkingSessionExtended();
        return null;
    }
    
    @Override
    public final Object assertFailure(final Object... objects) {
        assertParkingSessionNotExtended();
        return null;
    }
}
