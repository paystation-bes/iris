package com.digitalpaytech.bdd.scheduling.queue.alert.impl;

import javax.mail.MessagingException;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.dto.queue.QueueNotificationEmail;
import com.digitalpaytech.scheduling.queue.alert.impl.NotificationEmailProcessorImpl;
import com.digitalpaytech.service.EmailService;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.dto.Email;

@SuppressWarnings("PMD.BeanMembersShouldSerialize")
public class TestAlertEmailNotification implements JBehaveTestHandler {
    
    @Mock
    private EmailService emailService;
    
    @InjectMocks
    private NotificationEmailProcessorImpl notificationEmailProcessor;
    
    private final void sendEmail() {
        initializeMocks();
        QueueNotificationEmail queueNotificationEmail = null;
        do {
            queueNotificationEmail =
                    (QueueNotificationEmail) TestDBMap.getTestEventQueueFromMem().pollQueue(WebCoreConstants.QUEUE_TYPE_NOTIFICATION_EMAIL);
                    
            if (queueNotificationEmail != null) {
                this.notificationEmailProcessor.sendNotificationEmail(queueNotificationEmail);
                
            }
        } while (queueNotificationEmail != null);
    }
    
    private final void assertEmailSent(final String n) {
        final Integer nEmail = Integer.valueOf(n);
        try {
            Mockito.verify(this.emailService, Mockito.times(nEmail)).sendNow(Mockito.any(Email.class));
        } catch (MessagingException me) {
            me.printStackTrace();
        }
    }
    
    private final void assertEmailNotSent() {
        Mockito.verifyZeroInteractions(this.emailService);
    }
    
    private void initializeMocks() {
        MockitoAnnotations.initMocks(this);
        try {
            Mockito.doAnswer(new Answer<Object>() {
                @Override
                public Object answer(final InvocationOnMock invocation) throws Throwable {
                    return null;
                }
            }).when(this.emailService).sendNow(Mockito.any(Email.class));
        } catch (MessagingException me) {
            me.printStackTrace();
        }
        
    }
    
    @Override
    public Object performAction(Object... objects) {
        sendEmail();
        return null;
    }
    
    @Override
    public Object assertSuccess(Object... objects) {
        String nEmail = (String) objects[0];
        assertEmailSent(nEmail);
        return null;
    }
    
    @Override
    public Object assertFailure(Object... objects) {
        assertEmailNotSent();
        return null;
    }
}
