package com.digitalpaytech.bdd.scheduling.task;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.AlertSteps;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

//Because we're using Kafka for events, these test stories wouldn't work anymore.
@org.junit.Ignore
@RunWith(JUnitReportingRunner.class)
public class ClearAutoclearEventsStories extends AbstractStories {
    
    public ClearAutoclearEventsStories() {
        super();
        this.testHandlers.registerTestHandler("unwantedalarmsremove", TestRemoveUnwantedAlarms.class);
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    // Here we specify the steps classes
    @Override
    public final InjectableStepsFactory stepsFactory() {
        // varargs, can have more that one steps classes
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new AlertSteps(this.testHandlers));
    }
    
}
