package com.digitalpaytech.bdd.scheduling.task.kafka.consumer;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.dto.cps.TerminalMessage;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.scheduling.task.kafka.consumer.MerchantAccountUpdateTask;
import com.digitalpaytech.service.impl.CustomerServiceImpl;
import com.digitalpaytech.service.impl.MailerServiceImpl;
import com.digitalpaytech.service.impl.EmsPropertiesServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.PaystationSettingServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.ProcessorServiceImpl;
import com.digitalpaytech.service.paystation.impl.PosServiceStateServiceImpl;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.util.json.JSONConfigurationBean;
import com.digitalpaytech.util.kafka.impl.MessageConsumerFactoryImpl;
import com.digitalpaytech.util.kafka.impl.MessageHeadersServiceImpl;
import com.fasterxml.jackson.core.type.TypeReference;

@SuppressWarnings({ "PMD.ImmutableField", "unchecked", "PMD.UnusedPrivateField" })
public class TestMerchantAccountUpdateConsumer implements JBehaveTestHandler {
    
    private static final String TOPIC = "terminal_updates";
    
    @Mock
    private KafkaConsumer<String, String> mockConsumer;
    
    @Mock
    private MessageConsumerFactoryImpl messageConsumerFactory;
    
    @Mock
    private MessageHelper messageHelper;
    
    @SuppressWarnings("unused")
    private EntityDao entityDao = new EntityDaoTest();
    
    @InjectMocks
    private EmsPropertiesServiceImpl emsPropertiesService;
    
    @InjectMocks
    private PaystationSettingServiceImpl paystationSettingService;
    
    @InjectMocks
    private CustomerServiceImpl customerService;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    private PosServiceStateServiceImpl posServiceStateService;
    
    @InjectMocks
    private MerchantAccountUpdateTask merchantAccountUpdateTask;
    
    @InjectMocks
    private MessageHeadersServiceImpl messageHeadersService;
    
    @InjectMocks
    private MerchantAccountServiceImpl merchantAccountService;
    
    @InjectMocks
    private ProcessorServiceImpl processorService;
    
    @InjectMocks
    private MailerServiceImpl mailerService;
    
    private TypeReference<List<TerminalMessage>> terminalMessagesType = new TypeReference<List<TerminalMessage>>() {
    };
    
    public TestMerchantAccountUpdateConsumer() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(this.messageConsumerFactory.buildConsumer(Mockito.any(), Mockito.anyCollection(), Mockito.anyString()))
                .thenReturn(this.mockConsumer);
        Mockito.when(this.messageHelper.getMessageWithDefault(Mockito.anyString(), Mockito.anyString())).thenAnswer(new Answer<String>() {
            
            @Override
            public String answer(final InvocationOnMock invocation) throws Throwable {
                return invocation.getArgumentAt(1, String.class);
            }
            
        });
        
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public final Object assertFailure(final Object... arg0) {
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... arg0) {
        return null;
    }
    
    @Override
    public final Object performAction(final Object... arg0) {
        final JSON json = new JSON(new JSONConfigurationBean(true, false, false));
        this.messageConsumerFactory.setJSON(json);
        
        final List<ConsumerRecord<String, String>> recList = new ArrayList<>();
        try {
            final List<TerminalMessage> messages = json.deserialize((String) arg0[0], this.terminalMessagesType);
            for (TerminalMessage nd : messages) {
                
                @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
                final ConsumerRecord<String, String> record = new ConsumerRecord<String, String>(TOPIC, 0, 0L, null, json.serialize(nd));
                recList.add(record);
            }
            
        } catch (JsonException e) {
            final ConsumerRecord<String, String> record = new ConsumerRecord<String, String>(TOPIC, 0, 0L, "unknown", (String) arg0[0]);
            recList.add(record);
        }
        
        @SuppressWarnings("PMD.UseConcurrentHashMap")
        final Map<TopicPartition, List<ConsumerRecord<String, String>>> recordMap = new HashMap<>();
        final TopicPartition topicPartition = new TopicPartition(TOPIC, 0);
        
        recordMap.put(topicPartition, recList);
        
        final ConsumerRecords<String, String> records = new ConsumerRecords<String, String>(recordMap);
        Mockito.when(this.mockConsumer.poll(Mockito.any(Duration.class))).thenReturn(records);
        
        this.merchantAccountUpdateTask.process();
        return null;
    }
    
}
