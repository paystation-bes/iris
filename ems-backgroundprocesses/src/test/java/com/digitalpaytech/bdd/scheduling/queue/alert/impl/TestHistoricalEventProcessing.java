package com.digitalpaytech.bdd.scheduling.queue.alert.impl;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import java.util.Collection;

import junit.framework.Assert;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.services.CustomerAlertTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.EventActionTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.EventSeverityTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.EventTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.PointOfSaleServiceMockImpl;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.bdd.util.expression.StoryObjectBuilder;
import com.digitalpaytech.domain.PosEventHistory;
import com.digitalpaytech.dto.queue.QueueEvent;
import com.digitalpaytech.scheduling.queue.alert.impl.PosEventHistoryProcessorImpl;
import com.digitalpaytech.service.CustomerAlertTypeService;
import com.digitalpaytech.service.EventActionTypeService;
import com.digitalpaytech.service.EventSeverityTypeService;
import com.digitalpaytech.service.EventTypeService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.PosEventHistoryService;
import com.digitalpaytech.util.WebCoreConstants;

@SuppressWarnings({ "PMD.BeanMembersShouldSerialize", "PMD.ExcessiveImports", "PMD.JUnit4TestShouldUseTestAnnotation" })
public class TestHistoricalEventProcessing implements JBehaveTestHandler {
    
    @Mock
    private EventActionTypeService eventActionTypeService;
    
    @Mock
    private CustomerAlertTypeService customerAlertTypeService;
    
    @Mock
    private EventSeverityTypeService eventSeverityTypeService;
    
    @Mock
    private EventTypeService eventTypeService;
    
    @Mock
    private PointOfSaleService pointOfSaleService;
    
    @Mock
    private PosEventHistoryService posEventHistoryService;
    
    @InjectMocks
    private PosEventHistoryProcessorImpl posEventHistoryProcessor;
    
    private final void testHistoricalEvent() {
        MockitoAnnotations.initMocks(this);
        initializeMocks();
        
        QueueEvent queueEvent = null;
        do {
            queueEvent = (QueueEvent) TestDBMap.getTestEventQueueFromMem().pollQueue(WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT);
            if (queueEvent != null) {
                this.posEventHistoryProcessor.savePosEventHistory(queueEvent);
            }
        } while (queueEvent != null);
    }
    
    private final void testHistoricalQueueEventSaved(final String dataToAssert) {
        @SuppressWarnings("unchecked")
        final Collection<PosEventHistory> posEventHistoryCollection =
                (Collection<PosEventHistory>) TestDBMap.getTypeListFromMemory(PosEventHistory.class);
                
        TestLookupTables.getExpressionParser().assertExisted("Expected POSEventHistory was not found", posEventHistoryCollection, dataToAssert);
        
    }
    
    private void initializeMocks() {
        
        when(this.customerAlertTypeService.findCustomerAlertTypeById(anyInt())).then(CustomerAlertTypeServiceMockImpl.findCustomerAlertTypeById());
        when(this.customerAlertTypeService.findCustomerAlertTypesByCustomerIdAlertThresholdTypeIdAndIsActive(anyInt(), anyInt(), anyBoolean()))
        .thenAnswer(CustomerAlertTypeServiceMockImpl.findCustomerAlertTypesByCustomerIdAlertThresholdTypeIdAndIsActive());
        
        when(this.eventActionTypeService.findEventActionType(anyInt())).then(EventActionTypeServiceMockImpl.findEventActionType());
        when(this.eventSeverityTypeService.findEventSeverityType(anyInt())).then(EventSeverityTypeServiceMockImpl.findEventSeverityType());
        when(this.eventTypeService.findEventTypeById(Mockito.anyByte())).then(EventTypeServiceMockImpl.findEventTypeById());
        when(this.pointOfSaleService.findPointOfSaleById(anyInt())).then(PointOfSaleServiceMockImpl.findPointOfSaleById());
        Mockito.doAnswer(new Answer<Void>() {
            
            @Override
            public Void answer(final InvocationOnMock invocation) throws Throwable {
                final PosEventHistory posHistoryEvent = (PosEventHistory) invocation.getArguments()[0];
                TestContext.getInstance().getDatabase().save(posHistoryEvent);
                return null;
            }
        }).when(this.posEventHistoryService).savePosEventHistory(Mockito.any(PosEventHistory.class));
    }
    
    @Override
    public Object performAction(Object... objects) {
        testHistoricalEvent();
        return null;
    }
    
    @Override
    public Object assertSuccess(Object... objects) {
        testHistoricalQueueEventSaved((String) objects[0]);
        return null;
    }
    
    @Override
    public Object assertFailure(Object... objects) {
        // TODO Auto-generated method stub
        return null;
    }
}
