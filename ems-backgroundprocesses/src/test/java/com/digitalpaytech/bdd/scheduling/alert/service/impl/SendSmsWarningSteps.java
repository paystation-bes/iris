package com.digitalpaytech.bdd.scheduling.alert.service.impl;

import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.Given;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;

import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandlers;
import com.digitalpaytech.bdd.util.TestConstants;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.domain.ActivePermit;
import com.digitalpaytech.domain.ExtensiblePermit;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.dto.SmsAlertInfo;
import com.digitalpaytech.util.StandardConstants;

public final class SendSmsWarningSteps extends AbstractSteps {
    
    public SendSmsWarningSteps(JBehaveTestHandlers testHandlers) {
        super(testHandlers);
    }
    
    protected final Class<?>[] getStoryObjectTypes() {
        return new Class[] { Permit.class, Purchase.class, ExtensiblePermit.class, SmsAlertInfo.class, ActivePermit.class };
    }
    
    @BeforeScenario
    public final void setUpForController() {
        final ControllerFieldsWrapper controllerFields = new ControllerFieldsWrapper();
        controllerFields.setRequest(new MockHttpServletRequest());
        controllerFields.setResponse(new MockHttpServletResponse());
        controllerFields.setSession(new MockHttpSession());
        controllerFields.getRequest().setSession(controllerFields.getSession());
        TestDBMap.saveControllerFieldsWrapperToMem(controllerFields);
        TestLookupTables.getExpressionParser().register(getStoryObjectTypes());
    }
    
    /**
     * Constructs parking extension request objects and inject them into the database
     * To be used when the creation of the object is not being tested
     * 
     * *** This is only for send_sms_warning_stories.story ***
     * 
     * @.example a parking extension request is sent where the<br>
     *           data is 30<br>
     *           sender is 2522839938<br>
     * @param data
     *            the data to assign to the object
     */
    @Given("a parking extension request is sent where the $data")
    public final void addParkerRequestToMem(final String data) {
        
        final String[] lines = data.split(System.getProperty(StandardConstants.SYSTEM_NEW_LINE));
        final String extensionTime = lines[0].substring(TestConstants.DATA_IS_OFFSET);
        final String mobileNumber = lines[1].substring(TestConstants.SENDER_IS_OFFSET);
        final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
        
        final MockHttpServletRequest request = wrapperObject.getRequest();
        request.addParameter(TestConstants.SMS_REQ_DATA_PARAM_NAME, extensionTime);
        request.addParameter(TestConstants.SMS_REQ_SENDER_PARAM_NAME, mobileNumber);
        request.addParameter(TestConstants.SMS_REQ_NAME_PARAM_NAME, TestConstants.SMS_USER_NAME);
    }
}
