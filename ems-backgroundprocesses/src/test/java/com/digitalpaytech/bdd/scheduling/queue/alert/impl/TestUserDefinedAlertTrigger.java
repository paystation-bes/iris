package com.digitalpaytech.bdd.scheduling.queue.alert.impl;

import org.hibernate.Session;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.digitalpaytech.bdd.services.AlertsServiceMockImpl;
import com.digitalpaytech.bdd.services.CustomerAlertTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.EntityDaoServiceMockImpl;
import com.digitalpaytech.bdd.services.PosEventCurrentServiceMockImpl;
import com.digitalpaytech.bdd.services.QueueEventPublisherMockImpl;
import com.digitalpaytech.bdd.services.UserDefinedEventInfoServiceMockImpl;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.dto.queue.QueueCustomerAlertType;
import com.digitalpaytech.dto.queue.QueueEvent;
import com.digitalpaytech.scheduling.queue.alert.CustomerAlertTypeEventProcessor;
import com.digitalpaytech.scheduling.queue.alert.impl.CustomerAlertTypeEventProcessorImpl;
import com.digitalpaytech.service.AlertsService;
import com.digitalpaytech.service.CustomerAlertTypeService;
import com.digitalpaytech.service.PosAlertStatusService;
import com.digitalpaytech.service.PosEventCurrentService;
import com.digitalpaytech.service.ServiceHelper;
import com.digitalpaytech.service.UserDefinedEventInfoService;
import com.digitalpaytech.service.queue.impl.KafkaEventServiceImpl;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.queue.QueuePublisher;

@SuppressWarnings({ "PMD.BeanMembersShouldSerialize", "PMD.ExcessiveImports", "PMD.UnusedPrivateField", "PMD.JUnit4TestShouldUseTestAnnotation", })
public class TestUserDefinedAlertTrigger implements JBehaveTestHandler {
    
    @Mock
    private EntityDao entityDao;
    
    @Mock
    private AlertsService alertsService;
    
    @Mock
    private CustomerAlertTypeService customerAlertTypeService;
    
    @Mock
    private UserDefinedEventInfoService userDefinedEventInfoService;
    
    @Mock
    private PosEventCurrentService posEventCurrentService;
    
    @Mock
    private PosAlertStatusService posAlertStatusService;
    
    @InjectMocks
    private CustomerAlertTypeEventProcessorImpl customerAlertTypeEventProcessor;
    
    @Mock
    private QueuePublisher<QueueEvent> recentEventsQueuePublisher;
    
    @Mock
    private QueuePublisher<QueueEvent> historicalEventsQueuePublisher;
    
    @Mock
    private QueuePublisher<QueueEvent> processingEventsQueuePublisher;
    
    @InjectMocks
    private KafkaEventServiceImpl queueEventService;
    
    @Mock
    private ServiceHelper serviceHelper;
    
    private void testRecieveTrigger() {
        
        MockitoAnnotations.initMocks(this);
        QueueCustomerAlertType qcat = null;
        do {
            qcat = (QueueCustomerAlertType) TestDBMap.getTestEventQueueFromMem().pollQueue(WebCoreConstants.QUEUE_TYPE_CUSTOMER_ALERT_TYPE);
            if (qcat != null) {
                initalizeMocks(qcat);
                this.customerAlertTypeEventProcessor.setQueueEventService(this.queueEventService);
                this.customerAlertTypeEventProcessor.init();
                this.customerAlertTypeEventProcessor.handle(qcat);
            }
        } while (qcat != null);
        
    }
    
    @SuppressWarnings("unchecked")
    private void initalizeMocks(final QueueCustomerAlertType queueCustomerAlertType) {
        final Session session = Mockito.mock(Session.class);
        Mockito.when(this.entityDao.getCurrentSession()).thenReturn(session);
        Mockito.when(this.customerAlertTypeService.findCustomerAlertTypeByIdList(Mockito.anyList())).then(CustomerAlertTypeServiceMockImpl
                                                                                                                  .findCustomerAlertTypeByIdList());
        Mockito.when(this.posEventCurrentService.findActiveDefaultPosEventCurrentByPosIdListAndAttIdList(Mockito.eq(queueCustomerAlertType
                             .getPointOfSaleIds()), Mockito.anyList()))
                .thenAnswer(PosEventCurrentServiceMockImpl.findActiveDefaultPosEventCurrentByPosIdListAndAttIdList());
        Mockito.when(this.alertsService.findAlertThresholdTypeByAlertClassTypeId(Mockito.anyByte()))
                .then(AlertsServiceMockImpl.findAlertThresholdTypeByAlertClassTypeId());
        Mockito.when(this.customerAlertTypeService.findCustomerAlertTypeById(Mockito.anyInt())).thenAnswer(CustomerAlertTypeServiceMockImpl
                                                                                                                   .findCustomerAlertTypeById());
        Mockito.when(this.userDefinedEventInfoService.findEventsByNamedQueryAndPointOfSaleIdListAndCustomerAlertTypeId(Mockito.anyString(),
                                                                                                                       Mockito.anyList(),
                                                                                                                       Mockito.anyInt()))
                .thenAnswer(UserDefinedEventInfoServiceMockImpl.findEventsByNamedQueryAndPointOfSaleIdListAndCustomerAlertTypeId());
        Mockito.when(this.userDefinedEventInfoService.findEventsByNamedQueryAndPointOfSaleIdList(Mockito.anyString(), Mockito.anyList()))
                .thenAnswer(UserDefinedEventInfoServiceMockImpl.findEventsByNamedQueryAndPointOfSaleIdList());
        Mockito.doAnswer(PosEventCurrentServiceMockImpl.savePosEventCurrent()).when(this.posEventCurrentService)
                .savePosEventCurrent(Mockito.any(PosEventCurrent.class));
        Mockito.doAnswer(PosEventCurrentServiceMockImpl.findDefaultPosEventCurrentsToDisable()).when(this.posEventCurrentService)
                .findDefaultPosEventCurrentsToDisable(Mockito.anyList());
        Mockito.doAnswer(PosEventCurrentServiceMockImpl.findDefaultPosEventCurrentsToEnable()).when(this.posEventCurrentService)
                .findDefaultPosEventCurrentsToEnable(Mockito.anyList());
        Mockito.doAnswer(PosEventCurrentServiceMockImpl.findPosEventCurrentsByIdList()).when(this.posEventCurrentService)
                .findPosEventCurrentsByIdList(Mockito.anyList());
        
        Mockito.doAnswer(QueueEventPublisherMockImpl.offer(WebCoreConstants.QUEUE_TYPE_RECENT_EVENT)).when(this.recentEventsQueuePublisher)
                .offer(Mockito.any(QueueEvent.class));
        Mockito.doAnswer(QueueEventPublisherMockImpl.offer(WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT)).when(this.historicalEventsQueuePublisher)
                .offer(Mockito.any(QueueEvent.class));
        Mockito.doAnswer(CustomerAlertTypeServiceMockImpl.findDefaultCustomerAlertTypeByCustomerIdAndAlertThresholdTypeId())
                .when(this.customerAlertTypeService)
                .findDefaultCustomerAlertTypeByCustomerIdAndAlertThresholdTypeId(Mockito.anyInt(), Mockito.anyInt());
        Mockito.doAnswer(EntityDaoServiceMockImpl.delete()).when(this.entityDao).delete(Mockito.anyObject());
        Mockito.when(this.entityDao.findByNamedQueryAndNamedParam(Mockito.anyString(), Mockito.any(String[].class), Mockito.any(Object[].class)))
                .then(EntityDaoServiceMockImpl.scrollAlerts());
        Mockito.when(this.userDefinedEventInfoService.findEventsClassTypeAndLimit(Mockito.any(Class.class), Mockito.anyInt()))
                .thenAnswer(UserDefinedEventInfoServiceMockImpl.findEventsClassTypeAndLimit());
        
        Mockito.when(this.serviceHelper.bean(Mockito.anyString(), Mockito.eq(CustomerAlertTypeEventProcessor.class)))
                .thenReturn(this.customerAlertTypeEventProcessor);
        
    }
    
    @Override
    public final Object performAction(final Object... objects) {
        testRecieveTrigger();
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... objects) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Object assertFailure(final Object... objects) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
