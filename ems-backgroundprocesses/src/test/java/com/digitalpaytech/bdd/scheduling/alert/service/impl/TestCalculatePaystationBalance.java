package com.digitalpaytech.bdd.scheduling.alert.service.impl;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.digitalpaytech.bdd.services.CollectionTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.QueueEventPublisherMockImpl;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.dto.queue.QueueCustomerAlertType;
import com.digitalpaytech.scheduling.alert.service.impl.RunningTotalJobServiceImpl;
import com.digitalpaytech.service.AlertsService;
import com.digitalpaytech.service.CollectionTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.UserDefinedEventInfoService;
import com.digitalpaytech.service.queue.impl.KafkaCustomerAlertTypeServiceImpl;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.queue.QueuePublisher;

public class TestCalculatePaystationBalance implements JBehaveTestHandler {
    
    @Mock
    private CollectionTypeService collectionTypeService;
    
    @Mock
    private EmsPropertiesService emsPropertiesService;
    
    @Mock
    private AlertsService alertsService;
    
    @Mock
    private UserDefinedEventInfoService userDefinedEventInfoService;
    @Mock
    private QueuePublisher<QueueCustomerAlertType> customerAlertTypeQueuePublisher;
    
    @InjectMocks
    private KafkaCustomerAlertTypeServiceImpl queueCustomerAlertTypeService;
    
    @InjectMocks
    private RunningTotalJobServiceImpl runningTotalJobServiceImpl;
    
    public final void calculatePaystationBalance() {
        MockitoAnnotations.initMocks(this);
        this.runningTotalJobServiceImpl.setQueueCustomerAlertTypeService(this.queueCustomerAlertTypeService);
        
        when(this.collectionTypeService.calculatePaystationBalance(anyInt(), anyInt(), anyInt())).thenAnswer(CollectionTypeServiceMockImpl
                                                                                                                     .calculatePaystationBalance());
        
        Mockito.doAnswer(QueueEventPublisherMockImpl.offer(WebCoreConstants.QUEUE_TYPE_CUSTOMER_ALERT_TYPE))
                .when(this.customerAlertTypeQueuePublisher).offer(Mockito.any(QueueCustomerAlertType.class));
        
        this.runningTotalJobServiceImpl.calculatePaystationBalance(0, 0, 0);
    }

    @Override
    public Object performAction(Object... objects) {
        calculatePaystationBalance();
        return null;
    }

    @Override
    public Object assertSuccess(Object... objects) {
        return null;
    }

    @Override
    public Object assertFailure(Object... objects) {
        return null;
    }
}
