package com.digitalpaytech.bdd.scheduling.alert.service.impl;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.scheduling.alert.service.impl.smspermitalertjobservice.TestManageExtensiblePermit;
import com.digitalpaytech.bdd.scheduling.alert.support.servlet.upsidewireless.TestDoPost;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.RibbonClientBaseSteps;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.client.CoreCPSClient;
import com.digitalpaytech.client.PaymentClient;
import com.digitalpaytech.domain.ActivePermit;
import com.digitalpaytech.dto.SmsAlertInfo;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class SendSmsWarningStories extends AbstractStories {
    
    public SendSmsWarningStories() {
        super();
        this.testHandlers.registerTestHandler("smsalertprocess", TestManageExtensiblePermit.class);
        this.testHandlers.registerTestHandler("smswarningmessagesent", TestManageExtensiblePermit.class);
        this.testHandlers.registerTestHandler("smswarningmessagenotsent", TestManageExtensiblePermit.class);
        this.testHandlers.registerTestHandler("extensionprocess", TestDoPost.class);
        this.testHandlers.registerTestHandler("parkingsessionextended", TestDoPost.class);
        this.testHandlers.registerTestHandler("parkingsessionnotextended", TestDoPost.class);
        
        TestContext.getInstance().getObjectParser()
                .register(new Class[] { CoreCPSClient.class, SmsAlertInfo.class, ActivePermit.class, PaymentClient.class });
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    // Here we specify the steps classes
    @Override
    public final InjectableStepsFactory stepsFactory() {
        // varargs, can have more than one steps classes
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new SendSmsWarningSteps(this.testHandlers),
                new RibbonClientBaseSteps(this.testHandlers));
    }
    
}
