package com.digitalpaytech.bdd.scheduling.alert.service.impl.smspermitalertjobservice;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import com.digitalpaytech.bdd.scheduling.alert.support.wsclient.upsidewireless.UpsidewirelessServiceClientMockImpl;
import com.digitalpaytech.bdd.services.EmsPropertiesServiceMockImpl;
import com.digitalpaytech.bdd.services.ExtensibleRateAndPermissionServiceMockImpl;
import com.digitalpaytech.bdd.services.PointOfSaleServiceMockImpl;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.DBHelper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestConstants;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.ActivePermit;
import com.digitalpaytech.domain.SmsTransactionLog;
import com.digitalpaytech.dto.SmsAlertInfo;
import com.digitalpaytech.scheduling.alert.job.SmsPermitAlertJob;
import com.digitalpaytech.scheduling.alert.service.impl.SmsPermitAlertJobServiceImpl;
import com.digitalpaytech.scheduling.alert.support.wsclient.WsClient;
import com.digitalpaytech.scheduling.sms.support.SmsServiceImpl;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.ExtensibleRateAndPermissionService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.SmsTransactionLogService;
import com.digitalpaytech.service.impl.ActivePermitServiceImpl;
import com.digitalpaytech.service.impl.ExtensiblePermitServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.PermitServiceImpl;
import com.digitalpaytech.service.impl.PurchaseServiceImpl;

import junit.framework.Assert;

@SuppressWarnings({ "PMD.UnusedPrivateField", "PMD.ExcessiveImports", "PMD.TooManyStaticImports" })
public class TestManageExtensiblePermit implements JBehaveTestHandler {
    
    private static final int TIME_TO_CACHE = 500;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @Mock
    private ExtensibleRateAndPermissionService extensibleRateAndPermissionService;
    
    @InjectMocks
    private PurchaseServiceImpl purchaseService;
    
    @Mock
    private PointOfSaleService pointOfSaleService;
    
    @Mock
    private SmsTransactionLogService smsTransactionLogService;
    
    @Mock
    private EmsPropertiesService emsPropertiesService;
    
    @InjectMocks
    private ExtensiblePermitServiceImpl extensiblePermitService;
    
    @InjectMocks
    private ActivePermitServiceImpl activePermitService;
    
    @InjectMocks
    private SmsPermitAlertJobServiceImpl smsPermitAlertJobService;
    
    @InjectMocks
    private MerchantAccountServiceImpl merchantAccountService;
    
    @InjectMocks
    private PermitServiceImpl permitService;
    
    private void manageExtensiblePermit() throws IllegalAccessException {
        MockitoAnnotations.initMocks(this);
        
        ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        if (controllerFields == null) {
            controllerFields = new ControllerFieldsWrapper();
            TestDBMap.saveControllerFieldsWrapperToMem(controllerFields);
        }
        
        final Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put(TestConstants.SMS_MESSAGE, TestConstants.SMS_MESSAGE_NOT_SENT);
        controllerFields.setExpectedResult(dataMap);
        when(this.extensibleRateAndPermissionService.getSmsRateInfo((SmsAlertInfo) anyObject()))
                .thenAnswer(ExtensibleRateAndPermissionServiceMockImpl.getSmsRateInfo());
        final WsClient upsidewirelessServiceClient = Mockito.mock(WsClient.class);
        final SmsServiceImpl smsClient = Mockito.mock(SmsServiceImpl.class);
        try {
            when(upsidewirelessServiceClient.sendRequest()).thenAnswer(UpsidewirelessServiceClientMockImpl.sendRequest());
        } catch (Exception e) {
            e.printStackTrace();
        }
        when(this.emsPropertiesService.getPropertyValueAsBoolean(anyString(), anyBoolean()))
                .thenAnswer(EmsPropertiesServiceMockImpl.getPropertyValueAsBoolean());
        when(this.pointOfSaleService.findPointOfSaleById(anyInt())).thenAnswer(PointOfSaleServiceMockImpl.findPointOfSaleById());
        doNothing().when(this.smsTransactionLogService).saveSmsTransactionLog((SmsTransactionLog) anyObject());
        
        final SmsPermitAlertJob smsPermitAlertJob = new SmsPermitAlertJob();
        this.smsPermitAlertJobService.setJob(smsPermitAlertJob);
        
        final SmsAlertInfo smsAlertInfo = (SmsAlertInfo) DBHelper.list(SmsAlertInfo.class).get(0);
        
        final MessageSource messageSource = new ReloadableResourceBundleMessageSource();
        ((ReloadableResourceBundleMessageSource) messageSource).setCacheSeconds(TIME_TO_CACHE);
        ((ReloadableResourceBundleMessageSource) messageSource).setDefaultEncoding("UTF-8");
        ((ReloadableResourceBundleMessageSource) messageSource).setFallbackToSystemLocale(true);
        ((ReloadableResourceBundleMessageSource) messageSource).setUseCodeAsDefaultMessage(true);
        ((ReloadableResourceBundleMessageSource) messageSource).setBasenames("classpath: message");
        
        final MessageSourceAccessor messageAccessor = new MessageSourceAccessor(messageSource);
        
        TestContext.getInstance().autowiresFromTestObject(this);
        this.permitService.setEntityDao(this.entityDao);
        
        this.smsPermitAlertJobService.setMessageAccessor(messageAccessor);
        this.smsPermitAlertJobService.manageExtensiblePermit(smsAlertInfo);
        
    }
    
    private void assertSmsWarningMessageSentOrNotSent() {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final Map<String, Object> dataMap = (Map<String, Object>) controllerFields.getExpectedResult();
        final String smsMessage = (String) dataMap.get(TestConstants.SMS_MESSAGE);
        final Long permitId = (Long) TestDBMap.findObjectByIdFromMemory(TestConstants.PERMIT_ID, Long.class);
        ActivePermit ap = null;
        if (permitId != null) {
            ap = (ActivePermit) TestDBMap.findObjectByIdFromMemory(permitId, ActivePermit.class);
        }
        
        if (ap == null) {
            Assert.assertEquals(smsMessage, TestConstants.SMS_MESSAGE_NOT_SENT);
            return;
        }
        Assert.assertEquals(smsMessage, TestConstants.SMS_MESSAGE_SENT);
    }
    
    @Override
    public Object performAction(final Object... objects) {
        try {
            manageExtensiblePermit();
        } catch (IllegalAccessException e) {
            Assert.fail(e.getMessage());
        }
        return null;
    }
    
    @Override
    public Object assertSuccess(final Object... objects) {
        assertSmsWarningMessageSentOrNotSent();
        return null;
    }
    
    @Override
    public Object assertFailure(final Object... objects) {
        assertSmsWarningMessageSentOrNotSent();
        return null;
    }
    
}
