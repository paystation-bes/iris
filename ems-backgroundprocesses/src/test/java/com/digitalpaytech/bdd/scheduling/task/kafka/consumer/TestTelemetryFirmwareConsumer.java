package com.digitalpaytech.bdd.scheduling.task.kafka.consumer;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.scheduling.task.kafka.consumer.TelemetryFirmwareConsumerTask;
import com.digitalpaytech.scheduling.task.support.TelemetryFirmwareNotification;
import com.digitalpaytech.service.impl.CustomerServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.paystation.impl.PosServiceStateServiceImpl;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.util.json.JSONConfigurationBean;
import com.digitalpaytech.util.kafka.impl.MessageConsumerFactoryImpl;
import com.fasterxml.jackson.core.type.TypeReference;

@SuppressWarnings("unchecked")
public class TestTelemetryFirmwareConsumer implements JBehaveTestHandler {
    private static final String TOPIC = "ota.device.notification";
    
    @Mock
    private KafkaConsumer<String, String> mockConsumer;
    
    @Mock
    private MessageConsumerFactoryImpl messageConsumerFactory;
    
    @SuppressWarnings("unused")
    private EntityDao entityDao = new EntityDaoTest();
    
    @InjectMocks
    private CustomerServiceImpl customerService;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    private PosServiceStateServiceImpl posServiceStateService;
    
    @InjectMocks
    private TelemetryFirmwareConsumerTask telemetryFirmwareConsumerTask;
    
    private List<String> serialNumbers = new ArrayList<>();
    
    private TypeReference<List<TelemetryFirmwareNotification>> tmnListType = new TypeReference<List<TelemetryFirmwareNotification>>() {
    };
    
    public TestTelemetryFirmwareConsumer() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(this.messageConsumerFactory.buildConsumer(Mockito.any(), Mockito.anyCollection(), Mockito.anyString()))
                .thenReturn(this.mockConsumer);
        
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public final Object assertFailure(final Object... arg0) {
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... arg0) {
        return null;
    }
    
    @Override
    public final Object performAction(final Object... arg0) {
        final JSON json = new JSON(new JSONConfigurationBean(true, false, false));
        this.messageConsumerFactory.setJSON(json);
        
        final List<ConsumerRecord<String, String>> recList = new ArrayList<>();
        try {
            final List<TelemetryFirmwareNotification> notifications = json.deserialize((String) arg0[0], this.tmnListType);
            for (TelemetryFirmwareNotification nd : notifications) {
                this.serialNumbers.add(nd.getDeviceSerialNumber());
                final ConsumerRecord<String, String> record =
                        new ConsumerRecord<String, String>(TOPIC, 0, 0L, nd.getDeviceSerialNumber(), json.serialize(nd));
                recList.add(record);
            }
            
        } catch (JsonException e) {
            final ConsumerRecord<String, String> record = new ConsumerRecord<String, String>(TOPIC, 0, 0L, "unknown", (String) arg0[0]);
            recList.add(record);
        }
        
        final Map<TopicPartition, List<ConsumerRecord<String, String>>> recordMap = new HashMap<>();
        final TopicPartition topicPartition = new TopicPartition(TOPIC, 0);
        
        recordMap.put(topicPartition, recList);
        
        final ConsumerRecords<String, String> records = new ConsumerRecords<String, String>(recordMap);
        Mockito.when(this.mockConsumer.poll(Duration.ofMillis(Mockito.anyLong()))).thenReturn(records);
        
        this.telemetryFirmwareConsumerTask.process();
        return null;
    }
}
