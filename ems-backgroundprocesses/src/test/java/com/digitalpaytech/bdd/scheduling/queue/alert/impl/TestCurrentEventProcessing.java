package com.digitalpaytech.bdd.scheduling.queue.alert.impl;

import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import java.io.Serializable;

import org.hibernate.LockMode;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.digitalpaytech.bdd.services.CustomerAlertTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.EntityDaoMock;
import com.digitalpaytech.bdd.services.EventActionTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.EventSeverityTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.EventTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.MessageHelperMockImpl;
import com.digitalpaytech.bdd.services.PointOfSaleServiceMockImpl;
import com.digitalpaytech.bdd.services.PosEventCurrentServiceMockImpl;
import com.digitalpaytech.bdd.services.QueueEventPublisherMockImpl;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.dto.queue.QueueEvent;
import com.digitalpaytech.scheduling.queue.alert.impl.PosEventCurrentProcessorImpl;
import com.digitalpaytech.service.CustomerAlertTypeService;
import com.digitalpaytech.service.EventActionTypeService;
import com.digitalpaytech.service.EventSeverityTypeService;
import com.digitalpaytech.service.EventTypeService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.PosEventCurrentService;
import com.digitalpaytech.service.impl.PosAlertStatusServiceImpl;
import com.digitalpaytech.service.impl.PosEventDelayedServiceImpl;
import com.digitalpaytech.service.impl.RouteServiceImpl;
import com.digitalpaytech.service.queue.QueueEventService;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.WebCoreConstants;

@SuppressWarnings({ "PMD.BeanMembersShouldSerialize", "PMD.ExcessiveImports", "PMD.JUnit4TestShouldUseTestAnnotation", })
public class TestCurrentEventProcessing implements JBehaveTestHandler {
    
    private final TestContext ctx = TestContext.getInstance();
    
    @Mock
    private PosEventCurrentService posEventCurrentService;
    
    @Mock
    private QueueEventService queueEventService;
    
    @Mock
    private EventSeverityTypeService eventSeverityTypeService;
    
    @Mock
    private EventActionTypeService eventActionTypeService;
    
    @Mock
    private PointOfSaleService pointOfSaleService;
    
    @Mock
    private CustomerAlertTypeService customerAlertTypeService;
    
    @Mock
    private EventTypeService eventTypeService;
    
    @Mock
    private EntityDao entityDao;
    
    @Mock
    private MessageHelper messageHelper;
    
    @InjectMocks
    private PosAlertStatusServiceImpl posAlertStatusService;
    
    @InjectMocks
    private PosEventCurrentProcessorImpl currentEventProcessor;
    
    @InjectMocks
    private PosEventDelayedServiceImpl posEventDelayedService;
    
    @InjectMocks
    private RouteServiceImpl routeService;
    
    private void testReceiveQueueEvent() throws IllegalAccessException {
        MockitoAnnotations.initMocks(this);
        this.ctx.autowiresFromTestObject(this);
        this.posAlertStatusService.init();
        this.currentEventProcessor.setPosAlertStatusService(this.posAlertStatusService);
        initializeMocks();
        
        QueueEvent queueEvent = null;
        do {
            queueEvent = (QueueEvent) TestDBMap.getTestEventQueueFromMem().pollQueue(WebCoreConstants.QUEUE_TYPE_RECENT_EVENT);
            
            if (queueEvent != null) {
                this.currentEventProcessor.processQueueEvent(queueEvent);
            }
        } while (queueEvent != null);
    }
    
    @SuppressWarnings("unchecked")
    private void initializeMocks() {
        Mockito.doAnswer(EntityDaoMock.get()).when(this.entityDao).get(Mockito.any(), Mockito.any(Serializable.class));
        Mockito.doAnswer(EntityDaoMock.get()).when(this.entityDao).get(Mockito.any(), Mockito.any(Serializable.class), Mockito.any(LockMode.class));
        Mockito.doAnswer(EntityDaoMock.save()).when(this.entityDao).save(Mockito.any(Object.class));
        Mockito.doAnswer(EntityDaoMock.update()).when(this.entityDao).update(Mockito.any(Object.class));
        Mockito.doAnswer(EntityDaoMock.merge()).when(this.entityDao).merge(Mockito.any(Object.class));
        Mockito.doAnswer(EntityDaoMock.flush()).when(this.entityDao).flush();
        
        when(this.messageHelper.getMessage(Mockito.anyString())).then(MessageHelperMockImpl.getMessageEcho());
        
        when(this.posEventCurrentService.findPosEventCurrentByPointOfSaleIdAndEventTypeIdAndCustomerAlertTypeId(anyInt(), (byte) anyInt(), anyInt()))
                .then(PosEventCurrentServiceMockImpl.findPosEventCurrentByPointOfSaleIdAndEventTypeIdAndCustomerAlertTypeId());
        Mockito.doAnswer(PosEventCurrentServiceMockImpl.savePosEventCurrent()).when(this.posEventCurrentService)
                .savePosEventCurrent(Mockito.any(PosEventCurrent.class));
        Mockito.doAnswer(PosEventCurrentServiceMockImpl.updatePosEventCurrent()).when(this.posEventCurrentService)
                .updatePosEventCurrent(Mockito.any(PosEventCurrent.class));
        
        when(this.eventActionTypeService.findEventActionType(anyInt())).then(EventActionTypeServiceMockImpl.findEventActionType());
        when(this.eventSeverityTypeService.findEventSeverityType(anyInt())).then(EventSeverityTypeServiceMockImpl.findEventSeverityType());
        when(this.eventTypeService.findEventTypeById((byte) anyInt())).then(EventTypeServiceMockImpl.findEventTypeById());
        when(this.customerAlertTypeService.findCustomerAlertTypeById(anyInt())).then(CustomerAlertTypeServiceMockImpl.findCustomerAlertTypeById());
        
        when(this.customerAlertTypeService.findCustomerAlertTypesByCustomerIdAlertThresholdTypeIdAndIsActive(anyInt(), anyInt(), anyBoolean()))
                .thenAnswer(CustomerAlertTypeServiceMockImpl.findCustomerAlertTypesByCustomerIdAlertThresholdTypeIdAndIsActive());
        
        when(this.pointOfSaleService.findPointOfSaleById(anyInt())).then(PointOfSaleServiceMockImpl.findPointOfSaleById());
        
        Mockito.doAnswer(QueueEventPublisherMockImpl.offer(WebCoreConstants.QUEUE_TYPE_PROCESSING_EVENT)).when(this.queueEventService)
                .addItemToQueue(Mockito.any(QueueEvent.class), (byte) anyInt());
    }
    
    @Override
    public final Object performAction(final Object... objects) {
        try {
            testReceiveQueueEvent();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            fail();
        }
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... objects) {
        return null;
    }
    
    @Override
    public final Object assertFailure(final Object... objects) {
        return null;
    }
}
