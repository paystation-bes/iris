package com.digitalpaytech.bdd.scheduling.task;

import java.util.Date;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.digitalpaytech.bdd.services.EventExceptionServiceMockImpl;
import com.digitalpaytech.bdd.services.PosEventCurrentServiceMockImpl;
import com.digitalpaytech.bdd.services.QueueEventPublisherMockImpl;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.client.CardTypeClient;
import com.digitalpaytech.dto.queue.QueueEvent;
import com.digitalpaytech.ribbon.mock.MockClientFactory;
import com.digitalpaytech.scheduling.task.UnwantedAlarmsRemovalTask;
import com.digitalpaytech.service.EventExceptionService;
import com.digitalpaytech.service.PosEventCurrentService;
import com.digitalpaytech.service.impl.RouteServiceImpl;
import com.digitalpaytech.service.queue.impl.KafkaEventServiceImpl;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.queue.QueuePublisher;

public class TestRemoveUnwantedAlarms implements JBehaveTestHandler {
    
    private static final String HISTORICAL_EVENTS_QUEUE_PUBLISHER = "historicalEventsQueuePublisher";
    
    private static final String RECENT_EVENTS_QUEUE_PUBLISHER = "recentEventsQueuePublisher";
    
    @Mock
    private EventExceptionService eventExceptionService;
    
    @Mock
    private PosEventCurrentService posEventCurrentService;
    
    @Mock
    private KafkaEventServiceImpl queueEventService;
    
    @Mock
    private QueuePublisher<QueueEvent> recentEventsQueuePublisher;
    
    @Mock
    private QueuePublisher<QueueEvent> historicalEventsQueuePublisher;
    
    @InjectMocks
    private RouteServiceImpl routeService;
    
    @InjectMocks
    private UnwantedAlarmsRemovalTask unwantedAlarmsRemovalTask;
    
    
    public final void testRemoveUnwantedEvents() {
        initMocks();
        
        this.unwantedAlarmsRemovalTask.removeUnwantedAlarms();
    }
    
    private void initMocks() {
        
        MockitoAnnotations.initMocks(this);
        
        Mockito.when(this.posEventCurrentService.findActivePosEventCurrentByTimestamp((Date) Mockito.anyObject()))
                .thenAnswer(PosEventCurrentServiceMockImpl.findActivePosEventCurrentByTimestamp());
        
        Mockito.when(this.eventExceptionService.isException(Mockito.anyInt(), Mockito.anyInt()))
                .thenAnswer(EventExceptionServiceMockImpl.isException());
        
        Mockito.doCallRealMethod().when(this.queueEventService).addItemToQueue((QueueEvent) Mockito.anyObject(), Mockito.anyByte());
        
        Mockito.doAnswer(QueueEventPublisherMockImpl.offer(WebCoreConstants.QUEUE_TYPE_RECENT_EVENT)).when(this.recentEventsQueuePublisher)
                .offer(Mockito.any(QueueEvent.class));
        
        Mockito.doAnswer(QueueEventPublisherMockImpl.offer(WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT)).when(this.historicalEventsQueuePublisher)
                .offer(Mockito.any(QueueEvent.class));
        
        this.queueEventService.setEventExceptionService(this.eventExceptionService);
        
    }
    
    @Override
    public Object performAction(Object... objects) {
        testRemoveUnwantedEvents();
        return null;
    }
    
    @Override
    public Object assertSuccess(Object... objects) {
        return null;
    }
    
    @Override
    public Object assertFailure(Object... objects) {
        return null;
    }
}
