package com.digitalpaytech.bdd.alerts;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.AlertSteps;
import com.digitalpaytech.bdd.scheduling.queue.alert.impl.TestAlertEmailNotification;
import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.dto.queue.QueueNotificationEmail;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class HandlingNotificationEmailStories extends AbstractStories {
    
    public HandlingNotificationEmailStories() {
        super();
        this.testHandlers.registerTestHandler("recievedqueuenotificationemailnotification", TestAlertEmailNotification.class);
        this.testHandlers.registerTestHandler("alertemailsent", TestAlertEmailNotification.class);
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
        TestLookupTables.getExpressionParser().register(QueueNotificationEmail.class);
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new AlertSteps(this.testHandlers));
    }
    
}
