package com.digitalpaytech.bdd.alerts;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.AlertSteps;
import com.digitalpaytech.bdd.scheduling.queue.alert.impl.TestCurrentEventProcessing;
import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosEventCurrent;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class UpdateCurrentEventStories extends AbstractStories {
    
    public UpdateCurrentEventStories() {
        super();
        this.testHandlers.registerTestHandler("recievedqueueeventcurrentstatus", TestCurrentEventProcessing.class);
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
        TestLookupTables.getExpressionParser().register(new Class[] { PosEventCurrent.class, PointOfSale.class, });
    }
    
    // Here we specify the steps classes
    @Override
    public final InjectableStepsFactory stepsFactory() {
        // varargs, can have more that one steps classes
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new AlertSteps(this.testHandlers));
    }
}
