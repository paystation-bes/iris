package com.digitalpaytech.bdd.alerts;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.AlertSteps;
import com.digitalpaytech.bdd.scheduling.alert.service.impl.TestCalculatePaystationBalance;
import com.digitalpaytech.bdd.scheduling.queue.alert.impl.TestAlertEmailNotification;
import com.digitalpaytech.bdd.scheduling.queue.alert.impl.TestAlertProcessor;
import com.digitalpaytech.bdd.scheduling.queue.alert.impl.TestCurrentEventProcessing;
import com.digitalpaytech.bdd.scheduling.queue.alert.impl.TestHistoricalEventProcessing;
import com.digitalpaytech.bdd.scheduling.queue.alert.impl.TestUserDefinedAlertTrigger;
import com.digitalpaytech.bdd.scheduling.task.TestRemoveUnwantedAlarms;
import com.digitalpaytech.bdd.service.paystation.XMLRPCEventSteps;
import com.digitalpaytech.bdd.service.paystation.impl.eventalertserviceimpl.TestProcessEvent;
import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

//Because we're using Kafka for events, these test stories wouldn't work anymore.
@org.junit.Ignore
@RunWith(JUnitReportingRunner.class)
public class EndToEndStories extends AbstractStories {
    
    public EndToEndStories() {
        super();
        
        testHandlers.registerTestHandler("paystationeventtransmit", TestProcessEvent.class);
        testHandlers.registerTestHandler("paystationeventassert", TestProcessEvent.class);
        testHandlers.registerTestHandler("paystationeventignore", TestProcessEvent.class);
        testHandlers.registerTestHandler("unwantedalarmsremove", TestRemoveUnwantedAlarms.class);
        testHandlers.registerTestHandler("recievedqueuenotificationemailnotification", TestAlertEmailNotification.class);
        testHandlers.registerTestHandler("alertemailsent", TestAlertEmailNotification.class);
        testHandlers.registerTestHandler("alertemailnotsent", TestAlertEmailNotification.class);
        testHandlers.registerTestHandler("recievedqueueeventhistoricalalertprocessing", TestHistoricalEventProcessing.class);
        testHandlers.registerTestHandler("recievedqueueeventhistoricalevent", TestHistoricalEventProcessing.class);
        testHandlers.registerTestHandler("recievedqueueeventalertprocessing", TestAlertProcessor.class);
        testHandlers.registerTestHandler("recievedqueueeventcurrentstatus", TestCurrentEventProcessing.class);
        testHandlers.registerTestHandler("recieveduserdefinedalerttriggercustomeralerttype", TestUserDefinedAlertTrigger.class);
        testHandlers.registerTestHandler("calculateaddcustomeralerttypeevent", TestCalculatePaystationBalance.class);
        
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new AlertSteps(this.testHandlers),
                new XMLRPCEventSteps(this.testHandlers));
    }
}
