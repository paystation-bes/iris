package com.digitalpaytech.bdd.alerts.bugs;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.AlertSteps;
import com.digitalpaytech.bdd.scheduling.queue.alert.impl.TestAlertEmailNotification;
import com.digitalpaytech.bdd.scheduling.queue.alert.impl.TestAlertProcessor;
import com.digitalpaytech.bdd.scheduling.queue.alert.impl.TestCurrentEventProcessing;
import com.digitalpaytech.bdd.scheduling.queue.alert.impl.TestHistoricalEventProcessing;
import com.digitalpaytech.bdd.scheduling.queue.alert.impl.TestUserDefinedAlertTrigger;
import com.digitalpaytech.bdd.service.paystation.XMLRPCEventSteps;
import com.digitalpaytech.bdd.service.paystation.impl.eventalertserviceimpl.TestProcessEvent;
import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

//Because we're using Kafka for alerts, these test stories wouldn't work anymore.
@org.junit.Ignore
@RunWith(JUnitReportingRunner.class)
public class AlertBugsStories extends AbstractStories {
    
    public AlertBugsStories() {
        super();
        this.testHandlers.registerTestHandler("paystationeventtransmit", TestProcessEvent.class);
        this.testHandlers.registerTestHandler("paystationeventassert", TestProcessEvent.class);
        this.testHandlers.registerTestHandler("paystationeventignore", TestProcessEvent.class);
        this.testHandlers.registerTestHandler("recievedqueuenotificationemailnotification", TestAlertEmailNotification.class);
        this.testHandlers.registerTestHandler("alertemailsent", TestAlertEmailNotification.class);
        this.testHandlers.registerTestHandler("recieveduserdefinedalerttriggercustomeralerttype", TestUserDefinedAlertTrigger.class);
        this.testHandlers.registerTestHandler("recievedqueueeventcurrentstatus", TestCurrentEventProcessing.class);
        this.testHandlers.registerTestHandler("recievedqueueeventalertprocessing", TestAlertProcessor.class);
        this.testHandlers.registerTestHandler("recievedqueueeventhistoricalalertprocessing", TestHistoricalEventProcessing.class);
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    // Here we specify the steps classes
    @Override
    public final InjectableStepsFactory stepsFactory() {
        // varargs, can have more that one steps classes
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new AlertSteps(this.testHandlers),
                new XMLRPCEventSteps(this.testHandlers));
    }
}
