package com.digitalpaytech.bdd.alerts;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.AlertSteps;
import com.digitalpaytech.bdd.scheduling.queue.alert.impl.TestHistoricalEventProcessing;
import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.domain.AlertThresholdType;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosEventHistory;
import com.digitalpaytech.dto.queue.QueueEvent;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class PosHistoryEventCreationStories extends AbstractStories {
    public PosHistoryEventCreationStories() {
        super();
        this.testHandlers.registerTestHandler("recievedqueueeventhistoricalevent", TestHistoricalEventProcessing.class);
        this.testHandlers.registerTestHandler("historicalqueueeventsaved", TestHistoricalEventProcessing.class);
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
        TestLookupTables.getExpressionParser().register(new Class[] { PointOfSale.class, QueueEvent.class, AlertThresholdType.class,
            CustomerAlertType.class, PosEventHistory.class, });
    }
    
    // Here we specify the steps classes
    @Override
    public final InjectableStepsFactory stepsFactory() {
        // varargs, can have more that one steps classes
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new AlertSteps(this.testHandlers));
    }
}
