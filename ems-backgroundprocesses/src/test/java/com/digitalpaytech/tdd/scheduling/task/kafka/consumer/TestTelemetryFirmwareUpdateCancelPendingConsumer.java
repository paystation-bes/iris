package com.digitalpaytech.tdd.scheduling.task.kafka.consumer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollection;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.KafkaException;
import org.apache.kafka.common.TopicPartition;
import org.apache.log4j.Appender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.scheduling.task.kafka.consumer.TelemetryFirmwareCancelPendingConsumerTask;
import com.digitalpaytech.scheduling.task.support.TelemetryFirmwareCancelPendingNotification;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.paystation.impl.PosServiceStateServiceImpl;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.util.json.JSONConfigurationBean;
import com.digitalpaytech.util.kafka.impl.MessageConsumerFactoryImpl;

public class TestTelemetryFirmwareUpdateCancelPendingConsumer {
    private static final String TOPIC = "ota.device.cancel.pending.notification";
    private static final Logger LOG = Logger.getLogger(TestTelemetryFirmwareUpdateCancelPendingConsumer.class);
    
    private static final String POS_NULL_ERROR_MESSAGE = "There was no record found for the deviceSerialNumber provided from telemetry:";
    private static final String POS_SERVICE_STATE_NULL_ERROR_MESSAGE = "Update PosServiceState.IsNewPaystationSetting but PosServiceState not found for PointOfSaleId:";
    private static final String JSON_DESERIALIZATION_ERROR_MESSAGE = "Invalid JSON received from Kafka by TelemetryFirmwareCancelPendingConsumerTask:";
    private static final String KAFKA_TEST_ERROR_MESSAGE= "Test Kafka Exception";

    private JSON json = null;

    @Mock
    private Appender mockAppender;
    
    @Captor
    private ArgumentCaptor<LoggingEvent> captorLoggingEvent;
    
    @Mock
    private KafkaConsumer<String, String> mockConsumer;
    
    @Mock
    private MessageConsumerFactoryImpl messageConsumerFactory;

    @Mock
    private PointOfSaleServiceImpl pointOfSaleService;

    @Mock
    private PosServiceStateServiceImpl posServiceStateService;
    
    @InjectMocks
    private TelemetryFirmwareCancelPendingConsumerTask telemetryFirmwareCancelPendingConsumerTask;
    
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        when(this.messageConsumerFactory.buildConsumer(any(), anyCollection(), anyString()))
                .thenReturn(this.mockConsumer);
        
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        
        json = new JSON(new JSONConfigurationBean(true, false, false));
        this.messageConsumerFactory.setJSON(json);
        
        Logger root = (Logger) Logger.getRootLogger();
        root.addAppender(mockAppender);
        root.setLevel(Level.ERROR);
    }

    @Test
    public void happyPathSetOTATrue() {
        happyPathHelper(true);
    }

    @Test
    public void happyPathSetOTAFalse() {
        happyPathHelper(false);
    }

    @Test
    public void neutralPathEmptyConsumerRecords() {
        this.telemetryFirmwareCancelPendingConsumerTask.process();
        verify(mockAppender, atLeast(0)).doAppend(captorLoggingEvent.capture());
        List<LoggingEvent> allLoggingEvents = captorLoggingEvent.getAllValues();
        // nothing will be logged from our expected error messages, hence no logged messages should be filtered out
        List<String> filteredLogMessages = allLoggingEvents.stream().map(e -> (String) e.getMessage()).filter(m -> !m.contains(POS_NULL_ERROR_MESSAGE))
                .filter(m -> !m.contains(POS_SERVICE_STATE_NULL_ERROR_MESSAGE)).filter(m -> !m.contains(JSON_DESERIALIZATION_ERROR_MESSAGE))
                .collect(Collectors.toList());
        assertEquals(filteredLogMessages.size(), allLoggingEvents.size());
    }
    
    @Test
    public void failurePathConsumerRecordDeserializationFailed() {
        final List<ConsumerRecord<String, String>> recordList = createRecordList(true);        
        corruptRecords(recordList);
        setMockConsumerBehaviour(recordList);

        this.telemetryFirmwareCancelPendingConsumerTask.process();                
        verifyLogMessage(JSON_DESERIALIZATION_ERROR_MESSAGE);        
    }

    @Test
    public void failurePathNullPOS() {
        final List<ConsumerRecord<String, String>> recordList = createRecordList(true);        
        setMockConsumerBehaviour(recordList);
        
        this.telemetryFirmwareCancelPendingConsumerTask.process();                
        verifyLogMessage(POS_NULL_ERROR_MESSAGE);        
    }

    @Test
    public void failurePathNullPOSServiceState() {
        final List<ConsumerRecord<String, String>> recordList = createRecordList(true);        
        setMockConsumerBehaviour(recordList);

        PointOfSale pos = createPOS();
        when(this.pointOfSaleService.findPointOfSaleBySerialNumber(any())).thenReturn(pos);

        this.telemetryFirmwareCancelPendingConsumerTask.process();                
        verifyLogMessage(POS_SERVICE_STATE_NULL_ERROR_MESSAGE);        
    }

    @Test
    public void failurePathConsumerCommitException() {
        doThrow(new KafkaException(KAFKA_TEST_ERROR_MESSAGE)).when(this.mockConsumer).commitSync();
        
        this.telemetryFirmwareCancelPendingConsumerTask.process();        
        verifyLogMessage(KAFKA_TEST_ERROR_MESSAGE);        
    }
    
    private void verifyLogMessage(String errorMessage) {
        verify(mockAppender, atLeast(1)).doAppend(captorLoggingEvent.capture());
        List<LoggingEvent> allLoggingEvents = captorLoggingEvent.getAllValues();
        List<LoggingEvent> filteredLogEvents = allLoggingEvents.stream().filter(e -> e.getMessage().toString().contains(errorMessage))
                .collect(Collectors.toList());
        assertTrue(filteredLogEvents.size() == 1);
        assertEquals(Level.ERROR, filteredLogEvents.get(0).getLevel());
    }

    private void corruptRecords(final List<ConsumerRecord<String, String>> recordList) {
        ConsumerRecord<String, String> rec1 = recordList.get(0);
        ConsumerRecord<String, String> corruptRecord = new ConsumerRecord<String, String>(TOPIC, 0, 0L, rec1.key(), "Invalid");
        recordList.remove(rec1);
        recordList.add(0, corruptRecord);
    }

    private void happyPathHelper(boolean otaFlagValue) {
        final List<ConsumerRecord<String, String>> recordList = createRecordList(otaFlagValue);
        setMockConsumerBehaviour(recordList);
        
        PointOfSale pos = createPOS();
        when(this.pointOfSaleService.findPointOfSaleBySerialNumber(any())).thenReturn(pos);
        
        // set value in POSServiceState the opposite of what the final value will be. This way we can verify that the method to set this value to the correct value was indeed called
        PosServiceState posServiceState = createPOSServiceState(!otaFlagValue);
        when(this.posServiceStateService.findPosServiceStateById(pos.getId(), false)).thenReturn(posServiceState);
        
        this.telemetryFirmwareCancelPendingConsumerTask.process();
        assertEquals(otaFlagValue, posServiceState.isIsNewOTAFirmwareUpdateCancelPending());
    }
    
    private List<ConsumerRecord<String, String>> createRecordList(boolean otaFlagValue) {
        final List<ConsumerRecord<String, String>> recordList = new ArrayList<>();
        try {
            final List<TelemetryFirmwareCancelPendingNotification> notifications = createTestNotifications(otaFlagValue);
            createRecordsFromNotificationsAndAddToRecordsList(recordList, notifications);
        } catch (JsonException e) {
            fail("Json Exception occurred");
        }
        return recordList;
    }
    
    private void setMockConsumerBehaviour(final List<ConsumerRecord<String, String>> recordList) {
        final ConsumerRecords<String, String> records = createConsumerRecordsFromRecordsList(recordList);
        when(this.mockConsumer.poll(Duration.ofMillis(anyLong()))).thenReturn(records);
    }
    
    private PosServiceState createPOSServiceState(boolean flag) {
        PosServiceState posServiceState = new PosServiceState();
        posServiceState.setIsNewOTAFirmwareUpdateCancelPending(flag);
        return posServiceState;
    }
    
    private PointOfSale createPOS() {
        PointOfSale pos = new PointOfSale();
        pos.setId(1);
        return pos;
    }
    
    private ConsumerRecords<String, String> createConsumerRecordsFromRecordsList(final List<ConsumerRecord<String, String>> recordList) {
        final Map<TopicPartition, List<ConsumerRecord<String, String>>> recordMap = new HashMap<>();
        recordMap.put(new TopicPartition(TOPIC, 0), recordList);
        
        final ConsumerRecords<String, String> records = new ConsumerRecords<String, String>(recordMap);
        return records;
    }
    
    private void createRecordsFromNotificationsAndAddToRecordsList(final List<ConsumerRecord<String, String>> recordList,
        final List<TelemetryFirmwareCancelPendingNotification> notifications) throws JsonException {
        for (TelemetryFirmwareCancelPendingNotification notification : notifications) {
            final ConsumerRecord<String, String> record =
                    new ConsumerRecord<String, String>(TOPIC, 0, 0L, notification.getDeviceSerialNumber(), json.serialize(notification));
            recordList.add(record);
        }
    }
    
    private List<TelemetryFirmwareCancelPendingNotification> createTestNotifications(boolean flag) {
        final List<TelemetryFirmwareCancelPendingNotification> notifications = new ArrayList<>();
        TelemetryFirmwareCancelPendingNotification notification = new TelemetryFirmwareCancelPendingNotification();
        notification.setDeviceSerialNumber("123");
        notification.setUpgradeCancelPending(flag);
        notification.setCreationDate("123");
        notifications.add(notification);
        return notifications;
    }
}
