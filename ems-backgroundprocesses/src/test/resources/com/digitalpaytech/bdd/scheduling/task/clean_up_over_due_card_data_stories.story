Narrative: Clean up over due card data once every day

Scenario: When there exists over due card data
Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
And there exists a merchant account where the  
name is TDMerchantServices
customer is Mackenzie Parking
processor is processor.tdmerchant
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is CAD
And there exists a preauth transaction data where the 
preauth id is 1
serial number is 500000070001
merchant account is TDMerchantServices
preauth date is 8 days ago
authorization number is 1VNK9K
card data is 4030000010001234=1907101000000012300
charge amount is 200
is not expired
is approved
When the outstanding card data is triggered
Then there exists a preauth holding where the
serial number is 500000070001
merchant account is TDMerchantServices
expired is 1
is approved
authorization number is 1VNK9K
