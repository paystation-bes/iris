Narrative:
In order to test handling of sms extension warnings and requests 
when a user has had their stall purchased by another parker EMS-10615 
As a customer
I want to not send an sms message if there is no active permit associated with the permit

Scenario: Process SMS extension request for an original transaction
that was processed through the Card Processing Server 
Given a parking extension request is sent where the
data is 30
sender is 2522839938
And there exists a customer where the 
customer Name is Mackenzie Parking
Id is 1
is migrated
And there exists a pay station where the  
serial number is 500000070001
name is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
And there exists an sms alert where the 
id is 1
expire GMT is 30 minutes from now
And there exists an SMS alert info where the
original permit id is 1
location id is 1
expiry date is 30 minutes from now
time zone is Canada/Pacific
mobile number is 2522839938
customer id is 1
sms alert id is 1
And there exists a purchase where the
purchase id is 1
purchase date is 30 minutes ago
customer name is Mackenzie Parking
serial number is 500000070001
card paid amount is 300
And there exists a permit where the 
permit id is 1
mobile number is 2522839938
begin GMT is 30 minutes ago
expire GMT is 30 minutes from now
ticket number is 12
purchase customer is Mackenzie Parking
And there exists a processor transaction where the
ticket number is 12
authorization number is AKX07
purchase customer is Mackenzie Parking
purchased date is 30 minutes ago
is approved
And there exists a merchant account where the
name is TestCreditCall
customer is Mackenzie Parking
merchant account status is enabled
processor is processor.creditcall
Field1 is Elavon
Field2 is merchantID
Field3 is 11223344
Field4 is TRANSACTION
Field5 is CAD
And there exists a merchant pos where the
pay station is 500000070001
card type is credit card
name is TestCreditCall
And there exists a extensible permit where the
mobile number is 2522839938
And there exists a Transaction Card Token where the 
processor transaction authorization number is AKX07
token is 8383
And there exists an active permit where the
permit id is 12
When I process the extension
Then the parking session is extended

Scenario: Allow an sms extension request for mobile with an active permit associated with it
Given a parking extension request is sent where the
data is 30
sender is 2522839938
And there exists a customer where the 
customer Name is Mackenzie Parking
Id is 1
is migrated
And there exists a pay station where the  
serial number is 500000070001
name is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
And there exists an sms alert where the 
id is 2
expire GMT is 30 minutes from now
And there exists an SMS alert info where the
original permit id is 3
location id is 1
expiry date is 30 minutes from now
time zone is Canada/Pacific
mobile number is 2522839938
customer id is 1
sms alert id is 2
And there exists a purchase where the
purchase id is 1
purchase date is 30 minutes ago
customer name is Mackenzie Parking
serial number is 500000070001
card paid amount is 300
And there exists a payment card where the 
payment card id is 1
purchase customer is Mackenzie Parking
And there exists a permit where the 
permit id is 1
mobile number is 2522839938
begin GMT is 30 minutes ago
expire GMT is 30 minutes from now
ticket number is 13
purchase customer is Mackenzie Parking
And there exists a processor transaction where the
ticket number is 13
authorization number is AKX07
purchase customer is Mackenzie Parking
purchased date is 30 minutes ago
is approved
And there exists a merchant account where the
name is TestCreditCall
customer is Mackenzie Parking
merchant account status is enabled
processor is processor.paymentech
Field1 is Paymentech
Field2 is merchantID
Field3 is 11223344
Field4 is TRANSACTION
Field5 is CAD
And there exists a merchant pos where the
pay station is 500000070001
card type is credit card
name is TestCreditCall
And there exists a extensible permit where the
mobile number is 2522839938
And there exists a Transaction Card Token where the 
processor transaction authorization number is AKX07
token is 8383
And there exists an active permit where the
permit id is 13
When I process the extension
Then the parking session is extended

Scenario: Allow an sms extension request with a link merchant account for mobile with an active permit associated with it
Given a parking extension request is sent where the
data is 30
sender is 2522839938
And there exists a customer where the 
customer Name is Mackenzie Parking
Id is 1
is migrated
And there exists a pay station where the  
serial number is 500000070001
name is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
And there exists an sms alert where the 
id is 3
expire GMT is 30 minutes from now
And there exists a SMS alert info where the
original permit id is 5
location id is 1
expiry date is 30 minutes from now
time zone is Canada/Pacific
mobile number is 2522839938
customer id is 1
sms alert id is 3
And there exists a purchase where the
purchase id is 1
purchase date is 30 minutes ago
customer name is Mackenzie Parking
serial number is 500000070001
card paid amount is 300
And there exists a payment card where the 
payment card id is 1
And there exists a permit where the 
permit id is 1
mobile number is 2522839938
begin GMT is 30 minutes ago
expire GMT is 30 minutes from now
ticket number is 14
purchase customer is Mackenzie Parking
And there exists a processor transaction where the
ticket number is 14
authorization number is AKX07
purchase customer is Mackenzie Parking
purchased date is 30 minutes ago
is approved
And there exists a processor where the 
processor name is processor.paymentech.link
And there exists a merchant account where the
name is TestLinkMA
customer is Mackenzie Parking
merchant account status is enabled
processor is processor.paymentech.link
terminal token is 9f4bfda9-1b9b-4bbe-87ae-827e9ca38a12
is link
And there exists a merchant pos where the
pay station is 500000070001
card type is credit card
name is TestLinkMA
And there exists a extensible permit where the
mobile number is 2522839938
card data is 0d1a2dfd-d707-4a7d-b4aa-4795d42af915
And there exists a Transaction Card Token where the 
processor transaction authorization number is AKX07
token is 8383
And there exists an active permit where the
permit id is 14
And the Core CPS response with message 
{
  "status": {
    "responseStatus": "SUCCESS",
    "errors": null
  },
  "response": {
    "status": {
      "responseStatus": "Accepted",
      "errors": [
        {
          "identifier": "Approved",
          "message": "Request is approved"
        }
      ]
    },
    "chargeToken": "6121a1c7-5a02-476e-9ae5-239ca4a45e00",
    "cardToken": "0d1a2dfd-d707-4a7d-b4aa-4795d42af915",
    "authorizationNumber": "192539",
    "cardType": "MasterCard",
    "last4Digits": "2123",
    "referenceNumber": "000312",
    "processorTransactionId": "00000052",
    "processedDate": "2018-02-09T21:16:19.574Z",
    "terminalToken": "9f4bfda9-1b9b-4bbe-87ae-827e9ca38a12",
    "optionalData": null
  }
}
When I process the extension
Then the parking session is extended


Scenario: Refuse extension request when coreCPS denies the card
Given a parking extension request is sent where the
data is 30
sender is 2522839938
And there exists a customer where the 
customer Name is Mackenzie Parking
Id is 1
is migrated
And there exists a pay station where the  
serial number is 500000070001
name is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
And there exists an sms alert where the 
id is 3
expire GMT is 30 minutes from now
And there exists a SMS alert info where the
original permit id is 5
location id is 1
expiry date is 30 minutes from now
time zone is Canada/Pacific
mobile number is 2522839938
customer id is 1
sms alert id is 3
And there exists a purchase where the
purchase id is 1
purchase date is 30 minutes ago
customer name is Mackenzie Parking
serial number is 500000070001
card paid amount is 300
And there exists a payment card where the 
payment card id is 1
And there exists a permit where the 
permit id is 1
mobile number is 2522839938
begin GMT is 30 minutes ago
expire GMT is 30 minutes from now
ticket number is 14
purchase customer is Mackenzie Parking
And there exists a processor transaction where the
ticket number is 14
authorization number is AKX07
purchase customer is Mackenzie Parking
purchased date is 30 minutes ago
is approved
And there exists a processor where the 
processor name is processor.paymentech.link
And there exists a merchant account where the
name is TestLinkMA
customer is Mackenzie Parking
merchant account status is enabled
processor is processor.paymentech.link
terminal token is 9f4bfda9-1b9b-4bbe-87ae-827e9ca38a12
is link
And there exists a merchant pos where the
pay station is 500000070001
card type is credit card
name is TestLinkMA
And there exists a extensible permit where the
mobile number is 2522839938
card data is 0d1a2dfd-d707-4a7d-b4aa-4795d42af915
And there exists a Transaction Card Token where the 
processor transaction authorization number is AKX07
token is 8383
And there exists an active permit where the
permit id is 14
And the Core CPS response with message 
{
  "status": {
    "responseStatus": "SUCCESS",
    "errors": null
  },
  "response": {
    "status": {
      "responseStatus": "Declined",
      "errors": [
        {
          "identifier": "Declined",
          "message": "Request is Declined"
        }
      ]
    },
    "chargeToken": "6121a1c7-5a02-476e-9ae5-239ca4a45e00",
    "cardToken": "0d1a2dfd-d707-4a7d-b4aa-4795d42af915",
    "authorizationNumber": "192539",
    "cardType": "MasterCard",
    "last4Digits": "2123",
    "referenceNumber": "000312",
    "processorTransactionId": "00000052",
    "processedDate": "2018-02-09T21:16:19.574Z",
    "terminalToken": "9f4bfda9-1b9b-4bbe-87ae-827e9ca38a12",
    "optionalData": null
  }
}
When I process the extension
Then the parking session is not extended
Scenario: Stop sms extension request for mobile with no active permit associated with it
Given a parking extension request is sent where the
data is 30
sender is 2522839918
And there exists a customer where the 
customer Name is Mackenzie Parking
Id is 1
is migrated
And there exists an SMS alert info where the
original permit id is 6
location id is 6
expiry date is 30 minutes from now
time zone is Canada/Pacific
mobile number is 2522839918
customer id is 1
sms alert id is 6
And there exists a permit where the 
permit id is 6
mobile number is 2522839918
begin GMT is 30 minutes ago
expire GMT is 30 minutes from now
ticket number is 12
purchase customer is Mackenzie Parking
And there exists a purchase where the 
purchase id is 6
And there exists a extensible permit where the
mobile number is 2522839918
card data is 398283798
When I process the extension
Then the parking session is not extended

Scenario: Refuse extension request when the call to coreCPS fails
Given a parking extension request is sent where the
data is 30
sender is 2522839938
And there exists a customer where the 
customer Name is Mackenzie Parking
Id is 1
is migrated
And there exists a pay station where the  
serial number is 500000070001
name is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
And there exists an sms alert where the 
id is 3
expire GMT is 30 minutes from now
And there exists a SMS alert info where the
original permit id is 5
location id is 1
expiry date is 30 minutes from now
time zone is Canada/Pacific
mobile number is 2522839938
customer id is 1
sms alert id is 3
And there exists a purchase where the
purchase id is 1
purchase date is 30 minutes ago
customer name is Mackenzie Parking
serial number is 500000070001
card paid amount is 300
And there exists a payment card where the 
payment card id is 1
And there exists a permit where the 
permit id is 1
mobile number is 2522839938
begin GMT is 30 minutes ago
expire GMT is 30 minutes from now
ticket number is 14
purchase customer is Mackenzie Parking
And there exists a processor transaction where the
ticket number is 14
authorization number is AKX07
purchase customer is Mackenzie Parking
purchased date is 30 minutes ago
is approved
And there exists a processor where the 
processor name is processor.paymentech.link
And there exists a merchant account where the
name is TestLinkMA
customer is Mackenzie Parking
merchant account status is enabled
processor is processor.paymentech.link
terminal token is 9f4bfda9-1b9b-4bbe-87ae-827e9ca38a12
is link
And there exists a merchant pos where the
pay station is 500000070001
card type is credit card
name is TestLinkMA
And there exists a extensible permit where the
mobile number is 2522839938
card data is 0d1a2dfd-d707-4a7d-b4aa-4795d42af915
And there exists a Transaction Card Token where the 
processor transaction authorization number is AKX07
token is 8383
And there exists an active permit where the
permit id is 14
And the Core CPS response with message 
{
  "status": {
    "responseStatus": "SUCCESS",
    "errors": null
  },
  "response": {
    "status": {
      "responseStatus": "Declined",
When I process the extension
Then the parking session is not extended
Scenario: Stop sms extension request for mobile with no active permit associated with it
Given a parking extension request is sent where the
data is 30
sender is 2522839918
And there exists a customer where the 
customer Name is Mackenzie Parking
Id is 1
is migrated
And there exists an SMS alert info where the
original permit id is 6
location id is 6
expiry date is 30 minutes from now
time zone is Canada/Pacific
mobile number is 2522839918
customer id is 1
sms alert id is 6
And there exists a permit where the 
permit id is 6
mobile number is 2522839918
begin GMT is 30 minutes ago
expire GMT is 30 minutes from now
ticket number is 12
purchase customer is Mackenzie Parking
And there exists a purchase where the 
purchase id is 6
And there exists a extensible permit where the
mobile number is 2522839918
card data is 398283798
When I process the extension
Then the parking session is not extended

Scenario:  Send sms message for permit with active permit associated with it
Given there exists a customer where the 
customer Name is Mackenzie Parking
Id is 1
is migrated
And there exists a permit where the 
permit id is 1
mobile number is 6042839938
begin GMT is 30 minutes ago
expire GMT is 30 minutes from now
ticket number is 12
purchase customer is Mackenzie Parking
And there exists an SMS alert info where the
original permit id is 1
location id is 1
expiry date is 30 minutes from now
time zone is Canada/Pacific
mobile number is 6042839938
And there exists an active permit where the
permit id is 1
When I process the sms alert
Then the sms warning message is sent

Scenario: Stop sms extension request for mobile with no smsAlertInfo associated with it
Given there exists a customer where the 
customer Name is Mackenzie Parking
Id is 1
is migrated
And a parking extension request is sent where the
data is 30
sender is 2522839918
And there exists an active permit where the
permit id is 7
And there exists an SMS alert info where the
original permit id is 25
location id is 6
expiry date is 30 minutes from now
time zone is Canada/Pacific
mobile number is 6042832222
customer id is 3
sms alert id is 6
And there exists a permit where the 
permit id is 6
mobile number is 2522839918
begin GMT is 30 minutes ago
expire GMT is 30 minutes from now
ticket number is 12
purchase customer is Mackenzie Parking
And there exists a purchase where the 
purchase id is 6
And there exists a extensible permit where the
mobile number is 2522839918
card data is 398283798
When I process the extension
Then the parking session is not extended

Scenario:  Send sms message for permit with active permit associated with it
Given there exists a customer where the 
customer Name is Mackenzie Parking
Id is 1
is migrated
And there exists a permit where the 
permit id is 1
mobile number is 6042839938
begin GMT is 30 minutes ago
expire GMT is 30 minutes from now
ticket number is 12
purchase customer is Mackenzie Parking
And there exists an SMS alert info where the
original permit id is 1
location id is 1
expiry date is 30 minutes from now
time zone is Canada/Pacific
mobile number is 6042839938
And there exists an active permit where the
permit id is 1
When I process the sms alert
Then the sms warning message is sent

Scenario:  Do not send sms message for permit with no active permit associated with it
Given there exists a customer where the 
customer Name is Mackenzie Parking
Id is 1
is migrated
And there exists a permit where the 
permit id is 3
mobile number is 6042839938
begin GMT is 30 minutes ago
expire GMT is 30 minutes from now
ticket number is 12
purchase customer is Mackenzie Parking
And there exists an SMS alert info where the
original permit id is 3
location id is 3
expiry date is 30 minutes from now
time zone is Canada/Pacific
mobile number is 6042839938
And there exists an active permit where the
permit id is 4
When I process the sms alert
Then the sms warning message is not sent

