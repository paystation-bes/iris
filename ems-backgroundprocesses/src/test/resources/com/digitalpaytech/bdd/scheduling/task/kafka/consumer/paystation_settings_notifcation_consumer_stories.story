Narrative: Consumer notifications from Kafka and update POSServiceState.IsNewPayStationSetting

!-- Note that the JSON here is in a list []. Kafka-consumer will convert each message into a ConsumerRecords object, so this list is only for
!-- the test to be able to make the ConsumerRecords object. 

Scenario: Invalid json  
Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
unifi id is 1
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
is Visible
is not Deleted
is not Decommissioned
is not new paystation setting
location is Downtown 
customer is Mackenzie Parking
When I poll the fms.device.upgrade topic where the 
[{"device":"500000070001","groupId":"58fe4ac2330b801f015bc11b","customerId":"1"
Then there exists a pos service state where the
point of sale is 500000070001
is not new paystation setting
next settings file is null

Scenario: An fms.device.upgrade notification is polled from Kafka but PS does not exist
Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
unifi id is 1
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
is Visible
is not Deleted
is not Decommissioned
is not new paystation setting
location is Downtown 
customer is Mackenzie Parking
And there exists a settings file where the
name is test group name
When I poll the fms.device.upgrade topic where the 
[{"deviceSerialNumber":"500000070002","groupId":"58fe4ac2330b801f015bc11b","customerId":"1", "groupName":"test group name"}]
Then there exists a pos service state where the
point of sale is 500000070001
is not new paystation setting
next settings file is null

Scenario: An fms.device.upgrade notification is polled from Kafka but customer does not match
Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
unifi id is 1
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
is Visible
is not Deleted
is not Decommissioned
is not new paystation setting
location is Downtown 
customer is Mackenzie Parking
When I poll the fms.device.upgrade topic where the 
[{"deviceSerialNumber":"500000070001","groupId":"58fe4ac2330b801f015bc11b","customerId":"2", "groupName":"test group name"}]
Then there exists a pos service state where the
point of sale is 500000070001
is not new paystation setting
next settings file is null

Scenario: An fms.device.upgrade notification is polled from Kafka 
Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
unifi id is 1
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
is Visible
is not Deleted
is not Decommissioned
is not new paystation setting
location is Downtown 
customer is Mackenzie Parking
And there exists a settings file where the
name is test group name
customer is Mackenzie Parking
When I poll the fms.device.upgrade topic where the 
[{"deviceSerialNumber":"500000070001","groupId":"58fe4ac2330b801f015bc11b","customerId":"1", "groupName":"test group name"}]
Then there exists a pos service state where the
point of sale is 500000070001
is new paystation setting
next settings file is test group name

Scenario: When multiple notifications arrive from FMS for different POS only those POS in the message should be updated
Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
unifi id is 1
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
is Visible
is not Deleted
is not Decommissioned
is not new paystation setting
location is Downtown 
customer is Mackenzie Parking
And there exists a pay station where the  
serial number is 500000070002
is activated
is Visible
is not Deleted
is not Decommissioned
is not new paystation setting
location is Downtown 
customer is Mackenzie Parking
And there exists a pay station where the  
serial number is 500000070003
is activated
is Visible
is not Deleted
is not Decommissioned
is not new paystation setting
location is Downtown 
customer is Mackenzie Parking
And there exists a pay station where the  
serial number is 500000070004
is activated
is Visible
is not Deleted
is not Decommissioned
is not new paystation setting
location is Downtown 
customer is Mackenzie Parking
And there exists a settings file where the
name is test group name
customer is Mackenzie Parking
When I poll the fms.device.upgrade topic where the 
[{"deviceSerialNumber":"500000070001","groupId":"58fe4ac2330b801f015bc11b","customerId":"1","groupName":"test group name"},
{"deviceSerialNumber":"500000070002","groupId":"58fe4ac2330b801f015bc11b","customerId":"1", "groupName":"test group name"},
{"deviceSerialNumber":"500000070003","groupId":"58fe4ac2330b801f015bc11b","customerId":"1", "groupName":"test group name"}]
Then there exists a pos service state where the
point of sale is 500000070001
is new paystation setting
next settings file is test group name
Then there exists a pos service state where the
point of sale is 500000070002
is new paystation setting
next settings file is test group name
Then there exists a pos service state where the
point of sale is 500000070003
is new paystation setting
next settings file is test group name
Then there exists a pos service state where the
point of sale is 500000070004
is not new paystation setting
next settings file is null
