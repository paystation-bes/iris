Narrative: Consumer notifications from Kafka and update POSServiceState.isIsNewOTAFirmware

!-- Note that the JSON here is in a list []. Kafka-consumer will convert each message into a ConsumerRecords object, so this list is only for
!-- the test to be able to make the ConsumerRecords object. 

Scenario: Toggle OTA Notification off
Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
unifi id is 1
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is new ota firmware notification
is activated
is Visible
is not Deleted
is not Decommissioned
is not new paystation setting
location is Downtown 
customer is Mackenzie Parking
When I poll the ota.device.notification topic where the 
{"deviceSerialNumber":"500000070001", "upgradeAvailable": false}
Then there exists a pos service state where the
point of sale is 500000070001
is not new ota firmware notification

Scenario: Toggle OTA Notification on
Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
unifi id is 1
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is activated
is Visible
is not Deleted
is not Decommissioned
is not new paystation setting
location is Downtown 
customer is Mackenzie Parking
When I poll the ota.device.notification topic where the 
{"deviceSerialNumber":"500000070001", "upgradeAvailable": true}
Then there exists a pos service state where the
point of sale is 500000070001
is new ota firmware notification


Scenario: Invalid json  
Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
unifi id is 1
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is not new ota firmware notification
is activated
is Visible
is not Deleted
is not Decommissioned
is not new paystation setting
location is Downtown 
customer is Mackenzie Parking
When I poll the ota.device.notification topic where the 
{"de":"500000070001","groupId":"58fe4ac2330b801f015bc11b","customerId":"1"}
Then there exists a pos service state where the
point of sale is 500000070001
is not new ota firmware notification


Scenario: Paystation does not exist  
Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
unifi id is 1
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
serial number is 500000070001
is not new ota firmware notification
is activated
is Visible
is not Deleted
is not Decommissioned
is not new paystation setting
location is Downtown 
customer is Mackenzie Parking
When I poll the ota.device.notification topic where the 
{"deviceSerialNumber":"500000070002", "upgradeAvailable": true}
Then there exists a pos service state where the
point of sale is 500000070001
is not new ota firmware notification

