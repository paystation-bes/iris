Narrative: When certain alerts exist that are autoclear alerts the clear events are put into
to current event queue that will target them EMS-10850.

Scenario: When a paystation lower power shutdown event exists from 25 hours ago a
clear event for that event is created
Given there exists a pay station where the 
name is ps1
And there exists an event type where the
name is Pay Station Battery Depleted
And there exists an active alert where the
event type is Pay Station Battery Depleted
alert GMT is 25 hours ago
cleared GMT is null
pay station is ps1 
When I remove the unwanted alarms
Then a new Queue Event is placed in the Recent Event Queue where the
event type is Pay Station Battery Depleted
severity is Clear
is not active
pay station is ps1
Then a new Queue Event is placed in the Historical Event Queue where the
event type is Pay Station Battery Depleted
severity is Clear
is not active
pay station is ps1

Scenario: When a paystation shock alarm event exists from 25 hours ago a
clear event for that even is created
Given there exists a pay station where the 
name is ps2
And there exists an event type where the
name is Shock Alarm
And there exists an active alert where the
event type is Shock Alarm
alert GMT is 25 hours ago
cleared GMT is null
pay station is ps2
When I remove the unwanted alarms
Then a new Queue Event is placed in the Recent Event Queue where the
event type is Shock Alarm
severity is Clear
is not active
pay station is ps2
Then a new Queue Event is placed in the Historical Event Queue where the
event type is Shock Alarm
severity is Clear
is not active
pay station is ps2