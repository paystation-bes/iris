Narrative:
In order to test 'calculate pay station balance' background jobs
As a customer admin user
I want to have pay station balance automatically updated based on "status changes"

Scenario: There are permits purchased and PurchaseCollection has 2 records for same pay station
Given there exists a pay station where the 
name is paystation2
When the Add Customer Alert Type Event is triggered
Then a new User Defined Alert Trigger is placed in the Customer Alert Type Queue where the 
pay station is paystation2 
type is MODIFIED



Scenario: There are permits purchased and PurchaseCollection has 5 records for 3 pay stations
Given there exists a pay station where the 
name is 3
And there exists a pay station where the 
name is 4
When the Add Customer Alert Type Event is triggered
Then a new User Defined Alert Trigger is placed in the Customer Alert Type Queue where the 
pay station are 3, 4
type is MODIFIED
