Narrative: Consumer notifications from Kafka and update Merchant Accounts

!-- Note that the JSON here is in a list []. Kafka-consumer will convert each message into a ConsumerRecords object, so this list is only for
!-- the test to be able to make the ConsumerRecords object. 

Scenario: Create a new merchant account  
Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
unifi id is 7
And there exists a processor where the
processor name is processor.alliance.link
When I poll the terminal_updates topic where the 
[{
  "terminalToken": "cbf1ce19-49d5-4eaf-8050-7a45f6091fe0",
  "terminalName": "Concord Test Terminal",
  "isValidated": true,
  "isLocked": true,
  "isDeleted": false,
  "isEnabled": true,
  "merchantName": "Concord Test Merchant",
  "timeZone": "Canada/Pacific",
  "customerId": 7,
  "processorType": "processor.alliance"
}]
Then there exists a merchant account where the
name is Concord Test Merchant
customer is Mackenzie Parking
merchant account status is enabled
terminal token is cbf1ce19-49d5-4eaf-8050-7a45f6091fe0
terminal name is Concord Test Terminal
is validated
is link
reference counter is 0
time zone is Canada/Pacific
processor is processor.alliance.link

Scenario: Update an existing merchant account  
Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
unifi id is 7
And there exists a processor where the
processor name is processor.alliance.link
And there exists a merchant account where the
name is Concord Test Merchant
customer is Mackenzie Parking
merchant account status is enabled
terminal token is cbf1ce19-49d5-4eaf-8050-7a45f6091fe0
terminal name is Concord Test Terminal
is validated
is link
reference counter is 0
time zone is Canada/Pacific
processor is processor.alliance.link
When I poll the terminal_updates topic where the 
[{
  "terminalToken": "cbf1ce19-49d5-4eaf-8050-7a45f6091fe0",
  "terminalName": "Concord Test Terminal New Name",
  "isDeleted": false,
  "isEnabled": false,
  "isValidated": true,  
  "merchantName": "New and improved name",
  "timeZone": "Canada/Atlantic",
  "customerId": 7,
  "processorType": "processor.alliance"
}]
Then there exists a merchant account where the
name is New and improved name
customer is Mackenzie Parking
merchant account status is disabled
terminal token is cbf1ce19-49d5-4eaf-8050-7a45f6091fe0
terminal name is Concord Test Terminal New Name
is validated
is link
reference counter is 0
time zone is Canada/Atlantic
processor is processor.alliance.link

Scenario: Update an existing merchant account's customerId  
Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
unifi id is 7
And there exists a processor where the
processor name is processor.alliance.link
And there exists a merchant account where the
name is Concord Test Merchant
customer is Mackenzie Parking
merchant account status is enabled
terminal token is cbf1ce19-49d5-4eaf-8050-7a45f6091fe0
terminal name is Concord Test Terminal
is validated
is link
reference counter is 0
time zone is Canada/Pacific
processor is processor.alliance.link
When I poll the terminal_updates topic where the 
[{
  "terminalToken": "cbf1ce19-49d5-4eaf-8050-7a45f6091fe0",
  "terminalName": "Concord Test Terminal New Name",
  "isDeleted": false,
  "isEnabled": false,
  "merchantName": "New and improved name",
  "timeZone": "Canada/Atlantic",
  "customerId": 8,
  "processorType": "processor.moneris"
}]
Then there exists a merchant account where the
name is Concord Test Merchant
customer is Mackenzie Parking
merchant account status is enabled
terminal token is cbf1ce19-49d5-4eaf-8050-7a45f6091fe0
terminal name is Concord Test Terminal
is validated
is link
reference counter is 0
time zone is Canada/Pacific
processor is processor.alliance.link

Scenario: Update a non link merchant account  
Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
unifi id is 8
And there exists a processor where the
processor name is processor.alliance.link
And there exists a merchant account where the
name is Concord Test Merchant
customer is Mackenzie Parking
merchant account status is enabled
terminal token is cbf1ce19-49d5-4eaf-8050-7a45f6091fe0
terminal name is Concord Test Terminal
is validated
is not link
reference counter is 0
time zone is Canada/Pacific
processor is processor.alliance.link
When I poll the terminal_updates topic where the 
[{
  "terminalToken": "cbf1ce19-49d5-4eaf-8050-7a45f6091fe0",
  "terminalName": "Concord Test Terminal New Name",
  "isDeleted": false,
  "isEnabled": false,
  "merchantName": "New and improved name",
  "timeZone": "Canada/Atlantic",
  "customerId": 8,
  "processorType": "processor.moneris"
}]
Then there exists a merchant account where the
name is Concord Test Merchant
customer is Mackenzie Parking
merchant account status is enabled
terminal token is cbf1ce19-49d5-4eaf-8050-7a45f6091fe0
terminal name is Concord Test Terminal
is validated
is not link
reference counter is 0
time zone is Canada/Pacific
processor is processor.alliance.link

Scenario: Delete an existing merchant account  
Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
unifi id is 7
And there exists a processor where the
processor name is processor.alliance.link
And there exists a merchant account where the
name is Concord Test Merchant
customer is Mackenzie Parking
merchant account status is enabled
terminal token is cbf1ce19-49d5-4eaf-8050-7a45f6091fe0
terminal name is Concord Test Terminal
is validated
is link
reference counter is 0
time zone is Canada/Pacific
processor is processor.alliance.link
When I poll the terminal_updates topic where the 
[{
  "terminalToken": "cbf1ce19-49d5-4eaf-8050-7a45f6091fe0",
  "isDeleted": true
}]
Then there exists a merchant account where the
name is Concord Test Merchant
customer is Mackenzie Parking
merchant account status is deleted
terminal token is cbf1ce19-49d5-4eaf-8050-7a45f6091fe0
terminal name is Concord Test Terminal
is validated
is link
reference counter is 0
time zone is Canada/Pacific
processor is processor.alliance.link

Scenario: Delete an non-existing merchant account  
Given there exists a customer where the 
customer Name is Mackenzie Parking
is migrated
unifi id is 7
And there exists a processor where the
processor name is processor.alliance.link
And there exists a merchant account where the
name is Concord Test Merchant
customer is Mackenzie Parking
merchant account status is enabled
terminal token is cbf1ce19-49d5-4eaf-8050-7a45f6091fe0
terminal name is Concord Test Terminal
is validated
is link
reference counter is 0
time zone is Canada/Pacific
processor is processor.alliance.link
When I poll the terminal_updates topic where the 
[{
  "terminalToken": "cbf1ce19-49d5-4eaf-8050-7a45f6091fe0",
  "isDeleted": true
}]
Then there exists a merchant account where the
name is Concord Test Merchant
customer is Mackenzie Parking
merchant account status is deleted
terminal token is cbf1ce19-49d5-4eaf-8050-7a45f6091fe0
terminal name is Concord Test Terminal
is validated
is link
reference counter is 0
time zone is Canada/Pacific
processor is processor.alliance.link

