Narrative:
In order to test creation of events from Collection and Communication background jobs
As a customer Admin user
I want to have alerts automatically added, modified and cleared based on "status changes"

Scenario:  QueuecustomerAlertType with type ADDED where default should be cleared
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a pay station where the 
name is 1
customer is Mackenzie Parking
And there exists a pay station where the 
name is 2
customer is Mackenzie Parking
And there exists a customer alert where the
customer is Mackenzie Parking
name is Coin Collection Test
threshold is 100
threshold type is Coin Canister Count
And there exists a customer alert where the
customer is Mackenzie Parking
name is Coin Canister Count Default
threshold is 2000
threshold type is Coin Canister Count
is default
And pay station 1 has a balance 80 coin count and 0 bill count
And pay station 2 has a balance 2001 coin count and 0 bill count
And there exists a active alert where the 
customer alert is Coin Canister Count Default
pay station is 2
severity is Critical
is active
is not hidden
And there is a User Defined Alert Trigger in the Customer Alert Type Queue where the 
pay station are 1, 2
Type is ADDED
customer alerts are Coin Collection Test
When the User Defined Alert Trigger is received from the customer alert type Queue
Then a new Queue Event is placed in the Recent Event Queue where the 
Customer alert is Coin Canister Count Default
pay station is 2
severity is Clear
is not active
Then a new Queue Event is placed in the Historical Event Queue where the 
Customer alert is Coin Canister Count Default
pay station is 2
severity is Clear
is not active
And there exists a active alert where the 
customer alert is Coin Canister Count Default
pay station is 2
severity is Critical
is active
is hidden
And there exists a active alert where the 
Customer alert is Coin Collection Test
pay station is 2
And a new Queue Event is placed in the Recent Event Queue where the 
Customer alert is Coin Collection Test
pay station is 1
severity is Minor
is active
And there exists a active alert where the 
Customer alert is Coin Collection Test
pay station is 2
And a new Queue Event is placed in the Recent Event Queue where the 
Customer alert is Coin Collection Test
pay station is 2
severity is Critical
is active
And a new Queue Event is placed in the Historical Event Queue where the 
Customer alert is Coin Collection Test
pay station is 2
severity is Critical
is active

Scenario:  QueuecustomerAlertType with type ADDED multiple alerts
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a pay station where the 
name is 1
customer is Mackenzie Parking
And there exists a pay station where the 
name is 2
customer is Mackenzie Parking
And there exists a customer Alert where the
customer is Mackenzie Parking
name is Coin Collection Test
customer is Mackenzie Parking
threshold type is Coin Canister Count
threshold is 100
And there exists a customer alert where the
customer is Mackenzie Parking
Name is Bill Collection Test
threshold is 100 
threshold type is Bill Stacker Count
And pay station 1 has a balance 80 coin count and 80 bill count
And pay station 2 has a balance 2001 coin count and 2001 bill count
And there is a User Defined Alert Trigger in the Customer Alert Type Queue where the 
Type is ADDED
pay station are 1, 2
customer Alerts are Coin Collection Test, Bill Collection Test
When the User Defined Alert Trigger is received from the customer alert type Queue
Then there exists a active alert where the 
customer alert is Coin Collection Test
pay station is 1
And a new Queue Event is placed in the Recent Event Queue where the 
customer alert is Coin Collection Test
pay station is 1
severity is Minor
is active 
And there exists a active alert where the 
customer alert is Bill Collection Test
pay station is 1
And a new Queue Event is placed in the Recent Event Queue where the 
customer alert is Bill Collection Test
pay station is 1
severity is Minor
is active
And there exists a active alert where the 
customer alert is Coin Collection Test
pay station is 2
And a new Queue Event is placed in the Recent Event Queue where the 
customer alert is Coin Collection Test
pay station is 2
severity is Critical
is active
And a new Queue Event is placed in the Historical Event Queue where the 
customer alert is Coin Collection Test
pay station is 2
severity is Critical
is active
And there exists a active alert where the 
customer alert is Bill Collection Test
pay station is 2
And a new Queue Event is placed in the Recent Event Queue where the 
customer alert is Bill Collection Test
pay station is 2
severity is Critical
is active
And a new Queue Event is placed in the Historical Event Queue where the 
customer alert is Bill Collection Test
pay station is 2
severity is Critical
is active

Scenario:  QueuecustomerAlertType with type Modified with no customerAlertType Ids
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a pay station where the 
name is 1
customer is Mackenzie Parking
And there exists a customer alert where the
customer is Mackenzie Parking
Name is Coin Collection Test
threshold is 100
threshold type is Coin Canister Count
And there exists a customer alert where the
customer is Mackenzie Parking
Name is Bill Collection Test
threshold is 100
threshold type is Bill Stacker Count
And there exists a Active Alert where the
customer alert is Coin Collection Test
pay station is 1
severity is Clear
And there exists a Active Alert where the
customer alert is Bill Collection Test
pay station is 1
severity is Clear
And pay station 1 has a balance 80 coin count and 150 bill count
And there is a User Defined Alert Trigger in the Customer Alert Type Queue where the 
Type is MODIFIED
pay station are 1
When the User Defined Alert Trigger is received from the customer alert type Queue
Then a new Queue Event is placed in the Recent Event Queue where the 
customer alert is Bill Collection Test
pay station is 1
severity is Critical
is active
And a new Queue Event is placed in the Historical Event Queue where the 
customer alert is Bill Collection Test
pay station is 1
severity is Critical
is active
Then a new Queue Event is placed in the Recent Event Queue where the 
customer alert is Coin Collection Test
pay station is 1
severity is Minor
is active

Scenario:  QueuecustomerAlertType with type Modified with customerAlertType Ids - switch alert to active
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a pay station where the 
name is 1
customer is Mackenzie Parking
And there exists a customer alert where the
customer is Mackenzie Parking
name is Bill Collection Test
threshold is 300
threshold type is Bill Stacker Count
And there exists a Active Alert where the
customer alert is Bill Collection Test
pay station is 1
severity is Clear
And pay station 1 has a balance 0 coin count and 500 bill count
And there is a User Defined Alert Trigger in the Customer Alert Type Queue where the 
Type is MODIFIED
pay station are 1
customer alerts is Bill Collection Test
When the User Defined Alert Trigger is received from the customer alert type Queue
Then a new Queue Event is placed in the Recent Event Queue where the 
customer alert is Bill Collection Test
pay station is 1
severity is Critical
is Active
And a new Queue Event is placed in the Historical Event Queue where the 
customer alert is Bill Collection Test
pay station is 1
severity is Critical
is Active

Scenario:  QueuecustomerAlertType with type Modified with customerAlertType Ids - switch alert to inactive
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a pay station where the 
name is 1
customer is Mackenzie Parking
And there exists a customer alert where the 
customer is Mackenzie Parking
Name is Bill Collection Test
threshold is 300
threshold type is Bill Stacker Count
And pay station 1 has a balance 0 coin count and 0 bill count
And there is a User Defined Alert Trigger in the Customer Alert Type Queue where the 
Type is MODIFIED
pay station are 1
customer alerts is Bill Collection Test
And there exists a Active Alert where the 
customer alert is Bill Collection Test
pay station is 1
severity is Critical
is Active
When the User Defined Alert Trigger is received from the customer alert type Queue
Then a new Queue Event is placed in the Recent Event Queue where the 
customer alert is Bill Collection Test
pay station is 1
severity is Clear 
is not active
Then a new Queue Event is placed in the Historical Event Queue where the 
customer alert is Bill Collection Test
pay station is 1
severity is Clear 
is not active

Scenario:  QueuecustomerAlertType with type REMOVED where Default Alert should trigger
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a pay station where the 
name is 1
customer is Mackenzie Parking
And there exists a pay station where the 
name is 2
customer is Mackenzie Parking
And there exists a customer Alert where the 
customer is Mackenzie Parking
Name is Coin Collection Test
Threshold is 100
threshold type is Coin Canister Count
And there exists a customer alert where the 
customer is Mackenzie Parking
name is Coin Canister Count Default
threshold is 2000
threshold type is Coin Canister Count
is default
And pay station 1 has a balance 80 coin count and 0 bill count
And pay station 2 has a balance 2001 coin count and 0 bill count
And there exists a active alert where the
customer alert is Coin Collection Test
pay station is 1
severity is Minor
is active
And there exists a active alert where the
customer alert is Coin Collection Test
pay station is 2
severity is Critical
is active
And there exists a active alert where the 
customer alert is Coin Canister Count Default
pay station is 1
severity is Clear
is not active
is hidden
And there exists a active alert where the 
customer alert is Coin Canister Count Default
pay station is 2
severity is Clear
is not active
is hidden
And there is a User Defined Alert Trigger in the Customer Alert Type Queue where the 
Type is REMOVED
pay station are 1 , 2
customer alerts are Coin Collection Test
When the User Defined Alert Trigger is received from the customer alert type Queue
Then there is no active alert where the 
customer alert is Coin Collection Test
pay station is 2
And there is no active alert where the 
customer alert is Coin Collection Test
pay station is 1
And there exists a active alert where the 
customer alert is Coin Canister Count Default
pay station is 1
severity is Clear
is not active
is not hidden
And there exists a active alert where the 
customer alert is Coin Canister Count Default
pay station is 2
severity is Clear
is not active
is not hidden
And a new Queue Event is placed in the Historical Event Queue where the 
customer alert is Coin Collection Test
pay station is 2
severity is Clear
is not active
And a new Queue Event is placed in the Historical Event Queue where the
customer alert is Coin Canister Count Default
pay station is 2
severity is Critical
is active
And a new Queue Event is placed in the Recent Event Queue where the
customer alert is Coin Canister Count Default
pay station is 2
severity is Critical
is active

Scenario:  QueuecustomerAlertType with type REMOVED where Default Alert should not trigger
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a pay station where the 
name is 1
customer is Mackenzie Parking
And there exists a pay station where the 
name is 2
customer is Mackenzie Parking
And there exists a customer Alert where the
customer is Mackenzie Parking
Name is Coin Collection Test
threshold is 100
threshold type is Coin Canister Count
And there exists a customer alert where the
customer is Mackenzie Parking
name is Coin Canister Count Default
threshold is 2000
threshold type is Coin Canister Count
is default
And pay station 1 has a balance 80 coin count and 0 bill count
And pay station 2 has a balance 150 coin count and 0 bill count
And there exists a active alert where the
customer alert is Coin Collection Test
pay station is 1
severity is Minor
is active
And there exists a active alert where the
customer alert is Coin Collection Test
pay station is 2
severity is Critical
is active
And there exists a active alert where the
customer alert is Coin Canister Count Default
pay station is 1
severity is Clear
is active
And there exists a active alert where the
customer alert is Coin Canister Count Default
pay station is 2
severity is Clear
is active
And there is a User Defined Alert Trigger in the Customer Alert Type Queue where the 
Type is REMOVED
pay station are 1 , 2
customer alerts are Coin Collection Test
When the User Defined Alert Trigger is received from the customer alert type Queue
Then there is no active alert where the 
customer alert is Coin Collection Test
pay station is 2
severity is Critical
is active
And there is no active alert where the  
customer alert is Coin Collection Test
pay station is 1
Severity is Minor
is active
And a new Queue Event is placed in the Historical Event Queue where the 
customer alert is Coin Collection Test
pay station is 2
Severity is Clear
is not active

Scenario:  QueuecustomerAlertType with type Communication
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a pay station where the 
name is 1
customer is Mackenzie Parking
And there exists a pay station where the 
name is 2
customer is Mackenzie Parking
And there exists a customer Alert where the
customer is Mackenzie Parking
Name is Last Seen Interval 10hr
threshold is 10
threshold type is Last Seen Interval Hour
And there exists a active alert where the
customer alert is Last Seen Interval 10hr
pay station is 1
severity is Major
is active
And there exists a active alert where the
customer alert is Last Seen Interval 10hr
pay station is 2
severity is Clear
is not active
And there exists a heartbeat where the
pay station is 1
last seen is now
And there exists a heartbeat where the
pay station is 2
last seen is last month
And there is a User Defined Alert Trigger in the Customer Alert Type Queue where the 
Type is COMMUNICATION
When the User Defined Alert Trigger is received from the customer alert type Queue
Then a new Queue Event is placed in the Recent Event Queue where the 
customer alert is Last Seen Interval 10hr
pay station is 1
severity is Clear
is not active
And a new Queue Event is placed in the Recent Event Queue where the 
customer alert is Last Seen Interval 10hr
pay station is 2
severity is Major
is active
