Scenario: Tests EMS-10010
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a pay station where the 
name is testpaystation
customer is Mackenzie Parking
Major Communication Alerts Count is 1
And there exists a heartbeat where the 
pay station is testpaystation
last seen is 2 hours ago
And there exists a customer alert where the 
customer is Mackenzie Parking
name is Communication Alert Default
threshold is 25
threshold type is Last Seen Interval Hour
is default
And there exists a active alert where the 
pay station is testpaystation
severity is Major
is active
customer alert is Communication Alert Default
And there exists a customer alert where the
customer is Mackenzie Parking
Name is Last Seen Interval 2hr
threshold is 2
threshold type is Last Seen Interval Hour
And there is a User Defined Alert Trigger in the Customer Alert Type Queue where the 
Type is ADDED
pay station is testpaystation
customer alerts are Last Seen Interval 2hr
When the User Defined Alert Trigger is received from the customer alert type Queue
And the Queue Event is received from the Current Status Queue
And the Queue Event is received from the Alert Processing Queue
And the Queue Event is received from the Historical Alert Processing Queue
When the Queue Notification Email is received from the Notification Queue
Then there exists a active alert where the 
Customer alert is Communication Alert Default
pay station is testpaystation
severity is Clear
And there exists a active alert where the 
Customer alert is Last Seen Interval 2hr
pay station is testpaystation
severity is Major
And there exists a event history where the  
Customer alert is Communication Alert Default
pay station is testpaystation
severity is Clear
And there exists a event history where the  
Customer alert is Last Seen Interval 2hr
pay station is testpaystation
severity is Major

Scenario: Test EMS-9583, alerts coming out of order, alertGMT or clearGMT is only updated if it is the latest event, older events don't update status
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
name is testpaystation
customer is Mackenzie Parking
pay station type is LUKE II
location is Downtown
And there exists a Event Data where the 
type is Printer
Action is PaperOut
timestamp is 10:00
severity is Critical
pay station is testpaystation
When the testpaystation sends Printer event Paper Out 
And the Queue Event is received from the Current Status Queue
And the Queue Event is received from the Alert Processing Queue
And the Queue Event is received from the Historical Alert Processing Queue
And the Queue Notification Email is received from the Notification Queue
Then there exists a active alert where the 
event type is Paper Status
pay station is testpaystation
severity is Critical
alert GMT is 10:00
cleared GMT is null
And there exists a event history where the 
event type is Paper Status
pay station is testpaystation
severity is Critical
is active
Action Type is Empty
timestamp is 10:00
Given there exists a Event Data where the 
type is Printer
Action is PaperOut
timestamp is 10:10
severity is Critical
pay station is testpaystation
When the testpaystation sends Printer event Paper Out 
And the Queue Event is received from the Current Status Queue
And the Queue Event is received from the Alert Processing Queue
And the Queue Event is received from the Historical Alert Processing Queue
And the Queue Notification Email is received from the Notification Queue
Then there exists a active alert where the 
event type is Paper Status
pay station is testpaystation
severity is Critical
alert GMT is 10:10
cleared GMT is null
And there exists a event history where the 
event type is Paper Status
pay station is testpaystation
severity is Critical
is active
Action Type is Empty
timestamp is 10:10
Given there exists a Event Data where the 
type is Printer
Action is PaperOutCleared
timestamp is 10:05
severity is Cleared
pay station is testpaystation
When the testpaystation sends Printer event Paper Out Cleared
And the Queue Event is received from the Current Status Queue
And the Queue Event is received from the Alert Processing Queue
And the Queue Event is received from the Historical Alert Processing Queue
And the Queue Notification Email is received from the Notification Queue
Then there exists a active alert where the 
event type is Paper Status
pay station is testpaystation
severity is Critical
alert GMT is 10:10
cleared GMT is null
And there exists a event history where the  
event type is Paper Status
pay station is testpaystation
severity is Clear
is not active
Action Type is Cleared
timestamp is 10:05
Given there exists a Event Data where the 
type is Printer
Action is PaperOutCleared
timestamp is 10:25
severity is Cleared
pay station is testpaystation
When the testpaystation sends Printer event Paper Out Cleared
And the Queue Event is received from the Current Status Queue
And the Queue Event is received from the Alert Processing Queue
And the Queue Event is received from the Historical Alert Processing Queue
And the Queue Notification Email is received from the Notification Queue
Then there exists a active alert where the 
event type is Paper Status
pay station is testpaystation
severity is Clear
alert GMT is 10:10
cleared GMT is 10:25
And there exists a event history where the 
event type is Paper Status
pay station is testpaystation
severity is Clear
is not active
Action Type is Cleared
timestamp is 10:25	 	 
Given there exists a Event Data where the 
type is Printer
Action is PaperOutCleared
timestamp is 10:15
severity is Cleared
pay station is testpaystation
When the testpaystation sends Printer event Paper Out Cleared
And the Queue Event is received from the Current Status Queue
And the Queue Event is received from the Alert Processing Queue
And the Queue Event is received from the Historical Alert Processing Queue
And the Queue Notification Email is received from the Notification Queue
Then there exists a active alert where the 
event type is Paper Status
pay station is testpaystation
severity is Clear
alert GMT is 10:10
cleared GMT is 10:25
And there exists a event history where the  
event type is Paper Status
pay station is testpaystation
severity is Clear
is not active
Action Type is Cleared
timestamp is 10:15	 	 
Given there exists a Event Data where the 
type is Printer
Action is PaperOut
timestamp is 10:20
severity is Critical
pay station is testpaystation
When the testpaystation sends Printer event Paper Out 
And the Queue Event is received from the Current Status Queue
And the Queue Event is received from the Alert Processing Queue
And the Queue Event is received from the Historical Alert Processing Queue
And the Queue Notification Email is received from the Notification Queue
Then there exists a active alert where the 
event type is Paper Status
pay station is testpaystation
severity is Clear
alert GMT is 10:10
cleared GMT is 10:25
And there exists a event history where the 
event type is Paper Status
pay station is testpaystation
severity is Critical
is active
Action Type is Empty
timestamp is 10:20
Given there exists a Event Data where the 
type is Printer
Action is PaperOut
timestamp is 10:30
severity is Critical
pay station is testpaystation
When the testpaystation sends Printer event Paper Out 
And the Queue Event is received from the Current Status Queue
And the Queue Event is received from the Alert Processing Queue
And the Queue Event is received from the Historical Alert Processing Queue
And the Queue Notification Email is received from the Notification Queue
Then there exists a active alert where the 
event type is Paper Status
pay station is testpaystation
severity is Critical
alert GMT is 10:30
cleared GMT is null
And there exists a event history where the 
event type is Paper Status
pay station is testpaystation
severity is Critical
is active
Action Type is Empty
timestamp is 10:30