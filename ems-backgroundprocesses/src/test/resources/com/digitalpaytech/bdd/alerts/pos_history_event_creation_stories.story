Narrative:
In order to test the successful persistance of the PosEventHistory Object
As a developer
I want to use create various QueueEvent objects and add them to the historical alert processing queue
and check if they have been saved correctly.

Scenario:  QueueEvent with both event action type and customer alert type as null
Given there exists a pay station where the 
name is testpaystation 
And there is a queue event in the historical event Queue where the
pay station is testpaystation
severity is Critical
Event Type is Printer Paper Jam
active is true
notification is true
When the queue event is received from the historical event Queue
Then the Historical Queue Event is saved successfully where the
pay station is testpaystation
Event Type is Printer Paper Jam
customer alert is null
severity is Critical
action type is null
active is true


Scenario:   QueueEvent with customer alert type as null
Given there exists a pay station where the 
name is testpaystation
And there is a Queue Event in the Historical Event Queue where the 
pay station is testpaystation
action type is Low
severity is Major
Event Type is Paper Status
active is true
When the queue event is received from the historical event Queue
Then the Historical Queue Event is saved successfully where the
pay station is testpaystation
event type is Paper Status
severity is Major
action type is Low
active is true
customer alert is null

Scenario:  QueueEvent with assigned customer alert type and event action type.
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a pay station where the 
Name is testpaystation
customer is Mackenzie Parking
And there exists a customer alert where the 
Customer is Mackenzie Parking
Name is Running Total Test
Threshold type is Running Total Dollar
And there is a queue event in the historical event Queue where the
pay station is testpaystation
customer alert is Running Total Test
severity is Critical
Event Type is Customer Defined Alert
active is true
When the queue event is received from the historical event Queue
Then the Historical Queue Event is saved successfully where the
pay station is testpaystation
customer alert is Running Total Test
severity is Critical
Event Type is Customer Defined Alert
active is true
