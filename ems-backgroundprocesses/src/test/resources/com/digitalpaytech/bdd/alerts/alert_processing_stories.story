Narrative:
In order to test alert processing of Pay Station and User Defined alerts
As a customer Admin user
I want to have alerts framework send notifications to correct email lists

Scenario: Pay station alert emails should only be sent for alerts that have pay station in route. User has 3 alerts, 2 (1 doesn't include pay station) for different routes 1 for all pay stations.  
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a location where the 
Location Name is ThisIsALocation
And there exists a pay station where the 
Id is 1
Name is paystation1
Location is ThisIsALocation
customer is Mackenzie Parking
And there exists a route where the 
Name is hasPaystation
customer is Mackenzie Parking
Pay station list is paystation1
And there exists a route where the
Name is emptyRoute
customer is Mackenzie Parking
And there exists a customer alert where the 
Customer is Mackenzie Parking
Name is withPaystation
Threshold type is Pay Station Alert
Route is hasPaystation
Customer email is includes@test.com 
And there exists a customer alert where the 
Customer is Mackenzie Parking
Name is noPaystation
Threshold type is Pay Station Alert
Route is emptyRoute
Customer email is notincludes@test.com
And there exists a customer alert where the 
Customer is Mackenzie Parking
Name is allPaystations
Threshold type is Pay Station Alert
Customer email is allPaystations@test.com
And there is a Queue Event in the Alert Processing Queue where the 
pay station is paystation1
Severity is Major
Is active
Is notification
Event Type is Coin Changer Jam
When the Queue Event is received from the Alert Processing Queue
Then a new Notification is placed in the Notification Queue where the 
Address is includes@test.com,
Subject is alert.email.paystation.subject.template
And a new Notification is placed in the Notification Queue where the 
Address is allPaystations@test.com,
Subject is alert.email.paystation.subject.template
And a new Notification is not placed in the Notification Queue where the 
Address is notincludes@test.com,
Subject is alert.email.paystation.subject.template

Scenario: User defined alert with multiple emails for a user defined alert
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a pay station where the 
Name is paystation1
customer is Mackenzie Parking
And there exists a customer alert where the 
Customer is Mackenzie Parking
Name is Running Total Test
Threshold type is Running Total Dollar
Customer email are runningtotal1@test.com, runningtotal2@test.com
And there is a Queue Event in the Alert Processing Queue where the 
pay station is paystation1
Severity is Critical
Is active
Is notification
customer alert is Running Total Test
When the Queue Event is received from the Alert Processing Queue
Then a new Notification is placed in the Notification Queue where the 
Address is runningtotal1@test.com, runningtotal2@test.com,
Subject is alert.email.collection.subject.template

Scenario: Clear event for user defined alert 
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a pay station where the 
Name is paystation1
customer is Mackenzie Parking
And there exists a customer alert where the 
Customer is Mackenzie Parking
Name is Running Total Test
Threshold type is Running Total Dollar
Customer email are runningtotal1@test.com
And there is a Queue Event in the Alert Processing Queue where the 
pay station is paystation1
Severity is Clear
Is not active
Is notification
customer alert is Running Total Test
When the Queue Event is received from the Alert Processing Queue
Then a new Notification is placed in the Notification Queue where the 
Address is runningtotal1@test.com,
Subject is alert.recovery.email.collection.subject.template

Scenario: Clear event for pay station alerts 
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a location where the 
Location Name is ThisIsALocation
And there exists a pay station where the 
Name is paystation1
customer is Mackenzie Parking
Location is ThisIsALocation
And there exists a customer alert where the 
Customer is Mackenzie Parking
Name is allPaystations
Threshold type is Pay Station Alert
Customer email is allPaystations@test.com
And there is a Queue Event in the Alert Processing Queue where the 
pay station is paystation1
Severity is Clear
Is not active
Is notification
Event Type is Coin Changer Jam
When the Queue Event is received from the Alert Processing Queue
Then a new Notification is placed in the Notification Queue where the 
Address is allPaystations@test.com,
Subject is alert.recovery.email.paystation.subject.template


