Narrative: Testing that the PosEventCurrent is updated correctly when there are new event(s) from current status queue

Scenario: Iris receive active alert when there is no corresponding active alert in database
Given there exists a pay station where the 
name is testpaystation 
And there is a queue event in the current status Queue where the 
pay station is testpaystation
is active
event type is Printer Paper Jam
severity is Critical
event time is 10:00
When the queue event is received from the current status Queue
Then there exists a active alert where the
pay station is testpaystation
severity is Critical
is active
alert GMT is 10:00

Scenario: Iris received cleared alert when there is corresponding active alert in database
Given there exists a pay station where the 
name is testpaystation
Critical Pay Station Alerts Count is 1
And there exists a active alert where the
pay station is testpaystation
is active
event type is Printer Paper Jam
severity is Critical
alert GMT is 10:00
And there is a queue event in the current status Queue where the 
pay station is testpaystation
is not active
event type is Printer Paper Jam
severity is Clear
event time is 10:00
When the queue event is received from the current status Queue
Then there exists a active alert where the
pay station is testpaystation
severity is Clear
is not active
alert GMT is 10:00
cleared GMT is 10:00

Scenario: Iris received cleared alert when there is no corresponding active alert in database
Given there exists a pay station where the 
name is testpaystation
And there is a queue event in the current status Queue where the 
pay station is testpaystation
is not active
event type is Printer Paper Jam
severity is Clear
event time is 10:00
When the queue event is received from the current status Queue
Then there exists a active alert where the
pay station is testpaystation
severity is Clear
is not active
cleared GMT is 10:00

Scenario: Iris received out of order events
Given there exists a pay station where the 
name is paystation1
And there exists a pay station where the 
name is paystation2
Critical Pay Station Alerts Count is 1 
And there exists a active alert where the
pay station is paystation2
is active
event type is Printer Paper Jam
severity is Critical
alert GMT is 10:00
And there is a queue event in the current status Queue where the 
pay station is paystation1
is not active
event type is Printer Paper Jam
severity is Clear
event time is 10:01
And there is a queue event in the current status Queue where the 
pay station is paystation1
is active
event type is Printer Paper Jam
severity is Critical
event time is 10:00 
And there is a queue event in the current status Queue where the 
pay station is paystation2
is not active
event type is Printer Paper Jam
severity is Clear
event time is 9:00
And there is a queue event in the current status Queue where the 
pay station is paystation1
is active
event type is Printer Paper Jam
severity is Critical
event time is 8:00 
When the queue event is received from the current status Queue
Then there exists a active alert where the
pay station is paystation2
severity is Critical
is active
alert GMT is 10:00
And there exists a active alert where the
pay station is paystation1
severity is Clear
is not active
cleared GMT is 10:01

Scenario: Iris received higher severity of the same EventType
Given there exists a pay station where the 
name is testpaystation
Major Pay Station Alerts Count is 1
And there exists a active alert where the
pay station is testpaystation
is active
event type is Paper Status
action type is Low
severity is Major
alert GMT is 8:00
And there is a queue event in the current status Queue where the 
pay station is testpaystation
is active
event type is Paper Status
action type is Empty
severity is Critical
event time is 9:00
When the queue event is received from the current status Queue
Then there exists a active alert where the
pay station is testpaystation
event type is Paper Status
action type is Empty
severity is Critical
alert GMT is 9:00

Scenario: Iris received lower severity of the same EventType
Given there exists a pay station where the 
name is testpaystation
Critical Pay Station Alerts Count is 1
And there exists a active alert where the
pay station is testpaystation
is active
event type is Paper Status
action type is Empty
severity is Critical
alert GMT is 8:00
And there is a queue event in the current status Queue where the 
pay station is testpaystation
is active
event type is Paper Status
action type is Low
severity is Major
event time is 9:00
When the queue event is received from the current status Queue
Then there exists a active alert where the
pay station is testpaystation
event type is Paper Status
action type is Low
severity is Major
alert GMT is 9:00