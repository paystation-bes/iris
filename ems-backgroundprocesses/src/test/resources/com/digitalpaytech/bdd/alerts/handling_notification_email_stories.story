Narrative:
In order to test creation of alert emails for a defined list
As a User
I want to have the notification handler send emails to the specified email address

Scenario: Email Sent to defined email address
Given there is a Queue Notification Email in the Notification Queue where the
subject is The Running Total of 500000070001 has exceeded 20.0 dollars.
content is Collection Alert: emailtest
address is test@test.com
When the Queue Notification Email is received from the Notification Queue
Then the alert email is sent successfully