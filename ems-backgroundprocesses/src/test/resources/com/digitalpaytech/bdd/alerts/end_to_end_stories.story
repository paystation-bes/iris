Narrative:
In order to test the Alert Framework
As a developer
I want to ensure that queue events are handled correctly by the Alert Framework

Scenario:  An active Minor user defined alert is replaced with a an active Critical user defined alert
& An active Critical user defined alert is replaced with a an active Minor user defined alert
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a pay station where the  
name is 1 
customer is Mackenzie Parking
Minor Collection Alerts Count is 1
And there exists a pay station where the 
name is 2
customer is Mackenzie Parking
Critical Collection Alerts Count is 1
And there exists a customer alert where the 
customer is Mackenzie Parking
name is Coin Collection Test
threshold is 100
threshold type is Coin Canister Count
Customer email is criticalcleared@test.com 
And there exists a customer alert where the 
customer is Mackenzie Parking
name is Bill Collection Test
threshold is 100
threshold type is Bill Stacker Count
Customer email is critical@test.com 
And pay station 1 has a balance 0 coin count and 200 bill count
And pay station 2 has a balance 80 coin count and 0 bill count
And there exists a active alert where the 
customer alert is Bill Collection Test
pay station is 1
severity is Minor
is active
alert GMT is one hour ago
And there exists a active alert where the 
customer alert is Coin Collection Test
pay station is 2
severity is Critical
is active
alert GMT is one hour ago
When the Add Customer Alert Type Event is triggered
When the User Defined Alert Trigger is received from the customer alert type Queue
And the Queue Event is received from the Current Status Queue
And the Queue Event is received from the Alert Processing Queue
Then a new Notification is placed in the Notification Queue where the 
Address is critical@test.com,
Subject is alert.email.collection.subject.template
When the Queue Event is received from the Historical Alert Processing Queue
And the Queue Notification Email is received from the Notification Queue
Then there exists a active alert where the  
Customer alert is Bill Collection Test
pay station is 1
severity is Critical
And there exists a event history where the  
Customer alert is Bill Collection Test
pay station is 1
severity is Critical
And there exists a active alert where the  
Customer alert is Coin Collection Test
pay station is 2
severity is Minor
And the 2 alert emails are sent successfully
And there exists a Alert status where the 
pay station is 1
Minor Collection Alerts Count is 0
Major Collection Alerts Count is 0
Critical Collection Alerts Count is 1
And there exists a Alert status where the 
pay station is 2
Minor Collection Alerts Count is 1
Major Collection Alerts Count is 0
Critical Collection Alerts Count is 0

Scenario:  QueuecustomerAlertType with type ADDED where default should be cleared
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a pay station where the 
name is 1
customer is Mackenzie Parking
And there exists a pay station where the 
name is 2
customer is Mackenzie Parking
Critical Collection Alerts Count is 1
And there exists a customer alert where the
customer is Mackenzie Parking
name is Coin Collection Test
threshold is 100
threshold type is Coin Canister Count
Customer email are test@test.com
And there exists a customer alert where the 
customer is Mackenzie Parking
name is Coin Canister Count Default
threshold is 2000
threshold type is Coin Canister Count
is default
And pay station 1 has a balance 80 coin count and 0 bill count
And pay station 2 has a balance 2001 coin count and 0 bill count
And there exists a active alert where the 
customer alert is Coin Canister Count Default
pay station is 1
severity is Clear
is not active
is not hidden
And there exists a active alert where the 
customer alert is Coin Canister Count Default
pay station is 2
severity is Critical
is active
is not hidden
And there is a User Defined Alert Trigger in the Customer Alert Type Queue where the 
pay station are 1, 2
Type is ADDED
customer alerts are Coin Collection Test
When the User Defined Alert Trigger is received from the customer alert type Queue
And the Queue Event is received from the Current Status Queue
And the Queue Event is received from the Alert Processing Queue
Then a new Notification is placed in the Notification Queue where the 
Address is test@test.com,
Subject is alert.email.collection.subject.template
When the Queue Event is received from the historical event Queue
And the Queue Notification Email is received from the Notification Queue
Then there exists a active alert where the  
Customer alert is Coin Collection Test
pay station is 1
severity is Minor
And there exists a active alert where the  
Customer alert is Coin Collection Test
pay station is 2
severity is Critical
And there exists a active alert where the  
Customer alert is Coin Canister Count Default
pay station is 1
severity is Clear
is not active
is hidden
And there exists a active alert where the  
Customer alert is Coin Canister Count Default
pay station is 2
severity is Clear
is not active
is hidden
And there exists a event history where the  
Customer alert is Coin Canister Count Default
pay station is 2
severity is Clear
And there exists a event history where the  
Customer alert is Coin Collection Test
pay station is 2
severity is Critical
And the alert email is sent successfully
And there exists a Alert status where the 
pay station is 2
Minor Collection Alerts Count is 0
Major Collection Alerts Count is 0
Critical Collection Alerts Count is 1
And there exists a Alert status where the 
pay station is 1
Minor Collection Alerts Count is 1
Major Collection Alerts Count is 0
Critical Collection Alerts Count is 0

Scenario:  QueuecustomerAlertType with type ADDED multiple alerts
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a pay station where the 
name is 1
customer is Mackenzie Parking
And there exists a pay station where the 
name is 2
customer is Mackenzie Parking
And there exists a customer Alert where the
customer is Mackenzie Parking
name is Coin Collection Test
customer is Mackenzie Parking
threshold type is Coin Canister Count
threshold is 100
Customer email are test@Coin Canister Count.com
And there exists a customer alert where the
customer is Mackenzie Parking
Name is Bill Collection Test
threshold is 100 
threshold type is Bill Stacker Count
Customer email are test@Bill Stacker Count.com
And pay station 1 has a balance 80 coin count and 80 bill count
And pay station 2 has a balance 2001 coin count and 2001 bill count
And there is a User Defined Alert Trigger in the Customer Alert Type Queue where the 
Type is ADDED
pay station are 1, 2
customer Alerts are Coin Collection Test, Bill Collection Test
When the User Defined Alert Trigger is received from the customer alert type Queue
And the Queue Event is received from the Current Status Queue
And the Queue Event is received from the Alert Processing Queue
Then a new Notification is placed in the Notification Queue where the 
Address is test@Coin Canister Count.com,
Subject is alert.email.collection.subject.template
Then a new Notification is placed in the Notification Queue where the 
Address is test@Bill Stacker Count.com,
Subject is alert.email.collection.subject.template
When the Queue Event is received from the Historical Alert Processing Queue
And the Queue Notification Email is received from the Notification Queue
Then there exists a active alert where the  
Customer alert is Coin Collection Test
pay station is 1
severity is Minor
And there exists a active alert where the  
Customer alert is Bill Collection Test
pay station is 1
severity is Minor
And there exists a active alert where the  
Customer alert is Coin Collection Test
pay station is 2
severity is Critical
And there exists a active alert where the  
Customer alert is Bill Collection Test
pay station is 2
severity is Critical
And there exists a event history where the  
Customer alert is Coin Collection Test
pay station is 2
severity is Critical
And there exists a event history where the  
Customer alert is Bill Collection Test
pay station is 2
severity is Critical
And the 2 alert emails are sent successfully
And there exists a Alert status where the 
pay station is 2
Minor Collection Alerts Count is 0
Major Collection Alerts Count is 0
Critical Collection Alerts Count is 2
And there exists a Alert status where the 
pay station is 1
Minor Collection Alerts Count is 2
Major Collection Alerts Count is 0
Critical Collection Alerts Count is 0

Scenario:  QueuecustomerAlertType with type REMOVED where Default Alert should trigger
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a pay station where the 
name is 1
customer is Mackenzie Parking
And there exists a pay station where the 
name is 2
customer is Mackenzie Parking
And there exists a customer Alert where the 
customer is Mackenzie Parking
Name is Coin Collection Test
Threshold is 100
threshold type is Coin Canister Count
Customer email are test@test.com
And there exists a customer alert where the 
customer is Mackenzie Parking
name is Coin Canister Count Default
threshold is 2000
threshold type is Coin Canister Count
is default
And pay station 1 has a balance 80 coin count and 0 bill count
And pay station 2 has a balance 2001 coin count and 0 bill count
And there exists a active alert where the 
customer alert is Coin Collection Test
pay station is 1
severity is Minor
is active
And there exists a active alert where the 
customer alert is Coin Collection Test
pay station is 2
severity is Critical
is active
And there exists a active alert where the 
customer alert is Coin Canister Count Default
pay station is 1
severity is Clear
is not active
is hidden
And there exists a active alert where the 
customer alert is Coin Canister Count Default
pay station is 2
severity is Clear
is not active
is hidden
And there is a User Defined Alert Trigger in the Customer Alert Type Queue where the 
Type is REMOVED
pay station are 1 , 2
customer alerts are Coin Collection Test
When the User Defined Alert Trigger is received from the customer alert type Queue
And the Queue Event is received from the Current Status Queue
And the Queue Event is received from the Alert Processing Queue
When the Queue Event is received from the Historical Alert Processing Queue
And the Queue Notification Email is received from the Notification Queue
Then there exists a active alert where the  
Customer alert is Coin Canister Count Default
pay station is 1
severity is Clear
is not active
is not hidden
Then there exists a active alert where the  
Customer alert is Coin Canister Count Default
pay station is 2
severity is Critical
is active
is not hidden
Then there exists a event history where the  
Customer alert is Coin Canister Count Default
pay station is 2
severity is Critical
And there is no active alert where the  
customer alert is Coin Collection Test
pay station is 2
severity is Critical
And there is no active alert where the  
customer alert is Coin Collection Test
pay station is 1
severity is Minor
And the alert email is not sent
And there exists a Alert status where the 
pay station is 1
Critical Collection Alerts Count is 0
Minor Collection Alerts Count is 0
Major Collection Alerts Count is 0
And there exists a Alert status where the 
pay station is 2
Minor Collection Alerts Count is 0
Major Collection Alerts Count is 0
Critical Collection Alerts Count is 1

Scenario:  QueuecustomerAlertType with type REMOVED where Default Alert should not trigger
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a pay station where the 
name is 1
customer is Mackenzie Parking
And there exists a pay station where the 
name is 2
customer is Mackenzie Parking
And there exists a customer Alert where the
customer is Mackenzie Parking
Name is Coin Collection Test
threshold is 100
threshold type is Coin Canister Count
Customer email are test@test.com
And there exists a customer alert where the
customer is Mackenzie Parking
name is Coin Canister Count Default
threshold is 2000
threshold type is Coin Canister Count
is default
And pay station 1 has a balance 80 coin count and 0 bill count
And pay station 2 has a balance 150 coin count and 0 bill count
And there exists a active alert where the
customer alert is Coin Collection Test
pay station is 1
severity is Minor
is active
And there exists a active alert where the
customer alert is Coin Collection Test
pay station is 2
severity is Critical
is active
And there exists a active alert where the
customer alert is Coin Canister Count Default
pay station is 1
severity is Clear
is active
And there exists a active alert where the
customer alert is Coin Canister Count Default
pay station is 2
severity is Clear
is active
And there is a User Defined Alert Trigger in the Customer Alert Type Queue where the 
Type is REMOVED
pay station are 1 , 2
customer alerts are Coin Collection Test
When the User Defined Alert Trigger is received from the customer alert type Queue
And the Queue Event is received from the Current Status Queue
And the Queue Event is received from the Alert Processing Queue
And the Queue Event is received from the Historical Alert Processing Queue
And the Queue Notification Email is received from the Notification Queue
Then there is no active alert where the  
customer alert is Coin Collection Test
pay station is 2
severity is Critical
And there is no active alert where the  
customer alert is Coin Collection Test
pay station is 1
severity is Minor
And there exists a event history where the  
Customer alert is Coin Collection Test
pay station is 2
severity is Clear
And the alert email is not sent
And there exists a Alert status where the 
pay station is 1
Critical Collection Alerts Count is 0
Minor Collection Alerts Count is 0
Major Collection Alerts Count is 0
And there exists a Alert status where the 
pay station is 2
Minor Collection Alerts Count is 0
Major Collection Alerts Count is 0
Critical Collection Alerts Count is 0

Scenario:  A user defined alert is triggered with severity Critical and a notification is sent 
& A user defined alert is triggered with severity Minor and a notification is not sent
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a pay station where the  
name is 1 
customer is Mackenzie Parking
And there exists a pay station where the 
name is 2
customer is Mackenzie Parking
And there exists a customer alert where the 
customer is Mackenzie Parking
name is Coin Collection Test
threshold is 100
threshold type is Coin Canister Count
Customer email is test@test.com 
And there exists a customer alert where the 
customer is Mackenzie Parking
name is Bill Collection Test
threshold is 100
threshold type is Bill Stacker Count
And pay station 1 has a balance 80 coin count and 0 bill count
And pay station 2 has a balance 200 coin count and 0 bill count
And there exists a active alert where the 
customer alert is Coin Collection Test
pay station is 1
severity is Clear
is not active
And there exists a active alert where the 
customer alert is Coin Collection Test
pay station is 2
severity is Clear
is not active
When the Add Customer Alert Type Event is triggered
And the User Defined Alert Trigger is received from the customer alert type Queue
And the queue event is received from the Current Status Queue
And the Queue Event is received from the Alert Processing Queue
Then a new Notification is placed in the Notification Queue where the 
Address is test@test.com,
Subject is alert.email.collection.subject.template
When the Queue Event is received from the Historical Alert Processing Queue
When the Queue Notification Email is received from the Notification Queue
Then there exists a active alert where the 
Customer alert is Coin Collection Test
pay station is 2
severity is Critical
Then there exists a event history where the 
Customer alert is Coin Collection Test
pay station is 2
severity is Critical
Then there exists a active alert where the 
Customer alert is Coin Collection Test
pay station is 1
severity is Minor
And the alert email is sent successfully
And there exists a Alert status where the 
pay station is 1
Minor Collection Alerts Count is 1
Major Collection Alerts Count is 0
Critical Collection Alerts Count is 0
And there exists a Alert status where the 
pay station is 2
Minor Collection Alerts Count is 0
Major Collection Alerts Count is 0
Critical Collection Alerts Count is 1

Scenario:  An active Critical user defined alert is cleared and a notification is sent
& An active Minor user defined alert is cleared no notification
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a pay station where the  
name is 1 
customer is Mackenzie Parking
Minor Collection Alerts Count is 1
And there exists a pay station where the 
name is 2
customer is Mackenzie Parking
Critical Collection Alerts Count is 1
And there exists a customer alert where the 
customer is Mackenzie Parking
name is Coin Collection Test
threshold is 100
threshold type is Coin Canister Count
Customer email is test@test.com 
And pay station 1 has a balance 0 coin count and 0 bill count
And pay station 2 has a balance 0 coin count and 0 bill count
And there exists a active alert where the 
customer alert is Coin Collection Test
pay station is 1
severity is Minor
is active
alert GMT is one hour ago
And there exists a active alert where the 
customer alert is Coin Collection Test
pay station is 2
severity is Critical
is active
alert GMT is one hour ago
When the Add Customer Alert Type Event is triggered
And the User Defined Alert Trigger is received from the customer alert type Queue
And the Queue Event is received from the Current Status Queue
And the Queue Event is received from the Alert Processing Queue
Then a new Notification is placed in the Notification Queue where the 
Address is test@test.com,
Subject is alert.recovery.email.collection.subject.template
When the Queue Event is received from the Historical Alert Processing Queue
And the Queue Notification Email is received from the Notification Queue
Then there exists a active alert where the  
Customer alert is Coin Collection Test
pay station is 1
severity is Clear
Then there exists a active alert where the  
Customer alert is Coin Collection Test
pay station is 2
severity is Clear
Then there exists a event history where the  
Customer alert is Coin Collection Test
pay station is 2
severity is Clear
And the alert email is sent successfully
And there exists a Alert status where the 
pay station is 1
Minor Collection Alerts Count is 0
Major Collection Alerts Count is 0
Critical Collection Alerts Count is 0
And there exists a Alert status where the 
pay station is 2
Minor Collection Alerts Count is 0
Major Collection Alerts Count is 0
Critical Collection Alerts Count is 0

Scenario: A user defined communication alert is triggered
& A user defined communication alert is cleared
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a pay station where the 
name is 1
customer is Mackenzie Parking
MAjor Communication Alerts Count is 1
And there exists a pay station where the 
name is 2
customer is Mackenzie Parking
And there exists a customer Alert where the 
customer is Mackenzie Parking
Name is Last Seen Interval 10hr
threshold is 10
threshold type is Last Seen Interval Hour
And there exists a active alert where the 
customer alert is Last Seen Interval 10hr
pay station is 1
severity is Major
is active
alert GMT is one hour ago
And there exists a active alert where the 
customer alert is Last Seen Interval 10hr
pay station is 2
severity is Clear
is not active
And there exists a heartbeat where the 
pay station is 1
last seen is now
And there exists a heartbeat where the 
pay station is 2
last seen is last month
And there exists a customer alert where the 
customer is Mackenzie Parking
name is Last Seen Interval 
threshold is 25
threshold type is Last Seen Interval
And there is a User Defined Alert Trigger in the Customer Alert Type Queue where the 
Type is COMMUNICATION
When the User Defined Alert Trigger is received from the customer alert type Queue
And the Queue Event is received from the Current Status Queue
And the Queue Event is received from the Alert Processing Queue
And the Queue Event is received from the Historical Alert Processing Queue
When the Queue Notification Email is received from the Notification Queue
Then there exists a active alert where the  
Customer alert is Last Seen Interval 10hr
pay station is 1
severity is Clear
is not active
And there exists a event history where the 
Customer alert is Last Seen Interval 10hr
pay station is 2
severity is Major
is active
And the alert email is not sent
And there exists a Alert status where the  
pay station is 1
Critical Communication Alerts Count is 0
Minor Communication Alerts Count is 0
Major Communication Alerts Count is 0
And there exists a Alert status where the  
pay station is 2
Critical Communication Alerts Count is 0
Minor Communication Alerts Count is 0
Major Communication Alerts Count is 1

Scenario: When a paystation shock alarm event exists from 25 hours ago a
clear event for that even is created
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a location where the 
Location Name is ThisIsALocation
Given there exists a pay station where the 
name is ps2
customer is Mackenzie Parking
pay station type is LUKE II
Location is ThisIsALocation
Critical Pay Station Alerts Count is 1
And there exists an active alert where the
event type is Shock Alarm
alert GMT is 25 hours ago
cleared GMT is null
pay station is ps2
severity is Critical
is active
And there exists a customer alert where the 
customer is Mackenzie Parking
name is Pay Station Events
threshold is 1
threshold type is Pay Station Alert
customer email is test@test.com 
When I remove the unwanted alarms
Then a new Queue Event is placed in the Current Status Queue where the
event type is Shock Alarm
severity is Clear
is not active
pay station is ps2
Then a new Queue Event is placed in the Historical Event Queue where the
event type is Shock Alarm
severity is Clear
is not active
pay station is ps2
When the Queue Event is received from the Current Status Queue
And the Queue Event is received from the Alert Processing Queue
And the Queue Event is received from the historical event Queue
Then a new Notification is placed in the Notification Queue where the 
Address is test@test.com,
Subject is alert.recovery.email.paystation.subject.template
When the Queue Notification Email is received from the Notification Queue
Then the alert email is sent successfully
And there exists a Alert status where the  
pay station is ps2
Critical Pay Station Alerts Count is 0
Minor Pay Station Alerts Count is 0
Major Pay Station Alerts Count is 0

Scenario:  An XMLRPC message from a pay station triggers a warning an no notification is sent 
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a pay station where the  
name is testpaystation
customer is Mackenzie Parking
pay station type is LUKE II
And there exists a Event Data where the 
type is BillAcceptor
Action is Present
timestamp is now
severity is Clear
pay station is testpaystation
And there exists a customer alert where the 
customer is Mackenzie Parking
name is Bill Acceptor Present
threshold is 1
threshold type is Pay Station Alert
customer email is test@test.com 
When the testpaystation sends BillAcceptor event Present
And the Queue Event is received from the Current Status Queue
And the Queue Event is received from the Alert Processing Queue
And the Queue Event is received from the Historical Alert Processing Queue
When the Queue Notification Email is received from the Notification Queue
Then there is no active alert where the 
event type is Bill Acceptor Present
pay station is 1
severity is Clear
And the alert email is not sent

Scenario:  An XMLRPC message from a pay station triggers an alert
Needs customer alert type for email
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a location where the 
location name is Downtown
And there exists a pay station where the  
name is testpaystation
customer is Mackenzie Parking
pay station type is LUKE II
location is Downtown
And there exists a Event Data where the 
type is Printer
Action is PaperOut
timestamp is now
severity is Critical
pay station is testpaystation
And there exists a customer alert where the 
customer is Mackenzie Parking
name is Printer Paper Out
threshold is 1
threshold type is Pay Station Alert
customer email is test@test.com
is active
And there exists a active alert where the 
customer alert is Printer Paper Out
pay station is testpaystation
severity is Clear
is not active
When the testpaystation sends Printer event Paper Out Cleared
When the Queue Event is received from the Current Status Queue
And the Queue Event is received from the Alert Processing Queue
Then a new Notification is placed in the Notification Queue where the 
Address is test@test.com,
Subject is alert.email.paystation.subject.template
When the Queue Event is received from the Historical Alert Processing Queue
When the Queue Notification Email is received from the Notification Queue
Then there exists a active alert where the 
event type is Paper Status
pay station is testpaystation
severity is Critical
is Active
And there exists a event history where the 
event type is Paper Status
pay station is testpaystation
severity is Critical
is active
Action Type is Empty
And the alert email is sent successfully
And there exists a Alert status where the  
pay station is testpaystation
Critical Pay Station Alerts Count is 1
Minor Pay Station Alerts Count is 0
Major Pay Station Alerts Count is 0

Scenario:  An XMLRPC message from a pay station clears an alert
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a location where the 
Location Name is ThisIsALocation
And there exists a pay station where the  
name is testpaystation
customer is Mackenzie Parking
pay station type is LUKE II
Location is ThisIsALocation
Critical Pay Station Alerts Count is 1
And there exists a active alert where the 
Event Type is Paper Status
pay station is testpaystation
severity is Critical
is active
And there exists a Event Data where the 
type is Printer
Action is PaperOutCleared
timestamp is now
severity is Clear
pay station is testpaystation
And there exists a customer alert where the 
customer is Mackenzie Parking
name is Pay Station Events
threshold is 1
threshold type is Pay Station Alert
customer email is test@test.com 
When the testpaystation sends Printer event Paper Out Cleared
And the Queue Event is received from the Current Status Queue
And the Queue Event is received from the Alert Processing Queue
And the Queue Event is received from the historical event Queue
Then a new Notification is placed in the Notification Queue where the 
Address is test@test.com,
Subject is alert.recovery.email.paystation.subject.template
When the Queue Notification Email is received from the Notification Queue
Then there exists a active alert where the 
Event Type is Paper Status
pay station is testpaystation
severity is Clear
And there exists a event history where the 
event type is Paper Status
pay station is testpaystation
severity is Clear
Action Type is Cleared
And the alert email is sent successfully
And there exists a Alert status where the  
pay station is testpaystation
Critical Pay Station Alerts Count is 0

Scenario:  An XMLRPC message from a pay station triggers an alert, but the same alert is already active
Given there exists a customer where the 
customer Name is Mackenzie Parking
And there exists a location where the 
Location Name is ThisIsALocation
And there exists a pay station where the  
name is testpaystation
customer is Mackenzie Parking
pay station type is LUKE II
Location is ThisIsALocation
Critical Pay Station Alerts Count is 1
And there exists a customer alert where the 
customer is Mackenzie Parking
name is Pay Station Events
threshold is 1
threshold type is Pay Station Alert
customer email is test@test.com 
And there exists a active alert where the 
Event Type is Paper Status
pay station is testpaystation
severity is Critical
is active
alert GMT is 12/16/2015 10:00
And there exists a Event Data where the 
type is Printer
Action is PaperOut
timestamp is 12/17/2015 10:00
severity is Critical
pay station is testpaystation
When the testpaystation sends Printer event Paper Out
And the Queue Event is received from the Current Status Queue
And the Queue Event is received from the Alert Processing Queue
And the Queue Event is received from the Historical event Queue
Then a new Notification is not placed in the Notification Queue where the 
Address is test@test.com,
When the Queue Notification Email is received from the Notification Queue
Then the alert email is not sent
And there exists a Alert status where the  
pay station is testpaystation
Critical Pay Station Alerts Count is 1
Minor Pay Station Alerts Count is 0
Major Pay Station Alerts Count is 0
And there exists a active alert where the  
event type is Paper Status
pay station is testpaystation
severity is Critical
alert GMT is 12/17/2015 10:00
