Narrative: 
In order to verify if the paystation alert count is correct
As a User
I want to test if various events update the posalertstatus correctly

Scenario: Paystation events  severity change
Given there exists a pay station where the 
name is testpaystation
And there is a Queue Event in the Recent Event Queue where the 
Pay Station is testpaystation
event type is Paper Status
severity is Major
action type is Low
Event Time is now
is active
When the Queue Event is received from the Recent Event Queue
Then there exists a Alert status where the 
Pay station is testpaystation
Major Pay Station Alerts Count is 1
Given there is a Queue Event in the Recent Event Queue where the 
pay station is testpaystation
event type is Paper Status
severity is Critical
action type is Empty
Event Time is now
is active
When the Queue Event is received from the Recent Event Queue
Then there exists a Alert status where the 
Pay station is testpaystation
Major Pay Station Alerts Count is 0
Critical Pay Station Alerts Count is 1

Scenario: Accumulation of alerts of same severity
Given there exists a pay station where the
name is testpaystation
And there is a Queue Event in the Recent Event Queue where the 
pay station is testpaystation
event type is Paper Status
severity is Critical
action type is Empty
Event Time is now
is active
When the Queue Event is received from the Recent Event Queue
Then there exists a Alert status where the 
pay station is testpaystation
Critical Pay Station Alerts Count is 1
Given there is a Queue Event in the Recent Event Queue where the 
pay station is testpaystation
event type is Printer Paper Jam
severity is Critical
action type is Alerted
Event Time is now
is active
When the Queue Event is received from the Recent Event Queue
Then there exists a Alert status where the 
pay station is testpaystation
Critical Pay Station Alerts Count is 2

Scenario: Alert and then clear the alert
Given there exists a pay station where the
name is testpaystation
And there is a Queue Event in the Recent Event Queue where the 
pay station is testpaystation
event type is Paper Status
severity is Critical
action type is Empty
Event Time is now
is active
When the Queue Event is received from the Recent Event Queue
Then there exists a Alert status where the 
pay station is testpaystation
Critical Pay Station Alerts Count is 1
Given there is a Queue Event in the Recent Event Queue where the 
pay station is testpaystation
event type is Paper Status
severity is Clear
action type is Cleared
Event Time is now
When the Queue Event is received from the Recent Event Queue
Then there exists a Alert status where the 
pay station is testpaystation
Critical Pay Station Alerts Count is 0

Scenario: Ignore out of order older pay station event
Given there exists a pay station where the
name is testpaystation
And there is a Queue Event in the Recent Event Queue where the 
pay station is testpaystation
event type is Paper Status
severity is Critical
action type is Empty
Event Time is now
is active
When the Queue Event is received from the Recent Event Queue
Then there exists a Alert status where the 
pay station is testpaystation
Critical Pay Station Alerts Count is 1
Given there is a Queue Event in the Recent Event Queue where the 
pay station is testpaystation
event type is Paper Status
severity is Major
action type is Low
Event Time is 30 minutes ago
When the Queue Event is received from the Recent Event Queue
Then there exists a Alert status where the 
pay station is testpaystation
Critical Pay Station Alerts Count is 1

Scenario: Minor Alert for Collection Alert and upgrade to Major
Given there exists a pay station where the
name is testpaystation
And there exists a customer alert where the 
customer is Mackenzie Parking
name is Coin Collection Test
threshold is 100
threshold type is Coin Canister Count
And there is a Queue Event in the Recent Event Queue where the 
pay station is testpaystation
event type is Customer Defined Alert
customer alert is Coin Collection Test
severity is Minor
Event Time is now
is active
When the Queue Event is received from the Recent Event Queue
Then there exists a Alert status where the 
pay station is testpaystation
Minor Collection Alerts Count is 1
Given there is a Queue Event in the Recent Event Queue where the 
pay station is testpaystation
event type is Customer Defined Alert
customer alert is Coin Collection Test
severity is Major
Event Time is now
is active
When the Queue Event is received from the Recent Event Queue
Then there exists a Alert status where the  
pay station is testpaystation
Minor Collection Alerts Count is 0
Major Collection Alerts Count is 1