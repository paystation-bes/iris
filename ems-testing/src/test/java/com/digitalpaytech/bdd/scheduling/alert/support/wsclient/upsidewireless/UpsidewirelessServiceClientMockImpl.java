package com.digitalpaytech.bdd.scheduling.alert.support.wsclient.upsidewireless;

import java.util.HashMap;
import java.util.Map;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.TestConstants;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.upsidewireless.webservice.sms.SMSSendResult;

public final class UpsidewirelessServiceClientMockImpl {
    
    private UpsidewirelessServiceClientMockImpl() {
    }
    
    public static Answer<SMSSendResult> sendRequest() {
        return new Answer<SMSSendResult>() {
            @Override
            public SMSSendResult answer(final InvocationOnMock invocation) throws Throwable {
                final Map<String, Object> expectedResult = new HashMap<String, Object>(1);
                expectedResult.put(TestConstants.SMS_MESSAGE, TestConstants.SMS_MESSAGE_SENT);
                final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
                wrapperObject.setExpectedResult(expectedResult);
                final SMSSendResult smsSendResult = new SMSSendResult();
                smsSendResult.setIsOk(true);
                return smsSendResult;
            }
        };
    }
    
}
