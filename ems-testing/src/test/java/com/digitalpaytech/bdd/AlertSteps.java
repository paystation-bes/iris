package com.digitalpaytech.bdd;

import java.util.Queue;

import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.Aliases;
import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.mockito.MockitoAnnotations;

import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandlers;
import com.digitalpaytech.bdd.util.ObjectConstructionException;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.util.TestEventQueue;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.bdd.util.expression.StoryObjectBuilder;
import com.digitalpaytech.domain.AlertThresholdType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAlertEmail;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.CustomerEmail;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosBalance;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.domain.PosEventHistory;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.dto.paystation.EventData;
import com.digitalpaytech.dto.queue.QueueCustomerAlertType;
import com.digitalpaytech.dto.queue.QueueEvent;
import com.digitalpaytech.dto.queue.QueueNotificationEmail;
import com.digitalpaytech.util.WebCoreConstants;

@SuppressWarnings({ "PMD.ExcessiveImports", "PMD.UseObjectForClearerAPI" })
public final class AlertSteps extends AbstractSteps {
    
    private static final String SENT_STRING = "sent";
    private static final String ALERT_EMAIL = "alertEmail";
    private static final String NOT_STRING = "not";
    
    public AlertSteps(final JBehaveTestHandlers testHandlers) {
        super(testHandlers);
    }
    
    protected final Class<?>[] getStoryObjectTypes() {
        
        return new Class[] { Customer.class, PointOfSale.class, CustomerAlertType.class, PosBalance.class, QueueCustomerAlertType.class,
            AlertThresholdType.class, Route.class, CustomerAlertEmail.class, CustomerEmail.class, QueueEvent.class, Location.class,
            PosEventCurrent.class, PosHeartbeat.class, QueueNotificationEmail.class, EventData.class, PosEventHistory.class, };
    }
    
    @BeforeScenario
    public final void setUpForController() {
        MockitoAnnotations.initMocks(this);
        final ControllerFieldsWrapper controllerFields = new ControllerFieldsWrapper();
        TestDBMap.saveControllerFieldsWrapperToMem(controllerFields);
        TestLookupTables.getExpressionParser().register(getStoryObjectTypes());
        final TestEventQueue testEventQueue = new TestEventQueue();
        TestDBMap.saveTestEventQueueToMem(testEventQueue);
    }
    
    /**
     * 
     * Adds the PosBalance for the Pay station mentioned based on the coin & the Bill count passed.
     * 
     * @.example the pay station *testpaystation* has a balance *100* coin count and *150* bill count
     * @param posName
     *            Name of the Pay station for which the balance is being updated
     * @param coin
     *            The coin count
     * @param bill
     *            The bill count
     */
    @Given("pay station $posName has a balance $coin coin count and $bill bill count")
    public final void posBalance(final String posName, final String coin, final String bill) {
        final StoryObjectBuilder objBuilder = TestLookupTables.getObjectBuilder();
        final PosBalance posBalance = (PosBalance) objBuilder.instantiate("balance");
        posBalance.setCoinCount(Short.valueOf(coin));
        posBalance.setCoinAmount(Integer.valueOf(coin));
        posBalance.setBillCount(Short.valueOf(bill));
        posBalance.setBillAmount(Integer.valueOf(bill));
        final PointOfSale pos = (PointOfSale) TestDBMap.findObjectByFieldFromMemory(posName, PointOfSale.ALIAS_NAME, PointOfSale.class);
        posBalance.setPointOfSaleId(Integer.valueOf(pos.getId()));
        posBalance.setPointOfSale(pos);
        final int id = (int) (Math.random() * 100);
        TestDBMap.addToMemory(id, posBalance);
//        Assert.assertTrue(TestDBMap.findObjectByIdFromMemory(id, posBalance.getClass()) == posBalance);
    }
    
    /**
     * 
     * Puts the Object described in the desired Queue.
     * 
     * @.example there is a *User Defined Alert Trigger* in the *Customer Alert Type* Queue where the
     *           *Type is Removed
     *           Pay station are 1 , 2
     *           Customer Alerts are Coin Collection Test*
     * @param objectType
     *            Type of Object to add in the Queue
     * @param queueType
     *            The Queue to which the Object is added
     * @param data
     *            The data required to construct the objectType
     */
    @Given("there is a $objectType in the $queueType Queue where the $data")
    @Aliases(values = { "there is a $objectType in the $queueType queue where the $data", })
    public final void putQueueEventInQueue(final String objectType, final String queueType, final String data) {
        final StoryObjectBuilder objBuilder = TestLookupTables.getObjectBuilder();
        final Object instance = objBuilder.instantiate(objectType);
        TestLookupTables.getExpressionParser().parse(data, instance);
        
        Object id = objBuilder.generateId(instance);
        if (id == null) {
            id = objBuilder.getTypeManager().random(Long.class);
        }
        final TestEventQueue teq = TestDBMap.getTestEventQueueFromMem();
        teq.offerToQueue(instance, queueType);
        TestDBMap.saveTestEventQueueToMem(teq);
    }
    
    /**
     * 
     * Pulls the mentioned object out from the desired Queue for processing.
     * 
     * @.example the *Queue Event* is received from the *Current Status Queue*
     * @.example the *Queue Event* is received from the *Current Status Queue*
     *           
     * @param objectType
     *            Type of Object to get from the Queue.
     * @param queueType
     *            The Queue from which the Object is pulled.
     */
    @When("the $objectType is received from the $queueType Queue")
    @Aliases(values = { "the $objectType is received from the $queueType queue", })
    public final void triggerIsRecievedFromQueue(final String objectType, final String queueType) throws ObjectConstructionException {
        testHandlers.invokePerformMethod("recieved" + objectType, queueType);
    }
    
    /**
     * 
     * Asserts whether the Object with the data mentioned is present in the desired Queue. 
     * 
     * @.example Then a new *Queue Event* is *placed* in the *Current Status* Queue where the
     *           *event type is Shock alarm
     *           severity is Clear
     *           Pay station is ps2*
     * @.example Then a new *Queue Event* is *not placed* in the *Current Status* Queue where the
     *           *event type is Shock alarm
     *           severity is Clear
     *           Pay station is ps2*          
     * @param objectType
     *           Type of Object to be asserted for existence.
     * @param placed
     *           Existence assertion based on this parameter. "Not" in the string forces non existence assertion.
     * @param queueType
     *           The Queue where the object is asserted.
     * @param data
     *           Object to be asserted is constructed based on this parameter.         
     *           
     *            
     */
    @Then("a new $objectType is $placed in the $queueType Queue where the $data")
    public final void newEventPlacedInQueueType(final String objectType, final String placed, final String queueType, final String data) {
        final TestEventQueue tq = TestDBMap.getTestEventQueueFromMem();
        final Queue<Object> eventList = tq.getQueue(queueType);
        
        final StoryObjectBuilder objBuilder = TestLookupTables.getObjectBuilder();
        final Byte queueTypeId = tq.getQueueTypeFromString(queueType);
        switch (queueTypeId) {
            case WebCoreConstants.QUEUE_TYPE_CUSTOMER_ALERT_TYPE:
                final QueueCustomerAlertType expectedQCAT = (QueueCustomerAlertType) objBuilder.instantiate(QueueCustomerAlertType.ALIAS);
                TestLookupTables.getExpressionParser().parse(data, expectedQCAT);
                break;
            case WebCoreConstants.QUEUE_TYPE_RECENT_EVENT:
            case WebCoreConstants.QUEUE_TYPE_PROCESSING_EVENT:
            case WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT:
                final QueueEvent expectedQE = (QueueEvent) objBuilder.instantiate(QueueEvent.ALIAS);
                TestLookupTables.getExpressionParser().parse(data, expectedQE);
                break;
            case WebCoreConstants.QUEUE_TYPE_NOTIFICATION_EMAIL:
                final QueueNotificationEmail expectedQNE = (QueueNotificationEmail) objBuilder.instantiate(QueueNotificationEmail.ALIAS);
                TestLookupTables.getExpressionParser().parse(data, expectedQNE);
                break;
            default:
                break;
                
        }
        
        if (placed.contains(NOT_STRING)) {
            TestLookupTables.getExpressionParser().assertNotExisted("Unexpected Queue Event was found in Queue " + queueType, eventList, data);
        } else {
            TestLookupTables.getExpressionParser().assertExisted("Expected Queue Event was not found in Queue " + queueType, eventList, data);
        }
    }
    
    /**
     * 
     * Asserts successful assertion of an action on an object
     * 
     * @.example Then the *Historical Queue Event* is saved successfully where the
     *           *Pay station is testpaystation
     *           event type is Paper Status
     *           severity is Major
     *           action type is low
     *           active is true*       
     * @param object
     *           Type of Object to be asserted
     * @param action
     *           Action to be performed on the object
     * @param data
     *           Object to be asserted is constructed based on this parameter.         
     *            
     */
    @Then("the $object is $action successfully where the $data")
    public final void assertAction(final String object, final String action, final String data) {
        testHandlers.invokeAssertSuccessMethod(object, action, data);
    }
    
    /**
     * 
     * Asserts that the alert email is sent successfully
     * 
     * @.example the alert email is sent successfully
     * @.example the alert email is sent
     *            
     */   
    @Then("the alert email is sent successfully")
    @Aliases(values = { "the alert email is sent" })
    public final void assertEmailSent() throws ObjectConstructionException {
        testHandlers.invokeAssertSuccessMethod(ALERT_EMAIL, SENT_STRING, "1");
    }
    
    
    /**
     * 
     * Asserts that no alert email is sent.
     * 
     * @.example the alert email is not sent
     * @.example the alert email is not sent successfully
     *            
     */   
    @Then("the alert email is not sent")
    @Aliases(values = "the alert email is not sent successfully")
    public final void assertNoEmailSent() throws ObjectConstructionException {
        testHandlers.invokeAssertFailureMethod(ALERT_EMAIL, NOT_STRING + SENT_STRING);
    }
    
    /**
     * 
     * Asserts that certain number of alert emails have been sent.
     * 
     * @.example the *4* alert emails are sent successfully
     *            
     */  
    @Then("the $n alert emails are sent successfully")
    public final void assertNEmailsSent(final String n) throws ObjectConstructionException {
        testHandlers.invokeAssertSuccessMethod(ALERT_EMAIL, SENT_STRING, n);
    }
    
    /**
     * 
     * Triggers a background process event of a specific type
     * 
     * @.example the *Add Customer Alert Type Event* is triggered
     * 
     * @param objectType
     *           Type of event to be triggered
     *            
     */
    @When("the $objectType is triggered")
    public final void whenIsTriggered(final String objectType) throws ObjectConstructionException {
        testHandlers.invokePerformMethod("calculate", objectType);
    }
    
}
