package com.digitalpaytech.bdd.services;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.util.TestEventQueue;

public final class QueueEventPublisherMockImpl {
    private QueueEventPublisherMockImpl() {
    }
    
    public static Answer<Integer> offer(final byte queueType) {
        return new Answer<Integer>() {
            
            @Override
            public Integer answer(final InvocationOnMock invocation) throws Throwable {
                
                final Object[] args = invocation.getArguments();
                final Object queueEvent = (Object) args[0];
                
                final TestEventQueue testEventQueue = TestDBMap.getTestEventQueueFromMem();
                testEventQueue.offerToQueue(queueEvent, queueType);
                return null;
            }
        };
        
    }
}
