package com.digitalpaytech.bdd.services;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.DBHelper;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.expression.StoryObjectBuilder;

public class EntityDaoMock {
    private EntityDaoMock() {
    }
    
    public static Answer<Object> get() {
        return new Answer<Object>() {
            @SuppressWarnings("unchecked")
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                return TestContext.getInstance().getDatabase()
                        .findById((Class<Object>) invocation.getArgumentAt(0, Class.class), invocation.getArgumentAt(1, Object.class));
            }
        };
    }
    
    public static <T> Answer<List<T>> loadAll() {
        return new Answer<List<T>>() {
            @SuppressWarnings("unchecked")
            @Override
            public List<T> answer(final InvocationOnMock invocation) throws Throwable {
                return (List<T>) TestContext.getInstance().getDatabase().find((Class<Object>) invocation.getArgumentAt(0, Class.class));
            }
        };
    }
    
    public static Answer<Void> flush() {
        return new Answer<Void>() {
            @Override
            public Void answer(final InvocationOnMock invocation) throws Throwable {
                return null;
            }
        };
    }
    
    public static Answer<Void> evict() {
        return new Answer<Void>() {
            @Override
            public Void answer(final InvocationOnMock invocation) throws Throwable {
                return null;
            }
        };
    }
    
    public static Answer<Serializable> save() {
        return new Answer<Serializable>() {
            @Override
            public Serializable answer(final InvocationOnMock invocation) throws Throwable {
                return save(invocation.getArgumentAt(0, Object.class));
            }
        };
    }
    
    public static Answer<Void> saveOrUpdate() {
        return new Answer<Void>() {
            @Override
            public Void answer(final InvocationOnMock invocation) throws Throwable {
                merge(invocation.getArgumentAt(0, Object.class));
                return null;
            }
        };
    }
    
    public static Answer<Void> update() {
        return new Answer<Void>() {
            @Override
            public Void answer(final InvocationOnMock invocation) throws Throwable {
                final Object target = invocation.getArgumentAt(0, Object.class);
                final Object existing = existing(target);
                if (existing == null) {
                    throw new IllegalArgumentException("Non-existing object!");
                }
                
                PropertyUtils.copyProperties(existing, target);
                
                return null;
            }
        };
    }
    
    public static Answer<Object> merge() {
        return new Answer<Object>() {
            @Override
            public Object answer(final InvocationOnMock invocation) throws Throwable {
                return merge(invocation.getArgumentAt(0, Object.class));
            }
        };
    }
    
    public static Answer<Query> getNamedQuery() {
        return new Answer<Query>() {
            @Override
            public Query answer(final InvocationOnMock invocation) throws Throwable {
                return TestContext.getInstance().getDatabase().session().getNamedQuery(invocation.getArgumentAt(0, String.class));
            }
        };
    }
    
    public static Answer<List> findByNamedQueryAndNamedParam() {
        return new Answer<List>() {
            @Override
            public List answer(final InvocationOnMock invocation) throws Throwable {
                return TestContext.getInstance().getDatabase().session().getNamedQuery(invocation.getArgumentAt(0, String.class))
                        .setParameter(invocation.getArgumentAt(1, String.class), invocation.getArgumentAt(2, Object.class)).list();
            }
        };
    }
    
    @SuppressWarnings("unchecked")
    private static <T> T existing(final T obj) {
        T result = null;
        
        final StoryObjectBuilder builder = TestContext.getInstance().getObjectBuilder();
        final Object id = builder.getId(obj);
        if (id != null) {
            result = (T) TestContext.getInstance().getDatabase().findById(obj.getClass(), id);
        }
        
        return result;
    }
    
    private static Serializable save(final Object target) {
        final StoryObjectBuilder builder = TestContext.getInstance().getObjectBuilder();
        TestContext.getInstance().getDatabase().save(target);
        
        return (Serializable) builder.getId(target);
    }
    
    private static <T> T merge(final T target) {
        T existing = existing(target);
        if (existing == null) {
            save(target);
            existing = target;
        } else {
            try {
                PropertyUtils.copyProperties(existing, target);
            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                throw new RuntimeException("Failed to copy attributes!", e);
            }
        }
        
        return existing;
    }
}
