package com.digitalpaytech.bdd.services;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.domain.PointOfSale;

public final class PointOfSaleServiceMockImpl {
    
    private PointOfSaleServiceMockImpl() {
    }
    
    public static Answer<PointOfSale> findPointOfSaleById() {
        return new Answer<PointOfSale>() {
            @Override
            public PointOfSale answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                return (PointOfSale) TestDBMap.findObjectByIdFromMemory(args[0], PointOfSale.class);
            }
        };
    }
    
}
