package com.digitalpaytech.bdd.services;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.util.EntityMap;

public final class CollectionTypeServiceMockImpl {
    private CollectionTypeServiceMockImpl() {
    }

    @Deprecated
    public static Answer<List<Integer>> calculatePaystationBalance() {
        return new Answer<List<Integer>>() {
            @Override
            public List<Integer> answer(final InvocationOnMock invocation) {
                final List<Integer> posIdList = new ArrayList<Integer>();
                @SuppressWarnings("unchecked")
                final Collection<PointOfSale> posList = (Collection<PointOfSale>) TestDBMap.getTypeListFromMemory(PointOfSale.class);
                collectPosIds(posList, posIdList);
                return posIdList;
            }
        };
    }
    
    public static Answer<List<Integer>> calculatePaystationBalanceMock() {
        return new Answer<List<Integer>>() {
            @Override
            public List<Integer> answer(final InvocationOnMock invocation) {
                final Collection<PointOfSale> posList = EntityMap.INSTANCE.findDefaultPointOfSales(2);
                final List<Integer> posIdList = new ArrayList<Integer>(posList.size());
                return collectPosIds(posList, posIdList);
            }
        };
    }
    
    private static List<Integer> collectPosIds(final Collection<PointOfSale> posList, final List<Integer> posIdList) {
        final Iterator<PointOfSale> itr = posList.iterator();
        while (itr.hasNext()) {
            final PointOfSale pos = itr.next();
            posIdList.add(pos.getId());
        }
        return posIdList;
    }
}
