package com.digitalpaytech.bdd.services;

import java.util.List;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.ThirdPartyServiceAccount;

public final class PermitServiceMockImpl {
    
    private PermitServiceMockImpl() {
    }
    
    public static Answer<Permit> findPermitByIdForSms() {
        return new Answer<Permit>() {
            @Override
            public Permit answer(final InvocationOnMock invocation) throws Throwable {
                               
                final Permit permit = TestContext.getInstance().getDatabase().find(Permit.class).get(0);                
                //final Purchase purchase = permit.getPurchase();
                //final PointOfSale pos = new PointOfSale();
                //pos.setId(1);
                //purchase.setPointOfSale(pos);
                //permit.setPurchase(purchase);
                //TestDBMap.addToMemory(purchase.getId(), purchase);
                //TestDBMap.addToMemory(pos.getId(), pos);
                return permit;
            }
        };
    }
    
}
