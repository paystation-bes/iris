package com.digitalpaytech.bdd.services;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.domain.Purchase;

public final class PurchaseServiceMockImpl {
    
    private PurchaseServiceMockImpl() {
    }
    
    public static Answer<Purchase> findPurchaseById() {
        return new Answer<Purchase>() {
            @Override
            public Purchase answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                return (Purchase) TestDBMap.findObjectByIdFromMemory(args[0], Purchase.class);
            }
        };
    }
    
}
