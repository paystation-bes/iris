package com.digitalpaytech.bdd.services;

import java.util.Date;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.domain.CreditCardType;

public class TransactionFacadeMockImpl {
    
    public static Answer<CreditCardType> findCreditCardType() {
        return new Answer<CreditCardType>() {
            @Override
            public CreditCardType answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                //TODO: Make a map of the various cc types and use args to get right one
                final CreditCardType cct = new CreditCardType(2, "VISA", new Date(), 1);                               
                return cct;
            }
        };
    }       
    
}
