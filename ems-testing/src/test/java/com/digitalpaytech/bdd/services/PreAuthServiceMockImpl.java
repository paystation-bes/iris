package com.digitalpaytech.bdd.services;

import java.util.List;
import java.util.ArrayList;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.domain.PreAuth;

public final class PreAuthServiceMockImpl {
    private PreAuthServiceMockImpl() {
    }
    
    public static Answer<List<PreAuth>> findExpiredPreAuthsForMigratedCustomers() {
        return new Answer<List<PreAuth>>() {
            @Override
            public List<PreAuth> answer(final InvocationOnMock invocation) throws Throwable {
                return createPreAuths(5);
            }
        };
    }
    
    public static Answer<List<PreAuth>> getExpiredCardAuthorizationBackUp() {
        return new Answer<List<PreAuth>>() {
            @Override
            public List<PreAuth> answer(final InvocationOnMock invocation) throws Throwable {
                return new ArrayList<PreAuth>();
            }
        };
    }
    

    
    private static List<PreAuth> createPreAuths(final int size) {
        final List<PreAuth> list = new ArrayList<PreAuth>(size);
        for (int i = 1; i <= size; i++) {
            PreAuth preAuth = new PreAuth();
            preAuth.setId(Long.valueOf(i));
            list.add(preAuth);
        }
        return list;
    }
}
