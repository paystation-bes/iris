package com.digitalpaytech.bdd.services.util;

import java.util.Date;
import java.util.Locale;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PaymentType;
import com.digitalpaytech.domain.PaystationSetting;
import com.digitalpaytech.domain.PermitType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.TransactionType;

@SuppressWarnings({ "checkstyle:magicnumber" })
public final class TransactionFacadeMock {
    
    private static final String UNKNOWN = "Unknown";
    private static final String REGULAR = "Regular";
    
    private TransactionFacadeMock() {
    }
    
    public static Answer<String> decryptCardData() {
        return new Answer<String>() {
            @Override
            public String answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                return (String) args[0];
            }
        };
    }
    
    public static Answer<String> encryptCardData() {
        return new Answer<String>() {
            @Override
            public String answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                return (String) args[1];
            }
        };
    }
    
    public static Answer<Boolean> isCreditCard() {
        return new Answer<Boolean>() {
            @Override
            public Boolean answer(final InvocationOnMock invocation) throws Throwable {
                return true;
            }
        };
    }
    
    public static Answer<PreAuth> findApprovedPreAuth() {
        return new Answer<PreAuth>() {
            @Override
            public PreAuth answer(final InvocationOnMock invocation) throws Throwable {
                final String num = "12345678";
                final PreAuth pa = new PreAuth();
                pa.setId(invocation.getArgumentAt(0, long.class));
                pa.setCardData("4030000010001234=1907101000000012300XXXXX");
                pa.setCardHash("TEST_HASH");
                pa.setAmount(2);
                pa.setCardExpiry((short) 2201);
                pa.setAuthorizationNumber(num);
                pa.setCardType("VISA");
                pa.setReferenceNumber(num);
                pa.setPreAuthDate(new Date());
                pa.setPsRefId("535");
                pa.setLast4digitsOfCardNumber((short) 1234);
                
                final MerchantAccount ma = new MerchantAccount();
                ma.setId(1);
                pa.setMerchantAccount(ma);
                
                final PointOfSale pos = new PointOfSale();
                pos.setId(1);
                pa.setPointOfSale(pos);
                return pa;
            }
        };
    }
    
    public static Answer<PaymentType> findPaymentType() {
        return new Answer<PaymentType>() {
            @Override
            public PaymentType answer(final InvocationOnMock invocation) throws Throwable {
                final PaymentType pt = new PaymentType();
                pt.setId((byte) 2);
                pt.setName("CC (Swipe)");
                pt.setDescription("Credit Card Swipe Only");
                return pt;
            }
        };
    }
    
    public static Answer<TransactionType> findTransactionType() {
        return new Answer<TransactionType>() {
            @Override
            public TransactionType answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                int id = 2;
                String name = REGULAR;
                String desc = "Permit";
                if (args.length > 0) {
                    switch (((String) args[0]).toLowerCase(Locale.getDefault())) {
                        case "unknown":
                            id = 0;
                            name = UNKNOWN;
                            desc = UNKNOWN;
                            break;
                        case "addtime":
                            id = 1;
                            name = "AddTime";
                            desc = "Permit Extension";
                            break;
                        case "screcharge":
                            id = 4;
                            name = "SCRecharge";
                            desc = "Smart Card Recharge";
                            break;
                        case "cancelled":
                            id = 5;
                            name = "Cancelled";
                            desc = "Cancelled Purchase";
                            break;
                        case "test":
                            id = 6;
                            name = "Test";
                            desc = "Test Purchase";
                            break;
                        case "monthly":
                            id = 7;
                            name = "Monthly";
                            desc = "Monthly Permit";
                            break;
                        case "addtime ebp":
                            id = 8;
                            name = "AddTime EbP";
                            desc = "Extend by Phone Permit Extension";
                            break;
                        case "regular ebp":
                            id = 9;
                            name = "Regular EbP";
                            desc = "Extend by Phone Permit";
                            break;
                        default:
                            break;
                    }
                }
                
                final TransactionType tt = new TransactionType();
                tt.setId(id);
                tt.setName(name);
                tt.setDescription(desc);
                return tt;
            }
        };
    }
    
    public static Answer<CardType> findCardType() {
        return new Answer<CardType>() {
            @Override
            public CardType answer(final InvocationOnMock invocation) throws Throwable {
                final CardType ct = new CardType();
                ct.setId(1);
                ct.setName("Credit Card");
                return ct;
            }
        };
    }
    
    public static Answer<PermitType> findPermitType() {
        return new Answer<PermitType>() {
            @Override
            public PermitType answer(final InvocationOnMock invocation) throws Throwable {
                final PermitType pt = new PermitType();
                pt.setId(1);
                pt.setName(REGULAR);
                return pt;
            }
        };
    }
    
    public static Answer<PaystationSetting> findOrCreatePaystationSetting() {
        return new Answer<PaystationSetting>() {
            @Override
            public PaystationSetting answer(final InvocationOnMock invocation) throws Throwable {
                final PaystationSetting ps = new PaystationSetting();
                ps.setId(1);
                //                ps.setName("Parking");
                
                final Customer c = new Customer();
                c.setId(1);
                ps.setCustomer(c);
                return ps;
            }
        };
    }
}
