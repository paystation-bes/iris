package com.digitalpaytech.bdd.services;

import java.util.Map;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;


public final class MessageHelperMock {
    
    private MessageHelperMock() {
        
    }
    
    public static Answer<String> getMessage(final Map<String, String> labelsMapping) {
        return new Answer<String>() {
            @Override
            public String answer(final InvocationOnMock invocation) {
                return labelsMapping.get(invocation.getArgumentAt(0, String.class));
            }
        };
    }
}
