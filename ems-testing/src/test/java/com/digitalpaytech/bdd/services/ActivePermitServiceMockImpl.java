package com.digitalpaytech.bdd.services;

import java.util.List;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.domain.ActivePermit;

public final class ActivePermitServiceMockImpl {
    
    private ActivePermitServiceMockImpl() {
    }
    
    public static Answer<ActivePermit> findActivePermitByOriginalPermitId() {
        return new Answer<ActivePermit>() {
            @Override
            public ActivePermit answer(final InvocationOnMock invocation) throws Throwable {
                List<ActivePermit> apList = TestContext.getInstance().getDatabase().find(ActivePermit.class);
                ActivePermit ap = null;
                if (!apList.isEmpty()) {
                    ap = apList.get(0);
                }
                return ap;
            }
        };
    }
    
}
