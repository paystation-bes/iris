package com.digitalpaytech.bdd.services;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public final class EventExceptionServiceMockImpl {
    
    private EventExceptionServiceMockImpl() {        
    }
    
    public static Answer<Boolean> isException() {
        return new Answer<Boolean>() {
            
            @Override
            public Boolean answer(final InvocationOnMock invocation) throws Throwable {                
                return false;
            }
        };
    }    
    
}
