package com.digitalpaytech.bdd.services;

import java.util.List;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.DBHelper;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.domain.ExtensiblePermit;
import com.digitalpaytech.dto.SmsAlertInfo;

public final class ExtensiblePermitServiceMockImpl {
    
    private ExtensiblePermitServiceMockImpl() {
    }
    
    public static Answer<ExtensiblePermit> findByMobileNumber() {
        return new Answer<ExtensiblePermit>() {
            @Override
            public ExtensiblePermit answer(final InvocationOnMock invocation) throws Throwable {
                return (ExtensiblePermit) TestContext.getInstance().getDatabase().find(ExtensiblePermit.class).get(0);
            }
        };
    }
    
    public static Answer<SmsAlertInfo> getSmsAlertByParkerReply() {
        return new Answer<SmsAlertInfo>() {
            @Override
            public SmsAlertInfo answer(final InvocationOnMock invocation) throws Throwable {
                final List<SmsAlertInfo> infos = DBHelper.list(SmsAlertInfo.class);
                
                if (infos != null) {
                    for (SmsAlertInfo alertInfo : infos) {
                        if (invocation.getArgumentAt(0, String.class).equals(alertInfo.getMobileNumber())) {
                            return alertInfo;
                        }
                    }
                }
                return null;
            }
        };
    }
    
}
