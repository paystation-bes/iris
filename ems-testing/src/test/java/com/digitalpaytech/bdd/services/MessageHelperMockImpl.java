package com.digitalpaytech.bdd.services;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public final class MessageHelperMockImpl {
    private MessageHelperMockImpl() {
        
    }
    
    public static Answer<String> getMessageEcho() {
        return new Answer<String>() {
            @Override
            public String answer(final InvocationOnMock invocation) throws Throwable {
                return invocation.getArgumentAt(0, String.class);
            }
        };
    }
}
