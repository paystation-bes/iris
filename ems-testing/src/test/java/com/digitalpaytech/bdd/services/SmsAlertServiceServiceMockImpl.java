package com.digitalpaytech.bdd.services;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.domain.SmsAlert;

public final class SmsAlertServiceServiceMockImpl {
    private SmsAlertServiceServiceMockImpl() {
    }
    
    public static Answer<SmsAlert> findUnexpiredSmsAlertById() {
        return new Answer<SmsAlert>() {
            @Override
            public SmsAlert answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                return (SmsAlert) TestDBMap.findObjectByIdFromMemory(args[0], SmsAlert.class);
            }
        };
    }
    
}
