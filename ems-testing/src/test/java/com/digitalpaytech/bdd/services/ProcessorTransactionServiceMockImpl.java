package com.digitalpaytech.bdd.services;

import java.util.Date;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.domain.CPSData;
import com.digitalpaytech.domain.ProcessorTransactionType;
import com.digitalpaytech.domain.ProcessorTransaction;

public class ProcessorTransactionServiceMockImpl {
    
    public static Answer<ProcessorTransaction> findProcessorTransactionById() {
        return new Answer<ProcessorTransaction>() {
            @Override
            public ProcessorTransaction answer(final InvocationOnMock invocation) throws Throwable {
                return (ProcessorTransaction) TestContext.getInstance().getDatabase().find(ProcessorTransaction.class).get(0);
            }
        };
    }
    
    public static Answer<ProcessorTransaction> findProcTransByPurchaseIdTicketNoAndPurchasedDate() {
        return new Answer<ProcessorTransaction>() {
            @Override
            public ProcessorTransaction answer(final InvocationOnMock invocation) throws Throwable {
                return (ProcessorTransaction) TestContext.getInstance().getDatabase().find(ProcessorTransaction.class).get(0);
            }
        };
    }
    
    public static Answer<CPSData> findCPSDataByProcessorTransactionId() {
        return new Answer<CPSData>() {
            @Override
            public CPSData answer(final InvocationOnMock invocation) throws Throwable {
                return (CPSData) TestContext.getInstance().getDatabase().find(CPSData.class).get(0);
            }
        };
    }
    
    public static Answer<ProcessorTransactionType> getProcessorTransactionType() {
        return new Answer<ProcessorTransactionType>() {
            @Override
            public ProcessorTransactionType answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                return getTransactionType((Integer) args[0]);
            }
            
        };
    }
    
    public static ProcessorTransactionType getTransactionType(final Integer id) {
        final ProcessorTransactionType ptt;
        switch (id) {
            case 1:
                ptt = new ProcessorTransactionType(1, "Auth PlaceHolder", "PreAuth", "Authorized but not yet settled", false, new Date(), 1);
                break;
            case 2:
                ptt = new ProcessorTransactionType(2, "Real-Time", "Real-Time", "Settled with no associated refunds", false, new Date(), 1);
                break;
            case 3:
                ptt = new ProcessorTransactionType(3, "Batch", "Batched", "Charged with no associated refunds", false, new Date(), 1);
                break;
            case 4:
                ptt = new ProcessorTransactionType(4, "Refund", "Refunded", "Refund of a charge or settlement", true, new Date(), 1);
                break;
            case 5:
                ptt = new ProcessorTransactionType(5, "Reversal", "", "", false, new Date(), 1);
                break;
            case 6:
                ptt = new ProcessorTransactionType(6, "Real-Time", "Real-Time", "Settled with associated refunds", true, new Date(), 1);
                break;
            case 7:
                ptt = new ProcessorTransactionType(7, "Batch", "Batched", "Charged with associated refunds", true, new Date(), 1);
                break;
            case 8:
                ptt = new ProcessorTransactionType(8, "Uncloseable", "Uncloseable", "PreAuth expired, cannot settle", false, new Date(), 1);
                break;
            case 9:
                ptt = new ProcessorTransactionType(9, "3rd Party", "3rd Party", "Charged externally with no refunds", false, new Date(), 1);
                break;
            case 10:
                ptt = new ProcessorTransactionType(10, "Batch", "Batched", "Charged EMS retry with no refunds", false, new Date(), 1);
                break;
            case 0:
            case 11:
                ptt = new ProcessorTransactionType(11, "Store & Fwd", "Store & Fwd", "Charged SF with no refunds", false, new Date(), 1);
                break;
            case 12:
                ptt = new ProcessorTransactionType(12, "Batch", "Batched", "Charged EMS retry with refunds", true, new Date(), 1);
                break;
            case 13:
                ptt = new ProcessorTransactionType(13, "Store & Fwd", "Store & Fwd", "Charged SF with refunds", true, new Date(), 1);
                break;
            case 14:
                ptt = new ProcessorTransactionType(14, "Encryption Error", "Encryption Error", "Invalid encryption", false, new Date(), 1);
                break;
            case 17:
                ptt = new ProcessorTransactionType(17, "Real-Time", "Extend by Phone", "Charged with no associated refunds", false, new Date(), 1);
                break;
            case 18:
                ptt = new ProcessorTransactionType(18, "Refund", "Extend by Phone", "Refund of a charge or settlement", true, new Date(), 1);
                break;
            case 19:
                ptt = new ProcessorTransactionType(19, "Real-Time", "Extend by Phone", "Charged with associated refunds", true, new Date(), 1);
                break;
            case 20:
                ptt = new ProcessorTransactionType(20, "Recoverable", "Recoverable", "PreAuth expired but is recoverable", false, new Date(), 1);
                break;
            case 21:
                ptt = new ProcessorTransactionType(21, "Real-Time", "Single Trx", "Charged with no associated refunds", false, new Date(), 1);
                break;
            case 22:
                ptt = new ProcessorTransactionType(22, "Refund", "Single Trx", "Refund of a charge", true, new Date(), 1);
                break;
            case 23:
                ptt = new ProcessorTransactionType(23, "Real-Time", "Single Trx", "Charged with associated refunds", true, new Date(), 1);
                break;
            case 24:
                ptt = new ProcessorTransactionType(24, "'Expired-Refund'", "Expired Refund", "Expired Refund", true, new Date(), 1);
                break;
            case 99:
                ptt = new ProcessorTransactionType(99, "Cancel", "Cancel", "Cancelled", false, new Date(), 1);
                break;
            default:
                ptt = new ProcessorTransactionType(0, "N/A", "N/A", "N/A", false, new Date(), 1);
                break;
        }
        
        return ptt;
    }
    
}
