package com.digitalpaytech.bdd.services;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.DBHelper;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.domain.PaymentCard;

public class PaymentCardServiceMockImpl {
    
    public static Answer<PaymentCard> findByPurchaseId() {
        return new Answer<PaymentCard>() {
            @Override
            public PaymentCard answer(final InvocationOnMock invocation) throws Throwable {
                return (PaymentCard) DBHelper.list(PaymentCard.class).get(0);
            }
        };
    }
    
}
