package com.digitalpaytech.bdd.services;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import com.digitalpaytech.domain.CardRetryTransactionType;
import com.digitalpaytech.bdd.util.TestContext;

public class CardRetryTransactionTypeServiceMockImpl {

    public static Answer<CardRetryTransactionType> findCardRetryTransactionType() {
        return new Answer<CardRetryTransactionType>() {
            @Override
            public CardRetryTransactionType answer(final InvocationOnMock invocation) throws Throwable {
                return (CardRetryTransactionType) TestContext.getInstance().getDatabase().find(CardRetryTransactionType.class).get(0);
            }
        };
    } 
}
