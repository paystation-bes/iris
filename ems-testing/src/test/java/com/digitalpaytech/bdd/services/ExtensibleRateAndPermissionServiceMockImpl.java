package com.digitalpaytech.bdd.services;

import java.util.Calendar;
import java.util.Date;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.domain.ExtensibleRate;
import com.digitalpaytech.domain.ExtensibleRateType;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.dto.SmsAlertInfo;
import com.digitalpaytech.dto.SmsRateInfo;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

public final class ExtensibleRateAndPermissionServiceMockImpl {
    
    private static final int RATE_END_HOUR = 23;
    private static final int RATE_END_MIN = 59;
    private static final int RATE_AMOUNT = 229;
    private static final int SERVICE_FEE_AMT = 25;
    private static final int MIN_EXTENSION_MINUTES = 25;
    
    private ExtensibleRateAndPermissionServiceMockImpl() {
    }
    
    public static Answer<SmsRateInfo> getSmsRateInfo() {
        return new Answer<SmsRateInfo>() {
            @Override
            public SmsRateInfo answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                final SmsAlertInfo smsAlertInfo = (SmsAlertInfo) args[0];
                final SmsRateInfo smsRateInfo = new SmsRateInfo();
                final String localTimeZone = smsAlertInfo.getTimeZone();
                smsRateInfo.setExpiryDateGMT(smsAlertInfo.getExpiryDate());
                
                final int maxAllowedTimeBasedOnPermission = 1440;
                
                smsRateInfo.setMaxAllowedTimeByPermission(maxAllowedTimeBasedOnPermission);
                
                // step3:  check rates
                final ExtensibleRate rate1 = getExtensibleRate(smsAlertInfo, localTimeZone);
                smsRateInfo.setIsSecondPermissionNoParking(true);
                
                final Date expiryDateLocal = DateUtil.changeTimeZone(smsAlertInfo.getExpiryDate(), localTimeZone);
                smsRateInfo.setMaxExtensionTimeForFreeParking(timeDiffInMinutes(expiryDateLocal, rate1.getEndDateLocal()));
                
                smsRateInfo.setEmsRate1(rate1);
                smsRateInfo.setEmsRate2(null);
                return smsRateInfo;
            }
            
            private ExtensibleRate getExtensibleRate(final SmsAlertInfo smsAlertInfo, final String localTimeZone) {
                final ExtensibleRateType etr = new ExtensibleRateType();
                etr.setId(WebCoreConstants.EXTENSIBLE_RATE_TYPE_HOURLY_RATE);
                final Location loc = new Location();
                loc.setId(smsAlertInfo.getLocationId());
                loc.setName("Newport");
                final ExtensibleRate extensibleRate = new ExtensibleRate(loc, etr, "Daily", 0, 0, RATE_END_HOUR, RATE_END_MIN, RATE_AMOUNT,
                        SERVICE_FEE_AMT, MIN_EXTENSION_MINUTES, true, new Date(), new Date(), 1);
                
                final Date expiryDateLocal = DateUtil.changeTimeZone(smsAlertInfo.getExpiryDate(), localTimeZone);
                setStartEndDatesLocal(extensibleRate, expiryDateLocal, localTimeZone);
                
                return extensibleRate;
            }
            
            private void setStartEndDatesLocal(final ExtensibleRate currentEMSRate, final Date expiryDateLocal, final String localTimeZone) {
                final Calendar cal = Calendar.getInstance();
                cal.setTime(expiryDateLocal);
                final Calendar cal1 = Calendar.getInstance();
                cal1.setTime(expiryDateLocal);
                cal1.set(Calendar.HOUR_OF_DAY, currentEMSRate.getBeginHourLocal());
                cal1.set(Calendar.MINUTE, currentEMSRate.getBeginMinuteLocal());
                cal1.set(Calendar.SECOND, 0);
                final Date rateStartDateLocal = cal1.getTime();
                
                cal1.set(Calendar.HOUR_OF_DAY, currentEMSRate.getEndHourLocal());
                cal1.set(Calendar.MINUTE, currentEMSRate.getEndMinuteLocal());
                cal1.set(Calendar.SECOND, 0);
                if (currentEMSRate.getEndHourLocal() == 0) {
                    cal.add(Calendar.DAY_OF_MONTH, 1);
                }
                final Date rateEndDateLocal = cal.getTime();
                final Date rateEndDateGMT = DateUtil.getDatetimeInGMT(rateEndDateLocal, localTimeZone);
                
                currentEMSRate.setStartDateLocal(rateStartDateLocal);
                currentEMSRate.setEndDateLocal(rateEndDateLocal);
                currentEMSRate.setEndDateGMT(rateEndDateGMT);
            }
            
            private int timeDiffInMinutes(final Date dateStart, final Date dateEnd) {
                final Calendar cal = Calendar.getInstance();
                cal.setTime(dateStart);
                cal.set(Calendar.SECOND, 0);
                final Date newDateStart = cal.getTime();
                cal.setTime(dateEnd);
                cal.set(Calendar.SECOND, 0);
                final Date newDateEnd = cal.getTime();
                
                final int diff = (int) (newDateEnd.getTime() - newDateStart.getTime()) / StandardConstants.MINUTES_IN_MILLIS_1;
                return diff;
            }
            
        };
    }
    
}
