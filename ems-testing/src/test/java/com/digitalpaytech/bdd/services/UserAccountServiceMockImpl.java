package com.digitalpaytech.bdd.services;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.domain.UserAccount;

public class UserAccountServiceMockImpl {
    public static Answer<UserAccount> findUserAccount() {
        return new Answer<UserAccount> () {

            @Override
            public UserAccount answer(InvocationOnMock invocation) throws Throwable {
                final int userAccountId = (int) invocation.getArguments()[0];
                return (UserAccount) TestDBMap.findObjectByIdFromMemory(userAccountId, UserAccount.class);
            }
            
        };
    }
    
}
