package com.digitalpaytech.bdd.services;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public final class CommonControllerHelperMockImpl {
    public CommonControllerHelperMockImpl() {
    }
    
    public static Answer<Integer> resolveCustomerId() {
        return new Answer<Integer>() {
            @Override
            public Integer answer(final InvocationOnMock invocation) throws Throwable {
                return new Integer(1);
            }
        };
    }
}
