package com.digitalpaytech.bdd.services;

import java.util.HashMap;
import java.util.Map;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.DBHelper;
import com.digitalpaytech.bdd.util.TestConstants;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.dto.SmsAlertInfo;

public final class SmsPermitAlertJobServiceMockImpl {
    
    private SmsPermitAlertJobServiceMockImpl() {
    }
    
    public static Answer<SmsAlertInfo> getSmsAlertByParkerReply() {
        return new Answer<SmsAlertInfo>() {
            @Override
            public SmsAlertInfo answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                String mobileNumber = invocation.getArgumentAt(0, String.class);
                
                SmsAlertInfo result = null;
                if (mobileNumber != null) {
                    if (mobileNumber.startsWith(TestConstants.MOBILE_NUMBER_PLUS_SIGN)) {
                        mobileNumber = mobileNumber.substring(TestConstants.MOBILE_NUMBER_PLUS_SIGN.length());
                    }
                    
                    result = DBHelper.findOne(SmsAlertInfo.class, SmsAlertInfo.ALIAS_MOBILE_NUMBER, mobileNumber);
                }
                
                return result;
            }
        };
    }
    
    public static Answer<Integer> confirmExtensiblePermit() {
        return new Answer<Integer>() {
            @Override
            public Integer answer(final InvocationOnMock invocation) throws Throwable {
                final Map<String, Object> expectedResult = new HashMap<String, Object>(1);
                final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
                final Object[] args = invocation.getArguments();
                expectedResult.put(TestConstants.SMS_MESSAGE_TYPE, args[2]);
                wrapperObject.setExpectedResult(expectedResult);
                return (Integer) args[2];
            }
        };
    }
}
