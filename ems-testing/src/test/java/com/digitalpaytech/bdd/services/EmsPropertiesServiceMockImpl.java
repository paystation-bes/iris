package com.digitalpaytech.bdd.services;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.service.EmsPropertiesService;

public final class EmsPropertiesServiceMockImpl {
    
    private EmsPropertiesServiceMockImpl() {
    }
    
    public static Answer<Boolean> getPropertyValueAsBoolean() {
        return new Answer<Boolean>() {
            @Override
            public Boolean answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                if (EmsPropertiesService.SMS_SIMULATED_ENV.equals((String) args[0])) {
                    return EmsPropertiesService.SMS_SIMULATED_ENV_DEFAULT;
                }
                return false;
            }
        };
    }
    
    public static Answer<Integer> getPropertyValueAsInt() {
        return new Answer<Integer>() {
            @Override
            public Integer answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                if (EmsPropertiesService.DPT_CUSTOMER_ID.equals((String) args[0])) {
                    return EmsPropertiesService.DEFAULT_DPT_CUSTOMER_ID;
                }
                return 0;
            }
        };
    }
    
}
