package com.digitalpaytech.bdd.services;

import java.util.List;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.domain.ThirdPartyServiceAccount;


public final class ThirdPartyServiceAccountServiceMockImpl {
    
    private ThirdPartyServiceAccountServiceMockImpl() {
    }
    
    public static Answer<ThirdPartyServiceAccount> getThirdPartyServiceAccountByCustomerAndType() {
        return new Answer<ThirdPartyServiceAccount>() {
            @Override
            public ThirdPartyServiceAccount answer(final InvocationOnMock invocation) throws Throwable {
                final List<ThirdPartyServiceAccount> apList = TestContext.getInstance().getDatabase().find(ThirdPartyServiceAccount.class);
                ThirdPartyServiceAccount thirdPartyServiceAccount = null;
                if (!apList.isEmpty()) {
                    thirdPartyServiceAccount = apList.get(0);
                }                
                return thirdPartyServiceAccount;
            }
        };
    }
    
}
