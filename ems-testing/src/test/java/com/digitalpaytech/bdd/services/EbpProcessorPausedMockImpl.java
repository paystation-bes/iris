package com.digitalpaytech.bdd.services;

import java.util.ArrayList;
import java.util.List;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantPOS;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.domain.ProcessorTransactionType;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.dto.ProcessorInfo;

public class EbpProcessorPausedMockImpl {
    private EbpProcessorPausedMockImpl() {
    }
    
    public static Answer<List<MerchantPOS>> getMerchantPOSsEcho() {
        return new Answer<List<MerchantPOS>>() {
            @Override
            public List<MerchantPOS> answer(final InvocationOnMock invocation) throws Throwable {
                return getMerchantPOSs();
            }
        };
    }
    
    public static Answer<ProcessorTransactionType> getProcessorTransactionTypeEcho(final Integer processorTransactionTypeId) {
        return new Answer<ProcessorTransactionType>() {
            @Override
            public ProcessorTransactionType answer(final InvocationOnMock invocation) throws Throwable {
                return new ProcessorTransactionType(processorTransactionTypeId);
            }
        };
    }
    
    public static Answer<List<MerchantPOS>> findMerchPOSByPOSIdNoDeletedEcho() {
        return new Answer<List<MerchantPOS>>() {
            @Override
            public List<MerchantPOS> answer(final InvocationOnMock invocation) throws Throwable {
                return getMerchantPOSs();                
            }
        };
    }
    
    public static Answer<MerchantAccount> findByPointOfSaleIdAndCardTypeIdEcho() {
        return new Answer<MerchantAccount>() {
            @Override
            public MerchantAccount answer(final InvocationOnMock invocation) throws Throwable {
                final MerchantAccount ma = new MerchantAccount();
                ma.setId(1);
                return ma;                
            }
        };
    }

    public static Answer<ProcessorInfo> getEMVProcessorInfo() {
        return new Answer<ProcessorInfo>() {
            @Override
            public ProcessorInfo answer(final InvocationOnMock invocation) throws Throwable {
                return null;                
            }
        };
    }    
   
    
    private static List<MerchantPOS> getMerchantPOSs() {
        final Processor pr = new Processor();
        final MerchantAccount ma = new MerchantAccount();
        ma.setId(1);
        ma.setProcessor(pr);
        final CardType ct = new CardType();
        ct.setId(1);
        ct.setName("Credit Card");
        final MerchantPOS mpos = new MerchantPOS();
        mpos.setMerchantAccount(ma);
        mpos.setCardType(ct);
        mpos.setId(1);
        final List<MerchantPOS> list = new ArrayList<MerchantPOS>(1);
        list.add(mpos);
        return list;
    }
}
