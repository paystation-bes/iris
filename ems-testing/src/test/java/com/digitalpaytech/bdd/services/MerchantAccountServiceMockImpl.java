package com.digitalpaytech.bdd.services;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.domain.MerchantAccount;

public class MerchantAccountServiceMockImpl {
    
    public static Answer<MerchantAccount> findByPointOfSaleIdAndCardTypeId() {
        return new Answer<MerchantAccount>() {
            @Override
            public MerchantAccount answer(final InvocationOnMock invocation) throws Throwable {                
                return (MerchantAccount) TestContext.getInstance().getDatabase().find(MerchantAccount.class).get(0);
            }
        };
    }
    
}
