package com.digitalpaytech.testing.services.impl;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.ThirdPartyServiceStallSequence;
import com.digitalpaytech.service.ThirdPartyServiceStallSequenceService;

@Service("thirdPartyServiceStallSequenceServiceTest")
public class ThirdPartyServiceStallSequenceServiceTestImpl implements ThirdPartyServiceStallSequenceService {
    
    @Override
    public final long findSequenceNumber(final Location location, final int spaceNumber) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final ThirdPartyServiceStallSequence insertThirdPartyServiceStallSequence(
        final ThirdPartyServiceStallSequence thirdPartyServiceStallSequence) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void updateThirdPartyServiceStallSequence(final ThirdPartyServiceStallSequence thirdPartyServiceStallSequence) {
        // TODO Auto-generated method stub
        
    }
}
