package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.AlertClassType;
import com.digitalpaytech.domain.CardProcessMethodType;
import com.digitalpaytech.domain.CitationType;
import com.digitalpaytech.domain.CollectionType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.RevenueType;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.Tab;
import com.digitalpaytech.domain.TransactionType;
import com.digitalpaytech.domain.UnifiedRate;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetListType;
import com.digitalpaytech.domain.WidgetSubsetMember;
import com.digitalpaytech.domain.WidgetTierType;
import com.digitalpaytech.dto.TabsGroup;
import com.digitalpaytech.service.DashboardService;

@Service("dashboardServiceTest")
@SuppressWarnings("checkstyle:designforextension")
public class DashboardServiceTestImpl implements DashboardService {
    
    @Override
    public Collection<Tab> findTabsByUserAccountId(final int userAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Tab findTabByIdEager(final int id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Collection<Tab> findDefaultTabs() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Collection<Tab> findDefaultTabs(final boolean parentFlag) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Widget findWidgetById(final int widgetId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Widget findDefaultWidgetByIdAndEvict(final int widgetId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void deleteTabsByUserId(final int userId) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void delete(final Object object) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void saveOrUpdateTab(final Tab id, final UserAccount userAccount) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void saveTabs(final TabsGroup tabsGroup, final UserAccount userAccount) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateUserAccount(final UserAccount userAccount, final UserAccount mergedUserAccount) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public List<Location> findLocationsByCustomerId(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<Customer> findAllChildCustomers(final int parentCustomerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<Route> findRoutesByCustomerId(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public WidgetListType getWidgetListTypeById(final int widgetListTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<Widget> findWidgetMasterList(final UserAccount userAccount) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<UnifiedRate> findUnifiedRatesByCustomerId(final int customerId, final int pastMonths) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<WidgetSubsetMember> findSubsetMemberByWidgetIdTierTypeId(final Integer id, final int widgetTierType) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<Widget> findMapWidgetsByUserAccountId(final int userAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public WidgetTierType findWidgetTierTypeByTierTypeId(final int tierTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public UnifiedRate findUnifiedRateById(final int rateId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public RevenueType findRevenueTypeById(final byte revenueTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Route findRouteById(final int routeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public TransactionType findTransactionTypeById(final int transactionTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Location findLocationById(final int locationId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Customer findCustomerById(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public CollectionType findCollectionTypeById(final int collectionTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public MerchantAccount findMerchantAccountById(final int merchantAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public AlertClassType findAlertClassTypeById(final byte alertClassTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public CardProcessMethodType findCardProcessMethodTypeById(final int cardProcessMethodTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<CardProcessMethodType> findSettledCardProcessMethodTypes() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public <T> T findObjectByClassTypeAndId(final Class<T> clazz, final int id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CitationType findCitationTypeById(int citationTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
}
