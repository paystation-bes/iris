package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.AlertClassType;
import com.digitalpaytech.domain.AlertThresholdType;
import com.digitalpaytech.domain.AlertType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.service.AlertTypeService;

@Service("alertTypeServiceTest")
@SuppressWarnings("checkstyle:designforextension")
public class AlertTypeServiceTestImpl implements AlertTypeService {
    
    @Override
    public Collection<AlertType> findAlertTypeByIsUserDefined(final boolean isUserDefined) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Collection<AlertClassType> findAllAlertClassTypes() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public EventSeverityType findEventSeverityTypeById(final int eventSeverityTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<AlertType> findAllAlertTypes() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public AlertThresholdType findAlertThresholdTypeById(final int alertThresholdTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<String> findAlertThresholdTypeText(final Collection<Integer> ids) {
        // TODO Auto-generated method stub
        return null;
    }
}
