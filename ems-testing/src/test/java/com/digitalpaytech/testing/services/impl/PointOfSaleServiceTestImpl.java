package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.MerchantPOS;
import com.digitalpaytech.domain.ModemSetting;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PaystationType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosBalance;
import com.digitalpaytech.domain.PosDate;
import com.digitalpaytech.domain.PosDateType;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.domain.PosSensorState;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.PosStatus;
import com.digitalpaytech.dto.MapEntry;
import com.digitalpaytech.dto.PayStationSelectorDTO;
import com.digitalpaytech.dto.PointOfSaleAlertInfo;
import com.digitalpaytech.dto.PointOfSaleSearchCriteria;
import com.digitalpaytech.dto.customeradmin.PaystationListInfo;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.dto.mobile.rest.CollectPaystationSummaryDTO;
import com.digitalpaytech.service.PointOfSaleService;

@Service("pointOfSaleServiceTest")
@SuppressWarnings("checkstyle:designforextension")
public class PointOfSaleServiceTestImpl implements PointOfSaleService {
    
    @Override
    public List<PointOfSale> findPointOfSalesByRouteId(final int routeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSale> findPointOfSalesByLocationId(final int locationId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSale> findPointOfSalesByLocationIdOrderByName(final int locationId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSale> findPointOfSalesByCustomerId(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSale> findPointOfSalesByCustomerId(final int customerId, final boolean all) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSale> findValidPointOfSaleByCustomerIdOrderByName(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSale> findPointOfSaleAndPosStatusByCustomerIdNoCache(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public PointOfSale findPointOfSaleBySerialNumber(final String serialNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSale> findPointOfSaleBySerialNumberNoConditions(final String serialNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public PointOfSale findPointOfSaleByPaystationId(final int payStationId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Collection<PointOfSale> findAllPointOfSalesOrderByName() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public PointOfSale findPointOfSaleById(final Integer id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<Paystation> findPayStationsByLocationId(final int locationId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<Paystation> findPayStationsByCustomerId(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Paystation findPayStationById(final int payStationId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Paystation findPayStationByPointOfSaleId(final int pointOfSaleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public PosStatus findPointOfSaleStatusByPOSId(final int pointOfSaleId, final boolean useCache) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public PaystationType findPayStationTypeByPointOfSaleId(final int pointOfSaleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public PaystationType findPayStationTypeById(final int payStationTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public PosHeartbeat findPosHeartbeatByPointOfSaleId(final int pointOfSaleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public PosServiceState findPosServiceStateByPointOfSaleId(final int pointOfSaleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public PosSensorState findPosSensorStateByPointOfSaleId(final int pointOfSaleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<MerchantPOS> findMerchantPOSbyPointOfSaleId(final int pointOfSaleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<MerchantPOS> findMerchPOSByPOSIdNoDeleted(final int pointOfSaleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public PosBalance findPOSBalanceByPointOfSaleId(final int pointOfSaleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public PosBalance findPOSBalanceByPointOfSaleIdNoCache(final int pointOfSaleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Date findLastCollectionDateByPointOfSaleId(final int pointOfSaleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public ModemSetting findModemSettingByPointOfSaleid(final int pointOfSaleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void saveOrUpdatePointOfSaleHeartbeat(final PosHeartbeat posHeartbeat) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void saveOrUpdatePOSDate(final PosDate posDate) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public PosDateType findPosDateTypeById(final int posDateTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSale> findPointOfSalesBySettingsFileId(final int settingsFileId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSale> findPointOfSalesBySerialNumberAndCustomerId(final int customerId, final List<String> serialNumberList) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Collection<PointOfSale> findAllPointOfSalesBySerialNumberKeyword(final String keyword) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSale> findAllPointOfSalesBySerialNumberKeyword(final String keyword, final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Collection<PointOfSale> findAllPointOfSalesByPosNameKeyword(final String keyword) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSale> findAllPointOfSalesByPosNameKeyword(final String keyword, final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSale> findVirtualPointOfSalesByCustomerId(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSale> findNoneVirtualPointOfSalesByCustomerId(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void update(final PointOfSale pointOfSale) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public List<PointOfSale> findCurrentPointOfSalesBySettingsFileIdForPayStationSettingsPage(final Integer settingsFileId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSale> findFuturePointOfSalesBySettingsFileIdForPayStationSettingsPage(final Integer settingsFileId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSale> findUnAssignedPointOfSalesForPayStationSettingsPage(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public PlacementMapBordersInfo findMapBordersByCustomerId(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSale> findByCustomerIdAndLocationName(final Integer customerId, final String locationName) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSale> findPointOfSaleAndPosStatusByCustomerId(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSale> findPointOfSaleAndPosStatusByCustomerIdOrderByName(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public int countUnPlacedByCustomerIds(final Collection<Integer> customerIds) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public int countUnPlacedByLocationIds(final Collection<Integer> locationIds) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public int countUnPlacedByRouteIds(final Collection<Integer> routeIds) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public List<MapEntry> findMapEntriesByCustomerIds(final Collection<Integer> customerIds) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<MapEntry> findMapEntriesByLocationIds(final Collection<Integer> locationIds) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<MapEntry> findMapEntriesByRouteIds(final Collection<Integer> routeIds) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Map<Integer, MapEntry> findPointOfSaleMapEntryDetails(final Collection<Integer> pointOfSaleIds, final TimeZone timeZone) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Map<Integer, PointOfSale> findPosHashByCustomerIds(final Collection<Integer> customerIds) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Map<Integer, PointOfSale> findPosHashByLocationIds(final Collection<Integer> locationIds) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Map<Integer, PointOfSale> findPosHashByRouteIds(final Collection<Integer> routeIds) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Map<Integer, PointOfSale> findPosHashByPosIds(final Collection<Integer> posIds) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<Integer> findPosIdsByCustomerId(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<Integer> findAllIds(final List<Integer> customerId, final List<Integer> locationIds, final List<Integer> routeIds) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSale> findAllPointOfSales(final List<Integer> customerId, final List<Integer> locationIds, final List<Integer> routeIds,
        final List<Integer> pointOfSaleIds) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public PointOfSale findPointOfSaleBySerialNumberAndCustomerId(final String serialNumber, final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public boolean isPointOfSalesReadyForMigration(final Customer customer, final Set<String> versionSet, final boolean isForMigration) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public List<PointOfSale> findPointOfSaleNotReadyForMigration(final Integer customerId, final Set<String> versionSet) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PayStationSelectorDTO> findPayStationSelector(final PointOfSaleSearchCriteria criteria) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PaystationListInfo> findPaystation(final PointOfSaleSearchCriteria criteria) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public int findPayStationPage(final Integer posId, final PointOfSaleSearchCriteria criteria) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public void updatePosServicState(final PosServiceState posServiceState) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updatePosSensorState(final PosSensorState posSensorState) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updatePaystationActivityTime(final PointOfSale pos, final boolean isUpdated) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public List<PointOfSale> getDetachedPointOfSaleByLowerCaseNames(final int customerId, final Collection<String> names,
        final String locPosNameSeparator) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public long countPointOfSalesRequiringCollections(final Integer routeId, final int severity) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public List<PointOfSaleAlertInfo> findActivePointOfSalesWithCollectionAlertsInfo(final Integer customerId, final Integer routeId,
        final Date date, final Integer severity, final boolean onlyActive, final boolean onlyInactive) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<CollectPaystationSummaryDTO> findValidAndActivatedPointOfSalesByCustomerIdAndRouteType(final int customerId, final int routeTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSaleAlertInfo> findActivePointOfSalesWithRecentHeartbeatOrCollection(final Integer customerId, final Integer routeId,
        final Date sinceDate) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public int findPayStationCountByCustomerId(final Integer customerId) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public PointOfSale findTxablePointOfSaleBySerialNumber(final String serialNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSale> findPointOfSalesBySerialNumberKeywordWithLimit(final String keyword, final int customerId) {
        return null;
    }
    
    @Override
    public List<PointOfSale> findPointOfSalesByPosNameKeywordWithLimit(final String keyword, final int customerId) {
        return null;
    }
    
    @Override
    public PointOfSale findPointOfSaleAndPosStatusBySerialNumber(final String serialNumber) {
        return null;
    }

    @Override
    public List<PointOfSale> findLinuxPointOfSalesByCustomerId(Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
	
	@Override
	public List<PointOfSale> findAllPointOfSalesByPosNameForCustomer(String name, Integer customerId) {
		return null;
	}
}
