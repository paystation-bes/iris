package com.digitalpaytech.testing.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.ActivePermit;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.service.ActivePermitService;

@Service("activePermitServiceTest")
public class ActivePermitServiceTestImpl implements ActivePermitService {
    
    @Override
    public final ActivePermit getActivePermitByCustomerAndLicencePlate(final int customerId, final String plateNumberNumber, final int querySpaceBy) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ActivePermit getActivePermitByLocationAndLicencePlate(final int locationId, final String plateNumberNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ActivePermit> getValidByLocationId(final Integer locationId, final Date reportDate, final int startPosition,
        final int recordsReturned) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int getValidByLocationIdTotalCount(final Integer locationId, final Date reportDate) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final List<ActivePermit> getValidByCustomerId(final Integer customerId, final Date reportDate, final int startPosition,
        final int recordsReturned, final int querySpaceBy) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int getValidByCustomerIdTotalCount(final Integer customerId, final Date reportDate, final int querySpaceBy) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final List<ActivePermit> getPaystationSettingsFromLast14Days(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ActivePermit> getActivePermitByCustomerAndSpaceRange(final int customerId, final int startSpace, final int endSpace,
        final int querySpaceBy) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ActivePermit> getActivePermitByLocationAndSpaceRange(final int customerId, final int locationId, final int startSpace,
        final int endSpace) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ActivePermit> getActivePermitByPaystationSettingAndSpaceRange(final int customerId, final String paystationSettingName,
        final int startSpace, final int endSpace, final int querySpaceBy) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ActivePermit> getActivePermitByPosListAndSpaceRange(final int customerId, final List<PointOfSale> posList,
        final int startSpace, final int endSpace, final int querySpaceBy) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ActivePermit getActivePermitByCustomerAndAddTimeNumber(final int customerId, final int addTimeNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ActivePermit getActivePermitByCustomerAndAddTimeNumberOrderByLatestExpiry(final int customerId, final int addTimeNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ActivePermit getActivePermitByLocationAndAddTimeNumber(final int customerId, final int locationId, final int addTimeNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ActivePermit getActivePermitByLocationAndAddTimeNumberOrderByLatestExpiry(final int customerId, final int locationId,
        final int addTimeNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ActivePermit getActivePermitByCustomerAndSpaceNumber(final int customerId, final int spaceNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ActivePermit getActivePermitByCustomerAndSpaceNumberOrderByLatestExpiry(final int customerId, final int spaceNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ActivePermit getActivePermitByLocationAndSpaceNumber(final int customerId, final int locationId, final int spaceNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ActivePermit getActivePermitByLocationAndSpaceNumberOrderByLatestExpiry(final int customerId, final int locationId,
        final int spaceNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ActivePermit getActivePermitByPaystationSettingAndAddTimeNumber(final int customerId, final String paystationSettingName,
        final int addTimeNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ActivePermit getActivePermitByPaystationSettingAndSpaceNumber(final int customerId, final String paystationSettingName,
        final int spaceNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ActivePermit findActivePermitByOriginalPermitId(final Long originalPermitId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ActivePermit findActivePermitByLocationIdAndLicencePlateNumber(final Integer permitLocationId, final String licencePlateNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ActivePermit findActivePermitByLocationIdAndLicencePlateNumberOrderByLatestExpiry(final Integer permitLocationId,
        final String licencePlateNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ActivePermit findActivePermitByCustomerIdAndLicencePlateNumber(final Integer customerId, final String licencePlateNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ActivePermit findActivePermitByCustomerIdAndLicencePlateNumberOrderByLatestExpiry(final Integer customerId,
        final String licencePlateNumber) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void deleteActivePermit(ActivePermit activePermit) {
        // TODO Auto-generated method stub
        
    }
}
