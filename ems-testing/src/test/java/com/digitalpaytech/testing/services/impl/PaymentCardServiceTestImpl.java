package com.digitalpaytech.testing.services.impl;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.service.PaymentCardService;

@Service("paymentCardServiceTest")
public class PaymentCardServiceTestImpl implements PaymentCardService {
    
    @Override
    public final PaymentCard findPaymentCardByPurchaseIdAndProcessorTransactionId(final Long purchaseId, final Long processorTransactionId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final PaymentCard findByPurchaseIdAndCardType(final ProcessorTransaction processorTransaction, final Integer cardTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final PaymentCard findByPurchaseIdAndCardType(final ProcessorTransaction processorTransaction, final Integer cardTypeId,
        final boolean cacheable) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final PaymentCard findByPurchaseId(final Long purchaseId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final PaymentCard findLatestByCustomerCardId(final Integer customerCardId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void updateProcessorTransactionToBatchedSettled(final ProcessorTransaction processorTransaction, final Integer cardTypeId) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateProcessorTransactionTypeToSettled(final Long purchaseId, final Long processorTransactionId) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void savePaymentCard(final PaymentCard paymentCard) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void saveRefundPaymentCard(ProcessorTransaction refund) {
        // TODO Auto-generated method stub
        
    }
}
