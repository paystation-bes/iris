package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.digitalpaytech.client.dto.merchant.CardReaderConfig;
import com.digitalpaytech.client.util.CommunicationException;
import com.digitalpaytech.data.InfoAndMerchantAccount;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.GatewayProcessor;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantPOS;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.exception.DuplicateMerchantAccountException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.util.RandomKeyMapping;

@Service("merchantAccountServiceTest")
@SuppressWarnings("checkstyle:designforextension")
public class MerchantAccountServiceTestImpl implements MerchantAccountService {
    
    @Override
    public List<MerchantAccount> findMerchantAccounts(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<MerchantAccount> findMerchantAccounts(final int customerId, final boolean forValueCard) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<MerchantAccount> findValidMerchantAccountsByCustomerId(final Integer id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<MerchantAccount> findAllMerchantAccountsByCustomerId(final Integer id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<MerchantAccount> findRefundableMerchantAccounts(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public MerchantAccount findByProcessorAndFields(final Map<String, String> criteriaMap) throws DuplicateMerchantAccountException {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public MerchantAccount findActiveByProcessorAndFields(final Map<String, String> criteriaMap) throws DuplicateMerchantAccountException {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public MerchantAccount findByProcessorAndFieldsAndCustomer(final Map<String, String> criteriaMap) throws DuplicateMerchantAccountException {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<MerchantAccount> findByProcessorAndFields(final String[] params, final Object[] values) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void saveMerchantAccount(final MerchantAccount merchantAccount) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateMerchantAccount(final MerchantAccount merchantAccount) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public MerchantAccount findByPointOfSaleIdAndCardTypeId(final Integer pointOfSaleId, final Integer cardTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public MerchantAccount findByPOSAndCardTypeAndNotDeletedAndMerchantStatusEnabled(final Integer pointOfSaleId, final Integer cardTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public MerchantAccount findById(final int merchantAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void deleteMerchantAccount(final MerchantAccount merchantAccount) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public long updateNewReferenceNumber(final MerchantAccount merchantAccount) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public InfoAndMerchantAccount preFillMerchantAccount(final MerchantAccount merchantAccount) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public MerchantAccount findByDeletedProcessorFieldsAndMaxReferenceCounter(final MerchantAccount merchantAccount) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Collection<MerchantAccount> getValidMerchantAccountsForProcessor(final Integer customerId, final Integer processorId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Collection<MerchantPOS> findMerchantPosesByMerchantAccountIdAndCardTypeId(final Integer merchantAccountId, final int cardTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Collection<MerchantPOS> findMerchantPosesByMerchantAccountId(final Integer merchantAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<MerchantAccount> findAllMerchantAccountsForReportsByCustomerId(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<MerchantAccount> findMerchAcctsIncludeDeleted(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<FilterDTO> getMerchantAccountFiltersByCustomerId(final List<FilterDTO> result, final int customerId,
        final RandomKeyMapping keyMapping) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<FilterDTO> getRefundableMerchantAccountFiltersByCustomerId(final List<FilterDTO> result, final int customerId,
        final RandomKeyMapping keyMapping) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<String> findCustomerNamesByProcessorId(final int processorId) {
        return null;
    }
    
    @Override
    public List<MerchantAccount> findUnusedMerchantAccounts(final int customerId, final boolean forValueCard) {
        return null;
    }
    
    @Override
    public List<CustomerProperty> findTimeZoneByProcessorId(final Integer processorId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public MerchantAccount findFromMechantPOSByPointOfSaleAndProcId(final Integer pointOfSaleId, final Integer processorId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public MerchantAccount findByMerchantPOSPointOfSaleIdAndCardTypeId(final Integer pointOfSaleId, final Integer cardTypdId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public MerchantAccount findWithProcessorById(final Integer merchantAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<GatewayProcessor> findGatewayProcessors() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public MerchantAccount findByDeletedMerchantPOSPointOfSaleIdAndCardTypeId(final Integer pointOfSaleId, final Integer cardTypdId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public MerchantPOS findMerchantPosesByMerchantAccountIdAndCardTypeIdAndPOSId(final int pointOfSaleId, final int cardTypeId, final int posId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public MerchantPOS findMerchantPOSByMerchantAccountTerminalTokenAndSerialNumber(final String terminalToken, final String serialnumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void saveOrUpdateMerchantAccount(final MerchantAccount merchantAccount) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public boolean hasLinkMerchantAccount(final int customerId) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public MerchantAccount findByTerminalTokenAndIsLink(final String terminalToken) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public boolean hasIrisMerchantAccount(Integer customerId) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public MerchantAccount migrateMerchantAccount(Integer customerId, MerchantAccount merchantAccount) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public MerchantAccount findByTerminalTokenAndIsLink(final String terminalToken, final boolean isLink) {
        // TODO Auto-generated method stub
        return null;
    }
  
    @Override
    public CardReaderConfig getCardReaderConfig(String terminalToken) throws InvalidDataException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<MerchantAccount> populateMigrationInfo(List<MerchantAccount> merchantAccounts) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public String testTransaction(MerchantAccount merchantAccount) {
        return null;
    }
    
    @Override
    public final Optional<MerchantAccount> populateMerchantTerminalInformation(final MerchantAccount merchantAccount, final boolean forTest) {
        return null;
    }

    @Override
    public MerchantAccount findWithProcessorById(final Integer merchantAccountId, final boolean forTest) throws CommunicationException {
        return null;
    }
}
