package com.digitalpaytech.testing.services.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAgreement;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.CustomerURLReroute;
import com.digitalpaytech.dto.customeradmin.LocationTree;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.util.RandomKeyMapping;

@Service("customerServiceTest")
@SuppressWarnings("checkstyle:designforextension")
public class CustomerServiceTestImpl implements CustomerService {
    
    @Override
    public List<Customer> findCustomerBySubscriptionTypeId(final Integer subscriptionTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Customer findCustomer(final Integer id) {
        
        return null;
    }
    
    @Override
    public Customer findCustomerDetached(final Integer id) {
        return null;
    }
    
    @Override
    public Customer findCustomerNoCache(final Integer id) {
        
        return null;
    }
    
    @Override
    public Serializable saveCustomerAgreement(final CustomerAgreement customerAgreement) {
        
        return null;
    }
    
    @Override
    public Collection<Customer> findAllParentAndChildCustomers() {
        
        return null;
    }
    
    @Override
    public Collection<Customer> findAllParentCustomers() {
        
        return null;
    }
    
    @Override
    public Collection<Customer> findAllChildCustomers() {
        
        return null;
    }
    
    @Override
    public Collection<Customer> findAllParentAndChildCustomersBySearchKeyword(final String keyword, final Integer... customerTypeIds) {
        
        return null;
    }
    
    @Override
    public boolean update(final Customer customer) {
        
        return false;
    }
    
    @Override
    public LocationTree getCustomerTree(final int customerId, final RandomKeyMapping keyMapping) {
        
        return null;
    }
    
    @Override
    public List<Customer> findAllChildCustomers(final int parentCustomerId) {
        
        return null;
    }
    
    @Override
    public LocationTree getChildCustomerTree(final RandomKeyMapping keyMapping) {
        
        return null;
    }
    
    @Override
    public CustomerURLReroute findURLRerouteByCustomerId(final Integer customerId) {
        
        return null;
    }
    
    @Override
    public List<CustomerSubscription> findCustomerSubscriptions(final Integer customerId) {
        
        return null;
    }

    @Override
    public Customer findCustomerByUnifiId(Integer unifiId) {
        // TODO Auto-generated method stub
        return null;
    }
}
