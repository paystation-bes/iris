package com.digitalpaytech.testing.services.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.RestSessionToken;
import com.digitalpaytech.service.RestSessionTokenService;

@Service("restSessionTokenServiceTest")
public class RestSessionTokenServiceTestImpl implements RestSessionTokenService {
    
    @Override
    public final List<RestSessionToken> findRestSessionTokenBySessionToken(final String sessionToken) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final RestSessionToken findRestSessionTokenByRestAccountId(final int restAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int findCountBySessionToken(final String sessionToken) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public void saveRestSessionToken(final RestSessionToken restSessionToken) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateRestSessionToken(final RestSessionToken restSessionToken) {
        // TODO Auto-generated method stub
        
    }
}
