package com.digitalpaytech.testing.services.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CustomerMigrationValidationStatus;
import com.digitalpaytech.domain.CustomerMigrationValidationStatusType;
import com.digitalpaytech.service.CustomerMigrationValidationStatusService;

@Service("customerMigrationValidationStatusServiceTest")
public class CustomerMigrationValidationStatusServiceTestImpl implements CustomerMigrationValidationStatusService {
    
    @Override
    public final List<CustomerMigrationValidationStatus> findSystemWideValidations() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<CustomerMigrationValidationStatus> findUserFacingValidationsByCustomerId(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<CustomerMigrationValidationStatus> findInMigrationValidationsByCustomerId(final Integer customerId,
        final boolean isDuringMigration) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerMigrationValidationStatusType runValidations(final Integer customerId, final boolean includeSystemWide,
        final boolean isDuringMigration, final boolean beforeStart) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
