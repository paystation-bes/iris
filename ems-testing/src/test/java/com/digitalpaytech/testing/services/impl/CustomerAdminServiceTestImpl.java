package com.digitalpaytech.testing.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerPropertyType;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosStatus;
import com.digitalpaytech.domain.TimezoneVId;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.customeradmin.GlobalConfiguration;
import com.digitalpaytech.dto.customeradmin.SystemNotification;
import com.digitalpaytech.service.CardTypeService;
import com.digitalpaytech.service.CustomerAdminService;

@Service("customerAdminServiceTest")
@SuppressWarnings("checkstyle:designforextension")
public class CustomerAdminServiceTestImpl implements CustomerAdminService {
    
    @Override
    public GlobalConfiguration findGlobalPreferences(final Customer customer, final GlobalConfiguration globalConfiguration) {
        
        return null;
    }
    
    @Override
    public GlobalConfiguration findGlobalPreferencesTimeZoneOnly(final Customer customer, final GlobalConfiguration globalConfiguration) {
        
        return null;
    }
    
    @Override
    public SystemNotification findSystemNotifications() {
        
        return null;
    }
    
    @Override
    public List<CustomerProperty> getCustomerPropertyByCustomerId(final int customerId) {
        
        return null;
    }
    
    @Override
    public CustomerProperty getCustomerPropertyByCustomerIdAndCustomerPropertyType(final int customerId, final int customerPropertyType) {
        
        return null;
    }
    
    @Override
    public List<CustomerPropertyType> getCustomerPropertyTypes() {
        
        return null;
    }
    
    @Override
    public Set<CustomerProperty> findCustomerPropertyByCustomerId(final int customerId) {
        
        return null;
    }
    
    @Override
    public List<Customer> findAllChildCustomers(final int parentCustomerId) {
        
        return null;
    }
    
    @Override
    @SuppressWarnings("checkstyle:magicnumber")
    public List<TimezoneVId> getTimeZones() {
        final List<TimezoneVId> timeZoneList = new ArrayList<TimezoneVId>();
        timeZoneList.add(new TimezoneVId(360, "Canada/Atlantic"));
        timeZoneList.add(new TimezoneVId(361, "Canada/Central"));
        timeZoneList.add(new TimezoneVId(362, "Canada/East-Saskatchewan"));
        timeZoneList.add(new TimezoneVId(363, "Canada/Eastern"));
        timeZoneList.add(new TimezoneVId(364, "Canada/Mountain"));
        timeZoneList.add(new TimezoneVId(365, "Canada/Newfoundland"));
        timeZoneList.add(new TimezoneVId(366, "Canada/Pacific"));
        timeZoneList.add(new TimezoneVId(367, "Canada/Saskatchewan"));
        timeZoneList.add(new TimezoneVId(368, "Canada/Yukon"));
        timeZoneList.add(new TimezoneVId(473, "GMT"));
        timeZoneList.add(new TimezoneVId(571, "US/Alaska"));
        timeZoneList.add(new TimezoneVId(572, "US/Aleutian"));
        timeZoneList.add(new TimezoneVId(573, "US/Arizona"));
        timeZoneList.add(new TimezoneVId(574, "US/Central"));
        timeZoneList.add(new TimezoneVId(575, "US/East-Indiana"));
        timeZoneList.add(new TimezoneVId(576, "US/Eastern"));
        timeZoneList.add(new TimezoneVId(577, "US/Hawaii"));
        timeZoneList.add(new TimezoneVId(578, "US/Indiana-Starke"));
        timeZoneList.add(new TimezoneVId(579, "US/Michigan"));
        timeZoneList.add(new TimezoneVId(580, "US/Mountain"));
        timeZoneList.add(new TimezoneVId(581, "US/Pacific"));
        return timeZoneList;
    }
    
    @Override
    public List<FilterDTO> getQuerySpacesByOptions() {
        
        return null;
    }
    
    @Override
    public List<CardType> getCardTypes() {
        
        return null;
    }
    
    @Override
    public List<UserAccount> getUserAccountsByCustomerId(final int customerId) {
        
        return null;
    }
    
    @Override
    public Customer findCustomerByCustomerId(final int customerId) {
        
        return null;
    }
    
    @Override
    public void saveOrUpdateCustomerProperty(final CustomerProperty customerProperty) {
        
    }
    
    @Override
    public void saveOrUpdatePointOfSale(final PointOfSale pointOfSale) {
        
    }
    
    @Override
    public void saveOrUpdatePaystation(final Paystation payStation) {
        
    }
    
    @Override
    public void saveOrUpdateCustomerCardType(final CustomerCardType customerCardType) {
        
    }
    
    @Override
    public void saveOrUpdatePointOfSaleStatus(final PosStatus pointOfSaleStatus) {
        
    }
    
    @Override
    public boolean hasActiveCustomerCard(final CustomerCardType customerCardType) {
        
        return false;
    }
    
    @Override
    public CustomerCardType findCustomerCardTypeById(final int customerCardTypeId) {
        
        return null;
    }
    
    @Override
    public List<UserAccount> getUserAccountsWithAliasUsersByCustomerId(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void setCardTypeService(final CardTypeService cardTypeService) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void setCardTypes(List<CardType> cardTypes) {
        // TODO Auto-generated method stub
        
    }
}
