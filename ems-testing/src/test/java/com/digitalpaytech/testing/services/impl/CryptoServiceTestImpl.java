package com.digitalpaytech.testing.services.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.digitalpaytech.client.dto.KeyPackage;
import com.digitalpaytech.client.dto.RSAKeyInfo;
import com.digitalpaytech.domain.CryptoKey;
import com.digitalpaytech.dto.HashAlgorithm;
import com.digitalpaytech.dto.crypto.HashAlgorithmData;
import com.digitalpaytech.dto.systemadmin.CryptoKeyListInfo;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.util.KeyValuePair;

@Service("cryptoServiceTest")
public class CryptoServiceTestImpl implements CryptoService {
    
    @Override
    public final String encryptData(final int purpose, final String data) throws CryptoException {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final String decryptData(final String data) throws CryptoException {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final String decryptData(final String data, final int pointOfSaleId, final Date purchasedDate) throws CryptoException {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final boolean isNewKeyForData(final String data) throws CryptoException {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void checkAndProcessExternalCryptoKey(final Map<Integer, HashAlgorithmData> hashAlgorithmDataMap) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final void generateExternalCryptoKey(final String comment, final Map<Integer, HashAlgorithmData> hashAlgorithmDataMap)
        throws CryptoException {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void checkAndRemoveDeprecatedExternalCryptoKey() {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final boolean checkAndProcessInternalCryptoKey(final Map<Integer, HashAlgorithmData> hashAlgorithmDataMap) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void generateInternalCryptoKey(final String comment, final Map<Integer, HashAlgorithmData> hashAlgorithmDataMap) throws CryptoException {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void checkAndRemoveInternalCryptoKey() {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final boolean isKeyStoreUpToDate() {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public final String createKeyInfo(final int cryptoTypeId, final int cryptoIndex) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int countSpareKey(final CryptoKey key) throws CryptoException {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final int getNearestKeyIndex(final CryptoKey maxKey) throws CryptoException {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public void checkAndUpdateSigningKeyStore(final List<String> activeSignAlgorithms) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final CryptoKeyListInfo listActiveKeys(final int purpose) throws CryptoException {
        // TODO Auto-generated method stub
        return null;
    }
    
    public final String encryptData(final String data) throws CryptoException {
        // TODO Auto-generated method stub
        return null;
    }
    
    public final KeyValuePair<Integer, Integer> splitKeyInfo(final String keyName) {
        // TODO Auto-generated method stub
        return null;
    }
    
    public final RSAKeyInfo retrieveKeyInfo(final String keyAlias) throws CryptoException {
        // TODO Auto-generated method stub
        return null;
    }
    
    public final KeyPackage retrieveKey(final String keyAlias) throws CryptoException {
        // TODO Auto-generated method stub
        return null;
    }
    
    public final KeyPackage retrieveKey(final String keyAlias, final HashAlgorithm hashAlg) throws CryptoException {
        // TODO Auto-generated method stub
        return null;
    }
    
    public final String currentExternalKey() throws CryptoException {
        // TODO Auto-generated method stub
        return null;
    }
    
    public final String currentInternalKey() throws CryptoException {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final String generateDevicePassword(final String serialNumber) {
        return null;
    }
    
    @Override
    public boolean isLinkCryptoKey(final String encryptedData) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String currentExternalKey(boolean isCryptoLink, String serialNumber) throws CryptoException {
        // TODO Auto-generated method stub
        return null;
    }
}
