package com.digitalpaytech.testing.services.impl;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.ReportHistory;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.dto.AdhocReportInfo;
import com.digitalpaytech.dto.ReportSearchCriteria;
import com.digitalpaytech.dto.customeradmin.ReportQueueInfo.QueueInfo;
import com.digitalpaytech.service.ReportHistoryService;
import com.digitalpaytech.util.RandomKeyMapping;

@Service("reportHistoryServiceTest")
public class ReportHistoryServiceTestImpl implements ReportHistoryService {
    
    @Override
    public void saveReportHistory(final ReportHistory reportHistory, final ReportQueue reportQueue, final byte[] fileContent, final String fileName,
        final boolean isWithEmail) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateReportHistory(final ReportHistory reportHistory) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final List<ReportHistory> findFailedAndCancelledReportsByUserAccountId(final int userAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ReportHistory> findFailedAndCancelledReportsByCustomerId(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ReportHistory> findFinishedReportsByUserAccountId(final int userAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ReportHistory getReportHistory(final int id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void deleteReportHistory(final int id) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final int getCompletedReports() {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final int getCancelledReports() {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final int getFailedReports() {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final float getReportResponseTime() {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final ReportHistory findRecentlyExecutedReport(final int reportDefinitionId, final Date minExecutionEndGmt) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ReportHistory findRecentlyExecutedReport(final AdhocReportInfo adhocReportInfo) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public final List<ReportHistory> findFailedAndCancelledReportsByCustomerIdAndUserAccountId(final int userAccountId, final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public final List<QueueInfo> findFailedReports(final ReportSearchCriteria criteria, final RandomKeyMapping keyMapping, final TimeZone timeZone) {
        // TODO Auto-generated method stub
        return null;
    }
}
