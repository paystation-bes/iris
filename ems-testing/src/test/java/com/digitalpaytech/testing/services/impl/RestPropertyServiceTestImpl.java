package com.digitalpaytech.testing.services.impl;

import org.springframework.stereotype.Service;

import com.digitalpaytech.service.RestPropertyService;

@Service("restPropertyServiceTest")
public class RestPropertyServiceTestImpl implements RestPropertyService {
    
    @Override
    public final String getPropertyValue(final String propertyName) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final String getPropertyValue(final String propertyName, final String defaultValue) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final String getPropertyValue(final String propertyName, final String defaultValue, final boolean forceGet) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final String getPropertyValue(final String propertyName, final boolean forceGet) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int getPropertyValueAsInt(final String propertyName, final int defaultValue) throws Exception {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final int getPropertyValueAsInt(final String propertyName, final int defaultValue, final boolean forceGet) throws Exception {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final boolean getPropertyValueAsBoolean(final String propertyName, final boolean defaultValue) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public final boolean getPropertyValueAsBoolean(final String propertyName, final boolean defaultValue, final boolean forceGet) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void findPropertiesFile() {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void loadPropertiesFile() {
        // TODO Auto-generated method stub
        
    }
}
