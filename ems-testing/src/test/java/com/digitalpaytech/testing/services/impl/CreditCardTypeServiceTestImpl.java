package com.digitalpaytech.testing.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CreditCardType;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.service.CreditCardTypeService;
import com.digitalpaytech.util.RandomKeyMapping;

@Service("creditCardTypeServiceTest")
@SuppressWarnings("checkstyle:designforextension")
public class CreditCardTypeServiceTestImpl implements CreditCardTypeService {
    
    @Override
    public List<CreditCardType> loadAll() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Map<Integer, CreditCardType> getCreditCardTypesMap() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Map<String, CreditCardType> getCreditCardTypeLowerNamesMap() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public final CreditCardType getCreditCardTypeByName(final String name, final boolean defaultCreditCard) {
        return null;
    }
    
    @Override
    public CreditCardType getCreditCardTypeByName(final String name) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public String getText(final int creditCardId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<FilterDTO> getFilters(final List<FilterDTO> result, final RandomKeyMapping keyMapping, final boolean showOnlyValid) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public CreditCardType getCreditCardTypeById(final Integer id) {
        return null;
    }
}
