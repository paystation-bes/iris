package com.digitalpaytech.testing.services.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.WebServiceEndPoint;
import com.digitalpaytech.service.WebServiceEndPointService;

@Service("webServiceEndPointServiceTest")
@SuppressWarnings("checkstyle:designforextension")
public class WebServiceEndPointServiceTestImpl implements WebServiceEndPointService {
    
    @Override
    public List<WebServiceEndPoint> findWebServiceEndPointsByCustomer(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<WebServiceEndPoint> findWebServiceEndPointByCustomerAndType(final int customerId, final byte typeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public WebServiceEndPoint findWebServiceEndPointByToken(final String token) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public WebServiceEndPoint findWebServiceEndById(final int webServiceEndPointId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void saveOrUpdate(final WebServiceEndPoint webServiceEndPoint) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void delete(final WebServiceEndPoint webServiceEndPoint) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public List<WebServiceEndPoint> findPrivateWebServiceEndPointsByCustomer(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
}
