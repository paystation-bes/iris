package com.digitalpaytech.testing.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.ActivePermit;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.service.PermitService;

@Service("permitServiceTest")
public class PermitServiceTestImpl implements PermitService {
    
    @Override
    public final Permit findLatestExpiredPermitByPurchaseId(final Long purchaseId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ActivePermit> getStallStatusValidForPurchaseExpiryStallBySpecifiedPosDate(final List<PointOfSale> posList,
        final int startSpace, final int endSpace, final Date specifiedPosDate, final int stallType) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ActivePermit> getStallStatusByLocationBySpecifiedPosDate(final int locationId, final int startSpace, final int endSpace,
        final Date specifiedPosDate) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ActivePermit> getActivePermitByCustomerAndSpaceRange(final int customerId, final int startSpace, final int endSpace,
        final Date specifiedPosDate) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ActivePermit> getActivePermitByLocationAndSpaceRange(final int locationId, final int startSpace, final int endSpace,
        final Date specifiedPosDate) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ActivePermit> getActivePermitByPaystationSettingAndSpaceRange(final String paystationSettingName, final int startSpace,
        final int endSpace, final Date specifiedPosDate) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ActivePermit> getActivePermitByPosListAndSpaceRange(final List<PointOfSale> posList, final int startSpace, final int endSpace,
        final Date specifiedPosDate) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Permit findPermitByIdForSms(final Long id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Permit findPermitByPurchaseId(final Long purchaseId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<Permit> findPermitByPermitNumber(final int permitNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void saveExtendedPermit(final Permit originalPermit, final Permit permit) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final Permit findLatestOriginalPermitByPointOfSaleIdAndPermitNumber(final int pointOfSaleId, final int permitNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<Permit> findExtentionsByOriginalPermitId(final Long originalPermitId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Permit findLatestOriginalPermit(final int locationId, final int spaceNumber, final int addTimeNumber, final String licencePlateNumber) {
        // TODO Auto-generated method stub
        return null;
    }
}
