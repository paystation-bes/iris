package com.digitalpaytech.testing.services.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.RateProfileLocation;
import com.digitalpaytech.service.RateProfileLocationService;

@Service("rateProfileLocationServiceTest")
public class RateProfileLocationServiceTestImpl implements RateProfileLocationService {

    @Override
    public final List<RateProfileLocation> findByRateProfileId(final Integer rateProfileId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public final List<RateProfileLocation> findPublishedByLocationId(final Integer locationId, final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public final RateProfileLocation findById(final Integer rateProfileLocationId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public final int saveAll(final List<RateProfileLocation> locationsList, final Integer customerId) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public final int deleteByRateProfileId(final Integer rateProfileId) {
        // TODO Auto-generated method stub
        return 0;
    }
}
