package com.digitalpaytech.testing.services.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CustomerMigrationEMS6ModifiedRecord;
import com.digitalpaytech.service.CustomerMigrationEMS6ModifiedRecordService;

/**
 * This service class defines service logic to migrate customer data
 * from old EMS to new EMS.
 * 
 * @author Brian Kim
 * 
 */
@Service("customerMigrationEMS6ModifiedRecordServiceTest")
public class CustomerMigrationEMS6ModifiedRecordServiceTestImpl implements CustomerMigrationEMS6ModifiedRecordService {
    
    @Override
    public void saveCustomerMigrationEMS6ModifiedRecordService(final CustomerMigrationEMS6ModifiedRecord customerMigrationEMS6ModifiedRecord) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateCustomerMigrationEMS6ModifiedRecordService(final CustomerMigrationEMS6ModifiedRecord customerMigrationEMS6ModifiedRecord) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void deleteCustomerMigrationEMS6ModifiedRecordService(final CustomerMigrationEMS6ModifiedRecord customerMigrationEMS6ModifiedRecord) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final List<CustomerMigrationEMS6ModifiedRecord> findModifiedRecordsByCustomerId(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<CustomerMigrationEMS6ModifiedRecord> findModifiedRecordsByCustomerIdAndTableType(final Integer customerId, final String tableName) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerMigrationEMS6ModifiedRecord findModifiedRecordIfExists(final Integer customerId, final Integer tableId, final String tableName,
        final String columnName) {
        // TODO Auto-generated method stub
        return null;
    }
}
