package com.digitalpaytech.testing.services.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.MobileSessionToken;
import com.digitalpaytech.dto.MobileTokenMapEntry;
import com.digitalpaytech.service.MobileSessionTokenService;

@Service("mobileSessionTokenServiceTest")
public class MobileSessionTokenServiceTestImpl implements MobileSessionTokenService {
    
    @Override
    public final MobileSessionToken findMobileSessionTokenBySessionToken(final String sessionToken) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final long getTokenCount(final String sessionToken) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final List<MobileSessionToken> findMobileSessionTokenByUserAccountId(final int userAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void saveMobileSessionToken(final MobileSessionToken mobileSessionToken) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateMobileSessionToken(final MobileSessionToken mobileSesionToken) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void delete(final MobileSessionToken mobileSessionToken) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final List<MobileSessionToken> findMobileSessionTokenByUserAccountIdAndApplicationId(final int userAccountId, final int applicationId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final MobileSessionToken findMobileSessionTokenByUserAccountAndMobileLicense(final int userAccountId, final int mobileLicenseId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<MobileSessionToken> findMobileSessionTokensByMobileLicense(final Integer mobileLicenseKeyId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<MobileSessionToken> findMobileSessionTokensByMobileLicenses(final List<Integer> mobileLicenseKeyIds) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void deleteMobileSessionTokensByMobileLicenses(final List<Integer> mobileLicenseKeyIds) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final MobileSessionToken findMobileSessionTokenById(final int id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final MobileSessionToken findMobileSessionTokenByIdJoin(final int id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<MobileTokenMapEntry> findMobileSessionTokensMap(final int customerId, final int mobileApplicationId, final Date expiryDate,
        final String timeZone, final Map<Integer, String> statusNames) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<MobileSessionToken> findProvisionedMobileSessionTokenByCustomerAndApplicationByPage(final int customerId,
        final int applicationId, final int pageNo, final Date maxUpdatedTime) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<MobileTokenMapEntry> findRecentUsersForTokens(final List<MobileSessionToken> mobileSessionTokens, final String timeZone,
        final Map<Integer, String> statusNames) {
        // TODO Auto-generated method stub
        return null;
    }
}
