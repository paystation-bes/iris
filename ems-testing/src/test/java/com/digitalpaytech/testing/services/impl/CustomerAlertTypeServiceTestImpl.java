package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CustomerAlertEmail;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.service.CustomerAlertTypeService;

@Service("customerAlertTypeServiceTest")
@SuppressWarnings("checkstyle:designforextension")
public class CustomerAlertTypeServiceTestImpl implements CustomerAlertTypeService {
    
    @Override
    public Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdOrderByNameAsc(final Integer customerId, final Integer filterType) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdOrderByNameDesc(final Integer customerId, final Integer filterType) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdOrderByAlertTypeAsc(final Integer customerId, final Integer filterType) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdOrderByAlertTypeDesc(final Integer customerId, final Integer filterType) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public CustomerAlertType findCustomerAlertTypeById(final Integer id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public CustomerAlertType findCustomerAlertTypeByIdAndEvict(final Integer id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void saveCustomerAlertType(final CustomerAlertType customerAlertType) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateCustomerAlertType(final CustomerAlertType customerAlertType, final Collection<String> emailAddresses) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void deleteCustomerAlertType(final CustomerAlertType customerAlertType, final Integer userAccountId) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public int countByRouteId(final Integer... routeIds) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public void disableNonDefaultAlert(final Integer customerId, final Date date, final Integer userId) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdAlertThresholdTypeIdAndIsActive(final Integer customerId,
        final Integer alertThresholdTypeId, final boolean isActive) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public CustomerAlertType findDefaultCustomerAlertTypeByCustomerIdAndAlertThresholdTypeId(final Integer customerId, final int alertThresholdTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    public List<CustomerAlertEmail> findCustomerAlertEmailByCustomerAlertTypeId(final Integer customerAlertTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<CustomerAlertType> findCustomerAlertTypesByRouteId(final Integer routeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    //    @Override
    //    public List<CustomerAlertType> findCustomerAlertTypesByRouteIdAndPointOfSaleId(Integer routeId, Integer posId) {
    //        // TODO Auto-generated method stub
    //        return null;
    //    }
    
    @Override
    public List<Integer> findCustomerAlertTypeIdsByRouteId(final Integer routeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<CustomerAlertType> findCustomerAlertTypeByIdList(List<Integer> customerAlertTypeIdList) {
        // TODO Auto-generated method stub
        return null;
    }
}
