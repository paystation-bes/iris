package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.service.ReversalService;

@Service("reversalServiceTest")
public class ReversalServiceTestImpl implements ReversalService {
    
    @Override
    public final int findTotalReversalsByCardData(final String cardNumber) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final Collection<Reversal> findReversalsByCardData(final String cardNumber, final boolean isEvict) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void updateReversal(final Reversal reversal) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void archiveReversalData() {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final Collection<Reversal> getIncompleteReversalsForReversal(final Date processedBefore) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void saveReversal(final Reversal reversal) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Collection<Reversal> getIncompleteReversalsForReversal(final MerchantAccount merchantAccount) {
        // TODO Auto-generated method stub
        return null;
    }
}
