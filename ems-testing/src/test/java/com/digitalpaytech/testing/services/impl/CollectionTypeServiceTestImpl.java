package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CollectionType;
import com.digitalpaytech.service.CollectionTypeService;

@Service("collectionTypeServiceTest")
@SuppressWarnings("checkstyle:designforextension")
public class CollectionTypeServiceTestImpl implements CollectionTypeService {
    
    @Override
    public Collection<CollectionType> findAllCollectionTypes() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public CollectionType findCollectionType(final String name) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public CollectionType findCollectionType(final int id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public int setLock(final int clusterId, final int numberOfPaystionToBeExecuted) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public List<Integer> calculatePaystationBalance(final int collectionLockId, final int timeInterval, final int preAuthDelayTime) {
        // TODO Auto-generated method stub
        return null;
        
    }
    
    @Override
    public void processCollectionFlagUnsettledPreAuth(final int preAuthDelayTime) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void processCollectionReleaseLock(final int clusterId) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void processDeleteCollectionLock() {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public int getPOSBalanceRecalcableCount() {
        // TODO Auto-generated method stub
        return 0;
    }
}
