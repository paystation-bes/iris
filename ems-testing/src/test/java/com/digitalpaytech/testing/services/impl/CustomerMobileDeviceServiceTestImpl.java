package com.digitalpaytech.testing.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CustomerMobileDevice;
import com.digitalpaytech.dto.customeradmin.MobileDeviceInfo;
import com.digitalpaytech.service.CustomerMobileDeviceService;

@Service("customerMobileDeviceServiceTest")
public class CustomerMobileDeviceServiceTestImpl implements CustomerMobileDeviceService {
    
    @Override
    public final List<CustomerMobileDevice> findBlockedMobileDevicesByCustomer(final Integer customerId, final Integer pageNumber, final Integer pageSize) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerMobileDevice findCustomerMobileDeviceById(final Integer customerMobileDeviceId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerMobileDevice findBlockedMobileDevice(final Integer customerId, final Integer mobileDeviceId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void unBlockDevice(final Integer customerId, final Integer customerMobileDeviceId) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void blockDevice(final Integer customerId, final Integer customerMobileDeviceId, final int userAccountId) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void save(final CustomerMobileDevice customerMobileDevice) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void delete(final CustomerMobileDevice customerMobileDevice) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void saveOrUpdate(final CustomerMobileDevice customerMobileDevice) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final CustomerMobileDevice findCustomerMobileDeviceByCustomerAndDeviceUid(final Integer customerId, final String uid) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void releaseDevice(final Integer customerId, final Integer customerMobileDeviceId, final int userAccountId) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void releaseDevice(final Integer customerId, final Integer customerMobileDeviceId, final Integer applicationType, final int userAccountId) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final List<CustomerMobileDevice> findCustomerMobileDeviceByCustomerAndApplication(final Integer customerId, final Integer applicationId,
        final Integer page, final Integer pageSize) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<CustomerMobileDevice> findCustomerMobileDeviceByCustomer(final Integer customerId, final Integer page, final Integer pageSize) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final MobileDeviceInfo findDetailsForCustomerMobileDevice(final Integer customerMobileDeviceId, final Integer mobileApplicationTypeId,
        final Map<Integer, String> statusNames) {
        // TODO Auto-generated method stub
        return null;
    }
}
