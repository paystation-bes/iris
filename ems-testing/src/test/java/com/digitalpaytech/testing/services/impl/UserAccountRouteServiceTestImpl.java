package com.digitalpaytech.testing.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.UserAccountRoute;
import com.digitalpaytech.service.UserAccountRouteService;

@Service("userAccountRouteServiceTest")
public class UserAccountRouteServiceTestImpl implements UserAccountRouteService {
    
    @Override
    public final long getUserCountForRoute(final int routeId) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final long getUserCountForRouteAppIdAndCurrentSession(final int routeId, final int mobileApplicationTypeId, final Date currentDate) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public void saveOrUpdate(final UserAccountRoute userAccountRoute) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void delete(final UserAccountRoute userAccountRoute) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final List<UserAccountRoute> findUserAccountRouteByUserAccountId(final int userAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<UserAccountRoute> findUserAccountRouteByRouteId(final int routeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<UserAccountRoute> findUserAccountRouteByUserAccountIdAndRouteId(final int userAccountId, final int routeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final UserAccountRoute findUserAccountRouteByUserAccountIdAndApplicationId(final int userAccountId, final int applicationTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
}
