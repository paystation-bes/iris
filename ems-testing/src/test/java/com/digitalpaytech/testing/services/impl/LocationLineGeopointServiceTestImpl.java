package com.digitalpaytech.testing.services.impl;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.LocationLineGeopoint;
import com.digitalpaytech.dto.GeoLocationEntry;
import com.digitalpaytech.service.LocationLineGeopointService;

@Service("locationLineGeopointServiceTest")
@SuppressWarnings({ "checkstyle:designforextension" })
public class LocationLineGeopointServiceTestImpl implements LocationLineGeopointService {
    
    @Override
    public LocationLineGeopoint getLocationLineGeopointById(final long geopointId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public LocationLineGeopoint getLocationLineGeopointByLocationId(final int locationId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void deleteLocationLineGeopoint(final LocationLineGeopoint line) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void saveOrUpdateLocationLineGeopoint(final LocationLineGeopoint line) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void findOccupancyForGeoPoint(final GeoLocationEntry geoLocationEntry, final Integer locationId) {
        // TODO Auto-generated method stub
        
    }
    
}
