package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.ExtensiblePermit;
import com.digitalpaytech.dto.SmsAlertInfo;
import com.digitalpaytech.service.ExtensiblePermitService;

@Service("extensiblePermitServiceTest")
public class ExtensiblePermitServiceTestImpl implements ExtensiblePermitService {
    
    @Override
    public final ExtensiblePermit findByMobileNumber(final String mobileNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void updateLatestExpiryDate(final Date permitExpireGmt, final String cardData, final short last4digit, final String mobileNumber) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final int deleteExpiredSmsAlert() {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final int deleteExpiredEMSExtensiblePermit() {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final Collection<SmsAlertInfo> loadSmsAlertFromDB(final int countOfRows) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void deleteExtensiblePermitAndSMSAlert(final String mobileNumber) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final SmsAlertInfo getSmsAlertByParkerReply(final String mobileNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void updateSmsAlert(final boolean isAlerted, final int numOfRetry, final boolean isLocked, final SmsAlertInfo smsAlertInfo) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateSmsAlertAndLatestExpiryDate(final boolean isAlerted, final int numOfRetry, final boolean isLocked, final Date permitBeginGmt,
        final Date permitExpireGmt, final boolean isFreeParkingExtended, final SmsAlertInfo smsAlertInfo) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final int getExtensiblePermitByCount() {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public void updateSmsAlert(final boolean isAlerted, final int numOfRetry, final boolean isLocked, final SmsAlertInfo smsAlertInfo,
        final String callbackId) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateSmsAlert(final boolean isAlerted, final int numOfRetry, final boolean isLocked, final SmsAlertInfo smsAlertInfo,
        final String callbackId, final boolean isFreeParking) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateExtensiblePermit(final ExtensiblePermit extensiblePermit) {
        // TODO Auto-generated method stub
        
    }
}
