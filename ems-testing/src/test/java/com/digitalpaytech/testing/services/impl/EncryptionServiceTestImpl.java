package com.digitalpaytech.testing.services.impl;

import org.springframework.stereotype.Service;

import com.digitalpaytech.dto.EncryptionInfo;
import com.digitalpaytech.service.EncryptionService;

@Service("encryptionServiceTest")
public class EncryptionServiceTestImpl implements EncryptionService {
    
    @Override
    public final boolean isForce2LicensePage() {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public final EncryptionInfo getEncryptionInfo() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void initEncryptionMode() {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final String createHMACSecretKey() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final boolean verifyHMACSignature(final String hostName, final String httpMethod, final String passInSignature,
        final Object queryParamList, final String xmlString, final String encodedKey) {
        // TODO Auto-generated method stub
        return false;
    }
}
