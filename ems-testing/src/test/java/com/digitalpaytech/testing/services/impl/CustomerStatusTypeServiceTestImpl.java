package com.digitalpaytech.testing.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CustomerStatusType;
import com.digitalpaytech.service.CustomerStatusTypeService;

@Service("customerStatusTypeServiceTest")
public class CustomerStatusTypeServiceTestImpl implements CustomerStatusTypeService {
    
    @Override
    public final List<CustomerStatusType> loadAll() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Map<Integer, CustomerStatusType> getCustomerStatusTypesMap() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final String getText(final int customerStatusTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerStatusType findCustomerStatusType(final int customerStatusTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerStatusType findCustomerStatusType(final String name) {
        // TODO Auto-generated method stub
        return null;
    }
}
