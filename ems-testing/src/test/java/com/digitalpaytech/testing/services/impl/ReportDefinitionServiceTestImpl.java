package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportEmail;
import com.digitalpaytech.domain.ReportLocationValue;
import com.digitalpaytech.dto.AdhocReportInfo;
import com.digitalpaytech.service.ReportDefinitionService;

@Service("reportDefinitionServiceTest")
public class ReportDefinitionServiceTestImpl implements ReportDefinitionService {
    
    @Override
    public final boolean findReadyReportsWithLock() {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void updateReportDefinition(final ReportDefinition reportDefinition) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final ReportDefinition getReportDefinition(final int id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ReportDefinition> findScheduledReportsByUserAccountId(final int userAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ReportDefinition> findScheduledReportsByCustomerId(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void saveOrUpdateReportDefinition(final ReportDefinition reportDefinition, final ReportDefinition originalReportDefinition,
        final Collection<ReportLocationValue> reportLocationValueList, final Set<ReportEmail> emailList) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void saveOrUpdateReportDefinitionForOnline(final ReportDefinition reportDefinition, final ReportDefinition originalReportDefinition,
        final Collection<ReportLocationValue> reportLocationValueList, final Set<ReportEmail> emailList, final String timeZone) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final List<String> findEmailsByReportDefinitionId(final int reportDefinitionId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int getReadyReportCount() {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final AdhocReportInfo runAdhocReport(final ReportDefinition report, final String timeZone) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ReportDefinition getAdhocReportDefinition(final ReportDefinition reportDefinition) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ReportDefinition getReportDefinitionFromHistory(final int reportHistoryId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<ReportDefinition> findScheduledReportsByCustomerIdAndUserAccountId(int userAccountId, int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
}
