package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.data.RolePermissionInfo;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.service.PermissionService;

@Service("permissionServiceTest")
public class PermissionServiceTestImpl implements PermissionService {
    
    @Override
    public final Collection<RolePermissionInfo> findAllPermissionsByRoleId(final Integer roleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Collection<RolePermissionInfo> findAllPermissionsByCustomerTypeId(final Integer customerTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Collection<Permission> findRolePermissionsByRoleId(final Integer roleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<Permission> findAllPermissions() {
        // TODO Auto-generated method stub
        return null;
    }
}
