package com.digitalpaytech.testing.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.HashAlgorithmType;
import com.digitalpaytech.dto.crypto.HashAlgorithmData;
import com.digitalpaytech.exception.SystemException;
import com.digitalpaytech.service.HashAlgorithmTypeService;

@Service("hashAlgorithmTypeServiceTest")
public class HashAlgorithmTypeServiceTestImpl implements HashAlgorithmTypeService {
    
    @Override
    public final HashAlgorithmType findHashAlgorithmType(final Integer id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<HashAlgorithmType> loadAll() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Map<Integer, HashAlgorithmData> findHashAlgorithmNotSigningMap() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Map<Integer, HashAlgorithmData> findForInternalKeyRotation() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final HashAlgorithmData findForMessageDigest(final Integer hashAlgorithmTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final void reloadHashAlgorithmNotSigningMaps() throws SystemException {
        // TODO Auto-generated method stub
    }
    
    @Override
    public final void loadHashAlgorithmNotSigningMaps() throws SystemException {
        // TODO Auto-generated method stub
    }
    
    @Override
    public final List<String> findHashAlgorithmNames(final boolean isForSigning) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<String> findActiveHashAlgorithmNames(final boolean isForSigning) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<String> findActiveSigningHashAlgorithmNames() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final String findAlgorithmName(final int hashAlgorithmTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
}
