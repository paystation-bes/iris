package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.service.UserAccountService;

@Service("userAccountServiceTest")
public class UserAccountServiceTestImpl implements UserAccountService {
    
    @Override
    public final UserAccount findUserAccount(final int userAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final UserAccount findUserAccountAndEvict(final int userAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<Permission> findPermissions(final int userAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<Role> findRoles(final int userAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final UserAccount findUserAccount(final String userName) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void updatePassword(final UserAccount userAccount) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final Collection<UserAccount> findUserAccountsByCustomerIdAndUserAccountTypeId(final Integer customerId) {
        return null;
    }
    
    @Override
    public final Collection<UserAccount> findUserAccountsByCustomerIdAndControllerCustomerIdAndUserAccountType(final Integer customerId,
        final Integer controllerCustomerId) {
        return null;
    }
    
    @Override
    public void updateUserAccount(final UserAccount userAccount) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateUserAccountAndUserRoles(final UserAccount userAccount) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void saveUserAccountAndUserRole(final UserAccount userAccount) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final Collection<UserAccount> findUserAccountByCustomerIdAndRoleId(final Integer customerId, final Integer roleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void saveOrUpdateUserAccount(final UserAccount userAccount) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final UserAccount findAdminUserAccountByChildCustomerId(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final UserAccount findAdminUserAccountByParentCustomerId(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Collection<UserAccount> findUserAccountsByCustomerId(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<RolePermission> findUserRoleAndPermission(final Integer userAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<RolePermission> findUserRoleAndPermissionForCustomerType(final Integer userAccountId, final Integer customerTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final UserAccount findUndeletedUserAccount(final String userName) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final UserAccount findUserAccountForLogin(final int userAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void deleteUserRoles(final UserAccount userAccount) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final List<UserAccount> findUserAccountsByCustomerIdAndMobileApplicationTypeId(final int customerId, final int mobileApplicationTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<UserAccount> findChildUserAccountsByParentUserId(final Integer parentUserAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<UserAccount> findAdminUserAccountsForParent(final Integer customerId, final Integer parentCustomerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<UserAccount> findAdminUserAccountWithIsAllChilds(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Collection<UserAccount> findAliasUserAccountsByUserAccountId(final Integer userAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final UserAccount findAliasUserAccountByUserAccountIdAndCustomerId(final Integer userAccountId, final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final UserAccount findUserRoleByUserAccountIdAndRoleId(final Integer roleId, final Integer userAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<UserRole> findActiveUserRoleByUserAccountId(final Integer userAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<UserAccount> findUserAccountByRoleIdAndCustomerIdList(final Integer roleId, final List<Integer> customerIdList) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<UserAccount> findUserAccountByRoleIdAndCustomerId(final Integer roleId, final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Customer findChildCustomer(final Integer parentCustomerId, final String childCustomerName) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public UserAccount findUserAccountByLinkUserName(final String linkUserName) {
        return null;
    }
}
