package com.digitalpaytech.testing.services.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.Notification;
import com.digitalpaytech.service.NotificationService;

@Component("notificationServiceTest")
public class NotificationServiceTestImpl implements NotificationService {
    
    @Override
    public final List<Notification> findNotificationByEffectiveDate() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<Notification> findNotificationAllCurrentAndFuture() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<Notification> findNotificationInPast() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Notification findNotificationById(final Integer id, final boolean doEvict) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void saveNotification(final Notification notification) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateNotification(final Notification notification) {
        // TODO Auto-generated method stub
        
    }
}
