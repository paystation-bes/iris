package com.digitalpaytech.testing.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CustomerMigrationFailureType;
import com.digitalpaytech.service.CustomerMigrationFailureTypeService;

@Service("customerMigrationFailureTypeServiceTest")
public class CustomerMigrationFailureTypeServiceTestImpl implements CustomerMigrationFailureTypeService {
    
    @Override
    public final List<CustomerMigrationFailureType> loadAll() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Map<Byte, CustomerMigrationFailureType> getCustomerMigrationFailureTypesMap() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final String getText(final byte customerMigrationFailureTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerMigrationFailureType findCustomerMigrationFailureType(final byte customerMigrationFailureTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerMigrationFailureType findCustomerMigrationFailureType(final String name) {
        // TODO Auto-generated method stub
        return null;
    }
}
