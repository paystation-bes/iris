package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.BatchProcessStatus;
import com.digitalpaytech.dto.customeradmin.CouponSearchCriteria;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.service.BatchProcessingException;
import com.digitalpaytech.service.CouponService;

@Service("couponServiceTest")
public class CouponServiceTestImpl implements CouponService {
    
    @Override
    public final Coupon findCouponByCustomerIdAndCouponCode(final int customerId, final String couponCode) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Coupon findCouponWithShortestCode(final int customerId, final String couponCode) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Coupon findDeletedCouponWithShortestCode(final int customerId, final String couponCode, final Date minArchiveDate) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Coupon findByCustomerIdCouponCodeNoRestriction(final int customerId, final String couponCode) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Coupon findDeletedByCustomerIdCouponCodeNoRestriction(final int customerId, final String couponCode, final Date minArchiveDate) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<Coupon> findCouponsByCustomerId(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Coupon findCouponById(final int id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<Coupon> findCouponByCriteria(final CouponSearchCriteria criteria) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int findRecordPage(final Integer couponId, final CouponSearchCriteria criteria) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public void saveOrUpdateCoupon(final Coupon coupon) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void saveCoupon(final Coupon coupon) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void saveOrUpdateCoupons(final List<Coupon> coupons) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void saveCoupons(final List<Coupon> coupons) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void deleteCoupon(final Coupon coupon) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final Coupon processCouponAuthorization(final PointOfSale pointOfSale, final String couponCode, final int stallNumber)
        throws InvalidDataException {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void deleteAndSaveCoupons(final List<Coupon> coupons) throws BatchProcessingException {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final int findTotalCouponSize(final int customerId) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final Coupon modifyCoupon(final Coupon coupon, final Integer userId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int deleteCoupons(final Integer customerId, final String statusKey) throws BatchProcessingException {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final int deleteCouponsPaginated(final Integer customerId, final Date deleteTimeStamp, final Map<String, Object> context)
        throws BatchProcessingException {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final Future<Integer> processBatchUpdateAsync(final String statusKey, final int customerId, final Collection<Coupon> coupons,
        final String mode, final int userId) throws BatchProcessingException {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int processBatchUpdateWithStatus(final String statusKey, final int customerId, final Collection<Coupon> coupons, final String mode,
        final int userId) throws BatchProcessingException {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final int processBatchUpdate(final int customerId, final Collection<Coupon> coupons, final String mode, final int userId,
        final BatchProcessStatus status) throws BatchProcessingException {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final List<Coupon> findCouponByConsumerId(final int consumerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<Coupon> findByCustomerIdAndCouponIds(final Integer customerId, final Collection<Integer> couponIds) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int findCouponCountByCustomerId(final Integer customerId) {
        // TODO Auto-generated method stub
        return 0;
    }
}
