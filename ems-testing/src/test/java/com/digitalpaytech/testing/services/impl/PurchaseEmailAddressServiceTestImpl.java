package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.EmailAddress;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.PurchaseEmailAddress;
import com.digitalpaytech.dto.customeradmin.EmailAddressHistoryDetail;
import com.digitalpaytech.dto.customeradmin.TransactionReceiptDetails;
import com.digitalpaytech.service.PurchaseEmailAddressService;

@Service("purchaseEmailAddressServiceTest")
public class PurchaseEmailAddressServiceTestImpl implements PurchaseEmailAddressService {
    
    @Override
    public final Collection<PurchaseEmailAddress> findPurchaseEmailAddress(final int dataLoadingRows, final boolean b) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void sendReceipt(final PurchaseEmailAddress purchaseEmailAddress, final boolean isMigrated) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void savePurchaseEmail(final PurchaseEmailAddress purchaseEmailAddress) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void saveEmailAddress(final EmailAddress emailAddress) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final List<PurchaseEmailAddress> findPurchaseEmailAddressByPurchaseId(final Long purchaseId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final EmailAddress findEmailById(final Integer emailAddressId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final TransactionReceiptDetails getTransactionReceiptDetails(final Purchase purchase) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<EmailAddressHistoryDetail> findEmailAddressHistoryDetail(final Long purchaseId) {
        // TODO Auto-generated method stub
        return null;
    }
}
