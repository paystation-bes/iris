package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.ScrollableResults;
import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.PreAuthHolding;
import com.digitalpaytech.service.PreAuthService;

@Service("preAuthServiceTest")
public class PreAuthServiceTestImpl implements PreAuthService {
    
    @Override
    public final PreAuth getApprovedPreAuthByPsRefId(final int pointOfSaleId, final String psRefId, final String cardHash) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final PreAuth getApprovedPreAuthByPsRefIdAndPSAmount(final int pointOfSaleId, final String psRefId, final String cardHash,
        final int amount) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final PreAuth getApprovedPreAuthById(final Long id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final PreAuth getApprovedPreAuthByPSAmountAndAuthNumAndDate(final int pointOfSaleId, final int amount, final String authNumber,
        final long startDate, final long endDate) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final PreAuth getApprovedPreAuthByPSAmountAndAuthNum(final int pointOfSaleId, final int amount, final String authNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void cleanupExpiredPreauths(final Date expiredDate) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void deleteExpiredPreauths(final Date expiredDate) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final List<PreAuth> findExpiredPreAuths(final Date expiredDate) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Collection<PreAuth> findPreAuthsByCardData(final String cardData, final boolean isEvict) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void updatePreAuth(final PreAuth preAuth) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final int findTotalPreAuthsByCardData(final String cardData) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public void cleanupExpiredPreauthsForMigratedCustomers(final Date expiredDate) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void deleteExpiredPreauthsForMigratedCustomers(final Date expiredDate) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final List<PreAuth> findExpiredPreAuthsForMigratedCustomers(final Date expiredDate) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final PreAuthHolding findByApprovedPreAuthIdAuthorizationNumber(final Long preAuthId, final String authorizationNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<Long> findExpiredElavonPreAuths(final Date expiryDate, final int maxPreAuthLimit) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void deletePreauthsByIds(final Collection<Long> preAuthIds) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final List<PreAuth> findByIds(final Long... ids) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public ScrollableResults getExpiredCardAuthorizationBackUp(final Date expiryDate) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void cleanupExpiredPreauthForMigratedCustomer(final PreAuth preAuth) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void deleteExpiredPreauthForMigratedCustomer(final PreAuth preAuth) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public PreAuth findById(final Long id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public PreAuth getApprovedByCardRetryData(final Integer pointOfSaleId, final String cardHash, final int amount, final int ticketNumber) {
        return null;
    }
    
    @Override
    public void savePreauth(final PreAuth preauth) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public Collection<PreAuth> findByMerchantAccountId(final int merchantAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Collection<PreAuth> findApprovedByMerchantAcccountId(final Integer merchantAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
}
