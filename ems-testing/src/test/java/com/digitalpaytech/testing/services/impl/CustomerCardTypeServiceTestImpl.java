package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Future;
import java.util.zip.ZipOutputStream;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.util.RandomKeyMapping;

@Service("CustomerCardTypeServiceTest")
public class CustomerCardTypeServiceTestImpl implements CustomerCardTypeService {
    
    @Override
    public final CustomerCardType getUnknownCustomerCardType() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerCardType getCreditCardTypeByTrack2Data(final String track2Data) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerCardType getCreditCardTypeByAccountNumber(final String accountNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerCardType getCardTypeByAccountNumber(final String accountNumber, final List<CustomerCardType> customerCardTypes) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerCardType getCardTypeByTrack2Data(final String track2Data, final List<CustomerCardType> customerCardTypes) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerCardType getCardTypeByAccountNumber(final int customerId, final String accountNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerCardType getCardTypeByTrack2Data(final int customerId, final String track2Data) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerCardType getCardTypeByTrack2DataOrAccountNumber(final int customerId, final String track2Data) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<CustomerCardType> getAllCustomerCardType(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerCardType getCardType(final int id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void createCustomerCardType(final CustomerCardType cardType) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateCardType(final CustomerCardType cardType) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void deleteCardType(final CustomerCardType cardType) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final boolean isExistCardType(final CustomerCardType cardType) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public final int getTotalCustomerCardType(final CustomerCardType cardType) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final List<CustomerCardType> getCustomerCardTypesByCustomerAndName(final int customerId, final String name) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final boolean isCreditCardData(final String track2OrAcctNum) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public final boolean isCreditCardAccountNumber(final String accountNumber) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public final boolean isCreditCardTrack2(final String track2Data) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public final CustomerCardType getCreditCardTypeByCardNumber(final int customerId, final String cardNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final boolean isExistTrack2Pattern(final Integer customerId, final String track2pattern) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public final CustomerCardType findCustomerCardTypeForBadCard(final Integer customerId, final int cardTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final boolean generateBadCardList(final int customerId, final int cardTypeId, final ZipOutputStream zipfos) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public final CustomerCardType findCustomerCardTypeForTransaction(final Integer customerId, final Integer cardTypeId, final String cardNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<CustomerCardType> findByCustomerIdCardTypeIdIsLocked(final Integer customerId, final Integer cardTypeId,
        final boolean isLocked) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<CustomerCardType> findByCustomerIdIsLocked(final Integer customerId, final boolean isLocked) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerCardType getCustomerCardTypeById(final int id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerCardType getPrimaryType(final Integer customerId, final String name) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void ensurePrimaryReference(final Integer... customerCardTypeIds) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final Future<Boolean> ensurePrimaryReferenceAsync(final Integer... customerCardTypeIds) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerCardType createPrimaryTypeIfAbsent(final Integer customerId, final String name) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerCardType createPrimaryTypeIfAbsentFor(final Integer customerCardTypeId) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int deletePrimaryIfNoChild(final CustomerCardType primary) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final int countChildType(final Integer customerCardTypeId) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final List<FilterDTO> getBannableCardTypeFilters(final List<FilterDTO> result, final Integer customerId,
        final RandomKeyMapping keyMapping) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<CustomerCardType> getDetachedBannableCardTypes(final Integer customerId, final Collection<String> names) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<FilterDTO> getValueCardTypeFilters(final List<FilterDTO> result, final Integer customerId, final boolean showDeleted,
        final RandomKeyMapping keyMapping) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<CustomerCardType> getDetachedValueCardTypes(final Integer customerId, final Collection<String> names) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<CustomerCardType> getDetachedBannableCardTypes(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<FilterDTO> getValueCardTypeFiltersWithDeletedFlag(List<FilterDTO> result, Integer customerId, boolean showDeleted,
        RandomKeyMapping keyMapping) {
        // TODO Auto-generated method stub
        return null;
    }
}
