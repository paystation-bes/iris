package com.digitalpaytech.testing.services.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.SmsAlert;
import com.digitalpaytech.service.SmsAlertService;

@Service("smsAlertServiceTest")
public class SmsAlertServiceTestImpl implements SmsAlertService {
    
    @Override
    public final List<SmsAlert> findByAddTimeNumCustomerIdAndSpaceNumber(final Integer customerId, final int spaceNumber, final int addTimeNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<SmsAlert> findByAddTimeNumCustomerIdPaystationSettingNameAndSpaceNumber(final Integer customerId,
        final String paystationSettingName, final int spaceNumber, final int addTimeNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<SmsAlert> findByAddTimeNumLocationIdAndSpaceNumber(final Integer locationId, final int spaceNumber, final int addTimeNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<SmsAlert> findByLicencePlate(final Integer customerId, final String licencePlate) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<SmsAlert> findByCustomerIdPaystationSettingNameAndSpaceNumber(final Integer customerId, final String paystationSettingName,
        final int spaceNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<SmsAlert> findByCustomerIdAndSpaceNumber(final Integer customerId, final int spaceNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<SmsAlert> findByLocationIdAndSpaceNumber(final Integer locationId, final int spaceNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final SmsAlert findByMobileNumber(final String mobileNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final SmsAlert findSmsAlertById(final Integer id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int getSmsAlertNotAlerted() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public SmsAlert findUnexpiredSmsAlertById(Integer id) {
        // TODO Auto-generated method stub
        return null;
    }
}
