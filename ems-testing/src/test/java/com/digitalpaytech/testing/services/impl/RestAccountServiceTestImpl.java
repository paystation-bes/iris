package com.digitalpaytech.testing.services.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.RestAccount;
import com.digitalpaytech.service.RestAccountService;

@Service("restAccountServiceTest")
public class RestAccountServiceTestImpl implements RestAccountService {
    
    @Override
    public final List<RestAccount> findRestAccounts(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final RestAccount findRestAccountByAccountName(final String accountName) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<RestAccount> findNonUniqueRestAccountByAccountName(final String accountName) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final RestAccount findRestAccountBySecretKey(final String secretKey) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final RestAccount findRestAccountBySessionToken(final String sessionToken) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final RestAccount findRestAccountByPointOfSaleId(final int pointOfSaleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final RestAccount findRestAccountById(final int restAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void saveOrUpdate(final RestAccount restAccount) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void delete(final RestAccount restAccount) {
        // TODO Auto-generated method stub
        
    }
}
