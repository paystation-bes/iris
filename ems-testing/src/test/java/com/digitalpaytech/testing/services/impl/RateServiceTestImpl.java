package com.digitalpaytech.testing.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.Rate;
import com.digitalpaytech.service.RateService;

@Service("rateServiceTest")
public class RateServiceTestImpl implements RateService {

    @Override
    public final Rate findRateById(final Integer rateId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public final Rate findRateByCustomerIdAndName(final Integer customerId, final Byte rateTypeId, final String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public final List<Rate> findRatesByCustomerId(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public final List<Rate> findRatesByCustomerIdAndRateTypeId(final Integer customerId, final Byte rateTypeId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public final List<Rate> findPagedRateList(final Integer customerId, final Byte rateTypeId, final Date maxUpdateTime, final int pageNumber) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void saveOrUpdateRate(final Rate rate) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void deleteRate(final Rate rate, final Integer userAccountId) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public final List<Rate> searchRates(final Integer customerId, final String keyword) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public final int findRatePage(final Integer rateId, final Integer customerId, final Byte rateTypeId, final Date maxUpdateTime) {
        // TODO Auto-generated method stub
        return 0;
    }
}
