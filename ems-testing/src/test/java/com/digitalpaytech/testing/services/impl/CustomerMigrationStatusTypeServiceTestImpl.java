package com.digitalpaytech.testing.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CustomerMigrationStatusType;
import com.digitalpaytech.service.CustomerMigrationStatusTypeService;

@Service("customerMigrationStatusTypeServiceTest")
public class CustomerMigrationStatusTypeServiceTestImpl implements CustomerMigrationStatusTypeService {
    
    @Override
    public final List<CustomerMigrationStatusType> loadAll() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Map<Byte, CustomerMigrationStatusType> getCustomerMigrationStatusTypesMap() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final String getText(final byte customerMigrationStatusTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerMigrationStatusType findCustomerMigrationStatusType(final byte customerMigrationStatusTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerMigrationStatusType findCustomerMigrationStatusType(final String name) {
        // TODO Auto-generated method stub
        return null;
    }
}
