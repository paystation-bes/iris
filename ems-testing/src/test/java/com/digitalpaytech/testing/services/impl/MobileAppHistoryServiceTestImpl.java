package com.digitalpaytech.testing.services.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerMobileDevice;
import com.digitalpaytech.domain.MobileAppHistory;
import com.digitalpaytech.domain.MobileApplicationType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.service.MobileAppHistoryService;

@Service("MobileAppHistoryServiceTest")
public class MobileAppHistoryServiceTestImpl implements MobileAppHistoryService {
    
    @Override
    public final List<MobileAppHistory> getMobileAppHistoryByCustomer(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<MobileAppHistory> getMobileAppHistoryByDeviceUid(final String uid) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void logActivity(final Customer customer, final UserAccount userAccount, final MobileApplicationType appType,
        final CustomerMobileDevice device, final Integer activityTypeInt, final boolean isSuccessful, final String result) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final List<MobileAppHistory> getMobileAppHistoryByCustomerMobileDeviceId(final Integer customerMobileDeviceId, final Integer page,
        final Integer pageSize) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<MobileAppHistory> getMobileAppHistoryByCustomerMobileDeviceIdAndOrder(final Integer customerMobileDeviceId, final Integer page,
        final Integer pageSize, final String column, final String order) {
        // TODO Auto-generated method stub
        return null;
    }
}
