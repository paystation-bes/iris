package com.digitalpaytech.testing.services.impl;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.ReportContent;
import com.digitalpaytech.domain.ReportRepository;
import com.digitalpaytech.dto.ReportSearchCriteria;
import com.digitalpaytech.dto.customeradmin.ReportRepositoryInfo.RepositoryInfo;
import com.digitalpaytech.service.ReportRepositoryService;
import com.digitalpaytech.util.RandomKeyMapping;

@Service("reportRepositoryServiceTest")
public class ReportRepositoryServiceTestImpl implements ReportRepositoryService {
    
    @Override
    public void saveOrUpdateReportRepository(final ReportRepository reportRepository) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void deleteReportRepository(final int reportRepositoryId) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final ReportRepository getReportRepository(final int id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void deleteOldReportsByDay(final Date limitDay) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void deleteOldSystemAdminReports(final Date limitDay) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void addWarningToOldReportsByDay(final Date limitDay) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final List<Object[]> getUserAccountFileSizeTotals() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<Object[]> getUserAccountUnWarnedFileSizeTotals() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void deleteOldestReports(final int userAccountId, final int maxSize, final int totalSize) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void addWarningToOldestReports(final int userAccountId, final int maxSize, final int totalSize) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final ReportRepository findByReportHistoryId(final int reportHistoryId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ReportRepository> findByUserAccountId(final int userAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ReportRepository> findByCustomerId(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int countUnreadReportsByUserAccountId(final int userAccountId) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final ReportContent findReportContentByReportRepositoryId(final int reportRepositoryId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int deleteOlderAdhocReport(final Date targetTime) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public final List<ReportRepository> findByUserAccountIdForSystemAdmin(final int userAccountId, final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public final List<RepositoryInfo> findCompletedReports(final ReportSearchCriteria criteria, final RandomKeyMapping keyMapping, final TimeZone timeZone) {
        // TODO Auto-generated method stub
        return null;
    }
}
