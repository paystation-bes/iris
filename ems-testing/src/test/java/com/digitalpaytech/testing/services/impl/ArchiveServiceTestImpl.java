package com.digitalpaytech.testing.services.impl;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.RawSensorData;
import com.digitalpaytech.dto.paystation.EventData;
import com.digitalpaytech.service.ArchiveService;

@Service("archiveServiceTest")
public class ArchiveServiceTestImpl implements ArchiveService {
    
    @Override
    public void archiveRawSensorData(final RawSensorData rawSensorData) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void handleRawSensorDataTransformationError(final Exception e, final EventData event, final RawSensorData sensor, final int threadId) {
        // TODO Auto-generated method stub
        
    }
}
