package com.digitalpaytech.testing.services.impl;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.EventType;
import com.digitalpaytech.service.EventTypeService;

@Service("eventTypeServiceTest")
@SuppressWarnings("checkstyle:designforextension")
public class EventTypeServiceTestImpl implements EventTypeService {
    
    @Override
    public EventType findEventTypeById(final byte eventTypeId) {
        // TODO This is done for the PosEventCurrentProcessorImplTest class
        final EventType eventType = new EventType();
        eventType.setId(eventTypeId);
        
        return eventType;
    }
    
}
