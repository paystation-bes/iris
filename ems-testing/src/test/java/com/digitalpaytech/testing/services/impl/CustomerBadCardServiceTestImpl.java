package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.Future;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CustomerBadCard;
import com.digitalpaytech.dto.BatchProcessStatus;
import com.digitalpaytech.dto.customeradmin.CustomerBadCardSearchCriteria;
import com.digitalpaytech.service.BatchProcessingException;
import com.digitalpaytech.service.CustomerBadCardService;

@Service("customerBadCardServiceTest")
public class CustomerBadCardServiceTestImpl implements CustomerBadCardService {
    
    @Override
    public final Collection<CustomerBadCard> getCustomerBadCards(final Integer customerId, final String cardNumber, final Integer cardTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Collection<CustomerBadCard> findCustomerBadCards(final CustomerBadCardSearchCriteria criteria) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void createCustomerBadCard(final CustomerBadCard newCustomerBadCard) {
        // TODO Auto-generated method stub
        //        
    }
    
    @Override
    public void deleteCustomerBadCard(final CustomerBadCard customerBadCard) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final int deleteByCustomerId(final Integer customerId, final String statusKey) throws BatchProcessingException {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final int deleteByCustomerIdPaginated(final Integer customerId, final Date deleteTimeStamp, final Map<String, Object> context)
        throws BatchProcessingException {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public void updateCustomerBadCard(final CustomerBadCard customerBadCard) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateCustomerBadCards(final Collection<CustomerBadCard> customerBadCards) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final CustomerBadCard findCustomerBadCardById(final Integer id, final boolean isEvict) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Collection<CustomerBadCard> findCustomerBadCardByCustomerId(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Future<Integer> processBatchUpdateAsync(final String statusKey, final int customerId, final Collection<CustomerBadCard> badCardList,
        final String mode, final int userId) throws BatchProcessingException {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int processBatchUpdateWithStatus(final String statusKey, final int customerId, final Collection<CustomerBadCard> badCardList,
        final String mode, final int userId) throws BatchProcessingException {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final int processBatchUpdate(final int customerId, final Collection<CustomerBadCard> badCardList, final String mode, final int userId,
        final BatchProcessStatus status) throws BatchProcessingException {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final int getTotalBadCreditCard(final String cardData) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final Collection<CustomerBadCard> getBadCreditCardByCardData(final String cardData, final int howManyRows, final boolean isEvict) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Collection<CustomerBadCard> findNoExpiredBadCreditCard(final Integer customerId, final Integer customerCardTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int countByCustomerCardType(final Collection<Integer> customerCardTypeIds) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final void updateLast4DigitsOfCardNumber(final Integer id, final Short last4DigitsOfCardNumber) {
    }
    
    @Override
    public void createCustomerBadCard(Map<String, CustomerBadCard> badCards) {
        // TODO Auto-generated method stub
    }
}
