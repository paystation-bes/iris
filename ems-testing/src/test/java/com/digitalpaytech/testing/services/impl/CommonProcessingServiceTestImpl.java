package com.digitalpaytech.testing.services.impl;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.domain.Reversal;
import com.digitalpaytech.service.CommonProcessingService;

@Service("commonProcessingServiceTest")
public class CommonProcessingServiceTestImpl implements CommonProcessingService {
    
    @Override
    public final String getDestinationUrlString(final Processor processor) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final String getNewReferenceNumber(final MerchantAccount merchantAccount) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void sendCardProcessingAdminErrorAlert(final String subject, final String message, final Exception e) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final boolean isReversalExpired(final Reversal reversal) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public final String convertWildCards(final String value) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final boolean isTestMode(final int merchantAccountId) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isProcessorTestMode(int processorId) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Processor getProcessor(int processorId) {
        // TODO Auto-generated method stub
        return null;
    }
}
