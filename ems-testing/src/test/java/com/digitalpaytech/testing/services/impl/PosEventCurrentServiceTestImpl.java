package com.digitalpaytech.testing.services.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.EventType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.service.PosEventCurrentService;
import com.digitalpaytech.util.DateUtil;

@Service("posEventCurrentServiceTest")
@SuppressWarnings("checkstyle:designforextension")
public class PosEventCurrentServiceTestImpl implements PosEventCurrentService {
    
    @Override
    public PosEventCurrent findPosEventCurrentByPointOfSaleIdAndEventTypeId(final Integer pointOfSaleId, final byte eventTypeId) {
        // TODO 
        return null;
    }
    
    @Override
    public PosEventCurrent findPosEventCurrentByPointOfSaleIdAndEventTypeIdAndCustomerAlertTypeId(final Integer posId, final byte eventTypeId,
        final Integer customerAlertTypeId) {
        // TODO Done for the PosEventCurrentProcessorImplTest class
        final Date date = DateUtil.parse("Fri May 29 10:00:00 PDT 2015", "E MMM dd HH:mm:ss zzz YYYY", "GMT");
        final PointOfSale pos = new PointOfSale();
        pos.setId(posId);
        final EventType eventType = new EventType();
        eventType.setId((byte) eventTypeId);
        final CustomerAlertType customerAlertType = new CustomerAlertType();
        customerAlertType.setId(customerAlertTypeId);
        
        final PosEventCurrent posEventCurrent = new PosEventCurrent();
        posEventCurrent.setId(1L);
        posEventCurrent.setPointOfSale(pos);
        posEventCurrent.setEventType(eventType);
        posEventCurrent.setCustomerAlertType(customerAlertType);
        posEventCurrent.setEventSeverityType(new EventSeverityType(2));
        //TODO ACTIVE
        posEventCurrent.setIsActive(true);
        posEventCurrent.setAlertGmt(date);
        posEventCurrent.setClearedGmt(null);
        
        //TODO INACTIVE
        //        posEventCurrent.setIsActive(false);
        //        posEventCurrent.setAlertGmt(DateUtil.parse("Fri Nov 10 10:00:00 PDT 2014", "E MMM dd HH:mm:ss zzz YYYY", "GMT"));
        //        posEventCurrent.setClearedGmt(date);
        
        //TODO WRONG CLEARED
        //        posEventCurrent.setIsActive(false);
        //        posEventCurrent.setClearedGmt(DateUtil.getCurrentGmtDate());
        
        //TODO WRONG ALERTGMT
        //        posEventCurrent.setIsActive(false);
        //        posEventCurrent.setAlertGmt(DateUtil.getCurrentGmtDate());
        
        return posEventCurrent;
    }
    
    //    @Override
    //    public List<PosEventCurrent> findActivePosEventCurrentByEventTypeIdAndTimestamp(final List<Byte> eventTypeIdList, final Date timestamp) {
    //        // TODO Auto-generated method stub
    //        return null;
    //    }
    
    @Override
    public void savePosEventCurrent(final PosEventCurrent posEventCurrent) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updatePosEventCurrent(final PosEventCurrent posEventCurrent) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public List<PosEventCurrent> findActivePosEventCurrentByTimestamp(final Date timestamp) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void deletePosEventCurrent(final Integer posId, final byte eventTypeId, final Integer customerAlertTypeId) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public Collection<PosEventCurrent> findNextRecoveryDuplicateSerialNumberData(final int numOfRows) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PosEventCurrent> findMaintenanceCentreDetailByPointOfSaleId(final Integer pointOfSaleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PosEventCurrent> findCollectionCentreDetailByPointOfSaleId(final Integer pointOfSaleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PosEventCurrent> findActivePosEventCurrentByCustomerId(final Integer customerId) {
        // TODO Auto-generated method stub
        return new ArrayList<PosEventCurrent>();
    }
    
    @Override
    public List<PosEventCurrent> findActiveDefaultPosEventCurrentByPosIdListAndAttIdList(final List<Integer> pointOfSaleIdList,
        final List<Integer> alertThresholdTypeIdList) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public PosEventCurrent findActivePosEventCurrentById(final Long posEventCurrentId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public PosEventCurrent findActivePosEventCurrentByEventTypeIdAndPointOfSaleId(final byte eventTypeId, final Integer pointOfSaleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List findActivePosEventCurrentByPointOfSaleId(final Integer posId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void delete(final PosEventCurrent posEventCurrent) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public List<PosEventCurrent> findAllPosEventCurrentByPointOfSaleId(final Integer posId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<Long> findDefaultPosEventCurrentsToDisable(final List<Integer> pointOfSaleIdList) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<Long> findDefaultPosEventCurrentsToEnable(final List<Integer> pointOfSaleIdList) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PosEventCurrent> findPosEventCurrentsByIdList(final List<Long> pecIdList) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
