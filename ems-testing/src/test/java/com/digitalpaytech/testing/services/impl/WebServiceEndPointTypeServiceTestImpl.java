package com.digitalpaytech.testing.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.WebServiceEndPointType;
import com.digitalpaytech.service.WebServiceEndPointTypeService;

@Service("webServiceEndPointTypeServiceTest")
@SuppressWarnings("checkstyle:designforextension")
public class WebServiceEndPointTypeServiceTestImpl implements WebServiceEndPointTypeService {
    
    @Override
    public List<WebServiceEndPointType> loadAll() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Map<Byte, WebServiceEndPointType> getWebServiceEndPointTypesMap() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public String getText(final byte webServiceEndPointTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public WebServiceEndPointType findWebServiceEndPointType(final String name) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<WebServiceEndPointType> loadAllPrivate() {
        // TODO Auto-generated method stub
        return null;
    }
    
}
