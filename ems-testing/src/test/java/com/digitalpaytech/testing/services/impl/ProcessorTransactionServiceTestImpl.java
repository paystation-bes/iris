package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.PreAuthHolding;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.ProcessorTransactionType;
import com.digitalpaytech.domain.CPSData;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.dto.TransactionSearchCriteria;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.service.ProcessorTransactionService;

@Service("processorTransactionServiceTest")
public class ProcessorTransactionServiceTestImpl implements ProcessorTransactionService {
    
    @Override
    public final ProcessorTransaction findProcessorTransactionById(final Long id, final boolean isEvict) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Set<ProcessorTransaction> findRefundablePtdsByCard(final short last4DigitsofCardNumber, final int cardChecksum,
        final String cardHash, final String cardType, final Date startDate, final Date endDate, final int merchantAccountId,
        final Collection<Integer> psList) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Set<ProcessorTransaction> findRefundablePtdsByCardAndAuthNumber(final String authNumber, final short last4DigitsofCardNumber,
        final int cardChecksum, final String cardHash, final String cardType, final Date startDate, final Date endDate, final int merchantAccountId,
        final Collection<Integer> psList) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int updateProcessorTransactionType(final Long id, final int typeId, final int newTypeId) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final ProcessorTransaction getApprovedTransaction(final int pointOfSaleId, final Date purchasedDate, final int ticketNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ProcessorTransactionType getProcessorTransactionType(final Integer processorTransactionTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void updateProcessorTransactionToBatchedSettled(final ProcessorTransaction ptd) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateProcessorTransactionToSettled(final ProcessorTransaction ptd) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final Collection<ProcessorTransaction> findProcessorTransactionByPurchaseId(final Long purchaseId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Collection<ProcessorTransaction> findRefundedProcessorTransaction(final Long purchaseId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ProcessorTransaction findRefundProcessorTransaction(final Integer pointOfSaleId, final Date purchasedDate,
        final Integer ticketNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ProcessorTransaction findPartialSettlementProcessorTransaction(final Integer pointOfSaleId, final Date purchasedDate,
        final Integer ticketNumber, final boolean isEvict) throws ApplicationException {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ProcessorTransaction> findRefundableTransaction(final TransactionSearchCriteria criteria) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ProcessorTransaction findTransactionPosPurchaseById(final Long transactionId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void removePreAuthsInProcessorTransactions(final List<Long> preAuthIds) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateProcessorTransaction(final ProcessorTransaction ptd) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void saveProcessorTransaction(final ProcessorTransaction ptd) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void removePreAuthInProcessorTransactions(final Long preAuthIds) {
        // TODO Auto-generated method stub
    }
    
    @Override
    public final int markRecoverable(final ProcessorTransaction pt) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public ProcessorTransaction findProcTransByPurchaseIdTicketNoAndPurchasedDate(final Long purchaseId, final Date purchasedDate,
        final Integer ticketNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public ProcessorTransaction findByPointOfSaleIdPurchasedDateTicketNumber(final int pointOfSaleId, final Date purchasedDate,
        final int ticketNumber) {
        return null;
    }
    
    @Override
    public Collection<ProcessorTransaction> findProcessorTransactionByPurchaseIdAndIsApprovedAndIsRefund(final Long purchaseId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public boolean isIncomplete(final ProcessorTransaction processorTransaction) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean isIncomplete(final MerchantAccount ma, final ProcessorTransaction pt) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public Collection<ProcessorTransaction> findByMercAccIdAndProcTransTypeIdAndHasPurchase(final Integer merchantAccountId,
        final Integer transactionTypeId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isRefundableTransaction(final ProcessorTransaction ptd) {
        return false;
    }
}
