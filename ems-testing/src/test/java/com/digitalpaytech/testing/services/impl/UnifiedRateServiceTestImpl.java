package com.digitalpaytech.testing.services.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.ExtensibleRate;
import com.digitalpaytech.domain.UnifiedRate;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.service.UnifiedRateService;

@Service("unifiedRateServiceTest")
public class UnifiedRateServiceTestImpl implements UnifiedRateService {
    
    @Override
    public final List<UnifiedRate> findRatesByCustomerId(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void insertWithNewExtensibleRate(final UserAccount userAccount, final ExtensibleRate extensibleRate) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final UnifiedRate findRateByNameAndCustomerId(final String unifiedRateName, final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final UnifiedRate insert(final UserAccount userAccount, final String name) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final UnifiedRate findUnifiedRateById(final Integer unifiedRateId) {
        // TODO Auto-generated method stub
        return null;
    }
}
