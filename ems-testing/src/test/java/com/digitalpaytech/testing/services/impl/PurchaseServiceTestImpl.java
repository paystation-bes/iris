package com.digitalpaytech.testing.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.dto.customeradmin.TransactionReceiptSearchCriteria;
import com.digitalpaytech.service.PurchaseService;

@Service("purchaseServiceTest")
public class PurchaseServiceTestImpl implements PurchaseService {
    
    @Override
    public final Purchase findPurchaseById(final Long id, final boolean isEvict) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<Purchase> findPurchasesByPointOfSaleIdLast90Days(final int customerId, final int pointOfSaleId, final String sortOrder,
        final String sortItem, final Integer page) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Purchase findUniquePurchaseForSms(final Integer customerId, final Integer pointOfSaleId, final Date purchaseGmt,
        final int purchaseNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Purchase findLatestPurchaseByPointOfSaleIdAndPurchaseNumber(final int pointOfSaleId, final String purchaseNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Purchase findPurchaseByPointOfSaleIdAndPurchaseNumberAndpPurchaseGmt(final Integer pointOfSaleId, final Integer purchaseNumber,
        final Date purchaseGmt) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Object findPurchaseByCriteria(final TransactionReceiptSearchCriteria criteria) {
        // TODO Auto-generated method stub
        return null;
    }
}
