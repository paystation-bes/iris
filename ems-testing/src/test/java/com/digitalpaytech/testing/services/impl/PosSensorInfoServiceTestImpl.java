package com.digitalpaytech.testing.services.impl;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.PosSensorInfo;
import com.digitalpaytech.service.PosSensorInfoService;

@Service("posSensorInfoServiceTest")
public class PosSensorInfoServiceTestImpl implements PosSensorInfoService {
    
    @Override
    public final List<PosSensorInfo> findPosSensorInfoByPointOfSaleIdTypeId(final int pointOfSaleId, final int typeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void archiveOldPosSensorInfoData(final int archiveIntervalDays, final int archiveBatchSize, final String archiveServerName) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final List<PosSensorInfo> findPosSensorInfoByPointOfSaleIdAndTypeIdAndDateRange(final Set<Integer> pointOfSaleIdList,
        final int sensorInfoTypeId, final Date fromDate, final Date toDate, final int limit) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<PosSensorInfo> findPosSensorInfoByPointOfSaleIdAndTypeIdAndStartDate(final Integer pointOfSaleId,
        final Integer sensorInfoTypeId, final Date fromDate) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<PosSensorInfo> findPosSensorInfoByPointOfSaleIdAndTypeIdAndStartDateOrderBySensorGMTDesc(final Integer pointOfSaleId,
        final Integer sensorInfoTypeId, final Date fromDate) {
        // TODO Auto-generated method stub
        return null;
    }
}
