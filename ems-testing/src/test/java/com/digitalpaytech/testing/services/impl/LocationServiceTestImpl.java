package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.ExtensibleRate;
import com.digitalpaytech.domain.ExtensibleRateDayOfWeek;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.LocationDay;
import com.digitalpaytech.domain.ParkingPermission;
import com.digitalpaytech.domain.ParkingPermissionDayOfWeek;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.customeradmin.LocationTree;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.util.RandomKeyMapping;

@Service("locationServiceTest")
public class LocationServiceTestImpl implements LocationService {
    
    @Override
    public final List<Location> findLocationsByCustomerId(final Integer customerId) {
        
        return null;
    }
    
    @Override
    public final List<Location> findChildLocationsByCustomerId(final int customerId) {
        
        return null;
    }
    
    @Override
    public final List<Location> findParentLocationsByCustomerId(final int customerId) {
        
        return null;
    }
    
    @Override
    public final List<Location> getLocationsByParentLocationId(final int parentLocationId) {
        
        return null;
    }
    
    @Override
    public final List<LocationDay> findLocationDayByLocationId(final int locationId) {
        
        return null;
    }
    
    @Override
    public final Location findLocationByCustomerIdAndName(final int customerId, final String name) {
        
        return null;
    }
    
    @Override
    public final Location findLocationById(final int locationId) {
        
        return null;
    }
    
    @Override
    public final List<Integer> findAllParentIds(final int locationId) {
        
        return null;
    }
    
    @Override
    public final Location findUnassignedLocationByCustomerId(final int customerId) {
        
        return null;
    }
    
    @Override
    public final Location findLocationByPointOfSaleId(final int pointOfSaleId) {
        
        return null;
    }
    
    @Override
    public final boolean locationContainsPermitsById(final int locationId) {
        
        return false;
    }
    
    @Override
    public void deleteLocationById(final int locationId) {
        
    }
    
    @Override
    public void updateLocationOpenCloseTime(final int userAccountId, final int locationId, final int dayOfWeek, final Integer openTime,
        final Integer closeTime, final Integer closeTimeOfPreviousDay, final boolean open, final boolean closed) {
        
    }
    
    @Override
    public void saveOrUpdateLocation(final Location location) {
        
    }
    
    @Override
    public final List<List<ExtensibleRate>> getValidRatesByLocationId(final Integer locationId) {
        
        return null;
    }
    
    @Override
    public final List<ExtensibleRateDayOfWeek> getValidRatesDayOfWeekByLocationId(final Integer locationId) {
        
        return null;
    }
    
    @Override
    public final List<List<ParkingPermission>> getValidParkingPermissionsByLocationId(final Integer locationId) {
        
        return null;
    }
    
    @Override
    public final List<ParkingPermissionDayOfWeek> getValidParkingPermissionsDayOfWeekByLocationId(final Integer locationId) {
        
        return null;
    }
    
    @Override
    public final ExtensibleRate findExtensibleRate(final int extensibleRateId) {
        
        return null;
    }
    
    @Override
    public final ParkingPermission findParkingPermission(final int parkingPermissionId) {
        
        return null;
    }
    
    @Override
    public void saveOrUpdateExtensibleRate(final UserAccount userAccount, final ExtensibleRate extensibleRate) {
        
    }
    
    @Override
    public void saveOrUpdateParkingPermission(final UserAccount userAccount, final ParkingPermission parkingPermission) {
        
    }
    
    @Override
    public void deleteExtensibleRateById(final int extensibleRateId) {
        
    }
    
    @Override
    public void deleteLocationOpenByLocationId(final int locationId) {
        
    }
    
    @Override
    public void deleteParkingPermissionById(final int parkingPermissionId) {
        
    }
    
    @Override
    public final ExtensibleRate evictExtensibleRate(final ExtensibleRate extensibleRate) {
        
        return null;
    }
    
    @Override
    public final ParkingPermission evictParkingPermission(final ParkingPermission parkingPermission) {
        
        return null;
    }
    
    @Override
    public final List<ExtensibleRate> findOverlappedExtensibleRate(final ExtensibleRate extensibleRate, final List<Integer> daysOfWeek) {
        
        return null;
    }
    
    @Override
    public final List<ParkingPermission> findOverlappedParkingPermission(final ParkingPermission parkingPermission, final List<Integer> daysOfWeek) {
        
        return null;
    }
    
    @Override
    public final List<Location> findLocationsByCustomerIdAndName(final int customerId, final String name) {
        
        return null;
    }
    
    @Override
    public final List<Location> findLocationsWithPointOfSalesByCustomerId(final Integer customerId) {
        
        return null;
    }
    
    @Override
    public final LocationTree getLocationTreeByCustomerId(final int customerId, final boolean showActiveOnly, final boolean showAssignedOnly,
        final RandomKeyMapping keyMapping) {
        
        return null;
    }
    
    @Override
    public final LocationTree getLocationPayStationTreeByCustomerId(final int customerId, final boolean showActiveOnly,
        final boolean showAssignedOnly, final boolean showVisibleOnly, final RandomKeyMapping keyMapping) {
        
        return null;
    }
    
    @Override
    public final LocationTree getRoutePayStationTreeByCustomerId(final int customerId, final boolean showActiveOnly, final boolean showAssignedOnly,
        final boolean showVisibleOnly, final RandomKeyMapping keyMapping) {
        
        return null;
    }
    
    @Override
    public final List<Location> getLowestLocationsByCustomerIdAndName(final int customerId, final String locationName) {
        
        return null;
    }
    
    @Override
    public final List<FilterDTO> getLocationFiltersByCustomerId(final int customerId, final boolean showActiveOnly, final boolean showAssignedOnly,
        final RandomKeyMapping keyMapping) {
        
        return null;
    }
    
    @Override
    public final List<Location> getLocationByCustomerId(final int customerId, final boolean showActiveOnly, final boolean showAssignedOnly) {
        
        return null;
    }
    
    @Override
    public final List<Location> getDetachedLocationsByLowerCaseNames(final int customerId, final Collection<String> names) {
        
        return null;
    }
    
    @Override
    public final Location getParentLocationByCustomerIdLocationName(final int customerId, final String locationName) {
        
        return null;
    }
}
