package com.digitalpaytech.testing.services.impl;

import java.util.concurrent.Future;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.digitalpaytech.client.dto.llps.PreferredParkerFile;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.LicencePlate;
import com.digitalpaytech.domain.LicensePlateFileUploadStatus;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.dto.PlateCheck;
import com.digitalpaytech.dto.PlateCheckResponse;
import com.digitalpaytech.dto.paystation.TransactionDto;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.service.LicensePlateService;

@Service("licensePlateServiceTest")
public class LicensePlateServiceTestImpl implements LicensePlateService {
    
    @Override
    public final LicencePlate findByNumber(final String number) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final LicencePlate findById(final Integer id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public PlateCheckResponse checkPlateLookup(final PlateCheck plateCheck) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public PreferredParkerFile uploadLicensePlates(final Integer unifiId, final boolean isByLocation, final MultipartFile multipartFile) throws JsonException {
        // TODO Auto-generated method stub
        return null;
    }
    
    public final LicensePlateFileUploadStatus findLicensePlateFileUploadStatusByCustomerId(final Integer customerId) {
        return null;
    }
    
    @Override
    public final PreferredParkerFile getCurrentStatus(final String fileId, final Integer unifiId) throws JsonException {
        return null;
    }
    
    @Override
    public boolean compareJurisdictionType(final int paystationJurisdictionType, final CustomerProperty jurisdictionTypeProperty) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public Future<Boolean> updateLicensePlateService(final TransactionDto txDto, final PointOfSale pointOfSale, final Purchase purchase) {
        // TODO Auto-generated method stub
        return null;
    }
}
