package com.digitalpaytech.testing.services.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.ThirdPartyServiceAccount;
import com.digitalpaytech.service.ThirdPartyServiceAccountService;

@Service("thirdPartyServiceAccountServiceTest")
public class ThirdPartyServiceAccountServiceTestImpl implements ThirdPartyServiceAccountService {
    
    @Override
    public final List<ThirdPartyServiceAccount> findByCustomerId(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ThirdPartyServiceAccount getThirdPartyServiceAccountByCustomerAndType(final Integer customerId, final String type) {
        // TODO Auto-generated method stub
        return null;
    }
}
