package com.digitalpaytech.testing.services.impl;

import java.util.List;
import java.util.Map;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosTelemetry;
import com.digitalpaytech.domain.TelemetryType;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.service.paystation.PosTelemetryService;
import com.digitalpaytech.util.KeyValuePair;

public class PosTelemetryServiceTestImpl implements PosTelemetryService {
    @Override
    public List<KeyValuePair<String, String>> findActiveByPosId(Integer pointOfSaleId, boolean includePrivate) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void processPosTelemetry(PointOfSale pointOfSale, String timeStamp, Map<String, String> telemetryData, String type, Integer referenceCounter) throws InvalidDataException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public TelemetryType findTelemetryTypeByName(String telemetryTypeName) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TelemetryType findTelemetryTypeByLabel(String telemetryTypeLabel) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<PosTelemetry> findCurrentPosTelemetryByPointOfSaleIdAndTelemetryType(Integer pointOfSaleId, Integer telemetryTypeId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<PosTelemetry> findAllCurrentTelemetryForPointOfSaleByPointOfSaleId(Integer pointOfSaleId) {
        // TODO Auto-generated method stub
        return null;
    }

}
