package com.digitalpaytech.testing.services.impl;

import org.springframework.stereotype.Component;

import com.digitalpaytech.service.EmsPropertiesService;

@Component("emsPropertiesServiceTest")
@SuppressWarnings({ "checkstyle:designforextension" })
public class EmsPropertiesServiceTestImpl implements EmsPropertiesService {
    
    @Override
    public String getPropertyValue(final String propertyName) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public String getPropertyValue(final String propertyName, final String defaultValue) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public String getPropertyValue(final String propertyName, final String defaultValue, final boolean forceGet) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public String getPropertyValue(final String propertyName, final boolean forceGet) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public int getPropertyValueAsInt(final String propertyName, final int defaultValue) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public int getPropertyValueAsInt(final String propertyName, final int defaultValue, final boolean forceGet) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public int getPropertyValueAsIntQuiet(final String propertyName, final int defaultValue) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public void findPropertiesFile() {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public boolean isWindowsOS() {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void updatePrimaryServer(final String newPrimaryServer) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void loadPropertiesFile() {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public boolean getPropertyValueAsBoolean(final String propertyName, final boolean defaultValue) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public boolean getPropertyValueAsBoolean(final String propertyName, final boolean defaultValue, final boolean forceGet) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public <V> V getProperty(final String key, final Class<V> valueClass, final V defaulValue) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public String getIrisVersion() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public String getIrisBuildNumber() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public String getIrisBuildDate() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public String getStaticContentPath() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public String getStaticContentPatam() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public String getIrisDeploymentMode() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getPropertyValueAsLong(final String propertyName, final long defaultValue) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public long getPropertyValueAsLong(final String propertyName, final long defaultValue, final boolean forceGet) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void updatePatchExecution(boolean execute) {
        // TODO Auto-generated method stub
        
    }
}
