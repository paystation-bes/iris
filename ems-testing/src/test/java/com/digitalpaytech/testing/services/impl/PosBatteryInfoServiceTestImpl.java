package com.digitalpaytech.testing.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.PosBatteryInfo;
import com.digitalpaytech.service.PosBatteryInfoService;

@Service("posBatteryInfoServiceTest")
public class PosBatteryInfoServiceTestImpl implements PosBatteryInfoService {

    @Override
    public void archiveOldPosBatteryInfoData(final int archiveIntervalDays, final int archiveBatchSize, final String archiveServerName) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public final List<PosBatteryInfo> findAllPosBatteryInfoByPointOfSaleIdsAndFromDate(final Integer pointOfSaleId, final Date fromDate) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void addBatteryInfo(final Integer pointOfSaleId, final Integer sensorTypeId, final Date sensorDateGmt, final Float value) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public final List<PosBatteryInfo> findAllPosBatteryInfoByPointOfSaleIdsAndFromDateNoZeroes(final Integer pointOfSaleId, final Date fromDate) {
        // TODO Auto-generated method stub
        return null;
    }
}
