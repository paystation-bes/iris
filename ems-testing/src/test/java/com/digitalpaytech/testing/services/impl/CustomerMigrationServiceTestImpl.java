package com.digitalpaytech.testing.services.impl;

import java.io.Serializable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.domain.CustomerMigrationEmail;
import com.digitalpaytech.dto.CustomerMigrationSearchCriteria;
import com.digitalpaytech.dto.customermigration.CustomerMigrationData;
import com.digitalpaytech.dto.customermigration.RecentBoardedMigratedCustomerList;
import com.digitalpaytech.service.CustomerMigrationService;

@Service("customerMigrationServiceTest")
public class CustomerMigrationServiceTestImpl implements CustomerMigrationService {
    
    @Override
    public final Serializable updateCustomerMigration(final CustomerMigration customerMigration, final List<String> emailAddress) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void updateCustomerMigration(final CustomerMigration customerMigration) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final CustomerMigration findCustomerMigrationByCustomerId(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<CustomerMigration> findScheduledCustomerMigration(final int batchSize) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final boolean isMigrationInProgress() {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public final CustomerMigration findCustomerMigrationById(final Integer customerMigrationId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerMigration findNextAvailableCustomer() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerMigrationData getCurrentCustomerMigrationData() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerMigrationData getCurrentPaystationMigrationStatus() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<CustomerMigrationData> getCustomerMigrationData() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<CustomerMigrationData> getCustomerPaystationMigrationData() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<CustomerMigration> findThisWeeksSchedule() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<CustomerMigration> findCustomerMigrationByCriteria(final CustomerMigrationSearchCriteria criteria, final String timeZone) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final String getMigrationStatusText(final byte customerMigrationStatusTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int getMigrationStatusInt(final byte customerMigrationStatusTypeId) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final List<RecentBoardedMigratedCustomerList> getRecentCustomerActivityList(final HttpServletRequest request, final String sortOrder,
        final String sortItem, final Integer page, final boolean isBoarded) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<CustomerMigrationEmail> findCustomerMigrationEmails(final Integer customerMigrationId) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
