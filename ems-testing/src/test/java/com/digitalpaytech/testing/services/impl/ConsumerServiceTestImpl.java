package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.Consumer;
import com.digitalpaytech.domain.EmailAddress;
import com.digitalpaytech.dto.customeradmin.ConsumerSearchCriteria;
import com.digitalpaytech.service.ConsumerService;

@Service("consumerServiceTest")
public class ConsumerServiceTestImpl implements ConsumerService {
    
    @Override
    public final Collection<Consumer> findConsumerByCustomerId(final int customerId, final Integer pageNo, final Integer itermsPerPage,
        final String column, final String order, final Long currentTime) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Consumer findConsumerById(final int id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final boolean deleteConsumer(final Consumer consumer) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void updateConsumer(final Consumer consumer, final String email, final Collection<Integer> couponIds, final Collection<Integer> cardIds) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void createConsumer(final Consumer consumer) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final Collection<Consumer> findConsumerByCustomerIdAndFilterValue(final int customerId, final String filterValue, final Integer pageNo,
        final Integer itermsPerPage, final String column, final String order, final Long currentTime) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<Consumer> findConsumerByCriteria(final ConsumerSearchCriteria criteria) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int findRecordPage(final Integer consumerId, final ConsumerSearchCriteria criteria) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final Date findMaxLastModifiedGmtByCustomerId(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<Consumer> findOtherConsumerByCustomerIdAndEmailId(final int consumerId, final int customerId, final int emailAddressId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<EmailAddress> findEmailAddressByEmail(final String email) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void saveConsumer(final Consumer consumer, final Collection<Integer> couponIds, final Collection<Integer> customerCardIds) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void saveConsumer(final Consumer consumer) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final List<Consumer> findConsumersByFirstLastNameOrEmail(final int customerId, final String firstLastNameOrEmail) {
        // TODO Auto-generated method stub
        return null;
    }
}
