package com.digitalpaytech.testing.services.impl;

import java.util.Collection;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.RawSensorData;
import com.digitalpaytech.dto.paystation.EventData;
import com.digitalpaytech.service.RawSensorDataService;

@Service("rawSensorDataServiceTest")
public class RawSensorDataServiceTestImpl implements RawSensorDataService {
    
    @Override
    public void saveRawSensorData(final String serialNumber, final EventData eventData) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final RawSensorData getRawSensorData(final Long rawSensorDataId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void deleteRawSensorData(final RawSensorData rawSensorData) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateRawSensorData(final RawSensorData rawSensorData) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void archiveRawSensorData(final RawSensorData rawSensorData) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final Collection<RawSensorData> findRawSensorDataForServer(final int sensorDataBatchSize) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final boolean rawSensorDataExist(final Long id) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public final int getRawSensorDataCount() {
        // TODO Auto-generated method stub
        return 0;
    }
}
