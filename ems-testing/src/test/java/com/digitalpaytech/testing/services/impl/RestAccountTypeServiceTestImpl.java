package com.digitalpaytech.testing.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.RestAccountType;
import com.digitalpaytech.service.RestAccountTypeService;

@Service("restAccountTypeServiceTest")
public class RestAccountTypeServiceTestImpl implements RestAccountTypeService {
    
    @Override
    public final List<RestAccountType> loadAll() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Map<Integer, RestAccountType> getRestAccountTypesMap() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final String getText(final int restAccountTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final RestAccountType findRestAccountType(final String name) {
        // TODO Auto-generated method stub
        return null;
    }
}
