package com.digitalpaytech.testing.services.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.service.ProcessorService;

@Service("processorServiceTest")
public class ProcessorServiceTestImpl implements ProcessorService {
    
    @Override
    public final List<Processor> findAll() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Processor findProcessor(final Integer id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final void updateProcessor(final Processor processor) {
        return;
    }
    
    @Override
    public Processor findByName(final String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isProcessorReadyForMigration(Processor processor) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Processor findProcessorReadyForMigration(Integer processorId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isProcessorReadyForMigration(Processor processor, boolean isLinkSuffixAttached) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String getCorrespondingLinkProcName(String processorName) {
        // TODO Auto-generated method stub
        return null;
    }
}
