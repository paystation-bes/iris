package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.service.CardRetryTransactionService;

@Service("cardRetryTransactionServiceTest")
public class CardRetryTransactionServiceTestImpl implements CardRetryTransactionService {
    
    @Override
    public void clearRetriesExceededCardData() {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void clearRetriesExceededCardDataForMigratedCustomers() {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final Date getMaxRetryTimeForTransaction(final CardRetryTransaction crt) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int getMatchingTransactionsCountForCardData(final String cardData) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final Collection<CardRetryTransaction> getCardRetryTransactionByCardData(final String cardData, final int howManyRows,
        final boolean isEvict) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void updateCardRetryTransaction(final CardRetryTransaction cardRetryTransaction) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final Collection<CardRetryTransaction> findCardRetryTransactions(final Collection<Integer> pointOfSaleIds, final int maxRetries,
        final int numRecords, final Date retryDate) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CardRetryTransaction findByPointOfSalePurchasedDateTicketNumber(final Integer pointOfSaleId, final Date purchasedDate,
        final Integer ticketNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<String> findBadCardHashByPointOfSaleIdsDateAndNumRetries(final List<Integer> pointOfSaleIds, final int yymm,
        final int creditCardOfflineRetryLimit) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final EntityDao getEntityDao() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Object[]> findEmptyBadCardHash() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int updateBadCardHash(String cardHash, String badCardHash) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final void save(final CardRetryTransaction crt) {
    }

    @Override
    public final CardRetryTransaction findCardRetryTransaction(final Long id) {
        return null;
    }
    
    @Override
    public final void delete(final CardRetryTransaction cardRetryTransaction) {
    }
    
    @Override
    public final CardRetryTransaction getCardRetryTransaction(final List<Object> objects) {
        return null;
    }
    
    @Override
    public CardRetryTransaction getCardRetryTransaction(final PointOfSale pointOfSale, final Purchase purchase, final ProcessorTransaction ptd,
        final String encryptedCardData) {
        return null;
    }
}
