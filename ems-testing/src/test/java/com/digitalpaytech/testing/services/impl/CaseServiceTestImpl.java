package com.digitalpaytech.testing.services.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CaseCustomerAccount;
import com.digitalpaytech.domain.CaseCustomerFacility;
import com.digitalpaytech.domain.CaseLocationLot;
import com.digitalpaytech.domain.CaseOccupancyHour;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.dto.CaseLocationLotSearchCriteria;
import com.digitalpaytech.dto.CaseLocationLotSelectorDTO;
import com.digitalpaytech.service.CaseService;
import com.digitalpaytech.util.RandomKeyMapping;

@Service("caseServiceTest")
@SuppressWarnings({ "checkstyle:designforextension", "PMD.TooManyMethods" })
public class CaseServiceTestImpl implements CaseService {
    
    @Override
    public CaseCustomerAccount findCaseCustomerAccountByCaseAccountId(final Integer caseAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void saveCaseCustomerAccount(final CaseCustomerAccount caseCustomerAccount) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateCaseCustomerAccount(final CaseCustomerAccount caseCustomerAccount) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public List<CaseLocationLot> findCaseLocationLotByCustomerId(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<CaseLocationLot> findUnassignedLocationLotByCustomerId(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<CaseLocationLot> findCaseLocationLotByLocationId(final Integer locationId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void updateCaseLocationLot(final CaseLocationLot caseLocationLot) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public CaseLocationLot findCaseLocationLotById(final Integer caseLocationLotId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public CaseCustomerAccount findCaseCustomerAccountByCustomerId(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public CaseCustomerAccount findCaseCustomerAccountById(final Integer caseCustomerAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public boolean isCaseDataExtractionServer() {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public CaseCustomerFacility findCaseCustomerFacilityByCaseFacilityId(final Integer caseFacilityId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void saveCaseCustomerFacility(final CaseCustomerFacility caseCustomerFacility) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateCaseCustomerFacility(final CaseCustomerFacility caseCustomerFacility) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public CaseLocationLot findCaseLocationLotByLotId(final Integer caseLotId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<CaseCustomerFacility> findCaseCustomerFacilityByCustomerId(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void saveCaseLocationLot(final CaseLocationLot caseLocationLot) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public List<CaseCustomerAccount> findUnassignedCaseCustomerAccountByNameKeyword(final Integer customerId, final String keyword) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<CaseCustomerAccount> findCaseCustomerAccountByNameKeyword(final String keyword) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<CaseLocationLotSelectorDTO> findCaseLocationLotSelector(final CaseLocationLotSearchCriteria criteria,
        final RandomKeyMapping keyMapping) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<CaseCustomerAccount> findAssignedCaseCustomerAccount() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public boolean isCaseCustomerAccountListEmpty() {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public CaseCustomerFacility findCaseCustomerFacilityByCaseLotId(final Integer caseLotId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Integer findLatestCaseOccupancyHourTimeIdGMTByLocationId(final Integer locationId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Integer findLatestCaseOccupancyHourTimeIdLocalByLocationId(final Integer locationId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public CaseOccupancyHour findCaseOccupancyHourByLocationIdAndTimeIdGMT(final Integer locationId, final Integer timeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void saveOrUpdateCaseOccupancyHour(final CaseOccupancyHour caseOccupancyHour) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public List<Location> findActiveCaseLocationLots() {
        // TODO Auto-generated method stub
        return null;
    }
    
}
