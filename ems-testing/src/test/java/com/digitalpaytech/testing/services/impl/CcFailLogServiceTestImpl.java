package com.digitalpaytech.testing.services.impl;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CcFailLog;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.service.CcFailLogService;

@Service("ccFailLogServiceTest")
public class CcFailLogServiceTestImpl implements CcFailLogService {

    @Override
    public final void logFailedCcTransaction(final CcFailLog ccFailLog) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final void logFailedCcTransaction(final PointOfSale pointOfSale, 
                                             final MerchantAccount merchantAccount, 
                                             final Date procDate,
                                             final Date purchasedDate, 
                                             final int ticketNumber, 
                                             final int ccType, 
                                             final String failedReason) {
    }
}
