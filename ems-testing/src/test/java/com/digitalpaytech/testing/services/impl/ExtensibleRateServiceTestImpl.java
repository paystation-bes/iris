package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.ExtensibleRate;
import com.digitalpaytech.service.ExtensibleRateService;

@Service("extensibleRateServiceTest")
public class ExtensibleRateServiceTestImpl implements ExtensibleRateService {
    
    @Override
    public final ExtensibleRate findByNameLocationIdAndUnifiedRateId(final String name, final Integer locationId, final Integer unifiedRateId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Collection<ExtensibleRate> getCurrentExtensibleRate(final Integer locationId, final Date targetDateLocal) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List getEmsRatesForSMSMessage(final Date localExpiryDate, final Date localMaxExtendedDate, final Integer locationId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List getWeekendEmsRatesForSMSMessage(final Date localExpiryDate, final Date localMaxExtendedDate, final Integer locationId) {
        // TODO Auto-generated method stub
        return null;
    }
}
