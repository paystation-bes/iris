package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.RoutePOS;
import com.digitalpaytech.service.RouteService;

@Service("routeServiceTest")
//Checkstyle: Test classes to be extended in unit tests
//PMD: Domain object has too many columns
@SuppressWarnings({ "checkstyle:designforextension" })
public class RouteServiceTestImpl implements RouteService {
    
    @Override
    public Collection<Route> findRoutesByCustomerId(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Collection<Route> findRoutesByCustomerIdAndName(final int customerId, final String name) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Collection<Route> findRoutesByCustomerIdAndNameUpperCase(final int customerId, final String name) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Route findRouteById(final Integer id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Route findRouteByIdAndEvict(final Integer id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void saveRouteAndRoutePOS(final Route route) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateRouteAndRoutePOS(final Route route) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public List<Paystation> findPayStationsByRouteId(final int routeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<PointOfSale> findPointOfSalesByRouteId(final int routeId, final boolean includeArchived) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<RoutePOS> findRoutePOSesByPointOfSaleId(final Integer pointOfSaleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<Route> findRoutesByPointOfSaleId(final int pointOfSaleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<Route> findRoutesByPointOfSaleIdAndRouteTypeId(final int pointOfSaleId, final int routeTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void deleteRoute(final Integer routeId) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public RoutePOS findRoutePOSByIdAndPointOfSaleId(final Integer routeId, final Integer pointOfSaleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Collection<Route> findRoutesByCustomerIdAndRouteTypeId(final int customerId, final int routeTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<RoutePOS> findRoutePOSesByPointOfSaleIdAndRouteTypeId(final Integer pointOfSaleId, final Integer routeTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<Route> findRoutesWithPointOfSalesByCustomerId(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<Route> findRoutesWithPointOfSalesByCustomerIdAndRouteType(final Integer customerId, final Integer routeTypeId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Integer> findPointOfSaleIdsByRouteId(int routeId, boolean includeArchived) {
        // TODO Auto-generated method stub
        return null;
    }
}
