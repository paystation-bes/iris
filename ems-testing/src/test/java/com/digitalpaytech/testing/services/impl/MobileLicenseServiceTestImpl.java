package com.digitalpaytech.testing.services.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.MobileApplicationType;
import com.digitalpaytech.domain.MobileLicense;
import com.digitalpaytech.domain.MobileLicenseStatusType;
import com.digitalpaytech.dto.MobileLicenseInfo;
import com.digitalpaytech.dto.customeradmin.MobileDeviceInfo;
import com.digitalpaytech.service.MobileLicenseService;

@Service("mobileLicenseServiceTest")
public class MobileLicenseServiceTestImpl implements MobileLicenseService {
    
    @Override
    public final MobileLicense findMobileLicense(final int mobileLicenseId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<MobileLicense> findMobileLicenseByCustomer(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void saveOrUpdateMobileLicense(final MobileLicense mobileLicense) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void deleteMobileLicense(final MobileLicense mobileLicense) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final MobileLicense findMobileLicenseByCustomerAndApplicationAndDevice(final int customerId, final int applicationId, final int deviceId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final MobileLicense findAvailableMobileLicenseByCustomerAndApplication(final int customerId, final int applicationId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final MobileLicenseStatusType findMobileLicenseStatusTypeById(final int id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final MobileLicenseStatusType findMobileLicenseStatusTypeByName(final String name) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final MobileLicense findProvisionedMobileLicenseByCustomerAndApplicationAndDevice(final int customerId, final int applicationId,
        final int deviceId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<MobileLicense> findProvisionedMobileLicenseByCustomerAndApplication(final int customerId, final int applicationId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<MobileLicense> findProvisionedMobileLicensesByCustomerAndDevice(final int customerId, final int deviceId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<MobileLicense> findProvisionedMobileLicensesAndActiveTokensByCustomerIdAndDeviceId(final int customerId, final int deviceId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<MobileApplicationType> getMobileAppsForCustomer(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<MobileLicense> findMobileLicenseByCustomerAndApplication(final int customerId, final int applicationId,
        final Integer pageNumber, final Integer pageSize) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<MobileLicenseInfo> findMobileLicenseInfoForCustomerAndApplication(final Integer customerId, final Integer applicationId,
        final String timeZone) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void updateLicenseCount(final Integer customerId, final Integer applicationId, final int count) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final long countProvisionedLicensesForCustomerAndType(final Integer customerId, final Integer applicationId) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final long countTotalLicensesForCustomerAndType(final Integer customerId, final Integer applicationId) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final List<MobileDeviceInfo> findMobileDevicesByCustomerAndApplication(final Integer customerId, final Integer applicationId,
        final Integer pageNumber, final Integer pageSize) {
        // TODO Auto-generated method stub
        return null;
    }
}
