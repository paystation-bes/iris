package com.digitalpaytech.testing.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.EventDeviceType;
import com.digitalpaytech.service.EventDeviceTypeService;

@Service("eventDeviceTypeServiceTest")
public class EventDeviceTypeServiceTestImpl implements EventDeviceTypeService {
    
    @Override
    public final List<EventDeviceType> loadAll() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Map<Integer, EventDeviceType> getEventDeviceTypesMap() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final String getText(final int eventDeviceTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final String getTextNoSpace(final int eventDeviceTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final EventDeviceType findEventDeviceType(final String name) {
        // TODO Auto-generated method stub
        return null;
    }
}
