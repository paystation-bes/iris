package com.digitalpaytech.testing.services.impl;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.ActivityLog;
import com.digitalpaytech.domain.ActivityType;
import com.digitalpaytech.dto.customeradmin.ActivityLogDetail;
import com.digitalpaytech.service.ActivityLogService;

@Service("activityLogServiceTest")
public class ActivityLogServiceTestImpl implements ActivityLogService {
    
    @Override
    public final Collection<ActivityLog> findActivityLogsByUserAccountId(final Integer userAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ActivityLog> findActivityLogWithCriteria(final Calendar start, final Calendar end, final List<Integer> userIds,
        final List<Integer> logTypes, final String order, final String column, final int page, final int numOfRecord) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ActivityType findActivityTypeByActivityId(final int activityTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ActivityType> findActivityTypesByParentActivityTypeId(final Integer parentActivityTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ActivityType> findAllChildActivityTypes() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ActivityLogDetail getActivityLogDetailsByActivityLogId(final String activityLogId) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
