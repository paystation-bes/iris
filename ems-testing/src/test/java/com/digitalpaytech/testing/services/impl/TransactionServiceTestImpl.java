package com.digitalpaytech.testing.services.impl;

import java.util.List;
import java.util.Properties;

import org.springframework.stereotype.Service;

import com.digitalpaytech.data.TransactionData;
import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.domain.CustomerCard;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.PurchaseCollectionCancelled;
import com.digitalpaytech.dto.EmbeddedTxObject;
import com.digitalpaytech.dto.paystation.TransactionDto;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.DuplicateTransactionRequestException;
import com.digitalpaytech.exception.PaystationCommunicationException;
import com.digitalpaytech.service.TransactionService;

@Service("transactionServiceTest")
public class TransactionServiceTestImpl implements TransactionService {
    
    @Override
    public final List<Object> processTransactionWithSmsAlert(final TransactionData transactionData, final EmbeddedTxObject embeddedTxObject)
        throws CryptoException {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Properties getMappingProperties() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int getMappingPropertiesValue(final String key) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public void createTransaction(final Permit permit, final Purchase purchase, final PaymentCard paymentCard,
        final ProcessorTransaction processorTransaction) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void cancelTransaction(final Permit permit, final Purchase purchase, final PaymentCard paymentCard,
        final ProcessorTransaction processorTransaction, final CustomerCard customerCard, final Coupon coupon,
        final PurchaseCollectionCancelled purchaseCollectionCancelled) throws PaystationCommunicationException {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public List<Object> processTransaction(TransactionData transactionData, boolean isManualSave, TransactionDto transDto,
        final EmbeddedTxObject embeddedTxObject) throws DuplicateTransactionRequestException, CryptoException {
        // TODO Auto-generated method stub
        return null;
    }
    
}
