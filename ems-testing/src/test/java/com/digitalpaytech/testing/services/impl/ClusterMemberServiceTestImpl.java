package com.digitalpaytech.testing.services.impl;

import java.util.Collection;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.Cluster;
import com.digitalpaytech.service.ClusterMemberService;

@Service("clusterMemberServiceTest")
public class ClusterMemberServiceTestImpl implements ClusterMemberService {

    @Override
    public final String getClusterName() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public final boolean isClusterMember(final String name) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public final boolean isClusterMemberHost(final String hostname) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public final Collection<Cluster> getAllClusterMembers() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public final int getClusterId() {
        // TODO Auto-generated method stub
        return 0;
    }
}
