package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CustomerCard;
import com.digitalpaytech.dto.BatchProcessStatus;
import com.digitalpaytech.dto.customeradmin.CustomerCardSearchCriteria;
import com.digitalpaytech.service.BatchProcessingException;
import com.digitalpaytech.service.CustomerCardService;

@Service("customerCardServiceTest")
public class CustomerCardServiceTestImpl implements CustomerCardService {
    
    @Override
    public final CustomerCard getCustomerCard(final int customerCardId) {
        
        return null;
    }
    
    @Override
    public final CustomerCard findCustomerCardByCustomerIdAndCardNumber(final int customerId, final String cardNumber) {
        
        return null;
    }
    
    @Override
    public final List<CustomerCard> findCustomerCardByConsumerId(final int consumerId) {
        
        return null;
    }
    
    @Override
    public final int countByCustomerId(final Integer customerId) {
        
        return 0;
    }
    
    @Override
    public void createCustomerCard(final CustomerCard customerCard) {
        
    }
    
    @Override
    public void updateCustomerCard(final CustomerCard customerCard) {
        
    }
    
    @Override
    public void deleteCustomerCard(final CustomerCard customerCard) {
        
    }
    
    @Override
    public final Future<Integer> deleteCustomerCardsByCustomerIdAsync(final int customerId, final String statusKey) {
        
        return null;
    }
    
    @Override
    public final int deleteCustomerCardsByCustomerId(final int customerId, final String statusKey) throws BatchProcessingException {
        
        return 0;
    }
    
    @Override
    public final int deleteCustomerCardsPaginated(final Integer customerId, final Date maxLastUpdate, final Map<String, Object> context) {
        
        return 0;
    }
    
    @Override
    public final int deleteCustomerCardsByCustomerIdAndCardNumber(final int customerId, final String cardNumber) {
        
        return 0;
    }
    
    @Override
    public final int findTotalCustomerCardsByCustomerId(final int customerId) {
        
        return 0;
    }
    
    @Override
    public final int findTotalCustomerCardsByCustomerIdAndCardNumber(final int customerId, final String cardNumber) {
        
        return 0;
    }
    
    @Override
    public final Future<Integer> processBatchAddCustomerCardsAsync(final String statusKey, final int customerId,
        final Collection<CustomerCard> customerCards, final String mode, final int userId) throws BatchProcessingException {
        
        return null;
    }
    
    @Override
    public final int processBatchAddCustomerCardsWithStatus(final String statusKey, final int customerId,
        final Collection<CustomerCard> customerCards, final String mode, final int userId) throws BatchProcessingException {
        
        return 0;
    }
    
    @Override
    public final int processBatchAddCustomerCards(final int customerId, final Collection<CustomerCard> customerCards, final String mode,
        final int userId, final BatchProcessStatus status) throws BatchProcessingException {
        
        return 0;
    }
    
    @Override
    public final int findRecordPage(final Integer customerCardId, final CustomerCardSearchCriteria criteria) {
        
        return 0;
    }
    
    @Override
    public final List<CustomerCard> findCustomerCard(final CustomerCardSearchCriteria criteria) {
        
        return null;
    }
    
    @Override
    public final List<CustomerCard> findCustomerCardByCustomerId(final int customerId) {
        
        return null;
    }
    
    @Override
    public final List<CustomerCard> findByCustomerIdAndCardIds(final Integer customerId, final Collection<Integer> cardIds) {
        
        return null;
    }
    
    @Override
    public final int countByCustomerCardType(final Collection<Integer> customerCardTypeIds) {
        
        return 0;
    }
}
