package com.digitalpaytech.testing.services.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.CustomerRole;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.service.RoleService;

@Component("roleServiceTest")
public class RoleServiceTestImpl implements RoleService {
    
    @Override
    public void saveRoleAndRolePermission(final Role role) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateRoleAndRolePermission(final Role role) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final Role findRoleByRoleIdAndEvict(final Integer roleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void updateRole(final Role role) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void deleteRole(final Role role) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final Role findRoleByName(final Integer customerId, final String roleName) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<UserAccount> findUserAccountByRoleId(final Integer roleId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void saveCustomerRoles(final Set<CustomerRole> customerRoles) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public Map<Integer, Boolean> findNumberOfUserRolesPerRoleIdByCustomerId(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
