package com.digitalpaytech.testing.services.impl;

import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.service.CustomerSubscriptionService;

@Service("customerSubscriptionServiceTest")
public class CustomerSubscriptionServiceTestImpl implements CustomerSubscriptionService {
    
    @Override
    public final List<CustomerSubscription> findCustomerSubscriptionsByCustomerId(final int customerId, final boolean cacheable) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Set<CustomerSubscription> findByCustomerId(final Integer customerId, final boolean cacheable) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<CustomerSubscription> findActiveCustomerSubscriptionByCustomerId(final int customerId, final boolean cacheable) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void updateLicenseUsedForCustomer(final Integer customerId, final Integer subscriptionTypeId, final int licenseUsed) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateLicenseCountAndUsedForCustomer(final Integer customerId, final Integer subscriptionTypeId, final int licenseCount,
        final int licenseUsed, final int userAccountId) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateLicenseCountForCustomer(final Integer customerId, final Integer subscriptionTypeId, final int licenseCount) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final long countMobileLicensesForCustomer(final Integer customerId) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final boolean hasOneOfSubscriptions(final Integer customerId, final Integer... subscriptionTypeId) {
        // TODO Auto-generated method stub
        return false;
    }
}
