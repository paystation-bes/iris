package com.digitalpaytech.testing.services.impl;

import javax.mail.Address;

import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.CustomerAgreement;
import com.digitalpaytech.service.MailerService;

@Component("mailerServiceTest")
public class MailerServiceTestImpl implements MailerService {
    
    @Override
    public void sendMessage(final String toAddresses, final String content) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void sendMessage(final String toAddresses, final Object content, final String type) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void sendMessage(final String toAddresses, final String ccAddresses, final String bccAddresses, final Object content, final String type) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void sendMessage(final Address fromAddress, final String toAddresses, final String subject, final Object content, final String type) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void sendMessage(final String toAddresses, final String subject, final Object content, final String type) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void sendMessage(final Address fromAddress, final String toAddresses, final String ccAddresses, final String bccAddresses,
        final String subject, final Object content, final String type) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void sendMessage(final Address fromAddress, final String toAddresses, final String ccAddresses, final String bccAddresses,
        final String subject, final Object content, final String type, final byte[] attachment, final String filename) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void sendMessage(final String toAddresses, final String subject, final Object content, final String type, final byte[] attachment,
        final String filename) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void sendAdminErrorAlert(final String subject, final String content, final Exception exception) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void sendAdminWarnAlert(final String subject, final String content, final Exception exception) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void sendAdminAlert(final String subject, final String content, final Exception exception) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void sendServiceAgreementAccepted(final CustomerAgreement customerAgreement, final String customerName) {
        // TODO Auto-generated method stub
        
    }
}
