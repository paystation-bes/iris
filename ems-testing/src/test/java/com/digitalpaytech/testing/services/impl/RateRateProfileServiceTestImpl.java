package com.digitalpaytech.testing.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.RateRateProfile;
import com.digitalpaytech.service.RateRateProfileService;

@Service("rateRateProfileServiceTest")
public class RateRateProfileServiceTestImpl implements RateRateProfileService {
    
    @Override
    public final List<RateRateProfile> findPublishedRatesByRateId(final Integer rateId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<RateRateProfile> findActiveRatesByRateProfileIdAndDateRange(final Integer rateProfileId, final Date startDate,
        final Date endDate, final boolean isSingleDay) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<RateRateProfile> findPublishedRatesByLocationIdAndDateRange(final Integer locationId, final Integer customerId,
        final Date startDate, final Date endDate) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final RateRateProfile findRateRateProfileById(final Integer rateRateProfileId) {
        // TODO Auto-generated method stub
        return null;
    }
}
