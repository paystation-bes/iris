package com.digitalpaytech.testing.services.impl;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.RateProfile;
import com.digitalpaytech.domain.RateProfileLocation;
import com.digitalpaytech.domain.RateRateProfile;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.service.RateProfileService;
import com.digitalpaytech.util.RandomKeyMapping;

@Service("rateProfileServiceTest")
public class RateProfileServiceTestImpl implements RateProfileService {
    
    @Override
    public final RateProfile findRateProfileById(final Integer rateProfileId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final RateProfile findRateProfileByCustomerIdAndPermitIssueTypeIdAndName(final Integer customerId, final Integer permitIssueTypeId,
        final String name) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<RateProfile> findRateProfilesByCustomerId(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<FilterDTO> findPublishedRateProfileFiltersByCustomerId(final Integer customerId, final RandomKeyMapping keyMapping) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<RateProfile> findRateProfilesByCustomerIdAndPermitIssueTypeId(final Integer customerId, final Integer permitIssueTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<RateProfile> findPagedRateProfileList(final Integer customerId, final Integer permitIssueTypeId, final Boolean isPublished,
        final Date maxUpdateTime, final int pageNumber) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void saveOrUpdateRateProfile(final RateProfile rateProfile, final Collection<RateRateProfile> rateRateProfileList,
        final Collection<RateProfileLocation> rateProfileLocationList) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void deleteRateProfile(final RateProfile rateProfile, final Integer userAccountId) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final List<RateProfile> searchRateProfiles(final Integer customerId, final String keyword) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int findRateProfilePage(final Integer rateProfileId, final Integer customerId, final Integer permitIssueTypeId,
        final Boolean isPublished, final Date maxUpdateTime) {
        // TODO Auto-generated method stub
        return 0;
    }
}
