package com.digitalpaytech.testing.services.impl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.dto.PosAlertStatusDetailSummary;
import com.digitalpaytech.service.PosAlertStatusService;

@Service("posAlertStatusServiceTest")
@SuppressWarnings("checkstyle:designforextension")
public class PosAlertStatusServiceTestImpl implements PosAlertStatusService {
    
    @Override
    public int getCommunicationAlertsSum() {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public int getCollectionAlertsSum() {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public int getPaystationAlertsSum() {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public Map<Integer, PosAlertStatusDetailSummary> findPosAlertDetailSummaryByPointOfSaleId(final int pointOfSaleId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void updateAlertCount(final PosEventCurrent posEventCurrent, final int originalSeverity, final boolean isSeverityRecalc) {
        // TODO Auto-generated method stub
        
    }
}
