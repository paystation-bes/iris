package com.digitalpaytech.testing.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.SubscriptionType;
import com.digitalpaytech.service.SubscriptionTypeService;
import com.digitalpaytech.util.WebCoreConstants;

@Service("subscriptionTypeServiceTest")
//Test data magic numbers not relevant
@SuppressWarnings("checkstyle:magicnumber")
public class SubscriptionTypeServiceTestImpl implements SubscriptionTypeService {
    
    @Override
    public final List<SubscriptionType> loadAll() {
        final List<SubscriptionType> subscriptionTypeList = new ArrayList<SubscriptionType>();
        subscriptionTypeList.add(new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_STANDARD_REPORT, "Standard Reports", true, false, 1,
                new Date(), 1));
        subscriptionTypeList.add(new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_ALERTS, "Alerts", true, false, 2, new Date(), 1));
        subscriptionTypeList.add(new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_REALTIME_CC_PROCESSING, "Real-Time Credit Card Processing",
                true, false, 3, new Date(), 1));
        subscriptionTypeList.add(new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_BATCH_CC_PROCESSING, "Batch Credit Card Processing", true,
                false, 4, new Date(), 1));
        subscriptionTypeList.add(new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_REALTIME_CAMPUS_CARD_PROCESSING,
                "Real-Time Campus Card Processing", true, false, 5, new Date(), 1));
        subscriptionTypeList.add(new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_COUPONS, "Coupons", true, false, 8, new Date(), 1));
        subscriptionTypeList.add(new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_PASSCARDS, "Passcards", true, false, 6, new Date(), 1));
        subscriptionTypeList.add(new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_SMART_CARDS, "Smart Cards", true, false, 7, new Date(), 1));
        subscriptionTypeList.add(new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_EXTEND_BY_PHONE, "Extend-By-Phone", true, false, 9,
                new Date(), 1));
        subscriptionTypeList.add(new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_API_READ, "Digital API: Read", true, false, 13,
                new Date(), 1));
        subscriptionTypeList.add(new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_API_WRITE, "Digital API: Write", true, false, 14,
                new Date(), 1));
        subscriptionTypeList.add(new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_3RD_PARTY_PAY_BY_CELL, "3rd Party Pay-By_cell Integration",
                true, false, 16, new Date(), 1));
        subscriptionTypeList.add(new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_COLLECT, "Mobile: Digital Collect", true, false, 10,
                new Date(), 1));
        subscriptionTypeList.add(new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_ONLINE_PAYSTATION_CONFIGURATION,
                "Online Pay Station Configuration", true, false, 17, new Date(), 1));
        subscriptionTypeList.add(new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_ONLINE_RATE_CONFIGURATION,
                "Online Rate Configuration", true, false, 18, new Date(), 1));
        subscriptionTypeList.add(new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_FLEX_INTEGRATION, "FLEX Integration", true, false, 11,
                new Date(), 1));
        subscriptionTypeList.add(new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_CASE_INTEGRATION, "AutoCount Integration", true, false, 12,
                new Date(), 1));
        subscriptionTypeList.add(new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_API_XCHANGE, "Digital API: XChange", true, true, 15,
                new Date(), 1));
        return subscriptionTypeList;
    }
    
    @Override
    public final SubscriptionType findById(final Integer id) {
        // TODO Auto-generated method stub
        return null;
    }
}
