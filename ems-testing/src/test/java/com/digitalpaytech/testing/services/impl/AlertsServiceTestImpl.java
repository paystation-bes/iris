package com.digitalpaytech.testing.services.impl;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.AlertThresholdType;
import com.digitalpaytech.domain.EventDefinition;
import com.digitalpaytech.domain.EventDeviceType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.dto.ActiveAlertSearchCriteria;
import com.digitalpaytech.dto.AlertMapEntry;
import com.digitalpaytech.dto.AlertSearchCriteria;
import com.digitalpaytech.dto.SummarySearchCriteria;
import com.digitalpaytech.dto.customeradmin.AlertInfoEntry;
import com.digitalpaytech.dto.customeradmin.AlertSummaryEntry;
import com.digitalpaytech.dto.customeradmin.CollectionSummaryEntry;
import com.digitalpaytech.service.AlertsService;

@Service("alertsServiceTest")
@SuppressWarnings("checkstyle:designforextension")
public class AlertsServiceTestImpl implements AlertsService {
    
    @Override
    public EventDefinition findEventDefinitionByDeviceStatusActionSeverityId(final int deviceTypeId, final int statusTypeId, final int actionTypeId,
        final int severityTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public EventDefinition findEventDefinitionById(final int eventDefinitionId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<EventDeviceType> findAllEventDeviceTypes() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public EventDeviceType findEventDeviceTypeById(final int deviceTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<EventSeverityType> findAllEventSeverityTypes() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<AlertMapEntry> findPointOfSalesWithAlertMap(final ActiveAlertSearchCriteria criteria) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Integer locatePageContainsPointOfSaleWithAlert(final ActiveAlertSearchCriteria criteria) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<AlertMapEntry> findPointOfSalesWithAlertDetail(final ActiveAlertSearchCriteria criteria, final String timeZone) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<AlertMapEntry> findActivePointOfSalesByRecentCollections(final ActiveAlertSearchCriteria criteria, final Object[] collectionTypes,
        final Date minDate, final Date maxDate) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<CollectionSummaryEntry> findPointOfSalesWithBalanceAndAlertSeverity(final SummarySearchCriteria criteria, final String timeZone) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<AlertSummaryEntry> findPointOfSalesForAlertSummary(final SummarySearchCriteria criteria, final String timeZone) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<EventDeviceType> findAllEventDeviceTypesExceptPCM() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<AlertThresholdType> findAlertThresholdTypeByAlertClassTypeId(final Byte alertClassTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<AlertInfoEntry> findHistory(final AlertSearchCriteria criteria, final TimeZone timezone, final String sortItem,
        final String sortOrder) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<AlertMapEntry> findPointOfSaleWithClearedPSAlert(final AlertSearchCriteria criteria, final TimeZone timezone) {
        // TODO Auto-generated method stub
        return null;
    }
}
