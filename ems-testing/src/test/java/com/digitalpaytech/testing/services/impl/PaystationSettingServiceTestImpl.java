package com.digitalpaytech.testing.services.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.PaystationSetting;
import com.digitalpaytech.domain.SettingsFile;
import com.digitalpaytech.domain.SettingsFileContent;
import com.digitalpaytech.dto.customeradmin.CustomerAdminPayStationSettingInfo;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.service.PaystationSettingService;
import com.digitalpaytech.util.RandomKeyMapping;

@Service("paystationSettingServiceTest")
public class PaystationSettingServiceTestImpl implements PaystationSettingService {
    
    @Override
    public final PaystationSetting findPaystationSetting(final int id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<PaystationSetting> findPaystationSettingsByCustomerId(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final PaystationSetting findByCustomerIdAndLotName(final Customer customer, final String name) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final SettingsFile findSettingsFile(final Integer id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final SettingsFileContent findSettingsFileContentBySettingsFileId(final Integer settingsFile) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void processSettingsFileUpload(final Customer customer, final String fileFullPath, final SettingsFile newSetting,
        final List<String> serialNumbers) throws InvalidDataException {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final List<SettingsFile> findSettingsFileByCustomerId(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<SettingsFile> findSettingsFileByCustomerIdOrderByUploadGmt(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final SettingsFile findSettingsFileByCustomerIdAndName(final Integer customerId, final String name) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final SettingsFile findSettingsFileByCustomerIdAndUniqueIdentifier(final Integer customerId, final Integer uniqueIdentifier) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final boolean isPaystationSettingWaitingForUpdate(final Integer paystationSettingId) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void deleteSettingsFileContent(final SettingsFile settingsFile) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final List<CustomerAdminPayStationSettingInfo> findCustomerAdminPayStationSettingInfoListByCustomerId(final Integer customerId,
        final String timezone, final RandomKeyMapping keyMapping) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerAdminPayStationSettingInfo findCustomerAdminPayStationSettingInfoBySettingFileId(final Integer settingFileId,
        final String timezone, final RandomKeyMapping keyMapping) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final CustomerAdminPayStationSettingInfo findNoSettingCustomerAdminPayStationSettingInfoByCustomerId(final Integer customerId,
        final String timezone, final RandomKeyMapping keyMapping) {
        // TODO Auto-generated method stub
        return null;
    }
}
