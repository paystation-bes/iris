package com.digitalpaytech.testing.services.systemadmin.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.digitalpaytech.client.dto.FirmwareConfig;
import com.digitalpaytech.client.dto.fms.DeviceUser;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantPOS;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.service.systemadmin.SystemAdminService;

@Service("systemAdminServiceTest")
@SuppressWarnings("checkstyle:designforextension")
public class SystemAdminServiceTestImpl implements SystemAdminService {
    
    @Override
    public List<Customer> findAllNonDPTCustomers() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<MerchantAccount> findValidMerchantAccountsByCustomerId(final Integer id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public MerchantAccount findMerchantAccountById(final Integer merchantAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public void saveOrUpdateMerchantPos(final MerchantPOS merchantPos) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public CardType findCardTypeById(final int cardTypeId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public int createCustomer(final Customer customer, final UserAccount userAccount, final String timeZone) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public boolean updateCustomer(final Customer customer, final UserAccount userAccount) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public int createPointOfSale(final String serialNumber, final Integer customerId, final UserAccount userAccount) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public void updateMerchantPos(final MerchantPOS merchantPos) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public FirmwareConfig getFirmwareConfig(String serialNumber) throws ApplicationException {
        return null;
    }
    
    @Override
    public int movePointOfSale(Integer posId, Customer oldCustomer, Customer newCustomer, UserAccount userAccount, Boolean isLinux) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public boolean updateUserInMitreId(PointOfSale pointOfSale, Boolean isActivated, DeviceUser deviceUser) throws InvalidDataException {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void deleteMitreIdAndTelemetryAndLCDMS(PointOfSale pointOfSale, Customer customer) throws InvalidDataException {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void addOrUpdateMitreIdAndTelemetryAndLCDMS(PointOfSale pos, Customer customer, Boolean isLinux, Boolean isActivated, String actionType)
        throws InvalidDataException {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateDeviceInLCDMS(PointOfSale pointOfSale, Customer customer, Boolean isDeleted) throws InvalidDataException {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public boolean recreateMitreIdAndTelemetryAndLCDMS(String oldSerialNumber, String newSerialNumber, PointOfSale pointOfSale, Customer customer,
        Boolean isActive) throws InvalidDataException {
        // TODO Auto-generated method stub
        return false;
    }
}
