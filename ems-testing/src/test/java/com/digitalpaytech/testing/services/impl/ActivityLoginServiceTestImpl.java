package com.digitalpaytech.testing.services.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.ActivityLogin;
import com.digitalpaytech.service.ActivityLoginService;

@Component("activityLoginServiceTest")
public class ActivityLoginServiceTestImpl implements ActivityLoginService {
    
    @Override
    public final Serializable saveActivityLogin(final ActivityLogin activityLogin) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int countSuccessfulLoginWithinLastMinute(final Date endTime) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final List<ActivityLogin> findInvalidActivityLoginByLastModifiedGMT(final String username, final int maxLockTimeMinutes) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int findCountInvalidActivityLoginByActivityGMT(final Integer userAccountId, final int loginResultTypeId, final int maxLockTimeMinutes) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final List<ActivityLogin> findAllActivityForUsageCalculation(final Integer customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Date findLastSuccessByUserAccountId(final Integer userAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
}
