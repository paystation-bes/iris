package com.digitalpaytech.testing.services.impl;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.springframework.stereotype.Service;

import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.dto.ReportSearchCriteria;
import com.digitalpaytech.dto.customeradmin.ReportQueueInfo.QueueInfo;
import com.digitalpaytech.service.ReportQueueService;
import com.digitalpaytech.util.RandomKeyMapping;

@Service("reportQueueServiceTest")
public class ReportQueueServiceTestImpl implements ReportQueueService {
    
    @Override
    public void saveReportQueue(final ReportQueue reportQueue) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public void updateReportQueue(final ReportQueue reportQueue) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public final List<ReportQueue> findAll() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ReportQueue> findQueuedReports() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final ReportQueue findQueuedReportWithLock(final String clusterName) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ReportQueue> findQueuedReportsByUserAccountId(final int userAccountId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final List<ReportQueue> findQueuedReportsByCustomerId(final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final String fixStalledReports(final String clusterName, final Date idleTime) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final boolean cancelReport(final int reportQueueId) {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public final ReportQueue getReportQueue(final int id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final int getLaunchedReportCount() {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final int getScheduledReportCount() {
        // TODO Auto-generated method stub
        return 0;
    }
    
    @Override
    public final ReportQueue findRecentlyQueuedReport(final int reportDefinitionId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public final List<ReportQueue> findReportQueueByCustomerIdAndUserAccountId(final int userAccountId, final int customerId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public final List<QueueInfo> findQueuedReports(final ReportSearchCriteria criteria, final RandomKeyMapping keyMapping, final TimeZone timeZone) {
        // TODO Auto-generated method stub
        return null;
    }
}
