var REPO_RELEASE = "http://bitbucket.digitalpaytech.com/scm/rel/iris-release.git";
var REPO_REMOTE_TRACKING = "origin";

var APP_FOLDER = "./webapps/ROOT/";
var WEB_FOLDER = "./web/";

var SCRIPT_FOLDER = "WEB-INF/";

var DEFAULT_CONF_FILE = "deploy-default.json";

var CMD_UTILS = "deploy-utils.js";
var CMD_APP_CONFIGURE = "deploy-app-configure.js";
var CMD_WEB_CONFIGURE = "deploy-web-configure.js";

var DEPENDENCIES = "deploy-scripts.dependencies";

var PATH_BACKUP = "./.deploybak/";
	
var PATH_TOMCAT_BIN = "./bin/";
var PATH_TOMCAT_CTX = "./conf/context.xml";

var SCRIPTS = [ CMD_APP_CONFIGURE, CMD_WEB_CONFIGURE, CMD_UTILS, DEPENDENCIES, DEFAULT_CONF_FILE, "iris.js" ];

var os = require("os");
var fs = require("fs");
var path = require("path");
var childproc = require("child_process");

var GIT = true;
var RESET_ONLY = false;

/* Utilities */

function _isWindows() {
	return /^win/.test(process.platform);
}

function _isGitRepo(folder) {
	return (!GIT) || fs.existsSync(folder + "/.git");
}

function _isBinaryRepo() {
	return fs.existsSync(CMD_APP_CONFIGURE);
}

function listAllFiles(result, rootFolder, relDir) {
	var files = fs.readdirSync(rootFolder + relDir);
	var idx = files.length;
	while (--idx >= 0) {
		var file = files[idx];
		var relFile = relDir + "/" + file;
		var fullPath = rootFolder + relFile;
		if (!fs.statSync(fullPath).isDirectory()) {
			result.push(relFile);
		} else {
			listAllFiles(result, rootFolder, relFile);
		}
	}
	
	return result;
}

function restoreBackup(irisFolder) {
	if (fs.existsSync(PATH_BACKUP)) {
		listAllFiles([], PATH_BACKUP, ".").forEach(function(file, index) {
			var bak = PATH_BACKUP + "/" + file;
			var src = irisFolder + "/" + file;
			
			if (fs.existsSync(src)) {
				console.log("Deleting " + src + "...");
				fs.unlinkSync(src);
			}
			
			console.log("Restoring " + bak + " to original folder " + src);
			fs.renameSync(bak, src);
		});
	}
}

function createContext(conf) {
	var ctx = {
		"conf": conf,
		"irisFolder": ""
	};
	
	if (conf.irisFolder) {
		ctx.irisFolder = conf.irisFolder;
	}
	
	return ctx;
}

/* APP */

function refreshAppServer(ctx, callback) {
	_restoreAppConfigurations(ctx.conf, function() {
		_refreshBinaries(APP_FOLDER, ctx.branchInfo, function() {
			if (RESET_ONLY) {
				callback();
			} else {
				_copyScripts(APP_FOLDER, function() {
					_installDependencies(APP_FOLDER, function() {
						_configureApp(ctx.conf, callback);
					});
				});
			}
		});
	});
}

function _stopAppServer(callback) {
	var stopCmd;
	if (_isWindows()) {
		stopCmd = "shutdown.bat";
	} else {
		stopCmd = "./daemon.sh";
		if(fs.existsSync(stopCmd)) {
			stopCmd += " stop";
		} else {
			stopCmd = "./shutdown.sh -force";
		}
	}
	
	childproc.execFile(stopCmd, null, { "cwd": PATH_TOMCAT_BIN }, function(err, stdOut, stdErr) {
		if(err) {
			console.log("Failed to stop tomcat: " + err);
		} else {
			console.log("Tomcat stopped!");
		}

		if(callback) {
			callback();
		}
	});
}

function _restoreMemcachedConfiguration() {
	var backupCtxPath = PATH_TOMCAT_CTX + ".original";
	
	if (fs.existsSync(backupCtxPath)) {
		if (fs.existsSync(PATH_TOMCAT_CTX)) {
			console.log("Deleting \"" + PATH_TOMCAT_CTX + "\"...");
			fs.unlinkSync(PATH_TOMCAT_CTX);
		}
		
		fs.renameSync(backupCtxPath, PATH_TOMCAT_CTX);
	} else {
		console.log("WARNING: Skip restoring \"" + backupCtxPath + "\" because it does not existed!");
	}
}

function _configureApp(conf, callback) {
	console.log("Configuring Iris App...");

	var task;
	try {
		task = require("./" + CMD_APP_CONFIGURE);
	} catch (error) {
		// DO NOTHING.
		console.log(error);
	}

	if (task && (typeof task.exec === "function")) {
		task.exec(conf, callback);
	} else {
		console.log("WARNING: Skipped application configure because the script does not existed !");
		callback();
	}
}

function _restoreAppConfigurations(conf, callback) {
	console.log("Restoring Iris App Server Configuration...");
	_stopAppServer(function() {
		_restoreMemcachedConfiguration();
		restoreBackup((conf && conf.irisFolder) ? conf.irisFolder : "");
		if (typeof callback === "function") {
			callback();
		}
	});
}

/* WEB */

function refreshWebServer(ctx, callback) {
	_restoreWebConfigurations(ctx.conf, function() {
		_refreshBinaries(WEB_FOLDER, ctx.branchInfo, function() {
			if (RESET_ONLY) {
				callback();
			} else {
				_copyScripts(WEB_FOLDER, function() {
					_installDependencies(WEB_FOLDER, function() {
						_configureWeb(ctx.conf, callback);
					});
				});
			}
		});
	});
}

function _configureWeb(conf, callback) {
	console.log("Configuring Iris Web...");

	var task;
	try {
		task = require("./" + CMD_WEB_CONFIGURE);
	} catch (error) {
		// DO NOTHING.
		console.log(error);
	}

	if (task && (typeof task.exec === "function")) {
		task.exec(conf, callback);
	} else {
		console.log("WARNING: Skipped web configure because the script does not existed !");
		callback();
	}
}

function _stopWebServer(ctx, callback) {
	childproc.execFile("apachectl", [ "stop" ], { "cwd" : ctx.conf["cmd.apache.path"] }, function(err, stdOut, stdErr) {
		if(err) {
			console.log("Failed to stop Apache: " + err);
		} else {
			console.log("Apache stopped!");
		}
		
		console.log("Apache stopped!");
		
		if(typeof callback == "function") {
			callback();
		}
	});
}

function _restoreModJK(ctx) {
	var src = ctx.conf["modjk.conf.file"];
	var bak = src + ".template";
	
	if (fs.existsSync(src)) {
		console.log("Skip restoring to '" + src + "' because it's already existed.");
	} else {
		console.log("Restoring '" + bak + "' to '" + src + "'");
		fs.renameSync(bak, src);
	}
}

function _restoreWebConfigurations(conf, callback) {
	console.log("Restoring Iris Web Server Configuration...");
	var ctx = createContext(conf);
	//_stopWebServer(ctx, function() {
		restoreBackup(ctx);
		_restoreModJK(ctx);
		if(typeof callback == "function") {
			callback();
		}
	//});
}

/* GIT */

function _getGitCacheCommand() {
	var cacheCmd;
	if (_isWindows()) {
		cacheCmd = "wincred";
	} else {
		cacheCmd = "cache";
	}
	
	return cacheCmd;
}

function _fetch(folder, callback) {
	if (!GIT) {
		console.log("Skipping '_fetch' because GIT = false!");
		callback();
	} else {
		var userName = "root";
		var userEmail = userName + "@" + os.hostname();
		
		childproc.exec("git remote rm " + REPO_REMOTE_TRACKING, { "cwd": folder }, function(err, stdOut, stdErr) {
			console.log(stdOut);
			childproc.exec("git remote add " + REPO_REMOTE_TRACKING + " " + REPO_RELEASE + "", { "cwd": folder }, function(err, stdOut, stdErr) {
				console.log(stdOut);
				if (err) {
					throw err;
				}
				
				childproc.exec("git config user.email \"" + userEmail + "\"", { "cwd": folder }, function(err, stdOut, stdErr) {
					if (err) {
						throw err;
					}
					
					childproc.exec("git config user.name \"" + userName + "\"", { "cwd": folder }, function(err, stdOut, stdErr) {
						if (err) {
							throw err;
						}
						
						childproc.exec("git config credential.helper " + _getGitCacheCommand(), { "cwd": folder }, function(err, stdOut, stdErr) {
							console.log(stdOut);
							if (err) {
								throw err;
							}
							
							childproc.exec("git fetch --all", { "cwd": folder }, function(err, stdOut, stdErr) {
								if (err) {
									throw err;
								}
		
								console.log(stdOut);
								if (typeof callback === "function") {
									callback();
								}
							});
						});
					});
				});
			});
		});
	}
}

function _clone(folder, callback) {
	if (!GIT) {
		console.log("Skipping '_clone' because GIT = false!");
		callback();
	} else {
		childproc.exec("git init", { "cwd": folder }, function(err, stdOut, stdErr) {
			if (err) {
				throw err;
			} else {
				_fetch(folder, callback);
			}
		});
	}
}

function _refreshBinaries(folder, branchInfo, callback) {
	if (!GIT) {
		console.log("Skipping '_refreshBinaries' because GIT = false!");
		callback();
	} else {
		childproc.exec("git branch", { "cwd": folder }, function(err, stdOut, stdErr) {
			if (err) {
				throw err;
			} else {
				var currBranch = stdOut.match(/^\* (.*)$/m);
				if (currBranch) {
					currBranch = currBranch[1];
				}
				
				console.log("We are currently on " + currBranch);
				
				if ((!branchInfo.version) && (currBranch) && (branchInfo.name == currBranch)) {
					_pull(folder, branchInfo, callback);
				} else {
					var checkoutCmd;
					if (branchInfo.version) {
						console.log("Switching to version: " + branchInfo.version);
						checkoutCmd = "git checkout tags/" + branchInfo.version;
					} else if (stdOut.indexOf(branchInfo.name) < 0) {
						console.log("Checking out remote branch: " + branchInfo.name);
						checkoutCmd = "git checkout -b " + branchInfo.name + " --track " + REPO_REMOTE_TRACKING + "/" + branchInfo.name;
					} else {
						console.log("Checking out local branch: " + branchInfo.name);
						checkoutCmd = "git checkout " + branchInfo.name;
					}
					
					childproc.exec(checkoutCmd, { "cwd": folder }, function(err, stdOut, stdErr) {
						if (err) {
							throw err;
						} else {
							console.log(stdOut);
							if (branchInfo.version) {
								callback();
							} else {
								_pull(folder, branchInfo, callback);
							}
						}
					});
				}
			}
		});
	}
}

function _copyFile(src, target, callback) {
	var inStream = fs.createReadStream(src, { "flags": "r" });
	inStream.on("error", function(err) {
		console.log("ERROR: Cannot open read stream from file '" + src + "': " + err);
	});
	
	var outStream = fs.createWriteStream(target, { "flags": "w" });
	outStream.on("error", function(err) {
		console.log("ERROR: Cannot open write stream from file '" + target + "': " + err);
	});
	
	outStream.on("close", function() {
		callback();
	});
	
	inStream.pipe(outStream);
}

function _copyScripts(folder, callback) {
	var uncopied = SCRIPTS.length;
	var continueWhenDone = function() {
		--uncopied;
		if (uncopied == 0) {
			callback();
		}
	};
	
	var idx = SCRIPTS.length;
	while (--idx >= 0) {
		var src = folder + SCRIPT_FOLDER + SCRIPTS[idx];
		var target = "./" + SCRIPTS[idx];
		
		if (fs.existsSync(src)) {
			if (fs.existsSync(target)) {
				console.log("Deleting \"" + target + "\"...");
				fs.unlinkSync(target);
			}
			
			_copyFile(src, target, continueWhenDone);
		} else {
			console.log("WARNING: Skip copying script \"" + src + "\" because it does not existed!");
		}
	}
}

function _pull(folder, branchInfo, callback) {
	if (!GIT) {
		console.log("Skipping '_pull' because GIT = false!");
		callback();
	} else {
		childproc.exec("git pull --no-ff " + REPO_REMOTE_TRACKING + " " + branchInfo.name, { "cwd": folder }, function(err, stdOut, stdErr) {
			if (err) {
				throw err;
			} else {
				console.log(stdOut);
				callback();
			}
		});
	}
}

function _buildBranchInfo(argv) {
	var branchInfo = {
		"deployWebOnly": false,
		"deployAppOnly": false
	};
	
	var i = 0;
	while (i < argv.length) {
		var key = argv[i++];
		if (key.charAt(0) != '-') {
			console.log("WARNING: Unrecognize option: " + key);
		} else {
			if (i < argv.length) {
				var val = argv[i++];
				if (key == "-b") {
					branchInfo.name = "dev/" + (new Buffer(val)).toString("base64");
				} else if (key == "-v") {
					branchInfo.version = val;
				} else if (key == "-d") {
					if (val == "web") {
						branchInfo.deployWebOnly = true;
					}
					if (val == "app") {
						branchInfo.deployAppOnly = true;
					}
				} else {
					console.log("WARNING: Unrecognize option: " + key);
				}
			}
		}
	}
	
	if (!branchInfo.version && !branchInfo.name) {
		branchInfo.name = "master";
	}
	
	return branchInfo;
}

function _performNPMInstall(dependencies, curIdx, callback) {
	if (curIdx >= dependencies.length) {
		callback();
	} else {
		childproc.exec("npm ls " + dependencies[curIdx] + " -parseable=true ", null, function(err, stdOut, stdErr) {
			if (!err) {
				console.log("Module " + dependencies[curIdx] + " was already installed.");
				_performNPMInstall(dependencies, curIdx + 1, callback);
			} else {
				console.log("Installing module " + dependencies[curIdx] + "...");
				childproc.exec("npm install " + dependencies[curIdx], null, function(err, stdOut, stdErr) {
					if (err) {
						throw err;
					} else {
						console.log(stdOut);
						_performNPMInstall(dependencies, curIdx + 1, callback);
					}
				});
			}
		});
	}
}

function _installDependencies(folder, callback) {
	fs.readFile(DEPENDENCIES, "UTF-8", function(err, contentStr) {
		if(err) {
			throw new Error("Error reading dependencies" + err.message);
		}
		else {
			_performNPMInstall(contentStr.split("\n"), 0, callback);
		}
	});
}

function _printSuccess() {
	console.log("SUCCESS !!!");
}

function main(confFile, argv) {
	fs.readFile(confFile, "UTF-8", function(err, jsonStr) {
		if(err) {
			throw new Error("Error reading default configurations '" + confFile + "': " + err.message);
		}
		else {
			var conf = null;
			try {
				conf = JSON.parse(jsonStr);
			} catch (error) {
				throw new Error("Invalid jsON string: " + jsonStr);
			}
			
			var ctx = {
				"branchInfo": _buildBranchInfo(argv),
				"conf": conf
			};
			
			var performRefresh;
			if (ctx.branchInfo.deployWebOnly) {
				ctx.conf["irisFolder"] = WEB_FOLDER;
				performRefresh = function() {
					refreshWebServer(ctx, _printSuccess);
				};
			} else {
				ctx.conf["irisFolder"] = APP_FOLDER;
				ctx.conf["mode.staticContent"] = ctx.deployAppOnly;
				
				performRefresh = function() {
					refreshAppServer(ctx, _printSuccess);
				};
			}
			
			if (!_isGitRepo(ctx.conf.irisFolder)) {
				_clone(ctx.conf.irisFolder, performRefresh);
			} else {
				_fetch(ctx.conf.irisFolder, performRefresh);
			}
		}
	});
}

main(process.argv[2], process.argv.slice(3));