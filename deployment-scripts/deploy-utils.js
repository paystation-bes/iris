module.exports = {
	arrayToHash: arrayToHash,
	splitAndTrim: splitAndTrim,
	randomInt: randomInt,
	overrideHash: overrideHash,
	rmdirSync: rmdirSync,
	resolveLocalIP4s: resolveLocalIP4s,
	createTemplateObject: createTemplateObject,
	modifyStream: modifyStream,
	mergeTemplate: mergeTemplate,
	templateEscape: templateEscape,
	compileTemplate: compileTemplate,
	backup: backup,
	copyFile: copyFile,
	mkdirs: mkdirs
};

var os = require("os");
var fs = require("fs");

var PATH_BACKUP = "./.deploybak/";

/* Utils */

function arrayToHash(arr) {
	var hash = {};
	var idx = arr.length;
	while (--idx > -1) {
		hash[arr[idx]] = true;
	}
	
	return hash;
}

function splitAndTrim(src) {
	return (!src) ? [] : src.split(/\s/g).join("").split(",");
}

function randomInt(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low);
}

function overrideHash(original, extension, extensionPrefix) {
	var passthrough = false;
	var prefixLength = 0;
	if (extensionPrefix) {
		prefixLength = extensionPrefix.length;
		passthrough = prefixLength.length > 0;
	}
	
	for (var key in extension) {
		if (passthrough || (key.indexOf(extensionPrefix) == 0)) {
			var actualKey = (passthrough) ? key : key.substr(prefixLength);
			original[actualKey] = extension[key];
		}
	}
}


/* File */

function mkdirs(rootFolder, relPath) {
	var curDir = rootFolder + "/" + relPath;
	if (!fs.existsSync(curDir)) {
		var curIdx = relPath.lastIndexOf("/");
		if (curIdx > 0) {
			mkdirs(PATH_BACKUP, relPath.substring(0, curIdx));
		}
		
		fs.mkdirSync(curDir);
	}
}

function backup(rootFolder, relPath) {
	var src = rootFolder + relPath;
	var dest = PATH_BACKUP + relPath;
	
	var fileIdx = relPath.lastIndexOf("/");
	if (fileIdx > 0) {
		mkdirs(PATH_BACKUP, relPath.substring(0, fileIdx));
	}
	
	if (fs.existsSync(src)) {
		if (fs.existsSync(dest)) {
			console.log("Deleting (supposedly) old \"" + dest + "\"...");
			fs.unlinkSync(dest);
		}
		
		fs.renameSync(src, dest);
	} else {
		console.log("WARNING: Skip backing up \"" + src + "\" because it does not existed!");
		dest = null;
	}
	
	return dest;
}

function rmdirSync(path) {
	fs.readdirSync(path).forEach(function(file,index) {
		var curPath = path + "/" + file;
		if (fs.lstatSync(curPath).isDirectory()) {
			rmdirSync(curPath);
		} else {
			fs.unlinkSync(curPath);
		}
	});
	
    fs.rmdirSync(path);
}

function copyFile(src, target, callback) {
	var inStream = fs.createReadStream(src, { "flags": "r" });
	inStream.on("error", function(err) {
		console.log("ERROR: Cannot open read stream from file '" + src + "': " + err);
	});
	
	var outStream = fs.createWriteStream(target, { "flags": "w" });
	outStream.on("error", function(err) {
		console.log("ERROR: Cannot open write stream from file '" + target + "': " + err);
	});
	
	outStream.on("close", function() {
		callback();
	});
	
	inStream.pipe(outStream);
}

function modifyStream(inputStream, outputStream, modifier, callbackFn) {
	var done = false;
	var doneReading = false;
	var doneWritting = false;
	var endIfEnd = function() {
		if (!done) {
			done = true;
			
			outputStream.end("");
		}
	};
	
	var fileData = "";
	var buffer = [];
	var dumpBuffer = function() {
		doneWritting = false;
		
		var writeable = true;
		while (writeable && (buffer.length > 0)) {
			writeable = outputStream.write(buffer.shift() + "\n");
		}
		
		doneWritting = writeable || (buffer.length <= 0);
		if (!doneWritting) {
			outputStream.once("drain", dumpBuffer);
		} else if (doneReading) {
			endIfEnd();
		}
	};
	
	var ctx = {};
	var copyContentFn = function(data) {
		fileData += data;
		
		var lines = fileData.split(/\n/g);
		var idx = -1, lastIdx = (lines.length - 1);
		while (++idx < lastIdx) {
			var lineBuffer = modifier(lines[idx], ctx);
			if (lineBuffer != null) {
				if (Array.isArray(lineBuffer)) {
					buffer = buffer.concat(lineBuffer);
				} else {
					buffer.push(lineBuffer);
				}
				
				dumpBuffer();
			}
		}
		
		fileData = lines[lastIdx];
	};
	
	inputStream.on("data", copyContentFn);
	inputStream.on("end", function() {
		doneReading = true;
		if (fileData && (fileData.length() > 0)) {
			buffer.push(fileData);
			dumpBuffer();
		}
		
		if (doneWritting) {
			endIfEnd();
		}
	});
	
	outputStream.on("finish", callbackFn);
}

/* Networks */

function resolveLocalIP4s() {
	var result = {};
	
	var interfaces = os.networkInterfaces();
	for (var interfaceName in interfaces) {
		for (var interfaceIdx in interfaces[interfaceName]) {
			var address = interfaces[interfaceName][interfaceIdx];
			if (address.family === 'IPv4' && !address.internal) {
				result[address.address] = true;
	        }
		}
	}
	
	return result;
}

/* Template */

function createTemplateObject(jsonObj) {
	var result = {};
	
	var key, objChain;
	var chainIdx, lastChainIdx, buffer, currAttrName;
	for(key in jsonObj) {
		objChain = key.split(".");
		chainIdx = -1;
		lastChainIdx = objChain.length - 1;
		
		buffer = result
		while(++chainIdx < lastChainIdx) {
			currAttrName = objChain[chainIdx];
			if(!buffer[currAttrName]) {
				buffer[currAttrName] = {};
			}
			else if(typeof buffer[currAttrName] === "string") {
				buffer[currAttrName] = {
					"_value": buffer[currAttrName]
				};
			}
			
			buffer = buffer[currAttrName];
		}
		
		buffer[objChain[lastChainIdx]] = jsonObj[key];
	}
	
	return result;
}

function mergeTemplate(outputPath, templatePath, data, encoding) {
	fs.readFile(templatePath, encoding, function(err, fileContent) {
		if(err) {
			throw new Error("Error reading template '" + templatePath + "' [" + (encoding ? encoding : "UTF-8") + "]: " + err.message);
		}
		else {
			var templateFn = compileTemplate(fileContent);
			if(templateFn != null) {
				fs.writeFile(outputPath, templateFn(data), { encoding: encoding }, function(err) {
					if(err) {
						throw new Error("Error writing file '" + outputPath + "': " + err.message);
					}
				});
			}
		}
	});
}

function templateEscape(str) {
	return str.split("'").join("\\'").split("\n").join("\\n").split("\r").join("\\r");
}

function compileTemplate(templateStr) {
	var result = null;
	
	var templateType = typeof templateStr;
	if(templateType === "function") {
		result = templateStr;
	}
	else if(templateType !== "string") {
		throw new Error("compileTemplate only support either function or string, not " + templateType);
	}
	else {
		var implStr = [];
		implStr.push("var out = [];");
		implStr.push("with(context) {");
		implStr.push("for(var attr in context) {");
		implStr.push("context[attr] = (typeof context[attr] !== 'string') ? context[attr] : context[attr].split('\"').join('&quot;');");
		implStr.push("}");
	
		var expression = false, statement = false;
		var prevIdx = 0, currChar;
		var idx = -1, len = templateStr.length;
		while(++idx < len) {
			currChar = templateStr.charAt(idx);
			if((expression === false) && (statement === false)) {
				switch(currChar) {
					case '\n':
						if(prevIdx !== idx) {
							implStr.push("\tout.push('" + templateEscape(templateStr.substring(prevIdx, idx)) + "');");
							prevIdx = idx;
						}
						
						break;
					case '@':
						if(prevIdx !== idx) {
							implStr.push("\tout.push('" + templateEscape(templateStr.substring(prevIdx, idx)) + "');");
						}
						
						if(templateStr.charAt(idx + 1) === '$') {
							// Skip $
							++idx;
							statement = true;
						}
						else {
							expression = true;
						}
						
						// Skip @
						prevIdx = idx + 1;
						
						break;
					default:
						break;
				}
			}
			else {
				switch(currChar) {
					case ' ':
						if(expression === true) {
							implStr.push("\tout.push('" + templateEscape(templateStr.substring(prevIdx - 1, idx)) + "');");
							expression = false;
	
							prevIdx = idx;
						}
	
						break;
					case '\n':
						if(prevIdx === idx) {
							// DO NOTHING.
						}
						else {
							if(statement === true) {
								implStr.push(templateStr.substring(prevIdx, idx));
	
								// Skip new line
								prevIdx = idx + 1;
							}
							else {
								implStr.push("\tout.push('" + templateEscape(templateStr.substring(prevIdx - 1, idx)) + "\\n');");
								expression = false;
	
								prevIdx = idx;
							}
						}
						
						break;
					case '@':
						if(statement === true) {
							implStr.push(templateStr.substring(prevIdx, idx));
						}
						else {
							implStr.push("\tout.push(" + templateStr.substring(prevIdx, idx) + ");");
						}
						
						// Skip @
						prevIdx = idx + 1;
						
						statement = false;
						expression = false;
	
						break;
					default:
						break;
				}
			}
		}
		
		if(prevIdx < len) {
			if((statement === true) || (expression === true)) {
				implStr.push("\tout.push('" + templateEscape(templateStr.substring(prevIdx - 1, len)) + "');");
			}
			else {
				implStr.push("\tout.push('" + templateEscape(templateStr.substring(prevIdx, len)) + "');");
			}
		}
		
		implStr.push("}");
		implStr.push("return out.join('')");
		
		result = new Function("context", implStr.join('\n'));
	}
	
	return result;
}