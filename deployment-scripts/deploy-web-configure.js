var fs = require("fs");
var childproc = require('child_process');

var deployUtils = require("./deploy-utils.js");

var CHARSET_CONF_FILE = "utf-8";

var PATH_WEB_BAK = "./.deploybak/";

var REGEX_JK_UNMOUNT = "^(\\s)*JkUnMount /IRIS-[0-9]+/\\* (.*)$";
var REGEX_JK_UNMOUNT_ERROR = "^(\\s)*JkUnMount /errors/\\* (.*)$";

var DYNAMIC_CONTENTS = [ "META-INF", "WEB-INF", "index.vm", "index.html" ];

function startWebServer(ctx, callback) {
	childproc.execFile("apachectl", [ "start" ], { "cwd" : ctx.conf["cmd.apache.path"] }, function(err, stdOut, stdErr) {
		if(err) {
			throw new Error("Failed to start Apache: " + err);
		}
		
		console.log("Apache started!");
		
		if(typeof callback == "function") {
			callback();
		}
	});
}

function backupWebTemplates(templateSource, templateTarget) {
	console.log("Back up '" + templateSource + "' to '" + templateTarget + "'...");
	if (fs.existsSync(templateSource)) {
		if (fs.existsSync(templateTarget)) {
			console.log("Deleting \"" + templateTarget + "\"...");
			fs.unlinkSync(templateTarget);
		}
		
		fs.renameSync(templateSource, templateTarget);
	} else {
		console.log("WARNING: Skip backing up \"" + templateSource + "\" because it does not existed!");
	}
}

function configureModJK(ctx, callback) {
	var templateTarget = ctx.conf["modjk.conf.file"];
	var templateSource = templateTarget + ".template";
	
	console.log("Configuring '" + templateTarget + "'...");
	
	backupWebTemplates(templateTarget, templateSource);
	
	var modifier = function(line, modifierCtx) {
		var result = null;
		if (line.match(REGEX_JK_UNMOUNT)) {
			// DO NOTHING.
		} else {
			var matches = line.match(REGEX_JK_UNMOUNT_ERROR);
			if (!matches) {
				result = line;
			} else {
				result = [ line ];
				var idx = ctx.staticFolder.length;
				while (--idx >= 0) {
					result.push("JkUnMount /" + ctx.staticFolder[idx] + "/* " + matches[2]);
				}
			}
		}
		
		return result;
	};
	
	var inStream = fs.createReadStream(templateSource, { flags: "r", encoding: CHARSET_CONF_FILE, bufferSize: 64*1024 });
	var outStream = fs.createWriteStream(templateTarget, { flags: "w", encoding: CHARSET_CONF_FILE, bufferSize: 64*1024 });
	
	deployUtils.modifyStream(inStream, outStream, modifier, callback);
}

function locateStaticContents(ctx) {
	console.log("Search for static contents...");
	ctx.staticFolder = [];
	fs.readdirSync(ctx.irisFolder).forEach(function(file, idx) {
		var curPath = ctx.irisFolder + "/" + file;
		if (fs.lstatSync(curPath).isDirectory()) {
			if (file.match(/^IRIS-[0-9]+$/)) {
				console.log("Found Static Folder: '" + file + "'!");
				ctx.staticFolder.push(file);
			}
		}
	});
}

function handleDynamicContents(ctx) {
	console.log("Search & Destroy dynamic contents...");
	
	var file, bakPath;
	var idx = DYNAMIC_CONTENTS.length;
	while (--idx >= 0) {
		file = ctx.irisFolder + DYNAMIC_CONTENTS[idx];
		bakPath = PATH_WEB_BAK + "/" + DYNAMIC_CONTENTS[idx];
		if (fs.existsSync(bakPath)) {
			console.log("Deleting " + bakPath + "...");
			deployUtils.rmdirSync(bakPath);
		}
		
		console.log("Moving " + file + " to " + bakPath + "...");
		fs.renameSync(file, bakPath);
	}
}

function exec(conf, callback) {
	var ctx = {
		"conf": conf,
		"irisFolder": ""
	};
	
	if (conf.irisFolder) {
		ctx.irisFolder = conf.irisFolder;
	}
	
	if (fs.existsSync(PATH_WEB_BAK)) {
		deployUtils.rmdirSync(PATH_WEB_BAK);
	}
	
	fs.mkdirSync(PATH_WEB_BAK);
	
	locateStaticContents(ctx);
	handleDynamicContents(ctx);
	
	configureModJK(ctx, function() {
		//startWebServer(ctx, callback);
		callback();
	});
}

module.exports = {
	exec: exec
};