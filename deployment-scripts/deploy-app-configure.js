var fs = require("fs");
var childproc = require('child_process');

var MIN_BACKGROUND_COUNT = 2;

var dns = require("dns");
var dgram = require('dgram');
var os = require('os');
var xml2js = require("xml2js");

var deployUtils = require("./deploy-utils.js");

var PATH_TOMCAT_BIN = "./bin";
var PATH_TOMCAT_BAK = "./.deploybak";
var PATH_TOMCAT_CTX = "./conf/context.xml";

var DEFAULT_CONF_FILE = "WEB-INF/deploy-default.json";

var IRISCONF_VERSION = "WEB-INF/classes/StaticContentPaths.properties"
var TEMPLATES = [
                 	"WEB-INF/classes/jdbc.properties",
                 	"WEB-INF/classes/jdbc1.properties",
                 	"WEB-INF/classes/jdbc_reportQuery.properties",
                 	"WEB-INF/classes/jdbcMigrationEMS6.properties",
                 	"WEB-INF/classes/jdbcMigrationEMS6Slave.properties",
                 	"WEB-INF/classes/jdbcMigrationIRIS.properties",
                 	"WEB-INF/classes/ehcache.xml",
                 	"WEB-INF/classes/cluster.properties",
                 	"WEB-INF/classes/redis.properties",
                 	"WEB-INF/classes/mq.properties",
                 	"WEB-INF/classes/actionServices.xml",
                 	"WEB-INF/ems-backgroundprocesses.xml"
                ];

var NAME_MEMCACHED_SESSION_MANAGER = "de.javakaffee.web.msm.MemcachedBackupSessionManager";

function _isWindows() {
	return /^win/.test(process.platform);
}

function startAppServer(callback) {
	var startCmd;
	if (_isWindows()) {
		startCmd = "startup.bat";
	} else {
		startCmd = "./daemon.sh";
		if(fs.existsSync(startCmd)) {
			startCmd += " start";
		} else {
			startCmd = "./startup.sh";
		}
	}
	
	console.log(startCmd);
	childproc.execFile(startCmd, null, { "cwd": PATH_TOMCAT_BIN }, function(err, stdOut, stdErr) {
		if(err) {
			throw new Error("Failed to start tomcat: " + err);
		}
		
		console.log("Tomcat started!");
		
		if(typeof callback == "function") {
			callback();
		}
	});
}

function prepareTemplateData(ctx) {
	ctx.templateData["instance.name"] = ctx.clusterName;
	ctx.templateData["ehcache.mcast.port"] = ctx.mcastPort;
	
	var idx = ctx.bgMembers.length;
	var emptyIdx = idx;
	while (++emptyIdx <= MIN_BACKGROUND_COUNT) {
		ctx.templateData["member.bg" + emptyIdx] = "VANAPP0" + emptyIdx;
	}
	
	while (--idx >= 0) {
		ctx.templateData["member.bg" + (idx + 1)] = ctx.bgMembers[idx];
	}
	
	var startPort = ctx.conf["ehcache.startPort"];
	if (startPort) {
		ctx.templateData["ehcache.port"] = startPort + ctx.clusterIndex;
		ctx.templateData["ehcache.remoteObject.port"] = startPort + (ctx.clusterIndex * 2);
	}
	
	startPort = ctx.conf["tomcat.startPort"];
	if (startPort) {
		ctx.templateData["instance.port"] = startPort + ctx.clusterIndex;
	}
}

function configureMemcached(ctx, callback) {
	console.log("Configuring Memcached Session Manager...");
	var backupCtxPath = PATH_TOMCAT_CTX + ".original";
	if (!fs.existsSync(PATH_TOMCAT_CTX)) {
		throw new Error("Failed to configure memcached session manager! The context '" + PATH_TOMCAT_CTX + "' does not existed!");
	} else {
		var xmlParser = new xml2js.Parser();
		fs.readFile(PATH_TOMCAT_CTX, function(err, data) {
			if (err) {
				throw err;
			}
			
			xmlParser.parseString(data, function(err, obj) {
				if (err) {
					throw err;
				}
				
				var mcmgr = locateMemcachedSessionManager(obj.Context.Manager);
				if (!mcmgr) {
					throw new Error("Could not locate a Manager for memcached!");
				}
				
				assignMemcachedMembers(mcmgr, ctx);
				
				fs.renameSync(PATH_TOMCAT_CTX, backupCtxPath);
				
				var xmlBuilder = new xml2js.Builder();
				var xml = xmlBuilder.buildObject(obj);
				
				fs.writeFile(PATH_TOMCAT_CTX, xml, { encoding: "UTF-8" }, function(err) {
					if(err) {
						throw err;
					}
					
					callback();
				});
			});
		});
	}
}

function locateMemcachedSessionManager(managers) {
	var result = null;
	var managersList = managers;
	if (managersList) {
		if (!Array.isArray(managersList)) {
			managersList = [ managersList ];
		}
		
		// Locate the one for memcached. Create new one if not fouund.
		var idx = managersList.length;
		while ((result == null) && (--idx >= 0)) {
			if (managersList[idx]["$"]["className"] == NAME_MEMCACHED_SESSION_MANAGER) {
				result = managersList[idx];
			}
		}
	}
	
	return result;
}

function handleStaticContents(ctx) {
	var staticContentFlag = ctx.conf["mode.staticContent"];
	if (typeof staticContentFlag == "string") {
		staticContentFlag = (staticContentFlag.toLowerCase() == "true") || (staticContentFlag == "1");
	}
	
	console.log("mode.staticContent = " + staticContentFlag);
	if (staticContentFlag) {
		console.log("Search & Destroy static contents...");
		fs.readdirSync(ctx.irisFolder).forEach(function(file, idx) {
			var curPath = ctx.irisFolder + "/" + file;
			if (fs.lstatSync(curPath).isDirectory()) {
				if (file.match(/^IRIS-[0-9]+$/) || (file == "errors")) {
					var bakPath = PATH_TOMCAT_BAK + "/" + file;
					if (fs.existsSync(bakPath)) {
						console.log("Deleting " + bakPath + "...");
						deployUtils.rmdirSync(bakPath);
					}
					
					console.log("Moving " + curPath + " to " + bakPath + "...");
					fs.renameSync(curPath, bakPath);
				}
			}
		});
	}
}

function prepareStandard(ctx, callback) {
	console.log("Preparing Tomcat for Standard deployment...");
	
	console.log("Preparing Replacement Tokens for Data Injection...");
	ctx.templateData["no.data.injection.comment.start"] = "data.injection.uncomment.start -->";
	ctx.templateData["no.data.injection.comment.end"] = "<!-- data.injection.uncomment.end";
	ctx.templateData["no.data.injection.uncomment.start"] = "<!-- @data.injection.comment.start@";
	ctx.templateData["no.data.injection.uncomment.end"] = "@data.injection.comment.end@ -->";
	
	console.log("Searching & Removing datainjection jar...");
	var libFolder = ctx.irisFolder + "/WEB-INF/lib/";
	var libBakFolder = PATH_TOMCAT_BAK + "/WEB-INF/lib/";
	fs.readdirSync(libFolder).forEach(function(file, idx) {
		var curPath = libFolder + "/" + file;
		if (!fs.lstatSync(curPath).isDirectory()) {
			if (file.match(/^iris-datainjection-(.*)\\.jar$/)) {
				var bakPath = libBakFolder + file;
				if (fs.existsSync(bakPath)) {
					console.log("Deleting " + bakPath + "...");
					fs.unlinkSync(bakPath);
				}
				
				console.log("Moving " + curPath + " to " + bakPath + "...");
				fs.rename(curPath, bakPath);
			}
		}
	});
	
	callback();
}

function preparePreview(ctx, callback) {
	console.log("Prepareing Tomcat for Preview deployment...");
	
	console.log("Preparing Replacement Tokens for Data Injection...");
	ctx.templateData["data.injection.comment.start"] = "<!-- @no.data.injection.uncomment.start@";
	ctx.templateData["data.injection.comment.end"] = "@no.data.injection.uncomment.end@ -->";
	ctx.templateData["data.injection.uncomment.start"] = "no.data.injection.comment.start -->";
	ctx.templateData["data.injection.uncomment.end"] = "<!-- no.data.injection.comment.end";
	
	callback();
}

function prepareSandbox(ctx, callback) {
	console.log("Prepareing Tomcat for SandBox deployment...");
	callback();
}

function assignMemcachedMembers(memcachedManager, ctx) {
	var membersStr = [];
	var failoversStr = [];
	
	var memcachedMembers = deployUtils.splitAndTrim(ctx.conf["memcached.members"]);
	
	var thisNodeIdx = ctx.clusterIndex % memcachedMembers.length;
	if (ctx.memberNames[ctx.clusterIdx] == memcachedMembers[thisNodeIdx].split(":")[0]) {
		if (thisNodeIdx > 0) {
			--thisNodeIdx;
		} else {
			thisNodeIdx = memcachedMembers.length - 1;
		}
	}
	
	++thisNodeIdx;
	
	var idx = 0;
	while (++idx <= memcachedMembers.length) {
		var nodeName = memcachedMembers[idx - 1];
		membersStr.push("n" + idx + ":" + nodeName);
		if (idx != thisNodeIdx) {
			failoversStr.push("n" + idx);
		}
	}
	
	memcachedManager["$"]["memcachedNodes"] = membersStr.join(",");
	memcachedManager["$"]["failoverNodes"] = failoversStr.join(",");
}

function configureConfigurations(ctx, callback) {
	var deploymentMode;
	if (ctx.conf["mode.preview"]) {
		deploymentMode = "preview";
	} else if (ctx.conf["mode.sandbox"]) {
		deploymentMode = "sandbox";
	} else {
		deploymentMode = "standard";
	}
	
	var srcConfFile = deployUtils.backup(ctx.irisFolder, IRISCONF_VERSION);
	var targetConfFile = ctx.irisFolder + IRISCONF_VERSION;
	deployUtils.copyFile(srcConfFile, targetConfFile, function() {
		fs.appendFile(targetConfFile, os.EOL + "deploymentMode=" + deploymentMode + os.EOL, function(err) {
			if (err) {
				throw new Error("Failed to append deployment mode!");
			}
			
			callback();
		});
	});
}

function configureTemplates(ctx) {
	var templateData = deployUtils.createTemplateObject(ctx.templateData);
	var idx = TEMPLATES.length;
	var templateFile, confFile;
	while(--idx >= 0) {
		templateFile = deployUtils.backup(ctx.irisFolder, TEMPLATES[idx]);
		if (!templateFile) {
			console.log("Skipping a template because it doesn't existed: " + TEMPLATES[idx]);
		} else {
			confFile = ctx.irisFolder + TEMPLATES[idx];
			
			console.log("Configuring " + confFile + "...");
			deployUtils.mergeTemplate(confFile, templateFile, templateData, "UTF-8");
		}
	}
}

/* Database */

function configureDB(ctx, callback) {
	/* @ TODO */
}

function updateServerUrl(ctx, callback) {
	/* @ TODO */
}

function updateSigningServerLocation(ctx, callback) {
	/* @ TODO */
}

function updateRestDomainName() {
	/* @ TODO */
}

/* Networks */

function resolveIPs(ctx, callback) {
	ctx.bgMembers = deployUtils.splitAndTrim(ctx.conf["member.backgrounds"]);
	var fgMembers = deployUtils.splitAndTrim(ctx.conf["member.foregrounds"]);
	
	var localIPs = deployUtils.resolveLocalIP4s();
	
	ctx.clusterIndex = 0;
	
	ctx.membersHash = {};
	ctx.memberIPs = {};
	ctx.memberNames = ctx.bgMembers.concat(fgMembers);
	ctx.memberBGsHash = deployUtils.arrayToHash(ctx.bgMembers);
	ctx.memberFGsHash = deployUtils.arrayToHash(fgMembers);
	
	var membersCount = ctx.memberNames.length;
	var i = 0;
	while (i < ctx.memberNames.length) {
		var memberName = ctx.memberNames[i];
		if ((!memberName) || (memberName.length <= 0)) {
			throw new Error("Member name must not be empty string!");
		}
		
		if (ctx.membersHash[memberName]) {
			throw new Error("Duplicated member name: " + memberName);
		}
		
		if (ctx.bgMembers[i] === ctx.clusterName) {
			ctx.clusterIndex = i;
		}
		
		ctx.membersHash[memberName] = true;
		
		dns.lookup(memberName, { family: 4 }, function(err, address, family) {
			var foundMySelf = false;
			var currMember = ctx.memberNames[ctx.memberNames.length - membersCount];
			if (err) {
				console.log("WARNING: Could not resolve IP for " + currMember + ": " + err);
			} else {
				ctx.memberIPs[address] = currMember;
				foundMySelf = localIPs[address];
				if (foundMySelf) {
					ctx.clusterName = currMember;
				}
			}
			
			--membersCount;
			if (foundMySelf) {
				ctx.clusterIndex = membersCount;
			}
			
			if (membersCount == 0) {
				if (!ctx.clusterName) {
					throw new Error("Cannot resolve cluster name for this instance. " +
							"Please make sure you have setup hostname or host files properly!");
				}
				
				callback();
			}
		});
		
		++i;
	}
}

/* Multicast */

function prepareEhcacheMulticast(ctx, callback) {
	ctx.mcastPortIndex = 0;
	ctx.mcastPorts = [];
	ctx.mcastTimeout = 5000;
	
	var p = ctx.conf["multicast.startPort"];
	var len = p + ctx.conf["multicast.portlength"];
	while (p < len) {
		ctx.mcastPorts.push(p++);
	}
	
	var successHandler = function() {
		ctx.mcastPort = ctx.mcastPorts[ctx.mcastPortIndex];
		console.log("Found the correct multicast port: " + ctx.mcastPort);
		
		callback();
	};
	
	var failureHandler = function() {
		ctx.mcastPorts.splice(ctx.mcastPortIndex, 1);
		if (ctx.mcastPorts.length <= 0) {
			throw new Error("Could not find available port for ehcache multicast!");
		} else {
			ctx.mcastPortIndex = deployUtils.randomInt(0, ctx.mcastPorts.length);
		}
	};
	
	_testMulticast(ctx, successHandler, failureHandler);
}

function _testMulticast(ctx, successHandler, failureHandler) {
	var client = dgram.createSocket('udp4');
	
	var timeoutId = setTimeout(function() {
		successHandler();
		client.close();
	}, ctx.mcastTimeout);
	
	client.on("listening", function() {
		var address = client.address();
		console.log("Probing multicast on " + address.address + ":" + address.port);
		client.setBroadcast(true)
		client.setMulticastTTL(1); 
		client.addMembership(ctx.templateData["ehcache.mcast.addr"]);
	});
	
	client.on("message", function(message, remote) {
		clearTimeout(timeoutId);
		
		if (ctx.memberIPs[remote.address]) {
			successHandler();
		} else {
			console.log("Ehcache multicast " + ctx.templateData["ehcache.mcast.addr"] + ":" + ctx.mcastPorts[ctx.mcastPortIndex] + " is occupied by " + remote.address);
			failureHandler();
			
			_testMulticast(ctx, successHandler, failureHandler);
		}
		
		client.close();
	});
	
	client.bind(ctx.mcastPorts[ctx.mcastPortIndex]);
}

var DB_CONF_KEY = [ ".url", ".name", ".username", ".password", ".show.sql", ".minPoolSize", ".maxPoolSize" ];
var REDIS_CONF_KEY = [ ".host", ".port", ".minPoolSize", ".maxPoolSize" ];

function setupUnconfigureTemplateData(confKeys, confCtxId, defaultConfCtxId, templateData) {
	var key;
	var idx = confKeys.length;
	while (--idx >= 0) {
		key = confCtxId + confKeys[idx];
		if (!templateData[key]) {
			templateData[key] = templateData[defaultConfCtxId + confKeys[idx]];
		}
	}
}

function prepareBackupFolder(ctx) {
	if (fs.existsSync(PATH_TOMCAT_BAK)) {
		deployUtils.rmdirSync(PATH_TOMCAT_BAK);
	}
	
	fs.mkdirSync(PATH_TOMCAT_BAK);
	fs.mkdirSync(PATH_TOMCAT_BAK + "/WEB-INF");
	fs.mkdirSync(PATH_TOMCAT_BAK + "/WEB-INF/lib");
	fs.mkdirSync(PATH_TOMCAT_BAK + "/WEB-INF/classes");
}

function overrideConfigurations(ctx) {
	deployUtils.overrideHash(ctx.templateData, ctx.conf, "custom.");
	
	setupUnconfigureTemplateData(DB_CONF_KEY, "db.report", "db", ctx.templateData);
	setupUnconfigureTemplateData(DB_CONF_KEY, "db.migrationIRIS", "db", ctx.templateData);
	
	setupUnconfigureTemplateData(DB_CONF_KEY, "db.migrationEMS6", "dbEMS6", ctx.templateData);
	setupUnconfigureTemplateData(DB_CONF_KEY, "db.migrationEMS6Slave", "db.migrationEMS6", ctx.templateData);
	
	setupUnconfigureTemplateData(REDIS_CONF_KEY, "redis.elavon", "redis", ctx.templateData);
}

function exec(conf, callback) {
	var ctx = {
		"conf": conf,
		"irisFolder": ""
	};
	
	if (conf.irisFolder) {
		ctx.irisFolder = conf.irisFolder;
	}
	
	prepareBackupFolder(ctx);
	
	fs.readFile(ctx.irisFolder + DEFAULT_CONF_FILE, "UTF-8", function(err, jsonStr) {
		if(err) {
			throw new Error("Error reading default configurations" + err.message);
		}
		else {
			try {
				ctx["templateData"] = JSON.parse(jsonStr);
			} catch (error) {
				throw new Error("Invalid jsON string: " + jsonStr);
			}
			
			overrideConfigurations(ctx);
			
			resolveIPs(ctx, function() {
				prepareEhcacheMulticast(ctx, function() {
					var afterEnvCallback = function() {
						prepareTemplateData(ctx);
						configureConfigurations(ctx, function() {
							configureTemplates(ctx)
							handleStaticContents(ctx);
							
							configureMemcached(ctx, function() {
								startAppServer(callback);
							});
						});
					};
					
					if (ctx.conf["mode.preview"]) {
						preparePreview(ctx, afterEnvCallback);
					} else if (ctx.conf["mode.sandbox"]) {
						prepareSandbox(ctx, afterEnvCallback);
					} else {
						prepareStandard(ctx, afterEnvCallback);
					}
				});
			});
		}
	});
}

module.exports = {
	exec: exec
};
