package com.digitalpaytech.automation.transactionsuite;

public class CoinBillSummer {
	private String toCount;
	private int count;
	private double value;

	public CoinBillSummer(String toCount){
		this.toCount = toCount;
	}
	
	public void count(){
	String[] splitOne = toCount.split(",");
	String[] splitTwo;
	count = 0;
	value = 0;
	for(String commaSplit: splitOne){
		
		splitTwo = commaSplit.split(":");
		count += Integer.valueOf(splitTwo[1]);
		value += Double.valueOf(splitTwo[0]) * Double.valueOf(splitTwo[1]);
		
		}
	
	System.out.println(count);
	}
	public int getCount() {
		return count;
	}

	public Double getValue() {
		return value;
	}
}
