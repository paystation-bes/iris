package com.digitalpaytech.automation.transactions;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.digitalpaytech.automation.OrderedRunner;
import com.digitalpaytech.automation.AutomationGlobalsTest;
import com.digitalpaytech.automation.DataXmlParser;
import com.digitalpaytech.automation.Order;
import com.digitalpaytech.automation.customer.navigation.CustomerNavigation;
import com.digitalpaytech.automation.jmeter.TransXMLParser;
import com.digitalpaytech.automation.jmeter.JMeterTestPlan;
import com.digitalpaytech.automation.jmeter.SOAPRequestData;
import com.digitalpaytech.automation.transactionsuite.CoinBillSummer;

import org.junit.runner.RunWith;

@RunWith(OrderedRunner.class)
public class ChildCustomerCoinTest extends AutomationGlobalsTest {

	private static final String COIN_TRANSACTION_FILE = "TransactionData/CoinTransactionData.xml";
	private static final String ZERO_DOLLARS = "$0.00";
	private static final String PURCHASED_DATE = "PurchasedDate";
	private static final String EXPIRY_DATE = "ExpiryDate";
	private static final String TYPE = "Type";
	private static final String PAYMENT_TYPE = "PaymentType";
	private static final String CHARGED_AMOUNT = "ChargedAmount";
	private static final String PS_COM_ADDRESS =  "PaystationCommAddress";
	private static final String COIN_COL = "CoinCol";
	private static final String BILL_COL = "BillCol";
	private String passwordChild;

	private String usernameChild;
	private CustomerNavigation customerNavigation;
	private TransXMLParser transParser;
	private DataXmlParser dParse;
	private HashMap<String, String> transactionDetails;
	
	private int originalCoinCount;
	private String originalBillCount;
	
	private String originalCoinValue;
	private String originalBillValue;
	
	private String originalRunningTot;
	
	private String coinCol;
	private int coinCount;
	private Double coinValue;
	@Before
	public void setUp() {
		dParse = new DataXmlParser();
		dParse.parse();

		sendCashTransaction();
		passwordChild = dParse.getTestPasswordChild();
		usernameChild = dParse.getTestUsernameChild();
		testUrl = dParse.getTestUrl();
		this.testDriverDelay = dParse.getTestDriverDelay();
		this.browserType = dParse.getBrowserType();

		launchBrowser();
		this.customerNavigation = new CustomerNavigation(driver);
	}

	@After
	public void tearDown() {
		super.driver.quit();
	}

	@Test
	@Order(order = 1)
	public void testCashTransaction() {
		driverWait();
		super.driver.get(this.testUrl);
		super.driver.manage().window().maximize();
		driverWait();

		login(usernameChild, passwordChild);
		driverWait();

		assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
		driverWait();

		customerNavigation.navigateLeftHandMenu("Collections");

		customerNavigation.navigatePageTabs("Pay Station Summary");
		driverWait();
		WebElement posSummaryList = driver.findElement(By.id("posSummaryList"));
		ArrayList<WebElement> posList = (ArrayList<WebElement>) posSummaryList.findElements(By.tagName("li"));
		for(WebElement element: posList){
			WebElement col1 = element.findElement(By.className("col1"));
			if(col1.getText().contains(transactionDetails.get(PS_COM_ADDRESS))){
				WebElement col4 = element.findElement(By.className("col4"));
				WebElement col5 = element.findElement(By.className("col5"));
				WebElement col6 = element.findElement(By.className("col6"));
				WebElement col7 = element.findElement(By.className("col7"));
				WebElement col10 = element.findElement(By.className("col10"));
				originalCoinCount = Integer.valueOf(col4.getText());
				originalCoinValue = col5.getText();
				originalBillCount = col6.getText();
				originalBillValue = col7.getText();
				originalRunningTot = col10.getText();
				
			}
			
			
		}
		sendCashTransaction();
		driverWait();
		
		driver.navigate().refresh();
		driverWait();
		
		coinCol = transactionDetails.get(COIN_COL);
		CoinBillSummer coinSummer = new CoinBillSummer(coinCol);
		coinSummer.count();
		coinCount = coinSummer.getCount();
		coinValue = coinSummer.getValue();
		
		
		driverWait();
		WebElement pos2SummaryList = driver.findElement(By.id("posSummaryList"));
		ArrayList<WebElement> pos2List = (ArrayList<WebElement>) pos2SummaryList.findElements(By.tagName("li"));
		for(WebElement element: pos2List){
			WebElement col1 = element.findElement(By.className("col1"));
			if(col1.getText().contains(transactionDetails.get(PS_COM_ADDRESS))){
				WebElement col4 = element.findElement(By.className("col4"));
				WebElement col5 = element.findElement(By.className("col5"));
				WebElement col6 = element.findElement(By.className("col6"));
				WebElement col7 = element.findElement(By.className("col7"));
				WebElement col10 = element.findElement(By.className("col10"));
				assertEquals(col4.getText(), String.valueOf(originalCoinCount + coinCount));
				
			}
			
			
		}
		
	}

	
	public void sendCashTransaction() {
		transParser = new TransXMLParser();

		transactionDetails = transParser.parse(COIN_TRANSACTION_FILE);

		System.out.println("PURCHASE DATE" + "    "
				+ transactionDetails.get("PurchaseDate"));

		SOAPRequestData cashRequestData = new SOAPRequestData(
				transactionDetails);
		String xmlData = cashRequestData.buildXML(COIN_TRANSACTION_FILE);

		JMeterTestPlan testPlan = new JMeterTestPlan(xmlData);
		testPlan.createTestPlan();

		// if responseCode > 400 abort test before launching browser
		System.out.println(testPlan.getResponseCode());
		assertTrue(testPlan.getResponseCode() < 400);
		System.out.println(testPlan.getResult());
		assertTrue(testPlan.getResult().toLowerCase().contains("acknowledge"));

	}
}
