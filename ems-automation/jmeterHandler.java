package com.digitalpaytech.automation.jmeter;

import static org.junit.Assert.*;

import java.io.InputStream;

import org.apache.jmeter.engine.StandardJMeterEngine;
import org.apache.jmeter.reporters.ResultCollector;
import org.apache.jmeter.reporters.Summariser;
import org.apache.jmeter.save.SaveService;
import org.apache.jmeter.util.JMeterUtils;
import org.apache.jorphan.collections.HashTree;
import com.digitalpaytech.automation.AutomationGlobalsTest;
import com.digitalpaytech.automation.JMeterFactory;

public class JMeterHandler extends AutomationGlobalsTest{
    private StandardJMeterEngine jmeter;
       
    @org.junit.Before
    public void setUp() throws Exception {
        this.jmeter = JMeterFactory.getInstance().getStandardEngine();
    }
    
    @org.junit.Test
    public void sendCashOnly() throws Exception {
        
 
        InputStream in = JMeterHandler.class.getClassLoader().getResourceAsStream("JMeterTestPlans/CashTransaction2.jmx");
        assertNotNull(in);

        HashTree testPlanTree = SaveService.loadTree(in);
        in.close();
        
        Summariser summer = null;
        String summariserName = JMeterUtils.getPropDefault("summariser.name", "Summary");//$NON-NLS-1$
        if (summariserName.length() > 0) {
            summer = new Summariser(summariserName);
        }
              
        String logFile = "jMeterLogs.txt";
        ResultCollector logger = new ResultCollector(summer);
        
        logger.setFilename(logFile);
        testPlanTree.add(testPlanTree.getArray()[0], logger);
       
        
        jmeter.configure(testPlanTree);       
        jmeter.run();
        
       
    }
}