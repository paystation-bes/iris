package com.digitalpaytech.automation;

import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DataXmlParser extends AutomationGlobalsTest {
	public void parse() {
		try {
			 DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
					.newInstance();
			 DocumentBuilder docBuilder = docBuilderFactory
					.newDocumentBuilder();
			//  Document document = docBuilder.parse(new
			// File(".///SeleniumMaven///Data.xml"));
			 Document document = docBuilder.parse(this.getClass()
					.getClassLoader().getResourceAsStream("Data.xml"));

			// normalizing text representation
			document.getDocumentElement().normalize();

			// Obtaining URL
			 NodeList urlNodeList = document.getElementsByTagName("URL");
			// Node UrlNode = UrlNodeList.item(0);
			 Element url = (Element) urlNodeList.item(0);
			this.testUrl = url.getTextContent();

			// Obtaining Child Username
			 NodeList usernameNodeListChild = document
					.getElementsByTagName("ChildUsername");
			 Element usernameChild = (Element) usernameNodeListChild
					.item(0);
			this.testUsernameChild = usernameChild.getTextContent();

			// Obtaining Child Password
			 NodeList passwordNodeListChild = document
					.getElementsByTagName("ChildPassword");
			 Element passwordChild = (Element) passwordNodeListChild
					.item(0);
			this.testPasswordChild = passwordChild.getTextContent();

			// Obtaining Standalone Username
			 NodeList usernameNodeListStandAlone = document
					.getElementsByTagName("StandAloneUsername");
			 Element usernameSA = (Element) usernameNodeListStandAlone
					.item(0);
			this.testUsernameStandalone = usernameSA.getTextContent();

			// Obtaining Standalone Password
			 NodeList passwordNodeListStandAlone = document
					.getElementsByTagName("StandAlonePassword");
			 Element passwordSA = (Element) passwordNodeListStandAlone
					.item(0);
			this.testPasswordStandalone = passwordSA.getTextContent();

			// Obtaining Parent Username
			 NodeList usernameNodeListParent = document
					.getElementsByTagName("ParentUsername");
			 Element usernameParent = (Element) usernameNodeListParent
					.item(0);
			this.testUsernameParent = usernameParent.getTextContent();

			// Obtaining Parent Password
			 NodeList passwordNodeListParent = document
					.getElementsByTagName("ParentPassword");
			 Element passwordParent = (Element) passwordNodeListParent
					.item(0);
			this.testPasswordParent = passwordParent.getTextContent();

			// / Obtaining System Admin Username
			 NodeList usernameNodeListSystemAdmin = document
					.getElementsByTagName("SystemAdminUsrename");
			 Element usernameSystemAdmin = (Element) usernameNodeListSystemAdmin
					.item(0);
			this.testUsernameSystemAdmin = usernameSystemAdmin.getTextContent();

			// Obtaining System Admin Password
			 NodeList passwordNodeListSystemAdmin = document
					.getElementsByTagName("SystemAdminPassword");
			 Element passwordSystemAdmin = (Element) passwordNodeListSystemAdmin
					.item(0);
			this.testPasswordSystemAdmin = passwordSystemAdmin.getTextContent();

			// Obtaining default password = "password"
			 NodeList defaultPasswordNodeListSystemAdmin = document
					.getElementsByTagName("DefaultPassword");
			 Element defaultPasswordSystemAdmin = (Element) defaultPasswordNodeListSystemAdmin
					.item(0);
			this.testPasswordDefault = defaultPasswordSystemAdmin
					.getTextContent();

			// Obtaining Delay
			 NodeList delayList = document
					.getElementsByTagName("DriverDelayMilliSecond");
			 Element webDriverDelay = (Element) delayList.item(0);
			this.testDriverDelay = webDriverDelay.getTextContent();

			// Obtaining Browser Type
			 NodeList browserList = document
					.getElementsByTagName("BrowserType");
			 Element browserType = (Element) browserList.item(0);
			this.browserType = browserType.getTextContent();

		} catch (ParserConfigurationException | SAXException | IOException e) {
			// TODO
		}
	}
}
