package com.digitalpaytech.automation.customer.navigation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.digitalpaytech.automation.AutomationGlobalsTest;

public class CustomerAdminAllLinksTest extends AutomationGlobalsTest {
    
    private static final Logger LOGGER = Logger.getLogger(AutomationGlobalsTest.class);
    
    @Before
    public void setUp() throws Exception {
        
        try {
            final DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            //final Document document = docBuilder.parse(new File(".///SeleniumMaven///Data.xml"));
            final Document document = docBuilder.parse(this.getClass().getClassLoader().getResourceAsStream("Data.xml"));
            
            //normalizing text representation
            document.getDocumentElement().normalize();
            
            // Obtaining URL
            final NodeList urlNodeList = document.getElementsByTagName("URL");
            //Node UrlNode = UrlNodeList.item(0);
            final Element url = (Element) urlNodeList.item(0);
            this.testUrl = url.getTextContent();
            
            // Obtaining Child Username
            final NodeList usernameNodeListChild = document.getElementsByTagName("ChildUsername");
            final Element usernameChild = (Element) usernameNodeListChild.item(0);
            this.testUsernameChild = usernameChild.getTextContent();
            
            // Obtaining Child Password
            final NodeList passwordNodeListChild = document.getElementsByTagName("ChildPassword");
            final Element passwordChild = (Element) passwordNodeListChild.item(0);
            this.testPasswordChild = passwordChild.getTextContent();
            
            // Obtaining Standalone Username
            final NodeList usernameNodeListStandAlone = document.getElementsByTagName("StandAloneUsername");
            final Element usernameSA = (Element) usernameNodeListStandAlone.item(0);
            this.testUsernameStandalone = usernameSA.getTextContent();
            
            // Obtaining Standalone Password
            final NodeList passwordNodeListStandAlone = document.getElementsByTagName("StandAlonePassword");
            final Element passwordSA = (Element) passwordNodeListStandAlone.item(0);
            this.testPasswordStandalone = passwordSA.getTextContent();
            
            // Obtaining Parent Username
            final NodeList usernameNodeListParent = document.getElementsByTagName("ParentUsername");
            final Element usernameParent = (Element) usernameNodeListParent.item(0);
            this.testUsernameParent = usernameParent.getTextContent();
            
            // Obtaining Parent Password
            final NodeList passwordNodeListParent = document.getElementsByTagName("ParentPassword");
            final Element passwordParent = (Element) passwordNodeListParent.item(0);
            this.testPasswordParent = passwordParent.getTextContent();
            
            /// Obtaining System Admin Username
            final NodeList usernameNodeListSystemAdmin = document.getElementsByTagName("SystemAdminUsrename");
            final Element usernameSystemAdmin = (Element) usernameNodeListSystemAdmin.item(0);
            this.testUsernameSystemAdmin = usernameSystemAdmin.getTextContent();
            
            // Obtaining System Admin Password
            final NodeList passwordNodeListSystemAdmin = document.getElementsByTagName("SystemAdminPassword");
            final Element passwordSystemAdmin = (Element) passwordNodeListSystemAdmin.item(0);
            this.testPasswordSystemAdmin = passwordSystemAdmin.getTextContent();
            
            // Obtaining default password = "password"
            final NodeList defaultPasswordNodeListSystemAdmin = document.getElementsByTagName("DefaultPassword");
            final Element defaultPasswordSystemAdmin = (Element) defaultPasswordNodeListSystemAdmin.item(0);
            this.testPasswordDefault = defaultPasswordSystemAdmin.getTextContent();
            
            // Obtaining Delay
            final NodeList delayList = document.getElementsByTagName("DriverDelayMilliSecond");
            final Element webDriverDelay = (Element) delayList.item(0);
            this.testDriverDelay = webDriverDelay.getTextContent();
            
            // Obtaining Browser Type
            final NodeList browserList = document.getElementsByTagName("BrowserType");
            final Element browserType = (Element) browserList.item(0);
            this.browserType = browserType.getTextContent();
            
            // launching specific browser type
            launchBrowser();
            
        }
        
        catch (SAXParseException err) {
            //            System.out.println("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId());
            //            System.out.println(" " + err.getMessage());
            
        } catch (SAXException e) {
            final Exception x = e.getException();
            ((x == null) ? e : x).printStackTrace();
            
        } catch (Throwable t) {
            t.printStackTrace();
        }
        
    }
    
    @Test
    public void allLinksCustomerAdmin() {
               
        // Go to Iris
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        
        // Verifying page title
        // The test stops as it throws an exception if it fails
        assertEquals("Digital Iris - Main (Dashboard)", this.driver.getTitle());
        driverWait();
        
        //verifying the "Edit" button exists
        this.element = this.driver.findElement(By.xpath("//a[@class='linkButtonFtr edit']"));
        assertEquals(true, this.element.isDisplayed());
        //////////////////////////////////////////////////////////////////////
        
        // go to Reports page
        this.element = this.driver.findElement(By.xpath("//a[@title='Reports']"));
        this.element.click();
        driverWait();
        
        // Verifying page title
        assertEquals("Digital Iris - Reports", this.driver.getTitle());
        driverWait();
        
        // click on "Reporting Dashboard"
        this.element = this.driver.findElement(By.xpath("//a[@title='Reporting Dashboard']"));
        this.element.click();
        assertEquals(true, this.driver.findElement(By.xpath("//a[@title='Reporting Dashboard']")).isDisplayed());
        driverWait();
        
        // go to Transaction Lookup Page
        this.element = this.driver.findElement(By.xpath("//a[@title='Permit Lookup']"));
        this.element.click();
        driverWait();
        
        //verifying the "SEARCH" button exists
        this.element = this.driver.findElement(By.xpath("//a[@class='linkButtonFtr search right']"));
        assertEquals(true, this.element.isDisplayed());
        
        // Verifying page title
        assertEquals("Digital Iris - Reports : Permit Lookup", this.driver.getTitle());
        driverWait();
        ///////////////////////////////////////////////////////////////////////////////
        
        // go to Maintenance page
        this.element = this.driver.findElement(By.xpath("//a[@title='Maintenance']"));
        this.element.click();
        
        // Verifying page title
        assertEquals("Digital Iris - Maintenance", this.driver.getTitle());
        driverWait();
        
        // click on "Current Alerts"
        this.element = this.driver.findElement(By.xpath("//a[@title='Current Alerts']"));
        this.element.click();
        driverWait();
        
        //verifying the "SAVE" button exists
        this.element = this.driver.findElement(By.xpath("//a[@id='saveActiveList']"));
        assertEquals(true, this.element.isDisplayed());
        
        // go to "Resolved Alerts" page
        this.element = this.driver.findElement(By.xpath("//a[@title='Resolved Alerts']"));
        this.element.click();
        driverWait();
        
        // Verifying page title
        assertEquals("Digital Iris - Maintenance", this.driver.getTitle());
        driverWait();
        
        //verifying the severity dropdown object exists
        this.element = selectXpath("//a[@id='severityACExpand' and @title='Show Options']");
        assertEquals(true, this.element.isDisplayed());
        driverWait();
        driverWait();
        
        // click on "Pay Station Summary"
        this.element = this.driver.findElement(By.xpath("//a[@id='posSummaryList']"));
        this.element.click();
        driverWait();
        
        // Verifying page title
        assertEquals("Digital Iris - Maintenance : Pay Station Summary", this.driver.getTitle());
        driverWait();
        
        //verifying the dropdown object exists
        this.element = this.driver.findElement(By.xpath("//a[@id='filterLocationRouteExpand']"));
        assertEquals(true, this.element.isDisplayed());
        ///////////////////////////////////////////////////////////////////////////////
        
        // go to "Collections" page
        this.element = this.driver.findElement(By.xpath("//a[@title='Collections']"));
        this.element.click();
        driverWait();
        
        // Verifying page title
        assertEquals("Digital Iris - Collections", this.driver.getTitle());
        driverWait();
        
        //verifying save button exists
        this.element = this.driver.findElement(By.xpath("//a[@id='saveActiveList']"));
        assertEquals(true, this.element.isDisplayed());
        
        // click on "Collections Required"
        this.element = this.driver.findElement(By.xpath("//a[@title='Collections Required']"));
        this.element.click();
        driverWait();
        
        // go to "Resolved Alerts" page
        this.element = this.driver.findElement(By.xpath("//a[@title='Recent Collections']"));
        this.element.click();
        
        // Verifying page title
        assertEquals("Digital Iris - Collections", this.driver.getTitle());
        driverWait();
        
        //verifying dropdown object exists
        this.element = this.driver.findElement(By.xpath("//a[@id='locationRouteACExpand']"));
        assertEquals(true, this.element.isDisplayed());
        
        // go to "Digital Collect Users" page
        this.element = this.driver.findElement(By.xpath("//a[@title='Digital Collect Users']"));
        this.element.click();
        driverWait();
        
        //verifying text exists
        this.element = this.driver.findElement(By.xpath("//h2[contains(., 'Digital Collect  Users')]"));
        assertEquals(true, this.element.isDisplayed());
        
        // go to "Pay Station Summary" page
        this.element = this.driver.findElement(By.xpath("//a[@title='Pay Station Summary']"));
        this.element.click();
        
        // Verifying page title
        assertEquals("Digital Iris - Collections : Pay Station Summary", this.driver.getTitle());
        driverWait();
        
        //verifying dropdown object exists
        this.element = this.driver.findElement(By.xpath("//a[@id='filterLocationRouteExpand']"));
        assertEquals(true, this.element.isDisplayed());
        ///////////////////////////////////////////////////////////////////////////////
        
        // go to "Accounts" page
        this.element = this.driver.findElement(By.xpath("//a[@title='Accounts']"));
        this.element.click();
        
        // Verifying page title
        assertEquals("Digital Iris - Accounts : Customer Accounts", this.driver.getTitle());
        driverWait();
        
        // click on "Customer Accounts"
        this.element = this.driver.findElement(By.xpath("//a[@title='Customer Accounts']"));
        this.element.click();
        driverWait();
        
        // go to "Coupons" page
        this.element = this.driver.findElement(By.xpath("//a[@title='Coupons']"));
        this.element.click();
        driverWait();
        
        // Verifying page title
        assertEquals("Digital Iris - Accounts : Coupons", this.driver.getTitle());
        
        //verifying search button exists
        this.element = this.driver.findElement(By.xpath("//a[@class='linkButtonFtr autoWidthAsset search']"));
        assertEquals(true, this.element.isDisplayed());
        
        // go to "Passcards" page
        this.element = this.driver.findElement(By.xpath("//a[@title='Passcards']"));
        this.element.click();
        
        // Verifying page title
        assertEquals("Digital Iris - Accounts : Passcards", this.driver.getTitle());
        driverWait();
        
        //verifying search button exists
        this.element = this.driver.findElement(By.xpath("//a[@class='linkButtonFtr autoWidthAsset search']"));
        assertEquals(true, this.element.isDisplayed());
        ///////////////////////////////////////////////////////////////////////////////
        
        // go to "Card Management" page
        this.element = this.driver.findElement(By.xpath("//a[@title='Card Management']"));
        this.element.click();
        
        // Verifying page title
        assertEquals("Digital Iris - Card Management : Banned Cards", this.driver.getTitle());
        driverWait();
        
        // click on "Banned Cards"
        this.element = this.driver.findElement(By.xpath("//a[@title='Banned Cards']"));
        this.element.click();
        driverWait();
        
        // go to "Card Refunds" page
        this.element = this.driver.findElement(By.xpath("//a[@title='Card Refunds']"));
        this.element.click();
        
        // Verifying page title
        assertEquals("Digital Iris - Card Management : Card Refunds", this.driver.getTitle());
        driverWait();
        
        //verifying search button exists
        this.element = this.driver.findElement(By.xpath("//a[@class='linkButtonFtr search right']"));
        assertEquals(true, this.element.isDisplayed());
        ///////////////////////////////////////////////////////////////////////////////
        
        // go to "Settings" page
        this.element = this.driver.findElement(By.xpath("//a[@title='Settings']"));
        this.element.click();
        driverWait();
        
        // Verifying page title
        assertEquals("Digital Iris - Settings - Global : Settings", this.driver.getTitle());
        driverWait();
        
        // click on "Settings"
        this.element = this.driver.findElement(By.xpath("//a[@title='Settings']"));
        this.element.click();
        driverWait();
        
        // click on "System Notification"
        this.element = this.driver.findElement(By.xpath("//a[@title='System Notifications']"));
        this.element.click();
        
        // Verifying page title
        assertEquals("Digital Iris - Settings - Global : System Notifications", this.driver.getTitle());
        driverWait();
        
        // click on "Recent Activity"
        this.element = this.driver.findElement(By.xpath("//a[@title='Recent Activity']"));
        this.element.click();
        
        // Verifying page title
        assertEquals("Digital Iris - Settings - Global : Recent Activity", this.driver.getTitle());
        driverWait();
        
        //verifying text exists
        this.element = this.driver.findElement(By.xpath("//h2[contains(., 'Activity Log')]"));
        assertEquals(true, this.element.isDisplayed());
        
        // click on "Rates"
        this.element = this.driver.findElement(By.xpath("//a[@title='Rates']"));
        this.element.click();
        
        // Verifying page title
        assertEquals("Digital Iris - Settings - Rates : Rate Details", this.driver.getTitle());
        driverWait();
        
        //verifying text exists
        this.element = this.driver.findElement(By.xpath("//input[@id='rateAC']"));
        assertEquals(true, this.element.isDisplayed());
        
        // click on "Locations"
        this.element = this.driver.findElement(By.xpath("//a[@title='Locations']"));
        this.element.click();
        
        // Verifying page title
        assertEquals("Digital Iris - Settings - Locations : Location Details", this.driver.getTitle());
        driverWait();
        
        // click on "Location Details"
        this.element = this.driver.findElement(By.xpath("//a[@title='Location Details']"));
        this.element.click();
        driverWait();
        
        // click on "Extend By Phone"
        this.element = this.driver.findElement(By.xpath("//a[@title='Extend By Phone']"));
        this.element.click();
        
        // Verifying page title
        assertEquals("Digital Iris - Settings - Locations : Extend By Phone", this.driver.getTitle());
        driverWait();
        
        //verifying text exists
        this.element = this.driver.findElement(By.xpath("//h2[contains(., 'Global Settings')]"));
        assertEquals(true, this.element.isDisplayed());
        
        //verifying edit button exists
        this.element = this.driver.findElement(By.xpath("//a[@id='btnEditWrnngPeriod']"));
        assertEquals(true, this.element.isDisplayed());
        
        // click on "Pay Stations"
        this.element = this.driver.findElement(By.xpath("//a[@title='Pay Stations']"));
        this.element.click();
        
        // Verifying page title
        assertEquals("Digital Iris - Settings - Pay Stations : Pay Station List", this.driver.getTitle());
        driverWait();
        
        // click on "Pay Station List"
        this.element = this.driver.findElement(By.xpath("//a[@title='Pay Station List']"));
        this.element.click();
        driverWait();
        
        // click on "Pay Station Configuration"
        this.element = this.driver.findElement(By.xpath("//a[@title='Pay Station Configuration']"));
        this.element.click();
        
        // Verifying page title
        assertEquals("Digital Iris - Settings - Pay Stations : Pay Station Configuration", this.driver.getTitle());
        driverWait();
        
        //verifying text exists
        this.element = this.driver.findElement(By.xpath("//h2[contains(., 'Pay Station Configuration')]"));
        assertEquals(true, this.element.isDisplayed());
        
        // click on "Pay Station Placement"
        this.element = this.driver.findElement(By.xpath("//a[@title='Pay Station Placement']"));
        this.element.click();
        
        // Verifying page title
        assertEquals("Digital Iris - Settings - Pay Stations : Pay Station Placement", this.driver.getTitle());
        driverWait();
        
        //verifying text exists
        this.element = this.driver.findElement(By.xpath("//h2[contains(., 'Pay Stations')]"));
        assertEquals(true, this.element.isDisplayed());
        
        // click on "Routes"
        this.element = this.driver.findElement(By.xpath("//a[@title='Routes']"));
        this.element.click();
        
        // Verifying page title
        assertEquals("Digital Iris - Settings : Routes : Route Details", this.driver.getTitle());
        driverWait();
        
        //verifying text exists
        this.element = this.driver.findElement(By.xpath("//h2[contains(., 'Routes')]"));
        assertEquals(true, this.element.isDisplayed());
        
        // click on "Alerts"
        this.element = this.driver.findElement(By.xpath("//a[@title='Alerts']"));
        this.element.click();
        
        // Verifying page title
        assertEquals("Digital Iris - Settings - Alerts", this.driver.getTitle());
        driverWait();
        
        //verifying text exists
        this.element = this.driver.findElement(By.xpath("//h2[contains(., 'Alerts')]"));
        assertEquals(true, this.element.isDisplayed());
        
        // click on "Users"
        this.element = this.driver.findElement(By.xpath("//a[@title='Users']"));
        this.element.click();
        
        // Verifying page title
        assertEquals("Digital Iris - Settings - Users : User Account", this.driver.getTitle());
        driverWait();
        
        // click on "User Account" tab
        this.element = this.driver.findElement(By.xpath("//a[@title='User Account']"));
        this.element.click();
        driverWait();
        
        // click on "Roles"
        this.element = this.driver.findElement(By.xpath("//a[@title='Roles']"));
        this.element.click();
        
        // Verifying page title
        assertEquals("Digital Iris - Settings - Users : Roles", this.driver.getTitle());
        driverWait();
        
        //verifying text exists
        this.element = this.driver.findElement(By.xpath("//h2[contains(., 'Roles')]"));
        assertEquals(true, this.element.isDisplayed());
        
        // click on "Card Settings"
        this.element = this.driver.findElement(By.xpath("//a[@title='Card Settings']"));
        this.element.click();
        
        // Verifying page title
        assertEquals("Digital Iris - Settings - Card Retry Queue Settings", this.driver.getTitle());
        driverWait();
        
        //verifying text exists
        this.element = this.driver.findElement(By.xpath("//h2[contains(., 'Card Retry Queue Settings')]"));
        assertEquals(true, this.element.isDisplayed());
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        //assertTrue(true);
    }
    
    @After
    public void tearDown() throws Exception {
        
        this.driver.quit();
        //driver.close();
    }
    
    /*
     * Waits until element can be clicked and then sets the element parameter to that element
     * based on the Xpath value
     */
    public WebElement selectXpath(final String webElementXpath) {
        final WebDriverWait webDriverWait = new WebDriverWait(this.driver, WAIT_TIME_TWENTY_SECONDS);
        try {
            webDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath(webElementXpath)));
            this.element = this.driver.findElement(By.xpath(webElementXpath));
        } catch (TimeoutException e) {
            this.LOGGER.info(e.getMessage());
        }
        
        return this.element;
    }
    
}
