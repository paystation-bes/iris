package com.digitalpaytech.automation.customer.settings.routes;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.digitalpaytech.automation.Order;
import com.digitalpaytech.automation.OrderedRunner;
import com.digitalpaytech.automation.settings.routes.RoutesSuiteTest;

@RunWith(OrderedRunner.class)
public class RoutesSuiteChildUserTest extends RoutesSuiteTest {
    @Before
    public final void setUp() {
        super.setUpRoutesTest("child");
    }
    
    @After
    public final void tearDown() {
        super.tearDownRoutesTest();
    }

    /*@Test
    @Order(order = 1)
    public final void testChildAddRouteChrome() {
        super.addRouteTest("chrome");
    }
    
    @Test
    @Order(order = 2)
    public final void testChildEditRouteChrome() {
        super.editRouteTest("chrome");
    }
    
    @Test
    @Order(order = 3)
    public final void testChildDeleteRouteChrome() {
        super.deleteRouteTest("chrome");
    }*/
    
    @Test
    @Order(order = 4)
    public final void testChildAddRouteFirefox() {
        super.addRouteTest("firefox");
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 5)
    public final void testChildEditRouteFirefox() {
        super.editRouteTest("firefox");
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 6)
    public final void testChildDeleteRouteFirefox() {
        super.deleteRouteTest("firefox");
        //assertTrue(true);
    }
    
    /*@Test
    @Order(order = 7)
    public final void testChildAddRouteInternetExplorer() {
        super.addRouteTest("ie");
    }
    
    @Test
    @Order(order = 8)
    public final void testChildEditRouteInternetExplorer() {
        super.editRouteTest("ie");
    }
    
    @Test
    @Order(order = 9)
    public final void testChildDeleteRouteInternetExplorer() {
        super.deleteRouteTest("ie");
    }*/
}
