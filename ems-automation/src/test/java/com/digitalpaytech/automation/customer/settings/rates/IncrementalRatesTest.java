package com.digitalpaytech.automation.customer.settings.rates;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.digitalpaytech.automation.AutomationGlobalsTest;
import com.digitalpaytech.automation.Order;
import com.digitalpaytech.automation.OrderedRunner;
import com.digitalpaytech.automation.customer.navigation.CustomerNavigation;

@RunWith(OrderedRunner.class)
public class IncrementalRatesTest extends AutomationGlobalsTest {
    
    private CustomerNavigation customerNavigation = new CustomerNavigation();
    
    @Before
    public void setUp() throws Exception {
        
        try {
            final DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            //final Document document = docBuilder.parse(new File(".///SeleniumMaven///Data.xml"));
            final Document document = docBuilder.parse(this.getClass().getClassLoader().getResourceAsStream("Data.xml"));
            
            //normalizing text representation
            document.getDocumentElement().normalize();
            
            // Obtaining URL
            final NodeList urlNodeList = document.getElementsByTagName("URL");
            //Node UrlNode = UrlNodeList.item(0);
            final Element url = (Element) urlNodeList.item(0);
            this.testUrl = url.getTextContent();
            
            // Obtaining Child Username
            final NodeList usernameNodeListChild = document.getElementsByTagName("ChildUsername");
            final Element usernameChild = (Element) usernameNodeListChild.item(0);
            this.testUsernameChild = usernameChild.getTextContent();
            
            // Obtaining Child Password
            final NodeList passwordNodeListChild = document.getElementsByTagName("ChildPassword");
            final Element passwordChild = (Element) passwordNodeListChild.item(0);
            this.testPasswordChild = passwordChild.getTextContent();
            
            // Obtaining Standalone Username
            final NodeList usernameNodeListStandAlone = document.getElementsByTagName("StandAloneUsername");
            final Element usernameSA = (Element) usernameNodeListStandAlone.item(0);
            this.testUsernameStandalone = usernameSA.getTextContent();
            
            // Obtaining Standalone Password
            final NodeList passwordNodeListStandAlone = document.getElementsByTagName("StandAlonePassword");
            final Element passwordSA = (Element) passwordNodeListStandAlone.item(0);
            this.testPasswordStandalone = passwordSA.getTextContent();
            
            // Obtaining Parent Username
            final NodeList usernameNodeListParent = document.getElementsByTagName("ParentUsername");
            final Element usernameParent = (Element) usernameNodeListParent.item(0);
            this.testUsernameParent = usernameParent.getTextContent();
            
            // Obtaining Parent Password
            final NodeList passwordNodeListParent = document.getElementsByTagName("ParentPassword");
            final Element passwordParent = (Element) passwordNodeListParent.item(0);
            this.testPasswordParent = passwordParent.getTextContent();
            
            /// Obtaining System Admin Username
            final NodeList usernameNodeListSystemAdmin = document.getElementsByTagName("SystemAdminUsrename");
            final Element usernameSystemAdmin = (Element) usernameNodeListSystemAdmin.item(0);
            this.testUsernameSystemAdmin = usernameSystemAdmin.getTextContent();
            
            /// Obtaining Digital Collect Username
            final NodeList usernameNodeListCollectChild = document.getElementsByTagName("CollectChildUsername");
            final Element usernameCollectChild = (Element) usernameNodeListCollectChild.item(0);
            this.testUsernameCollectChild = usernameCollectChild.getTextContent();
            
            /// Obtaining Digital Collect Username
            final NodeList usernameNodeListCollectStandalone = document.getElementsByTagName("CollectSAUsername");
            final Element usernameCollectStandalone = (Element) usernameNodeListCollectStandalone.item(0);
            this.testUsernameCollectStandalone = usernameCollectStandalone.getTextContent();
            
            // Obtaining System Admin Password
            final NodeList passwordNodeListSystemAdmin = document.getElementsByTagName("SystemAdminPassword");
            final Element passwordSystemAdmin = (Element) passwordNodeListSystemAdmin.item(0);
            this.testPasswordSystemAdmin = passwordSystemAdmin.getTextContent();
            
            // Obtaining default password = "password"
            final NodeList defaultPasswordNodeListSystemAdmin = document.getElementsByTagName("DefaultPassword");
            final Element defaultPasswordSystemAdmin = (Element) defaultPasswordNodeListSystemAdmin.item(0);
            this.testPasswordDefault = defaultPasswordSystemAdmin.getTextContent();
            
            // Obtaining Delay
            final NodeList delayList = document.getElementsByTagName("DriverDelayMilliSecond");
            final Element webDriverDelay = (Element) delayList.item(0);
            this.testDriverDelay = webDriverDelay.getTextContent();
            
            // Obtaining Browser Type
            final NodeList browserList = document.getElementsByTagName("BrowserType");
            final Element browserType = (Element) browserList.item(0);
            this.browserType = browserType.getTextContent();
            
            // launching specific browser type
            launchBrowser();
            this.customerNavigation = new CustomerNavigation(driver);
            
        }
        
        catch (SAXParseException err) {
            //            System.out.println("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId());
            //            System.out.println(" " + err.getMessage());
            
        } catch (SAXException e) {
            final Exception x = e.getException();
            ((x == null) ? e : x).printStackTrace();
            
        } catch (Throwable t) {
            t.printStackTrace();
        }
        
    }
    
    @After
    public void tearDown() throws Exception {
        
        super.driver.quit();
        //driver.close();
    }
    
    @Test
    @Order(order = 1)
    public final void goToRates() {
        
        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameStandalone, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Rates > Global
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Rates");
        customerNavigation.navigatePageSubTabs("Rates");
        driverWait();
        
        assertEquals("Digital Iris - Settings - Rates : Rate Details", super.driver.getTitle());
        driverWait();
        
        // Validating static texts and dropdown items/labels
        super.element = super.selectWebElementByXpath("//section[@class='groupBox menuBox rateList']/header/h2");
        driverWait();
        assertEquals("RATES", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@class='filterHeader activityLog horizontal']/section[@class='title']");
        driverWait();
        assertEquals("FILTER:", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//li[@class='autoComplete']/form[@id='rateFilterForm']/label[@class='hiddenLabel']");
        driverWait();
        assertEquals("Rate / Type", super.element.getText());
        driverWait();
        
        // Clicking on the + button and verifying all static contents
        super.element = super.selectWebElementByXpath("//a[@id='btnAddRates' and @class='hdrButtonIcn add']");
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//h2[@id='addHeading']");
        driverWait();
        assertEquals("ADD RATE", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@id='step1']/section[@class='left']");
        driverWait();
        assertEquals("STEP 1:", super.element.getText());
        driverWait();
        
        super.element = super
                .selectWebElementByXpath("//section[@id='step1']/section[@class='left details']/section[@class='col1 left']/label[@class='detailLabel ' and @for='formRateName']");
        driverWait();
        assertEquals("Rate Name", super.element.getText());
        driverWait();
        
        super.element = super
                .selectWebElementByXpath("//section[@id='step1']/section[@class='left details']/section[@class='col2 left']/label[@class='detailLabel ' and @for='rateType']");
        driverWait();
        assertEquals("Rate Type", super.element.getText());
        driverWait();
        
        super.selectOptionFromDropDown("formRateType", "choose");
        driverWait();
        
        super.selectOptionFromDropDown("formRateType", "Flat Rate");
        driverWait();
        
        super.selectOptionFromDropDown("formRateType", "Daily");
        driverWait();
        
        super.selectOptionFromDropDown("formRateType", "Hourly");
        driverWait();
        
        super.selectOptionFromDropDown("formRateType", "Incremental");
        driverWait();
        
        super.selectOptionFromDropDown("formRateType", "Monthly");
        driverWait();
        
        super.selectOptionFromDropDown("formRateType", "Parking Restriction");
        driverWait();
        
        super.selectOptionFromDropDown("formRateType", "Incremental");
        driverWait();
        
        // Step 2 Validation
        super.element = super.selectWebElementByXpath("//section[@id='step2']/section[@class='left']");
        driverWait();
        assertEquals("STEP 2:", super.element.getText());
        driverWait();
        
        super.element = super
                .selectWebElementByXpath("//section[@id='step2']/section[@class='left details']/section[@class='col1 left']/label[@class='detailLabel ' and @for='rateID']");
        driverWait();
        assertEquals("Rate Amount", super.element.getText());
        driverWait();
        
        super.element = super
                .selectWebElementByXpath("//section[@id='step2']/section[@class='left details']/section[@class='col1 left']/span[@id='formExceedHour']");
        driverWait();
        assertEquals("per Hour", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@class='col1 left']/label[@class='detailLabel ' and @for='INC-minPayment']");
        driverWait();
        assertEquals("Minimum Payment", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@class='col2 left']/label[@class='detailLabel ' and @for='INC-maxPAyment']");
        driverWait();
        assertEquals("Maximum Payment", super.element.getText());
        driverWait();
        
        super.element = super
                .selectWebElementByXpath("//label[@id='flatRateCheckLabel' and @class='checkboxLabel switch' and @for='INC-flatRateCheckHiddenHidden']");
        driverWait();
        assertEquals("  Enable Flat Rate", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@class='col2 left']/h3[@style='padding-left: 5px;']");
        driverWait();
        assertEquals("Payment Type Accepted", super.element.getText());
        driverWait();
        
        //checking payment type checkbox objects
        // enable all checkbox objects
        super.element = super.selectWebElementByXpath("//label[@id='paymentCoinLabel' and @for='paymentCoinHidden']");
        driverWait();
        assertEquals("  Coins", super.element.getText());
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//label[@id='paymentBillLabel' and @for='paymentBillHidden']");
        driverWait();
        assertEquals("  Bills", super.element.getText());
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//label[@id='paymentCCLabel' and @for='paymentCCHidden']");
        driverWait();
        assertEquals("  Credit Cards", super.element.getText());
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//label[@id='paymentRFLabel' and @for='paymentRFHidden']");
        driverWait();
        assertEquals("  Contactless", super.element.getText());
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//label[@id='paymentSCLabel' and @for='paymentSCHidden']");
        driverWait();
        assertEquals("  Smart Cards", super.element.getText());
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//label[@id='paymentPCLabel' and @for='paymentPCHidden']");
        driverWait();
        assertEquals("  Passcards", super.element.getText());
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//label[@id='paymentCouponLabel' and @for='paymentCouponHidden']");
        driverWait();
        assertEquals("  Coupons", super.element.getText());
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@id='couponPrompt']/label[@class='detailLabel ']");
        driverWait();
        assertEquals("Prompt For Coupons:", super.element.getText());
        driverWait();
        
        super.selectOptionFromDropDown("promptCoupon", "No");
        driverWait();
        
        super.selectOptionFromDropDown("promptCoupon", "Yes");
        driverWait();
        
        super.element = super.selectWebElementByXpath("//a[@id='INC-flatRateCheck' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super
                .selectWebElementByXpath("//section[@id='flatRateDetails']/section[@class='col1 left']/label[@class='detailLabel ' and @for='INC-startTime']");
        driverWait();
        assertEquals("Start Time", super.element.getText());
        driverWait();
        
        super.element = super
                .selectWebElementByXpath("//section[@id='flatRateDetails']/section[@class='col2 left']/label[@class='detailLabel ' and @for='INC-endTime']");
        driverWait();
        assertEquals("End Time", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//a[@id='addTime' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//a[@id='ADT-useAddNumber' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//a[@id='ADT-useSpaceNumber' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//a[@id='ADT-XBPhone' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//a[@id='INC-lastValueCheck' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//a[@id='inc02Check' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        // Card Increments
        super.element = super.selectWebElementByXpath("//section[@id='cardIncrementsArea']/h3[contains(., 'Card Increments')]");
        driverWait();
        assertEquals("Card Increments", super.element.getText());
        driverWait();
        
        super.element = super
                .selectWebElementByXpath("//section[@id='cardIncrementsArea']/label[@class='checkboxLabel switch' and @for='INC-lastValueCheckHidden']");
        driverWait();
        assertEquals("  Keep Using Last Increment Value", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@id='addTimeArea']/label[@class='checkboxLabel switch' and @for='addTimeHidden']");
        driverWait();
        assertEquals("  Enable Add Time", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@id='addTimeDetailArea']/section[@class='col1']/label[@for='ADT-useAddNumberHidden']");
        driverWait();
        assertEquals("  Use Add Time Number", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@id='addTimeDetailArea']/section[@class='col1']/label[@for='ADT-useSpaceNumberHidden']");
        driverWait();
        assertEquals("  Use Space/Plate Number", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@id='addTimeDetailArea']/section[@class='col2']/label[@for='ADT-XBPhoneHidden']");
        driverWait();
        assertEquals("  Use Extend By Phone", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//label[@class='detailLabel ' and @for='ADT-xbpServiceFee']");
        driverWait();
        assertEquals("Service Fee", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//label[@class='detailLabel ' and @for='ADT-xbpMinExtension']");
        driverWait();
        assertEquals("Minimum Extension", super.element.getText());
        driverWait();
        
        // validating buttons
        super.element = super.selectWebElementByXpath("//a[@class='linkButtonFtr textButton save' and @title='Save']");
        driverWait();
        assertEquals("SAVE", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//a[@class='linkButton textButton cancel' and @title='Cancel']");
        driverWait();
        assertEquals("CANCEL", super.element.getText());
        driverWait();
        
        //logout
        super.element = super.selectWebElementByXpath("//a[@title='Logout']");
        super.element.click();
        
        // validate the content of the pop-up message
        super.element = super.selectWebElementByXpath("//section[@id='messageResponseAlertBox']/section[@class='innerBorder']/h4/strong");
        driverWait();
        assertEquals("Attention!", super.element.getText());
        super.element = super.selectWebElementByXpath("//section[@id='messageResponseAlertBox']/section[@class='innerBorder']/article");
        driverWait();
        assertEquals("You haven't saved or cancelled your changes. Please select an option below to cancel and continue to exit the page or to return to the page to save.",
                     super.element.getText());
        
        // confirm the logout
        super.element = super
                .selectWebElementByXpath("//div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']/div[@class='ui-dialog-buttonset']/button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only btnCancel']/span[@class='ui-button-text']");
        driverWait();
        assertEquals("CANCEL CHANGES", super.element.getText());
        driverWait();
        super.element.click();
        
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
        
    }
    
    @Test
    @Order(order = 2)
    public final void addIncRate() {
        
        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Rates > Global
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Rates");
        customerNavigation.navigatePageSubTabs("Rates");
        driverWait();
        
        assertEquals("Digital Iris - Settings - Rates : Rate Details", super.driver.getTitle());
        driverWait();
        
        // Clicking on the + button 
        super.element = super.selectWebElementByXpath("//a[@id='btnAddRates' and @class='hdrButtonIcn add']");
        driverWait();
        super.element.click();
        driverWait();
        
        // Fill out the form to add an incremental rate
        super.element = super.selectWebElementByXpath("//input[@id='formRateName' and @type='text' and @name='wrappedObject.name']");
        driverWait();
        super.element.click();
        driverWait();
        super.element.sendKeys("Incremental Rate");
        driverWait();
        
        super.selectOptionFromDropDown("formRateType", "Incremental");
        driverWait();
        
        // Payment Type:  Credit Card
        super.element = super.selectWebElementByXpath("//a[@id='paymentCC' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//a[@id='paymentCoin' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//a[@id='paymentBill' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//a[@id='paymentRF' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//a[@id='paymentSC' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//a[@id='paymentPC' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//a[@id='paymentCoupon' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        super.selectOptionFromDropDown("promptCoupon", "Yes");
        driverWait();
        
        super.element = super.selectWebElementByXpath("//input[@id='formRateAmount' and @name='wrappedObject.rateAmount']");
        driverWait();
        super.element.sendKeys("6");
        driverWait();
        
        super.element = super.selectWebElementByXpath("//input[@id='INC-minPayment' and @name='wrappedObject.minPaymentAmount']");
        driverWait();
        super.element.sendKeys("1");
        driverWait();
        
        super.element = super.selectWebElementByXpath("//input[@id='INC-maxPayment' and @name='wrappedObject.maxPaymentAmount']");
        driverWait();
        super.element.sendKeys("60");
        driverWait();
        
        // Enable Flat Rate
        super.element = super.selectWebElementByXpath("//a[@id='INC-flatRateCheck' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//input[@id='INC-flatRateAmount' and @name='wrappedObject.flatRateAmount']");
        driverWait();
        super.element.sendKeys("10");
        driverWait();
        
        super.assignText("INC-startTime", "displayStartTime", "09:00");
        driverWait();
        super.assignText("INC-endTime", "displayEndTime", "23:00");
        driverWait();
        //        super.element = super.selectWebElementByXpath("//input[@id='INC-startTime' and @name='wrappedObject.flatRateStartTime']");
        //        driverWait();
        //        super.element.sendKeys("09:00");
        //        driverWait();
        //        
        //        super.element = super.selectWebElementByXpath("//input[@id='INC-endTime' and @name='wrappedObject.flatRateEndTime']");
        //        driverWait();
        //        super.element.sendKeys("23:00");
        //        driverWait();
        
        // Enable Add Time
        super.element = super.selectWebElementByXpath("//a[@id='addTime' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        // Use Add Time Number
        super.element = super.selectWebElementByXpath("//a[@id='ADT-useAddNumber' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        // Use Space/Plate Number
        super.element = super.selectWebElementByXpath("//a[@id='ADT-useSpaceNumber' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        // Use Extend by Phone
        super.element = super.selectWebElementByXpath("//a[@id='ADT-XBPhone' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//input[@id='ADT-xbpServiceFee' and @name='wrappedObject.ebPServiceFeeAmount']");
        driverWait();
        super.element.sendKeys("1");
        driverWait();
        
        super.element = super.selectWebElementByXpath("//input[@id='ADT-xbpMinExtension' and @name='wrappedObject.ebPMiniumExt']");
        driverWait();
        super.element.sendKeys("30");
        driverWait();
        
        // Card Increments
        super.element = super.selectWebElementByXpath("//a[@id='INC-lastValueCheck' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//a[@id='inc02Check' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//input[@id='INC-minsInc02' and @name='wrappedObject.minsInc02']");
        driverWait();
        super.element.clear();
        super.element.sendKeys("15");
        driverWait();
        
        // Save
        super.element = super.selectWebElementByXpath("//a[@class='linkButtonFtr textButton save' and @title='Save']");
        driverWait();
        super.element.click();
        driverWait();
        
        // The following block of code checks to see if the above rate is successfully added
        super.element = super.selectWebElementByXpath("//li[contains(., 'Incremental Rate')]");
        driverWait();
        super.element.click();
        
        //logout
        logout();
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
        
    }
    
    @Test
    @Order(order = 3)
    public final void viewIncRate() {

        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Rates > Global
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Rates");
        customerNavigation.navigatePageSubTabs("Rates");
        driverWait();
        
        assertEquals("Digital Iris - Settings - Rates : Rate Details", super.driver.getTitle());
        driverWait();
        
        // View Incremental Rate Added 
        super.element = super.selectWebElementByXpath("//li/section/span[contains(., 'Incremental Rate')]");
        driverWait();
        super.element.click();
        driverWait();
        
        // Verify all fields//////////////////////////////
        super.element = super.selectWebElementByXpath("//header/h2[@id='rateName' and contains(., 'Incremental Rate')]");
        driverWait();
        assertEquals("INCREMENTAL RATE", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//h3[@class='detailList' and contains(., 'Details')]");
        driverWait();
        assertEquals("Details", super.element.getText());
        driverWait();
        
        // Verify Rate Name
        super.element = super.selectWebElementByXpath("//dt[@id='detailRateNameLabel' and @class='detailLabel']");
        driverWait();
        assertEquals("Rate Name:", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//dd[@id='detailRateName' and @class='detailValue']");
        driverWait();
        assertEquals("Incremental Rate", super.element.getText());
        driverWait();
        
        // Verify Rate Amount
        super.element = super.selectWebElementByXpath("//dt[@id='detailRateAmountLabel' and @class='detailLabel']");
        driverWait();
        assertEquals("Rate Amount:", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//dd[@id='detailRateAmount' and @class='detailValue']");
        driverWait();
        assertEquals("$6.00", super.element.getText());
        driverWait();
        
        // Verify Minimum Payment
        super.element = super.selectWebElementByXpath("//dt[@id='detailINC-minPaymentLabel' and @class='detailLabel']");
        driverWait();
        assertEquals("Minimum Payment:", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//dd[@id='detailINC-minPayment' and @class='detailValue']");
        driverWait();
        assertEquals("$1.00", super.element.getText());
        driverWait();
        
        // Verify Maximum Payment
        super.element = super.selectWebElementByXpath("//dt[@id='detailINC-maxPaymentLabel' and @class='detailLabel']");
        driverWait();
        assertEquals("Maximum Payment:", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//dd[@id='detailINC-maxPayment' and @class='detailValue']");
        driverWait();
        assertEquals("$60.00", super.element.getText());
        driverWait();
        
        // Verify Flate Rate
        super.element = super.selectWebElementByXpath("//dt[@id='detailINC-flatRateAmountLabel' and @class='detailLabel']");
        driverWait();
        assertEquals("Flat Rate Amount:", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//dd[@id='detailINC-flatRateAmount' and @class='detailValue']");
        driverWait();
        assertEquals("$10.00", super.element.getText());
        driverWait();
        
        // Verify Start Time
        super.element = super.selectWebElementByXpath("//dt[@id='detailINC-startTimeLabel' and @class='detailLabel']");
        driverWait();
        assertEquals("Start Time:", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//dd[@id='detailINC-startTime' and @class='detailValue']");
        driverWait();
        assertEquals("09:00 AM", super.element.getText());
        driverWait();
        
        // Verify End Time
        super.element = super.selectWebElementByXpath("//dt[@id='detailINC-endTimeLabel' and @class='detailLabel']");
        driverWait();
        assertEquals("End Time:", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//dd[@id='detailINC-endTime' and @class='detailValue']");
        driverWait();
        assertEquals("11:00 PM", super.element.getText());
        driverWait();
        
        // Verify Card Increments
        super.element = super.selectWebElementByXpath("//h3[@class='detailList' and contains(., 'Card Increments')]");
        driverWait();
        assertEquals("Card Increments", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@id='detailuseLastIncremental' and @class='details']");
        driverWait();
        assertEquals("Keep Using Last Increment Value", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//ol[@id='detailCardIncremental' and @class='details']");
        driverWait();
        super.element = super.selectWebElementByXpath("//ol[@id='detailCardIncremental' and @class='details']/li[contains(., '10')]");
        driverWait();
        assertEquals("10 min.", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//ol[@id='detailCardIncremental' and @class='details']");
        driverWait();
        super.element = super.selectWebElementByXpath("//ol[@id='detailCardIncremental' and @class='details']/li[contains(., '15')]");
        driverWait();
        assertEquals("15 min.", super.element.getText());
        driverWait();
        
        // Verify Add Time
        super.element = super.selectWebElementByXpath("//h3[@class='detailList' and contains(., 'Add Time')]");
        driverWait();
        assertEquals("Add Time", super.element.getText());
        driverWait();
        
        // Verify Selected Options
        super.element = super.selectWebElementByXpath("//dt[@id='detailAddTimeTypesLabel' and @class='detailLabel']");
        driverWait();
        assertEquals("Selected Options:", super.element.getText());
        driverWait();
        
        // Verify labels
        
        super.element = super.selectWebElementByXpath("//dd[@id='detailAddTimeTypes' and @class='detailValue' and contains(., 'Use Add Time Number')]");
        driverWait();
        super.element = super.selectWebElementByXpath("//dd[@id='detailAddTimeTypes' and @class='detailValue' and contains(., 'Use Space/Plate Number')]");
        driverWait();
        super.element = super.selectWebElementByXpath("//dd[@id='detailAddTimeTypes' and @class='detailValue' and contains(., 'Use Extend By Phone')]");
        driverWait();
        
        // Verify Service Fee
        super.element = super.selectWebElementByXpath("//dd[@id='detailAddTimeTypes' and @class='detailValue']/span[@class='detailLabel']");
        driverWait();
        assertEquals("Service Fee:", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dd[@id='detailAddTimeTypes' and @class='detailValue' and contains(., '1.00')]");
        driverWait();
        
        // Verify Minimum Extension
        super.element = super
                .selectWebElementByXpath("//dd[@id='detailAddTimeTypes' and @class='detailValue']/span[@class='detailLabel' and contains(., 'Minimum Extension:')]");
        driverWait();
        assertEquals("Minimum Extension:", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dd[@id='detailAddTimeTypes' and @class='detailValue' and contains(., '30')]");
        driverWait();
        
        // Verify Rate Type
        super.element = super.selectWebElementByXpath("//dt[@id='detailRateTypeLabel' and @class='detailLabel']");
        driverWait();
        assertEquals("Rate Type:", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dd[@id='detailRateType' and @class='detailValue']");
        driverWait();
        assertEquals("Incremental", super.element.getText());
        
        // Verify Payment Type Accepted
        super.element = super.selectWebElementByXpath("//dt[@id='detailPaymentTypesLabel' and @class='detailLabel']");
        driverWait();
        assertEquals("Payment Type Accepted:", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dd[@id='detailPaymentTypes' and @class='detailValue' and contains(., 'Coins')]");
        driverWait();
        super.element = super.selectWebElementByXpath("//dd[@id='detailPaymentTypes' and @class='detailValue' and contains(., 'Bills')]");
        driverWait();
        super.element = super.selectWebElementByXpath("//dd[@id='detailPaymentTypes' and @class='detailValue' and contains(., 'Credit Cards')]");
        driverWait();
        super.element = super.selectWebElementByXpath("//dd[@id='detailPaymentTypes' and @class='detailValue' and contains(., 'Smart Cards')]");
        driverWait();
        super.element = super.selectWebElementByXpath("//dd[@id='detailPaymentTypes' and @class='detailValue' and contains(., 'Contactless')]");
        driverWait();
        super.element = super.selectWebElementByXpath("//dd[@id='detailPaymentTypes' and @class='detailValue' and contains(., 'Passcards')]");
        driverWait();
        super.element = super.selectWebElementByXpath("//dd[@id='detailPaymentTypes' and @class='detailValue' and contains(., 'Coupons (prompted)')]");
        driverWait();
        
        //logout
        logout();
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 4)
    public final void editIncRate() {

        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Rates > Global
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Rates");
        customerNavigation.navigatePageSubTabs("Rates");
        driverWait();
        
        assertEquals("Digital Iris - Settings - Rates : Rate Details", super.driver.getTitle());
        driverWait();
        
        // The following block of code goes to the edit page of a rate///////////////////
        super.element = super.selectWebElementByXpath("//li[contains(., 'Incremental Rate')]");
        driverWait();
        String objectId = super.element.getAttribute("id").split("_")[1];
        driverWait();
        
        super.element = super.selectWebElementByXpath("//a[contains(@id, objectId) and @class='menuListButton menu']");
        super.element.click();
        Sleeper.sleepTightInSeconds(super.SLEEP_TIME_ONE_SECOND);
        
        super.element = super.selectWebElementByXpath("//a[contains(., objectId) and @class='edit btnEditRoute']");
        driverWait();
        super.element.click();
        Sleeper.sleepTightInSeconds(super.SLEEP_TIME_ONE_SECOND);
        
        // change rate name
        super.element = super.selectWebElementByXpath("//input[@id='formRateName' and @name='wrappedObject.name']");
        driverWait();
        super.element.clear();
        driverWait();
        super.element.sendKeys("Incremental RateEdit");
        driverWait();
        
        // Save the form
        super.element = super.selectWebElementByXpath("//a[@class='linkButtonFtr textButton save' and @title='Save']");
        driverWait();
        super.element.click();
        driverWait();
        
        //logout
        super.logout();
        driverWait();
        
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 5)
    public final void deleteIncRate() {

        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Rates > Global
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Rates");
        customerNavigation.navigatePageSubTabs("Rates");
        driverWait();
        
        assertEquals("Digital Iris - Settings - Rates : Rate Details", super.driver.getTitle());
        driverWait();
        
        // The following block of code goes to the delete page of a rate///////////////////
        super.element = super.selectWebElementByXpath("//li[contains(., 'Incremental RateEdit')]");
        driverWait();
        String objectId = super.element.getAttribute("id").split("_")[1];
        driverWait();
        
        super.element = super.selectWebElementByXpath("//a[contains(@id, objectId) and @class='menuListButton menu']");
        super.element.click();
        Sleeper.sleepTightInSeconds(super.SLEEP_TIME_ONE_SECOND);
        
        super.element = super.selectWebElementByXpath("//a[contains(., objectId) and @class='delete btnDeleteRoute']");
        driverWait();
        super.element.click();
        Sleeper.sleepTightInSeconds(super.SLEEP_TIME_ONE_SECOND);
        
        super.element = super.selectWebElementByXpath("//div[@class='ui-dialog-buttonset']/button/span[contains(., 'Delete')]");
        driverWait();
        super.element.click();
        Sleeper.sleepTightInSeconds(super.SLEEP_TIME_ONE_SECOND);
        driverWait();
        
        //logout
        logout();
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
        
    }
    
}
