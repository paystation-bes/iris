package com.digitalpaytech.automation.customer.collections.recentcollections;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.digitalpaytech.automation.Order;
import com.digitalpaytech.automation.OrderedRunner;
import com.digitalpaytech.automation.collections.recentcollections.RecentCollectionsSuiteTest;

@RunWith(OrderedRunner.class)
public class RecentCollectionsSuiteStandaloneUserTest extends RecentCollectionsSuiteTest {
    @Before
    public final void setUp() {
        genericSetUp("Collections.xml", "standalone");
    }
    
    @After
    public final void tearDown() {
        genericTearDown();
    }
    
    /*@Test
    @Order(order = 1)
    public final void testStandaloneRecentCollectionsCollectionsCenterListChrome() {
        recentCollectionsCollectionsCenterListTest("chrome");
    }
    
    @Test
    @Order(order = 2)
    public final void testStandaloneRecentCollectionsPayStationDetailsChrome() {
        recentCollectionsPayStationDetailsTest("chrome");
    }
    
    @Test
    @Order(order = 3)
    public final void testStandaloneRecentCollectionsCollectionsCenterMapChrome() {
        recentCollectionsCollectionsCenterMapTest("chrome");
    }*/
    
    @Test
    @Order(order = 4)
    public final void testStandaloneRecentCollectionsCollectionsCenterListFirefox() {
        recentCollectionsCollectionsCenterListTest("firefox");
    }
    
    @Test
    @Order(order = 5)
    public final void testStandaloneRecentCollectionsPayStationDetailsFirefox() {
        recentCollectionsPayStationDetailsTest("firefox");
    }
    
    /*@Test
    @Order(order = 6)
    public final void testStandaloneRecentCollectionsCollectionsCenterMapFirefox() {
        recentCollectionsCollectionsCenterMapTest("firefox");
    }
    
    @Test
    @Order(order = 7)
    public final void testStandaloneRecentCollectionsCollectionsCenterListInternetExplorer() {
        recentCollectionsCollectionsCenterListTest("ie");
    }
    
    @Test
    @Order(order = 8)
    public final void testStandaloneRecentCollectionsPayStationDetailsInternetExplorer() {
        recentCollectionsPayStationDetailsTest("ie");
    }
    
    @Test
    @Order(order = 9)
    public final void testStandaloneRecentCollectionsCollectionsCenterMapInternetExplorer() {
        recentCollectionsCollectionsCenterMapTest("ie");
    }*/
}
