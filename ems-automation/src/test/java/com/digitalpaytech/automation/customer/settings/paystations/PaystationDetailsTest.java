package com.digitalpaytech.automation.customer.settings.paystations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.digitalpaytech.automation.AutomationGlobalsTest;
import com.digitalpaytech.automation.Order;
import com.digitalpaytech.automation.OrderedRunner;
import com.digitalpaytech.automation.customer.navigation.CustomerNavigation;

@RunWith(OrderedRunner.class)
public class PaystationDetailsTest extends AutomationGlobalsTest {
    
    // defining a set of constants to make the process data-driven.
    
    private CustomerNavigation customerNavigation;
    
    @Before
    public void setUp() throws Exception {
        
        try {
            final DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            //final Document document = docBuilder.parse(new File(".///SeleniumMaven///Data.xml"));
            final Document document = docBuilder.parse(this.getClass().getClassLoader().getResourceAsStream("Data.xml"));
            
            //normalizing text representation
            document.getDocumentElement().normalize();
            
            // Obtaining URL
            final NodeList urlNodeList = document.getElementsByTagName("URL");
            //Node UrlNode = UrlNodeList.item(0);
            final Element url = (Element) urlNodeList.item(0);
            this.testUrl = url.getTextContent();
            
            // Obtaining Child Username
            final NodeList usernameNodeListChild = document.getElementsByTagName("ChildUsername");
            final Element usernameChild = (Element) usernameNodeListChild.item(0);
            this.testUsernameChild = usernameChild.getTextContent();
            
            // Obtaining Child Password
            final NodeList passwordNodeListChild = document.getElementsByTagName("ChildPassword");
            final Element passwordChild = (Element) passwordNodeListChild.item(0);
            this.testPasswordChild = passwordChild.getTextContent();
            
            // Obtaining Standalone Username
            final NodeList usernameNodeListStandAlone = document.getElementsByTagName("StandAloneUsername");
            final Element usernameSA = (Element) usernameNodeListStandAlone.item(0);
            this.testUsernameStandalone = usernameSA.getTextContent();
            
            // Obtaining Standalone Password
            final NodeList passwordNodeListStandAlone = document.getElementsByTagName("StandAlonePassword");
            final Element passwordSA = (Element) passwordNodeListStandAlone.item(0);
            this.testPasswordStandalone = passwordSA.getTextContent();
            
            // Obtaining Parent Username
            final NodeList usernameNodeListParent = document.getElementsByTagName("ParentUsername");
            final Element usernameParent = (Element) usernameNodeListParent.item(0);
            this.testUsernameParent = usernameParent.getTextContent();
            
            // Obtaining Parent Password
            final NodeList passwordNodeListParent = document.getElementsByTagName("ParentPassword");
            final Element passwordParent = (Element) passwordNodeListParent.item(0);
            this.testPasswordParent = passwordParent.getTextContent();
            
            /// Obtaining System Admin Username
            final NodeList usernameNodeListSystemAdmin = document.getElementsByTagName("SystemAdminUsrename");
            final Element usernameSystemAdmin = (Element) usernameNodeListSystemAdmin.item(0);
            this.testUsernameSystemAdmin = usernameSystemAdmin.getTextContent();
            
            /// Obtaining Digital Collect Username
            final NodeList usernameNodeListCollectChild = document.getElementsByTagName("CollectChildUsername");
            final Element usernameCollectChild = (Element) usernameNodeListCollectChild.item(0);
            this.testUsernameCollectChild = usernameCollectChild.getTextContent();
            
            /// Obtaining Digital Collect Username
            final NodeList usernameNodeListCollectStandalone = document.getElementsByTagName("CollectSAUsername");
            final Element usernameCollectStandalone = (Element) usernameNodeListCollectStandalone.item(0);
            this.testUsernameCollectStandalone = usernameCollectStandalone.getTextContent();
            
            // Obtaining System Admin Password
            final NodeList passwordNodeListSystemAdmin = document.getElementsByTagName("SystemAdminPassword");
            final Element passwordSystemAdmin = (Element) passwordNodeListSystemAdmin.item(0);
            this.testPasswordSystemAdmin = passwordSystemAdmin.getTextContent();
            
            // Obtaining default password = "password"
            final NodeList defaultPasswordNodeListSystemAdmin = document.getElementsByTagName("DefaultPassword");
            final Element defaultPasswordSystemAdmin = (Element) defaultPasswordNodeListSystemAdmin.item(0);
            this.testPasswordDefault = defaultPasswordSystemAdmin.getTextContent();
            
            // Obtaining Delay
            final NodeList delayList = document.getElementsByTagName("DriverDelayMilliSecond");
            final Element webDriverDelay = (Element) delayList.item(0);
            this.testDriverDelay = webDriverDelay.getTextContent();
            
            // Obtaining Browser Type
            final NodeList browserList = document.getElementsByTagName("BrowserType");
            final Element browserType = (Element) browserList.item(0);
            this.browserType = browserType.getTextContent();
            
            // launching specific browser type
            launchBrowser();
            this.customerNavigation = new CustomerNavigation(driver);
            
        }
        
        catch (SAXParseException err) {
            //            System.out.println("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId());
            //            System.out.println(" " + err.getMessage());
            
        } catch (SAXException e) {
            final Exception x = e.getException();
            ((x == null) ? e : x).printStackTrace();
            
        } catch (Throwable t) {
            t.printStackTrace();
        }
        
    }
    
    @After
    public void tearDown() throws Exception {
        
        super.driver.quit();
        //driver.close();
    }
    
    @Test
    @Order(order = 1)
    public final void paystationListCurrentStatusTest() {
        
        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Pay Stations > Pay Station List
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Pay Stations");
        customerNavigation.navigatePageSubTabs("Pay Station List");
        
        assertEquals("Digital Iris - Settings - Pay Stations : Pay Station List", super.driver.getTitle());
        driverWait();
        
        // Verifying static contents
        super.element = super.driver.findElement(By.xpath("//section[@class='groupBox menuBox paystationList']/header/h2"));
        assertEquals("PAY STATIONS", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@class='filterHeader activityLog horizontal']/section[@class='title' and contains(., 'SEARCH:')]"));
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//label[@for='locationPOSAC']"));
        assertEquals("Pay Station / Location / Route", super.element.getText());
        driverWait();
        
        // click on the unassigned pay station
        super.selectOptionFromDropDown("locationPOSAC", "Unassigned");
        driverWait();
        //super.element = super.driver.findElement(By.xpath("//li[@class='actv-true hddn-false']/section[@class='severityNull' ]"));
        super.element = super.driver.findElement(By.xpath("//li/section"));
        super.element.click();
        //super.selectOptionFromDropDown("locationPOSAC", "827300000700");
        driverWait();
        
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // The following block of code checks to see if all texts, links and buttons are placed as expected in UI.
        
        //verifying text exists
        super.element = super.driver.findElement(By.xpath("//h2[@id='detailHeading']"));
        assertEquals(true, super.element.isDisplayed());
        assertEquals("PAY STATION DETAILS", super.element.getText());
        driverWait();
        
        //verifying Edit button exists
        super.element = super.driver.findElement(By.xpath("//a[@id='opBtn_pageContent' and @class='hdrButtonIcn menu' and @title='Option Menu']"));
        assertEquals(true, super.element.isDisplayed());
        driverWait();
        
        // verifying that edit button exist
        super.element = super.driver.findElement(By.xpath("//a[@id='opBtn_batteryVoltage' and @class='hdrButtonIcn menu']"));
        assertEquals(true, super.element.isDisplayed());
        driverWait();
        
        // verifying that edit button exist
        super.element = super.driver.findElement(By.xpath("//a[@id='opBtn_inputCurrent' and @class='hdrButtonIcn menu']"));
        assertEquals(true, super.element.isDisplayed());
        driverWait();
        
        // verifying that edit button exist
        super.element = super.driver.findElement(By.xpath("//a[@id='opBtn_ControllerTemperature' and @class='hdrButtonIcn menu']"));
        assertEquals(true, super.element.isDisplayed());
        driverWait();
        
        // verifying that edit button exist
        super.element = super.driver.findElement(By.xpath("//a[@id='opBtn_relativeHumidity' and @class='hdrButtonIcn menu']"));
        assertEquals(true, super.element.isDisplayed());
        driverWait();
        
        //verifying text exists
        super.element = super.driver.findElement(By.xpath("//a[@class='tab' and @title='Current Status']"));
        assertEquals(true, super.element.isDisplayed());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//article[@id='statusArea']/section[@id='psLastSeen']/section[@class='title']"));
        assertEquals("LAST SEEN:", super.element.getText().trim());
        driverWait();
        
        //verifying text exists
        super.element = super.driver.findElement(By.xpath("//section[@id='psLastSeen']/section[@class='title']/img[@title='filter']"));
        assertEquals(true, super.element.isDisplayed());
        driverWait();
        
        //verifying BATTERY VOLTAGE section
        super.element = super.driver.findElement(By.xpath("//section[@id='batteryVoltageWdgt']/header/span[@class='wdgtName']"));
        assertEquals("BATTERY VOLTAGE", super.element.getText());
        driverWait();
        super.element = super.driver
                .findElement(By
                        .xpath("//section[@class='col1']/section[@id='batteryVoltageWdgt']/section[@class='groupBoxBody']/section[@class='snglValue']/h1[@class='valueDisplay']"));
        assertEquals("N/AV", super.element.getText().trim());
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='col1']/section[@id='batteryVoltageWdgt']/section[@class='smallHeading']"));
        assertEquals("Recent Sensor Data", super.element.getText().trim());
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@class='col1']/section[@id='batteryVoltageWdgt']/section[@id='batteryVoltageGraph']/section[@class='noWdgtData']/span"));
        assertEquals("Sorry, no history is available for this sensor.", super.element.getText().trim());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//a[@id='opBtn_batteryVoltage']"));
        driverWait();
        super.element.click();
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='menuOptions innerBorder']/a[@id='batteryVoltageReload']"));
        driverWait();
        assertEquals("RELOAD", super.element.getText().trim());
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='menuOptions innerBorder']/a[@id='batteryVoltageHistory']"));
        driverWait();
        assertEquals("EXPORT HISTORY AS .CSV", super.element.getText().trim());
        driverWait();
        
        //verifying CHARGING CURRENT section
        super.element = super.driver.findElement(By.xpath("//section[@id='inputCurrentWdgt']/header/span[@class='wdgtName']"));
        assertEquals("CHARGING CURRENT", super.element.getText());
        driverWait();
        super.element = super.driver
                .findElement(By
                        .xpath("//section[@class='col2']/section[@id='inputCurrentWdgt']/section[@class='groupBoxBody']/section[@class='snglValue']/h1[@class='valueDisplay']"));
        assertEquals("N/AmA", super.element.getText().trim());
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='col2']/section[@id='inputCurrentWdgt']/section[@class='smallHeading']"));
        assertEquals("Recent Sensor Data", super.element.getText().trim());
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@class='col2']/section[@id='inputCurrentWdgt']/section[@id='inputCurrentGraph']/section[@class='noWdgtData']/span"));
        assertEquals("Sorry, no history is available for this sensor.", super.element.getText().trim());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//a[@id='opBtn_inputCurrent']"));
        driverWait();
        super.element.click();
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='menuOptions innerBorder']/a[@id='inputCurrentReload']"));
        driverWait();
        assertEquals("RELOAD", super.element.getText().trim());
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='menuOptions innerBorder']/a[@id='inputCurrentHistory']"));
        driverWait();
        assertEquals("EXPORT HISTORY AS .CSV", super.element.getText().trim());
        driverWait();
        
        //verifying TEMPERATURE section
        super.element = super.driver.findElement(By.xpath("//section[@id='ControllerTemperatureWdgt']/header/span[@class='wdgtName']"));
        assertEquals("TEMPERATURE", super.element.getText());
        driverWait();
        super.element = super.driver
                .findElement(By
                        .xpath("//section[@class='col1']/section[@id='ControllerTemperatureWdgt']/section[@class='groupBoxBody']/section[@class='snglValue']/h1[@class='valueDisplay']"));
        assertEquals("N/A� C", super.element.getText().trim());
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='col1']/section[@id='ControllerTemperatureWdgt']/section[@class='smallHeading']"));
        assertEquals("Recent Sensor Data", super.element.getText().trim());
        driverWait();
        super.element = super.driver
                .findElement(By
                        .xpath("//section[@class='col1']/section[@id='ControllerTemperatureWdgt']/section[@id='ControllerTemperatureGraph']/section[@class='noWdgtData']/span"));
        assertEquals("Sorry, no history is available for this sensor.", super.element.getText().trim());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//a[@id='opBtn_ControllerTemperature']"));
        driverWait();
        super.element.click();
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='menuOptions innerBorder']/a[@id='ControllerTemperatureReload']"));
        driverWait();
        assertEquals("RELOAD", super.element.getText().trim());
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='menuOptions innerBorder']/a[@id='ControllerTemperatureHistory']"));
        driverWait();
        assertEquals("EXPORT HISTORY AS .CSV", super.element.getText().trim());
        driverWait();
        
        //verifying RELATIVE HUMIDITY section
        super.element = super.driver.findElement(By.xpath("//section[@id='relativeHumidityWdgt']/header/span[@class='wdgtName']"));
        assertEquals("RELATIVE HUMIDITY", super.element.getText());
        driverWait();
        super.element = super.driver
                .findElement(By
                        .xpath("//section[@class='col2']/section[@id='relativeHumidityWdgt']/section[@class='groupBoxBody']/section[@class='snglValue']/h1[@class='valueDisplay']"));
        assertEquals("N/A%", super.element.getText().trim());
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='col2']/section[@id='relativeHumidityWdgt']/section[@class='smallHeading']"));
        assertEquals("Recent Sensor Data", super.element.getText().trim());
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@class='col2']/section[@id='relativeHumidityWdgt']/section[@id='relativeHumidityGraph']/section[@class='noWdgtData']/span"));
        assertEquals("Sorry, no history is available for this sensor.", super.element.getText().trim());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//a[@id='opBtn_relativeHumidity']"));
        driverWait();
        super.element.click();
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='menuOptions innerBorder']/a[@id='relativeHumidityReload']"));
        driverWait();
        assertEquals("RELOAD", super.element.getText().trim());
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='menuOptions innerBorder']/a[@id='relativeHumidityHistory']"));
        driverWait();
        assertEquals("EXPORT HISTORY AS .CSV", super.element.getText().trim());
        driverWait();
        
        // next is to do an export and reload and sub reload
        // also the main eidt button to the right of pay station details
        
        //logout
        this.element = super.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
        
    }
    
    @Test
    @Order(order = 2)
    public final void paystationRecentActivityTest() {
        
        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Pay Stations > Pay Station List
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Pay Stations");
        customerNavigation.navigatePageSubTabs("Pay Station List");
        
        assertEquals("Digital Iris - Settings - Pay Stations : Pay Station List", super.driver.getTitle());
        driverWait();
        
        // click on the unassigned pay station
        super.selectOptionFromDropDown("locationPOSAC", "Unassigned");
        driverWait();
        //super.element = super.driver.findElement(By.xpath("//li[@class='actv-true hddn-false']/section[@class='severityNull' ]"));
        super.element = super.driver.findElement(By.xpath("//li/section"));
        super.element.click();
        //super.selectOptionFromDropDown("locationPOSAC", "827300000700");
//        super.element = super.driver.findElement(By.xpath("//li[@class='actv-true hddn-false']/section[@class='severityNull' ]"));
//        super.element.click();
        driverWait();
        
        assertEquals("Digital Iris - Settings - Pay Stations : Pay Station List", super.driver.getTitle());
        driverWait();
        
        // Go to Recent Activity page
        super.element = super.driver.findElement(By
                .xpath("//section[@id='paystationNavArea']/nav[@id='psNav']/div[@id='alertsBtn']/a[@title='Recent Activity']"));
        super.element.click();
        driverWait();
        
        // The following tests validate the existence of all objects
        
        // verifying FILTER text exists
        super.element = super.driver.findElement(By.xpath("//article[@id='alertsArea']/section[@id='alertFilterBox']/section[@class='title']"));
        driverWait();
        assertEquals("FILTER:", super.element.getText());
        driverWait();
        
        // verifying all "Status" types are available
        super.element = super.driver.findElement(By.xpath("//section[@class='filterTitle']/label[@for='filterUser' and contains(., 'Status:')]"));
        driverWait();
        assertEquals("Status:", super.element.getText());
        super.element.click();
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedStatusList']/li[@id='All']"));
        driverWait();
        assertEquals("All", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedStatusList']/li[@id='1']"));
        driverWait();
        assertEquals("Active", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedStatusList']/li[@id='0']"));
        driverWait();
        assertEquals("Cleared", super.element.getText());
        driverWait();
        
        // verifying all "Severity" types are available
        super.element = super.driver.findElement(By.xpath("//section[@class='filterTitle']/label[@for='filterUser' and contains(., 'Severity:')]"));
        driverWait();
        assertEquals("Severity:", super.element.getText());
        super.element.click();
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedSeverityList']/li[@id='All']"));
        driverWait();
        assertEquals("All", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedSeverityList']/li[contains(., 'Minor')]"));
        driverWait();
        assertEquals("Minor", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedSeverityList']/li[contains(., 'Major')]"));
        driverWait();
        assertEquals("Major", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedSeverityList']/li[contains(., 'Critical')]"));
        driverWait();
        assertEquals("Critical", super.element.getText());
        driverWait();
        
        // verifying all "Module" types are available
        super.element = super.driver.findElement(By.xpath("//section[@class='filterTitle']/label[@for='filterUser' and contains(., 'Module:')]"));
        driverWait();
        assertEquals("Module:", super.element.getText());
        super.element.click();
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedModuleList']/li[@id='All']"));
        driverWait();
        assertEquals("All", super.element.getText().trim());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedModuleList']/li[contains(., 'Battery')]"));
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedModuleList']/li[contains(., 'Bill Acceptor')]"));
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedModuleList']/li[contains(., 'Bill Stacker')]"));
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedModuleList']/li[contains(., 'Card Reader')]"));
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedModuleList']/li[contains(., 'Coin Acceptor')]"));
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedModuleList']/li[contains(., 'Coin Hopper 1')]"));
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedModuleList']/li[contains(., 'Coin Hopper 2')]"));
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedModuleList']/li[contains(., 'Door')]"));
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedModuleList']/li[contains(., 'Door - Lower')]"));
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedModuleList']/li[contains(., 'Door - Upper')]"));
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedModuleList']/li[contains(., 'Pay Station')]"));
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedModuleList']/li[contains(., 'Printer')]"));
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedModuleList']/li[contains(., 'Shock Alarm')]"));
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedModuleList']/li[contains(., 'Coin Canister')]"));
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedModuleList']/li[contains(., 'Maintenance Door')]"));
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedModuleList']/li[contains(., 'Cash Vault Door')]"));
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedModuleList']/li[contains(., 'Coin Escrow')]"));
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@class='filterMenu']/ul[@id='selectedModuleList']/li[contains(., 'Customer Defined Alert')]"));
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedModuleList']/li[contains(., 'Modem')]"));
        driverWait();
        super.element = super.driver.findElement(By.xpath("//section[@class='filterMenu']/ul[@id='selectedModuleList']/li[contains(., 'Rfid Card Reader')]"));
        driverWait();
        
        // verifying the buttons
        super.element = super.driver.findElement(By.xpath("//a[@id='posActivityExport']"));
        driverWait();
        assertEquals("EXPORT", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//article[@id='alertsArea']/a[@class='linkButton reload right clear refreshPOSCurrentTab']"));
        driverWait();
        assertEquals("RELOAD", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//a[@id='posActivityExport' and @class='linkButton export right clear']"));
        driverWait();
        assertEquals("EXPORT", super.element.getText());
        driverWait();
        
        ////////////////////////////////////////////////////////////////////
        // we need to validate the two EXPORT button functionalities as well
        ////////////////////////////////////////////////////////////////////
        
        // verifying legends
        super.element = super.driver.findElement(By.xpath("//span[@class='severityLegend note']"));
        driverWait();
        assertEquals("Severity Level:  Minor  Major  Critical", super.element.getText().trim());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//span[@class='severityLegend note']/div[@class='severityLow']"));
        driverWait();
        //        assertEquals("Minor", super.element.getText().trim());
        //        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//span[@class='severityLegend note']/div[@class='severityMedium']"));
        driverWait();
        //        assertEquals("Major", super.element.getText().trim());
        //        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//span[@class='severityLegend note']/div[@class='severityHigh']"));
        driverWait();
        //        assertEquals("Critical", super.element.getText().trim());
        //        driverWait();
        
        // verifying table
        super.element = super.driver.findElement(By.xpath("//ul[@id='crntAlertListHeader']/li/div[@class='col1 clickable sort']"));
        driverWait();
        assertEquals("Recent Activity", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//ul[@id='crntAlertListHeader']/li/div[@class='col2 clickable sort']"));
        driverWait();
        assertEquals("Alert Date", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//ul[@id='crntAlertListHeader']/li/div[@class='col3 clickable sort']"));
        driverWait();
        assertEquals("Resolved Date", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//ul[@id='crntAlertListHeader']/li/div[@class='col4 current clickable sort']"));
        driverWait();
        assertEquals("Status", super.element.getText());
        driverWait();
        
        //logout
        this.element = super.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
        
    }
    
    @Test
    @Order(order = 3)
    public final void paystationInformationTest() {
        
        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Pay Stations > Pay Station List
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Pay Stations");
        customerNavigation.navigatePageSubTabs("Pay Station List");
        
        assertEquals("Digital Iris - Settings - Pay Stations : Pay Station List", super.driver.getTitle());
        driverWait();
        
        // click on the unassigned pay station
        super.selectOptionFromDropDown("locationPOSAC", "Unassigned");
        driverWait();
        //super.element = super.driver.findElement(By.xpath("//li[@class='actv-true hddn-false']/section[@class='severityNull' ]"));
        super.element = super.driver.findElement(By.xpath("//li/section"));
        super.element.click();
        //super.selectOptionFromDropDown("locationPOSAC", "827300000700");
        driverWait();
        
        assertEquals("Digital Iris - Settings - Pay Stations : Pay Station List", super.driver.getTitle());
        driverWait();
        
        // Go to Recent Activity page
        super.element = super.driver.findElement(By
                .xpath("//section[@id='paystationNavArea']/nav[@id='psNav']/div[@id='infoBtn']/a[@class='tab' and @title='Information']"));
        driverWait();
        super.element.click();
        driverWait();
        
        // The following tests validate the existence of all objects
        super.element = super.driver.findElement(By.xpath("//article[@id='infoArea']/dl[@class='details']/section[@id='psNameLine']/dt[@class='detailLabel']"));
        driverWait();
        assertEquals("Name:", super.element.getText());
        driverWait();
        super.element = super.driver.findElement(By.xpath("//article[@id='infoArea']/dl[@class='details']/section[@id='psNameLine']/dd[@id='psName']"));
        driverWait();
        assertEquals("827300000700", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//article[@id='infoArea']/dl[@class='details']/section[@id='psSerialNumberLine']/dt[@class='detailLabel']"));
        driverWait();
        assertEquals("Serial Number:", super.element.getText());
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//article[@id='infoArea']/dl[@class='details']/section[@id='psSerialNumberLine']/dd[@id='psSerialNumber']"));
        driverWait();
        assertEquals("827300000700", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//article[@id='infoArea']/dl[@class='details']/section[@id='psTypeLine']/dt[@class='detailLabel']"));
        driverWait();
        assertEquals("Type:", super.element.getText());
        driverWait();
        super.element = super.driver.findElement(By.xpath("//article[@id='infoArea']/dl[@class='details']/section[@id='psTypeLine']/dd[@id='psType']"));
        driverWait();
        assertEquals("Test/Demo", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//article[@id='infoArea']/dl[@class='details']/section[@id='psStatusLine']/dt[@class='detailLabel']"));
        driverWait();
        assertEquals("Status:", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//article[@id='infoArea']/dl[@class='details']/section[@id='psLocationLine']/dt[@class='detailLabel']"));
        driverWait();
        assertEquals("Location:", super.element.getText());
        driverWait();
        super.element = super.driver.findElement(By.xpath("//article[@id='infoArea']/dl[@class='details']/section[@id='psLocationLine']/dd[@id='psLocation']"));
        driverWait();
        assertEquals("Unassigned", super.element.getText());
        driverWait();
        super.element = super.driver.findElement(By.xpath("//a[contains(., 'Unassigned')]"));
        driverWait();
        super.element.click();
        
        // link to location is clicked.  Now, we are in location page
        super.element = super.driver.findElement(By.xpath("//header/h2[@id='locationName']"));
        driverWait();
        assertEquals("UNASSIGNED", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//section[@class='isUnassignedLabel col1']/h3[@class='detailList']"));
        driverWait();
        assertEquals("Details", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//dl[@class='details']/dt[@class='detailLabel' and contains(., 'Name:')]"));
        driverWait();
        //assertEquals("Name:", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//dl[@class='details']/dd[@class='detailValue UnassignedName']"));
        driverWait();
        assertEquals("Unassigned", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//section[@id='PaystationChildren']/h3[@class='detailList' and contains(., 'Pay Stations')]"));
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//ul[@id='psChildList']/li/a"));
        driverWait();
        assertEquals("827300000700", super.element.getText());
        driverWait();
        
        // click on the pay station
        super.element.click();
        
        assertEquals("Digital Iris - Settings - Pay Stations : Pay Station List", super.driver.getTitle());
        driverWait();
        
        //logout
        this.element = super.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
        
    }
    
    @Test
    @Order(order = 4)
    public final void paystationCollectionTest() {
        
        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Pay Stations > Pay Station List
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Pay Stations");
        customerNavigation.navigatePageSubTabs("Pay Station List");
        
        assertEquals("Digital Iris - Settings - Pay Stations : Pay Station List", super.driver.getTitle());
        driverWait();
        
        // click on the unassigned pay station
        super.selectOptionFromDropDown("locationPOSAC", "Unassigned");
        driverWait();
        //super.element = super.driver.findElement(By.xpath("//li[@class='actv-true hddn-false']/section[@class='severityNull' ]"));
        super.element = super.driver.findElement(By.xpath("//li/section"));
        super.element.click();
        //super.selectOptionFromDropDown("locationPOSAC", "827300000700");
        driverWait();
        
        assertEquals("Digital Iris - Settings - Pay Stations : Pay Station List", super.driver.getTitle());
        driverWait();
        
        // Click on "Reports" tab
        super.element = super.driver.findElement(By
                .xpath("//section[@id='paystationNavArea']/nav[@id='psNav']/div[@id='reportsBtn']/a[@class='tab' and @title='Reports']"));
        driverWait();
        super.element.click();
        driverWait();
        
        // Click on "Collection Reports" tab
        super.element = super.driver.findElement(By.xpath("//a[@id='collectionReportBtn' and @title='Collection Reports']"));
        driverWait();
        super.element.click();
        driverWait();
        
        // The following tests validate the existence of all objects
        super.element = super.driver.findElement(By.xpath("//ul[@id='collectionReportListHeader']/li/div[@name='sort_date']/p"));
        assertEquals("Date", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//ul[@id='collectionReportListHeader']/li/div[@name='sort_type']/p"));
        assertEquals("Type", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//ul[@id='collectionReportListHeader']/li/div[@class='col3 notclickable']/p"));
        assertEquals("Amount", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//article[@id='reportsArea']/a[@class='linkButton reload right clear refreshPOSCurrentTab']"));
        assertEquals("RELOAD", super.element.getText());
        driverWait();
        super.element.click();
        driverWait();
        
        // click on a transaction report and validate the content
        super.element = super.driver.findElement(By.xpath("//li[contains(@id, 'report_')]/div[@class='col2']/p[contains(., 'Card')]"));
        driverWait();
        super.element.click();
        
        super.element = super.driver.findElement(By.xpath("//span[@class='ui-dialog-title']"));
        assertEquals("COLLECTION REPORT", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='collectionDetailView']/article[contains(., 'Location: Unassigned') and contains(., 'Pay Station: 827300000700')]"));
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//section[@id='collectionDetailView']/article/span[@class='note']"));
        assertEquals("827300000700", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Collection Type')]"));
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue' and contains(., 'Card')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Report #')]"));
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue' and contains(., '1')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Collected By')]"));
        driverWait();
        super.element = super.driver
                .findElement(By
                        .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue' and contains(., 'Arash Collect Child')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Collection Date')]"));
        driverWait();
        
        super.element = super.driver
                .findElement(By
                        .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Previous Collection')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Machine Name')]"));
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue' and contains(., '827300000700')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Permit Count')]"));
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue' and contains(., '6')]"));
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/h2"));
        driverWait();
        assertEquals("Credit Cards", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Visa')]"));
        driverWait();
        super.element = super.driver
                .findElement(By
                        .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue dollarDisplay' and contains(., '$2.25')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'MasterCard')]"));
        driverWait();
        super.element = super.driver
                .findElement(By
                        .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue dollarDisplay' and contains(., '$0.00')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Amex')]"));
        driverWait();
        super.element = super.driver
                .findElement(By
                        .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue dollarDisplay' and contains(., '$1.23')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Discover')]"));
        driverWait();
        super.element = super.driver
                .findElement(By
                        .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue dollarDisplay' and contains(., '$0.00')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Diners Club')]"));
        driverWait();
        super.element = super.driver
                .findElement(By
                        .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue dollarDisplay' and contains(., '$0.00')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Smart Cards')]"));
        driverWait();
        super.element = super.driver
                .findElement(By
                        .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue dollarDisplay' and contains(., '$0.00')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Other')]"));
        driverWait();
        super.element = super.driver
                .findElement(By
                        .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue dollarDisplay' and contains(., '$2.75')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Total')]"));
        driverWait();
        super.element = super.driver
                .findElement(By
                        .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue dollarDisplay' and contains(., '$6.23')]"));
        driverWait();
        
        // check and click on CLOSE button
        super.element = super.driver.findElement(By.xpath("//div[@class='ui-dialog-buttonset']/button[@type='button']/span[@class='ui-button-text']"));
        driverWait();
        assertEquals("CLOSE", super.element.getText());
        driverWait();
        super.element = super.driver.findElement(By.xpath("//div[@class='ui-dialog-buttonset']/button[@type='button']"));
        driverWait();
        super.element.click();
        driverWait();
        
        //logout
        this.element = super.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
        
    }
    
    @Test
    @Order(order = 5)
    public final void paystationTransactionTest() {
        
        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Pay Stations > Pay Station List
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Pay Stations");
        customerNavigation.navigatePageSubTabs("Pay Station List");
        
        assertEquals("Digital Iris - Settings - Pay Stations : Pay Station List", super.driver.getTitle());
        driverWait();
        
        // click on the unassigned pay station
        super.selectOptionFromDropDown("locationPOSAC", "500000080001");
        //        driverWait();
        //        super.element = super.driver.findElement(By.xpath("//section[@class='severityNull']/span[@class='container' and contains(., '827300000700')]"));
        //super.element.click();
        driverWait();
        
        assertEquals("Digital Iris - Settings - Pay Stations : Pay Station List", super.driver.getTitle());
        driverWait();
        
        // Click on "Reports" tab
        super.element = super.driver.findElement(By
                .xpath("//section[@id='paystationNavArea']/nav[@id='psNav']/div[@id='reportsBtn']/a[@class='tab' and @title='Reports']"));
        driverWait();
        super.element.click();
        driverWait();
        
        // Click on "Transaction Reports" tab
        super.element = super.driver.findElement(By.xpath("//a[@id='transactionReportBtn' and @title='Transaction Reports']"));
        driverWait();
        super.element.click();
        driverWait();
        
        // The following tests validate the existence of all objects
        super.element = super.driver.findElement(By.xpath("//ul[@id='transactionReportListHeader']/li/div[@name='sort_date']/p"));
        assertEquals("Date", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//ul[@id='transactionReportListHeader']/li/div[@name='sort_type']/p"));
        assertEquals("Type", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//ul[@id='transactionReportListHeader']/li/div[@name='sort_paymentType']/p"));
        assertEquals("Payment Type", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//ul[@id='transactionReportListHeader']/li/div[@class='col4 notclickable']/p"));
        assertEquals("Amount", super.element.getText());
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//article[@id='reportsArea']/a[@class='linkButton reload right clear refreshPOSCurrentTab']"));
        assertEquals("RELOAD", super.element.getText());
        driverWait();
        
        // click on a transaction report and validate the content
        super.element = super.driver.findElement(By.xpath("//li[contains(@id, 'report_')]/div[@class='col3']/p[contains(., 'Cash')]"));
        driverWait();
        super.element.click();
        
        super.element = super.driver.findElement(By.xpath("//span[@class='ui-dialog-title']"));
        assertEquals("TRANSACTION DETAILS", super.element.getText().trim());
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/article[contains(., 'Location: Downtown') and contains(., 'Pay Station: 500000080001')]"));
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//section[@id='transactionDetailView']/article/span[@class='note']"));
        assertEquals("500000080001", super.element.getText().trim());
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Transaction #')]"));
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue' and contains(., '1')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Setting')]"));
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue' and contains(., 'None')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Space #')]"));
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue' and contains(., 'N/A')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'License Plate #')]"));
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue' and contains(., 'N/A')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Type')]"));
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue' and contains(., 'Regular')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Payment Type')]"));
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue' and contains(., 'Cash')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Add Time Number')]"));
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue' and contains(., 'N/A')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Purchase Date')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Expiry Date')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Charged Amount')]"));
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue dollarDisplay' and contains(., '$13.50')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Charged Amount')]"));
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue dollarDisplay' and contains(., '$13.50')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Cash Paid')]"));
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue dollarDisplay' and contains(., '$13.50')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Card Paid')]"));
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue dollarDisplay' and contains(., '$0.00')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Smart Card Paid')]"));
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue dollarDisplay' and contains(., '$0.00')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Passcard Paid')]"));
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue dollarDisplay' and contains(., '$0.00')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Excess Payment')]"));
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue dollarDisplay' and contains(., '$0.00')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Change Dispensed')]"));
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue dollarDisplay' and contains(., '$0.00')]"));
        driverWait();
        
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dt[@class='detailLabel' and contains(., 'Refund Slip')]"));
        driverWait();
        super.element = super.driver.findElement(By
                .xpath("//section[@id='transactionDetailView']/section[@class='reportBody']/dl/dd[@class='detailValue dollarDisplay' and contains(., 'N/A')]"));
        driverWait();
        
        // check and click on CLOSE button
        super.element = super.driver.findElement(By.xpath("//div[@class='ui-dialog-buttonset']/button[@type='button']/span[@class='ui-button-text']"));
        driverWait();
        assertEquals("CLOSE", super.element.getText());
        driverWait();
        super.element = super.driver.findElement(By.xpath("//div[@class='ui-dialog-buttonset']/button[@type='button']"));
        driverWait();
        super.element.click();
        driverWait();
        
        //logout
        this.element = super.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
        
    }
    
    @Test
    @Order(order = 6)
    public final void paystationEditTest() {
        
        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Pay Stations > Pay Station List
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Pay Stations");
        customerNavigation.navigatePageSubTabs("Pay Station List");
        
        assertEquals("Digital Iris - Settings - Pay Stations : Pay Station List", super.driver.getTitle());
        driverWait();
        
        // click on a pay station to edit it
        super.element = super.selectWebElementByXpath("//li[@class='actv-true hddn-false']/section/span[contains(., '500000080002')]");
        driverWait();
        final String objectId = super.element.getAttribute("id").split("_")[0];
        driverWait();
        super.element.click();
        
        // select the pay station
        super.element = super.selectWebElementByXpath("//li[contains(@id, '" + objectId
                                                      + "') and @class='actv-true hddn-false selected']/a[contains(@id, '" + objectId
                                                      + "') and @title='Edit']");
        super.element.click();
        Sleeper.sleepTightInSeconds(super.SLEEP_TIME_ONE_SECOND);
        driverWait();
        
        // Edit the pay station and save the form*/
        //assertTrue(true);
        
        
        
        
    }
    
}
