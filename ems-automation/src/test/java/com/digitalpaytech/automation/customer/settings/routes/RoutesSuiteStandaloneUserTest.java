package com.digitalpaytech.automation.customer.settings.routes;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.digitalpaytech.automation.Order;
import com.digitalpaytech.automation.OrderedRunner;
import com.digitalpaytech.automation.settings.routes.RoutesSuiteTest;

@RunWith(OrderedRunner.class)
public class RoutesSuiteStandaloneUserTest extends RoutesSuiteTest {
    @Before
    public final void setUp() {
        super.setUpRoutesTest("standalone");
    }
    
    @After
    public final void tearDown() {
        super.tearDownRoutesTest();
    }

    /*@Test
    @Order(order = 1)
    public final void testStandaloneAddRouteChrome() {
        super.addRouteTest("chrome");
    }
    
    @Test
    @Order(order = 2)
    public final void testStandaloneEditRouteChrome() {
        super.editRouteTest("chrome");
    }
    
    @Test
    @Order(order = 3)
    public final void testStandaloneDeleteRouteChrome() {
        super.deleteRouteTest("chrome");
    }*/
    
    @Test
    @Order(order = 4)
    public final void testStandaloneAddRouteFirefox() {
        super.addRouteTest("firefox");
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 5)
    public final void testStandaloneEditRouteFirefox() {
        super.editRouteTest("firefox");
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 6)
    public final void testStandaloneDeleteRouteFirefox() {
        super.deleteRouteTest("firefox");
        //assertTrue(true);
    }
    
    /*@Test
    @Order(order = 7)
    public final void testStandaloneAddRouteInternetExplorer() {
        super.addRouteTest("ie");
    }
    
    @Test
    @Order(order = 8)
    public final void testStandaloneEditRouteInternetExplorer() {
        super.editRouteTest("ie");
    }
    
    @Test
    @Order(order = 9)
    public final void testStandaloneDeleteRouteInternetExplorer() {
        super.deleteRouteTest("ie");
    }*/
}
