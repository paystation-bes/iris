package com.digitalpaytech.automation.customer.settings.locations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.digitalpaytech.automation.AutomationGlobalsTest;
import com.digitalpaytech.automation.Order;
import com.digitalpaytech.automation.OrderedRunner;
import com.digitalpaytech.automation.customer.navigation.CustomerNavigation;

@RunWith(OrderedRunner.class)
public class LocationsTest extends AutomationGlobalsTest {
    
    private CustomerNavigation customerNavigation = new CustomerNavigation();
    
    @Before
    public void setUp() throws Exception {
        
        try {
            final DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            //final Document document = docBuilder.parse(new File(".///SeleniumMaven///Data.xml"));
            final Document document = docBuilder.parse(this.getClass().getClassLoader().getResourceAsStream("Data.xml"));
            
            //normalizing text representation
            document.getDocumentElement().normalize();
            
            // Obtaining URL
            final NodeList urlNodeList = document.getElementsByTagName("URL");
            //Node UrlNode = UrlNodeList.item(0);
            final Element url = (Element) urlNodeList.item(0);
            this.testUrl = url.getTextContent();
            
            // Obtaining Child Username
            final NodeList usernameNodeListChild = document.getElementsByTagName("ChildUsername");
            final Element usernameChild = (Element) usernameNodeListChild.item(0);
            this.testUsernameChild = usernameChild.getTextContent();
            
            // Obtaining Child Password
            final NodeList passwordNodeListChild = document.getElementsByTagName("ChildPassword");
            final Element passwordChild = (Element) passwordNodeListChild.item(0);
            this.testPasswordChild = passwordChild.getTextContent();
            
            // Obtaining Standalone Username
            final NodeList usernameNodeListStandAlone = document.getElementsByTagName("StandAloneUsername");
            final Element usernameSA = (Element) usernameNodeListStandAlone.item(0);
            this.testUsernameStandalone = usernameSA.getTextContent();
            
            // Obtaining Standalone Password
            final NodeList passwordNodeListStandAlone = document.getElementsByTagName("StandAlonePassword");
            final Element passwordSA = (Element) passwordNodeListStandAlone.item(0);
            this.testPasswordStandalone = passwordSA.getTextContent();
            
            // Obtaining Parent Username
            final NodeList usernameNodeListParent = document.getElementsByTagName("ParentUsername");
            final Element usernameParent = (Element) usernameNodeListParent.item(0);
            this.testUsernameParent = usernameParent.getTextContent();
            
            // Obtaining Parent Password
            final NodeList passwordNodeListParent = document.getElementsByTagName("ParentPassword");
            final Element passwordParent = (Element) passwordNodeListParent.item(0);
            this.testPasswordParent = passwordParent.getTextContent();
            
            /// Obtaining System Admin Username
            final NodeList usernameNodeListSystemAdmin = document.getElementsByTagName("SystemAdminUsrename");
            final Element usernameSystemAdmin = (Element) usernameNodeListSystemAdmin.item(0);
            this.testUsernameSystemAdmin = usernameSystemAdmin.getTextContent();
            
            /// Obtaining Digital Collect Username
            final NodeList usernameNodeListCollectChild = document.getElementsByTagName("CollectChildUsername");
            final Element usernameCollectChild = (Element) usernameNodeListCollectChild.item(0);
            this.testUsernameCollectChild = usernameCollectChild.getTextContent();
            
            /// Obtaining Digital Collect Username
            final NodeList usernameNodeListCollectStandalone = document.getElementsByTagName("CollectSAUsername");
            final Element usernameCollectStandalone = (Element) usernameNodeListCollectStandalone.item(0);
            this.testUsernameCollectStandalone = usernameCollectStandalone.getTextContent();
            
            // Obtaining System Admin Password
            final NodeList passwordNodeListSystemAdmin = document.getElementsByTagName("SystemAdminPassword");
            final Element passwordSystemAdmin = (Element) passwordNodeListSystemAdmin.item(0);
            this.testPasswordSystemAdmin = passwordSystemAdmin.getTextContent();
            
            // Obtaining default password = "password"
            final NodeList defaultPasswordNodeListSystemAdmin = document.getElementsByTagName("DefaultPassword");
            final Element defaultPasswordSystemAdmin = (Element) defaultPasswordNodeListSystemAdmin.item(0);
            this.testPasswordDefault = defaultPasswordSystemAdmin.getTextContent();
            
            // Obtaining Delay
            final NodeList delayList = document.getElementsByTagName("DriverDelayMilliSecond");
            final Element webDriverDelay = (Element) delayList.item(0);
            this.testDriverDelay = webDriverDelay.getTextContent();
            
            // Obtaining Browser Type
            final NodeList browserList = document.getElementsByTagName("BrowserType");
            final Element browserType = (Element) browserList.item(0);
            this.browserType = browserType.getTextContent();
            
            // launching specific browser type
            launchBrowser();
            this.customerNavigation = new CustomerNavigation(driver);
        }
        
        catch (SAXParseException err) {
            //            System.out.println("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId());
            //            System.out.println(" " + err.getMessage());
            
        } catch (SAXException e) {
            final Exception x = e.getException();
            ((x == null) ? e : x).printStackTrace();
            
        } catch (Throwable t) {
            t.printStackTrace();
        }
        
    }
    
    @After
    public void tearDown() throws Exception {
        
        super.driver.quit();
        //driver.close();
    }
    
    @Test
    @Order(order = 1)
    public final void goToLocations() {
        
        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Rates > Global
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Locations");
        customerNavigation.navigatePageSubTabs("Location Details");
        driverWait();
        
        assertEquals("Digital Iris - Settings - Locations : Location Details", super.driver.getTitle());
        driverWait();
        
        // Validating static texts and dropdown items/labels
        super.element = super.selectWebElementByXpath("//section[@class='groupBox menuBox locationList']/header/h2");
        driverWait();
        assertEquals("LOCATIONS", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@class='filterHeader activityLog horizontal']/section[@class='title']");
        driverWait();
        assertEquals("SEARCH:", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//ul[@class='filterForm']/li[@class='autoComplete']/label[@class='hiddenLabel' and @for='locationAC']");
        driverWait();
        assertEquals("Location", super.element.getText());
        driverWait();
        
        // click on the Add button and validate everything
        super.clickAddButton("btnAddLocation");
        driverWait();
        
        // validate all details in Add Location form
        super.element = super.selectWebElementByXpath("//section[@id='locationDetails']/header/h2[@id='addHeading']");
        driverWait();
        assertEquals("ADD LOCATION", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@id='locationDetailsForm']/h3[@class='detailList' and contains(., 'Details')]");
        driverWait();
        
        super.element = super
                .selectWebElementByXpath("//section[@id='locationDetailsForm' and @class='col1']/h3[@class='detailList']/aside[@id='parentFlag-edit']/label[@class='checkboxLabel switch 'and @for='parentFlagCheckHidden']");
        driverWait();
        assertEquals("  PARENT LOCATION", super.element.getText());
        driverWait();
        
        super.element = super
                .selectWebElementByXpath("//section[@id='locationDetailsForm' and @class='col1']/section/label[@class='detailLabel ' and @for='formLocationName']");
        driverWait();
        assertEquals("Name:", super.element.getText());
        driverWait();
        
        super.element = super
                .selectWebElementByXpath("//section[@id='locationDetailsForm' and @class='col1']/section/label[@class='detailLabel ' and @for='formDescription']");
        driverWait();
        assertEquals("Description:", super.element.getText());
        driverWait();
        
        super.element = super
                .selectWebElementByXpath("//label[@id='formOperatingModeTitle' and @class='detailLabel']");
        driverWait();
        assertEquals("Operating Mode:", super.element.getText().trim());
        driverWait();
        
        super.element = super
                .selectWebElementByXpath("//section[@id='locationDetailsForm' and @class='col1']/section/label[@id='formCapacityTitle' and @for='formCapacity']");
        driverWait();
        assertEquals("Capacity (Number of spaces):", super.element.getText());
        driverWait();
        
        super.element = super
                .selectWebElementByXpath("//section[@id='locationDetailsForm' and @class='col1']/section/label[@id='formTargetRevenueTitle' and @for='formTargetRevenue']");
        driverWait();
        assertEquals("Target Monthly Revenue:", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@id='formPaystationChildren' and @class='optionListArea']/h3[@class='detailList']");
        driverWait();
        assertEquals("Pay Stations", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@class='fullListHeader detailLabel' and contains(., 'All Pay Stations:')]");
        driverWait();
        //assertEquals("All Pay Stations: ", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//label[@class='checkboxLabel switch right ' and @for='psSelections']");
        driverWait();
        assertEquals("  Select all", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@class='selectedListHeader detailLabel']/section[@class='left']");
        driverWait();
        assertEquals("Selected Pay Stations:", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@class='selectedListHeader detailLabel']/a[@id='clearSelected']");
        driverWait();
        assertEquals("Clear", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@id='formOperationHours' and @class='col2']/h3[@class='detailList']");
        driverWait();
        assertEquals("Operating Hours", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@id='formOperationHours' and @class='col2']/table/tbody/tr/th[@class='opentime']");
        driverWait();
        assertEquals("Open", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@id='formOperationHours' and @class='col2']/table/tbody/tr/th[@class='closedtime']");
        driverWait();
        assertEquals("Close", super.element.getText());
        driverWait();
        
        super.element = super
                .selectWebElementByXpath("//section[@id='formOperationHours' and @class='col2']/table/tbody/tr[@id='Day0']/td[@class='detailLabel']");
        driverWait();
        assertEquals("Sun", super.element.getText());
        driverWait();
        
        super.element = super
                .selectWebElementByXpath("//section[@id='formOperationHours' and @class='col2']/table/tbody/tr[@id='Day1']/td[@class='detailLabel']");
        driverWait();
        assertEquals("Mon", super.element.getText());
        driverWait();
        
        super.element = super
                .selectWebElementByXpath("//section[@id='formOperationHours' and @class='col2']/table/tbody/tr[@id='Day2']/td[@class='detailLabel']");
        driverWait();
        assertEquals("Tues", super.element.getText());
        driverWait();
        
        super.element = super
                .selectWebElementByXpath("//section[@id='formOperationHours' and @class='col2']/table/tbody/tr[@id='Day3']/td[@class='detailLabel']");
        driverWait();
        assertEquals("Wed", super.element.getText());
        driverWait();
        
        super.element = super
                .selectWebElementByXpath("//section[@id='formOperationHours' and @class='col2']/table/tbody/tr[@id='Day4']/td[@class='detailLabel']");
        driverWait();
        assertEquals("Thur", super.element.getText());
        driverWait();
        
        super.element = super
                .selectWebElementByXpath("//section[@id='formOperationHours' and @class='col2']/table/tbody/tr[@id='Day5']/td[@class='detailLabel']");
        driverWait();
        assertEquals("Fri", super.element.getText());
        driverWait();
        
        super.element = super
                .selectWebElementByXpath("//section[@id='formOperationHours' and @class='col2']/table/tbody/tr[@id='Day6']/td[@class='detailLabel']");
        driverWait();
        assertEquals("Sat", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@id='formOperationHours' and @class='col2']/table/tbody/tr/th[@class='open24']");
        driverWait();
        assertEquals("24hrs", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@id='formOperationHours' and @class='col2']/table/tbody/tr/th[@class='closed24']");
        driverWait();
        assertEquals("Closed", super.element.getText());
        driverWait();
        
        // verifying Rate Profile section
        super.element = super.selectWebElementByXpath("//section[@id='formRateProfile' and @class='optionListArea']/h3[@class='detailList']");
        driverWait();
        assertEquals("Rate Profiles", super.element.getText().trim());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@id='profileAssignmentAreaHdr']/section[@class='locationNameBox assignAreaHdr detailLabel']");
        driverWait();
        assertEquals("Rate Profile", super.element.getText().trim());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@id='profileAssignmentAreaHdr']/section[@class='operatingMode assignAreaHdr detailLabel']");
        driverWait();
        assertEquals("Operating Mode", super.element.getText().trim());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@id='profileAssignmentAreaHdr']/section[@class='dateRange assignAreaHdr detailLabel']");
        driverWait();
        assertEquals("Date Range", super.element.getText().trim());
        driverWait();
        
        super.selectOptionFromDropDown("profileAssign0", "Choose");
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@class='btnSet']/a[@class='linkButtonFtr textButton save']");
        driverWait();
        assertEquals("SAVE", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@class='btnSet']/a[@class='linkButton textButton cancel']");
        driverWait();
        assertEquals("CANCEL", super.element.getText());
        driverWait();
        
        //logout
        super.element = super.selectWebElementByXpath("//a[@title='Logout']");
        super.element.click();
        
        super.element = super.selectWebElementByXpath("//section[@id='messageResponseAlertBox']/section[@class='innerBorder']/article");
        driverWait();
        assertEquals("You haven't saved or cancelled your changes. Please select an option below to cancel and continue to exit the page or to return to the page to save.", super.element.getText().trim());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//button[contains(@class, 'btnCancel')]");
        driverWait();
        super.element.click();
        driverWait();        
        
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
        
    }
    
    @Test
    @Order(order = 2)
    public final void addParentLocation() {
        
        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Rates > Global
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Locations");
        customerNavigation.navigatePageSubTabs("Location Details");
        driverWait();
        
        assertEquals("Digital Iris - Settings - Locations : Location Details", super.driver.getTitle());
        driverWait();
        
        // click on the Add button and validate everything
        super.clickAddButton("btnAddLocation");
        driverWait();
        
        // create a parent location
        super.element = super
                .selectWebElementByXpath("//label[@class='checkboxLabel switch ' and @for='parentFlagCheckHidden']/a[@id='parentFlagCheck' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//input[@id='formLocationName' and @name='wrappedObject.name']");
        driverWait();
        super.element.click();
        driverWait();
        super.element.sendKeys("1234");
        driverWait();
        
        super.element = super.selectWebElementByXpath("//textarea[@id='formDescription' and @name='wrappedObject.description']");
        driverWait();
        super.element.click();
        driverWait();
        super.element.sendKeys("This is a parent location");
        driverWait();
        
        super.saveButton();
        
        // validate the name of the location
        super.element = super.selectWebElementByXpath("//li[contains(@id, 'loc') and @class='Parent' and contains(@title, '1234')]");
        driverWait();
        //assertEquals("1234", super.element.getText());
        super.element.click();
        driverWait();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dt[@class='detailLabel']");
        driverWait();
        assertEquals("Name:", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dd[@id='detailLocationName' and @class='detailValue']");
        driverWait();
        assertEquals("1234", super.element.getText());
        
        //logout
        super.element = super.selectWebElementByXpath("//a[@title='Logout']");
        super.element.click();
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
        
    }
    
    @Test
    @Order(order = 3)
    public final void addChildLocation() {
        
        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Rates > Global
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Locations");
        customerNavigation.navigatePageSubTabs("Location Details");
        driverWait();
        
        assertEquals("Digital Iris - Settings - Locations : Location Details", super.driver.getTitle());
        driverWait();
        
        // create a child location//////////////////////////////////////////
        super.clickAddButton("btnAddLocation");
        driverWait();
        
        super.element = super.selectWebElementByXpath("//input[@id='formLocationName' and @name='wrappedObject.name']");
        driverWait();
        super.element.click();
        driverWait();
        super.element.sendKeys("Thistextshouldbe25chars00");
        driverWait();
        
        super.element = super.selectWebElementByXpath("//textarea[@id='formDescription' and @name='wrappedObject.description']");
        driverWait();
        super.element.click();
        driverWait();
        super.element.sendKeys("This is a child location");
        driverWait();
        
        // choosing a parent location
        super.element = super.selectWebElementByXpath("//label[@id='formParentLocationTitle' and @for='formParentLocationName']");
        driverWait();
        assertEquals("Parent Location:", super.element.getText());
        driverWait();
        super.selectOptionFromDropDown("formParentLocationName", "Choose");
        driverWait();
        super.selectOptionFromDropDown("formParentLocationName", "1234");
        driverWait();
        
        // assigning capacity       
        super.assignText("formCapacity", "wrappedObject.capacity", "100");
        
        // assigning Target Monthly Revenue
        super.assignText("formTargetRevenue", "wrappedObject.targetMonthlyRevenue", "150000");
        
        // assigning pay stations
        super.element = super.selectWebElementByXpath("//ul[@id='locPSFullList']/li[contains(., ' 827300000700')]");
        driverWait();
        super.element.click();
        driverWait();
        
        // assigning operating hours
        super.enableCheckBox("Sun24hr");
        super.enableCheckBox("MonClsd");
        super.enableCheckBox("Tues24hr");
        super.enableCheckBox("Wed24hr");
        super.enableCheckBox("ThurClsd");
        super.enableCheckBox("Fri24hr");
        
        // Assigning time
        super.element = super.selectWebElementByXpath("//tbody/tr[@id='Day6']/td[@class='opentime']/select[@id='SatOpen']/option[@value='1']");
        driverWait();
        super.element.click();
        driverWait();
        super.element = super.selectWebElementByXpath("//tbody/tr[@id='Day6']/td[@class='closedtime']/select[@id='SatClose']/option[@value='12']");
        driverWait();
        super.element.click();
        driverWait();
        
        // Assigning Operating Mode
        super.selectOptionFromDropDown("formOperatingMode", "Multiple");
        driverWait();
        
        super.saveButton();
        driverWait();
        driver.navigate().refresh();
        
        // make sure the child location is added
        super.element = super.selectWebElementByXpath("//li[@class='Parent' and contains(@title, '1234')]");
        driverWait();
        super.element.click();
        driverWait();
        super.element = super.selectWebElementByXpath("//li[@class='Child' and @title='Thistextshouldbe25chars00  ']");
        driverWait();
        super.element.click();
        driverWait();
        
        // view the location and validate the name
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dt[@class='detailLabel']");
        driverWait();
        assertEquals("Name:", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dd[@id='detailLocationName' and @class='detailValue']");
        driverWait();
        assertEquals("Thistextshouldbe25chars00", super.element.getText());
        
        //logout
        super.element = super.selectWebElementByXpath("//a[@title='Logout']");
        super.element.click();
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
        
    }
    
    @Test
    @Order(order = 4)
    public final void addStandaloneLocation() {
        
        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Rates > Global
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Locations");
        customerNavigation.navigatePageSubTabs("Location Details");
        driverWait();
        
        assertEquals("Digital Iris - Settings - Locations : Location Details", super.driver.getTitle());
        driverWait();
        
        // create a standalone location//////////////////////////////////////////
        super.clickAddButton("btnAddLocation");
        driverWait();
        
        super.element = super.selectWebElementByXpath("//input[@id='formLocationName' and @name='wrappedObject.name']");
        driverWait();
        super.element.click();
        driverWait();
        super.element.sendKeys("Standalone Location");
        driverWait();
        
        super.element = super.selectWebElementByXpath("//textarea[@id='formDescription' and @name='wrappedObject.description']");
        driverWait();
        super.element.click();
        driverWait();
        super.element.sendKeys("This is a Standalone location");
        driverWait();
        
        // assigning capacity       
        super.assignText("formCapacity", "wrappedObject.capacity", "100");
        
        // assigning Target Monthly Revenue
        super.assignText("formTargetRevenue", "wrappedObject.targetMonthlyRevenue", "150000");
        
        // assigning operating hours
        super.enableCheckBox("Sun24hr");
        super.enableCheckBox("MonClsd");
        super.enableCheckBox("Tues24hr");
        super.enableCheckBox("Wed24hr");
        super.enableCheckBox("ThurClsd");
        super.enableCheckBox("Fri24hr");
        
        // Assigning time
        super.element = super.selectWebElementByXpath("//tbody/tr[@id='Day6']/td[@class='opentime']/select[@id='SatOpen']/option[@value='1']");
        driverWait();
        super.element.click();
        driverWait();
        super.element = super.selectWebElementByXpath("//tbody/tr[@id='Day6']/td[@class='closedtime']/select[@id='SatClose']/option[@value='12']");
        driverWait();
        super.element.click();
        driverWait();
        
        // Assigning Operating Mode
        super.selectOptionFromDropDown("formOperatingMode", "Multiple");
        driverWait();
        
        super.saveButton();
        driverWait();
        driver.navigate().refresh();
        
        // make sure the standalone location is added
        super.element = super.selectWebElementByXpath("//li[@class='Child' and @title='Standalone Location  ']");
        driverWait();
        super.element.click();
        driverWait();
        
        // view the location and validate the name
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dt[@class='detailLabel']");
        driverWait();
        assertEquals("Name:", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dd[@id='detailLocationName' and @class='detailValue']");
        driverWait();
        assertEquals("Standalone Location", super.element.getText());
        
        //logout
        super.element = super.selectWebElementByXpath("//a[@title='Logout']");
        super.element.click();
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
        
    }
    
    @Test
    @Order(order = 5)
    public final void editStandaloneLocation() {
        
        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Rates > Global
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Locations");
        customerNavigation.navigatePageSubTabs("Location Details");
        driverWait();
        
        assertEquals("Digital Iris - Settings - Locations : Location Details", super.driver.getTitle());
        driverWait();
        
        // Go to the edit page of standalone location
        super.element = super.selectWebElementByXpath("//li[@class='Child' and @title='Standalone Location  ']");
        driverWait();
        final String objectId = super.element.getAttribute("id").split("_")[1];
        driverWait();
        
        super.element = super.selectWebElementByXpath("//li[@title='Standalone Location  ']/a[contains(@id, '" + objectId
                                                      + "') and @class='menuListButton menu']");
        super.element.click();
        Sleeper.sleepTightInSeconds(super.SLEEP_TIME_ONE_SECOND);
        
        super.element = super.selectWebElementByXpath("//section[contains(@id, '" + objectId
                                                      + "') and @class='ddMenu optionMenu']/section/a[@class='edit btnEditLocation']");
        driverWait();
        super.element.click();
        Sleeper.sleepTightInSeconds(super.SLEEP_TIME_ONE_SECOND);
        
        // modify the name and save the form
        super.element = super.selectWebElementByXpath("//input[@id='formLocationName' and @name='wrappedObject.name']");
        driverWait();
        super.element.clear();
        super.element.click();
        driverWait();
        super.element.sendKeys("Standalone Location Edit");
        driverWait();
        
        super.saveButton();
        driverWait();
        
        // make sure the name is modified
        assertEquals("Standalone Location Edit", super.selectWebElementByXpath(".//*[@id='detailLocationName']").getText());
        
        //logout
        logout();
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
        
    }
    
    @Test
    @Order(order = 6)
    public final void editChildLocation() {
        
        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Rates > Global
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Locations");
        customerNavigation.navigatePageSubTabs("Location Details");
        driverWait();
        
        assertEquals("Digital Iris - Settings - Locations : Location Details", super.driver.getTitle());
        driverWait();
        
        // Go to the edit page of child location
        super.element = super.selectWebElementByXpath("//li[contains(@id, 'loc') and @class='Parent' and contains(@title, '1234')]");
        driverWait();
        super.element.click();
        driverWait();
        
        super.element = super.selectWebElementByXpath("//li[@class='Child' and @title='Thistextshouldbe25chars00  ']");
        driverWait();
        final String objectId = super.element.getAttribute("id").split("_")[1];
        driverWait();
        
        super.element = super.selectWebElementByXpath("//li[@title='Thistextshouldbe25chars00  ']/a[contains(@id, '" + objectId
                                                      + "') and @class='menuListButton menu']");
        super.element.click();
        Sleeper.sleepTightInSeconds(super.SLEEP_TIME_ONE_SECOND);
        
        super.element = super.selectWebElementByXpath("//section[contains(@id, '" + objectId
                                                      + "') and @class='ddMenu optionMenu']/section/a[@class='edit btnEditLocation']");
        driverWait();
        super.element.click();
        Sleeper.sleepTightInSeconds(super.SLEEP_TIME_ONE_SECOND);
        
        // modify the name, remove the pay station, and save the form
        super.element = super.selectWebElementByXpath("//input[@id='formLocationName' and @name='wrappedObject.name']");
        driverWait();
        super.element.clear();
        super.element.click();
        driverWait();
        super.element.sendKeys("Child Location Edit");
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@class='selectedListHeader detailLabel']/a[contains(., 'Clear')]");
        driverWait();
        super.element.click();
        
        super.saveButton();
        driverWait();
        
        // make sure the name is modified
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dt[@class='detailLabel']");
        driverWait();
        assertEquals("Name:", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dd[@id='detailLocationName' and @class='detailValue']");
        driverWait();
        assertEquals("Child Location Edit", super.element.getText());
        
        //logout
        logout();
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
        
    }
    
    @Test
    @Order(order = 7)
    public final void editParentLocation() {
        
        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Rates > Global
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Locations");
        customerNavigation.navigatePageSubTabs("Location Details");
        driverWait();
        
        assertEquals("Digital Iris - Settings - Locations : Location Details", super.driver.getTitle());
        driverWait();
        
        // Go to the edit page of the parent location
        super.element = super.selectWebElementByXpath("//li[@class='Parent' and contains(@title, '1234')]");
        driverWait();
        final String objectId = super.element.getAttribute("id").split("_")[1];
        driverWait();
        
        super.element = super.selectWebElementByXpath("//li[@title='1234 - Click to view sub-locations. ']/a[contains(@id, '" + objectId
                                                      + "') and @class='menuListButton menu']");
        super.element.click();
        Sleeper.sleepTightInSeconds(super.SLEEP_TIME_ONE_SECOND);
        
        super.element = super.selectWebElementByXpath("//section[contains(@id, '" + objectId
                                                      + "') and @class='ddMenu optionMenu']/section/a[@class='edit btnEditLocation']");
        driverWait();
        super.element.click();
        Sleeper.sleepTightInSeconds(super.SLEEP_TIME_ONE_SECOND);
        
        // modify the name, remove the child location, and save the form
        super.element = super.selectWebElementByXpath("//input[@id='formLocationName' and @name='wrappedObject.name']");
        driverWait();
        super.element.clear();
        super.element.click();
        driverWait();
        super.element.sendKeys("Parent Location Edit");
        driverWait();
        
        super.element = super.selectWebElementByXpath("//label[@for='locSelections']/a[@id='locSelectionsAll' and @class='checkBox']");
        driverWait();
        super.element.click();
        driverWait();
        super.element.click();
        
        super.saveButton();
        driverWait();
        
        // make sure the name is modified
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dt[@class='detailLabel']");
        driverWait();
        assertEquals("Name:", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dd[@id='detailLocationName' and @class='detailValue']");
        driverWait();
        assertEquals("Parent Location Edit", super.element.getText());
        
        //logout
        logout();
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        
    }
    
    @Test
    @Order(order = 8)
    public final void viewParentLocation() {
        
        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Rates > Global
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Locations");
        customerNavigation.navigatePageSubTabs("Location Details");
        driverWait();
        
        assertEquals("Digital Iris - Settings - Locations : Location Details", super.driver.getTitle());
        driverWait();
        
        // Go to the view page of the parent location
        super.element = super.selectWebElementByXpath("//li[@class='Parent' and contains(@title, 'Parent Location Edit')]");
        driverWait();
        final String objectId = super.element.getAttribute("id").split("_")[1];
        driverWait();
        super.element.click();
        
        // validate everything
        super.element = super.selectWebElementByXpath("//header/h2[@id='locationName']");
        driverWait();
        assertEquals("PARENT LOCATION EDIT", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//section[@id='locationDetailsView']/h3[@class='detailList' and contains(., 'Details')]");
        driverWait();
        
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dt[@class='detailLabel']");
        driverWait();
        assertEquals("Name:", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dd[@id='detailLocationName' and @class='detailValue']");
        driverWait();
        assertEquals("Parent Location Edit", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dt[@id='detailDescriptionTitle' and @class='detailLabel']");
        driverWait();
        assertEquals("Description:", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dd[@id='detailDescription' and @class='detailValue']");
        driverWait();
        assertEquals("This is a parent location", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dt[@class='detailLabel' and contains(., 'Capacity')]/span[@class='note']");
        driverWait();
        assertEquals("(Number of spaces)", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dd[@id='detailCapacity' and @class='detailValue']");
        driverWait();
        assertEquals("0", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dt[@class='detailLabel' and contains(., 'Monthly')]");
        driverWait();
        assertEquals("Target Monthly Revenue:", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dd[@id='detailTargetRevenue' and @class='detailValue']");
        driverWait();
        assertEquals("$0.00", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//h3[@class='detailList' and contains(., 'Parent Location')]");
        driverWait();
        //assertEquals("PARENT LOCATION", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//section[@id='LocationChildren']/h3[@class='detailList']");
        driverWait();
        assertEquals("Locations", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//ul[@id='locChildList']/li[contains(., 'locations')]");
        driverWait();
        assertEquals("No locations found.", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//article[@id='locationView']/section[@id='locationDetailsView']/h3/aside[@id='parentFlag-view']");
        driverWait();
        assertEquals("PARENT LOCATION", super.element.getText().trim());
        driverWait();
        
        //logout
        logout();
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
        
    }
    
    @Test
    @Order(order = 9)
    public final void viewChildLocation() {
        
        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Rates > Global
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Locations");
        customerNavigation.navigatePageSubTabs("Location Details");
        driverWait();
        
        assertEquals("Digital Iris - Settings - Locations : Location Details", super.driver.getTitle());
        driverWait();
        
        // Go to the view page of the parent location
        super.element = super.selectWebElementByXpath("//li[@class='Child' and contains(@title, 'Child Location Edit')]");
        driverWait();
        final String objectId = super.element.getAttribute("id").split("_")[1];
        driverWait();
        super.element.click();
        
        // validate everything
        super.element = super.selectWebElementByXpath("//header/h2[@id='locationName']");
        driverWait();
        assertEquals("CHILD LOCATION EDIT(Unassigned)", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//header/h2[@id='locationName']/span[@class='notation']");
        driverWait();
        assertEquals("(Unassigned)", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//section[@id='locationDetailsView']/h3[@class='detailList' and contains(., 'Details')]");
        driverWait();
        
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dt[@class='detailLabel']");
        driverWait();
        assertEquals("Name:", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dd[@id='detailLocationName' and @class='detailValue']");
        driverWait();
        assertEquals("Child Location Edit", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dt[@id='detailOpModeTitle' and @class='detailLabel']");
        driverWait();
        assertEquals("Operating Mode:", super.element.getText().trim());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dd[@id='detailOpMode' and @class='detailValue']");
        driverWait();
        assertEquals("Multiple", super.element.getText().trim());
        
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dt[@id='detailDescriptionTitle' and @class='detailLabel']");
        driverWait();
        assertEquals("Description:", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dd[@id='detailDescription' and @class='detailValue']");
        driverWait();
        assertEquals("This is a child location", super.element.getText());
        
        
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dt[@class='detailLabel' and contains(., 'Capacity')]/span[@class='note']");
        driverWait();
        assertEquals("(Number of spaces)", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dd[@id='detailCapacity' and @class='detailValue']");
        driverWait();
        assertEquals("100", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dt[@class='detailLabel' and contains(., 'Monthly')]");
        driverWait();
        assertEquals("Target Monthly Revenue:", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dd[@id='detailTargetRevenue' and @class='detailValue']");
        driverWait();
        assertEquals("$150,000.00", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//section[@id='PaystationChildren']/h3[@class='detailList' and contains(., 'Pay Stations')]");
        driverWait();
        //assertEquals("PARENT LOCATION", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//ul[@id='psChildList']/li[contains(., 'pay stations')]");
        driverWait();
        assertEquals("No pay stations found.", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//section[@id='operationHours']/h3[@class='detailList']");
        driverWait();
        assertEquals("Operating Hours", super.element.getText());
        
        super.element = super
                .selectWebElementByXpath("//section[@id='operationHours']/dl[@class='details timeSelection']/dt[@class='detailLabel' and contains(., 'Sunday')]");
        driverWait();
        assertEquals("Sunday", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details timeSelection']/dd[@id='SundayOpen' and @class='open24']");
        driverWait();
        assertEquals("Open 24 hours", super.element.getText());
        
        super.element = super
                .selectWebElementByXpath("//section[@id='operationHours']/dl[@class='details timeSelection']/dt[@class='detailLabel' and contains(., 'Monday')]");
        driverWait();
        assertEquals("Monday", super.element.getText());
        driverWait();
        super.element = super.driver.findElement(By.xpath("//dl[@class='details timeSelection']/dd[@id='MondayOpen' and @class='open24']"));
        driverWait();
        assertEquals("Open 24 hours", super.element.getText().trim());
        
        super.element = super
                .selectWebElementByXpath("//section[@id='operationHours']/dl[@class='details timeSelection']/dt[@class='detailLabel' and contains(., 'Tuesday')]");
        driverWait();
        assertEquals("Tuesday", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details timeSelection']/dd[@id='TuesdayOpen' and @class='open24']");
        driverWait();
        assertEquals("Open 24 hours", super.element.getText());
        
        super.element = super
                .selectWebElementByXpath("//section[@id='operationHours']/dl[@class='details timeSelection']/dt[@class='detailLabel' and contains(., 'Wednesday')]");
        driverWait();
        assertEquals("Wednesday", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details timeSelection']/dd[@id='WednesdayOpen' and @class='open24']");
        driverWait();
        assertEquals("Open 24 hours", super.element.getText());
        
        super.element = super
                .selectWebElementByXpath("//section[@id='operationHours']/dl[@class='details timeSelection']/dt[@class='detailLabel' and contains(., 'Thursday')]");
        driverWait();
        assertEquals("Thursday", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details timeSelection']/dd[@id='ThursdayClosed' and @class='closed24']");
        driverWait();
        assertEquals("Closed 24 hours", super.element.getText());
        
        super.element = super
                .selectWebElementByXpath("//section[@id='operationHours']/dl[@class='details timeSelection']/dt[@class='detailLabel' and contains(., 'Friday')]");
        driverWait();
        assertEquals("Friday", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details timeSelection']/dd[@id='FridayOpen' and @class='open24']");
        driverWait();
        assertEquals("Open 24 hours", super.element.getText());
        
        super.element = super
                .selectWebElementByXpath("//section[@id='operationHours']/dl[@class='details timeSelection']/dt[@class='detailLabel' and contains(., 'Saturday')]");
        driverWait();
        assertEquals("Saturday", super.element.getText());
        driverWait();
        super.element = super
                .selectWebElementByXpath("//dl[@class='details timeSelection']/dd[@id='SaturdayTime' and @class='detailValue']/span[@id='SaturdayStart']");
        driverWait();
        assertEquals("12:15 am", super.element.getText());
        driverWait();
        super.element = super
                .selectWebElementByXpath("//dl[@class='details timeSelection']/dd[@id='SaturdayTime' and @class='detailValue']/span[@class='info']");
        driverWait();
        assertEquals("to", super.element.getText());
        driverWait();
        super.element = super
                .selectWebElementByXpath("//dl[@class='details timeSelection']/dd[@id='SaturdayTime' and @class='detailValue']/span[@id='SaturdayEnd']");
        driverWait();
        assertEquals("03:15 am", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//article[@id='locationView']/section[@id='rateProfile']/h3[@class='detailList']");
        driverWait();
        assertEquals("Rate Profiles", super.element.getText().trim());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//article[@id='locationView']/section[@id='rateProfile']/section[@id='rateProfileCal']/span");
        driverWait();
        assertEquals("No Rate Profiles Assigned to this Location.", super.element.getText().trim());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@id='PaystationChildren']/h3[@class='detailList']");
        driverWait();
        assertEquals("Pay Stations", super.element.getText().trim());
        driverWait();
        super.element = super.selectWebElementByXpath("//section[@id='PaystationChildren']/ul[@id='psChildList']");
        driverWait();
        assertEquals("No pay stations found.", super.element.getText().trim());
        driverWait();
        
        //logout
        logout();
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
        
    }
    
    @Test
    @Order(order = 10)
    public final void viewStandaloneLocation() {
        
        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Rates > Global
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Locations");
        customerNavigation.navigatePageSubTabs("Location Details");
        driverWait();
        
        assertEquals("Digital Iris - Settings - Locations : Location Details", super.driver.getTitle());
        driverWait();
        
        // Go to the view page of the standalone location
        super.element = super.selectWebElementByXpath("//li[@class='Child' and contains(@title, 'Standalone Location Edit')]");
        driverWait();
        final String objectId = super.element.getAttribute("id").split("_")[1];
        driverWait();
        super.element.click();
        
        // validate everything
        super.element = super.selectWebElementByXpath("//header/h2[@id='locationName']");
        driverWait();
        assertEquals("STANDALONE LOCATION EDIT", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//section[@id='locationDetailsView']/h3[@class='detailList' and contains(., 'Details')]");
        driverWait();
        
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dt[@class='detailLabel']");
        driverWait();
        assertEquals("Name:", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dd[@id='detailLocationName' and @class='detailValue']");
        driverWait();
        assertEquals("Standalone Location Edit", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dt[@id='detailDescriptionTitle' and @class='detailLabel']");
        driverWait();
        assertEquals("Description:", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dd[@id='detailDescription' and @class='detailValue']");
        driverWait();
        assertEquals("This is a Standalone location", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dt[@id='detailOpModeTitle' and @class='detailLabel']");
        driverWait();
        assertEquals("Operating Mode:", super.element.getText().trim());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dd[@id='detailOpMode' and @class='detailValue']");
        driverWait();
        assertEquals("Multiple", super.element.getText().trim());
                
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dt[@class='detailLabel' and contains(., 'Capacity')]/span[@class='note']");
        driverWait();
        assertEquals("(Number of spaces)", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dd[@id='detailCapacity' and @class='detailValue']");
        driverWait();
        assertEquals("100", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dt[@class='detailLabel' and contains(., 'Monthly')]");
        driverWait();
        assertEquals("Target Monthly Revenue:", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details']/dd[@id='detailTargetRevenue' and @class='detailValue']");
        driverWait();
        assertEquals("$150,000.00", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//section[@id='PaystationChildren']/h3[@class='detailList' and contains(., 'Pay Stations')]");
        driverWait();
        //assertEquals("PARENT LOCATION", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//ul[@id='psChildList']/li[contains(., 'pay stations')]");
        driverWait();
        assertEquals("No pay stations found.", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//section[@id='operationHours']/h3[@class='detailList']");
        driverWait();
        assertEquals("Operating Hours", super.element.getText());
        
        super.element = super
                .selectWebElementByXpath("//section[@id='operationHours']/dl[@class='details timeSelection']/dt[@class='detailLabel' and contains(., 'Sunday')]");
        driverWait();
        assertEquals("Sunday", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details timeSelection']/dd[@id='SundayOpen' and @class='open24']");
        driverWait();
        assertEquals("Open 24 hours", super.element.getText());
        
        super.element = super
                .selectWebElementByXpath("//section[@id='operationHours']/dl[@class='details timeSelection']/dt[@class='detailLabel' and contains(., 'Monday')]");
        driverWait();
        assertEquals("Monday", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details timeSelection']/dd[@id='MondayClosed' and @class='closed24']");
        driverWait();
        assertEquals("Closed 24 hours", super.element.getText());
        
        super.element = super
                .selectWebElementByXpath("//section[@id='operationHours']/dl[@class='details timeSelection']/dt[@class='detailLabel' and contains(., 'Tuesday')]");
        driverWait();
        assertEquals("Tuesday", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details timeSelection']/dd[@id='TuesdayOpen' and @class='open24']");
        driverWait();
        assertEquals("Open 24 hours", super.element.getText());
        
        super.element = super
                .selectWebElementByXpath("//section[@id='operationHours']/dl[@class='details timeSelection']/dt[@class='detailLabel' and contains(., 'Wednesday')]");
        driverWait();
        assertEquals("Wednesday", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details timeSelection']/dd[@id='WednesdayOpen' and @class='open24']");
        driverWait();
        assertEquals("Open 24 hours", super.element.getText());
        
        super.element = super
                .selectWebElementByXpath("//section[@id='operationHours']/dl[@class='details timeSelection']/dt[@class='detailLabel' and contains(., 'Thursday')]");
        driverWait();
        assertEquals("Thursday", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details timeSelection']/dd[@id='ThursdayClosed' and @class='closed24']");
        driverWait();
        assertEquals("Closed 24 hours", super.element.getText());
        
        super.element = super
                .selectWebElementByXpath("//section[@id='operationHours']/dl[@class='details timeSelection']/dt[@class='detailLabel' and contains(., 'Friday')]");
        driverWait();
        assertEquals("Friday", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//dl[@class='details timeSelection']/dd[@id='FridayOpen' and @class='open24']");
        driverWait();
        assertEquals("Open 24 hours", super.element.getText());
        
        super.element = super
                .selectWebElementByXpath("//section[@id='operationHours']/dl[@class='details timeSelection']/dt[@class='detailLabel' and contains(., 'Saturday')]");
        driverWait();
        assertEquals("Saturday", super.element.getText());
        driverWait();
        super.element = super
                .selectWebElementByXpath("//dl[@class='details timeSelection']/dd[@id='SaturdayTime' and @class='detailValue']/span[@id='SaturdayStart']");
        driverWait();
        assertEquals("12:15 am", super.element.getText());
        driverWait();
        super.element = super
                .selectWebElementByXpath("//dl[@class='details timeSelection']/dd[@id='SaturdayTime' and @class='detailValue']/span[@class='info']");
        driverWait();
        assertEquals("to", super.element.getText());
        driverWait();
        super.element = super
                .selectWebElementByXpath("//dl[@class='details timeSelection']/dd[@id='SaturdayTime' and @class='detailValue']/span[@id='SaturdayEnd']");
        driverWait();
        assertEquals("03:15 am", super.element.getText());
        
        super.element = super.selectWebElementByXpath("//article[@id='locationView']/section[@id='rateProfile']/h3[@class='detailList']");
        driverWait();
        assertEquals("Rate Profiles", super.element.getText().trim());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//article[@id='locationView']/section[@id='rateProfile']/section[@id='rateProfileCal']/span");
        driverWait();
        assertEquals("No Rate Profiles Assigned to this Location.", super.element.getText().trim());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[@id='PaystationChildren']/h3[@class='detailList']");
        driverWait();
        assertEquals("Pay Stations", super.element.getText().trim());
        driverWait();
        super.element = super.selectWebElementByXpath("//section[@id='PaystationChildren']/ul[@id='psChildList']");
        driverWait();
        assertEquals("No pay stations found.", super.element.getText().trim());
        driverWait();

        //logout
        logout();
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
        
    }
    
    @Test
    @Order(order = 11)
    public final void deleteStandaloneLocation() {
        
        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Rates > Global
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Locations");
        customerNavigation.navigatePageSubTabs("Location Details");
        driverWait();
        
        assertEquals("Digital Iris - Settings - Locations : Location Details", super.driver.getTitle());
        driverWait();
        
        // Select and delete the stand alone location
        super.element = super.selectWebElementByXpath("//li[@class='Child' and @title='Standalone Location Edit  ']");
        driverWait();
        final String objectId = super.element.getAttribute("id").split("_")[1];
        driverWait();
        
        // delete the location
        super.element = super.selectWebElementByXpath("//li[@title='Standalone Location Edit  ']/a[contains(@id, '" + objectId
                                                      + "') and @class='menuListButton menu']");
        super.element.click();
        Sleeper.sleepTightInSeconds(super.SLEEP_TIME_ONE_SECOND);
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//section[contains(@id, '" + objectId
                                                      + "') and @class='ddMenu optionMenu']/section/a[@class='delete btnDeleteLocation']"));
        driverWait();
        super.element.click();
        Sleeper.sleepTightInSeconds(super.SLEEP_TIME_ONE_SECOND);
        driverWait();
        
        // validate confirmation message
        super.element = super.selectWebElementByXpath("//section[@id='messageResponseAlertBox']/section[@class='innerBorder']/h4/strong");
        driverWait();
        assertEquals("Attention!", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//section[@id='messageResponseAlertBox']/section/article");
        driverWait();
        assertEquals("Are you sure you would like to delete this location?", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//div[@class='ui-dialog-buttonset']/button[@role='button']/span[contains(., 'Delete')]");
        driverWait();
        super.element.click();
        Sleeper.sleepTightInSeconds(super.SLEEP_TIME_ONE_SECOND);
        
        //logout
        logout();
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
        
    }
    
    @Test
    @Order(order = 12)
    public final void deleteChildLocation() {
        
        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Rates > Global
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Locations");
        customerNavigation.navigatePageSubTabs("Location Details");
        driverWait();
        
        assertEquals("Digital Iris - Settings - Locations : Location Details", super.driver.getTitle());
        driverWait();
        
        // Select and delete the stand alone location
        super.element = super.selectWebElementByXpath("//li[@class='Child' and @title='Child Location Edit  ']");
        driverWait();
        final String objectId = super.element.getAttribute("id").split("_")[1];
        driverWait();
        
        // delete the location
        super.element = super.selectWebElementByXpath("//li[@title='Child Location Edit  ']/a[contains(@id, '" + objectId
                                                      + "') and @class='menuListButton menu']");
        super.element.click();
        Sleeper.sleepTightInSeconds(super.SLEEP_TIME_ONE_SECOND);
        driverWait();
        
        super.element = super.driver.findElement(By.xpath("//section[contains(@id, '" + objectId
                                                      + "') and @class='ddMenu optionMenu']/section/a[@class='delete btnDeleteLocation']"));
        driverWait();
        super.element.click();
        Sleeper.sleepTightInSeconds(super.SLEEP_TIME_ONE_SECOND);
        driverWait();
        
        // validate confirmation message
        super.element = super.selectWebElementByXpath("//section[@id='messageResponseAlertBox']/section[@class='innerBorder']/h4/strong");
        driverWait();
        assertEquals("Attention!", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//section[@id='messageResponseAlertBox']/section/article");
        driverWait();
        assertEquals("Are you sure you would like to delete this location?", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//div[@class='ui-dialog-buttonset']/button[@role='button']/span[contains(., 'Delete')]");
        driverWait();
        super.element.click();
        Sleeper.sleepTightInSeconds(super.SLEEP_TIME_ONE_SECOND);
        
        //logout
        logout();
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
        
    }
    
    @Test
    @Order(order = 13)
    public final void deleteParentLocation() {
        
        // Go to Iris
        driverWait();
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        // going to Settings > Rates > Global
        customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Locations");
        customerNavigation.navigatePageSubTabs("Location Details");
        driverWait();
        
        assertEquals("Digital Iris - Settings - Locations : Location Details", super.driver.getTitle());
        driverWait();
        
        // Select and delete the stand alone location
        super.element = super.selectWebElementByXpath("//li[@class='Parent' and contains(@title, 'Parent Location Edit')]");
        driverWait();
        final String objectId = super.element.getAttribute("id").split("_")[1];
        driverWait();
        
        // delete the location
        super.element = super.selectWebElementByXpath("//li[contains(@title, 'Parent Location Edit')]/a[contains(@id, '" + objectId
                                                      + "') and @class='menuListButton menu']");
        super.element.click();
        Sleeper.sleepTightInSeconds(super.SLEEP_TIME_ONE_SECOND);
        driverWait();
        
        super.element = super.selectWebElementByXpath("//section[contains(@id, '" + objectId
                                                      + "') and @class='ddMenu optionMenu']/section/a[@class='delete btnDeleteLocation']");
        driverWait();
        super.element.click();
        Sleeper.sleepTightInSeconds(super.SLEEP_TIME_ONE_SECOND);
        driverWait();
        
        // validate confirmation message
        super.element = super.selectWebElementByXpath("//section[@id='messageResponseAlertBox']/section[@class='innerBorder']/h4/strong");
        driverWait();
        assertEquals("Attention!", super.element.getText());
        driverWait();
        super.element = super.selectWebElementByXpath("//section[@id='messageResponseAlertBox']/section/article");
        driverWait();
        assertEquals("Are you sure you would like to delete this location?", super.element.getText());
        driverWait();
        
        super.element = super.selectWebElementByXpath("//div[@class='ui-dialog-buttonset']/button[@role='button']/span[contains(., 'Delete')]");
        driverWait();
        super.element.click();
        Sleeper.sleepTightInSeconds(super.SLEEP_TIME_ONE_SECOND);
        
        //logout
        logout();
        assertEquals("Digital Iris - Login.", super.driver.getTitle());
        driverWait();
        //assertTrue(true);
        
    }
    
}
