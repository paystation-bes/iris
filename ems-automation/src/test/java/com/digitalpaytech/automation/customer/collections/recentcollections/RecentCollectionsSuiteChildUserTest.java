package com.digitalpaytech.automation.customer.collections.recentcollections;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.digitalpaytech.automation.Order;
import com.digitalpaytech.automation.OrderedRunner;
import com.digitalpaytech.automation.collections.recentcollections.RecentCollectionsSuiteTest;

@RunWith(OrderedRunner.class)
public class RecentCollectionsSuiteChildUserTest extends RecentCollectionsSuiteTest {
    @Before
    public final void setUp() {
        genericSetUp("Collections.xml", "child");
    }
    
    @After
    public final void tearDown() {
        genericTearDown();
    }
    
    /*@Test
    @Order(order = 1)
    public final void testChildRecentCollectionsCollectionsCenterListChrome() {
        recentCollectionsCollectionsCenterListTest("chrome");
    }
    
    @Test
    @Order(order = 2)
    public final void testChildRecentCollectionsPayStationDetailsChrome() {
        recentCollectionsPayStationDetailsTest("chrome");
    }
    
    @Test
    @Order(order = 3)
    public final void testChildRecentCollectionsCollectionsCenterMapChrome() {
        recentCollectionsCollectionsCenterMapTest("chrome");
    }*/
    
    @Test
    @Order(order = 4)
    public final void testChildRecentCollectionsCollectionsCenterListFirefox() {
        recentCollectionsCollectionsCenterListTest("firefox");
    }
    
    @Test
    @Order(order = 5)
    public final void testChildRecentCollectionsPayStationDetailsFirefox() {
        recentCollectionsPayStationDetailsTest("firefox");
    }
    
    /*@Test
    @Order(order = 6)
    public final void testChildRecentCollectionsCollectionsCenterMapFirefox() {
        recentCollectionsCollectionsCenterMapTest("firefox");
    }
    
    @Test
    @Order(order = 7)
    public final void testChildRecentCollectionsCollectionsCenterListInternetExplorer() {
        recentCollectionsCollectionsCenterListTest("ie");
    }
    
    @Test
    @Order(order = 8)
    public final void testChildRecentCollectionsPayStationDetailsInternetExplorer() {
        recentCollectionsPayStationDetailsTest("ie");
    }
    
    @Test
    @Order(order = 9)
    public final void testChildRecentCollectionsCollectionsCenterMapInternetExplorer() {
        recentCollectionsCollectionsCenterMapTest("ie");
    }*/
}
