package com.digitalpaytech.automation.transactions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.digitalpaytech.automation.AutomationGlobalsTest;
import com.digitalpaytech.automation.transactionsuite.TransactionDetails;

public class TransDetailVerify extends AutomationGlobalsTest {
    
    private static final String ZERO_DOLLARS = "$0.00";
    private final TransactionDetails td;
    
    public TransDetailVerify(final TransactionDetails transactionDetails) {
        this.td = transactionDetails;
    }
    
    public final void assertTransDetails(final WebElement reportBody) {
        
        final List<WebElement> detailLabels = (ArrayList<WebElement>) reportBody.findElements(By.tagName("dt"));
        final List<WebElement> detailEntries = (ArrayList<WebElement>) reportBody.findElements(By.tagName("dd"));
        for (int k = 0; k < detailLabels.size(); k++) {
            final WebElement currentLabel = detailLabels.get(k);
            final WebElement currentEntry = detailEntries.get(k);
            final String currentLabelText = currentLabel.getText();
            final String currentEntryText = currentEntry.getText();
            matchPaymentDetails(currentLabelText, currentEntryText);
            matchAmountDetails(currentLabelText, currentEntryText);
            matchCCDetails(currentLabelText, currentEntryText);
            matchOtherDetails(currentLabelText, currentEntryText);
        }
    }
    
    private void matchPaymentDetails(final String currentLabelText, final String currentEntryText) {
        switch (currentLabelText) {
            case "Transaction #":
                assertEquals(currentEntryText, this.td.getTicketNumber());
                break;
            case "Payment Type":
                assertTrue(paymentTypeCheck(currentEntryText));
                break;
            case "Add Time Number":
                assertEquals(currentEntryText, this.td.getAddTimeNum());
                break;
            case "Purchase Date":
                assertEquals(currentEntryText, this.td.getPurchasedDate());
                break;
            case "Expiry Date":
                // using contains here to deal with (Active) in Permit Lookup
                assertTrue(currentEntryText.contains(this.td.getExpiryDate()));
                break;
            default:
                break;
        }
        
    }
    
    private void matchAmountDetails(final String currentLabelText, final String currentEntryText) {
        switch (currentLabelText) {
            case "Charged Amount":
                assertEquals(currentEntryText, ammountCheckNull(this.td.getChargedAmount()));
                break;
            case "Cash Paid":
                assertEquals(currentEntryText, ammountCheckNull(this.td.getCashPaid()));
                break;
            case "Card Paid":
                assertEquals(currentEntryText, ammountCheckNull(this.td.getCardPaid()));
                break;
            default:
                break;
        
        }
    }
    
    private void matchCCDetails(final String currentLabelText, final String currentEntryText) {
        switch (currentLabelText) {
            case "Status":
                assertTrue(currentEntryText.contains("Real-Time"));
                break;
            case "Authorization ID":
                assertEquals(currentEntryText, this.td.getCardAuthorizationId());
                break;
            default:
                break;
        }
    }
    
    private void matchOtherDetails(final String currentLabelText, final String currentEntryText) {
        switch (currentLabelText) {
        // TODO handle smart cards
            case "Smart Card Paid":
                assertEquals(currentEntryText, ammountCheckNull(null));
                break;
            // TODO handle pass cards
            case "Passcard Paid":
                assertEquals(currentEntryText, ammountCheckNull(null));
                break;
            case "Excess Payment":
                assertEquals(currentEntryText, ammountCheckNull(this.td.getExcessPayment()));
                break;
            case "Change Dispensed":
                assertEquals(currentEntryText, ammountCheckNull(null));
                break;
            case "Refund Slip":
                assertEquals(currentEntryText, "N/A");
                break;
            default:
                break;
        }
        
    }
    
    //Payment Type is used differently in permit lookup and transaction reports 
    private boolean paymentTypeCheck(final String paymentType) {
        boolean correct = false;
        final String[] pTypes = { "mastercard", "visa", "amex", "discover", "diners", "cash", "cash/card", "cash/cc", "creditcard", "passcard",
            "smartcard", "Credit Card", };
        for (String str : pTypes) {
            if (paymentType.equalsIgnoreCase(str)) {
                correct = true;
            }
        }
        return correct;
    }
    
    private String ammountCheckNull(final String toCheck) {
        if (toCheck == null || "".equals(toCheck)) {
            return ZERO_DOLLARS;
        } else {
            return "$" + toCheck;
        }
    }
    
    public final WebElement findTransaction(final WebElement transactionList) {
        final List<WebElement> transactions = (ArrayList<WebElement>) transactionList.findElements(By.tagName("li"));
        for (WebElement transaction : transactions) {
            
            final WebElement col1 = transaction.findElement(By.className("col1"));
            final WebElement col2 = transaction.findElement(By.className("col2"));
            final WebElement col3 = transaction.findElement(By.className("col3"));
            final WebElement col4 = transaction.findElement(By.className("col4"));
            final boolean col1Result = col1.getText().contains(this.td.getPurchasedDate());
            final boolean col2Result = col2.getText().contains(this.td.getType());
            final boolean col3Result = col3.getText().contains(this.td.getPaymentType());
            final boolean col4Result = col4.getText().contains(this.td.getChargedAmount());
            
            if (col1Result && col2Result && col3Result && col4Result) {
                return transaction;
            }
        }
        return transactionList;
    }
}
