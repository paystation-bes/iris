package com.digitalpaytech.automation.transactions;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.digitalpaytech.automation.AutomationGlobalsTest;
import com.digitalpaytech.automation.DataXmlParser;
import com.digitalpaytech.automation.Order;
import com.digitalpaytech.automation.OrderedRunner;
import com.digitalpaytech.automation.customer.navigation.CustomerNavigation;
import com.digitalpaytech.automation.transactionsuite.Transaction;
import com.digitalpaytech.automation.transactionsuite.TransactionDetails;

@RunWith(OrderedRunner.class)
public class CoinBillVerify extends AutomationGlobalsTest {
    
    private static final String COIN_TRANSACTION_FILE = "TransactionData/CoinOnlyData.xml";
    private static final String BILL_TRANSACTION_FILE = "TransactionData/BillOnlyData.xml";
    private static final String COIN_VALUES_FILE = "src/main/resources/TransactionData/CoinSummaryExpected.txt";
    private static final String BILL_VALUES_FILE = "src/main/resources/TransactionData/BillSummaryExpected.txt";
    private static final int THREE = 3;
    
    private String passwordChild;
    private String usernameChild;
    private String serialNumber;
    private int expectedCoinCount;
    private int expectedBillCount;
    private Double expectedCoinValue;
    private Double expectedBillValue;
    private CustomerNavigation customerNavigation;
    
    @Before
    public final void setUp() {
        final DataXmlParser dParse = new DataXmlParser();
        dParse.parse();
        this.passwordChild = dParse.getTestPasswordChild();
        this.usernameChild = dParse.getTestUsernameChild();
        testUrl = dParse.getTestUrl();
        this.testDriverDelay = dParse.getTestDriverDelay();
        this.browserType = dParse.getBrowserType();
        
        launchBrowser();
        this.customerNavigation = new CustomerNavigation(driver);
    }
    
    @After
    public final void tearDown() {
        super.driver.quit();
    }
    
    @Test
    @Order(order = 1)
    public final void verifyCoinTransaction() {
        verifyCashSummary(COIN_TRANSACTION_FILE, COIN_VALUES_FILE);
    }
    
    @Test
    @Order(order = 2)
    public final void verifyBillTransaction() {
        verifyCashSummary(BILL_TRANSACTION_FILE, BILL_VALUES_FILE);
    }
    
    public final void verifyCashSummary(final String xmlFile, final String expectedResults) {
        
        getExpectedValues(expectedResults);
        
        final Transaction transaction = new Transaction(xmlFile);
        final TransactionDetails td = transaction.getTransactionDetails();
        
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        LOGGER.info("Logging into " + this.usernameChild);
        login(this.usernameChild, this.passwordChild);
        driverWait();
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        LOGGER.info("Navigating to Collections");
        this.customerNavigation.navigateLeftHandMenu("Collections");
        driverWait();
        
        LOGGER.info("Navigating to Pay Station Summary");
        this.customerNavigation.navigatePageTabs("Pay Station Summary");
        driverWait();
        
        this.serialNumber = td.getPaystationCommAddress();
        
        LOGGER.info("Assert values in UI equal expected values");
        assertNewBalance();
        
    }
    
    private void getExpectedValues(final String path) {
        final File cbData = new File(path);
        try {
            final Scanner scanner = new Scanner(cbData);
            final String output = scanner.useDelimiter("\\Z").next();
            scanner.close();
            final String[] splitOut = output.split(",");
            
            this.expectedCoinCount = Integer.valueOf(splitOut[0]);
            this.expectedCoinValue = Double.valueOf(splitOut[1]);
            this.expectedBillCount = Integer.valueOf(splitOut[2]);
            this.expectedBillValue = Double.valueOf(splitOut[THREE]);
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    
    private String getDollarVal(final Double toFormat) {
        if (toFormat == 0.0) {
            return "$0.00";
        }
        final DecimalFormat df = new DecimalFormat("#.00");
        return "$" + df.format(toFormat);
        
    }
    
    private void assertNewBalance() {
        final WebElement posSummaryList = driver.findElement(By.id("posSummaryList"));
        final List<WebElement> posList = (ArrayList<WebElement>) posSummaryList.findElements(By.tagName("li"));
        for (WebElement element : posList) {
            final WebElement col1 = element.findElement(By.className("col1"));
            
            if (col1.getText().contains(this.serialNumber)) {
                final WebElement col4 = element.findElement(By.className("col4"));
                final WebElement col5 = element.findElement(By.className("col5"));
                final WebElement col6 = element.findElement(By.className("col6"));
                final WebElement col7 = element.findElement(By.className("col7"));
                
                assertEquals(col4.getText(), String.valueOf(this.expectedCoinCount));
                assertEquals(col5.getText(), getDollarVal(this.expectedCoinValue));
                assertEquals(col6.getText(), String.valueOf(this.expectedBillCount));
                assertEquals(col7.getText(), getDollarVal(this.expectedBillValue));
            }
        }
    }
}
