package com.digitalpaytech.automation.transactions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.digitalpaytech.automation.AutomationGlobalsTest;
import com.digitalpaytech.automation.DataXmlParser;
import com.digitalpaytech.automation.Order;
import com.digitalpaytech.automation.OrderedRunner;
import com.digitalpaytech.automation.customer.navigation.CustomerNavigation;
import com.digitalpaytech.automation.transactionsuite.Transaction;
import com.digitalpaytech.automation.transactionsuite.TransactionDetails;

@RunWith(OrderedRunner.class)
public class TransactionTest extends AutomationGlobalsTest {
    private static final String CASH_TRANSACTION_FILE = "TransactionData/CashTransactionData.xml";
    private static final String CC_POSTAUTH_FILE = "TransactionData/CCPostAuth.xml";
    private static final String MIXED_TRANSACTION_FILE = "TransactionData/CashAndCC.xml";
    private static final int ONE = 1;
    private static final int TWO = 2;
    private static final int THREE = 3;
    
    private String passwordChild;
    private String usernameChild;
    private CustomerNavigation customerNavigation;
    
    @Before
    public final void setUp() {
        final DataXmlParser dParse = new DataXmlParser();
        dParse.parse();
        this.passwordChild = dParse.getTestPasswordChild();
        this.usernameChild = dParse.getTestUsernameChild();
        testUrl = dParse.getTestUrl();
        this.testDriverDelay = dParse.getTestDriverDelay();
        this.browserType = dParse.getBrowserType();
        
        launchBrowser();
        this.customerNavigation = new CustomerNavigation(driver);
    }
    
    @After
    public final void tearDown() {
        super.driver.quit();
    }
    
    @Test
    @Order(order = ONE)
    public final void testCashTransaction() {
        final Transaction cashTransaction = new Transaction(CASH_TRANSACTION_FILE);
        cashTransaction.sendCashTransaction();
        assertTrue(cashTransaction.getSuccess());
        verifyTransactionReports(cashTransaction);
        
    }
    
    @Test
    @Order(order = TWO)
    public final void testCCTransaction() {
        final Transaction ccTransaction = new Transaction(CC_POSTAUTH_FILE);
        ccTransaction.sendCCTransaction();
        assertTrue(ccTransaction.getSuccess());
        verifyTransactionReports(ccTransaction);
        
    }
    
    @Test
    @Order(order = THREE)
    public final void testMixedTransaction() {
        final Transaction mixedTransaction = new Transaction(MIXED_TRANSACTION_FILE);
        mixedTransaction.sendCCTransaction();
        assertTrue(mixedTransaction.getSuccess());
        verifyTransactionReports(mixedTransaction);
        
    }
    
    private void verifyTransactionReports(final Transaction transaction) {
        
        final TransactionDetails transactionDetails = transaction.getTransactionDetails();
        final TransDetailVerify transTest = new TransDetailVerify(transactionDetails);
        
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        LOGGER.info("Logging into " + this.usernameChild);
        login(this.usernameChild, this.passwordChild);
        driverWait();
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        LOGGER.info("Navigating to Settings");
        this.customerNavigation.navigateLeftHandMenu("Settings");
        
        LOGGER.info("Finding Time Zone");
        setTimeZone(transactionDetails);
        
        LOGGER.info("Navigate to Pay Stations");
        // Navigate to Pay Stations
        this.customerNavigation.navigatePageTabs("Pay Stations");
        
        super.selectOptionFromDropDown("locationPOSAC", transactionDetails.getPaystationCommAddress());
        
        assertEquals("Digital Iris - Settings - Pay Stations : Pay Station List", super.driver.getTitle());
        driverWait();
        
        LOGGER.info("Click on Reports Tab");
        super.element = super.driver.findElement(By.xpath("//a[@class='tab' and @title='Reports']"));
        driverWait();
        super.element.click();
        driverWait();
        
        LOGGER.info("Click on Transaction Reports Tab");
        super.element = super.driver.findElement(By.xpath("//a[@id='transactionReportBtn' and @title='Transaction Reports']"));
        driverWait();
        super.element.click();
        driverWait();
        
        LOGGER.info("Finding the correct Transaction");
        final WebElement transactionList = super.driver.findElement(By.id("transactionReportList"));
        final WebElement transWebEl = transTest.findTransaction(transactionList);
        
        assertNotNull(transWebEl);
        transWebEl.click();
        driverWait();
        
        final WebElement transDetails = driver.findElement(By.className("ui-dialog-title"));
        assertEquals("TRANSACTION DETAILS", transDetails.getText().trim());
        driverWait();
        final WebElement reportBody = driver.findElement(By.className("reportBody"));
        
        LOGGER.info("Asserting Transaction Details are correct");
        transTest.assertTransDetails(reportBody);
        driverWait();
        
        LOGGER.info("Closing Transaction Details are correct");
        final WebElement closeButton = driver.findElement(By.xpath("//span[text()='Close']"));
        closeButton.click();
        driverWait();
        
        LOGGER.info("Navigate to Reports");
        this.customerNavigation.navigateLeftHandMenu("Reports");
        driverWait();
        
        LOGGER.info("Navigate to Permit Lookup");
        this.customerNavigation.navigatePageTabs("Permit Lookup");
        driverWait();
        
        LOGGER.info("Search for transaction by plate num");
        final WebElement pltSearch = driver.findElement(By.id("searchPlate"));
        final WebElement searchBttn = driver.findElement(By.xpath("//a[@title='Search']"));
        pltSearch.sendKeys(transactionDetails.getLicensePlateNo());
        driverWait();
        
        searchBttn.click();
        driverWait();
        
        final List<WebElement> permits = (ArrayList<WebElement>) driver.findElements(By.id("transReceiptListContainer"));
        
        LOGGER.info("Selecting correct Transaction");
        for (WebElement permit : permits) {
            final WebElement target = permit.findElement(By.className("transactionPlateSpot"));
            assertTrue(target.getText().contains(transactionDetails.getLicensePlateNo()));
            target.click();
        }
        
        LOGGER.info("Asserting transaction detials");
        final WebElement transReport = driver.findElement(By.id("transactionDetails"));
        transTest.assertTransDetails(transReport);
        driverWait();
        
        LOGGER.info("Asserting reciept details");
        final WebElement receiptExpires = driver.findElement(By.id("rcpt_expires"));
        final String receiptExpiry = replaceSpace(receiptExpires);
        assertTrue(receiptExpiry.contains(transactionDetails.getExpiryDate()));
        driverWait();
        
        final WebElement receiptPD = driver.findElement(By.id("rcpt_purchaseDate"));
        final WebElement receiptPT = driver.findElement(By.id("rcpt_purchaseTime"));
        final String purDate = replaceSpace(receiptPD);
        final String purTime = replaceSpace(receiptPT);
        assertTrue(transactionDetails.getPurchasedDate().contains(purDate));
        assertTrue(transactionDetails.getPurchasedDate().contains(purTime));
        
    }
    
    private void setTimeZone(final TransactionDetails transactionDetails) {
        final WebElement timeZoneWE = driver.findElement(By.id("curTimeZone"));
        
        // Set the timeZone to be correct for customer TZ
        final String timeZoneText = timeZoneWE.getText().trim();
        
        transactionDetails.setDateFmt("MMM dd, YYYY h:mm a");
        transactionDetails.setTimeZone(timeZoneText);
        transactionDetails.formatPurchaseDate();
        transactionDetails.formatExpiryDate();
    }
    
    private String replaceSpace(final WebElement toReplace) {
        return toReplace.getText().replaceAll("\n", " ");
    }
}
