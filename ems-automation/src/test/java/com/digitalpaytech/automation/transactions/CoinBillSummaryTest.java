package com.digitalpaytech.automation.transactions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.digitalpaytech.automation.AutomationGlobalsTest;
import com.digitalpaytech.automation.DataXmlParser;
import com.digitalpaytech.automation.Order;
import com.digitalpaytech.automation.OrderedRunner;
import com.digitalpaytech.automation.customer.navigation.CustomerNavigation;
import com.digitalpaytech.automation.transactionsuite.CoinBillSummer;
import com.digitalpaytech.automation.transactionsuite.Transaction;
import com.digitalpaytech.automation.transactionsuite.TransactionDetails;

@RunWith(OrderedRunner.class)
public class CoinBillSummaryTest extends AutomationGlobalsTest {
    
    private static final String COIN_TRANSACTION_FILE = "TransactionData/CoinOnlyData.xml";
    private static final String BILL_TRANSACTION_FILE = "TransactionData/BillOnlyData.xml";
    private static final String COIN_VALUES_FILE = "src/main/resources/TransactionData/CoinSummaryExpected.txt";
    private static final String BILL_VALUES_FILE = "src/main/resources/TransactionData/BillSummaryExpected.txt";
    private String passwordChild;
    private String usernameChild;
    private String serialNumber;
    private CustomerNavigation customerNavigation;
    
    private int originalCoinCount;
    private int originalBillCount;
    private Double originalCoinValue;
    private Double originalBillValue;
    
    private int coinCount;
    private Double coinValue;
    private int billCount;
    private Double billValue;
    
    @Before
    public final void setUp() {
        
        final DataXmlParser dParse = new DataXmlParser();
        dParse.parse();
        this.passwordChild = dParse.getTestPasswordChild();
        this.usernameChild = dParse.getTestUsernameChild();
        testUrl = dParse.getTestUrl();
        this.testDriverDelay = dParse.getTestDriverDelay();
        this.browserType = dParse.getBrowserType();
        
        launchBrowser();
        this.customerNavigation = new CustomerNavigation(driver);
    }
    
    @After
    public final void tearDown() {
        super.driver.quit();
    }
    
    @Test
    @Order(order = 1)
    public final void testCoinTrnasctions() {
        psSummaryTransaction(COIN_TRANSACTION_FILE, COIN_VALUES_FILE);
    }
    
    @Test
    @Order(order = 2)
    public final void testBillTrnasctions() {
        psSummaryTransaction(BILL_TRANSACTION_FILE, BILL_VALUES_FILE);
    }
    
    public final void psSummaryTransaction(final String xmlFile, final String expectedValuesFile) {
        
        final Transaction transaction = new Transaction(xmlFile);
        final TransactionDetails td = transaction.getTransactionDetails();
        
        super.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        LOGGER.info("Logging into " + this.usernameChild);
        login(this.usernameChild, this.passwordChild);
        driverWait();
        assertEquals("Digital Iris - Main (Dashboard)", super.driver.getTitle());
        driverWait();
        
        LOGGER.info("Navigating to Collections");
        this.customerNavigation.navigateLeftHandMenu("Collections");
        driverWait();
        
        LOGGER.info("Navigating to Pay Station Summary");
        this.customerNavigation.navigatePageTabs("Pay Station Summary");
        driverWait();
        
        this.serialNumber = td.getPaystationCommAddress();
        
        LOGGER.info("Finding original values in PS Summary table");
        findPSEntry();
        sumCoinBillValues(td);
        
        LOGGER.info("Send new coin only transaction");
        
        transaction.sendCashTransaction();
        assertTrue(transaction.getSuccess());
        
        writeCountToFile(expectedValuesFile);
        driverWait();
    }
    
    private void findPSEntry() {
        final WebElement posSummaryList = driver.findElement(By.id("posSummaryList"));
        final List<WebElement> posList = (ArrayList<WebElement>) posSummaryList.findElements(By.tagName("li"));
        for (WebElement element : posList) {
            final WebElement col1 = element.findElement(By.className("col1"));
            
            if (col1.getText().contains(this.serialNumber)) {
                final WebElement col4 = element.findElement(By.className("col4"));
                final WebElement col5 = element.findElement(By.className("col5"));
                final WebElement col6 = element.findElement(By.className("col6"));
                final WebElement col7 = element.findElement(By.className("col7"));
                
                this.originalCoinCount = getIntCount(col4.getText());
                this.originalCoinValue = getDblVal(col5.getText());
                this.originalBillCount = getIntCount(col6.getText());
                this.originalBillValue = getDblVal(col7.getText());
            }
        }
    }
    
    private void writeCountToFile(final String expectedValuesFile) {
        final File summaryData = new File(expectedValuesFile);
        final String comma = ",";
        try {
            final PrintWriter printWriter = new PrintWriter(summaryData);
            printWriter.println(this.coinCount + comma + this.coinValue + comma + this.billCount + comma + this.billValue + comma);
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            assertTrue(false);
        }
    }
    
    private void sumCoinBillValues(final TransactionDetails td) {
        final String coinCol = td.getCoinCol();
        final String billCol = td.getBillCol();
        final CoinBillSummer coinSummer = new CoinBillSummer(coinCol, billCol);
        this.coinCount = coinSummer.getCoinCount() + this.originalCoinCount;
        this.coinValue = coinSummer.getCoinValue() + this.originalCoinValue;
        this.billCount = coinSummer.getBillCount() + this.originalBillCount;
        this.billValue = coinSummer.getBillValue() + this.originalBillValue;
        
    }
    
    private int getIntCount(final String count) {
        return Integer.valueOf(count);
    }
    
    private Double getDblVal(final String val) {
        final String str = val.replace("$", "");
        return Double.valueOf(str);
    }
    
}
