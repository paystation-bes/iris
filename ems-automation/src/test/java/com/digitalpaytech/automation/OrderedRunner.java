package com.digitalpaytech.automation;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;

/*
 * Code courtesy of: http://stackoverflow.com/questions/3089151/specifying-an-order-to-junit-4-tests-at-the-method-level-not-class-level
 */
public class OrderedRunner extends BlockJUnit4ClassRunner {
    public OrderedRunner(final Class<?> klass) throws InitializationError {
        super(klass);
    }

    @Override
    protected final List<FrameworkMethod> computeTestMethods() {
        final List<FrameworkMethod> list = super.computeTestMethods();
        Collections.sort(list, new Comparator<FrameworkMethod>() {
            @Override
            public int compare(final FrameworkMethod f1, final FrameworkMethod f2) {
                final Order o1 = f1.getAnnotation(Order.class);
                final Order o2 = f2.getAnnotation(Order.class);
                
                if (o1 == null || o2 == null) {
                    return -1;
                }
                
                return o1.order() - o2.order();
            }
        });
        return list;
    }
}
