package com.digitalpaytech.automation.collections.recentcollections;

import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.digitalpaytech.automation.customer.collections.recentcollections.RecentCollectionsSuite;
import com.digitalpaytech.automation.customer.navigation.CustomerNavigation;
import com.digitalpaytech.automation.customer.settings.paystation.PayStationsSuite;
import com.digitalpaytech.automation.maps.MapSuite;

import com.digitalpaytech.automation.AutomationGlobalsTest;

public class RecentCollectionsSuiteTest extends RecentCollectionsSuite {
    
    private CustomerNavigation customerNavigation = new CustomerNavigation();
    
    /*
     * Starts a browser then logs into the specified user account.
     */
    private void setUpRecentCollectionsTest(final String browserType) {
        startBrowser(browserType);
        loginLogout.goToLoginPageAndLogin(super.userName, super.password, super.xmlData, super.driver);
    }
    
    /*
     * Method includes all the logic used in testing Recent Collections in the Collections Center list.
     */
    protected final void recentCollectionsCollectionsCenterListTest(final String browserType) {
        // Get the username of the user who performed the Collection (specified as the first CollectionUser in the XML).
        final String collectionUsername = getCollectionUsername();
        final MapSuite mapSuite = new MapSuite("Recent Collections", driver);
        this.setUpRecentCollectionsTest(browserType);
        //Making a list of existing recent collections types
        String[] collectionTypes = {"coin", "ccard", "bills", "total"};
        String[] CollectionTypes = {"Coin", "Card", "Bill", "1"};
        // Goes to the "Recent Collections" tab in the "Collections Center".
        // Ensures that map is fully loaded before continuing (even though we aren't testing the map, if
        // the map fails to load, the Collections list will also fail to load.
        driver.findElement(By.cssSelector("a[title='Collections']")).click();
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        driver.findElement(By.id("recentListTab")).click();
        //mapSuite.fullyLoadMap("Collections", "map");
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        //Going through each type of recent collections
        for (int i=0; i<collectionTypes.length; i++ )
        {
            driver.findElement(By.id(collectionTypes[i] + "Check")).click();
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            //It picks the first recent collection from the list by a default link
            if (collectionTypes[i].equals("ccard"))
                driver.findElement(By.cssSelector("h3")).click();
            else
                driver.findElement(By.cssSelector("section.severityNull.clickable")).click();
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            driver.findElement(By.id("collDetailBtn")).click();
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            assertEquals(driver.findElement(By.cssSelector("dl.fixedWidthFont > dd.detailValue")).getText(),CollectionTypes[i]);
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            driver.findElement(By.cssSelector(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")).click();
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            driver.findElement(By.id(collectionTypes[i] + "Check")).click();
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        }
        
        // Click collection in the list and verify it
        /*clickAndVerifyCollectionFromRecentCollectionsList(COLLECTION_TYPE_ALL, collectionUsername);
        clickAndVerifyCollectionFromRecentCollectionsList(COLLECTION_TYPE_BILL, collectionUsername);
        clickAndVerifyCollectionFromRecentCollectionsList(COLLECTION_TYPE_CARD, collectionUsername);
        clickAndVerifyCollectionFromRecentCollectionsList(COLLECTION_TYPE_COIN, collectionUsername);*/
        
        
    }
    
    /*
     * Method includes all the logic used in testing Recent Collections in the Collections Center map.
     */
    protected final void recentCollectionsCollectionsCenterMapTest(final String browserType) {
        // Get the username of the user who performed the Collection (specified as the first CollectionUser in the XML).
        final String collectionUsername = getCollectionUsername();
        final MapSuite mapSuite = new MapSuite("Recent Collections", driver);
        
        this.setUpRecentCollectionsTest(browserType);
        
        // Goes to the "Recent Collections" tab in the "Collections Center".
        // Ensures that map is fully loaded before continuing.
        driver.findElement(By.cssSelector("a[title='Collections']")).click();
        //mapSuite.fullyLoadMap("Collections", "map");
        
        // Verify collections in map
        mapSuite.setPayStationMapStructure();
        final String payStationName = xmlData.getPayStationByCustomerName(super.customerName).get(0).getName();
        final String payStationSerial = xmlData.getPayStationByCustomerName(super.customerName).get(0).getSerialNumber();
        LOGGER.info("Finding pay station " + payStationSerial + ".");
        super.payStationName = payStationName;
        super.payStationSerial = payStationSerial;
        mapSuite.findSpecificPayStationData(payStationSerial);
        verifyCollectionMapPopUp(collectionUsername);
        
        LOGGER.info("Map pop-up for pay station " + super.payStationSerial + " has been verified.");
    }
    
    /*
     * Method includes all the logic used in testing Recent Collections in the Pay Station Details section.
     */
    protected final void recentCollectionsPayStationDetailsTest(final String browserType) {
        // Get the username of the user who performed the Collection (specified as the first CollectionUser in the XML).
        final String collectionUsername = getCollectionUsername();
        final PayStationsSuite payStationsSuite = new PayStationsSuite();
        
        this.setUpRecentCollectionsTest(browserType);
        
        // Get to the "Pay Station Details" section of the first pay station specified in your XML
        /*customerNavigation.navigateLeftHandMenu("Settings");
        customerNavigation.navigatePageTabs("Pay Stations");*/
        driver.findElement(By.cssSelector("img[alt='Settings']")).click();
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        driver.findElement(By.linkText("Pay Stations")).click();
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        if (driver.findElements(By.cssSelector("section.severityNull")).size() > 0 && driver.findElement(By.cssSelector("section.severityNull")).isDisplayed())
            driver.findElement(By.cssSelector("section.severityNull")).click();
        else
            driver.findElement(By.cssSelector("section.severityMedium")).click();
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        driver.findElement(By.cssSelector("#reportsBtn > a.tab")).click();
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        //Making a list of all the existing recent collections
        String[] CollectionTypes = {"Coin", "Card", "Bill", "All"};
        for (int i = 0; i<CollectionTypes.length; i++)
        {
            //Going through each type of recent collection
            driver.findElement(By.xpath("//li/div/*[contains(., '" + CollectionTypes[i] + "')]")).click();
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            if (CollectionTypes[i].equals("All"))
                //If the type is All, the first element to be checked is only the report number
                assertEquals(driver.findElement(By.cssSelector("dl.fixedWidthFont > dt.detailLabel")).getText(), "Report #");
            else
                //Otherwise, the first element is the report type
                assertEquals(driver.findElement(By.cssSelector("dl.fixedWidthFont > dd.detailValue")).getText(), CollectionTypes[i]);
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            driver.findElement(By.cssSelector(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.ui-state-focus.active")).click();
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        }
        // Gets all the pay stations in the XML for the particular customer and clicks on the first one pay station in the XML.
        /*payStationsSuite.payStationElements.addAll((Collection<? extends WebElement>) driver.findElement(By.xpath("//ul[@id='paystationList']/span/li")));
        payStationsSuite.clickPayStationFromList(xmlData.getPayStationByCustomerName(super.customerName).get(0).getName());
        
        // Get basic pay station data
        //payStationsSuite.navigatePayStationDetailsTabs("Information");
        driver.findElement(By.xpath("//nav[@id='psNav']/div/a[@title='Information']")).click();
        setWebElementById("psName");
        super.payStationName = super.element.getText();
        setWebElementById("psSerialNumber");
        super.payStationSerial = super.element.getText();
        
        // Get to the "Collections Report" sub-tab
        driver.findElement(By.linkText("Reports")).click();
        /*payStationsSuite.navigatePayStationDetailsTabs("Reports");
        payStationsSuite.navigatePayStationDetailsReportsSubTabs("Collections Report");*/
        
        /*clickAndVerifyCollectionsFromPayStationDetailsList(COLLECTION_TYPE_ALL, collectionUsername);
        clickAndVerifyCollectionsFromPayStationDetailsList(COLLECTION_TYPE_BILL, collectionUsername);
        clickAndVerifyCollectionsFromPayStationDetailsList(COLLECTION_TYPE_CARD, collectionUsername);
        clickAndVerifyCollectionsFromPayStationDetailsList(COLLECTION_TYPE_COIN, collectionUsername);
        
        LOGGER.info("Collection Report pop-up for pay station " + super.payStationSerial + " has been verified.");*/
        
        
    }
}
