package com.digitalpaytech.automation.sessioncontrol;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.digitalpaytech.automation.XmlData;
import com.digitalpaytech.automation.dto.AutomationUser;

// These tests should be run in order to test the functionality of the "Service Agreement" and
// the "Change Password" pages.
public class NewUserLoginTest extends NewUserLogin {
    @Before
    public final void setUp() throws Exception {
        driver = null;
        super.xmlData = new XmlData("NewUserLogin.xml");
        // Scrolling on the Service Agreement page for IE does not work.
        // Therefore make sure to use either Chrome or FF instead.
        switch (super.xmlData.getBrowsers().get(0).toLowerCase()) {
            case "chrome":
                driver = new ChromeDriver();
                break;
            case "firefox":
                driver = new FirefoxDriver();
                break;
            default:
                driver = new FirefoxDriver();
                break;
        }
        
        driver.get(super.xmlData.getUrl());
        super.users = super.xmlData.getUserList();
    }
    
    @After
    public final void tearDown() throws Exception {
        driver.quit();
        driver = null;
    }
    
    @Test
    public final void testNewUserLogin() {
        // This method should be run after every nightly build
        final LoginLogout loginLogout = new LoginLogout();
        AutomationUser user;
        
        for (int i = 0; i < super.users.size(); i++) {
            user = super.users.get(i);
            super.customerName = user.getCustomerName();
            super.password = "password";
            super.userName = user.getUserName();
            super.userType = user.getUserType();
            
            LOGGER.info("NewUserLogin for: " + super.customerName + "with Password: " + super.password);
            
            assertEquals("Digital Iris - Login.", driver.getTitle());
            
            loginLogout.login(super.userName, super.password, super.driver);
            // If the user has already changed their password, the login will fail
            // and user will still be on login page.  This checks to see if login fails.
            // If login fails, this test case is ignored.
            assumeTrue(!"Digital Iris - Login.".equals(driver.getTitle()));
            Sleeper.sleepTightInSeconds(SLEEPTIMESECONDS);
            super.password = "Password$1";
            
            acceptServiceAgreement(super.customerName);
            Sleeper.sleepTightInSeconds(SLEEPTIMESECONDS);
            
            changeDefaultPassword(super.password);
            Sleeper.sleepTightInSeconds(SLEEPTIMESECONDS);
            
            loginLogout.logout(super.driver);
            Sleeper.sleepTightInSeconds(SLEEPTIMESECONDS);
            
            assertEquals("Digital Iris - Login.", driver.getTitle());
        }
        //assertTrue(true);
    }
}
