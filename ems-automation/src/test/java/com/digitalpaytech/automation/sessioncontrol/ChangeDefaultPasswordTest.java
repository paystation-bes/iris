package com.digitalpaytech.automation.sessioncontrol;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.digitalpaytech.automation.AutomationGlobalsTest;
import com.digitalpaytech.automation.Order;
import com.digitalpaytech.automation.OrderedRunner;
import com.digitalpaytech.automation.customer.navigation.CustomerNavigation;

//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(OrderedRunner.class)
public class ChangeDefaultPasswordTest extends AutomationGlobalsTest {
    
    @Before
    public void setUp() throws Exception {
        
        try {
            final DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            //final Document document = docBuilder.parse(new File(".///SeleniumMaven///Data.xml"));
            final Document document = docBuilder.parse(this.getClass().getClassLoader().getResourceAsStream("Data.xml"));
            
            //normalizing text representation
            document.getDocumentElement().normalize();
            
            // Obtaining URL
            final NodeList urlNodeList = document.getElementsByTagName("URL");
            //Node UrlNode = UrlNodeList.item(0);
            final Element url = (Element) urlNodeList.item(0);
            this.testUrl = url.getTextContent();
            
            // Obtaining Child Username
            final NodeList usernameNodeListChild = document.getElementsByTagName("ChildUsername");
            final Element usernameChild = (Element) usernameNodeListChild.item(0);
            this.testUsernameChild = usernameChild.getTextContent();
            
            // Obtaining Child Password
            final NodeList passwordNodeListChild = document.getElementsByTagName("ChildPassword");
            final Element passwordChild = (Element) passwordNodeListChild.item(0);
            this.testPasswordChild = passwordChild.getTextContent();
            
            // Obtaining Standalone Username
            final NodeList usernameNodeListStandAlone = document.getElementsByTagName("StandAloneUsername");
            final Element usernameSA = (Element) usernameNodeListStandAlone.item(0);
            this.testUsernameStandalone = usernameSA.getTextContent();
            
            // Obtaining Standalone Password
            final NodeList passwordNodeListStandAlone = document.getElementsByTagName("StandAlonePassword");
            final Element passwordSA = (Element) passwordNodeListStandAlone.item(0);
            this.testPasswordStandalone = passwordSA.getTextContent();
            
            // Obtaining Parent Username
            final NodeList usernameNodeListParent = document.getElementsByTagName("ParentUsername");
            final Element usernameParent = (Element) usernameNodeListParent.item(0);
            this.testUsernameParent = usernameParent.getTextContent();
            
            // Obtaining Parent Password
            final NodeList passwordNodeListParent = document.getElementsByTagName("ParentPassword");
            final Element passwordParent = (Element) passwordNodeListParent.item(0);
            this.testPasswordParent = passwordParent.getTextContent();
            
            /// Obtaining System Admin Username
            final NodeList usernameNodeListSystemAdmin = document.getElementsByTagName("SystemAdminUsrename");
            final Element usernameSystemAdmin = (Element) usernameNodeListSystemAdmin.item(0);
            this.testUsernameSystemAdmin = usernameSystemAdmin.getTextContent();
            
            /// Obtaining Digital Collect Username
            final NodeList usernameNodeListCollectChild = document.getElementsByTagName("CollectChildUsername");
            final Element usernameCollectChild = (Element) usernameNodeListCollectChild.item(0);
            this.testUsernameCollectChild = usernameCollectChild.getTextContent();
            
            /// Obtaining Digital Collect Username
            final NodeList usernameNodeListCollectStandalone = document.getElementsByTagName("CollectSAUsername");
            final Element usernameCollectStandalone = (Element) usernameNodeListCollectStandalone.item(0);
            this.testUsernameCollectStandalone = usernameCollectStandalone.getTextContent();
            
            // Obtaining System Admin Password
            final NodeList passwordNodeListSystemAdmin = document.getElementsByTagName("SystemAdminPassword");
            final Element passwordSystemAdmin = (Element) passwordNodeListSystemAdmin.item(0);
            this.testPasswordSystemAdmin = passwordSystemAdmin.getTextContent();
            
            // Obtaining default password = "password"
            final NodeList defaultPasswordNodeListSystemAdmin = document.getElementsByTagName("DefaultPassword");
            final Element defaultPasswordSystemAdmin = (Element) defaultPasswordNodeListSystemAdmin.item(0);
            this.testPasswordDefault = defaultPasswordSystemAdmin.getTextContent();
            
            // Obtaining Delay
            final NodeList delayList = document.getElementsByTagName("DriverDelayMilliSecond");
            final Element webDriverDelay = (Element) delayList.item(0);
            this.testDriverDelay = webDriverDelay.getTextContent();
            
            // Obtaining Browser Type
            final NodeList browserList = document.getElementsByTagName("BrowserType");
            final Element browserType = (Element) browserList.item(0);
            this.browserType = browserType.getTextContent();
            
            // launching specific browser type
            launchBrowser();
            
        }
        
        catch (SAXParseException err) {
            //            System.out.println("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId());
            //            System.out.println(" " + err.getMessage());
            
        } catch (SAXException e) {
            final Exception x = e.getException();
            ((x == null) ? e : x).printStackTrace();
            
        } catch (Throwable t) {
            t.printStackTrace();
        }
        
    }
    
    @Test
    @Order(order = 1)
    public void TestStandAloneCustomer() {
        
        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameStandalone, this.testPasswordDefault);
        driverWait();
        
        // check to see if the password is already changed or not
        assumeTrue(!"Digital Iris - Login.".equals(this.driver.getTitle()));
        
        // Fill Out Service Agreement
        /*this.element = this.driver.findElement(By.xpath("//input[@id='name']"));
        this.element.sendKeys("Arash");
        driverWait();
        
        this.element = this.driver.findElement(By.xpath("//input[@id='title']"));
        this.element.sendKeys("Automation");
        driverWait();
        
        this.element = this.driver.findElement(By.xpath("//input[@id='organization']"));
        this.element.sendKeys("DPT");
        driverWait();
        
        this.element = this.driver.findElement(By.xpath("//article[@id='saDetails']"));
        this.element.click();
        for (int i = 0; i < BROWSER_SCROLL_COUNT; i++) {
            this.element.sendKeys(Keys.END);
        }
        this.element = this.driver.findElement(By.xpath("//input[@id='saAccept']"));
        this.element.click();*/
        
        // change password
        this.element = this.driver.findElement(By.xpath("//input[@id='newPassword']"));
        this.element.sendKeys(this.testPasswordStandalone);
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='c_newPassword']"));
        this.element.sendKeys(this.testPasswordStandalone);
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='changePassword']"));
        this.element.click();
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", this.driver.getTitle());
        driverWait();
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 2)
    public void TestParentCustomer() {

        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameParent, this.testPasswordDefault);
        driverWait();
        
        // check to see if the password is already changed or not
        assumeTrue(!"Digital Iris - Login.".equals(this.driver.getTitle()));
        
        // Fill Out Service Agreement
        /*this.element = this.driver.findElement(By.xpath("//input[@id='name']"));
        this.element.sendKeys("Arash");
        driverWait();
        
        this.element = this.driver.findElement(By.xpath("//input[@id='title']"));
        this.element.sendKeys("Automation");
        driverWait();
        
        this.element = this.driver.findElement(By.xpath("//input[@id='organization']"));
        this.element.sendKeys("DPT");
        driverWait();
        
        // Scroll Down the Text
        //Actions action = new Actions(driver);
        //action.sendKeys(Keys.DOWN).perform();
        //action.keyDown(Keys.CONTROL).sendKeys(Keys.PAGE_DOWN).perform();
        //for(int i=0; i<20; i++) {
        //	driver.findElement(By.tagName("body")).sendKeys(Keys.DOWN);
        //}
        this.element = this.driver.findElement(By.xpath("//article[@id='saDetails']"));
        this.element.click();
        for (int i = 0; i < BROWSER_SCROLL_COUNT; i++) {
            this.element.sendKeys(Keys.END);
        }
        this.element = this.driver.findElement(By.xpath("//input[@id='saAccept']"));
        this.element.click();*/
        
        // change password
        this.element = this.driver.findElement(By.xpath("//input[@id='newPassword']"));
        this.element.sendKeys(this.testPasswordParent);
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='c_newPassword']"));
        this.element.sendKeys(this.testPasswordParent);
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='changePassword']"));
        this.element.click();
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", this.driver.getTitle());
        driverWait();
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 3)
    public void TestChildCustomer() {
        
        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameChild, this.testPasswordDefault);
        driverWait();
        
        // check to see if the password is already changed or not
        assumeTrue(!"Digital Iris - Login.".equals(this.driver.getTitle()));
        
        // change password
        this.element = this.driver.findElement(By.xpath("//input[@id='newPassword']"));
        this.element.sendKeys(this.testPasswordChild);
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='c_newPassword']"));
        this.element.sendKeys(this.testPasswordChild);
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='changePassword']"));
        this.element.click();
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", this.driver.getTitle());
        driverWait();
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 4)
    public void TestSystemAdmin() {
        
        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameSystemAdmin, this.testPasswordDefault);
        driverWait();
        
        // check to see if the password is already changed or not
        assumeTrue(!"Digital Iris - Login.".equals(this.driver.getTitle()));
        
        // Fill Out Service Agreement
        /*this.element = this.driver.findElement(By.xpath("//input[@id='name']"));
        this.element.sendKeys("ArashT");
        driverWait();
        
        this.element = this.driver.findElement(By.xpath("//input[@id='title']"));
        this.element.sendKeys("Automation");
        driverWait();
        
        this.element = this.driver.findElement(By.xpath("//input[@id='organization']"));
        this.element.sendKeys("DPT");
        driverWait();
        
        // Scroll Down the Text
        this.element = this.driver.findElement(By.xpath("//article[@id='saDetails']"));
        this.element.click();
        for (int i = 0; i < BROWSER_SCROLL_COUNT; i++) {
            this.element.sendKeys(Keys.END);
        }
        this.element = this.driver.findElement(By.xpath("//input[@id='saAccept']"));
        this.element.click();*/
        
        // change password
        this.element = this.driver.findElement(By.xpath("//input[@id='newPassword']"));
        this.element.sendKeys(this.testPasswordSystemAdmin);
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='c_newPassword']"));
        this.element.sendKeys(this.testPasswordSystemAdmin);
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='changePassword']"));
        this.element.click();
        driverWait();
        
        assertEquals("Digital Iris - System Administration", this.driver.getTitle());
        driverWait();
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 5)
    public void TestChildCollect() {

        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameCollectChild, this.testPasswordDefault);
        driverWait();
        
        // check to see if the password is already changed or not
        assumeTrue(!"Digital Iris - Login.".equals(this.driver.getTitle()));
        
        // change password
        this.element = this.driver.findElement(By.xpath("//input[@id='newPassword']"));
        this.element.sendKeys(this.testPasswordChild);
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='c_newPassword']"));
        this.element.sendKeys(this.testPasswordChild);
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='changePassword']"));
        this.element.click();
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", this.driver.getTitle());
        driverWait();
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 6)
    public void TestStandaloneCollect() {

        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameCollectStandalone, this.testPasswordDefault);
        driverWait();
        
        // check to see if the password is already changed or not
        assumeTrue(!"Digital Iris - Login.".equals(this.driver.getTitle()));
        
        // change password
        this.element = this.driver.findElement(By.xpath("//input[@id='newPassword']"));
        this.element.sendKeys(this.testPasswordChild);
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='c_newPassword']"));
        this.element.sendKeys(this.testPasswordChild);
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='changePassword']"));
        this.element.click();
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", this.driver.getTitle());
        driverWait();
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 7)
    public void Test2ParentCustomer() {

        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login("qaAutomation2@parent", "password");
        driverWait();
        
        // check to see if the password is already changed or not
        assumeTrue(!"Digital Iris - Login.".equals(this.driver.getTitle()));
        
        // Fill Out Service Agreement
        /*this.element = this.driver.findElement(By.xpath("//input[@id='name']"));
        this.element.sendKeys("Arash");
        driverWait();
        
        this.element = this.driver.findElement(By.xpath("//input[@id='title']"));
        this.element.sendKeys("Automation");
        driverWait();
        
        this.element = this.driver.findElement(By.xpath("//input[@id='organization']"));
        this.element.sendKeys("DPT");
        driverWait();
        
        // Scroll Down the Text
        //Actions action = new Actions(driver);
        //action.sendKeys(Keys.DOWN).perform();
        //action.keyDown(Keys.CONTROL).sendKeys(Keys.PAGE_DOWN).perform();
        //for(int i=0; i<20; i++) {
        //  driver.findElement(By.tagName("body")).sendKeys(Keys.DOWN);
        //}
        this.element = this.driver.findElement(By.xpath("//article[@id='saDetails']"));
        this.element.click();
        for (int i = 0; i < BROWSER_SCROLL_COUNT; i++) {
            this.element.sendKeys(Keys.END);
        }
        this.element = this.driver.findElement(By.xpath("//input[@id='saAccept']"));
        this.element.click();*/
        
        // change password
        this.element = this.driver.findElement(By.xpath("//input[@id='newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='c_newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='changePassword']"));
        this.element.click();
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", this.driver.getTitle());
        driverWait();
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 8)
    public void OranjParentCustomer() {

        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login("oranjparentqa", "password");
        driverWait();
        
        // check to see if the password is already changed or not
        assumeTrue(!"Digital Iris - Login.".equals(this.driver.getTitle()));
        
        // Fill Out Service Agreement
        /*this.element = this.driver.findElement(By.xpath("//input[@id='name']"));
        this.element.sendKeys("Arash");
        driverWait();
        
        this.element = this.driver.findElement(By.xpath("//input[@id='title']"));
        this.element.sendKeys("Automation");
        driverWait();
        
        this.element = this.driver.findElement(By.xpath("//input[@id='organization']"));
        this.element.sendKeys("DPT");
        driverWait();
        
        // Scroll Down the Text
        //Actions action = new Actions(driver);
        //action.sendKeys(Keys.DOWN).perform();
        //action.keyDown(Keys.CONTROL).sendKeys(Keys.PAGE_DOWN).perform();
        //for(int i=0; i<20; i++) {
        //  driver.findElement(By.tagName("body")).sendKeys(Keys.DOWN);
        //}
        this.element = this.driver.findElement(By.xpath("//article[@id='saDetails']"));
        this.element.click();
        for (int i = 0; i < BROWSER_SCROLL_COUNT; i++) {
            this.element.sendKeys(Keys.END);
        }
        this.element = this.driver.findElement(By.xpath("//input[@id='saAccept']"));
        this.element.click();*/
        
        // change password
        this.element = this.driver.findElement(By.xpath("//input[@id='newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='c_newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='changePassword']"));
        this.element.click();
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", this.driver.getTitle());
        driverWait();
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 9)
    public void OranjAdminCustomer() {
        
        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login("admin@oranj", this.testPasswordDefault);
        driverWait();
        
        // check to see if the password is already changed or not
        assumeTrue(!"Digital Iris - Login.".equals(this.driver.getTitle()));
        
        // change password
        this.element = this.driver.findElement(By.xpath("//input[@id='newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='c_newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='changePassword']"));
        this.element.click();
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", this.driver.getTitle());
        driverWait();
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 10)
    public void TestQA1ChildCustomer() {
        
        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login("admin@qa1child", this.testPasswordDefault);
        driverWait();
        
        // check to see if the password is already changed or not
        assumeTrue(!"Digital Iris - Login.".equals(this.driver.getTitle()));
        
        // change password
        this.element = this.driver.findElement(By.xpath("//input[@id='newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='c_newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='changePassword']"));
        this.element.click();
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", this.driver.getTitle());
        driverWait();
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 11)
    public void Test2ChildCustomer() {
        
        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login("qaAutomation2@child", this.testPasswordDefault);
        driverWait();
        
        // check to see if the password is already changed or not
        assumeTrue(!"Digital Iris - Login.".equals(this.driver.getTitle()));
        
        // change password
        this.element = this.driver.findElement(By.xpath("//input[@id='newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='c_newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='changePassword']"));
        this.element.click();
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", this.driver.getTitle());
        driverWait();
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 12)
    public void TestQA1Child2Customer() {
        
        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login("admin@qa1child2", this.testPasswordDefault);
        driverWait();
        
        // check to see if the password is already changed or not
        assumeTrue(!"Digital Iris - Login.".equals(this.driver.getTitle()));
        
        // change password
        this.element = this.driver.findElement(By.xpath("//input[@id='newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='c_newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='changePassword']"));
        this.element.click();
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", this.driver.getTitle());
        driverWait();
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 13)
    public void TestQA2ChildCustomer() {
        
        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login("admin@qa2child", this.testPasswordDefault);
        driverWait();
        
        // check to see if the password is already changed or not
        assumeTrue(!"Digital Iris - Login.".equals(this.driver.getTitle()));
        
        // change password
        this.element = this.driver.findElement(By.xpath("//input[@id='newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='c_newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='changePassword']"));
        this.element.click();
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", this.driver.getTitle());
        driverWait();
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 14)
    public void TestQA2Child2Customer() {
        
        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login("admin@qa2child2", this.testPasswordDefault);
        driverWait();
        
        // check to see if the password is already changed or not
        assumeTrue(!"Digital Iris - Login.".equals(this.driver.getTitle()));
        
        // change password
        this.element = this.driver.findElement(By.xpath("//input[@id='newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='c_newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='changePassword']"));
        this.element.click();
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", this.driver.getTitle());
        driverWait();
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 15)
    public void OranjLizaCustomer() {
        
        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login("liza@oranj", this.testPasswordDefault);
        driverWait();
        
        // check to see if the password is already changed or not
        assumeTrue(!"Digital Iris - Login.".equals(this.driver.getTitle()));
        
        // change password
        this.element = this.driver.findElement(By.xpath("//input[@id='newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='c_newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='changePassword']"));
        this.element.click();
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", this.driver.getTitle());
        driverWait();
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 16)
    public void OranjChildCustomer() {
        
        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login("oranjchildqa", this.testPasswordDefault);
        driverWait();
        
        // check to see if the password is already changed or not
        assumeTrue(!"Digital Iris - Login.".equals(this.driver.getTitle()));
        
        // change password
        this.element = this.driver.findElement(By.xpath("//input[@id='newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='c_newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='changePassword']"));
        this.element.click();
        driverWait();
        
        assertEquals("Digital Iris - Main (Dashboard)", this.driver.getTitle());
        driverWait();
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 17)
    public void Test2SystemAdmin() {
        
        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login("qaAutomation2@systemadmin", this.testPasswordDefault);
        driverWait();
        
        // check to see if the password is already changed or not
        assumeTrue(!"Digital Iris - Login.".equals(this.driver.getTitle()));
        
        // Fill Out Service Agreement
        /*this.element = this.driver.findElement(By.xpath("//input[@id='name']"));
        this.element.sendKeys("ArashT");
        driverWait();
        
        this.element = this.driver.findElement(By.xpath("//input[@id='title']"));
        this.element.sendKeys("Automation");
        driverWait();
        
        this.element = this.driver.findElement(By.xpath("//input[@id='organization']"));
        this.element.sendKeys("DPT");
        driverWait();
        
        // Scroll Down the Text
        this.element = this.driver.findElement(By.xpath("//article[@id='saDetails']"));
        this.element.click();
        for (int i = 0; i < BROWSER_SCROLL_COUNT; i++) {
            this.element.sendKeys(Keys.END);
        }
        this.element = this.driver.findElement(By.xpath("//input[@id='saAccept']"));
        this.element.click();*/
        
        // change password
        this.element = this.driver.findElement(By.xpath("//input[@id='newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='c_newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='changePassword']"));
        this.element.click();
        driverWait();
        
        assertEquals("Digital Iris - System Administration", this.driver.getTitle());
        driverWait();
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 18)
    public void Test2StandAlone() {
        
        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login("qaAutomation2@standalone", this.testPasswordDefault);
        driverWait();
        
        // check to see if the password is already changed or not
        assumeTrue(!"Digital Iris - Login.".equals(this.driver.getTitle()));
        
        // Fill Out Service Agreement
        /*this.element = this.driver.findElement(By.xpath("//input[@id='name']"));
        this.element.sendKeys("ArashT");
        driverWait();
        
        this.element = this.driver.findElement(By.xpath("//input[@id='title']"));
        this.element.sendKeys("Automation");
        driverWait();
        
        this.element = this.driver.findElement(By.xpath("//input[@id='organization']"));
        this.element.sendKeys("DPT");
        driverWait();
        
        // Scroll Down the Text
        this.element = this.driver.findElement(By.xpath("//article[@id='saDetails']"));
        this.element.click();
        for (int i = 0; i < BROWSER_SCROLL_COUNT; i++) {
            this.element.sendKeys(Keys.END);
        }
        this.element = this.driver.findElement(By.xpath("//input[@id='saAccept']"));
        this.element.click();*/
        
        // change password
        this.element = this.driver.findElement(By.xpath("//input[@id='newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='c_newPassword']"));
        this.element.sendKeys("Password$1");
        driverWait();
        this.element = this.driver.findElement(By.xpath("//input[@id='changePassword']"));
        this.element.click();
        driverWait();
        
        assertEquals("Digital Iris - System Administration", this.driver.getTitle());
        driverWait();
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @After
    public void tearDown() throws Exception {
        
        this.driver.quit();
        //driver.close();
    }
    
}
