package com.digitalpaytech.automation.sessioncontrol;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.digitalpaytech.automation.AutomationGlobalsTest;

public class UserLoginLogoutTest extends AutomationGlobalsTest {
    
    @Before
    public void setUp() throws Exception {
        
        try {
            final DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            //final Document document = docBuilder.parse(new File(".///SeleniumMaven///Data.xml"));
            final Document document = docBuilder.parse(this.getClass().getClassLoader().getResourceAsStream("Data.xml"));
            
            //normalizing text representation
            document.getDocumentElement().normalize();
            
            // Obtaining URL
            final NodeList urlNodeList = document.getElementsByTagName("URL");
            //Node UrlNode = UrlNodeList.item(0);
            final Element url = (Element) urlNodeList.item(0);
            this.testUrl = url.getTextContent();
            
            // Obtaining Child Username
            final NodeList usernameNodeListChild = document.getElementsByTagName("ChildUsername");
            final Element usernameChild = (Element) usernameNodeListChild.item(0);
            this.testUsernameChild = usernameChild.getTextContent();
            
            // Obtaining Child Password
            final NodeList passwordNodeListChild = document.getElementsByTagName("ChildPassword");
            final Element passwordChild = (Element) passwordNodeListChild.item(0);
            this.testPasswordChild = passwordChild.getTextContent();
            
            // Obtaining Standalone Username
            final NodeList usernameNodeListStandAlone = document.getElementsByTagName("StandAloneUsername");
            final Element usernameSA = (Element) usernameNodeListStandAlone.item(0);
            this.testUsernameStandalone = usernameSA.getTextContent();
            
            // Obtaining Standalone Password
            final NodeList passwordNodeListStandAlone = document.getElementsByTagName("StandAlonePassword");
            final Element passwordSA = (Element) passwordNodeListStandAlone.item(0);
            this.testPasswordStandalone = passwordSA.getTextContent();
            
            // Obtaining Parent Username
            final NodeList usernameNodeListParent = document.getElementsByTagName("ParentUsername");
            final Element usernameParent = (Element) usernameNodeListParent.item(0);
            this.testUsernameParent = usernameParent.getTextContent();
            
            // Obtaining Parent Password
            final NodeList passwordNodeListParent = document.getElementsByTagName("ParentPassword");
            final Element passwordParent = (Element) passwordNodeListParent.item(0);
            this.testPasswordParent = passwordParent.getTextContent();
            
            /// Obtaining Digital Collect Username
            final NodeList usernameNodeListCollectChild = document.getElementsByTagName("CollectChildUsername");
            final Element usernameCollectChild = (Element) usernameNodeListCollectChild.item(0);
            this.testUsernameCollectChild = usernameCollectChild.getTextContent();
            
            /// Obtaining Digital Collect Username
            final NodeList usernameNodeListCollectStandalone = document.getElementsByTagName("CollectSAUsername");
            final Element usernameCollectStandalone = (Element) usernameNodeListCollectStandalone.item(0);
            this.testUsernameCollectStandalone = usernameCollectStandalone.getTextContent();
            
            /// Obtaining System Admin Username
            final NodeList usernameNodeListSystemAdmin = document.getElementsByTagName("SystemAdminUsrename");
            final Element usernameSystemAdmin = (Element) usernameNodeListSystemAdmin.item(0);
            this.testUsernameSystemAdmin = usernameSystemAdmin.getTextContent();
            
            // Obtaining System Admin Password
            final NodeList passwordNodeListSystemAdmin = document.getElementsByTagName("SystemAdminPassword");
            final Element passwordSystemAdmin = (Element) passwordNodeListSystemAdmin.item(0);
            this.testPasswordSystemAdmin = passwordSystemAdmin.getTextContent();
            
            // Obtaining Delay
            final NodeList delayList = document.getElementsByTagName("DriverDelayMilliSecond");
            final Element webDriverDelay = (Element) delayList.item(0);
            this.testDriverDelay = webDriverDelay.getTextContent();
            
            // Obtaining Browser Type
            final NodeList browserList = document.getElementsByTagName("BrowserType");
            final Element browserType = (Element) browserList.item(0);
            this.browserType = browserType.getTextContent();
            
            // launching specific browser type
            launchBrowser();
            
        }
        
        catch (SAXParseException err) {
            //            System.out.println("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId());
            //            System.out.println(" " + err.getMessage());
            
        } catch (SAXException e) {
            final Exception x = e.getException();
            ((x == null) ? e : x).printStackTrace();
            
        } catch (Throwable t) {
            t.printStackTrace();
        }
        
    }
    
    @Test
    public void testChildCustomer() {

        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        //webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("userLabel")));
        // Log into test account
        login(this.testUsernameChild, this.testPasswordChild);
        this.driverWait();
        // Verifying page title. The test stops as it throws an exception if it fails
        assertEquals("Digital Iris - Main (Dashboard)", this.driver.getTitle());
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        driverWait();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    public void testStandAloneCustomer() {

        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameStandalone, this.testPasswordStandalone);
        driverWait();
        
        // Verifying page title. The test stops as it throws an exception if it fails
        assertEquals("Digital Iris - Main (Dashboard)", this.driver.getTitle());
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    public void testParentCustomer() {

        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameParent, this.testPasswordParent);
        driverWait();
        
        // Verifying page title
        // The test stops as it throws an exception if it fails
        assertEquals("Digital Iris - Main (Dashboard)", this.driver.getTitle());
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    public void testSystemAdmin() {

        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameSystemAdmin, this.testPasswordSystemAdmin);
        driverWait();
        
        // Verifying page title
        // The test stops as it throws an exception if it fails
        assertEquals("Digital Iris - System Administration", this.driver.getTitle());
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    public void testChildCollect() {

        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameCollectChild, this.testPasswordSystemAdmin);
        driverWait();
        
        // Verifying page title
        // The test stops as it throws an exception if it fails
        assertEquals("Digital Iris - Main (Dashboard)", this.driver.getTitle());
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        driverWait();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @Test
    public void testStandaloneCollect() {

        // Go to Iris
        driverWait();
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameCollectStandalone, this.testPasswordSystemAdmin);
        driverWait();
        
        // Verifying page title
        // The test stops as it throws an exception if it fails
        assertEquals("Digital Iris - Main (Dashboard)", this.driver.getTitle());
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        driverWait();
        //assertTrue(true);
    }
    
    @After
    public void tearDown() throws Exception {
        
        this.driver.quit();
        //driver.close();
    }
    
}
