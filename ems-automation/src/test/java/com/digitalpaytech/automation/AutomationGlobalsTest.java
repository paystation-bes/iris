package com.digitalpaytech.automation;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AutomationGlobalsTest extends AutomationGlobals {
    
    protected static final Logger LOGGER = Logger.getLogger(AutomationGlobalsTest.class);
    protected static final int WAIT_TIME_TWENTY_SECONDS = 20;
    protected static final int BROWSER_SCROLL_COUNT = 10;
    private static final int IMPLICIT_WAIT_TIME = 30;
    private static final int WEB_DRIVER_WAIT_TIME = 5;
    
    protected String testUrl = new String("");
    protected String testUsernameParent = new String("");
    protected String testPasswordParent = new String("");
    protected String testUsernameChild = new String("");
    protected String testPasswordChild = new String("");
    protected String testUsernameStandalone = new String("");
    protected String testPasswordStandalone = new String("");
    protected String testUsernameSystemAdmin = new String("");
    protected String testPasswordSystemAdmin = new String("");
    protected String testPasswordDefault = new String("");
    protected String testUsernameCollectChild = new String("");
    protected String testUsernameCollectStandalone = new String("");
    
    protected String testDriverDelay = new String("");
    protected WebElement element;

    protected String browserType = new String("");
   

	protected WebDriverWait webDriverWait;
    		
    // defining webDriver
    
	
    public final void logout() {
        super.element = super.selectWebElementByXpath("//a[@title='Logout']");
        super.element.click();
    }
    
    // Logs into specified account
    public final void login(final String username, final String password) {
        WebElement element = super.driver.findElement(By.name("j_username"));
        element.sendKeys(username);
        driverWait();
        element = super.driver.findElement(By.name("j_password"));
        element.sendKeys(password);
        driverWait();
        element = super.driver.findElement(By.name("submit"));
        element.click();
    }
    
    public final void driverWait() {
        try {
            Thread.sleep(Long.parseLong(this.testDriverDelay));
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }
    
    public final void launchBrowser() {
        //Check to see which browser is to be run and launch the browser
        if (this.browserType.equalsIgnoreCase("Firefox")) {
            super.driver = new FirefoxDriver();
        }
        
        if (this.browserType.equalsIgnoreCase("IE")) {
            super.driver = new InternetExplorerDriver();
        }
        
        if (this.browserType.equalsIgnoreCase("Chrome")) {
            super.driver = new ChromeDriver();
        }
        
        this.webDriverWait = new WebDriverWait(super.driver, WEB_DRIVER_WAIT_TIME);
        super.driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT_TIME, TimeUnit.SECONDS);
    }
    
    public final String getTestUrl() {
        return this.testUrl;
    }
    
    public final void setTestUrl(final String testUrl) {
        this.testUrl = testUrl;
    }
    
    public final String getTestUsernameParent() {
        return this.testUsernameParent;
    }
    
    public final void setTestUsernameParent(final String testUsernameParent) {
        this.testUsernameParent = testUsernameParent;
    }
    
    public final String getTestPasswordParent() {
        return this.testPasswordParent;
    }
    
    public final void setTestPasswordParent(final String testPasswordParent) {
        this.testPasswordParent = testPasswordParent;
    }
    
    public final String getTestUsernameChild() {
        return this.testUsernameChild;
    }
    
    public final void setTestUsernameChild(final String testUsernameChild) {
        this.testUsernameChild = testUsernameChild;
    }
    
    public final String getTestPasswordChild() {
        return this.testPasswordChild;
    }
    
    public final void setTestPasswordChild(final String testPasswordChild) {
        this.testPasswordChild = testPasswordChild;
    }
    
    public final String getTestUsernameStandalone() {
        return this.testUsernameStandalone;
    }
    
    public final void setTestUsernameStandalone(final String testUsernameStandalone) {
        this.testUsernameStandalone = testUsernameStandalone;
    }
    
    public final String getTestPasswordStandalone() {
        return this.testPasswordStandalone;
    }
    
    public final void setTestPasswordStandalone(final String testPasswordStandalone) {
        this.testPasswordStandalone = testPasswordStandalone;
    }
    
    public final String getTestUsernameSystemAdmin() {
        return this.testUsernameSystemAdmin;
    }
    
    public final void setTestUsernameSystemAdmin(final String testUsernameSystemAdmin) {
        this.testUsernameSystemAdmin = testUsernameSystemAdmin;
    }
    
    public final String getTestPasswordSystemAdmin() {
        return this.testPasswordSystemAdmin;
    }
    
    public final void setTestPasswordSystemAdmin(final String testPasswordSystemAdmin) {
        this.testPasswordSystemAdmin = testPasswordSystemAdmin;
    }
    
    public final String getTestPasswordDefault() {
        return this.testPasswordDefault;
    }
    
    public final void setTestPasswordDefault(final String testPasswordDefault) {
        this.testPasswordDefault = testPasswordDefault;
    }
    
    public final String getTestUsernameCollectChild() {
        return this.testUsernameCollectChild;
    }
    
    public final void setTestUsernameCollectChild(final String testUsernameCollectChild) {
        this.testUsernameCollectChild = testUsernameCollectChild;
    }
    
    public final String getTestUsernameCollectStandalone() {
        return this.testUsernameCollectStandalone;
    }
    
    public final void setTestUsernameCollectStandalone(final String testUsernameCollectStandalone) {
        this.testUsernameCollectStandalone = testUsernameCollectStandalone;
    }
    
    public final String getTestDriverDelay() {
        return this.testDriverDelay;
    }
    
    public final void setTestDriverDelay(final String testDriverDelay) {
        this.testDriverDelay = testDriverDelay;
    }
    
    public String getBrowserType() {
		return browserType;
	}

	public void setBrowserType(String browserType) {
		this.browserType = browserType;
	}
    
}
