package com.digitalpaytech.automation.performance;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.digitalpaytech.automation.AutomationGlobalsTest;

public class KPIDashboardTest extends AutomationGlobalsTest {
    private static final int SIXTYSECONDS = 60;
    private static final int ONESECOND = 1;
    private static final int NUMBEROFROWS = 300;
    private int maxRowsOfData;
    private String url = "https://perf.digitalpaytech.com/monitoring/KPI?name=&sql=100000";
    private int numberOfMinutes;
    private String[][] allData;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    
    @Before
    public final void setUp() {
        driver = new FirefoxDriver();
        this.readDataFromXml("KPIDashboard.xml");
        driver.get(this.url);
    }

    @Test
    public final void testGetKPIDashboardData() {
        // Refreshes the page every minute and grabs the data off the page
        for (int i = 0; i < this.numberOfMinutes; i++) {
            setWebElementByXpath("//html/body/pre");
            moveDataToArray(super.element.getText(), i);
            Sleeper.sleepTightInSeconds(SIXTYSECONDS);
            driver.navigate().refresh();
            Sleeper.sleepTightInSeconds(ONESECOND);
        }
        try {
            String line = "";
            final String comma = ",";
            final File file = new File("kpiData_" + System.currentTimeMillis() + ".csv");
            final PrintWriter writer = new PrintWriter(file, "UTF-8");
            for (int i = 0; i < this.maxRowsOfData; i++) {
                // Concatenates the entire row together, splitting values by a comma
                for (int j = 0; j < this.numberOfMinutes + 1; j++) {
                    line += this.allData[i][j] + comma;
                }
                line += "\n";
                writer.append(line);
                line = "";
            }
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //assertTrue(true);
    }
    
    /*
     * Reads all the data from the XML
     */
    private void readDataFromXml(final String fileName) {
        try {
            final Document document;
            final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            NodeList nodeList;
            
            document = documentBuilder.parse(this.getClass().getClassLoader().getResourceAsStream(fileName));
            document.getDocumentElement().normalize();
            
            nodeList = document.getElementsByTagName("URL");
            if (nodeList.getLength() > 0) {
                this.url = nodeList.item(0).getTextContent();
            }
            nodeList = document.getElementsByTagName("NumberOfMinutes");
            if (nodeList.getLength() > 0) {
                this.numberOfMinutes = Integer.parseInt(nodeList.item(0).getTextContent());
                // Add one extra column for the header field
                this.allData = new String[NUMBEROFROWS][this.numberOfMinutes + 1];
                // Set all data in the array to an empty string
                for (int i = 0; i < NUMBEROFROWS; i++) {
                    for (int j = 0; j < this.numberOfMinutes + 1; j++) {
                        this.allData[i][j] = "";
                    }
                }
            }
        } catch (SAXException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (ParserConfigurationException e) {
            System.out.println(e.getMessage());
        }
    }
    
    /*
     * Moves the data on the web page to the "allData" array
     */
    private void moveDataToArray(final String pageData, final int columnNumber) {
        final String[] pageDataInArray = pageData.split("\n");
        String parameter = "";
        String value = "";
        int rowNumber = 0;
        // Goes through each line on the dashboard and associates them with other similar values
        for (int i = 0; i < pageDataInArray.length; i++) {
            parameter = pageDataInArray[i].split("=")[0];
            value = pageDataInArray[i].split("=")[1];
            rowNumber = this.rowNumberOfParamerterInFirstColumn(parameter);
            switch (parameter) {
                case "!timeInterval":
                    // Split by the space character and get the end date timestamp (i.e. the 9th (index = 8) value of the array)
                    value = value.split(" ")[8];
                    break;
                case "build.date":
                    // Formats the "build.date" values
                    try {
                        value = this.simpleDateFormat.parse(value).toString();
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    break;
                default:
                    break;
            }
            // If rowNumber == -1, then the parameter doesn't exist and we need to add it to the number of rows
            if (rowNumber == -1) {
                this.allData[this.maxRowsOfData][0] = parameter;
                this.allData[this.maxRowsOfData][columnNumber + 1] = value;
                this.maxRowsOfData++;
            } else {
                this.allData[rowNumber][columnNumber + 1] = value;
            }
        }
    }
    
    /*
     * Returns the row number of the parameter in the first column
     */
    private int rowNumberOfParamerterInFirstColumn(final String parameter) {
        for (int j = 0; j < this.maxRowsOfData; j++) {
            if (parameter.equals(this.allData[j][0])) {
                return j;
            }
        }
        return -1;
    }
}
