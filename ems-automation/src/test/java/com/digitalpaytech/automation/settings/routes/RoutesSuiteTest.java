package com.digitalpaytech.automation.settings.routes;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.server.browserlaunchers.Sleeper;

public class RoutesSuiteTest extends RoutesSuite {
    // Performs all the logic for the test case setUp method
    protected final void setUpRoutesTest(final String userType) {
        super.genericSetUp("Routes.xml", userType);
    }
    
    // Performs all the logic for the test case tearDown method
    protected final void tearDownRoutesTest() {
        super.genericTearDown();
    }
    
    // Performs all the logic for the Add Route test suite
    protected final void addRouteTest(final String browserStr) {
        super.startBrowser(browserStr);
        if ("systemadmin".equalsIgnoreCase(super.userType)) {
            super.getToRoutesPageSystemAdmin(true);
        } else {
            super.getToRoutesPageCustomer(true);
        }
        super.addRoute("Test Route A", "Collections", super.payStationNames);
        
        // Checks to see if route has been created with the correct values
        // Refresh the page to make sure the changes were actually made
        driver.navigate().refresh();
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        assertTrue(super.assertRouteExistsInList(super.routeName));
        super.verifyRoute("Test Route A", "Collections", super.payStationNames);
        super.checkPSPlacementButton();
    }
    
    // Performs all the logic for the Edit Route test suite
    // TODO (Maybe): Fix up logic for adding/removing pay stations from lists
    protected final void editRouteTest(final String browserStr) {
        super.startBrowser(browserStr);
        if ("systemadmin".equalsIgnoreCase(super.userType)) {
            super.getToRoutesPageSystemAdmin(true);
        } else {
            super.getToRoutesPageCustomer(true);
        }
        // Edit route to "Maintenance"
        if (super.payStationNames.size() > 0) {
            super.payStationNames.remove(0);
            super.payStations.remove(0);
        }
        super.editRoute("Test Route A", "Test Route B", "Maintenance", super.payStationNames);
        // Checks to see if route has been edited with the correct values
        super.payStationNames.clear();
        super.payStations.clear();
        if (super.allPayStations.size() > 0) {
            super.payStationNames.add(super.allPayStationNames.get(0));
            super.payStations.add(super.allPayStations.get(0));
        }
        // Refresh the page to make sure the changes were actually made
        driver.navigate().refresh();
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        assertTrue(super.assertRouteExistsInList(super.routeName));
        super.verifyRoute("Test Route B", "Maintenance", super.payStationNames);
        super.checkPSPlacementButton();
        
        // Edit route to "Other"
        super.payStationNames.clear();
        super.payStations.clear();
        for (int i = 0; i < super.allPayStations.size(); i++) {
            super.payStationNames.add(super.allPayStationNames.get(i));
            super.payStations.add(super.allPayStations.get(i));
        }
        super.editRoute("Test Route B", "Test Route C", "Other", super.payStationNames);
        // Checks to see if route has been edited with the correct values
        super.payStationNames.remove(0);
        super.payStations.remove(0);
        // Refresh the page to make sure the changes were actually made
        driver.navigate().refresh();
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        assertTrue(super.assertRouteExistsInList(super.routeName));
        super.verifyRoute("Test Route C", "Other", super.payStationNames);
        super.checkPSPlacementButton();
    }

    // Performs all the logic for the Delete Route test suite
    protected final void deleteRouteTest(final String browserStr) {
        super.startBrowser(browserStr);
        if ("systemadmin".equalsIgnoreCase(super.userType)) {
            super.getToRoutesPageSystemAdmin(false);
        } else {
            super.getToRoutesPageCustomer(false);
        }
        super.deleteRoute("Test Route C");
        // Checks to see if route has been deleted
        // Refresh the page to make sure the changes were actually made
        driver.navigate().refresh();
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        assertFalse(super.assertRouteExistsInList(super.routeName));
    }
}
