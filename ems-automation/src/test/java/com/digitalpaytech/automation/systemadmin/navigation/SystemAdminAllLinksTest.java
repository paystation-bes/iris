package com.digitalpaytech.automation.systemadmin.navigation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.digitalpaytech.automation.AutomationGlobalsTest;

public class SystemAdminAllLinksTest extends AutomationGlobalsTest {
    
    @Before
    public void setUp() throws Exception {
        
        try {
            final DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            //final Document document = docBuilder.parse(new File(".///SeleniumMaven///Data.xml"));
            final Document document = docBuilder.parse(this.getClass().getClassLoader().getResourceAsStream("Data.xml"));
            
            //normalizing text representation
            document.getDocumentElement().normalize();
            
            // Obtaining URL
            final NodeList urlNodeList = document.getElementsByTagName("URL");
            //Node UrlNode = UrlNodeList.item(0);
            final Element url = (Element) urlNodeList.item(0);
            this.testUrl = url.getTextContent();
            
            // Obtaining Child Username
            final NodeList usernameNodeListChild = document.getElementsByTagName("ChildUsername");
            final Element usernameChild = (Element) usernameNodeListChild.item(0);
            this.testUsernameChild = usernameChild.getTextContent();
            
            // Obtaining Child Password
            final NodeList passwordNodeListChild = document.getElementsByTagName("ChildPassword");
            final Element passwordChild = (Element) passwordNodeListChild.item(0);
            this.testPasswordChild = passwordChild.getTextContent();
            
            // Obtaining Standalone Username
            final NodeList usernameNodeListStandAlone = document.getElementsByTagName("StandAloneUsername");
            final Element usernameSA = (Element) usernameNodeListStandAlone.item(0);
            this.testUsernameStandalone = usernameSA.getTextContent();
            
            // Obtaining Standalone Password
            final NodeList passwordNodeListStandAlone = document.getElementsByTagName("StandAlonePassword");
            final Element passwordSA = (Element) passwordNodeListStandAlone.item(0);
            this.testPasswordStandalone = passwordSA.getTextContent();
            
            // Obtaining Parent Username
            final NodeList usernameNodeListParent = document.getElementsByTagName("ParentUsername");
            final Element usernameParent = (Element) usernameNodeListParent.item(0);
            this.testUsernameParent = usernameParent.getTextContent();
            
            // Obtaining Parent Password
            final NodeList passwordNodeListParent = document.getElementsByTagName("ParentPassword");
            final Element passwordParent = (Element) passwordNodeListParent.item(0);
            this.testPasswordParent = passwordParent.getTextContent();
            
            /// Obtaining System Admin Username
            final NodeList usernameNodeListSystemAdmin = document.getElementsByTagName("SystemAdminUsrename");
            final Element usernameSystemAdmin = (Element) usernameNodeListSystemAdmin.item(0);
            this.testUsernameSystemAdmin = usernameSystemAdmin.getTextContent();
            
            // Obtaining System Admin Password
            final NodeList passwordNodeListSystemAdmin = document.getElementsByTagName("SystemAdminPassword");
            final Element passwordSystemAdmin = (Element) passwordNodeListSystemAdmin.item(0);
            this.testPasswordSystemAdmin = passwordSystemAdmin.getTextContent();
            
            // Obtaining default password = "password"
            final NodeList defaultPasswordNodeListSystemAdmin = document.getElementsByTagName("DefaultPassword");
            final Element defaultPasswordSystemAdmin = (Element) defaultPasswordNodeListSystemAdmin.item(0);
            this.testPasswordDefault = defaultPasswordSystemAdmin.getTextContent();
            
            // Obtaining Delay
            final NodeList delayList = document.getElementsByTagName("DriverDelayMilliSecond");
            final Element webDriverDelay = (Element) delayList.item(0);
            this.testDriverDelay = webDriverDelay.getTextContent();
            
            // Obtaining Browser Type
            final NodeList browserList = document.getElementsByTagName("BrowserType");
            final Element browserType = (Element) browserList.item(0);
            super.browserType = browserType.getTextContent();
            
            // launching specific browser type
            launchBrowser();
        }
        
        catch (SAXParseException err) {
            //            System.out.println("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId());
            //            System.out.println(" " + err.getMessage());
            
        } catch (SAXException e) {
            final Exception x = e.getException();
            ((x == null) ? e : x).printStackTrace();
            
        } catch (Throwable t) {
            t.printStackTrace();
        }
        
    }
    
    @Test
    public void allLinksCustomerAdmin() {

        // Go to Iris
        this.driver.get(this.testUrl);
        super.driver.manage().window().maximize();
        driverWait();
        
        // Log into test account
        login(this.testUsernameSystemAdmin, this.testPasswordSystemAdmin);
        
        // Verifying page title
        // The test stops as it throws an exception if it fails
        assertEquals("Digital Iris - System Administration", this.driver.getTitle());
        driverWait();
        //////////////////////////////////////////////////////////////////////
        
        // go to a customer:  qa1child
        this.element = this.driver.findElement(By.xpath("//input[@id='search']"));
        this.element.click();
        this.element.sendKeys("qa1c");
        //        this.element.sendKeys(Keys.ENTER);
        //        this.element = this.driver.findElement(By.xpath("//section[@id='mainContent']"));
        //        this.element.click();
        driverWait();
        this.element = this.driver.findElement(By.xpath("//a[@id='ui-id-2']"));
        this.element.click();
        driverWait();
        
        this.webDriverWait.until(ExpectedConditions.elementToBeClickable(By.id("btnGo")));
        this.element = this.driver.findElement(By.id("btnGo"));
        this.element.click();
        driverWait();
        
        // Verifying page title
        assertEquals("Digital Iris - System Administration - qa1child : Customer Details", this.driver.getTitle());
        driverWait();
        
        // click on "Customer Details"
        this.element = this.driver.findElement(By.xpath("//a[@title='Customer Details']"));
        this.element.click();
        driverWait();
        
        // verification of an object
        this.element = this.driver.findElement(By.xpath("//a[@id='btnEditCustomer']"));
        assertEquals(true, this.element.isDisplayed());
        
        // go to Reports Page
        //this.element = this.driver.findElement(By.xpath("//a[@class='tab']" && "//a[@title='Reports']"));
        this.element = this.driver.findElement(By.xpath("//a[contains(@class, 'tab') and contains(@title, 'Reports')]"));
        this.element.click();
        // Verifying page title
        //assertEquals("Digital Iris - System Administration - qa1child : Reports", this.driver.getTitle());
        driverWait();
        
        // verification of an object
        this.element = this.driver.findElement(By.xpath("//a[@id='btnAddScheduleReportWS']"));
        assertEquals(true, this.element.isDisplayed());
        
        // go to Licenses Page
        //this.element = this.driver.findElement(By.xpath("//a[@class='tab']" && "//a[@title='Reports']"));
        this.element = this.driver.findElement(By.xpath("//a[contains(@class, 'tab') and contains(@title, 'Licenses')]"));
        this.element.click();
        // Verifying page title
        assertEquals("Digital Iris - System Administration - qa1child : Licenses : Digital API", this.driver.getTitle());
        driverWait();
        
        // verification of an object
        //verifying text exists
        this.element = this.driver.findElement(By.xpath("//h2[contains(., 'Digital API - Read')]"));
        assertEquals(true, this.element.isDisplayed());
        driverWait();
        
        // go to Mobile Licenses Page
        //this.element = this.driver.findElement(By.xpath("//a[@class='tab']" && "//a[@title='Reports']"));
        this.element = this.driver.findElement(By.xpath("//a[contains(@class, 'tab') and contains(@title, 'Mobile Licenses')]"));
        this.element.click();
        driverWait();
        // Verifying page title
        assertEquals("Digital Iris - System Administration - qa1child : Licenses : Mobile Licenses", this.driver.getTitle());
        driverWait();
        
        // verification of an object
        this.element = this.driver.findElement(By.xpath("//a[@id='btnEditLicenseCount']"));
        assertEquals(true, this.element.isDisplayed());
        driverWait();
        
        // go to Pay Stations Page
        this.element = this.driver.findElement(By.xpath("//a[@title='Pay Stations']"));
        this.element.click();
        // Verifying page title
        assertEquals("Digital Iris - System Administration - qa1child : Pay Station Details", this.driver.getTitle());
        driverWait();
        
        // click on "Pay Station Details"
        this.element = this.driver.findElement(By.xpath("//a[@title='Pay Station Details']"));
        this.element.click();
        driverWait();
        
        // go to Pay Station Placement
        this.element = this.driver.findElement(By.xpath("//a[@title='Pay Station Placement']"));
        this.element.click();
        driverWait();
        // Verifying page title
        assertEquals("Digital Iris - System Administration - qa1child : Pay Station Placement", this.driver.getTitle());
        driverWait();
        
        // verification of an object
        this.element = this.driver.findElement(By.xpath("//h2[contains(., 'Pay Stations')]"));
        assertEquals(true, this.element.isDisplayed());
        driverWait();
        
        // go to Locations
        this.element = this.driver.findElement(By.xpath("//a[@title='Locations']"));
        this.element.click();
        // Verifying page title
        assertEquals("Digital Iris - System Administration - qa1child : Locations", this.driver.getTitle());
        driverWait();
        
        // verification of an object
        this.element = this.driver.findElement(By.xpath("//h2[contains(., 'Locations')]"));
        assertEquals(true, this.element.isDisplayed());
        driverWait();
        
        // go to Routes
        this.element = this.driver.findElement(By.xpath("//a[@title='Routes']"));
        this.element.click();
        // Verifying page title
        assertEquals("Digital Iris - System Administration - qa1child : Routes", this.driver.getTitle());
        driverWait();
        
        // verification of an object
        this.element = this.driver.findElement(By.xpath("//h2[contains(., 'Routes')]"));
        assertEquals(true, this.element.isDisplayed());
        driverWait();
        
        // go to Alerts
        this.element = this.driver.findElement(By.xpath("//a[@title='Alerts']"));
        this.element.click();
        // Verifying page title
        assertEquals("Digital Iris - System Administration - qa1child : Alerts", this.driver.getTitle());
        driverWait();
        
        // verification of an object
        this.element = this.driver.findElement(By.xpath("//h2[contains(., 'Alerts')]"));
        assertEquals(true, this.element.isDisplayed());
        driverWait();
        ///////////////////////////////////////////////////////////////////////////////
        
        // go to Reports
        this.element = this.driver.findElement(By.xpath("//a[@title='Reports']"));
        this.element.click();
        // Verifying page title
        assertEquals("Digital Iris - System Administration - Reports", this.driver.getTitle());
        driverWait();
        
        // click on "Create Report" button
        this.element = this.driver.findElement(By.xpath("//a[@id='btnAddScheduleReport']"));
        this.element.click();
        // Verifying page title
        assertEquals("Digital Iris - System Administration - Reports", this.driver.getTitle());
        driverWait();
        
        // verification of an object
        this.element = this.driver.findElement(By.xpath("//a[@id='btnStep2Select']"));
        driverWait();
        assertEquals(true, this.element.isDisplayed());
        driverWait();
        ///////////////////////////////////////////////////////////////////////////////
        
        // go to User Administration
        this.element = this.driver.findElement(By.xpath("//a[@title='User Administration']"));
        this.element.click();
        // Verifying page title
        assertEquals("Digital Iris - System Administration - User Administration : Admin User Accounts", this.driver.getTitle());
        driverWait();
        
        // click on "Admin User Accounts"
        this.element = this.driver.findElement(By.xpath("//a[@title='Admin User Accounts']"));
        this.element.click();
        driverWait();
        
        // go to Admin User Roles
        this.element = this.driver.findElement(By.xpath("//a[@title='Admin User Roles']"));
        this.element.click();
        // Verifying page title
        assertEquals("Digital Iris - System Administration - User Administration : Admin User Roles", this.driver.getTitle());
        driverWait();
        
        // verification of an object
        this.element = this.driver.findElement(By.xpath("//a[@id='btnAddRole']"));
        assertEquals(true, this.element.isDisplayed());
        driverWait();
        ///////////////////////////////////////////////////////////////////////////////
        
        // go to Notifications
        this.element = this.driver.findElement(By.xpath("//a[@title='Notifications']"));
        this.element.click();
        // Verifying page title
        assertEquals("Digital Iris - System Administration - Notifications", this.driver.getTitle());
        driverWait();
        
        // verification of an object
        this.element = this.driver.findElement(By.xpath("//a[@id='btnAddNotification']"));
        assertEquals(true, this.element.isDisplayed());
        driverWait();
        ///////////////////////////////////////////////////////////////////////////////
        
        // go to System Activity
        this.element = this.driver.findElement(By.xpath("//a[@title='System Activity']"));
        this.element.click();
        // Verifying page title
        assertEquals("Digital Iris - System Administration - System Activity : System Monitor", this.driver.getTitle());
        driverWait();
        
        // click on "System Monitor"
        this.element = this.driver.findElement(By.xpath("//a[@title='System Monitor']"));
        this.element.click();
        driverWait();
        
        // go to Server Admin
        this.element = this.driver.findElement(By.xpath("//a[@title='Server Admin']"));
        this.element.click();
        driverWait();
        // Verifying page title
        assertEquals("Digital Iris - System Administration - System Activity : Server Admin", this.driver.getTitle());
        driverWait();
        
        // verification of an object
        this.element = this.driver.findElement(By.xpath("//a[@id='btnShutDownTomcat']"));
        assertEquals(true, this.element.isDisplayed());
        driverWait();
        /////////////////////////////////////////////////////////////////////////////
        
        //logout
        this.element = this.driver.findElement(By.xpath("//a[@title='Logout']"));
        this.element.click();
        assertEquals("Digital Iris - Login.", this.driver.getTitle());
        //assertTrue(true);
    }
    
    @After
    public void tearDown() throws Exception {
        
        this.driver.quit();
        //driver.close();
    }
    
}