package com.digitalpaytech.automation.systemadmin.settings.routes;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.digitalpaytech.automation.Order;
import com.digitalpaytech.automation.OrderedRunner;
import com.digitalpaytech.automation.settings.routes.RoutesSuiteTest;

@RunWith(OrderedRunner.class)
public class RoutesSuiteSystemAdminUserTest extends RoutesSuiteTest {
    @Before
    public final void setUp() {
        super.setUpRoutesTest("systemadmin");
    }
    
    @After
    public final void tearDown() {
        super.tearDownRoutesTest();
    }

    /*@Test
    @Order(order = 1)
    public final void testSystemAdminAddRouteChrome() {
        super.addRouteTest("chrome");
    }
    
    @Test
    @Order(order = 2)
    public final void testSystemAdminEditRouteChrome() {
        super.editRouteTest("chrome");
    }
    
    @Test
    @Order(order = 3)
    public final void testSystemAdminDeleteRouteChrome() {
        super.deleteRouteTest("chrome");
    }*/
    
    @Test
    @Order(order = 4)
    public final void testSystemAdminAddRouteFirefox() {
        super.addRouteTest("firefox");
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 5)
    public final void testSystemAdminEditRouteFirefox() {
        super.editRouteTest("firefox");
        //assertTrue(true);
    }
    
    @Test
    @Order(order = 6)
    public final void testSystemAdminDeleteRouteFirefox() {
        super.deleteRouteTest("firefox");
        //assertTrue(true);
    }
    
    /*@Test
    @Order(order = 7)
    public final void testSystemAdminAddRouteInternetExplorer() {
        super.addRouteTest("ie");
    }
    
    @Test
    @Order(order = 8)
    public final void testSystemAdminEditRouteInternetExplorer() {
        super.editRouteTest("ie");
    }
    
    @Test
    @Order(order = 9)
    public final void testSystemAdminDeleteRouteInternetExplorer() {
        super.deleteRouteTest("ie");
    }*/
}
