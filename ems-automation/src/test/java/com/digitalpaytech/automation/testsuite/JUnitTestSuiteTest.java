package com.digitalpaytech.automation.testsuite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.digitalpaytech.automation.customer.collections.recentcollections.RecentCollectionsSuiteChildUserTest;
import com.digitalpaytech.automation.customer.collections.recentcollections.RecentCollectionsSuiteStandaloneUserTest;
import com.digitalpaytech.automation.customer.navigation.CustomerAdminAllLinksTest;
import com.digitalpaytech.automation.customer.settings.locations.LocationsTest;
import com.digitalpaytech.automation.customer.settings.paystations.PaystationDetailsTest;
import com.digitalpaytech.automation.customer.settings.rates.IncrementalRatesTest;
import com.digitalpaytech.automation.customer.settings.routes.RoutesSuiteChildUserTest;
import com.digitalpaytech.automation.customer.settings.routes.RoutesSuiteStandaloneUserTest;
import com.digitalpaytech.automation.sessioncontrol.ChangeDefaultPasswordTest;
import com.digitalpaytech.automation.sessioncontrol.NewUserLoginTest;
import com.digitalpaytech.automation.sessioncontrol.UserLoginLogoutTest;
import com.digitalpaytech.automation.systemadmin.navigation.SystemAdminAllLinksTest;
import com.digitalpaytech.automation.systemadmin.settings.locations.SystemAdminLocationsTest;
import com.digitalpaytech.automation.systemadmin.settings.paystations.SystemAdminPaystationDetailsTest;
import com.digitalpaytech.automation.systemadmin.settings.routes.RoutesSuiteSystemAdminUserTest;
import com.digitalpaytech.automation.transactions.CoinBillSummaryTest;
import com.digitalpaytech.automation.transactions.CoinBillVerify;
import com.digitalpaytech.automation.transactions.TransactionTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({ ChangeDefaultPasswordTest.class, NewUserLoginTest.class, CustomerAdminAllLinksTest.class, UserLoginLogoutTest.class,
    SystemAdminAllLinksTest.class, RoutesSuiteChildUserTest.class, RoutesSuiteStandaloneUserTest.class, RoutesSuiteSystemAdminUserTest.class,
    PaystationDetailsTest.class, SystemAdminPaystationDetailsTest.class, IncrementalRatesTest.class, CoinBillSummaryTest.class, LocationsTest.class,
    SystemAdminLocationsTest.class, RecentCollectionsSuiteChildUserTest.class, RecentCollectionsSuiteStandaloneUserTest.class, CoinBillVerify.class,
    TransactionTest.class })
public class JUnitTestSuiteTest {
    
    public JUnitTestSuiteTest() {
        // TODO Auto-generated constructor stub
    }
    
}
