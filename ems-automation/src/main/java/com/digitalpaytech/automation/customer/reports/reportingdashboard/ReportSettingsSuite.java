package com.digitalpaytech.automation.customer.reports.reportingdashboard;

import org.apache.log4j.Logger;

import com.digitalpaytech.automation.AutomationGlobals;

public class ReportSettingsSuite extends AutomationGlobals {
    protected static final Logger LOGGER = Logger.getLogger(ReportSettingsSuite.class);
    
    protected final void createCollectionReportsReport(final String reportType, final String type, final Boolean includeArchivedPayStations,
                                                       final String account, final String reportTypeOtherParameters, final String groupBy,
                                                       final String outputFormat) {
        this.clickCreateReportButton();
        this.selectReportType(reportType);
        this.selectType(type);
        this.selectToIncludeArchivedPayStations(includeArchivedPayStations);
        this.selectAccount(account);
        this.selectOtherParameter(reportTypeOtherParameters);
        this.selectGroupByOption(groupBy);
        this.selectOutputFormat(outputFormat);
        this.clickSecondNextStepButton();
        this.clickSaveButton();
    }
    
    /*
     * Clicks the "Create Report" button
     */
    private void clickCreateReportButton() {
        super.setWebElementById("btnAddScheduleReport");
        super.element.click();
    }
    
    /*
     * Selects the specified Route Type
     */
    private void selectReportType(final String reportType) {
        super.selectOptionFromDropDown("reportTypeID", reportType);
        super.setWebElementById("btnStep2Select");
        super.element.click();
    }
    
    /*
     * Selects between a Summary and a Details report
     */
    private void selectType(final String type) {
        switch (type.toLowerCase()) {
            case "summary":
                super.setWebElementById("switchIsSummary");
                break;
            case "details":
                super.setWebElementById("switchIsNotSummary");
                break;
            default:
                break;
        }
        super.element.click();
    }
    
    /*
     * Selects whether to include archived pay stations in the report
     */
    private void selectToIncludeArchivedPayStations(final Boolean includeArchivedPayStations) {
        if (includeArchivedPayStations) {
            super.setWebElementById("switchShowHiddenPayStations");
        } else {
            super.setWebElementById("switchNotShowHiddenPayStations");
        }
        super.element.click();
    }
    
    /*
     * Selects specified account from the "Account" drop-down
     * TODO: Add the logic for the drop-down selection part
     */
    private void selectAccount(final String account) {
        super.setWebElementById("accountFilterValue");
        super.element.clear();
        super.element.sendKeys(account);
    }
    
    /*
     * Selects the specified other parameter value
     */
    private void selectOtherParameter(final String otherParameterValue) {
        super.selectOptionFromDropDown("formOtherParameters", otherParameterValue);
    }
    
    /*
     * Selects the specified group by option
     */
    private void selectGroupByOption(final String groupBy) {
        super.selectOptionFromDropDown("formGroupBy", groupBy);
    }
    
    /*
     * Selects the specified output format
     */
    private void selectOutputFormat(final String outputFormat) {
        switch (outputFormat.toLowerCase()) {
            case "pdf":
                this.setWebElementById("switchIsPDF");
                break;
            case "csv":
                this.setWebElementById("switchIsNotPDF");
                break;
            default:
                break;
        }
        super.element.click();
    }
    
    /*
     * Clicks the "Next Step" button to go into "Step 3: Report Schedule"
     */
    private void clickSecondNextStepButton() {
        super.setWebElementById("btnStep3Select");
        super.element.click();
    }
    
    /*
     * Clicks the "Save" button at the end of the Report creation page
     */
    private void clickSaveButton() {
        super.setWebElementByXpath("//section[@id='reportButtonsSection']/a[text()='Save']");
        super.element.click();
    }
}
