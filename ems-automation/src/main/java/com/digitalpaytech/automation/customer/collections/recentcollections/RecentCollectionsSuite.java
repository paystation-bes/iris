package com.digitalpaytech.automation.customer.collections.recentcollections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.digitalpaytech.automation.AutomationGlobals;
import com.digitalpaytech.automation.dto.AutomationUser;

public class RecentCollectionsSuite extends AutomationGlobals {
    protected String payStationName = "";
    protected String payStationSerial = "";
    
    /*
     * Gets the First and Last Name of the user who did the collection.
     * Currently, the Username it returns is the first Collections User in the XML with the same
     * customer name as the user that is currently logged in.
     */
    protected final String getCollectionUsername() {
        AutomationUser user = new AutomationUser();
        String collectionUsername = "";
        for (int i = 0; i < xmlData.getCollectionUserList().size(); i++) {
            user = xmlData.getCollectionUserList().get(i);
            if (super.customerName.equals(user.getCustomerName())) {
                collectionUsername = user.getFirstName() + " " + user.getLastName();
                LOGGER.info("Collection username is: " + collectionUsername);
            }
        }
        return collectionUsername;
    }
    
    /*
     * Selects a collection from the list of Recent Collections
     */
    protected final void selectCollectionFromRecentCollectionList(final String collectionTypeStr, final Boolean checkForCollectionUsername) {
        final List<WebElement> collectionElements = new ArrayList<WebElement>();
        WebElement recentCollection;
        
        // Make sure all elements are loaded in the list
        loadAllElementsInList("//ul[@id='recentList']");
        
        setWebElementsByXpath("//ul[@id='recentList']/span[@class='listItems']/li");
        collectionElements.addAll(driver.findElements(By.xpath("//ul[@id='recentList']/span[@class='listItems']/li")));
        //collectionElements.addAll(super.elementList);
        
        for (int i = 0; i < collectionElements.size(); i++) {
            recentCollection = collectionElements.get(i).findElement(By.xpath("section/span[@class='collectionTypeMsg']"));
            
            // Checks if the collection type matches that of the specified type
            if (recentCollection.getText().contains(collectionTypeStr)) {
                // Get the pay station serial number
                recentCollection = collectionElements.get(i);
                this.payStationSerial = recentCollection.getAttribute("serial");
                this.payStationName = collectionElements.get(i).findElement(By.xpath("section/h3")).getText();
                LOGGER.info("Collection retrieved from pay station: " + this.payStationSerial + " (" + this.payStationName + ").");
                
                recentCollection.click();
                LOGGER.info("Collection clicked.");
                Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
                Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
                
                // Click collections until you find a collection with a Collection Username.
                if (checkForCollectionUsername) {
                    WebElement collectedByField;
                    collectedByField = driver.findElement(By.id("collectedBy"));
                    if (collectedByField.getText().isEmpty()) {
                        LOGGER.info("Collection did not have a Collection Username field.  Moving to next Collection.");
                        continue;
                    } else {
                        LOGGER.info("Collection found.");
                        return;
                    }
                } else {
                    LOGGER.info("Collection found.");
                    return;
                }
            } else {
                continue;
            }
        }
        String noCollectionWithUsernameStr = "";
        if (checkForCollectionUsername) {
            noCollectionWithUsernameStr = " with a Collection Username field";
        }
        LOGGER.info("Could not find a \"" + collectionTypeStr + "\" Collection" + noCollectionWithUsernameStr + ".");
    }
    
    /*
     * Clicks the first collection report that matches the specified collection type
     */
    protected final void selectCollectionFromPayStationDetailsList(final String collectionTypeStr, final Boolean checkForCollectionUsername) {
        final List<WebElement> collectionReports = new ArrayList<WebElement>();
        String actualCollectionType = "";
        
        // Make sure all elements are loaded in the list
        loadAllElementsInList("//ul[@id='collectionReportList']");
        setWebElementsByXpath("//ul[@id='collectionReportList']/span/li");
        collectionReports.addAll(super.elementList);
        
        // Looks for a collection report with the specified type
        for (int i = 0; i < super.elementList.size(); i++) {
            actualCollectionType = super.elementList.get(i).findElement(By.xpath("div[@class='col2']/p")).getText();
            if (actualCollectionType.equalsIgnoreCase(collectionTypeStr)) {
                super.elementList.get(i).click();
                LOGGER.info("Collection Report clicked.");
                Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
                
                // Checks if selected Collection Report contains a Collection Username field
                if (checkForCollectionUsername) {
                    Boolean isCollectionUsernameVisible = false;
                    List<WebElement> collectionDetails = new ArrayList<WebElement>();
                    collectionDetails = driver.findElements(By
                            .xpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl[@class='fixedWidthFont']/dt"));
                    for (int j = 0; j < collectionDetails.size(); j++) {
                        // Check for the "Collected By" field
                        if ("Collected By".equals(collectionDetails.get(j).getText())) {
                            isCollectionUsernameVisible = true;
                            break;
                        }
                    }
                    if (isCollectionUsernameVisible) {
                        LOGGER.info("\"" + collectionTypeStr + "\" Collection Report with Collection Username field found.");
                        return;
                    } else {
                        LOGGER.info("Selected Collection Report did not have a Collection Username field. Closing pop-up and trying next report.");
                        this.closeColletionPopUp();
                        continue;
                    }
                }
                LOGGER.info("\"" + collectionTypeStr + "\" Collection Report found.");
                return;
            } else {
                continue;
            }
        }
        LOGGER.info("Unable to find collection report with type \"" + collectionTypeStr + "\" in the list.");
    }
    
    /*
     * Selects a pay station with recent collections from the visible portion of the map.
     * Assumes that there is a pay station with recent collections visible.
     * Will not work without first running the "selectRecentCollectionFromList" method.
     * TODO: Add method to check if pay station is visible (may be clustered)
     */
    protected final void selectRecentCollectionFromMap() {
        setWebElementByXpath("//div/div/div/img[@title='" + this.payStationName + "']");
        super.element.click();
        LOGGER.info("Pay Station in map clicked.");
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
    }
    
    /*
     * Clicks and verifies a Recent Collection from the "Recent Collections" list.
     */
    protected final void clickAndVerifyCollectionFromRecentCollectionsList(final String collectionTypeStr, final String collectionUsername) {
        LOGGER.info("Start Recent Collections Test: " + "Collection Type (\"" + collectionTypeStr + "\") + Collection Username (\"" + collectionUsername
                    + "\").");
        this.selectCollectionFromRecentCollectionList(collectionTypeStr, isCollectionUsernameParameterNotEmpty(collectionUsername));
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        this.verifyCollectionSection(collectionUsername);
        this.clickDetailsButton();
        this.verifyCollectionDetailsPopUp(collectionUsername);
        this.closeColletionPopUp();
        LOGGER.info("End Recent Collections Test.");
    }
    
    /*
     * Clicks and verifies a Recent Collection from the "Pay Station Details" -> "Reports" -> "Collection Reports" list.
     */
    protected final void clickAndVerifyCollectionsFromPayStationDetailsList(final String collectionTypeStr, final String collectionUsername) {
        LOGGER.info("Start Recent Collections Test: " + "Collection Type (\"" + collectionTypeStr + "\") + Collection Username (\"" + collectionUsername
                    + "\").");
        this.selectCollectionFromPayStationDetailsList("All", isCollectionUsernameParameterNotEmpty(collectionUsername));
        this.verifyCollectionDetailsPopUp(collectionUsername);
        this.closeColletionPopUp();
        LOGGER.info("End Recent Collections Test.");
    }
    
    /*
     * Clicks the "Details" button that appears when clicking an item in the "Recent Collections" list
     */
    protected final void clickDetailsButton() {
        // Checks for Details button
        setWebElementById("collDetailBtn");
        super.element.click();
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
    }
    
    /*
     * Closes the collection pop-up that appears when the "Details" button is pressed
     */
    protected final void closeColletionPopUp() {
        setWebElementByXpath("//div/div/button/span[text()='Close']");
        super.element.click();
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
    }
    
    /*
     * Checks if the "collectionUsername" parameter is empty. If it is empty, the method returns "false".
     */
    private Boolean isCollectionUsernameParameterNotEmpty(final String collectionUsername) {
        Boolean checkForCollectionUsername = true;
        if (collectionUsername.isEmpty()) {
            checkForCollectionUsername = false;
        }
        return checkForCollectionUsername;
    }
    
    /*
     * TODO: Add assertions based on values within report and report type
     * May also want to verify if the pop-up can move
     */
    protected final void verifyCollectionDetailsPopUp(final String collectionUsername) {
        setWebElementsByXpath("//section[@id='collectionDetailView']/section[@class='reportBody']/dl[@class='fixedWidthFont']/*");
        for (int i = 0; i < super.elementList.size(); i++) {
            // Check for the "Collected By" field
            if ("dt".equals(super.elementList.get(i).getTagName())) {
                switch (super.elementList.get(i).getText()) {
                    case "Collected By":
                        assertEquals(collectionUsername, super.elementList.get(i + 1).getText());
                        break;
                    case "Serial #":
                        assertEquals(this.payStationSerial, super.elementList.get(i + 1).getText());
                        break;
                    default:
                        break;
                }
            }
        }
    }
    
    /*
     * Verifies the section that appears when a "Collection Report" is clicked.
     * TODO: Add more assertions to check the values that appear in the details section
     * These assertions should also reflect the type of report that was clicked
     */
    protected final void verifyCollectionSection(final String collectionUsername) {
        // Checks if pay station serial number matches the selected item in the list
        setWebElementById("selectedPayStation");
        assertTrue(super.element.isDisplayed());
        assertEquals(this.payStationSerial, super.element.getAttribute("serial"));
        
        // Checks if pay station name matches the selected item in the list
        setWebElementByXpath("//section[@id='selectedPOSdetails']/h2");
        assertEquals(this.payStationName, super.element.getText());
        
        // Checks if the collectionUsername parameter actually matches the value shown in the details section
        setWebElementById("collectedBy");
        assertEquals(collectionUsername, super.element.getText().replace("Collected By: ", ""));
        
        // Checks for Details button
        setWebElementById("collDetailBtn");
    }
    
    /*
     * Verifies the pop-up that appears on the map showing the recent collections.
     */
    protected final void verifyCollectionMapPopUp(final String collectionUsername) {
        setWebElementByXpath("//div[@class='leaflet-popup-content']/section[@class='mapPopUp']/h1");
        assertEquals(this.payStationName, super.element.getText());
        setWebElementByXpath("//div[@class='leaflet-popup-content']/section[@class='mapPopUp']/d1[@class='psDetails']");
        
        String currentDetailLabel = "";
        String currentDetailValue = "";
        List<WebElement> detailLabelList = new ArrayList<WebElement>();
        List<WebElement> detailValueList = new ArrayList<WebElement>();
        // Initialize two lists (one for the labels, and a corresponding list for the values)
        setWebElementsByXpath("//dl[@class='psDetails']/dt[contains(@class,'detailLabel')]");
        detailLabelList = super.elementList;
        setWebElementsByXpath("//dl[@class='psDetails']/dd[contains(@class,'detailValue')]");
        detailValueList = super.elementList;
        
        // Loops through all the data in the pop-up and verifies the various pay station data
        for (int i = 0; i < detailLabelList.size(); i++) {
            currentDetailLabel = detailLabelList.get(i).getText().toLowerCase();
            currentDetailValue = detailValueList.get(i).getText();
            
            if (currentDetailLabel.contains("last seen")) {
                // Verify Last Seen value
                LOGGER.info("Pay station Last Seen value is: " + currentDetailValue);
            } else if (currentDetailLabel.contains("route")) {
                // Verify Pay Station Collection Route(s)
                final String[] routes = currentDetailValue.split(",");
                for (int j = 0; j < routes.length; j++) {
                    LOGGER.info("Pay station is a part of route: " + routes[j]);
                }
            } else if (currentDetailLabel.contains("collections today")) {
                // Verify Collection(s) made today
                List<WebElement> collectionTypes = new ArrayList<WebElement>();
                List<WebElement> collectionValues = new ArrayList<WebElement>();
                List<WebElement> collectionUsernames = new ArrayList<WebElement>();
                // Collection Types
                collectionTypes = detailValueList.get(i).findElements(By.xpath("a[@class='collDetailLink']"));
                // Collection Values
                collectionValues = detailValueList.get(i).findElements(By.xpath("strong[@class='right']"));
                // Collection Usernames
                collectionUsernames = detailValueList.get(i).findElements(By.xpath("strong[@class='note collectionUser']"));
                
                for (int j = 0; j < collectionTypes.size(); j++) {
                    assertEquals(collectionUsernames.get(i).getText(), collectionUsername);
                }
            } else {
                // An unaccounted value appeared in the pop-up details.
                // Need to add logic to account for any detail that is not handled above.
                LOGGER.info("Unknown/unhandled detail in popup: " + currentDetailLabel + " " + currentDetailValue);
            }
        }
    }
}
