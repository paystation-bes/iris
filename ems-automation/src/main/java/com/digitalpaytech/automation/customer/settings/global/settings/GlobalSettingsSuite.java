package com.digitalpaytech.automation.customer.settings.global.settings;

import com.digitalpaytech.automation.AutomationGlobals;

public class GlobalSettingsSuite extends AutomationGlobals {
    public final void setTimeZone() {
        super.setWebElementById("curTimeZone");
        super.timeZone = super.element.getText();
        LOGGER.info("Customer time zone is set to: " + super.timeZone);
    }
}
