package com.digitalpaytech.automation.customer.navigation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.server.browserlaunchers.Sleeper;

import com.digitalpaytech.automation.AutomationGlobals;

public class CustomerNavigation extends AutomationGlobals {
    // Navigates to the specified page on the left hand menu
    
    public CustomerNavigation(WebDriver driver) {
        this.driver = driver;
    }
    
    public CustomerNavigation() {
    }
    
    public final void navigateLeftHandMenu(final String menuOption) {
        final String menuOptionXPath = "//section/a[@title='" + menuOption + "']";
        this.element = driver.findElement(By.xpath(menuOptionXPath));
        super.element.click();
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
    }
    
    // Navigates to the specified tab at the top of the page
    public final void navigatePageTabs(final String tabOption) {
        final String tabOptionXPath = "//div/a[@class='tab' and @title='" + tabOption + "']";
        
        super.setWebElementByXpath(tabOptionXPath);
        super.element.click();
        //driver.findElement(By.linkText(tabOption)).click();
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
    }
    
    // Navigates to the specified sub-tab at the top of the page
    public final void navigatePageSubTabs(final String tabOption) {
        final String tabOptionXPath = "//nav/a[contains(@class,'tab') and @title='" + tabOption + "']";
        super.setWebElementByXpath(tabOptionXPath);
        super.element.click();
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
    }
}
