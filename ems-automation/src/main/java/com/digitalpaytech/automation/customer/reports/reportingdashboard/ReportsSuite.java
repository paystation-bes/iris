package com.digitalpaytech.automation.customer.reports.reportingdashboard;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.digitalpaytech.automation.AutomationGlobals;

public class ReportsSuite extends AutomationGlobals {
    private int numberOfCompletedReports;
    private int numberOfIncompleteReports;
    private List<WebElement> elementList;
    
    //    protected WebDriver driver;
    
    public ReportsSuite() {
        this.setNumberOfCompletedReports();
        this.setNumberOfIncompleteReports();
    }
    
    /*
     * This method checks if any reports in the "Pending and Incomplete Reports"
     * section have not finished (i.e. status of "Submitted" or "Running")
     */
    public final Boolean areTherePendingReports() {
        Boolean pendingReports = false;
        String reportStatus = "";
        super.setWebElementById("queuedReportsList");
        this.elementList = super.element.findElements(By.xpath("/li/div[@class='col2']/p"));
        for (int i = 0; i < this.elementList.size(); i++) {
            reportStatus = this.elementList.get(i).getText().toLowerCase();
            if (reportStatus.contains("running") || reportStatus.contains("submitted")) {
                pendingReports = true;
            }
        }
        return pendingReports;
    }
    
    public final void viewReport(final String reportType) {
        
    }
    
    /*
     * TODO: This one could get ugly
     */
    public final void editAndRerunReport(final String reportType) {
        
    }
    
    public final void deleteReport(final String reportType) {
        
    }
    
    /*
     * This method reloads the Reports tables
     */
    public final void reloadReportsTables() {
        super.setWebElementById("opBtn_reloadReport");
        this.setNumberOfCompletedReports();
        this.setNumberOfIncompleteReports();
    }
    
    /*
     * Gets the number of reports in the "Pending and Incomplete Reports" section
     */
    private void setNumberOfIncompleteReports() {
        super.setWebElementById("queuedReportsList");
        this.elementList = super.element.findElements(By.xpath("/li"));
        this.numberOfIncompleteReports = this.elementList.size();
    }
    
    /*
     * Gets the number of reports in the "Completed Reports" section
     */
    private void setNumberOfCompletedReports() {
        super.setWebElementById("reportsList");
        this.elementList = super.element.findElements(By.xpath("/li"));
        this.numberOfCompletedReports = this.elementList.size();
    }
}
