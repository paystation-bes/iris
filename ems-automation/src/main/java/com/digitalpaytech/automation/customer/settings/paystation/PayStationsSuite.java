package com.digitalpaytech.automation.customer.settings.paystation;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.digitalpaytech.automation.AutomationGlobals;
import com.digitalpaytech.automation.dto.AutomationPayStation;

public class PayStationsSuite extends AutomationGlobals {
    private static final Logger LOGGER = Logger.getLogger(PayStationsSuite.class);
    public List<WebElement> payStationElements = new ArrayList<WebElement>();
    
    public PayStationsSuite() {
        
    }
    
    public PayStationsSuite(WebDriver driver)
    {
        this.driver = driver;
    }
    
    /*
     * Sets the "payStationElements" variable to a list of all the customer's pay stations
     */
    private void setPayStationElements() {
        //super.setWebElementsByXpath("//ul[@id='paystationList']/span/li");
        //this.payStationElements.addAll(super.elementList);
        this.payStationElements.addAll(driver.findElements(By.xpath("//ul[@id='paystationList']/span/li")));
    }
    
    /*
     * Clicks the specified pay station from the pay station list
     */
    public final void clickPayStationFromList(final String payStationName) {
        WebElement payStationElement;
        
        //this.setPayStationElements();
        //this.payStationElements.addAll(driver.findElements(By.xpath("//ul[@id='paystationList']/span/li")));
        for (int i = 0; i < this.payStationElements.size(); i++) {
            payStationElement = this.payStationElements.get(i);
            if (payStationName.equalsIgnoreCase(payStationElement.getText())) {
                payStationElement.click();
                Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            }
        }
    }
    
    /*
     * Clicks the specified tab in the Pay Station details section
     */
    public final void navigatePayStationDetailsTabs(final String detailsTab) {
        super.setWebElementByXpath("//nav[@id='psNav']/div/a[@title='" + detailsTab + "']");
        super.element.click();
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
    }
    
    /*
     * Clicks the specified sub tab in the "Reports" tab in the Pay Station Details section
     */
    public final void navigatePayStationDetailsReportsSubTabs(final String reportsSubTab) {
        super.setWebElementByXpath("//section[@id='thirdNavArea']/nav/a[text()='" + reportsSubTab + "']");
        super.element.click();
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
    }
    
    /*
     * TODO: The following methods may be moved to a different class as their use is more
     * specific to other functionality.
     */
    
    /*
     * Gets all the activated and deactivated Pay Stations assigned to the customer
     * This method does not get the decomissioned or archived pay stations
     * This method assumes you are on the "Pay Stations" page.
     */
    public final List<AutomationPayStation> getActiveandDeactivatedPayStations() {
        //final WebDriverWait webDriverWait = new WebDriverWait(driver, WAIT_TIME_TWENTY_SECONDS);
        final List<AutomationPayStation> payStations = new ArrayList<AutomationPayStation>();
        
        //this.setPayStationElements();
        
        for (int i = 0; i < this.payStationElements.size(); i++) {
            final AutomationPayStation payStation = new AutomationPayStation();
            //webDriverWait.until(ExpectedConditions.elementToBeClickable(this.payStationElements.get(i)));
            super.element = this.payStationElements.get(i);
            LOGGER.info("Getting pay station info for: " + super.element.getText());
            super.element.click();
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            
            super.setWebElementByXpath("//h1[@id='paystationName']/span[@class='note']");
            if (super.element.getText().toLowerCase().contains("decommissioned") || super.element.getText().toLowerCase().contains("archived")) {
                LOGGER.info("Pay station is either archived or decommissioned. Moving to next pay station.");
                continue;
            }
            LOGGER.info("Pay station is neither archived nor decommissioned. Continuing to retrieve data.");
            
            super.setWebElementById("map");
            if (super.element.getText().contains("No geolocation data…")) {
                LOGGER.info("Pay station is not placed.");
                payStation.setIsPlaced(false);
            } else {
                LOGGER.info("Pay station is placed.");
                payStation.setIsPlaced(true);
            }
            
            this.navigatePayStationDetailsTabs(INFORMATION_TAB);
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            
            super.setWebElementById("psName");
            payStation.setName(super.element.getText());
            
            super.setWebElementById("psSerialNumber");
            payStation.setSerialNumber(super.element.getText());
            
            super.setWebElementById("psLocation");
            payStation.setLocation(super.element.getText());
            LOGGER.info("Retrieved pay station with Location: " + payStation.getLocation());
            
            payStations.add(payStation);
        }
        return payStations;
    }
}
