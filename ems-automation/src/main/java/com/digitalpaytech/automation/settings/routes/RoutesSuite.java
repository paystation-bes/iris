package com.digitalpaytech.automation.settings.routes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.digitalpaytech.automation.AutomationGlobals;
import com.digitalpaytech.automation.customer.navigation.CustomerNavigation;
import com.digitalpaytech.automation.customer.settings.paystation.PayStationsSuite;
import com.digitalpaytech.automation.dto.AutomationPayStation;
import com.digitalpaytech.automation.dto.AutomationUser;
import com.digitalpaytech.automation.systemadmin.navigation.SystemAdminNavigation;

public class RoutesSuite extends AutomationGlobals {
    protected static final Logger LOGGER = Logger.getLogger(RoutesSuite.class);
    
    protected String routeName = "";
    protected String newRouteName = "";
    protected String routeType = "";
    protected List<String> payStationNames = new ArrayList<String>();
    protected List<String> allPayStationNames = new ArrayList<String>();
    protected List<AutomationPayStation> payStations = new ArrayList<AutomationPayStation>();
    protected List<AutomationPayStation> allPayStations = new ArrayList<AutomationPayStation>();
    
    private CustomerNavigation customerNavigation = new CustomerNavigation();
    private SystemAdminNavigation systemAdminNavigation = new SystemAdminNavigation();
    
    /*
     * Check if the current assigned pay stations has a unplaced pay station
     * TODO (maybe): Currently on 7.0, system admins do not see the place pay station pin
     * for unplaced pay stations assigned to Routes. I originally thought there was a
     * button visible, but it looks like that's not the case. May need to modify this
     * method later.
     * Then runs an assertion for the pay station placement button based on placed/unplaced pay stations
     */
    protected final void checkPSPlacementButton() {
        /*if (!"systemadmin".equalsIgnoreCase(this.userType)) {
            for (int i = 0; i < this.payStations.size(); i++) {
                if (Boolean.FALSE.equals(this.payStations.get(i).getIsPlaced())) {
                    LOGGER.info("Checking if pay station placement button is visible...");
                    assertTrue(assertPSPlacementButton());
                    return;
                }
            }
            LOGGER.info("Checking if pay station placement button is not visible...");
            assertFalse(assertPSPlacementButton());
        }*/
        
        //Checking if the placement button exists on the routes page
        if (driver.findElements(By.id("PlacePS")).size() > 0 && driver.findElement(By.id("PlacePS")).isDisplayed())
        {
            //If the placement button is present, it gets clicked and then the list of unplaced pay stations are present
            driver.findElement(By.id("PlacePS")).click();
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            assertEquals(driver.findElement(By.cssSelector("div.current.notclickable")).getText(), "Unmapped Pay Stations");
        }
        
        //If the button is not there, all the existing pay stations get checked to make sure none is required to be placed
        else
        {
            //The pattern of pay stations's links are numbered in an xpath pattern except the first one that does not hav any numbers
            for (int i = 1; i <= this.payStations.size(); i++) {
                if (i==1)
                {
                    driver.findElement(By.xpath("//ul[@id='psChildList']/li/a")).click();
                    Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
                    Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
                    assertThat(driver.findElement(By.cssSelector("article > a")).getText(), not(equalTo("Pay Station Placement")));
                    driver.findElement(By.linkText("Routes")).click();
                    Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
                    Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
                    driver.findElement(By.xpath("//li/div/*[contains(., 'Test Route')]")).click();
                    Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
                    Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
                }
                //From the second pay station on the list, all the rest are numbered.
                else
                {
                    driver.findElement(By.xpath("//ul[@id='psChildList']/li[" + Integer.toString(i) + "]/a")).click();
                    Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
                    Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
                    assertThat(driver.findElement(By.cssSelector("article > a")).getText(), not(equalTo("Pay Station Placement")));
                    driver.findElement(By.linkText("Routes")).click();
                    Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
                    Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
                    driver.findElement(By.xpath("//li/div/*[contains(., 'Test Route')]")).click();
                    Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
                    Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
                }
            }
        }
    }
    
    /*
     * The following section are Custom assert methods
     */
    // Checks if the Pay Station placement button is visible
    // needs to be removed
    private boolean assertPSPlacementButton() {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, WAIT_TIME_TWENTY_SECONDS);
        
        try {
            webDriverWait.until(ExpectedConditions.elementToBeClickable(By.id("PlacePS")));
            LOGGER.info("Pay station placement button is visible.");
            return true;
        } catch (TimeoutException e) {
            LOGGER.info(e.getMessage());
            LOGGER.info("Pay station placement button is not visible.");
            return false;
        }
    }
    
    // Checks if the specified Route exists in the "Routes" list
    protected final boolean assertRouteExistsInList(final String routeNameValue) {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, WAIT_TIME_TWENTY_SECONDS);
        try {
            webDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ul[@id='routeList']/li/div/p[text()='" + routeNameValue + "']")));
            LOGGER.info("Route exists in route list.");
            return true;
        } catch (TimeoutException e) {
            LOGGER.info("Route does not exist in route list.");
            LOGGER.info(e.getMessage());
            return false;
        }
    }
    
    // Adds a new Route
    protected final void addRoute(final String routeNameValue, final String routeTypeValue, final List<String> payStationNamesValue) {
        LOGGER.info("Adding Route...");
        
        // Clicks the Add button
        super.setWebElementById("btnAddRoute");
        LOGGER.info("Clicking the Add route button...");
        super.element.click();
        LOGGER.info("Add route button has been clicked.");
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        
        this.changeRouteData(routeNameValue, routeTypeValue, payStationNamesValue);
        
        LOGGER.info("Route " + this.routeName + " has been added.");
    }
    
    // Edits an existing Route
    protected final void editRoute(final String oldRouteNameValue, final String newRouteNameValue, final String routeTypeValue,
                                   final List<String> payStationNamesValue) {
        LOGGER.info("Editing Route...");
        
        this.routeName = oldRouteNameValue;
        LOGGER.info("Clicking the Edit route option for Route " + oldRouteNameValue);
        this.clickOptionMenuBtn(oldRouteNameValue, "edit");
        LOGGER.info("Edit route option has been clicked");
        this.changeRouteData(newRouteNameValue, routeTypeValue, payStationNamesValue);
        
        LOGGER.info("Route " + oldRouteNameValue + " has been edited.");
    }
    
    // Deletes an existing Route
    protected final void deleteRoute(final String routeNameValue) {
        LOGGER.info("Deleting Route...");
        
        LOGGER.info("Clicking the Delete route option for Route " + routeNameValue);
        this.clickOptionMenuBtn(routeNameValue, "delete");
        LOGGER.info("Delete route option has been clicked");
        
        super.setWebElementByXpath("//button/span[text()='Delete']");
        super.element.click();
        
        LOGGER.info("Route " + routeNameValue + " has been deleted.");
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
    }
    
    // Verifies a specified Route
    // There is currently some issues preventing this method from working correctly
    protected final void verifyRoute(final String routeNameValue, final String routeTypeValue, final List<String> payStationNamesValue) {
        LOGGER.info("Verifying Route...");
        final WebDriverWait webDriverWait = new WebDriverWait(driver, WAIT_TIME_TWENTY_SECONDS);
        
        // Opens the details tab
        if (!driver.findElement(By.id("routeDetails")).isDisplayed()) {
            super.setWebElementById("route_" + this.getRouteId(routeNameValue));
            LOGGER.info("Opening the Details tab for route " + routeNameValue);
            super.element.click();
            LOGGER.info("Details tab has been opened.");
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        }
        
        super.setWebElementById("detailRouteName");
        assertEquals(routeNameValue, element.getText());
        LOGGER.info("Expected Route Name is: " + routeNameValue);
        LOGGER.info("Actual Route Name is: " + super.element.getText());
        
        super.setWebElementById("detailRouteType");
        assertEquals(routeTypeValue, element.getText());
        LOGGER.info("Expected Route Type is: " + routeTypeValue);
        LOGGER.info("Actual Route Type is: " + super.element.getText());
        
        super.setWebElementById("psChildList");
        
        // Checks if Pay Stations appear in Route
        for (String index : payStationNamesValue) {
            LOGGER.info("Checking Route for Pay Station \"" + index + "\".");
            
            // If pay station does not exist, test will break here
            webDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li/a[text()='" + index + "']")));
            LOGGER.info("Pay Station \"" + index + "\" exists.");
        }
        
        // If the list of pay stations is 0, then no pay stations were found.
        if (payStationNamesValue.size() == 0) {
            assertEquals("No pay stations found.", super.element.getText());
        }
        LOGGER.info("Route verified.");
    }
    
    // Changes the route data (Used in "addRoute" and "editRoute" methods
    private void changeRouteData(final String routeNameValue, final String routeTypeValue, final List<String> payStationNamesValue) {
        // Sets Route Name
        super.setWebElementById("formRouteName");
        LOGGER.info("Changing Route Name to: " + routeNameValue);
        super.element.clear();
        super.element.sendKeys(routeNameValue);
        LOGGER.info("Route Name has been changed.");
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        
        // Sets Route Type
        super.setWebElementById("formRouteType");
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        LOGGER.info("Changing Route Type to: " + routeTypeValue);
        super.element.clear();
        // Was having some issues directly clicking on the element.
        // Decided to type the Route Type in the drop down first and then selecting the option.
        super.element.sendKeys(routeTypeValue);
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        super.setWebElementByXpath("//li[@class='ui-menu-item']/a[text()='" + routeTypeValue + "']");
        super.element.click();
        LOGGER.info("Route Type has been changed.");
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        
        // Searches for and adds/removes all the specified pay stations
        for (String index : payStationNamesValue) {
            LOGGER.info("Adding/removing pay station \"" + index + "\" to the Route.");
            
            // Checks if pay station is on the the left hand list (not selected)
            // If pay station is in list, the pay station is selected (i.e. moved to the selected list)
            super.setWebElementByXpath("//ul[@id='routePSFullList']");
            if (super.element.getText().contains(index)) {
                LOGGER.info("Adding pay station \"" + index + "\".");
                super.setWebElementByXpath("//ul[@id='routePSFullList']/li[normalize-space(text())='" + index + "']");
                super.element.click();
                LOGGER.info("Pay station \"" + index + "\" added.");
                Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
                continue;
            }
            
            // Checks if pay station is on the right hand list (selected)
            // If pay station is not in list, the pay station is removed (i.e. moved to the left hand list)
            super.setWebElementByXpath("//ul[@id='routePSSelectedList']");
            if (super.element.getText().contains(index)) {
                LOGGER.info("Removing pay station \"" + index + "\".");
                super.setWebElementByXpath("//ul[@id='routePSSelectedList']/li[normalize-space(text())='" + index + "']");
                super.element.click();
                LOGGER.info("Pay station \"" + index + "\" removed.");
                Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
                continue;
            }
            LOGGER.info("Unable to add/remove pay station \"" + index + "\".");
        }
        
        // Clicks the Save button
        super.setWebElementByXpath("//section[@class='btnSet']/a[text()='Save']");
        super.element.click();
        LOGGER.info("Save button has been clicked");
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        
        // Saves values for future use
        this.routeName = routeNameValue;
        this.routeType = routeTypeValue;
        this.payStationNames = payStationNamesValue;
    }
    
    // Gets the unique ID of the Route
    private String getRouteId(final String routeNameValue) {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, WAIT_TIME_TWENTY_SECONDS);
        WebElement element;
        String routeId = "";
        
        webDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ul[@id='routeList']/li/div/p[text()='" + routeNameValue + "']")));
        element = driver.findElement(By.xpath("//ul[@id='routeList']/li/div/p[text()='" + routeNameValue + "']/../.."));
        routeId = element.getAttribute("id").split("_")[1];
        
        LOGGER.info("Route Id is: " + routeId);
        
        return routeId;
    }
    
    // Clicks the Option Menu button that appears beside the Route
    private void clickOptionMenuBtn(final String routeNameValue, final String option) {
        final String routeId = this.getRouteId(routeNameValue);
        
        super.setWebElementById("opBtn_" + routeId);
        super.element.click();
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        
        super.setWebElementById(option + "_" + routeId);
        super.element.click();
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
    }
    
    // Gets the user's pay stations and goes to the Routes page
    protected final void getToRoutesPageSystemAdmin(final Boolean getPaystations) {
        loginLogout.goToLoginPageAndLogin(super.userName, super.password, super.xmlData, super.driver);
        
        // Go to Customer workspace
        List<AutomationUser> users = new ArrayList<AutomationUser>();
        users = xmlData.getUserList();
        for (int i = 0; i < users.size(); i++) {
            if (!("Parent".equals(users.get(i).getUserType()) || "SystemAdmin".equals(users.get(i).getUserType()))) {
                systemAdminNavigation.systemAdminCustomerSearch(driver, users.get(i).getCustomerName());
                LOGGER.info("Now in the customer workspace of: " + users.get(i).getCustomerName());
                Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
                break;
            }
        }
        
        this.getToRoutesPageAllUsers(getPaystations);
    }
    
    // Gets to the "Routes" page for a customer user
    protected final void getToRoutesPageCustomer(final Boolean getPaystations) {
        loginLogout.goToLoginPageAndLogin(super.userName, super.password, super.xmlData, super.driver);
        
        // Navigate to the "Settings" page
        //customerNavigation.navigateLeftHandMenu("Settings");
        driver.findElement(By.cssSelector("img[alt='Settings']")).click();
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        
        this.getToRoutesPageAllUsers(getPaystations);
    }
    
    // This is the logic that will go to the Routes page for any account
    private void getToRoutesPageAllUsers(final Boolean getPaystations) {
        if (getPaystations) {
            // Navigate to the "Pay Stations" tab
            //customerNavigation.navigatePageTabs("Pay Stations");
            driver.findElement(By.linkText("Pay Stations")).click();
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            
            // Get all the pay stations for the selected user
            final PayStationsSuite payStationTestSuite = new PayStationsSuite(driver);
            payStationTestSuite.payStationElements.addAll(driver.findElements(By.xpath("//ul[@id='paystationList']/span/li")));
            this.payStations = payStationTestSuite.getActiveandDeactivatedPayStations();
            this.allPayStations.addAll(this.payStations);
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            
            // Get Pay Station Names
            for (int i = 0; i < this.payStations.size(); i++) {
                this.payStationNames.add(this.payStations.get(i).getName());
            }
            this.allPayStationNames.addAll(this.payStationNames);
        }
        
        // Go to "Routes" page
        //customerNavigation.navigatePageTabs("Routes");
        driver.findElement(By.linkText("Routes")).click();
    }
}
