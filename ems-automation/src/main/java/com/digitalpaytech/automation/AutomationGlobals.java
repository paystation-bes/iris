package com.digitalpaytech.automation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeTrue;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.SessionNotFoundException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.digitalpaytech.automation.dto.AutomationUser;
import com.digitalpaytech.automation.sessioncontrol.LoginLogout;

public class AutomationGlobals implements AutomationConstants {
    protected static final Logger LOGGER = Logger.getLogger(AutomationGlobals.class);
    public WebDriver driver;
    protected XmlData xmlData;
    protected LoginLogout loginLogout;
    
    protected WebElement element;
    protected List<WebElement> elementList;
    
    // User parameters for the current user that is logged in when the script(s) run
    protected String password = "";
    protected String userName = "";
    protected String userType = "";
    protected String customerName = "";
    protected String timeZone = "";
    
    public AutomationGlobals() {
        this.loginLogout = new LoginLogout();
    }
    
    // Checks if browser is specified in XML and then opens up the specified browser
    protected final void startBrowser(final String browserTypeStr) {
        assumeTrue(xmlData.isBrowserSpecifiedInXML(browserTypeStr));
        this.initializeDriver(this.findBrowserType(browserTypeStr));
    }
    
    // Grabs the browser type based on string
    private int findBrowserType(final String browserTypeStr) {
        if ("chrome".equalsIgnoreCase(browserTypeStr)) {
            return DRIVER_TYPE_CHROME;
        } else if ("ie".equalsIgnoreCase(browserTypeStr)) {
            return DRIVER_TYPE_IE;
        } else {
            return DRIVER_TYPE_FIREFOX;
        }
    }
    
    // Starts a new driver for the specified browser
    private void initializeDriver(final int browserType) {
        switch (browserType) {
            case DRIVER_TYPE_CHROME:
                driver = new ChromeDriver();
                break;
            case DRIVER_TYPE_FIREFOX:
                driver = new FirefoxDriver();
                break;
            case DRIVER_TYPE_IE:
                driver = new InternetExplorerDriver();
                break;
            default:
                driver = new FirefoxDriver();
                break;
        }
    }
    
    /*
     * This is a generic set up method that can be used for the @Before part of the script.
     * The method nulls the driver variable, sets up variables based on values in the specified XML,
     * checks if there is a valid browser in the XML, sets up the user info to login, and finally
     * logs into the specified account.
     */
    protected final void genericSetUp(final String xmlFileName, final String userTypeStr) {
        /*
         * Need to null driver just in case it was used in a previous test case and is still initialized.
         */
        //driver = null;
        xmlData = new XmlData(xmlFileName);
        assumeTrue(xmlData.getBrowsers().size() > 0);
        // Checks to see if there is a browser specified in the XML
        final String checkBrowser = xmlData.getBrowsers().get(0);
        boolean isValidBrowser = false;
        if ("chrome".equalsIgnoreCase(checkBrowser) || "firefox".equalsIgnoreCase(checkBrowser) || "ie".equalsIgnoreCase(checkBrowser)) {
            isValidBrowser = true;
        }
        assumeTrue(isValidBrowser);
        
        final AutomationUser user = xmlData.getUserByUserType(userTypeStr);
        assumeTrue(userTypeStr.equalsIgnoreCase(user.getUserType()));
        
        this.password = user.getPassword();
        this.userName = user.getUserName();
        this.userType = user.getUserType();
        this.customerName = user.getCustomerName();
    }
    
    /*
     * This is a generic tear down method that can be used for the @After part of the script.
     */
    protected final void genericTearDown() {
        try {
            loginLogout.logout(this.driver);
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            driver.quit();
        } catch (NullPointerException e) {
            LOGGER.info(e.getMessage());
        } catch (SessionNotFoundException e) {
            // This exception is thrown when a driver is not open, but the driver variable is not null.
            LOGGER.info(e.getMessage());
        }
        /*
         * Driver is set to null. If driver is not set to null, there is the possibility that the next test case
         * will run, but never start a driver (driver has quit from previous line). Therefore you would have a
         * driver value that is initialized to the previous used driver, but the actual browser would not be visible.
         * This situation will cause an error. However if the driver is set to "null" and there is no driver visible,
         * the test case will be skipped.
         */
        //driver = null;
    }
    
    /*
     * Performs the logic to choose an option from a drop-down menu
     */
    protected final void selectOptionFromDropDown(final String dropDownId, final String option) {
        this.setWebElementById(dropDownId);
        this.element.clear();
        this.element.sendKeys(option);
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        this.setWebElementByXpath("//li[@class='ui-menu-item']/a[text()='" + option + "']");
        this.element.click();
    }
    
    /*
     * Loads all elements in a list (e.g. the Collection Center's "Recent Collections" list or the
     * Maintenance Center's "Resolved Alerts" list by selecting the list and pressing the "END" key
     * until no more elements appear. Once all elements are loaded, the "HOME" key is pressed.
     * @param: xPath - The xPath of the list to load.
     */
    protected final void loadAllElementsInList(final String xPath) {
        int previousListSize = 0;
        this.setWebElementByXpath(xPath);
        do {
            previousListSize = this.element.findElements(By.xpath("span[@class='listItems']/li")).size();
            this.element.sendKeys(Keys.END);
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            this.setWebElementByXpath(xPath);
        } while (this.element.findElements(By.xpath("span[@class='listItems']/li")).size() != previousListSize);
        this.element.sendKeys(Keys.HOME);
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
    }
    
    /*
     * Waits until element can be clicked and then sets the element parameter to that element
     * based on the Id value
     */
    protected final void setWebElementById(final String webElementId) {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, WAIT_TIME_TWENTY_SECONDS);
        try {
            webDriverWait.until(ExpectedConditions.elementToBeClickable(By.id(webElementId)));
            this.element = driver.findElement(By.id(webElementId));
        } catch (TimeoutException e) {
            LOGGER.info(e.getMessage());
        }
    }
    
    /*
     * Waits until element can be clicked and then sets the element parameter to that element
     * based on the Id value
     */
    protected final WebElement selecttWebElementById(final String webElementId) {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, WAIT_TIME_TWENTY_SECONDS);
        
        webDriverWait.until(ExpectedConditions.elementToBeClickable(By.id(webElementId)));
        this.element = driver.findElement(By.id(webElementId));
        
        return this.element;
    }
    
    /*
     * Waits until element can be clicked and then sets the element parameter to that element
     * based on the Xpath value
     */
    protected final void setWebElementByXpath(final String webElementXpath) {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, WAIT_TIME_TWENTY_SECONDS);
        try {
            webDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath(webElementXpath)));
            this.element = driver.findElement(By.xpath(webElementXpath));
        } catch (TimeoutException e) {
            LOGGER.info(e.getMessage());
        }
    }
    
    /*
     * Waits until element can be clicked and then sets the element parameter to that element
     * based on the Xpath value
     */
    protected final WebElement selectWebElementByXpath(final String webElementXpath) {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, WAIT_TIME_TWENTY_SECONDS);
        
        webDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath(webElementXpath)));
        this.element = driver.findElement(By.xpath(webElementXpath));
        
        return this.element;
    }
    
    /*
     * Waits until element can be clicked and then sets the assigned text to the textbox
     * based on the Xpath value
     */
    protected final void assignText(final String webId, final String webName, final String value) {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, WAIT_TIME_TWENTY_SECONDS);
        
        webDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='" + webId + "'" + "and @name='" + webName + "'" + "]")));
        this.element = driver.findElement(By.xpath("//input[@id='" + webId + "'" + "and @name='" + webName + "'" + "]"));
        this.element.clear();
        
        this.element.sendKeys(value);
    }
    
    /*
     * Waits until element can be clicked and then sets the assigned text to the textbox
     * based on the Xpath value. This method is used to type a name of a customer and go there in System Admin
     */
    protected final void goToCustomerSystemAdmin(final String webId, final String webType, final String value) {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, WAIT_TIME_TWENTY_SECONDS);
        
        webDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='" + webId + "'" + "and @type='" + webType + "'" + "]")));
        this.element = driver.findElement(By.xpath("//input[@id='" + webId + "'" + "and @type='" + webType + "'" + "]"));
        
        this.element.sendKeys(value);
        waitOneSecond();
        
        driver.findElement(By.cssSelector("span.info")).click();
        waitOneSecond();
        
        this.element = driver.findElement(By.xpath("//a[@id='btnGo' and @class='linkButtonFtr textButton search']"));
        waitOneSecond();
        this.element.click();
    }
    
    /*
     * Waits until element can be clicked and then checks the checkbox
     * based on the Xpath value
     */
    protected final void enableCheckBox(final String webId) {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, WAIT_TIME_TWENTY_SECONDS);
        
        webDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@id='" + webId + "'" + "and @class='checkBox']")));
        this.element = driver.findElement(By.xpath("//a[@id='" + webId + "'" + "and @class='checkBox']"));
        
        this.element.click();
    }
    
    /*
     * Waits until element can be clicked and then sets the elements parameter to that element
     * based on the Id value
     */
    protected final void setWebElementsById(final String webElementId) {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, WAIT_TIME_TWENTY_SECONDS);
        try {
            webDriverWait.until(ExpectedConditions.elementToBeClickable(By.id(webElementId)));
            this.elementList = driver.findElements(By.id(webElementId));
        } catch (TimeoutException e) {
            LOGGER.info(e.getMessage());
        }
    }
    
    /*
     * Waits until element can be clicked and then sets the elements parameter to that element
     * based on the Xpath value
     */
    protected final void setWebElementsByXpath(final String webElementXpath) {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, WAIT_TIME_TWENTY_SECONDS);
        try {
            webDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath(webElementXpath)));
            this.elementList = driver.findElements(By.xpath(webElementXpath));
        } catch (TimeoutException e) {
            LOGGER.info(e.getMessage());
        }
    }
    
    /*
     * This method provides the ability to click on the + button to load every single Add form
     */
    protected final void clickAddButton(final String webElementId) {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, WAIT_TIME_TWENTY_SECONDS);
        
        webDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@id='" + webElementId + "'"
                                                                             + "and @class='hdrButtonIcn add' and @title='Add'" + "]")));
        this.element = driver.findElement(By.xpath("//a[@id='" + webElementId + "'" + "and @class='hdrButtonIcn add' and @title='Add'" + "]"));
        
        this.element.click();
    }
    
    /*
     * This method provides the ability to click on the SAVE button to save a form
     */
    protected final void saveButton() {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, WAIT_TIME_TWENTY_SECONDS);
        
        webDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='btnSet']/a[@class='linkButtonFtr textButton save']")));
        this.element = driver.findElement(By.xpath("//section[@class='btnSet']/a[@class='linkButtonFtr textButton save']"));
        
        this.element.click();
    }
    
    /*
     * This method provides the ability to click on the CANCEL button to save a form
     */
    protected final void cancelButton() {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, WAIT_TIME_TWENTY_SECONDS);
        
        webDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//section[@class='btnSet']/a[@class='linkButton textButton cancel']")));
        this.element = driver.findElement(By.xpath("//section[@class='btnSet']/a[@class='linkButton textButton cancel']"));
        
        this.element.click();
    }
    
    /*
     * This method provides the ability to continue with the rest of the test if an assertion fails
     */
    protected final void assertEqualsContinue(String validateString) {
        //final StringBuffer verificationErrors = new StringBuffer(); 
        try {
            assertEquals("Error Occurred:  ", validateString, this.element.getText());
        }//
        catch (AssertionError e) {
            //verificationErrors.append(e.toString()); 
            //assertEquals(null, validateString, this.driver.getTitle());
            //System.out.println(e);
            LOGGER.debug(e);
            
        }
    }
    
    /*
     * This method provides the ability to continue with the rest of the test if an assertion fails
     */
    protected final void assertEqualsTitleContinue(String validateString) {
        final StringBuffer verificationErrors = new StringBuffer();
        try {
            assertEquals("Error Occurred:  ", validateString, this.driver.getTitle());
        } catch (AssertionError e) {
            //verificationErrors.append(e.toString()); 
            //assertEquals(null, validateString, this.driver.getTitle());
            LOGGER.debug(e);
        }
    }
    
    protected final void waitOneSecond() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }
    
}
