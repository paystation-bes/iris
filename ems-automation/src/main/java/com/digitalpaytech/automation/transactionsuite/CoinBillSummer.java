package com.digitalpaytech.automation.transactionsuite;

public class CoinBillSummer {
    private static final String COMMA = ",";
    private static final String COLON = ":";
    private static final Double ONE_HUNDRED = 100.0;
    private final int coinCount;
    private final double coinValue;
    private final int billCount;
    private final double billValue;
    
    public CoinBillSummer(final String coinToCount, final String billToCount) {
        this.coinCount = count(coinToCount);
        this.coinValue = value(coinToCount);
        this.billCount = count(billToCount);
        this.billValue = value(billToCount);
    }
    
    public final int count(final String toCount) {
        final String[] splitOne = toCount.split(CoinBillSummer.COMMA);
        String[] splitTwo;
        int count = 0;
        for (String commaSplit : splitOne) {
            
            splitTwo = commaSplit.split(CoinBillSummer.COLON);
            count += Integer.valueOf(splitTwo[1]);
        }
        return count;
    }
    
    public final Double value(final String toCount) {
        final String[] splitOne = toCount.split(CoinBillSummer.COMMA);
        String[] splitTwo;
        Double value = 0.0;
        for (String commaSplit : splitOne) {
            splitTwo = commaSplit.split(CoinBillSummer.COLON);
            value += Double.valueOf(splitTwo[0]) * Double.valueOf(splitTwo[1]);
            
        }
        return value;
    }
    
    public final int getCoinCount() {
        return this.coinCount;
    }
    
    public final double getCoinValue() {
        return this.coinValue / ONE_HUNDRED;
    }
    
    public final int getBillCount() {
        return this.billCount;
    }
    
    public final double getBillValue() {
        return this.billValue;
    }
    
}
