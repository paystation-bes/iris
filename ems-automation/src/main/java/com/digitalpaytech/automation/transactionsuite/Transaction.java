package com.digitalpaytech.automation.transactionsuite;

import com.digitalpaytech.automation.jmeter.JMeterTestPlan;
import com.digitalpaytech.automation.jmeter.PreAuthResultsParser;
import com.digitalpaytech.automation.jmeter.SOAPRequest;
import com.digitalpaytech.automation.jmeter.TransXMLParser;

public class Transaction {
    
    private static final String CC_PREAUTH_FILE = "TransactionData/CCPreAuth.xml";
    private static final String ACKNOWLEDGE = "acknowledge";
    private static final int FOUR_HUNDRED = 400;
    private boolean success;
    private final TransXMLParser transParser;
    private final String filePath;
    private TransactionDetails transactionDetails;
    
    public Transaction(final String filePath) {
        this.transParser = new TransXMLParser();
        this.transactionDetails = this.transParser.parse(filePath);
        this.filePath = filePath;
    }
    
    public final void sendCashTransaction() {
        final SOAPRequest cashRequestData = new SOAPRequest(this.transactionDetails);
        final String xmlData = cashRequestData.buildXML(this.filePath);
        
        final JMeterTestPlan testPlan = new JMeterTestPlan(xmlData);
        testPlan.createTestPlan();
        
        this.success = checkResponseCode(ACKNOWLEDGE, testPlan);
        
    }
    
    public final void sendCCTransaction() {
        
        final TransactionDetails preAuthDetails = this.transParser.parse(Transaction.CC_PREAUTH_FILE);
        
        final SOAPRequest preAuthRequestData = new SOAPRequest(preAuthDetails);
        final String preAuthXmlData = preAuthRequestData.buildXML(Transaction.CC_PREAUTH_FILE);
        
        final JMeterTestPlan preAuthtestPlan = new JMeterTestPlan(preAuthXmlData);
        preAuthtestPlan.createTestPlan();
        
        final boolean preAuthSuccess = checkResponseCode("authorized", preAuthtestPlan);
        
        final PreAuthResultsParser preAuthResParser = new PreAuthResultsParser(preAuthtestPlan.getResult());
        preAuthResParser.parse();
        
        this.transactionDetails = this.transParser.parse(this.filePath);
        
        this.transactionDetails.setEmsPreAuthId(preAuthResParser.getEmsPreAuthId());
        this.transactionDetails.setCardAuthorizationId(preAuthResParser.getCardAuthId());
        
        final SOAPRequest postAuthRequestData = new SOAPRequest(this.transactionDetails);
        final String postAuthxmlData = postAuthRequestData.buildXML(this.filePath);
        
        final JMeterTestPlan postAuthtestPlan = new JMeterTestPlan(postAuthxmlData);
        postAuthtestPlan.createTestPlan();
        
        this.success = checkResponseCode(ACKNOWLEDGE, postAuthtestPlan);
    }
    
    private boolean checkResponseCode(final String expected, final JMeterTestPlan testPlan) {
        final boolean responseCode = testPlan.getResponseCode() < FOUR_HUNDRED;
        System.out.println(testPlan.getResult());
        final boolean responseMessage = testPlan.getResult().toLowerCase().contains(expected);
        
        return responseMessage && responseCode;
        
    }
    
    public final boolean getSuccess() {
        return this.success;
    }
    
    public final TransactionDetails getTransactionDetails() {
        return this.transactionDetails;
    }
}
