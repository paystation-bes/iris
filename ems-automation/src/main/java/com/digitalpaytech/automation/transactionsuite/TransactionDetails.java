package com.digitalpaytech.automation.transactionsuite;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

public class TransactionDetails {
    private static final long SIXTY_THOUSAND = 60000L;
    private static final int ONE_HUNDRED = 100;
    private static final int TEN_THOUSAND = 10000;
    private static final int ONE_THOUSAND = 1000;
    private static final String CASH = "Cash";
    private static final String ZERO_DEC = "0.00";
    private static final String CASHCC = "Cash/CC";
    private static final String CREDIT_CARD = "Credit Card";
    
    private static final DecimalFormat DEC_FORMAT = new DecimalFormat(ZERO_DEC);
    private String messageNumber;
    private String paystationCommAddress;
    private String licensePlateNo;
    private String lotNumber;
    private String stallNumber;
    private String addTimeNum;
    private String type;
    private String paymentType;
    private String purchasedDate;
    private String parkingTimePurchased;
    private String originalAmount;
    private String chargedAmount;
    private String cashPaid;
    private String cardPaid;
    private String cardAuthorizationId;
    private String emsPreAuthId;
    private String expiryDate;
    private String ticketNumber;
    private String coinCol;
    private String billCol;
    private String dateFmt = "yyyy:MM:dd:HH:mm:ss':GMT'";
    private String timeZone = "GMT";
    private long expiryTimeLong;
    private long purchaseTimeLong;
    private Date purchaseTimeDate;
    private String excessPayment;
    
    public TransactionDetails() {
        final Random randomGenerator = new Random();
        this.ticketNumber = String.valueOf(randomGenerator.nextInt(ONE_HUNDRED) + 1);
        this.licensePlateNo = String.valueOf(randomGenerator.nextInt(TEN_THOUSAND) + 1);
        this.messageNumber = String.valueOf(randomGenerator.nextInt(ONE_HUNDRED) + 1);
        this.stallNumber = String.valueOf(randomGenerator.nextInt(ONE_HUNDRED));
        this.purchaseTimeLong = (long) (Math.floor(System.currentTimeMillis() / ONE_THOUSAND) * ONE_THOUSAND);
        this.paymentType = CASH;
    }
    
    // TODO Expand this to use smart cards
    public final void setPaymentType(final String cashAmount, final String cardAmount) {
        
        if ("".equals(cardAmount)) {
            this.paymentType = CASH;
        } else if ("".equals(cashAmount)) {
            this.paymentType = CREDIT_CARD;
        } else {
            this.paymentType = CASHCC;
        }
    }
    
    public final void formatPurchaseDate() {
        this.purchaseTimeDate = new Date(this.purchaseTimeLong);
        this.purchasedDate = String.valueOf(format(this.purchaseTimeDate, this.timeZone));
    }
    
    public final void formatExpiryDate() {
        if (this.parkingTimePurchased == null) {
            this.parkingTimePurchased = "0";
        }
        this.expiryTimeLong = this.purchaseTimeLong + (Long.valueOf(this.parkingTimePurchased) * SIXTY_THOUSAND);
        this.expiryDate = String.valueOf(format(new Date(this.expiryTimeLong), this.timeZone));
    }
    
    public final String getMessageNumber() {
        return this.messageNumber;
    }
    
    public final void setMessageNumber(final String messageNumber) {
        this.messageNumber = messageNumber;
    }
    
    public final String getPaystationCommAddress() {
        return this.paystationCommAddress;
    }
    
    public final void setPaystationCommAddress(final String paystationCommAddress) {
        this.paystationCommAddress = paystationCommAddress;
    }
    
    public final String getLicensePlateNo() {
        return this.licensePlateNo;
    }
    
    public final void setLicensePlateNo(final String licensePlateNo) {
        this.licensePlateNo = licensePlateNo;
    }
    
    public final String getLotNumber() {
        return this.lotNumber;
    }
    
    public final void setLotNumber(final String lotNumber) {
        this.lotNumber = lotNumber;
    }
    
    public final String getStallNumber() {
        return this.stallNumber;
    }
    
    public final void setStallNumber(final String stallNumber) {
        this.stallNumber = stallNumber;
    }
    
    public final String getAddTimeNum() {
        return this.addTimeNum;
    }
    
    public final void setAddTimeNum(final String addTimeNum) {
        this.addTimeNum = addTimeNum;
    }
    
    public final String getType() {
        return this.type;
    }
    
    public final void setType(final String type) {
        this.type = type;
    }
    
    public final String getPurchasedDate() {
        return this.purchasedDate;
    }
    
    public final void setPurchasedDate(final String purchasedDate) {
        this.purchasedDate = purchasedDate;
    }
    
    public final String getParkingTimePurchased() {
        return this.parkingTimePurchased;
    }
    
    public final void setParkingTimePurchased(final String parkingTimePurchased) {
        this.parkingTimePurchased = parkingTimePurchased;
    }
    
    public final String getOriginalAmount() {
        return this.originalAmount;
    }
    
    public final void setOriginalAmount(final String originalAmount) {
        this.originalAmount = originalAmount;
    }
    
    public final String getChargedAmount() {
        return this.chargedAmount;
    }
    
    public final void setChargedAmount(final String chargedAmount) {
        this.chargedAmount = chargedAmount;
    }
    
    public final String getCashPaid() {
        return this.cashPaid;
    }
    
    public final void setCashPaid(final String cashPaid) {
        this.cashPaid = cashPaid;
    }
    
    public final String getCardPaid() {
        return this.cardPaid;
    }
    
    public final void setCardPaid(final String cardPaid) {
        this.cardPaid = cardPaid;
    }
    
    public final String getCardAuthorizationId() {
        return this.cardAuthorizationId;
    }
    
    public final void setCardAuthorizationId(final String cardAuthorizationId) {
        this.cardAuthorizationId = cardAuthorizationId;
    }
    
    public final String getEmsPreAuthId() {
        return this.emsPreAuthId;
    }
    
    public final void setEmsPreAuthId(final String emsPreAuthId) {
        this.emsPreAuthId = emsPreAuthId;
    }
    
    public final String getExpiryDate() {
        return this.expiryDate;
    }
    
    public final void setExpiryDate(final String expiryDate) {
        this.expiryDate = expiryDate;
    }
    
    public final String getPaymentType() {
        return this.paymentType;
    }
    
    public final void setPaymentType(final String paymentType) {
        this.paymentType = paymentType;
    }
    
    public final String getTicketNumber() {
        return this.ticketNumber;
    }
    
    public final void setTicketNumber(final String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }
    
    public final String getDateFmt() {
        return this.dateFmt;
    }
    
    public final void setDateFmt(final String dateFmt) {
        this.dateFmt = dateFmt;
    }
    
    public final String getTimeZone() {
        return this.timeZone;
    }
    
    public final void setTimeZone(final String timeZone) {
        this.timeZone = timeZone;
    }
    
    public final long getExpiryTimeLong() {
        return this.expiryTimeLong;
    }
    
    public final void setExpiryTimeLong(final long expiryTimeLong) {
        this.expiryTimeLong = expiryTimeLong;
    }
    
    public final long getPurchaseTimeLong() {
        return this.purchaseTimeLong;
    }
    
    public final void setPurchaseTimeLong(final long purchaseTimeLong) {
        this.purchaseTimeLong = purchaseTimeLong;
    }
    
    public final Date getPurchaseTimeDate() {
        return this.purchaseTimeDate;
    }
    
    public final void setPurchaseTimeDate(final Date purchaseTimeDate) {
        this.purchaseTimeDate = purchaseTimeDate;
    }
    
    public final String getCoinCol() {
        return this.coinCol;
    }
    
    public final void setCoinCol(final String coinCol) {
        this.coinCol = coinCol;
    }
    
    public final String getBillCol() {
        return this.billCol;
    }
    
    public final void setBillCol(final String billCol) {
        this.billCol = billCol;
    }
    
    public final String getExcessPayment() {
        setExcessPayment();
        return this.excessPayment;
    }
    
    public final void setExcessPayment() {
        String chargeAmountStr = this.chargedAmount;
        String cardAmountStr = this.cardPaid;
        String cashAmountStr = this.cashPaid;
        if ("".equals(chargeAmountStr)) {
            chargeAmountStr = ZERO_DEC;
        }
        if ("".equals(cardAmountStr)) {
            cardAmountStr = ZERO_DEC;
        }
        if ("".equals(cashAmountStr)) {
            cashAmountStr = ZERO_DEC;
        }
        final Double chargeAmount = Double.valueOf(chargeAmountStr);
        final Double cardAmount = Double.valueOf(cardAmountStr);
        final Double cashAmount = Double.valueOf(cashAmountStr);
        final Double total = cardAmount + cashAmount;
        this.excessPayment = TransactionDetails.DEC_FORMAT.format(total - chargeAmount);
    }
    
    private String format(final Date dateStr, final String timeZoneStr) {
        final SimpleDateFormat formatter = new SimpleDateFormat(this.dateFmt);
        formatter.setTimeZone(TimeZone.getTimeZone(timeZoneStr));
        return formatter.format(dateStr);
    }
    
}
