package com.digitalpaytech.automation.sessioncontrol;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.interactions.Actions;

import com.digitalpaytech.automation.AutomationGlobals;
import com.digitalpaytech.automation.XmlData;
import com.digitalpaytech.automation.dto.AutomationUser;

public class NewUserLogin extends AutomationGlobals {
    protected static final Logger LOGGER = Logger.getLogger(NewUserLogin.class);
    protected static final int SLEEPTIMESECONDS = 3;
    protected static final int WAITTIMESECONDS = 20;
    
    protected XmlData xmlData;
    
    protected String password = "";
    protected String customerName = "";
    protected String userName = "";
    protected String userType = "";
    
    protected List<AutomationUser> users = new ArrayList<AutomationUser>();
    
    // Checks if on the Service Agreement page
    public final boolean isOnServiceAgreementPage() {
        boolean isOnServiceAgreementPage = false;
        switch (this.userType.toLowerCase()) {
            case "systemadmin":
                assertEquals("Digital Iris - Service Agreement", driver.getTitle());
                isOnServiceAgreementPage = true;
                break;
            case "parent":
                assertEquals("Digital Iris - Service Agreement", driver.getTitle());
                isOnServiceAgreementPage = true;
                break;
            case "child":
                isOnServiceAgreementPage = false;
                break;
            case "standalone":
                isOnServiceAgreementPage = true;
                break;
            default:
                isOnServiceAgreementPage = false;
                break;
        }
        
        return isOnServiceAgreementPage;
    }
    
    //Performs the automation to accept the service agreement
    public final void acceptServiceAgreement(final String customerNameValue) {
        // Return if not on Service Agreement page
        if (!isOnServiceAgreementPage()) {
            return;
        }
        
        final String[] titles = {"Gambit", "The Hulk", "Wolverine", "Professor X", "Cyclops", "Silver Surfer", "Punisher"};
        
        super.setWebElementById("name");
        super.element.sendKeys(customerNameValue);
        
        super.setWebElementById("title");
        super.element.sendKeys(titles[new Random().nextInt(titles.length)]);
        
        super.setWebElementById("organization");
        super.element.sendKeys("Digital Payment Technologies");
        
        super.setWebElementById("saDetails");
        
        // Scrolls to the bottom of the Service Agreement so that the "Accept" button
        // becomes visible.
        // THis method does not work in IE, therefore make sure test is either Chrome or FF.
        final Actions builder = new Actions(driver);
        builder.moveToElement(super.element)
            .click()
            .sendKeys(Keys.END)
            .build()
            .perform();
        
        super.setWebElementById("saAccept");
        super.element.click();
    }
    
    //Changes the user password
    public final void changeDefaultPassword(final String newPassword) {
        assertEquals("Digital Iris - Change Password", driver.getTitle());
        
        super.setWebElementById("newPassword");
        super.element.sendKeys(newPassword);
        
        super.setWebElementById("c_newPassword");
        super.element.sendKeys(newPassword);
        
        super.setWebElementById("changePassword");
        super.element.click();
        
        Sleeper.sleepTightInSeconds(SLEEPTIMESECONDS);
        
        switch (this.userType.toLowerCase()) {
            case "systemadmin":
                assertEquals("Digital Iris - System Administration", driver.getTitle());
                break;
            case "parent":
                assertEquals("Digital Iris - Main (Dashboard)", driver.getTitle());
                break;
            case "child":
                assertEquals("Digital Iris - Main (Dashboard)", driver.getTitle());
                break;
            case "standalone":
                assertEquals("Digital Iris - Main (Dashboard)", driver.getTitle());
                break;
            default:
                break;
        }
    }
}
