package com.digitalpaytech.automation.sessioncontrol;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.digitalpaytech.automation.AutomationConstants;
import com.digitalpaytech.automation.XmlData;

public class LoginLogout {
    private static final int WAITTIMESECONDS = 10;
    private static final Logger LOGGER = Logger.getLogger(LoginLogout.class);
    
    /*
     * Goes to the login page and logs in.
     */
    public final void goToLoginPageAndLogin(final String userName, final String password, final XmlData xmlData, final WebDriver driver) {
        goToLoginPage(xmlData.getUrl(), driver);
        login(userName, password, driver);
        Sleeper.sleepTightInSeconds(AutomationConstants.SLEEP_TIME_ONE_SECOND);
    }
    
    // Does additional logic to see if the server is running
    public final void goToLoginPage(final String url, final WebDriver driver) {
        driver.get(url);
        try {
            driver.findElement(By.id("username"));
        } catch (NoSuchElementException e) {
            LOGGER.info("Could not connect to: " + url + ". Server is most likely down.");
            LOGGER.info(e.getMessage());
            driver.close();
        }
    }
    
    //Login method - logs into specified user account
    public final void login(final String userName, final String password, final WebDriver driver) {
        WebElement element;
        
        LOGGER.info(userName + " logging in...");
        element = driver.findElement(By.id("username"));
        element.sendKeys(userName);
        element = driver.findElement(By.id("password"));
        element.sendKeys(password);
        element = driver.findElement(By.id("submit"));
        element.click();
        
        LOGGER.info(userName + " logged in.");
        LOGGER.info("UserName: " + userName + " Password: " + password);
    }
    
    // Logout method - logs out of account
    public final void logout(final WebDriver driver) {
        WebElement element;
        
        LOGGER.info("User logging out...");
        
        element = driver.findElement(By.xpath("//aside/a[@title='Logout']"));
        element.click();
        
        LOGGER.info("User logged out.");
        
        try {
            if (driver.findElement(By.id("messageResponseAlertBox")).isDisplayed()) {
                this.closeLogoutAlertBox(driver);
            }
        } catch (NoSuchElementException e) {
            LOGGER.info(e.getMessage());
        }
    }
    
    // Gets the user type
    public final String getUserType(final WebDriver driver) {
        String userType = "";
        userType = driver.getTitle();
        if (userType.toLowerCase().contains("system admin")) {
            return "SystemAdmin";
        } else {
            return "Other";
        }
    }
    
    // Sometimes there may be an alert box that appears when logging out.
    // Not too sure why this happens... possibly timing issues.
    // This method closes the alert box if it appears.
    public final void closeLogoutAlertBox(final WebDriver driver) {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, WAITTIMESECONDS);
        WebElement element;
        
        LOGGER.info("Alert Box visible. Closing box...");
        
        webDriverWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@title='Close']")));
        element = driver.findElement(By.xpath("//a[@title='Close']"));
        element.click();
        
        LOGGER.info("Alert Box closed.");
    }
}
