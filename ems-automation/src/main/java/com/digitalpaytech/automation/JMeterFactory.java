package com.digitalpaytech.automation;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;

import org.apache.jmeter.engine.StandardJMeterEngine;
import org.apache.jmeter.save.SaveService;
import org.apache.jmeter.util.JMeterUtils;

public final class JMeterFactory {
    private JMeterFactory() {
        try {
            setupJMeter();
        } catch (IOException ioe) {
            throw new RuntimeException("Error initializing JMeterFactory!", ioe);
        }
    }
    
    public static JMeterFactory getInstance() {
        return SingletonHolder.INSTANCE;
    }
    
    public StandardJMeterEngine getStandardEngine() {
        return new StandardJMeterEngine();
    }
    
    protected void setupJMeter() throws IOException {
        final File jmeterFolder = new File("./target/.jmeter");
        if (!jmeterFolder.exists()) {
            jmeterFolder.mkdir();
        }
        
        final File jmeterBinFolder = new File(jmeterFolder, "bin");
        if (!jmeterBinFolder.exists()) {
            jmeterBinFolder.mkdir();
        }
        
        materializeConfig(jmeterBinFolder, "jmeter.properties");
        materializeConfig(jmeterBinFolder, "saveservice.properties");
        materializeConfig(jmeterBinFolder, "upgrade.properties");
        materializeConfig(jmeterBinFolder, "system.properties");
        materializeConfig(jmeterBinFolder, "user.properties");
        
        JMeterUtils.loadJMeterProperties(jmeterBinFolder.getAbsolutePath() + "/jmeter.properties");
        JMeterUtils.setJMeterHome(jmeterFolder.getAbsolutePath());
        JMeterUtils.initLocale();
        
        // you can comment the following line out to see extra log messages of i.e. DEBUG level
        JMeterUtils.initLogging();
        SaveService.loadProperties();
    }
    
    private void materializeConfig(final File jmeterBinFolder, final String confName) throws IOException {
        final File outFile = new File(jmeterBinFolder, confName);
        if (!outFile.exists()) {
            outFile.createNewFile();
            ReadableByteChannel inChannel = null;
            FileChannel outChannel = null;
            try {
                outChannel = new RandomAccessFile(outFile, "rw").getChannel();
                inChannel = Channels.newChannel(this.getClass().getClassLoader().getResourceAsStream(confName));
                outChannel.transferFrom(inChannel, 0L, Long.MAX_VALUE);
            } finally {
                if (outChannel != null) {
                    outChannel.close();
                }
                if (inChannel != null) {
                    inChannel.close();
                }
            }
        }
    }
    
    private static class SingletonHolder {
        private static final JMeterFactory INSTANCE = new JMeterFactory();
    }
}
