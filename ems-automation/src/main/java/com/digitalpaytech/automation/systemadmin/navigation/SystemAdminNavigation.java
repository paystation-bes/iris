package com.digitalpaytech.automation.systemadmin.navigation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SystemAdminNavigation extends com.digitalpaytech.automation.customer.navigation.CustomerNavigation {
    private static final int WAITTIMESECONDS = 20;
    
    //Searches for specified customer in the Customer Search bar
    //TO FIX: Method assumes that customer exists... will fix later
    public final void systemAdminCustomerSearch(final WebDriver driver, final String customerName) {
        final WebDriverWait webDriverWait = new WebDriverWait(driver, WAITTIMESECONDS);
        WebElement element;
        
        webDriverWait.until(ExpectedConditions.elementToBeClickable(By.id("search")));
        element = driver.findElement(By.id("search"));
        element.sendKeys(customerName);
        webDriverWait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("span.info")));
        driver.findElement(By.cssSelector("span.info")).click();
        
        webDriverWait.until(ExpectedConditions.elementToBeClickable(By.id("btnGo")));
        element = driver.findElement(By.id("btnGo"));
        element.click();
    }
}
