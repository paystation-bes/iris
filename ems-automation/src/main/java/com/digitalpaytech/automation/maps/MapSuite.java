package com.digitalpaytech.automation.maps;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.digitalpaytech.automation.AutomationGlobals;
import com.digitalpaytech.automation.customer.navigation.CustomerNavigation;
import com.digitalpaytech.automation.dto.AutomationMapPayStation;

public class MapSuite extends AutomationGlobals {
    protected static final Logger LOGGER = Logger.getLogger(MapSuite.class);
    private final List<AutomationMapPayStation> payStationList = new ArrayList<AutomationMapPayStation>();
    private final List<Point> previousPayStationPath = new ArrayList<Point>();
    public WebDriver mapDriver;
    // "mapTab" value is the tab name where the map is located.
    
    private String mapTab = "";
    
    private CustomerNavigation customerNavigation = new CustomerNavigation(driver);
    
    public MapSuite(final String mapTab, WebDriver driver) {
        this.mapTab = mapTab;
        this.driver = driver;
    }
    
    public final List<AutomationMapPayStation> getPayStationList() {
        return this.payStationList;
    }
    
    /*
     * This is a basic implementation of going through a tree.
     * Goes into every cluster and records the pay station(s) and the path to get to those pay stations.
     */
    public final void setPayStationMapStructure() {
        final List<WebElement> payStations = new ArrayList<WebElement>();
        final List<WebElement> clusters = new ArrayList<WebElement>();
        
        // Get clusters first because it makes more sense for logging purposes.
        clusters.addAll(this.getClusters());
        LOGGER.info("The number of clusters on this level of the map is: " + clusters.size());
        for (int i = 0; i < clusters.size(); i++) {
            clusters.clear();
            // Adding following line because recursion might have erased it from memory
            clusters.addAll(getClusters());
            
            // TODO: Possibly add check here to see if the div element has already been checked.
            // This is needed because elements may not always appear in the same order.
            LOGGER.info("Adding path point: " + clusters.get(i).getLocation());
            
            this.previousPayStationPath.add(clusters.get(i).getLocation());
            clusters.get(i).click();
            /*
             * Sleep needs to occur after every page change or else you will get "element not in DOM" message.
             * Without the sleep, the next lines of code will look at the 'older' elements and not the current elements.
             */
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            setPayStationMapStructure();
        }
        
        // Get pay stations after clusters
        payStations.addAll(this.getPayStations());
        LOGGER.info("The number of pay stations on this level of the map is: " + payStations.size());
        for (int i = 0; i < payStations.size(); i++) {
            savePayStationData(payStations.get(i));
        }
        
        // Goes to the previous cluster zoom level
        LOGGER.info("All placed pay stations retrieved for path: " + this.previousPayStationPath);
        if (this.previousPayStationPath.size() > 0) {
            LOGGER.info("Removing path point: " + this.previousPayStationPath.get(this.previousPayStationPath.size() - 1));
            this.previousPayStationPath.remove(this.previousPayStationPath.size() - 1);
            getToPreviousClusterLevel();
        }
    }
    
    /*
     * Goes to, clicks on, and gets specific pay station data
     */
    public final void findSpecificPayStationData(final String payStationSerial) {
        AutomationMapPayStation payStation = new AutomationMapPayStation();
        
        if (!isPayStationAlreadyAdded(payStationSerial)) {
            LOGGER.info("Pay station does not exist in map. Possible incorrect serial number used.");
            return;
        }
        
        LOGGER.info("Going to pay station " + payStationSerial + " in the map.");
        
        // Go to correct pay station zoom level/location
        for (int i = 0; i < this.payStationList.size(); i++) {
            if (payStationSerial.equals(this.payStationList.get(i).getSerialNumber())) {
                this.previousPayStationPath.clear();
                this.previousPayStationPath.addAll(this.payStationList.get(i).getPayStationPath());
                getToSpecificPayStation();
                payStation = this.payStationList.get(i);
                break;
            }
        }
        LOGGER.info("Pay station " + payStationSerial + " data has been located, clicked on, and retrieved.");
        return;
    }
    
    /*
     * Saves/sets the basic pay station data based on the HTML attributes
     */
    private void savePayStationData(final WebElement payStationElement) {
        final AutomationMapPayStation payStation = new AutomationMapPayStation();
        if (!isPayStationAlreadyAdded(payStationElement.getAttribute("alt"))) {
            payStation.setName(payStationElement.getAttribute("title"));
            payStation.setSerialNumber(payStationElement.getAttribute("alt"));
            payStation.setIsPlaced(true);
            payStation.setPayStationPath(this.previousPayStationPath);
            payStation.addPayStationPoint(payStationElement.getLocation());
            this.payStationList.add(payStation);
            LOGGER.info(payStation.getName() + "(" + payStation.getSerialNumber() + ") has been checked.");
        }
    }
    
    /*
     * Checks to see if the pay station is already added to the list of pay stations.
     * Returns TRUE if pay station already exists in list
     * Returns FALSE if pay station does not exist in list
     */
    private Boolean isPayStationAlreadyAdded(final String serialNumber) {
        Boolean isPayStationAdded = false;
        for (int i = 0; i < this.payStationList.size(); i++) {
            if (serialNumber.equals(this.payStationList.get(i).getSerialNumber())) {
                isPayStationAdded = true;
                break;
            }
        }
        return isPayStationAdded;
    }
    
    /*
     * Continues refreshing the page until the map is fully loaded.
     * May be better to use the page URL instead of the "mapId" value.
     */
    public final void fullyLoadMap(final String leftHandMenuOption, final String mapId) {
        do {
            // Go to the tab with the map on the specified page.
            customerNavigation.navigateLeftHandMenu(leftHandMenuOption);
            customerNavigation.navigatePageTabs(this.mapTab);
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
            this.element = driver.findElement(By.id(mapId));
            Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        } while (!this.isMapFullyLoaded(super.element));
    }
    
    /*
     * Checks to see if map is fully loaded (if all the tiles are visible or not).
     */
    public final Boolean isMapFullyLoaded(final WebElement map) {
        Boolean isMapFullyLoaded = true;
        
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        final List<WebElement> mapTiles = map.findElements(By.xpath("//div[@class='leaflet-tile-container leaflet-zoom-animated']/img"));
        if (mapTiles.size() == 0) {
            LOGGER.info("No tiles found in the map.");
            return false;
        }
        for (int i = 0; i < mapTiles.size(); i++) {
            if (!"leaflet-tile leaflet-tile-loaded".equals(mapTiles.get(i).getAttribute("class"))) {
                LOGGER.info("Tile not loaded in map.");
                isMapFullyLoaded = false;
                break;
            }
        }
        return isMapFullyLoaded;
    }
    
    /*
     * Gets all the clusters on the current zoom level
     */
    private List<WebElement> getClusters() {
        return getElements("//div[@class='leaflet-marker-pane']/div");
    }
    
    /*
     * Gets all the pay stations on the current zoom level
     */
    private List<WebElement> getPayStations() {
        //        /* 
        //         * Logic to remove "bad" images.
        //         * Originally grabbed the pay stations based on the image naming conventions.
        //         * However, it now works by using regular XPath.  Leaving following commented out if XPath stops working.
        //         * Possible reason why didn't work: Not enough wait time after clicks.
        //         * Gets all images on the page
        //         */
        //        super.setWebElementsByXpath("//div/img");
        //        for (int i = 0; i < super.elementList.size(); i++) {
        //            imageSourceStr = super.elementList.get(i).getAttribute("src");
        //            // Checks if string contains "mapIcn", but not "Shdw"
        //            // Also checks if the element is displayed (i.e. visible) on the map
        //            if (imageSourceStr.contains("mapIcn") && !imageSourceStr.contains("Shdw") && super.elementList.get(i).isDisplayed()) {
        //                payStations.add(super.elementList.get(i));
        //            }
        //        }
        return getElements("//div[@class='leaflet-marker-pane']/img");
    }
    
    /*
     * Gets the displayed elements based on XPath
     */
    private List<WebElement> getElements(final String xPath) {
        final List<WebElement> elements = new ArrayList<WebElement>();
        
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        
        super.elementList = this.driver.findElements(By.xpath(xPath));
        for (int i = 0; i < super.elementList.size(); i++) {
            // Adds the cluster to the list only if it is visible.
            if (super.elementList.get(i).isDisplayed()) {
                elements.add(super.elementList.get(i));
            }
        }
        super.elementList.clear();
        return elements;
    }
    
    /*
     * Performs the logic to move the map to the previous zoom level
     */
    private void getToPreviousClusterLevel() {
        Point clusterLocation;
        final List<WebElement> clusters = new ArrayList<WebElement>();
        
        // Refreshes the page and navigates back to the appropriate tab
        LOGGER.info("Refreshing page and going back to the previous level...");
        driver.navigate().refresh();
        customerNavigation.navigatePageTabs(this.mapTab);
        Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
        
        // Goes to the previous zoom level by clicking on the saved path points
        for (int i = 0; i < this.previousPayStationPath.size(); i++) {
            // Gets the clusters on the current zoom level
            clusters.addAll(getClusters());
            clusterLocation = this.previousPayStationPath.get(i);
            LOGGER.info("Previous cluster location is: " + clusterLocation);
            
            for (int j = 0; j < clusters.size(); j++) {
                if (clusterLocation.equals(clusters.get(j).getLocation())) {
                    LOGGER.info("Clicking on location: " + clusterLocation);
                    // Clicks the cluster point recorded in the path
                    clusters.get(j).click();
                    /*
                     * Sleep needs to occur after every page change or else you will get "element not in DOM" message.
                     * Without the sleep, the next lines of code will look at the 'older' elements and not the current elements.
                     */
                    Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
                    break;
                }
            }
            clusters.clear();
        }
    }
    
    /*
     * Performs the logic to move the map to the specified pay station
     */
    private void getToSpecificPayStation() {
        final List<WebElement> payStations = new ArrayList<WebElement>();
        Point payStationPoint;
        
        payStationPoint = this.previousPayStationPath.get(this.previousPayStationPath.size() - 1);
        this.previousPayStationPath.remove(payStationPoint);
        getToPreviousClusterLevel();
        
        // Clicks the specified pay station
        payStations.addAll(getPayStations());
        for (int i = 0; i < payStations.size(); i++) {
            if (payStationPoint.equals(payStations.get(i).getLocation())) {
                LOGGER.info("Clicking on location: " + payStationPoint);
                payStations.get(i).click();
                Sleeper.sleepTightInSeconds(SLEEP_TIME_ONE_SECOND);
                break;
            }
        }
    }
}
