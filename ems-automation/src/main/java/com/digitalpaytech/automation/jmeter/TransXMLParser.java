package com.digitalpaytech.automation.jmeter;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.digitalpaytech.automation.transactionsuite.TransactionDetails;

public class TransXMLParser {
    private TransactionDetails td;
    
    public final TransactionDetails parse(final String fileName) {
        
        this.td = new TransactionDetails();
        final DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        try {
            docBuilder = docBuilderFactory.newDocumentBuilder();
            
            Document document;
            
            document = docBuilder.parse(this.getClass().getClassLoader().getResourceAsStream(fileName));
            document.getDocumentElement().normalize();
            
            NodeList nodeList = document.getElementsByTagName("Transaction");
            
            if (nodeList == null) {
                nodeList = document.getElementsByTagName("AuthorizeCardRequest");
            }
            
            parsePaystationCommAddress(document);
            parseLotNumber(document);
            parseAddTimeNum(document);
            parseType(document);
            parsePTimePur(document);
            parseOriginalAmount(document);
            parseChargedAmount(document);
            parseCashPaid(document);
            parseCardPaid(document);
            parseCoinCol(document);
            parseBillCol(document);
            parseCardAuthorizationId(document);
            parseEmsPreAuthId(document);
            setTime();
        } catch (SAXException | IOException | ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return this.td;
        
    }
    
    public final void parsePaystationCommAddress(final Document document) {
        final Node psCommAddress = document.getElementsByTagName("PaystationCommAddress").item(0);
        if (psCommAddress != null) {
            this.td.setPaystationCommAddress(psCommAddress.getTextContent());
        }
    }
    
    public final void parseLotNumber(final Document document) {
        final Node lotNumber = document.getElementsByTagName("LotNumber").item(0);
        if (lotNumber != null) {
            this.td.setLotNumber(lotNumber.getTextContent());
        }
    }
    
    public final void parseAddTimeNum(final Document document) {
        final Node addTimeNum = document.getElementsByTagName("AddTimeNum").item(0);
        if (addTimeNum != null) {
            this.td.setAddTimeNum(addTimeNum.getTextContent());
        }
    }
    
    public final void parseType(final Document document) {
        final Node type = document.getElementsByTagName("Type").item(0);
        if (type != null) {
            this.td.setType(type.getTextContent());
        }
    }
    
    public final void parsePTimePur(final Document document) {
        final Node pTimePur = document.getElementsByTagName("ParkingTimePurchased").item(0);
        if (pTimePur != null) {
            this.td.setParkingTimePurchased(pTimePur.getTextContent());
        }
    }
    
    public final void parseOriginalAmount(final Document document) {
        final Node originalAmount = document.getElementsByTagName("OriginalAmount").item(0);
        if (originalAmount != null) {
            this.td.setOriginalAmount(originalAmount.getTextContent());
        }
    }
    
    public final void parseChargedAmount(final Document document) {
        final Node chargedAmount = document.getElementsByTagName("ChargedAmount").item(0);
        if (chargedAmount != null) {
            this.td.setChargedAmount(chargedAmount.getTextContent());
        }
    }
    
    public final void parseCashPaid(final Document document) {
        final Node cashPaid = document.getElementsByTagName("CashPaid").item(0);
        if (cashPaid != null) {
            this.td.setCashPaid(cashPaid.getTextContent());
        }
    }
    
    public final void parseCardPaid(final Document document) {
        final Node cardPaid = document.getElementsByTagName("CardPaid").item(0);
        if (cardPaid != null) {
            this.td.setCardPaid(cardPaid.getTextContent());
        }
    }
    
    public final void parseCoinCol(final Document document) {
        final Node coinCol = document.getElementsByTagName("CoinCol").item(0);
        if (coinCol != null) {
            this.td.setCoinCol(coinCol.getTextContent());
        }
    }
    
    public final void parseBillCol(final Document document) {
        final Node billCol = document.getElementsByTagName("BillCol").item(0);
        if (billCol != null) {
            this.td.setBillCol(billCol.getTextContent());
        }
    }
    
    public final void parseCardAuthorizationId(final Document document) {
        final Node cardAuthorizationId = document.getElementsByTagName("CardAuthorizationId").item(0);
        if (cardAuthorizationId != null) {
            this.td.setCardAuthorizationId(cardAuthorizationId.getTextContent());
        }
    }
    
    public final void parseEmsPreAuthId(final Document document) {
        final Node emsPreAuthId = document.getElementsByTagName("EmsPreAuthId").item(0);
        if (emsPreAuthId != null) {
            this.td.setEmsPreAuthId(emsPreAuthId.getTextContent());
        }
    }
    
    public final void setTime() {
        
        this.td.formatPurchaseDate();
        this.td.formatExpiryDate();
        this.td.setPaymentType(this.td.getCashPaid(), this.td.getCardPaid());
        this.td.setPaymentType(this.td.getCashPaid(), this.td.getCardPaid());
        
    }
    
}
