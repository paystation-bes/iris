package com.digitalpaytech.automation.jmeter;

import java.io.IOException;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.digitalpaytech.automation.transactionsuite.TransactionDetails;

public class SOAPRequest {
    private static final String FOOTER = "</value></param></params></methodCall>";
    private final TransactionDetails transactionData;
    private String header = "";
    private Document document;
    
    public SOAPRequest(final TransactionDetails transactionData) {
        
        this.transactionData = transactionData;
        
    }
    
    public final String buildXML(final String file) {
        Node trans;
        final String xmlPath = new String(file);
        
        this.header += "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><methodCall>";
        this.header += "<methodName>XMLRPCRequestHandler.processXML</methodName><params><param><value>";
        
        final DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        
        try {
            docBuilder = docBuilderFactory.newDocumentBuilder();
            this.document = docBuilder.parse(this.getClass().getClassLoader().getResourceAsStream(xmlPath));
        } catch (SAXException | IOException | ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        this.document.getDocumentElement().normalize();
        
        trans = this.document.getElementsByTagName("Transaction").item(0);
        if (trans == null) {
            trans = this.document.getElementsByTagName("AuthorizeCardRequest").item(0);
        }
        this.determineType(trans);
        this.getNodes();
        return this.buidXML();
    }
    
    public final void determineType(final Node transaction) {
        
        final NamedNodeMap attr = transaction.getAttributes();
        
        final Node msgNum = attr.getNamedItem("MessageNumber");
        if (msgNum != null) {
            msgNum.setTextContent(this.transactionData.getMessageNumber());
        }
    }
    
    public final void getNodes() {
        
        final Node number = this.document.getElementsByTagName("Number").item(0);
        if (number != null) {
            number.setTextContent(this.transactionData.getTicketNumber());
        }
        
        final Node licence = this.document.getElementsByTagName("LicensePlateNo").item(0);
        if (licence != null) {
            licence.setTextContent(this.transactionData.getLicensePlateNo());
        }
        
        final Node purchasedDate = this.document.getElementsByTagName("PurchasedDate").item(0);
        if (purchasedDate != null) {
            purchasedDate.setTextContent(this.transactionData.getPurchasedDate());
        }
        
        final Node expiryDate = this.document.getElementsByTagName("ExpiryDate").item(0);
        if (expiryDate != null) {
            expiryDate.setTextContent(this.transactionData.getExpiryDate());
        }
        
        final Node stallNumber = this.document.getElementsByTagName("StallNumber").item(0);
        if (stallNumber != null) {
            stallNumber.setTextContent(this.transactionData.getStallNumber());
        }
        
        final Node cardAuthorizationId = this.document.getElementsByTagName("CardAuthorizationId").item(0);
        if (cardAuthorizationId != null) {
            cardAuthorizationId.setTextContent(this.transactionData.getCardAuthorizationId());
        }
        
        final Node emsPreAuthId = this.document.getElementsByTagName("EmsPreAuthId").item(0);
        if (emsPreAuthId != null) {
            emsPreAuthId.setTextContent(this.transactionData.getEmsPreAuthId());
        }
    }
    
    public final String buidXML() {
        String request = "";
        final DOMSource source = new DOMSource(this.document);
        final StringWriter writer = new StringWriter();
        final StreamResult result = new StreamResult(writer);
        final TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = tf.newTransformer();
            
            transformer.transform(source, result);
        } catch (TransformerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        writer.flush();
        request = writer.toString();
        
        request = this.header + request + SOAPRequest.FOOTER;
        
        System.out.println(request);
        return request;
        
    }
}
