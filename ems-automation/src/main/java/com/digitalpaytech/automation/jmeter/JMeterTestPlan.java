package com.digitalpaytech.automation.jmeter;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.config.gui.ArgumentsPanel;
import org.apache.jmeter.control.LoopController;
import org.apache.jmeter.control.gui.LoopControlPanel;
import org.apache.jmeter.control.gui.TestPlanGui;
import org.apache.jmeter.engine.StandardJMeterEngine;
import org.apache.jmeter.protocol.http.sampler.SoapSampler;
import org.apache.jmeter.reporters.ResultCollector;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jmeter.testelement.TestPlan;
import org.apache.jmeter.threads.SetupThreadGroup;
import org.apache.jmeter.threads.gui.ThreadGroupGui;
import org.apache.jorphan.collections.HashTree;

import com.digitalpaytech.automation.JMeterFactory;

public class JMeterTestPlan {
    private static final String CHARSET = "ISO-8859-1";
    private String result;
    private int responseCode;
    private final String xmlData;
    
    public JMeterTestPlan(final String xmlData) {
        this.xmlData = xmlData;
        
    }
    
    public final void createTestPlan() {
        final StandardJMeterEngine jmeter = JMeterFactory.getInstance().getStandardEngine();
        
        // JMeter Test Plan
        final HashTree testPlanTree = new HashTree();
        
        // SOAP Sampler
        final SoapSampler soapSampler = new SoapSampler();
        soapSampler.setURLData("https://dev.digitalpaytech.com:443/XMLRPC_PS2");
        soapSampler.setMethod("POST");
        soapSampler.setPostBodyRaw(true);
        soapSampler.setXmlData(this.xmlData);
        soapSampler.setSendSOAPAction(true);
        
        // Loop Controller
        final LoopController loopController = new LoopController();
        loopController.setLoops(1);
        loopController.setFirst(true);
        loopController.setProperty(TestElement.TEST_CLASS, LoopController.class.getName());
        loopController.setProperty(TestElement.GUI_CLASS, LoopControlPanel.class.getName());
        loopController.initialize();
        
        // Thread Group
        final SetupThreadGroup threadGroup = new SetupThreadGroup();
        threadGroup.setName("SOAPThreadGroup");
        threadGroup.setNumThreads(1);
        threadGroup.setRampUp(1);
        threadGroup.setEnabled(true);
        threadGroup.setSamplerController(loopController);
        threadGroup.setProperty(TestElement.TEST_CLASS, ThreadGroup.class.getName());
        threadGroup.setProperty(TestElement.GUI_CLASS, ThreadGroupGui.class.getName());
        
        final ResultCollector resultsTree = new ResultCollector();
        soapSampler.addTestElement(resultsTree);
        
        // Test Plan
        final TestPlan testPlan = new TestPlan("Create JMeter Script From Java Code");
        testPlan.setProperty(TestElement.TEST_CLASS, TestPlan.class.getName());
        testPlan.setProperty(TestElement.GUI_CLASS, TestPlanGui.class.getName());
        testPlan.setUserDefinedVariables((Arguments) new ArgumentsPanel().createTestElement());
        
        // Construct Test Plan from previously initialized elements
        testPlanTree.add(testPlan);
        final HashTree threadGroupHashTree = testPlanTree.add(testPlan, threadGroup);
        threadGroupHashTree.add(soapSampler);
        
        final SampleResult soapResult = soapSampler.sample();
        
        jmeter.configure(testPlanTree);
        jmeter.run();
        
        setResponse(soapResult);
    }
    
    public final void setResponse(final SampleResult soapResult) {
        final String header = "<\\?xml version=\"1.0\" encoding=\"ISO-8859-1\"\\?><methodResponse><params><param><value><base64>";
        final String footer = "</base64></value></param></params></methodResponse>";
        
        this.responseCode = Integer.valueOf(soapResult.getResponseCode());
        final String removeHeader = soapResult.getResponseDataAsString().replaceAll(header, "");
        final String removeFooter = removeHeader.replaceAll(footer, "");
        
        final byte[] decodeResp = Base64.decodeBase64(removeFooter.getBytes(Charset.forName(CHARSET)));
        
        try {
            this.result = new String(decodeResp, CHARSET);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
    
    public final int getResponseCode() {
        return this.responseCode;
    }
    
    public final String getResult() {
        return this.result;
    }
    
}
