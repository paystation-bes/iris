package com.digitalpaytech.automation.jmeter;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class PreAuthResultsParser {
    private static final String EMS_PRE_AUTH_ID = "EmsPreAuthId";
    private static final String CARD_AUTH_ID = "AuthorizationId";
    private final String result;
    private String emsPreAuthId;
    private String cardAuthId;

    public PreAuthResultsParser(final String result) {
        this.result = result;
    }

    public final void parse() {
        try {
            final DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();

            final InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(this.result));

            final Document document = docBuilder.parse(src);

            document.getDocumentElement().normalize();

            this.emsPreAuthId = document.getElementsByTagName(EMS_PRE_AUTH_ID).item(0).getTextContent();
            if (this.emsPreAuthId.equals("0")) {
                this.emsPreAuthId = "1";
            }
            this.cardAuthId = document.getElementsByTagName(CARD_AUTH_ID).item(0).getTextContent();

        } catch (SAXException | IOException | ParserConfigurationException e) {
            e.printStackTrace();

        }

    }

    public final String getEmsPreAuthId() {
        return this.emsPreAuthId;
    }

    public final String getCardAuthId() {
        return this.cardAuthId;
    }
}
