package com.digitalpaytech.automation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.digitalpaytech.automation.dto.AutomationPayStation;
import com.digitalpaytech.automation.dto.AutomationUser;

public class XmlData {
    private static final Logger LOGGER = Logger.getLogger(XmlData.class);
    
    private List<String> browsers = new ArrayList<String>();
    private String url = "http://172.16.1.63:8080/";
    private List<AutomationUser> userList = new ArrayList<AutomationUser>();
    private List<AutomationUser> collectionUserList = new ArrayList<AutomationUser>();
    private List<AutomationPayStation> payStationList = new ArrayList<AutomationPayStation>();
    private Document document;
    
    public XmlData(final String fileName) {
        try {
            final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            final DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            
            this.document = documentBuilder.parse(this.getClass().getClassLoader().getResourceAsStream(fileName));
            this.document.getDocumentElement().normalize();
            
            setGlobalValues();
        } catch (SAXException e) {
            LOGGER.error(e.getMessage());
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        } catch (ParserConfigurationException e) {
            LOGGER.error(e.getMessage());
        }
    }
    
    private void setGlobalValues() {
        this.setBrowsers();
        this.setUrl();
        this.setUserList();
        this.setCollectionUserList();
        this.setPayStationList();
    }
    
    // Gets the browsers specified in the XML
    public final List<String> getBrowsers() {
        return this.browsers;
    }
    
    // Gets the URL specified in the XML
    public final String getUrl() {
        return this.url;
    }
    
    // Gets the list of users from the XML
    public final List<AutomationUser> getUserList() {
        return this.userList;
    }
    
    // Gets the list of Collection users from the XML
    public final List<AutomationUser> getCollectionUserList() {
        return this.collectionUserList;
    }
    
    // Gets the list of pay stations from the XML
    public final List<AutomationPayStation> getPayStationList() {
        return this.payStationList;
    }
    
    // Sets the "browsers" property
    private void setBrowsers() {
        NodeList browsersNodeList;
        
        browsersNodeList = this.document.getElementsByTagName("Browser");
        for (int i = 0; i < browsersNodeList.getLength(); i++) {
            if (!browsersNodeList.item(i).getTextContent().isEmpty()) {
                this.browsers.add(browsersNodeList.item(i).getTextContent());
                LOGGER.info("Browser includes: " + browsersNodeList.item(i).getTextContent());
            } else {
                this.browsers.add("N/A");
                LOGGER.info("Browser XML node was empty.");
            }
        }
    }
    
    // Sets the "url" property
    private void setUrl() {
        NodeList urlNodeList;
        
        urlNodeList = this.document.getElementsByTagName("URL");
        if (urlNodeList.getLength() > 0) {
            this.url = urlNodeList.item(0).getTextContent();
            LOGGER.info("URL is: " + this.url);
        }
    }
    
    // Sets the "collectionUserList" property
    private void setCollectionUserList() {
        this.collectionUserList = this.setUserListAll("//CollectionUsers/User");
    }
    
    // Sets the "userList" property
    private void setUserList() {
        this.userList = this.setUserListAll("//Users/User");
    }
    
    /*
     * Generic method to set the user lists for specified groups of users
     * (e.g. "Users", "CollectionUsers").
     */
    private List<AutomationUser> setUserListAll(final String userGroup) {
        final List<AutomationUser> genericUserList = new ArrayList<AutomationUser>();
        NodeList usersNodeList;
        // Gets Users to test from XML
        final XPath xPath = XPathFactory.newInstance().newXPath();
        try {
            // Check xPath for nodes.
            usersNodeList = (NodeList) xPath.compile(userGroup).evaluate(this.document, XPathConstants.NODESET);
            if (usersNodeList.getLength() > 0) {
                for (int i = 0; i < usersNodeList.getLength(); i++) {
                    genericUserList.add(i, this.getUser(usersNodeList.item(i)));
                }
            }
        } catch (XPathExpressionException e) {
            LOGGER.info(e.getMessage());
        }
        return genericUserList;
    }
    
    /*
     * Gets and returns a single user from the XML 
     */
    private AutomationUser getUser(final Node userNode) {
        final AutomationUser user = new AutomationUser();
        NodeList userParametersNodeList;
        
        userParametersNodeList = userNode.getChildNodes();
        
        for (int j = 0; j < userParametersNodeList.getLength(); j++) {
            final String userNodeName = userParametersNodeList.item(j).getNodeName();
            final String userNodeValue = userParametersNodeList.item(j).getTextContent();
            switch (userNodeName) {
                case "UserName":
                    user.setUserName(userNodeValue);
                    LOGGER.info("UserName is: " + userNodeValue);
                    break;
                case "CustomerName":
                    user.setCustomerName(userNodeValue);
                    LOGGER.info("CustomerName is: " + userNodeValue);
                    break;
                case "UserType":
                    user.setUserType(userNodeValue);
                    LOGGER.info("UserType is: " + userNodeValue);
                    break;
                case "Password":
                    user.setPassword(userNodeValue);
                    LOGGER.info("Password is: " + userNodeValue);
                    break;
                case "FirstName":
                    user.setFirstName(userNodeValue);
                    LOGGER.info("First Name is: " + userNodeValue);
                    break;
                case "LastName":
                    user.setLastName(userNodeValue);
                    LOGGER.info("Last Name is: " + userNodeValue);
                    break;
                default:
                    break;
            }
        }
        return user;
    }
    
    // Sets the "payStationList" property
    private void setPayStationList() {
        NodeList payStationNodeList;
        NodeList payStationParametersNodeList;
        
        // Gets Users to test from XML
        payStationNodeList = this.document.getElementsByTagName("PayStation");
        for (int i = 0; i < payStationNodeList.getLength(); i++) {
            final AutomationPayStation payStation = new AutomationPayStation();
            payStationParametersNodeList = payStationNodeList.item(i).getChildNodes();
            
            for (int j = 0; j < payStationParametersNodeList.getLength(); j++) {
                final String payStationNodeName = payStationParametersNodeList.item(j).getNodeName();
                final String payStationNodeValue = payStationParametersNodeList.item(j).getTextContent();
                switch (payStationNodeName) {
                    case "Name":
                        payStation.setName(payStationNodeValue);
                        LOGGER.info("Pay Station Name is: " + payStationNodeValue);
                        break;
                    case "Serial":
                        payStation.setSerialNumber(payStationNodeValue);
                        LOGGER.info("Pay Station Serial is: " + payStationNodeValue);
                        break;
                    case "CustomerName":
                        payStation.setCustomerName(payStationNodeValue);
                        LOGGER.info("Pay Station belongs to customer: " + payStationNodeValue);
                        break;
                    default:
                        break;
                }
            }
            this.payStationList.add(i, payStation);
        }
    }
    
    // Gets a user based on their UserType from the XML
    public final AutomationUser getUserByUserType(final String userType) {
        AutomationUser user = new AutomationUser();
        
        for (int i = 0; i < this.userList.size(); i++) {
            user = this.userList.get(i);
            if (this.userList.get(i).getUserType().equalsIgnoreCase(userType)) {
                user = this.userList.get(i);
                break;
            }
        }
        return user;
    }
    
    /*
     * Gets the list of pay stations from the XML that are associated with the customer.
     */
    public final List<AutomationPayStation> getPayStationByCustomerName(final String customerName) {
        final List<AutomationPayStation> payStations = new ArrayList<AutomationPayStation>();
        payStations.addAll(this.getPayStationList());
        // Removes all pay stations not associated with the specified customer from the list.
        for (int i = 0; i < payStationList.size(); i++) {
            if (!customerName.equals(payStationList.get(i).getCustomerName())) {
                payStations.remove(i);
            }
        }
        return payStations;
    }
    
    // Checks if specified browser is in XML
    public final Boolean isBrowserSpecifiedInXML(final String browser) {
        Boolean isBrowserSpecified = false;
        for (int i = 0; i < this.getBrowsers().size(); i++) {
            if (browser.equalsIgnoreCase(this.getBrowsers().get(i).toLowerCase())) {
                isBrowserSpecified = true;
            }
        }
        return isBrowserSpecified;
    }
}
