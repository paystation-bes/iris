package com.digitalpaytech.automation.dto;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Point;

/*
 * This class is an extension of the "AutomationPayStation" class and includes a "payStationPath"
 * variable used to track the path through the clusters to get to the pay station(s).
 */
public class AutomationMapPayStation extends AutomationPayStation {
    private List<Point> payStationPath = new ArrayList<Point>();
    
    /*
     * Adds the location (top left corner) of the cluster element.
     */
    public final void addPayStationPoint(final Point payStationPoint) {
        this.payStationPath.add(payStationPoint);
    }
    
    /*
     * Removes the specified cluster location (point) from the path.
     */
    public final void removePayStationPoint(final int index) {
        this.payStationPath.remove(index);
    }
    
    /*
     * Returns the recorded path (the clusters that were clicked) in order to get to the pay station.
     */
    public final List<Point> getPayStationPath() {
        return this.payStationPath;
    }
    
    /*
     * Sets the pay station path value.
     */
    public final void setPayStationPath(final List<Point> payStationPath) {
        this.payStationPath.clear();
        this.payStationPath.addAll(payStationPath);
    }
}
