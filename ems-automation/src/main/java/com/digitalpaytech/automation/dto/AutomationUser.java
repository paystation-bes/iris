package com.digitalpaytech.automation.dto;

public class AutomationUser {
    private String userName = "";
    private String customerName = "";
    private String userType = "";
    private String password = "";
    private String firstName = "";
    private String lastName = "";
    
    public final void setUserName(final String userName) {
        this.userName = userName;
    }
    
    public final void setCustomerName(final String customerName) {
        this.customerName = customerName;
    }
    
    public final void setUserType(final String userType) {
        this.userType = userType;
    }
    
    public final void setPassword(final String password) {
        this.password = password;
    }
    
    public final void setFirstName(final String firstName) {
        this.firstName = firstName;
    }
    
    public final void setLastName(final String lastName) {
        this.lastName = lastName;
    }
    
    public final String getUserName() {
        return this.userName;
    }
    
    public final String getCustomerName() {
        return this.customerName;
    }
    
    public final String getUserType() {
        return this.userType;
    }
    
    public final String getPassword() {
        return this.password;
    }
    
    public final String getFirstName() {
        return this.firstName;
    }
    
    public final String getLastName() {
        return this.lastName;
    }
}
