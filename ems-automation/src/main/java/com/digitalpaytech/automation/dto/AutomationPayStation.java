package com.digitalpaytech.automation.dto;

import java.util.ArrayList;
import java.util.List;

public class AutomationPayStation {
    private String serialNumber = "";
    private String name = "";
    private String location = "";
    private List<String> routes = new ArrayList<String>();
    private Boolean isPlaced = false;
    private String lastSeen = "";
    private String customerName = "";
    
    public final String getSerialNumber() {
        return this.serialNumber;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final String getLocation() {
        return this.location;
    }
    
    public final List<String> getRoutes() {
        return this.routes;
    }
    
    public final Boolean getIsPlaced() {
        return this.isPlaced;
    }
    
    public final String getLastSeen() {
        return this.lastSeen;
    }
    
    public final String getCustomerName() {
        return this.customerName;
    }
    
    public final void setSerialNumber(final String serialNumber) {
        this.serialNumber = serialNumber;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    public final void setLocation(final String location) {
        this.location = location;
    }
    
    public final void addRoute(final String route) {
        this.routes.add(route);
    }
    
    public final void setIsPlaced(final Boolean isPlaced) {
        this.isPlaced = isPlaced;
    }
    
    public final void setLastSeen(final String lastSeen) {
        this.lastSeen = lastSeen;
    }
    
    public final void setCustomerName(final String customerName) {
        this.customerName = customerName;
    }
}
