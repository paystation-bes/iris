package com.digitalpaytech.automation;

public interface AutomationConstants {
    public static final int DRIVER_TYPE_CHROME = 1;
    public static final int DRIVER_TYPE_FIREFOX = 2;
    public static final int DRIVER_TYPE_IE = 3;
    
    public static final int WAIT_TIME_TWENTY_SECONDS = 20;
    public static final int SLEEP_TIME_ONE_SECOND = 1;
    
    // Constants for the Collection Report Types
    public static final String COLLECTION_TYPE_ALL = "All";
    public static final String COLLECTION_TYPE_BILL = "Bill";
    public static final String COLLECTION_TYPE_CARD = "Card";
    public static final String COLLECTION_TYPE_COIN = "Coin";
    
    // Constants for the Pay Station Details section
    public static final String CURRENT_STATUS_TAB = "Current Status";
    public static final String RECENT_ACTIVITY_TAB = "Recent Activity ";
    public static final String INFORMATION_TAB = "Information";
    public static final String REPORTS_TAB = "Reports";
    
}
