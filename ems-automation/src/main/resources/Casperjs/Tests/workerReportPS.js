var testhost, utils, help, username, password, start_time, end_time, result;
var test_results = [];
var count = 0;
var x = require('casper').selectXPath;
utils = require("utils");
help = casper.cli.get("help");
username = casper.cli.get("username") || 'RoboUser1@Impark Vancouver';
password = casper.cli.get("password") || 'Password.1';
testhost = casper.cli.get("testhost") || 'staging.digitalpaytech.com';

if (help) {
    casper.echo("Usage: $ casperjs test worker.js [OPTIONS]\n\nOptions:\n\t--username=<username>\tuse supplied username (default:admin@dptpreview)\n\t--password=<password>\tuse supplied password\n\t--testhost=<testhost>\tuse supplied host for tests (default:preview.digitalpaytech.com)\n\t--help\t\t\tdisplays usage information").exit(1);
}

// Handle failure events: capture screenshot
casper.test.on("fail", function(failure) {
    var date = new Date();
    casper.capture('test-failure.' + date.getTime() + '.png');
    result = 'fail';
    post_log();
    return casper.exit(1);
});

// Handle timeout events: capture screenshot
casper.options.onWaitTimeout = function() {
    var date = new Date();
    this.capture('timeout.' + date.getTime() + '.png');
    result = 'fail';
    post_log();
    return this.exit(1);
};

// Log current time before test is run
casper.test.setUp(function() {
    var timestamp = new Date().getTime();
    test_results.push(timestamp);
});

// Log test duration
casper.test.tearDown(function() {
    var b = test_results.pop();
    var e = new Date().getTime();
    test_results.push(e - b);
});

// Save test results to remote resource
function post_log() {
    var url = 'http://127.0.0.1:3000';

    this.open(url, {
        method: 'post',
        data: {
            'start_time': start_time,
            'end_time': end_time,
            'result': result,
        }
    });
}


start_time = new Date().getTime();

casper.test.begin('Testing ' + testhost + ' at: ' + start_time, 40, function suite(test) {

    /*
     * Test Login
     */
    casper.start("https://" + testhost + "/secure/", function() {
        this.viewport(1900, 1080);
        this.test.assertTextExists("Your session is invalid or has expired.", "Cannot access secure without login");
        this.test.assertUrlMatch(/\//, "Redirect to login");
        this.test.assertTitle("Digital Iris - Login.");
        this.test.assertExists("form.login");
        return this.fill("form.login", {
            "j_username": username,
            "j_password": password
        }, true);
    });

    casper.then(function() {
        return this.test.assertUrlMatch(/\/secure\/dashboard\/index.html/);
    });

    /*
     * Test Dashboard
     */
    casper.thenOpen("https://" + testhost + "/customers/dashboard", function() {
        this.test.assertTitle("Digital Iris - Main (Dashboard)");
        return this.test.assertEvalEquals(function() {
            return __utils__.findAll("section.widgetData").length;
        }, 9, "Found 9 widgetData objects");
    });


    /*
     * Navigate to Settings - Global
     */
    casper.thenClick('A[title="Settings"]', function() {
        this.test.assertTitle("Digital Iris - Settings - Global : Settings");
    });

    /*
     * Navigate to Settings - Pay Stations
     */
    casper.thenClick('A[title="Pay Stations"]', function() {
        this.test.assertTitle("Digital Iris - Settings - Pay Stations : Pay Station List");
    });

    /*
     * Click on First PS
     */
    casper.thenClick('ul#paystationList>span>li', function() {
        this.waitUntilVisible('#batteryVoltageWdgt', function() {
            this.test.assertVisible('#batteryVoltageWdgt');
            this.test.assertVisible('#ControllerTemperatureWdgt');
            this.test.assertVisible('#inputCurrentWdgt');
            this.test.assertVisible('#relativeHumidityWdgt');
        });
    });

    /*
     * Navigate to Recent Activity
     */
    casper.thenClick('A[title="Recent Activity"]', function() {
        this.waitUntilVisible('#crntAlertListHeader', function() {
            this.test.assertVisible('#crntAlertListHeader');
            this.test.assertVisible('#alertFilterMenu');
        });
    });
    /*
     * Navigate to PS Reports
     */
    casper.thenClick('#reportsBtn>a', function() {
        this.waitUntilVisible('#collectionReportListHeader', function() {
            this.test.assertVisible('#collectionReportListHeader');
            this.test.assertVisible('#transactionReportBtn');
        });
    });

    /*
     * Navigate to Recent Activity
     */
    casper.thenClick('A[title="Recent Activity"]', function() {
        this.waitUntilVisible('#alertsArea', function() {
            this.test.assertVisible('#alertsArea');
        });
    });

    /*
     * Navigate to Current Status
     */
    casper.thenClick('A[title="Current Status"]', function() {
        this.waitUntilVisible('#batteryVoltageWdgt', function() {
            this.test.assertVisible('#batteryVoltageWdgt');
            this.test.assertVisible('#ControllerTemperatureWdgt');
            this.test.assertVisible('#inputCurrentWdgt');
            this.test.assertVisible('#relativeHumidityWdgt');
        });
    });
    /*
     * Test Maintenance Page
     */
    casper.thenClick('A[title="Maintenance"]', function() {
        this.test.assertTitle("Digital Iris - Maintenance");
        this.test.assertExists("section#Maintenance");
    });

    /*
     * Test Collections Page
     */
    casper.thenClick('A[title="Collections"]', function() {
        this.test.assertTitle("Digital Iris - Collections");
        this.test.assertExists("section#Collections");
    });
    /*
     * Test Reports Page
     */
    casper.thenClick('A[title="Reports"]', function() {
        this.test.assertTitle("Digital Iris - Reports");
        this.test.assertVisible('A[title="Reporting Dashboard"]');
    });

    /*
     * Delete one report
     */
    casper.then(function() {
        if (this.exists('#reportsList>li:nth-of-type(2)')) {
            this.thenClick('#reportsList>li>a', function() {
                this.thenClick('#reportsList>section>section>a.delete', function() {
                    this.waitUntilVisible('#messageResponseAlertBox', function() {
                        this.thenClick(x('//button[contains(., "Ok")]'), function() {
                            this.wait(2000, function() {
                                this.log('Deleted a Report');
                            })
                        });
                    });
                });
            });
        }
    });


    /*
     * Assert Report page loaded correctly
     */
    casper.then(function() {
        this.test.assertTitle("Digital Iris - Reports");
        this.test.assertVisible('A[title="Reporting Dashboard"]');
    });


    /*
     * Test New Report Page
     */
    casper.thenClick('A#btnAddScheduleReport', function() {
        this.test.assertTitle("Digital Iris - Reports");
        this.test.assertVisible('section#reportFormBox');
    });

    /*
     * Test Generate New Report
     */
    casper.thenClick('input#reportTypeID', function() {
        this.test.assertTitle("Digital Iris - Reports");
        this.test.assertVisible("A#btnStep2Select");
        this.sendKeys('input#reportTypeID', "Transaction - All");
    });

    /*
     * Select first report type (Transaction - All)
     */
    casper.thenClick(x('//ul/li'));

    /*
     * Click Next Step button
     */
    casper.thenClick('#btnStep2Select', function() {
        this.waitUntilVisible('#btnStep3Select');
    });

    /*
     * Click 2nd Next Step button
     */
    casper.thenClick('#btnStep3Select', function() {
        this.waitUntilVisible('#scheduling');
        this.test.assertVisible('.linkButtonFtr.textButton.save');
    });

    /*
     * Wait to ensure that save button is ready to be clicked
     */
    casper.then(function() {
        this.waitWhileVisible('.linkButtonFtr.textButton.disabled.save', function() {
            this.click('.linkButtonFtr.textButton.save')
        });
    });

    /*
     * Wait for the Reports page to load again
     */
    casper.then(function() {
        this.waitUntilVisible('#queuedReportsList');
    });

    /*
     * Assert that the Reports page loaded successfully
     */
    casper.then(function() {
        this.test.assertVisible('#queuedReportsList');
        this.test.assertVisible('#opBtn_reloadReport');
    });

    /*
     * Grab ammount of failed reports
     */
    casper.then(function() {
        this.test.assertTitle("Digital Iris - Reports");
        try {
            count = this.getElementsBounds(x('//p[contains(text(),"Failed")]')).length;
        } catch (error) {
            this.log('No Failed Reports', 'info');
        }
    });

    /*
     * Grab log and echo the amount of failed reports
     */
    casper.then(function() {
        this.test.assertTitle("Digital Iris - Reports");
        if (count > 0) {
            this.log('Failed Reports so far for this user: ' + count, 'info');
        }
    });

    /*
     * Test Logout
     */
    casper.thenClick('A[title="Logout"]', function() {
        return this.test.assertUrlMatch(/\//, "Redirected to login page after logout");
    });

    /*
     * Run Tests
     */
    casper.run(function() {
        this.test.done();
        end_time = new Date().getTime();
        result = 'pass';
        post_log();
    });

});