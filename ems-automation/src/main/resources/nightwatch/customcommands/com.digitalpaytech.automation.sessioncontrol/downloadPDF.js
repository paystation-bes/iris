/**
 * @summary Custom Command: Download PDF *NOTE* Doesn't actually test functionality of the 'export' buttons
 * @since 7.3.6
 * @version 1.0
 * @author Mandy F
 */

/**
 * @description Downloads file from the site for the export options
 * @param option - the part of the site you want to export for: Reports
 * @param devURL - the URL/branch you are currently on
 * @param fileName - the name of the file WITHOUT extension name to save as 
 * @param directory - directory of where the file is to be saved
 * @param id - name of the report to export
 * @returns client
 */
exports.command = function (option, devURL, fileName, directory, id) {
	var fs = require('fs');
	var request = require('request');
	var client = this;
	var url = new String();
	var selectedID = new String();
	var cookieName = new String();
	var cookieValue = new String();
	var optionEdited = option.toLowerCase().trim();
	var dest = directory + "\\" + fileName + ".pdf";
	var file = fs.createWriteStream(dest);

	client.getCookie("JSESSIONID", function (result) {
		cookieName = result.name;
		cookieValue = result.value;
	})
	client.pause(1000, function () {
		// export PDF URL for reports
		if ((optionEdited === "report") || (optionEdited === "reports")) {
			client.assert.title('Digital Iris - Reports');
			client.useXpath().getAttribute("(//ul[@id='reportsList']//p[contains(text(), '" + id + "')]//..//..)[1]", "id", function (result) {				
				selectedID = result.value.trim().substr(7);
			});
			client.pause(1000, function () {
				url = devURL + "/secure/reporting/viewReport.html?&repositoryID=" + selectedID; 
			});
		}
		
		// using the URL from earlier to get the downloaded file and then put it in desired file
		client.pause(1000, function () {
			var options = {
				url : url,
				method : 'GET',
				headers : {
					'Cookie' : cookieName + "=" + cookieValue,
				},
			};
			request(options, function (error, response, body) {
				if (!error && response.statusCode === 200) {
					console.log("File Download Success!") 
				} else {
					console.log("File Download Error: " + response.statusCode)
				}
			}).pipe(file);
			client.pause(2000);
		})
	});

	return client;
};
