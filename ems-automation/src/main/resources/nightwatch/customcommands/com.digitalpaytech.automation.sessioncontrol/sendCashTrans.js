/**
 * @summary Sends the event to Iris and asserts the response
 * @since 7.3.6
 * @version 1.2
 * @author Thomas C, Mandy F
 * @param eventArray - an array of event objects {type:type, action: action}
 * @returns client
 **/
var data;

try {
    data = require('../../data.js');
} catch (error) {
    data = require('nightwatch/data.js');
}
var request;
try {
    request = require('../../node_modules/request');
} catch (error) {
    request = require('request');
}
var devurl = data("URL");
var paystationCommAddress = data("PaystationCommAddress");


exports.command = function(transArray, serialNum) {
    client = this;
    if (serialNum != null) {
        paystationCommAddress = serialNum;
    }
    var statusCode;
    var date = new Date();
    //Gen random messageNum <= 1000
    var messageNum = Math.floor(Math.random() * 100) + 1;
	var body = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><methodCall><methodName>XMLRPCRequestHandler.processXML</methodName><params><param><value><?xml version=\"1.0\"?><Paystation_2 Password=\"zaq123$ESZxsw234%RDX\" UserId=\"emsadmin\" Version=\"6.3 build 0013\"><Message4EMS>";
    var footer = "</Message4EMS></Paystation_2></value></param></params></methodCall>";

    for (i = 0; i < transArray.length; i++) {
		//converts date to GMT
		var gmtDate = new Date(date.valueOf() + date.getTimezoneOffset() * 60000);
		var expiryDate = new Date(date.valueOf() + 60000 * 30 + date.getTimezoneOffset() * 60000);
		var gmtDateString = gmtDate.getFullYear() + ":" + ('0' + (gmtDate.getMonth() + 1)).slice(-2) + ":" + ('0' + gmtDate.getDate()).slice(-2) + ':' + ('0' + gmtDate.getHours()).slice(-2) + ":" + ('0' + gmtDate.getMinutes()).slice(-2) + ":" + ('0' + gmtDate.getSeconds()).slice(-2) + ":GMT";
		var gmtExpDateString = expiryDate.getFullYear() + ":" + ('0' + (expiryDate.getMonth() + 1)).slice(-2) + ":" + ('0' + expiryDate.getDate()).slice(-2) + ':' + ('0' + expiryDate.getHours()).slice(-2) + ":" + ('0' + expiryDate.getMinutes()).slice(-2) + ":" + ('0' + expiryDate.getSeconds()).slice(-2) + ":GMT";
	
		if (transArray[i].purchaseDate != null) {
			gmtDateString = transArray[i].purchaseDate;
		}
	
		if (transArray[i].expiryDate != null) {
			gmtExpDateString = transArray[i].expiryDate;
		}

        body += "<Transaction MessageNumber=\"" + (messageNum + i) + "\">";
        body += "<PaystationCommAddress>" + paystationCommAddress + "</PaystationCommAddress>";
        body += "<Number>" + (messageNum + i) + "</Number>";
        body += "<LotNumber>Automation Lot</LotNumber>";
        body += "<LicensePlateNo>" + transArray[i].licenceNum + "</LicensePlateNo>";
        body += "<AddTimeNum>270000</AddTimeNum>";
        body += "<StallNumber>" + transArray[i].stallNum + "</StallNumber>";
        body += "<Type>" + transArray[i].type + "</Type>";
        body += "<PurchasedDate>" + gmtDateString + "</PurchasedDate>";
        body += "<ParkingTimePurchased>30</ParkingTimePurchased>";
        body += "<OriginalAmount>" + transArray[i].originalAmount + "</OriginalAmount>";
        body += "<ChargedAmount>" + transArray[i].chargedAmount + "</ChargedAmount>";
        body += "<CouponNumber></CouponNumber>";
        body += "<CashPaid>" + transArray[i].cashPaid + "</CashPaid>";
        body += "<CardPaid></CardPaid>";
        body += "<CardData></CardData>";
        body += "<IsRefundSlip>" + transArray[i].isRefundSlip + "</IsRefundSlip>";
        body += "<ExpiryDate>" + gmtExpDateString + "</ExpiryDate>";
        body += "<Tax1Rate>7.24</Tax1Rate>";
        body += "<Tax1Value>1</Tax1Value>";
        body += "<Tax1Name>GST</Tax1Name>";
        body += "<Tax2Rate>5.00</Tax2Rate>";
        body += "<Tax2Value>1.00</Tax2Value>";
        body += "<Tax2Name>PST</Tax2Name>";
        body += "<LicensePlateNo>" + transArray[i].licenceNum + "</LicensePlateNo>";
        body += "<RateName>early bird</RateName>";
        body += "<RateID>1</RateID>";
        body += "<RateValue>400</RateValue>";
        body += "<TaxType>N/A</TaxType>";
        body += "</Transaction>";
    }

    body += footer;
    console.log(body)
    var postRequest = {
        uri: devurl + '/XMLRPC_PS2',
        method: "POST",
        agent: false,
        timeout: 1000,
        headers: {
            'content-type': 'text/xml',
            'content-length': Buffer.byteLength(body, [''])
        }
    };

    var req = request.post(postRequest, function(err, httpResponse, body) {

        var response = body.replace("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><methodResponse><params><param><value><base64>", "");
        response = response.replace("</base64></value></param></params></methodResponse>", "");
        result = new Buffer(response, 'base64').toString('ascii');
        console.log("Response : " + result)
        client.assert.equal(httpResponse.statusCode, 200, "Status Code Correct");
        client.assert.equal(result.indexOf("Acknowledge") > -1, true, "Message accepted");


    });

    client.pause(2000)
    req.write(body);
    req.end();
	
}