/**
 * @summary Custom Command: Assert text in PDF
 * @since 7.4.1
 * @version 1.0
 * @author Mandy F
 */

/**
 * @description asserts that the items specified is found in the PDF
 * @param fileName - the name of the PDF file
 * @param directory - the directory of the file (usually __dirname)
 * @param items - the texts you want to find in the PDF, in an array
 * @returns client
 */
 
exports.command = function (fileName, directory, items) {
	var client = this;
	var filePath = directory + "\\" + fileName;
	var selector = "//div[contains(text(), '";
	
	client.url(filePath);
	client.pause(2000);
	for (var i = 0; i < items.length; i++) {
		client.useXpath().assert.visible(selector + items[i].trim() + "')]");
		client.pause(1000);
	}
	client.back();
	client.pause(2000);
	
	return client;
};
