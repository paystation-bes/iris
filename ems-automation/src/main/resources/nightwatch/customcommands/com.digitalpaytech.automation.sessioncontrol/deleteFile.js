/**
 * @summary Custom Command: Delete File
 * @since 7.4.1
 * @version 1.0
 * @author Mandy F
 */

/**
 * @description Deletes files from your directory/system
 * @param fileName - the name of the file to be deleted with file extension included
 * @param directory - the directory of the file (usually __dirname)
 * @returns client
 */
 
exports.command = function (fileName, directory) {
	var client = this;
	var fs = require('fs');
	
	var filePath = directory + "\\" + fileName; 
	fs.unlink(filePath, function (err) {
		if (err) throw err;
		console.log("File deleted successfully!");
	});
	
	client.pause(1000);
	return client;
};
