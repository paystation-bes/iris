/**
 * @summary EMS-6269: Iris 7.1: Empty Image in footer contains link to digitalpaytech.com
 * @since 7.4.2
 * @version 1.0
 * @author Mandy F
 * @see EMS-6269
 **/
 
var data;
try {
	data = require('../../data.js');
} catch (error) {
	data = require('nightwatch/data.js');
}
var devurl = data("URL");

module.exports = {	

	tags : ['completetesttag', 'bugresolutiontag'],
	
	/**
	 * @description Navigates to the login page
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"testEmptyFooterLinkGone: Navigate to Login Page" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.assert.title("Digital Iris - Login.")
		.pause(1000);
	},
	
	/**
	 * @description Verify that the empty footer image/link is gone
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"testEmptyFooterLinkGone: Verify Empty Footer Image/Link Gone" : function (browser) {
		var footerLink = "//footer//a[@href='http://digitalpaytech.com/']";
		var footerImg = footerLink + "//img";
		browser
		.useXpath()
		.assert.elementNotPresent(footerLink)
		.assert.elementNotPresent(footerImg)
		.pause(1000)
		.end();
	},
	
};
