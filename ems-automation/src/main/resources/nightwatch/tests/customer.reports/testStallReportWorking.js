/**
 * @summary EMS-6428 Pay station stall report will not work intermittently
 * @since 7.4.2
 * @version 1.0
 * @author Mandy F
 * @see EMS-6428
 **/

var data;
try {
	data = require('../../data.js');
} catch (error) {
	data = require('nightwatch/data.js');
}

var devurl = data('URL');
var username = data("ChildUserName");
var password = data("Password");

var psSerialNum = new String();
var psLocName = new String();

module.exports = {
	tags : ['completetesttag', 'bugresolutiontag'],

	/**
	 * @description Logs into child customer
	 * @param browser - The web browser object
	 * @returns void
	 **/
	
	"testStallReportWorking: Log Into Child Customer" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	 * @description Navigates to Settings: Pay Stations tab
	 * @param browser - The web browser object
	 * @returns void
	 **/
	
	"testStallReportWorking: Go to Settings: Pay Stations" : function (browser) {
		browser
		.navigateTo("Settings")
		.pause(1000)
		.navigateTo("Pay Stations")
		.pause(1000)
		.navigateTo("Pay Station List")
		.pause(1000);
	},

	/**
	 * @description Gets a pay station serial number and location name
	 * @param browser - The web browser object
	 * @returns void
	 **/

	"testStallReportWorking: Get Pay Station Serial Number and Location Name" : function (browser) {
		var firstPS = "//ul[@id='paystationList']//li//span";
		var firstLoc = "//section[@id='psLocationLine']//dd[@id='psLocation']//a";
		browser
		.useXpath()
		.waitForElementVisible(firstPS, 1000)
		.getText(firstPS, function (result) {
			psSerialNum = result.value.trim();
			console.log(psSerialNum);
		})
		.pause(1000)
		.click(firstPS)
		.pause(1000)
		.navigateTo("Information")
		.pause(1000)
		.getText(firstLoc, function (result) {
			psLocName = result.value.trim();
			console.log(psLocName);
		})
		.pause(1000);
	},	
	
	/**
	 * @description Navigate to Reports page
	 * @param browser - The web browser object
	 * @returns void
	 **/

	"testStallReportWorking: Navigate to Reports Page" : function (browser) {
		var firstPS = "//ul[@id='paystationList']//li//span";
		var firstLoc = "//section[@id='psLocationLine']//dd[@id='psLocation']//a";
		browser
		.navigateTo("Reports")
		.pause(1000);
	},

	/**
	 * @description Sends multiple valid transactions
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testStallReportWorking: Send Valid Transactions" : function (browser) {
		for (var i = 0; i < 5; i++) {
			var randomStallNum = Math.floor(Math.random() * 999) + 1;
			var cashTrans = {
				licenceNum : 'ABC123',
				stallNum : randomStallNum,
				type : 'Regular',
				originalAmount : '400',
				chargedAmount : '4.00',
				cashPaid : '4.00',
				isRefundSlip : 'false'
			}
			browser.sendCashTrans([cashTrans], psSerialNum);
		}
	},	
	
	 /**
	 * @description Sends multiple expired transactions
	 * @param browser - The web browser object
	 * @returns void
	 **/
 
	"testStallReportWorking: Send Expired Transactions" : function (browser) {
		// generates a purchase and expiry date that is one day before the current date
		var date = new Date();
		date.setDate(date.getDate() - 1);
		var gmtDate = new Date(date.valueOf() + date.getTimezoneOffset() * 60000);
		var expiryDate = new Date(date.valueOf() + 60000 * 30 + date.getTimezoneOffset() * 60000);
		var gmtDateString = gmtDate.getFullYear() + ":" + ('0' + (gmtDate.getMonth() + 1)).slice(-2) + ":" + ('0' + gmtDate.getDate()).slice(-2) + ':' + ('0' + gmtDate.getHours()).slice(-2) + ":" + ('0' + gmtDate.getMinutes()).slice(-2) + ":" + ('0' + gmtDate.getSeconds()).slice(-2) + ":GMT";
		var gmtExpDateString = expiryDate.getFullYear() + ":" + ('0' + (expiryDate.getMonth() + 1)).slice(-2) + ":" + ('0' + expiryDate.getDate()).slice(-2) + ':' + ('0' + expiryDate.getHours()).slice(-2) + ":" + ('0' + expiryDate.getMinutes()).slice(-2) + ":" + ('0' + expiryDate.getSeconds()).slice(-2) + ":GMT";
		
		for (var i = 0; i < 5; i++) {
			var randomStallNum = Math.floor(Math.random() * 999) + 1;
			var cashTrans = {
				licenceNum : 'ABC123',
				stallNum : randomStallNum,
				type : 'Regular',
				originalAmount : '400',
				chargedAmount : '4.00',
				cashPaid : '4.00',
				isRefundSlip : 'false',
				purchaseDate : gmtDateString,
				expiryDate : gmtExpDateString
			}
			browser.sendCashTrans([cashTrans], psSerialNum);
		}
	},	
	
	/**
	 * @description Generate a new stall report
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testStallReportWorking: Create a New All Stall Report" : function (browser) {
		var createReport = "//a[@id='btnAddScheduleReport']";
		var chooseType = "//input[@id='reportTypeID']";
		var stallReportType = "//li//a[contains(., 'Stall Reports')]";
		var nextStep = "//a[@id='btnStep2Select']";
		var locationOpt = "//input[@id='spaceLocation']";
		var location = "//li//a[contains(., '" + psLocName + "')]";
		var otherParameters = "//input[@id='formOtherParameters']";
		var allReportType = "//li//a[text() = 'All']";
		var validReportType = "//li//a[contains(., 'Valid')]";
		var expiredReportType = "//li//a[contains(., 'Expired')]";
		var nextStep2 = "//a[@id='btnStep3Select']";
		var save = "//a[@class='linkButtonFtr textButton save']";
		
		browser
		.useXpath()
		.waitForElementVisible(createReport, 1000)
		.click(createReport)
		.pause(2000)
		.waitForElementVisible(chooseType, 1000)
		.click(chooseType)
		.pause(1000)
		.waitForElementVisible(stallReportType, 1000)
		.click(stallReportType)
		.pause(1000)
		.waitForElementVisible(nextStep, 1000)
		.click(nextStep)
		.pause(2000)
		.waitForElementVisible(locationOpt, 1000)
		.click(locationOpt)
		.pause(1000)
		.waitForElementVisible(location, 1000)
		.click(location)
		.pause(1000)
		.waitForElementVisible(otherParameters, 1000)
		.click(otherParameters)
		.pause(1000)
		.waitForElementVisible(allReportType, 1000)
		.click(allReportType)
		.pause(1000)
		.waitForElementVisible(nextStep2, 1000)
		.click(nextStep2)
		.pause(2000)
		.waitForElementVisible(save, 1000)
		.click(save)
		.pause(2000)
	},
	
	/**
	 * @description Verify All Stall Report is generated successfully
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testStallReportWorking: Verify All Stall Report is Generated Successfully" : function (browser) {
		var reportSubmitted = "//ul[@id='queuedReportsList']//div[@class='col1']//p[contains(., 'Stall Reports')]//..//..//div[@class='col2']//p[contains(., 'Submitted')]";
		var refresh = "//a[@title='Reload']";
		var reportSuccess = "//ul[@id='reportsList']//div[@class='col1']//p[contains(., 'Stall Reports')]//..//..//div[@class='col2']//p[contains(., 'just now')]";
		var itemsToAssert = ["Digital Iris All Enforcement Report", psLocName];
		
		browser
		.useXpath()
		.elements("xpath", reportSubmitted, function (result) {
			if (result.value.length > 0) {
				browser
				.useXpath()
				.waitForElementVisible(reportSubmitted, 1000);
				console.log("Waiting 30 seconds then refreshing");
				browser
				.useXpath()
				.pause(30000)
				.waitForElementVisible(refresh, 1000)
				.click(refresh)
				.pause(2000);
			} 
			browser
			.waitForElementVisible(reportSuccess, 1000)
			.assert.visible(reportSuccess)
			.pause(2000)
			.downloadPDF("reports", devurl, "AllReports", __dirname, "Stall Reports")
			.pause(1000)
			.assertPDFTextPresent("AllReports.pdf", __dirname, itemsToAssert)
			.pause(1000);
		});
	},		
	
	/**
	 * @description Generate a new valid stall report
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testStallReportWorking: Create a New Valid Stall Report" : function (browser) {
		var createReport = "//a[@id='btnAddScheduleReport']";
		var chooseType = "//input[@id='reportTypeID']";
		var stallReportType = "//li//a[contains(., 'Stall Reports')]";
		var nextStep = "//a[@id='btnStep2Select']";
		var locationOpt = "//input[@id='spaceLocation']";
		var location = "//li//a[contains(., '" + psLocName + "')]";
		var otherParameters = "//input[@id='formOtherParameters']";
		var validReportType = "//li//a[contains(., 'Valid')]";
		var expiredReportType = "//li//a[contains(., 'Expired')]";
		var nextStep2 = "//a[@id='btnStep3Select']";
		var save = "//a[@class='linkButtonFtr textButton save']";
		
		browser
		.useXpath()
		.waitForElementVisible(createReport, 1000)
		.click(createReport)
		.pause(2000)
		.waitForElementVisible(chooseType, 1000)
		.click(chooseType)
		.pause(1000)
		.waitForElementVisible(stallReportType, 1000)
		.click(stallReportType)
		.pause(1000)
		.waitForElementVisible(nextStep, 1000)
		.click(nextStep)
		.pause(2000)
		.waitForElementVisible(locationOpt, 1000)
		.click(locationOpt)
		.pause(1000)
		.waitForElementVisible(location, 1000)
		.click(location)
		.pause(1000)
		.waitForElementVisible(otherParameters, 1000)
		.click(otherParameters)
		.pause(1000)
		.waitForElementVisible(validReportType, 1000)
		.click(validReportType)
		.pause(1000)
		.waitForElementVisible(nextStep2, 1000)
		.click(nextStep2)
		.pause(2000)
		.waitForElementVisible(save, 1000)
		.click(save)
		.pause(2000)
	},
	
	/**
	 * @description Verify valid Stall Report is generated successfully
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testStallReportWorking: Verify Valid Stall Report is Generated Successfully" : function (browser) {
		var reportSubmitted = "//ul[@id='queuedReportsList']//div[@class='col1']//p[contains(., 'Stall Reports')]//..//..//div[@class='col2']//p[contains(., 'Submitted')]";
		var refresh = "//a[@title='Reload']";
		var reportSuccess = "//ul[@id='reportsList']//div[@class='col1']//p[contains(., 'Stall Reports')]//..//..//div[@class='col2']//p[contains(., 'just now')]";
		var itemsToAssert = ["Digital Iris Valid Enforcement Report", psLocName];
		
		browser
		.useXpath()
		.elements("xpath", reportSubmitted, function (result) {
			if (result.value.length > 0) {
				browser
				.useXpath()
				.waitForElementVisible(reportSubmitted, 1000);
				console.log("Waiting 30 seconds then refreshing");
				browser
				.useXpath()
				.pause(30000)
				.waitForElementVisible(refresh, 1000)
				.click(refresh)
				.pause(2000);
			} 
			browser
			.waitForElementVisible(reportSuccess, 1000)
			.assert.visible(reportSuccess)
			.pause(2000)
			.downloadPDF("reports", devurl, "ValidReports", __dirname, "Stall Reports")
			.pause(1000)
			.assertPDFTextPresent("ValidReports.pdf", __dirname, itemsToAssert)
			.pause(1000);
		});
	},	
		
	/**
	 * @description Generate a new expired stall report
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testStallReportWorking: Create a New Expired Stall Report" : function (browser) {
		var createReport = "//a[@id='btnAddScheduleReport']";
		var chooseType = "//input[@id='reportTypeID']";
		var stallReportType = "//li//a[contains(., 'Stall Reports')]";
		var nextStep = "//a[@id='btnStep2Select']";
		var locationOpt = "//input[@id='spaceLocation']";
		var location = "//li//a[contains(., '" + psLocName + "')]";
		var otherParameters = "//input[@id='formOtherParameters']";
		var expiredReportType = "//li//a[contains(., 'Expired')]";
		var nextStep2 = "//a[@id='btnStep3Select']";
		var save = "//a[@class='linkButtonFtr textButton save']";
		
		browser
		.useXpath()
		.waitForElementVisible(createReport, 1000)
		.click(createReport)
		.pause(2000)
		.waitForElementVisible(chooseType, 1000)
		.click(chooseType)
		.pause(1000)
		.waitForElementVisible(stallReportType, 1000)
		.click(stallReportType)
		.pause(1000)
		.waitForElementVisible(nextStep, 1000)
		.click(nextStep)
		.pause(2000)
		.waitForElementVisible(locationOpt, 1000)
		.click(locationOpt)
		.pause(1000)
		.waitForElementVisible(location, 1000)
		.click(location)
		.pause(1000)
		.waitForElementVisible(otherParameters, 1000)
		.click(otherParameters)
		.pause(1000)
		.waitForElementVisible(expiredReportType, 1000)
		.click(expiredReportType)
		.pause(1000)
		.waitForElementVisible(nextStep2, 1000)
		.click(nextStep2)
		.pause(2000)
		.waitForElementVisible(save, 1000)
		.click(save)
		.pause(2000)
	},
	
	/**
	 * @description Verify expired Stall Report is generated successfully
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testStallReportWorking: Verify Expired Stall Report is Generated Successfully" : function (browser) {
		var reportSubmitted = "//ul[@id='queuedReportsList']//div[@class='col1']//p[contains(., 'Stall Reports')]//..//..//div[@class='col2']//p[contains(., 'Submitted')]";
		var refresh = "//a[@title='Reload']";
		var reportSuccess = "//ul[@id='reportsList']//div[@class='col1']//p[contains(., 'Stall Reports')]//..//..//div[@class='col2']//p[contains(., 'just now')]";
		var itemsToAssert = ["Digital Iris Expired Enforcement Report", psLocName];
		
		browser
		.useXpath()
		.elements("xpath", reportSubmitted, function (result) {
			if (result.value.length > 0) {
				browser
				.useXpath()
				.waitForElementVisible(reportSubmitted, 1000);
				console.log("Waiting 30 seconds then refreshing");
				browser
				.useXpath()
				.pause(30000)
				.waitForElementVisible(refresh, 1000)
				.click(refresh)
				.pause(2000);
			} 
			browser
			.waitForElementVisible(reportSuccess, 1000)
			.assert.visible(reportSuccess)
			.pause(2000)
			.downloadPDF("reports", devurl, "ExpiredReports", __dirname, "Stall Reports")
			.pause(1000)
			.assertPDFTextPresent("ExpiredReports.pdf", __dirname, itemsToAssert)
			.pause(1000);
		});
	},		
	
	/**
	 * @description Deletes downloaded reports
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testStallReportWorking: Delete Downloaded Reports" : function (browser) {		
		browser
		.deleteFile("AllReports.pdf", __dirname)
		.deleteFile("ValidReports.pdf", __dirname)
		.deleteFile("ExpiredReports.pdf", __dirname)
		.pause(1000);
	},	
	
	/**
	 * @description Log out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testStallReportWorking: Log out of Iris" : function (browser) {
		browser.logout();
	},	
};