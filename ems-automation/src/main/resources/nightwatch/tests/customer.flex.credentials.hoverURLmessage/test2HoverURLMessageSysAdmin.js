/**
* @summary Flex: Flex Credentials: System Admin: EMS-6412
* @since 7.3.5
* @version 1.0
* @author Kianoush Nesvaderani
* @requires 
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");


module.exports = {

	/**
	*@description Logs the user into Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - System Administration');
	},
	
	/**
	*@description Searches the customer and navigates to its page
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigate to Customer Admin" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("#search", 5000)
		.setValue("#search", "oranj")
		.useXpath()
		.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'oranj')]", 2000)
		.click("//a[@class='ui-corner-all'][contains(text(),'oranj')]")
		.click("//a[@id='btnGo']");
	},
	
	/**
	*@description Navigates to Flex Credentials Page
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigate to Flex Credentials Section" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[contains(text(),'Licenses')]", 5000)
		.click("//a[contains(text(),'Licenses')]")
		.click("//a[contains(text(),'Flex Credentials')]");
	},
	
	/**
	*@description Opens the Flex Credentials Edit and Hover Over URL Box
	*@param browser - The web browser object
	*@returns void
	**/
	"Enter Flex Credentials but Cancel" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("#btnEditFlexCredentials", 10000)
		.click("#btnEditFlexCredentials")
		.waitForElementVisible("#formURL", 5000)
		.moveToElement("#formURL", 10, 10);
	},
	
	/**
	*@description Verifies the the Hover message
	*@param browser - The web browser object
	*@returns void
	**/
	"Verify the Success Message" : function (browser) {
			browser.getAttribute("#formURL", "tooltip", function(result) {
				browser.assert.equal(result.value, "Enter your Flex Server Web Services URL in the format https://<server name>/<directory>/<servicename>.asmx");
		});
	},
	
	/**
	*@description Logs the user out of Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Logout" : function (browser) {
		browser.logout();
	},
};
