/**
* @summary Flex: Flex Credentials: EMS-6412
* @since 7.3.5
* @version 1.0
* @author Kianoush Nesvaderani
* @requires 
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("FlexChildName");
var password = data("Password");


module.exports = {

	/**
	*@description Logs the user into Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
		.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	/**
	*@description Navigates the user to the Global tab
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigate to Global tab" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//img[@alt='Settings']", 5000)
		.click("//img[@alt='Settings']")
		.waitForElementVisible("//a[contains(text(),'Global')]", 5000)
		.click("//a[contains(text(),'Global')]");
	},
	
	/**
	*@description Opens the Flex Credentials Edit and Hover Over URL Box
	*@param browser - The web browser object
	*@returns void
	**/
	"Enter Flex Credentials but Cancel" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("#btnEditFlexCredentials", 10000)
		.click("#btnEditFlexCredentials")
		.waitForElementVisible("#formURL", 5000)
		.moveToElement("#formURL", 10, 10);
	},
	
	/**
	*@description Verifies the the Hover message
	*@param browser - The web browser object
	*@returns void
	**/
	"Verify the Success Message" : function (browser) {
			browser.getAttribute("#formURL", "tooltip", function(result) {
				browser.assert.equal(result.value, "Enter your Flex Server Web Services URL in the format https://<server name>/<directory>/<servicename>.asmx");
		});
	},
	
	/**
	*@description Logs the user out of Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Logout" : function (browser) {
		browser.logout();
	},
};
