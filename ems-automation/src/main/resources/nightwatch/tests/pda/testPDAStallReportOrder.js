/**
 * @summary EMS-6345 Iris - PDA page reports intermittently display records out of order
 * @since 7.4.2
 * @version 1.0
 * @author Mandy F
 * @see EMS-6345
 **/

var data;
try {
	data = require('../../data.js');
} catch (error) {
	data = require('nightwatch/data.js');
}

var devurl = data('URL');
var pdaurl = data("PDAURL");
var username = data("ChildUserName");
var password = data("Password");

var psSerialNum = new String();
var psLocName = new String();

module.exports = {
	tags : ['completetesttag', 'bugresolutiontag'],

	/**
	 * @description Logs into child customer
	 * @param browser - The web browser object
	 * @returns void
	 **/
	
	"testPDAStallReportOrder: Log Into Child Customer" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	 * @description Navigates to Settings: Pay Stations tab
	 * @param browser - The web browser object
	 * @returns void
	 **/
	
	"testPDAStallReportOrder: Go to Settings: Pay Stations" : function (browser) {
		browser
		.navigateTo("Settings")
		.pause(1000)
		.navigateTo("Pay Stations")
		.pause(1000)
		.navigateTo("Pay Station List")
		.pause(1000);
	},

	/**
	 * @description Gets a pay station serial number and location name
	 * @param browser - The web browser object
	 * @returns void
	 **/

	"testPDAStallReportOrder: Get Pay Station Serial Number and Location Name" : function (browser) {
		var firstPS = "//ul[@id='paystationList']//li//span";
		var firstLoc = "//section[@id='psLocationLine']//dd[@id='psLocation']//a";
		browser
		.useXpath()
		.waitForElementVisible(firstPS, 1000)
		.getText(firstPS, function (result) {
			psSerialNum = result.value.trim();
			console.log(psSerialNum);
		})
		.pause(1000)
		.click(firstPS)
		.pause(1000)
		.navigateTo("Information")
		.pause(1000)
		.getText(firstLoc, function (result) {
			psLocName = result.value.trim();
			console.log(psLocName);
		})
		.pause(1000);
	},

	/**
	 * @description Logs the user out
	 * @param browser - The web browser object
	 * @returns void
	 **/

	"testPDAStallReportOrder: Log out of the new Customer account" : function (browser) {
		var logoutBtn = "//a[@title='Logout']";
		browser
		.useXpath()
		.waitForElementVisible(logoutBtn, 1000)
		.click(logoutBtn)
		.pause(1000);
	},

	/**
	 * @description Log user into PDA page
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testPDAStallReportOrder: Log User into PDA page" : function (browser) {
		var email = "//input[@name='j_username']";
		var pw = "//input[@name='j_password']";
		var login = "//input[@type='submit']";
		browser
		.useXpath()
		.url(pdaurl)
		.pause(2000)
		.waitForElementVisible(email, 1000)
		.setValue(email, username)
		.pause(1000)
		.waitForElementVisible(pw, 1000)
		.setValue(pw, password)
		.pause(1000)
		.waitForElementVisible(login, 1000)
		.click(login)
		.pause(3000)
		.assert.title('Digital Iris - EMS Search');
	},

	/**
	 * @description Sends multiple valid transactions
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testPDAStallReportOrder: Send Valid Transactions" : function (browser) {
		for (var i = 0; i < 5; i++) {
			var randomStallNum = Math.floor(Math.random() * 999) + 1;
			var cashTrans = {
				licenceNum : 'ABC123',
				stallNum : randomStallNum,
				type : 'Regular',
				originalAmount : '400',
				chargedAmount : '4.00',
				cashPaid : '4.00',
				isRefundSlip : 'false'
			}
			browser.sendCashTrans([cashTrans], psSerialNum);
		}
	},	
	
	 /**
	 * @description Sends multiple expired transactions
	 * @param browser - The web browser object
	 * @returns void
	 **/
 
	"testPDAStallReportOrder: Send Expired Transactions" : function (browser) {
		// generates a purchase and expiry date that is one day before the current date
		var date = new Date();
		date.setDate(date.getDate() - 1);
		var gmtDate = new Date(date.valueOf() + date.getTimezoneOffset() * 60000);
		var expiryDate = new Date(date.valueOf() + 60000 * 30 + date.getTimezoneOffset() * 60000);
		var gmtDateString = gmtDate.getFullYear() + ":" + ('0' + (gmtDate.getMonth() + 1)).slice(-2) + ":" + ('0' + gmtDate.getDate()).slice(-2) + ':' + ('0' + gmtDate.getHours()).slice(-2) + ":" + ('0' + gmtDate.getMinutes()).slice(-2) + ":" + ('0' + gmtDate.getSeconds()).slice(-2) + ":GMT";
		var gmtExpDateString = expiryDate.getFullYear() + ":" + ('0' + (expiryDate.getMonth() + 1)).slice(-2) + ":" + ('0' + expiryDate.getDate()).slice(-2) + ':' + ('0' + expiryDate.getHours()).slice(-2) + ":" + ('0' + expiryDate.getMinutes()).slice(-2) + ":" + ('0' + expiryDate.getSeconds()).slice(-2) + ":GMT";
		
		for (var i = 0; i < 5; i++) {
			var randomStallNum = Math.floor(Math.random() * 999) + 1;
			var cashTrans = {
				licenceNum : 'ABC123',
				stallNum : randomStallNum,
				type : 'Regular',
				originalAmount : '400',
				chargedAmount : '4.00',
				cashPaid : '4.00',
				isRefundSlip : 'false',
				purchaseDate : gmtDateString,
				expiryDate : gmtExpDateString
			}
			browser.sendCashTrans([cashTrans], psSerialNum);
		}
	},	
	
	/**
	 * @description Generate a Valid Space Query searching by Configuration 
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testPDAStallReportOrder: Generate a Valid Space Query Searching by Configuration" : function (browser) {
		var validSpaceRangeBtn = "//a[contains(., 'Valid Space Range')]";
		var configurationBtn = "//label//input[@value='paystation']";
		var configOption = "//select[@name='wrappedObject.paystationSettingName']";
		var automationLot = "//select[@name='wrappedObject.paystationSettingName']//option[@value='Automation Lot']";
		var startingSpaceNum = "//input[@name='wrappedObject.startStall' and @type='text']";
		var endingSpaceNum = "//input[@name='wrappedObject.endStall' and @type='text']";
		var search = "//input[@type='submit']";
		
		browser
		.useXpath()
		.waitForElementVisible(validSpaceRangeBtn, 1000)
		.click(validSpaceRangeBtn)
		.pause(2000)
		.waitForElementVisible(configurationBtn, 1000)
		.click(configurationBtn)
		.pause(1000)
		.waitForElementVisible(configOption, 1000)
		.click(configOption)
		.pause(1000)
		.waitForElementVisible(automationLot, 1000)
		.click(automationLot)
		.pause(1000)
		.waitForElementVisible(startingSpaceNum, 1000)
		.setValue(startingSpaceNum, "0")
		.pause(1000)
		.waitForElementVisible(endingSpaceNum, 1000)
		.setValue(endingSpaceNum, "999")
		.pause(1000)
		.waitForElementVisible(search, 1000)
		.click(search)
		.pause(1000);
	},
	
	/**
	 * @description Verify Valid Stall Numbers are in order for Configuration
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testPDAStallReportOrder: Verify Valid Stall Numbers are in order for Configuration" : function (browser) {
		var reportCount = "//tr//td[1]";
		var count = 0;
		var ok = "OK";
		browser
		.useXpath()
		.elements("xpath", reportCount, function (result) {
			count = result.value.length - 2;
			
			for (var i = 3; i < count + 3; i++) {
				var space = "//tr[" + i + "]//td[1]";
				var prevNum = 0;
				var num = 0;
				var ok = "OK";
				browser
				.useXpath()
				.getText(space, function (result1) {
					num = parseInt(result1.value);
						
					if (num < prevNum) {
						ok = "Not OK";
					}
					
					browser.assert.equal(ok, "OK", "Stall Number is in order");
					prevNum = num;
				});
			};
		})
	},	
	
	/**
	 * @description Generate a Valid Space Query searching by Location 
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testPDAStallReportOrder: Generate a Valid Space Query Searching by Location" : function (browser) {
		var locationBtn = "//label//input[@value='location']";
		var locationOption = "//select[@name='wrappedObject.locationRandomId']";
		var location = "//select[@name='wrappedObject.locationRandomId']//option[contains(., '"  + psLocName +  "')]";
		var startingSpaceNum = "//input[@name='wrappedObject.startStall' and @type='text']";
		var endingSpaceNum = "//input[@name='wrappedObject.endStall' and @type='text']";
		var search = "//input[@type='submit']";
		
		browser
		.useXpath()
		.waitForElementVisible(locationBtn, 1000)
		.click(locationBtn)
		.pause(2000)
		.waitForElementVisible(locationOption, 1000)
		.click(locationOption)
		.pause(1000)
		.waitForElementVisible(location, 1000)
		.click(location)
		.pause(1000)
		.waitForElementVisible(startingSpaceNum, 1000)
		.clearValue(startingSpaceNum)
		.pause(1000)
		.setValue(startingSpaceNum, "0")
		.pause(1000)
		.waitForElementVisible(endingSpaceNum, 1000)
		.clearValue(endingSpaceNum)
		.pause(1000)
		.setValue(endingSpaceNum, "999")
		.pause(1000)
		.waitForElementVisible(search, 1000)
		.click(search)
		.pause(1000);
	},
	
	/**
	 * @description Verify Valid Stall Numbers are in order for Location
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testPDAStallReportOrder: Verify Valid Stall Numbers are in order for Location" : function (browser) {
		var reportCount = "//tr//td[1]";
		var count = 0;
		browser
		.useXpath()
		.elements("xpath", reportCount, function (result) {
			count = result.value.length - 2;
			
			for (var i = 3; i < count + 3; i++) {
				var space = "//tr[" + i + "]//td[1]";
				var prevNum = 0;
				var num = 0;
				var ok = "OK";
				browser
				.useXpath()
				.getText(space, function (result1) {
					num = parseInt(result1.value);
						
					if (num < prevNum) {
						ok = "Not OK";
					}
					
					browser.assert.equal(ok, "OK", "Stall Number is in order");
					prevNum = num;
				});
			};
		});
	},	
	
	/**
	 * @description Generate a Valid Space Query searching by Customer 
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testPDAStallReportOrder: Generate a Valid Space Query Searching by Customer" : function (browser) {
		var customerBtn = "//label//input[@value='customer']";
		var startingSpaceNum = "//input[@name='wrappedObject.startStall' and @type='text']";
		var endingSpaceNum = "//input[@name='wrappedObject.endStall' and @type='text']";
		var search = "//input[@type='submit']";
		
		browser
		.useXpath()
		.waitForElementVisible(customerBtn, 1000)
		.click(customerBtn)
		.pause(2000)
		.waitForElementVisible(startingSpaceNum, 1000)
		.clearValue(startingSpaceNum)
		.pause(1000)
		.setValue(startingSpaceNum, "0")
		.pause(1000)
		.waitForElementVisible(endingSpaceNum, 1000)
		.clearValue(endingSpaceNum)
		.pause(1000)
		.setValue(endingSpaceNum, "999")
		.pause(1000)
		.waitForElementVisible(search, 1000)
		.click(search)
		.pause(1000);
	},
	
	/**
	 * @description Verify Valid Stall Numbers are in order for Customer
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testPDAStallReportOrder: Verify Valid Stall Numbers are in order for Customer" : function (browser) {
		var reportCount = "//tr//td[1]";
		var count = 0;
		browser
		.useXpath()
		.elements("xpath", reportCount, function (result) {
			count = result.value.length - 2;
			
			for (var i = 3; i < count + 3; i++) {
				var space = "//tr[" + i + "]//td[1]";
				var prevNum = 0;
				var num = 0;
				var ok = "OK";
				browser
				.useXpath()
				.getText(space, function (result1) {
					num = parseInt(result1.value);
						
					if (num < prevNum) {
						ok = "Not OK";
					}
					
					browser.assert.equal(ok, "OK", "Stall Number is in order");
					prevNum = num;
				});
			};
		});
	},		
	
	/**
	 * @description Generate an Expired Space Query searching by Configuration 
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testPDAStallReportOrder: Generate an Expired Space Query searching by Configuration" : function (browser) {
		var homeBtn = "//a[@title='Home']";
		var expiredSpaceRangeBtn = "//a[contains(., 'Expired Space Range')]";
		var configurationBtn = "//label//input[@value='paystation']";
		var configOption = "//select[@name='wrappedObject.paystationSettingName']";
		var automationLot = "//select[@name='wrappedObject.paystationSettingName']//option[@value='Automation Lot']";
		var startingSpaceNum = "//input[@name='wrappedObject.startStall' and @type='text']";
		var endingSpaceNum = "//input[@name='wrappedObject.endStall' and @type='text']";
		var search = "//input[@type='submit']";
		
		browser
		.useXpath()
		.waitForElementVisible(homeBtn, 1000)
		.click(homeBtn)
		.pause(2000)
		.waitForElementVisible(expiredSpaceRangeBtn, 1000)
		.click(expiredSpaceRangeBtn)
		.pause(2000)
		.waitForElementVisible(configurationBtn, 1000)
		.click(configurationBtn)
		.pause(1000)
		.waitForElementVisible(configOption, 1000)
		.click(configOption)
		.pause(1000)
		.waitForElementVisible(automationLot, 1000)
		.click(automationLot)
		.pause(1000)
		.waitForElementVisible(startingSpaceNum, 1000)
		.setValue(startingSpaceNum, "0")
		.pause(1000)
		.waitForElementVisible(endingSpaceNum, 1000)
		.setValue(endingSpaceNum, "999")
		.pause(1000)
		.waitForElementVisible(search, 1000)
		.click(search)
		.pause(1000);
	},	
		
	/**
	 * @description Verify Expired Stall Numbers are in order for Configuration
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testPDAStallReportOrder: Verify Expired Stall Numbers are in order for Configuration" : function (browser) {
		var reportCount = "//tr//td[1]";
		var count = 0;
		browser
		.useXpath()
		.elements("xpath", reportCount, function (result) {
			count = result.value.length - 2;
			
			for (var i = 3; i < count + 3; i++) {
				var space = "//tr[" + i + "]//td[1]";
				var prevNum = 0;
				var num = 0;
				var ok = "OK";
				browser
				.useXpath()
				.getText(space, function (result1) {
					num = parseInt(result1.value);
						
					if (num < prevNum) {
						ok = "Not OK";
					}
					
					browser.assert.equal(ok, "OK", "Stall Number is in order");
					prevNum = num;
				});
			};
		});
	},
	
	/**
	 * @description Generate a Expired Space Query searching by Location 
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"testPDAStallReportOrder: Generate a Expired Space Query Searching by Location" : function (browser) {
		var locationBtn = "//label//input[@value='location']";
		var locationOption = "//select[@name='wrappedObject.locationRandomId']";
		var location = "//select[@name='wrappedObject.locationRandomId']//option[contains(., '"  + psLocName +  "')]";
		var startingSpaceNum = "//input[@name='wrappedObject.startStall' and @type='text']";
		var endingSpaceNum = "//input[@name='wrappedObject.endStall' and @type='text']";
		var search = "//input[@type='submit']";
		
		browser
		.useXpath()
		.waitForElementVisible(locationBtn, 1000)
		.click(locationBtn)
		.pause(2000)
		.waitForElementVisible(locationOption, 1000)
		.click(locationOption)
		.pause(1000)
		.waitForElementVisible(location, 1000)
		.click(location)
		.pause(1000)
		.waitForElementVisible(startingSpaceNum, 1000)
		.clearValue(startingSpaceNum)
		.pause(1000)
		.setValue(startingSpaceNum, "0")
		.pause(1000)
		.waitForElementVisible(endingSpaceNum, 1000)
		.clearValue(endingSpaceNum)
		.pause(1000)
		.setValue(endingSpaceNum, "999")
		.pause(1000)
		.waitForElementVisible(search, 1000)
		.click(search)
		.pause(1000);
	},
	
	/**
	 * @description Verify Expired Stall Numbers are in order for Location
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testPDAStallReportOrder: Verify Expired Stall Numbers are in order for Location" : function (browser) {
		var reportCount = "//tr//td[1]";
		var count = 0;
		browser
		.useXpath()
		.elements("xpath", reportCount, function (result) {
			count = result.value.length - 2;
			
			for (var i = 3; i < count + 3; i++) {
				var space = "//tr[" + i + "]//td[1]";
				var prevNum = 0;
				var num = 0;
				var ok = "OK";
				browser
				.useXpath()
				.getText(space, function (result1) {
					num = parseInt(result1.value);
						
					if (num < prevNum) {
						ok = "Not OK";
					}
					
					browser.assert.equal(ok, "OK", "Stall Number is in order");
					prevNum = num;
				});
			};
		});
	},	
	
	/**
	 * @description Generate a Expired Space Query searching by Customer 
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testPDAStallReportOrder: Generate a Expired Space Query Searching by Customer" : function (browser) {
		var customerBtn = "//label//input[@value='customer']";
		var startingSpaceNum = "//input[@name='wrappedObject.startStall' and @type='text']";
		var endingSpaceNum = "//input[@name='wrappedObject.endStall' and @type='text']";
		var search = "//input[@type='submit']";
		
		browser
		.useXpath()
		.waitForElementVisible(customerBtn, 1000)
		.click(customerBtn)
		.pause(2000)
		.waitForElementVisible(startingSpaceNum, 1000)
		.clearValue(startingSpaceNum)
		.pause(1000)
		.setValue(startingSpaceNum, "0")
		.pause(1000)
		.waitForElementVisible(endingSpaceNum, 1000)
		.clearValue(endingSpaceNum)
		.pause(1000)
		.setValue(endingSpaceNum, "999")
		.pause(1000)
		.waitForElementVisible(search, 1000)
		.click(search)
		.pause(1000);
	},
	
	/**
	 * @description Verify Expired Stall Numbers are in order for Customer
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testPDAStallReportOrder: Verify Expired Stall Numbers are in order for Customer" : function (browser) {
		var reportCount = "//tr//td[1]";
		var count = 0;
		browser
		.useXpath()
		.elements("xpath", reportCount, function (result) {
			count = result.value.length - 2;
			
			for (var i = 3; i < count + 3; i++) {
				var space = "//tr[" + i + "]//td[1]";
				var prevNum = 0;
				var num = 0;
				var ok = "OK";
				browser
				.useXpath()
				.getText(space, function (result1) {
					num = parseInt(result1.value);
						
					if (num < prevNum) {
						ok = "Not OK";
					}
					
					browser.assert.equal(ok, "OK", "Stall Number is in order");
					prevNum = num;
				});
			};
		});
	},		
	
	/**
	 * @description Log out of PDA page
	 * @param browser - The web browser object
	 * @returns void
	 **/
	 
	"testPDAStallReportOrder: Log out of PDA page" : function (browser) {
		var logoutBtn = "//a[@title='Logout']";
		browser
        .useXpath()
        .waitForElementVisible(logoutBtn, 1000)
        .click(logoutBtn)
        .pause(1000)
        .end();
	},	
};