/**
 * @summary EMS-6283 - Email inputs in a system admin account will throw an invalid character warning
 * @since 7.4.2
 * @version 1.0
 * @author Mandy F
 * @see EMS-6283
 **/

var data;
try {
	data = require('../../data.js');
} catch (error) {
	data = require('nightwatch/data.js');
}
var devurl = data("URL");
var username = data("SysAdminUserName");
var childUser = "qa1child";
var parentUser = "qa1parent";
var password = data("Password");

module.exports = {

	tags : ['completetesttag', 'bugresolutiontag'],

	/**
	 * @description Logs the system admin into Iris
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test03EmailInputSystemAdmin: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - System Administration');
	},
	
	/**
	 * @description Search up child customer
	 * @param browser - The web browser object
	 * @returns void
	 */
	 
	"test03EmailInputSystemAdmin: Search Child Customer" : function (browser) {
		var searchBar = "//input[@id='search']";
		var childCust = "//ul//a[text() = '" + childUser + "']";
		var goBtn = "//a[@id='btnGo']";
		browser
		.useXpath()
		.waitForElementVisible(searchBar, 1000)
		.setValue(searchBar, childUser)
		.pause(1000)
		.waitForElementVisible(childCust, 1000)
		.click(childCust)
		.pause(1000)
		.waitForElementVisible(goBtn, 1000)
		.click(goBtn)
		.pause(1000);
	},		
	
	/**
	 * @description Go to reports tab and create report for child customer
	 * @param browser - The web browser object
	 * @returns void
	 */
	 
	"test03EmailInputSystemAdmin: Go to Reports Tab and Create Report For Child Customer" : function (browser) {
		var reportsTab = "//section[@id='secondNavArea']//a[@title='Reports']";
		var createReportBtn = "//a[@id='btnAddScheduleReportWS']";
		var reportTypeOpt = "//input[@id='reportTypeID']";
		var reportType = "//ul//a[text() = 'Transaction - All']";
		var nextStep1 = "//a[@id='btnStep2Select']";
		var nextStep2 = "//a[@id='btnStep3Select']";
		browser
		.useXpath()
		.waitForElementVisible(reportsTab, 1000)
		.click(reportsTab)
		.pause(1000)
		.waitForElementVisible(createReportBtn, 1000)
		.click(createReportBtn)
		.pause(1000)
		.waitForElementVisible(reportTypeOpt, 1000)
		.click(reportTypeOpt)
		.pause(1000)
		.waitForElementVisible(reportType, 1000)
		.click(reportType)
		.pause(1000)
		.waitForElementVisible(nextStep1, 1000)
		.click(nextStep1)
		.pause(2000)
		.waitForElementVisible(nextStep2, 1000)
		.click(nextStep2)
		.pause(1000);		
	},		
	
	/**
	 * @description Verify invalid character message warning for child customer report email input
	 * @param browser - The web browser object
	 * @returns void
	 */
	 
	"test03EmailInputSystemAdmin: Verify Invalid Character Message Warning for Child Customer Report Email Input" : function (browser) {
		var emailForm = "//input[@id='formNewEmail']";
		var alertBox = "//section[@id='messageResponseAlertBox']";
		var attentionNotice = "//article[contains(., 'This field only accepts Letters, digits and special characters (except space, comma and special characters ')]";
		var closeNotice = "//a[@title='Close']";
		var invalidChars = [" ", ",", ":", ";", "\\", "<", ">"];
		for (var i = 0; i < invalidChars.length; i++) {
			browser
			.useXpath()
			.waitForElementVisible(emailForm, 1000)
			.setValue(emailForm, invalidChars[i])
			.pause(1000)
			.waitForElementVisible(alertBox, 1000)
			.assert.visible(attentionNotice)
			.click(closeNotice)
			.pause(1000);
		}		
	},		
	
	/**
	 * @description Cancel child customer creating report
	 * @param browser - The web browser object
	 * @returns void
	 */
	 
	"test03EmailInputSystemAdmin: Cancel Child Customer Create Report" : function (browser) {
		var cancelBtn = "//a[@class='linkButton textButton cancel']";
		var confirmCancel = "//button//span[text() = 'Yes, cancel now']";
		browser
		.useXpath()
		.waitForElementVisible(cancelBtn, 1000)
		.click(cancelBtn)
		.pause(1000)
		.waitForElementVisible(confirmCancel, 1000)
		.click(confirmCancel)
		.pause(1000);
	},		
	
	/**
	 * @description Go to child customer alerts tab and add alert
	 * @param browser - The web browser object
	 * @returns void
	 */
	 
	"test03EmailInputSystemAdmin: Go to Child Customer Alerts Tab and Add Alert" : function (browser) {
		var alertsTab = "//section[@id='secondNavArea']//a[@title='Alerts']";
		var addAlert = "//a[@id='btnAddAlert']";
		browser
		.useXpath()
		.waitForElementVisible(alertsTab, 1000)
		.click(alertsTab)
		.pause(1000)
		.waitForElementVisible(addAlert, 1000)
		.click(addAlert)
		.pause(1000);
	},	
		
	/**
	 * @description Verify invalid character message warning for child customer alert email input
	 * @param browser - The web browser object
	 * @returns void
	 */
	 
	"test03EmailInputSystemAdmin: Verify Invalid Character Message Warning for Child Customer Alert Email Input" : function (browser) {
		var emailForm = "//input[@id='newNotificationEmail']";
		var alertBox = "//section[@id='messageResponseAlertBox']";
		var attentionNotice = "//article[contains(., 'This field only accepts Letters, digits and special characters (except space, comma and special characters ')]";
		var closeNotice = "//a[@title='Close']";
		var invalidChars = [" ", ",", ":", ";", "\\", "<", ">"];
		for (var i = 0; i < invalidChars.length; i++) {
			browser
			.useXpath()
			.waitForElementVisible(emailForm, 1000)
			.setValue(emailForm, invalidChars[i])
			.pause(1000)
			.waitForElementVisible(alertBox, 1000)
			.assert.visible(attentionNotice)
			.click(closeNotice)
			.pause(1000);
		}
	},		
	
	/**
	 * @description Cancel add alert form
	 * @param browser - The web browser object
	 * @returns void
	 */
	 
	"test03EmailInputSystemAdmin: Cancel Add Alert Form" : function (browser) {
		var cancelBtn = "//a[@class='linkButton textButton cancel']";
		browser
		.useXpath()
		.waitForElementVisible(cancelBtn, 1000)
		.click(cancelBtn)
		.pause(1000);
	},	
	
	/**
	 * @description Search up parent customer
	 * @param browser - The web browser object
	 * @returns void
	 */
	 
	"test03EmailInputSystemAdmin: Search Parent Customer" : function (browser) {
		var searchBar = "//input[@id='search']";
		var parentCust = "//ul//a[text() = '" + parentUser + "']";
		var goBtn = "//a[@id='btnGo']";
		browser
		.useXpath()
		.waitForElementVisible(searchBar, 1000)
		.setValue(searchBar, parentUser)
		.pause(1000)
		.waitForElementVisible(parentCust, 1000)
		.click(parentCust)
		.pause(1000)
		.waitForElementVisible(goBtn, 1000)
		.click(goBtn)
		.pause(1000);
	},		
	
	/**
	 * @description Go to reports tab and create report for parent customer
	 * @param browser - The web browser object
	 * @returns void
	 */
	 
	"test03EmailInputSystemAdmin: Go to Reports Tab and Create Report For Parent Customer" : function (browser) {
		var reportsTab = "//section[@id='secondNavArea']//a[@title='Reports']";
		var createReportBtn = "//a[@id='btnAddScheduleReportWS']";
		var reportTypeOpt = "//input[@id='reportTypeID']";
		var reportType = "//ul//a[text() = 'Transaction - All']";
		var nextStep1 = "//a[@id='btnStep2Select']";
		var nextStep2 = "//a[@id='btnStep3Select']";
		browser
		.useXpath()
		.waitForElementVisible(reportsTab, 1000)
		.click(reportsTab)
		.pause(1000)
		.waitForElementVisible(createReportBtn, 1000)
		.click(createReportBtn)
		.pause(1000)
		.waitForElementVisible(reportTypeOpt, 1000)
		.click(reportTypeOpt)
		.pause(1000)
		.waitForElementVisible(reportType, 1000)
		.click(reportType)
		.pause(1000)
		.waitForElementVisible(nextStep1, 1000)
		.click(nextStep1)
		.pause(2000)
		.waitForElementVisible(nextStep2, 1000)
		.click(nextStep2)
		.pause(1000);		
	},		
	
	/**
	 * @description Verify invalid character message warning for parent customer report email input
	 * @param browser - The web browser object
	 * @returns void
	 */
	 
	"test03EmailInputSystemAdmin: Verify Invalid Character Message Warning for Parent Customer Report Email Input" : function (browser) {
		var emailForm = "//input[@id='formNewEmail']";
		var alertBox = "//section[@id='messageResponseAlertBox']";
		var attentionNotice = "//article[contains(., 'This field only accepts Letters, digits and special characters (except space, comma and special characters ')]";
		var closeNotice = "//a[@title='Close']";
		var invalidChars = [" ", ",", ":", ";", "\\", "<", ">"];
		for (var i = 0; i < invalidChars.length; i++) {
			browser
			.useXpath()
			.waitForElementVisible(emailForm, 1000)
			.setValue(emailForm, invalidChars[i])
			.pause(1000)
			.waitForElementVisible(alertBox, 1000)
			.assert.visible(attentionNotice)
			.click(closeNotice)
			.pause(1000);
		}		
	},		
	
	/**
	 * @description Cancel parent customer creating report
	 * @param browser - The web browser object
	 * @returns void
	 */
	 
	"test03EmailInputSystemAdmin: Cancel Parent Customer Create Report" : function (browser) {
		var cancelBtn = "//a[@class='linkButton textButton cancel']";
		var confirmCancel = "//button//span[text() = 'Yes, cancel now']";
		browser
		.useXpath()
		.waitForElementVisible(cancelBtn, 1000)
		.click(cancelBtn)
		.pause(1000)
		.waitForElementVisible(confirmCancel, 1000)
		.click(confirmCancel)
		.pause(1000);
	},		
	
	/**
	 * @description Navigates to the System Admin Reports tab
	 * @param browser - The web browser object
	 * @returns void
	 */
	 
	"test03EmailInputSystemAdmin: Navigates to System Admin Reports Tab" : function (browser) {
		browser
		.navigateTo("Reports")
		.pause(1000);
	},	
	
	/**
	 * @description Create a System Admin Report
	 * @param browser - The web browser object
	 * @returns void
	 */
	 
	"test03EmailInputSystemAdmin: Create a System Admin Report" : function (browser) {
		var createReportBtn = "//a[@id='btnAddScheduleReport']";
		var reportTypeOpt = "//input[@id='reportTypeID']";
		var reportType = "//ul//a[text() = 'Paystation Inventory Report']";
		var nextStep1 = "//a[@id='btnStep2Select']";
		var nextStep2 = "//a[@id='btnStep3Select']";
		browser
		.useXpath()
		.waitForElementVisible(createReportBtn, 1000)
		.click(createReportBtn)
		.pause(1000)
		.waitForElementVisible(reportTypeOpt, 1000)
		.click(reportTypeOpt)
		.pause(1000)
		.waitForElementVisible(reportType, 1000)
		.click(reportType)
		.pause(1000)
		.waitForElementVisible(nextStep1, 1000)
		.click(nextStep1)
		.pause(2000)
		.waitForElementVisible(nextStep2, 1000)
		.click(nextStep2)
		.pause(1000);		
	},
	
	/**
	 * @description Verify invalid character message warning for system admin report email input
	 * @param browser - The web browser object
	 * @returns void
	 */
	 
	"test03EmailInputSystemAdmin: Verify Invalid Character Message Warning for System Admin Report Email Input" : function (browser) {
		var emailForm = "//input[@id='formNewEmail']";
		var alertBox = "//section[@id='messageResponseAlertBox']";
		var attentionNotice = "//article[contains(., 'This field only accepts Letters, digits and special characters (except space, comma and special characters ')]";
		var closeNotice = "//a[@title='Close']";
		var invalidChars = [" ", ",", ":", ";", "\\", "<", ">"];
		for (var i = 0; i < invalidChars.length; i++) {
			browser
			.useXpath()
			.waitForElementVisible(emailForm, 1000)
			.setValue(emailForm, invalidChars[i])
			.pause(1000)
			.waitForElementVisible(alertBox, 1000)
			.assert.visible(attentionNotice)
			.click(closeNotice)
			.pause(1000);
		}
	},
	
	/**
	 * @description Logs out
	 * @param browser -The web browser object
	 * @returns void
	 */
	 
	"test03EmailInputSystemAdmin: Logout" : function (browser) {
		browser.logout();
	},

};
