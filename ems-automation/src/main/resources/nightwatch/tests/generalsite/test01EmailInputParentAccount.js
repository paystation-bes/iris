/**
 * @summary EMS-6283 - Email inputs in a parent account will throw an invalid character warning 
 * @since 7.4.2
 * @version 1.0
 * @author Mandy F
 * @see EMS-6283
 **/

var data;
try {
	data = require('../../data.js');
} catch (error) {
	data = require('nightwatch/data.js');
}
var devurl = data("URL");
var username = data("ParentUserName");
var password = data("Password");

module.exports = {

	tags : ['completetesttag', 'bugresolutiontag'],

	/**
	 * @description Logs the parent user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test01EmailInputParentAccount: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password");
	},

	/**
	 * @description Navigates to the Reports tab
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test01EmailInputParentAccount: Navigates to Reports Tab" : function (browser) {
		browser
		.navigateTo("Reports")
		.pause(1000);
	},	
	
	/**
	 * @description Create a Report
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test01EmailInputParentAccount: Create a Report" : function (browser) {
		var createReportBtn = "//a[@id='btnAddScheduleReport']";
		var reportTypeOpt = "//input[@id='reportTypeID']";
		var reportType = "//ul//a[text() = 'Transaction - All']";
		var nextStep1 = "//a[@id='btnStep2Select']";
		var nextStep2 = "//a[@id='btnStep3Select']";
		browser
		.useXpath()
		.waitForElementVisible(createReportBtn, 1000)
		.click(createReportBtn)
		.pause(1000)
		.waitForElementVisible(reportTypeOpt, 1000)
		.click(reportTypeOpt)
		.pause(1000)
		.waitForElementVisible(reportType, 1000)
		.click(reportType)
		.pause(1000)
		.waitForElementVisible(nextStep1, 1000)
		.click(nextStep1)
		.pause(2000)
		.waitForElementVisible(nextStep2, 1000)
		.click(nextStep2)
		.pause(1000);		
	},	
	
	/**
	 * @description Verify invalid character message warning for report email input
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test01EmailInputParentAccount: Verify Invalid Character Message Warning for Report Email Input" : function (browser) {
		var emailForm = "//input[@id='formNewEmail']";
		var alertBox = "//section[@id='messageResponseAlertBox']";
		var attentionNotice = "//article[contains(., 'This field only accepts Letters, digits and special characters (except space, comma and special characters ')]";
		var closeNotice = "//a[@title='Close']";
		var invalidChars = [" ", ",", ":", ";", "\\", "<", ">"];
		for (var i = 0; i < invalidChars.length; i++) {
			browser
			.useXpath()
			.waitForElementVisible(emailForm, 1000)
			.setValue(emailForm, invalidChars[i])
			.pause(1000)
			.waitForElementVisible(alertBox, 1000)
			.assert.visible(attentionNotice)
			.click(closeNotice)
			.pause(1000);
		}		
	},

	/**
	 * @description Logs out
	 * @param browser -The web browser object
	 * @returns void
	 */
	"test01EmailInputParentAccount: Logout" : function (browser) {
		browser.logout();
	},

};
