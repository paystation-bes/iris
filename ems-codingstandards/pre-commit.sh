#!/bin/sh
#
# ------------------------------------------------------------------
# Author: mikev
# Last change Date: 03/17/2015
# Release: 2.0
#
# This script is a git pre-commit hook for:
# - applying checkstyle rules on the java files
# - applying jshint rules on the javascript files
# ------------------------------------------------------------------

source .git/hooks/pre-commit.config

PROJECT_PATH=`pwd` # get the path of the project
CHECKSTYLE_GOOD_RESULT="Starting audit... Audit done."


commit=true
for file in $(git diff --cached --name-only --diff-filter=ACM | grep '.java$')
do
   echo "-------------------------------------"	
   echo "START checkstyle for ... ${file}"
   echo ""
   command=("java -jar ${CHECKSTYLE_JAR} -c ${CHECKSTYLE_XML} ${PROJECT_PATH}/${file}")
   result=`eval $command`
   echo $result
   echo ""
   if [ ${#result} -eq ${#CHECKSTYLE_GOOD_RESULT} ]
   then
      echo "END checkstyle for ... ${file} ... PASS"
      echo "-------------------------------------"
      
      echo "START pmd for ... ${file}"
      echo ""
      cd .git/hooks
      command=("cmd //c run_pmd.bat ${PMD_PATH}/ ${PROJECT_PATH}/${file} ${PMD_RULESET}")
      result=`eval $command`
      echo $result
      # calculate the # of chars displayed in terminal if no PMD errors && add line termination
      cmdString="${PROJECT_PATH}/.git/hooks>${PMD_PATH}/pmd -d ${PROJECT_PATH}/${file} -f text -R ${PMD_RULESET}\n"
      echo ""
      if [ ${#result} -gt ${#cmdString} ]
      then
          echo "END pmd check for ... ${file} ... FAIL"
          commit=false
          break
      else
          echo "END pmd check for ... ${file} ... PASS"
          echo "-------------------------------------"

	  if [ ${USE_FINDBUGS} = true ]
	  then
	  	echo "START findbugs for ... ${file}"
	  	echo ""
	  	javaFile=${file##*/}
	  	# Strip out .java string
	  	class=${javaFile%.java}
          	echo "java -jar ${FINDBUGS_JAR} -textui ${FINDBUGS_TARGET}/${class}*" > run_findbugs.bat
          	cmdString="\n${PROJECT_PATH}/.git/hooks>java -jar ${FINDBUGS_JAR} -textui ${FINDBUGS_TARGET}/${class}*\r\n"
	  	command=("cmd //c run_findbugs.bat")
	  	# redirect to both the terminal and the file
	  	exec ${command} 2>&1 | tee findbugs_result.txt
	  	noOfBytesInResult=$(wc -c < findbugs_result.txt)
          	if [ ${noOfBytesInResult} -gt ${#cmdString} ]
	  	then
	      		echo "END findbugs check for ... ${file} ... FAIL"
              		commit=false
              		break
         	else
	      		echo "END findbugs check for ... ${file} ... PASS"
              		echo "-------------------------------------"
	 	fi
	    else
    		echo "findbugs ckeck ... ABORT"		    
	    fi
      fi
      cd ${PROJECT_PATH}
   else
      echo "END checkstyle for ... ${file} ... FAIL"
      commit=false
      break
   fi 
done

# start checking js files
files=$(git diff --cached --name-only --diff-filter=ACMR -- *.js **/*.js)
echo " "
if [ "$files" != "" ]; then
    for file in ${files}; do
	stringResult="${file} is OK. "
        result=$(jslint ${file})
        if [ ${#result} != ${#stringResult} ]; then
	    echo "***** START jslint for ... ${file} *****"	
	    echo -e $result | tr "#" "\n#"
            commit=false
    	    echo "***** END jslint for ... ${file} *******"
        else
	    echo -e $result
    	fi
        echo ""
    done
fi

if [ $commit = false ]
then
   echo "-------------------------------------"	
   echo "SORRY, COMMIT FAILED ... DO NOT SHOOT THE MESSENGER!!!"
   exit 1
else
   exit 0  	
fi

