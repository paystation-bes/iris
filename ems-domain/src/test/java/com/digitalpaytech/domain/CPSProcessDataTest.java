package com.digitalpaytech.domain;

import org.junit.*;
import static org.junit.Assert.*;
import java.util.*;

public class CPSProcessDataTest {
    @Test
    public void isExpired() {
        final CPSProcessData data = new CPSProcessData();
        assertTrue(data.isExpired());
        
        data.setProcessedGMT(new Date());
        assertFalse(data.isExpired());
        data.setReferenceNumber("123");
        assertFalse(data.isExpired());
        data.setAuthorizationNumber("435");
        assertFalse(data.isExpired());
        data.setProcessorTransactionId("ABC");
        assertFalse(data.isExpired());
    }
}
