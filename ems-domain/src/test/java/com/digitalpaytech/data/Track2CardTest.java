package com.digitalpaytech.data;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class Track2CardTest {

    @Test
    public void getValidCreditCardPanAndExpiry() {
        final String panAndExp = Track2Card.getCreditCardPanAndExpiry("4111411141114111=1901434800008000800");
        assertEquals("4111411141114111=1901", panAndExp);
        
        final String panAndExpMC = Track2Card.getCreditCardPanAndExpiry("5411117437552411=2011111623532000800");
        assertEquals("5411117437552411=2011", panAndExpMC);
        
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void getInvalidCreditCardPanAndExpiry() {
        final String nullCardData = null;
        Track2Card.getCreditCardPanAndExpiry(nullCardData);
        
        final String invalidCardData = "41114111411141111901434800008000800";
        Track2Card.getCreditCardPanAndExpiry(invalidCardData);
    }
}
