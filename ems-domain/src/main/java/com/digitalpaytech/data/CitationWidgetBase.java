package com.digitalpaytech.data;

abstract public class CitationWidgetBase implements java.io.Serializable {
    
    private static final long serialVersionUID = 2687183328757108255L;
    
    public abstract int getTotalCitation();
    
    public abstract void setTotalCitation(final int totalCitation);
    
}
