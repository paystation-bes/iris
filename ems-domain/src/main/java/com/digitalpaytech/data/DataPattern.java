package com.digitalpaytech.data;

import java.io.Serializable;
import java.util.regex.Pattern;

public class DataPattern implements Serializable {
	/**
     * 
     */
    private static final long serialVersionUID = 5161917529095476608L;
    private Pattern track2Pattern;
	private Pattern accountNumberPattern;

	public DataPattern(String track2PatternString) {
		track2Pattern = Pattern.compile(track2PatternString);
		accountNumberPattern = Pattern.compile(extractAccountNumber(track2PatternString, Track2Card.TRACK_2_DELIMITER));
	}
	
	// Copies from EMS 6.3.11 com.digitalpioneer.util.Track2Card
	private static String extractAccountNumber(String track2Data, char delimiter) {
		int iPos = track2Data.indexOf(delimiter);
		if (iPos > 0) {
			return track2Data.substring(0, iPos);
		}
		return track2Data;
	}
	
	
	public Pattern getTrack2Pattern() {
    	return track2Pattern;
    }
	public void setTrack2Pattern(Pattern track2Pattern) {
    	this.track2Pattern = track2Pattern;
    }
	public Pattern getAccountNumberPattern() {
    	return accountNumberPattern;
    }
	public void setAccountNumberPattern(Pattern accountNumberPattern) {
    	this.accountNumberPattern = accountNumberPattern;
    }
}
