package com.digitalpaytech.data;

public class ElavonBatchInfo implements java.io.Serializable {
    private static final long serialVersionUID = -7933670292590232235L;
    private Integer activeBatchSummaryId;
    private Short batchNumber;
    private Integer merchantAccountId;
    private Integer noOfTransactions;

    
    public final Integer getActiveBatchSummaryId() {
        return this.activeBatchSummaryId;
    }

    public final void setActiveBatchSummaryId(final Integer activeBatchSummaryId) {
        this.activeBatchSummaryId = activeBatchSummaryId;
    }

    public final Short getBatchNumber() {
        return this.batchNumber;
    }

    public final void setBatchNumber(final Short batchNumber) {
        this.batchNumber = batchNumber;
    }

    public final Integer getMerchantAccountId() {
        return this.merchantAccountId;
    }

    public final void setMerchantAccountId(final Integer merchantAccountId) {
        this.merchantAccountId = merchantAccountId;
    }

    public final Integer getNoOfTransactions() {
        return this.noOfTransactions;
    }

    public final void setNoOfTransactions(final Integer noOfTxn) {
        this.noOfTransactions = noOfTxn;
    }
}
