package com.digitalpaytech.data;

import java.io.Serializable;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class RolePermissionInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8704107528658702679L;
	private Integer roleId;
	private String roleName;
	private Integer status;
	private Integer parentPermissionId;
	private String parentPermissionName;
	private Integer childPermissionId;
	private String childPermissionName;

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Integer getParentPermissionId() {
		return parentPermissionId;
	}

	public void setParentPermissionId(Integer parentPermissionId) {
		this.parentPermissionId = parentPermissionId;
	}

	public String getParentPermissionName() {
		return parentPermissionName;
	}

	public void setParentPermissionName(String parentPermissionName) {
		this.parentPermissionName = parentPermissionName;
	}

	public Integer getChildPermissionId() {
		return childPermissionId;
	}

	public void setChildPermissionId(Integer childPermissionId) {
		this.childPermissionId = childPermissionId;
	}

	public String getChildPermissionName() {
		return childPermissionName;
	}

	public void setChildPermissionName(String childPermissionName) {
		this.childPermissionName = childPermissionName;
	}
}
