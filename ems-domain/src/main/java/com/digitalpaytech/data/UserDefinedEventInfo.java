package com.digitalpaytech.data;

import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.PosEventCurrent;

@SuppressWarnings({ "checkstyle:designforextension" })
public class UserDefinedEventInfo implements java.io.Serializable {
    
    private static final long serialVersionUID = 2235758368404927116L;
    private static final int SEVERITY_CLEAR = 0;
    private static final int SEVERITY_MINOR = 1;
    private static final int SEVERITY_MAJOR = 2;
    private static final int SEVERITY_CRITICAL = 3;
    
    protected Long posEventCurrentId;
    protected PosEventCurrent posEventCurrent;
    protected CustomerAlertType customerAlertType;
    protected Integer pointOfSaleId;
    
    protected Double gapToMinor;
    protected Double gapToMajor;
    protected Double gapToCritical;
    
    public Long getPosEventCurrentId() {
        return this.posEventCurrentId;
    }
    
    public void setPosEventCurrentId(final Long posEventCurrentId) {
        this.posEventCurrentId = posEventCurrentId;
    }
    
    public void setGapToMinor(final Double gapToMinor) {
        this.gapToMinor = gapToMinor;
    }
    
    public void setGapToMajor(final Double gapToMajor) {
        this.gapToMajor = gapToMajor;
    }
    
    public void setGapToCritical(final Double gapToCritical) {
        this.gapToCritical = gapToCritical;
    }
    
    public PosEventCurrent getPosEventCurrent() {
        return this.posEventCurrent;
    }
    
    public void setPosEventCurrent(final PosEventCurrent posEventCurrent) {
        this.posEventCurrent = posEventCurrent;
    }
    
    public Double getGapToMinor() {
        return this.gapToMinor;
    }
    
    public void setGapToMinor(final Number gapToMinor) {
        this.gapToMinor = new Double(gapToMinor.toString());
    }
    
    public Double getGapToCritical() {
        return this.gapToCritical;
    }
    
    public void setGapToCritical(final Number gapToCritical) {
        this.gapToCritical = new Double(gapToCritical.toString());
    }
    
    public Double getGapToMajor() {
        return this.gapToMajor;
    }
    
    public void setGapToMajor(final Number gapToMajor) {
        this.gapToMajor = new Double(gapToMajor.toString());
    }
    
    public int calculateSeverity() {
        int severity;
        if (this.gapToCritical.doubleValue() < 0) {
            severity = SEVERITY_CRITICAL;
        } else if (this.gapToMajor < 0) {
            severity = SEVERITY_MAJOR;
        } else if (this.gapToMinor.doubleValue() < 0) {
            severity = SEVERITY_MINOR;
        } else {
            severity = SEVERITY_CLEAR;
        }
        
        return severity;
    }
    
    public CustomerAlertType getCustomerAlertType() {
        return this.customerAlertType;
    }
    
    public void setCustomerAlertType(final CustomerAlertType customerAlertType) {
        this.customerAlertType = customerAlertType;
    }
    
    public Integer getPointOfSaleId() {
        return this.pointOfSaleId;
    }
    
    public void setPointOfSaleId(final Integer pointOfSaleId) {
        this.pointOfSaleId = pointOfSaleId;
    }
}
