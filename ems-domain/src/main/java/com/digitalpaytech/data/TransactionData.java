package com.digitalpaytech.data;

import com.digitalpaytech.domain.ExtensiblePermit;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.SingleTransactionCardData;
import com.digitalpaytech.domain.SmsAlert;

public class TransactionData implements java.io.Serializable {
    private static final long serialVersionUID = 7843843840726251619L;
    private PointOfSale pointOfSale;
    private Purchase purchase;
    private Permit permit;
    private ProcessorTransaction processorTransaction;
    private SmsAlert smsAlert;
    private ExtensiblePermit extensiblePermit;
    private PosServiceState posServiceState;
    private SingleTransactionCardData singleTransactionCardData;
    private PreAuth preAuth;
    
    private String version;
    private boolean uploadFromBoss;
    private boolean isOffline;
    private boolean isStoreForward;
    private boolean isSingleTransaction;
    private boolean isCancelledTransaction;
    private boolean isForRecoverable;
    private boolean isEMV;
    private boolean isCoreCPS;
    private String authorizationNumber;
    private String cardType;
    
    public TransactionData(PointOfSale pointOfSale, Purchase purchase, ProcessorTransaction processorTransaction, SmsAlert smsAlert,
            ExtensiblePermit extensiblePermit, PosServiceState posServiceState, String version) {
        this.pointOfSale = pointOfSale;
        this.purchase = purchase;
        this.processorTransaction = processorTransaction;
        this.smsAlert = smsAlert;
        this.extensiblePermit = extensiblePermit;
        this.posServiceState = posServiceState;
        this.version = version;
    }
    
    public TransactionData(PointOfSale pointOfSale, Purchase purchase, Permit permit, ProcessorTransaction processorTransaction, SmsAlert smsAlert,
            ExtensiblePermit extensiblePermit, PosServiceState posServiceState, String version) {
        this.pointOfSale = pointOfSale;
        this.purchase = purchase;
        this.permit = permit;
        this.processorTransaction = processorTransaction;
        this.smsAlert = smsAlert;
        this.extensiblePermit = extensiblePermit;
        this.posServiceState = posServiceState;
        this.version = version;
    }
    
    public TransactionData(PointOfSale pointOfSale, Purchase purchase, ProcessorTransaction processorTransaction, PosServiceState posServiceState) {
        this.pointOfSale = pointOfSale;
        this.purchase = purchase;
        this.processorTransaction = processorTransaction;
        this.posServiceState = posServiceState;
    }
    
    public PointOfSale getPointOfSale() {
        return pointOfSale;
    }
    
    public void setPointOfSale(PointOfSale pointOfSale) {
        this.pointOfSale = pointOfSale;
    }
    
    public Purchase getPurchase() {
        return purchase;
    }
    
    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }
    
    public Permit getPermit() {
        return permit;
    }
    
    public void setPermit(Permit permit) {
        this.permit = permit;
    }
    
    public ProcessorTransaction getProcessorTransaction() {
        return processorTransaction;
    }
    
    public void setProcessorTransaction(ProcessorTransaction processorTransaction) {
        this.processorTransaction = processorTransaction;
    }
    
    public SmsAlert getSmsAlert() {
        return smsAlert;
    }
    
    public void setSmsAlert(SmsAlert smsAlert) {
        this.smsAlert = smsAlert;
    }
    
    public ExtensiblePermit getExtensiblePermit() {
        return extensiblePermit;
    }
    
    public void setExtensiblePermit(ExtensiblePermit extensiblePermit) {
        this.extensiblePermit = extensiblePermit;
    }
    
    public PosServiceState getPosServiceState() {
        return posServiceState;
    }
    
    public void setPosServiceState(PosServiceState posServiceState) {
        this.posServiceState = posServiceState;
    }
    
    public SingleTransactionCardData getSingleTransactionCardData() {
        return singleTransactionCardData;
    }
    
    public void setSingleTransactionCardData(SingleTransactionCardData singleTransactionCardData) {
        this.singleTransactionCardData = singleTransactionCardData;
    }
    
    public PreAuth getPreAuth() {
        return preAuth;
    }
    
    public void setPreAuth(PreAuth preAuth) {
        this.preAuth = preAuth;
    }
    
    public String getVersion() {
        return version;
    }
    
    public void setVersion(String version) {
        this.version = version;
    }
    
    public boolean isUploadFromBoss() {
        return uploadFromBoss;
    }
    
    public void setUploadFromBoss(boolean uploadFromBoss) {
        this.uploadFromBoss = uploadFromBoss;
    }
    
    public boolean isOffline() {
        return isOffline;
    }
    
    public void setOffline(boolean isOffline) {
        this.isOffline = isOffline;
    }
    
    public boolean isStoreForward() {
        return isStoreForward;
    }
    
    public void setStoreForward(boolean isStoreForward) {
        this.isStoreForward = isStoreForward;
    }
    
    public boolean isSingleTransaction() {
        return isSingleTransaction;
    }
    
    public void setSingleTransaction(boolean isSingleTransaction) {
        this.isSingleTransaction = isSingleTransaction;
    }
    
    public boolean isCancelledTransaction() {
        return isCancelledTransaction;
    }
    
    public void setCancelledTransaction(boolean isCancelledTransaction) {
        this.isCancelledTransaction = isCancelledTransaction;
    }
    
    public boolean isForRecoverable() {
        return isForRecoverable;
    }
    
    public void setForRecoverable(boolean isForRecoverable) {
        this.isForRecoverable = isForRecoverable;
    }
    
    public boolean isEMV() {
        return isEMV;
    }
    
    public void setEMV(boolean isEMV) {
        this.isEMV = isEMV;
    }
    
    public String getAuthorizationNumber() {
        return authorizationNumber;
    }
    
    public void setAuthorizationNumber(String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }
    
    public boolean isCoreCPS() {
        return isCoreCPS;
    }
    
    public void setCoreCPS(boolean isCoreCPS) {
        this.isCoreCPS = isCoreCPS;
    }

    public boolean isEMVOrCoreCPS() {
        return isEMV || isCoreCPS;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
}
