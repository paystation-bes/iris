package com.digitalpaytech.data;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import com.digitalpaytech.domain.Widget;

public class SectionColumn implements Serializable {
	/**
     * 
     */
    private static final long serialVersionUID = 7823741077165079380L;
	private int columnId;
	private List<Widget> widgets;

	public SectionColumn() {
		widgets = new ArrayList<Widget>();
	}
	
	public List<Widget> getWidgets() {
    	return widgets;
    }

	public void setWidgets(int columnId, List<Widget> widgets) {
		setColumnId(columnId);
		Iterator<Widget> iter = widgets.iterator();
		while (iter.hasNext()) {
			Widget w = iter.next();
			w.setColumnId(columnId);
		}
    	this.widgets = widgets;
    }
	
	public void clear() {
		widgets.clear();
	}
	
	public int size() {
		return widgets.size();
	}
	
	public void add(Widget widget) {
		widgets.add(widget);
		setColumnId(widget.getColumnId());
	}
	
	public void remove(String widgetRandomId) {
		Widget widget = null;
		for (Widget wig : widgets) {
			if (StringUtils.isNotBlank(wig.getRandomId()) && wig.getRandomId().equalsIgnoreCase(widgetRandomId)) {
				widget = wig;
				break;
			}
		}
		if (widget != null) {
			widgets.remove(widget);
		}
	}
	
	public void remove(Widget widget) {
		widgets.remove(widget);
	}
	
	public void removeAll() {
		widgets.removeAll(widgets);
	}

	public int getColumnId() {
    	return columnId;
    }

	public void setColumnId(int columnId) {
    	this.columnId = columnId;
    }
}
