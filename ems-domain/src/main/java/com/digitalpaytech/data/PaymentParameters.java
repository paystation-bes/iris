package com.digitalpaytech.data;

import java.util.UUID;

import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PreAuth;
import com.digitalpaytech.domain.ProcessorTransaction;

public class PaymentParameters {
    private PreAuth preAuth;
    private ProcessorTransaction processorTransaction;
    private PointOfSale pointOfSale;
    private String cardData;
    private UUID chargeToken;
    private String cardTypeName;
    private MerchantAccount merchantAccount;
    
    /**
     * @param preAuth
     * @param processorTransaction
     * @param pointOfSale
     * @param merchantAccount
     * @param cardData
     * @param chargeToken
     * @param cardTypeName
     */
    public PaymentParameters(final PreAuth preAuth, final ProcessorTransaction processorTransaction, final PointOfSale pointOfSale,
            final MerchantAccount merchantAccount, final String cardData, final UUID chargeToken, final String cardTypeName) {
        this.preAuth = preAuth;
        this.processorTransaction = processorTransaction;
        this.pointOfSale = pointOfSale;
        this.merchantAccount = merchantAccount;
        this.cardData = cardData;
        this.chargeToken = chargeToken;
        this.cardTypeName = cardTypeName;
    }
    
    
    public final MerchantAccount getMerchantAccount() {
        return this.merchantAccount;
    }
    
    public final void setMerchantAccount(final MerchantAccount merchantAccount) {
        this.merchantAccount = merchantAccount;
    }
    
    public final PreAuth getPreAuth() {
        return this.preAuth;
    }
    
    public final void setPreAuth(final PreAuth preAuth) {
        this.preAuth = preAuth;
    }
    
    public final ProcessorTransaction getProcessorTransaction() {
        return this.processorTransaction;
    }
    
    public final void setProcessorTransaction(final ProcessorTransaction processorTransaction) {
        this.processorTransaction = processorTransaction;
    }
    
    public final PointOfSale getPointOfSale() {
        return this.pointOfSale;
    }
    
    public final void setPointOfSale(final PointOfSale pointOfSale) {
        this.pointOfSale = pointOfSale;
    }
    
    public final String getCardData() {
        return this.cardData;
    }
    
    public final void setCardData(final String cardData) {
        this.cardData = cardData;
    }
    
    public final UUID getChargeToken() {
        return this.chargeToken;
    }
    
    public final void setChargeToken(final UUID chargeToken) {
        this.chargeToken = chargeToken;
    }
    
    public final String getCardTypeName() {
        return this.cardTypeName;
    }
    
    public final void setCardTypeName(final String cardTypeName) {
        this.cardTypeName = cardTypeName;
    }
}
