package com.digitalpaytech.data;

import com.digitalpaytech.domain.MerchantAccount;

public class InfoAndMerchantAccount {
    private String info;
    private MerchantAccount merchantAccount;
    
    public InfoAndMerchantAccount(String info, MerchantAccount merchantAccount) {
        this.info = info;
        this.merchantAccount = merchantAccount;
    }

    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }
    public MerchantAccount getMerchantAccount() {
        return merchantAccount;
    }
    public void setMerchantAccount(MerchantAccount merchantAccount) {
        this.merchantAccount = merchantAccount;
    }
}
