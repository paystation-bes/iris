package com.digitalpaytech.data;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import com.digitalpaytech.domain.CreditCardType;
import com.digitalpaytech.domain.CustomerCardType;

// Copies from EMS 6.3.11 com.digitalpioneer.util.Track2Card

public class Track2Card implements Serializable {
    
    public static final char TRACK_2_DELIMITER = '=';
    public static final int INTERNAL_TYPE_UNKNOWN = 0;
    public static final int INTERNAL_TYPE_PATROLLER_CARD = 3;
    public static final int INTERNAL_TYPE_CREDIT_CARD = 1;
    public static final int INTERNAL_TYPE_CUSTOM_CARD = 3;
    
    private static final long serialVersionUID = 5179989165453737313L;
    
    private String decryptedCardData;
    private int customerId;
    private int accountNumberLength;
    private int cardTypeId;
    private String customerCardTypeName;
    private CustomerCardType customerCardType;
    private CreditCardType creditCardType;
    private String cardToken;
    
    public Track2Card(final String cardToken) {
        this.setCardToken(cardToken);
        this.decryptedCardData = null;
    }
    
    public Track2Card(final CustomerCardType customerCardType, final int customerId, final String decryptedCardData) {
        if (StringUtils.isEmpty(decryptedCardData)) {
            this.decryptedCardData = "";
        } else {
            this.decryptedCardData = decryptedCardData.trim();
            this.customerCardType = customerCardType;
            this.accountNumberLength = decryptedCardData.indexOf(TRACK_2_DELIMITER);
            if (this.accountNumberLength < 0) {
                this.accountNumberLength = decryptedCardData.length();
            }
        }
        this.customerId = customerId;
    }
    
    public Track2Card(final CreditCardType creditCardType, final int customerId, final String decryptedCardData) {
        if (StringUtils.isEmpty(decryptedCardData)) {
            this.decryptedCardData = "";
        } else {
            this.decryptedCardData = decryptedCardData.trim();
            this.accountNumberLength = decryptedCardData.indexOf(TRACK_2_DELIMITER);
            if (this.accountNumberLength < 0) {
                this.accountNumberLength = decryptedCardData.length();
            }
        }
        this.customerId = customerId;
        this.creditCardType = creditCardType;
    }
    
    public String getPrimaryAccountNumber() {
        if (this.accountNumberLength > 0 && StringUtils.isNotBlank(this.decryptedCardData)) {
            return this.decryptedCardData.substring(0, this.accountNumberLength);
        }
        return this.decryptedCardData;
    }
    
    // For credit card track2, there must be 7 or more digits after equals sign
    // So for our purposes, if there are more than 4 digits after equals sign
    // will be sufficient
    public boolean isCardDataCreditCardTrack2Data() {
        if (isCreditCard()) {
            final String afterEquals = this.decryptedCardData.substring(this.accountNumberLength + 1);
            return afterEquals.length() > 4;
        }
        return false;
    }
    
    public boolean isCreditCard() {
        if (this.customerCardType != null) {
            return this.customerCardType.getCardType().getId() == INTERNAL_TYPE_CREDIT_CARD;
        } else if (this.creditCardType != null) {
            return true;
        }
        return false;
    }
    
    public boolean isCustomCard() {
        if (this.customerCardType != null && this.customerCardType.getCardType().getId() == INTERNAL_TYPE_CUSTOM_CARD) {
            return true;
        }
        return false;
    }
    
    public boolean isPatrollerCard() {
        if (this.customerCardType != null && this.customerCardType.getCardType().getId() == INTERNAL_TYPE_PATROLLER_CARD) {
            return true;
        }
        return false;
    }
    
    public String getExpirationMonth() {
        if ((this.customerCardType != null && this.customerCardType.getCardType().getId() == INTERNAL_TYPE_CREDIT_CARD)
            || this.creditCardType != null) {
            if (this.decryptedCardData.length() < this.accountNumberLength + 5) {
                throw new IllegalArgumentException("Invalid Credit Card - Missing Expiration Data");
            }
            return this.decryptedCardData.substring(this.accountNumberLength + 3, this.accountNumberLength + 5);
        }
        return null;
    }
    
    public String getExpirationYear() {
        if ((this.customerCardType != null && this.customerCardType.getCardType().getId() == INTERNAL_TYPE_CREDIT_CARD)
            || this.creditCardType != null) {
            if (this.decryptedCardData.length() < this.accountNumberLength + 3) {
                throw new IllegalArgumentException("Invalid Credit Card - Missing Expiration Data");
            }
            return this.decryptedCardData.substring(this.accountNumberLength + 1, this.accountNumberLength + 3);
        }
        
        return null;
    }
    
    public String getDisplayCardNumber() {
        final String pan = getPrimaryAccountNumber();
        
        if (isPatrollerCard()) {
            if (this.accountNumberLength > 4) {
                return pan.substring(this.accountNumberLength - 4, this.accountNumberLength);
            }
            return pan;
        } else if (isCreditCard()) {
            final StringBuffer accountNumber = new StringBuffer(pan);
            for (int i = 0; i < (this.accountNumberLength - 4); i++) {
                accountNumber.setCharAt(i, 'X');
            }
            return accountNumber.toString();
        } else {
            final StringBuffer accountNumber = new StringBuffer(pan);
            for (int i = 4; i < (this.accountNumberLength - 4); i++) {
                accountNumber.setCharAt(i, 'X');
            }
            return accountNumber.toString();
        }
    }
    
    public String getCreditCardPanAndExpiry() {
        if ((this.customerCardType != null && this.customerCardType.getCardType().getId() == INTERNAL_TYPE_CREDIT_CARD)
            || (this.creditCardType != null && this.creditCardType.isIsValid())) {
            if (this.decryptedCardData.length() < this.accountNumberLength + 5) {
                throw new IllegalArgumentException("Track2Card, getCreditCardPanAndExpiry, Invalid Credit Card - Missing Expiration Data");
            }
            return this.decryptedCardData.substring(0, this.accountNumberLength + 5);
        }
        
        // Card Processing hash is only used when doing a refund search for credit cards
        // We store the value card data in transaction table which can be used for
        // searching if so desired.
        return "";
    }
    
    public static final String getCreditCardPanAndExpiry(final String decryptedCardData) {
        if (StringUtils.isBlank(decryptedCardData) || decryptedCardData.indexOf(TRACK_2_DELIMITER) == -1) {
            throw new IllegalArgumentException("Invalid Track2 - it's null or invalid format.");
        }
        return decryptedCardData.substring(0, decryptedCardData.indexOf(TRACK_2_DELIMITER) + 5);
    }
    
    public short getLast4DigitsOfAccountNumber() {
        try {
            if (this.decryptedCardData.length() == 0) {
                return (0);
            } else if (this.accountNumberLength > 4) {
                return Short.parseShort(this.decryptedCardData.substring(this.accountNumberLength - 4, this.accountNumberLength));
            }
            return Short.parseShort(this.decryptedCardData.substring(0, this.accountNumberLength));
        } catch (NumberFormatException nfe) {
            return 0;
        }
    }
    
    public static String getLast4DigitsOfAccountNumber(final String track2Data, final char delimiter) {
        final String accountNumber = extractAccountNumber(track2Data, delimiter);
        if (accountNumber.length() > 4) {
            return accountNumber.substring(accountNumber.length() - 4, accountNumber.length());
        }
        return accountNumber;
    }
    
    public static String extractAccountNumber(final String track2Data, final char delimiter) {
        final int iPos = track2Data.indexOf(delimiter);
        if (iPos > 0) {
            return track2Data.substring(0, iPos);
        }
        return track2Data;
    }
    
    public String getDisplayName() {
        if (this.customerCardType != null) {
            return this.customerCardType.getName();
        } else if (creditCardType != null) {
            return creditCardType.getName();
        } else {
            return "N/A";
        }
    }
    
    /**
     * Service Code is the three-digit after the expiry date on track 2.
     * 
     * @return String return null if unable to extract service code.
     */
    public final String getServiceCode() {
        if (!isCardDataCreditCardTrack2Data()) {
            return null;
        }
        final String afterEqual = this.decryptedCardData.substring(this.accountNumberLength + 5);
        return afterEqual.substring(0, 3);
    }
    
    public String getDecryptedCardData() {
        return this.decryptedCardData;
    }
    
    public int getCustomerId() {
        return this.customerId;
    }
    
    public int getAccountNumberLength() {
        return this.accountNumberLength;
    }
    
    public int getCardTypeId() {
        return this.cardTypeId;
    }
    
    public String getCustomerCardTypeName() {
        return this.customerCardTypeName;
    }
    
    public void setCustomerCardTypeName(final String customerCardTypeName) {
        this.customerCardTypeName = customerCardTypeName;
    }
    
    public CustomerCardType getCustomerCardType() {
        return this.customerCardType;
    }
    
    public CreditCardType getCreditCardType() {
        return this.creditCardType;
    }
    
    public String getCardToken() {
        return this.cardToken;
    }
    
    public void setCardToken(final String cardToken) {
        this.cardToken = cardToken;
    }
}
