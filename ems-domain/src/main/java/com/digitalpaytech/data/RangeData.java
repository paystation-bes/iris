package com.digitalpaytech.data;

import java.io.Serializable;
import java.text.MessageFormat;

public class RangeData<T extends Serializable> implements Serializable {
    private static final long serialVersionUID = -1106275507411090137L;
    
    private static final String FMT_TO_STRING = "[{0} to {1}]";
    
    private T start;
    private T end;
    
    public RangeData() {
        
    }
    
    public RangeData(T start, T end) {
        this.start = start;
        this.end = end;
    }
    
    public RangeData(RangeData<T> other) {
        this.start = other.start;
        this.end = other.end;
    }

    public T getStart() {
        return start;
    }

    public void setStart(T start) {
        this.start = start;
    }

    public T getEnd() {
        return end;
    }

    public void setEnd(T end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return MessageFormat.format(FMT_TO_STRING, this.start, this.end);
    }
}
