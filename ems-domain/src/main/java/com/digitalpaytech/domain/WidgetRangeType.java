package com.digitalpaytech.domain;

// Generated 10-May-2012 2:57:12 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.digitalpaytech.annotation.StoryAlias;

/**
 * WidgetRangeType generated by hbm2java
 */
@Entity
@Table(name = "WidgetRangeType")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@StoryAlias(WidgetRangeType.ALIAS)
public class WidgetRangeType implements java.io.Serializable {
    public static final String ALIAS = "Widget Range Type";
    
    public static final String ALIAS_NAME = "Name";
    public static final String ALIAS_LABEL = "Label";
    
    private static final long serialVersionUID = 9067487490548553686L;
    
    private int id;
    private String name;
    private short label;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    private Set<Widget> widgets = new HashSet<Widget>(0);
    private String randomId;
    
    public WidgetRangeType() {
    }

    public WidgetRangeType(final int id, final String name, final short label, final Date lastModifiedGmt,
        final int lastModifiedByUserId) {
        this.id = id;
        this.name = name;
        this.label = label;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }

    public WidgetRangeType(final int id, final String name, final short label, final Date lastModifiedGmt,
        final int lastModifiedByUserId, final Set<Widget> widgets) {
        this.id = id;
        this.name = name;
        this.label = label;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
        this.widgets = widgets;
    }

    @Id
    @Column(name = "Id", nullable = false)
    public int getId() {
        return this.id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    @Column(name = "Name", unique = true, nullable = false, length = 30)
    @StoryAlias(ALIAS_NAME)
    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Column(name = "Label", unique = true, nullable = false)
    @StoryAlias(ALIAS_LABEL)
    public short getLabel() {
        return this.label;
    }

    public void setLabel(final short label) {
        this.label = label;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = 19)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }

    public void setLastModifiedGmt(final Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }

    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }

    public void setLastModifiedByUserId(final int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "widgetRangeType")
    public Set<Widget> getWidgets() {
        return this.widgets;
    }

    public void setWidgets(final Set<Widget> widgets) {
        this.widgets = widgets;
    }

    @Transient
    public String getRandomId() {
        return randomId;
    }

    public void setRandomId(final String randomId) {
        this.randomId = randomId;
    }   
}
