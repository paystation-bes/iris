package com.digitalpaytech.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.domain.util.HibernateConstants;

@Entity
@Table(name = "POSEventDelayed")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({ @NamedQuery(name = "POSEventDelayed.findDelayedGMTPast", query = "FROM PosEventDelayed ped WHERE ped.delayedGmt <= :currentTime"),
    @NamedQuery(name = "POSEventDelayed.deleteByCustomerAlertType", query = "DELETE PosEventDelayed ped WHERE ped.customerAlertType.id = :customerAlertTypeId"),
    @NamedQuery(name = "POSEventDelayed.findByCustomerAlertTypeAndPointOfSaleAndEventType", query = "FROM PosEventDelayed ped WHERE ped.customerAlertType.id = :customerAlertTypeId AND ped.pointOfSale.id = :pointOfSaleId AND ped.eventType.id = :eventTypeId") })
public class PosEventDelayed implements Serializable {
    
    private static final long serialVersionUID = 810689668164748619L;
    private Long id;
    private int version;
    private PointOfSale pointOfSale;
    private EventSeverityType eventSeverityType;
    private EventActionType eventActionType;
    private CustomerAlertType customerAlertType;
    private EventType eventType;
    private String alertInfo;
    private boolean isActive;
    private boolean isHidden;
    private Date alertGmt;
    private Date clearedGmt;
    private Date delayedGmt;
    
    public PosEventDelayed() {
        super();
    }
    
    public PosEventDelayed(final PointOfSale pointOfSale, final EventSeverityType eventSeverityType, final EventActionType eventActionType,
            final CustomerAlertType customerAlertType, final EventType eventType, final String alertInfo, final boolean isActive,
            final boolean isHidden, final Date alertGmt, final Date clearedGmt, final Date delayedGmt) {
        super();
        this.pointOfSale = pointOfSale;
        this.eventSeverityType = eventSeverityType;
        this.eventActionType = eventActionType;
        this.customerAlertType = customerAlertType;
        this.eventType = eventType;
        this.alertInfo = alertInfo;
        this.isActive = isActive;
        this.isHidden = isHidden;
        this.alertGmt = alertGmt;
        this.clearedGmt = clearedGmt;
        this.delayedGmt = delayedGmt;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    @Version
    @Column(name = "VERSION", nullable = false)
    public int getVersion() {
        return this.version;
    }
    
    public void setVersion(final int version) {
        this.version = version;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PointOfSaleId", nullable = false)
    public PointOfSale getPointOfSale() {
        return this.pointOfSale;
    }
    
    public void setPointOfSale(final PointOfSale pointOfSale) {
        this.pointOfSale = pointOfSale;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EventSeverityTypeId", nullable = false)
    public EventSeverityType getEventSeverityType() {
        return this.eventSeverityType;
    }
    
    public void setEventSeverityType(final EventSeverityType eventSeverityType) {
        this.eventSeverityType = eventSeverityType;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EventActionTypeId")
    public EventActionType getEventActionType() {
        return this.eventActionType;
    }
    
    public void setEventActionType(final EventActionType eventActionType) {
        this.eventActionType = eventActionType;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerAlertTypeId")
    public CustomerAlertType getCustomerAlertType() {
        return this.customerAlertType;
    }
    
    public void setCustomerAlertType(final CustomerAlertType customerAlertType) {
        this.customerAlertType = customerAlertType;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EventTypeId", nullable = false)
    public EventType getEventType() {
        return this.eventType;
    }
    
    public void setEventType(final EventType eventType) {
        this.eventType = eventType;
    }
    
    @Column(name = "IsActive", nullable = false)
    public boolean isIsActive() {
        return this.isActive;
    }
    
    public void setIsActive(final boolean isActive) {
        this.isActive = isActive;
    }
    
    @Column(name = "IsHidden", nullable = false)
    public boolean isIsHidden() {
        return this.isHidden;
    }
    
    public void setIsHidden(final boolean isHidden) {
        this.isHidden = isHidden;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "AlertGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getAlertGmt() {
        return this.alertGmt;
    }
    
    public void setAlertGmt(final Date alertGmt) {
        this.alertGmt = alertGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ClearedGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getClearedGmt() {
        return this.clearedGmt;
    }
    
    public void setClearedGmt(final Date clearedGmt) {
        this.clearedGmt = clearedGmt;
    }
    
    public String getAlertInfo() {
        return this.alertInfo;
    }
    
    public void setAlertInfo(final String alertInfo) {
        this.alertInfo = alertInfo;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DelayedGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getDelayedGmt() {
        return this.delayedGmt;
    }
    
    public void setDelayedGmt(final Date delayedGmt) {
        this.delayedGmt = delayedGmt;
    }
    
}
