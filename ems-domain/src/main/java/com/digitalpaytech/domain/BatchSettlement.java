package com.digitalpaytech.domain;

// Generated 4-Jun-2015 7:36:02 AM by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.digitalpaytech.domain.util.HibernateConstants;

/**
 * BatchSettlement generated by hbm2java
 */
@Entity
@Table(name = "BatchSettlement")
@SuppressWarnings("checkstyle:designforextension")
public class BatchSettlement implements java.io.Serializable {
    
    private static final long serialVersionUID = 4018966450649255301L;
    private Long id;
    private Cluster cluster;
    private ActiveBatchSummary activeBatchSummary;
    private BatchSettlementType batchSettlementType;
    private short batchSize;
    private Date batchSettlementStartGmt;
    private Date batchSettlementEndGmt;
    private Set<BatchSettlementDetail> batchSettlementDetails = new HashSet<BatchSettlementDetail>(0);
    
    public BatchSettlement() {
    }
    
    public BatchSettlement(final Cluster cluster, final ActiveBatchSummary activeBatchSummary, final BatchSettlementType batchSettlementType,
            final short batchSize, final Date batchSettlementStartGmt) {
        this.cluster = cluster;
        this.activeBatchSummary = activeBatchSummary;
        this.batchSettlementType = batchSettlementType;
        this.batchSize = batchSize;
        this.batchSettlementStartGmt = batchSettlementStartGmt;
    }
    
    public BatchSettlement(final Cluster cluster, final ActiveBatchSummary activeBatchSummary, final BatchSettlementType batchSettlementType,
            final short batchSize, final Date batchSettlementStartGmt, final Date batchSettlementEndGmt,
            final Set<BatchSettlementDetail> batchSettlementDetails) {
        this.cluster = cluster;
        this.activeBatchSummary = activeBatchSummary;
        this.batchSettlementType = batchSettlementType;
        this.batchSize = batchSize;
        this.batchSettlementStartGmt = batchSettlementStartGmt;
        this.batchSettlementEndGmt = batchSettlementEndGmt;
        this.batchSettlementDetails = batchSettlementDetails;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ClusterId", nullable = false)
    public Cluster getCluster() {
        return this.cluster;
    }
    
    public void setCluster(final Cluster cluster) {
        this.cluster = cluster;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ActiveBatchSummaryId", nullable = false)
    public ActiveBatchSummary getActiveBatchSummary() {
        return this.activeBatchSummary;
    }
    
    public void setActiveBatchSummary(final ActiveBatchSummary activeBatchSummary) {
        this.activeBatchSummary = activeBatchSummary;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BatchSettlementTypeId", nullable = false)
    public BatchSettlementType getBatchSettlementType() {
        return this.batchSettlementType;
    }
    
    public void setBatchSettlementType(final BatchSettlementType batchSettlementType) {
        this.batchSettlementType = batchSettlementType;
    }
    
    @Column(name = "BatchSize", nullable = false)
    public short getBatchSize() {
        return this.batchSize;
    }
    
    public void setBatchSize(final short batchSize) {
        this.batchSize = batchSize;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "BatchSettlementStartGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getBatchSettlementStartGmt() {
        return this.batchSettlementStartGmt;
    }
    
    public void setBatchSettlementStartGmt(final Date batchSettlementStartGmt) {
        this.batchSettlementStartGmt = batchSettlementStartGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "BatchSettlementEndGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getBatchSettlementEndGmt() {
        return this.batchSettlementEndGmt;
    }
    
    public void setBatchSettlementEndGmt(final Date batchSettlementEndGmt) {
        this.batchSettlementEndGmt = batchSettlementEndGmt;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "batchSettlement")
    public Set<BatchSettlementDetail> getBatchSettlementDetails() {
        return this.batchSettlementDetails;
    }
    
    public void setBatchSettlementDetails(final Set<BatchSettlementDetail> batchSettlementDetails) {
        this.batchSettlementDetails = batchSettlementDetails;
    }
    
}
