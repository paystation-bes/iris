package com.digitalpaytech.domain;

// Generated 6-Aug-2014 1:24:49 PM by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

/**
 * RaterateProfile generated by hbm2java
 */
@Entity
@Table(name = "RateRateProfile")
@NamedQueries({
        @NamedQuery(name = "RateRateProfile.findActiveRatesByRateId", query = "SELECT rrp FROM RateRateProfile rrp LEFT JOIN FETCH rrp.rateProfile rp WHERE rrp.rate.id = :rateId AND rrp.isDeleted = 0 AND rp.isDeleted = 0", cacheable = true),
        @NamedQuery(name = "RateRateProfile.findPublishedRatesByRateId", query = "SELECT rrp FROM RateRateProfile rrp LEFT JOIN FETCH rrp.rateProfile rp WHERE rrp.rate.id = :rateId AND rrp.isDeleted = 0 AND rp.isDeleted = 0 AND rp.isPublished = 1", cacheable = true),
        @NamedQuery(name = "RateRateProfile.findActiveRatesByRateProfileId", query = "SELECT rrp FROM RateRateProfile rrp LEFT JOIN FETCH rrp.rateProfile rp WHERE rrp.rateProfile.id = :rateProfileId AND rrp.isDeleted = 0 AND rp.isDeleted = 0", cacheable = true),
        @NamedQuery(name = "RateRateProfile.findPublishedRatesByRateProfileId", query = "SELECT rrp FROM RateRateProfile rrp LEFT JOIN FETCH rrp.rateProfile rp WHERE rrp.rateProfile.id = :rateProfileId AND rrp.isDeleted = 0 AND rp.isDeleted = 0 AND rp.isPublished = 1", cacheable = true),
        @NamedQuery(name = "RateRateProfile.findActiveRatesByRateProfileIdAndDateRange", query = "SELECT rrp FROM RateRateProfile rrp LEFT JOIN FETCH rrp.rateProfile rp LEFT JOIN FETCH rrp.rate r WHERE rrp.rateProfile.id = :rateProfileId AND rrp.isDeleted = 0 AND (rrp.availabilityStartDate IS NULL OR (rrp.availabilityStartDate <= :endDate AND rrp.availabilityEndDate >= :startDate))", cacheable = true),
        @NamedQuery(name = "RateRateProfile.findById", query = "SELECT rrp FROM RateRateProfile rrp WHERE rrp.id = :id", cacheable = true),
        @NamedQuery(name = "RateRateProfile.findPublishedRatesByRateProfileIdsAndDateRange", query =
                "SELECT rrp FROM RateRateProfile rrp " +
                "LEFT JOIN FETCH rrp.rateProfile rp " +
                "LEFT JOIN FETCH rrp.rate r " +
                "WHERE rrp.isDeleted = 0 AND (rrp.availabilityStartDate IS NULL OR (rrp.availabilityStartDate <= :endDate AND rrp.availabilityEndDate >= :startDate)) " +
                "AND rp.id IN(:rateProfileIds) "
                , cacheable = true)
})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class RateRateProfile implements java.io.Serializable {
    
    private static final long serialVersionUID = -9057825221033308767L;
    private Integer id;
    private int version;
    private RateExpiryDayType rateExpiryDayType;
    private Rate rate;
    private RateProfile rateProfile;
    private RateFlatType rateFlatType;
    private Customer customer;
    private String description;
    private boolean isRateBlendingEnabled;
    private boolean isOverride;
    private byte displayPriority;
    private Date availabilityStartDate;
    private Date availabilityEndDate;
    private String availabilityStartTime;
    private String availabilityEndTime;
    private Short durationNumberOfDays;
    private Short durationNumberOfHours;
    private Short durationNumberOfMins;
    private boolean isMaximumExpiryEnabled;
    private String expiryTime;
    private boolean isAdvancePurchaseEnabled;
    private boolean isScheduled;
    private boolean isSunday;
    private boolean isMonday;
    private boolean isTuesday;
    private boolean isWednesday;
    private boolean isThursday;
    private boolean isFriday;
    private boolean isSaturday;
    private boolean isDeleted;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    
    public RateRateProfile() {
    }
    
    public RateRateProfile(Rate rate, RateProfile rateProfile, Customer customer, boolean isRateBlendingEnabled, boolean isOverride, byte displayPriority,
            boolean isMaximumExpiryEnabled, boolean isAdvancePurchaseEnabled, boolean isScheduled, boolean isSunday, boolean isMonday, boolean isTuesday,
            boolean isWednesday, boolean isThursday, boolean isFriday, boolean isSaturday, boolean isDeleted, Date lastModifiedGmt, int lastModifiedByUserId) {
        this.rate = rate;
        this.rateProfile = rateProfile;
        this.customer = customer;
        this.isRateBlendingEnabled = isRateBlendingEnabled;
        this.isOverride = isOverride;
        this.displayPriority = displayPriority;
        this.isMaximumExpiryEnabled = isMaximumExpiryEnabled;
        this.isAdvancePurchaseEnabled = isAdvancePurchaseEnabled;
        this.isScheduled = isScheduled;
        this.isSunday = isSunday;
        this.isMonday = isMonday;
        this.isTuesday = isTuesday;
        this.isWednesday = isWednesday;
        this.isThursday = isThursday;
        this.isFriday = isFriday;
        this.isSaturday = isSaturday;
        this.isDeleted = isDeleted;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    public RateRateProfile(RateExpiryDayType rateExpiryDayType, Rate rate, RateProfile rateProfile, RateFlatType rateFlatType, Customer customer,
            String description, boolean isRateBlendingEnabled, boolean isOverride, byte displayPriority, Date availabilityStartDate, Date availabilityEndDate,
            String availabilityStartTime, String availabilityEndTime, Short durationNumberOfDays, Short durationNumberOfHours, Short durationNumberOfMins,
            boolean isMaximumExpiryEnabled, String expiryTime, boolean isAdvancePurchaseEnabled, boolean isScheduled, boolean isSunday, boolean isMonday,
            boolean isTuesday, boolean isWednesday, boolean isThursday, boolean isFriday, boolean isSaturday, boolean isDeleted, Date lastModifiedGmt,
            int lastModifiedByUserId) {
        this.rateExpiryDayType = rateExpiryDayType;
        this.rate = rate;
        this.rateProfile = rateProfile;
        this.rateFlatType = rateFlatType;
        this.customer = customer;
        this.description = description;
        this.isRateBlendingEnabled = isRateBlendingEnabled;
        this.isOverride = isOverride;
        this.displayPriority = displayPriority;
        this.availabilityStartDate = availabilityStartDate;
        this.availabilityEndDate = availabilityEndDate;
        this.availabilityStartTime = availabilityStartTime;
        this.availabilityEndTime = availabilityEndTime;
        this.durationNumberOfDays = durationNumberOfDays;
        this.durationNumberOfHours = durationNumberOfHours;
        this.durationNumberOfMins = durationNumberOfMins;
        this.isMaximumExpiryEnabled = isMaximumExpiryEnabled;
        this.expiryTime = expiryTime;
        this.isAdvancePurchaseEnabled = isAdvancePurchaseEnabled;
        this.isScheduled = isScheduled;
        this.isSunday = isSunday;
        this.isMonday = isMonday;
        this.isTuesday = isTuesday;
        this.isWednesday = isWednesday;
        this.isThursday = isThursday;
        this.isFriday = isFriday;
        this.isSaturday = isSaturday;
        this.isDeleted = isDeleted;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    @Version
    @Column(name = "VERSION", nullable = false)
    public int getVersion() {
        return this.version;
    }
    
    public void setVersion(int version) {
        this.version = version;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RateExpiryDayTypeId")
    public RateExpiryDayType getRateExpiryDayType() {
        return this.rateExpiryDayType;
    }
    
    public void setRateExpiryDayType(RateExpiryDayType rateExpiryDayType) {
        this.rateExpiryDayType = rateExpiryDayType;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RateId", nullable = false)
    public Rate getRate() {
        return this.rate;
    }
    
    public void setRate(Rate rate) {
        this.rate = rate;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RateProfileId", nullable = false)
    public RateProfile getRateProfile() {
        return this.rateProfile;
    }
    
    public void setRateProfile(RateProfile rateProfile) {
        this.rateProfile = rateProfile;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RateFlatTypeId", nullable = true)
    public RateFlatType getRateFlatType() {
        return this.rateFlatType;
    }
    
    public void setRateFlatType(RateFlatType rateFlatType) {
        this.rateFlatType = rateFlatType;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerId", nullable = false)
    public Customer getCustomer() {
        return this.customer;
    }
    
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    
    @Column(name = "Description", length = 40)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    @Column(name = "IsRateBlendingEnabled", nullable = false)
    public boolean isIsRateBlendingEnabled() {
        return this.isRateBlendingEnabled;
    }
    
    public void setIsRateBlendingEnabled(boolean isRateBlendingEnabled) {
        this.isRateBlendingEnabled = isRateBlendingEnabled;
    }
    
    @Column(name = "IsOverride", nullable = false)
    public boolean isIsOverride() {
        return this.isOverride;
    }
    
    public void setIsOverride(boolean isOverride) {
        this.isOverride = isOverride;
    }
    
    @Column(name = "DisplayPriority", nullable = false)
    public byte getDisplayPriority() {
        return this.displayPriority;
    }
    
    public void setDisplayPriority(byte displayPriority) {
        this.displayPriority = displayPriority;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "AvailabilityStartDate", length = 19)
    public Date getAvailabilityStartDate() {
        return this.availabilityStartDate;
    }
    
    public void setAvailabilityStartDate(Date availabilityStartDate) {
        this.availabilityStartDate = availabilityStartDate;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "AvailabilityEndDate", length = 19)
    public Date getAvailabilityEndDate() {
        return this.availabilityEndDate;
    }
    
    public void setAvailabilityEndDate(Date availabilityEndDate) {
        this.availabilityEndDate = availabilityEndDate;
    }
    
    @Column(name = "AvailabilityStartTime", length = 8)
    public String getAvailabilityStartTime() {
        return this.availabilityStartTime;
    }
    
    public void setAvailabilityStartTime(String availabilityStartTime) {
        this.availabilityStartTime = availabilityStartTime;
    }
    
    @Column(name = "AvailabilityEndTime", length = 8)
    public String getAvailabilityEndTime() {
        return this.availabilityEndTime;
    }
    
    public void setAvailabilityEndTime(String availabilityEndTime) {
        this.availabilityEndTime = availabilityEndTime;
    }
    
    @Column(name = "DurationNumberOfDays")
    public Short getDurationNumberOfDays() {
        return this.durationNumberOfDays;
    }
    
    public void setDurationNumberOfDays(Short durationNumberOfDays) {
        this.durationNumberOfDays = durationNumberOfDays;
    }
    
    @Column(name = "DurationNumberOfHours")
    public Short getDurationNumberOfHours() {
        return this.durationNumberOfHours;
    }
    
    public void setDurationNumberOfHours(Short durationNumberOfHours) {
        this.durationNumberOfHours = durationNumberOfHours;
    }
    
    @Column(name = "DurationNumberOfMins")
    public Short getDurationNumberOfMins() {
        return this.durationNumberOfMins;
    }
    
    public void setDurationNumberOfMins(Short durationNumberOfMins) {
        this.durationNumberOfMins = durationNumberOfMins;
    }
    
    @Column(name = "IsMaximumExpiryEnabled", nullable = false)
    public boolean isIsMaximumExpiryEnabled() {
        return this.isMaximumExpiryEnabled;
    }
    
    public void setIsMaximumExpiryEnabled(boolean isMaximumExpiryEnabled) {
        this.isMaximumExpiryEnabled = isMaximumExpiryEnabled;
    }
    
    @Column(name = "ExpiryTime", length = 8)
    public String getExpiryTime() {
        return this.expiryTime;
    }
    
    public void setExpiryTime(String expiryTime) {
        this.expiryTime = expiryTime;
    }
    
    @Column(name = "IsAdvancePurchaseEnabled", nullable = false)
    public boolean isIsAdvancePurchaseEnabled() {
        return this.isAdvancePurchaseEnabled;
    }
    
    public void setIsAdvancePurchaseEnabled(boolean isAdvancePurchaseEnabled) {
        this.isAdvancePurchaseEnabled = isAdvancePurchaseEnabled;
    }
    
    @Column(name = "IsScheduled", nullable = false)
    public boolean isIsScheduled() {
        return this.isScheduled;
    }
    
    public void setIsScheduled(boolean isScheduled) {
        this.isScheduled = isScheduled;
    }
    
    @Column(name = "IsSunday")
    public boolean getIsSunday() {
        return this.isSunday;
    }
    
    public void setIsSunday(boolean isSunday) {
        this.isSunday = isSunday;
    }
    
    @Column(name = "IsMonday")
    public boolean getIsMonday() {
        return this.isMonday;
    }
    
    public void setIsMonday(boolean isMonday) {
        this.isMonday = isMonday;
    }
    
    @Column(name = "IsTuesday")
    public boolean getIsTuesday() {
        return this.isTuesday;
    }
    
    public void setIsTuesday(boolean isTuesday) {
        this.isTuesday = isTuesday;
    }
    
    @Column(name = "IsWednesday")
    public boolean getIsWednesday() {
        return this.isWednesday;
    }
    
    public void setIsWednesday(boolean isWednesday) {
        this.isWednesday = isWednesday;
    }
    
    @Column(name = "IsThursday")
    public boolean getIsThursday() {
        return this.isThursday;
    }
    
    public void setIsThursday(boolean isThursday) {
        this.isThursday = isThursday;
    }
    
    @Column(name = "IsFriday")
    public boolean getIsFriday() {
        return this.isFriday;
    }
    
    public void setIsFriday(boolean isFriday) {
        this.isFriday = isFriday;
    }
    
    @Column(name = "IsSaturday")
    public boolean getIsSaturday() {
        return this.isSaturday;
    }
    
    public void setIsSaturday(boolean isSaturday) {
        this.isSaturday = isSaturday;
    }
    
    @Column(name = "IsDeleted", nullable = false)
    public boolean isIsDeleted() {
        return this.isDeleted;
    }
    
    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = 19)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }
    
    public void setLastModifiedGmt(Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }
    
    public void setLastModifiedByUserId(int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
}
