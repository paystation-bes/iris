package com.digitalpaytech.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@Entity
@Table(name = "LinuxConfigurationFile")
@NamedQueries({
    @NamedQuery(name = "LinuxConfigurationFile.findLinuxConfigurationFilesByPointOfSaleIdAndSnapShotId", query = "FROM LinuxConfigurationFile lcf WHERE lcf.pointOfSaleId = :pointOfSaleId AND lcf.snapShotId = :snapShotId", cacheable = true),
    @NamedQuery(name = "LinuxConfigurationFile.deleteLinuxConfigurationFilesByPointOfSaleIdAndSnapShotIdOrdinal", query = "DELETE LinuxConfigurationFile lcf WHERE lcf.pointOfSaleId = ? AND lcf.snapShotId = ?", cacheable = true),
    @NamedQuery(name = "LinuxConfigurationFile.findLinuxConfigurationFilesByPointOfSaleIdAndSnapShotIdAndFileId", query = "FROM LinuxConfigurationFile lcf WHERE lcf.pointOfSaleId = :pointOfSaleId AND lcf.snapShotId = :snapShotId AND lcf.fileId = :fileId", cacheable = true),
    @NamedQuery(name = "LinuxConfigurationFile.findLatestLinuxConfigurationFilesByPointOfSaleId", query = "FROM LinuxConfigurationFile lcf WHERE lcf.pointOfSaleId = :pointOfSaleId ORDER BY id DESC", cacheable = true),
    @NamedQuery(name = "LinuxConfigurationFile.deleteLinuxConfigurationFilesByPointOfSaleId", query = "DELETE LinuxConfigurationFile lcf WHERE lcf.pointOfSaleId = ?", cacheable = true),
})
@SuppressWarnings({ "checkstyle:designforextension" })
public class LinuxConfigurationFile {
    
    private int id;
    private int pointOfSaleId;
    private String fileId;
    private String snapShotId;
    private String fileName;
    private String hash;
    private long size;
    
    public LinuxConfigurationFile() {
    }
    
    public LinuxConfigurationFile(final int pointOfSaleId, final String fileId, final String snapShotId, final String fileName, final String hash,
            final long size) {
        this.pointOfSaleId = pointOfSaleId;
        this.fileId = fileId;
        this.snapShotId = snapShotId;
        this.fileName = fileName;
        this.hash = hash;
        this.size = size;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    public int getId() {
        return this.id;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    @Column(name = "PointOfSaleId", nullable = false)
    public int getPointOfSaleId() {
        return this.pointOfSaleId;
    }
    
    public void setPointOfSaleId(final int pointOfSaleId) {
        this.pointOfSaleId = pointOfSaleId;
    }
    
    @Column(name = "FileId", nullable = false)
    public String getFileId() {
        return this.fileId;
    }
    
    public void setFileId(final String fileId) {
        this.fileId = fileId;
    }
    
    @Column(name = "SnapShotId", nullable = false)
    public String getSnapShotId() {
        return this.snapShotId;
    }
    
    public void setSnapShotId(final String snapShotId) {
        this.snapShotId = snapShotId;
    }
    
    @Column(name = "FileName", nullable = false)
    public String getFileName() {
        return this.fileName;
    }
    
    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }
    
    @Column(name = "Hash", nullable = false)
    public String getHash() {
        return this.hash;
    }
    
    public void setHash(final String hash) {
        this.hash = hash;
    }
    
    @Column(name = "Size")
    public long getSize() {
        return this.size;
    }
    
    public void setSize(final long size) {
        this.size = size;
    }
}
