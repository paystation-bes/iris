package com.digitalpaytech.domain;

// Generated 30-Oct-2012 9:20:21 AM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.CascadeType;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryLookup;

/**
 * Paymentcard generated by hbm2java
 */
@Entity
@Table(name = "PaymentCard")
@StoryAlias(PaymentCard.ALIAS)
@NamedQueries({
    @NamedQuery(name = "PaymentCard.findPaymentCardByPurchaseIdAndProcessorTransactionId", query = "FROM PaymentCard pc WHERE pc.purchase.id = :purchaseId AND pc.processorTransaction.id = :processorTransactionId"),
    @NamedQuery(name = "PaymentCard.findByPurchaseIdAndCardType", query = "FROM PaymentCard pc WHERE pc.purchase.id = :purchaseId AND pc.cardType.id = :cardTypeId ORDER BY pc.id ASC"),
    @NamedQuery(name = "PaymentCard.findByPurchaseId", query = "FROM PaymentCard pc WHERE pc.purchase.id = :purchaseId ORDER BY pc.id ASC"),
    @NamedQuery(name = "PaymentCard.findLatestByCustomerCardId", query = "FROM PaymentCard pc WHERE pc.customerCard.id = :customerCardId ORDER BY pc.cardProcessedGmt DESC"),
    @NamedQuery(name = "PaymentCard.updateProcessorTransactionType", query = "UPDATE PaymentCard pc SET pc.processorTransactionType.id = :processorTransactionTypeId WHERE pc.purchase.id = :purchaseId AND pc.processorTransaction.id = :processorTransactionId"),
    @NamedQuery(name = "PaymentCard.updateProcessorTransactionAndType", query = "UPDATE PaymentCard pc SET pc.processorTransaction.id = :processorTransactionId, pc.processorTransactionType.id = :processorTransactionTypeId WHERE pc.id = :id") })
public class PaymentCard implements java.io.Serializable {
    public static final String ALIAS = "payment card";
    public static final String ALIAS_ID = "payment card id";
    public static final String ALIAS_MERCHANT = "merchant";
    public static final String ALIAS_PURCHASE_CUSTOMER = "purchase customer";
    public static final String ALIAS_PROCESSOR_TRANSACTION_FOR = "processor transaction for";
    
    /**
     * 
     */
    private static final long serialVersionUID = -5695013010399524804L;
    private Long id;
    private ProcessorTransaction processorTransaction;
    private CardType cardType;
    private CustomerCard customerCard;
    private Purchase purchase;
    private MerchantAccount merchantAccount;
    private ProcessorTransactionType processorTransactionType;
    private CreditCardType creditCardType;
    private int amount;
    private short cardLast4digits;
    private Date cardProcessedGmt;
    private boolean isUploadedFromBoss;
    private boolean isRfid;
    private boolean isApproved;
    
    public PaymentCard() {
    }
    
    public PaymentCard(final CardType cardType, final Purchase purchase, final CreditCardType creditCardType, final int amount,
            final short cardLast4digits, final Date cardProcessedGmt, final boolean isUploadedFromBoss, final boolean isRfid,
            final boolean isApproved) {
        this.cardType = cardType;
        this.purchase = purchase;
        this.creditCardType = creditCardType;
        this.amount = amount;
        this.cardLast4digits = cardLast4digits;
        this.cardProcessedGmt = cardProcessedGmt;
        this.isUploadedFromBoss = isUploadedFromBoss;
        this.isRfid = isRfid;
        this.isApproved = isApproved;
    }
    
    public PaymentCard(final ProcessorTransaction processorTransaction, final CardType cardType, final CustomerCard customerCard,
            final Purchase purchase, final CreditCardType creditCardType, final int amount, final short cardLast4digits, final Date cardProcessedGmt,
            final boolean isUploadedFromBoss, final boolean isRfid, final boolean isApproved) {
        this.processorTransaction = processorTransaction;
        this.cardType = cardType;
        this.customerCard = customerCard;
        this.purchase = purchase;
        this.creditCardType = creditCardType;
        this.amount = amount;
        this.cardLast4digits = cardLast4digits;
        this.cardProcessedGmt = cardProcessedGmt;
        this.isUploadedFromBoss = isUploadedFromBoss;
        this.isRfid = isRfid;
        this.isApproved = isApproved;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @StoryAlias(ALIAS_ID)
    @Column(name = "Id", nullable = false)
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ProcessorTransactionId")
    @StoryAlias(ALIAS_PROCESSOR_TRANSACTION_FOR)
    @StoryLookup(type = ProcessorTransaction.ALIAS, searchAttribute = ProcessorTransaction.ALIAS_PURCHASE_CUSTOMER)
    public ProcessorTransaction getProcessorTransaction() {
        return this.processorTransaction;
    }
    
    public void setProcessorTransaction(final ProcessorTransaction processorTransaction) {
        this.processorTransaction = processorTransaction;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CardTypeId", nullable = false)
    @StoryAlias(CardType.ALIAS)
    @StoryLookup(type = CardType.ALIAS, searchAttribute = CardType.ALIAS_NAME)
    public CardType getCardType() {
        return this.cardType;
    }
    
    public void setCardType(final CardType cardType) {
        this.cardType = cardType;
    }
    
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "CustomerCardId")
    public CustomerCard getCustomerCard() {
        return this.customerCard;
    }
    
    public void setCustomerCard(final CustomerCard customerCard) {
        this.customerCard = customerCard;
    }
    
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "PurchaseId", nullable = false)
    @StoryAlias(ALIAS_PURCHASE_CUSTOMER)
    @StoryLookup(type = Purchase.ALIAS, searchAttribute = Customer.ALIAS_NAME)
    public Purchase getPurchase() {
        return this.purchase;
    }
    
    public void setPurchase(final Purchase purchase) {
        this.purchase = purchase;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MerchantAccountId")
    @StoryAlias(ALIAS_MERCHANT)
    @StoryLookup(type = MerchantAccount.ALIAS, searchAttribute = MerchantAccount.ALIAS_NAME)
    public MerchantAccount getMerchantAccount() {
        return this.merchantAccount;
    }
    
    public void setMerchantAccount(final MerchantAccount merchantAccount) {
        this.merchantAccount = merchantAccount;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ProcessorTransactionTypeId")
    public ProcessorTransactionType getProcessorTransactionType() {
        return this.processorTransactionType;
    }
    
    public void setProcessorTransactionType(final ProcessorTransactionType processorTransactionType) {
        this.processorTransactionType = processorTransactionType;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CreditCardTypeId", nullable = false)
    public CreditCardType getCreditCardType() {
        return this.creditCardType;
    }
    
    public void setCreditCardType(final CreditCardType creditCardType) {
        this.creditCardType = creditCardType;
    }
    
    @Column(name = "Amount", nullable = false)
    public int getAmount() {
        return this.amount;
    }
    
    public void setAmount(final int amount) {
        this.amount = amount;
    }
    
    @Column(name = "CardLast4Digits", nullable = false)
    public short getCardLast4digits() {
        return this.cardLast4digits;
    }
    
    public void setCardLast4digits(final short cardLast4digits) {
        this.cardLast4digits = cardLast4digits;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CardProcessedGMT", nullable = false, length = 19)
    public Date getCardProcessedGmt() {
        return this.cardProcessedGmt;
    }
    
    public void setCardProcessedGmt(final Date cardProcessedGmt) {
        this.cardProcessedGmt = cardProcessedGmt;
    }
    
    @Column(name = "IsUploadedFromBoss", nullable = false)
    public boolean isIsUploadedFromBoss() {
        return this.isUploadedFromBoss;
    }
    
    public void setIsUploadedFromBoss(final boolean isUploadedFromBoss) {
        this.isUploadedFromBoss = isUploadedFromBoss;
    }
    
    @Column(name = "IsRFID", nullable = false)
    public boolean isIsRfid() {
        return this.isRfid;
    }
    
    public void setIsRfid(final boolean isRfid) {
        this.isRfid = isRfid;
    }
    
    @Column(name = "IsApproved", nullable = false)
    public boolean isIsApproved() {
        return this.isApproved;
    }
    
    public void setIsApproved(final boolean isApproved) {
        this.isApproved = isApproved;
    }
    
}
