package com.digitalpaytech.domain;

// Generated 6-Oct-2014 1:38:59 PM by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.domain.util.HibernateConstants;

/**
 * Flexdatawscallstatus generated by hbm2java
 */
@Entity
@Table(name = "FlexDataWSCallStatus")
@NamedQueries({ @NamedQuery(name = "FlexDataWSCallStatus.findLastCallByCustomerIdAndTypeId", query = "FROM FlexDataWSCallStatus fd WHERE fd.customer.id = :customerId AND fd.flexETLDataType.id = :flexETLDataTypeId AND fd.status = 2 ORDER BY fd.requestedToDateGmt DESC", cacheable = true) })
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
//Checkstyle: Hibernate proxies have issues when methods are defined final
@SuppressWarnings({ "checkstyle:designforextension" })
public class FlexDataWSCallStatus implements java.io.Serializable {
    
    private static final long serialVersionUID = -5420493960686779905L;
    private Integer id;
    private FlexETLDataType flexETLDataType;
    private Customer customer;
    private Date requestedFromDateGmt;
    private Date requestedToDateGmt;
    private Date wscallBeginGmt;
    private Date wscallEndGmt;
    private Integer lastUniqueIdentifier;
    private Byte status;
    
    public FlexDataWSCallStatus() {
    }
    
    public FlexDataWSCallStatus(final FlexETLDataType flexETLDataType, final Customer customer, final Date requestedFromDateGmt
            , final Date requestedToDateGmt) {
        this.flexETLDataType = flexETLDataType;
        this.customer = customer;
        this.requestedFromDateGmt = requestedFromDateGmt;
        this.requestedToDateGmt = requestedToDateGmt;
    }
    
    public FlexDataWSCallStatus(final FlexETLDataType flexETLDataType, final Customer customer, final Date requestedFromDateGmt
            , final Date requestedToDateGmt, final Date wscallBeginGmt, final Date wscallEndGmt, final Integer lastUniqueIdentifier, final Byte status) {
        this.flexETLDataType = flexETLDataType;
        this.customer = customer;
        this.requestedFromDateGmt = requestedFromDateGmt;
        this.requestedToDateGmt = requestedToDateGmt;
        this.wscallBeginGmt = wscallBeginGmt;
        this.wscallEndGmt = wscallEndGmt;
        this.lastUniqueIdentifier = lastUniqueIdentifier;
        this.status = status;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FlexETLDataTypeId", nullable = false)
    public FlexETLDataType getFlexETLDataType() {
        return this.flexETLDataType;
    }
    
    public void setFlexETLDataType(final FlexETLDataType flexETLDataType) {
        this.flexETLDataType = flexETLDataType;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerId", nullable = false)
    public Customer getCustomer() {
        return this.customer;
    }
    
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RequestedFromDateGmt", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getRequestedFromDateGmt() {
        return this.requestedFromDateGmt;
    }
    
    public void setRequestedFromDateGmt(final Date requestedFromDateGmt) {
        this.requestedFromDateGmt = requestedFromDateGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RequestedToDateGmt", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getRequestedToDateGmt() {
        return this.requestedToDateGmt;
    }
    
    public void setRequestedToDateGmt(final Date requestedToDateGmt) {
        this.requestedToDateGmt = requestedToDateGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "WSCallBeginGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getWscallBeginGmt() {
        return this.wscallBeginGmt;
    }
    
    public void setWscallBeginGmt(final Date wscallBeginGmt) {
        this.wscallBeginGmt = wscallBeginGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "WSCallEndGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getWscallEndGmt() {
        return this.wscallEndGmt;
    }
    
    public void setWscallEndGmt(final Date wscallEndGmt) {
        this.wscallEndGmt = wscallEndGmt;
    }
    
    @Column(name = "LastUniqueIdentifier")
    public Integer getLastUniqueIdentifier() {
        return this.lastUniqueIdentifier;
    }
    
    public void setLastUniqueIdentifier(final Integer lastUniqueIdentifier) {
        this.lastUniqueIdentifier = lastUniqueIdentifier;
    }
    
    @Column(name = "Status")
    public Byte getStatus() {
        return this.status;
    }
    
    public void setStatus(final Byte status) {
        this.status = status;
    }
    
}
