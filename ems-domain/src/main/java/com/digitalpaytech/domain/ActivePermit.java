package com.digitalpaytech.domain;

// Generated 14-Nov-2012 8:53:53 AM by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryDelegateAttribute;
import com.digitalpaytech.annotation.StoryLookup;
import com.digitalpaytech.annotation.StoryTransformer;
import com.digitalpaytech.domain.util.HibernateConstants;

/**
 * Activepermit generated by hbm2java
 * 
 * TODO: refactor the duplicate 'order by...' code.
 */
@Entity
@Table(name = "ActivePermit")
@NamedQueries({
    @NamedQuery(name = "ActivePermit.findLatestExpiredPermitByCustomerAndLicencePlate", query = "FROM ActivePermit ap WHERE ap.customer.id = :customerId AND ap.licencePlateNumber = :licencePlateNumber AND ap.permitExpireGmt>=:limitDate ORDER BY ap.permitBeginGmt DESC"),
    @NamedQuery(name = "ActivePermit.findLatestExpiredPermitByCustomerAndLicencePlateLatestExpiry", query = "FROM ActivePermit ap WHERE ap.customer.id = :customerId AND ap.licencePlateNumber = :licencePlateNumber AND ap.permitExpireGmt>=:limitDate ORDER BY ap.permitExpireGmt DESC"),
    @NamedQuery(name = "ActivePermit.findLatestExpiredPermitByPaystationSettingAndAddTimeNumber", query = "FROM ActivePermit ap WHERE ap.customer.id = :customerId AND ap.paystationSettingName = :paystationSettingName AND ap.addTimeNumber = :addTimeNumber AND ap.permitExpireGmt>=:limitDate ORDER BY ap.permitBeginGmt DESC"),
    @NamedQuery(name = "ActivePermit.findLatestExpiredPermitByCustomerAndAddTimeNumber", query = "FROM ActivePermit ap WHERE ap.customer.id = :customerId AND ap.addTimeNumber = :addTimeNumber AND ap.permitExpireGmt>=:limitDate ORDER BY ap.permitBeginGmt DESC"),
    @NamedQuery(name = "ActivePermit.findLatestExpiredPermitByCustomerAndAddTimeNumberOrderByLatestExpiry", query = "FROM ActivePermit ap WHERE ap.customer.id = :customerId AND ap.addTimeNumber = :addTimeNumber AND ap.permitExpireGmt>=:limitDate ORDER BY ap.permitExpireGmt DESC"),
    @NamedQuery(name = "ActivePermit.findLatestExpiredPermitByLocationAndLicencePlate", query = "FROM ActivePermit ap WHERE ap.location.id = :locationId AND ap.licencePlateNumber = :licencePlateNumber AND ap.permitExpireGmt>=:limitDate ORDER BY ap.permitBeginGmt DESC"),
    @NamedQuery(name = "ActivePermit.findLatestExpiredPermitByLocationAndAddTimeNumber", query = "FROM ActivePermit ap WHERE ap.customer.id = :customerId AND ap.location.id = :locationId AND ap.addTimeNumber = :addTimeNumber AND ap.permitExpireGmt>=:limitDate ORDER BY ap.permitBeginGmt DESC"),
    @NamedQuery(name = "ActivePermit.findLatestExpiredPermitByLocationAndAddTimeNumberOrderByLatestExpiry", query = "FROM ActivePermit ap WHERE ap.customer.id = :customerId AND ap.location.id = :locationId AND ap.addTimeNumber = :addTimeNumber AND ap.permitExpireGmt>=:limitDate ORDER BY ap.permitExpireGmt DESC"),
    @NamedQuery(name = "ActivePermit.findLatestExpiredPermitByCustomerAndSpaceNumber", query = "FROM ActivePermit ap WHERE ap.customer.id = :customerId AND ap.spaceNumber = :spaceNumber ORDER BY ap.permitBeginGmt DESC"),
    @NamedQuery(name = "ActivePermit.findLatestExpiredPermitByCustomerAndSpaceNumberOrderByLatestExpiry", query = "FROM ActivePermit ap WHERE ap.customer.id = :customerId AND ap.spaceNumber = :spaceNumber ORDER BY ap.permitExpireGmt DESC"),
    @NamedQuery(name = "ActivePermit.findLatestExpiredPermitByLocationAndSpaceNumber", query = "FROM ActivePermit ap WHERE ap.customer.id = :customerId AND ap.location.id = :locationId AND ap.spaceNumber = :spaceNumber ORDER BY ap.permitBeginGmt DESC"),
    @NamedQuery(name = "ActivePermit.findLatestExpiredPermitByLocationAndSpaceNumberOrderByLatestExpiry", query = "FROM ActivePermit ap WHERE ap.customer.id = :customerId AND ap.location.id = :locationId AND ap.spaceNumber = :spaceNumber ORDER BY ap.permitExpireGmt DESC"),
    @NamedQuery(name = "ActivePermit.findLatestExpiredPermitByPaystationSettingAndSpaceNumber", query = "FROM ActivePermit ap WHERE ap.customer.id = :customerId AND ap.paystationSettingName = :paystationSettingName AND ap.spaceNumber = :spaceNumber ORDER BY ap.permitBeginGmt DESC"),
    @NamedQuery(name = "ActivePermit.findActivePermitByOriginalPermitId", query = "FROM ActivePermit ap WHERE ap.permit.id = :originalPermitId"),
    @NamedQuery(name = "ActivePermit.findLatestValidPermitByLocationAndSpaceNumber", query = "FROM ActivePermit ap WHERE ap.customer.id = :customerId AND ap.location.id = :locationId AND ap.spaceNumber = :spaceNumber AND ap.permitExpireGmt>=:limitDate ORDER BY ap.permitBeginGmt DESC"),
    @NamedQuery(name = "ActivePermit.findActivePermitByLocationIdAndSpaceNumber", query = "FROM ActivePermit ap WHERE ap.location.id = :locationId AND ap.spaceNumber = :spaceNumber"),
    @NamedQuery(name = "ActivePermit.findLatestExpiredPermitByLocationId", query = "FROM ActivePermit ap WHERE ap.location.id = :locationId ORDER BY ap.permitExpireGmt DESC"),
    @NamedQuery(name = "ActivePermit.findActivePermitByLocationIdAndLicencePlateNumber", query = "FROM ActivePermit ap WHERE ap.location.id = :permitLocationId AND ap.licencePlateNumber = :licencePlateNumber ORDER BY ap.permitBeginGmt DESC"),
    @NamedQuery(name = "ActivePermit.findActivePermitByLocationIdAndLicencePlateNumberOrderByLatestExpiry", query = "FROM ActivePermit ap WHERE ap.location.id = :permitLocationId AND ap.licencePlateNumber = :licencePlateNumber ORDER BY ap.permitExpireGmt DESC"),
    @NamedQuery(name = "ActivePermit.findActivePermitByCustomerIdAndLicencePlateNumber", query = "FROM ActivePermit ap WHERE ap.customer.id = :customerId AND ap.licencePlateNumber = :licencePlateNumber ORDER BY ap.permitBeginGmt DESC"),
    @NamedQuery(name = "ActivePermit.findActivePermitByCustomerIdAndLicencePlateNumberOrderByLatestExpiry", query = "FROM ActivePermit ap WHERE ap.customer.id = :customerId AND ap.licencePlateNumber = :licencePlateNumber ORDER BY ap.permitExpireGmt DESC"),
    @NamedQuery(name = "ActivePermit.findOriginalPermit", query = "SELECT per2 FROM Permit per2 WHERE per2.id = (SELECT (CASE WHEN orgPer IS NULL THEN per.id ELSE orgPer.id END) FROM ActivePermit ap INNER JOIN ap.permit per LEFT JOIN per.permit orgPer WHERE ap.location.id = :locationId AND ap.spaceNumber = :spaceNumber AND ap.licencePlateNumber = :licencePlateNumber AND ap.addTimeNumber = :addTimeNumber ORDER BY ap.permitExpireGmt DESC)") })
@StoryAlias(ActivePermit.ALIAS)
@SuppressWarnings({ "checkstyle:designforextension" })
public class ActivePermit implements java.io.Serializable {
    /**
     * 
     */
    public static final String ALIAS = "active permit";
    public static final String ALIAS_LOCATION = "location";
    public static final String ALIAS_STALL_NUMBER = "stall number";
    public static final String ALIAS_PERMIT_BEGIN = "permit begin";
    public static final String ALIAS_LICENCE_PLATE_NUMBER = "licence plate number";
    private static final long serialVersionUID = -5690825576615462407L;
    private Long id;
    private int version;
    private PointOfSale pointOfSale;
    private Location location;
    private Customer customer;
    private Permit permit;
    private String paystationSettingName;
    private String licencePlateNumber;
    private Integer spaceNumber;
    private Integer addTimeNumber;
    private Date permitBeginGmt;
    private Date permitOriginalExpireGmt;
    private Date permitExpireGmt;
    private byte numberOfExtensions;
    private Date lastModifiedGmt;
    
    public ActivePermit() {
    }
    
    public ActivePermit(final PointOfSale pointOfSale, final Location location, final Customer customer, final Permit permit,
            final String paystationSettingName, final String licencePlateNumber, final Integer spaceNumber, final Integer addTimeNumber,
            final Date permitBeginGmt, final Date permitOriginalExpireGmt, final Date permitExpireGmt, final byte numberOfExtensions) {
        this.pointOfSale = pointOfSale;
        this.location = location;
        this.customer = customer;
        this.permit = permit;
        this.paystationSettingName = paystationSettingName;
        this.licencePlateNumber = licencePlateNumber;
        this.spaceNumber = spaceNumber;
        this.addTimeNumber = addTimeNumber;
        this.permitBeginGmt = permitBeginGmt;
        this.permitOriginalExpireGmt = permitOriginalExpireGmt;
        this.permitExpireGmt = permitExpireGmt;
        this.numberOfExtensions = numberOfExtensions;
    }
    
    public ActivePermit(final PointOfSale pointOfSale, final Location location, final Customer customer, final Permit permit,
            final String paystationSettingName, final String licencePlateNumber, final Integer spaceNumber, final Integer addTimeNumber,
            final Date permitBeginGmt, final Date permitOriginalExpireGmt, final Date permitExpireGmt, final byte numberOfExtensions,
            final Date lastModifiedGmt) {
        this.pointOfSale = pointOfSale;
        this.location = location;
        this.customer = customer;
        this.permit = permit;
        this.paystationSettingName = paystationSettingName;
        this.licencePlateNumber = licencePlateNumber;
        this.spaceNumber = spaceNumber;
        this.addTimeNumber = addTimeNumber;
        this.permitBeginGmt = permitBeginGmt;
        this.permitOriginalExpireGmt = permitOriginalExpireGmt;
        this.permitExpireGmt = permitExpireGmt;
        this.numberOfExtensions = numberOfExtensions;
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PointOfSaleId", nullable = false)
    public PointOfSale getPointOfSale() {
        return this.pointOfSale;
    }
    
    public void setPointOfSale(final PointOfSale pointOfSale) {
        this.pointOfSale = pointOfSale;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PermitLocationId", nullable = false)
    @StoryAlias(ALIAS_LOCATION)
    @StoryLookup(type = Location.ALIAS, searchAttribute = Location.ALIAS_LOCATION_NAME)
    public Location getLocation() {
        return this.location;
    }
    
    public void setLocation(final Location location) {
        this.location = location;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerId", nullable = false)
    public Customer getCustomer() {
        return this.customer;
    }
    
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OriginalPermitId", nullable = false)
    @StoryAlias("permit id")
    @StoryLookup(type = Permit.ALIAS, searchAttribute = Permit.ALIAS_TICKET_NUMBER)
    public Permit getPermit() {
        return this.permit;
    }
    
    public void setPermit(final Permit permit) {
        this.permit = permit;
    }
    
    @Column(name = "PaystationSettingName", nullable = false, length = HibernateConstants.VARCHAR_LENGTH_20)
    public String getPaystationSettingName() {
        return this.paystationSettingName;
    }
    
    public void setPaystationSettingName(final String paystationSettingName) {
        this.paystationSettingName = paystationSettingName;
    }
    
    @Column(name = "LicencePlateNumber", nullable = false, length = HibernateConstants.VARCHAR_LENGTH_15)
    @StoryAlias(ALIAS_LICENCE_PLATE_NUMBER)
    public String getLicencePlateNumber() {
        return this.licencePlateNumber;
    }
    
    public void setLicencePlateNumber(final String licencePlateNumber) {
        this.licencePlateNumber = licencePlateNumber;
    }
    
    @Column(name = "SpaceNumber", nullable = false)
    @StoryAlias(ALIAS_STALL_NUMBER)
    public Integer getSpaceNumber() {
        return this.spaceNumber;
    }
    
    public void setSpaceNumber(final Integer spaceNumber) {
        this.spaceNumber = spaceNumber;
    }
    
    @Column(name = "AddTimeNumber", nullable = false)
    public Integer getAddTimeNumber() {
        return this.addTimeNumber;
    }
    
    public void setAddTimeNumber(final Integer addTimeNumber) {
        this.addTimeNumber = addTimeNumber;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "PermitBeginGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    @StoryAlias(ALIAS_PERMIT_BEGIN)
    @StoryTransformer("dateString")
    public Date getPermitBeginGmt() {
        return this.permitBeginGmt;
    }
    
    public void setPermitBeginGmt(final Date permitBeginGmt) {
        this.permitBeginGmt = permitBeginGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "PermitOriginalExpireGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getPermitOriginalExpireGmt() {
        return this.permitOriginalExpireGmt;
    }
    
    public void setPermitOriginalExpireGmt(final Date permitOriginalExpireGmt) {
        this.permitOriginalExpireGmt = permitOriginalExpireGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "PermitExpireGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getPermitExpireGmt() {
        return this.permitExpireGmt;
    }
    
    public void setPermitExpireGmt(final Date permitExpireGmt) {
        this.permitExpireGmt = permitExpireGmt;
    }
    
    @Column(name = "NumberOfExtensions", nullable = false)
    public byte getNumberOfExtensions() {
        return this.numberOfExtensions;
    }
    
    public void setNumberOfExtensions(final byte numberOfExtensions) {
        this.numberOfExtensions = numberOfExtensions;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }
    
    public void setLastModifiedGmt(final Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Version
    @Column(name = "VERSION", nullable = false)
    public int getVersion() {
        return this.version;
    }
    
    public void setVersion(final int version) {
        this.version = version;
    }
    
}
