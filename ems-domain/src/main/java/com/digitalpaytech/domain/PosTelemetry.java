package com.digitalpaytech.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryId;
import com.digitalpaytech.annotation.StoryLookup;

@Entity
@Table(name = "POSTelemetry")
@NamedQueries({
    @NamedQuery(name = "PosTelemetry.findPubliclyActiveByPointOfSaleId", query = "select tt.label as key, t.value as value, tt.name as keyName from PosTelemetry t join t.telemetryType tt where (t.pointOfSale.id = :pointOfSaleId) and (tt.isPrivate = false) and (t.isHistory = false)"),
    @NamedQuery(name = "PosTelemetry.findAllActiveByPointOfSaleId", query = "select tt.label as key, t.value as value, tt.name as keyName from PosTelemetry t join t.telemetryType tt where (t.pointOfSale.id = :pointOfSaleId) and (t.isHistory = false)"),
    @NamedQuery(name = "PosTelemetry.findCurrentPosTelemetryByPointOfSaleIdAndTelemetryType", query = "select posTel from PosTelemetry posTel where (posTel.pointOfSale.id = :pointOfSaleId) and (posTel.telemetryType.id = :telemetryTypeId) and (posTel.isHistory = false)"),
    @NamedQuery(name = "PosTelemetry.findAllCurrentTelemetryForPointOfSaleByPointOfSaleId", query = "select posTel from PosTelemetry posTel where (posTel.pointOfSale.id = :pointOfSaleId) and (posTel.isHistory = false)") })
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@StoryAlias(PosTelemetry.ALIAS)
@SuppressWarnings({ "checkstyle:designforextension" })
public class PosTelemetry implements Serializable {
    public static final String ALIAS = "Paystation Telemetry";
    public static final String ALIAS_NAME = "Name";
    public static final String ALIAS_VALUE = "Value";
    public static final String ALIAS_POS = "Paystation";
    public static final String ALIAS_TIME = "Time";
    public static final String ALIAS_HISTORY = "History";
    public static final String ALIAS_CREATED_GMT = "Created Time in GMT";
    public static final String ALIAS_ARCHIVED_GMT = "Archived Time in GMT";
    
    /**
     * 
     */
    private static final long serialVersionUID = 3603122886939474674L;
    private Long id;
    private int version;
    private PointOfSale pointOfSale;
    private TelemetryType telemetryType;
    private String value;
    private Boolean isHistory;
    private Date timestampGMT;
    private Date createdGMT;
    private Date archivedGMT;
    
    public PosTelemetry() {
        
    }
    
    @Id
    @Column(name = "Id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @StoryId
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    @Version
    @Column(name = "VERSION", nullable = false)
    public int getVersion() {
        return this.version;
    }
    
    public void setVersion(final int version) {
        this.version = version;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PointOfSaleId", nullable = false)
    @StoryAlias(ALIAS_POS)
    @StoryLookup(type = PointOfSale.ALIAS, searchAttribute = PointOfSale.ALIAS_SERIAL_NUMBER)
    public PointOfSale getPointOfSale() {
        return this.pointOfSale;
    }
    
    public void setPointOfSale(final PointOfSale pointOfSale) {
        this.pointOfSale = pointOfSale;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TelemetryTypeId", nullable = false)
    @StoryAlias(ALIAS_NAME)
    @StoryLookup(type = TelemetryType.ALIAS, searchAttribute = TelemetryType.ALIAS_NAME)
    public TelemetryType getTelemetryType() {
        return this.telemetryType;
    }
    
    public void setTelemetryType(final TelemetryType telemetryType) {
        this.telemetryType = telemetryType;
    }
    
    @Column(name = "Value", nullable = false)
    @StoryAlias(ALIAS_VALUE)
    public String getValue() {
        return this.value;
    }
    
    public void setValue(final String value) {
        this.value = value;
    }
    
    @Column(name = "IsHistory", nullable = false)
    @StoryAlias(ALIAS_HISTORY)
    public Boolean getIsHistory() {
        return this.isHistory;
    }
    
    public void setIsHistory(final Boolean isHistory) {
        this.isHistory = isHistory;
    }
    
    @Column(name = "TimestampGMT", nullable = false)
    @StoryAlias(ALIAS_TIME)
    public Date getTimestampGMT() {
        return this.timestampGMT;
    }
    
    public void setTimestampGMT(final Date timestampGMT) {
        this.timestampGMT = timestampGMT;
    }
    
    @Column(name = "CreatedGMT", nullable = false)
    @StoryAlias(ALIAS_CREATED_GMT)
    public Date getCreatedGMT() {
        return this.createdGMT;
    }
    
    public void setCreatedGMT(final Date createdGMT) {
        this.createdGMT = createdGMT;
    }
    
    @Column(name = "ArchivedGMT")
    @StoryAlias(ALIAS_ARCHIVED_GMT)
    public Date getArchivedGMT() {
        return this.archivedGMT;
    }
    
    public void setArchivedGMT(final Date archivedGMT) {
        this.archivedGMT = archivedGMT;
    }
    
}
