package com.digitalpaytech.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.digitalpaytech.domain.util.HibernateConstants;

@Entity
@Table(name = "MerchantAccountMigrationStatus")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MerchantAccountMigrationStatus {
    
    public static final int PENDING = 1;
    public static final int SUCCESS = 2;
    public static final int FAILED = 3;
    
    private Integer id;
    private String name;
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @Column(name = "Name", length = HibernateConstants.VARCHAR_LENGTH_15)
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
}
