package com.digitalpaytech.domain;

// Generated 30-Oct-2012 9:20:21 AM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.LazyInitializationException;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryLookup;
import com.digitalpaytech.domain.util.HibernateConstants;

/**
 * Processortransaction generated by hbm2javaupdateProcessorTransactionToSettled
 */
@Entity
@Table(name = "ProcessorTransaction")
@NamedQueries({
    @NamedQuery(name = "ProcessorTransaction.findRefundablePTDsByCard", query = "FROM ProcessorTransaction pt WHERE (pt.pointOfSale.id IN (:psList)) AND (pt.purchasedDate BETWEEN :startDate AND :endDate) AND (pt.isApproved = true) "
                                                                                + "AND (pt.processorTransactionType.id IN (2,3,10,11,17,21)) AND (pt.merchantAccount.id = :merchantAccountId) AND (pt.cardChecksum IN (:cardChecksum,0)) "
                                                                                + "AND (pt.last4digitsOfCardNumber = :last4Digits) AND (pt.cardType = :cardType)", cacheable = true),
    
    @NamedQuery(name = "ProcessorTransaction.findRefundablePTDsByCardHash", query = "FROM ProcessorTransaction pt WHERE (pt.pointOfSale.id IN (:psList)) AND (pt.purchasedDate BETWEEN :startDate AND :endDate) AND (pt.isApproved = true) "
                                                                                    + "AND (pt.processorTransactionType.id IN (2,3,10,11,17,21)) AND (pt.merchantAccount.id = :merchantAccountId) AND (pt.cardHash = :cardHash) "
                                                                                    + "AND (pt.last4digitsOfCardNumber = :last4Digits) AND (pt.cardType = :cardType)", cacheable = true),
    
    @NamedQuery(name = "ProcessorTransaction.findRefundablePTDsByCardAndAuthNum", query = "FROM ProcessorTransaction pt WHERE (pt.pointOfSale.id IN (:psList)) AND (pt.purchasedDate BETWEEN :startDate AND :endDate) AND (pt.isApproved = true) "
                                                                                          + "AND (pt.processorTransactionType.id IN (2,3,10,11,17,21)) AND (pt.merchantAccount.id = :merchantAccountId) AND (pt.cardChecksum IN (:cardChecksum,0)) "
                                                                                          + "AND (pt.last4digitsOfCardNumber = :last4Digits) AND (pt.cardType = :cardType) AND (pt.authorizationNumber = :authNumber)", cacheable = true),
    
    @NamedQuery(name = "ProcessorTransaction.findRefundablePTDsByCardHashAndAuthNum", query = "FROM ProcessorTransaction pt WHERE (pt.pointOfSale.id IN (:psList)) AND (pt.purchasedDate BETWEEN :startDate AND :endDate) AND (pt.isApproved = true) "
                                                                                              + "AND (pt.processorTransactionType.id IN (2,3,10,11,17,21)) AND (pt.merchantAccount.id = :merchantAccountId) AND (pt.cardHash = :cardHash) "
                                                                                              + "AND (pt.last4digitsOfCardNumber = :last4Digits) AND (pt.cardType = :cardType) AND (pt.authorizationNumber = :authNumber)", cacheable = true),
    
    @NamedQuery(name = "ProcessorTransaction.updateProcessorTransactionType", query = "UPDATE ProcessorTransaction pt SET pt.processorTransactionType.id = :newTypeId "
                                                                                      + "WHERE pt.id = :id AND pt.isApproved = 1 AND pt.processorTransactionType.id = :typeId", cacheable = false),
    
    @NamedQuery(name = "ProcessorTransaction.getApprovedTransactionByPSDateAndTicketNum", query = "FROM ProcessorTransaction pt WHERE (pt.pointOfSale.id = :pointOfSaleId) AND (pt.purchasedDate = :purchasedDate) AND (pt.ticketNumber = :ticketNumber) AND (pt.isApproved = true) AND pt.processorTransactionType.id NOT IN (4,5,19,23)", cacheable = false),
    
    @NamedQuery(name = "ProcessorTransaction.findByPointOfSaleIdPurchasedDateTicketNumber", query = "FROM ProcessorTransaction pt WHERE (pt.pointOfSale.id = :pointOfSaleId) AND (pt.purchasedDate = :purchasedDate) AND (pt.ticketNumber = :ticketNumber) AND pt.processorTransactionType.id NOT IN (4,5,19,23)", cacheable = false),
    
    @NamedQuery(name = "ProcessorTransaction.updateProcessorTransactionToSettled", query = "UPDATE ProcessorTransaction pt SET "
                                                                                           + "pt.processorTransactionType.id = 2, pt.merchantAccount.id = :merchantAccountId, pt.amount = :amountInCents, pt.cardType = :cardType, pt.last4digitsOfCardNumber = :last4DigitsOfCardNumber, "
                                                                                           + "pt.cardHash = :cardHash, pt.processorTransactionId = :processorTransactionId, pt.authorizationNumber = :authorizationNumber, pt.referenceNumber = :referenceNumber, "
                                                                                           + "pt.isApproved = true, pt.processingDate = :processingDate, pt.isRfid = :isRFID, pt.preAuth = null WHERE pt.pointOfSale.id = :pointOfSaleId AND pt.purchasedDate = :purchasedDate AND pt.ticketNumber = :ticketNumber AND pt.isApproved = true AND pt.processorTransactionType.id = 1 ", cacheable = false),
    
    @NamedQuery(name = "ProcessorTransaction.updateProcessorTransactionToBatchedSettled", query = "UPDATE ProcessorTransaction pt SET "
                                                                                                  + "pt.processorTransactionType.id = 12, pt.merchantAccount.id = :merchantAccountId, pt.amount = :amountInCents, pt.cardType = :cardType, pt.last4digitsOfCardNumber = :last4DigitsOfCardNumber, "
                                                                                                  + "pt.cardHash = :cardHash, pt.processorTransactionId = :processorTransactionId, pt.authorizationNumber = :authorizationNumber, pt.referenceNumber = :referenceNumber, pt.isApproved = true, "
                                                                                                  + "pt.processingDate = :processingDate, pt.isRfid = :isRFID, pt.preAuth = null WHERE pt.pointOfSale.id = :pointOfSaleId AND pt.purchasedDate = :purchasedDate AND pt.ticketNumber = :ticketNumber AND pt.isApproved = true AND pt.processorTransactionType.id = 1 ", cacheable = false),
    @NamedQuery(name = "ProcessorTransaction.findProcessorTransactionByPurchaseId", query = "FROM ProcessorTransaction pt WHERE pt.purchase.id = :purchaseId AND pt.isApproved = true AND pt.processorTransactionType.id IN (1,2,3,6,7,8,9,10,11,12,13,14,16,17,19,20,21,23) ORDER BY pt.processingDate DESC"),
    @NamedQuery(name = "ProcessorTransaction.findProcessorTransactionByPurchaseIdAndIsRefundAndIsApproved", query = "FROM ProcessorTransaction pt WHERE pt.purchase.id = :purchaseId AND pt.isApproved = true AND pt.processorTransactionType.id = 4 AND pt.isApproved = true"),
    @NamedQuery(name = "ProcessorTransaction.findRefundedProcessorTransaction", query = "FROM ProcessorTransaction pt WHERE pt.purchase.id = :purchaseId AND pt.isApproved = true AND pt.processorTransactionType.id IN (4,18,22)"),
    @NamedQuery(name = "ProcessorTransaction.getAuthorizedProcessorTransactionsForSettlement", query = "FROM ProcessorTransaction pt WHERE pt.processorTransactionType.id = 1 AND pt.processingDate < :maxReceiveTime"),
    @NamedQuery(name = "ProcessorTransaction.findUncloseableProcessorTransaction", query = "FROM ProcessorTransaction pt WHERE pt.processorTransactionType.id = 20 AND pt.purchasedDate < :maxHoldingDay"),
    @NamedQuery(name = "ProcessorTransaction.findRefundProcessorTransaction", query = "FROM ProcessorTransaction pt WHERE pt.pointOfSale.id = :pointOfSaleId AND pt.purchasedDate = :purchasedDate AND pt.ticketNumber = :ticketNumber AND pt.isApproved = true AND pt.processorTransactionType.id IN (4,18,22)"),
    @NamedQuery(name = "ProcessorTransaction.findPartialSettlementProcessorTransaction", query = "FROM ProcessorTransaction pt WHERE pt.pointOfSale.id = :pointOfSaleId AND pt.purchasedDate = :purchasedDate AND pt.ticketNumber = :ticketNumber AND pt.isApproved = true AND pt.processorTransactionType.id = 9"),
    @NamedQuery(name = "ProcessorTransaction.deletePreAuthIdByPreAuthId", query = "UPDATE ProcessorTransaction pt SET pt.preAuth.id = null WHERE pt.preAuth.id = :preAuthId"),
    @NamedQuery(name = "ProcessorTransaction.findProcessorTransactionByPreAuthId", query = "SELECT pt FROM ProcessorTransaction pt WHERE pt.preAuth.id = :preAuthId"),
    @NamedQuery(name = "ProcessorTransaction.findPPPByIds", query = "select pt from ProcessorTransaction pt join fetch pt.pointOfSale pos left join fetch pt.purchase p where pt.id in(:processorTransactionIds)"),
    @NamedQuery(name = "ProcessorTransaction.findProcTransByPurchaseIdPurchasedDateAndTicketNo", query = "FROM ProcessorTransaction pt WHERE pt.purchase.id = :purchaseId AND pt.purchasedDate = :purchasedDate AND pt.ticketNumber = :ticketNumber AND pt.isApproved = true"),
    @NamedQuery(name = "ProcessorTransaction.findByMerchantAccountIdAndProcessorTransactionTypeIdAndHasPurchase", query = "SELECT pt FROM ProcessorTransaction pt inner join pt.purchase WHERE pt.merchantAccount.id = :merchantAccountId AND pt.processorTransactionType.id = :processorTransactionTypeId") })
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings({ "checkstyle:designforextension", "checkstyle:noclone" })
@StoryAlias(ProcessorTransaction.ALIAS)
public class ProcessorTransaction implements java.io.Serializable, Cloneable {
    /**
     * 
     */
    public static final String ALIAS = "processor transaction";
    public static final String ALIAS_CARD_HASH = "card hash";
    public static final String ALIAS_ID = "id";
    public static final String ALIAS_AMOUNT = "amount";
    public static final String ALIAS_MERCHANT_ACCOUNT = "merchant account";
    public static final String ALIAS_CARD_TYPE = "card type";
    public static final String ALIAS_TICKET_NUMBER = "ticket number";
    public static final String ALIAS_PURCHASE_CUSTOMER = "purchase customer";
    public static final String ALIAS_AUTHORIZATION_NUMBER = "authorization number";
    public static final String ALIAS_APPROVED = "approved";
    public static final String ALIAS_LAST_4_DIGITS_OF_CARD = "last 4 digits of card";
    public static final String ALIAS_PROCESSOR_TRANSACTION_TYPE = "processor transaction type";
    public static final String ALIAS_PAYSTATION = "paystation";
    public static final String ALIAS_PROCESSING_DATE = "processing date";
    public static final String ALIAS_PROCESSOR_TRANSACTION_ID = "processor transaction id";
    public static final String ALIAS_PURCHASED_DATE = "purchased date";
    private static final long serialVersionUID = -5158038236205689891L;
    
    private static final String SEPARATOR_AUTH_PREAUTH = ":";
    
    private static final String BLANK = "";
    
    private Long id;
    private PointOfSale pointOfSale;
    private Purchase purchase;
    private MerchantAccount merchantAccount;
    private ProcessorTransactionType processorTransactionType;
    private PreAuth preAuth;
    private Date purchasedDate;
    private int ticketNumber;
    private int amount;
    private String cardType;
    private short last4digitsOfCardNumber;
    private int cardChecksum;
    private Date processingDate;
    private String processorTransactionId;
    private String authorizationNumber;
    private String referenceNumber;
    private boolean isApproved;
    private String cardHash;
    private boolean isUploadedFromBoss;
    private boolean isRfid;
    private Set<PaymentCard> paymentCards = new HashSet<PaymentCard>(0);
    private Set<ElavonTransactionDetail> elavonTransactionDetails = new HashSet<ElavonTransactionDetail>(0);
    
    private String timeZone;
    private int refundResponseCode;
    private String cpsResponseMessage;
    private boolean isOffline;
    
    // For ETL
    private Date createdGmt;
    
    /*
     * Transient Flags
     * By default it's set to true that most of objects would be populated already.
     * It's set to false in "OutstandingCardTransactionCleanUpTask.java, processOutstandingTransactionSettlementsAndCleanupCardData"
     * because the record was retrieved from database and objects weren't set yet.
     */
    private boolean loaded = true;
    
    public ProcessorTransaction() {
    }
    
    public ProcessorTransaction(final ProcessorTransaction other) {
        this.id = other.getId();
        
        if (other.getPointOfSale() != null) {
            try {
                this.pointOfSale = new PointOfSale(other.getPointOfSale());
            } catch (LazyInitializationException lie) {
                this.pointOfSale = other.getPointOfSale();
            }
        }
        
        if (other.getPurchase() != null) {
            try {
                this.purchase = new Purchase(other.getPurchase().getId());
            } catch (LazyInitializationException lie) {
                this.purchase = other.getPurchase();
            }
        }
        
        if (other.getMerchantAccount() != null) {
            try {
                this.merchantAccount = new MerchantAccount(other.getMerchantAccount());
            } catch (LazyInitializationException lie) {
                this.merchantAccount = other.getMerchantAccount();
            }
        }
        
        if (other.getProcessorTransactionType() != null) {
            try {
                this.processorTransactionType = new ProcessorTransactionType(other.getProcessorTransactionType().getId());
            } catch (LazyInitializationException lie) {
                this.processorTransactionType = other.getProcessorTransactionType();
            }
        }
        
        if (other.getPreAuth() != null) {
            try {
                this.preAuth = new PreAuth(other.preAuth);
            } catch (LazyInitializationException lie) {
                this.preAuth = other.getPreAuth();
            }
            
        }
        this.purchasedDate = other.getPurchasedDate();
        this.ticketNumber = other.getTicketNumber();
        this.amount = other.getAmount();
        this.cardType = other.getCardType();
        this.last4digitsOfCardNumber = other.getLast4digitsOfCardNumber();
        this.cardChecksum = other.getCardChecksum();
        this.processingDate = other.getProcessingDate();
        this.processorTransactionId = other.getProcessorTransactionId();
        this.authorizationNumber = other.getAuthorizationNumber();
        this.referenceNumber = other.getReferenceNumber();
        this.isApproved = other.isIsApproved();
        this.cardHash = other.getCardHash();
        this.isUploadedFromBoss = other.isIsUploadedFromBoss();
        this.isRfid = other.isIsRfid();
        
        this.timeZone = other.getTimeZone();
        this.refundResponseCode = other.getRefundResponseCode();
        this.isOffline = other.isIsOffline();
        
        this.createdGmt = other.getCreatedGmt();
        
    }
    
    public ProcessorTransaction(final PointOfSale pointOfSale, final MerchantAccount merchantAccount,
            final ProcessorTransactionType processorTransactionType, final Date purchasedDate, final int ticketNumber, final int amount,
            final String cardType, final short last4digitsOfCardNumber, final short cardChecksum, final Date processingDate,
            final String processorTransactionId, final String authorizationNumber, final String referenceNumber, final boolean isApproved,
            final String cardHash, final boolean isUploadedFromBoss, final boolean isRfid) {
        this.pointOfSale = pointOfSale;
        this.merchantAccount = merchantAccount;
        this.processorTransactionType = processorTransactionType;
        this.purchasedDate = purchasedDate;
        this.ticketNumber = ticketNumber;
        this.amount = amount;
        this.cardType = cardType;
        this.last4digitsOfCardNumber = last4digitsOfCardNumber;
        this.cardChecksum = cardChecksum;
        this.processingDate = processingDate;
        this.processorTransactionId = processorTransactionId;
        this.authorizationNumber = authorizationNumber;
        this.referenceNumber = referenceNumber;
        this.isApproved = isApproved;
        this.cardHash = cardHash;
        this.isUploadedFromBoss = isUploadedFromBoss;
        this.isRfid = isRfid;
    }
    
    public ProcessorTransaction(final PointOfSale pointOfSale, final MerchantAccount merchantAccount,
            final ProcessorTransactionType processorTransactionType, final Date purchasedDate, final int ticketNumber, final int amount,
            final String cardType, final short last4digitsOfCardNumber, final short cardChecksum, final Date processingDate,
            final String processorTransactionId, final String authorizationNumber, final String referenceNumber, final boolean isApproved,
            final String cardHash, final boolean isUploadedFromBoss, final boolean isRfid, final Set<PaymentCard> paymentCards) {
        this.pointOfSale = pointOfSale;
        this.merchantAccount = merchantAccount;
        this.processorTransactionType = processorTransactionType;
        this.purchasedDate = purchasedDate;
        this.ticketNumber = ticketNumber;
        this.amount = amount;
        this.cardType = cardType;
        this.last4digitsOfCardNumber = last4digitsOfCardNumber;
        this.cardChecksum = cardChecksum;
        this.processingDate = processingDate;
        this.processorTransactionId = processorTransactionId;
        this.authorizationNumber = authorizationNumber;
        this.referenceNumber = referenceNumber;
        this.isApproved = isApproved;
        this.cardHash = cardHash;
        this.isUploadedFromBoss = isUploadedFromBoss;
        this.isRfid = isRfid;
        this.paymentCards = paymentCards;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    @StoryAlias(ALIAS_ID)
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PointOfSaleId", nullable = false)
    @StoryAlias(value = ALIAS_PAYSTATION)
    @StoryLookup(type = PointOfSale.ALIAS, searchAttribute = PointOfSale.ALIAS_SERIAL_NUMBER)
    public PointOfSale getPointOfSale() {
        return this.pointOfSale;
    }
    
    public void setPointOfSale(final PointOfSale pointOfSale) {
        this.pointOfSale = pointOfSale;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PurchaseId")
    @StoryAlias(ALIAS_PURCHASE_CUSTOMER)
    @StoryLookup(type = Purchase.ALIAS, searchAttribute = Customer.ALIAS_NAME)
    public Purchase getPurchase() {
        return this.purchase;
    }
    
    public void setPurchase(final Purchase purchase) {
        this.purchase = purchase;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MerchantAccountId")
    @StoryAlias(ALIAS_MERCHANT_ACCOUNT)
    @StoryLookup(type = MerchantAccount.ALIAS, searchAttribute = MerchantAccount.ALIAS_NAME)
    public MerchantAccount getMerchantAccount() {
        return this.merchantAccount;
    }
    
    public void setMerchantAccount(final MerchantAccount merchantAccount) {
        this.merchantAccount = merchantAccount;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ProcessorTransactionTypeId", nullable = false)
    @StoryAlias(ALIAS_PROCESSOR_TRANSACTION_TYPE)
    @StoryLookup(type = ProcessorTransactionType.ALIAS, searchAttribute = ProcessorTransactionType.ALIAS_NAME)
    public ProcessorTransactionType getProcessorTransactionType() {
        return this.processorTransactionType;
    }
    
    public void setProcessorTransactionType(final ProcessorTransactionType processorTransactionType) {
        this.processorTransactionType = processorTransactionType;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PreAuthId")
    @StoryAlias(PreAuth.ALIAS)
    @StoryLookup(type = PreAuth.ALIAS, searchAttribute = PreAuth.ALIAS_PREAUTH_ID)
    public PreAuth getPreAuth() {
        return this.preAuth;
    }
    
    public void setPreAuth(final PreAuth preAuth) {
        this.preAuth = preAuth;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @StoryAlias(ALIAS_PURCHASED_DATE)
    @Column(name = "PurchasedDate", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getPurchasedDate() {
        return this.purchasedDate;
    }
    
    public void setPurchasedDate(final Date purchasedDate) {
        this.purchasedDate = purchasedDate;
    }
    
    @Column(name = "TicketNumber", nullable = false)
    @StoryAlias(ALIAS_TICKET_NUMBER)
    public int getTicketNumber() {
        return this.ticketNumber;
    }
    
    public void setTicketNumber(final int ticketNumber) {
        this.ticketNumber = ticketNumber;
    }
    
    @Column(name = "Amount", nullable = false)
    @StoryAlias(ALIAS_AMOUNT)
    public int getAmount() {
        return this.amount;
    }
    
    public void setAmount(final int amount) {
        this.amount = amount;
    }
    
    @Column(name = "CardType", nullable = false, length = HibernateConstants.VARCHAR_LENGTH_20)
    @StoryAlias(ALIAS_CARD_TYPE)
    public String getCardType() {
        return this.cardType;
    }
    
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }
    
    @Column(name = "Last4DigitsOfCardNumber", nullable = false)
    @StoryAlias(ALIAS_LAST_4_DIGITS_OF_CARD)
    public short getLast4digitsOfCardNumber() {
        return this.last4digitsOfCardNumber;
    }
    
    public void setLast4digitsOfCardNumber(final short last4digitsOfCardNumber) {
        this.last4digitsOfCardNumber = last4digitsOfCardNumber;
    }
    
    @Column(name = "CardChecksum", nullable = false)
    public int getCardChecksum() {
        return this.cardChecksum;
    }
    
    public void setCardChecksum(final int cardChecksum) {
        this.cardChecksum = cardChecksum;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ProcessingDate", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    @StoryAlias(ALIAS_PROCESSING_DATE)
    public Date getProcessingDate() {
        return this.processingDate;
    }
    
    public void setProcessingDate(final Date processingDate) {
        this.processingDate = processingDate;
    }
    
    @Column(name = "ProcessorTransactionId", nullable = false, length = HibernateConstants.VARCHAR_LENGTH_50)
    @StoryAlias(ALIAS_PROCESSOR_TRANSACTION_ID)
    public String getProcessorTransactionId() {
        if (this.processorTransactionId == null) {
            return BLANK;
        }
        return this.processorTransactionId;
    }
    
    public void setProcessorTransactionId(final String processorTransactionId) {
        this.processorTransactionId = processorTransactionId;
    }
    
    @Column(name = "AuthorizationNumber", length = HibernateConstants.VARCHAR_LENGTH_30)
    @StoryAlias(ALIAS_AUTHORIZATION_NUMBER)
    public String getAuthorizationNumber() {
        return this.authorizationNumber;
    }
    
    public void setAuthorizationNumber(final String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }
    
    @Transient
    public String getAuthorizationNumberAndPreAuthId() {
        return formatAuthAndPreAuth(this.authorizationNumber, (this.preAuth == null) ? null : this.preAuth.getId());
    }
    
    public static String formatAuthAndPreAuth(final String authNumber, final String preAuthIdStr) {
        Long preAuthId;
        try {
            preAuthId = new Long(preAuthIdStr);
        } catch (NumberFormatException nfe) {
            preAuthId = null;
        }
        
        return formatAuthAndPreAuth(authNumber, preAuthId);
    }
    
    public static String formatAuthAndPreAuth(final String authNumber, final Long preAuthId) {
        final StringBuilder result = new StringBuilder();
        if (authNumber != null) {
            result.append(authNumber);
        }
        
        // Append :preAuthId only when there is a PreAuthId
        if ((preAuthId != null) && (preAuthId > 0)) {
            if (result.indexOf(SEPARATOR_AUTH_PREAUTH) < 0) {
                result.append(SEPARATOR_AUTH_PREAUTH);
                result.append(preAuthId);
            }
        }
        
        return result.toString();
    }
    
    @Transient
    public String getSanitizedAuthorizationNumber() {
        final int indexOfResult = this.authorizationNumber == null ? -1 : this.authorizationNumber.indexOf(':');
        if (indexOfResult > -1) {
            return this.authorizationNumber.substring(0, indexOfResult);
            
        } else {
            return this.authorizationNumber;
        }
    }
    
    @Column(name = "ReferenceNumber", nullable = false, length = HibernateConstants.VARCHAR_LENGTH_30)
    public String getReferenceNumber() {
        return this.referenceNumber;
    }
    
    public void setReferenceNumber(final String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }
    
    @Column(name = "IsApproved", nullable = false)
    @StoryAlias(ALIAS_APPROVED)
    public boolean isIsApproved() {
        return this.isApproved;
    }
    
    public void setIsApproved(final boolean isApproved) {
        this.isApproved = isApproved;
    }
    
    @Column(name = "CardHash", nullable = false, length = HibernateConstants.VARCHAR_LENGTH_30)
    @StoryAlias(ALIAS_CARD_HASH)
    public String getCardHash() {
        return this.cardHash;
    }
    
    public void setCardHash(final String cardHash) {
        this.cardHash = cardHash;
    }
    
    @Column(name = "IsUploadedFromBoss", nullable = false)
    public boolean isIsUploadedFromBoss() {
        return this.isUploadedFromBoss;
    }
    
    public void setIsUploadedFromBoss(final boolean isUploadedFromBoss) {
        this.isUploadedFromBoss = isUploadedFromBoss;
    }
    
    @Column(name = "IsRFID", nullable = false)
    public boolean isIsRfid() {
        return this.isRfid;
    }
    
    public void setIsRfid(final boolean isRfid) {
        this.isRfid = isRfid;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "processorTransaction")
    public Set<PaymentCard> getPaymentCards() {
        return this.paymentCards;
    }
    
    public void setPaymentCards(final Set<PaymentCard> paymentCards) {
        this.paymentCards = paymentCards;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "processorTransaction")
    public Set<ElavonTransactionDetail> getElavonTransactionDetails() {
        return this.elavonTransactionDetails;
    }
    
    public void setElavonTransactionDetails(final Set<ElavonTransactionDetail> elavonTransactionDetails) {
        this.elavonTransactionDetails = elavonTransactionDetails;
    }
    
    @Column(name = "CreatedGMT", nullable = false)
    public Date getCreatedGmt() {
        return this.createdGmt;
    }
    
    public void setCreatedGmt(final Date createdGmt) {
        this.createdGmt = createdGmt;
    }
    
    @Transient
    public String getTimeZone() {
        return this.timeZone;
    }
    
    public void setTimeZone(final String timeZone) {
        this.timeZone = timeZone;
    }
    
    @Transient
    public int getRefundResponseCode() {
        return this.refundResponseCode;
    }
    
    public void setRefundResponseCode(final int refundResponseCode) {
        this.refundResponseCode = refundResponseCode;
    }
    
    @Transient
    public String getCPSResponseMessage() {
        return cpsResponseMessage;
    }
    
    public void setCPSResponseMessage(final String cpsResponseMessage) {
        this.cpsResponseMessage = cpsResponseMessage;
    }
    
    @Transient
    public boolean isIsOffline() {
        return this.isOffline;
    }
    
    public void setIsOffline(final boolean isOffline) {
        this.isOffline = isOffline;
    }
    
    @Transient
    public boolean isLoaded() {
        return this.loaded;
    }
    
    public void setLoaded(final boolean loaded) {
        this.loaded = loaded;
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException {
        final ProcessorTransaction ptx = (ProcessorTransaction) super.clone();
        ptx.setMerchantAccount(this.merchantAccount);
        ptx.setProcessorTransactionType(this.processorTransactionType);
        return ptx;
    }
    
    @Override
    public String toString() {
        final StringBuilder str = new StringBuilder("{ ");
        
        str.append("ProcessorTransactionTypeId: ");
        
        if (this.getProcessorTransactionType() == null) {
            str.append(HibernateConstants.STRING_NULL);
        } else {
            str.append(getProcessorTransactionType().getId());
        }
        
        str.append(", PointOfSale: ");
        if (this.pointOfSale != null) {
            str.append(this.pointOfSale.getId());
        } else {
            str.append(HibernateConstants.STRING_NULL);
        }
        
        str.append(", MerchantAccountId: ");
        if (getMerchantAccount() != null) {
            str.append(getMerchantAccount().getId());
        } else {
            str.append(HibernateConstants.STRING_NULL);
        }
        
        str.append(", TicketNumber: ");
        str.append(getTicketNumber());
        
        str.append(", Amount: ");
        str.append(getAmount());
        
        str.append(", CardType: ");
        str.append(getCardType());
        
        str.append(", Last4Digits: ");
        str.append(getLast4digitsOfCardNumber());
        
        str.append(", CardHash: ");
        str.append(getCardHash());
        
        str.append(", PurchasedDate: ");
        if (getPurchasedDate() == null) {
            str.append(HibernateConstants.STRING_NULL);
        } else {
            str.append(getPurchasedDate());
        }
        
        str.append(", ProcessingDate: ");
        if (getProcessingDate() == null) {
            str.append(HibernateConstants.STRING_NULL);
        } else {
            str.append(getProcessingDate());
        }
        
        str.append(", ProcessorTransactionId: ");
        str.append(getProcessorTransactionId());
        
        str.append(", AuthorizationNumber: ");
        str.append(getAuthorizationNumber());
        
        str.append(", ReferenceNumber: ");
        str.append(getReferenceNumber());
        
        str.append(", Approved: ");
        str.append(isIsApproved() ? "1" : "0");
        str.append(" }");
        
        return str.toString();
    }
    
}
