package com.digitalpaytech.domain;

// Generated 31-Aug-2012 2:05:49 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.CascadeType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryDelegateAttribute;
import com.digitalpaytech.annotation.StoryDelegateObject;
import com.digitalpaytech.annotation.StoryDelegateObjects;
import com.digitalpaytech.annotation.StoryId;
import com.digitalpaytech.annotation.StoryLookup;
import com.digitalpaytech.domain.util.HibernateConstants;

/**
 * Purchase generated by hbm2java
 */
@Entity
@Table(name = "Purchase")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({
    @NamedQuery(name = "Purchase.findUniquePurchaseForSms", query = "FROM Purchase p WHERE p.customer.id = :customerId AND p.pointOfSale.id = :pointOfSaleId AND p.purchaseGmt = :purchaseGmt AND p.purchaseNumber = :purchaseNumber"),
    @NamedQuery(name = "Purchase.findLatestPurchaseByPointOfSaleIdAndPurchaseNumber", query = "FROM Purchase p WHERE p.pointOfSale.id = :pointOfSaleId AND p.purchaseNumber = :purchaseNumber ORDER BY p.purchaseGmt DESC"),
    @NamedQuery(name = "Purchase.findPurchaseByPointOfSaleIdAndPurchaseNumberAndpPurchaseGmt", query = "FROM Purchase p WHERE p.pointOfSale.id = :pointOfSaleId AND p.purchaseNumber = :purchaseNumber AND p.purchaseGmt = :purchaseGmt ORDER BY p.purchaseGmt DESC") })
@StoryAlias(Purchase.ALIAS)
@SuppressWarnings({ "checkstyle:designforextension" })
public class Purchase implements java.io.Serializable {
    
    public static final String ALIAS_CASH_PAID_AMOUNT = "cash paid amount";
    public static final String ALIAS = "purchase";
    public static final String ALIAS_ID = "purchase id";
    public static final String ALIAS_CUSTOMER  = "customer";
    public static final String ALIAS_TRANSACTION_TYPE = "transaction type";
    public static final String ALIAS_PAYMENT_TYPE = "payment type";
    public static final String ALIAS_REFUND_SLIP = "refund slip";
    public static final String ALIAS_ORIGINAL_AMOUNT = "original amount";
    public static final String ALIAS_CHARGED_AMOUNT = "charged amount";
    public static final String ALIAS_EXCESS_PAYMENT_AMOUNT = "excess payment amount";
    public static final String ALIAS_LOCATION = "location";
    public static final String ALIAS_PURCHASE_DATE = "purchase date";
    public static final String ALIAS_CARD_PAID_AMOUNT = "card paid amount";
    public static final String ALIAS_UNIFIED_RATE = "unified rate";
    public static final String ALIAS_PROCESSOR_TRANSACTION = "processor transaction";
    public static final String ALIAS_MERCHANT_ACCOUNT = "merchant account";
    public static final String ALIAS_SERIAL_NUMBER="serial number";
    
    private static final long serialVersionUID = 9022199191257044022L;
    private Long id;
    private PointOfSale pointOfSale;
    private PaystationSetting paystationSetting;
    private Location location;
    private TransactionType transactionType;
    private UnifiedRate unifiedRate;
    private Customer customer;
    private Coupon coupon;
    private PaymentType paymentType;
    private Date purchaseGmt;
    private int purchaseNumber;
    private int originalAmount;
    private int chargedAmount;
    private int changeDispensedAmount;
    private int excessPaymentAmount;
    private int cashPaidAmount;
    private int coinPaidAmount;
    private int billPaidAmount;
    private int coinCount;
    private int billCount;
    private int cardPaidAmount;
    private int rateAmount;
    private int rateRevenueAmount;
    private boolean isOffline;
    private boolean isRefundSlip;
    private Date transactionGmt;
    private Date createdGmt;
    private Set<ProcessorTransaction> processorTransactions = new HashSet<ProcessorTransaction>(0);
    private Set<Permit> permits = new HashSet<Permit>(0);
    private Set<PurchaseCommit> purchaseCommits = new HashSet<PurchaseCommit>(0);
    private Set<PurchaseCollection> purchaseCollections = new HashSet<PurchaseCollection>(0);
    private Set<PaymentCash> paymentCashs = new HashSet<PaymentCash>(0);
    private Set<PaymentCard> paymentCards = new HashSet<PaymentCard>(0);
    private Set<PurchaseTax> purchaseTaxes = new HashSet<PurchaseTax>(0);
    // cardData & paymentSmartCard are transient property for carrying information.
    private String cardData;
    private PaymentSmartCard paymentSmartCard;
    
    public Purchase() {
    }
    
    public Purchase(final Long id) {
        this.id = id;
    }
    
    public Purchase(final PointOfSale pointOfSale, final PaystationSetting paystationSetting, final Location location,
            final TransactionType transactionType, final UnifiedRate unifiedRate, final PaymentType paymentType, final Date purchaseGmt,
            final int purchaseNumber, final int originalAmount, final int chargedAmount, final int changeDispensedAmount,
            final int excessPaymentAmount, final int cashPaidAmount, final int coinPaidAmount, final int billPaidAmount, final int cardPaidAmount,
            final int rateAmount, final int rateRevenueAmount, final boolean isOffline, final boolean isRefundSlip, final Date createdGmt) {
        this.pointOfSale = pointOfSale;
        this.paystationSetting = paystationSetting;
        this.location = location;
        this.transactionType = transactionType;
        this.unifiedRate = unifiedRate;
        this.paymentType = paymentType;
        this.purchaseGmt = purchaseGmt;
        this.purchaseNumber = purchaseNumber;
        this.originalAmount = originalAmount;
        this.chargedAmount = chargedAmount;
        this.changeDispensedAmount = changeDispensedAmount;
        this.excessPaymentAmount = excessPaymentAmount;
        this.cashPaidAmount = cashPaidAmount;
        this.coinPaidAmount = coinPaidAmount;
        this.billPaidAmount = billPaidAmount;
        this.cardPaidAmount = cardPaidAmount;
        this.rateAmount = rateAmount;
        this.rateRevenueAmount = rateRevenueAmount;
        this.isOffline = isOffline;
        this.isRefundSlip = isRefundSlip;
        this.createdGmt = createdGmt;
    }
    
    public Purchase(final PointOfSale pointOfSale, final PaystationSetting paystationSetting, final Location location,
            final TransactionType transactionType, final UnifiedRate unifiedRate, final Coupon coupon, final PaymentType paymentType,
            final Date purchaseGmt, final int purchaseNumber, final int originalAmount, final int chargedAmount, final int changeDispensedAmount,
            final int excessPaymentAmount, final int cashPaidAmount, final int coinPaidAmount, final int billPaidAmount, final int cardPaidAmount,
            final int rateAmount, final int rateRevenueAmount, final boolean isOffline, final boolean isRefundSlip, final Date createdGmt,
            final Set<Permit> permits, final Set<PurchaseCommit> purchaseCommits, final Set<PaymentCash> paymentCashs,
            final Set<PurchaseTax> purchaseTaxes, final Set<PaymentCard> paymentCards) {
        this.pointOfSale = pointOfSale;
        this.paystationSetting = paystationSetting;
        this.location = location;
        this.transactionType = transactionType;
        this.unifiedRate = unifiedRate;
        this.coupon = coupon;
        this.paymentType = paymentType;
        this.purchaseGmt = purchaseGmt;
        this.purchaseNumber = purchaseNumber;
        this.originalAmount = originalAmount;
        this.chargedAmount = chargedAmount;
        this.changeDispensedAmount = changeDispensedAmount;
        this.excessPaymentAmount = excessPaymentAmount;
        this.cashPaidAmount = cashPaidAmount;
        this.coinPaidAmount = coinPaidAmount;
        this.billPaidAmount = billPaidAmount;
        this.cardPaidAmount = cardPaidAmount;
        this.rateAmount = rateAmount;
        this.rateRevenueAmount = rateRevenueAmount;
        this.isOffline = isOffline;
        this.isRefundSlip = isRefundSlip;
        this.createdGmt = createdGmt;
        this.permits = permits;
        this.purchaseCommits = purchaseCommits;
        this.paymentCashs = paymentCashs;
        this.purchaseTaxes = purchaseTaxes;
        this.paymentCards = paymentCards;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    @StoryId
    @StoryAlias(ALIAS_ID)
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PointOfSaleId", nullable = false)
    @StoryAlias(ALIAS_SERIAL_NUMBER)
    @StoryLookup(type = PointOfSale.ALIAS, searchAttribute = PointOfSale.ALIAS_SERIAL_NUMBER)
    public PointOfSale getPointOfSale() {
        return this.pointOfSale;
    }
    
    public void setPointOfSale(final PointOfSale pointOfSale) {
        this.pointOfSale = pointOfSale;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PaystationSettingId", nullable = false)
    @StoryAlias("pay station setting")
    @StoryLookup(type = PaystationSetting.ALIAS, searchAttribute = PaystationSetting.ALIAS_NAME)
    public PaystationSetting getPaystationSetting() {
        return this.paystationSetting;
    }
    
    public void setPaystationSetting(final PaystationSetting paystationSetting) {
        this.paystationSetting = paystationSetting;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LocationId", nullable = false)
    @StoryAlias(ALIAS_LOCATION)
    @StoryLookup(type = Location.ALIAS, searchAttribute = Location.ALIAS_LOCATION_NAME)
    public Location getLocation() {
        return this.location;
    }
    
    public void setLocation(final Location location) {
        this.location = location;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TransactionTypeId", nullable = false)
    @StoryAlias(ALIAS_TRANSACTION_TYPE)
    @StoryLookup(type = TransactionType.ALIAS, searchAttribute = TransactionType.ALIAS_NAME)
    public TransactionType getTransactionType() {
        return this.transactionType;
    }
    
    public void setTransactionType(final TransactionType transactionType) {
        this.transactionType = transactionType;
    }
    
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "UnifiedRateId", nullable = false)
    @StoryAlias(ALIAS_UNIFIED_RATE)
    @StoryLookup(type = UnifiedRate.ALIAS, searchAttribute = UnifiedRate.ALIAS_NAME)
    public UnifiedRate getUnifiedRate() {
        return this.unifiedRate;
    }
    
    public void setUnifiedRate(final UnifiedRate unifiedRate) {
        this.unifiedRate = unifiedRate;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerId", nullable = false)
    @StoryAlias(value = Customer.ALIAS_NAME)
    @StoryLookup(type = Customer.ALIAS, searchAttribute = Customer.ALIAS_NAME)
    public Customer getCustomer() {
        return this.customer;
    }
    
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CouponId")
    public Coupon getCoupon() {
        return this.coupon;
    }
    
    public void setCoupon(final Coupon coupon) {
        this.coupon = coupon;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PaymentTypeId", nullable = false)
    @StoryAlias(ALIAS_PAYMENT_TYPE)
    @StoryLookup(type = PaymentType.ALIAS, searchAttribute = PaymentType.ALIAS_NAME)
    public PaymentType getPaymentType() {
        return this.paymentType;
    }
    
    public void setPaymentType(final PaymentType paymentType) {
        this.paymentType = paymentType;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "PurchaseGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    @StoryAlias(ALIAS_PURCHASE_DATE)
    public Date getPurchaseGmt() {
        return this.purchaseGmt;
    }
    
    public void setPurchaseGmt(final Date purchaseGmt) {
        this.purchaseGmt = purchaseGmt;
    }
    
    @Column(name = "PurchaseNumber", nullable = false)
    public int getPurchaseNumber() {
        return this.purchaseNumber;
    }
    
    public void setPurchaseNumber(final int purchaseNumber) {
        this.purchaseNumber = purchaseNumber;
    }
    
    @Column(name = "OriginalAmount", nullable = false)
    @StoryAlias(ALIAS_ORIGINAL_AMOUNT)
    public int getOriginalAmount() {
        return this.originalAmount;
    }
    
    public void setOriginalAmount(final int originalAmount) {
        this.originalAmount = originalAmount;
    }
    
    @Column(name = "ChargedAmount", nullable = false)
    @StoryAlias(ALIAS_CHARGED_AMOUNT)
    public int getChargedAmount() {
        return this.chargedAmount;
    }
    
    public void setChargedAmount(final int chargedAmount) {
        this.chargedAmount = chargedAmount;
    }
    
    @Column(name = "ChangeDispensedAmount", nullable = false)
    public int getChangeDispensedAmount() {
        return this.changeDispensedAmount;
    }
    
    public void setChangeDispensedAmount(final int changeDispensedAmount) {
        this.changeDispensedAmount = changeDispensedAmount;
    }
    
    @Column(name = "ExcessPaymentAmount", nullable = false)
    @StoryAlias(ALIAS_EXCESS_PAYMENT_AMOUNT)
    public int getExcessPaymentAmount() {
        return this.excessPaymentAmount;
    }
    
    public void setExcessPaymentAmount(final int excessPaymentAmount) {
        this.excessPaymentAmount = excessPaymentAmount;
    }
    
    @Column(name = "CashPaidAmount", nullable = false)
    @StoryAlias(ALIAS_CASH_PAID_AMOUNT)
    public int getCashPaidAmount() {
        return this.cashPaidAmount;
    }
    
    public void setCashPaidAmount(final int cashPaidAmount) {
        this.cashPaidAmount = cashPaidAmount;
    }
    
    @Column(name = "CoinPaidAmount", nullable = false)
    public int getCoinPaidAmount() {
        return this.coinPaidAmount;
    }
    
    public void setCoinPaidAmount(final int coinPaidAmount) {
        this.coinPaidAmount = coinPaidAmount;
    }
    
    @Column(name = "BillPaidAmount", nullable = false)
    public int getBillPaidAmount() {
        return this.billPaidAmount;
    }
    
    public void setBillPaidAmount(final int billPaidAmount) {
        this.billPaidAmount = billPaidAmount;
    }
    
    @Column(name = "CardPaidAmount", nullable = false)
    @StoryAlias(ALIAS_CARD_PAID_AMOUNT)
    public int getCardPaidAmount() {
        return this.cardPaidAmount;
    }
    
    public void setCardPaidAmount(final int cardPaidAmount) {
        this.cardPaidAmount = cardPaidAmount;
    }
    
    @Column(name = "RateAmount", nullable = false)
    public int getRateAmount() {
        return this.rateAmount;
    }
    
    public void setRateAmount(final int rateAmount) {
        this.rateAmount = rateAmount;
    }
    
    @Column(name = "RateRevenueAmount", nullable = false)
    public int getRateRevenueAmount() {
        return this.rateRevenueAmount;
    }
    
    public void setRateRevenueAmount(final int rateRevenueAmount) {
        this.rateRevenueAmount = rateRevenueAmount;
    }
    
    @Column(name = "IsOffline", nullable = false)
    public boolean isIsOffline() {
        return this.isOffline;
    }
    
    public void setIsOffline(final boolean isOffline) {
        this.isOffline = isOffline;
    }
    
    @Column(name = "IsRefundSlip", nullable = false)
    @StoryAlias(ALIAS_REFUND_SLIP)
    public boolean isIsRefundSlip() {
        return this.isRefundSlip;
    }
    
    public void setIsRefundSlip(final boolean isRefundSlip) {
        this.isRefundSlip = isRefundSlip;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TransactionGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getTransactionGmt() {
        return this.transactionGmt;
    }
    
    public void setTransactionGmt(final Date transactionGmt) {
        this.transactionGmt = transactionGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getCreatedGmt() {
        return this.createdGmt;
    }
    
    public void setCreatedGmt(final Date createdGmt) {
        this.createdGmt = createdGmt;
    }
    
    @Transient
    public Permit getFirstPermit() {
        Permit result = null;
        if (this.permits != null) {
            final Iterator<Permit> itr = this.permits.iterator();
            if (itr.hasNext()) {
                result = itr.next();
            }
        }
        
        return result;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "purchase", cascade = CascadeType.ALL)
    public Set<Permit> getPermits() {
        return this.permits;
    }
    
    public void setPermits(final Set<Permit> permits) {
        this.permits = permits;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "purchase", cascade = CascadeType.ALL)
    public Set<PurchaseCommit> getPurchaseCommits() {
        return this.purchaseCommits;
    }
    
    public void setPurchaseCommits(final Set<PurchaseCommit> purchaseCommits) {
        this.purchaseCommits = purchaseCommits;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "purchase", cascade = CascadeType.ALL)
    public Set<PurchaseCollection> getPurchaseCollections() {
        return this.purchaseCollections;
    }
    
    public void setPurchaseCollections(final Set<PurchaseCollection> purchaseCollections) {
        this.purchaseCollections = purchaseCollections;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "purchase", cascade = CascadeType.ALL)
    public Set<PaymentCash> getPaymentCashs() {
        return this.paymentCashs;
    }
    
    public void setPaymentCashs(final Set<PaymentCash> paymentCashs) {
        this.paymentCashs = paymentCashs;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "purchase", cascade = CascadeType.ALL)
    public Set<PurchaseTax> getPurchaseTaxes() {
        return this.purchaseTaxes;
    }
    
    public void setPurchaseTaxes(final Set<PurchaseTax> purchaseTaxes) {
        this.purchaseTaxes = purchaseTaxes;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "purchase", cascade = CascadeType.ALL)
    public Set<PaymentCard> getPaymentCards() {
        return this.paymentCards;
    }
    
    public void setPaymentCards(final Set<PaymentCard> paymentCards) {
        this.paymentCards = paymentCards;
    }
    
    @Column(name = "CoinCount", nullable = false)
    public int getCoinCount() {
        return this.coinCount;
    }
    
    public void setCoinCount(final int coinCount) {
        this.coinCount = coinCount;
    }
    
    @Column(name = "BillCount", nullable = false)
    public int getBillCount() {
        return this.billCount;
    }
    
    public void setBillCount(final int billCount) {
        this.billCount = billCount;
    }
    
    @StoryDelegateObjects({
        @StoryDelegateObject({
            @StoryDelegateAttribute(from = "purchase - processorTransaction", to = ProcessorTransaction.ALIAS_PURCHASE_CUSTOMER, dependsOn = "this"),
            @StoryDelegateAttribute(from = ALIAS_MERCHANT_ACCOUNT, to = ProcessorTransaction.ALIAS_MERCHANT_ACCOUNT, persistent = true),
            @StoryDelegateAttribute(from = "approved processor transaction", to = ProcessorTransaction.ALIAS_APPROVED),
            @StoryDelegateAttribute(from = "processor transaction type" , to = ProcessorTransaction.ALIAS_PROCESSOR_TRANSACTION_TYPE),
            @StoryDelegateAttribute(from = "card type" , to = ProcessorTransaction.ALIAS_CARD_TYPE),
            @StoryDelegateAttribute(from = "processing date" , to = ProcessorTransaction.ALIAS_PROCESSING_DATE),
            @StoryDelegateAttribute(from = "last4digits" , to = ProcessorTransaction.ALIAS_LAST_4_DIGITS_OF_CARD),
            @StoryDelegateAttribute(from = "authorization number" , to = ProcessorTransaction.ALIAS_AUTHORIZATION_NUMBER),
            @StoryDelegateAttribute(from = "processor transaction amount" , to = ProcessorTransaction.ALIAS_AMOUNT, dependsOn = ALIAS_ORIGINAL_AMOUNT),
            @StoryDelegateAttribute(from = "point of sale" , to = ProcessorTransaction.ALIAS_PAYSTATION, dependsOn = ALIAS_SERIAL_NUMBER),
            @StoryDelegateAttribute(from = "ticket number" , to = ProcessorTransaction.ALIAS_TICKET_NUMBER ),
        })
    })
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "purchase")
    public Set<ProcessorTransaction> getProcessorTransactions() {
        return this.processorTransactions;
    }
    
    public void setProcessorTransactions(final Set<ProcessorTransaction> processorTransactions) {
        this.processorTransactions = processorTransactions;
    }
    
    @Transient
    public String getCardData() {
        return this.cardData;
    }
    
    public void setCardData(final String cardData) {
        this.cardData = cardData;
    }
    
    @Transient
    public PaymentSmartCard getPaymentSmartCard() {
        return this.paymentSmartCard;
    }
    
    public void setPaymentSmartCard(final PaymentSmartCard paymentSmartCard) {
        this.paymentSmartCard = paymentSmartCard;
    }
}
