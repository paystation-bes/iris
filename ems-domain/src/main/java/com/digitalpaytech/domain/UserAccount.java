package com.digitalpaytech.domain;

// Generated 18-Apr-2012 2:22:51 PM by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.annotation.StorageType;
import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryId;
import com.digitalpaytech.annotation.StoryLookup;
import com.digitalpaytech.domain.util.HibernateConstants;

/**
 * UserAccount generated by hbm2java
 */
@Entity
@Table(name = "UserAccount")
@NamedQueries({
    @NamedQuery(name = "UserAccount.findUserAccountByUserName", query = "from UserAccount ua where lower(ua.userName) = :userName AND ua.isAliasUser = 0", cacheable = true),
    @NamedQuery(name = "UserAccount.findAdminUserAccountsForParent", query = "SELECT ua FROM UserAccount ua INNER JOIN ua.userAccount ua2 WHERE ua.customer.id = :customerId AND ua2.customer.id = :parentCustomerId AND ua.userStatusType.id != 2", cacheable = true),
    @NamedQuery(name = "UserAccount.findAdminUserAccountWithIsAllChilds", query = "from UserAccount ua where ua.customer.id = :customerId AND ua.isAllChilds = 1 AND ua.userStatusType.id != 2", cacheable = true),
    @NamedQuery(name = "UserAccount.findChildUserAccountsByParentUserId", query = "from UserAccount ua where ua.isAliasUser = 1 AND ua.userStatusType.id = 1 AND ua.userAccount.id = :parentUserAccountId", cacheable = true),
    @NamedQuery(name = "UserAccount.findUndeletedUserAccountByUserName", query = "from UserAccount ua where lower(ua.userName) = :userName and UserStatusTypeId <> 2 AND ua.isAliasUser = 0", cacheable = true),
    @NamedQuery(name = "UserAccount.findUserAccountsByCustomerId", query = "from UserAccount ua where ua.customer.id = :customerId AND ua.isAliasUser = 0 order by ua.lastName, ua.firstName", cacheable = true),
    @NamedQuery(name = "UserAccount.findUserAccountsWithAliasUsersByCustomerId", query = "from UserAccount ua where ua.customer.id = :customerId order by ua.lastName, ua.firstName", cacheable = true),
    @NamedQuery(name = "UserAccount.findUserAccountsByCustomerIdAndUserAccountTypeId", query = "from UserAccount ua where ua.customer.id = :customerId and ua.userStatusType.id != 2 order by ua.firstName, ua.lastName", cacheable = true),
    @NamedQuery(name = "UserAccount.findUserAccountsByCustomerIdAndControllerCustomerIdAndUserAccountTypeId", query = "from UserAccount ua where (ua.customer.id = :customerId OR ua.customer.id = :controllerCustomerId) and ua.userStatusType.id != 2 AND ua.isAliasUser = 0 order by ua.firstName, ua.lastName", cacheable = true),
    @NamedQuery(name = "UserAccount.findUserAccountsByCustomerIdAndMobileApplicationTypeIdAndDate", query = "SELECT ua from UserAccount ua JOIN ua.mobileSessionTokens mst JOIN mst.mobileLicense license JOIN license.mobileApplicationType mat WHERE ua.customer.id =:customerId AND mat.id = :mobileApplicationTypeId AND mst.expiryDate > :date AND ua.isAliasUser = 0"),
    @NamedQuery(name = "UserAccount.findUserAccountsByCustomerIdAndRoleId", query = "SELECT ua FROM UserAccount ua JOIN ua.userRoles ur WHERE ua.customer.id = :customerId AND ua.userStatusType.id != 2 AND ur.role.id = :roleId  AND ua.isAliasUser = 0 ORDER BY ua.firstName, ua.lastName", cacheable = true),
    @NamedQuery(name = "UserAccount.findUndeletedUserAccountByUserAccountId", query = "from UserAccount ua where ua.userAccount.id = :userAccountId and ua.userStatusType.id <> 2", cacheable = true),
    @NamedQuery(name = "UserAccount.findUndeletedUserAccountByUserAccountIdAndCustomerId", query = "from UserAccount ua where ua.userAccount.id = :userAccountId and ua.customer.id = :customerId and ua.userStatusType.id <> 2", cacheable = true),
    @NamedQuery(name = "UserAccount.findUndeletedAliasUsersByCustomerId", query = "from UserAccount ua where ua.customer.id = :customerId AND ua.isAliasUser = 1 AND ua.userStatusType.id <> 2", cacheable = true),
    @NamedQuery(name = "UserAccount.findUserAccountByLinkUserName", query = "from UserAccount ua where linkUserName = :linkUserName", cacheable = false),})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@StoryAlias(UserAccount.ALIAS)
@SuppressWarnings({ "checkstyle:designforextension" })
public class UserAccount implements java.io.Serializable {
    public static final String ALIAS = "User Account";
    public static final String ALIAS_ID = "Id";
    public static final String ALIAS_USER_NAME = "User Name";
    public static final String ALIAS_PASSWORD = "password";
    public static final String ALIAS_USER_STATUS_TYPE = "User status type";
    public static final String ALIAS_USER_ACCOUNT_CUSTOMER = "User Account Customer";
    public static final String ALIAS_PERMISSION = "Permission";
    
    /**
     * 
     */
    private static final long serialVersionUID = -405032856886655162L;
    private Integer id;
    private int version;
    private UserAccount userAccount;
    private CustomerEmail customerEmail;
    private Customer customer;
    private UserStatusType userStatusType;
    private String userName;
    private String linkUserName;
    private String firstName;
    private String lastName;
    private String password;
    private boolean isPasswordTemporary;
    private boolean isAliasUser;
    private boolean isAllChilds;
    private boolean isDefaultAlias;
    private Date archiveDate;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    private String passwordSalt;
    private Set<Role> roles = new HashSet<Role>(0);
    private Set<UserRole> userRoles = new HashSet<UserRole>(0);
    private Set<Notification> notifications = new HashSet<Notification>(0);
    private Set<RolePermission> rolePermissions = new HashSet<RolePermission>(0);
    private Set<EmsProperties> emsPropertieses = new HashSet<EmsProperties>(0);
    private Set<ActivityLogin> activityLogins = new HashSet<ActivityLogin>(0);
    private Set<CustomerAgreement> customerAgreements = new HashSet<CustomerAgreement>(0);
    private Set<Permission> permissions = new HashSet<Permission>(0);
    private Set<ServiceAgreement> serviceAgreements = new HashSet<ServiceAgreement>(0);
    private Set<CustomerRole> customerRoles = new HashSet<CustomerRole>(0);
    private Set<UserDefaultDashboard> userDefaultDashboards = new HashSet<UserDefaultDashboard>(0);
    private Set<MobileSessionToken> mobileSessionTokens = new HashSet<MobileSessionToken>(0);
    private Set<UserAccountRoute> userAccountRoutes = new HashSet<UserAccountRoute>(0);
    private Set<MobileAppHistory> mobileAppHistories = new HashSet<MobileAppHistory>(0);
    private Set<UserAccount> childUserAccounts = new HashSet<UserAccount>(0);
    
    @Transient
    private String fullName;
    
    public UserAccount() {
    }
    
    public UserAccount(final Customer customer, final UserStatusType userStatusType, final String userName, final String firstName,
            final String lastName, final String password, final String passwordSalt, final boolean isPasswordTemporary, final Date archiveDate,
            final Date lastModifiedGmt, final int lastModifiedByUserId) {
        this.customer = customer;
        this.userStatusType = userStatusType;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.passwordSalt = passwordSalt;
        this.isPasswordTemporary = isPasswordTemporary;
        this.archiveDate = archiveDate;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    public UserAccount(final Customer customer, final UserStatusType userStatusType, final String userName, final CustomerEmail customerEmail,
            final String firstName, final String lastName, final String password, final String passwordSalt, final boolean isPasswordTemporary,
            final Date archiveDate, final Date lastModifiedGmt, final int lastModifiedByUserId, final Set<UserRole> userRoles, final Set<Role> roles,
            final Set<Notification> notifications, final Set<RolePermission> rolePermissions, final Set<EmsProperties> emsPropertieses,
            final Set<ActivityLogin> activityLogins, final Set<CustomerAgreement> customerAgreements, final Set<Permission> permissions,
            final Set<ServiceAgreement> serviceAgreements, final Set<CustomerRole> customerRoles) {
        this.customer = customer;
        this.userStatusType = userStatusType;
        this.userName = userName;
        this.customerEmail = customerEmail;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.passwordSalt = passwordSalt;
        this.isPasswordTemporary = isPasswordTemporary;
        this.archiveDate = archiveDate;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
        this.userRoles = userRoles;
        this.roles = roles;
        this.notifications = notifications;
        this.rolePermissions = rolePermissions;
        this.emsPropertieses = emsPropertieses;
        this.activityLogins = activityLogins;
        this.customerAgreements = customerAgreements;
        this.permissions = permissions;
        this.serviceAgreements = serviceAgreements;
        this.customerRoles = customerRoles;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id")
    @StoryId
    @StoryAlias(value = ALIAS_ID)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @Version
    @Column(name = "VERSION", nullable = false)
    public int getVersion() {
        return this.version;
    }
    
    public void setVersion(final int version) {
        this.version = version;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UserAccountId", nullable = true)
    public UserAccount getUserAccount() {
        return this.userAccount;
    }
    
    public void setUserAccount(final UserAccount userAccount) {
        this.userAccount = userAccount;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerId", nullable = false)
    @StoryAlias(ALIAS_USER_ACCOUNT_CUSTOMER)
    @StoryLookup(type = Customer.ALIAS, searchAttribute = Customer.ALIAS_NAME, storage = StorageType.DB)
    public Customer getCustomer() {
        return this.customer;
    }
    
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UserStatusTypeId", nullable = false)
    @StoryAlias(ALIAS_USER_STATUS_TYPE)
    @StoryLookup(type = UserStatusType.ALIAS, searchAttribute = UserStatusType.ALIAS_NAME)
    public UserStatusType getUserStatusType() {
        return this.userStatusType;
    }
    
    public void setUserStatusType(final UserStatusType userStatusType) {
        this.userStatusType = userStatusType;
    }
    
    @StoryAlias(value = ALIAS_USER_NAME)
    @Column(name = "UserName", unique = true, nullable = false)
    public String getUserName() {
        return this.userName;
    }
    
    public void setUserName(final String userName) {
        this.userName = userName;
    }
    
    @Column(name = "LinkUserName", unique = true, nullable = true)
    public String getLinkUserName() {
        return this.linkUserName;
    }
    
    public void setLinkUserName(final String linkUserName) {
        this.linkUserName = linkUserName;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerEmailId")
    public CustomerEmail getCustomerEmail() {
        return this.customerEmail;
    }
    
    public void setCustomerEmail(final CustomerEmail customerEmail) {
        this.customerEmail = customerEmail;
    }
    
    @Column(name = "FirstName", nullable = false, length = HibernateConstants.VARCHAR_LENGTH_75)
    public String getFirstName() {
        return this.firstName;
    }
    
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }
    
    @Column(name = "LastName", nullable = false, length = HibernateConstants.VARCHAR_LENGTH_75)
    public String getLastName() {
        return this.lastName;
    }
    
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }
    
    @Column(name = "Password", nullable = false, length = HibernateConstants.VARCHAR_LENGTH_128)
    @StoryAlias (ALIAS_PASSWORD)
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(final String password) {
        this.password = password;
    }
    
    @Column(name = "PasswordSalt", nullable = false, length = HibernateConstants.VARCHAR_LENGTH_16)
    public String getPasswordSalt() {
        return this.passwordSalt;
    }
    
    public void setPasswordSalt(final String passwordSalt) {
        this.passwordSalt = passwordSalt;
    }
    
    @Column(name = "IsPasswordTemporary", nullable = false)
    public boolean isIsPasswordTemporary() {
        return this.isPasswordTemporary;
    }
    
    public void setIsPasswordTemporary(final boolean isPasswordTemporary) {
        this.isPasswordTemporary = isPasswordTemporary;
    }
    
    @Column(name = "IsAliasUser", nullable = false)
    public boolean isIsAliasUser() {
        return this.isAliasUser;
    }
    
    public void setIsAliasUser(final boolean isAliasUser) {
        this.isAliasUser = isAliasUser;
    }
    
    @Column(name = "IsAllChilds", nullable = false)
    public boolean isIsAllChilds() {
        return this.isAllChilds;
    }
    
    public void setIsAllChilds(final boolean isAllChilds) {
        this.isAllChilds = isAllChilds;
    }
    
    @Column(name = "IsDefaultAlias", nullable = false)
    public boolean getIsDefaultAlias() {
        return this.isDefaultAlias;
    }
    
    public void setIsDefaultAlias(final boolean isDefaultAlias) {
        this.isDefaultAlias = isDefaultAlias;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ArchiveDate", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getArchiveDate() {
        return this.archiveDate;
    }
    
    public void setArchiveDate(final Date archiveDate) {
        this.archiveDate = archiveDate;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }
    
    public void setLastModifiedGmt(final Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }
    
    public void setLastModifiedByUserId(final int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userAccount")
    public Set<UserRole> getUserRoles() {
        return this.userRoles;
    }
    
    public void setUserRoles(final Set<UserRole> userRoles) {
        this.userRoles = userRoles;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userAccount")
    @BatchSize(size = 16)
    public Set<Role> getRoles() {
        return this.roles;
    }
    
    public void setRoles(final Set<Role> roles) {
        this.roles = roles;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userAccount")
    @BatchSize(size = 8)
    public Set<Notification> getNotifications() {
        return this.notifications;
    }
    
    public void setNotifications(final Set<Notification> notifications) {
        this.notifications = notifications;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userAccount")
    @BatchSize(size = 32)
    public Set<RolePermission> getRolePermissions() {
        return this.rolePermissions;
    }
    
    public void setRolePermissions(final Set<RolePermission> rolePermissions) {
        this.rolePermissions = rolePermissions;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userAccount")
    public Set<EmsProperties> getEmsPropertieses() {
        return this.emsPropertieses;
    }
    
    public void setEmsPropertieses(final Set<EmsProperties> emsPropertieses) {
        this.emsPropertieses = emsPropertieses;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userAccount")
    public Set<ActivityLogin> getActivityLogins() {
        return this.activityLogins;
    }
    
    public void setActivityLogins(final Set<ActivityLogin> activityLogins) {
        this.activityLogins = activityLogins;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userAccount")
    public Set<CustomerAgreement> getCustomerAgreements() {
        return this.customerAgreements;
    }
    
    public void setCustomerAgreements(final Set<CustomerAgreement> customerAgreements) {
        this.customerAgreements = customerAgreements;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userAccount")
    @StoryAlias(ALIAS_PERMISSION)
    @StoryLookup(type = RolePermission.ALIAS, searchAttribute = RolePermission.AlIAS_PERMISSIONID, returnAttribute = RolePermission.AlIAS_PERMISSIONID)
    public Set<Permission> getPermissions() {
        return this.permissions;
    }
    
    public void setPermissions(final Set<Permission> permissions) {
        this.permissions = permissions;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userAccount")
    public Set<ServiceAgreement> getServiceAgreements() {
        return this.serviceAgreements;
    }
    
    public void setServiceAgreements(final Set<ServiceAgreement> serviceAgreements) {
        this.serviceAgreements = serviceAgreements;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userAccount")
    public Set<CustomerRole> getCustomerRoles() {
        return this.customerRoles;
    }
    
    public void setCustomerRoles(final Set<CustomerRole> customerRoles) {
        this.customerRoles = customerRoles;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userAccount")
    public Set<UserDefaultDashboard> getUserDefaultDashboards() {
        return this.userDefaultDashboards;
    }
    
    public void setUserDefaultDashboards(final Set<UserDefaultDashboard> userDefaultDashboards) {
        this.userDefaultDashboards = userDefaultDashboards;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userAccount")
    public Set<MobileSessionToken> getMobileSessionTokens() {
        return this.mobileSessionTokens;
    }
    
    public void setMobileSessionTokens(final Set<MobileSessionToken> mobileSessionTokens) {
        this.mobileSessionTokens = mobileSessionTokens;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userAccount")
    public Set<UserAccountRoute> getUserAccountRoutes() {
        return this.userAccountRoutes;
    }
    
    public void setUserAccountRoutes(final Set<UserAccountRoute> userAccountRoutes) {
        this.userAccountRoutes = userAccountRoutes;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userAccount")
    public Set<MobileAppHistory> getMobileAppHistories() {
        return this.mobileAppHistories;
    }
    
    public void setMobileAppHistories(final Set<MobileAppHistory> mobileAppHistories) {
        this.mobileAppHistories = mobileAppHistories;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userAccount")
    public Set<UserAccount> getChildUserAccounts() {
        return this.childUserAccounts;
    }
    
    public void setChildUserAccounts(final Set<UserAccount> childUserAccounts) {
        this.childUserAccounts = childUserAccounts;
    }
    
    @Transient
    public UserDefaultDashboard getUserDefaultDashboard() {
        final Iterator<UserDefaultDashboard> iter = getUserDefaultDashboards().iterator();
        if (!iter.hasNext()) {
            throw new RuntimeException("UserAccount & UserDefaultDashboard should be 1 to 1 mapping and UserDefaultDashboard should NOT be null.");
        }
        
        return iter.next();
    }
    
    @Transient
    public void setUserDefaultDashboard(final UserDefaultDashboard userDefaultDashboard) {
        final Set<UserDefaultDashboard> set = new HashSet<UserDefaultDashboard>(0);
        set.add(userDefaultDashboard);
        setUserDefaultDashboards(set);
    }
    
    @Transient
    public boolean getIsDefaultDashboard() {
        return getUserDefaultDashboard().getIsDefaultDashboard();
    }
    
    @Transient
    public void setIsDefaultDashboard(final boolean isDefaultDashboard) {
        getUserDefaultDashboard().setIsDefaultDashboard(isDefaultDashboard);
    }
    
    @Formula(value = "CONCAT(FirstName,' ',LastName)")
    public String getFullName() {
        return this.fullName;
    }
    
    public void setFullName(final String fullName) {
        this.fullName = fullName;
    }
    
    @Transient
    public Integer getRealUserId() {
        if (this.userAccount == null) {
            return this.id;
        } else {
            return this.userAccount.getId();
        }
    }
    
    @Transient
    public String getRealFirstName() {
        if (this.userAccount == null) {
            return this.firstName;
        } else {
            return this.userAccount.getFirstName();
        }
    }
    
    @Transient
    public String getRealLastName() {
        if (this.userAccount == null) {
            return this.lastName;
        } else {
            return this.userAccount.getLastName();
        }
    }
    
    @Transient
    public String getRealUserName() {
        if (this.userAccount == null) {
            return this.userName;
        } else {
            return this.userAccount.getUserName();
        }
    }
}
