package com.digitalpaytech.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.domain.util.HibernateConstants;

@Entity
@Table(name = "MerchantAccountMigration")
@NamedQueries({
    @NamedQuery(name = "MerchantAccountMigration.findDelayUntilGMTPastAndPending", query = "SELECT mam FROM MerchantAccountMigration mam LEFT JOIN FETCH mam.merchantAccount WHERE mam.delayUntilGMT <= :currentTime AND mam.merchantAccountMigrationStatus.id = :pendingStatusId"),
    @NamedQuery(name = "MerchantAccountMigration.findById", query = "FROM MerchantAccountMigration mam WHERE mam.id = :id"),
    @NamedQuery(name = "MerchantAccountMigration.findByCustomerId", query = "FROM MerchantAccountMigration mam WHERE mam.merchantAccount.customer.id = :customerId"),
    @NamedQuery(name = "MerchantAccountMigration.findByMerchantAccountId", query = "FROM MerchantAccountMigration mam WHERE mam.merchantAccount.id = :merchantAccountId") })
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MerchantAccountMigration implements Serializable {
    private static final long serialVersionUID = 311319453114569298L;
    private Integer id;
    private MerchantAccount merchantAccount;
    private MerchantAccountMigrationStatus merchantAccountMigrationStatus;
    private byte[] failureCause;
    private Date createdGMT;
    private Date delayUntilGMT;
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @OneToOne
    @JoinColumn(name = "MerchantAccountId", nullable = false)
    public MerchantAccount getMerchantAccount() {
        return this.merchantAccount;
    }
    
    public void setMerchantAccount(final MerchantAccount merchantAccount) {
        this.merchantAccount = merchantAccount;
    }
    
    @ManyToOne
    @JoinColumn(name = "MerchantAccountMigrationStatusId", nullable = false)
    public MerchantAccountMigrationStatus getMerchantAccountMigrationStatus() {
        return this.merchantAccountMigrationStatus;
    }
    
    public void setMerchantAccountMigrationStatus(final MerchantAccountMigrationStatus merchantAccountMigrationStatus) {
        this.merchantAccountMigrationStatus = merchantAccountMigrationStatus;
    }
    
    @Lob
    @Column(name = "FailureCause")
    public byte[] getFailureCause() {
        return failureCause;
    }
    
    public void setFailureCause(final byte[] failureCause) {
        this.failureCause = failureCause;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getCreatedGMT() {
        return this.createdGMT;
    }
    
    public void setCreatedGMT(final Date createdGMT) {
        this.createdGMT = createdGMT;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DelayUntilGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getDelayUntilGMT() {
        return this.delayUntilGMT;
    }
    
    public void setDelayUntilGMT(final Date delayUntilGMT) {
        this.delayUntilGMT = delayUntilGMT;
    }
}
