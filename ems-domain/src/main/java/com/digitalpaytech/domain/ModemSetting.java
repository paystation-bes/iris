package com.digitalpaytech.domain;

// Generated 25-Sep-2012 9:57:42 AM by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.domain.util.HibernateConstants;

/**
 * ModemSetting generated by hbm2java
 */
@Entity
@Table(name = "ModemSetting")
@NamedQueries({
    @NamedQuery(name = "ModemSetting.findModemSettingByPointOfSaleId", query = "from ModemSetting ms where ms.pointOfSale.id = :pointOfSaleId") })
//Checkstyle: Hibernate proxies have issues when methods are defined final
@SuppressWarnings({ "checkstyle:designforextension" })
public class ModemSetting implements java.io.Serializable {
    /**
     * 
     */
    
    private static final int MEIDSIZE = 18;
    private static final long serialVersionUID = 2119792811546110527L;
    
    private Integer id;
    private PointOfSale pointOfSale;
    private AccessPoint accessPoint;
    private ModemType modemType;
    private Carrier carrier;
    private String ccid;
    private String meid;
    private String hardwareManufacturer;
    private String hardwareModel;
    private String hardwareFirmware;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    private int version;
    
    public ModemSetting() {
        super();
    }
    
    public ModemSetting(final PointOfSale pointOfSale, final Date lastModifiedGmt, final int lastModifiedByUserId) {
        this.pointOfSale = pointOfSale;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    public ModemSetting(final PointOfSale pointOfSale, final AccessPoint accessPoint, final ModemType modemType, final Carrier carrier,
            final String ccid, final String hardwareManufacturer, final String hardwareModel, final String hardwareFirmware,
            final Date lastModifiedGmt, final int lastModifiedByUserId) {
        
        this.pointOfSale = pointOfSale;
        this.accessPoint = accessPoint;
        this.modemType = modemType;
        this.carrier = carrier;
        this.ccid = ccid;
        this.hardwareManufacturer = hardwareManufacturer;
        this.hardwareModel = hardwareModel;
        this.hardwareFirmware = hardwareFirmware;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
        
        if (ccid != null && ccid.length() == MEIDSIZE) {
            final Long first10digits = Long.valueOf(ccid.substring(0, 10));
            final Long last8digits = Long.valueOf(ccid.substring(10));
            this.meid = Long.toHexString(first10digits).toUpperCase() + Long.toHexString(last8digits).toUpperCase();
        }
        
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PointOfSaleId", nullable = false)
    public PointOfSale getPointOfSale() {
        return this.pointOfSale;
    }
    
    //Point of sale
    public void setPointOfSale(final PointOfSale pointOfSale) {
        this.pointOfSale = pointOfSale;
    }
    
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "AccessPointId")
    public AccessPoint getAccessPoint() {
        return this.accessPoint;
    }
    
    public void setAccessPoint(final AccessPoint accessPoint) {
        this.accessPoint = accessPoint;
    }
    
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "ModemTypeId")
    public ModemType getModemType() {
        return this.modemType;
    }
    
    public void setModemType(final ModemType modemType) {
        this.modemType = modemType;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CarrierId")
    public Carrier getCarrier() {
        return this.carrier;
    }
    
    public void setCarrier(final Carrier carrier) {
        this.carrier = carrier;
    }
    
    @Column(name = "CCID", length = HibernateConstants.VARCHAR_LENGTH_40)
    public String getCcid() {
        return this.ccid;
    }
    
    public void setCcid(final String ccid) {
        this.ccid = ccid;
    }
    
    @Column(name = "MEID", length = HibernateConstants.VARCHAR_LENGTH_14)
    public String getMeid() {
        return this.meid;
    }
    
    public void setMeid(final String meid) {
        this.meid = meid;
    }
    
    @Column(name = "HardwareManufacturer", length = HibernateConstants.VARCHAR_LENGTH_50)
    public String getHardwareManufacturer() {
        return this.hardwareManufacturer;
    }
    
    public void setHardwareManufacturer(final String hardwareManufacturer) {
        this.hardwareManufacturer = hardwareManufacturer;
    }
    
    @Column(name = "HardwareModel", length = HibernateConstants.VARCHAR_LENGTH_50)
    public String getHardwareModel() {
        return this.hardwareModel;
    }
    
    public void setHardwareModel(final String hardwareModel) {
        this.hardwareModel = hardwareModel;
    }
    
    @Column(name = "HardwareFirmware", length = HibernateConstants.VARCHAR_LENGTH_50)
    public String getHardwareFirmware() {
        return this.hardwareFirmware;
    }
    
    public void setHardwareFirmware(final String hardwareFirmware) {
        this.hardwareFirmware = hardwareFirmware;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }
    
    public void setLastModifiedGmt(final Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }
    
    public void setLastModifiedByUserId(final int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @Version
    @Column(name = "VERSION", nullable = false)
    public int getVersion() {
        return this.version;
    }
    
    public void setVersion(final int version) {
        this.version = version;
    }
}
