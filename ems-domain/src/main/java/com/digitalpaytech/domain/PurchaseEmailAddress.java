package com.digitalpaytech.domain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@Entity
@Table(name = "PurchaseEmailAddress")
@NamedQueries({
        @NamedQuery(name = "PurchaseEmailAddress.findPurchaseEmailAddress", query = "FROM PurchaseEmailAddress pe WHERE pe.isEmailSent != 1"),
        @NamedQuery(name = "PurchaseEmailAddress.findPurchaseEmailAddressByPurchaseId", query = "FROM PurchaseEmailAddress pea JOIN FETCH pea.emailAddress ea WHERE pea.purchase.id = :purchaseId ORDER BY pea.sentEmailGMT desc") })
public class PurchaseEmailAddress implements java.io.Serializable {
    
    private static final long serialVersionUID = -716176675712707405L;
    private Long id;
    private Purchase purchase;
    private EmailAddress emailAddress;
    private Date sentEmailGMT;
    private boolean isEmailSent;
    
    public PurchaseEmailAddress() {
    }
    
    public PurchaseEmailAddress(Purchase purchase, EmailAddress emailAddress, Date sentEmailGMT, boolean isEmailSent) {
        this.purchase = purchase;
        this.emailAddress = emailAddress;
        this.sentEmailGMT = sentEmailGMT;
        this.isEmailSent = isEmailSent;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    @ManyToOne
    @JoinColumn(name = "PurchaseId", nullable = false)
    public Purchase getPurchase() {
        return this.purchase;
    }
    
    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }
    
    @ManyToOne
    @JoinColumn(name = "EmailAddressId", nullable = false)
    public EmailAddress getEmailAddress() {
        return this.emailAddress;
    }
    
    public void setEmailAddress(EmailAddress emailAddress) {
        this.emailAddress = emailAddress;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "SentEmailGMT", length = 19)
    public Date getSentEmailGMT() {
        return this.sentEmailGMT;
    }
    
    public void setSentEmailGMT(Date sentEmailGMT) {
        this.sentEmailGMT = sentEmailGMT;
    }
    
    @Column(name = "IsEmailSent", nullable = false)
    public boolean isIsEmailSent() {
        return this.isEmailSent;
    }
    
    public void setIsEmailSent(boolean isEmailSent) {
        this.isEmailSent = isEmailSent;
    }
}
