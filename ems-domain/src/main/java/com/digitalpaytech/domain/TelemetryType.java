package com.digitalpaytech.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryId;
import com.digitalpaytech.annotation.StoryTransformer;

@Entity
@Table(name = "TelemetryType")
@NamedQueries({
    @NamedQuery(name = "TelemetryType.findTelemetryTypeByName", query = "SELECT teltype from TelemetryType teltype WHERE teltype.name = :telemetryTypeName"),
    @NamedQuery(name = "TelemetryType.findTelemetryTypeByLabel", query = "SELECT teltype from TelemetryType teltype WHERE teltype.label = :telemetryTypeLabel"),
    @NamedQuery(name = "TelemetryType.findAllTelemetryTypes", query = "SELECT teltype from TelemetryType teltype", cacheable = true) })
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings({ "checkstyle:designforextension" })
@StoryAlias(TelemetryType.ALIAS)
public class TelemetryType implements Serializable {
    public static final String ALIAS = "Telemetry Type";
    public static final String ALIAS_ID = "Id";
    public static final String ALIAS_NAME = "Name";
    public static final String ALIAS_LABEL = "Label";
    public static final String ALIAS_PRIVATE = "Private";
    public static final String ALIAS_CREATED_GMT = "Created Time in GMT";
    
    /**
     * 
     */
    private static final long serialVersionUID = 4657835056534688308L;
    private Integer id;
    private String name;
    private String label;
    private Boolean isPrivate;
    private Date createdGMT;
    
    public TelemetryType() {
        
    }
    
    @Id
    @Column(name = "Id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @StoryAlias(ALIAS_ID)
    @StoryId
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @Column(name = "Name", nullable = false)
    @StoryAlias(ALIAS_NAME)
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    @Column(name = "Label")
    @StoryAlias(ALIAS_LABEL)
    public String getLabel() {
        return this.label;
    }
    
    public void setLabel(final String label) {
        this.label = label;
    }
    
    @Column(name = "IsPrivate", nullable = false)
    @StoryAlias(ALIAS_PRIVATE)
    public Boolean getIsPrivate() {
        return this.isPrivate;
    }
    
    public void setIsPrivate(final Boolean isPrivate) {
        this.isPrivate = isPrivate;
    }
    
    @Column(name = "CreatedGMT", nullable = false)
    @StoryAlias(value = ALIAS_CREATED_GMT, defaultData = "now")
    @StoryTransformer("dateString")
    public Date getCreatedGMT() {
        return this.createdGMT;
    }
    
    public void setCreatedGMT(final Date createdGMT) {
        this.createdGMT = createdGMT;
    }
    
}
