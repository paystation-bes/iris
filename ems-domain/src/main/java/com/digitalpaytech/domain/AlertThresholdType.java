package com.digitalpaytech.domain;

// Generated 12-Apr-2013 11:12:02 AM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.domain.util.HibernateConstants;

/**
 * AlertthresholdType generated by hbm2java
 */
@Entity
@Table(name = "AlertThresholdType")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({
    @NamedQuery(name = "AlertThresholdType.findByIds", query = "SELECT att FROM AlertThresholdType att WHERE att.id IN(:alertThresholdTypeIds) ORDER BY att.name", cacheable = true),
    @NamedQuery(name = "AlertThresholdType.findByAlertClassTypeId", query = "FROM AlertThresholdType att WHERE att.alertType.alertClassType.id = :alertClassTypeId", cacheable = true) })
@SuppressWarnings({ "checkstyle:designforextension" })
@StoryAlias(AlertThresholdType.ALIAS)
public class AlertThresholdType implements java.io.Serializable {
    
    public static final String ALIAS_NAME = "name";
    public static final String ALIAS = "threshold type";
    private static final long serialVersionUID = 3044011337077686649L;
    private int id;
    private AlertType alertType;
    private String name;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    
    public AlertThresholdType() {
        super();
    }
    
    public AlertThresholdType(final int id) {
        this.id = id;
    }
    
    public AlertThresholdType(final int id, final AlertType alertType, final String name, final Date lastModifiedGmt, final int lastModifiedByUserId) {
        this.id = id;
        this.alertType = alertType;
        this.name = name;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @Id
    @Column(name = "Id", nullable = false)
    public int getId() {
        return this.id;
    }
    
    public void setId(final int id) {
        this.id = id;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AlertTypeId", nullable = false)
    public AlertType getAlertType() {
        return this.alertType;
    }
    
    public void setAlertType(final AlertType alertType) {
        this.alertType = alertType;
    }
    
    @Column(name = "Name", unique = true, nullable = false, length = HibernateConstants.VARCHAR_LENGTH_50)
    @StoryAlias(ALIAS_NAME)
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }
    
    public void setLastModifiedGmt(final Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }
    
    public void setLastModifiedByUserId(final int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
}
