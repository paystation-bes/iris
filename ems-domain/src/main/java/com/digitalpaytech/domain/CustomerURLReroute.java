package com.digitalpaytech.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@Entity
@Table(name = "CustomerURLReroute")
@NamedQueries({
    @NamedQuery(name = "CustomerURLReroute.findCustomerURLRerouteByCustomerId", query = "from CustomerURLReroute cur join fetch cur.urlReroute where cur.customer.id = :customerId", cacheable = true)
})

@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CustomerURLReroute implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 4794701345579936320L;
    private int id;
    private Customer customer;
    private URLReroute urlReroute;
    
    @Id
    @Column(name = "Id", nullable = false)   
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerId", nullable = false)  
    public Customer getCustomer() {
        return customer;
    }
    
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "URLRerouteId", nullable = false)    
    public URLReroute getUrlReroute() {
        return urlReroute;
    }
    
    public void setUrlReroute(URLReroute urlReroute) {
        this.urlReroute = urlReroute;
    }
}