package com.digitalpaytech.domain;

// Generated 16-Oct-2012 11:35:05 AM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Sensorinfotype generated by hbm2java
 */
@Entity
@Table(name = "SensorInfoType")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SensorInfoType implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 3020521051482527265L;
    private int id;
    private String name;
    private float minValue;
    private float maxValue;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    private Set<PosSensorInfo> posSensorInfos = new HashSet<PosSensorInfo>(0);

    public SensorInfoType() {
    }

    public SensorInfoType(int id, String name, Date lastModifiedGmt,
            int lastModifiedByUserId) {
        this.id = id;
        this.name = name;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }

    public SensorInfoType(int id, String name, Date lastModifiedGmt,
            int lastModifiedByUserId, Set<PosSensorInfo> posSensorInfos) {
        this.id = id;
        this.name = name;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
        this.posSensorInfos = posSensorInfos;
    }

    @Id
    @Column(name = "Id", nullable = false)
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "Name", unique = true, nullable = false, length = 30)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name="MinValue", nullable = false, precision=12, scale=0)
    public float getMinValue() {
        return this.minValue;
    }
    
    public void setMinValue(float minValue) {
        this.minValue = minValue;
    }

    
    @Column(name="MaxValue", nullable = false, precision=12, scale=0)
    public float getMaxValue() {
        return this.maxValue;
    }
    
    public void setMaxValue(float maxValue) {
        this.maxValue = maxValue;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = 19)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }

    public void setLastModifiedGmt(Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }

    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }

    public void setLastModifiedByUserId(int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "sensorInfoType")
    public Set<PosSensorInfo> getPosSensorInfos() {
        return this.posSensorInfos;
    }

    public void setPosSensorInfos(Set<PosSensorInfo> posSensorInfos) {
        this.posSensorInfos = posSensorInfos;
    }
}
