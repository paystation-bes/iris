package com.digitalpaytech.domain;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.domain.util.HibernateConstants;


@Entity
@Table(name = "EMVTransactionDataMapping")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@NamedQueries({
    @NamedQuery(name = "EMVTransactionDataMapping.findEMVTransactionDataMappingByProcessorId", query = "from EMVTransactionDataMapping etdm where etdm.processor.id = :processorId", cacheable = true)
})
public class EMVTransactionDataMapping implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 7365776434968794006L;
    private Integer id;    
    private Processor processor;    
    private String label1;
    private String label2;
    private String label3;
    private String label4;
    private String label5;
    private String label6;
    private String label7;    
    private Date createdGmt;
    
    public EMVTransactionDataMapping() {        
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }    
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ProcessorId", nullable = false)
    public Processor getProcessor() {
        return this.processor;
    }
    
    public void setProcessor(final Processor processor) {
        this.processor = processor;
    }    

    @Column(name = "Label1", length = HibernateConstants.VARCHAR_LENGTH_50)
    public String getLabel1() {
        return label1;
    }
    
    public void setLabel1(String label1) {
        this.label1 = label1;
    }
    
    @Column(name = "Label2", length = HibernateConstants.VARCHAR_LENGTH_50)
    public String getLabel2() {
        return label2;
    }
    
    public void setLabel2(String label2) {
        this.label2 = label2;
    }
    
    @Column(name = "Label3", length = HibernateConstants.VARCHAR_LENGTH_50)
    public String getLabel3() {
        return label3;
    }
    
    public void setLabel3(String label3) {
        this.label3 = label3;
    }
    
    @Column(name = "Label4", length = HibernateConstants.VARCHAR_LENGTH_50)
    public String getLabel4() {
        return label4;
    }
    
    public void setLabel4(String label4) {
        this.label4 = label4;
    }
    
    @Column(name = "Label5", length = HibernateConstants.VARCHAR_LENGTH_50)
    public String getLabel5() {
        return label5;
    }
    
    public void setLabel5(String label5) {
        this.label5 = label5;
    }
    
    @Column(name = "Label6", length = HibernateConstants.VARCHAR_LENGTH_50)
    public String getLabel6() {
        return label6;
    }
    
    public void setLabel6(String label6) {
        this.label6 = label6;
    }
    
    @Column(name = "Label7", length = HibernateConstants.VARCHAR_LENGTH_50)
    public String getLabel7() {
        return label7;
    }
    
    public void setLabel7(String label7) {
        this.label7 = label7;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getCreatedGmt() {
        return this.createdGmt;
    }
    
    public void setCreatedGmt(final Date createdGmt) {
        this.createdGmt = createdGmt;
    }    
}
