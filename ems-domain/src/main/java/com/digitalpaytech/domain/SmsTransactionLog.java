package com.digitalpaytech.domain;

// Generated 4-Sep-2012 4:25:12 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * SmsTransactionLog generated by hbm2java
 */
@Entity
@Table(name = "SmsTransactionLog")
public class SmsTransactionLog implements java.io.Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5885621214766034067L;
    private Integer id;
    private MobileNumber mobileNumber;
    private PointOfSale pointOfSale;
    private SmsMessageType smsMessageType;
    private Date purchaseGmt;
    private int ticketNumber;
    private Date expiryDateGmt;
    private String consumerResponse;
    private Date timeStampGmt;

    public SmsTransactionLog() {
    }

    public SmsTransactionLog(PointOfSale pointOfSale,
            SmsMessageType smsMessageType, Date purchaseGmt, int ticketNumber,
            Date expiryDateGmt, Date timeStampGmt) {
        this.pointOfSale = pointOfSale;
        this.smsMessageType = smsMessageType;
        this.purchaseGmt = purchaseGmt;
        this.ticketNumber = ticketNumber;
        this.expiryDateGmt = expiryDateGmt;
        this.timeStampGmt = timeStampGmt;
    }

    public SmsTransactionLog(MobileNumber mobileNumber,
            PointOfSale pointOfSale, SmsMessageType smsMessageType,
            Date purchaseGmt, int ticketNumber, Date expiryDateGmt,
            String consumerResponse, Date timeStampGmt) {
        this.mobileNumber = mobileNumber;
        this.pointOfSale = pointOfSale;
        this.smsMessageType = smsMessageType;
        this.purchaseGmt = purchaseGmt;
        this.ticketNumber = ticketNumber;
        this.expiryDateGmt = expiryDateGmt;
        this.consumerResponse = consumerResponse;
        this.timeStampGmt = timeStampGmt;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MobileNumberId")
    public MobileNumber getMobileNumber() {
        return this.mobileNumber;
    }

    public void setMobileNumber(MobileNumber mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PointOfSaleId", nullable = false)
    public PointOfSale getPointOfSale() {
        return this.pointOfSale;
    }

    public void setPointOfSale(PointOfSale pointOfSale) {
        this.pointOfSale = pointOfSale;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SMSMessageTypeId", nullable = false)
    public SmsMessageType getSmsMessageType() {
        return this.smsMessageType;
    }

    public void setSmsMessageType(SmsMessageType smsMessageType) {
        this.smsMessageType = smsMessageType;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "PurchaseGMT", nullable = false, length = 19)
    public Date getPurchaseGmt() {
        return this.purchaseGmt;
    }

    public void setPurchaseGmt(Date purchaseGmt) {
        this.purchaseGmt = purchaseGmt;
    }

    @Column(name = "TicketNumber", nullable = false)
    public int getTicketNumber() {
        return this.ticketNumber;
    }

    public void setTicketNumber(int ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ExpiryDateGMT", nullable = false, length = 19)
    public Date getExpiryDateGmt() {
        return this.expiryDateGmt;
    }

    public void setExpiryDateGmt(Date expiryDateGmt) {
        this.expiryDateGmt = expiryDateGmt;
    }

    @Column(name = "ConsumerResponse", length = 30)
    public String getConsumerResponse() {
        return this.consumerResponse;
    }

    public void setConsumerResponse(String consumerResponse) {
        this.consumerResponse = consumerResponse;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TimeStampGMT", nullable = false, length = 19)
    public Date getTimeStampGmt() {
        return this.timeStampGmt;
    }

    public void setTimeStampGmt(Date timeStampGmt) {
        this.timeStampGmt = timeStampGmt;
    }

}
