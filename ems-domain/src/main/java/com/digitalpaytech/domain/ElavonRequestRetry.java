package com.digitalpaytech.domain;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the ElavonRequestRetry database table.
 * 
 */
@Entity
@Table(name="ElavonRequestRetry")
public class ElavonRequestRetry implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 6846788834794391458L;
    private String id;
    private String cardData;
    private Date createdGMT;
    private boolean isRetrySuccessful;
    private Date lastRetryGMT;
    private MerchantAccount merchantAccount;
    private int noOfRetry;
    private int originalAuthAmount;
    private PointOfSale pointOfSale;
    private PreAuth preAuth;
    private Date purchasedDate;
    private int ticketNumber;
    private int transactionAmount;

    public ElavonRequestRetry() {
    }


    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false)
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }


    @Column(nullable = false, length=100)
    public String getCardData() {
        return this.cardData;
    }

    public void setCardData(String cardData) {
        this.cardData = cardData;
    }


    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    public Date getCreatedGMT() {
        return this.createdGMT;
    }

    public void setCreatedGMT(Date createdGMT) {
        this.createdGMT = createdGMT;
    }


    @Column(nullable = false)
    public boolean getIsRetrySuccessful() {
        return this.isRetrySuccessful;
    }

    public void setIsRetrySuccessful(boolean isRetrySuccessful) {
        this.isRetrySuccessful = isRetrySuccessful;
    }


    @Temporal(TemporalType.TIMESTAMP)
    public Date getLastRetryGMT() {
        return this.lastRetryGMT;
    }

    public void setLastRetryGMT(Date lastRetryGMT) {
        this.lastRetryGMT = lastRetryGMT;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MerchantAccountId", nullable = false)
    public MerchantAccount getMerchantAccount() {
        return this.merchantAccount;
    }

    public void setMerchantAccount(MerchantAccount merchantAccount) {
        this.merchantAccount = merchantAccount;
    }


    @Column(nullable = false)
    public int getNoOfRetry() {
        return this.noOfRetry;
    }

    public void setNoOfRetry(int noOfRetry) {
        this.noOfRetry = noOfRetry;
    }


    @Column(nullable = false)
    public int getOriginalAuthAmount() {
        return this.originalAuthAmount;
    }

    public void setOriginalAuthAmount(int originalAuthAmount) {
        this.originalAuthAmount = originalAuthAmount;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PointOfSaleId")
    public PointOfSale getPointOfSale() {
        return this.pointOfSale;
    }

    public void setPointOfSale(PointOfSale pointOfSale) {
        this.pointOfSale = pointOfSale;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PreAuthId")
    public PreAuth getPreAuth() {
        return this.preAuth;
    }
    
    public void setPreAuth(PreAuth preAuth) {
        this.preAuth = preAuth;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    public Date getPurchasedDate() {
        return this.purchasedDate;
    }

    public void setPurchasedDate(Date purchasedDate) {
        this.purchasedDate = purchasedDate;
    }


    @Column(nullable = false)
    public int getTicketNumber() {
        return this.ticketNumber;
    }

    public void setTicketNumber(int ticketNumber) {
        this.ticketNumber = ticketNumber;
    }


    @Column(nullable = false)
    public int getTransactionAmount() {
        return this.transactionAmount;
    }

    public void setTransactionAmount(int transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

}