package com.digitalpaytech.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@Entity
@Table(name = "MobileLicenseStatusType")
@NamedQueries({
    @NamedQuery(name = "MobileLicenseStatusType.findMobileLicenseStatusTypeById", query = "SELECT mlst FROM MobileLicenseStatusType mlst WHERE mlst.id =:id", cacheable=true),
    @NamedQuery(name = "MobileLicenseStatusType.findMobileLicenseStatusTypeByName", query = "SELECT mlst FROM MobileLicenseStatusType mlst WHERE mlst.name =:name", cacheable=true)
})
public class MobileLicenseStatusType implements java.io.Serializable {

    private static final long serialVersionUID = -4118342804878458211L;
    private int id;
    private String name;
    
    @Id
    @Column(name = "Id", nullable = false)
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    @Column(name = "name", length = 80)
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    

}
