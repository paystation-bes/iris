package com.digitalpaytech.domain;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the TransactionSettlementStatusType database table.
 * 
 */
@Entity
@Table(name="TransactionSettlementStatusType")
@NamedQueries({
    @NamedQuery(name = "TransactionSettlementStatusType.findById", query = "FROM TransactionSettlementStatusType tsst WHERE tsst.id =:id", cacheable = true)
})
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class TransactionSettlementStatusType implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 675049204091195801L;
    private Integer id;
    private String description;
    private int lastModifiedByUserId;
    private Date lastModifiedGMT;
    private String name;
    private List<ElavonTransactionDetail> elavontransactiondetails;

    public TransactionSettlementStatusType() {
    }


    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    @Column(nullable = false, length=255)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Column(nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }

    public void setLastModifiedByUserId(int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }


    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    public Date getLastModifiedGMT() {
        return this.lastModifiedGMT;
    }

    public void setLastModifiedGMT(Date lastModifiedGMT) {
        this.lastModifiedGMT = lastModifiedGMT;
    }


    @Column(nullable = false, length=30)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }


    //bi-directional many-to-one association to Elavontransactiondetail
    @OneToMany(mappedBy="transactionSettlementStatusType")
    public List<ElavonTransactionDetail> getElavontransactiondetails() {
        return this.elavontransactiondetails;
    }

    public void setElavontransactiondetails(List<ElavonTransactionDetail> elavontransactiondetails) {
        this.elavontransactiondetails = elavontransactiondetails;
    }

    public ElavonTransactionDetail addElavontransactiondetail(ElavonTransactionDetail elavontransactiondetail) {
        getElavontransactiondetails().add(elavontransactiondetail);
        elavontransactiondetail.setTransactionSettlementStatusType(this);

        return elavontransactiondetail;
    }

    public ElavonTransactionDetail removeElavontransactiondetail(ElavonTransactionDetail elavontransactiondetail) {
        getElavontransactiondetails().remove(elavontransactiondetail);
        elavontransactiondetail.setTransactionSettlementStatusType(null);

        return elavontransactiondetail;
    }

}