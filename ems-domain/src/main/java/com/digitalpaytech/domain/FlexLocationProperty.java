package com.digitalpaytech.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.domain.util.HibernateConstants;

@Entity
@Table(name = "FlexLocationProperty")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({
    @NamedQuery(name = "FlexLocationProperty.findFlexLocationPropertiesByCustomerId", query = "from FlexLocationProperty flp where flp.customer.id = :customerId", cacheable = true),
    @NamedQuery(name = "FlexLocationProperty.findActiveFlexLocationPropertyIdByCustomerId", query = "select flp.id from FlexLocationProperty flp where flp.customer.id = :customerId and flp.proIsActive = 1 and flp.location.id NOT IN(select flp2.location.id from FlexLocationProperty flp2 where flp2.customer.id = :customerId and flp2.location.id NOT IN(:locationIds))", cacheable = true),
    @NamedQuery(name = "FlexLocationProperty.findFlexLocationPropertyIdByLocationIds", query = "select flp.id from FlexLocationProperty flp where flp.location.id IN(:locationIds) and flp.proIsActive = 1", cacheable = true),
    @NamedQuery(name = "FlexLocationProperty.findFlexLocationPropertyByLocationIds", query = "from FlexLocationProperty flp where flp.location.id = :locationId and flp.proIsActive = 1", cacheable = true),
    @NamedQuery(name = "FlexLocationProperty.findActiveFlexLocationPropertyByCustomerId", query = "from FlexLocationProperty flp where flp.customer.id = :customerId and flp.proIsActive = 1 and flp.location.id NOT IN(select flp2.location.id  from FlexLocationProperty flp2 where flp2.customer.id = :customerId and flp2.location.id NOT IN(:locationIds))", cacheable = true),
    @NamedQuery(name = "FlexLocationProperty.findByCustomerIdAndFlexIds", query = "from FlexLocationProperty flp where flp.customer.id = :customerId and flp.proUid IN(:flexIds) ORDER BY flp.proUid", cacheable = true)
})
//Checkstyle: Hibernate proxies have issues when methods are defined final
@SuppressWarnings({ "checkstyle:designforextension" })
public class FlexLocationProperty implements java.io.Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -7066466897780056444L;
    
    private Integer id;
    private Customer customer;
    private Location location;
    private Integer proUid;
    private String proName;
    private String proCity;
    private boolean proIsActive;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    
    private String randomId;
    
    public FlexLocationProperty() {
        super();
    }
    
    public FlexLocationProperty(final Integer id, final int proUid, final String proName, final String proCity, final boolean proIsActive,
            final Location location, final Customer customer, final Date lastModifiedGmt, final int lastModifiedByUserId) {
        this.id = id;
        this.proUid = proUid;
        this.proName = proName;
        this.proCity = proCity;
        this.proIsActive = proIsActive;
        this.location = location;
        this.customer = customer;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerId", nullable = false)
    public Customer getCustomer() {
        return this.customer;
    }
    
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LocationId", nullable = true)
    public Location getLocation() {
        return this.location;
    }
    
    public void setLocation(final Location location) {
        this.location = location;
    }
    
    @Column(name = "PRO_UID", nullable = false)
    public int getProUid() {
        return this.proUid;
    }
    
    public void setProUid(final int proUid) {
        this.proUid = proUid;
    }
    
    @Column(name = "PRO_NAME", nullable = false, length = HibernateConstants.VARCHAR_LENGTH_128)
    public String getProName() {
        return this.proName;
    }
    
    public void setProName(final String proName) {
        this.proName = proName;
    }
    
    @Column(name = "PRO_CITY", nullable = false, length = HibernateConstants.VARCHAR_LENGTH_32)
    public String getProCity() {
        return this.proCity;
    }
    
    public void setProCity(final String proCity) {
        this.proCity = proCity;
    }
    
    @Column(name = "PRO_IS_ACTIVE", nullable = false)
    public boolean isProIsActive() {
        return this.proIsActive;
    }
    
    public void setProIsActive(final boolean proIsActive) {
        this.proIsActive = proIsActive;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }
    
    public void setLastModifiedGmt(final Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }
    
    public void setLastModifiedByUserId(final int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @Transient
    public String getRandomId() {
        return this.randomId;
    }
    
    @Transient
    public void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
}
