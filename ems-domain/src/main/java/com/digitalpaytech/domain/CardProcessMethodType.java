package com.digitalpaytech.domain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@Entity
@Table(name = "CardProcessMethodType")
@NamedQueries({
    @NamedQuery(name = "CardProcessMethodType.findSettledTypes", query = "FROM CardProcessMethodType cmt WHERE cmt.id IN (1, 2, 3)", cacheable = true) 
})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CardProcessMethodType implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -6364245350420498285L;
    private int id;
    private String name;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    private String randomId;
    
    public CardProcessMethodType() {
    }
    
    public CardProcessMethodType(int id, String name, Date lastModifiedGmt, int lastModifiedByUserId) {
        this.id = id;
        this.name = name;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @Id
    @Column(name = "Id", nullable = false)
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "Name", unique = true, nullable = false, length = 40)
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = 19)
    public Date getLastModifiedGmt() {
        return lastModifiedGmt;
    }

    public void setLastModifiedGmt(Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return lastModifiedByUserId;
    }
    
    public void setLastModifiedByUserId(int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }

    @Transient
    public String getRandomId() {
        return randomId;
    }

    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }   
}
