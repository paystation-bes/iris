package com.digitalpaytech.domain;

// Generated 6-Aug-2014 1:24:49 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 * Ratehourly generated by hbm2java
 */
@Entity
@Table(name = "RateHourly")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class RateHourly implements java.io.Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -5139542311594289506L;
    private int rateId;
    private int version;
    private Rate rate;
    private short maximumHours;
    private boolean isVariable;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    
    public RateHourly() {
    }
    
    public RateHourly(Rate rate, short maximumHours, boolean isVariable, Date lastModifiedGmt, int lastModifiedByUserId) {
        this.rate = rate;
        this.maximumHours = maximumHours;
        this.isVariable = isVariable;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "rate"))
    @Id
    @GeneratedValue(generator = "generator")
    @Column(name = "RateId", nullable = false)
    public int getRateId() {
        return this.rateId;
    }
    
    public void setRateId(int rateId) {
        this.rateId = rateId;
    }
    
    @Version
    @Column(name = "VERSION", nullable = false)
    public int getVersion() {
        return this.version;
    }
    
    public void setVersion(int version) {
        this.version = version;
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public Rate getRate() {
        return this.rate;
    }
    
    public void setRate(Rate rate) {
        this.rate = rate;
    }
    
    @Column(name = "MaximumHours", nullable = false)
    public short getMaximumHours() {
        return this.maximumHours;
    }
    
    public void setMaximumHours(short maximumHours) {
        this.maximumHours = maximumHours;
    }
    
    @Column(name = "IsVariable", nullable = false)
    public boolean isIsVariable() {
        return this.isVariable;
    }
    
    public void setIsVariable(boolean isVariable) {
        this.isVariable = isVariable;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = 19)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }
    
    public void setLastModifiedGmt(Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }
    
    public void setLastModifiedByUserId(int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
}
