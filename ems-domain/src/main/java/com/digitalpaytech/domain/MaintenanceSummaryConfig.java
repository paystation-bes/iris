package com.digitalpaytech.domain;

// Generated 9-Jul-2014 1:43:46 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

/**
 * MaintenanceSummaryConfig generated by hbm2java
 */
@Entity
@Table(name = "MaintenanceSummaryConfig")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({
    @NamedQuery(name = "MaintenanceSummaryConfig.findColumnShowByUserAccountId", 
                query = "SELECT msc FROM MaintenanceSummaryConfig msc WHERE msc.userAccount.id = :userAccountId", cacheable = true)
})
public class MaintenanceSummaryConfig implements java.io.Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -2641594850879034337L;
    private Integer id;
    private UserAccount userAccount;
    private boolean routeVisible;
    private boolean locationVisible;
    private boolean lastSeenVisible;
    private boolean batteryVoltageVisible;
    private boolean paperStatusVisible;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    
    public MaintenanceSummaryConfig() {
    }
    
    public MaintenanceSummaryConfig(UserAccount userAccount, boolean routeVisible, boolean locationVisible, boolean lastSeenVisible,
            boolean batteryVoltageVisible, boolean paperStatusVisible, Date lastModifiedGmt, int lastModifiedByUserId) {
        this.userAccount = userAccount;
        this.routeVisible = routeVisible;
        this.locationVisible = locationVisible;
        this.lastSeenVisible = lastSeenVisible;
        this.batteryVoltageVisible = batteryVoltageVisible;
        this.paperStatusVisible = paperStatusVisible;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UserAccountId", nullable = false)
    public UserAccount getUserAccount() {
        return this.userAccount;
    }
    
    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }
    
    @Column(name = "RouteVisible", nullable = false)
    public boolean isRouteVisible() {
        return this.routeVisible;
    }
    
    public void setRouteVisible(boolean routeVisible) {
        this.routeVisible = routeVisible;
    }
    
    @Column(name = "LocationVisible", nullable = false)
    public boolean isLocationVisible() {
        return this.locationVisible;
    }
    
    public void setLocationVisible(boolean locationVisible) {
        this.locationVisible = locationVisible;
    }
    
    @Column(name = "LastSeenVisible", nullable = false)
    public boolean isLastSeenVisible() {
        return this.lastSeenVisible;
    }
    
    public void setLastSeenVisible(boolean lastSeenVisible) {
        this.lastSeenVisible = lastSeenVisible;
    }
    
    @Column(name = "BatteryVoltageVisible", nullable = false)
    public boolean isBatteryVoltageVisible() {
        return this.batteryVoltageVisible;
    }
    
    public void setBatteryVoltageVisible(boolean batteryVoltageVisible) {
        this.batteryVoltageVisible = batteryVoltageVisible;
    }
    
    @Column(name = "PaperStatusVisible", nullable = false)
    public boolean isPaperStatusVisible() {
        return this.paperStatusVisible;
    }
    
    public void setPaperStatusVisible(boolean paperStatusVisible) {
        this.paperStatusVisible = paperStatusVisible;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = 19)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }
    
    public void setLastModifiedGmt(Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }
    
    public void setLastModifiedByUserId(int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
}
