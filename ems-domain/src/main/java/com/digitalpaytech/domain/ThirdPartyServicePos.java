package com.digitalpaytech.domain;

// Generated 17-Dec-2012 10:44:10 AM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * ThirdPartyServicePos generated by hbm2java
 */
@Entity
@Table(name = "ThirdPartyServicePOS")
public class ThirdPartyServicePos implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 7618584920288069046L;
    private Integer id;
    private ThirdPartyServiceAccount thirdPartyServiceAccount;
    private PointOfSale pointOfSale;

    public ThirdPartyServicePos() {
    }

    public ThirdPartyServicePos(
            ThirdPartyServiceAccount thirdPartyServiceAccount,
            PointOfSale pointOfSale) {
        this.thirdPartyServiceAccount = thirdPartyServiceAccount;
        this.pointOfSale = pointOfSale;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ThirdPartyServiceAccountId", nullable = false)
    public ThirdPartyServiceAccount getThirdPartyServiceAccount() {
        return this.thirdPartyServiceAccount;
    }

    public void setThirdPartyServiceAccount(
            ThirdPartyServiceAccount thirdPartyServiceAccount) {
        this.thirdPartyServiceAccount = thirdPartyServiceAccount;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PointOfSaleId", nullable = false)
    public PointOfSale getPointOfSale() {
        return this.pointOfSale;
    }

    public void setPointOfSale(PointOfSale pointOfSale) {
        this.pointOfSale = pointOfSale;
    }

}
