package com.digitalpaytech.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.digitalpaytech.domain.util.HibernateConstants;

@Entity
@Table(name = "CPSReversalData")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({
    @NamedQuery(name = "CPSReversalData.findByChargeTokenAndRefundToken", query = "FROM CPSReversalData cpsrd WHERE cpsrd.chargeToken = :chargeToken AND cpsrd.refundToken = :refundToken", cacheable = false),
    @NamedQuery(name = "CPSReversalData.findByEarliest", query = "FROM CPSReversalData cpsrd ORDER BY cpsrd.createdGMT ASC", cacheable = false) })
public class CPSReversalData implements Serializable {
    private Integer id;
    private Integer responseCode;
    private String chargeToken;
    private String refundToken;
    private String message;
    private Integer retryCount = 0;
    private Date createdGMT;
    private ReversalType reversalType;
    private static final long serialVersionUID = 2613622087853737221L;
    
    public enum ReversalType {
        CHARGE, PREAUTH, REFUND, CAPTURE;
    }

    public CPSReversalData() {
        super();
    }
    
    public CPSReversalData(final String chargeToken, 
                           final String refundToken, 
                           final Integer responseCode, 
                           final String message, 
                           final ReversalType reversalType,
                           final Date createdGMT) {
        
        this.chargeToken = chargeToken;
        this.refundToken = refundToken;
        this.responseCode = responseCode;
        this.message = message;
        this.retryCount = 0;
        this.createdGMT = createdGMT;
        this.reversalType = reversalType;
    }
    
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("responseCode", responseCode).append("chargeToken", chargeToken)
                .append("refundToken", refundToken).append("message", message).append("retryCount", retryCount).append("createdGMT", createdGMT)
                .append("reversalType", reversalType).toString();
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @Column(name = "RefundToken", length = HibernateConstants.CHAR_LENGTH_UUID)
    public String getRefundToken() {
        return this.refundToken;
    }
    
    public void setRefundToken(final String refundToken) {
        this.refundToken = refundToken;
    }
    
    @Column(name = "ChargeToken", length = HibernateConstants.CHAR_LENGTH_UUID)
    public String getChargeToken() {
        return this.chargeToken;
    }
    
    public void setChargeToken(final String chargeToken) {
        this.chargeToken = chargeToken;
    }
    
    @Column(name = "ResponseCode")
    public Integer getResponseCode() {
        return this.responseCode;
    }
    
    public void setResponseCode(final Integer responseCode) {
        this.responseCode = responseCode;
    }
    
    @Column(name = "Message", length = HibernateConstants.VARCHAR_LENGTH_5000)
    public String getMessage() {
        return this.message;
    }
    
    public void setMessage(final String message) {
        this.message = message;
    }
    
    @Column(name = "RetryCount")
    public Integer getRetryCount() {
        return this.retryCount;
    }
    
    public void setRetryCount(final Integer retryCount) {
        this.retryCount = retryCount;
    }
    
    @Enumerated(EnumType.STRING)
    @Column(name = "ReversalType")
    public ReversalType getReversalType() {
        return this.reversalType;
    }
    
    public void setReversalType(final ReversalType reversalType) {
        this.reversalType = reversalType;
    }
    
    @Column(name = "CreatedGMT", nullable = false)
    public Date getCreatedGMT() {
        return this.createdGMT;
    }
    
    public void setCreatedGMT(final Date createdGMT) {
        this.createdGMT = createdGMT;
    }
}
