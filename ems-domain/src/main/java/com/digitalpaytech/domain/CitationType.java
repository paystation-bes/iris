package com.digitalpaytech.domain;

// Generated 6-Oct-2014 1:38:59 PM by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.domain.util.HibernateConstants;

/**
 * Citationtype generated by hbm2java
 */
@Entity
@Table(name = "CitationType")
@NamedQueries({
    @NamedQuery(name = "CitationType.findByCustomerId", query = "FROM CitationType ct WHERE ct.customer.id = :customerId", cacheable = true),
    @NamedQuery(name = "CitationType.findActiveByCustomerId", query = "FROM CitationType ct WHERE ct.customer.id = :customerId and ct.isActive = true", cacheable = true),
    @NamedQuery(name = "CitationType.findByCustomerIdAndFlexCitationTypeId", query = "FROM CitationType ct WHERE ct.customer.id = :customerId AND ct.flexCitationTypeId = :flexCitationTypeId", cacheable = true),
    @NamedQuery(name = "CitationType.findByCustomerIdAndFlexIds", query = "from CitationType ct where ct.customer.id = :customerId and ct.flexCitationTypeId IN(:flexIds) ORDER BY ct.flexCitationTypeId", cacheable = true)
})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings({ "checkstyle:designforextension" })
public class CitationType implements java.io.Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1731316219871806838L;
    private Integer id;
    private Customer customer;
    private int flexCitationTypeId;    
    private String name;
    private boolean isActive; 
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    
    private String randomId;
    
    public CitationType() {
    }
    
    public CitationType(final Customer customer, final int flexCitationTypeId, final String name, final boolean isActive, 
                        final Date lastModifiedGmt, final int lastModifiedByUserId) {
        this.customer = customer;
        this.flexCitationTypeId = flexCitationTypeId;
        this.name = name;
        this.isActive = isActive;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerId", nullable = false)
    public Customer getCustomer() {
        return this.customer;
    }
    
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }
    
    @Column(name = "FlexCitationTypeId", nullable = false)
    public int getFlexCitationTypeId() {
        return this.flexCitationTypeId;
    }
    
    public void setFlexCitationTypeId(final int flexCitationTypeId) {
        this.flexCitationTypeId = flexCitationTypeId;
    }
    
    @Column(name = "Name", nullable = false, length = HibernateConstants.VARCHAR_LENGTH_40)
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }
    
    public void setLastModifiedGmt(final Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }
    
    public void setLastModifiedByUserId(final int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @Transient
    public String getRandomId() {
        return this.randomId;
    }
    
    public void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    @Column(name = "IsActive", nullable = false)
    public boolean getIsActive() {
        return this.isActive;
    }
    
    public void setIsActive(final boolean isActive) {
        this.isActive = isActive;
    }            
}
