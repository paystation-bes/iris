package com.digitalpaytech.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@Entity
@Table(name = "PaymentSmartCard")
@NamedQueries({
    @NamedQuery(name = "PaymentSmartCard.findByPurchaseId", query = "from PaymentSmartCard psc where psc.purchase.id = :purchaseId", cacheable = true)
})
public class PaymentSmartCard implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 3361249359037862592L;
    private Integer id;
    private Purchase purchase;
    private String smartCardType;
    private String smartCardData;
    
    public PaymentSmartCard() {
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)    
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PurchaseId", nullable = false)    
    public Purchase getPurchase() {
        return purchase;
    }
    
    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }
    
    @Column(name = "SmartCardType", length = 15)
    public String getSmartCardType() {
        return smartCardType;
    }
    
    public void setSmartCardType(String smartCardType) {
        this.smartCardType = smartCardType;
    }
    
    @Column(name = "SmartCardData", nullable = false, length = 40)
    public String getSmartCardData() {
        return smartCardData;
    }
    
    public void setSmartCardData(String smartCardData) {
        this.smartCardData = smartCardData;
    }
}
