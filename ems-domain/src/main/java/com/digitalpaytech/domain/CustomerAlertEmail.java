package com.digitalpaytech.domain;

// Generated 31-Aug-2012 2:05:49 PM by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryDelegateAttribute;
import com.digitalpaytech.annotation.StoryId;
import com.digitalpaytech.domain.util.HibernateConstants;

/**
 * AlertEmail generated by hbm2java
 */
@Entity
@Table(name = "CustomerAlertEmail")
@NamedQueries({
    @NamedQuery(name = "CustomerAlertEmail.deleteAllById", query = "delete CustomerAlertEmail ae where ae.id in (:id)"),
    @NamedQuery(name = "CustomerAlertEmail.findByCustomerAlertTypeId", query = "SELECT ae FROM CustomerAlertEmail ae JOIN FETCH ae.customerEmail ce WHERE ae.customerAlertType.id = :customerAlertTypeId", cacheable = true) })
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
//Checkstyle: Hibernate proxies have issues when methods are defined final
@SuppressWarnings({ "checkstyle:designforextension" })
@StoryAlias(CustomerAlertEmail.ALIAS)
public class CustomerAlertEmail implements java.io.Serializable {
    
    public static final String ALIAS = "customer alert email";
    public static final String ALIAS_CUSTOMER_EMAIL = "customer email";
    
    private static final long serialVersionUID = -417447098756648817L;
    private Integer id;
    private int version;
    private CustomerEmail customerEmail;
    private CustomerAlertType customerAlertType;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    private boolean isDeleted;
    
    public CustomerAlertEmail() {
        super();
    }
    
    public CustomerAlertEmail(final CustomerEmail customerEmail, final CustomerAlertType customerAlertType, final Date lastModifiedGmt,
            final int lastModifiedByUserId, final boolean isDeleted) {
        this.customerEmail = customerEmail;
        this.customerAlertType = customerAlertType;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
        this.isDeleted = isDeleted;
    }
    
    public CustomerAlertEmail(final Integer id, final CustomerEmail customerEmail, final CustomerAlertType customerAlertType,
            final Date lastModifiedGmt, final int lastModifiedByUserId, final boolean isDeleted) {
        this.id = id;
        this.customerEmail = customerEmail;
        this.customerAlertType = customerAlertType;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
        this.isDeleted = isDeleted;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    @StoryId
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @Version
    @Column(name = "VERSION", nullable = false)
    public int getVersion() {
        return this.version;
    }
    
    public void setVersion(final int version) {
        this.version = version;
    }
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CustomerEmailId", nullable = false)
    @StoryDelegateAttribute(from = ALIAS_CUSTOMER_EMAIL, to = CustomerEmail.ALIAS_EMAIL)
    public CustomerEmail getCustomerEmail() {
        return this.customerEmail;
    }
    
    public void setCustomerEmail(final CustomerEmail customerEmail) {
        this.customerEmail = customerEmail;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerAlertTypeId", nullable = false)
    public CustomerAlertType getCustomerAlertType() {
        return this.customerAlertType;
    }
    
    public void setCustomerAlertType(final CustomerAlertType customerAlertType) {
        this.customerAlertType = customerAlertType;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }
    
    public void setLastModifiedGmt(final Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }
    
    public void setLastModifiedByUserId(final int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @Column(name = "IsDeleted", nullable = false)
    public boolean isIsDeleted() {
        return this.isDeleted;
    }
    
    public void setIsDeleted(final boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
}
