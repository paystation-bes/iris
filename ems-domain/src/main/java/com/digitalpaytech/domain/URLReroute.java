package com.digitalpaytech.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "URLReroute")
@Cache(usage = CacheConcurrencyStrategy.NONE)
public class URLReroute implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 2057484715235227589L;
    private int id;
    private String digitalConnectIP;
    private String newManagementSysURL;
    private String newManagementSysIP;

    @Id
    @Column(name = "Id", nullable = false)    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    @Column(name = "DigitalConnectIP", nullable = false, length = 70)
    public String getDigitalConnectIP() {
        return digitalConnectIP;
    }
    
    public void setDigitalConnectIP(String digitalConnectIP) {
        this.digitalConnectIP = digitalConnectIP;
    }
    
    @Column(name = "NewManagementSysURL", nullable = false, length = 70)
    public String getNewManagementSysURL() {
        return newManagementSysURL;
    }
    
    public void setNewManagementSysURL(String newManagementSysURL) {
        this.newManagementSysURL = newManagementSysURL;
    }
    
    @Column(name = "NewManagementSysIP", nullable = false, length = 70)
    public String getNewManagementSysIP() {
        return newManagementSysIP;
    }
    
    public void setNewManagementSysIP(String newManagementSysIP) {
        this.newManagementSysIP = newManagementSysIP;
    }
}
