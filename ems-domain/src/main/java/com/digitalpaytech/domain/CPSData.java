package com.digitalpaytech.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryLookup;
import com.digitalpaytech.domain.util.HibernateConstants;

@Entity
@Table(name = "CPSData")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({
    @NamedQuery(name = "CPSData.findCPSDataByProcessorTransactionId", query = "SELECT cpsd FROM CPSData cpsd WHERE cpsd.processorTransaction.id = :processorTransactionId", cacheable = true),
    @NamedQuery(name = "CPSData.findCPSDataByProcessorTransactionIdAndRefundToken", query = "SELECT cpsd FROM CPSData cpsd WHERE cpsd.processorTransaction.id = :processorTransactionId AND cpsd.refundToken = :refundToken", cacheable = true),
    @NamedQuery(name = "CPSData.findCPSDataByProcessorTransactionIdAndRefundTokenIsNull", query = "SELECT cpsd FROM CPSData cpsd WHERE cpsd.processorTransaction.id = :processorTransactionId AND cpsd.refundToken IS NULL", cacheable = true),
    @NamedQuery(name = "CPSData.findCPSDataByChargeToken", query = "FROM CPSData cpsd WHERE cpsd.chargeToken = :chargeToken", cacheable = true),
    @NamedQuery(name = "CPSData.findCPSDataByChargeTokenAndRefundToken", query = "FROM CPSData cpsd WHERE cpsd.chargeToken = :chargeToken AND cpsd.refundToken = :refundToken", cacheable = true),
    @NamedQuery(name = "CPSData.findCPSDataByChargeTokenAndRefundTokenIsNull", query = "FROM CPSData cpsd WHERE cpsd.chargeToken = :chargeToken AND cpsd.refundToken IS NULL", cacheable = true) })
@StoryAlias(CPSData.ALIAS)
@SuppressWarnings({ "checkstyle:designforextension" })
public class CPSData implements java.io.Serializable {
    public static final String REFUND_TOKEN = "refund token";
    public static final String ALIAS = "Transaction Card Token";
    public static final String ALIAS_ID = "id";
    public static final String ALIAS_PROCESSOR_TRANSACTION = "processor transaction authorization number";
    public static final String ALIAS_TOKEN = "token";
    public static final String ALIAS_REF = "ref";
    public static final String ALIAS_CVM = "cvm";
    public static final String ALIAS_APL = "apl";
    public static final String ALIAS_AID = "aid";
    private static final long serialVersionUID = 4165158795391341068L;
    private Integer id;
    private ProcessorTransaction processorTransaction;
    private String chargeToken;
    private String cardToken;
    private String ref;
    private String cvm;
    private String apl;
    private String aid;
    private String refundToken;
    
    public CPSData() {
    }
    
    public CPSData(final ProcessorTransaction processorTransaction, final String chargeToken) {
        this.processorTransaction = processorTransaction;
        this.chargeToken = chargeToken;
    }
    
    public CPSData(final ProcessorTransaction processorTransaction, final String chargeToken, final String refundToken) {
        this.processorTransaction = processorTransaction;
        this.chargeToken = chargeToken;
        this.refundToken = refundToken;
    }
    
    public CPSData(final ProcessorTransaction processorTransaction, final String chargeToken, final String ref, final String cvm, final String apl,
            final String aid) {
        this.processorTransaction = processorTransaction;
        this.chargeToken = chargeToken;
        this.cvm = cvm;
        this.ref = ref;
        this.aid = aid;
        this.apl = apl;
    }
    
    public CPSData(final ProcessorTransaction processorTransaction, final String chargeToken, final String cardToken, final String ref, final String cvm, 
        final String apl, final String aid) {
        this(processorTransaction, chargeToken, ref, cvm, apl, aid);
        this.cardToken = cardToken;
    }    
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    @StoryAlias(ALIAS_ID)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ProcessorTransactionId")
    @StoryAlias(ALIAS_PROCESSOR_TRANSACTION)
    @StoryLookup(type = ProcessorTransaction.ALIAS, searchAttribute = ProcessorTransaction.ALIAS_AUTHORIZATION_NUMBER)
    public ProcessorTransaction getProcessorTransaction() {
        return this.processorTransaction;
    }
    
    public void setProcessorTransaction(final ProcessorTransaction processorTransaction) {
        this.processorTransaction = processorTransaction;
    }
    
    @StoryAlias(ALIAS_TOKEN)
    @Column(name = "ChargeToken", length = HibernateConstants.CHAR_LENGTH_UUID)
    public String getChargeToken() {
        return this.chargeToken;
    }
    
    public void setChargeToken(final String token) {
        this.chargeToken = token;
    }

    @Column(name = "CardToken", length = HibernateConstants.CHAR_LENGTH_UUID)    
    public String getCardToken() {
        return this.cardToken;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }
    
    @StoryAlias(ALIAS_CVM)
    @Column(name = "CVM")
    public String getCvm() {
        return this.cvm;
    }
    
    public void setCvm(final String cvm) {
        this.cvm = cvm;
    }
    
    @StoryAlias(ALIAS_REF)
    @Column(name = "Ref")
    public String getRef() {
        return this.ref;
    }
    
    public void setRef(final String ref) {
        this.ref = ref;
    }
    
    @StoryAlias(ALIAS_AID)
    @Column(name = "AID")
    public String getAid() {
        return this.aid;
    }
    
    public void setAid(final String aid) {
        this.aid = aid;
    }
    
    @StoryAlias(ALIAS_APL)
    @Column(name = "APL")
    public String getApl() {
        return this.apl;
    }
    
    public void setApl(final String apl) {
        this.apl = apl;
    }
    
    @StoryAlias(REFUND_TOKEN)
    @Column(name = "RefundToken")
    public String getRefundToken() {
        return this.refundToken;
    }
    
    public void setRefundToken(final String refundToken) {
        this.refundToken = refundToken;
    }
    
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("processorTransaction", processorTransaction).append("chargeToken", chargeToken)
                .append("cardToken", cardToken).append("ref", ref).append("cvm", cvm).append("apl", apl).append("aid", aid)
                .append("refundToken", refundToken).toString();
    }
}
