package com.digitalpaytech.domain;

// Generated 4-Sep-2012 4:25:12 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

/**
 * ExtensibleRateDayOfWeek generated by hbm2java
 */
@Entity
@Table(name = "ExtensibleRateDayOfWeek")
@NamedQueries({
    @NamedQuery(name = "ExtensibleRateDayOfWeek.findAllValidRateDayOfWeekByLocationId", 
                query = "FROM ExtensibleRateDayOfWeek erdow INNER JOIN erdow.extensibleRate as extr WHERE extr.isActive = 1 " +
                        "AND extr.extensibleRateType.id = 1 AND extr.location.id = :locationId ORDER BY erdow.dayOfWeek.id, extr.beginHourLocal, extr.beginMinuteLocal", 
                cacheable = true),
                
    @NamedQuery(name = "ExtensibleRateDayOfWeek.findByExtensibleRateId", query = "FROM ExtensibleRateDayOfWeek erdow WHERE erdow.extensibleRate.id = :extensibleRateId", cacheable = true),
    @NamedQuery(name = "ExtensibleRateDayOfWeek.deleteByExtensibleRateId", query = "DELETE FROM ExtensibleRateDayOfWeek erdow WHERE erdow.extensibleRate.id = :extensibleRateId")
    
})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ExtensibleRateDayOfWeek implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 7471416569418157518L;
    private Integer id;
    private int version;
    private ExtensibleRate extensibleRate;
    private DayOfWeek dayOfWeek;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;

    public ExtensibleRateDayOfWeek() {
    }

    public ExtensibleRateDayOfWeek(ExtensibleRate extensibleRate,
            DayOfWeek dayOfWeek, Date lastModifiedGmt, int lastModifiedByUserId) {
        this.extensibleRate = extensibleRate;
        this.dayOfWeek = dayOfWeek;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Version
    @Column(name = "VERSION", nullable = false)
    public int getVersion() {
        return this.version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ExtensibleRateId", nullable = false)
    public ExtensibleRate getExtensibleRate() {
        return this.extensibleRate;
    }

    public void setExtensibleRate(ExtensibleRate extensibleRate) {
        this.extensibleRate = extensibleRate;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DayOfWeekId", nullable = false)
    public DayOfWeek getDayOfWeek() {
        return this.dayOfWeek;
    }

    public void setDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = 19)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }

    public void setLastModifiedGmt(Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }

    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }

    public void setLastModifiedByUserId(int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    public ExtensibleRateDayOfWeek copyExtensibleRateDayOfWeek() {
        ExtensibleRateDayOfWeek extensibleRateDayOfWeek = new ExtensibleRateDayOfWeek(extensibleRate, dayOfWeek, lastModifiedGmt, lastModifiedByUserId);
        return extensibleRateDayOfWeek;
    }

}
