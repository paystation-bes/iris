package com.digitalpaytech.domain.util;

import org.hibernate.type.BigDecimalType;
import org.hibernate.type.BooleanType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.hibernate.type.Type;

public final class HibernateConstants {
    
    public static final int DATEFIELD_LENGTH = 19;
    
    public static final int LIST_SIZE_100 = 100;
    
    public static final int CHAR_LENGTH_UUID = 36;
    
    public static final int VARCHAR_LENGTH_5 = 5;
    public static final int VARCHAR_LENGTH_8 = 8;
    public static final int VARCHAR_LENGTH_10 = 10;
    public static final int VARCHAR_LENGTH_14 = 14;
    public static final int VARCHAR_LENGTH_15 = 15;
    public static final int VARCHAR_LENGTH_16 = 16;
    public static final int VARCHAR_LENGTH_20 = 20;
    public static final int VARCHAR_LENGTH_25 = 25;
    public static final int VARCHAR_LENGTH_30 = 30;
    public static final int VARCHAR_LENGTH_32 = 32;
    public static final int VARCHAR_LENGTH_36 = 36;
    public static final int VARCHAR_LENGTH_40 = 40;
    public static final int VARCHAR_LENGTH_45 = 45;
    public static final int VARCHAR_LENGTH_50 = 50;
    public static final int VARCHAR_LENGTH_60 = 60;
    public static final int VARCHAR_LENGTH_64 = 64;
    public static final int VARCHAR_LENGTH_75 = 75;
    public static final int VARCHAR_LENGTH_80 = 80;
    public static final int VARCHAR_LENGTH_100 = 100;
    public static final int VARCHAR_LENGTH_120 = 120;
    public static final int VARCHAR_LENGTH_128 = 128;
    public static final int VARCHAR_LENGTH_255 = 255;
    public static final int VARCHAR_LENGTH_340 = 340;
    public static final int VARCHAR_LENGTH_500 = 500;
    public static final int VARCHAR_LENGTH_1000 = 1000;
    public static final int VARCHAR_LENGTH_1336 = 1336;
    public static final int VARCHAR_LENGTH_5000 = 5000;
    
    public static final int COORDINATE_PRECISION = 18;
    public static final int COORDINATE_SCALE = 14;
    
    public static final int NUMBER_PRECISION = 22;
    public static final int NUMBER_SCALE = 0;
    
    // HQL parameters
    public static final String ID = "id";
    public static final String POS_EVENT_CURRENT_ID = "posEventCurrentId";
    public static final String POS_EVENT_HISTORY_ID = "posEventHistoryId";
    public static final String CUSTOMER_ID = "customerId";
    public static final String PARENT_CUSTOMER_ID = "parentCustomerId";
    public static final String TYPE_ID = "typeId";
    public static final String LOCATION_ID = "locationId";
    public static final String CUSTOMER_ALERT_TYPE_ID = "customerAlertTypeId";
    public static final String CUSTOMER_ALERT_TYPE_IDS = "customerAlertTypeIds";
    public static final String ALERT_THRESHOLD_TYPE_ID = "alertThresholdTypeId";
    public static final String ALERT_THRESHOLD_TYPE_ID_LIST = "alertThresholdTypeIdList";
    public static final String CURRENT_TIME = "currentTime";
    public static final String PAYSTATION_ID = "paystationId";
    public static final String POINTOFSALE_ID = "pointOfSaleId";
    public static final String POINTOFSALE_ID_LIST = "pointOfSaleIdList";
    public static final String POINTOFSALE_IDS = "pointOfSaleIds";
    public static final String ROUTE_ID = "routeId";
    public static final String ROUTE_IDS = "routeIds";
    public static final String ROUTE_TYPE_ID = "routeTypeId";
    public static final String USER_ACCOUNT_ID = "userAccountId";
    public static final String USER_NAME = "userName";
    public static final String ROLE_ID = "roleId";
    public static final String IS_DURING_MIGRATION = "isDuringMigration";
    public static final String PERMIT_EXPIRE_GMT = "permitExpireGmt";
    public static final String CARD_DATA = "cardData";
    public static final String LAST_4_DIGIT = "last4digit";
    public static final String MOBILE_NUMBER = "mobileNumber";
    public static final String NUMBER = "number";
    public static final String NAME = "name";
    public static final String START_DATE = "startDate";
    public static final String END_DATE = "endDate";
    public static final String YEAR = "year";
    public static final String MONTH = "month";
    public static final String DAY = "day";
    public static final String HOUR = "hour";
    public static final String QUATER_OF_HOUR = "quaterOfHour";
    public static final String EVENT_TYPE_ID = "eventTypeId";
    public static final String EVENT_TYPE_ID_LIST = "eventTypeIdList";
    public static final String TIMESTAMP = "timestamp";
    public static final String ALERT_CLASS_TYPE_ID = "alertClassTypeId";
    public static final String DEVICE_TYPE_ID = "deviceTypeId";
    public static final String EVENT_DEVICE_TYPE_ID = "eventDeviceTypeId";
    public static final String STATUS_TYPE_ID = "statusTypeId";
    public static final String ACTION_TYPE_ID = "actionTypeId";
    public static final String SEVERITY_TYPE_ID = "severityTypeId";
    public static final String FILTER_TYPE = "filterType";
    public static final String CASE_ACCOUNT_ID = "caseAccountId";
    public static final String CASE_FACILITY_ID = "caseFacilityId";
    public static final String CASE_LOT_ID = "caseLotId";
    public static final String KEYWORD = "keyword";
    public static final String TIME_ID = "timeId";
    public static final String EMAIL = "email";
    public static final String STATUS_EXPLANATION = "statusExplanation";
    public static final String EXECUTION_BEGIN_GMT = "executionBeginGmt";
    public static final String START_TIME_ID = "startTimeId";
    public static final String END_TIME_ID = "endTimeId";
    public static final String PEC_ID = "pecId";
    public static final String PEC_ID_LIST = "pecIdList";
    public static final String PURCHASED_DATE = "purchasedDate";
    public static final String PURCHASE_GMT = "purchaseGmt";
    public static final String PURCHASE_NUMBER = "purchaseNumber";
    public static final String TICKET_NUMBER = "ticketNumber";
    
    public static final String PROCESSOR_ID = "processorId";
    public static final String SUBSCRIPTION_TYPE_ID = "subscriptionTypeId";
    
    public static final String FLEX_ETL_DATA_TYPE_ID = "flexETLDataTypeId";
    public static final String FLEX_CITATION_TYPE_ID = "flexCitationTypeId";

    public static final String COLUMN_ID = "Id";
    public static final String COLUMN_NAME = "Name";
    public static final String COLUMN_SMS_ALERT_ID = "SmsAlertId";
    public static final String COLUMN_POINTOFSALE_ID = "PointOfSaleId";
    public static final String COLUMN_ORIGINAL_PERMIT_ID = "OriginalPermitId";
    public static final String COLUMN_MOBILE_NUMBER_ID = "MobileNumberId";
    public static final String COLUMN_MOBILE_NUMBER = "MobileNumber";
    public static final String COLUMN_EXPIRY_DATE = "ExpiryDate";
    public static final String COLUMN_CUSTOMER_ID = "CustomerId";
    public static final String COLUMN_LOCATION_ID = "LocationId";
    public static final String COLUMN_ORIGINAL_PURCHASED_DATE = "OriginalPurchasedDate";
    public static final String COLUMN_TICKET_NUMBER = "TicketNumber";
    public static final String COLUMN_PURCHASED_DATE = "PurchasedDate";
    public static final String COLUMN_TIME_ZONE = "TimeZone";
    public static final String COLUMN_LAST_4_DIGIT = "Last4Digit";
    public static final String COLUMN_LICENCE_PLATE_ID = "LicencePlateId";
    public static final String COLUMN_PLATE_NUMBER = "PlateNumber";
    public static final String COLUMN_STALL_NUMBER = "StallNumber";
    public static final String COLUMN_IS_AUTO_EXTENDED = "IsAutoExtended";
    public static final String COLUMN_COMM_ADDRESS = "CommAddress";
    public static final String COLUMN_LOWER_LIMIT = "LowerLimit";
    public static final String COLUMN_UPPER_LIMIT = "UpperLimit";
    public static final String COLUMN_MAX_ID = "MaxId";
    public static final String COLUMN_COUNT = "Count";
    public static final String COLUMN_ACTIVE_BATCH_SUMMARY_ID = "ActiveBatchSummaryId";
    public static final String COLUMN_BATCH_NUMBER = "BatchNumber";
    public static final String COLUMN_MERCHANT_ACCOUNT_ID = "MerchantAccountId";
    public static final String COLUMN_NO_OF_TRANSACTIONS = "NoOfTransactions";
    public static final String COLUMN_CLOSE_BATCH_SIZE = "CloseBatchSize";
    public static final String COLUMN_CLOSE_QUARTER_OF_DAY = "CloseQuarterOfDay";
    public static final String COLUMN_CURRENT_QUARTER_OF_DAY = "CurrentQuarterOfDay";
    public static final String COLUMN_LAST_BATCH_CLOSED_BY_TIME_TIME = "LastBatchClosedByTimeTime";
    
    public static final String HQL_CUSTOMER_ID = "customer.id";
    public static final String HQL_IS_ACTIVE = "isActive";
    
    public static final String PARAMETER_LAST_MODIFIED_GMT = "lastModifiedGmt";
    public static final String PARAMETER_LOCATION_IDS = "locationIds";
    
    public static final String PARAMETER_LOCATION_ID = LOCATION_ID;
    
    public static final Type TYPE_STRING = new StringType();
    public static final Type TYPE_DATETIME = new TimestampType();
    public static final Type TYPE_BOOLEAN = new BooleanType();
    public static final Type TYPE_INTEGER = new IntegerType();
    public static final Type TYPE_LONG = new LongType();
    public static final Type TYPE_DECIMAL = new BigDecimalType();
    public static final String STRING_NULL = "null";
    
    public static final String ALIAS_POS_BALANCE = "posBalance";
    public static final String ALIAS_POS_HEARTBEAT = "posHeartbeat";
    public static final String ALIAS_POS_SERVICE_STATE = "posServiceState";
    public static final String ALIAS_POS_SENSOR_STATE = "posSensorState";
    public static final String ALIAS_POS_ALERT_STATUS = "posAlertStatus";
    public static final String ALIAS_POS_MAC_ADDRESS = "posMacAddress";
    
    private HibernateConstants() {
    }
}
