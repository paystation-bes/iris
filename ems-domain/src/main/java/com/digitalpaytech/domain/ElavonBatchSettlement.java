package com.digitalpaytech.domain;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the ElavonBatchSettlement database table.
 * 
 */
@Entity
@Table(name="ElavonBatchSettlement")
public class ElavonBatchSettlement implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -5359390623590797209L;
    private String id;
    private Date batchEndDateTime;
    private int batchNumber;
    private Date batchStartDateTime;
    private MerchantAccount merchantAccount;

    public ElavonBatchSettlement() {
    }


    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false)
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }


    @Temporal(TemporalType.TIMESTAMP)
    public Date getBatchEndDateTime() {
        return this.batchEndDateTime;
    }

    public void setBatchEndDateTime(Date batchEndDateTime) {
        this.batchEndDateTime = batchEndDateTime;
    }


    @Column(nullable = false)
    public int getBatchNumber() {
        return this.batchNumber;
    }

    public void setBatchNumber(int batchNumber) {
        this.batchNumber = batchNumber;
    }


    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    public Date getBatchStartDateTime() {
        return this.batchStartDateTime;
    }

    public void setBatchStartDateTime(Date batchStartDateTime) {
        this.batchStartDateTime = batchStartDateTime;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MerchantAccountId", nullable = false)
    public MerchantAccount getMerchantAccount() {
        return this.merchantAccount;
    }

    public void setMerchantAccount(MerchantAccount merchantAccount) {
        this.merchantAccount = merchantAccount;
    }
}