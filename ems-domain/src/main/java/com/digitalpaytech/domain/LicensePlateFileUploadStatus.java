package com.digitalpaytech.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@Entity
@Table(name = "LicensePlateFileUploadStatus")
@NamedQueries({
    @NamedQuery(name = "LicensePlateFileUploadStatus.findByCustomerIdAndStatus", query = "from LicensePlateFileUploadStatus l where l.customer.id = :customerId and l.status = :status", cacheable = false),
    @NamedQuery(name = "LicensePlateFileUploadStatus.findByCustomerIdOrderByLastUpdatedGMTDesc", query = "FROM LicensePlateFileUploadStatus l WHERE l.customer.id = :customerId ORDER BY l.lastUpdatedGMT DESC", cacheable = false)
})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class LicensePlateFileUploadStatus implements Serializable {
    public static final int ERROR_STRING_MAX_LEN = 500;
    public enum ProcessStatus {
        IN_PROCESS, SUCCESS, ERROR_OUT
    }
    private static final long serialVersionUID = -1648130551214402554L;    
    private int id;
    private Customer customer;
    private String fileId;
    private String fileName;
    private String status;
    private String error;
    private Date lastUpdatedGMT;
    private int lastModifiedByUserId;
    
    public LicensePlateFileUploadStatus() {
        super();
    }
    
    public LicensePlateFileUploadStatus(final String fileId, final Customer customer, final String fileName, 
                                        final String status, final String error, final int lastModifiedByUserId) {
        super();
        this.fileId = fileId;
        this.customer = customer;
        this.fileName = fileName;
        this.status = status;
        this.error = error;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }

    public LicensePlateFileUploadStatus(final String fileId, final Customer customer, final String fileName, 
                                        final String status, final String error, final int lastModifiedByUserId, final Date lastUpdatedGMT) {
        this(fileId, customer, fileName, status, error, lastModifiedByUserId);
        this.lastUpdatedGMT = lastUpdatedGMT;
    }

        
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    @Column(name = "FileId", nullable = true, unique = true)
    public String getFileId() {
        return fileId;
    }
    
    public void setFileId(String fileId) {
        this.fileId = fileId;
    }
    
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "CustomerId", nullable = false)
    public Customer getCustomer() {
        return customer;
    }
    
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    
    @Column(name = "FileName", nullable = false)
    public String getFileName() {
        return fileName;
    }
    
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    @Column(name = "Status")
    public String getStatus() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    
    @Column(name = "Error", length = 500)
    public String getError() {
        return error;
    }
    
    public void setError(String error) {
        this.error = error;
    }
    
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastUpdatedGMT", nullable = false)
    public Date getLastUpdatedGMT() {
        return lastUpdatedGMT;
    }
    
    public void setLastUpdatedGMT(Date lastUpdatedGMT) {
        this.lastUpdatedGMT = lastUpdatedGMT;
    }
    
    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return lastModifiedByUserId;
    }
    
    public void setLastModifiedByUserId(int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @Override
    public String toString() {
        return "LicensePlateFileUploadStatus [id=" + id + ", fileId=" + fileId + ", customer=" + customer + ", fileName=" + fileName + ", status="
               + status + ", error=" + error + ", lastUpdatedGMT=" + lastUpdatedGMT + ", lastModifiedByUserId=" + lastModifiedByUserId + "]";
    }
    
}
