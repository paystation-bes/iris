package com.digitalpaytech.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.domain.util.HibernateConstants;

@Entity
@Table(name = "FlexWSUserAccount")
@NamedQueries({
    @NamedQuery(name = "FlexWSUserAccount.findFlexWSUserAccountByCustomerId", query = "FROM FlexWSUserAccount fwsua WHERE fwsua.customer.id = :customerId", cacheable = true),
    @NamedQuery(name = "FlexWSUserAccount.findById", query = "FROM FlexWSUserAccount fwsusa WHERE fwsusa.id = :id", cacheable = true) })
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
//Checkstyle: Hibernate proxies have issues when methods are defined final
@SuppressWarnings({ "checkstyle:designforextension" })
public class FlexWSUserAccount implements Serializable {
    private static final long serialVersionUID = -2352915708089707928L;
    
    public enum Blocker {
        CITATION_TYPE_MISSING   (0B000001),
        FACILITY_MISSING        (0B000010),
        PROPERTY_MISSING        (0B000100);
        
        private int bitFlags;
        
        Blocker(final int bitFlags) {
            this.bitFlags = bitFlags;
        }
        
        public final void block(final FlexWSUserAccount flexAccount) {
            flexAccount.setBlockingStatus(flexAccount.getBlockingStatus() | this.bitFlags);
        }
        
        public final void unblock(final FlexWSUserAccount flexAccount) {
            flexAccount.setBlockingStatus(flexAccount.getBlockingStatus() & ~this.bitFlags);
        }
    }
    
    private Integer id;
    private Customer customer;
    private String url;
    private String username;
    private String password;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    private String timeZone;
    private int version;
    private int blockingStatus;
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerId", nullable = false)
    public Customer getCustomer() {
        return this.customer;
    }
    
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }
    
    @Column(name = "URL", nullable = false, length = HibernateConstants.VARCHAR_LENGTH_500)
    public String getUrl() {
        return this.url;
    }
    
    public void setUrl(final String url) {
        this.url = url;
    }
    
    @Column(name = "Username", nullable = false, length = HibernateConstants.VARCHAR_LENGTH_50)
    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(final String username) {
        this.username = username;
    }
    
    @Column(name = "Password", nullable = false, length = HibernateConstants.VARCHAR_LENGTH_50)
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(final String password) {
        this.password = password;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }
    
    public void setLastModifiedGmt(final Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }
    
    public void setLastModifiedByUserId(final int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    // This is just timezone offset, not the actual timezone region, so daylight saving has to be handle manually if this get used.
    @Column(name = "TimeZone", nullable = false, length = HibernateConstants.VARCHAR_LENGTH_30)
    public String getTimeZone() {
        return this.timeZone;
    }

    public void setTimeZone(final String timeZone) {
        this.timeZone = timeZone;
    }
    
    @Version
    @Column(name = "VERSION", nullable = false)
    public int getVersion() {
        return this.version;
    }
    
    public void setVersion(int version) {
        this.version = version;
    }
    
    @Column(name = "BlockingStatus", nullable = false)
    public int getBlockingStatus() {
        return this.blockingStatus;
    }
    
    public void setBlockingStatus(final int blockingStatus) {
        this.blockingStatus = blockingStatus;
    }
    
    @Transient
    public boolean isBlocked() {
        return this.blockingStatus != 0;
    }
}
