package com.digitalpaytech.domain;

// Generated 14-Jun-2012 7:52:50 AM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.digitalpaytech.annotation.StoryAlias;

/**
 * Widgettiertype generated by hbm2java
 */
@Entity
@Table(name = "WidgetTierType")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@StoryAlias(WidgetTierType.ALIAS)
public class WidgetTierType implements Serializable, Cloneable {
    public static final String ALIAS = "Widget Tier Type";
    
    public static final String ALIAS_NAME = "Name";
    public static final String ALIAS_LABEL = "Label";
    
    private static final long serialVersionUID = 5572639665173724982L;
    
    private Integer id;
    private Integer listNumber;
    private String name;
    private short label;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    private Set<WidgetSubsetMember> widgetSubsetMembers = new HashSet<WidgetSubsetMember>(0);
    private Set<Widget> widgetsForWidgetTier1TypeId = new HashSet<Widget>(0);
    private Set<Widget> widgetsForWidgetTier2TypeId = new HashSet<Widget>(0);
    private Set<Widget> widgetsForWidgetTier3TypeId = new HashSet<Widget>(0);
    private String randomId;
    
    public WidgetTierType() {
    }

    public WidgetTierType(final Integer id, final Integer listNumber, final String name, final short label,
        final Date lastModifiedGmt, final int lastModifiedByUserId) {
        this.id = id;
        this.listNumber = listNumber;
        this.name = name;
        this.label = label;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }

    public WidgetTierType(final Integer id, final Integer listNumber, final String name, final short label,
        final Date lastModifiedGmt, final int lastModifiedByUserId,
        final Set<WidgetSubsetMember> widgetSubsetMembers,
        final Set<Widget> widgetsForWidgetTier3TypeId,
        final Set<Widget> widgetsForWidgetTier1TypeId,
        final Set<Widget> widgetsForWidgetTier2TypeId) {
        this.id = id;
        this.listNumber = listNumber;
        this.name = name;
        this.label = label;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
        this.widgetSubsetMembers = widgetSubsetMembers;
        this.widgetsForWidgetTier3TypeId = widgetsForWidgetTier3TypeId;
        this.widgetsForWidgetTier1TypeId = widgetsForWidgetTier1TypeId;
        this.widgetsForWidgetTier2TypeId = widgetsForWidgetTier2TypeId;
    }

    @Id
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    @Column(name = "ListNumber", nullable = false)
    public Integer getListNumber() {
        return this.listNumber;
    }

    public void setListNumber(final Integer listNumber) {
        this.listNumber = listNumber;
    }

    @Column(name = "Name", unique = true, nullable = false, length = 30)
    @StoryAlias(ALIAS_NAME)
    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }
    
    @Column(name = "Label", unique = true, nullable = false)
    @StoryAlias(ALIAS_LABEL)
    public short getLabel() {
        return this.label;
    }

    public void setLabel(final short label) {
        this.label = label;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = 19)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }

    public void setLastModifiedGmt(final Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }

    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }

    public void setLastModifiedByUserId(final int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "widgetTierType")
    public Set<WidgetSubsetMember> getWidgetSubsetMembers() {
        return this.widgetSubsetMembers;
    }

    public void setWidgetSubsetMembers(final Set<WidgetSubsetMember> widgetSubsetMembers) {
        this.widgetSubsetMembers = widgetSubsetMembers;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "widgetTierTypeByWidgetTier3Type")
    public Set<Widget> getWidgetsForWidgetTier3TypeId() {
        return this.widgetsForWidgetTier3TypeId;
    }

    public void setWidgetsForWidgetTier3TypeId(final Set<Widget> widgetsForWidgetTier3TypeId) {
        this.widgetsForWidgetTier3TypeId = widgetsForWidgetTier3TypeId;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "widgetTierTypeByWidgetTier1Type")
    public Set<Widget> getWidgetsForWidgetTier1TypeId() {
        return this.widgetsForWidgetTier1TypeId;
    }

    public void setWidgetsForWidgetTier1TypeId(final Set<Widget> widgetsForWidgetTier1TypeId) {
        this.widgetsForWidgetTier1TypeId = widgetsForWidgetTier1TypeId;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "widgetTierTypeByWidgetTier2Type")
    public Set<Widget> getWidgetsForWidgetTier2TypeId() {
        return this.widgetsForWidgetTier2TypeId;
    }

    public void setWidgetsForWidgetTier2TypeId(final Set<Widget> widgetsForWidgetTier2TypeId) {
        this.widgetsForWidgetTier2TypeId = widgetsForWidgetTier2TypeId;
    }

    @Transient
    public String getRandomId() {
        return this.randomId;
    }

    public void setRandomId(final String randomId) {
        this.randomId = randomId;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        final WidgetTierType tierType = (WidgetTierType) super.clone();
        return tierType;
    }
}
