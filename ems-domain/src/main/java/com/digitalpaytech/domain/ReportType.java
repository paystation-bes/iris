package com.digitalpaytech.domain;

// Generated 16-Nov-2012 4:23:06 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * Reporttype generated by hbm2java
 */
@Entity
@Table(name = "ReportType")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ReportType implements java.io.Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6151504145487532113L;
    private short id;
    private String name;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    private boolean allowSystemAdmin;
    private boolean allowParentCustomerAdmin;
    private boolean allowChildCustomerAdmin;

    public ReportType() {
    }
    
    public ReportType(short id) {
        this.id = id;
    }

    public ReportType(short id, String name, Date lastModifiedGmt, int lastModifiedByUserId) {
        this.id = id;
        this.name = name;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }


    @Id
    @Column(name = "Id", nullable = false)
    public short getId() {
        return this.id;
    }

    public void setId(short id) {
        this.id = id;
    }

    @Column(name = "Name", unique = true, nullable = false, length = 40)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = 19)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }

    public void setLastModifiedGmt(Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }

    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }

    public void setLastModifiedByUserId(int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @Column(name = "AllowSystemAdmin")
    public boolean isAllowSystemAdmin() {
        return allowSystemAdmin;
    }

    public void setAllowSystemAdmin(boolean allowSystemAdmin) {
        this.allowSystemAdmin = allowSystemAdmin;
    }
    
    @Column(name = "AllowParentCustomerAdmin")
    public boolean isAllowParentCustomerAdmin() {
        return allowParentCustomerAdmin;
    }

    public void setAllowParentCustomerAdmin(boolean allowParentCustomerAdmin) {
        this.allowParentCustomerAdmin = allowParentCustomerAdmin;
    }
    
    @Column(name = "AllowChildCustomerAdmin")
    public boolean isAllowChildCustomerAdmin() {
        return allowChildCustomerAdmin;
    }

    public void setAllowChildCustomerAdmin(boolean allowChildCustomerAdmin) {
        this.allowChildCustomerAdmin = allowChildCustomerAdmin;
    }

}
