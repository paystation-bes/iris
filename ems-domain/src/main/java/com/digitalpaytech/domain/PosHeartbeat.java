package com.digitalpaytech.domain;

// Generated 27-Sep-2012 11:54:11 AM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Parameter;

import com.digitalpaytech.annotation.StorageType;
import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryLookup;
import com.digitalpaytech.annotation.StoryTransformer;
import com.digitalpaytech.domain.util.HibernateConstants;

/**
 * Posheartbeat generated by hbm2java
 */
@Entity
@Table(name = "POSHeartbeat")
@NamedQuery(name = "PosHeartbeat.findPosHeartbeatForPointOfSaleById", query = "from PosHeartbeat phb where phb.pointOfSale.id = :pointOfSaleId")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@StoryAlias(PosHeartbeat.ALIAS)
@SuppressWarnings("checkstyle:designforextension")
public class PosHeartbeat implements java.io.Serializable {
    public static final String ALIAS = "heartbeat";
    public static final String ALIAS_LAST_SEEN = "last seen";
    private static final long serialVersionUID = 3865701700330590662L;
    private int pointOfSaleId;
    private PointOfSale pointOfSale;
    private Date lastHeartbeatGmt;
    private int version;
    
    public PosHeartbeat() {
        super();
    }
    
    public PosHeartbeat(final PointOfSale pointOfSale, final Date lastHeartbeatGmt) {
        this.pointOfSale = pointOfSale;
        this.lastHeartbeatGmt = lastHeartbeatGmt;
    }
    
    @GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "pointOfSale"))
    @Id
    @GeneratedValue(generator = "generator")
    @Column(name = "PointOfSaleId", nullable = false)
    public int getPointOfSaleId() {
        return this.pointOfSaleId;
    }
    
    public void setPointOfSaleId(final int pointOfSaleId) {
        this.pointOfSaleId = pointOfSaleId;
    }
    
    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @PrimaryKeyJoinColumn
    @StoryAlias(PointOfSale.ALIAS)
    @StoryLookup(type = PointOfSale.ALIAS, searchAttribute = PointOfSale.ALIAS_NAME, storage = StorageType.DB)
    public PointOfSale getPointOfSale() {
        return this.pointOfSale;
    }
    
    public void setPointOfSale(final PointOfSale pointOfSale) {
        this.pointOfSale = pointOfSale;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastHeartbeatGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    @StoryAlias(ALIAS_LAST_SEEN)
    @StoryTransformer("dateString")
    public Date getLastHeartbeatGmt() {
        return this.lastHeartbeatGmt;
    }
    
    public void setLastHeartbeatGmt(final Date lastHeartbeatGmt) {
        this.lastHeartbeatGmt = lastHeartbeatGmt;
    }
    
    @Column(name = "VERSION", nullable = false)
    public int getVersion() {
        return this.version;
    }
    
    public void setVersion(final int version) {
        this.version = version;
    }
    
}
