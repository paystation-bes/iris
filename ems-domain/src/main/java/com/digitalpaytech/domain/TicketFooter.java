package com.digitalpaytech.domain;

// Generated 13-Dec-2012 1:26:14 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * TicketFooter generated by hbm2java
 */
@Entity
@Table(name = "TicketFooter")
public class TicketFooter implements java.io.Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3614637344518169912L;
    private Integer id;
    private int version;
    private Location location;
    private Customer customer;
    private String line1;
    private String line2;
    private String line3;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;

    public TicketFooter() {
    }

    public TicketFooter(Customer customer, String line1, String line2,
            String line3, Date lastModifiedGmt, int lastModifiedByUserId) {
        this.customer = customer;
        this.line1 = line1;
        this.line2 = line2;
        this.line3 = line3;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }

    public TicketFooter(Location location, Customer customer, String line1,
            String line2, String line3, Date lastModifiedGmt,
            int lastModifiedByUserId) {
        this.location = location;
        this.customer = customer;
        this.line1 = line1;
        this.line2 = line2;
        this.line3 = line3;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Version
    @Column(name = "VERSION", nullable = false)
    public int getVersion() {
        return this.version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LocationId")
    public Location getLocation() {
        return this.location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerId", nullable = false)
    public Customer getCustomer() {
        return this.customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Column(name = "Line1", nullable = false, length = 24)
    public String getLine1() {
        return this.line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    @Column(name = "Line2", nullable = false, length = 24)
    public String getLine2() {
        return this.line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    @Column(name = "Line3", nullable = false, length = 24)
    public String getLine3() {
        return this.line3;
    }

    public void setLine3(String line3) {
        this.line3 = line3;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = 19)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }

    public void setLastModifiedGmt(Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }

    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }

    public void setLastModifiedByUserId(int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }

}
