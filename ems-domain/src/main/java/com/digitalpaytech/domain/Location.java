package com.digitalpaytech.domain;

// Generated 22-May-2012 9:11:50 AM by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryId;

/**
 * Location generated by hbm2java
 */
@Entity
@Table(name = "Location")
@NamedQueries({
    @NamedQuery(name = "Location.findLocationsByCustomerId", query = "from Location location where location.customer.id in (:customerId) AND isDeleted = 0 ORDER BY location.name", cacheable = true),
    @NamedQuery(name = "Location.findLocationTreeByCustomerId", cacheable = true, query = " select location.id as id, parentLocation.id as parentId, location.name as name, location.isParent as isParent, location.isUnassigned as isUnassigned, location.isDeleted as isDeleted, location.customer.id as customerId"
                                                                                          + " from Location location left join location.location parentLocation"
                                                                                          + " where (location.customer.id in (:customerId))"
                                                                                          + " and ((location.isDeleted <> :showActive) or (location.isDeleted = :showDeleted))"
                                                                                          + " and ((location.isUnassigned <> :showAssigned) or (location.isUnassigned = :showUnassigned))"
                                                                                          + " order by location.customer.id, parentLocation.id, location.name"),
    
    @NamedQuery(name = "Location.findChildLocationsByCustomerId", query = "from Location location where location.customer.id in (:customerId) AND isDeleted = 0 AND isParent = 0 ORDER BY location.name", cacheable = true),
    @NamedQuery(name = "Location.findParentLocationsByCustomerId", query = "from Location location where location.customer.id in (:customerId) AND isDeleted = 0 AND isParent = 1 ORDER BY location.name", cacheable = true),
    @NamedQuery(name = "Location.findLocationsByParentId", query = "from Location location where location.location.id = :parentLocationId AND isDeleted = 0 ORDER BY location.name", cacheable = true),
    @NamedQuery(name = "Location.findUnassignedLocationByCustomerId", query = "from Location location where location.customer.id = :customerId AND location.name = 'Unassigned' AND location.isDeleted = 0", cacheable = true),
    @NamedQuery(name = "Location.findUnassignedLocationIdByCustomerId", query = "select id from Location where (isUnassigned = true) and (customer.id = :customerId)", cacheable = true),
    @NamedQuery(name = "Location.findLocationsByCustomerIdAndName", query = "from Location location where location.customer.id = :customerId AND location.name = :name AND location.isDeleted = 0", cacheable = true),
    @NamedQuery(name = "Location.findLowestLocationsByCustomerIdAndName", query = "from Location as location where location.customer.id = :customerId AND location.location.id in (select parent.id from Location parent WHERE parent.customer.id = :customerId and parent.name = :locationName)", cacheable = true),
    @NamedQuery(name = "Location.findLocationFilterByCustomerId", cacheable = true, query = " select location.id as value, location.name as label, parentLocation.name as desc"
                                                                                            + " from Location location left join location.location parentLocation"
                                                                                            + " where (location.customer.id in (:customerIds)) "
                                                                                            + " and ((location.isDeleted <> :showActive) or (location.isDeleted = :showDeleted))"
                                                                                            + " and ((location.isUnassigned <> :showAssigned) or (location.isUnassigned = :showUnassigned))"
                                                                                            + " order by location.customer.id, location.name"),
    @NamedQuery(name = "Location.findLocationByCustomerId", cacheable = true, query = " select location"
                                                                                      + " from Location location left join fetch location.location parentLocation"
                                                                                      + " where (location.customer.id in (:customerIds)) "
                                                                                      + " and ((location.isDeleted <> :showActive) or (location.isDeleted = :showDeleted))"
                                                                                      + " and ((location.isUnassigned <> :showAssigned) or (location.isUnassigned = :showUnassigned))"
                                                                                      + " order by location.customer.id, location.name"),
    @NamedQuery(name = "Location.findByLowerCaseNames", query = "from Location as loc where (loc.customer.id = :customerId) and (loc.isDeleted = 0) and (lower(loc.name) in(:locationNames))", cacheable = true),
    @NamedQuery(name = "Location.findParentLocationByCustomerIdLocationName", query = "from Location location where location.customer.id in (:customerId) AND isDeleted = 0 AND isParent = 1 AND LOWER(location.name) = LOWER(:locationName)", cacheable = true),
    @NamedQuery(name = "Location.findByIdFetchParent", query = "select loc from Location loc left join fetch loc.location parentLoc where loc.id = :locationId", cacheable = true),
    @NamedQuery(name = "Location.findLocationByActiveCaseLocationLots", query = "SELECT DISTINCT loc FROM Location loc JOIN loc.caseLocationLots cll WHERE cll.isActive = 1 AND loc.customer.id IN (SELECT cs.customer.id FROM CustomerSubscription cs WHERE cs.subscriptionType.id = 1800 AND cs.isEnabled = 1)", cacheable = true) })
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings({ "checkstyle:designforextension" })
@StoryAlias(value = Location.ALIAS)
public class Location implements java.io.Serializable {
    
    /**
     * 
     */
    public static final String ALIAS = "location";
    public static final String ALIAS_LOCATION_NAME = "Location Name";
    public static final String ALIAS_ID = "Id";
    public static final String ALIAS_CAPACITY = "Capacity";
    
    private static final long serialVersionUID = 8893862942286490901L;
    private Integer id;
    private int version;
    private Location location;
    private Customer customer;
    private PermitIssueType permitIssueType;
    private String name;
    private int numberOfSpaces;
    private int targetMonthlyRevenueAmount;
    private String description;
    private boolean isParent;
    private boolean isUnassigned;
    private boolean isDeleted;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    private Set<Location> locations = new HashSet<Location>(0);
    private Set<Permit> permits = new HashSet<Permit>(0);
    private Set<Purchase> purchases = new HashSet<Purchase>(0);
    private Set<TicketFooter> ticketFooters = new HashSet<TicketFooter>(0);
    private Set<LocationPosLog> locationPosLogs = new HashSet<LocationPosLog>(0);
    private Set<CustomerAlertType> customerAlertTypes = new HashSet<CustomerAlertType>(0);
    private Set<LocationOpen> locationOpens = new HashSet<LocationOpen>(0);
    private Set<LocationGeopoint> locationGeopoints = new HashSet<LocationGeopoint>(0);
    private Set<Coupon> coupons = new HashSet<Coupon>(0);
    private Set<ParkingPermission> parkingPermissions = new HashSet<ParkingPermission>(0);
    private Set<LocationDay> locationDays = new HashSet<LocationDay>(0);
    private Set<CustomerCard> customerCards = new HashSet<CustomerCard>(0);
    private Set<PointOfSale> pointOfSales = new HashSet<PointOfSale>(0);
    private Set<ExtensibleRate> extensibleRates = new HashSet<ExtensibleRate>(0);
    private Set<SmsAlert> smsAlerts = new HashSet<SmsAlert>(0);
    private Set<CaseLocationLot> caseLocationLots = new HashSet<CaseLocationLot>(0);
    
    private String randomId;
    
    public Location() {
    }
    
    public Location(final Customer customer, final String name, final int numberOfSpaces, final int targetMonthlyRevenueAmount,
            final boolean isParent, final boolean isUnassigned, final boolean isDeleted, final Date lastModifiedGmt, final int lastModifiedByUserId) {
        this.customer = customer;
        this.name = name;
        this.numberOfSpaces = numberOfSpaces;
        this.targetMonthlyRevenueAmount = targetMonthlyRevenueAmount;
        this.isParent = isParent;
        this.isUnassigned = isUnassigned;
        this.isDeleted = isDeleted;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    public Location(final Location location, final Customer customer, final PermitIssueType permitIssueType, final String name,
            final int numberOfSpaces, final int targetMonthlyRevenueAmount, final String description, final boolean isParent,
            final boolean isUnassigned, final boolean isDeleted, final Date lastModifiedGmt, final int lastModifiedByUserId,
            final Set<Location> locations, final Set<Permit> permits, final Set<Purchase> purchases, final Set<TicketFooter> ticketFooters,
            final Set<LocationPosLog> locationPosLogs, final Set<CustomerAlertType> customerAlertTypes, final Set<LocationOpen> locationOpens,
            final Set<LocationGeopoint> locationGeopoints, final Set<Coupon> coupons, final Set<ParkingPermission> parkingPermissions,
            final Set<LocationDay> locationDays, final Set<CustomerCard> customerCards, final Set<PointOfSale> pointOfSales,
            final Set<ExtensibleRate> extensibleRates, final Set<SmsAlert> smsAlerts) {
        this.location = location;
        this.customer = customer;
        this.permitIssueType = permitIssueType;
        this.name = name;
        this.numberOfSpaces = numberOfSpaces;
        this.targetMonthlyRevenueAmount = targetMonthlyRevenueAmount;
        this.description = description;
        this.isParent = isParent;
        this.isUnassigned = isUnassigned;
        this.isDeleted = isDeleted;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
        this.locations = locations;
        this.permits = permits;
        this.purchases = purchases;
        this.ticketFooters = ticketFooters;
        this.locationPosLogs = locationPosLogs;
        this.customerAlertTypes = customerAlertTypes;
        this.locationOpens = locationOpens;
        this.locationGeopoints = locationGeopoints;
        this.coupons = coupons;
        this.parkingPermissions = parkingPermissions;
        this.locationDays = locationDays;
        this.customerCards = customerCards;
        this.pointOfSales = pointOfSales;
        this.extensibleRates = extensibleRates;
        this.smsAlerts = smsAlerts;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    @StoryAlias(value = ALIAS_ID)
    @StoryId
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @Version
    @Column(name = "VERSION", nullable = false)
    public int getVersion() {
        return this.version;
    }
    
    public void setVersion(final int version) {
        this.version = version;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ParentLocationId")
    public Location getLocation() {
        return this.location;
    }
    
    public void setLocation(final Location location) {
        this.location = location;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerId", nullable = false)
    public Customer getCustomer() {
        return this.customer;
    }
    
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PermitIssueTypeId", nullable = false)
    public PermitIssueType getPermitIssueType() {
        return this.permitIssueType;
    }
    
    public void setPermitIssueType(final PermitIssueType permitIssueType) {
        this.permitIssueType = permitIssueType;
    }
    
    @Column(name = "Name", nullable = false, length = 25)
    @StoryAlias(value = ALIAS_LOCATION_NAME)
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    @Column(name = "NumberOfSpaces", nullable = false)
    @StoryAlias(value = ALIAS_CAPACITY)
    public int getNumberOfSpaces() {
        return this.numberOfSpaces;
    }
    
    public void setNumberOfSpaces(final int numberOfSpaces) {
        this.numberOfSpaces = numberOfSpaces;
    }
    
    @Column(name = "TargetMonthlyRevenueAmount", nullable = false)
    public int getTargetMonthlyRevenueAmount() {
        return this.targetMonthlyRevenueAmount;
    }
    
    public void setTargetMonthlyRevenueAmount(final int targetMonthlyRevenueAmount) {
        this.targetMonthlyRevenueAmount = targetMonthlyRevenueAmount;
    }
    
    @Column(name = "Description", length = 100)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(final String description) {
        this.description = description;
    }
    
    @Column(name = "IsParent", nullable = false)
    public boolean getIsParent() {
        return this.isParent;
    }
    
    public void setIsParent(final boolean isParent) {
        this.isParent = isParent;
    }
    
    @Column(name = "IsUnassigned", nullable = false)
    public boolean getIsUnassigned() {
        return this.isUnassigned;
    }
    
    public void setIsUnassigned(final boolean isUnassigned) {
        this.isUnassigned = isUnassigned;
    }
    
    @Column(name = "IsDeleted", nullable = false)
    public boolean getIsDeleted() {
        return this.isDeleted;
    }
    
    public void setIsDeleted(final boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = 19)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }
    
    public void setLastModifiedGmt(final Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }
    
    public void setLastModifiedByUserId(final int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    public Set<Location> getLocations() {
        return this.locations;
    }
    
    public void setLocations(final Set<Location> locations) {
        this.locations = locations;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    public Set<Permit> getPermits() {
        return this.permits;
    }
    
    public void setPermits(final Set<Permit> permits) {
        this.permits = permits;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    public Set<Purchase> getPurchases() {
        return this.purchases;
    }
    
    public void setPurchases(final Set<Purchase> purchases) {
        this.purchases = purchases;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    public Set<TicketFooter> getTicketFooters() {
        return this.ticketFooters;
    }
    
    public void setTicketFooters(final Set<TicketFooter> ticketFooters) {
        this.ticketFooters = ticketFooters;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    public Set<LocationPosLog> getLocationPosLogs() {
        return this.locationPosLogs;
    }
    
    public void setLocationPosLogs(final Set<LocationPosLog> locationPosLogs) {
        this.locationPosLogs = locationPosLogs;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    public Set<CustomerAlertType> getCustomerAlertTypes() {
        return this.customerAlertTypes;
    }
    
    public void setCustomerAlertTypes(final Set<CustomerAlertType> customerAlertTypes) {
        this.customerAlertTypes = customerAlertTypes;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    public Set<LocationOpen> getLocationOpens() {
        return this.locationOpens;
    }
    
    public void setLocationOpens(final Set<LocationOpen> locationOpens) {
        this.locationOpens = locationOpens;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    public Set<LocationGeopoint> getLocationGeopoints() {
        return this.locationGeopoints;
    }
    
    public void setLocationGeopoints(final Set<LocationGeopoint> locationGeopoints) {
        this.locationGeopoints = locationGeopoints;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    public Set<Coupon> getCoupons() {
        return this.coupons;
    }
    
    public void setCoupons(final Set<Coupon> coupons) {
        this.coupons = coupons;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    public Set<ParkingPermission> getParkingPermissions() {
        return this.parkingPermissions;
    }
    
    public void setParkingPermissions(final Set<ParkingPermission> parkingPermissions) {
        this.parkingPermissions = parkingPermissions;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    public Set<LocationDay> getLocationDays() {
        return this.locationDays;
    }
    
    public void setLocationDays(final Set<LocationDay> locationDays) {
        this.locationDays = locationDays;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    public Set<CustomerCard> getCustomerCards() {
        return this.customerCards;
    }
    
    public void setCustomerCards(final Set<CustomerCard> customerCards) {
        this.customerCards = customerCards;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    public Set<PointOfSale> getPointOfSales() {
        return this.pointOfSales;
    }
    
    public void setPointOfSales(final Set<PointOfSale> pointOfSales) {
        this.pointOfSales = pointOfSales;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    public Set<ExtensibleRate> getExtensibleRates() {
        return this.extensibleRates;
    }
    
    public void setExtensibleRates(final Set<ExtensibleRate> extensibleRates) {
        this.extensibleRates = extensibleRates;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    public Set<SmsAlert> getSmsAlerts() {
        return this.smsAlerts;
    }
    
    public void setSmsAlerts(final Set<SmsAlert> smsAlerts) {
        this.smsAlerts = smsAlerts;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "location")
    public Set<CaseLocationLot> getCaseLocationLots() {
        return this.caseLocationLots;
    }
    
    public void setCaseLocationLots(final Set<CaseLocationLot> caseLocationLots) {
        this.caseLocationLots = caseLocationLots;
    }
    
    public void removeLocation(final Location location) {
        getLocations().remove(location);
    }
    
    public void addLocation(final Location location) {
        getLocations().add(location);
    }
    
    @Transient
    public String getRandomId() {
        return this.randomId;
    }
    
    public void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
}
