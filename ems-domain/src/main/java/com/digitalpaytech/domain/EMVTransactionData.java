package com.digitalpaytech.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.digitalpaytech.domain.util.HibernateConstants;

@Entity
@Table(name = "EMVTransactionData")
public class EMVTransactionData implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 2222440524383143777L;
    private Integer id;
    private EMVTransactionDataMapping emvTransactionDataMapping;
    private ProcessorTransaction processorTransaction;
    private String value1;
    private String value2;
    private String value3;
    private String value4;
    private String value5;
    private String value6;
    private String value7;
    private Date createdGmt;
 
    
    public EMVTransactionData() {        
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }    
    
    @Column(name = "Value1", length = HibernateConstants.VARCHAR_LENGTH_100)
    public String getValue1() {
        return this.value1;
    }
    
    public void setValue1(final String value1) {
        this.value1 = value1;
    }
    
    @Column(name = "Value2", length = HibernateConstants.VARCHAR_LENGTH_100)
    public String getValue2() {
        return this.value2;
    }
    
    public void setValue2(final String value2) {
        this.value2 = value2;
    }
    
    @Column(name = "Value3", length = HibernateConstants.VARCHAR_LENGTH_100)
    public String getValue3() {
        return this.value3;
    }
    
    public void setValue3(final String value3) {
        this.value3 = value3;
    }
    
    @Column(name = "Value4", length = HibernateConstants.VARCHAR_LENGTH_100)
    public String getValue4() {
        return this.value4;
    }
    
    public void setValue4(final String value4) {
        this.value4 = value4;
    }
    
    @Column(name = "Value5", length = HibernateConstants.VARCHAR_LENGTH_100)
    public String getValue5() {
        return this.value5;
    }
    
    public void setValue5(final String value5) {
        this.value5 = value5;
    }
    
    @Column(name = "Value6", length = HibernateConstants.VARCHAR_LENGTH_100)
    public String getValue6() {
        return this.value6;
    }
    
    public void setValue6(final String value6) {
        this.value6 = value6;
    }
    
    @Column(name = "Value7", length = HibernateConstants.VARCHAR_LENGTH_100)
    public String getValue7() {
        return this.value7;
    }
    
    public void setValue7(final String value7) {
        this.value7 = value7;
    }

    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EMVTransactionDataMappingId", nullable = false)    
    public EMVTransactionDataMapping getEMVTransactionDataMapping() {
        return emvTransactionDataMapping;
    }

    public void setEMVTransactionDataMapping(EMVTransactionDataMapping emvTransactionDataMapping) {
        this.emvTransactionDataMapping = emvTransactionDataMapping;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getCreatedGmt() {
        return this.createdGmt;
    }
    
    public void setCreatedGmt(final Date createdGmt) {
        this.createdGmt = createdGmt;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ProcessorTransactionId", nullable = false) 
    public ProcessorTransaction getProcessorTransaction() {
        return processorTransaction;
    }

    public void setProcessorTransaction(ProcessorTransaction processorTransaction) {
        this.processorTransaction = processorTransaction;
    }
    
}
