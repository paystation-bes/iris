package com.digitalpaytech.domain;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import java.util.Date;


/**
 * The persistent class for the ActiveBatchSummary database table.
 * 
 */
@Entity
@Table(name="ActiveBatchSummary")
@NamedQueries({    
    @NamedQuery(name = "ActiveBatchSummary.findByBatchNoAndMerchantId", query = "from ActiveBatchSummary abs where abs.batchNumber = :batchNo and abs.merchantAccount.id = :merchantId", cacheable = true),
    @NamedQuery(name = "ActiveBatchSummary.findActiveBatchSummaryForCloseBatch", query = "from ActiveBatchSummary abs where abs.merchantAccount.id = :merchantAccountId and abs.batchNumber = :batchNumber", cacheable = false),
    @NamedQuery(name = "ActiveBatchSummary.hasActiveBatchSummaryClosed", query = "from ActiveBatchSummary abs where abs.id = :id and abs.batchstatustype.id = 2", cacheable = false)
    })
public class ActiveBatchSummary implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 808143569364427800L;
    private Integer id;
    private short batchNumber;
    private Date createdGMT;
    private Date lastModifiedGMT;
    private MerchantAccount merchantAccount;
    private int noOfTransactions;
    private BatchStatusType batchstatustype;

	public ActiveBatchSummary() {
	}


    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


	@Column(nullable = false)
	public short getBatchNumber() {
		return this.batchNumber;
	}

	public void setBatchNumber(short batchNumber) {
		this.batchNumber = batchNumber;
	}


    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    public Date getCreatedGMT() {
        return this.createdGMT;
    }

    public void setCreatedGMT(Date createdGMT) {
        this.createdGMT = createdGMT;
    }


    @Temporal(TemporalType.TIMESTAMP)
    public Date getLastModifiedGMT() {
        return this.lastModifiedGMT;
    }

    public void setLastModifiedGMT(Date lastModifiedGMT) {
        this.lastModifiedGMT = lastModifiedGMT;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MerchantAccountId", nullable = false)
    public MerchantAccount getMerchantAccount() {
        return this.merchantAccount;
    }

    public void setMerchantAccount(MerchantAccount merchantAccount) {
        this.merchantAccount = merchantAccount;
    }

    @Column(nullable = false)
    public int getNoOfTransactions() {
        return this.noOfTransactions;
    }

    public void setNoOfTransactions(int noOfTransactions) {
        this.noOfTransactions = noOfTransactions;
    }


    //bi-directional many-to-one association to Batchstatustype
    @ManyToOne
    @JoinColumn(name="BatchStatusTypeId", nullable = false)
    public BatchStatusType getBatchstatustype() {
        return this.batchstatustype;
    }

    public void setBatchstatustype(BatchStatusType batchstatustype) {
        this.batchstatustype = batchstatustype;
    }

}
