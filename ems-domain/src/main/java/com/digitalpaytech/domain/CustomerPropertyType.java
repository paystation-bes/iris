package com.digitalpaytech.domain;

// Generated 9-Jul-2012 3:04:19 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.digitalpaytech.annotation.StoryAlias;

/**
 * Customerpropertytype generated by hbm2java
 */
@Entity
@Table(name = "CustomerPropertyType")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@StoryAlias(CustomerPropertyType.ALIAS)
public class CustomerPropertyType implements java.io.Serializable {
    public static final String ALIAS = "Customer Property Type";
    public static final String ALIAS_ID = "Id";
    public static final String ALIAS_NAME = "Name";
    
    /**
     * 
     */
    private static final long serialVersionUID = 7381582646519636481L;
    private int id;
    private String name;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    private Set<CustomerProperty> customerProperties = new HashSet<CustomerProperty>(
            0);

    public CustomerPropertyType() {
    }
    
    public CustomerPropertyType(int id) {
        this.id = id;
    }

    public CustomerPropertyType(int id, String name, Date lastModifiedGmt,
            int lastModifiedByUserId) {
        this.id = id;
        this.name = name;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }

    public CustomerPropertyType(int id, String name, Date lastModifiedGmt,
            int lastModifiedByUserId, Set<CustomerProperty> customerProperties) {
        this.id = id;
        this.name = name;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
        this.customerProperties = customerProperties;
    }

    @Id
    @Column(name = "Id", nullable = false)
    @StoryAlias(ALIAS_ID)
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "Name", unique = true, nullable = false, length = 40)
    @StoryAlias(ALIAS_NAME)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = 19)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }

    public void setLastModifiedGmt(Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }

    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }

    public void setLastModifiedByUserId(int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customerPropertyType")
    public Set<CustomerProperty> getCustomerProperties() {
        return this.customerProperties;
    }

    public void setCustomerProperties(Set<CustomerProperty> customerProperties) {
        this.customerProperties = customerProperties;
    }

}
