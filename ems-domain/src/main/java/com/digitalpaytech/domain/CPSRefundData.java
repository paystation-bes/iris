package com.digitalpaytech.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.domain.util.HibernateConstants;

@Entity
@Table(name = "CPSRefundData")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@StoryAlias(CPSRefundData.ALIAS)
@NamedQueries({
    @NamedQuery(name = "CPSRefundData.findByChargeToken", query = "FROM CPSRefundData crd WHERE crd.chargeToken = :chargeToken", cacheable = true),
    @NamedQuery(name = "CPSRefundData.findByRefundToken", query = "FROM CPSRefundData crd WHERE crd.refundToken = :refundToken", cacheable = true) })
public class CPSRefundData {
    
    public static final String ALIAS = "cps refund placeholder";
    public static final String ALIAS_PROCESSOR_TRANSACTION_ID = "processor transaction id";
    public static final String ALIAS_REFERENCE_NUMBER = "reference number";
    public static final String ALIAS_LAST_FOUR_DIGITS = "last four digits";
    public static final String ALIAS_CARD_EXPIRY = "card expiry";
    public static final String ALIAS_AMOUNT = "amount";
    public static final String ALIAS_CHARGE_TOKEN = "charge token";
    public static final String ALIAS_AUTHORIZATION_NUMBER = "authorization number";
    private static final String ALIAS_REFUND_TOKEN = "refund token";
    
    private Integer id;
    private String refundToken;
    private int amount;
    private String cardExpiry;
    private String chargeToken;
    private String last4Digits;
    private String authorizationNumber;
    private String referenceNumber;
    private String processorTransactionId;
    private Date createdGMT;
    private String terminalToken;
    
    public CPSRefundData() {
        super();
    }
    
    public CPSRefundData(final Integer id, final String refundToken, final int amount, final String cardExpiry, final String chargeToken,
            final String last4Digits, final String authorizationNumber, final String referenceNumber, final String processorTransactionId,
            final Date createdGMT, final String terminalToken) {
        super();
        this.id = id;
        this.refundToken = refundToken;
        this.amount = amount;
        this.cardExpiry = cardExpiry;
        this.chargeToken = chargeToken;
        this.last4Digits = last4Digits;
        this.authorizationNumber = authorizationNumber;
        this.referenceNumber = referenceNumber;
        this.processorTransactionId = processorTransactionId;
        this.createdGMT = createdGMT;
        this.terminalToken = terminalToken;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @Column(name = "RefundToken", length = HibernateConstants.CHAR_LENGTH_UUID)
    @StoryAlias(ALIAS_REFUND_TOKEN)
    public String getRefundToken() {
        return this.refundToken;
    }
    
    public void setRefundToken(final String refundToken) {
        this.refundToken = refundToken;
    }
    
    @Column(name = "ChargeToken", length = HibernateConstants.CHAR_LENGTH_UUID)
    @StoryAlias(ALIAS_CHARGE_TOKEN)
    public String getChargeToken() {
        return this.chargeToken;
    }
    
    public void setChargeToken(final String chargeToken) {
        this.chargeToken = chargeToken;
    }
    
    @Column(name = "Amount")
    @StoryAlias(ALIAS_AMOUNT)
    public int getAmount() {
        return this.amount;
    }
    
    public void setAmount(final int amount) {
        this.amount = amount;
    }
    
    @Column(name = "CardExpiry")
    @StoryAlias(ALIAS_CARD_EXPIRY)
    public String getCardExpiry() {
        return this.cardExpiry;
    }
    
    public void setCardExpiry(final String cardExpiry) {
        this.cardExpiry = cardExpiry;
    }
    
    @Column(name = "Last4Digits")
    @StoryAlias(ALIAS_LAST_FOUR_DIGITS)
    public String getLast4Digits() {
        return this.last4Digits;
    }
    
    public void setLast4Digits(final String last4) {
        this.last4Digits = last4;
    }
    
    @Column(name = "AuthorizationNumber")
    @StoryAlias(ALIAS_AUTHORIZATION_NUMBER)
    public String getAuthorizationNumber() {
        return this.authorizationNumber;
    }
    
    public void setAuthorizationNumber(final String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }
    
    @Column(name = "ReferenceNumber")
    @StoryAlias(ALIAS_REFERENCE_NUMBER)
    public String getReferenceNumber() {
        return this.referenceNumber;
    }
    
    public void setReferenceNumber(final String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }
    
    @Column(name = "ProcessorTransactionId")
    @StoryAlias(ALIAS_PROCESSOR_TRANSACTION_ID)
    public String getProcessorTransactionId() {
        return this.processorTransactionId;
    }
    
    public void setProcessorTransactionId(final String processorTransactionId) {
        this.processorTransactionId = processorTransactionId;
    }
    
    @Column(name = "CreatedGMT", nullable = false)
    public Date getCreatedGMT() {
        return this.createdGMT;
    }
    
    public void setCreatedGMT(final Date createdGMT) {
        this.createdGMT = createdGMT;
    }
    
    public String getTerminalToken() {
        return this.terminalToken;
    }
    
    public void setTerminalToken(final String terminalToken) {
        this.terminalToken = terminalToken;
    }
    
    @Transient
    public boolean isExpired() {
        return StringUtils.isBlank(getAuthorizationNumber()) && StringUtils.isBlank(getProcessorTransactionId())
               && StringUtils.isBlank(getReferenceNumber());
    }
    
    @Override
    public String toString() {
        return new StringBuilder().append("OfflineRefundResultMessage [amount=").append(this.amount).append(", cardExpiry=").append(this.cardExpiry)
                .append(", chargeToken=").append(this.chargeToken).append(", last4Digits=").append(this.last4Digits).append(", authorizationNumber=")
                .append(this.authorizationNumber).append(", referenceNumber=").append(this.referenceNumber).append(", processorTransactionId=")
                .append(this.processorTransactionId).append("]").toString();
    }
    
}
