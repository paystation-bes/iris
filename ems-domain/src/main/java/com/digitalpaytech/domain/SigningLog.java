package com.digitalpaytech.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SigningLog")
public class SigningLog implements java.io.Serializable {

    private static final long serialVersionUID = -2127048530275695958L;
    private int id;
    private int userAccountId;
    private String userAccountName;
    private String signature;
    private Date signDate;
    private String comments;

    public SigningLog() {

    }

    public SigningLog(int id, int userAccountId, String userAccountName,
            String signature, Date signDate, String comments) {
        super();
        this.id = id;
        this.userAccountId = userAccountId;
        this.userAccountName = userAccountName;
        this.signature = signature;
        this.signDate = signDate;
        this.comments = comments;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "UserAccountId", nullable = false)
    public int getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(int userAccountId) {
        this.userAccountId = userAccountId;
    }

    @Column(name = "UserAccountName", nullable = false)
    public String getUserAccountName() {
        return userAccountName;
    }

    public void setUserAccountName(String userAccountName) {
        this.userAccountName = userAccountName;
    }

    @Column(name = "signature", nullable = false)
    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Column(name = "signDate", nullable = false)
    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    @Column(name = "comments", nullable = false)
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

}
