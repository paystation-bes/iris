package com.digitalpaytech.domain;

// Generated 3-Oct-2013 10:36:56 AM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.data.UserDefinedEventInfo;
import com.digitalpaytech.domain.util.HibernateConstants;

@Entity
@Table(name = "AlertThresholdType13EventClear_v")
@NamedQueries({
    @NamedQuery(name = "AlertThresholdType13EventClearV.findByPointOfSaleId", query = "FROM AlertThresholdType13EventClearV atte JOIN FETCH atte.posEventCurrent pec JOIN FETCH pec.eventType et WHERE atte.pointOfSaleId IN(:pointOfSaleIdList)"),
    @NamedQuery(name = "AlertThresholdType13EventClearV.findByPointOfSaleIdAndCustomerAlertTypeId", query = "FROM AlertThresholdType13EventClearV atte JOIN FETCH atte.posEventCurrent pec JOIN FETCH pec.eventType et WHERE atte.pointOfSaleId IN(:pointOfSaleIdList) AND atte.customerAlertType.id = :customerAlertTypeId") })
@SuppressWarnings({ "checkstyle:designforextension" })
public class AlertThresholdType13EventClearV extends UserDefinedEventInfo {
    
    private static final long serialVersionUID = 6759786293187698216L;
    
    public AlertThresholdType13EventClearV() {
        super();
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerAlertTypeId", nullable = false)
    public CustomerAlertType getCustomerAlertType() {
        return this.customerAlertType;
    }
    
    public void setCustomerAlertType(final CustomerAlertType customerAlertType) {
        this.customerAlertType = customerAlertType;
    }
    
    @Id
    @Column(name = "PointOfSaleId")
    public Integer getPointOfSaleId() {
        return this.pointOfSaleId;
    }
    
    public void setPointOfSaleId(final Integer pointOfSaleId) {
        this.pointOfSaleId = pointOfSaleId;
    }
    
    @Column(name = "GapToMinor", precision = HibernateConstants.NUMBER_PRECISION, scale = HibernateConstants.NUMBER_SCALE)
    public Double getGapToMinor() {
        return this.gapToMinor;
    }
    
    public void setGapToMinor(final Double gapToMinor) {
        this.gapToMinor = gapToMinor;
    }
    
    @Column(name = "GapToMajor", precision = HibernateConstants.NUMBER_PRECISION, scale = HibernateConstants.NUMBER_SCALE)
    public Double getGapToMajor() {
        return this.gapToMajor;
    }
    
    public void setGapToMajor(final Double gapToMajor) {
        this.gapToMajor = gapToMajor;
    }
    
    @Column(name = "GapToCritical", precision = HibernateConstants.NUMBER_PRECISION, scale = HibernateConstants.NUMBER_SCALE)
    public Double getGapToCritical() {
        return this.gapToCritical;
    }
    
    public void setGapToCritical(final Double gapToCritical) {
        this.gapToCritical = gapToCritical;
    }
    
    @Id
    @Column(name = "POSEventCurrentId", nullable = false)
    public Long getPosEventCurrentId() {
        return this.posEventCurrentId;
    }
    
    public void setPosEventCurrentId(final Long posEventCurrentId) {
        this.posEventCurrentId = posEventCurrentId;
    }
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "POSEventCurrentId", nullable = false, insertable = false, updatable = false)
    public PosEventCurrent getPosEventCurrent() {
        return this.posEventCurrent;
    }
    
    public void setPosEventCurrent(final PosEventCurrent posEventCurrent) {
        this.posEventCurrent = posEventCurrent;
    }
}
