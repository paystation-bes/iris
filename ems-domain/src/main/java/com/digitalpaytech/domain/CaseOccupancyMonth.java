package com.digitalpaytech.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@Entity
@Table(name = "CaseOccupancyMonth")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
//@NamedQueries({
//    @NamedQuery(name = "CaseOccupancyHour.findLatestOccupancyMonth", query = "SELECT MAX(coh.timeGmt) FROM CaseOccupancyMonth coh", cacheable = true)})
//  
public class CaseOccupancyMonth {
 
    
    private Long id;
    private Time localTime;
    private Customer customer;
    private Time timeGmt;
    private int numberOfSpaces;
    private Location location;
    private int numOfPermits;
    
    public CaseOccupancyMonth() {
    }
    
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TimeIdLocal", nullable = false)
    public Time getLocalTime() {
        return this.localTime;
    }
    
    public void setLocalTime(final Time localTime) {
        this.localTime = localTime;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerId", nullable = false)
    public Customer getCustomer() {
        return this.customer;
    }
    
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TimeIdGMT", nullable = false)
    public Time getTimeGmt() {
        return this.timeGmt;
    }
    
    public void setTimeGmt(final Time timeGmt) {
        this.timeGmt = timeGmt;
    }
    
    @Column(name = "NumberOfSpaces", nullable = false)
    public int getNumberOfSpaces() {
        return this.numberOfSpaces;
    }
    
    public void setNumberOfSpaces(final int numberOfSpaces) {
        this.numberOfSpaces = numberOfSpaces;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LocationId", nullable = false)
    public Location getLocation() {
        return location;
    }


    public void setLocation(Location location) {
        this.location = location;
    }


    @Column(name = "NoOfPermits", nullable = false)
    public int getNumOfPermits() {
        return numOfPermits;
    }


    public void setNumOfPermits(int numOfPermits) {
        this.numOfPermits = numOfPermits;
    }
}
