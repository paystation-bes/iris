package com.digitalpaytech.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@Entity
@Table(name = "CollectionSummaryConfig")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({
    @NamedQuery(name = "CollectionSummaryConfig.findColumnShowByUserAccountId", 
                query = "SELECT csc FROM CollectionSummaryConfig csc WHERE csc.userAccount.id = :userAccountId", cacheable = true)
})
public class CollectionSummaryConfig implements Serializable {
   
    private static final long serialVersionUID = 6773890116258489687L;
    
    private int id;
    private UserAccount userAccount;
    private boolean routeVisible;
    private boolean locationVisible;
    private boolean coinCountVisible;
    private boolean coinTotalVisible;
    private boolean billCountVisible;
    private boolean billTotalVisible;
    private boolean cardCountVisible;
    private boolean cardTotalVisible;
    private boolean runningTotalVisible;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;   
    
    @Id
    @Column(name = "Id", nullable = false)    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UserAccountId",  unique = true, nullable = false)   
    public UserAccount getUserAccount() {
        return userAccount;
    }
    
    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }
    
    @Column(name = "RouteVisible", nullable = false)
    public boolean getRouteVisible() {
        return routeVisible;
    }
    
    public void setRouteVisible(boolean routeVisible) {
        this.routeVisible = routeVisible;
    }
    
    @Column(name = "LocationVisible", nullable = false)
    public boolean getLocationVisible() {
        return locationVisible;
    }
    
    public void setLocationVisible(boolean locationVisible) {
        this.locationVisible = locationVisible;
    }
    
    @Column(name = "CoinCountVisible", nullable = false)
    public boolean getCoinCountVisible() {
        return coinCountVisible;
    }
    
    public void setCoinCountVisible(boolean coinCountVisible) {
        this.coinCountVisible = coinCountVisible;
    }
    
    @Column(name = "CoinTotalVisible", nullable = false)
    public boolean getCoinTotalVisible() {
        return coinTotalVisible;
    }
    
    public void setCoinTotalVisible(boolean coinTotalVisible) {
        this.coinTotalVisible = coinTotalVisible;
    }
    
    @Column(name = "BillCountVisible", nullable = false)
    public boolean getBillCountVisible() {
        return billCountVisible;
    }
    
    public void setBillCountVisible(boolean billCountVisible) {
        this.billCountVisible = billCountVisible;
    }
    
    @Column(name = "BillTotalVisible", nullable = false)
    public boolean getBillTotalVisible() {
        return billTotalVisible;
    }
    
    public void setBillTotalVisible(boolean billTotalVisible) {
        this.billTotalVisible = billTotalVisible;
    }
    
    @Column(name = "CardCountVisible", nullable = false)
    public boolean getCardCountVisible() {
        return cardCountVisible;
    }
    
    public void setCardCountVisible(boolean cardCountVisible) {
        this.cardCountVisible = cardCountVisible;
    }
    
    @Column(name = "CardTotalVisible", nullable = false)
    public boolean getCardTotalVisible() {
        return cardTotalVisible;
    }
    
    public void setCardTotalVisible(boolean cardTotalVisible) {
        this.cardTotalVisible = cardTotalVisible;
    }
    
    @Column(name = "RunningTotalVisible", nullable = false)
    public boolean getRunningTotalVisible() {
        return runningTotalVisible;
    }
    
    public void setRunningTotalVisible(boolean runningTotalVisible) {
        this.runningTotalVisible = runningTotalVisible;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = 19)    
    public Date getLastModifiedGmt() {
        return lastModifiedGmt;
    }
    
    public void setLastModifiedGmt(Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return lastModifiedByUserId;
    }
    
    public void setLastModifiedByUserId(int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }       
}
