package com.digitalpaytech.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.domain.util.HibernateConstants;

@Entity
@Table(name = "FlexLocationFacility")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({
    @NamedQuery(name = "FlexLocationFacility.findFlexLocationFacilitiesByCustomerId", query = "from FlexLocationFacility flf where flf.customer.id = :customerId", cacheable = true),
    @NamedQuery(name = "FlexLocationFacility.findActiveFlexLocationFacilityIdByCustomerId", query = "select flf.id from FlexLocationFacility flf where flf.customer.id = :customerId and flf.facIsActive = 1 and flf.location.id NOT IN(select flf2.location.id from FlexLocationFacility flf2 where flf2.customer.id = :customerId and flf2.location.id NOT IN(:locationIds))", cacheable = true),
    @NamedQuery(name = "FlexLocationFacility.findFlexLocationFacilityIdByLocationIds", query = "select flf.id from FlexLocationFacility flf where flf.location.id IN(:locationIds) and flf.facIsActive = 1", cacheable = true),
    @NamedQuery(name = "FlexLocationFacility.findFlexLocationFacilityByLocationIds", query = "from FlexLocationFacility flf where flf.location.id = :locationId and flf.facIsActive = 1", cacheable = true),
    @NamedQuery(name = "FlexLocationFacility.findActiveFlexLocationFacilityByCustomerId", query = "from FlexLocationFacility flf where flf.customer.id = :customerId and flf.facIsActive = 1 and flf.location.id NOT IN(select flf2.location.id from FlexLocationFacility flf2 where flf2.customer.id = :customerId and flf2.location.id NOT IN(:locationIds))", cacheable = true),
    @NamedQuery(name = "FlexLocationFacility.findByCustomerIdAndFlexIds", query = "from FlexLocationFacility flf where flf.customer.id = :customerId and flf.facUid IN(:flexIds) ORDER BY flf.facUid", cacheable = true)
})
//Checkstyle: Hibernate proxies have issues when methods are defined final
@SuppressWarnings({ "checkstyle:designforextension" })
public class FlexLocationFacility implements java.io.Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -4407225502352831272L;
    
    private Integer id;
    private Customer customer;
    private Location location;
    private Integer facUid;
    private String facCode;
    private String facDescription;
    private boolean facIsActive;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    
    private String randomId;
    
    public FlexLocationFacility() {
        super();
    }
    
    public FlexLocationFacility(final Integer id, final int facUid, final String facCode, final String facDescription, final boolean facIsActive,
            final Location location, final Customer customer, final Date lastModifiedGmt, final int lastModifiedByUserId) {
        this.id = id;
        this.facUid = facUid;
        this.facCode = facCode;
        this.facDescription = facDescription;
        this.facIsActive = facIsActive;
        this.location = location;
        this.customer = customer;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerId", nullable = false)
    public Customer getCustomer() {
        return this.customer;
    }
    
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LocationId", nullable = true)
    public Location getLocation() {
        return this.location;
    }
    
    public void setLocation(final Location location) {
        this.location = location;
    }
    
    @Column(name = "FAC_UID", nullable = false)
    public int getFacUid() {
        return this.facUid;
    }
    
    public void setFacUid(final int facUid) {
        this.facUid = facUid;
    }
    
    @Column(name = "FAC_CODE", nullable = false, length = HibernateConstants.VARCHAR_LENGTH_8)
    public String getFacCode() {
        return this.facCode;
    }
    
    public void setFacCode(final String facCode) {
        this.facCode = facCode;
    }
    
    @Column(name = "FAC_DESCRIPTION", nullable = false, length = HibernateConstants.VARCHAR_LENGTH_32)
    public String getFacDescription() {
        return this.facDescription;
    }
    
    public void setFacDescription(final String facDescription) {
        this.facDescription = facDescription;
    }
    
    @Column(name = "FAC_IS_ACTIVE", nullable = false)
    public boolean isFacIsActive() {
        return this.facIsActive;
    }
    
    public void setFacIsActive(final boolean facIsActive) {
        this.facIsActive = facIsActive;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }
    
    public void setLastModifiedGmt(final Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }
    
    public void setLastModifiedByUserId(final int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @Transient
    public String getRandomId() {
        return this.randomId;
    }
    
    @Transient
    public void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
}
