package com.digitalpaytech.domain;

// Generated 21-Nov-2012 2:34:06 PM by Hibernate Tools 3.4.0.CR1

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

/**
 * Reportlocationvalue generated by hbm2java
 */
@Entity
@Table(name = "ReportLocationValue")
@NamedQueries({ @NamedQuery(name = "ReportLocationValue.findReportLocationValuesByReportDefinition", query = "FROM ReportLocationValue WHERE reportDefinition.id IN(:reportDefinitionId)", cacheable = false) })
public class ReportLocationValue implements java.io.Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2051049404381717910L;
    private Integer id;
    private ReportDefinition reportDefinition;
    private int objectId;
    private String objectType;

    public ReportLocationValue() {
    }

    public ReportLocationValue(int id, ReportDefinition reportDefinition, int objectId, String objectType) {
        this.id = id;
        this.reportDefinition = reportDefinition;
        this.objectId = objectId;
        this.objectType = objectType;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ReportDefinitionId", nullable = false)
    public ReportDefinition getReportDefinition() {
        return this.reportDefinition;
    }

    public void setReportDefinition(ReportDefinition reportDefinition) {
        this.reportDefinition = reportDefinition;
    }

    @Column(name = "ObjectId", nullable = false)
    public int getObjectId() {
        return this.objectId;
    }

    public void setObjectId(int objectId) {
        this.objectId = objectId;
    }

    @Column(name = "ObjectType", nullable = false, length = 100)
    public String getObjectType() {
        return this.objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }
    
    public Identity createIdentity() {
        return new Identity(this);
    }
    
    public static class Identity implements Serializable {
        private static final long serialVersionUID = 7385163451661419372L;
        
        private String objectType;
        private int objectId;
        
        private Identity(ReportLocationValue rlv) {
            this.objectType = rlv.objectType;
            this.objectId = rlv.objectId;
        }
        
        public String getObjectType() {
            return objectType;
        }

        public int getObjectId() {
            return objectId;
        }
        
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + objectId;
            result = prime * result
                    + ((objectType == null) ? 0 : objectType.hashCode());
            return result;
        }
        
        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Identity other = (Identity) obj;
            if (objectId != other.objectId)
                return false;
            if (objectType == null) {
                if (other.objectType != null)
                    return false;
            } else if (!objectType.equals(other.objectType))
                return false;
            return true;
        }
    }
}
