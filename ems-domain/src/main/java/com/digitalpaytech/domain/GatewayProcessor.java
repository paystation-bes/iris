package com.digitalpaytech.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.digitalpaytech.domain.util.HibernateConstants;

@Entity
@Table(name = "GatewayProcessor")
public class GatewayProcessor implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 2763212526442221519L;
    private Integer id;
    private Processor processor;
    private String name;
    private String value;
    private Date createdGmt;

    public GatewayProcessor() {
    }
    
    public GatewayProcessor(final Integer id, 
                            final Processor processor, 
                            final String name, 
                            final String value, 
                            final Date createdGmt) {
        this.id = id;
        this.processor = processor;
        this.name = name;
        this.value = value;
        this.createdGmt = createdGmt;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ProcessorId", nullable = false)
    public Processor getProcessor() {
        return this.processor;
    }
    
    public void setProcessor(final Processor processor) {
        this.processor = processor;
    }
    
    @Column(name = "Name", length = HibernateConstants.VARCHAR_LENGTH_60)    
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    @Column(name = "Value", length = HibernateConstants.VARCHAR_LENGTH_100)
    public String getValue() {
        return this.value;
    }
    
    public void setValue(final String value) {
        this.value = value;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getCreatedGmt() {
        return this.createdGmt;
    }
    
    public void setCreatedGmt(final Date createdGmt) {
        this.createdGmt = createdGmt;
    }
}
