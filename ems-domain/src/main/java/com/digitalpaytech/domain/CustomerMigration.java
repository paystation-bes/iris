package com.digitalpaytech.domain;

// Generated 27-Nov-2012 11:53:47 AM by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.domain.util.HibernateConstants;

/**
 * CustomerMigration generated by hbm2java
 */
@Entity
@Table(name = "CustomerMigration")
@NamedQueries({
    @NamedQuery(name = "CustomerMigration.findScheduledCustomerMigration", query = "SELECT cm FROM CustomerMigration cm LEFT JOIN FETCH cm.customer c WHERE cm.customerMigrationStatusType.id = 1 AND cm.migrationRequestedGmt < :processWaitTime ORDER BY migrationRequestedGmt ASC", cacheable = false),
    @NamedQuery(name = "CustomerMigration.countMigratingCustomers", query = "SELECT COUNT(cm.id) FROM CustomerMigration cm WHERE cm.customerMigrationStatusType.id NOT IN (0, 1, 8, 9)", cacheable = false),
    @NamedQuery(name = "CustomerMigration.findNextAvailableCustomer", query = "SELECT cm FROM CustomerMigration cm LEFT JOIN FETCH cm.customer c WHERE cm.customerMigrationStatusType.id NOT IN (10) AND ((cm.customerMigrationStatusType.id = 1 AND cm.migrationScheduledGmt < :currentTime) OR cm.customerMigrationStatusType.id IN (3, 4, 7) OR (cm.customerMigrationStatusType.id IN (0, 1, 2, 5, 6, 8, 9, 90) AND cm.backGroundJobNextTryGmt < :currentTime) OR (cm.customerMigrationStatusType.id = 99 AND cm.backGroundJobNextTryGmt IS NOT NULL AND cm.backGroundJobNextTryGmt < :currentTime)) AND cm.isMigrationInProgress = 0 ORDER BY cm.lastModifiedGmt ASC, cm.customerMigrationStatusType.id ASC", cacheable = false),
    @NamedQuery(name = "CustomerMigration.findThisWeeksSchedule", query = "SELECT cm FROM CustomerMigration cm LEFT JOIN FETCH cm.customer c WHERE (cm.customerMigrationStatusType.id = 90 AND cm.customerBoardedGmt BETWEEN :startTime AND :endTime) OR (cm.customerMigrationStatusType.id = 0 AND cm.migrationRequestedGmt BETWEEN :startTime AND :endTime) OR (cm.customerMigrationStatusType.id = 1 AND cm.migrationScheduledGmt BETWEEN :startTime AND :endTime) OR (cm.customerMigrationStatusType.id IN (2, 3) AND cm.migrationCancelGmt BETWEEN :startTime AND :endTime) OR (cm.customerMigrationStatusType.id NOT IN (0, 1, 2, 3, 90, 99) AND cm.migrationStartGmt BETWEEN :startTime AND :endTime)", cacheable = false),
    @NamedQuery(name = "CustomerMigration.findCustomerMigrationByCustomerId", query = "FROM CustomerMigration cm WHERE cm.customer.id = :customerId") })
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
// Checkstyle: Hibernate proxies have issues when methods are defined final
// PMD: Domain object has too many columns
@SuppressWarnings({ "checkstyle:designforextension", "PMD.GodClass", "PMD.ExcessivePublicCount" })
public class CustomerMigration implements java.io.Serializable {
    
    private static final long serialVersionUID = -7191917762400439903L;
    private Integer id;
    private int version;
    private CustomerMigrationFailureType customerMigrationFailureType;
    private UserAccount userAccount;
    private CustomerMigrationStatusType customerMigrationStatusType;
    private CustomerMigrationValidationStatusType customerMigrationValidationStatusType;
    private Customer customer;
    private boolean isContacted;
    private Date customerBoardedGmt;
    private Date migrationRequestedGmt;
    private Date migrationScheduledGmt;
    private Date migrationStartGmt;
    private Date migrationEndGmt;
    private Date migrationCancelGmt;
    private byte failureCount;
    private boolean isMigrationInProgress;
    private Date startEmailSentGmt;
    private Date ems6CustomerLockedGmt;
    private Date dataMigrationConfirmedGmt;
    private Date ems7CustomerEnabledGmt;
    private Date heartbeatsResetGmt;
    private Date ems6ForwardFlagSetGmt;
    private Date completionEmailSentGmt;
    private Date paystationHeartbeatSetGmt;
    private Date communicationEmailSentGmt;
    private Date backGroundJobNextTryGmt;
    private int totalUserLoggedMinutes;
    private int communicatedPaystationCount;
    private int totalOnlinePaystationCount;
    private int totalPaystationCount;
    private Date allPayStationsCommunicatedGmt;
    private Date createdGmt;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    private Set<CustomerMigrationValidationStatus> customerMigrationValidationStatuses = new HashSet<CustomerMigrationValidationStatus>(0);
    private Set<CustomerMigrationEmail> customerMigrationEmails = new HashSet<CustomerMigrationEmail>(0);
    
    private boolean resendScheduleEmail;
    
    public CustomerMigration() {
        
    }
    
    public CustomerMigration(final CustomerMigrationStatusType customerMigrationStatusType, final Customer customer, final boolean isContacted,
            final byte failureCount, final boolean isMigrationInProgress, final int totalUserLoggedMinutes, final int communicatedPaystationCount,
            final int totalOnlinePaystationCount, final int totalPaystationCount, final Date createdGmt, final Date lastModifiedGmt,
            final int lastModifiedByUserId) {
        this.customerMigrationStatusType = customerMigrationStatusType;
        this.customer = customer;
        this.isContacted = isContacted;
        this.failureCount = failureCount;
        this.isMigrationInProgress = isMigrationInProgress;
        this.totalUserLoggedMinutes = totalUserLoggedMinutes;
        this.communicatedPaystationCount = communicatedPaystationCount;
        this.totalOnlinePaystationCount = totalOnlinePaystationCount;
        this.totalPaystationCount = totalPaystationCount;
        this.createdGmt = createdGmt;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @Version
    @Column(name = "VERSION", nullable = false)
    public int getVersion() {
        return this.version;
    }
    
    public void setVersion(final int version) {
        this.version = version;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerMigrationFailureTypeId")
    public CustomerMigrationFailureType getCustomerMigrationFailureType() {
        return this.customerMigrationFailureType;
    }
    
    public void setCustomerMigrationFailureType(final CustomerMigrationFailureType customerMigrationFailureType) {
        this.customerMigrationFailureType = customerMigrationFailureType;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MigrationAdminUserId")
    public UserAccount getUserAccount() {
        return this.userAccount;
    }
    
    public void setUserAccount(final UserAccount userAccount) {
        this.userAccount = userAccount;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerMigrationStatusTypeId", nullable = false)
    public CustomerMigrationStatusType getCustomerMigrationStatusType() {
        return this.customerMigrationStatusType;
    }
    
    public void setCustomerMigrationStatusType(final CustomerMigrationStatusType customerMigrationStatusType) {
        this.customerMigrationStatusType = customerMigrationStatusType;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerMigrationValidationStatusTypeId", nullable = false)
    public CustomerMigrationValidationStatusType getCustomerMigrationValidationStatusType() {
        return this.customerMigrationValidationStatusType;
    }
    
    public void setCustomerMigrationValidationStatusType(final CustomerMigrationValidationStatusType customerMigrationValidationStatusType) {
        this.customerMigrationValidationStatusType = customerMigrationValidationStatusType;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerId", unique = true, nullable = false)
    public Customer getCustomer() {
        return this.customer;
    }
    
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }
    
    @Column(name = "IsContacted", nullable = false)
    public boolean isIsContacted() {
        return this.isContacted;
    }
    
    public void setIsContacted(final boolean isContacted) {
        this.isContacted = isContacted;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CustomerBoardedGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getCustomerBoardedGmt() {
        return this.customerBoardedGmt;
    }
    
    public void setCustomerBoardedGmt(final Date customerBoardedGmt) {
        this.customerBoardedGmt = customerBoardedGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "MigrationRequestedGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getMigrationRequestedGmt() {
        return this.migrationRequestedGmt;
    }
    
    public void setMigrationRequestedGmt(final Date migrationRequestedGmt) {
        this.migrationRequestedGmt = migrationRequestedGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "MigrationScheduledGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getMigrationScheduledGmt() {
        return this.migrationScheduledGmt;
    }
    
    public void setMigrationScheduledGmt(final Date migrationScheduledGmt) {
        this.migrationScheduledGmt = migrationScheduledGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "MigrationStartGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getMigrationStartGmt() {
        return this.migrationStartGmt;
    }
    
    public void setMigrationStartGmt(final Date migrationStartGmt) {
        this.migrationStartGmt = migrationStartGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "MigrationEndGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getMigrationEndGmt() {
        return this.migrationEndGmt;
    }
    
    public void setMigrationEndGmt(final Date migrationEndGmt) {
        this.migrationEndGmt = migrationEndGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "MigrationCancelGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getMigrationCancelGmt() {
        return this.migrationCancelGmt;
    }
    
    public void setMigrationCancelGmt(final Date migrationCancelGmt) {
        this.migrationCancelGmt = migrationCancelGmt;
    }
    
    @Column(name = "FailureCount", nullable = false)
    public byte getFailureCount() {
        return this.failureCount;
    }
    
    public void setFailureCount(final byte failureCount) {
        this.failureCount = failureCount;
    }
    
    @Column(name = "IsMigrationInProgress", nullable = false)
    public boolean isIsMigrationInProgress() {
        return this.isMigrationInProgress;
    }
    
    public void setIsMigrationInProgress(final boolean isMigrationInProgress) {
        this.isMigrationInProgress = isMigrationInProgress;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "StartEmailSentGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getStartEmailSentGmt() {
        return this.startEmailSentGmt;
    }
    
    public void setStartEmailSentGmt(final Date startEmailSentGmt) {
        this.startEmailSentGmt = startEmailSentGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Ems6CustomerLockedGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getEms6CustomerLockedGmt() {
        return this.ems6CustomerLockedGmt;
    }
    
    public void setEms6CustomerLockedGmt(final Date ems6CustomerLockedGmt) {
        this.ems6CustomerLockedGmt = ems6CustomerLockedGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DataMigrationConfirmedGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getDataMigrationConfirmedGmt() {
        return this.dataMigrationConfirmedGmt;
    }
    
    public void setDataMigrationConfirmedGmt(final Date dataMigrationConfirmedGmt) {
        this.dataMigrationConfirmedGmt = dataMigrationConfirmedGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "EMs7CustomerEnabledGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getEms7CustomerEnabledGmt() {
        return this.ems7CustomerEnabledGmt;
    }
    
    public void setEms7CustomerEnabledGmt(final Date ems7CustomerEnabledGmt) {
        this.ems7CustomerEnabledGmt = ems7CustomerEnabledGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "HeartbeatsResetGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getHeartbeatsResetGmt() {
        return this.heartbeatsResetGmt;
    }
    
    public void setHeartbeatsResetGmt(final Date heartbeatsResetGmt) {
        this.heartbeatsResetGmt = heartbeatsResetGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "EMs6ForwardFlagSetGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getEms6ForwardFlagSetGmt() {
        return this.ems6ForwardFlagSetGmt;
    }
    
    public void setEms6ForwardFlagSetGmt(final Date ems6ForwardFlagSetGmt) {
        this.ems6ForwardFlagSetGmt = ems6ForwardFlagSetGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CompletionEmailSentGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getCompletionEmailSentGmt() {
        return this.completionEmailSentGmt;
    }
    
    public void setCompletionEmailSentGmt(final Date completionEmailSentGmt) {
        this.completionEmailSentGmt = completionEmailSentGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "PaystationHeartbeatSetGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getPaystationHeartbeatSetGmt() {
        return this.paystationHeartbeatSetGmt;
    }
    
    public void setPaystationHeartbeatSetGmt(final Date paystationHeartbeatSetGmt) {
        this.paystationHeartbeatSetGmt = paystationHeartbeatSetGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CommunicationEmailSentGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getCommunicationEmailSentGmt() {
        return this.communicationEmailSentGmt;
    }
    
    public void setCommunicationEmailSentGmt(final Date communicationEmailSentGmt) {
        this.communicationEmailSentGmt = communicationEmailSentGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "BackGroundJobNextTryGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getBackGroundJobNextTryGmt() {
        return this.backGroundJobNextTryGmt;
    }
    
    public void setBackGroundJobNextTryGmt(final Date backGroundJobNextTryGmt) {
        this.backGroundJobNextTryGmt = backGroundJobNextTryGmt;
    }
    
    @Column(name = "TotalUserLoggedMinutes", nullable = false)
    public int getTotalUserLoggedMinutes() {
        return this.totalUserLoggedMinutes;
    }
    
    public void setTotalUserLoggedMinutes(final int totalUserLoggedMinutes) {
        this.totalUserLoggedMinutes = totalUserLoggedMinutes;
    }
    
    @Column(name = "CommunicatedPaystationCount", nullable = false)
    public int getCommunicatedPaystationCount() {
        return this.communicatedPaystationCount;
    }
    
    public void setCommunicatedPaystationCount(final int communicatedPaystationCount) {
        this.communicatedPaystationCount = communicatedPaystationCount;
    }
    
    @Column(name = "TotalOnlinePaystationCount", nullable = false)
    public int getTotalOnlinePaystationCount() {
        return this.totalOnlinePaystationCount;
    }
    
    public void setTotalOnlinePaystationCount(final int totalOnlinePaystationCount) {
        this.totalOnlinePaystationCount = totalOnlinePaystationCount;
    }
    
    @Column(name = "TotalPaystationCount", nullable = false)
    public int getTotalPaystationCount() {
        return this.totalPaystationCount;
    }
    
    public void setTotalPaystationCount(final int totalPaystationCount) {
        this.totalPaystationCount = totalPaystationCount;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "AllPayStationsCommunicatedGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getAllPayStationsCommunicatedGmt() {
        return this.allPayStationsCommunicatedGmt;
    }
    
    public void setAllPayStationsCommunicatedGmt(final Date allPayStationsCommunicatedGmt) {
        this.allPayStationsCommunicatedGmt = allPayStationsCommunicatedGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreatedGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getCreatedGmt() {
        return this.createdGmt;
    }
    
    public void setCreatedGmt(final Date createdGmt) {
        this.createdGmt = createdGmt;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }
    
    public void setLastModifiedGmt(final Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }
    
    public void setLastModifiedByUserId(final int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customerMigration")
    public Set<CustomerMigrationValidationStatus> getCustomerMigrationValidationStatuses() {
        return this.customerMigrationValidationStatuses;
    }
    
    public void setCustomerMigrationValidationStatuses(final Set<CustomerMigrationValidationStatus> customerMigrationValidationStatuses) {
        this.customerMigrationValidationStatuses = customerMigrationValidationStatuses;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customerMigration")
    public Set<CustomerMigrationEmail> getCustomerMigrationEmails() {
        return this.customerMigrationEmails;
    }
    
    public void setCustomerMigrationEmails(final Set<CustomerMigrationEmail> customerMigrationEmails) {
        this.customerMigrationEmails = customerMigrationEmails;
    }
    
    @Transient
    public boolean getResendScheduleEmail() {
        return this.resendScheduleEmail;
    }
    
    @Transient
    public void setResendScheduleEmail(final boolean resendScheduleEmail) {
        this.resendScheduleEmail = resendScheduleEmail;
    }
    
}
