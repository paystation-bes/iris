package com.digitalpaytech.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.annotation.StorageType;
import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryLookup;
import com.digitalpaytech.domain.util.HibernateConstants;

@Entity
@Table(name = "TransactionFileUpload")
@NamedQueries({
    @NamedQuery(name = "TransactionFileUpload.findByCustomerIdFileNameAndChecksum", query = "FROM TransactionFileUpload tfu WHERE tfu.customer.id = :customerId AND tfu.fileName = :fileName AND tfu.checksum = :checksum")
    })
@StoryAlias(value = "File")
@SuppressWarnings({"checkstyle:designforextension"})
public class TransactionFileUpload implements Serializable {
    public static final String ALIAS_FILE_NAME = "File Name";
    private static final long serialVersionUID = 7657127847422852848L;
    private Integer id;
    private Customer customer;
    private String fileName;
    private String checksum;
    private Date uploadedGmt;
    private int noOfUploadAttempt;
    
    public TransactionFileUpload() {
    }
    
     
    public TransactionFileUpload(final Customer customer, 
                                 final String fileName, 
                                 final String checksum, 
                                 final Date uploadedGmt, 
                                 final int noOfUploadAttempt) {
        this.customer = customer;
        this.fileName = fileName;
        this.checksum = checksum;
        this.uploadedGmt = uploadedGmt;
        this.noOfUploadAttempt = noOfUploadAttempt;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerId", nullable = false)
    @StoryAlias(value = "File owner")
    @StoryLookup(type = "customer", searchAttribute = "customer name", storage = StorageType.DB)
    public Customer getCustomer() {
        return this.customer;
    }
    
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }
    
    @Column(name = "FileName", nullable = false)
    @StoryAlias(value = ALIAS_FILE_NAME)
    public String getFileName() {
        return this.fileName;
    }
    
    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }
    
    @Column(name = "CheckSum", nullable = false)
    public String getChecksum() {
        return this.checksum;
    }
    
    public void setChecksum(final String checksum) {
        this.checksum = checksum;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UploadedGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)    
    public Date getUploadedGmt() {
        return this.uploadedGmt;
    }
    
    public void setUploadedGmt(final Date uploadedGmt) {
        this.uploadedGmt = uploadedGmt;
    }
    
    @Column(name = "NoOfUploadAttempt", nullable = false)
    public int getNoOfUploadAttempt() {
        return this.noOfUploadAttempt;
    }
    
    public void setNoOfUploadAttempt(final int noOfUploadAttempt) {
        this.noOfUploadAttempt = noOfUploadAttempt;
    }
}
