package com.digitalpaytech.domain;

// Generated 31-Jul-2014 3:27:13 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 * Rateaddtime generated by hbm2java
 */
@Entity
@Table(name = "RateAddTime")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class RateAddTime implements java.io.Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -2791403029631175149L;
    private int rateId;
    private int version;
    private Rate rate;
    private boolean isAddTimeNumber;
    private boolean isSpaceOrPlateNumber;
    private boolean isEbP;
    private Integer serviceFeeAmount;
    private Short minExtention;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    
    public RateAddTime() {
    }
    
    public RateAddTime(Rate rate, boolean isAddTimeNumber, boolean isSpaceOrPlateNumber, boolean isEbP, Date lastModifiedGmt, int lastModifiedByUserId) {
        this.rate = rate;
        this.isAddTimeNumber = isAddTimeNumber;
        this.isSpaceOrPlateNumber = isSpaceOrPlateNumber;
        this.isEbP = isEbP;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    public RateAddTime(Rate rate, boolean isAddTimeNumber, boolean isSpaceOrPlateNumber, boolean isEbP, Integer serviceFeeAmount, Short minExtention,
            Date lastModifiedGmt, int lastModifiedByUserId) {
        this.rate = rate;
        this.isAddTimeNumber = isAddTimeNumber;
        this.isSpaceOrPlateNumber = isSpaceOrPlateNumber;
        this.isEbP = isEbP;
        this.serviceFeeAmount = serviceFeeAmount;
        this.minExtention = minExtention;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @GenericGenerator(name = "generator", strategy = "foreign", parameters = @Parameter(name = "property", value = "rate"))
    @Id
    @GeneratedValue(generator = "generator")
    @Column(name = "RateId", nullable = false)
    public int getRateId() {
        return this.rateId;
    }
    
    public void setRateId(int rateId) {
        this.rateId = rateId;
    }
    
    @Version
    @Column(name = "VERSION", nullable = false)
    public int getVersion() {
        return this.version;
    }
    
    public void setVersion(int version) {
        this.version = version;
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public Rate getRate() {
        return this.rate;
    }
    
    public void setRate(Rate rate) {
        this.rate = rate;
    }
    
    @Column(name = "IsAddTimeNumber", nullable = false)
    public boolean isIsAddTimeNumber() {
        return this.isAddTimeNumber;
    }
    
    public void setIsAddTimeNumber(boolean isAddTimeNumber) {
        this.isAddTimeNumber = isAddTimeNumber;
    }
    
    @Column(name = "IsSpaceOrPlateNumber", nullable = false)
    public boolean isIsSpaceOrPlateNumber() {
        return this.isSpaceOrPlateNumber;
    }
    
    public void setIsSpaceOrPlateNumber(boolean isSpaceOrPlateNumber) {
        this.isSpaceOrPlateNumber = isSpaceOrPlateNumber;
    }
    
    @Column(name = "IsEbP", nullable = false)
    public boolean isIsEbP() {
        return this.isEbP;
    }
    
    public void setIsEbP(boolean isEbP) {
        this.isEbP = isEbP;
    }
    
    @Column(name = "ServiceFeeAmount")
    public Integer getServiceFeeAmount() {
        return this.serviceFeeAmount;
    }
    
    public void setServiceFeeAmount(Integer serviceFeeAmount) {
        this.serviceFeeAmount = serviceFeeAmount;
    }
    
    @Column(name = "MinExtention")
    public Short getMinExtention() {
        return this.minExtention;
    }
    
    public void setMinExtention(Short minExtention) {
        this.minExtention = minExtention;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = 19)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }
    
    public void setLastModifiedGmt(Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }
    
    public void setLastModifiedByUserId(int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
}
