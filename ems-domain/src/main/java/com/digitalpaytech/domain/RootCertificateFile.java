package com.digitalpaytech.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.domain.util.HibernateConstants;

@Entity
@Table(name = "RootCertificateFile")
@NamedQueries({
    @NamedQuery(name = "RootCertificateFile.findByMaxId", query = "FROM RootCertificateFile rcf WHERE rcf.id = (SELECT MAX(r.id) FROM RootCertificateFile r)") })
public class RootCertificateFile {
    private Integer id;
    private String linuxHash;
    private String winCEHash;
    private Date createdGMT;
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @Column(name = "LinuxHash", nullable = true, length = HibernateConstants.VARCHAR_LENGTH_32)
    public String getLinuxHash() {
        return this.linuxHash;
    }
    
    public void setLinuxHash(final String linuxHash) {
        this.linuxHash = linuxHash;
    }
    
    @Column(name = "WinCEHash", nullable = true, length = HibernateConstants.VARCHAR_LENGTH_32)
    public String getWinCEHash() {
        return this.winCEHash;
    }
    
    public void setWinCEHash(final String winCEHash) {
        this.winCEHash = winCEHash;
    }
    
    @Column(name = "CreatedGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getCreatedGMT() {
        return this.createdGMT;
    }
    
    public void setCreatedGMT(final Date createdGMT) {
        this.createdGMT = createdGMT;
    }
    
}
