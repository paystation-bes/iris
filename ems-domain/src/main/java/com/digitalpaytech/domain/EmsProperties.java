package com.digitalpaytech.domain;

// Generated 18-Apr-2012 2:22:51 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

/**
 * Emsproperties generated by hbm2java
 */
@Entity
@Table(name = "EmsProperties")
@NamedQueries({
    @NamedQuery(name = "EmsProperties.findAll", query = "from EmsProperties order by name"),
    @NamedQuery(name = "EmsProperties.findByName", query = "from EmsProperties where name = :name")})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EmsProperties implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -8222326575275451997L;
    private int id;
    private UserAccount userAccount;
    private String name;
    private String value;
    private Date lastModifiedGmt;

    public EmsProperties() {
    }

    public EmsProperties(String name, String value) {
        this.name = name;
        this.value = value;
    }   
    
    public EmsProperties(UserAccount userAccount, String name, String value,
            Date lastModifiedGmt) {
        this.userAccount = userAccount;
        this.name = name;
        this.value = value;
        this.lastModifiedGmt = lastModifiedGmt;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LastModifiedByUserId", nullable = false)
    public UserAccount getUserAccount() {
        return this.userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    @Column(name = "Name", unique = true, nullable = false, length = 40)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "Value", nullable = false)
    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = 19)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }

    public void setLastModifiedGmt(Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }

}
