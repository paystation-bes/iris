package com.digitalpaytech.domain;

// Generated 31-Jul-2014 3:27:13 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * RateType generated by hbm2java
 */
@Entity
@Table(name = "RateType")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class RateType implements java.io.Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -4550794890704384941L;
    private byte id;
    private String name;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    private Set<Rate> rates = new HashSet<Rate>(0);
    
    public RateType() {
    }
    
    public RateType(byte id, String name, Date lastModifiedGmt, int lastModifiedByUserId) {
        this.id = id;
        this.name = name;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    public RateType(byte id, String name, Date lastModifiedGmt, int lastModifiedByUserId, Set<Rate> rates) {
        this.id = id;
        this.name = name;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
        this.rates = rates;
    }
    
    @Id
    @Column(name = "Id", nullable = false)
    public byte getId() {
        return this.id;
    }
    
    public void setId(byte id) {
        this.id = id;
    }
    
    @Column(name = "Name", unique = true, nullable = false, length = 30)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = 19)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }
    
    public void setLastModifiedGmt(Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }
    
    public void setLastModifiedByUserId(int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "rateType")
    public Set<Rate> getRates() {
        return this.rates;
    }
    
    public void setRates(Set<Rate> rates) {
        this.rates = rates;
    }
    
}
