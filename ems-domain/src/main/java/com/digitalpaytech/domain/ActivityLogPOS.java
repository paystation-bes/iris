package com.digitalpaytech.domain;

import static javax.persistence.GenerationType.IDENTITY;

// Generated 18-Apr-2012 2:22:51 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Activitylogpos generated by hbm2java
 */
@Entity
@Table(name = "ActivityLogPOS")
public class ActivityLogPOS implements java.io.Serializable {
    
    private static final long serialVersionUID = 1920535658721568968L;
    
    private Long id;
    private ActivityLog activityLog;
    private PointOfSale pointOfSale;

    public ActivityLogPOS() {
    }

    public ActivityLogPOS(final ActivityLog activityLog, final PointOfSale pointOfSale) {
        this.activityLog = activityLog;
        this.pointOfSale = pointOfSale;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "UserActivityId", nullable = false)
    public ActivityLog getActivityLog() {
        return this.activityLog;
    }

    public void setActivityLog(final ActivityLog activityLog) {
        this.activityLog = activityLog;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PointOfSaleId", nullable = false)
    public PointOfSale getPointOfSale() {
        return this.pointOfSale;
    }

    public void setPointOfSale(final PointOfSale pointOfSale) {
        this.pointOfSale = pointOfSale;
    }

}
