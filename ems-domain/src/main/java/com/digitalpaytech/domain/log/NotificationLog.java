package com.digitalpaytech.domain.log;

// Generated 18-Apr-2012 2:22:51 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import com.digitalpaytech.domain.ChangeType;

/**
 * NotificationLog generated by hbm2java
 */
@Entity
@Table(name = "Notification_Log")
public class NotificationLog implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6833494628178853352L;
	private Integer logId;
	private int version;
	private int id;
	private String title;
	private String message;
	private String messageUrl;
	private Date beginGmt;
	private Date endGmt;
	private Date lastModifiedGmt;
	private ChangeType changeType;
	
	public NotificationLog() {
	}

	public NotificationLog(int id, String message, String title,
			Date beginGmt, Date endGmt, Date lastModifiedGmt) {
		this.id = id;
		this.title = title;
		this.message = message;
		this.beginGmt = beginGmt;
		this.endGmt = endGmt;
		this.lastModifiedGmt = lastModifiedGmt;
	}


	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "LogId", nullable = false)
	public Integer getLogId() {
		return this.logId;
	}

	public void setLogId(Integer logId) {
		this.logId = logId;
	}

	@Version
	@Column(name = "VERSION", nullable = false)
	public int getVersion() {
		return this.version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Column(name = "Id", nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "Message", nullable = false)
	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Column(name = "MessageURL")
	public String getMessageUrl() {
		return this.messageUrl;
	}

	public void setMessageUrl(String messageUrl) {
		this.messageUrl = messageUrl;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "BeginGMT", nullable = false, length = 19)
	public Date getBeginGmt() {
		return this.beginGmt;
	}

	public void setBeginGmt(Date beginGmt) {
		this.beginGmt = beginGmt;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EndGMT", nullable = false, length = 19)
	public Date getEndGmt() {
		return this.endGmt;
	}

	public void setEndGmt(Date endGmt) {
		this.endGmt = endGmt;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LastModifiedGMT", nullable = false, length = 19)
	public Date getLastModifiedGmt() {
		return this.lastModifiedGmt;
	}

	public void setLastModifiedGmt(Date lastModifiedGmt) {
		this.lastModifiedGmt = lastModifiedGmt;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ChangeTypeId", nullable = false)
	public ChangeType getChangeType() {
		return this.changeType;
	}

	public void setChangeType(ChangeType changeType) {
		this.changeType = changeType;
	}

	@Column(name = "Title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}	
}
