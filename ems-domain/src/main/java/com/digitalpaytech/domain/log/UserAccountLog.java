package com.digitalpaytech.domain.log;

// Generated 9-Jul-2012 4:01:17 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.domain.ChangeType;

/**
 * UseraccountLog generated by hbm2java
 */
@Entity
@Table(name = "useraccount_log")
@NamedQuery(name = "UserAccountLog.getUserAccountLogByLogId", query = "from UserAccountLog log where log.id = :logId")
public class UserAccountLog implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4741976348975208033L;
	private Integer logId;
	private int version;
	private ChangeType changeType;
	private int id;
	private int customerId;
	private int userStatusTypeId;
	private String userName;
	private String email;
	private String firstName;
	private String lastName;
	private String password;
	private String passwordSalt;
	private boolean isPasswordTemporary;
	private Date lastModifiedGmt;
	private int lastModifiedByUserId;
	private boolean isDeleted;

	public UserAccountLog() {
	}

	public UserAccountLog(ChangeType changeType, int id, int customerId,
			int userStatusTypeId, String userName, String firstName,
			String lastName, String password, String passwordSalt,
			boolean isPasswordTemporary, Date lastModifiedGmt,
			int lastModifiedByUserId, boolean isDeleted) {
		this.changeType = changeType;
		this.id = id;
		this.customerId = customerId;
		this.userStatusTypeId = userStatusTypeId;
		this.userName = userName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.passwordSalt = passwordSalt;
		this.isPasswordTemporary = isPasswordTemporary;
		this.lastModifiedGmt = lastModifiedGmt;
		this.lastModifiedByUserId = lastModifiedByUserId;
		this.isDeleted = isDeleted;
	}

	public UserAccountLog(ChangeType changeType, int id, int customerId,
			int userStatusTypeId, String userName, String email,
			String firstName, String lastName, String password,
			String passwordSalt, boolean isPasswordTemporary,
			Date lastModifiedGmt, int lastModifiedByUserId, boolean isDeleted) {
		this.changeType = changeType;
		this.id = id;
		this.customerId = customerId;
		this.userStatusTypeId = userStatusTypeId;
		this.userName = userName;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.password = password;
		this.passwordSalt = passwordSalt;
		this.isPasswordTemporary = isPasswordTemporary;
		this.lastModifiedGmt = lastModifiedGmt;
		this.lastModifiedByUserId = lastModifiedByUserId;
		this.isDeleted = isDeleted;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "LogId", nullable = false)
	public Integer getLogId() {
		return this.logId;
	}

	public void setLogId(Integer logId) {
		this.logId = logId;
	}

	@Version
	@Column(name = "VERSION", nullable = false)
	public int getVersion() {
		return this.version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ChangeTypeId", nullable = false)
	public ChangeType getChangeType() {
		return this.changeType;
	}

	public void setChangeType(ChangeType changeType) {
		this.changeType = changeType;
	}

	@Column(name = "Id", nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "CustomerId", nullable = false)
	public int getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	@Column(name = "UserStatusTypeId", nullable = false)
	public int getUserStatusTypeId() {
		return this.userStatusTypeId;
	}

	public void setUserStatusTypeId(int userStatusTypeId) {
		this.userStatusTypeId = userStatusTypeId;
	}

	@Column(name = "UserName", nullable = false, length = 765)
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "Email", length = 340)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "FirstName", nullable = false, length = 75)
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "LastName", nullable = false, length = 75)
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "Password", nullable = false, length = 128)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "PasswordSalt", nullable = false, length = 8)
	public String getPasswordSalt() {
		return this.passwordSalt;
	}

	public void setPasswordSalt(String passwordSalt) {
		this.passwordSalt = passwordSalt;
	}

	@Column(name = "IsPasswordTemporary", nullable = false)
	public boolean isIsPasswordTemporary() {
		return this.isPasswordTemporary;
	}

	public void setIsPasswordTemporary(boolean isPasswordTemporary) {
		this.isPasswordTemporary = isPasswordTemporary;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LastModifiedGMT", nullable = false, length = 19)
	public Date getLastModifiedGmt() {
		return this.lastModifiedGmt;
	}

	public void setLastModifiedGmt(Date lastModifiedGmt) {
		this.lastModifiedGmt = lastModifiedGmt;
	}

	@Column(name = "LastModifiedByUserId", nullable = false)
	public int getLastModifiedByUserId() {
		return this.lastModifiedByUserId;
	}

	public void setLastModifiedByUserId(int lastModifiedByUserId) {
		this.lastModifiedByUserId = lastModifiedByUserId;
	}

	@Column(name = "IsDeleted", nullable = false)
	public boolean isIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
}
