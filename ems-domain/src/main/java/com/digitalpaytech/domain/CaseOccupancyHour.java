package com.digitalpaytech.domain;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@Entity
@Table(name = "CaseOccupancyHour")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({
    @NamedQuery(name = "CaseOccupancyHour.findCaseOccupancyHourByLocationIdAndTimeIdGMT", query = "FROM CaseOccupancyHour coh WHERE coh.location.id = :locationId AND coh.timeIdGmt = :timeId", cacheable = true),
    @NamedQuery(name = "CaseOccupancyHour.findLatestCaseOccupancyHourTimeIdGMTByLocationId", query = "SELECT MAX(coh.timeIdGmt) FROM CaseOccupancyHour coh WHERE coh.location.id = :locationId", cacheable = true),
    @NamedQuery(name = "CaseOccupancyHour.findLatestCaseOccupancyHourTimeIdLocalByLocationId", query = "SELECT MAX(coh.timeIdLocal) FROM CaseOccupancyHour coh WHERE coh.location.id = :locationId", cacheable = true) })
@SuppressWarnings({ "checkstyle:designforextension" })
public class CaseOccupancyHour {
    
    private Long id;
    private int timeIdLocal;
    private Customer customer;
    private int timeIdGmt;
    private int numberOfSpaces;
    private Location location;
    private int numOfPermits;
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Long getId() {
        return this.id;
    }
    
    public void setId(final Long id) {
        this.id = id;
    }
    
    @Column(name = "TimeIdLocal", nullable = false)
    public int getTimeIdLocal() {
        return this.timeIdLocal;
    }
    
    public void setTimeIdLocal(final int timeIdLocal) {
        this.timeIdLocal = timeIdLocal;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerId", nullable = false)
    public Customer getCustomer() {
        return this.customer;
    }
    
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }
    
    @Column(name = "TimeIdGMT", nullable = false)
    public int getTimeIdGmt() {
        return this.timeIdGmt;
    }
    
    public void setTimeIdGmt(final int timeIdGmt) {
        this.timeIdGmt = timeIdGmt;
    }
    
    @Column(name = "NumberOfSpaces", nullable = false)
    public int getNumberOfSpaces() {
        return this.numberOfSpaces;
    }
    
    public void setNumberOfSpaces(final int numberOfSpaces) {
        this.numberOfSpaces = numberOfSpaces;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LocationId", nullable = false)
    public Location getLocation() {
        return this.location;
    }
    
    public void setLocation(final Location location) {
        this.location = location;
    }
    
    //    @Column(name = "NoOfPermits", nullable = false)
    //    public int getNumOfPermits() {
    //        return this.numOfPermits;
    //    }
    @Column(name = "NoOfPermits", nullable = false)
    public int getNumOfPermits() {
        return this.numOfPermits;
    }
    
    //    public void setNumOfPermits(final int numOfPermits) {
    //        this.numOfPermits = numOfPermits;
    //    }
    public void setNumOfPermits(final int numOfPermits) {
        this.numOfPermits = numOfPermits;
    }
}
