package com.digitalpaytech.domain;

// Generated 31-Aug-2012 2:05:49 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryId;
import com.digitalpaytech.domain.util.HibernateConstants;

/**
 * CustomerEmail generated by hbm2java
 */
@Entity
@Table(name = "CustomerEmail")
@NamedQueries({ @NamedQuery(name = "CustomerEmail.findCustomerEmailByCustomerIdAndEmail", query = "from CustomerEmail ce where ce.customer.id = :customerId and ce.email in(:email)") })
@SuppressWarnings({ "checkstyle:designforextension" })
@StoryAlias(CustomerEmail.ALIAS)
public class CustomerEmail implements java.io.Serializable {
    
    public static final String ALIAS = "customer email";
    public static final String ALIAS_EMAIL = "email";
    
    /**
     * 
     */
    private static final long serialVersionUID = 1736705126894867678L;
    private Integer id;
    private int version;
    private boolean isDeleted;
    private Customer customer;
    private String email;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    
    //private Set<CustomerAlertEmail> alertEmails = new HashSet<CustomerAlertEmail>(0);
    
    public CustomerEmail() {
        super();
    }
    
    public CustomerEmail(final Customer customer, final String email) {
        this.customer = customer;
        this.email = email;
    }
    
    public CustomerEmail(final Customer customer, final String email, final Date lastModifiedGmt, final int lastModifiedByUserId,
            final boolean isDeleted) {
        this.customer = customer;
        this.email = email;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
        this.isDeleted = isDeleted;
    }
    
    /*
     * public CustomerEmail(Customer customer, String email, Date lastModifiedGmt,
     * int lastModifiedByUserId, Set<CustomerAlertEmail> alertEmails,
     * boolean isDeleted) {
     * this.customer = customer;
     * this.email = email;
     * this.lastModifiedGmt = lastModifiedGmt;
     * this.lastModifiedByUserId = lastModifiedByUserId;
     * // this.alertEmails = alertEmails;
     * this.isDeleted = isDeleted;
     * }
     */
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    @StoryId
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @Version
    @Column(name = "VERSION", nullable = false)
    public int getVersion() {
        return this.version;
    }
    
    public void setVersion(final int version) {
        this.version = version;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerId", nullable = false)
    public Customer getCustomer() {
        return this.customer;
    }
    
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }
    
    @Column(name = "Email", unique = true, nullable = false, length = HibernateConstants.VARCHAR_LENGTH_340)
    @StoryAlias(ALIAS_EMAIL)
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(final String email) {
        this.email = email;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }
    
    public void setLastModifiedGmt(final Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }
    
    public void setLastModifiedByUserId(final int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    /*
     * @OneToMany(fetch = FetchType.LAZY, mappedBy = "customerEmail")
     * public Set<CustomerAlertEmail> getAlertemails() {
     * return this.alertEmails;
     * }
     * public void setAlertemails(Set<CustomerAlertEmail> alertEmails) {
     * this.alertEmails = alertEmails;
     * }
     */
    
    @Column(name = "IsDeleted", nullable = false)
    public boolean isIsDeleted() {
        return this.isDeleted;
    }
    
    public void setIsDeleted(final boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
    
}
