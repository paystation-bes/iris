package com.digitalpaytech.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the BatchStatusType database table.
 * 
 */
@Entity
@Table(name="BatchStatusType")
public class BatchStatusType implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -4252141297496357517L;
    private Integer id;
    private int lastModifiedByUserId;
    private Date lastModifiedGMT;
    private String name;
    private List<ActiveBatchSummary> activebatchsummaries;

    public BatchStatusType() {
    }


    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    @Column(nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }

    public void setLastModifiedByUserId(int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }


    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    public Date getLastModifiedGMT() {
        return this.lastModifiedGMT;
    }

    public void setLastModifiedGMT(Date lastModifiedGMT) {
        this.lastModifiedGMT = lastModifiedGMT;
    }


    @Column(nullable = false, length=30)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }


    //bi-directional many-to-one association to Activebatchsummary
    @OneToMany(mappedBy="batchstatustype")
    public List<ActiveBatchSummary> getActivebatchsummaries() {
        return this.activebatchsummaries;
    }

    public void setActivebatchsummaries(List<ActiveBatchSummary> activebatchsummaries) {
        this.activebatchsummaries = activebatchsummaries;
    }

    public ActiveBatchSummary addActivebatchsummary(ActiveBatchSummary activebatchsummary) {
        getActivebatchsummaries().add(activebatchsummary);
        activebatchsummary.setBatchstatustype(this);
        
        return activebatchsummary;
    }

    public ActiveBatchSummary removeActivebatchsummary(ActiveBatchSummary activebatchsummary) {
        getActivebatchsummaries().remove(activebatchsummary);
        activebatchsummary.setBatchstatustype(null);
        
        return activebatchsummary;
    }

}