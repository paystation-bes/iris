package com.digitalpaytech.domain.emvrefund;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

@Entity
@Table(name = "EMVRefund")
@NamedQueries({
    @NamedQuery(name = "EMVRefund.findByEarliestLastRetryDate", query = "FROM EMVRefund emvr WHERE emvr.numRetries < :maxNumRetries ORDER BY emvr.lastRetryDate ASC ") 
})
@SuppressWarnings({ "checkstyle:designforextension" })
public class EMVRefund implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -3253977577155545028L;
    private Integer id;
    private String serialNumber;
    private Date processedDate;
    private String cardTransactionJson;
    /** cardEaseData.cardReference */
    private String cardEaseReference;
    /** cardEaseData.ref */
    private String processorTransactionId;
    /** cardEaseData.authCode */
    private String cardAuthorizationId;
    private int amount;
    private String chargeToken;
    private String terminalToken;
    private String cardType;
    private String referenceNumber;
    private short last4DigitsOfCardNumber;
    private int numRetries;
    private String lastResponse;
    private Date creationDate;
    private Date lastRetryDate;
    
    public EMVRefund() {
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }

    @Column(name = "SerialNumber", nullable = false)
    public String getSerialNumber() {
        return this.serialNumber;
    }

    public void setSerialNumber(final String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ProcessedDate", nullable = false, length = 19)
    public Date getProcessedDate() {
        return this.processedDate;
    }

    public void setProcessedDate(final Date processedDate) {
        this.processedDate = processedDate;
    }

    @Column(name = "CardTransaction", nullable = false)
    public String getCardTransactionJson() {
        return this.cardTransactionJson;
    }

    public void setCardTransactionJson(final String cardTransactionJson) {
        this.cardTransactionJson = cardTransactionJson;
    }

    @Column(name = "CardEaseReference", nullable = false)
    public String getCardEaseReference() {
        return this.cardEaseReference;
    }

    public void setCardEaseReference(final String cardEaseReference) {
        this.cardEaseReference = cardEaseReference;
    }
    
    @Column(name = "Amount", nullable = false)
    public int getAmount() {
        return this.amount;
    }

    public void setAmount(final int amount) {
        this.amount = amount;
    }

    @Column(name = "ChargeToken")
    public String getChargeToken() {
        return this.chargeToken;
    }

    public void setChargeToken(final String chargeToken) {
        this.chargeToken = chargeToken;
    }

    @Column(name = "TerminalToken", nullable = false)
    public String getTerminalToken() {
        return this.terminalToken;
    }

    public void setTerminalToken(final String terminalToken) {
        this.terminalToken = terminalToken;
    }

    @Column(name = "CardType", nullable = false)
    public String getCardType() {
        return this.cardType;
    }

    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }

    @Column(name = "Last4DigitsOfCardNumber", nullable = false)
    public short getLast4DigitsOfCardNumber() {
        return this.last4DigitsOfCardNumber;
    }

    public void setLast4DigitsOfCardNumber(final short last4DigitsOfCardNumber) {
        this.last4DigitsOfCardNumber = last4DigitsOfCardNumber;
    }

    @Column(name = "ProcessorTransactionId", nullable = false)
    public String getProcessorTransactionId() {
        return this.processorTransactionId;
    }

    public void setProcessorTransactionId(final String processorTransactionId) {
        this.processorTransactionId = processorTransactionId;
    }

    @Column(name = "CardAuthorizationId", nullable = false)
    public String getCardAuthorizationId() {
        return this.cardAuthorizationId;
    }

    public void setCardAuthorizationId(final String cardAuthorizationId) {
        this.cardAuthorizationId = cardAuthorizationId;
    }

    @Column(name = "ReferenceNumber", nullable = false)
    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(final String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }
    
    @Column(name = "NumRetries", nullable = false)
    public int getNumRetries() {
        return this.numRetries;
    }

    public void setNumRetries(final int numRetries) {
        this.numRetries = numRetries;
    }

    @Column(name = "LastResponse", nullable = false)
    public String getLastResponse() {
        return this.lastResponse;
    }

    public void setLastResponse(final String lastResponse) {
        this.lastResponse = lastResponse;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CreationDate", nullable = false, length = 19)
    public Date getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(final Date creationDate) {
        this.creationDate = creationDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastRetryDate", nullable = false, length = 19)
    public Date getLastRetryDate() {
        return this.lastRetryDate;
    }

    public void setLastRetryDate(final Date lastRetryDate) {
        this.lastRetryDate = lastRetryDate;
    }
}
