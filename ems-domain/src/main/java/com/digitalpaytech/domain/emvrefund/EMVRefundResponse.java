package com.digitalpaytech.domain.emvrefund;

public class EMVRefundResponse {
    private String code;
    private String message;
    
    public EMVRefundResponse(final String code, final String message) {
        this.code = code;
        this.message = message;
    }

    public final String getCode() {
        return this.code;
    }

    public final void setCode(final String code) {
        this.code = code;
    }

    public final String getMessage() {
        return this.message;
    }

    public final void setMessage(final String message) {
        this.message = message;
    }
}
