package com.digitalpaytech.domain;

// Generated 18-Apr-2012 2:22:51 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;


/**
 * Activitytype generated by hbm2java
 */
@Entity
@Table(name = "ActivityType")
@NamedQueries({
    @NamedQuery(name = "ActivityType.findAllChildActivityTypes", query = "from ActivityType activityType where activityType.isParent = false", cacheable = true),
    @NamedQuery(name = "ActivityType.findActivityTypeByParentActivityTypeId", query = "from ActivityType activityType where activityType.parentActivityType.id = :parentActivityTypeId", cacheable = true)
})
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ActivityType implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 182591146953555548L;
    private int id;
    private ActivityType parentActivityType;
    private String name;
    private String description;
    private String messagePropertiesKey;
    private boolean isParent;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    private Set<ActivityLog> activityLogs = new HashSet<ActivityLog>(0);
    private Set<ActivityType> activityTypes = new HashSet<ActivityType>(0);

    public ActivityType() {
    }

    public ActivityType(int id, String name, String messagePropertiesKey, boolean isParent,
            Date lastModifiedGmt, int lastModifiedByUserId) {
        this.id = id;
        this.name = name;
        this.messagePropertiesKey = messagePropertiesKey;
        this.isParent = isParent;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }

    public ActivityType(int id, ActivityType activityType, String name,
            String description, String messagePropertiesKey, boolean isParent,
            Date lastModifiedGmt, int lastModifiedByUserId,
            Set<ActivityLog> activityLogs, Set<ActivityType> activityTypes) {
        this.id = id;
        this.parentActivityType = activityType;
        this.name = name;
        this.description = description;
        this.messagePropertiesKey = messagePropertiesKey;
        this.isParent = isParent;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
        this.activityLogs = activityLogs;
        this.activityTypes = activityTypes;
    }

    @Id
    @Column(name = "Id", nullable = false)
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ParentActivityTypeId")
    public ActivityType getParentActivityType() {
        return this.parentActivityType;
    }

    public void setParentActivityType(ActivityType parentActivityType) {
        this.parentActivityType = parentActivityType;
    }

    @Column(name = "Name", unique = true, nullable = false, length = 40)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "Description")
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "MessagePropertiesKey")
    public String getMessagePropertiesKey() {
        return messagePropertiesKey;
    }

    public void setMessagePropertiesKey(String messagePropertiesKey) {
        this.messagePropertiesKey = messagePropertiesKey;
    }
    
    @Column(name = "IsParent", nullable = false)
    public boolean isIsParent() {
        return this.isParent;
    }

    public void setIsParent(boolean isParent) {
        this.isParent = isParent;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = 19)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }

    public void setLastModifiedGmt(Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }

    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }

    public void setLastModifiedByUserId(int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "activityType")
    public Set<ActivityLog> getActivityLogs() {
        return this.activityLogs;
    }

    public void setActivityLogs(Set<ActivityLog> activityLogs) {
        this.activityLogs = activityLogs;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parentActivityType")
    public Set<ActivityType> getActivityTypes() {
        return this.activityTypes;
    }

    public void setActivityTypes(Set<ActivityType> activityTypes) {
        this.activityTypes = activityTypes;
    }
}
