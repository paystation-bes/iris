package com.digitalpaytech.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.domain.util.HibernateConstants;

@Entity
@Table(name = "CPSProcessData")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({
    @NamedQuery(name = "CPSProcessData.findByTerminalTokenAndChargeToken", query = "FROM CPSProcessData cpd WHERE cpd.terminalToken = :terminalToken AND cpd.chargeToken = :chargeToken", cacheable = true) })
public class CPSProcessData implements Serializable {
    private static final long serialVersionUID = 6645997593377702505L;
    private Integer id;
    private String serialNumber;
    private String terminalToken;
    private String chargeToken;
    private int amount;
    private String authorizationNumber;
    private String cardType;
    private short last4Digits;
    private String referenceNumber;
    private String processorTransactionId;
    private Date processedGMT;
    private Date createdGMT;

    public CPSProcessData() {
        super();
    }
    
    public CPSProcessData(final String serialNumber, final String terminalToken, final String chargeToken, final int amount,
            final String authorizationNumber, final String cardType, final short last4Digits, final String referenceNumber,
            final String processorTransactionId, final Date processedGMT, final Date createdGMT) {
        
        this.serialNumber = serialNumber;
        this.terminalToken = terminalToken;
        this.chargeToken = chargeToken;
        this.amount = amount;
        this.authorizationNumber = authorizationNumber;
        this.cardType = cardType;
        this.last4Digits = last4Digits;
        this.referenceNumber = referenceNumber;
        this.processorTransactionId = processorTransactionId;
        this.processedGMT = processedGMT;
        this.createdGMT = createdGMT;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @Column(name = "SerialNumber", length = HibernateConstants.VARCHAR_LENGTH_20)
    public String getSerialNumber() {
        return this.serialNumber;
    }
    
    public void setSerialNumber(final String serialNumber) {
        this.serialNumber = serialNumber;
    }
    
    @Column(name = "TerminalToken", length = HibernateConstants.CHAR_LENGTH_UUID)
    public String getTerminalToken() {
        return this.terminalToken;
    }
    
    public void setTerminalToken(final String terminalToken) {
        this.terminalToken = terminalToken;
    }
    
    @Column(name = "ChargeToken", length = HibernateConstants.CHAR_LENGTH_UUID)
    public String getChargeToken() {
        return this.chargeToken;
    }
    
    public void setChargeToken(final String chargeToken) {
        this.chargeToken = chargeToken;
    }
    
    @Column(name = "Amount")
    public int getAmount() {
        return this.amount;
    }
    
    public void setAmount(final int amount) {
        this.amount = amount;
    }
    
    @Column(name = "AuthorizationNumber", length = HibernateConstants.VARCHAR_LENGTH_50)
    public String getAuthorizationNumber() {
        return this.authorizationNumber;
    }
    
    public void setAuthorizationNumber(final String authorizationNumber) {
        this.authorizationNumber = authorizationNumber;
    }
    
    @Column(name = "CardType")
    public String getCardType() {
        return this.cardType;
    }
    
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }
    
    @Column(name = "Last4Digits")
    public short getLast4Digits() {
        return this.last4Digits;
    }
    
    public void setLast4Digits(final short last4Digits) {
        this.last4Digits = last4Digits;
    }
    
    @Column(name = "ReferenceNumber")
    public String getReferenceNumber() {
        return this.referenceNumber;
    }
    
    public void setReferenceNumber(final String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }
    
    @Column(name = "ProcessorTransactionId")
    public String getProcessorTransactionId() {
        return this.processorTransactionId;
    }
    
    public void setProcessorTransactionId(final String processorTransactionId) {
        this.processorTransactionId = processorTransactionId;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ProcessedGMT", length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getProcessedGMT() {
        return this.processedGMT;
    }
    
    public void setProcessedGMT(final Date processedGMT) {
        this.processedGMT = processedGMT;
    }
    
    @Column(name = "CreatedGMT", nullable = false)
    public Date getCreatedGMT() {
        return this.createdGMT;
    }
    
    public void setCreatedGMT(final Date createdGMT) {
        this.createdGMT = createdGMT;
    }
    
    @Transient
    public boolean isExpired() {
        return StringUtils.isBlank(getProcessorTransactionId()) 
                && StringUtils.isBlank(getAuthorizationNumber())
                && StringUtils.isBlank(getReferenceNumber())
                && getProcessedGMT() == null;
    }
    
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("serialNumber", serialNumber).append("terminalToken", terminalToken)
                .append("chargeToken", chargeToken).append("amount", amount).append("authorizationNumber", authorizationNumber)
                .append("cardType", cardType).append("last4Digits", last4Digits).append("referenceNumber", referenceNumber)
                .append("processorTransactionId", processorTransactionId).append("processedGMT", processedGMT).append("createdGMT", createdGMT)
                .toString();
    }
}
