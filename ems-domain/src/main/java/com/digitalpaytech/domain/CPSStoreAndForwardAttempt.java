package com.digitalpaytech.domain;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.domain.util.HibernateConstants;

@Entity
@Table(name = "CPSStoreAndForwardAttempt")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({
    @NamedQuery(name = "CPSStoreAndForwardAttempt.findByPointOfSaleIdAndPurchasedDateAndTicketNumber", query = "FROM CPSStoreAndForwardAttempt csfa WHERE csfa.pointOfSale.id = :pointOfSale AND csfa.purchasedDate = :purchasedDate AND csfa.ticketNumber = :ticketNumber", cacheable = false),
    @NamedQuery(name = "CPSStoreAndForwardAttempt.findByChargeToken", query = "FROM CPSStoreAndForwardAttempt csfa WHERE csfa.chargeToken = :chargeToken", cacheable = false) })
public class CPSStoreAndForwardAttempt implements Serializable {

    private static final long serialVersionUID = 513205306064098349L;
    
    private Integer id;
    private PointOfSale pointOfSale;
    private Date purchasedDate;
    private int ticketNumber;
    private String chargeToken;
    private boolean isSuccessful;
    
    public CPSStoreAndForwardAttempt() {
    }
    
    public CPSStoreAndForwardAttempt(final PointOfSale pointOfSale, final Date purchasedDate, final int ticketNumber, final String chargeToken) {
        this.pointOfSale = pointOfSale;
        this.purchasedDate = purchasedDate;
        this.ticketNumber = ticketNumber;
        this.chargeToken = chargeToken;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PointOfSaleId", nullable = false)
    public PointOfSale getPointOfSale() {
        return this.pointOfSale;
    }
    
    public void setPointOfSale(final PointOfSale pointOfSale) {
        this.pointOfSale = pointOfSale;
    }
    
    @Column(name = "PurchasedDate", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getPurchasedDate() {
        return this.purchasedDate;
    }
    
    public void setPurchasedDate(final Date purchasedDate) {
        this.purchasedDate = purchasedDate;
    }
    
    @Column(name = "TicketNumber", nullable = false)
    public int getTicketNumber() {
        return this.ticketNumber;
    }
    
    public void setTicketNumber(final int ticketNumber) {
        this.ticketNumber = ticketNumber;
    }
    
    @Column(name = "ChargeToken", nullable = false, length = HibernateConstants.CHAR_LENGTH_UUID)
    public String getChargeToken() {
        return this.chargeToken;
    }
    
    public void setChargeToken(final String chargeToken) {
        this.chargeToken = chargeToken;
    }
    
    @Column(name = "IsSuccessful", nullable = false)
    public boolean isIsSuccessful() {
        return this.isSuccessful;
    }
    
    public void setIsSuccessful(final boolean isSuccessful) {
        this.isSuccessful = isSuccessful;
    }
}
