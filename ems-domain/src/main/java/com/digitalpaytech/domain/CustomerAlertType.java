package com.digitalpaytech.domain;

// Generated 31-Aug-2012 2:05:49 PM by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.digitalpaytech.annotation.StorageType;
import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryDelegateAttribute;
import com.digitalpaytech.annotation.StoryDelegateObject;
import com.digitalpaytech.annotation.StoryDelegateObjects;
import com.digitalpaytech.annotation.StoryId;
import com.digitalpaytech.annotation.StoryLookup;
import com.digitalpaytech.domain.util.HibernateConstants;

/**
 * CustomerAlertType generated by hbm2java
 */
@Entity
@Table(name = "CustomerAlertType")
@NamedQueries({
    @NamedQuery(name = "CustomerAlertType.findCustomerAlertTypesByCustomerIdOrderByNameAsc", query = "from CustomerAlertType cat where cat.customer.id = :customerId and cat.isDeleted = FALSE and cat.isDefault = FALSE order by cat.name"),
    @NamedQuery(name = "CustomerAlertType.findCustomerAlertTypesByCustomerIdOrderByNameDesc", query = "from CustomerAlertType cat where cat.customer.id = :customerId and cat.isDeleted = FALSE and cat.isDefault = FALSE order by cat.name desc"),
    @NamedQuery(name = "CustomerAlertType.findCustomerAlertTypesByCustomerIdAndAlertClassTypeIdOrderByNameAsc", query = "from CustomerAlertType cat where cat.customer.id = :customerId and cat.alertThresholdType.alertType.alertClassType.id = :filterType and cat.isDeleted = FALSE and cat.isDefault = FALSE order by cat.name"),
    @NamedQuery(name = "CustomerAlertType.findCustomerAlertTypesByCustomerIdAndAlertClassTypeIdOrderByNameDesc", query = "from CustomerAlertType cat where cat.customer.id = :customerId and cat.alertThresholdType.alertType.alertClassType.id = :filterType and cat.isDeleted = FALSE and cat.isDefault = FALSE order by cat.name desc"),
    @NamedQuery(name = "CustomerAlertType.findCustomerAlertTypesByCustomerIdOrderByAlertTypeAsc", query = "from CustomerAlertType cat where cat.customer.id = :customerId and cat.isDeleted = FALSE and cat.isDefault = FALSE order by cat.alertThresholdType.alertType.alertClassType.name"),
    @NamedQuery(name = "CustomerAlertType.findCustomerAlertTypesByCustomerIdOrderByAlertTypeDesc", query = "from CustomerAlertType cat where cat.customer.id = :customerId and cat.isDeleted = FALSE and cat.isDefault = FALSE order by cat.alertThresholdType.alertType.alertClassType.name desc"),
    @NamedQuery(name = "CustomerAlertType.findCustomerAlertTypesByCustomerIdAndAlertClassTypeIdOrderByAlertTypeAsc", query = "from CustomerAlertType cat where cat.customer.id = :customerId and cat.alertThresholdType.alertType.alertClassType.id = :filterType and cat.isDeleted = FALSE and cat.isDefault = FALSE order by cat.alertThresholdType.alertType.alertClassType.name"),
    @NamedQuery(name = "CustomerAlertType.findCustomerAlertTypesByCustomerIdAndAlertClassTypeIdOrderByAlertTypeDesc", query = "from CustomerAlertType cat where cat.customer.id = :customerId and cat.alertThresholdType.alertType.alertClassType.id = :filterType and cat.isDeleted = FALSE and cat.isDefault = FALSE order by cat.alertThresholdType.alertType.alertClassType.name desc"),
    @NamedQuery(name = "CustomerAlertType.findCustomerAlertTypesByCustomerIdAlertThresholdTypeIdAndIsActive", query = "from CustomerAlertType cat where cat.customer.id = :customerId and cat.alertThresholdType.id = :alertThresholdTypeId and cat.isActive = :isActive and cat.isDeleted = FALSE and cat.isDefault = FALSE "),
    @NamedQuery(name = "CustomerAlertType.findDefaultCustomerAlertTypeByCustomerIdAlertThresholdTypeId", query = "from CustomerAlertType cat where cat.customer.id = :customerId and cat.alertThresholdType.id = :alertThresholdTypeId and cat.isDefault = TRUE "),
    @NamedQuery(name = "CustomerAlertType.findCustomerAlertTypesByCustomerIdAndRouteId", query = "from CustomerAlertType cat where cat.customer.id = :customerId and cat.route.id = :routeId and cat.alertThresholdType.id in (1,2,3,4,5,6,7,8,9,10,11,13) and cat.isDeleted = FALSE and cat.isDefault = FALSE "),
    @NamedQuery(name = "CustomerAlertType.findCustomerAlertTypesByCustomerIdAndRouteIdAllAlertThresholdTypes", query = "from CustomerAlertType cat where cat.customer.id = :customerId and cat.route.id = :routeId and cat.isDefault = FALSE "),
    @NamedQuery(name = "CustomerAlertType.countByRouteIds", query = "select count(id) from CustomerAlertType where (isDefault = false) and (isDeleted = false) and (route.id in(:routeIds))", cacheable = true),
    @NamedQuery(name = "CustomerAlertType.findNonDefaultByCustomerIds", query = "select cat from CustomerAlertType cat where (cat.customer.id in(:customerId)) and (cat.isDefault = false) and (cat.isActive = true) and (cat.isDeleted = false)", cacheable = true),
    @NamedQuery(name = "CustomerAlertType.findByIds", query = "select cat from CustomerAlertType cat where (cat.id in(:customerAlertTypeIds))", cacheable = true),
    @NamedQuery(name = "CustomerAlertType.findCustomerAlertTypesByRouteIdNotNull", query = "SELECT cat FROM CustomerAlertType cat where cat.route.id = :routeId", cacheable = true),
    @NamedQuery(name = "CustomerAlertType.findCustomerAlertTypesByRouteIdNull", query = "SELECT cat FROM CustomerAlertType cat where cat.route.id IS NULL", cacheable = true),
    @NamedQuery(name = "CustomerAlertType.findCustomerAlertTypeIdsByRouteId", query = "SELECT cat.id FROM CustomerAlertType cat where cat.route.id = :routeId", cacheable = true),
    @NamedQuery(name = "CustomerAlertType.findPointOfSaleIds", query = "select pos.id from PointOfSale pos join pos.posStatus pStat where pStat.isDecommissioned = 0 AND pStat.isDeleted = 0 AND pos.customer.id = (select cat.customer.id from CustomerAlertType cat where cat.id in(:customerAlertTypeIds))") })
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings({ "checkstyle:designforextension" })
@StoryAlias(CustomerAlertType.ALIAS)
public class CustomerAlertType implements java.io.Serializable {
    public static final String ALIAS_IS_DEFAULT = "default";
    public static final String ALIAS_THRESHOLD_TYPE = "threshold type";
    public static final String ALIAS_THRESHOLD = "threshold";
    public static final String ALIAS_NAME = "name";
    public static final String ALIAS_ROUTE = "route";
    public static final String ALIAS = "customer alert";
    public static final String ALIAS_ID = "id";
    public static final String ALIAS_CUSTOMER_EMAIL_LIST = "customer email";
    public static final String ALIAS_IS_ACTIVE = "active";
    
    private static final long serialVersionUID = -52964076428942246L;
    private Integer id;
    private int version;
    private Route route;
    private Location location;
    private AlertThresholdType alertThresholdType;
    private Customer customer;
    private String name;
    private float threshold;
    private boolean isActive;
    private boolean isDeleted;
    private boolean isDefault;
    private boolean isDelayed;
    private int delayedByMinutes;
    private Date lastModifiedGmt;
    private int lastModifiedByUserId;
    private Set<CustomerAlertEmail> customerAlertEmails = new HashSet<CustomerAlertEmail>(0);
    
    public CustomerAlertType() {
        super();
    }
    
    public CustomerAlertType(final AlertThresholdType alertThresholdType, final Customer customer, final String name, final float threshold,
            final boolean isActive, final boolean isDeleted, final boolean isDefault, final Date lastModifiedGmt, final int lastModifiedByUserId) {
        this.alertThresholdType = alertThresholdType;
        this.customer = customer;
        this.name = name;
        this.threshold = threshold;
        this.isActive = isActive;
        this.isDeleted = isDeleted;
        this.isDefault = isDefault;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    public CustomerAlertType(final Route route, final Location location, final AlertThresholdType alertThresholdType, final Customer customer,
            final String name, final float threshold, final boolean isActive, final boolean isDeleted, final boolean isDefault,
            final Date lastModifiedGmt, final int lastModifiedByUserId, final Set<CustomerAlertEmail> customerAlertEmails) {
        this.route = route;
        this.location = location;
        this.alertThresholdType = alertThresholdType;
        this.customer = customer;
        this.name = name;
        this.threshold = threshold;
        this.isActive = isActive;
        this.isDeleted = isDeleted;
        this.isDefault = isDefault;
        this.lastModifiedGmt = lastModifiedGmt;
        this.lastModifiedByUserId = lastModifiedByUserId;
        this.customerAlertEmails = customerAlertEmails;
    }
    
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "Id", nullable = false)
    @StoryId
    @StoryAlias(ALIAS_ID)
    public Integer getId() {
        return this.id;
    }
    
    public void setId(final Integer id) {
        this.id = id;
    }
    
    @Version
    @Column(name = "VERSION", nullable = false)
    public int getVersion() {
        return this.version;
    }
    
    public void setVersion(final int version) {
        this.version = version;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RouteId")
    @StoryAlias(ALIAS_ROUTE)
    @StoryLookup(type = Route.ALIAS, searchAttribute = Route.ALIAS_NAME, storage = StorageType.DB)
    public Route getRoute() {
        return this.route;
    }
    
    public void setRoute(final Route route) {
        this.route = route;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LocationId")
    public Location getLocation() {
        return this.location;
    }
    
    public void setLocation(final Location location) {
        this.location = location;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AlertThresholdTypeId", nullable = false)
    @StoryAlias(ALIAS_THRESHOLD_TYPE)
    @StoryLookup(type = AlertThresholdType.ALIAS, searchAttribute = AlertThresholdType.ALIAS_NAME)
    public AlertThresholdType getAlertThresholdType() {
        return this.alertThresholdType;
    }
    
    public void setAlertThresholdType(final AlertThresholdType alertThresholdType) {
        this.alertThresholdType = alertThresholdType;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CustomerId", nullable = false)
    @StoryAlias(Customer.ALIAS)
    @StoryLookup(type = Customer.ALIAS, searchAttribute = Customer.ALIAS_NAME, storage = StorageType.DB)
    public Customer getCustomer() {
        return this.customer;
    }
    
    public void setCustomer(final Customer customer) {
        this.customer = customer;
    }
    
    @Column(name = "Name", nullable = false, length = HibernateConstants.VARCHAR_LENGTH_30)
    @StoryAlias(ALIAS_NAME)
    public String getName() {
        return this.name;
    }
    
    public void setName(final String name) {
        this.name = name;
    }
    
    @Column(name = "Threshold", nullable = false, precision = HibernateConstants.COORDINATE_PRECISION, scale = HibernateConstants.COORDINATE_SCALE)
    @StoryAlias(ALIAS_THRESHOLD)
    public float getThreshold() {
        return this.threshold;
    }
    
    public void setThreshold(final float threshold) {
        this.threshold = threshold;
    }
    
    @Column(name = "IsActive", nullable = false)
    @StoryAlias(ALIAS_IS_ACTIVE)
    public boolean isIsActive() {
        return this.isActive;
    }
    
    public void setIsActive(final boolean isActive) {
        this.isActive = isActive;
    }
    
    @Column(name = "IsDeleted", nullable = false)
    public boolean isIsDeleted() {
        return this.isDeleted;
    }
    
    public void setIsDeleted(final boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
    
    @Column(name = "IsDefault", nullable = false)
    @StoryAlias(ALIAS_IS_DEFAULT)
    public boolean isIsDefault() {
        return this.isDefault;
    }
    
    public void setIsDefault(final boolean isDefault) {
        this.isDefault = isDefault;
    }
    
    @Column(name = "IsDelayed", nullable = false)
    public boolean getIsDelayed() {
        return this.isDelayed;
    }
    
    public void setIsDelayed(boolean isDelayed) {
        this.isDelayed = isDelayed;
    }
    
    @Column(name = "DelayedByMinutes", nullable = false)
    public int getDelayedByMinutes() {
        return this.delayedByMinutes;
    }
    
    public void setDelayedByMinutes(int delayedByMinutes) {
        this.delayedByMinutes = delayedByMinutes;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastModifiedGMT", nullable = false, length = HibernateConstants.DATEFIELD_LENGTH)
    public Date getLastModifiedGmt() {
        return this.lastModifiedGmt;
    }
    
    public void setLastModifiedGmt(final Date lastModifiedGmt) {
        this.lastModifiedGmt = lastModifiedGmt;
    }
    
    @Column(name = "LastModifiedByUserId", nullable = false)
    public int getLastModifiedByUserId() {
        return this.lastModifiedByUserId;
    }
    
    public void setLastModifiedByUserId(final int lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "customerAlertType")
    @StoryDelegateObjects({ @StoryDelegateObject({ @StoryDelegateAttribute(from = ALIAS_CUSTOMER_EMAIL_LIST, to = CustomerAlertEmail.ALIAS_CUSTOMER_EMAIL) }) })
    public Set<CustomerAlertEmail> getCustomerAlertEmails() {
        return this.customerAlertEmails;
    }
    
    public void setCustomerAlertEmails(final Set<CustomerAlertEmail> customerAlertEmails) {
        this.customerAlertEmails = customerAlertEmails;
    }
}
