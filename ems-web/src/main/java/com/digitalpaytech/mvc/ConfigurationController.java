package com.digitalpaytech.mvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.util.WebCoreConstants;

@Controller
public class ConfigurationController extends CommonLandingController {
    
    @RequestMapping(value = "settings/payStations/configGroup.html")
    @ResponseBody
    public final String configGroup(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        return "settings/loctions/configGroup.vm";
    }
    
    @RequestMapping(value = "settings/payStations/configGroupDetails.html")
    @ResponseBody
    public final String configGroupDetails(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    @RequestMapping(value = "settings/payStations/configGroupDetailsSave.html")
    @ResponseBody
    public final String configGroupDetailsSave(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    @RequestMapping(value = "settings/payStations/configAccessList.html")
    @ResponseBody
    public final String configAccessList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    @RequestMapping(value = "settings/payStations/configAccess.html")
    @ResponseBody
    public final String configAccess(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        return "settings/loctions/configAccess.vm";
    }
    
    @RequestMapping(value = "settings/payStations/configAccessDetails.html")
    @ResponseBody
    public final String configAccessDetails(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    @RequestMapping(value = "settings/payStations/configAccessSave.html")
    @ResponseBody
    public final String configAccessSave(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    @RequestMapping(value = "settings/payStations/configAccessDelete.html")
    @ResponseBody
    public final String configAccessDelete(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
}
