package com.digitalpaytech.mvc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosDate;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.domain.PosStatus;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.MapEntry;
import com.digitalpaytech.dto.PointOfSaleSearchCriteria;
import com.digitalpaytech.dto.PosAlertStatusDetailSummary;
import com.digitalpaytech.dto.SearchCustomerResult;
import com.digitalpaytech.dto.SearchCustomerResultSetting;
import com.digitalpaytech.dto.customeradmin.LocationRouteFilterInfo;
import com.digitalpaytech.dto.customeradmin.PaystationListInfo;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.mvc.customeradmin.support.CustomerAdminPayStationEditForm;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.PointOfSaleSearchForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.PosAlertStatusService;
import com.digitalpaytech.service.PosEventCurrentService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.service.queue.QueueCustomerAlertTypeService;
import com.digitalpaytech.service.systemadmin.SystemAdminService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObjectId;

@Controller
public class PayStationListController {
    
    protected static Logger log = Logger.getLogger(WidgetController.class);
    
    protected static final String FORM_SEARCH = "payStationSearchForm";
    
    //    private static final String STATUS_ACTIVE = "1";
    //    private static final String STATUS_CLEARED = "0";
    
    @Autowired
    protected LocationService locationService;
    @Autowired
    protected RouteService routeService;
    @Autowired
    protected PointOfSaleService pointOfSaleService;
    @Autowired
    protected CommonControllerHelper commonControllerHelper;
    @Autowired
    protected CustomerAdminService customerAdminService;
    @Autowired
    protected SystemAdminService systemAdminService;
    @Autowired
    protected PosAlertStatusService posAlertStatusService;
    @Autowired
    private PosEventCurrentService posEventCurrentService;
    @Autowired
    @Qualifier("kafkaCustomerAlertTypeService")
    private QueueCustomerAlertTypeService queueCustomerAlertTypeService;
    
    public final void setLocationService(final LocationService locationService) {
        this.locationService = locationService;
    }
    
    public final void setRouteService(final RouteService routeService) {
        this.routeService = routeService;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }
    
    public final void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    public final void setSystemAdminService(final SystemAdminService systemAdminService) {
        this.systemAdminService = systemAdminService;
    }
    
    public final void setPosAlertStatusService(final PosAlertStatusService posAlertStatusService) {
        this.posAlertStatusService = posAlertStatusService;
    }
    
    //========================================================================================================================
    // "Pay Station List" tab
    //========================================================================================================================
    
    /**
     * This method creates a list of Id's of routes and locations which have pay
     * stations for the current user and puts them into the model to be used in
     * the UI to filter pay stations
     * 
     * @param request
     * @param response
     * @param model
     * @return List of locations and routes stored in the model as attribute
     *         "filterValues" used for filtering pay stations. Target vm file
     *         URI which is mapped to Settings->pay station list form
     *         (/settings/locations/paystationList.vm)
     */
    public final String payStationList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        final String urlOnSuccess, final boolean isSystemAdmin) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        
        final List<Location> locations = this.locationService.findLocationsByCustomerId(customerId);
        
        final LocationRouteFilterInfo filterValues = this.commonControllerHelper.createLocationRouteFilter(customerId, keyMapping);
        
        final LocationRouteFilterInfo validValues = new LocationRouteFilterInfo();
        
        if (locations != null) {
            for (Location location : locations) {
                if (!location.getIsParent()) {
                    location.setRandomId(keyMapping.getRandomString(location, WebCoreConstants.ID_LOOK_UP_NAME));
                    if (location.getLocation() != null) {
                        validValues.addLocation(location.getName(), location.getRandomId(), location.getLocation().getName(), location.getIsParent());
                    } else {
                        validValues.addLocation(location.getName(), location.getRandomId(), "", location.getIsParent());
                    }
                }
            }
        }
        
        final PlacementMapBordersInfo mapBorders = this.pointOfSaleService.findMapBordersByCustomerId(customerId);
        model.put("mapBorders", mapBorders);
        
        model.put("filterValues", filterValues);
        //model.put("paystationList", getpayStationList(request, response, customerId, isSystemAdmin));
        model.put("validValues", validValues);
        return urlOnSuccess;
    }
    
    /**
     * This method takes an optional location or route Id and creates a list of
     * pay stations. This list is returned to the UI as JSON for display
     * 
     * @param request
     * @param response
     * @return Pay station information (names and random ID's) converted into
     *         JSON
     */
    private List<PaystationListInfo> getpayStationList(final HttpServletRequest request, final HttpServletResponse response,
        final Integer customerId, final boolean isSystemAdmin) {
        
        final String strRandomLocationId = request.getParameter("locationID");
        final String strRandomRouteId = request.getParameter("routeID");
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final PointOfSaleSearchCriteria criteria = new PointOfSaleSearchCriteria();
        criteria.setCustomerId(customerId);
        
        if (strRandomLocationId != null) {
            final Integer locationId = verifyRandomIdAndReturnActual(response, keyMapping, strRandomLocationId,
                                                                     "+++ Invalid locationID in PayStationListController.payStations() method. +++");
            if (locationId == null) {
                return null;
            }
            
            criteria.setLocationId(locationId);
            criteria.setShowDecommissioned(false);
            criteria.setShowDeactivated(false);
            criteria.setShowHidden(false);
        } else if (strRandomRouteId != null) {
            final Integer routeId = verifyRandomIdAndReturnActual(response, keyMapping, strRandomLocationId,
                                                                  "+++ Invalid routeID in PayStationListController.payStations() method. +++");
            if (routeId == null) {
                return null;
            }
            
            criteria.setRouteId(routeId);
            criteria.setShowDecommissioned(false);
            criteria.setShowDeactivated(false);
            criteria.setShowHidden(false);
        }
        
        final List<PaystationListInfo> pointOfSaleInformation = this.pointOfSaleService.findPaystation(criteria);
        
        return pointOfSaleInformation;
    }
    
    /**
     * This method takes a pay station id as input and returns map information
     * and payStationDetails.vm
     * 
     * @param request
     * @param response
     * @param model
     * @return A MapEntry object with the requested payStationId's coordinates
     *         in the model as attribute "mapInfo". Target vm file URI which is
     *         mapped to Settings->payStationDetails
     *         (/settings/location/payStationDetails.vm)
     */
    public final String payStationDetailPageSuper(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strPointOfSaleId = request.getParameter("payStationID");
        final Integer pointOfSaleId = verifyRandomIdAndReturnActual(response, keyMapping, strPointOfSaleId,
                                                                    "+++ Invalid payStationID in PayStationListController.payStationDetailPage() method. +++");
        if (pointOfSaleId == null) {
            return null;
        }
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        if (pointOfSale == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Paystation payStation = this.pointOfSaleService.findPayStationByPointOfSaleId(pointOfSale.getId());
        
        // Highest active alert severity
        // EventSeverityType eventSeverityType = this.pointOfSaleService.findGreatestActiveSeverityTypeByPointOfSaleId(pointOfSale.getId());
        // int severity = eventSeverityType.getId();
        
        int criticalCount = 0;
        int majorCount = 0;
        int minorCount = 0;
        int severity = 0;
        
        final PosAlertStatusDetailSummary pasds = this.posAlertStatusService.findPosAlertDetailSummaryByPointOfSaleId(pointOfSale.getId())
                .get(pointOfSale.getId());
        if (pasds != null) {
            criticalCount = pasds.getTotalCritical();
            majorCount = pasds.getTotalMajor();
            minorCount = pasds.getTotalMinor();
            severity = pasds.getSeverity();
        }
        
        final MapEntry mapInfo = new MapEntry(payStation.getPaystationType().getId(), pointOfSale.getName(), pointOfSale.getLatitude(),
                pointOfSale.getLongitude(), severity, null, null, 0, null, null, null, pointOfSale.getSerialNumber());
        
        final Location location = this.locationService.findLocationByPointOfSaleId(pointOfSale.getId());
        location.setRandomId(keyMapping.getRandomString(location, WebCoreConstants.ID_LOOK_UP_NAME));
        final PosStatus pointOfSaleStatus = this.pointOfSaleService.findPointOfSaleStatusByPOSId(pointOfSale.getId(), true);
        
        final CustomerAdminPayStationEditForm payStationEditForm = new CustomerAdminPayStationEditForm();
        
        payStationEditForm.setLocationRandomId(location.getRandomId());
        payStationEditForm.setLocationName(location.getName());
        payStationEditForm.setActive(pointOfSaleStatus.isIsActivated());
        payStationEditForm.setHidden(!pointOfSaleStatus.isIsVisible());
        payStationEditForm.setName(pointOfSale.getName());
        payStationEditForm.setRandomId(strPointOfSaleId);
        payStationEditForm.setParentLocationName(location.getLocation() == null ? "" : location.getLocation().getName());
        payStationEditForm.setSerialNumber(pointOfSale.getSerialNumber());
        payStationEditForm.setMapInfo(mapInfo);
        payStationEditForm.setIsDecommissioned(pointOfSaleStatus.isIsDecommissioned());
        payStationEditForm.setAlertCount(criticalCount + majorCount + minorCount);
        payStationEditForm.setAlertSeverity(criticalCount > 0 ? WebCoreConstants.SEVERITY_CRITICAL : majorCount > 0 ? WebCoreConstants.SEVERITY_MAJOR
                : minorCount > 0 ? WebCoreConstants.SEVERITY_MINOR : WebCoreConstants.SEVERITY_CLEAR);
        
        final String timeZone = WebSecurityUtil.getCustomerTimeZone();
        // Last seen
        final PosHeartbeat heartbeat = this.pointOfSaleService.findPosHeartbeatByPointOfSaleId(pointOfSaleId);
        if (heartbeat == null) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }
        payStationEditForm.setLastSeen(heartbeat.getLastHeartbeatGmt() == null ? "" : DateUtil.getRelativeTimeString(heartbeat.getLastHeartbeatGmt(),
                                                                                                                     timeZone));
        if (pointOfSale.isIsLinux() != null && pointOfSale.isIsLinux().booleanValue()) {
            payStationEditForm.setIsLinux(Boolean.TRUE);
        } else {
            payStationEditForm.setIsLinux(Boolean.FALSE);
        }
        
        return WidgetMetricsHelper.convertToJson(payStationEditForm, "payStationDetails", true);
    }
    
    protected final void insertNewPosDate(final PointOfSale pointOfSale, final int posDateTypeId, final UserAccount userAccount,
        final boolean setting, final String comment) {
        final PosDate posDate = new PosDate();
        posDate.setPointOfSale(pointOfSale);
        posDate.setPosDateType(this.pointOfSaleService.findPosDateTypeById(posDateTypeId));
        posDate.setCurrentSetting(setting);
        if (comment != null && !WebCoreConstants.EMPTY_STRING.equals(comment.trim())) {
            posDate.setComment(comment);
        }
        posDate.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        posDate.setLastModifiedByUserId(userAccount);
        posDate.setChangedGmt(DateUtil.getCurrentGmtDate());
        this.pointOfSaleService.saveOrUpdatePOSDate(posDate);
    }
    
    /**
     * private method that verifies a random Id and returns the actual DB id
     * 
     * @param response
     * @param keyMapping
     * @param strRandomLocationId
     * @return object database Id
     */
    protected final Integer verifyRandomIdAndReturnActual(final HttpServletResponse response, final RandomKeyMapping keyMapping,
        final String strRandomLocationId, final String message) {
        
        final String[] msgs = new String[] { message, message };
        return WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, strRandomLocationId, msgs);
    }
    
    protected final PointOfSaleSearchCriteria populatePosSearchCriteria(final HttpServletRequest request, final PointOfSaleSearchForm form,
        final Integer customerId, final boolean isSystemAdmin) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final PointOfSaleSearchCriteria criteria = new PointOfSaleSearchCriteria();
        criteria.setCustomerId(customerId);
        criteria.setLocationId(keyMapping.getKey(form.getLocationRandomId(), Location.class, Integer.class));
        criteria.setRouteId(keyMapping.getKey(form.getRouteRandomId(), Route.class, Integer.class));
        criteria.setPointOfSaleId(keyMapping.getKey(form.getItemId(), PointOfSale.class, Integer.class));
        
        final WebObjectId selectedId = keyMapping.getKeyObject(form.getSelectedId());
        if (selectedId != null) {
            if (Location.class.isAssignableFrom(selectedId.getObjectType())) {
                criteria.setSelectedLocationId((Integer) selectedId.getId());
            } else if (Route.class.isAssignableFrom(selectedId.getObjectType())) {
                criteria.setSelectedRouteId((Integer) selectedId.getId());
            } else {
                throw new RuntimeException("UnExpected Object Type for Location/Route's pay station selector: " + selectedId.getObjectType());
            }
        }
        
        if (StringUtils.isNotBlank(form.getLocationRandomId()) && (criteria.getLocationId() == null)) {
            throw new RuntimeException("Invalid Location Random ID for Customer: " + customerId);
        }
        if (StringUtils.isNotBlank(form.getRouteRandomId()) && (criteria.getRouteId() == null)) {
            throw new RuntimeException("Invalid Route Random ID for Customer: " + customerId);
        }
        if (StringUtils.isNotBlank(form.getItemId()) && (criteria.getPointOfSaleId() == null)) {
            throw new RuntimeException("Invalid PointOfSale Random ID for Customer: " + customerId);
        }
        
        if (form.getPage() != null) {
            criteria.setPage(form.getPage());
        }
        
        criteria.setItemsPerPage(WebCoreConstants.PAY_STATIONS_PER_PAGE);
        
        if ((form.getDataKey() != null) && (form.getDataKey().length() > 0)) {
            try {
                criteria.setMaxUpdatedTime(new Date(Long.parseLong(form.getDataKey())));
            } catch (NumberFormatException nfe) {
                // DO NOTHING
            }
        }
        
        if (criteria.getMaxUpdatedTime() == null) {
            criteria.setMaxUpdatedTime(new Date());
        }
        
        return criteria;
    }
    
    @ModelAttribute(FORM_SEARCH)
    public final WebSecurityForm<PointOfSaleSearchForm> initPosSearchForm(final HttpServletRequest request) {
        return new WebSecurityForm<PointOfSaleSearchForm>(null, new PointOfSaleSearchForm());
    }
    
    protected final void resetAlerts(final boolean isActive, final Integer pointOfSaleId, final UserAccount userAccount,
        final boolean deletePosEventCurrent) {
        final List<Integer> posIdList = new ArrayList<Integer>();
        posIdList.add(pointOfSaleId);
        if (isActive) {
            this.queueCustomerAlertTypeService.addCustomerAlertTypeEvent(null, null, posIdList);
        } else {
            final List<PosEventCurrent> activePosEventList = this.posEventCurrentService.findActivePosEventCurrentByPointOfSaleId(pointOfSaleId);
            for (PosEventCurrent posEventCurrent : activePosEventList) {
                boolean hasUserDefinedAlerts = false;
                if (posEventCurrent.getCustomerAlertType() == null) {
                    this.commonControllerHelper.createClearEvent(posEventCurrent.getEventActionType() == null ? null : posEventCurrent
                            .getEventActionType().getId(), posEventCurrent.getEventType(), userAccount.getId(), pointOfSaleId);
                } else {
                    hasUserDefinedAlerts = true;
                }
                if (hasUserDefinedAlerts && !deletePosEventCurrent) {
                    this.queueCustomerAlertTypeService.addCustomerAlertTypeEvent(null, null, posIdList);
                }
            }
            
            if (deletePosEventCurrent) {
                final List<PosEventCurrent> posEventCurrentToBeDeletedList = this.posEventCurrentService
                        .findAllPosEventCurrentByPointOfSaleId(pointOfSaleId);
                final List<Integer> customerAlertTypeIdList = new ArrayList<Integer>();
                for (PosEventCurrent posEventCurrent : posEventCurrentToBeDeletedList) {
                    if (posEventCurrent.getCustomerAlertType() == null) {
                        this.posEventCurrentService.delete(posEventCurrent);
                    } else {
                        customerAlertTypeIdList.add(posEventCurrent.getCustomerAlertType().getId());
                    }
                }
                this.queueCustomerAlertTypeService.addCustomerAlertTypeEvent(null, posIdList, null, customerAlertTypeIdList);
            }
        }
    }
    
    /**
     * Method which returns the paystation for a particular customer
     * 
     * 
     */
    protected final String searchPos(final HttpServletRequest request, final HttpServletResponse response) {
        
        final String search = request.getParameter("search");
        
        final SearchCustomerResult searchCustomerResult = new SearchCustomerResult();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        
        if (customerId == null) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        if (!StringUtils.isBlank(search)) {
            if (!search.matches(WebCoreConstants.REGEX_PAYSTATION_TEXT)) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            /* retrieve first numbers (e.g. 20, defined in EmsProperties db table) of pay stations by serial number. */
            if (StringUtils.isNumeric(search)) {
                final Collection<PointOfSale> posList = this.pointOfSaleService.findPointOfSalesBySerialNumberKeywordWithLimit(search, customerId);
                for (PointOfSale pos : posList) {
                    searchCustomerResult.addPosSearchResult(new SearchCustomerResultSetting(
                            keyMapping.getRandomString(PointOfSale.class, pos.getId()), pos.getSerialNumber(), pos.getLocation().getName()));
                }
            }
            
            /* retrieve all pay stations by PointOfSale name. */
            final Collection<PointOfSale> posList = this.pointOfSaleService.findPointOfSalesByPosNameKeywordWithLimit(search, customerId);
            for (PointOfSale pos : posList) {
                if (pos.getName().equals(pos.getSerialNumber())) {
                    continue;
                }
                
                searchCustomerResult.addPosNameSearchResult(new SearchCustomerResultSetting(
                        keyMapping.getRandomString(PointOfSale.class, pos.getId()), pos.getName(), pos.getLocation().getName()));
            }
            
        }
        
        return WidgetMetricsHelper.convertToJson(searchCustomerResult, "searchCustomerResult", true);
    }
    
}
