package com.digitalpaytech.mvc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.CaseLocationLot;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.FlexLocationFacility;
import com.digitalpaytech.domain.FlexLocationProperty;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.LocationDay;
import com.digitalpaytech.domain.LocationLineGeopoint;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.domain.RateProfileLocation;
import com.digitalpaytech.domain.RateRateProfile;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.GeoLocationEntry;
import com.digitalpaytech.dto.WidgetMapInfo;
import com.digitalpaytech.dto.customeradmin.LocationTree;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.mvc.customeradmin.support.LocationEditForm;
import com.digitalpaytech.mvc.customeradmin.support.LocationLineGeopointEditForm;
import com.digitalpaytech.mvc.helper.RateProfileHelper;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.RateProfileLocationForm;
import com.digitalpaytech.mvc.support.RateRateProfileEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.CaseService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.FlexWSService;
import com.digitalpaytech.service.LocationLineGeopointService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.PermitIssueTypeService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.PosEventCurrentService;
import com.digitalpaytech.service.RateProfileLocationService;
import com.digitalpaytech.service.RateProfileService;
import com.digitalpaytech.service.RateRateProfileService;
import com.digitalpaytech.util.DashboardUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObjectId;
import com.digitalpaytech.validation.customeradmin.LocationGeopointValidator;
import com.digitalpaytech.validation.customeradmin.LocationValidator;

@Controller
@SessionAttributes({ WebSecurityConstants.WEB_SECURITY_FORM })
@SuppressWarnings({ "PMD.GodClass", "PMD.TooManyFields", "PMD.TooManyMethods", "PMD.ExcessiveImports", "PMD.CouplingBetweenObjects",
    "PMD.UseConcurrentHashMap" })
public class LocationDetailsController {
    protected static final Logger LOGGER = Logger.getLogger(WidgetController.class);
    private static final String LOCATION_FORM_DETAILS = "locationFormDetails";
    private static final String LOCATION_ID = "locationID";
    private static final String INVALID_LOCATION_ID = "+++ Invalid randomised locationID in LocationsController.getLocationDetails() method. +++";
    private static final String ERROR_STATUS = "errorStatus";
    private static final String ERROR_SYSADMIN_CUSTOMER_NOT_MIGRATED = "error.customer.notMigrated.systemAdmin";
    private static final String ERROR_CUSTOMER_NOT_MIGRATED = "error.customer.notMigrated";
    private static final Integer ONE_HUNDRED = 100;
    private static final Integer SEVEN_WEEK_DAYS = 7;
    private static final Integer SIX_WEEK_DAYS = 6;
    
    @Autowired
    protected LocationService locationService;
    @Autowired
    protected CustomerAdminService customerAdminService;
    @Autowired
    protected CustomerService customerService;
    @Autowired
    protected PointOfSaleService pointOfSaleService;
    @Autowired
    protected PosEventCurrentService posEventCurrentService;
    @Autowired
    protected LocationValidator locationValidator;
    @Autowired
    protected LocationGeopointValidator locationGeopointValidator;
    @Autowired
    protected PermitIssueTypeService permitIssueTypeService;
    @Autowired
    protected RateProfileService rateProfileService;
    @Autowired
    protected RateProfileLocationService rateProfileLocationService;
    @Autowired
    protected RateProfileHelper rateProfileHelper;
    @Autowired
    protected RateRateProfileService rateRateProfileService;
    @Autowired
    protected FlexWSService flexWSService;
    @Autowired
    protected LocationLineGeopointService locationLineGeopointService;
    @Autowired
    protected CaseService caseService;
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    public final RateProfileHelper getRateProfileHelper() {
        return this.rateProfileHelper;
    }
    
    public final void setRateProfileHelper(final RateProfileHelper rateProfileHelper) {
        this.rateProfileHelper = rateProfileHelper;
    }
    
    public final RateRateProfileService getRateRateProfileService() {
        return this.rateRateProfileService;
    }
    
    public final void setRateRateProfileService(final RateRateProfileService rateRateProfileService) {
        this.rateRateProfileService = rateRateProfileService;
    }
    
    public final LocationService getLocationService() {
        return this.locationService;
    }
    
    public final CustomerAdminService getCustomerAdminService() {
        return this.customerAdminService;
    }
    
    public final CustomerService getCustomerService() {
        return this.customerService;
    }
    
    public final PointOfSaleService getPointOfSaleService() {
        return this.pointOfSaleService;
    }
    
    public final LocationValidator getLocationValidator() {
        return this.locationValidator;
    }
    
    public final LocationGeopointValidator getLocationGeopointValidator() {
        return this.locationGeopointValidator;
    }
    
    public final PermitIssueTypeService getPermitIssueTypeService() {
        return this.permitIssueTypeService;
    }
    
    public final RateProfileService getRateProfileService() {
        return this.rateProfileService;
    }
    
    public final RateProfileLocationService getRateProfileLocationService() {
        return this.rateProfileLocationService;
    }
    
    public final FlexWSService getFlexWSService() {
        return this.flexWSService;
    }
    
    public final LocationLineGeopointService getLocationLineGeopointService() {
        return this.locationLineGeopointService;
    }
    
    public final CaseService getCaseService() {
        return this.caseService;
    }
    
    public final void setLocationService(final LocationService locationService) {
        this.locationService = locationService;
    }
    
    public final void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final void setLocationValidator(final LocationValidator locationValidator) {
        this.locationValidator = locationValidator;
    }
    
    public final void setLocationGeopointValidator(final LocationGeopointValidator locationGeopointValidator) {
        this.locationGeopointValidator = locationGeopointValidator;
    }
    
    public final void setPermitIssueTypeService(final PermitIssueTypeService permitIssueTypeService) {
        this.permitIssueTypeService = permitIssueTypeService;
    }
    
    public final void setRateProfileService(final RateProfileService rateProfileService) {
        this.rateProfileService = rateProfileService;
    }
    
    public final void setRateProfileLocationService(final RateProfileLocationService rateProfileLocationService) {
        this.rateProfileLocationService = rateProfileLocationService;
    }
    
    public final void setFlexWSService(final FlexWSService flexWSService) {
        this.flexWSService = flexWSService;
    }
    
    public final void setLocationLineGeopointService(final LocationLineGeopointService locationLineGeopointService) {
        this.locationLineGeopointService = locationLineGeopointService;
    }
    
    public final void setCaseService(final CaseService caseService) {
        this.caseService = caseService;
    }
    
    public final PosEventCurrentService getPosEventCurrentService() {
        return this.posEventCurrentService;
    }
    
    public final void setPosEventCurrentService(final PosEventCurrentService posEventCurrentService) {
        this.posEventCurrentService = posEventCurrentService;
    }
    
    //========================================================================================================================
    // "Location Details" tab
    //========================================================================================================================
    
    /**
     * This method will retrieve all customer's location information, format
     * that information into a hierarchical tree and puts it in the model as
     * "locations" and returns locations/index.vm
     * 
     * @param request
     * @param response
     * @param model
     * @return Model containing current customer location information in tree
     *         form and returns target URL which is mapped to
     *         Settings->Locations (/settings/locations/index.html)
     */
    public final String locationList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        final String urlOnSuccess) {
        
        List<Location> locations = new ArrayList<Location>();
        final Integer customerId = resolveCustomerId(request, response);
        
        locations = (List<Location>) this.locationService.findLocationsByCustomerId(customerId);
        
        /* random key mapping on location ID's */
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        for (Location loc : locations) {
            loc.setRandomId(keyMapping.getRandomString(loc, WebCoreConstants.ID_LOOK_UP_NAME));
        }
        
        final List<LocationTree> locationTree = createLocationTree(locations, customerId);
        
        final LocationEditForm locationEditForm = new LocationEditForm();
        final WebSecurityForm<LocationEditForm> secForm = new WebSecurityForm<LocationEditForm>(null, locationEditForm);
        final PlacementMapBordersInfo mapBorders = this.pointOfSaleService.findMapBordersByCustomerId(customerId);
        model.put("mapBorders", mapBorders);
        //TODO Remove SessionToken
        // SessionTool sessionTool = SessionTool.getInstance(request);
        // model.put(WebSecurityConstants.SESSION_TOKEN, sessionTool.getAttribute(WebSecurityConstants.SESSION_TOKEN));
        model.put(WebSecurityConstants.WEB_SECURITY_FORM, secForm);
        
        model.put("locations", WidgetMetricsHelper.convertToJson(locationTree, "Locations", true));
        model.put(WebCoreConstants.LOCATION_TREE, locationTree);
        
        if (this.rateProfileService != null) {
            model.put(RateProfileLocationController.FORM_EDIT_LOCATION, initEditLocationForm(request));
            model.put("rateProfiles", this.rateProfileService.findPublishedRateProfileFiltersByCustomerId(customerId, keyMapping));
        }
        
        if (this.permitIssueTypeService != null) {
            model.put("permitIssueTypeList", this.permitIssueTypeService.findAll());
        }
        
        return urlOnSuccess;
    }
    
    /**
     * This method will return the JSON location from containing pay stations
     * and locations for the customer, this is for a new location.
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON formatted location edit form
     */
    protected final String getLocationEditFormSuper(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        final LocationEditForm locationEditForm = new LocationEditForm();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer customerId = resolveCustomerId(request, response);
        
        final List<PointOfSale> pointOfSales = this.pointOfSaleService.findValidPointOfSaleByCustomerIdOrderByName(customerId);
        final List<Location> locations = this.locationService.findLocationsByCustomerId(customerId);
        
        if (pointOfSales != null) {
            for (PointOfSale pointOfSale : pointOfSales) {
                pointOfSale.setRandomId(keyMapping.getRandomString(pointOfSale, WebCoreConstants.ID_LOOK_UP_NAME));
                
                locationEditForm.addPayStation(pointOfSale.getName(), pointOfSale.getRandomId(), WebCoreConstants.ACTIVE,
                                               pointOfSale.getPaystation().getPaystationType().getId(), getSeverity(pointOfSale));
            }
        }
        
        if (locations != null) {
            for (Location location : locations) {
                if (!location.getIsParent() && !location.getIsUnassigned() && !location.getIsDeleted()) {
                    location.setRandomId(keyMapping.getRandomString(location, WebCoreConstants.ID_LOOK_UP_NAME));
                    locationEditForm.addChildLocation(location.getName(), location.getRandomId(), WebCoreConstants.ACTIVE);
                }
            }
        }
        
        return WidgetMetricsHelper.convertToJson(locationEditForm, LOCATION_FORM_DETAILS, true);
    }
    
    /**
     * This method takes a locationId as input through request parameter
     * "locationID" and gathers details associated to that location to send to
     * the UI for displaying location details, or inserting into the location
     * edit form for editing
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON string having location detail information. "false" in case
     *         of any exception or validation failure.
     */
    protected final String getLocationDetailsSuper(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        final String strRandomLocationId = request.getParameter(LOCATION_ID);
        
        if (!DashboardUtil.validateRandomId(strRandomLocationId)) {
            LOGGER.warn(INVALID_LOCATION_ID);
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Object actualLocationId = keyMapping.getKey(strRandomLocationId);
        
        if (!DashboardUtil.validateDBIntegerPrimaryKey(actualLocationId.toString())) {
            LOGGER.warn("+++ Invalid locationID in LocationsController.getLocationDetails() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }
        
        final int locationId = Integer.parseInt(actualLocationId.toString());
        
        final Location location = this.locationService.findLocationById(locationId);
        if (location == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return null;
        }
        if (location.getIsDeleted()) {
            final MessageInfo message = new MessageInfo();
            message.setError(true);
            message.setToken(null);
            message.addMessage(this.commonControllerHelper.getMessage(ErrorConstants.REQUESTED_ITEM_DELETED));
            return WidgetMetricsHelper.convertToJson(message, WebCoreConstants.UI_MESSAGES, true);
        }
        
        LocationEditForm locationEditForm = new LocationEditForm();
        
        locationEditForm.setRandomId(strRandomLocationId);
        locationEditForm.setName(location.getName());
        locationEditForm.setDescription(location.getDescription());
        locationEditForm.setIsUnassigned(location.getIsUnassigned());
        locationEditForm.setPermitIssueTypeId(location.getPermitIssueType().getId());
        
        final Integer customerId = resolveCustomerId(request, response);
        if (location.getIsParent()) {
            locationEditForm = updateLocationEditFormAsParent(locationEditForm, keyMapping, location, customerId);
        } else {
            locationEditForm = updateLocationEditFormAsChild(locationEditForm, keyMapping, location, customerId);
            locationEditForm = updateLocationEditFormHoursOfOperation(locationEditForm, location);
        }
        
        if (this.rateProfileHelper != null && this.rateProfileLocationService != null) {
            this.rateProfileHelper.clearLocationSessionData(request, strRandomLocationId);
            locationEditForm.setRateProfilesList(this.rateProfileHelper
                    .aggregateRateProfileLocationList(this.rateProfileLocationService.findPublishedByLocationId(locationId, customerId),
                                                      this.rateProfileHelper.getLocationSessionData(request, strRandomLocationId), keyMapping));
        }
        
        return WidgetMetricsHelper.convertToJson(locationEditForm, LOCATION_FORM_DETAILS, true);
    }
    
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    private List<LocationTree> createLocationTree(final List<Location> locations, final Integer customerId) {
        /* Iterate through the list of locations to create a tree structure based off parent/child relationships to return to the front end */
        final List<LocationTree> locationTree = new ArrayList<LocationTree>();
        final Collection<PosEventCurrent> activePosEventCurrentAlerts = this.posEventCurrentService.findActivePosEventCurrentByCustomerId(customerId);
        final Map<Integer, Integer> locationIdToAlertSeverityMap = mapLocationIdToAlertSeverity(locations, activePosEventCurrentAlerts);
        
        for (int i = 0; i < locations.size();) {
            
            final Location loc = locations.get(i);
            
            if (loc.getIsDeleted()) {
                locations.remove(i);
                continue;
            }
            
            if (loc.getLocation() == null || loc.getLocation().getIsUnassigned()) {
                /* loc is a root parent location or a child of unassigned */
                Integer severity = null;
                if (!loc.getIsParent()) {
                    severity = locationIdToAlertSeverityMap.get(loc.getId());
                }
                
                locationTree.add(new LocationTree(loc.getRandomId(), loc.getName(), loc.getIsParent(), loc.getIsUnassigned(), loc.getId().intValue(),
                        severity == null ? 0 : severity));
                locations.remove(i);
                i = 0;
                continue;
                
            } else {
                /* Loops through the existing list of parent locations */
                boolean isParentFound = false;
                existingTree: for (LocationTree existingLocTree : locationTree) {
                    
                    /* If the parent is found in the existing tree */
                    
                    if (existingLocTree.getId() == loc.getLocation().getId().intValue()) {
                        
                        final Integer severity = locationIdToAlertSeverityMap.get(loc.getId());
                        
                        /* add the child to it's parent, remove the child from the iterator and exit the loops */
                        existingLocTree.addChild(new LocationTree(loc.getRandomId(), loc.getName(), loc.getIsParent(), loc.getIsUnassigned(),
                                loc.getId(), severity == null ? 0 : severity));
                        locations.remove(i);
                        isParentFound = true;
                        if (severity != null
                            && (existingLocTree.getSeverity() == null || existingLocTree.getSeverity().intValue() < severity.intValue())) {
                            existingLocTree.setSeverity(severity);
                        }
                        
                        break existingTree;
                    }
                }
                if (isParentFound) {
                    i = 0;
                } else {
                    i++;
                }
            }
        }
        return locationTree;
    }
    
    private Map<Integer, Integer> mapLocationIdToAlertSeverity(final List<Location> locations,
        final Collection<PosEventCurrent> activePosEventCurrentAlerts) {
        
        final Map<Integer, Integer> locationIdToAlertSeverityMap = new HashMap<Integer, Integer>();
        for (Location location : locations) {
            locationIdToAlertSeverityMap.put(location.getId(), getHighestSeverity(activePosEventCurrentAlerts, location));
        }
        return locationIdToAlertSeverityMap;
    }
    
    private Integer getHighestSeverity(final Collection<PosEventCurrent> activePosEventCurrentAlerts, final Location loc) {
        
        Integer severity = null;
        final Iterator<PosEventCurrent> activePosEventCurrentAlertIterator = activePosEventCurrentAlerts.iterator();
        while (activePosEventCurrentAlertIterator.hasNext()) {
            final PosEventCurrent activePosEventCurrentAlert = activePosEventCurrentAlertIterator.next();
            if (activePosEventCurrentAlert.getPointOfSale().getLocation().getId().intValue() == loc.getId().intValue()) {
                if (severity == null || severity < activePosEventCurrentAlert.getEventSeverityType().getId()) {
                    severity = activePosEventCurrentAlert.getEventSeverityType().getId();
                }
                if (severity == WebCoreConstants.SEVERITY_CRITICAL) {
                    activePosEventCurrentAlertIterator.remove();
                    continue;
                }
                activePosEventCurrentAlertIterator.remove();
            }
        }
        return severity;
    }
    
    private LocationEditForm updateLocationEditFormHoursOfOperation(final LocationEditForm locationEditForm, final Location location) {
        
        final List<LocationDay> locationDays = this.locationService.findLocationDayByLocationId(location.getId());
        
        final List<Integer> locationOpen = new ArrayList<Integer>();
        final List<Integer> locationClose = new ArrayList<Integer>();
        final List<Boolean> openEntireDay = new ArrayList<Boolean>();
        final List<Boolean> closedEntireDay = new ArrayList<Boolean>();
        
        for (LocationDay locationDay : locationDays) {
            locationOpen.add(locationDay.getOpenQuarterHourNumber());
            locationClose.add(locationDay.getCloseQuarterHourNumber());
            openEntireDay.add(locationDay.isIsOpen24hours());
            closedEntireDay.add(locationDay.isIsClosed());
        }
        
        locationEditForm.setLocationOpen(locationOpen);
        locationEditForm.setLocationClose(locationClose);
        locationEditForm.setOpenEntireDay(openEntireDay);
        locationEditForm.setClosedEntireDay(closedEntireDay);
        
        return locationEditForm;
    }
    
    @SuppressWarnings("checkstyle:NPathComplexityCheck")
    private LocationEditForm updateLocationEditFormAsParent(final LocationEditForm locationEditForm, final RandomKeyMapping keyMapping,
        final Location location, final Integer customerId) {
        
        final List<Location> childLocations = this.locationService.getLocationsByParentLocationId(location.getId());
        final WidgetMapInfo mapInfo = new WidgetMapInfo();
        
        final List<Location> unselectedLocations = this.locationService.findLocationsByCustomerId(customerId);
        
        if (unselectedLocations != null) {
            if (childLocations != null) {
                unselectedLocations.removeAll(childLocations);
            }
            for (Location unselectedLocation : unselectedLocations) {
                unselectedLocation.setRandomId(keyMapping.getRandomString(unselectedLocation, WebCoreConstants.ID_LOOK_UP_NAME));
                if (!unselectedLocation.equals(location) && !unselectedLocation.getIsParent() && !unselectedLocation.getIsUnassigned()) {
                    locationEditForm.addChildLocation(unselectedLocation.getName(), unselectedLocation.getRandomId(), WebCoreConstants.ACTIVE);
                }
            }
        }
        
        if (childLocations != null) {
            final List<Integer> locationIds = new ArrayList<Integer>();
            for (Location child : childLocations) {
                child.setRandomId(keyMapping.getRandomString(child, WebCoreConstants.ID_LOOK_UP_NAME));
                locationIds.add(child.getId());
                
                locationEditForm.addChildLocation(child.getName(), keyMapping.getRandomString(child, WebCoreConstants.ID_LOOK_UP_NAME).toString(),
                                                  WebCoreConstants.SELECTED);
                
                final List<PointOfSale> pointOfSaleList = this.pointOfSaleService.findPointOfSalesByLocationIdOrderByName(child.getId());
                if (pointOfSaleList != null) {
                    for (PointOfSale pointOfSale : pointOfSaleList) {
                        pointOfSale.setRandomId(keyMapping.getRandomString(pointOfSale, WebCoreConstants.ID_LOOK_UP_NAME));
                        
                        mapInfo.addMapEntry(pointOfSale.getPaystation().getPaystationType().getId(), pointOfSale, pointOfSale.getId(),
                                            getSeverity(pointOfSale), null, null, 0, null, null);
                    }
                }
            }
            final int unplacedCount = this.pointOfSaleService.countUnPlacedByLocationIds(locationIds);
            if (unplacedCount > 0) {
                mapInfo.setMissingPayStation(true);
            }
            
            //sum spaces and target from children
            int parentCapacity = 0;
            int parentTargetRevenue = 0;
            for (Location child : childLocations) {
                parentCapacity += child.getNumberOfSpaces();
                parentTargetRevenue += child.getTargetMonthlyRevenueAmount();
            }
            locationEditForm.setCapacity(Integer.toString(parentCapacity));
            locationEditForm.setTargetMonthlyRevenue(new BigDecimal(parentTargetRevenue).divide(new BigDecimal(ONE_HUNDRED)).toString());
        }
        
        locationEditForm.setIsParent(true);
        locationEditForm.setMapInfo(mapInfo);
        
        final List<PointOfSale> pointOfSales = this.pointOfSaleService.findValidPointOfSaleByCustomerIdOrderByName(customerId);
        
        if (pointOfSales != null) {
            for (PointOfSale pointOfSale : pointOfSales) {
                pointOfSale.setRandomId(keyMapping.getRandomString(pointOfSale, WebCoreConstants.ID_LOOK_UP_NAME));
                locationEditForm.addPayStation(pointOfSale.getName(), pointOfSale.getRandomId(), WebCoreConstants.ACTIVE,
                                               pointOfSale.getPaystation().getPaystationType().getId(), 0);
            }
        }
        
        return locationEditForm;
    }
    
    @SuppressWarnings("checkstyle:NPathComplexityCheck")
    private LocationEditForm updateLocationEditFormAsChild(final LocationEditForm locationEditForm, final RandomKeyMapping keyMapping,
        final Location location, final Integer customerId) {
        
        final List<PointOfSale> pointOfSales = this.pointOfSaleService.findPointOfSalesByLocationIdOrderByName(location.getId());
        final WidgetMapInfo mapInfo = new WidgetMapInfo();
        
        final List<PointOfSale> unselectedPointOfSales = this.pointOfSaleService.findValidPointOfSaleByCustomerIdOrderByName(customerId);
        
        if (pointOfSales != null && unselectedPointOfSales != null) {
            unselectedPointOfSales.removeAll(pointOfSales);
            for (PointOfSale pointOfSale : unselectedPointOfSales) {
                pointOfSale.setRandomId(keyMapping.getRandomString(pointOfSale, WebCoreConstants.ID_LOOK_UP_NAME));
                
                locationEditForm.addPayStation(pointOfSale.getName(), pointOfSale.getSerialNumber(), pointOfSale.getRandomId(),
                                               WebCoreConstants.ACTIVE, pointOfSale.getPaystation().getPaystationType().getId(),
                                               getSeverity(pointOfSale));
            }
            
        }
        
        if (pointOfSales != null) {
            
            //set Pay Stations randomID and initialize map data
            for (PointOfSale pointOfSale : pointOfSales) {
                pointOfSale.setRandomId(keyMapping.getRandomString(pointOfSale, WebCoreConstants.ID_LOOK_UP_NAME));
                
                mapInfo.addMapEntry(pointOfSale.getPaystation().getPaystationType().getId(), pointOfSale, pointOfSale.getId(),
                                    getSeverity(pointOfSale), null, null, 0, null, null);
                
                locationEditForm.addPayStation(pointOfSale.getName(), pointOfSale.getSerialNumber(), pointOfSale.getRandomId(),
                                               WebCoreConstants.SELECTED, pointOfSale.getPaystation().getPaystationType().getId(),
                                               getSeverity(pointOfSale));
                
            }
        }
        
        final List<FlexLocationFacility> selectedFlexLocationFacility = this.flexWSService.findFlexLocationFacilityByLocationId(location.getId());
        
        if (selectedFlexLocationFacility != null) {
            for (FlexLocationFacility flexLocationFacility : selectedFlexLocationFacility) {
                flexLocationFacility.setRandomId(keyMapping.getRandomString(flexLocationFacility, WebCoreConstants.ID_LOOK_UP_NAME));
                locationEditForm.addFacility(flexLocationFacility.getFacCode(), flexLocationFacility.getFacDescription(),
                                             flexLocationFacility.getRandomId(), WebCoreConstants.SELECTED);
            }
        }
        
        final List<FlexLocationProperty> selectedFlexLocationProperty = this.flexWSService.findFlexLocationPropertyByLocationId(location.getId());
        
        if (selectedFlexLocationProperty != null) {
            for (FlexLocationProperty flexLocationProperty : selectedFlexLocationProperty) {
                flexLocationProperty.setRandomId(keyMapping.getRandomString(flexLocationProperty, WebCoreConstants.ID_LOOK_UP_NAME));
                locationEditForm.addProperty(flexLocationProperty.getProUid(), flexLocationProperty.getProName(), flexLocationProperty.getRandomId(),
                                             WebCoreConstants.SELECTED);
            }
        }
        
        final List<CaseLocationLot> selectedCaseLocationLot = this.caseService.findCaseLocationLotByLocationId(location.getId());
        
        if (selectedCaseLocationLot != null) {
            for (CaseLocationLot caseLocationLot : selectedCaseLocationLot) {
                caseLocationLot.setRandomId(keyMapping.getRandomString(caseLocationLot, WebCoreConstants.ID_LOOK_UP_NAME));
                locationEditForm.addLot(caseLocationLot.getLotName(), caseLocationLot.getRandomId(), WebCoreConstants.SELECTED);
            }
        }
        
        // adding geopoints
        addGeopoints(mapInfo, location);
        
        locationEditForm.setIsParent(false);
        locationEditForm.setCapacity(Integer.toString(location.getNumberOfSpaces()));
        locationEditForm
                .setTargetMonthlyRevenue(new BigDecimal(location.getTargetMonthlyRevenueAmount()).divide(new BigDecimal(ONE_HUNDRED)).toString());
        locationEditForm.setMapInfo(mapInfo);
        
        if (location.getLocation() != null) {
            locationEditForm.setParentLocation(location.getLocation().getName());
            final Location parent = this.locationService.findLocationById(location.getLocation().getId());
            locationEditForm.setParentLocationId(keyMapping.getRandomString(parent, WebCoreConstants.ID_LOOK_UP_NAME));
        }
        
        locationEditForm.setSelectedChildLocationRandomIds(new ArrayList<String>());
        final List<Location> locations = this.locationService.findLocationsByCustomerId(customerId);
        
        if (locations != null) {
            for (Location childLocation : locations) {
                if (!childLocation.getIsUnassigned() && !childLocation.getIsDeleted()) {
                    childLocation.setRandomId(keyMapping.getRandomString(childLocation, WebCoreConstants.ID_LOOK_UP_NAME));
                    if (!childLocation.equals(location) && !childLocation.getIsParent()) {
                        locationEditForm.addChildLocation(childLocation.getName(), childLocation.getRandomId(), WebCoreConstants.ACTIVE);
                    }
                }
            }
        }
        
        return locationEditForm;
    }
    
    private void addGeopoints(final WidgetMapInfo mapInfo, final Location location) {
        if (location.getId() != null) {
            final LocationLineGeopoint locationLine = this.locationLineGeopointService.getLocationLineGeopointByLocationId(location.getId());
            if (locationLine != null) {
                final List<GeoLocationEntry> geoLocationInfo = new ArrayList<GeoLocationEntry>();
                final GeoLocationEntry geoLocationEntry = new GeoLocationEntry();
                geoLocationEntry.setRandomId(new WebObjectId(Location.class, location.getId()));
                geoLocationEntry.setLocationName(location.getName());
                geoLocationEntry.setJsonString(locationLine.getJsonstring());
                this.locationLineGeopointService.findOccupancyForGeoPoint(geoLocationEntry, location.getId());
                geoLocationInfo.add(geoLocationEntry);
                mapInfo.setGeoLocationInfo(geoLocationInfo);
            }
        }
        
    }
    
    /**
     * This method takes a WebSecurityForm from the model and after validating
     * the contents of the location edit form, saves the location information to
     * the database. It may also save pay station information, child location
     * information, and open close times.
     * 
     * @param webSecurityForm
     * @param result
     * @param request
     * @param response
     * @param model
     * @return A response of true for a successful save, JSON document for
     *         validation errors, or null for database records not found
     */
    @SuppressWarnings({ "PMD.AvoidInstantiatingObjectsInLoops", "PMD.UselessParentheses" })
    protected final String saveLocationSuper(
        @ModelAttribute(WebSecurityConstants.WEB_SECURITY_FORM) final WebSecurityForm<LocationEditForm> webSecurityForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        final WebUser user = (WebUser) WebSecurityUtil.getAuthentication().getPrincipal();
        final Customer customer = this.customerAdminService.findCustomerByCustomerId(resolveCustomerId(request, response));
        
        boolean isMigrated = true;
        if ((!user.isSystemAdmin() && !user.getIsMigrated()) || (user.isSystemAdmin() && !customer.isIsMigrated())) {
            isMigrated = false;
        }
        
        /* validate user input. */
        this.locationValidator.validate(webSecurityForm, result, isMigrated, customer.getId());
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), ERROR_STATUS, true);
        }
        webSecurityForm.resetToken();
        
        final LocationEditForm form = webSecurityForm.getWrappedObject();
        
        Location location = null;
        
        if (form.getLocationId() == 0) {
            // this is a new location
            location = new Location();
            
        } else {
            //this is editing an existing location
            location = this.locationService.findLocationById(form.getLocationId());
            if (location == null) {
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return null;
            }
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        if (!form.isUnassigned()) {
            location.setIsParent(form.isParent());
            location.setName(form.getName().trim());
            location.setDescription(form.getDescription());
            location.setCustomer(customer);
            location.setIsUnassigned(false);
            location.setIsDeleted(false);
            location.setPermitIssueType(this.permitIssueTypeService.findPermitIssueTypeById(form.getPermitIssueTypeId()));
            
            if (!form.isParent()) {
                if (form.getCapacity() == null) {
                    location.setNumberOfSpaces(0);
                } else {
                    location.setNumberOfSpaces(Integer.parseInt(form.getCapacity()));
                }
                if (form.getTargetMonthlyRevenue() == null) {
                    location.setTargetMonthlyRevenueAmount(0);
                } else {
                    location.setTargetMonthlyRevenueAmount(new BigDecimal(form.getTargetMonthlyRevenue()).multiply(new BigDecimal(ONE_HUNDRED))
                            .intValue());
                }
            }
            
            if (form.getParentLocationRealId() == 0) {
                location.setLocation(null);
            } else {
                final Location parentLocation = this.locationService.findLocationById(form.getParentLocationRealId());
                if (parentLocation == null) {
                    response.setStatus(HttpStatus.NOT_FOUND.value());
                    return null;
                }
                location.setLocation(parentLocation);
            }
            
            location.setLastModifiedGmt(new Date());
            location.setLastModifiedByUserId(userAccount.getId());
            this.locationService.saveOrUpdateLocation(location);
            
            if (isMigrated && !updateChildLocations(location, customer, form, userAccount)) {
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return null;
                
            }
            if (form.isParent()) {
                this.locationService.deleteLocationOpenByLocationId(location.getId());
            } else {
                updateLocationOpenCloseTimeOnForm(form, location, userAccount);
            }
        }
        if (isMigrated) {
            if (!updatePayStations(location, customer, form, userAccount)) {
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return null;
            }
            if (!updateFacilities(location, customer, form, userAccount)) {
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return null;
            }
            if (!updateProperties(location, customer, form, userAccount)) {
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return null;
            }
            if (!updateLots(location, form, userAccount)) {
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return null;
            }
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        if (this.rateProfileHelper != null) {
            final Date now = new Date();
            final Map<String, RateProfileLocationForm> sessionData = this.rateProfileHelper.getLocationSessionData(request, form.getRandomId());
            if (sessionData != null) {
                final List<RateProfileLocation> updatedRates = new ArrayList<RateProfileLocation>(sessionData.size());
                final Set<Map.Entry<String, RateProfileLocationForm>> entriesSet = sessionData.entrySet();
                for (Map.Entry<String, RateProfileLocationForm> entry : entriesSet) {
                    final RateProfileLocationForm rplForm = entry.getValue();
                    
                    final RateProfileLocation domain =
                            this.rateProfileHelper.populate(rplForm, new RateProfileLocation(), keyMapping, customer, location);
                    domain.setLastModifiedByUserId(userAccount.getId());
                    domain.setLastModifiedGmt(now);
                    
                    domain.setIsPublished(true);
                    domain.setNextPublisGmt(now);
                    
                    updatedRates.add(domain);
                }
                
                this.rateProfileLocationService.saveAll(updatedRates, customer.getId());
            }
        }
        
        if (this.rateProfileHelper != null) {
            this.rateProfileHelper.clearLocationSessionData(request, form.getRandomId());
        }
        
        return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(WebCoreConstants.COLON).append(webSecurityForm.getInitToken()).toString();
    }
    
    @SuppressWarnings("checkstyle:CyclomaticComplexityCheck")
    private void updateLocationOpenCloseTimeOnForm(final LocationEditForm form, final Location location, final UserAccount userAccount) {
        
        for (int dayOfWeek = 0; dayOfWeek < SEVEN_WEEK_DAYS; dayOfWeek++) {
            Integer openTime = null;
            Integer closeTime = null;
            Integer closeTimeOfPreviousDay = null;
            boolean open = false;
            boolean closed = false;
            if (form.getLocationOpen() != null && form.getLocationOpen().size() == SEVEN_WEEK_DAYS) {
                openTime = form.getLocationOpen().get(dayOfWeek);
            }
            if (form.getLocationClose() != null && form.getLocationClose().size() == SEVEN_WEEK_DAYS) {
                closeTime = form.getLocationClose().get(dayOfWeek);
                if (dayOfWeek == 0) {
                    closeTimeOfPreviousDay = form.getLocationClose().get(SIX_WEEK_DAYS);
                } else {
                    closeTimeOfPreviousDay = form.getLocationClose().get(dayOfWeek - 1);
                }
            }
            if (form.getOpenEntireDay() != null && form.getOpenEntireDay().size() == SEVEN_WEEK_DAYS) {
                open = form.getOpenEntireDay().get(dayOfWeek);
            }
            if (form.getClosedEntireDay() != null && form.getClosedEntireDay().size() == SEVEN_WEEK_DAYS) {
                closed = form.getClosedEntireDay().get(dayOfWeek);
            }
            //if ((openTime == null || closeTime == null) && closed == false) {
            if ((openTime == null || closeTime == null) && !closed) {
                // default to 24 hour a day open if no open/close time is specified.
                open = true;
            }
            this.locationService.updateLocationOpenCloseTime(userAccount.getId(), location.getId(), dayOfWeek + 1, openTime, closeTime,
                                                             closeTimeOfPreviousDay, open, closed);
        }
        
    }
    
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    private boolean updateChildLocations(final Location location, final Customer customer, final LocationEditForm form,
        final UserAccount userAccount) {
        
        List<Location> locations = new ArrayList<Location>();
        if (location.getId() != null) {
            locations = this.locationService.getLocationsByParentLocationId(location.getId());
            if (locations != null && !locations.isEmpty()) {
                for (Location childLocation : locations) {
                    final Location unassignedLocation = this.locationService.findUnassignedLocationByCustomerId(customer.getId());
                    boolean isRemoved = true;
                    if (form.getChildLocationIdList() != null) {
                        for (Integer childLocationId : form.getChildLocationIdList()) {
                            if (childLocationId.intValue() == childLocation.getId().intValue()) {
                                isRemoved = false;
                                break;
                            }
                        }
                    }
                    if (isRemoved) {
                        childLocation.setLocation(unassignedLocation);
                        location.removeLocation(childLocation);
                        childLocation.setLastModifiedGmt(new Date());
                        childLocation.setLastModifiedByUserId(userAccount.getId());
                        this.locationService.saveOrUpdateLocation(childLocation);
                    }
                }
            }
        }
        if (form.getChildLocationIdList() != null) {
            for (Integer childLocationId : form.getChildLocationIdList()) {
                final Location childLocation = this.locationService.findLocationById(childLocationId);
                if (childLocation == null) {
                    return false;
                }
                childLocation.setLocation(location);
                location.addLocation(childLocation);
                childLocation.setLastModifiedGmt(new Date());
                childLocation.setLastModifiedByUserId(userAccount.getId());
                this.locationService.saveOrUpdateLocation(childLocation);
            }
        }
        return true;
    }
    
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    private boolean updatePayStations(final Location location, final Customer customer, final LocationEditForm form, final UserAccount userAccount) {
        
        List<PointOfSale> pointOfSaleList = new ArrayList<PointOfSale>();
        if (location.getId() != null) {
            pointOfSaleList = this.pointOfSaleService.findPointOfSalesByLocationIdOrderByName(location.getId());
            if (pointOfSaleList != null && !pointOfSaleList.isEmpty()) {
                final Location unassignedLocation = this.locationService.findUnassignedLocationByCustomerId(customer.getId());
                for (PointOfSale pointOfSale : pointOfSaleList) {
                    boolean isRemoved = true;
                    if (form.getPointOfSaleIdList() != null) {
                        for (Integer pointOfSaleId : form.getPointOfSaleIdList()) {
                            if (pointOfSaleId.intValue() == pointOfSale.getId().intValue()) {
                                isRemoved = false;
                                break;
                            }
                        }
                    }
                    if (isRemoved) {
                        pointOfSale.setLocation(unassignedLocation);
                        pointOfSale.setLastModifiedGmt(new Date());
                        pointOfSale.setLastModifiedByUserId(userAccount.getId());
                        this.customerAdminService.saveOrUpdatePointOfSale(pointOfSale);
                    }
                }
            }
        }
        if (form.getPointOfSaleIdList() != null) {
            for (Integer pointOfSaleId : form.getPointOfSaleIdList()) {
                final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
                if (pointOfSale == null) {
                    return false;
                }
                if (pointOfSale.getCustomer().getId().intValue() != customer.getId().intValue()) {
                    continue;
                }
                pointOfSale.setLocation(location);
                pointOfSale.setLastModifiedGmt(new Date());
                pointOfSale.setLastModifiedByUserId(userAccount.getId());
                this.customerAdminService.saveOrUpdatePointOfSale(pointOfSale);
            }
        }
        return true;
    }
    
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    private boolean updateFacilities(final Location location, final Customer customer, final LocationEditForm form, final UserAccount userAccount) {
        
        List<FlexLocationFacility> selectedFlexLocationFacilityList = new ArrayList<FlexLocationFacility>();
        if (location.getId() != null) {
            selectedFlexLocationFacilityList = this.flexWSService.findFlexLocationFacilityByLocationId(location.getId());
            if (selectedFlexLocationFacilityList != null && !selectedFlexLocationFacilityList.isEmpty()) {
                for (FlexLocationFacility flexLocationFacility : selectedFlexLocationFacilityList) {
                    boolean isRemoved = true;
                    if (form.getSelectedFacilityIdList() != null) {
                        for (Integer flexLocationFacilityId : form.getSelectedFacilityIdList()) {
                            if (flexLocationFacilityId.intValue() == flexLocationFacility.getId().intValue()) {
                                isRemoved = false;
                                break;
                            }
                        }
                    }
                    if (isRemoved) {
                        flexLocationFacility.setLocation(null);
                        flexLocationFacility.setLastModifiedGmt(new Date());
                        flexLocationFacility.setLastModifiedByUserId(userAccount.getId());
                        this.flexWSService.updateFlexLocationFacility(flexLocationFacility);
                    }
                }
            }
        }
        if (form.getSelectedFacilityIdList() != null) {
            for (Integer flexLocationFacilityId : form.getSelectedFacilityIdList()) {
                final FlexLocationFacility flexLocationFacility = this.flexWSService.findFlexLocationFacilityById(flexLocationFacilityId);
                if (flexLocationFacility == null) {
                    return false;
                }
                if (flexLocationFacility.getCustomer().getId().intValue() != customer.getId().intValue()) {
                    continue;
                }
                flexLocationFacility.setLocation(location);
                flexLocationFacility.setLastModifiedGmt(new Date());
                flexLocationFacility.setLastModifiedByUserId(userAccount.getId());
                this.flexWSService.updateFlexLocationFacility(flexLocationFacility);
            }
        }
        return true;
    }
    
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    private boolean updateProperties(final Location location, final Customer customer, final LocationEditForm form, final UserAccount userAccount) {
        
        List<FlexLocationProperty> selectedFlexLocationPropertyList = new ArrayList<FlexLocationProperty>();
        if (location.getId() != null) {
            selectedFlexLocationPropertyList = this.flexWSService.findFlexLocationPropertyByLocationId(location.getId());
            if (selectedFlexLocationPropertyList != null && !selectedFlexLocationPropertyList.isEmpty()) {
                for (FlexLocationProperty flexLocationProperty : selectedFlexLocationPropertyList) {
                    boolean isRemoved = true;
                    if (form.getSelectedPropertyIdList() != null) {
                        for (Integer flexLocationPropertyId : form.getSelectedPropertyIdList()) {
                            if (flexLocationPropertyId.intValue() == flexLocationProperty.getId().intValue()) {
                                isRemoved = false;
                                break;
                            }
                        }
                    }
                    if (isRemoved) {
                        flexLocationProperty.setLocation(null);
                        flexLocationProperty.setLastModifiedGmt(new Date());
                        flexLocationProperty.setLastModifiedByUserId(userAccount.getId());
                        this.flexWSService.updateFlexLocationProperty(flexLocationProperty);
                    }
                }
            }
        }
        if (form.getSelectedPropertyIdList() != null) {
            for (Integer flexLocationPropertyId : form.getSelectedPropertyIdList()) {
                final FlexLocationProperty flexLocationProperty = this.flexWSService.findFlexLocationPropertyById(flexLocationPropertyId);
                if (flexLocationProperty == null) {
                    return false;
                }
                if (flexLocationProperty.getCustomer().getId().intValue() != customer.getId().intValue()) {
                    continue;
                }
                flexLocationProperty.setLocation(location);
                flexLocationProperty.setLastModifiedGmt(new Date());
                flexLocationProperty.setLastModifiedByUserId(userAccount.getId());
                this.flexWSService.updateFlexLocationProperty(flexLocationProperty);
            }
        }
        return true;
    }
    
    private boolean updateLots(final Location location, final LocationEditForm form, final UserAccount userAccount) {
        List<CaseLocationLot> selectedCaseLocationLotList = new ArrayList<CaseLocationLot>();
        if (location.getId() != null) {
            selectedCaseLocationLotList = this.caseService.findCaseLocationLotByLocationId(location.getId());
            final Map<Integer, CaseLocationLot> selectedCaseLocationLotMap = new HashMap<Integer, CaseLocationLot>();
            if (selectedCaseLocationLotList != null && !selectedCaseLocationLotList.isEmpty()) {
                for (CaseLocationLot caseLocationLot : selectedCaseLocationLotList) {
                    selectedCaseLocationLotMap.put(caseLocationLot.getId(), caseLocationLot);
                    
                }
            }
            if (form.getSelectedLotIdList() != null) {
                for (Integer caseLocationLotId : form.getSelectedLotIdList()) {
                    if (selectedCaseLocationLotMap.containsKey(caseLocationLotId)) {
                        selectedCaseLocationLotMap.remove(caseLocationLotId);
                    } else {
                        final CaseLocationLot caseLocationLot = this.caseService.findCaseLocationLotById(caseLocationLotId);
                        caseLocationLot.setLocation(location);
                        caseLocationLot.setOccupancy(null);
                        caseLocationLot.setTimeIdGmt(null);
                        caseLocationLot.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
                        caseLocationLot.setLastModifiedByUserId(userAccount.getId());
                        this.caseService.updateCaseLocationLot(caseLocationLot);
                    }
                }
            }
            for (CaseLocationLot caseLocationLot : selectedCaseLocationLotMap.values()) {
                caseLocationLot.setLocation(null);
                caseLocationLot.setOccupancy(null);
                caseLocationLot.setTimeIdGmt(null);
                caseLocationLot.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
                caseLocationLot.setLastModifiedByUserId(userAccount.getId());
                this.caseService.updateCaseLocationLot(caseLocationLot);
            }
        }
        return true;
    }
    
    /**
     * This method verifies that a location can be deleted. It checks for
     * children and permits, and returns true or false.
     * 
     * @param request
     * @param response
     * @param model
     * @return "true" or "false" depending on successful validation
     */
    protected final String verifyDeleteLocationSuper(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        if (((WebUser) WebSecurityUtil.getAuthentication().getPrincipal()).isSystemAdmin()) {
            final String customerRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
            final Integer customerId = (Integer) keyMapping.getKey(customerRandomId);
            
            final Customer customer = this.customerService.findCustomer(customerId);
            
            if (!customer.isIsMigrated()) {
                return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON
                       + this.locationValidator.getMessage(ERROR_SYSADMIN_CUSTOMER_NOT_MIGRATED);
            }
        } else if (!WebSecurityUtil.isCustomerMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON + this.locationValidator.getMessage(ERROR_CUSTOMER_NOT_MIGRATED);
        }
        
        final String strLocationId = request.getParameter(LOCATION_ID);
        final int locationId = verifyRandomIdAndReturnActual(response, keyMapping, strLocationId);
        
        final Location location = this.locationService.findLocationById(locationId);
        if (location == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        if (this.locationService.locationContainsPermitsById(locationId)) {
            //non-standard response code - returned if trying to delete a location which has active permits
            response.setHeader(WebCoreConstants.HEADER_CUSTOM_STATUS_CODE, Integer.toString(WebCoreConstants.HTTP_STATUS_CODE_482));
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        if (this.locationService.getLocationsByParentLocationId(locationId) != null
            && this.locationService.getLocationsByParentLocationId(locationId).size() > 0) {
            //non-standard response code - returned if trying to delete a location which has child locations
            response.setHeader(WebCoreConstants.HEADER_CUSTOM_STATUS_CODE, Integer.toString(WebCoreConstants.HTTP_STATUS_CODE_480));
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        if (this.pointOfSaleService.findPointOfSalesByLocationId(locationId) != null
            && this.pointOfSaleService.findPointOfSalesByLocationId(locationId).size() > 0) {
            //non-standard response code - returned if trying to delete a location which has pay stations
            response.setHeader(WebCoreConstants.HEADER_CUSTOM_STATUS_CODE, Integer.toString(WebCoreConstants.HTTP_STATUS_CODE_481));
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    /**
     * This method takes a location Id and deletes it from the database if it
     * passes validation from verifyDeleteLocation and if the suer has valid
     * permissions
     * 
     * @param request
     * @param response
     * @param model
     * @return True or false depending on success of the deletion.
     */
    protected final String deleteLocationSuper(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        if (((WebUser) WebSecurityUtil.getAuthentication().getPrincipal()).isSystemAdmin()) {
            final String customerRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
            final Integer customerId = (Integer) keyMapping.getKey(customerRandomId);
            final Customer customer = this.customerService.findCustomer(customerId);
            if (!customer.isIsMigrated()) {
                return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON
                       + this.locationValidator.getMessage(ERROR_SYSADMIN_CUSTOMER_NOT_MIGRATED);
            }
        } else if (!WebSecurityUtil.isCustomerMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON + this.locationValidator.getMessage(ERROR_CUSTOMER_NOT_MIGRATED);
        }
        
        final String strLocationId = request.getParameter(LOCATION_ID);
        
        if (!DashboardUtil.validateRandomId(strLocationId)) {
            LOGGER.warn("+++ Invalid randomised locationID in LocationsController.deleteLocation() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Object actualLocationId = keyMapping.getKey(strLocationId);
        
        if (!DashboardUtil.validateDBIntegerPrimaryKey(actualLocationId.toString())) {
            LOGGER.warn("+++ Invalid locationID in LocationsController.deleteLocation() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final int locationId = Integer.parseInt(actualLocationId.toString());
        
        final Location location = this.locationService.findLocationById(locationId);
        if (location == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return null;
        }
        
        if (verifyDeleteLocationSuper(request, response, model).equals(WebCoreConstants.RESPONSE_TRUE)) {
            this.locationService.deleteLocationById(locationId);
            return WebCoreConstants.RESPONSE_TRUE;
        } else {
            return WebCoreConstants.RESPONSE_FALSE;
        }
    }
    
    protected final String cancelLocationFormSuper(final HttpServletRequest request, final HttpServletResponse response,
        final String locationRandomId, final ModelMap model) {
        this.rateProfileHelper.clearLocationSessionData(request, locationRandomId);
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    protected final String listRateRateProfilesSuper(final HttpServletRequest request, final HttpServletResponse response,
        final String locationRandomId, final String startDateStr, final String endDateStr, final ModelMap model) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final Integer locationId = keyMapping.getKey(locationRandomId, Location.class, Integer.class);
        if (locationId == null || startDateStr == null || endDateStr == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Location location = this.locationService.findLocationById(locationId);
        if (location == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Date startDate = DateUtil.createStartDate(startDateStr, TimeZone.getTimeZone(WebCoreConstants.GMT));
        final Date endDate = DateUtil.createEndDate(endDateStr, TimeZone.getTimeZone(WebCoreConstants.GMT));
        final Integer customerId = resolveCustomerId(request, response);
        
        final List<RateRateProfile> rateRateProfileList =
                this.rateRateProfileService.findPublishedRatesByLocationIdAndDateRange(locationId, customerId, startDate, endDate);
        final List<RateRateProfileEditForm> result =
                this.rateProfileHelper.populateRateRateProfileEditFormList(rateRateProfileList, new HashMap<String, RateRateProfileEditForm>(2),
                                                                           startDateStr, endDateStr, keyMapping);
        
        return WidgetMetricsHelper.convertToJson(result, "rateRateProfileListInfo", true);
    }
    
    public final WebSecurityForm<RateProfileLocationForm> initEditLocationForm(final HttpServletRequest request) {
        return new WebSecurityForm<RateProfileLocationForm>((Object) null, new RateProfileLocationForm(),
                SessionTool.getInstance(request).locatePostToken(RateProfileLocationController.FORM_EDIT_LOCATION));
    }
    
    /*
     * Pay station related functions are in the "Pay Station List" section of this controller.
     * Map function is located in the "Pay Station Placement" section of this controller
     */
    
    protected final Integer verifyRandomIdAndReturnActual(final HttpServletResponse response, final RandomKeyMapping keyMapping,
        final String strRandomLocationId) {
        
        final String[] msgs = new String[] { "+++ Invalid randomised locationID in LocationsController. +++",
            "+++ Invalid locationID in LocationsController method. +++" };
        return WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, strRandomLocationId, msgs);
    }
    
    private Integer resolveCustomerId(final HttpServletRequest request, final HttpServletResponse response) {
        Integer customerId;
        final Customer customer = WebSecurityUtil.getUserAccount().getCustomer();
        if (customer.getCustomerType() == null || customer.getCustomerType().getId() != WebCoreConstants.CUSTOMER_TYPE_DPT) {
            customerId = customer.getId();
        } else {
            customerId = WebCoreUtil.verifyRandomIdAndReturnActual(response, SessionTool.getInstance(request).getKeyMapping(),
                                                                   request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID),
                                                                   new String[] { "+++ Invalid randomised customerID in LocationsController. +++" });
        }
        
        return customerId;
    }
    
    private Integer getSeverity(final PointOfSale pointOfSale) {
        final int criticalCount = pointOfSale.getPosAlertStatus().getCommunicationCritical() + pointOfSale.getPosAlertStatus().getCollectionCritical()
                                  + pointOfSale.getPosAlertStatus().getPayStationCritical();
        final int majorCount = pointOfSale.getPosAlertStatus().getCommunicationMajor() + pointOfSale.getPosAlertStatus().getCollectionMajor()
                               + pointOfSale.getPosAlertStatus().getPayStationMajor();
        final int minorCount = pointOfSale.getPosAlertStatus().getCommunicationMinor() + pointOfSale.getPosAlertStatus().getCollectionMinor()
                               + pointOfSale.getPosAlertStatus().getPayStationMinor();
        final int severity = criticalCount > 0 ? WebCoreConstants.SEVERITY_CRITICAL : majorCount > 0 ? WebCoreConstants.SEVERITY_MAJOR
                : minorCount > 0 ? WebCoreConstants.SEVERITY_MINOR : WebCoreConstants.SEVERITY_CLEAR;
        
        return severity;
    }
    
    /**
     * This method takes a locationId as input through request parameter
     * "locationID" and gathers details associated to that location to send to
     * the UI for displaying location pay station geo-point, or inserting
     * into the location geo-point edit form for editing
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON string having location geo-point detail information. "false" in case
     *         of any exception or validation failure.
     */
    public final String getLocationGeopoint(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        final String randomLocationId = request.getParameter(LOCATION_ID);
        
        if (!DashboardUtil.validateRandomId(randomLocationId)) {
            LOGGER.warn(INVALID_LOCATION_ID);
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }
        
        return null;
    }
    
    @SuppressWarnings("PMD.UselessParentheses")
    public final String saveLocationGeopoint(
        @ModelAttribute(WebSecurityConstants.WEB_SECURITY_FORM) final WebSecurityForm<LocationLineGeopointEditForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        final WebUser user = (WebUser) WebSecurityUtil.getAuthentication().getPrincipal();
        final Customer customer = this.customerAdminService.findCustomerByCustomerId(resolveCustomerId(request, response));
        
        boolean isMigrated = true;
        if ((!user.isSystemAdmin() && !user.getIsMigrated()) || (user.isSystemAdmin() && !customer.isIsMigrated())) {
            isMigrated = false;
        }
        
        /* validate user input. */
        this.locationGeopointValidator.validate(webSecurityForm, result, isMigrated, customer.getId());
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), ERROR_STATUS, true);
        }
        webSecurityForm.resetToken();
        
        return null;
    }
}
