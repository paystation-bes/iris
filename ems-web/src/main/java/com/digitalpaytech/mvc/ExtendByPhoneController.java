package com.digitalpaytech.mvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.DayOfWeek;
import com.digitalpaytech.domain.ExtensibleRate;
import com.digitalpaytech.domain.ExtensibleRateDayOfWeek;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.ParkingPermission;
import com.digitalpaytech.domain.ParkingPermissionDayOfWeek;
import com.digitalpaytech.domain.UnifiedRate;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.customeradmin.GlobalConfiguration;
import com.digitalpaytech.mvc.customeradmin.CustomerAdminExtendByPhoneController;
import com.digitalpaytech.mvc.customeradmin.support.EbpRatePermissionEditForm;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.ExtensibleRateDayOfWeekService;
import com.digitalpaytech.service.ExtensibleRateTypeService;
import com.digitalpaytech.service.ParkingPermissionDayOfWeekService;
import com.digitalpaytech.service.ParkingPermissionTypeService;
import com.digitalpaytech.service.UnifiedRateService;
import com.digitalpaytech.util.CustomerAdminUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.customeradmin.ExtendByPhoneValidator;

/**
 * ExtendByPhoneController contains logic for Overview, View/Add/Edit Rate and Policy.
 * 
 * @author Allen Liang
 */

@Controller
@SessionAttributes({ WebSecurityConstants.WEB_SECURITY_FORM })
public class ExtendByPhoneController extends LocationDetailsController {
    private static Logger log = Logger.getLogger(CustomerAdminExtendByPhoneController.class);
    
    @Autowired
    private ExtendByPhoneValidator extendByPhoneValidator;
    
    @Autowired
    private EntityService entityService;
    
    @Autowired
    private UnifiedRateService unifiedRateService;
    
    @Autowired
    private ExtensibleRateDayOfWeekService extensibleRateDayOfWeekService;
    
    @Autowired
    private ExtensibleRateTypeService extensibleRateTypeService;
    
    @Autowired
    private ParkingPermissionTypeService parkingPermissionTypeService;
    
    @Autowired
    private ParkingPermissionDayOfWeekService parkingPermissionDayOfWeekService;
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    private Map<Integer, DayOfWeek> dayOfWeekMap;
    
    @PostConstruct
    private void createDayOfWeekMap() {
        final List<DayOfWeek> list = entityService.loadAll(DayOfWeek.class);
        dayOfWeekMap = new HashMap<Integer, DayOfWeek>(list.size());
        final Iterator<DayOfWeek> iter = list.iterator();
        while (iter.hasNext()) {
            final DayOfWeek dow = iter.next();
            dayOfWeekMap.put(dow.getDayOfWeek(), dow);
        }
        log.info("LocationsExtendByPhoneController, createDayOfWeekMap done, dayOfWeekMap size: " + dayOfWeekMap.size());
    }
    
    private void loadLocationTreeIfNecessary(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        Object treeObj = sessionTool.getAttribute(WebCoreConstants.LOCATION_TREE);
        if (treeObj == null) {
            super.locationList(request, response, model, "/settings/locations/locationDetails");
            treeObj = model.get(WebCoreConstants.LOCATION_TREE);
            sessionTool.setAttribute(WebCoreConstants.LOCATION_TREE, treeObj);
        }
        model.put("locations", WidgetMetricsHelper.convertToJson(treeObj, "Locations", true));
        model.put(WebCoreConstants.LOCATION_TREE, treeObj);
    }
    
    /**
     * 'showExtendByPhoneLocationList' is called when customer or system admin clicks on Locations -> Extend By Phone and it'll reset all session attributes.
     * 
     * This method will retrieve all customer's location information, format
     * that information into a hierarchical tree and puts it in the model as
     * "locations" and returns locations/index.vm
     * 
     * @param request
     * @param response
     * @param model
     * @return Model containing current customer location information in tree
     *         form and returns target URL which is mapped to
     *         Settings->Locations (/settings/locations/index.html)
     */
    public String showExtendByPhoneLocationList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        resetExtendByPhoneAttributes(request);
        
        loadLocationTreeIfNecessary(request, response, model);
        
        final WebSecurityForm<EbpRatePermissionEditForm> webSecurityForm =
                new WebSecurityForm<EbpRatePermissionEditForm>(null, new EbpRatePermissionEditForm());
        //TODO Remove SessionToken
        //		  SessionTool sessionTool = SessionTool.getInstance(request);
        //        model.put(WebSecurityConstants.SESSION_TOKEN, sessionTool.getAttribute(WebSecurityConstants.SESSION_TOKEN));
        model.put(WebSecurityConstants.WEB_SECURITY_FORM, webSecurityForm);
        
        if (model.get(WebCoreConstants.GLOBAL_CONFIGURATION) == null) {
            GlobalConfiguration globalCfg = new GlobalConfiguration();
            final UserAccount userAccount = WebSecurityUtil.getUserAccount();
            globalCfg = customerAdminService.findGlobalPreferences(userAccount.getCustomer(), globalCfg);
            model.put(WebCoreConstants.GLOBAL_CONFIGURATION, globalCfg);
        }
        
        return "/settings/locations/extendByPhone";
    }
    
    /**
     * Retrieves current ExtensibleRate & ParkingPermission records.
     * 
     * @return String Path to overview.vm
     */
    public String showExtendByPhoneLocationOverview(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        final String locIdStr = request.getParameter("locId");
        final Integer locId =
                verifyAndReturnId(request, response, locIdStr,
                                  "+++ Invalid randomised id in LocationsExtendByPhoneController.extendByPhoneLocationOverview() method. +++");
        if (locId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        // Checks if location is changed.
        resetExtendByPhoneAttributes(request, locIdStr);
        sessionTool.setAttribute(WebCoreConstants.LOC_RANDOM_ID, locIdStr);
        
        // Loads locations if it's not in HttpSession.
        loadLocationTreeIfNecessary(request, response, model);
        
        // Retrieves ExtensibleRate records and places in session.
        final List<List<ExtensibleRate>> rateDayOfWeeks = getValidDayOfWeekRates(request, locId);
        model.put(WebCoreConstants.CURRENT_RATES_DAY_OF_WEEK, rateDayOfWeeks);
        
        // Retrieves ParkingPermission records and places in session.
        final List<List<ParkingPermission>> permissionDayOfWeeks = getValidDayOfWeekParkingPermissions(request, locId);
        model.put(WebCoreConstants.CURRENT_PARKING_PERMISSIONS_DAY_OF_WEEK, permissionDayOfWeeks);
        
        return "/settings/locations/xbpOverview";
    }
    
    @SuppressWarnings("unchecked")
    private List<List<ExtensibleRate>> getValidDayOfWeekRates(final HttpServletRequest request, final Integer locId) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        final List<List<ExtensibleRate>> rateDayOfWeeks;
        final Object obj = sessionTool.getAttribute(WebCoreConstants.CURRENT_RATES_DAY_OF_WEEK);
        if (obj != null) {
            rateDayOfWeeks = (List<List<ExtensibleRate>>) obj;
        } else {
            rateDayOfWeeks = getValidDayOfWeekRatesByLocationId(request, locId);
            sessionTool.setAttribute(WebCoreConstants.CURRENT_RATES_DAY_OF_WEEK, rateDayOfWeeks);
        }
        return rateDayOfWeeks;
    }
    
    @SuppressWarnings("unchecked")
    private List<List<ParkingPermission>> getValidDayOfWeekParkingPermissions(final HttpServletRequest request, final Integer locId) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        final List<List<ParkingPermission>> permissionDayOfWeeks;
        final Object obj = sessionTool.getAttribute(WebCoreConstants.CURRENT_PARKING_PERMISSIONS_DAY_OF_WEEK);
        if (obj != null) {
            permissionDayOfWeeks = (List<List<ParkingPermission>>) obj;
        } else {
            permissionDayOfWeeks = getValidDayOfWeekParkingPermissionsByLocationId(request, locId);
            sessionTool.setAttribute(WebCoreConstants.CURRENT_PARKING_PERMISSIONS_DAY_OF_WEEK, permissionDayOfWeeks);
        }
        return permissionDayOfWeeks;
    }
    
    private List<List<ExtensibleRate>> getValidDayOfWeekRatesByLocationId(final HttpServletRequest request, final int locationId) {
        final List<List<ExtensibleRate>> rateDayOfWeeks = locationService.getValidRatesByLocationId(locationId);
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        Iterator<ExtensibleRate> erIter;
        final Iterator<List<ExtensibleRate>> iter = rateDayOfWeeks.iterator();
        while (iter.hasNext()) {
            erIter = iter.next().iterator();
            while (erIter.hasNext()) {
                final ExtensibleRate er = erIter.next();
                er.setRandomId(keyMapping.getRandomString(er, WebCoreConstants.ID_LOOK_UP_NAME));
            }
        }
        return rateDayOfWeeks;
    }
    
    private List<List<ParkingPermission>> getValidDayOfWeekParkingPermissionsByLocationId(final HttpServletRequest request, final int locationId) {
        final List<List<ParkingPermission>> permissionDayOfWeeks = locationService.getValidParkingPermissionsByLocationId(locationId);
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        Iterator<ParkingPermission> ppIter;
        final Iterator<List<ParkingPermission>> iter = permissionDayOfWeeks.iterator();
        while (iter.hasNext()) {
            ppIter = iter.next().iterator();
            while (ppIter.hasNext()) {
                final ParkingPermission pp = ppIter.next();
                pp.setRandomId(keyMapping.getRandomString(pp, WebCoreConstants.ID_LOOK_UP_NAME));
            }
        }
        return permissionDayOfWeeks;
    }
    
    private Integer verifyAndReturnId(final HttpServletRequest request, final HttpServletResponse response, final String id, final String msg) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        return WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, id, new String[] { msg, msg });
    }
    
    /**
     * For showing 'Current Rates' and 'Rate Details'.
     */
    public String rateList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        final String locIdStr = request.getParameter("locId");
        final Integer locId = verifyAndReturnId(request, response, locIdStr,
                                                "+++ Invalid locationId in LocationsExtendByPhoneController.viewRateList() method. +++");
        if (locId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final List<List<ExtensibleRate>> rateDayOfWeeks = getValidDayOfWeekRates(request, locId);
        model.put(WebCoreConstants.CURRENT_RATES_DAY_OF_WEEK, rateDayOfWeeks);
        
        sessionTool.setAttribute(WebCoreConstants.LOC_RANDOM_ID, locIdStr);
        
        loadLocationTreeIfNecessary(request, response, model);
        
        final WebSecurityForm<EbpRatePermissionEditForm> webSecurityForm =
                new WebSecurityForm<EbpRatePermissionEditForm>(null, new EbpRatePermissionEditForm());
        model.put(WebSecurityConstants.WEB_SECURITY_FORM, webSecurityForm);
        //TODO Remove SessionToken
        //        model.put(WebSecurityConstants.SESSION_TOKEN, sessionTool.getAttribute(WebSecurityConstants.SESSION_TOKEN));
        
        return "/settings/locations/xbpRate";
    }
    
    /**
     * For showing 'Current Policies' and 'Policy Details'.
     */
    public String policyList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        final String locIdStr = request.getParameter("locId");
        final Integer locId = verifyAndReturnId(request, response, locIdStr,
                                                "+++ Invalid locationId in LocationsExtendByPhoneController.viewPolicyList() method. +++");
        if (locId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final List<List<ParkingPermission>> permissionDayOfWeeks = getValidDayOfWeekParkingPermissions(request, locId);
        model.put(WebCoreConstants.CURRENT_PARKING_PERMISSIONS_DAY_OF_WEEK, permissionDayOfWeeks);
        
        sessionTool.setAttribute(WebCoreConstants.LOC_RANDOM_ID, locIdStr);
        
        loadLocationTreeIfNecessary(request, response, model);
        
        final WebSecurityForm<EbpRatePermissionEditForm> webSecurityForm =
                new WebSecurityForm<EbpRatePermissionEditForm>(null, new EbpRatePermissionEditForm());
        model.put(WebSecurityConstants.WEB_SECURITY_FORM, webSecurityForm);
        //TODO Remove SessionToken
        //        model.put(WebSecurityConstants.SESSION_TOKEN, sessionTool.getAttribute(WebSecurityConstants.SESSION_TOKEN));
        
        return "/settings/locations/xbpPolicy";
    }
    
    /**
     * For showing 'Edit Rate'.
     */
    public String viewEditRate(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        final EbpRatePermissionEditForm form;
        final String rateIdStr = request.getParameter("rateId");
        
        // List of UnifiedRate names
        final List<String> unifiedRateNames = findUnifiedRateNames(WebSecurityUtil.getUserAccount().getCustomer().getId());
        model.put("unifiedRateNames", unifiedRateNames);
        
        // Edit existing ExtensibleRate.
        if (StringUtils.isNotBlank(rateIdStr)) {
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            ExtensibleRate extRate = findExtensibleRateInSession(request, rateIdStr);
            // Loads from the database if it's not in HttpSession.
            if (extRate == null) {
                final int rateId = verifyAndReturnId(request, response, rateIdStr,
                                                     "+++ Invalid randomised id in ExtendByPhoneController.viewEditRate() method. +++");
                extRate = locationService.findExtensibleRate(rateId);
            }
            
            if (!extRate.isIsActive()) {
                final MessageInfo message = new MessageInfo();
                message.setError(true);
                message.setToken(null);
                message.addMessage(this.commonControllerHelper.getMessage(ErrorConstants.REQUESTED_ITEM_DELETED));
                return WidgetMetricsHelper.convertToJson(message, WebCoreConstants.UI_MESSAGES, true);
            }
            
            extRate.setRandomId(keyMapping.getRandomString(extRate, WebCoreConstants.ID_LOOK_UP_NAME));
            extRate.setExtensibleRateDayOfWeeks(extensibleRateDayOfWeekService.findByExtensibleRateId(extRate.getId()));
            extRate.setLocation(createLocationWithRandomId((String) sessionTool.getAttribute(WebCoreConstants.LOC_RANDOM_ID)));
            form = new EbpRatePermissionEditForm(extRate);
            
        } else {
            // Creates new EbpRatePermissionEditForm.
            form = new EbpRatePermissionEditForm();
            form.setLocationId((String) sessionTool.getAttribute(WebCoreConstants.LOC_RANDOM_ID));
        }
        
        return WidgetMetricsHelper.convertToJson(form, "ebpRatePermissionEditForm", true);
    }
    
    private ExtensibleRate findExtensibleRateInSession(final HttpServletRequest request, final String rateIdStr) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        final Object obj = sessionTool.getAttribute(WebCoreConstants.CURRENT_RATES_DAY_OF_WEEK);
        if (obj == null) {
            return null;
        }
        
        ExtensibleRate er;
        Iterator<ExtensibleRate> erIter;
        @SuppressWarnings("unchecked")
        final Iterator<List<ExtensibleRate>> iter = ((List<List<ExtensibleRate>>) obj).iterator();
        while (iter.hasNext()) {
            erIter = iter.next().iterator();
            while (erIter.hasNext()) {
                er = erIter.next();
                if (er.getRandomId() != null && er.getRandomId().equalsIgnoreCase(rateIdStr)) {
                    return er;
                }
            }
        }
        return null;
    }
    
    /**
     * For showing 'Edit Policy'.
     */
    public String viewEditPolicy(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        final EbpRatePermissionEditForm form;
        final String policyIdStr = request.getParameter("policyId");
        
        // Edit existing ParkingPermission.
        if (StringUtils.isNotBlank(policyIdStr)) {
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            ParkingPermission pPermission = findParkingPermissionInSession(request, policyIdStr);
            // Loads from the database if it's not in HttpSession.
            if (pPermission == null) {
                final int policyId = verifyAndReturnId(request, response, policyIdStr,
                                                       "+++ Invalid randomised id in ExtendByPhoneController.viewEditRate() method. +++");
                pPermission = locationService.findParkingPermission(policyId);
                if (pPermission == null) {
                    final MessageInfo message = new MessageInfo();
                    message.setError(true);
                    message.setToken(null);
                    message.addMessage(this.commonControllerHelper.getMessage(ErrorConstants.REQUESTED_ITEM_DELETED));
                    return WidgetMetricsHelper.convertToJson(message, WebCoreConstants.UI_MESSAGES, true);
                }
            } else {
                pPermission = (ParkingPermission) entityService.merge(pPermission);
            }
            
            pPermission.setRandomId(keyMapping.getRandomString(pPermission, WebCoreConstants.ID_LOOK_UP_NAME));
            pPermission.setParkingPermissionDayOfWeeks(parkingPermissionDayOfWeekService.findByParkingPermissionId(pPermission.getId()));
            pPermission.getParkingPermissionType()
                    .setRandomId(keyMapping.getRandomString(pPermission.getParkingPermissionType(), WebCoreConstants.ID_LOOK_UP_NAME));
            pPermission.setLocation(createLocationWithRandomId((String) sessionTool.getAttribute(WebCoreConstants.LOC_RANDOM_ID)));
            form = new EbpRatePermissionEditForm(pPermission);
            
        } else {
            // Creates new EbpRatePermissionEditForm.
            form = new EbpRatePermissionEditForm();
            form.setLocationId((String) sessionTool.getAttribute(WebCoreConstants.LOC_RANDOM_ID));
        }
        
        return WidgetMetricsHelper.convertToJson(form, "ebpRatePermissionEditForm", true);
    }
    
    private ParkingPermission findParkingPermissionInSession(final HttpServletRequest request, final String policyIdStr) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        final Object obj = sessionTool.getAttribute(WebCoreConstants.CURRENT_PARKING_PERMISSIONS_DAY_OF_WEEK);
        if (obj == null) {
            return null;
        }
        
        ParkingPermission pp;
        Iterator<ParkingPermission> ppIter;
        @SuppressWarnings("unchecked")
        final Iterator<List<ParkingPermission>> iter = ((List<List<ParkingPermission>>) obj).iterator();
        while (iter.hasNext()) {
            ppIter = iter.next().iterator();
            while (ppIter.hasNext()) {
                pp = ppIter.next();
                if (pp.getRandomId() != null && pp.getRandomId().equalsIgnoreCase(policyIdStr)) {
                    return pp;
                }
            }
        }
        return null;
    }
    
    /**
     * Creates new or updates existing ExtensibleRate or ParkingPermission using input EbpRatePermissionEditForm and stores it in the database.
     * 
     * @return String 'true' or 'false' response.
     */
    public String saveOrUpdate(final WebSecurityForm<EbpRatePermissionEditForm> webSecurityForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!((WebUser) WebSecurityUtil.getAuthentication().getPrincipal()).isSystemAdmin()
            && !this.extendByPhoneValidator.validateMigrationStatus(webSecurityForm)) {
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        final String locIdStr = webSecurityForm.getWrappedObject().getLocationId();
        final Integer locId = verifyAndReturnId(request, response, locIdStr,
                                                "+++ Invalid locationId in LocationsExtendByPhoneController.saveOrUpdate() method. +++");
        webSecurityForm.getWrappedObject().setLocationId(String.valueOf(locId));
        webSecurityForm.getWrappedObject().setDaysOfWeeks(createSelectedDaysOfWeek(webSecurityForm.getWrappedObject()));
        
        extendByPhoneValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        webSecurityForm.resetToken();
        final UserAccount userAcct = WebSecurityUtil.getUserAccount();
        
        final EbpRatePermissionEditForm form = webSecurityForm.getWrappedObject();
        
        if (StringUtils.isNotBlank(form.getExtensibleRateFlag()) && form.getExtensibleRateFlag().equalsIgnoreCase(WebCoreConstants.RESPONSE_TRUE)) {
            final ExtensibleRate extensibleRate = populateExtensibleRate(request, response, form);
            locationService.saveOrUpdateExtensibleRate(userAcct, extensibleRate);
            
            resetExtendByPhoneAttributes(request);
            final List<List<ExtensibleRate>> rateDayOfWeeks = getValidDayOfWeekRatesByLocationId(request, extensibleRate.getLocation().getId());
            model.put(WebCoreConstants.CURRENT_RATES_DAY_OF_WEEK, rateDayOfWeeks);
            
        } else if (StringUtils.isNotBlank(form.getParkingPermissionFlag())
                   && form.getParkingPermissionFlag().equalsIgnoreCase(WebCoreConstants.RESPONSE_TRUE)) {
            final ParkingPermission parkingPermission = populateParkingPermission(request, response, form);
            locationService.saveOrUpdateParkingPermission(userAcct, parkingPermission);
            
            resetExtendByPhoneAttributes(request);
            final List<List<ParkingPermission>> permissionDayOfWeeks =
                    getValidDayOfWeekParkingPermissionsByLocationId(request, parkingPermission.getLocation().getId());
            model.put(WebCoreConstants.CURRENT_PARKING_PERMISSIONS_DAY_OF_WEEK, permissionDayOfWeeks);
            
        } else {
            webSecurityForm.addErrorStatus(new FormErrorStatus("options", "error.common.required"));
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(":").append(webSecurityForm.getInitToken()).toString();
    }
    
    public String deleteRate(final HttpServletRequest request, final HttpServletResponse response) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        if (!((WebUser) WebSecurityUtil.getAuthentication().getPrincipal()).isSystemAdmin() && !WebSecurityUtil.isCustomerMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + ":" + this.extendByPhoneValidator.getMessage("error.customer.notMigrated");
        }
        
        final String rateIdStr = request.getParameter("id");
        if (rateIdStr == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        final Integer rateId = verifyAndReturnId(request, response, rateIdStr,
                                                 "+++ Invalid randomised id in LocationsExtendByPhoneController.deleteRate() method. +++");
        if (rateId == null || rateId <= 0) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        locationService.deleteExtensibleRateById(rateId);
        sessionTool.removeAttribute(WebCoreConstants.CURRENT_RATES_DAY_OF_WEEK);
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    public String deletePolicy(final HttpServletRequest request, final HttpServletResponse response) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        if (!((WebUser) WebSecurityUtil.getAuthentication().getPrincipal()).isSystemAdmin() && !WebSecurityUtil.isCustomerMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + ":" + this.extendByPhoneValidator.getMessage("error.customer.notMigrated");
        }
        
        final String ppIdStr = request.getParameter("id");
        if (ppIdStr == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        final Integer ppId = verifyAndReturnId(request, response, ppIdStr,
                                               "+++ Invalid randomised id in LocationsExtendByPhoneController.deletePolicy() method. +++");
        if (ppId == null || ppId <= 0) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        locationService.deleteParkingPermissionById(ppId);
        sessionTool.removeAttribute(WebCoreConstants.CURRENT_PARKING_PERMISSIONS_DAY_OF_WEEK);
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    public String saveWarningPeriod(final WebSecurityForm<EbpRatePermissionEditForm> webSecurityForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!((WebUser) WebSecurityUtil.getAuthentication().getPrincipal()).isSystemAdmin()
            && !this.extendByPhoneValidator.validateMigrationStatus(webSecurityForm)) {
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        extendByPhoneValidator.validateWarningPeriod(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        final UserAccount ua = WebSecurityUtil.getUserAccount();
        CustomerProperty prop = null;
        final Iterator<CustomerProperty> iter = customerAdminService.getCustomerPropertyByCustomerId(ua.getCustomer().getId()).iterator();
        while (iter.hasNext()) {
            prop = iter.next();
            if (prop.getCustomerPropertyType().getId() == WebCoreConstants.CUSTOMER_PROPERTY_TYPE_SMS_WARNING_PERIOD) {
                break;
            }
        }
        
        if (prop == null) {
            prop = new CustomerProperty();
            prop.setCustomerPropertyType(CustomerAdminUtil.findCustomerPropertyType(WebCoreConstants.CUSTOMER_PROPERTY_TYPE_SMS_WARNING_PERIOD));
            prop.setCustomer(ua.getCustomer());
            prop.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
            prop.setLastModifiedByUserId(ua.getId());
        }
        prop.setPropertyValue(webSecurityForm.getWrappedObject().getWarningPeriod());
        customerAdminService.saveOrUpdateCustomerProperty(prop);
        
        return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(":").append(webSecurityForm.getInitToken()).toString();
    }
    
    private Location findLocation(final HttpServletRequest request, final HttpServletResponse response) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        final Integer locId =
                verifyAndReturnId(request, response, (String) sessionTool.getAttribute(WebCoreConstants.LOC_RANDOM_ID),
                                  "+++ Invalid randomised id in LocationsExtendByPhoneController.extendByPhoneLocationOverview() method. +++");
        return locationService.findLocationById(locId);
    }
    
    private ExtensibleRate populateExtensibleRate(final HttpServletRequest request, final HttpServletResponse response,
        final EbpRatePermissionEditForm form) {
        final Location loc = findLocation(request, response);
        
        final ExtensibleRate extensibleRate = new ExtensibleRate();
        // Update ExtensibleRate.
        if (form.getExtensibleRateRealId() != 0) {
            extensibleRate.setId(form.getExtensibleRateRealId());
        }
        
        extensibleRate.setName(form.getName().trim());
        extensibleRate.setLocation(loc);
        
        // Hard code ExtensibleRateTypeId to 1 for "Hourly" rate since don't have "Special" rate logic.
        extensibleRate.setExtensibleRateType(extensibleRateTypeService.loadAll().get("hourly"));
        String[] hrMin = form.getStartTime().split(WebCoreConstants.COLON);
        hrMin = DateUtil.addMinIfMissing(hrMin);
        extensibleRate.setBeginHourLocal(Short.parseShort(hrMin[0]));
        extensibleRate.setBeginMinuteLocal(Short.parseShort(hrMin[1]));
        
        hrMin = form.getEndTime().split(WebCoreConstants.COLON);
        hrMin = DateUtil.addMinIfMissing(hrMin);
        extensibleRate.setEndHourLocal(Short.parseShort(hrMin[0]));
        extensibleRate.setEndMinuteLocal(Short.parseShort(hrMin[1]));
        
        extensibleRate.setRateAmount(WebCoreUtil.convertToBase100IntValue(form.getRateValue()));
        extensibleRate.setServiceFeeAmount(WebCoreUtil.convertToBase100IntValue(form.getRateServiceFee()));
        extensibleRate.setMinExtensionMinutes(Integer.parseInt(form.getMinExtensionMinutes()));
        
        extensibleRate.setIsActive(true);
        
        setDaysOfWeek(extensibleRate, form);
        
        return extensibleRate;
    }
    
    private ParkingPermission populateParkingPermission(final HttpServletRequest request, final HttpServletResponse response,
        final EbpRatePermissionEditForm form) {
        final Location loc = findLocation(request, response);
        
        final ParkingPermission parkingPermission = new ParkingPermission();
        // Update ParkingPermission.
        if (form.getParkingPermissionRealId() != 0) {
            parkingPermission.setId(form.getParkingPermissionRealId());
            
        } else {
            parkingPermission.setCreatedGmt(DateUtil.getCurrentGmtDate());
        }
        
        parkingPermission.setName(form.getName().trim());
        parkingPermission.setLocation(loc);
        parkingPermission.setParkingPermissionType(parkingPermissionTypeService.loadAll().get(form.getPermissionStatus().toLowerCase()));
        
        String[] hrMin = form.getStartTime().split(WebCoreConstants.COLON);
        hrMin = DateUtil.addMinIfMissing(hrMin);
        parkingPermission.setBeginHourLocal(Short.parseShort(hrMin[0]));
        parkingPermission.setBeginMinuteLocal(Short.parseShort(hrMin[1]));
        
        hrMin = form.getEndTime().split(WebCoreConstants.COLON);
        hrMin = DateUtil.addMinIfMissing(hrMin);
        parkingPermission.setEndHourLocal(Short.parseShort(hrMin[0]));
        parkingPermission.setEndMinuteLocal(Short.parseShort(hrMin[1]));
        
        parkingPermission.setIsActive(true);
        parkingPermission.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        parkingPermission.setLastModifiedByUserId(WebSecurityUtil.getUserAccount().getId());
        
        if (form.getPermissionStatus().equalsIgnoreCase("unlimited")) {
            parkingPermission.setMaxDurationMinutes(getMaxDurationUnlimited(form));
            parkingPermission.setIsLimitedOrUnlimited(false);
        } else {
            parkingPermission.setMaxDurationMinutes(Integer.parseInt(form.getMaxDurationMinutes()));
            parkingPermission.setIsLimitedOrUnlimited(true);
        }
        setDaysOfWeek(parkingPermission, form);
        
        return parkingPermission;
    }
    
    private Integer getMaxDurationUnlimited(final EbpRatePermissionEditForm form) {
        String[] hrMin = form.getStartTime().split(WebCoreConstants.COLON);
        hrMin = DateUtil.addMinIfMissing(hrMin);
        final int startHr = Integer.parseInt(hrMin[0]);
        final int startMin = Integer.parseInt(hrMin[1]);
        final int startCombinedMin = (startHr * 60) + startMin;
        
        hrMin = form.getEndTime().split(WebCoreConstants.COLON);
        hrMin = DateUtil.addMinIfMissing(hrMin);
        final int endHr = Integer.parseInt(hrMin[0]);
        final int endMin = Integer.parseInt(hrMin[1]);
        final int endCombinedMin = (endHr * 60) + endMin;
        
        return endCombinedMin - startCombinedMin;
    }
    
    private ExtensibleRate setDaysOfWeek(final ExtensibleRate extensibleRate, final EbpRatePermissionEditForm form) {
        final Integer userId = WebSecurityUtil.getUserAccount().getId();
        final Date currentDate = DateUtil.getCurrentGmtDate();
        
        // Creates a new list of ExtensibleRateDayOfWeek object with selected days.
        final List<DayOfWeek> selectedDaysOfWeek = createSelectedDaysOfWeek(form);
        final Set<ExtensibleRateDayOfWeek> newErDayOfWeek = new HashSet<ExtensibleRateDayOfWeek>(selectedDaysOfWeek.size());
        for (DayOfWeek day : selectedDaysOfWeek) {
            final ExtensibleRateDayOfWeek erDay = new ExtensibleRateDayOfWeek();
            erDay.setDayOfWeek(day);
            erDay.setExtensibleRate(extensibleRate);
            erDay.setLastModifiedByUserId(userId);
            erDay.setLastModifiedGmt(currentDate);
            newErDayOfWeek.add(erDay);
        }
        
        // Removed existing ExtensibleRateDayOfWeeks if exists.
        if (extensibleRate.getExtensibleRateDayOfWeeks() != null) {
            final Set<ExtensibleRateDayOfWeek> set = extensibleRate.getExtensibleRateDayOfWeeks();
            extensibleRate.getExtensibleRateDayOfWeeks().removeAll(set);
        }
        extensibleRate.setExtensibleRateDayOfWeeks(newErDayOfWeek);
        return extensibleRate;
    }
    
    private ParkingPermission setDaysOfWeek(final ParkingPermission parkingPermission, final EbpRatePermissionEditForm form) {
        final Integer userId = WebSecurityUtil.getUserAccount().getId();
        final Date currentDate = DateUtil.getCurrentGmtDate();
        
        // Creates a new list of ParkingPermissionDayOfWeek object with selected days.
        final List<DayOfWeek> selectedDaysOfWeek = createSelectedDaysOfWeek(form);
        final Set<ParkingPermissionDayOfWeek> newPpDayOfWeek = new HashSet<ParkingPermissionDayOfWeek>(selectedDaysOfWeek.size());
        for (DayOfWeek day : selectedDaysOfWeek) {
            final ParkingPermissionDayOfWeek ppDay = new ParkingPermissionDayOfWeek();
            ppDay.setDayOfWeek(day);
            ppDay.setParkingPermission(parkingPermission);
            ppDay.setLastModifiedByUserId(userId);
            ppDay.setLastModifiedGmt(currentDate);
            newPpDayOfWeek.add(ppDay);
        }
        
        // Removed existing ParkingPermissionDayOfWeek if exists.
        if (parkingPermission.getParkingPermissionDayOfWeeks() != null) {
            final Set<ParkingPermissionDayOfWeek> set = parkingPermission.getParkingPermissionDayOfWeeks();
            parkingPermission.getParkingPermissionDayOfWeeks().removeAll(set);
        }
        parkingPermission.setParkingPermissionDayOfWeeks(newPpDayOfWeek);
        return parkingPermission;
    }
    
    private Location createLocationWithRandomId(final String randomId) {
        final Location loc = new Location();
        loc.setRandomId(randomId);
        return loc;
    }
    
    /**
     * Creates a list of selected DayOfWeek ids.
     * E.g. [Monday, Thursday, Friday, Sunday] returns [2, 5, 6, 1]
     * 
     * @param form
     * @return
     */
    private List<DayOfWeek> createSelectedDaysOfWeek(final EbpRatePermissionEditForm form) {
        final List<DayOfWeek> selectedDaysOfWeek = new ArrayList<DayOfWeek>();
        if (StringUtils.isNotBlank(form.getSunday()) && form.getSunday().equalsIgnoreCase(WebCoreConstants.RESPONSE_TRUE)) {
            selectedDaysOfWeek.add(getDayOfWeek(WebCoreConstants.SUNDAY));
        }
        if (StringUtils.isNotBlank(form.getMonday()) && form.getMonday().equalsIgnoreCase(WebCoreConstants.RESPONSE_TRUE)) {
            selectedDaysOfWeek.add(getDayOfWeek(WebCoreConstants.MONDAY));
        }
        if (StringUtils.isNotBlank(form.getTuesday()) && form.getTuesday().equalsIgnoreCase(WebCoreConstants.RESPONSE_TRUE)) {
            selectedDaysOfWeek.add(getDayOfWeek(WebCoreConstants.TUESDAY));
        }
        if (StringUtils.isNotBlank(form.getWednesday()) && form.getWednesday().equalsIgnoreCase(WebCoreConstants.RESPONSE_TRUE)) {
            selectedDaysOfWeek.add(getDayOfWeek(WebCoreConstants.WEDNESDAY));
        }
        if (StringUtils.isNotBlank(form.getThursday()) && form.getThursday().equalsIgnoreCase(WebCoreConstants.RESPONSE_TRUE)) {
            selectedDaysOfWeek.add(getDayOfWeek(WebCoreConstants.THURSDAY));
        }
        if (StringUtils.isNotBlank(form.getFriday()) && form.getFriday().equalsIgnoreCase(WebCoreConstants.RESPONSE_TRUE)) {
            selectedDaysOfWeek.add(getDayOfWeek(WebCoreConstants.FRIDAY));
        }
        if (StringUtils.isNotBlank(form.getSaturday()) && form.getSaturday().equalsIgnoreCase(WebCoreConstants.RESPONSE_TRUE)) {
            selectedDaysOfWeek.add(getDayOfWeek(WebCoreConstants.SATURDAY));
        }
        return selectedDaysOfWeek;
    }
    
    private void resetExtendByPhoneAttributes(final HttpServletRequest request) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        sessionTool.removeAttribute(WebCoreConstants.LOCATION_TREE);
        sessionTool.removeAttribute(WebCoreConstants.CURRENT_RATES_DAY_OF_WEEK);
        sessionTool.removeAttribute(WebCoreConstants.CURRENT_PARKING_PERMISSIONS_DAY_OF_WEEK);
        sessionTool.removeAttribute(WebCoreConstants.LOC_RANDOM_ID);
    }
    
    private void resetExtendByPhoneAttributes(final HttpServletRequest request, final String locIdStr) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        final Object obj = sessionTool.getAttribute(WebCoreConstants.LOC_RANDOM_ID);
        if (obj != null) {
            final String savedLocIdStr = (String) obj;
            if (!savedLocIdStr.equals(locIdStr)) {
                resetExtendByPhoneAttributes(request);
            }
        }
    }
    
    private List<String> findUnifiedRateNames(final Integer customerId) {
        final List<UnifiedRate> list = unifiedRateService.findRatesByCustomerId(WebSecurityUtil.getUserAccount().getCustomer().getId());
        final List<String> names = new ArrayList<String>(list.size());
        final Iterator<UnifiedRate> iter = list.iterator();
        while (iter.hasNext()) {
            names.add(iter.next().getName());
        }
        return names;
    }
    
    private DayOfWeek getDayOfWeek(final int dayOfWeekIdx) {
        return dayOfWeekMap.get(dayOfWeekIdx);
    }
    
    public final void setExtendByPhoneValidator(final ExtendByPhoneValidator extendByPhoneValidator) {
        this.extendByPhoneValidator = extendByPhoneValidator;
    }
    
    public final Map<Integer, DayOfWeek> getDayOfWeekList() {
        return dayOfWeekMap;
    }
    
    public final void setDayOfWeekList(final Map<Integer, DayOfWeek> dayOfWeekMap) {
        this.dayOfWeekMap = dayOfWeekMap;
    }
    
    public final void setEntityService(final EntityService entityService) {
        this.entityService = entityService;
    }
    
    public final void setUnifiedRateService(final UnifiedRateService unifiedRateService) {
        this.unifiedRateService = unifiedRateService;
    }
    
    public final void setExtensibleRateDayOfWeekService(final ExtensibleRateDayOfWeekService extensibleRateDayOfWeekService) {
        this.extensibleRateDayOfWeekService = extensibleRateDayOfWeekService;
    }
    
    public final void setExtensibleRateTypeService(final ExtensibleRateTypeService extensibleRateTypeService) {
        this.extensibleRateTypeService = extensibleRateTypeService;
    }
    
    public final void setParkingPermissionTypeService(final ParkingPermissionTypeService parkingPermissionTypeService) {
        this.parkingPermissionTypeService = parkingPermissionTypeService;
    }
    
    public final void setParkingPermissionDayOfWeekService(final ParkingPermissionDayOfWeekService parkingPermissionDayOfWeekService) {
        this.parkingPermissionDayOfWeekService = parkingPermissionDayOfWeekService;
    }
}
