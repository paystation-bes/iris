package com.digitalpaytech.mvc;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.exception.InvalidUserAccountException;
import com.digitalpaytech.mvc.support.ChangePasswordForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.PasswordProfileValidator;

/**
 * ChangePasswordController prepares displaying Change Password page and delegates password change logic to PasswordProfileValidator.java
 * and UserAccountService.java.
 * 
 * @author Allen Liang
 */

@Controller
@RequestMapping(value = "/secure/password.html")
public class ChangePasswordController {
    private static Logger log = Logger.getLogger(ChangePasswordController.class);
    
    @Autowired
    private UserAccountService userAccountService;
    
    @Autowired
    private PasswordProfileValidator passwordProfileValidator;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    public void setUserAccountService(UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }
    
    public void setPasswordProfileValidator(PasswordProfileValidator passwordProfileValidator) {
        this.passwordProfileValidator = passwordProfileValidator;
    }
    
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public String showChangePassword(HttpServletRequest request, ModelMap model,
        @ModelAttribute(WebSecurityConstants.WEB_SECURITY_FORM) WebSecurityForm<ChangePasswordForm> secForm) {
        // public WebSecurityForm(Object primaryKey, Object wrappedObject)
        
        // Default set as create action
        secForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        
        //TODO Remove SessionToken
        //		SessionTool sessionTool = SessionTool.getInstance(request);
        //		model.put(WebSecurityConstants.SESSION_TOKEN, sessionTool.getAttribute(WebSecurityConstants.SESSION_TOKEN));
        model.put(WebSecurityConstants.WEB_SECURITY_FORM, secForm);
        
        return "password";
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public String changePassword(@ModelAttribute(WebSecurityConstants.WEB_SECURITY_FORM) WebSecurityForm<ChangePasswordForm> webSecurityForm,
        BindingResult result, ModelMap model, HttpServletRequest request, HttpServletResponse response) throws InvalidUserAccountException {
        // Compares sessionToken.
        if (!PasswordProfileValidator.isSameSessionToken(request, request.getSession(false))) {
            throw new InvalidUserAccountException("invalid");
        }
        
        passwordProfileValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            //TODO Remove SessionToken
            //			SessionTool sessionTool = SessionTool.getInstance(request);
            //			model.put(WebSecurityConstants.SESSION_TOKEN, sessionTool.getAttribute(WebSecurityConstants.SESSION_TOKEN));
            return "password";
        }
        
        UserAccount userAccount = WebSecurityUtil.getUserAccount();
        ChangePasswordForm passwdProfile = webSecurityForm.getWrappedObject();
        
        passwordProfileValidator.validateNewAndOldPasswords(webSecurityForm, passwdProfile.getNewPasswordAgain(), userAccount);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return "password";
        }
        
        // Everything is ok, save to database now.
        String salt = RandomStringUtils.randomAlphanumeric(WebSecurityConstants.RANDOMKEY_MIN_HEX_LENGTH);
        String encodedPassword = passwordEncoder.encodePassword(passwdProfile.getNewPasswordAgain(), salt);
        
        if (userAccount.getUserAccount() != null) {
            final UserAccount originalUser = userAccount.getUserAccount();
            originalUser.setPassword(encodedPassword);
            originalUser.setPasswordSalt(salt);
            originalUser.setIsPasswordTemporary(false);
            originalUser.setLastModifiedGmt(new Date());
            userAccountService.updatePassword(originalUser);
        } else {
            userAccount.setPassword(encodedPassword);
            userAccount.setPasswordSalt(salt);
            userAccount.setIsPasswordTemporary(false);
            userAccount.setLastModifiedGmt(new Date());
            userAccountService.updatePassword(userAccount);
        }
        webSecurityForm.resetToken();
        
        WebUser webUser = (WebUser) WebSecurityUtil.getAuthentication().getPrincipal();
        webUser.setIsTemporaryPassword(false);
        
        if (log.isDebugEnabled()) {
            log.debug("UserAccount is updated.");
        }
        
        if (userAccount.getCustomer().getCustomerType().getId() == WebCoreConstants.CUSTOMER_TYPE_DPT) {
            return "redirect:/systemAdmin/index.html";
        } else {
            return "redirect:/secure/dashboard/index.html";
        }
        
    }
    
    @ModelAttribute(WebSecurityConstants.WEB_SECURITY_FORM)
    public WebSecurityForm<ChangePasswordForm> initForm(HttpServletRequest request) {
        return new WebSecurityForm<ChangePasswordForm>((Object) null, new ChangePasswordForm(), SessionTool.getInstance(request)
                .locatePostToken(WebSecurityConstants.WEB_SECURITY_FORM));
    }
}
