package com.digitalpaytech.mvc.cache;

import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import redis.clients.jedis.JedisPool;

import com.digitalpaytech.dto.customeradmin.TransactionDetail;
import com.digitalpaytech.dto.customeradmin.TransactionReceiptSearchCriteria;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.util.EasyPool;
import com.digitalpaytech.util.KeyValuePair;
import com.digitalpaytech.util.PooledResource;
import com.digitalpaytech.util.PrefixZeroNumberFormatBuilder;
import com.digitalpaytech.util.cache.CacheConfiguration;
import com.digitalpaytech.util.cache.RedisAggregatedCache;
import com.digitalpaytech.util.cache.RedisCache;

@Component("PermitCacheManager")
public class PermitCacheManager {
    public static final String KEYTYPE_IRIS = "D";
    public static final String KEYTYPE_FLEX = "T";
    
    public static final String KEYGROUP_DEFAULT = "00";
    
    public static final String CACHENAME_PERMIT = "AggregatedPermit";
    
    public static final int SECS_EXPIRY = 7200;
    public static final long MILLISECS_ONE_SECOND = 1000L;
    public static final long MAX_MILLISECS = (new GregorianCalendar(10000, 12, 31)).getTimeInMillis();
    
    public static final int LENGTH_PERMIT_ID = 10;
    public static final int CONF_OBJCACHE_MIN_SIZE = 5;
    
    private static final String FMT_JSON_CUSTOMER_ID = "'{ \"customerId\"': {0, number, ####} '}";
    
    @Autowired
    @Qualifier("cacheConfig")
    private CacheConfiguration cacheConfig;
    
    @Autowired
    @Qualifier("jedis")
    private JedisPool jedisPool;
    
    private EasyPool<DecimalFormat> keyFormatPool = new EasyPool<DecimalFormat>(new PrefixZeroNumberFormatBuilder(LENGTH_PERMIT_ID), null,
            CONF_OBJCACHE_MIN_SIZE);
    
    public PermitCacheManager() {
        
    }
    
    public final RedisAggregatedCache<TransactionDetail> getCache(final TransactionReceiptSearchCriteria criteria) {
        final RedisAggregatedCache<TransactionDetail> result = new PermitCache(WidgetMetricsHelper.convertToJson(criteria, "PermitCriteria", true),
                this.jedisPool, this.cacheConfig.getPermitDatabaseIndex());
        
        return result;
    }
    
    public final TransactionDetail getIrisPermit(final Integer customerId, final Long permitId) {
        final RedisCache<TransactionDetail> cache = new PermitCache(MessageFormat.format(FMT_JSON_CUSTOMER_ID, customerId), this.jedisPool,
                this.cacheConfig.getPermitDatabaseIndex());
        return cache.get(this.formatIrisPermitId(permitId));
    }
    
    public final TransactionDetail getFlexPermit(final Integer customerId, final Long permitId) {
        final RedisCache<TransactionDetail> cache = new PermitCache(MessageFormat.format(FMT_JSON_CUSTOMER_ID, customerId), this.jedisPool,
                this.cacheConfig.getPermitDatabaseIndex());
        return cache.get(this.formatFlexPermitId(permitId));
    }
    
    public final List<Map.Entry<String, TransactionDetail>> transform(final List<TransactionDetail> txList) {
        final List<Map.Entry<String, TransactionDetail>> result = new ArrayList<Map.Entry<String, TransactionDetail>>(txList.size());
        for (TransactionDetail tx : txList) {
            result.add(new KeyValuePair<String, TransactionDetail>(formatPermitId(tx), tx));
        }
        
        return result;
    }
    
    public final String formatPermitId(final TransactionDetail tx) {
        return (tx.getIsFlex()) ? formatFlexPermitId(tx.getPermitId()) : formatIrisPermitId(tx.getPermitId());
    }
    
    protected final String formatIrisPermitId(final Long permitId) {
        final PooledResource<DecimalFormat> fmt = this.keyFormatPool.take();
        final String result = KEYTYPE_IRIS + fmt.get().format(permitId);
        fmt.close();
        
        return result;
    }
    
    protected final String formatFlexPermitId(final Long permitId) {
        final PooledResource<DecimalFormat> fmt = this.keyFormatPool.take();
        final String result = KEYTYPE_FLEX + fmt.get().format(permitId);
        fmt.close();
        
        return result;
    }
    
    protected static String formatHashCacheName(final String customerId) {
        return CACHENAME_PERMIT + ":" + customerId;
    }
    
    private static class PermitCache extends RedisAggregatedCache<TransactionDetail> {
        private static Pattern customerIdPattern = Pattern.compile("\"customerId\"\\:\\s*(\\d+)\\s*(,|})");
        
        public PermitCache(final String cacheName, final JedisPool jedisPool, final int dbIndex) {
            super(cacheName, TransactionDetail.class, jedisPool, dbIndex);
            
            this.setExpirySecs(SECS_EXPIRY);
        }
        
        @Override
        protected String transformHashName(final String cacheName) {
            String customerId = null;
            
            final Matcher m = customerIdPattern.matcher(cacheName);
            if (!m.find()) {
                throw new IllegalArgumentException("CacheName does not contain \"customerId\"");
            } else {
                customerId = m.group(1).trim();
            }
            
            return formatHashCacheName(customerId);
        }
        
        @Override
        public String identifyKeyType(final String key) {
            return ((key == null) || (!key.startsWith(KEYTYPE_FLEX))) ? KEYTYPE_IRIS : KEYTYPE_FLEX;
        }
        
        @Override
        protected String resolveKeyGroup(final String key) {
            final StringBuilder result = new StringBuilder();
            if ((key == null) || (key.length() < KEYGROUP_DEFAULT.length())) {
                result.append(KEYGROUP_DEFAULT);
            } else {
                result.append(key.charAt(0));
                result.append(key.substring(key.length() - KEYGROUP_DEFAULT.length()));
            }
            
            return result.toString();
        }
        
        @Override
        public List<String> keyTypes() {
            final List<String> result = new ArrayList<String>(2);
            result.add(KEYTYPE_IRIS);
            result.add(KEYTYPE_FLEX);
            
            return result;
        }
        
        @Override
        public float score(final TransactionDetail obj, final Map<String, Object> ctx) {
            float score = 0;
            if (obj.getExpiryDate() != null) {
                score = obj.getExpiryDate().getTime();
            } else if (obj.getPurchaseDateGmt() != null) {
                score = obj.getPurchaseDateGmt().getTime();
            }
            
            if (score == 0) {
                // Make the Transaction with no expiry date nore purchase date to show up the last one
                score = Float.MAX_VALUE;
            } else if ((obj.getIsActive() != null) && (obj.getIsActive().booleanValue())) {
                score = score - MAX_MILLISECS;
            }
            
            return score / MILLISECS_ONE_SECOND;
        }
    }
}
