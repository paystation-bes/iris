package com.digitalpaytech.mvc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.RoutePOS;
import com.digitalpaytech.domain.RouteType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.PointOfSaleSearchCriteria;
import com.digitalpaytech.dto.RandomIdNameSetting;
import com.digitalpaytech.dto.WidgetMapInfo;
import com.digitalpaytech.dto.customeradmin.PaystationListInfo;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.dto.customeradmin.RouteDetails;
import com.digitalpaytech.dto.customeradmin.RouteInputFormInfo;
import com.digitalpaytech.dto.customeradmin.RouteSettingInfo;
import com.digitalpaytech.mvc.customeradmin.support.RouteEditForm;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.CustomerAlertTypeService;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.service.RouteTypeService;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObject;
import com.digitalpaytech.util.support.WebObjectId;
import com.digitalpaytech.validation.customeradmin.RouteSettingValidator;

/**
 * This class handles the Route setting request in Settings menu.
 * 
 * @author Brian Kim
 * 
 */
@Controller
@SessionAttributes({ "routeEditForm" })
public class RoutesController {
    
    @Autowired
    private RouteService routeService;
    
    @Autowired
    private RouteTypeService routeTypeService;
    
    @Autowired
    private LocationService locationService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private RouteSettingValidator routeSettingValidator;
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    public final void setRouteService(final RouteService routeService) {
        this.routeService = routeService;
    }
    
    public final void setEntityService(final EntityService entityService) {
    }
    
    public final void setRouteTypeService(final RouteTypeService routeTypeService) {
        this.routeTypeService = routeTypeService;
    }
    
    public final void setLocationService(final LocationService locationService) {
        this.locationService = locationService;
    }
    
    public final void setRouteSettingValidator(final RouteSettingValidator routeSettingValidator) {
        this.routeSettingValidator = routeSettingValidator;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final void setCustomerAlertTypeService(final CustomerAlertTypeService customerAlertTypeService) {
    }
    
    public final void setMessageHelper(final MessageHelper messageHelper) {
    }
    
    /**
     * This method will retrieve all customer's route information and display
     * them in Settings->Routes main page.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return target vm file URI which is mapped to Settings->Routes
     *         (/settings/routes/index.vm)
     */
    public String getRouteListByCustomer(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        final Customer customer, final String urlOnSuccess) {
        
        /* retrieves all route information based on the customer ID. */
        final Collection<WebObject> routeObjCol = this.findRoutesByCustomerId(request, customer.getId());
        final Collection<RouteSettingInfo> routeList = new ArrayList<RouteSettingInfo>();
        
        for (WebObject obj : routeObjCol) {
            final Route route = (Route) obj.getWrappedObject();
            routeList.add(new RouteSettingInfo(obj.getPrimaryKey().toString(), route.getName(), route.getRouteType().getName(),
                    route.getRouteType().getLabel()));
        }
        
        final Collection<RandomIdNameSetting> routeTypeList = this.findAllRouteType(request);
        
        /* make an instance for route form. */
        final WebSecurityForm<RouteEditForm> routeEditForm = new WebSecurityForm<RouteEditForm>(null, new RouteEditForm());
        
        /* retrieve customer's location and pay station information. */
        final Collection<RouteInputFormInfo> locationPaystations = this.populateLocationPaystation(request, customer.getId());
        
        /* retrieve customer's route and pay station information. */
        final Collection<RouteInputFormInfo> routePaystations = this.populateRoutePaystation(request, customer.getId());
        
        final PlacementMapBordersInfo mapBorders = this.pointOfSaleService.findMapBordersByCustomerId(customer.getId());
        
        /* put in the model for UI. */
        model.put("mapBorders", mapBorders);
        model.put("routeEditForm", routeEditForm);
        model.put("routeList", routeList);
        model.put("routeTypeList", routeTypeList);
        model.put("locationPaystations", locationPaystations);
        model.put("routePaystations", routePaystations);
        
        return urlOnSuccess;
    }
    
    /**
     * This method will sort the route list based on user input and convert them
     * into JSON list and pass back to UI.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return Velocity file URL (/settings/include/sortRouteList.vm). "false"
     *         in case of any exception or validation failure.
     */
    public String sortRouteList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model, final Customer customer,
        final String urlOnSuccess) {
        
        /* retrieves all route information based on the customer ID. */
        final Collection<WebObject> routeObjCol = this.findRoutesByCustomerId(request, customer.getId());
        final Collection<RouteSettingInfo> routeList = new ArrayList<RouteSettingInfo>();
        
        for (WebObject obj : routeObjCol) {
            final Route route = (Route) obj.getWrappedObject();
            routeList.add(new RouteSettingInfo(obj.getPrimaryKey().toString(), route.getName(), route.getRouteType().getName(),
                    route.getRouteType().getLabel()));
        }
        
        model.put("routeList", routeList);
        
        return urlOnSuccess;
    }
    
    /**
     * This method will view Route Detail information on the page. When
     * "Edit Route" is clicked, it will also be called from front-end logic
     * automatically after calling getRouteForm() method.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return JSON string having route detail information. "false" in case of
     *         any exception or validation failure.
     */
    public String viewRoutesDetails(final HttpServletRequest request, final HttpServletResponse response, final Integer customerId) {
        
        /* validate the randomized routeID from request parameter. */
        final String actualRouteId = routeSettingValidator.validateRouteId(request);
        if (actualRouteId.equals(WebCoreConstants.RESPONSE_FALSE)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Integer routeId = new Integer(actualRouteId);
        final Route route = routeService.findRouteById(routeId);
        
        if (route != null) {
            final RouteDetails routeDetails = this.prepareRouteDetailForJSON(request, customerId, route);
            return WidgetMetricsHelper.convertToJson(routeDetails, "routeDetails", true);
        } else {
            final MessageInfo message = new MessageInfo();
            message.setError(true);
            message.setToken(null);
            message.addMessage(this.commonControllerHelper.getMessage(ErrorConstants.REQUESTED_ITEM_DELETED));
            return WidgetMetricsHelper.convertToJson(message, WebCoreConstants.UI_MESSAGES, true);
        }
    }
    
    /**
     * This method will be called when "Add Route" is clicked.
     * It will return list of all pay stations with JSON format.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return String JSON string containing all pay station list.
     */
    public String getRouteForm(final HttpServletRequest request, final HttpServletResponse response, final Customer customer) {
        
        /* retrieve all pay station list for customer. */
        //        Collection<PaystationListInfo> allPaystationList = this.populateAllPaystationsByCustomer(request, customer.getId(), null);
        
        /* convert RouteDetails to JSON string. */
        //        return WidgetMetricsHelper.convertToJson(allPaystationList, "allPaystationList", true);
        return "true";
    }
    
    /**
     * This method saves Route information in database when save button is
     * clicked from UI.
     * 
     * @param webSecurityForm
     *            user input form
     * @param result
     *            Error object
     * @param response
     *            HttpServletResponse object
     * @return "true" string value if process succeeds, JSON error message if
     *         process fails
     */
    public String saveRoute(final WebSecurityForm<RouteEditForm> webSecurityForm, final BindingResult result, final HttpServletResponse response,
        final Customer customer) {
        
        /* validate user input. */
        this.routeSettingValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        webSecurityForm.resetToken();
        
        final RouteEditForm routeEditForm = webSecurityForm.getWrappedObject();
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_CREATE) {
            /* Add Route. */
            addRoute(routeEditForm, customer);
        } else if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
            /* Edit Route. */
            editRoute(routeEditForm, customer);
        }
        
        return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(":").append(webSecurityForm.getInitToken()).toString();
    }
    
    /**
     * This method deletes Route information and it updates the status flag in
     * Route table.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return "true" string value if process succeeds, "false" string value if
     *         process fails.
     */
    public String deleteRoute(final HttpServletRequest request, final HttpServletResponse response) {
        
        /* validate the randomized routeID from request parameter. */
        final String actualRouteId = routeSettingValidator.validateRouteId(request);
        if (actualRouteId.equals(WebCoreConstants.RESPONSE_FALSE)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Integer routeId = new Integer(actualRouteId);
        
        routeService.deleteRoute(routeId);
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    /**
     * This method finds the customer's Route record from database and set the
     * randomised id.
     * 
     * @param request
     *            HttpServletRequest instance
     * @param customerId
     *            customer ID
     * @return collection of Route
     */
    private Collection<WebObject> findRoutesByCustomerId(final HttpServletRequest request, final Integer customerId) {
        
        /* retrieves all route information based on the customer id. */
        final Collection<Route> routes = routeService.findRoutesByCustomerId(customerId);
        return hideObjectById(request, routes);
    }
    
    /**
     * This method finds customer's locations.
     * 
     * @param request
     *            request HttpServletRequest object
     * @param customerId
     *            customer id
     * @return List of RouteFormInfo object
     */
    private Collection<RouteInputFormInfo> populateLocationPaystation(final HttpServletRequest request, final Integer customerId) {
        
        final Collection<WebObject> webObjects = this.findLocationsByCustomerId(request, customerId);
        final List<RouteInputFormInfo> lists = new ArrayList<RouteInputFormInfo>();
        for (WebObject obj : webObjects) {
            final RouteInputFormInfo routeFormInfo = new RouteInputFormInfo();
            final Location location = (Location) obj.getWrappedObject();
            routeFormInfo.setRandomId(obj.getPrimaryKey().toString());
            routeFormInfo.setName(location.getName());
            if (location.getLocation() != null) {
                routeFormInfo.setParentName(location.getLocation().getName());
            }
            routeFormInfo.setIsParent(location.getIsParent());
            
            lists.add(routeFormInfo);
        }
        
        return lists;
    }
    
    /**
     * This method finds customer's routes.
     * 
     * @param request
     *            request HttpServletRequest object
     * @param customerId
     *            customer id
     * @return List of RouteFormInfo object
     */
    private Collection<RouteInputFormInfo> populateRoutePaystation(final HttpServletRequest request, final Integer customerId) {
        
        final Collection<WebObject> webObjects = this.findRoutesByCustomerId(request, customerId);
        final List<RouteInputFormInfo> lists = new ArrayList<RouteInputFormInfo>();
        for (WebObject obj : webObjects) {
            final RouteInputFormInfo routeFormInfo = new RouteInputFormInfo();
            final Route route = (Route) obj.getWrappedObject();
            routeFormInfo.setRandomId(obj.getPrimaryKey().toString());
            routeFormInfo.setName(route.getName());
            
            lists.add(routeFormInfo);
        }
        
        return lists;
    }
    
    /**
     * This method retrieves all pay station list for customer and return the
     * collection of PaystationListInfo object.
     * 
     * @param request
     *            request HttpServletRequest object
     * @param customerId
     *            customer id
     * @param routeId
     *            route id
     * @return Collection of PaystationListInfo object
     */
    //    private List<PaystationListInfo> populateAllPaystationsByCustomer(HttpServletRequest request, Integer customerId, Integer routeId) {
    //        WebObjectId routeWOI = new WebObjectId(Route.class, routeId);
    //        PointOfSaleSearchCriteria criteria = new PointOfSaleSearchCriteria();
    //        criteria.setCustomerId(customerId);
    //        criteria.setIncludeAlertStatus(false);
    //        criteria.setShowDeactivated(true);
    //        criteria.setShowDecommissioned(false);
    //        criteria.setShowHidden(false);
    //        
    //        
    //        List<PaystationListInfo> posList = this.pointOfSaleService.findPaystation(criteria);
    //        for (PaystationListInfo pos : posList) {
    //            if (pos.getRandomRouteId().contains(routeWOI)) {
    //                pos.setStatus("selected");
    //            } else {
    //                pos.setStatus("active");
    //            }
    //        }
    //        
    //        return posList;
    //    }
    
    private List<PaystationListInfo> populateAllPaystationsByRoute(final HttpServletRequest request, final Integer customerId,
        final Integer routeId) {
        final WebObjectId routeWOI = new WebObjectId(Route.class, routeId);
        final PointOfSaleSearchCriteria criteria = new PointOfSaleSearchCriteria();
        criteria.setCustomerId(customerId);
        criteria.setIncludeAlertStatus(false);
        criteria.setShowDeactivated(true);
        criteria.setShowDecommissioned(false);
        criteria.setShowHidden(false);
        criteria.setRouteId(routeId);
        
        final List<PaystationListInfo> posList = this.pointOfSaleService.findPaystation(criteria);
        for (PaystationListInfo pos : posList) {
            if (pos.getRandomRouteId().contains(routeWOI)) {
                pos.setStatus("selected");
            } else {
                pos.setStatus("active");
            }
        }
        
        return posList;
    }
    
    /**
     * This method finds customer's location information and wraps them with
     * WebObject.
     * 
     * @param request
     *            HttpServletRequest object
     * @param customerId
     *            customer ID
     * @return Collection of WebObject objects
     */
    private Collection<WebObject> findLocationsByCustomerId(final HttpServletRequest request, final Integer customerId) {
        /* retrieves all route information based on the customer id. */
        final List<Location> locations = locationService.findLocationsByCustomerId(customerId);
        return hideObjectById(request, locations);
    }
    
    /**
     * This method finds all route types from RouteType table.
     * 
     * @param request
     *            HttpServletRequest object
     * @return Collection of WebObject
     */
    private Collection<RandomIdNameSetting> findAllRouteType(final HttpServletRequest request) {
        
        /* retrieve all route types. */
        final Collection<RouteType> routeTypes = routeTypeService.findAllRouteType();
        final Collection<WebObject> objList = hideObjectById(request, routeTypes);
        final Collection<RandomIdNameSetting> result = new ArrayList<RandomIdNameSetting>();
        for (WebObject obj : objList) {
            final RouteType type = (RouteType) obj.getWrappedObject();
            result.add(new RandomIdNameSetting(obj.getPrimaryKey().toString(), type.getName(), type.getLabel()));
        }
        return result;
    }
    
    /**
     * This method wraps Collection with the primary key and return back.
     * 
     * @param request
     *            HttpServletRequest object
     * @param collection
     *            Collection instance
     * @return Collection of WebObject
     */
    @SuppressWarnings("unchecked")
    private Collection<WebObject> hideObjectById(final HttpServletRequest request, final Collection<?> collection) {
        
        /* make randomised key and set the value to randomId in Route table. */
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        return keyMapping.hideProperties(collection, WebCoreConstants.ID_LOOK_UP_NAME);
    }
    
    /**
     * This method prepares RouteDetails to make JSON string.
     * 
     * @param route
     *            Route instance
     * @return RouteDetails instance
     */
    private RouteDetails prepareRouteDetailForJSON(final HttpServletRequest request, final Integer customerId, final Route route) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        /* prepare RouteDetails instance for JSON string. */
        final RouteDetails routeDetails = new RouteDetails();
        routeDetails.setRandomRouteId(request.getParameter("routeID"));
        routeDetails.setName(route.getName());
        routeDetails.setRandomTypeId(keyMapping.getRandomString(route.getRouteType(), WebCoreConstants.ID_LOOK_UP_NAME));
        routeDetails.setTypeName(route.getRouteType().getName());
        final List<RoutePOS> routePOSList = new ArrayList<RoutePOS>(route.getRoutePOSes());
        
        Collections.sort(routePOSList, new Comparator<RoutePOS>() {
            public int compare(final RoutePOS r1, final RoutePOS r2) {
                return r1.getId() - r2.getId() < 0 ? -1 : (r1.getId() - r2.getId() > 0) ? 1 : 0;
            }
        });
        
        /* get widget map info. */
        final WidgetMapInfo widgetMapInfo = new WidgetMapInfo();
        for (RoutePOS routePOS : routePOSList) {
            if ((routePOS.getPointOfSale().getLatitude() != null) && (routePOS.getPointOfSale().getLongitude() != null)) {
                widgetMapInfo.addMapEntry(routePOS.getPointOfSale().getPaystation().getPaystationType().getId(), routePOS.getPointOfSale());
            } else {
                if ((routePOS.getPointOfSale().getPaystation().getPaystationType().getId() != WebCoreConstants.PAY_STATION_TYPE_TEST_VIRTUAL)
                    && routePOS.getPointOfSale().getPosStatus().isIsActivated() && routePOS.getPointOfSale().getPosStatus().isIsVisible()) {
                    widgetMapInfo.setMissingPayStation(true);
                }
            }
        }
        final List<Integer> routeIds = new ArrayList<Integer>();
        routeIds.add(route.getId());
        //        int unplacedCount = pointOfSaleService.countUnPlacedByRouteIds(routeIds);
        //        if (unplacedCount>0) {
        //            widgetMapInfo.setMissingPayStation(true);
        //        }
        routeDetails.setWidgetMapInfo(widgetMapInfo);
        
        /* get route type list. */
        routeDetails.setRouteTypeList(this.findAllRouteType(request));
        
        /* get location paystation list. */
        routeDetails.setLocationPaystationList(this.populateLocationPaystation(request, customerId));
        
        /* get route pay station list. */
        //        routeDetails.setRoutePaystationList(this.populateRoutePaystation(request, customerId));
        
        /* get all pay station list for the customer. */
        //        routeDetails.setAllPaystationList(this.populateAllPaystationsByCustomer(request, customerId, routeId));
        routeDetails.setAllPaystationList(this.populateAllPaystationsByRoute(request, customerId, route.getId()));
        
        return routeDetails;
    }
    
    /**
     * This method adds new Route.
     * 
     * @param routeEditForm
     *            User input form from UI.
     */
    private void addRoute(final RouteEditForm routeEditForm, final Customer customer) {
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        /* set up new Route instance. */
        final Route route = new Route();
        final Date currentTime = new Date();
        final Integer userId = userAccount.getId();
        final RouteType routeType = new RouteType();
        routeType.setId(routeEditForm.getRouteTypeId().byteValue());
        route.setCustomer(customer);
        route.setRouteType(routeType);
        route.setName(routeEditForm.getRouteName());
        route.setLastModifiedGmt(currentTime);
        route.setLastModifiedByUserId(userId);
        
        final Set<RoutePOS> routePOSes = getRoutePOSSet(routeEditForm, route, currentTime, userId);
        route.setRoutePOSes(routePOSes);
        
        this.routeService.saveRouteAndRoutePOS(route);
    }
    
    /**
     * This method updates existing Route.
     * 
     * @param routeEditForm
     *            User input form from UI.
     */
    private void editRoute(final RouteEditForm routeEditForm, final Customer customer) {
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        /* retrieve existing Route information from DB. */
        final Route route = this.routeService.findRouteByIdAndEvict(routeEditForm.getRouteId());
        
        final Date currentTime = new Date();
        final Integer userId = userAccount.getId();
        final RouteType routeType = new RouteType();
        routeType.setId(routeEditForm.getRouteTypeId().byteValue());
        route.setName(routeEditForm.getRouteName());
        route.setRouteType(routeType);
        
        route.setLastModifiedByUserId(userId);
        route.setLastModifiedGmt(currentTime);
        
        final Set<RoutePOS> routePOSes = getRoutePOSSet(routeEditForm, route, currentTime, userId);
        route.setRoutePOSes(routePOSes);
        
        this.routeService.updateRouteAndRoutePOS(route);
    }
    
    /**
     * This method prepares Set<RoutePOS> to be used to add or update Route
     * information.
     * 
     * @param routeEditForm
     *            User input form from UI.
     * @param route
     *            Route object to be added or updated.
     * @param currentTime
     *            Current time
     * @param userId
     *            User account ID
     * @return Set<RoutePOS>
     */
    private Set<RoutePOS> getRoutePOSSet(final RouteEditForm routeEditForm, final Route route, final Date currentTime, final Integer userId) {
        
        final Map<String, RoutePOS> rposes = new HashMap<String, RoutePOS>();
        final Collection<Integer> pointOfSaleIds = routeEditForm.getPointOfSaleIds();
        if (!pointOfSaleIds.isEmpty()) {
            for (Integer id : pointOfSaleIds) {
                final RoutePOS rp = new RoutePOS();
                final PointOfSale pos = new PointOfSale();
                pos.setId(id);
                rp.setPointOfSale(pos);
                rp.setRoute(route);
                final String rposKey = id.toString();
                if (!rposes.containsKey(rposKey)) {
                    rposes.put(rposKey, rp);
                }
            }
        }
        final Set<RoutePOS> routePOSes = new HashSet<RoutePOS>(rposes.values());
        return routePOSes;
    }
}
