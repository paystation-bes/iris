package com.digitalpaytech.mvc;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;

import com.digitalpaytech.data.RolePermissionInfo;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerRole;
import com.digitalpaytech.domain.CustomerType;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.RoleStatusType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.UserName;
import com.digitalpaytech.dto.customeradmin.PermissionTree;
import com.digitalpaytech.dto.customeradmin.PermissionTreeWrapper;
import com.digitalpaytech.dto.customeradmin.RoleSettingDetails;
import com.digitalpaytech.dto.customeradmin.RoleSettingInfo;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.RoleEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.PermissionService;
import com.digitalpaytech.util.DashboardUtil;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObject;
import com.digitalpaytech.validation.RoleSettingValidator;

/**
 * This class handles the User Role setting request.
 * 
 * @author Brian Kim
 * 
 */
@Controller
public class RoleSettingController extends UserAccountSettingController {
    
    private static Logger log = Logger.getLogger(RoleSettingController.class);
    
    @Autowired
    private PermissionService permissionService;
    
    @Autowired
    private RoleSettingValidator roleSettingValidator;
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    public final void setPermissionService(final PermissionService permissionService) {
        this.permissionService = permissionService;
    }
    
    public final void setRoleSettingValidator(final RoleSettingValidator roleSettingValidator) {
        this.roleSettingValidator = roleSettingValidator;
    }
    
    /**
     * This method will retrieve user account's role information.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @param urlOnSuccess
     *            URL when the process is successful.
     * @return urlOnSuccess value if process succeeds, null if fails.
     */
    protected final String getRoleList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        final String urlOnSuccess) {
        
        /* make an instance for role edit form. */
        final WebSecurityForm<RoleEditForm> roleEditForm = new WebSecurityForm<RoleEditForm>(null, new RoleEditForm());
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        /* retrieve role information. */
        final Collection<RoleSettingInfo> roleSettingInfo =
                retrieveRoleInfo(request, userAccount.getCustomer(), userAccount.getCustomer().getParentCustomer());
        
        model.put("roleEditForm", roleEditForm);
        model.put("roleSettingInfo", roleSettingInfo);
        
        return urlOnSuccess;
    }
    
    /**
     * This method will view User's Role Detail information on the page. When
     * "Edit Role" is clicked, it will also be called from front-end logic
     * automatically after calling getRoleForm() method.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return JSON string having role detail information. "false" in case of
     *         any exception or validation failure.
     */
    @SuppressWarnings("checkstyle:designforextension")
    protected String viewRoleDetails(final HttpServletRequest request, final HttpServletResponse response) {
        
        /* validate the randomized roleId from request parameter. */
        final String actualRoleId = this.validateRoleId(request);
        if (actualRoleId.equals(WebCoreConstants.RESPONSE_FALSE)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Integer roleId = new Integer(actualRoleId);
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final Role role = this.roleService.findRoleByRoleIdAndEvict(roleId);
        
        if (role == null) {
            final MessageInfo message = new MessageInfo();
            message.setError(true);
            message.setToken(null);
            message.addMessage(this.commonControllerHelper.getMessage(ErrorConstants.REQUESTED_ITEM_DELETED));
            return WidgetMetricsHelper.convertToJson(message, WebCoreConstants.UI_MESSAGES, true);
        }
        
        final RoleSettingDetails roleSettingDetails = prepareRoleDetailsForJSON(request, userAccount, role);
        
        /* convert RoleSettingDetails to JSON string. */
        return WidgetMetricsHelper.convertToJson(roleSettingDetails, "roleSettingDetails", true);
    }
    
    /**
     * This method will be called when "Add Role" is clicked. It
     * will set the user form and list of Role information.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return JSON string having all permission lists, "false" if fails.
     */
    protected final String formUserRole(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* retrieve user's permission information. */
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final PermissionTreeWrapper permissionTreeWrapper = new PermissionTreeWrapper();
        final int customerType = userAccount.getCustomer().getCustomerType().getId();
        permissionTreeWrapper.setPermissionTrees(this.populatePermissionTreeList(request, userAccount, null, true, customerType));
        if (customerType == WebCoreConstants.CUSTOMER_TYPE_PARENT) {
            permissionTreeWrapper.setSecondaryPermissionTrees(this.populatePermissionTreeList(request, userAccount, null, true,
                                                                                              WebCoreConstants.CUSTOMER_TYPE_CHILD));
        }
        
        /* convert RouteDetails to JSON string. */
        return WidgetMetricsHelper.convertToJson(permissionTreeWrapper, "permissionTreeWrapper", true);
    }
    
    /**
     * This method saves role information in Role and RolePermission table.
     * 
     * @param webSecurityForm
     *            user input form
     * @param result
     *            Error object
     * @param response
     *            HttpServletResponse object
     * @return "true" + WebCoreConstants.COLON + token string value if process succeeds, JSON error
     *         message if process fails
     */
    @SuppressWarnings("checkstyle:designforextension")
    protected String saveRole(final WebSecurityForm<RoleEditForm> webSecurityForm, final BindingResult result, final HttpServletResponse response) {
        
        /* validate user input. */
        this.roleSettingValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final RoleEditForm form = webSecurityForm.getWrappedObject();
        final Role existingRole = this.roleService.findRoleByName(userAccount.getCustomer().getId(), form.getRoleName());
        if ((existingRole != null) && ((form.getRoleId() == null) || (!form.getRoleId().equals(existingRole.getId())))) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("roleName",
                    this.roleSettingValidator.getMessage("error.common.duplicated", new Object[] { this.roleSettingValidator.getMessage("label.role"),
                        this.roleSettingValidator.getMessage("label.settings.roles.RoleName"), })));
            webSecurityForm.resetToken();
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        webSecurityForm.resetToken();
        
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_CREATE) {
            /* Add Role. */
            return addRole(webSecurityForm, userAccount);
        } else if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
            /* Edit Role. */
            return editRole(webSecurityForm, userAccount);
        } else {
            return WebCoreConstants.RESPONSE_FALSE;
        }
    }
    
    /**
     * This method verifies that a role can be deleted. It checks for
     * users and returns true or false.
     * 
     * @param request
     * @param response
     * @param model
     * @return "true" or "false" depending on successful validation
     */
    
    @SuppressWarnings("checkstyle:designforextension")
    public String verifyDeleteRole(final HttpServletRequest request, final HttpServletResponse response) {
        
        /* validate the randomized role ID from request parameter. */
        final String actualRoleId = this.validateRoleId(request);
        if (actualRoleId.equals(WebCoreConstants.RESPONSE_FALSE)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Integer roleId = new Integer(actualRoleId);
        final Role role = this.roleService.findRoleByRoleIdAndEvict(roleId);
        
        if (role.isIsLocked()) {
            return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON
                   + this.roleSettingValidator.getMessage("error.settings.roles.locked.role");
        }
        final List<UserAccount> userAccountList = this.roleService.findUserAccountByRoleId(roleId);
        if (userAccountList != null && !userAccountList.isEmpty()) {
            final StringBuilder userList = new StringBuilder();
            try {
                for (UserAccount userAccount : userAccountList) {
                    userList.append(URLDecoder.decode(userAccount.getRealUserName(), WebSecurityConstants.URL_ENCODING_LATIN1)).append(", ");
                }
                userList.delete(userList.length() - 2, userList.length());
            } catch (UnsupportedEncodingException e) {
                log.error("+++ Failed to encode user name when updating new user.", e);
                return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON
                       + this.roleSettingValidator.getMessage("error.settings.roles.without.users");
            }
            return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON
                   + this.roleSettingValidator.getMessage("error.settings.roles.with.users", userList.toString());
        }
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    /**
     * This method deletes role information by setting type to 2 (deleted)
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return "true" string value if process succeeds, "false" string value if
     *         process fails.
     */
    
    @SuppressWarnings("checkstyle:designforextension")
    protected String deleteRole(final HttpServletRequest request, final HttpServletResponse response) {
        
        /* validate the randomized role ID from request parameter. */
        final String actualRoleId = this.validateRoleId(request);
        if (actualRoleId.equals(WebCoreConstants.RESPONSE_FALSE)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Integer roleId = new Integer(actualRoleId);
        final Role role = this.roleService.findRoleByRoleIdAndEvict(roleId);
        
        if (role.isIsLocked()) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final RoleStatusType roleStatusType = new RoleStatusType();
        roleStatusType.setId(WebCoreConstants.ROLE_STATUS_TYPE_DELETED);
        role.setRoleStatusType(roleStatusType);
        role.setLastModifiedGmt(new Date());
        role.setUserAccount(userAccount);
        
        this.roleService.deleteRole(role);
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    /**
     * This method constructs permission tree to display in Role Details page
     * and Edit form page.
     * 
     * @param request
     *            HttpServletRequest object
     * @param rolePermissionInfo
     *            collection of WebObject hidden by child permission Id
     * @param isEditable
     *            true if this tree is shown in add, edit role page (when
     *            required randomized IDs), false if this tree is shown detail
     *            page which is not editable.
     * @return Map object (key: parent permission name, value: collection of
     *         PermissionTree objects).
     */
    protected final Map<String, Collection<PermissionTree>> constructPermissionTree(final HttpServletRequest request,
        final Collection<RolePermissionInfo> rolePermissionInfo, final boolean isEditable) {
        
        final Map<String, Collection<PermissionTree>> map = new LinkedHashMap<String, Collection<PermissionTree>>();
        
        if (isEditable) {
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            @SuppressWarnings("unchecked")
            final Collection<WebObject> hiddenRolePermissionInfo =
                    keyMapping.hideProperties(rolePermissionInfo, FormFieldConstants.PARAMETER_CHILD_PERMISSION_ID);
            
            for (WebObject obj : hiddenRolePermissionInfo) {
                final String randomChildPermissionId = obj.getPrimaryKey().toString();
                final RolePermissionInfo info = (RolePermissionInfo) obj.getWrappedObject();
                final String key = info.getParentPermissionName();
                if (map.containsKey(key)) {
                    Collection<PermissionTree> val = map.get(key);
                    
                    if (val == null) {
                        val = new ArrayList<PermissionTree>();
                    }
                    
                    val.add(new PermissionTree(randomChildPermissionId, info.getChildPermissionName(), StandardConstants.STRING_ACTIVE));
                    
                } else {
                    final Collection<PermissionTree> value = new ArrayList<PermissionTree>();
                    value.add(new PermissionTree(randomChildPermissionId, info.getChildPermissionName(), StandardConstants.STRING_ACTIVE));
                    
                    map.put(key, value);
                }
            }
        } else {
            for (RolePermissionInfo info : rolePermissionInfo) {
                final String key = info.getParentPermissionName();
                if (map.containsKey(key)) {
                    Collection<PermissionTree> val = map.get(key);
                    if (val == null) {
                        val = new ArrayList<PermissionTree>();
                    }
                    
                    val.add(new PermissionTree(info.getChildPermissionName()));
                } else {
                    final Collection<PermissionTree> value = new ArrayList<PermissionTree>();
                    value.add(new PermissionTree(info.getChildPermissionName()));
                    map.put(key, value);
                }
            }
        }
        
        return map;
    }
    
    /**
     * This method constructs permission tree to display all permission lists.
     * 
     * @param request
     *            HttpServletRequest object
     * @param rolePermissionInfo
     *            collection of WebObject hidden by child permission Id
     * @param roleId
     *            Role ID
     * @return Map object (key: parent permission name, value: collection of
     *         PermissionTree objects).
     */
    protected final Map<String, Collection<PermissionTree>> constructAllPermissionTree(final HttpServletRequest request,
        final Collection<RolePermissionInfo> rolePermissionInfo, final Integer roleId) {
        
        final Map<String, Collection<PermissionTree>> map = new LinkedHashMap<String, Collection<PermissionTree>>();
        
        /* retrieve all permissions that the role has. */
        final Collection<RolePermissionInfo> selectedPermissions = this.permissionService.findAllPermissionsByRoleId(roleId);
        final Collection<Integer> selectedPermissionIdList = new ArrayList<Integer>();
        for (RolePermissionInfo in : selectedPermissions) {
            selectedPermissionIdList.add(in.getChildPermissionId());
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        @SuppressWarnings("unchecked")
        final Collection<WebObject> hiddenRolePermissionInfo =
                keyMapping.hideProperties(rolePermissionInfo, FormFieldConstants.PARAMETER_CHILD_PERMISSION_ID);
        
        for (WebObject obj : hiddenRolePermissionInfo) {
            final String randomChildPermissionId = obj.getPrimaryKey().toString();
            final RolePermissionInfo info = (RolePermissionInfo) obj.getWrappedObject();
            final String key = info.getParentPermissionName();
            if (map.containsKey(key)) {
                Collection<PermissionTree> val = map.get(key);
                
                if (val == null) {
                    val = new ArrayList<PermissionTree>();
                }
                
                val.add(new PermissionTree(randomChildPermissionId, info.getChildPermissionName(),
                        selectedPermissionIdList.contains(info.getChildPermissionId()) ? StandardConstants.STRING_SELECTED
                                : StandardConstants.STRING_ACTIVE));
            } else {
                final Collection<PermissionTree> value = new ArrayList<PermissionTree>();
                value.add(new PermissionTree(randomChildPermissionId, info.getChildPermissionName(),
                        selectedPermissionIdList.contains(info.getChildPermissionId()) ? StandardConstants.STRING_SELECTED
                                : StandardConstants.STRING_ACTIVE));
                map.put(key, value);
            }
        }
        
        return map;
    }
    
    /**
     * This method validates user input randomized role ID.
     * 
     * @param request
     *            HttpServletRequest object
     * @return actual role ID if process succeeds, "false" string if process
     *         fails.
     */
    private String validateRoleId(final HttpServletRequest request) {
        
        /* validate the randomized role ID from request parameter. */
        final String randomizedRoleId = request.getParameter(FormFieldConstants.PARAMETER_ROLE_ID);
        if (StringUtils.isBlank(randomizedRoleId) || !DashboardUtil.validateRandomId(randomizedRoleId)) {
            log.warn("### Invalid roleID blank in the request. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        /* validate the actual role ID. */
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Object actualRoleId = keyMapping.getKey(randomizedRoleId);
        if (actualRoleId == null || !DashboardUtil.validateDBIntegerPrimaryKey(actualRoleId.toString())) {
            log.warn("### Invalid roleID doesn't exist in the request. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return actualRoleId.toString();
    }
    
    /**
     * This method prepares RoleSettingDetails information for JSON string.
     * 
     * @param request
     *            HttpServletRequest object
     * @param userAccount
     *            UserAccount object
     * @param roleId
     *            Role ID
     * @return RoleSettingDetails object
     */
    private RoleSettingDetails prepareRoleDetailsForJSON(final HttpServletRequest request, final UserAccount userAccount, final Role role) {
        
        final List<RolePermissionInfo> rolePermissionInfo =
                (List<RolePermissionInfo>) this.permissionService.findAllPermissionsByRoleId(role.getId());
        final Map<String, Collection<PermissionTree>> tempMap = constructPermissionTree(request, rolePermissionInfo, false);
        
        final RoleSettingDetails details = new RoleSettingDetails();
        details.setRandomId(request.getParameter(FormFieldConstants.PARAMETER_ROLE_ID));
        if (!tempMap.isEmpty()) {
            details.setRoleName(((RolePermissionInfo) rolePermissionInfo.get(0)).getRoleName());
            
            if (((RolePermissionInfo) rolePermissionInfo.get(0)).getStatus().intValue() == WebCoreConstants.ROLE_STATUS_TYPE_ENABLED) {
                details.setIsEnabled(true);
            }
        }
        
        // 3 different behaviour depending on the parent type
        List<UserAccount> userAccountList = null;
        if (userAccount.getCustomer().isIsParent()) {
            // 1) Parent company include child users with the customer name that created the user
            final List<Integer> customerIdList = new ArrayList<Integer>();
            customerIdList.add(userAccount.getCustomer().getId());
            final List<Customer> childCustomerList = customerService.findAllChildCustomers(userAccount.getCustomer().getId());
            for (Customer customer : childCustomerList) {
                customerIdList.add(customer.getId());
            }
            userAccountList = this.userAccountService.findUserAccountByRoleIdAndCustomerIdList(role.getId(), customerIdList);
            for (UserAccount account : userAccountList) {
                try {
                    details.getAssignedUsers()
                            .add(new UserName(URLDecoder.decode(account.getRealFirstName(), WebSecurityConstants.URL_ENCODING_LATIN1),
                                    URLDecoder.decode(account.getRealLastName(), WebSecurityConstants.URL_ENCODING_LATIN1),
                                    account.getCustomer().getName()));
                } catch (UnsupportedEncodingException e) {
                    log.error("+++ Failed to encode user name when preparing role details.", e);
                    details.getAssignedUsers()
                            .add(new UserName(account.getRealFirstName(), account.getRealLastName(), account.getCustomer().getName()));
                }
            }
        } else {
            userAccountList = this.userAccountService.findUserAccountByRoleIdAndCustomerId(role.getId(), userAccount.getCustomer().getId());
            if (userAccount.getCustomer().getParentCustomer() != null) {
                // 2) Child customers include users created by parent customer and the customer name that created the user
                for (UserAccount account : userAccountList) {
                    String customerName = null;
                    if (account.isIsAliasUser()) {
                        customerName = userAccount.getCustomer().getParentCustomer().getName();
                    } else {
                        customerName = userAccount.getCustomer().getName();
                    }
                    try {
                        details.getAssignedUsers()
                                .add(new UserName(URLDecoder.decode(account.getRealFirstName(), WebSecurityConstants.URL_ENCODING_LATIN1),
                                        URLDecoder.decode(account.getRealLastName(), WebSecurityConstants.URL_ENCODING_LATIN1), customerName));
                        
                    } catch (UnsupportedEncodingException e) {
                        log.error("+++ Failed to encode user name when preparing role details.", e);
                        details.getAssignedUsers().add(new UserName(account.getRealFirstName(), account.getRealLastName(), customerName));
                    }
                }
            } else {
                // 3) No customer name is necessary since all users belong to the same customer
                for (UserAccount account : userAccountList) {
                    try {
                        details.getAssignedUsers()
                                .add(new UserName(URLDecoder.decode(account.getRealFirstName(), WebSecurityConstants.URL_ENCODING_LATIN1),
                                        URLDecoder.decode(account.getRealLastName(), WebSecurityConstants.URL_ENCODING_LATIN1)));
                        
                    } catch (UnsupportedEncodingException e) {
                        log.error("+++ Failed to encode user name when preparing role details.", e);
                        details.getAssignedUsers().add(new UserName(account.getRealFirstName(), account.getRealLastName()));
                    }
                }
                
            }
        }
        
        details.setTypeId(role.getCustomerType().getId());
        details.setPermissionList(populatePermissionTreeList(request, userAccount, role.getId(), false, role.getCustomerType().getId()));
        
        return details;
    }
    
    /**
     * This method returns PermissionTree collection which is used to display
     * all permissions for adding or editing role.
     * 
     * @param request
     *            HttpServletRequest object
     * @param userAccount
     *            UserAccount object
     * @param roleId
     *            Role ID
     * @param isEditable
     *            true if this tree is shown in add, edit role page (when
     *            required randomized IDs), false if this tree is shown detail
     *            page which is not editable.
     * @return List of PermissionTree objects.
     */
    private List<PermissionTree> populatePermissionTreeList(final HttpServletRequest request, final UserAccount userAccount, final Integer roleId,
        final boolean isEditable, final Integer customerType) {
        
        final List<RolePermissionInfo> rolePermissionInfo =
                (List<RolePermissionInfo>) this.permissionService.findAllPermissionsByCustomerTypeId(customerType);
        
        Map<String, Collection<PermissionTree>> map = null;
        
        if (roleId == null) {
            map = constructPermissionTree(request, rolePermissionInfo, isEditable);
        } else {
            map = constructAllPermissionTree(request, rolePermissionInfo, roleId);
        }
        
        final List<PermissionTree> permissions = new ArrayList<PermissionTree>();
        if (!map.isEmpty()) {
            final Set<String> keys = map.keySet();
            for (String key : keys) {
                permissions.add(new PermissionTree(key, map.get(key)));
            }
        }
        
        return permissions;
    }
    
    /**
     * This method adds new role to Role and RolePermission table.
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     * @param userAccount
     *            UserAccount object
     */
    private String addRole(final WebSecurityForm<RoleEditForm> webSecurityForm, final UserAccount userAccount) {
        
        final RoleEditForm roleEditForm = webSecurityForm.getWrappedObject();
        final Date lastModifiedGmt = new Date();
        
        final Role role = new Role();
        final RoleStatusType roleStatusType = new RoleStatusType();
        
        role.setCustomer(userAccount.getCustomer());
        final CustomerType customerType = new CustomerType();
        customerType.setId(roleEditForm.getTypeId());
        role.setCustomerType(customerType);
        role.setIsFromParent(userAccount.getCustomer().getCustomerType().getId() == WebCoreConstants.CUSTOMER_TYPE_PARENT ? true : false);
        
        role.setName(roleEditForm.getRoleName().trim());
        role.setIsAdmin(false);
        
        /* status is removed from the page, so will comment out for future use. */
        //      if (roleEditForm.getStatusEnabled()) {
        roleStatusType.setId(WebCoreConstants.ROLE_STATUS_TYPE_ENABLED);
        //      } else {
        //          roleStatusType.setId(WebCoreConstants.ROLE_STATUS_TYPE_DISABLED);
        //      }
        
        role.setRoleStatusType(roleStatusType);
        role.setIsLocked(false);
        role.setLastModifiedGmt(lastModifiedGmt);
        role.setUserAccount(userAccount);
        
        /* set up CustomerRole. */
        final Set<CustomerRole> customerRoles = new HashSet<CustomerRole>();
        final CustomerRole customerRole = new CustomerRole();
        customerRole.setCustomer(userAccount.getCustomer());
        customerRole.setRole(role);
        customerRole.setLastModifiedGmt(lastModifiedGmt);
        customerRole.setUserAccount(userAccount);
        customerRoles.add(customerRole);
        if (userAccount.getCustomer().getCustomerType().getId() == WebCoreConstants.CUSTOMER_TYPE_PARENT
            && role.getCustomerType().getId() == WebCoreConstants.CUSTOMER_TYPE_CHILD) {
            final List<Customer> childCustomerList = this.customerService.findAllChildCustomers(userAccount.getCustomer().getId());
            for (Customer childCustomer : childCustomerList) {
                final CustomerRole childCustomerRole = new CustomerRole();
                childCustomerRole.setCustomer(childCustomer);
                childCustomerRole.setRole(role);
                childCustomerRole.setLastModifiedGmt(lastModifiedGmt);
                childCustomerRole.setUserAccount(userAccount);
                customerRoles.add(childCustomerRole);
            }
        }
        
        role.setCustomerRoles(customerRoles);
        role.setRolePermissions(this.setRolePermissionList(role, roleEditForm, lastModifiedGmt, userAccount));
        this.roleService.saveRoleAndRolePermission(role);
        
        return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(WebCoreConstants.COLON).append(webSecurityForm.getInitToken()).toString();
    }
    
    /**
     * This method updates existing role to Role table.
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     * @param userAccount
     *            UserAccount object
     */
    private String editRole(final WebSecurityForm<RoleEditForm> webSecurityForm, final UserAccount userAccount) {
        
        final RoleEditForm roleEditForm = webSecurityForm.getWrappedObject();
        final Date lastModifiedGmt = new Date();
        
        final Integer roleId = roleEditForm.getRoleId();
        final Role role = this.roleService.findRoleByRoleIdAndEvict(roleId);
        
        final CustomerType customerType = new CustomerType();
        customerType.setId(roleEditForm.getTypeId());
        role.setCustomerType(customerType);
        
        role.setName(roleEditForm.getRoleName().trim());
        
        if (role.isIsLocked()) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        /* status is removed from the page, so will comment out for future use. */
        //        if (roleEditForm.getStatusEnabled()) {
        //            RoleStatusType type = new RoleStatusType();
        //            type.setId(WebCoreConstants.ROLE_STATUS_TYPE_ENABLED);
        //            role.setRoleStatusType(type);
        //        } else {
        //            RoleStatusType type = new RoleStatusType();
        //            type.setId(WebCoreConstants.ROLE_STATUS_TYPE_DISABLED);
        //            role.setRoleStatusType(type);
        //        }
        
        role.setLastModifiedGmt(lastModifiedGmt);
        role.setUserAccount(userAccount);
        
        role.setRolePermissions(this.setRolePermissionList(role, roleEditForm, lastModifiedGmt, userAccount));
        this.roleService.updateRoleAndRolePermission(role);
        
        return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(WebCoreConstants.COLON).append(webSecurityForm.getInitToken()).toString();
    }
    
    /**
     * This method sets user input permission lists to RolePermission set to
     * save or update RolePermission table.
     * 
     * @param roleEditForm
     *            UserAccountEditForm object
     * @param lastModifiedGmt
     *            Date object showing last modified time
     * @param userAccount
     *            UserAccount object
     * @return RolePermission set
     */
    private Set<RolePermission> setRolePermissionList(final Role role, final RoleEditForm roleEditForm, final Date lastModifiedGmt,
        final UserAccount userAccount) {
        
        final Set<RolePermission> rolePermissions = new HashSet<RolePermission>();
        final Collection<Integer> permissionIds = roleEditForm.getPermissionIds();
        for (Integer id : permissionIds) {
            final RolePermission rp = new RolePermission();
            final Permission p = new Permission();
            p.setId(id);
            rp.setRole(role);
            rp.setPermission(p);
            rp.setLastModifiedGmt(lastModifiedGmt);
            rp.setUserAccount(userAccount);
            rolePermissions.add(rp);
        }
        return rolePermissions;
    }
    
    /**
     * This method retrieves necessary data for input form from database and
     * prepares for the form for adding, editing user.
     * 
     * @param request
     *            HttpServletRequest object
     * @param userAccount
     *            UserAccount object
     * @param userSelectedRoleIdList
     *            current user's role id list
     * @return Collection of RoleSettingInfo object
     */
    private Collection<RoleSettingInfo> retrieveRoleInfo(final HttpServletRequest request, final Customer customer, final Customer parentCustomer) {
        
        final Collection<WebObject> rolesObjList = super.findCustomerRolesByCustomerId(request, customer.getId());
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final Map<Integer, Boolean> roleSettingMap = roleService.findNumberOfUserRolesPerRoleIdByCustomerId(customer.getId());
        
        final Collection<RoleSettingInfo> roleSettingInfo = new ArrayList<RoleSettingInfo>();
        for (WebObject obj : rolesObjList) {
            final CustomerRole customerRole = (CustomerRole) obj.getWrappedObject();
            boolean isLocked = customerRole.getRole().isIsLocked();
            
            if ((userAccount.getCustomer().getCustomerType().getId() == WebCoreConstants.CUSTOMER_TYPE_CHILD)
                && customerRole.getRole().getIsFromParent()) {
                isLocked = true;
            }
            
            Boolean hasUserRole = roleSettingMap.get(customerRole.getRole().getId());
            if (hasUserRole == null) {
                hasUserRole = false;
            }
            
            final RoleSettingInfo roleSetting =
                    new RoleSettingInfo(obj.getPrimaryKey().toString(), customerRole.getRole().getName(), StandardConstants.STRING_ACTIVE,
                            customerRole.getRole().getRoleStatusType().getId() == WebCoreConstants.ROLE_STATUS_TYPE_ENABLED ? true : false, isLocked,
                            hasUserRole, customerRole.getRole().getCustomerType().getId(), customerRole.getRole().getIsFromParent());
            if (parentCustomer != null) {
                if (customerRole.getRole().getIsFromParent()) {
                    roleSetting.setCustomerName(parentCustomer.getName());
                } else {
                    roleSetting.setCustomerName(customer.getName());
                }
            }
            roleSettingInfo.add(roleSetting);
        }
        return roleSettingInfo;
    }
}
