package com.digitalpaytech.mvc.helper;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.Consumer;
import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.domain.CustomerCard;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.customeradmin.CouponDTO;
import com.digitalpaytech.dto.customeradmin.CustomerCardDTO;
import com.digitalpaytech.exception.RandomKeyMappingException;
import com.digitalpaytech.mvc.customeradmin.support.CouponForm;
import com.digitalpaytech.mvc.customeradmin.support.CustomerCardForm;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.PooledResource;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObjectId;

/**
 * Lazy helper for all the account controllers.
 * 
 * @author wittawatv
 * 
 */
@Component("AccountBeanHelper")
public class AccountBeanHelper {
    public static final Character MAX_USAGE_UNLIMITED = 'U';
    public static final Character MAX_USAGE_SPECIFY = 'S';
    
    @Autowired
    private MessageHelper messageHelper;
    
    @Autowired
    private LocationService locationService;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    public void populate(final Coupon coupon, final CouponDTO dto, final RandomKeyMapping keyMapping, final boolean useRelativeDateTime,
                         final boolean formatDiscount) {
        final UserAccount user = WebSecurityUtil.getUserAccount();
        final String timeZone = WebCoreConstants.GMT;
        
        final BeanUtilsBean beanUtils = BeanUtilsBean.getInstance();
        final PooledResource<DateFormat> dateFmt = useRelativeDateTime ? null : DateUtil.takeDateFormat(WebCoreConstants.DATE_UI_FORMAT, DateUtil.GMT);
        
        try {
            dto.setRandomId(keyMapping.getRandomString(coupon, WebCoreConstants.ID_LOOK_UP_NAME));
            dto.setCustomerRandomId(keyMapping.getRandomString(user.getCustomer(), WebCoreConstants.ID_LOOK_UP_NAME));
            
            beanUtils.copyProperties(dto, coupon);
            Date startDate = coupon.getStartDateLocal();
            if ((startDate == null) || (startDate.getTime() == WebCoreConstants.NO_START_DATE.getTime())) {
                startDate = null;
            }
            
            Date endDate = coupon.getEndDateLocal();
            if ((endDate == null) || (endDate.getTime() == WebCoreConstants.NO_END_DATE.getTime())) {
                endDate = null;
            }
            
            if (dateFmt == null) {
                if (startDate != null) {
                    dto.setStartDate(DateUtil.getRelativeTimeString(startDate, timeZone, false));
                }
                if (endDate != null) {
                    dto.setEndDate(DateUtil.getRelativeTimeString(endDate, timeZone, false));
                }
            } else {
                if (startDate != null) {
                    dto.setStartDate(dateFmt.get().format(startDate));
                }
                if (endDate != null) {
                    dto.setEndDate(dateFmt.get().format(endDate));
                }
            }
            
            if (dto.getPercentDiscount() > 0) {
                if (formatDiscount) {
                    dto.setDiscount(dto.getPercentDiscount() + "%");
                } else {
                    dto.setDiscount(dto.getPercentDiscount() + "");
                }
                
                dto.setDiscountType(WebCoreConstants.DISCOUNT_TYPE_PERCENT);
            } else if (dto.getDollarDiscountAmount() > 0) {
                if (formatDiscount) {
                    dto.setDiscount("$" + WebCoreUtil.formatBase100Value(dto.getDollarDiscountAmount()));
                } else {
                    dto.setDiscount(WebCoreUtil.formatBase100Value(dto.getDollarDiscountAmount()));
                }
                
                dto.setDiscountType(WebCoreConstants.DISCOUNT_TYPE_DOLLARS);
            } else {
                dto.setDiscount("0");
                dto.setDiscountType(WebCoreConstants.DISCOUNT_TYPE_DOLLARS);
            }
            
            if (coupon.getLocation() != null) {
                dto.setLocationName(coupon.getLocation().getName());
                dto.setLocationRandomId(keyMapping.getRandomString(Location.class, coupon.getLocation().getId()));
            }
            
            if (coupon.getNumberOfUsesRemaining() != null && WebCoreConstants.COUPON_USES_UNLIMITED == coupon.getNumberOfUsesRemaining()) {
                dto.setNumUsesType(MAX_USAGE_UNLIMITED);
                dto.setNumberOfUsesRemaining(this.messageHelper.getMessage("label.coupon.maxNumOfUses.unlimited"));
            } else {
                dto.setNumUsesType(MAX_USAGE_SPECIFY);
            }
            
            dto.setPndEnabled(coupon.isIsPndEnabled());
            dto.setPbsEnabled(coupon.isIsPbsEnabled());
            dto.setOffline(coupon.isIsOffline());
            
            if (coupon.getConsumer() != null) {
                dto.setConsumerName(coupon.getConsumer().getFirstName() + " " + coupon.getConsumer().getLastName());
                dto.setConsumerRandomId(keyMapping.getRandomString(Consumer.class, coupon.getConsumer().getId()));
            } else {
                dto.setConsumerName("");
                dto.setConsumerRandomId("");
            }
        } catch (Exception e) {
            throw new RuntimeException("Conversion Failed: ", e);
        } finally {
            if (dateFmt != null) {
                dateFmt.close();
            }
        }
    }
    
    public void populate(final CouponForm theForm, final Coupon c, final RandomKeyMapping keyMapping) {
        final PooledResource<DateFormat> dateFormat = DateUtil.takeDateFormat(DateUtil.UI_FORMAT, DateUtil.GMT);
        // This is GMT on purpose so that when we send this to database, the timestamp will already adjusted with customer timezone offset.
        
        c.setId(theForm.getId());
        if (theForm.getCouponCode() != null) {
            c.setCoupon(theForm.getCouponCode().toUpperCase());
        }
        
        final UserAccount ua = WebSecurityUtil.getUserAccount();
        
        if (!ua.getCustomer().getId().equals(theForm.getCustomerId())) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("UserAccount.getCustomerId is: ").append(ua.getCustomer().getId()).append(" but customerId on the page form is: ")
                    .append(theForm.getCustomerId());
            throw new RandomKeyMappingException(bdr.toString());
        }
        c.setCustomer(ua.getCustomer());
        c.setDescription(theForm.getDescription());
        
        if (theForm.getDiscountType().equalsIgnoreCase(WebCoreConstants.DISCOUNT_TYPE_PERCENT)) {
            c.setPercentDiscount(Short.parseShort(theForm.getDiscountValue()));
            c.setDollarDiscountAmount(0);
        } else {
            c.setDollarDiscountAmount(WebCoreUtil.convertToBase100IntValue(theForm.getDiscountValue()));
            c.setPercentDiscount((short) 0);
        }
        
        try {
            if (StringUtils.isBlank(theForm.getStartDate())) {
                c.setStartDateLocal(WebCoreConstants.NO_START_DATE);
            } else {
                c.setStartDateLocal(dateFormat.get().parse(theForm.getStartDate() + " " + DateUtil.UI_DEFAULT_START_TIME));
            }
            if (StringUtils.isBlank(theForm.getEndDate())) {
                c.setEndDateLocal(WebCoreConstants.NO_END_DATE);
            } else {
                c.setEndDateLocal(dateFormat.get().parse(theForm.getEndDate() + " " + DateUtil.UI_DEFAULT_END_TIME));
            }
        } catch (ParseException pe) {
            dateFormat.close();
            throw new RuntimeException(pe);
        }
        if (theForm.getLocationId() != null) {
            c.setLocation(this.locationService.findLocationById(theForm.getLocationId()));
        } else {
            c.setLocation((Location) null);
        }
        
        if (!MAX_USAGE_SPECIFY.equals(theForm.getNumUsesType())) {
            c.setNumberOfUsesRemaining(WebCoreConstants.COUPON_USES_UNLIMITED);
        } else {
            if (StringUtils.isNotBlank(theForm.getNumUses())) {
                c.setNumberOfUsesRemaining(Short.parseShort(theForm.getNumUses()));
            } else {
                c.setNumberOfUsesRemaining((short) WebCoreConstants.COUPON_USES_MAX);
            }
        }
        
        c.setSpaceRange(theForm.getStallRange());
        if (theForm.isValidForNumOfDay()) {
            c.setValidForNumOfDay(1);
        } else {
            c.setValidForNumOfDay(0);
        }
        if (theForm.isPbsEnabled()) {
            c.setIsPbsEnabled(true);
        } else {
            c.setIsPbsEnabled(false);
        }
        if (theForm.isPndEnabled()) {
            c.setIsPndEnabled(true);
        } else {
            c.setIsPndEnabled(false);
        }
        
        dateFormat.close();
    }
    
    public void populate(final CustomerCard card, final CustomerCardDTO dto, final RandomKeyMapping keyMapping, final boolean useRelativeDateTime) {
        final String timeZone = WebSecurityUtil.getCustomerTimeZone();
        final PooledResource<DateFormat> dateFmt = useRelativeDateTime ? null : DateUtil.takeDateFormat(WebCoreConstants.DATE_UI_FORMAT, timeZone);
        
        dto.setRandomId(keyMapping.getRandomString(card, WebCoreConstants.ID_LOOK_UP_NAME));
        dto.setCardNumber(card.getCardNumber());
        if (card.getCustomerCardType() != null) {
            dto.setCardTypeRandomId(keyMapping.getRandomString(CustomerCardType.class, card.getCustomerCardType().getId()));
            dto.setCardType(card.getCustomerCardType().getName());
        }
        
        dto.setComment(card.getComment());
        
        dto.setRestricted(card.isIsForNonOverlappingUse());
        if (dto.getRestricted()) {
            if (card.getGracePeriodMinutes() == null) {
                dto.setGracePeriodMinutes((short) 0);
            } else {
                dto.setGracePeriodMinutes(card.getGracePeriodMinutes());
            }
        }
        
        if (card.getMaxNumberOfUses() != null) {
            dto.setMaxNumOfUses(card.getMaxNumberOfUses().toString());
            if (card.getNumberOfUses() != null) {
                dto.setRemainingNumOfUses(Integer.toString(card.getMaxNumberOfUses() - card.getNumberOfUses()));
            } else {
                dto.setRemainingNumOfUses(Integer.toString(card.getMaxNumberOfUses()));
            }
        } else {
            dto.setMaxNumOfUses("");
        }
        
        if ((card.getCardBeginGmt() != null) && (card.getCardBeginGmt().getTime() != WebCoreConstants.NO_START_DATE.getTime())) {
            if (dateFmt == null) {
                dto.setStartValidDate(DateUtil.getRelativeTimeString(card.getCardBeginGmt(), timeZone));
            } else {
                dto.setStartValidDate(dateFmt.get().format(card.getCardBeginGmt()));
            }
        }
        
        if ((card.getCardExpireGmt() != null) && (card.getCardExpireGmt().getTime() != WebCoreConstants.NO_END_DATE.getTime())) {
            if (dateFmt == null) {
                dto.setExpireDate(DateUtil.getRelativeTimeString(card.getCardExpireGmt(), timeZone));
            } else {
                dto.setExpireDate(dateFmt.get().format(card.getCardExpireGmt()));
            }
        }
        
        if (card.getPointOfSale() != null) {
            dto.setLocationRandomId(keyMapping.getRandomString(PointOfSale.class, card.getPointOfSale().getId()));
            dto.setLocation(card.getPointOfSale().getName());
        } else if (card.getLocation() != null) {
            dto.setLocationRandomId(keyMapping.getRandomString(Location.class, card.getLocation().getId()));
            dto.setLocation(card.getLocation().getName());
        } else {
            dto.setLocationRandomId("");
            dto.setLocation("");
        }
        
        if (card.getConsumer() != null) {
            dto.setConsumerName(card.getConsumer().getFirstName() + " " + card.getConsumer().getLastName());
            dto.setConsumerRandomId(keyMapping.getRandomString(Consumer.class, card.getConsumer().getId()));
        } else {
            dto.setConsumerName("");
            dto.setConsumerRandomId("");
        }
        
        if (dateFmt != null) {
            dateFmt.close();
        }
    }
    
    public void populate(final CustomerCardForm form, final CustomerCard po, final RandomKeyMapping keyMapping) {
        final String timeZone = WebSecurityUtil.getCustomerTimeZone();
        final String datePattern = WebCoreConstants.DATE_UI_FORMAT + " HH:mm:ss";
        final PooledResource<DateFormat> dateFmt = DateUtil.takeDateFormat(datePattern, timeZone);
        
        final UserAccount user = WebSecurityUtil.getUserAccount();
        
        po.setComment(form.getComment());
        
        po.setIsForNonOverlappingUse((form.getIsRestricted() == null) ? false : form.getIsRestricted().booleanValue());
        if ((form.getMaxNumOfUses() != null) && (form.getMaxNumOfUses().trim().length() > 0)) {
            final short maxNumberOfUses = Short.valueOf(form.getMaxNumOfUses().trim());
            if (po.getMaxNumberOfUses() != null && po.getMaxNumberOfUses().shortValue() != maxNumberOfUses) {
                po.setNumberOfUses((Short) null);
            }
            po.setMaxNumberOfUses(maxNumberOfUses);
        } else {
            po.setMaxNumberOfUses((Short) null);
            po.setNumberOfUses((Short) null);
        }
        
        try {
            if (form.getStartValidDate() != null) {
                po.setCardBeginGmt(dateFmt.get().parse(form.getStartValidDate() + " 00:00:00"));
            } else {
                po.setCardBeginGmt(WebCoreConstants.NO_START_DATE);
            }
            if (form.getExpireDate() != null) {
                po.setCardExpireGmt(dateFmt.get().parse(form.getExpireDate() + " 23:59:59"));
            } else {
                po.setCardExpireGmt(WebCoreConstants.NO_END_DATE);
            }
        } catch (ParseException pe) {
            throw new RuntimeException("Invalid Date input", pe);
        }
        
        if (form.getGracePeriod() != null) {
            po.setGracePeriodMinutes(new Short(form.getGracePeriod().trim()));
        } else {
            po.setGracePeriodMinutes((short) 0);
        }
        
        po.setLocation((Location) null);
        po.setPointOfSale((PointOfSale) null);
        if ((form.getLocationRandomId() != null) && (form.getLocationRandomId().length() > 0) && (!"0".equals(form.getLocationRandomId()))) {
            final WebObjectId locPosId = keyMapping.getKeyObject(form.getLocationRandomId());
            if (locPosId == null) {
                throw new IllegalStateException("Invalid Random Location ID");
            } else {
                if (PointOfSale.class.equals(locPosId.getObjectType())) {
                    final PointOfSale pos = new PointOfSale();
                    pos.setId((Integer) locPosId.getId());
                    
                    po.setPointOfSale(pos);
                } else if (Location.class.equals(locPosId.getObjectType())) {
                    final Location loc = new Location();
                    loc.setId((Integer) locPosId.getId());
                    
                    po.setLocation(loc);
                } else {
                    throw new IllegalStateException("Invalid Random Location ID");
                }
            }
        }
        
        po.setLastModifiedByUserId(user.getId());
        po.setLastModifiedGmt(new Date());
        
        dateFmt.close();
    }
}
