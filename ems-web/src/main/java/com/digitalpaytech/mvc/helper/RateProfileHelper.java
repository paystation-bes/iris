package com.digitalpaytech.mvc.helper;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.RateProfile;
import com.digitalpaytech.domain.RateProfileLocation;
import com.digitalpaytech.domain.RateRateProfile;
import com.digitalpaytech.mvc.support.RateProfileLocationForm;
import com.digitalpaytech.mvc.support.RateRateProfileEditForm;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.RateProfileService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.PooledResource;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;

@Component("rateProfileHelper")
public class RateProfileHelper {
    public static final String SESSION_KEY_PTTRN_RATE_LOCATION = "rateProfileLocation_{0}";
    public static final String SESSION_KEY_PTTRN_LOCATION_RATE = "locationRateProfile_{0}";
    
    @Autowired
    private LocationService locationService;
    
    @Autowired
    private RateProfileService rateProfileService;
    
    public final void setLocationService(final LocationService locationService) {
        this.locationService = locationService;
    }
    
    public final void setRateProfileService(final RateProfileService rateProfileService) {
        this.rateProfileService = rateProfileService;
    }
    
    public final void clearRateProfileSessionData(final HttpServletRequest request, final String rateProfileRandomId) {
        SessionTool.getInstance(request).removeAttribute(MessageFormat.format(SESSION_KEY_PTTRN_RATE_LOCATION, rateProfileRandomId));
    }
    
    public final void clearLocationSessionData(final HttpServletRequest request, final String locationRandomId) {
        SessionTool.getInstance(request).removeAttribute(MessageFormat.format(SESSION_KEY_PTTRN_LOCATION_RATE, locationRandomId));
    }
    
    public final RateProfileLocationForm populate(final RateProfileLocation domain, final RateProfileLocationForm form,
        final RandomKeyMapping keyMapping, final DateFormat dateFormat) {
        form.setRandomId(keyMapping.getRandomString(RateProfileLocation.class, domain.getId()));
        form.setRateProfileRandomId(keyMapping.getRandomString(RateProfile.class, domain.getRateProfile().getId()));
        form.setLocationRandomId((domain.getLocation() == null) ? "0" : keyMapping.getRandomString(Location.class, domain.getLocation().getId()));
        
        form.setSpaceRange(domain.getSpaceRange());
        
        form.setStartDateObj(domain.getStartGmt());
        form.setStartDate(dateFormat.format(domain.getStartGmt()));
        
        form.setEndDateObj(domain.getEndGmt());
        if (domain.getEndGmt() != null) {
            form.setEndDate(dateFormat.format(domain.getEndGmt()));
        }
        
        form.setRateProfileName(domain.getRateProfile().getName());
        
        return form;
    }
    
    public final RateProfileLocation populate(final RateProfileLocationForm form, final RateProfileLocation domain,
        final RandomKeyMapping keyMapping, final Customer customer) {
        final Integer locId = keyMapping.getKey(form.getLocationRandomId(), Location.class, Integer.class);
        if (locId != null) {
            domain.setLocation(this.locationService.findLocationById(locId));
            if (domain.getLocation() == null) {
                throw new IllegalStateException("Empty Location !");
            }
        }
        
        populate(form, domain, keyMapping, customer, domain.getLocation());
        
        return domain;
    }
    
    public final RateProfileLocation populate(final RateProfileLocationForm form, final RateProfileLocation domain,
        final RandomKeyMapping keyMapping, final Customer customer, final Location location) {
        domain.setId(keyMapping.getKey(form.getRandomId(), RateProfileLocation.class, Integer.class));
        if (domain.getId() != null && domain.getId() < 0) {
            domain.setId((Integer) null);
        }
        
        domain.setRateProfile(this.rateProfileService.findRateProfileById(keyMapping.getKey(form.getRateProfileRandomId(), RateProfile.class,
                                                                                            Integer.class)));
        domain.setLocation(location);
        domain.setCustomer(customer);
        
        domain.setStartGmt(form.getStartDateObj());
        domain.setEndGmt(form.getEndDateObj());
        domain.setSpaceRange(form.getSpaceRange());
        domain.setIsDeleted(form.isIsDeleted());
        
        return domain;
    }
    
    public final List<RateProfileLocationForm> aggregateRateProfileLocationList(final List<RateProfileLocation> dbList,
        final Map<String, RateProfileLocationForm> sessionList, final RandomKeyMapping keyMapping) {
        final PooledResource<DateFormat> dateFormat = DateUtil.takeDateFormat(DateUtil.DATE_UI_FORMAT, "GMT");
        final Map<String, RateProfileLocationForm> sessionData = (sessionList == null) ? new HashMap<String, RateProfileLocationForm>(2)
                : new HashMap<String, RateProfileLocationForm>(sessionList);
        
        final List<RateProfileLocationForm> formList = new ArrayList<RateProfileLocationForm>(dbList.size());
        
        RateProfileLocationForm form = null;
        for (RateProfileLocation domain : dbList) {
            final String randomId = keyMapping.getRandomString(RateProfileLocation.class, domain.getId());
            form = sessionData.remove(randomId);
            if (form == null) {
                form = populate(domain, new RateProfileLocationForm(), keyMapping, dateFormat.get());
            }
            
            if (!form.isIsDeleted()) {
                formList.add(form);
            }
        }
        
        for (Map.Entry<String, RateProfileLocationForm> formEntry : sessionData.entrySet()) {
            form = formEntry.getValue();
            if (!form.isIsDeleted()) {
                formList.add(form);
            }
        }
        
        dateFormat.close();
        
        return formList;
    }
    
    @SuppressWarnings("unchecked")
    public final Map<String, RateProfileLocationForm> getRateProfileSessionData(final HttpServletRequest request, final String rateProfileRandomId) {
        return (Map<String, RateProfileLocationForm>) SessionTool.getInstance(request).getAttribute(MessageFormat
                                                                                                            .format(SESSION_KEY_PTTRN_RATE_LOCATION,
                                                                                                                    rateProfileRandomId));
    }
    
    public final Map<String, RateProfileLocationForm> createRateProfileSessionDataIfAbsent(final HttpServletRequest request,
        final String rateProfileRandomId) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        final String key = MessageFormat.format(SESSION_KEY_PTTRN_RATE_LOCATION, rateProfileRandomId);
        @SuppressWarnings("unchecked")
        Map<String, RateProfileLocationForm> sessionData = (Map<String, RateProfileLocationForm>) sessionTool.getAttribute(key);
        if (sessionData == null) {
            sessionData = new HashMap<String, RateProfileLocationForm>();
            sessionTool.setAttribute(key, sessionData);
        }
        
        return sessionData;
    }
    
    @SuppressWarnings("unchecked")
    public final Map<String, RateProfileLocationForm> getLocationSessionData(final HttpServletRequest request, final String locationRandomId) {
        return (Map<String, RateProfileLocationForm>) SessionTool.getInstance(request).getAttribute(MessageFormat
                                                                                                            .format(SESSION_KEY_PTTRN_LOCATION_RATE,
                                                                                                                    locationRandomId));
    }
    
    public final Map<String, RateProfileLocationForm> createLocationSessionDataIfAbsent(final HttpServletRequest request,
        final String locationRandomId) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        final String key = MessageFormat.format(SESSION_KEY_PTTRN_LOCATION_RATE, locationRandomId);
        @SuppressWarnings("unchecked")
        Map<String, RateProfileLocationForm> sessionData = (Map<String, RateProfileLocationForm>) sessionTool.getAttribute(key);
        if (sessionData == null) {
            sessionData = new HashMap<String, RateProfileLocationForm>();
            sessionTool.setAttribute(key, sessionData);
        }
        
        return sessionData;
    }
    
    public final List<RateRateProfileEditForm> populateRateRateProfileEditFormList(final List<RateRateProfile> rateRateProfileList,
        final Map<String, RateRateProfileEditForm> ratesMap, final String startDateStr, final String endDateStr, final RandomKeyMapping keyMapping) {
        final List<RateRateProfileEditForm> rateRateProfileEditFormList = new ArrayList<RateRateProfileEditForm>();
        for (RateRateProfile rateRateProfile : rateRateProfileList) {
            if (!rateRateProfile.isIsDeleted()) {
                RateRateProfileEditForm rrpf = ratesMap.remove(keyMapping.getRandomString(RateRateProfile.class, rateRateProfile.getId()));
                if (rrpf == null) {
                    rrpf = populateRateRateProfileEditForm(rateRateProfile, keyMapping);
                }
                
                if (!rrpf.getIsToBeDeleted()) {
                    rateRateProfileEditFormList.add(rrpf);
                }
            }
        }
        
        for (Map.Entry<String, RateRateProfileEditForm> formEntry : ratesMap.entrySet()) {
            final RateRateProfileEditForm rrpf = formEntry.getValue();
            if (!rrpf.getIsToBeDeleted()
                && (!rrpf.getUseScheduled() || startDateStr.compareTo(rrpf.getAvailabilityStartDate()) <= 0
                                               && endDateStr.compareTo(rrpf.getAvailabilityStartDate()) >= 0)) {
                rateRateProfileEditFormList.add(rrpf);
            }
        }
        
        return rateRateProfileEditFormList;
    }
    
    public final RateRateProfileEditForm populateRateRateProfileEditForm(final RateRateProfile rateRateProfile, final RandomKeyMapping keyMapping) {
        final RateRateProfileEditForm rateRateProfileEditForm = new RateRateProfileEditForm();
        rateRateProfileEditForm.setRateRateProfileRandomId(keyMapping.getRandomString(rateRateProfile, WebCoreConstants.ID_LOOK_UP_NAME));
        rateRateProfileEditForm.setRateRandomId(keyMapping.getRandomString(rateRateProfile.getRate(), WebCoreConstants.ID_LOOK_UP_NAME));
        rateRateProfileEditForm
                .setRateProfileRandomId(keyMapping.getRandomString(rateRateProfile.getRateProfile(), WebCoreConstants.ID_LOOK_UP_NAME));
        rateRateProfileEditForm.setRateName(rateRateProfile.getRate().getName());
        rateRateProfileEditForm.setRateTypeName(rateRateProfile.getRate().getRateType().getName());
        rateRateProfileEditForm.setAvailabilityStartDate(rateRateProfile.getAvailabilityStartDate() == null ? null : DateUtil
                .createDateOnlyString(rateRateProfile.getAvailabilityStartDate(), true));
        rateRateProfileEditForm.setAvailabilityEndDate(rateRateProfile.getAvailabilityEndDate() == null ? null : DateUtil
                .createDateOnlyString(rateRateProfile.getAvailabilityEndDate(), true));
        rateRateProfileEditForm.setAvailabilityStartTime(rateRateProfile.getAvailabilityStartTime());
        rateRateProfileEditForm.setAvailabilityEndTime(rateRateProfile.getAvailabilityEndTime());
        
        rateRateProfileEditForm.setUseScheduled(rateRateProfile.isIsScheduled());
        rateRateProfileEditForm.setUseMonday(rateRateProfile.getIsMonday());
        rateRateProfileEditForm.setUseTuesday(rateRateProfile.getIsTuesday());
        rateRateProfileEditForm.setUseWednesday(rateRateProfile.getIsWednesday());
        rateRateProfileEditForm.setUseThursday(rateRateProfile.getIsThursday());
        rateRateProfileEditForm.setUseFriday(rateRateProfile.getIsFriday());
        rateRateProfileEditForm.setUseSaturday(rateRateProfile.getIsSaturday());
        rateRateProfileEditForm.setUseSunday(rateRateProfile.getIsSunday());
        
        return rateRateProfileEditForm;
    }
    
    public final RateRateProfileEditForm populateRateRateProfileEditFormComplete(final RateRateProfile rateRateProfile,
        final RandomKeyMapping keyMapping) {
        final RateRateProfileEditForm rateRateProfileEditForm = populateRateRateProfileEditForm(rateRateProfile, keyMapping);
        rateRateProfileEditForm.setDescription(rateRateProfile.getDescription());
        rateRateProfileEditForm.setNumberOfDays(rateRateProfile.getDurationNumberOfDays() == null ? null : rateRateProfile.getDurationNumberOfDays()
                .intValue());
        rateRateProfileEditForm.setNumberOfHours(rateRateProfile.getDurationNumberOfHours() == null ? null : rateRateProfile
                .getDurationNumberOfHours().intValue());
        rateRateProfileEditForm.setNumberOfMins(rateRateProfile.getDurationNumberOfMins() == null ? null : rateRateProfile.getDurationNumberOfMins()
                .intValue());
        rateRateProfileEditForm.setExpiryTime(rateRateProfile.getExpiryTime());
        if (rateRateProfile.getRateExpiryDayType() != null) {
            rateRateProfileEditForm.setExpiryDayType((int) rateRateProfile.getRateExpiryDayType().getId());
        }
        
        return rateRateProfileEditForm;
    }
}
