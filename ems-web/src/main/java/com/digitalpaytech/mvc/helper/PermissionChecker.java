package com.digitalpaytech.mvc.helper;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Map;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.scripting.operand.Operand;

public class PermissionChecker extends Operand<Boolean> {
	private static final long serialVersionUID = -1744329658647284441L;

	private static final String PRINT_PATTERN = "(user.permission == {0})";
	
	private int expectedPermission;
	
	public PermissionChecker(int expectedPermission) {
		this.expectedPermission = expectedPermission;
	}
	
	@Override
	public Boolean execute(Map<String, Object> context) {
		boolean result = false;
		
		WebUser user = (WebUser) WebSecurityUtil.getAuthentication().getPrincipal();
		Collection<Integer> permissions = user.getUserPermissions();
		if(permissions != null) {
			result = permissions.contains(this.expectedPermission);
		}
		
		return result;
	}

	@Override
	public String toString() {
		return MessageFormat.format(PRINT_PATTERN, this.expectedPermission);
	}
}
