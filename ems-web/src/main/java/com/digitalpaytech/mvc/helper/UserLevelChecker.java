package com.digitalpaytech.mvc.helper;

import java.text.MessageFormat;
import java.util.Map;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.scripting.operand.Operand;

public class UserLevelChecker extends Operand<Boolean> {
	private static final long serialVersionUID = -6024304770788459797L;

	private static final String PRINT_PATTERN = "user.level = {0}";
	
	private boolean expectedSystemAdmin;
	
	public UserLevelChecker(boolean expectedSystemAdmin) {
		this.expectedSystemAdmin = expectedSystemAdmin;
	}
	
	@Override
	public Boolean execute(Map<String, Object> context) {
		WebUser user = (WebUser) WebSecurityUtil.getAuthentication().getPrincipal();
		return this.expectedSystemAdmin == user.isSystemAdmin();
	}
	
	@Override
	public String toString() {
		return MessageFormat.format(PRINT_PATTERN, (this.expectedSystemAdmin) ? "SystemAdmin" : "CustomerAdmin");
	}
}
