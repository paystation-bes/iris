package com.digitalpaytech.mvc.helper;

import java.text.MessageFormat;
import java.util.Map;

import com.digitalpaytech.dto.customeradmin.CustomerDetails;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.scripting.operand.Operand;

public class SubscriptionChecker extends Operand<Boolean> {
	private static final long serialVersionUID = -7897403970424209026L;
	
	public static final String CTXKEY_CUSTOMER_DETAILS = "custDtls";
	
	private static final String PRINT_PATTERN = "(user.subscription == {0})";
	
	private int expectedSubscription;
	
	public SubscriptionChecker(int expectedSubscription) {
		this.expectedSubscription = expectedSubscription;
	}

	@Override
	public Boolean execute(Map<String, Object> context) {
		boolean result = false;
		
		CustomerDetails custDtl = null;
		if(context != null) {
			custDtl = (CustomerDetails) context.get(CTXKEY_CUSTOMER_DETAILS);
		}
		
		if(custDtl == null) {
			result = WebSecurityUtil.hasValidCustomerSubscription(this.expectedSubscription);
		}
		else {
			result = custDtl.hasSubscription(this.expectedSubscription);
		}
		
		return result;
	}

	@Override
	public String toString() {
		return MessageFormat.format(PRINT_PATTERN, this.expectedSubscription);
	}
}
