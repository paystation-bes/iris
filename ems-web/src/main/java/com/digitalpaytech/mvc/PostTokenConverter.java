package com.digitalpaytech.mvc;

import com.digitalpaytech.util.PostToken;
import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;

public class PostTokenConverter extends AbstractSingleValueConverter {
	@Override
	public boolean canConvert(@SuppressWarnings("rawtypes") Class type) {
		return PostToken.class.isAssignableFrom(type);
	}

	@Override
	public Object fromString(String str) {
		return new PostToken(str);
	}

}