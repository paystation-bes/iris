package com.digitalpaytech.mvc;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.digitalpaytech.domain.CaseLocationLot;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.dto.CaseLocationLotSearchCriteria;
import com.digitalpaytech.dto.CaseLocationLotSelectorDTO;
import com.digitalpaytech.dto.PaginatedList;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.LocationIntegrationSearchForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.CaseService;
import com.digitalpaytech.service.CustomerSubscriptionService;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;

@Controller
public class CaseLocationLotListController {
    
    protected static final String FORM_SEARCH = "caseLocationLotSearchForm";
    
    private static final Logger LOGGER = Logger.getLogger(CaseLocationLotListController.class);
    
    @Autowired
    protected CaseService caseService;
    
    @Autowired
    protected CustomerSubscriptionService customerSubscriptionService;
    
    @Autowired
    protected CommonControllerHelper commonControllerHelper;
    
    public final CaseService getCaseService() {
        return this.caseService;
    }
    
    public final CustomerSubscriptionService getCustomerSubscriptionService() {
        return this.customerSubscriptionService;
    }
    
    public final CommonControllerHelper getCommonControllerHelper() {
        return this.commonControllerHelper;
    }
    
    public final void setCaseService(final CaseService caseService) {
        this.caseService = caseService;
    }
    
    public final void setCustomerSubscriptionService(final CustomerSubscriptionService customerSubscriptionService) {
        this.customerSubscriptionService = customerSubscriptionService;
    }
    
    public final void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }
    
    protected final CaseLocationLotSearchCriteria populateCaseLocationLotSearchCriteria(final HttpServletRequest request,
        final LocationIntegrationSearchForm form, final Integer customerId, final boolean isSystemAdmin, final RandomKeyMapping keyMapping)
        throws ApplicationException {
        
        final CaseLocationLotSearchCriteria criteria = new CaseLocationLotSearchCriteria();
        criteria.setCustomerId(customerId);
        criteria.setLocationId(keyMapping.getKey(form.getSelectedId(), Location.class, Integer.class));
        criteria.setCaseLocationLotId(keyMapping.getKey(form.getItemId(), CaseLocationLot.class, Integer.class));
        
        if (form.getPage() != null) {
            criteria.setPage(form.getPage());
        }
        
        if (StringUtils.isNotBlank(form.getLocationRandomId()) && criteria.getLocationId() == null) {
            throw new ApplicationException("Invalid Location Random ID for Customer: " + customerId);
        }
        if (StringUtils.isNotBlank(form.getItemId()) && criteria.getCaseLocationLotId() == null) {
            throw new ApplicationException("Invalid CaseLocationLot Random ID for Customer: " + customerId);
        }
        
        criteria.setItemsPerPage(WebCoreConstants.CASE_LOCATION_LOTS_PER_PAGE);
        
        if (form.getDataKey() != null && form.getDataKey().length() > 0) {
            try {
                criteria.setMaxUpdatedTime(new Date(Long.parseLong(form.getDataKey())));
            } catch (NumberFormatException nfe) {
                criteria.setMaxUpdatedTime(new Date());
            }
        }
        
        if (criteria.getMaxUpdatedTime() == null) {
            criteria.setMaxUpdatedTime(new Date());
        }
        
        return criteria;
    }
    
    protected final String caseLocationLotSelectorsSuper(final WebSecurityForm<LocationIntegrationSearchForm> form, final HttpServletRequest request,
        final HttpServletResponse response) {
        
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        
        if (!this.customerSubscriptionService.hasOneOfSubscriptions(customerId, WebCoreConstants.SUBSCRIPTION_TYPE_CASE_INTEGRATION)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        try {
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            final CaseLocationLotSearchCriteria criteria = populateCaseLocationLotSearchCriteria(request, form.getWrappedObject(), customerId, false,
                                                                                                 keyMapping);
            
            final PaginatedList<CaseLocationLotSelectorDTO> result = new PaginatedList<CaseLocationLotSelectorDTO>();
            result.setElements(this.caseService.findCaseLocationLotSelector(criteria, keyMapping));
            result.setDataKey(Long.toString(criteria.getMaxUpdatedTime().getTime(), WebCoreConstants.PAGED_LIST_MAXUPDATETIME_LONG_LENGTH));
            
            return WidgetMetricsHelper.convertToJson(result, "caseLocationLotList", true);
            
        } catch (ApplicationException ae) {
            
            LOGGER.error(ae.getMessage());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
    }
    
    @ModelAttribute(FORM_SEARCH)
    public final WebSecurityForm<LocationIntegrationSearchForm> initPosSearchForm(final HttpServletRequest request) {
        return new WebSecurityForm<LocationIntegrationSearchForm>(null, new LocationIntegrationSearchForm());
    }
}
