package com.digitalpaytech.mvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Rate;
import com.digitalpaytech.domain.RateAcceptedPayment;
import com.digitalpaytech.domain.RateAddTime;
import com.digitalpaytech.domain.RateDaily;
import com.digitalpaytech.domain.RateFlat;
import com.digitalpaytech.domain.RateHourly;
import com.digitalpaytech.domain.RateHourlyRule;
import com.digitalpaytech.domain.RateIncremental;
import com.digitalpaytech.domain.RateMonthly;
import com.digitalpaytech.domain.RateRateProfile;
import com.digitalpaytech.domain.RateRestriction;
import com.digitalpaytech.domain.RateType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.PaginatedList;
import com.digitalpaytech.dto.SearchCustomerResult;
import com.digitalpaytech.dto.SearchCustomerResultSetting;
import com.digitalpaytech.dto.customeradmin.RateListInfo;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.RateEditForm;
import com.digitalpaytech.mvc.support.RateSearchForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.RateRateProfileService;
import com.digitalpaytech.service.RateService;
import com.digitalpaytech.service.RateTypeService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.RateValidator;

/**
 * This class handles the Alert setting request.
 * 
 * @author Brian Kim
 */
@Controller
public class RateController implements MessageSourceAware {
    
    protected static final String FORM_SEARCH = "rateSearchForm";
    protected static final String LIST_NAME = "rateListInfo";
    protected static final String PARAMETER_OBJECT_ID = "rateID";
    protected static final String PARAMETER_SEARCH = "search";
    private static final byte MINUTES_60 = 60;
    
    private static Logger logger = Logger.getLogger(RateController.class);
    
    @Autowired
    protected CustomerService customerService;
    
    @Autowired
    protected RateService rateService;
    
    @Autowired
    protected RateRateProfileService rateRateProfileService;
    
    @Autowired
    protected RateTypeService rateTypeService;
    
    @Autowired
    protected RateValidator rateValidator;
    
    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final void setRateTypeService(final RateTypeService rateTypeService) {
        this.rateTypeService = rateTypeService;
    }
    
    public final void setRateService(final RateService rateService) {
        this.rateService = rateService;
    }
    
    public final void setRateRateProfileService(final RateRateProfileService rateRateProfileService) {
        this.rateRateProfileService = rateRateProfileService;
    }
    
    public final void setRateValidator(final RateValidator rateValidator) {
        this.rateValidator = rateValidator;
    }
    
    @Override
    public final void setMessageSource(final MessageSource messageSource) {
        this.messageSource = messageSource;
    }
    
    public final void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }
    
    protected final String view(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model, final String urlOnSucceed) {
        
        final List<RateType> rateTypeList = this.rateTypeService.findAllRateTypesWithoutHoliday();
        
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        
        final WebSecurityForm<RateEditForm> rateEditForm = new WebSecurityForm<RateEditForm>(null, new RateEditForm());
        
        model.put("rateEditForm", rateEditForm);
        model.put("rateTypeList", rateTypeList);
        
        return urlOnSucceed;
    }
    
    protected final String search(final HttpServletRequest request, final HttpServletResponse response) {
        
        final String search = request.getParameter(PARAMETER_SEARCH);
        
        final SearchCustomerResult searchCustomerResult = new SearchCustomerResult();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        
        if (customerId == null) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        if (!StringUtils.isBlank(search)) {
            if (!search.matches(WebCoreConstants.REGEX_PRINTABLE_TEXT)) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            /* retrieve rate by name. */
            final List<Rate> rateList = this.rateService.searchRates(customerId, search);
            for (Rate rate : rateList) {
                searchCustomerResult.addPosNameSearchResult(new SearchCustomerResultSetting(keyMapping.getRandomString(Rate.class, rate.getId()), rate
                        .getName(), rate.getRateType().getName()));
            }
            
        }
        
        return WidgetMetricsHelper.convertToJson(searchCustomerResult, "searchRateResult", true);
    }
    
    protected final String getList(final WebSecurityForm<RateSearchForm> form, final HttpServletRequest request, final HttpServletResponse response) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        
        Date maxUpdatedTime = null;
        if (form.getWrappedObject().getPage() == 1) {
            maxUpdatedTime = DateUtil.getCurrentGmtDate();
        }
        if ((form.getWrappedObject().getDataKey() != null) && (form.getWrappedObject().getDataKey().length() > 0)) {
            try {
                maxUpdatedTime = new Date(Long.parseLong(form.getWrappedObject().getDataKey()));
            } catch (NumberFormatException nfe) {
                logger.info("NumberFormatException in RateController.getList(), currentTime is used for maxUpdateTime");
            }
        }
        
        final List<Rate> rateList = this.rateService.findPagedRateList(customerId, form.getWrappedObject().getRateTypeId(), maxUpdatedTime, form
                .getWrappedObject().getPage());
        
        final PaginatedList<RateListInfo> result = new PaginatedList<RateListInfo>();
        result.setElements(populateRateListInfo(rateList, keyMapping));
        result.setDataKey(Long.toString(maxUpdatedTime.getTime(), WebCoreConstants.PAGED_LIST_MAXUPDATETIME_LONG_LENGTH));
        
        return WidgetMetricsHelper.convertToJson(result, LIST_NAME, true);
    }
    
    protected final String locatePageWith(final WebSecurityForm<RateSearchForm> form, final HttpServletRequest request, final HttpServletResponse response) {
        
        String resultJson = WebCoreConstants.RESPONSE_FALSE;
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        int targetPage = -1;
        final Integer targetId = keyMapping.getKey(form.getWrappedObject().getTargetRandomId(), Rate.class, Integer.class);
        final Date maxUpdateTime = new Date();
        if (targetId != null) {
            targetPage = this.rateService.findRatePage(targetId, customerId, form.getWrappedObject().getRateTypeId(), maxUpdateTime);
        }
        
        if (targetPage > 0) {
            
            final List<Rate> rateList = this.rateService.findPagedRateList(customerId, form.getWrappedObject().getRateTypeId(), maxUpdateTime, form
                    .getWrappedObject().getPage());
            
            final PaginatedList<RateListInfo> result = new PaginatedList<RateListInfo>();
            result.setElements(populateRateListInfo(rateList, keyMapping));
            result.setDataKey(Long.toString(maxUpdateTime.getTime(), WebCoreConstants.PAGED_LIST_MAXUPDATETIME_LONG_LENGTH));
            
            resultJson = WidgetMetricsHelper.convertToJson(result, LIST_NAME, true);
        }
        
        return resultJson;
    }
    
    protected final String save(final WebSecurityForm<RateEditForm> webSecurityForm, final BindingResult result, final HttpServletRequest request,
                                final HttpServletResponse response, final ModelMap model) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        
        this.rateValidator.validate(webSecurityForm, result, customerId, keyMapping);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        webSecurityForm.resetToken();
        
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_CREATE) {
            /* Add Alert. */
            return saveRate(request, response, webSecurityForm, new Rate());
        } else if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
            /* Edit Alert. */
            final Rate rate = this.rateService.findRateById(webSecurityForm.getWrappedObject().getRateId());
            return saveRate(request, response, webSecurityForm, rate);
        } else {
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
    }
    
    protected final String verifyDelete(final HttpServletRequest request, final HttpServletResponse response) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strRateId = request.getParameter(PARAMETER_OBJECT_ID);
        final Integer rateId = WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, strRateId, new String[] {
                "Invalid randomised rateID in RateController.verifyDelete().", "+++ Invalid randomised rateID in RateController.verifyDelete(). +++", });
        if (rateId == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Rate rate = this.rateService.findRateById(rateId);
        if (rate == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        final List<RateRateProfile> rateRateProfileList = this.rateRateProfileService.findPublishedRatesByRateId(rateId);
        
        if (rateRateProfileList != null && !rateRateProfileList.isEmpty()) {
            MessageInfo messages = new MessageInfo();
            String errorResponse = this.rateValidator.getMessage("error.settings.rates.rateAssignedToActiveProfiles");
            messages.setError(true);
            messages.addMessage(errorResponse);
            
            return WidgetMetricsHelper.convertToJson(messages, "messages", true);
        }
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    protected final String delete(final HttpServletRequest request, final HttpServletResponse response) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final String strRateId = request.getParameter(PARAMETER_OBJECT_ID);
        final Integer rateId = WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, strRateId, new String[] {
                "Invalid randomised rateID in RateController.delete().", "+++ Invalid randomised rateID in RateController.delete(). +++", });
        if (rateId == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Rate rate = this.rateService.findRateById(rateId);
        if (rate == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        this.rateService.deleteRate(rate, userAccount.getId());
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    protected final String getInfo(final HttpServletRequest request, final HttpServletResponse response) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strRateId = request.getParameter(PARAMETER_OBJECT_ID);
        final Integer rateId = WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, strRateId, new String[] {
                "Invalid randomised rateID in RateController.getInfo(). +++", "+++ Invalid randomised rateID in RateController.getInfo().", });
        if (rateId == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Rate rate = this.rateService.findRateById(rateId);
        if (rate == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return WidgetMetricsHelper.convertToJson(populateRateInfo(rate, strRateId, keyMapping), "rateInfo", true);
    }
    
    private RateEditForm populateRateInfo(final Rate rate, final String randomId, final RandomKeyMapping keyMapping) {
        final RateEditForm rateInfo = new RateEditForm();
        rateInfo.setRandomId(randomId);
        rateInfo.setName(rate.getName().trim());
        rateInfo.setRateType((int) rate.getRateType().getId());
        
        if (rate.getRateType().getId() != WebCoreConstants.RATE_TYPE_PARKING_RESTRICTION) {
            populateAcceptedPayment(rateInfo, rate.getRateAcceptedPayment());
        }
        
        if (rate.getRateAddTime() != null) {
            populateAddTime(rateInfo, rate.getRateAddTime());
        }
        
        switch (rate.getRateType().getId()) {
            case WebCoreConstants.RATE_TYPE_FLAT:
                populateFlat(rateInfo, rate.getRateFlat());
                break;
            case WebCoreConstants.RATE_TYPE_DAILY:
                populateDaily(rateInfo, rate.getRateDaily());
                break;
            case WebCoreConstants.RATE_TYPE_HOURLY:
                populateHourly(rateInfo, rate.getRateHourly());
                break;
            case WebCoreConstants.RATE_TYPE_INCREMENTAL:
                populateIncremental(rateInfo, rate.getRateIncremental());
                break;
            case WebCoreConstants.RATE_TYPE_MONTHLY:
                populateMonthly(rateInfo, rate.getRateMonthly());
                break;
            case WebCoreConstants.RATE_TYPE_PARKING_RESTRICTION:
                populateRestriction(rateInfo, rate.getRateRestriction());
                break;
            default:
                break;
        }
        return rateInfo;
    }
    
    private void populateAcceptedPayment(final RateEditForm rateInfo, final RateAcceptedPayment rateAcceptedPayment) {
        rateInfo.setPaymentBill(rateAcceptedPayment.isIsBill());
        rateInfo.setPaymentCoin(rateAcceptedPayment.isIsCoin());
        rateInfo.setPaymentCC(rateAcceptedPayment.isIsCreditCard());
        rateInfo.setPaymentRF(rateAcceptedPayment.isIsContactless());
        rateInfo.setPaymentSC(rateAcceptedPayment.isIsSmartCard());
        rateInfo.setPaymentPC(rateAcceptedPayment.isIsPasscard());
        rateInfo.setPaymentCoupon(rateAcceptedPayment.isIsCoupon());
        rateInfo.setPromptForCoupon(rateAcceptedPayment.isIsPromptForCoupon());
        
    }
    
    private void populateAddTime(final RateEditForm rateInfo, final RateAddTime rateAddTime) {
        rateInfo.setEnableAddTime(true);
        rateInfo.setUseAddTimeNumber(rateAddTime.isIsAddTimeNumber());
        rateInfo.setUseSpaceOrPlate(rateAddTime.isIsSpaceOrPlateNumber());
        rateInfo.setUseEbP(rateAddTime.isIsEbP());
        if (rateAddTime.isIsEbP()) {
            rateInfo.setEbPServiceFeeAmount(WebCoreUtil.convertBase100ValueToFloatString(rateAddTime.getServiceFeeAmount()));
            rateInfo.setEbPMiniumExt(rateAddTime.getMinExtention().intValue());
        }
    }
    
    //TODO
    private void populateFlat(final RateEditForm rateInfo, final RateFlat rateFlat) {
        
    }
    
    //TODO
    private void populateDaily(final RateEditForm rateInfo, final RateDaily rateDaily) {
    }
    
    //TODO
    private void populateHourly(final RateEditForm rateInfo, final RateHourly rateHourly) {
    }
    
    //TODO
    private void populateIncremental(final RateEditForm rateInfo, final RateIncremental rateIncremental) {
        
        rateInfo.setRateAmount(WebCoreUtil.convertBase100ValueToFloatString(rateIncremental.getAmount()));
        rateInfo.setMinPaymentAmount(WebCoreUtil.convertBase100ValueToFloatString(rateIncremental.getMinAmount()));
        rateInfo.setMaxPaymentAmount(WebCoreUtil.convertBase100ValueToFloatString(rateIncremental.getMaxAmount()));
        
        rateInfo.setUseFlatRate(rateIncremental.isIsFlatRateEnabled());
        if (rateIncremental.isIsFlatRateEnabled()) {
            rateInfo.setFlatRateAmount(WebCoreUtil.convertBase100ValueToFloatString(rateIncremental.getFlatRateAmount()));
            rateInfo.setFlatRateStartTime(rateIncremental.getFlatRateStartTime());
            rateInfo.setFlatRateEndTime(rateIncremental.getFlatRateEndTime());
        }
        if (rateInfo.getPaymentCC()) {
            rateInfo.setUseLastIncrement(rateIncremental.isIsUseLastIncrementValue());
            rateInfo.setMinsInc01((int) rateIncremental.getCcinc01());
            rateInfo.setMinsInc02((int) rateIncremental.getCcinc02());
            rateInfo.setMinsInc03((int) rateIncremental.getCcinc03());
            rateInfo.setMinsInc04((int) rateIncremental.getCcinc04());
            rateInfo.setMinsInc05((int) rateIncremental.getCcinc05());
            rateInfo.setMinsInc06((int) rateIncremental.getCcinc06());
            rateInfo.setMinsInc07((int) rateIncremental.getCcinc07());
            rateInfo.setMinsInc08((int) rateIncremental.getCcinc08());
            rateInfo.setMinsInc09((int) rateIncremental.getCcinc09());
            rateInfo.setMinsInc10((int) rateIncremental.getCcinc10());
            
            rateInfo.setUseInc10(rateIncremental.getCcinc10() > 0);
            rateInfo.setUseInc09(rateIncremental.getCcinc09() > 0 || rateInfo.getUseInc10());
            rateInfo.setUseInc08(rateIncremental.getCcinc08() > 0 || rateInfo.getUseInc09());
            rateInfo.setUseInc07(rateIncremental.getCcinc07() > 0 || rateInfo.getUseInc08());
            rateInfo.setUseInc06(rateIncremental.getCcinc06() > 0 || rateInfo.getUseInc07());
            rateInfo.setUseInc05(rateIncremental.getCcinc05() > 0 || rateInfo.getUseInc06());
            rateInfo.setUseInc04(rateIncremental.getCcinc04() > 0 || rateInfo.getUseInc05());
            rateInfo.setUseInc03(rateIncremental.getCcinc03() > 0 || rateInfo.getUseInc04());
            rateInfo.setUseInc02(rateIncremental.getCcinc02() > 0 || rateInfo.getUseInc03());
            rateInfo.setUseInc01(rateIncremental.getCcinc01() > 0 || rateInfo.getUseInc02());
            
        }
    }
    
    //TODO
    private void populateMonthly(final RateEditForm rateInfo, final RateMonthly rateMonthly) {
    }
    
    //TODO
    private void populateRestriction(final RateEditForm rateInfo, final RateRestriction rateRestriction) {
    }
    
    /**
     * This method save alert information in CustomerAlertType table.
     * 
     * @param request
     *            HttpServletRequest object
     * @param webSecurityForm
     *            user input form
     * @return "true" + form token value if process succeeds, "false" string if
     *         process fails.
     */
    private String saveRate(final HttpServletRequest request, final HttpServletResponse response, final WebSecurityForm<RateEditForm> webSecurityForm,
                            final Rate rate) {
        
        final RateEditForm form = webSecurityForm.getWrappedObject();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final Date currentTime = new Date();
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        final Customer customer = this.customerService.findCustomer((Integer) customerId);
        
        rate.setCustomer(customer);
        rate.setName(form.getName().trim());
        rate.setRateType(this.rateTypeService.findRateTypeById(form.getRateType().byteValue()));
        rate.setLastModifiedByUserId(userAccount.getId());
        rate.setLastModifiedGmt(currentTime);
        
        if (form.getRateType() != WebCoreConstants.RATE_TYPE_PARKING_RESTRICTION) {
            rate.setRateAcceptedPayment(createAcceptedPayment(form, rate, rate.getRateAcceptedPayment(), userAccount.getId(), currentTime));
        } else {
            rate.setRateAcceptedPayment(null);
        }
        
        if (form.getEnableAddTime()) {
            rate.setRateAddTime(createAddTime(form, rate, rate.getRateAddTime(), userAccount.getId(), currentTime));
        } else {
            rate.setRateAddTime(null);
        }
        
        switch (form.getRateType().byteValue()) {
            case WebCoreConstants.RATE_TYPE_FLAT:
                rate.setRateFlat(createFlat(form, rate, rate.getRateFlat(), userAccount.getId(), currentTime));
                rate.setRateDaily(null);
                rate.setRateHourly(null);
                rate.setRateIncremental(null);
                rate.setRateMonthly(null);
                rate.setRateRestriction(null);
                break;
            case WebCoreConstants.RATE_TYPE_DAILY:
                rate.setRateFlat(null);
                rate.setRateDaily(createDaily(form, rate, rate.getRateDaily(), userAccount.getId(), currentTime));
                rate.setRateHourly(null);
                rate.setRateIncremental(null);
                rate.setRateMonthly(null);
                rate.setRateRestriction(null);
                break;
            case WebCoreConstants.RATE_TYPE_HOURLY:
                rate.setRateFlat(null);
                rate.setRateDaily(null);
                rate.setRateHourly(createHourly(form, rate, rate.getRateHourly(), userAccount.getId(), currentTime));
                rate.setRateIncremental(null);
                rate.setRateMonthly(null);
                rate.setRateRestriction(null);
                break;
            case WebCoreConstants.RATE_TYPE_INCREMENTAL:
                rate.setRateFlat(null);
                rate.setRateDaily(null);
                rate.setRateHourly(null);
                rate.setRateIncremental(createIncremental(form, rate, rate.getRateIncremental(), userAccount.getId(), currentTime));
                rate.setRateMonthly(null);
                rate.setRateRestriction(null);
                break;
            case WebCoreConstants.RATE_TYPE_MONTHLY:
                rate.setRateFlat(null);
                rate.setRateDaily(null);
                rate.setRateHourly(null);
                rate.setRateIncremental(null);
                rate.setRateMonthly(createMonthly(form, rate, rate.getRateMonthly(), userAccount.getId(), currentTime));
                rate.setRateRestriction(null);
                break;
            case WebCoreConstants.RATE_TYPE_PARKING_RESTRICTION:
                rate.setRateFlat(null);
                rate.setRateDaily(null);
                rate.setRateHourly(null);
                rate.setRateIncremental(null);
                rate.setRateMonthly(null);
                rate.setRateRestriction(createRestriction(form, rate, rate.getRateRestriction(), userAccount.getId(), currentTime));
                break;
            default:
                break;
        }
        
        this.rateService.saveOrUpdateRate(rate);
        
        return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(WebCoreConstants.COLON).append(webSecurityForm.getInitToken()).toString();
    }
    
    @SuppressWarnings({ "checkstyle:finalparameters", "checkstyle:parameterassignment" })
    private RateAcceptedPayment createAcceptedPayment(final RateEditForm form, final Rate rate, RateAcceptedPayment rateAcceptedPayment,
                                                      final Integer userAccountId, final Date currentTime) {
        if (rateAcceptedPayment == null) {
            rateAcceptedPayment = new RateAcceptedPayment();
        }
        rateAcceptedPayment.setIsBill(form.getPaymentBill());
        rateAcceptedPayment.setIsCoin(form.getPaymentCoin());
        rateAcceptedPayment.setIsCreditCard(form.getPaymentCC());
        rateAcceptedPayment.setIsContactless(form.getPaymentRF());
        rateAcceptedPayment.setIsSmartCard(form.getPaymentSC());
        rateAcceptedPayment.setIsPasscard(form.getPaymentPC());
        rateAcceptedPayment.setIsCoupon(form.getPaymentCoupon());
        if (form.getPaymentCoupon()) {
            rateAcceptedPayment.setIsPromptForCoupon(form.getPromptForCoupon());
        }
        rateAcceptedPayment.setLastModifiedByUserId(userAccountId);
        rateAcceptedPayment.setLastModifiedGmt(currentTime);
        rateAcceptedPayment.setRate(rate);
        
        return rateAcceptedPayment;
    }
    
    @SuppressWarnings({ "checkstyle:finalparameters", "checkstyle:parameterassignment" })
    private RateAddTime createAddTime(final RateEditForm form, final Rate rate, RateAddTime rateAddTime, final Integer userAccountId, final Date currentTime) {
        if (rateAddTime == null) {
            rateAddTime = new RateAddTime();
        }
        
        rateAddTime.setIsAddTimeNumber(form.getUseAddTimeNumber());
        rateAddTime.setIsSpaceOrPlateNumber(form.getUseSpaceOrPlate());
        rateAddTime.setIsEbP(form.getUseEbP());
        if (form.getUseEbP()) {
            rateAddTime.setServiceFeeAmount(WebCoreUtil.convertToBase100IntValue(form.getEbPServiceFeeAmount()));
            rateAddTime.setMinExtention(form.getEbPMiniumExt().shortValue());
        }
        rateAddTime.setRate(rate);
        rateAddTime.setLastModifiedByUserId(userAccountId);
        rateAddTime.setLastModifiedGmt(currentTime);
        return rateAddTime;
    }
    
    //TODO
    @SuppressWarnings({ "checkstyle:finalparameters", "checkstyle:parameterassignment" })
    private RateFlat createFlat(final RateEditForm form, final Rate rate, RateFlat rateFlat, final Integer userAccountId, final Date currentTime) {
        
        if (rateFlat == null) {
            rateFlat = new RateFlat();
        }
        
        rateFlat.setRate(rate);
        rateFlat.setLastModifiedByUserId(userAccountId);
        rateFlat.setLastModifiedGmt(currentTime);
        return rateFlat;
    }
    
    //TODO
    @SuppressWarnings({ "checkstyle:finalparameters", "checkstyle:parameterassignment" })
    private RateDaily createDaily(final RateEditForm form, final Rate rate, RateDaily rateDaily, final Integer userAccountId, final Date currentTime) {
        if (rateDaily == null) {
            rateDaily = new RateDaily();
        }
        
        rateDaily.setRate(rate);
        rateDaily.setLastModifiedByUserId(userAccountId);
        rateDaily.setLastModifiedGmt(currentTime);
        return rateDaily;
    }
    
    //TODO
    @SuppressWarnings({ "checkstyle:finalparameters", "checkstyle:parameterassignment" })
    private RateHourly createHourly(final RateEditForm form, final Rate rate, RateHourly rateHourly, final Integer userAccountId, final Date currentTime) {
        if (rateHourly == null) {
            rateHourly = new RateHourly();
        }
        
        rateHourly.setRate(rate);
        rateHourly.setLastModifiedByUserId(userAccountId);
        rateHourly.setLastModifiedGmt(currentTime);
        return rateHourly;
    }
    
    @SuppressWarnings({ "checkstyle:finalparameters", "checkstyle:parameterassignment" })
    private RateIncremental createIncremental(final RateEditForm form, final Rate rate, RateIncremental rateIncremental, final Integer userAccountId,
                                              final Date currentTime) {
        if (rateIncremental == null) {
            rateIncremental = new RateIncremental();
        }
        
        rateIncremental.setAmount(WebCoreUtil.convertToBase100IntValue(form.getRateAmount()));
        rateIncremental.setLength(MINUTES_60);
        rateIncremental.setMinAmount(WebCoreUtil.convertToBase100IntValue(form.getMinPaymentAmount()));
        rateIncremental.setMaxAmount(WebCoreUtil.convertToBase100IntValue(form.getMaxPaymentAmount()));
        rateIncremental.setIsFlatRateEnabled(form.getUseFlatRate());
        if (form.getUseFlatRate()) {
            rateIncremental.setFlatRateAmount(WebCoreUtil.convertToBase100IntValue(form.getFlatRateAmount()));
            rateIncremental.setFlatRateStartTime(form.getFlatRateStartTime());
            rateIncremental.setFlatRateEndTime(form.getFlatRateEndTime());
        }
        if (form.getPaymentCC()) {
            rateIncremental.setIsUseLastIncrementValue(form.getUseLastIncrement());
            rateIncremental.setCcinc01(form.getMinsInc01() != null ? form.getMinsInc01().byteValue() : 0);
            rateIncremental.setCcinc02(form.getMinsInc02() != null ? form.getMinsInc02().byteValue() : 0);
            rateIncremental.setCcinc03(form.getMinsInc03() != null ? form.getMinsInc03().byteValue() : 0);
            rateIncremental.setCcinc04(form.getMinsInc04() != null ? form.getMinsInc04().byteValue() : 0);
            rateIncremental.setCcinc05(form.getMinsInc05() != null ? form.getMinsInc05().byteValue() : 0);
            rateIncremental.setCcinc06(form.getMinsInc06() != null ? form.getMinsInc06().byteValue() : 0);
            rateIncremental.setCcinc07(form.getMinsInc07() != null ? form.getMinsInc07().byteValue() : 0);
            rateIncremental.setCcinc08(form.getMinsInc08() != null ? form.getMinsInc08().byteValue() : 0);
            rateIncremental.setCcinc09(form.getMinsInc09() != null ? form.getMinsInc09().byteValue() : 0);
            rateIncremental.setCcinc10(form.getMinsInc10() != null ? form.getMinsInc10().byteValue() : 0);
        }
        rateIncremental.setRate(rate);
        rateIncremental.setLastModifiedByUserId(userAccountId);
        rateIncremental.setLastModifiedGmt(currentTime);
        return rateIncremental;
    }
    
    //TODO
    @SuppressWarnings({ "checkstyle:finalparameters", "checkstyle:parameterassignment" })
    private RateMonthly createMonthly(final RateEditForm form, final Rate rate, RateMonthly rateMonthly, final Integer userAccountId, final Date currentTime) {
        if (rateMonthly == null) {
            rateMonthly = new RateMonthly();
        }
        
        rateMonthly.setRate(rate);
        rateMonthly.setLastModifiedByUserId(userAccountId);
        rateMonthly.setLastModifiedGmt(currentTime);
        return rateMonthly;
    }
    
    //TODO
    @SuppressWarnings({ "checkstyle:finalparameters", "checkstyle:parameterassignment" })
    private RateRestriction createRestriction(final RateEditForm form, final Rate rate, RateRestriction rateRestriction, final Integer userAccountId,
                                              final Date currentTime) {
        if (rateRestriction == null) {
            rateRestriction = new RateRestriction();
        }
        
        rateRestriction.setRate(rate);
        rateRestriction.setLastModifiedByUserId(userAccountId);
        rateRestriction.setLastModifiedGmt(currentTime);
        return rateRestriction;
    }
    
    private List<RateListInfo> populateRateListInfo(final List<Rate> rateList, final RandomKeyMapping keyMapping) {
        final List<RateListInfo> rateListInfoList = new ArrayList<RateListInfo>();
        for (Rate rate : rateList) {
            final RateListInfo rateListInfo = new RateListInfo();
            rateListInfo.setRandomRateId(keyMapping.getRandomString(rate, WebCoreConstants.ID_LOOK_UP_NAME));
            rateListInfo.setName(rate.getName());
            rateListInfo.setRateTypeId((int) rate.getRateType().getId());
            rateListInfo.setRateTypeName(rate.getRateType().getName());
            rateListInfo.setRateValue(getRateValue(rate));
            rateListInfoList.add(rateListInfo);
        }
        return rateListInfoList;
    }
    
    private String getRateValue(final Rate rate) {
        final StringBuilder rateValue = new StringBuilder();
        if (rate.getRateType().getId() != WebCoreConstants.RATE_TYPE_PARKING_RESTRICTION) {
            switch (rate.getRateType().getId()) {
                case WebCoreConstants.RATE_TYPE_FLAT:
                    rateValue.append(WebCoreConstants.DOLLAR_SIGN);
                    rateValue.append(WebCoreUtil.convertBase100ValueToFloatString(rate.getRateFlat().getAmount()));
                    break;
                case WebCoreConstants.RATE_TYPE_INCREMENTAL:
                    rateValue.append(WebCoreConstants.DOLLAR_SIGN);
                    rateValue.append(WebCoreUtil.convertBase100ValueToFloatString(rate.getRateIncremental().getAmount()));
                    rateValue.append(WebCoreConstants.EMPTY_SPACE);
                    rateValue.append(this.messageSource.getMessage("label.rate.perHour", null, null));
                    break;
                case WebCoreConstants.RATE_TYPE_HOURLY:
                    if (rate.getRateHourly().isIsVariable()) {
                        rateValue.append(this.messageSource.getMessage("label.rate.variable", null, null));
                    } else {
                        final RateHourlyRule[] rateHourlyRules = rate.getRateHourlyRules().toArray(new RateHourlyRule[1]);
                        rateValue.append(WebCoreConstants.DOLLAR_SIGN);
                        rateValue.append(WebCoreUtil.convertBase100ValueToFloatString(rateHourlyRules[0].getAmount() / rateHourlyRules[0].getHours()));
                        rateValue.append(WebCoreConstants.EMPTY_SPACE);
                        rateValue.append(this.messageSource.getMessage("label.rate.perHour", null, null));
                    }
                    break;
                case WebCoreConstants.RATE_TYPE_DAILY:
                    if (rate.getRateDaily().isIsVariable()) {
                        rateValue.append(this.messageSource.getMessage("label.rate.variable", null, null));
                    } else {
                        rateValue.append(WebCoreConstants.DOLLAR_SIGN);
                        rateValue.append(WebCoreUtil.convertBase100ValueToFloatString(rate.getRateDaily().getMonAmount()));
                        rateValue.append(WebCoreConstants.EMPTY_SPACE);
                        rateValue.append(this.messageSource.getMessage("label.rate.perDay", null, null));
                    }
                    break;
                case WebCoreConstants.RATE_TYPE_MONTHLY:
                    rateValue.append(WebCoreConstants.DOLLAR_SIGN);
                    rateValue.append(WebCoreUtil.convertBase100ValueToFloatString(rate.getRateMonthly().getAmount()));
                    rateValue.append(WebCoreConstants.EMPTY_SPACE);
                    rateValue.append(this.messageSource.getMessage("label.rate.perMonth", null, null));
                    break;
                default:
                    break;
            }
        }
        return rateValue.toString();
    }
    
    @ModelAttribute(FORM_SEARCH)
    public final WebSecurityForm<RateSearchForm> initRateSearchForm(final HttpServletRequest request) {
        return new WebSecurityForm<RateSearchForm>(null, new RateSearchForm());
    }
    
}
