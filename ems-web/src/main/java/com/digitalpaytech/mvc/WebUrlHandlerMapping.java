package com.digitalpaytech.mvc;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;
import com.digitalpaytech.util.WebCoreConstants;


public class WebUrlHandlerMapping extends SimpleUrlHandlerMapping
{
    
	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.handler.AbstractUrlHandlerMapping#lookupHandler(java.lang.String, javax.servlet.http.HttpServletRequest)
	 */
	protected Object lookupHandler(String urlPath, HttpServletRequest request) throws Exception
	{
		Object handler = super.lookupHandler(urlPath, request);
		if(handler == null)
		{
			request.setAttribute("error", WebCoreConstants.ERROR_FEATUREINVALID);
			handler = this.getHandlerMap().get("/error.html");
		}
		return handler;
	}

}
