package com.digitalpaytech.mvc;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Section;
import com.digitalpaytech.domain.Tab;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetSubsetMember;
import com.digitalpaytech.dto.TabHeading;
import com.digitalpaytech.dto.WidgetCacheKey;
import com.digitalpaytech.dto.WidgetData;
import com.digitalpaytech.dto.WidgetMasterList;
import com.digitalpaytech.dto.WidgetMasterListInfo;
import com.digitalpaytech.exception.RandomKeyMappingException;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.WebObjectCacheManager;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.DashboardService;
import com.digitalpaytech.service.WebWidgetService;
import com.digitalpaytech.service.WebWidgetServiceFactory;
import com.digitalpaytech.util.DashboardUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.LabelConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.WidgetConstants;
import com.digitalpaytech.util.support.RelativeDateTime;

/**
 * WidgetController gets the request for creating, deleting, ordering, and
 * retrieving dashboard widgets and pass them back to the frond end UI.
 * 
 * @author Brian Kim
 */
@Controller
@SessionAttributes({ WebSecurityConstants.WEB_SECURITY_FORM })
public class WidgetController extends CommonLandingController {
    
    private static final Logger LOG = Logger.getLogger(WidgetController.class);
    
    @Autowired
    private DashboardService dashboardService;
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    @Autowired
    private WebObjectCacheManager cacheManager;
    
    private WebWidgetServiceFactory webWidgetServiceFactory;
    
    private String[] addWidgetCategoryNames;
    
    public final void setDashboardService(final DashboardService dashboardService) {
        this.dashboardService = dashboardService;
    }
    
    public final void setWebWidgetServiceFactory(final WebWidgetServiceFactory webWidgetServiceFactory) {
        this.webWidgetServiceFactory = webWidgetServiceFactory;
    }
    
    public final void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }
    
    @PostConstruct
    private void createCategoryNamesIfNecessary() {
        this.addWidgetCategoryNames = new String[] { this.commonControllerHelper.getMessage("label.dashboard.widgetSettings.Metric.ActiveAlerts"),
            this.commonControllerHelper.getMessage("label.dashboard.widgetSettings.Metric.AverageDuration"),
            this.commonControllerHelper.getMessage("label.dashboard.widgetSettings.Metric.AveragePrice"),
            this.commonControllerHelper.getMessage("label.dashboard.widgetSettings.Metric.AverageRevenue"),
            this.commonControllerHelper.getMessage("label.dashboard.widgetSettings.Metric.SettledPurchases"),
            this.commonControllerHelper.getMessage("label.dashboard.widgetSettings.Metric.SettledRevenue"),
            this.commonControllerHelper.getMessage("label.dashboard.widgetSettings.Metric.SettledCards"),
            this.commonControllerHelper.getMessage("label.dashboard.widgetSettings.Metric.Collections"),
            this.commonControllerHelper.getMessage("label.dashboard.widgetSettings.Metric.Map"),
            this.commonControllerHelper.getMessage("label.dashboard.widgetSettings.Metric.Occupancy"),
            this.commonControllerHelper.getMessage("label.dashboard.widgetSettings.Metric.Purchases"),
            this.commonControllerHelper.getMessage("label.dashboard.widgetSettings.Metric.Revenue"),
            this.commonControllerHelper.getMessage("label.dashboard.widgetSettings.Metric.Turnover"),
            this.commonControllerHelper.getMessage("label.dashboard.widgetSettings.Metric.Utilization"),
            this.commonControllerHelper.getMessage("label.dashboard.widgetSettings.Metric.Citation"),
            this.commonControllerHelper.getMessage("label.dashboard.widgetSettings.Metric.CitationMap"),
            this.commonControllerHelper.getMessage("label.dashboard.widgetSettings.Metric.PhysicalOccupancy"),
            this.commonControllerHelper.getMessage("label.dashboard.widgetSettings.Metric.CompareOccupancy"),
            this.commonControllerHelper.getMessage("label.dashboard.widgetSettings.Metric.OccupancyMap"), };
    }
    
    @RequestMapping(value = "/secure/dashboard/addWidget.html")
    public final String addWidget(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        final String tabId = request.getParameter("tabID");
        if (!DashboardUtil.validateRandomId(tabId)) {
            LOG.warn("### Invalid tabID in the request. ###");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }
        
        final String sectionId = request.getParameter("sectionID");
        if (!DashboardUtil.validateRandomId(sectionId)) {
            LOG.warn("### Invalid sectionId in the request. ###");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }
        
        final String widgetTypeId = request.getParameter("widgetTypeID");
        if (!DashboardUtil.validateRandomId(widgetTypeId)) {
            LOG.warn("### Invalid widgetTypeId in the request. ###");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }
        
        @SuppressWarnings("unchecked")
        final Map<String, Tab> tabTreeMap = (Map<String, Tab>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        final RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        Tab tab = tabTreeMap.get(tabId);
        
        if (tab == null) {
            final Object actualId = keyMapping.getKey(tabId);
            if (!DashboardUtil.validateDBIntegerPrimaryKey(actualId.toString())) {
                LOG.warn("### Invalid tabId in the request. ###");
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return null;
            }
            final int id = Integer.parseInt(actualId.toString());
            tab = this.dashboardService.findTabByIdEager(id);
            // Generates randomId for Tab, Sections and Widgets.
            tab = DashboardUtil.setRandomIds(request, tab);
            if (tab.getUserAccount().getId() == WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID) {
                tab = removeMasterList(request, tab, WebSecurityUtil.getUserAccount().getCustomer().isIsParent());
            }
            
            if (tab == null) {
                LOG.warn("### No matched Tab from temporary map. ###");
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return null;
            }
            tabTreeMap.put(tab.getRandomId(), tab);
        }
        
        final Section section = tab.getSection(sectionId);
        if (section == null) {
            LOG.warn("### No matched Section from temporary map. ###");
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return null;
        }
        
        final Object actualWidgetId = keyMapping.getKey(widgetTypeId);
        final Widget newWidget = createWidget(request, actualWidgetId.toString());
        if (newWidget == null) {
            LOG.warn("### No Widget found from widgetTypeID. ###");
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return null;
        }
        
        newWidget.setRandomId(DashboardUtil.getTemporaryId(WebCoreConstants.TEMP_WIDGET_ID_PREFIX));
        
        String randomStr = null;
        try {
            randomStr = keyMapping.hideProperty(newWidget, "randomId").getPrimaryKey().toString();
        } catch (RandomKeyMappingException e) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return null;
        }
        newWidget.setRandomId(randomStr);
        
        final List<Widget> widgets = section.getSectionColumn("0").getWidgets();
        for (Widget w : widgets) {
            w.setOrderNumber(w.getOrderNumber() + 1);
        }
        
        newWidget.setLastModifiedGmt(new GregorianCalendar().getTime());
        newWidget.setSection(section);
        
        section.addWidget(newWidget);
        tabTreeMap.put(tab.getRandomId(), tab);
        model.put("widget", newWidget);
        model.put("widgetID", newWidget.getRandomId());
        
        return "/dashboard/include/widgetShell";
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/dashboard/widgetOrder.html", method = RequestMethod.POST)
    public final String updateWidgetOrder(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        final String tabId = request.getParameter("tabID");
        if (!DashboardUtil.validateRandomId(tabId)) {
            LOG.warn("### Invalid tabID in updateWidgetOrder method. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String[] sectionIds = request.getParameterValues("section[]");
        if (sectionIds == null || sectionIds.length < 1) {
            LOG.warn("### Invalid section[] in updateWidgetOrder method. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        for (String sectionId : sectionIds) {
            if (!DashboardUtil.validateRandomId(sectionId)) {
                LOG.warn("### Invalid sectionID in updateWidgetOrder method. ###");
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            for (int i = 0; i < WebCoreConstants.DEFAULT_NUMBER_OF_COLUMN; i++) {
                final StringBuffer buffer = new StringBuffer("sec");
                final String[] widgetIds = request.getParameterValues(buffer.append(sectionId).append("col").append(i).append("[]").toString());
                if (widgetIds != null) {
                    for (int j = 0; j < widgetIds.length; j++) {
                        if (!DashboardUtil.validateRandomId(widgetIds[j])) {
                            LOG.warn("### Invalid widgetID in updateWidgetOrder method. ###");
                            return WebCoreConstants.RESPONSE_FALSE;
                        }
                    }
                }
            }
        }
        
        @SuppressWarnings("unchecked")
        final Map<String, Tab> tabTreeMap = (Map<String, Tab>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        Tab tab = tabTreeMap.get(tabId);
        final RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        
        if (tab == null) {
            final Object actualId = keyMapping.getKey(tabId);
            if (!DashboardUtil.validateDBIntegerPrimaryKey(actualId.toString())) {
                LOG.warn("### Invalid tabId in the request. ###");
                return WebCoreConstants.RESPONSE_FALSE;
            }
            final int id = Integer.parseInt(actualId.toString());
            tab = this.dashboardService.findTabByIdEager(id);
            if (tab == null) {
                LOG.warn("### No matched Tab from temporary map. ###");
                return WebCoreConstants.RESPONSE_FALSE;
            }
            // Generates randomId for Tab, Sections and Widgets.
            tab = DashboardUtil.setRandomIds(request, tab);
            tabTreeMap.put(tab.getRandomId(), tab);
        }
        
        for (String sectionId : sectionIds) {
            for (int i = 0; i < WebCoreConstants.DEFAULT_NUMBER_OF_COLUMN; i++) {
                final StringBuffer builder = new StringBuffer("sec");
                final String[] widgetIds = request.getParameterValues(builder.append(sectionId).append("col").append(i).append("[]").toString());
                if (widgetIds != null) {
                    for (int j = 0; j < widgetIds.length; j++) {
                        final Section section = tab.getSection(sectionId);
                        Widget widget = section.getWidget(widgetIds[j]);
                        if (widget == null) {
                            //If widget is null, the widget was moved from another section. Find the widget and set the section                         
                            sectionLoop: for (Section sec : tab.getSections()) {
                                for (Widget wid : sec.getWidgets()) {
                                    if (wid.getRandomId().equals(widgetIds[j])) {
                                        widget = wid;
                                        break sectionLoop;
                                    }
                                }
                            }
                            if (widget == null) {
                                return WebCoreConstants.RESPONSE_FALSE;
                            }
                            final Section oldSection = widget.getSection();
                            oldSection.deleteWidget(widgetIds[j]);
                            widget.setSection(section);
                            section.addWidget(widget);
                        }
                        widget.setColumnId(i);
                        widget.setOrderNumber(j + 1);
                    }
                }
            }
        }
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/dashboard/deleteWidget.html", method = RequestMethod.GET)
    public final String deleteWidget(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        final String tabId = request.getParameter("tabID");
        if (!DashboardUtil.validateRandomId(tabId)) {
            LOG.warn("### Invalid tabID in the request. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String sectionId = request.getParameter("sectionID");
        if (!DashboardUtil.validateRandomId(sectionId)) {
            LOG.warn("### Invalid sectionID in the request. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String widgetId = request.getParameter("widgetID");
        if (!DashboardUtil.validateRandomId(widgetId)) {
            LOG.warn("### Invalid widgetID in the request. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String colId = request.getParameter("colID");
        if (!DashboardUtil.validateDBIntegerPrimaryKey(colId)) {
            LOG.warn("### Invalid columnID in the request. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        @SuppressWarnings("unchecked")
        final Map<String, Tab> tabTreeMap = (Map<String, Tab>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        final RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        Tab tab = tabTreeMap.get(tabId);
        
        if (tab == null) {
            final Object actualId = keyMapping.getKey(tabId);
            if (!DashboardUtil.validateDBIntegerPrimaryKey(actualId.toString())) {
                LOG.warn("### Invalid tabId in the request. ###");
                return WebCoreConstants.RESPONSE_FALSE;
            }
            final int id = Integer.parseInt(actualId.toString());
            tab = this.dashboardService.findTabByIdEager(id);
            if (tab == null) {
                LOG.warn("### No matched Tab from temporary map. ###");
                return WebCoreConstants.RESPONSE_FALSE;
            }
            // Generates randomId for Tab, Sections and Widgets.
            tab = DashboardUtil.setRandomIds(request, tab);
            tabTreeMap.put(tab.getRandomId(), tab);
        }
        
        final Section section = tab.getSection(sectionId);
        final Widget w = section.getWidget(widgetId);
        final int orderNum = w.getOrderNumber();
        
        Widget wig = null;
        final List<Widget> listWidget = section.getSectionColumn(colId).getWidgets();
        for (Widget widget : listWidget) {
            if (widget.getRandomId().equals(widgetId)) {
                wig = widget;
                break;
            }
        }
        
        if (wig != null && wig.getId() != null && wig.getId() > 0) {
            @SuppressWarnings("unchecked")
            List<Integer> widgetIds = (List<Integer>) sessionTool.getAttribute(WebCoreConstants.SESSION_WIDGET_IDS_TO_BE_DELETED);
            if (widgetIds == null) {
                widgetIds = new ArrayList<Integer>();
            }
            widgetIds.add(wig.getId());
            sessionTool.setAttribute(WebCoreConstants.SESSION_WIDGET_IDS_TO_BE_DELETED, widgetIds);
        }
        
        section.deleteWidget(widgetId);
        tab.setSection(sectionId, section);
        
        for (Widget widget : listWidget) {
            if (widget.getOrderNumber() > orderNum) {
                widget.setOrderNumber(widget.getOrderNumber() - 1);
            }
        }
        
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, tabTreeMap);
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    private Widget createWidget(final HttpServletRequest request, final String widgetId) {
        
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final WebUser webUser = (WebUser) authentication.getPrincipal();
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final Widget widget = this.dashboardService.findDefaultWidgetByIdAndEvict(Integer.parseInt(widgetId.toString()));
        if (widget == null) {
            return null;
        }
        
        widget.getWidgetListType().setId(WidgetConstants.LIST_TYPE_USER);
        widget.setId(null);
        widget.setUserAccountId(webUser.getUserAccountId());
        widget.setCustomerId(webUser.getCustomerId());
        widget.setColumnId(0);
        widget.setOrderNumber(1);
        
        for (WidgetSubsetMember subsetMember : widget.getWidgetSubsetMembers()) {
            subsetMember.setId(null);
            subsetMember.setWidget(widget);
        }
        
        if (userAccount.getCustomer().getParentCustomer() == null) {
            widget.setParentCustomerId(new Integer(0));
        } else {
            widget.setParentCustomerId(userAccount.getCustomer().getParentCustomer().getId());
        }
        
        return widget;
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/dashboard/widgetMasterList.html")
    public final String showWidgetMasterList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        final List<Widget> masterList = this.dashboardService.findWidgetMasterList(WebSecurityUtil.getUserAccount());
        if (masterList == null) {
            LOG.warn("+++ masterList of widgets not found in WidgetController.showWidgetMasterList() method +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }
        
        final WidgetMasterList widgetMasterList = new WidgetMasterList();
        widgetMasterList.setCategoryNames(this.addWidgetCategoryNames);
        final RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        
        for (Widget masterListEntry : masterList) {
            final WidgetMasterListInfo masterListInfo = new WidgetMasterListInfo(masterListEntry);
            masterListInfo.setId(keyMapping.getRandomString(masterListEntry, "id"));
            widgetMasterList.add(masterListInfo);
        }
        
        final String json = WidgetMetricsHelper.convertToJson(widgetMasterList, "widgetMasterList", false);
        return json;
    }
    
    // ------------------------------------------------------------------------------------
    // Widget Data
    // ------------------------------------------------------------------------------------
    
    @ResponseBody
    @RequestMapping(value = "/secure/dashboard/widget.html")
    public final String getWidgetData(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
        }
        
        final String strWidgetId = request.getParameter("widgetID");
        
        if (!DashboardUtil.validateRandomId(strWidgetId)) {
            LOG.warn("+++ Invalid randomised widgetID in WidgetController.getWidgetData() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        WidgetData data = null;
        WidgetCacheKey cacheKey = null;
        Widget widget = findWidgetFromMapIfExists(request, strWidgetId);
        if (widget != null) {
            cacheKey = new WidgetCacheKey(widget, userAccount.getCustomer().getId());
        }
        
        if (cacheKey != null && this.cacheManager.isWidgetDataInCache(cacheKey)) {
            data = this.cacheManager.getWidgetData(cacheKey);
            
        } else {
            if (widget == null) {
                /* retrieve widget from db. */
                
                /* convert the randomised id to the actual database id. */
                final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
                final Object actualWidgetId = keyMapping.getKey(strWidgetId);
                if (actualWidgetId == null || !DashboardUtil.validateDBIntegerPrimaryKey(actualWidgetId.toString())) {
                    LOG.warn(String.format("Invalid widgetID [%s]", actualWidgetId));
                    keyMapping.logReverseMap();
                    response.setStatus(HttpStatus.BAD_REQUEST.value());
                    return null;
                }
                
                final int widgetId = Integer.parseInt(actualWidgetId.toString());
                
                widget = this.dashboardService.findWidgetById(widgetId);
                if (widget == null) {
                    response.setStatus(HttpStatus.NOT_FOUND.value());
                    return null;
                }
            }
            
            // It's default dashboard, verify widget owner is DPT admin, or
            // widget owner is the current user for new widgets on default
            // dashboard
            if (userAccount.getIsDefaultDashboard() && widget.getUserAccountId() != WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID
                && widget.getUserAccountId() != userAccount.getId()) {
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return null;
                
            } else if (!userAccount.getIsDefaultDashboard() && userAccount.getId().intValue() != widget.getUserAccountId()) {
                // It's not default dashboard, verify widget owner is logged-in
                // user.
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return null;
            }
            
            cacheKey = new WidgetCacheKey(widget, userAccount.getCustomer().getId());
            
            if (cacheKey != null && this.cacheManager.isWidgetDataInCache(cacheKey)) {
                data = this.cacheManager.getWidgetData(cacheKey);
            } else {
                
                this.webWidgetServiceFactory = WebWidgetServiceFactory.getInstance();
                final WebWidgetService webWidgetService = this.webWidgetServiceFactory.getWebWidgetService(widget.getWidgetMetricType().getId());
                
                data = webWidgetService.getWidgetData(widget, userAccount);
                data.getWidgetMapInfo().setLastUpdate(new RelativeDateTime(WebSecurityUtil.getCustomerTimeZone()));
                
                // set randomId of widgetData to match the widget randomId
                data.setRandomId(strWidgetId);
                data.setSeriesDataFromMap();
                
                if (data.getChartType() == WidgetConstants.CHART_TYPE_LIST) {
                    data.setListData(widget);
                }
                
                this.cacheManager.cacheWidgetData(cacheKey, data);
            }
        }
        
        data.setWidgetName(widget.getName());
        
        if (data.isLimitExceeded()) {
            LOG.warn("### Data limit exceeded. ###");
            response.setHeader(WebCoreConstants.HEADER_CUSTOM_STATUS_CODE, "" + WebCoreConstants.HTTP_STATUS_CODE_509);
        } else if (data.isZeroValues()) {
            LOG.debug("### Data was all zero values. ###");
            response.setHeader(WebCoreConstants.HEADER_CUSTOM_STATUS_CODE, "" + WebCoreConstants.HTTP_STATUS_CODE_449);
        }
        
        return WidgetMetricsHelper.convertToJson(data, "widgetData", true);
    }
    
    // ------------------------------------------------------------------------------------
    // Widget Data CSV File
    // ------------------------------------------------------------------------------------
    @ResponseBody
    @RequestMapping(value = "/secure/dashboard/widgetCSV.html")
    public final String getWidgetDataAsCSV(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        final String strWidgetId = request.getParameter("widgetID");
        
        if (!DashboardUtil.validateRandomId(strWidgetId)) {
            LOG.warn("+++ Invalid randomised widgetID in WidgetController.getWidgetDataAsCSV() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final String timeZone = WebSecurityUtil.getCustomerTimeZone();
        
        WidgetData data = null;
        WidgetCacheKey cacheKey = null;
        Widget widget = findWidgetFromMapIfExists(request, strWidgetId);
        if (widget != null) {
            cacheKey = new WidgetCacheKey(widget, userAccount.getCustomer().getId());
        }
        
        if (cacheKey != null && this.cacheManager.isWidgetDataInCache(cacheKey)) {
            data = this.cacheManager.getWidgetData(cacheKey);
        } else {
            /* get user account ID. */
            final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            final WebUser webUser = (WebUser) authentication.getPrincipal();
            
            if (widget == null) {
                /* retrieve widget from db. */
                
                /* convert the randomised id to the actual database id. */
                final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
                final Object actualWidgetId = keyMapping.getKey(strWidgetId);
                if (!DashboardUtil.validateDBIntegerPrimaryKey(actualWidgetId.toString())) {
                    LOG.warn("+++ Invalid widgetID in MainLandingController.getActualWidgetData() method. +++");
                    response.setStatus(HttpStatus.BAD_REQUEST.value());
                    return null;
                }
                
                final int widgetId = Integer.parseInt(actualWidgetId.toString());
                
                widget = this.dashboardService.findWidgetById(widgetId);
                if (widget == null) {
                    response.setStatus(HttpStatus.NOT_FOUND.value());
                    return null;
                }
            }
            
            // It's default dashboard, verify widget owner is DPT admin, or
            // widget owner is the current user for new widgets on default
            // dashboard
            if (userAccount.getIsDefaultDashboard() && widget.getUserAccountId() != WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID
                && widget.getUserAccountId() != webUser.getUserAccountId()) {
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return null;
                
            } else if (!userAccount.getIsDefaultDashboard() && webUser.getUserAccountId().intValue() != widget.getUserAccountId()) {
                // It's not default dashboard, verify widget owner is logged-in
                // user.
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return null;
            }
            
            cacheKey = new WidgetCacheKey(widget, userAccount.getCustomer().getId());
            
            if (cacheKey != null && this.cacheManager.isWidgetDataInCache(cacheKey)) {
                data = this.cacheManager.getWidgetData(cacheKey);
            } else {
                LOG.warn("+++ WidgetData does not exist in WidgetController.getWidgetDataAsCSV() method. +++");
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        final byte[] fileContent = createWidgetCSVContent(widget, data);
        
        final StringBuilder bdr = new StringBuilder();
        bdr.append(WebCoreUtil.escapeFileName(widget.getName())).append("-")
                .append(DateUtil.createDateString(new SimpleDateFormat(DateUtil.EXPORT_DATA_DATE_TIME), timeZone, new Date()));
        bdr.append(".").append(ReportingConstants.REPORTS_FORMAT_CSV);
        final String exportFileName = bdr.toString();
        
        try {
            response.reset();
            response.setHeader("Content-Disposition", "attachment; filename=\"" + exportFileName + StandardConstants.STRING_DOUBLE_QUOTES);
            response.setContentType("text/csv");
            final ServletOutputStream outStream = response.getOutputStream();
            
            outStream.write(fileContent);
            outStream.flush();
            outStream.close();
        } catch (IOException e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    private byte[] createWidgetCSVContent(final Widget widget, final WidgetData data) {
        
        if (data == null) {
            return null;
        }
        boolean isAmount = false;
        
        final StringBuilder csvFileContent = new StringBuilder();
        csvFileContent.append(getTierType(widget.getWidgetTierTypeByWidgetTier1Type().getId()));
        // Paid vs Counted Occupancy widget substitutes Tier2Type with occupancyType
        if (widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_COMPARE_OCCUPANCY) {
            csvFileContent.append(this.commonControllerHelper.getMessage(LabelConstants.LABEL_CSV_COLUMN_NAME_OCCUPANCY_TYPE))
                    .append(StandardConstants.STRING_COMMA);
        } else {
            csvFileContent.append(getTierType(widget.getWidgetTierTypeByWidgetTier2Type().getId()));
            csvFileContent.append(getTierType(widget.getWidgetTierTypeByWidgetTier3Type().getId()));
        }
        
        switch (widget.getWidgetMetricType().getId()) {
            case WidgetConstants.METRIC_TYPE_REVENUE:
            case WidgetConstants.METRIC_TYPE_COLLECTIONS:
            case WidgetConstants.METRIC_TYPE_SETTLED_CARD:
            case WidgetConstants.METRIC_TYPE_AVERAGE_PRICE:
            case WidgetConstants.METRIC_TYPE_AVERAGE_REVENUE:
            case WidgetConstants.METRIC_TYPE_CARD_PROCESSING_REV:
                csvFileContent.append(this.commonControllerHelper.getMessage("label.csv.column.name.amount"));
                isAmount = true;
                break;
            case WidgetConstants.METRIC_TYPE_PAID_OCCUPANCY:
            case WidgetConstants.METRIC_TYPE_UTILIZATION:
            case WidgetConstants.METRIC_TYPE_PHYSICAL_OCCUPANCY:
            case WidgetConstants.METRIC_TYPE_COMPARE_OCCUPANCY:
                csvFileContent.append(this.commonControllerHelper.getMessage(LabelConstants.LABEL_CSV_COLUMN_NAME_VALUE))
                        .append(StandardConstants.STRING_EMPTY_SPACE);
                csvFileContent.append(this.commonControllerHelper.getMessage(LabelConstants.LABEL_CSV_COLUMN_NAME_PERCENT_SIGN_BRACKETS));
                break;
            case WidgetConstants.METRIC_TYPE_PURCHASES:
            case WidgetConstants.METRIC_TYPE_TURNOVER:
            case WidgetConstants.METRIC_TYPE_DURATION:
            case WidgetConstants.METRIC_TYPE_ACTIVE_ALERTS:
            case WidgetConstants.METRIC_TYPE_ALERT_STATUS:
            case WidgetConstants.METRIC_TYPE_AVERAGE_DURATION:
            case WidgetConstants.METRIC_TYPE_CARD_PROCESSING_TX:
            case WidgetConstants.METRIC_TYPE_CITATIONS:
                csvFileContent.append(this.commonControllerHelper.getMessage(LabelConstants.LABEL_CSV_COLUMN_NAME_VALUE));
                break;
            default:
                break;
        }
        csvFileContent.append(StandardConstants.STRING_END_OF_LINE);
        
        if (widget.getWidgetTierTypeByWidgetTier1Type().getId().intValue() != WidgetConstants.TIER_TYPE_NA
            && widget.getWidgetTierTypeByWidgetTier2Type().getId().intValue() == WidgetConstants.TIER_TYPE_NA
            && widget.getWidgetTierTypeByWidgetTier3Type().getId().intValue() == WidgetConstants.TIER_TYPE_NA
            && widget.getWidgetMetricType().getId() != WidgetConstants.METRIC_TYPE_COMPARE_OCCUPANCY) {
            final int xSize = data.getTier1Names().size();
            for (int i = 0; i < xSize; i++) {
                csvFileContent.append(StandardConstants.STRING_DOUBLE_QUOTES);
                csvFileContent.append(data.getTier1Names().get(i));
                csvFileContent.append(StandardConstants.STRING_DOUBLE_QUOTES);
                csvFileContent.append(StandardConstants.STRING_COMMA);
                if (isAmount) {
                    csvFileContent.append(WebCoreConstants.DOLLAR_SIGN);
                    csvFileContent.append(WebCoreUtil.formatToDollars(data.getSeriesList().get(0).getSeriesData().get(i)));
                } else {
                    csvFileContent.append(data.getSeriesList().get(0).getSeriesData().get(i));
                }
                csvFileContent.append(StandardConstants.STRING_END_OF_LINE);
            }
            
        } else if (widget.getWidgetTierTypeByWidgetTier1Type().getId().intValue() != WidgetConstants.TIER_TYPE_NA
                   && widget.getWidgetTierTypeByWidgetTier2Type().getId().intValue() != WidgetConstants.TIER_TYPE_NA
                   && widget.getWidgetTierTypeByWidgetTier3Type().getId().intValue() == WidgetConstants.TIER_TYPE_NA
                   || widget.getWidgetMetricType().getId() == WidgetConstants.METRIC_TYPE_COMPARE_OCCUPANCY) {
            final int xSize = data.getTier1Names().size();
            final int seriesSize = data.getSeriesList().size();
            for (int i = 0; i < xSize; i++) {
                for (int j = 0; j < seriesSize; j++) {
                    csvFileContent.append(StandardConstants.STRING_DOUBLE_QUOTES);
                    csvFileContent.append(data.getTier1Names().get(i));
                    csvFileContent.append(StandardConstants.STRING_DOUBLE_QUOTES);
                    csvFileContent.append(StandardConstants.STRING_COMMA);
                    csvFileContent.append(StandardConstants.STRING_DOUBLE_QUOTES);
                    csvFileContent.append(data.getSeriesList().get(j).getSeriesName());
                    csvFileContent.append(StandardConstants.STRING_DOUBLE_QUOTES);
                    csvFileContent.append(StandardConstants.STRING_COMMA);
                    if (isAmount) {
                        csvFileContent.append(WebCoreConstants.DOLLAR_SIGN);
                        csvFileContent.append(WebCoreUtil.formatToDollars(data.getSeriesList().get(j).getSeriesData().get(i)));
                    } else {
                        csvFileContent.append(data.getSeriesList().get(j).getSeriesData().get(i));
                    }
                    csvFileContent.append(StandardConstants.STRING_END_OF_LINE);
                }
            }
        } else if (widget.getWidgetTierTypeByWidgetTier1Type().getId().intValue() != WidgetConstants.TIER_TYPE_NA
                   && widget.getWidgetTierTypeByWidgetTier2Type().getId().intValue() != WidgetConstants.TIER_TYPE_NA
                   && widget.getWidgetTierTypeByWidgetTier3Type().getId().intValue() != WidgetConstants.TIER_TYPE_NA) {
            final int xSize = data.getTier1Names().size();
            final int seriesSize = data.getSeriesList().size();
            for (int i = 0; i < xSize; i++) {
                for (int j = 0; j < seriesSize; j++) {
                    csvFileContent.append(StandardConstants.STRING_DOUBLE_QUOTES);
                    csvFileContent.append(data.getTier1Names().get(i));
                    csvFileContent.append(StandardConstants.STRING_DOUBLE_QUOTES);
                    csvFileContent.append(StandardConstants.STRING_COMMA);
                    csvFileContent.append(StandardConstants.STRING_DOUBLE_QUOTES);
                    csvFileContent.append(data.getSeriesList().get(j).getStackName());
                    csvFileContent.append(StandardConstants.STRING_DOUBLE_QUOTES);
                    csvFileContent.append(StandardConstants.STRING_COMMA);
                    csvFileContent.append(StandardConstants.STRING_DOUBLE_QUOTES);
                    csvFileContent.append(data.getSeriesList().get(j).getSeriesName());
                    csvFileContent.append(StandardConstants.STRING_DOUBLE_QUOTES);
                    csvFileContent.append(StandardConstants.STRING_COMMA);
                    if (isAmount) {
                        csvFileContent.append(WebCoreConstants.DOLLAR_SIGN);
                        csvFileContent.append(WebCoreUtil.formatToDollars(data.getSeriesList().get(j).getSeriesData().get(i)));
                    } else {
                        csvFileContent.append(data.getSeriesList().get(j).getSeriesData().get(i));
                    }
                    csvFileContent.append(StandardConstants.STRING_END_OF_LINE);
                }
            }
            
        } else {
            if (isAmount) {
                csvFileContent.append(WebCoreConstants.DOLLAR_SIGN);
                csvFileContent.append(WebCoreUtil.formatToDollars(data.getSingleValue()));
            } else {
                csvFileContent.append(data.getSingleValue());
            }
            csvFileContent.append(StandardConstants.STRING_END_OF_LINE);
        }
        
        return csvFileContent.toString().getBytes();
    }
    
    private String getTierType(final Integer tierId) {
        
        if (tierId == null) {
            return null;
        }
        
        final StringBuilder str = new StringBuilder();
        switch (tierId) {
            case WidgetConstants.TIER_TYPE_NA:
                str.append(WebCoreConstants.EMPTY_STRING);
                break;
            case WidgetConstants.TIER_TYPE_HOUR:
                str.append(this.commonControllerHelper.getMessage("label.csv.column.name.hour")).append(StandardConstants.STRING_COMMA);
                break;
            case WidgetConstants.TIER_TYPE_DAY:
                str.append(this.commonControllerHelper.getMessage("label.csv.column.name.day")).append(StandardConstants.STRING_COMMA);
                break;
            case WidgetConstants.TIER_TYPE_MONTH:
                str.append(this.commonControllerHelper.getMessage("label.csv.column.name.month")).append(StandardConstants.STRING_COMMA);
                break;
            case WidgetConstants.TIER_TYPE_ALL_ORG:
            case WidgetConstants.TIER_TYPE_ORG:
                str.append(this.commonControllerHelper.getMessage("label.csv.column.name.organization")).append(StandardConstants.STRING_COMMA);
                break;
            case WidgetConstants.TIER_TYPE_PARENT_LOCATION:
            case WidgetConstants.TIER_TYPE_LOCATION:
                str.append(this.commonControllerHelper.getMessage("label.csv.column.name.location")).append(StandardConstants.STRING_COMMA);
                break;
            case WidgetConstants.TIER_TYPE_ROUTE:
                str.append(this.commonControllerHelper.getMessage("label.csv.column.name.route")).append(StandardConstants.STRING_COMMA);
                break;
            case WidgetConstants.TIER_TYPE_PAYSTATION:
                str.append(this.commonControllerHelper.getMessage("label.csv.column.name.payStation")).append(StandardConstants.STRING_COMMA);
                break;
            case WidgetConstants.TIER_TYPE_RATE:
                str.append(this.commonControllerHelper.getMessage("label.csv.column.name.rate")).append(StandardConstants.STRING_COMMA);
                break;
            case WidgetConstants.TIER_TYPE_MERCHANT_ACCOUNT:
                str.append(this.commonControllerHelper.getMessage("label.csv.column.name.merchantAccount")).append(StandardConstants.STRING_COMMA);
                break;
            case WidgetConstants.TIER_TYPE_REVENUE_TYPE:
                str.append(this.commonControllerHelper.getMessage("label.csv.column.name.revenueType")).append(StandardConstants.STRING_COMMA);
                break;
            case WidgetConstants.TIER_TYPE_TRANSACTION_TYPE:
                str.append(this.commonControllerHelper.getMessage("label.csv.column.name.transactionType")).append(StandardConstants.STRING_COMMA);
                break;
            case WidgetConstants.TIER_TYPE_COLLECTION_TYPE:
                str.append(this.commonControllerHelper.getMessage("label.csv.column.name.collectionType")).append(StandardConstants.STRING_COMMA);
                break;
            case WidgetConstants.TIER_TYPE_ALERT_TYPE:
                str.append(this.commonControllerHelper.getMessage("label.csv.column.name.alertType")).append(StandardConstants.STRING_COMMA);
                break;
            case WidgetConstants.TIER_TYPE_CARD_PROCESS_METHOD_TYPE:
                str.append(this.commonControllerHelper.getMessage("label.csv.column.name.cardProcessMethodType"))
                        .append(StandardConstants.STRING_COMMA);
                break;
            case WidgetConstants.TIER_TYPE_CITATION_TYPE:
                str.append(this.commonControllerHelper.getMessage("label.csv.column.name.citationType")).append(StandardConstants.STRING_COMMA);
                break;
            
            default:
                str.append(WebCoreConstants.EMPTY_STRING);
                break;
        }
        return str.toString();
    }
    
    // Looks in tabtreemap for the passed widgetRandomId, returns null if not
    // found.
    private Widget findWidgetFromMapIfExists(final HttpServletRequest request, final String widgetRandomId) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        @SuppressWarnings("unchecked")
        final List<TabHeading> tabHeadings = (List<TabHeading>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_HEADINGS);
        @SuppressWarnings("unchecked")
        final Map<String, Tab> tabTreeMap = (Map<String, Tab>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        
        String tabRandomId = null;
        Tab tab = null;
        if (tabHeadings != null) {
            for (TabHeading tabHeading : tabHeadings) {
                if (tabHeading.getCurrent().equals(WebCoreConstants.RESPONSE_TRUE)) {
                    tabRandomId = tabHeading.getId();
                    break;
                }
            }
            if (tabTreeMap != null) {
                tab = tabTreeMap.get(tabRandomId);
                if (tab == null) {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
        for (Section sec : tab.getSections()) {
            if (sec.getWidget(widgetRandomId) != null) {
                return sec.getWidget(widgetRandomId);
            }
        }
        return null;
    }
}
