package com.digitalpaytech.mvc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.digitalpaytech.domain.Section;
import com.digitalpaytech.domain.Tab;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.dto.TabHeading;
import com.digitalpaytech.service.DashboardService;
import com.digitalpaytech.util.DashboardUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.WidgetConstants;

@Controller
public class CommonLandingController {
	
	@Autowired
	protected DashboardService dashboardService;
	
	protected void setDashboardService(DashboardService dashboardService) {
		this.dashboardService = dashboardService;
	}

	protected List<TabHeading> getTabs(HttpServletRequest request, int id) {

		Collection<Tab> tabCollection = dashboardService.findTabsByUserAccountId(id);
		
		if( tabCollection == null || tabCollection.size() == 0 ){
			// User has no tabs, retrieve default. For parent admin there is no Parker Metrics tab.
		    tabCollection = dashboardService.findDefaultTabs(WebSecurityUtil.getUserAccount().getCustomer().isIsParent());
		}
		
		SessionTool sessionTool = SessionTool.getInstance(request);
		RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
		
		List<Tab> tabList = new ArrayList<Tab>(tabCollection);
		Collections.sort(tabList, new Comparator<Tab>(){
			public int compare(Tab t1, Tab t2) {
				int diff = t1.getOrderNumber() - t2.getOrderNumber();				
				return diff;
			}			
		});
		
		List<TabHeading> tabHeadings = new ArrayList<TabHeading>();
		
		int currentTabIdx = DashboardUtil.getCurrentTabFromCookie(request);
		
		boolean cookieFlag = false;
		Tab tab;
		int index = 0;
		Iterator<Tab> tabIter = tabList.iterator();		
		while (tabIter.hasNext()) {
			tab = tabIter.next();			
			setTabRandomId(tab, keyMapping);
			
			TabHeading tabHeader = new TabHeading(tab);
			if(index == currentTabIdx) {
				tabHeader.setCurrent(Boolean.TRUE.toString());
				cookieFlag = true;
			}
			
			tabHeadings.add(tabHeader);
			index++;
		}
		
		// Set first tab as the default 'current' tab if there is no 'current' in the cookie.
		if (!cookieFlag) {
			tabHeadings.get(0).setCurrent(Boolean.TRUE.toString());
		}

		sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, tabHeadings);
		
		return tabHeadings;
	}
	
	private Tab setTabRandomId(Tab tab, RandomKeyMapping keyMapping) {
		if (tab.getId() == null || tab.getId().intValue() == 0 || tab.getId().intValue() == -1) {
			tab.setRandomId(DashboardUtil.getTemporaryId(WebCoreConstants.TEMP_TAB_ID_PREFIX));
			tab.setRandomId(keyMapping.getRandomString(tab, WebCoreConstants.RANDOM_ID_LOOK_UP_NAME));

		} else {
			tab.setRandomId(keyMapping.getRandomString(tab, WebCoreConstants.ID_LOOK_UP_NAME));
		}
		return tab;
	}
	
	
	/**
	 * Loads tab tree into Map if it doesn't exist.
	 * @param request HttpServletRequest with Map<String, Tab> in the session.
	 * @param tabId String random Tab id.
	 * @return Map<String, Tab> Map with requested Tab object in Map.
	 */
	Map<String, Tab> loadTabTreeToMapIfNecessary(HttpServletRequest request, String tabId) {
		SessionTool sessionTool = SessionTool.getInstance(request);
		
		@SuppressWarnings("unchecked")
		Map<String, Tab> tabTreeMap = (Map<String, Tab>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);		
		if (!tabTreeMap.containsKey(tabId)) {				
			RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
			int id = Integer.parseInt(keyMapping.getKey(tabId).toString());
			
			Tab tab = dashboardService.findTabByIdEager(id);
			// Generates randomId for Tab, Sections and Widgets.
			tab = DashboardUtil.setRandomIds(request, tab);
			tabTreeMap.put(tab.getRandomId(), tab);
		}
		return tabTreeMap;
	}
	

    /**
     * Prevents the widgets which are assigned widgetListType = 1 (Master List) or widgetListType = 4 (Parent Master List) from being returned to the front end.
     * 
     * @param request
     * @param tab
     * @param parentFlag return true if Customer 'isIsParent' is true.
     * @return tab with master list widgets removed
     */
    protected Tab removeMasterList(HttpServletRequest request, Tab tab, boolean parentFlag) {
        
        Collection<Widget> toRemove = new LinkedList<Widget>();
        List<Section> newSectionList = new LinkedList<Section>();
        
        for (Section sec : tab.getSections()) {
            for (Widget wid : sec.getWidgets()) {
                // Remove both WidgetListType = Master List or Parent Master List.
                if (wid.getWidgetListType().getId() == WidgetConstants.LIST_TYPE_MASTER
                    || wid.getWidgetListType().getId() == WidgetConstants.LIST_TYPE_PARENT_MASTER) {
                    toRemove.add(wid);
                } else if (needToRemoveForParentOrChildDashboard(parentFlag, wid)) {
                    toRemove.add(wid);
                }
            }
            for (Widget wid : toRemove) {
                sec.deleteWidget(wid.getRandomId());
            }
            toRemove = new LinkedList<Widget>();
            newSectionList.add(sec);
        }
        tab.setSections(newSectionList);
        
        return tab;
    }

    /**
     * Remove default widgets for Parents for child customers and vice versa 
     *  - parentFlag = true and widgetListTypeId == WidgetConstants.LIST_TYPE_DEFAULT (2)
     *  - parentFlag = false and widgetListTypeId == WidgetConstants.LIST_TYPE_DEFAULT_PARENT (5)
     * @param parentFlag boolean Customer object 'isIsParent' boolean value
     * @param widget Widget object from the database.
     * @return boolean return true if do not want to show in dashboard.
     */
    private boolean needToRemoveForParentOrChildDashboard(boolean parentFlag, Widget widget) {
        if (parentFlag && widget.getWidgetListType().getId() == WidgetConstants.LIST_TYPE_DEFAULT) {
            return true;
        } else if (!parentFlag && widget.getWidgetListType().getId() == WidgetConstants.LIST_TYPE_DEFAULT_PARENT) { 
            return true;
        }
        return false;
    }

}
