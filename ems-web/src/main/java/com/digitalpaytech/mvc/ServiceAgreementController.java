package com.digitalpaytech.mvc;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.util.WebUtils;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.ActivityLogin;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAgreement;
import com.digitalpaytech.domain.LoginResultType;
import com.digitalpaytech.domain.ServiceAgreement;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.exception.ServiceAgreementException;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.ServiceAgreementForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.ActivityLoginService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.ServiceAgreementService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.ServiceAgreementValidator;

@Controller
@SessionAttributes({ WebSecurityConstants.WEB_SECURITY_FORM })
public class ServiceAgreementController {
    
    private static final String REDIRECT_PDA = "redirect:/pda/";
    private static final String REDIRECT_PDA_MAIN_HTML = "redirect:/pda/main.html";
    private static final String PASSWORD_EXPIRED = "PASSWORD_EXPIRED";
    private static final String NO_PARENT_CUSTOMER_AGREEMENT = "NO_PARENT_CUSTOMER_AGREEMENT";
    private static final String NO_PARENT_CUSTOMER_FOUND = "NO_PARENT_CUSTOMER_FOUND";
    private static final String REDIRECT_HOME = "redirect:/";
    private static final String REDIRECT_SECURE_DASHBOARD_INDEX_HTML = "redirect:/secure/dashboard/index.html";
    private static final String REDIRECT_SYSTEM_ADMIN_INDEX_HTML = "redirect:/systemAdmin/index.html";
    private static final String REDIRECT_SECURE_PASSWORD_HTML = "redirect:/secure/password.html";
    private static final String TERMS = "terms";
    private static final String LATEST_SERVICE_AGREEMENT = "latestServiceAgreement";
    private static final String AGREEMENT_CONTENT = "agreementContent";
    
    private static final Logger LOG = Logger.getLogger(ServiceAgreementController.class);
    
    @Autowired
    private ServiceAgreementService serviceAgreementService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private ServiceAgreementValidator serviceAgreementValidator;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private UserAccountService userAccountService;
    
    @Autowired
    private ActivityLoginService activityLoginService;
    
    public final void setServiceAgreementService(final ServiceAgreementService serviceAgreementService) {
        this.serviceAgreementService = serviceAgreementService;
    }
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final void setServiceAgreementValidator(final ServiceAgreementValidator serviceAgreementValidator) {
        this.serviceAgreementValidator = serviceAgreementValidator;
    }
    
    public final void setMailerService(final MailerService mailerService) {
        this.mailerService = mailerService;
    }
    
    public final void setUserAccountService(final UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }
    
    public final void setActivityLoginService(final ActivityLoginService activityLoginService) {
        this.activityLoginService = activityLoginService;
    }
    
    //TODO Remove debug messages after fixing stuck login problem
    @RequestMapping(value = "/secure/terms.html")
    public final String showServiceAgreement(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model)
        throws IOException {
        
        boolean shouldChangePassword = false;
        boolean shouldSignLA = false;
        String reloginError = null;
        /* retrieve user information. */
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final WebUser user = (WebUser) authentication.getPrincipal();
        
        Customer parentCustomer = null;
        boolean amIParentCustomer = false;
        
        final UserAccount userAccount = userAccountService.findUserAccount(user.getUserAccountId());
        
        /* retrieve the parent customer. */
        /* Determine User's Status */
        if (userAccount.getCustomer().getParentCustomer() == null) {
            parentCustomer = userAccount.getCustomer();
            amIParentCustomer = true;
        } else {
            parentCustomer = this.customerService.findCustomer(userAccount.getCustomer().getParentCustomer().getId());
            
        }
        
        if (parentCustomer == null) {
            reloginError = NO_PARENT_CUSTOMER_FOUND;
        } else if (parentCustomer.getCustomerAgreements().size() <= 0) {
            shouldSignLA = amIParentCustomer;
            if (!shouldSignLA) {
                reloginError = NO_PARENT_CUSTOMER_AGREEMENT;
            }
        } else {
            shouldChangePassword = (!user.isComplexPassword()) || (userAccount.isIsPasswordTemporary());
        }
        // for filter
        user.setIsAgreementSigned(!shouldSignLA);
        
        /* Process according to User's Status */
        if (user.isLoginViaPDA()) {
            if (shouldSignLA) {
                reloginError = NO_PARENT_CUSTOMER_AGREEMENT;
            } else if (shouldChangePassword) {
                reloginError = PASSWORD_EXPIRED;
            }
            
            if (reloginError == null) {
                return REDIRECT_PDA_MAIN_HTML;
            } else {
                final HttpSession session = request.getSession(false);
                if (session != null) {
                    session.invalidate();
                }
                
                request.getSession();
                WebUtils.setSessionAttribute(request, WebAttributes.AUTHENTICATION_EXCEPTION, new ServiceAgreementException(reloginError));
                return REDIRECT_PDA;
            }
        } else {
            if (reloginError != null) {
                final HttpSession session = request.getSession(false);
                if (session != null) {
                    session.invalidate();
                }
                
                request.getSession();
                WebUtils.setSessionAttribute(request, WebAttributes.AUTHENTICATION_EXCEPTION, new ServiceAgreementException(reloginError));
                return REDIRECT_HOME;
            } else if (shouldSignLA) {
                setLatestServiceAgreement(request, user, model);
                return TERMS;
            } else if (shouldChangePassword) {
                return REDIRECT_SECURE_PASSWORD_HTML;
            } else if (user.isSystemAdmin()) {
                return REDIRECT_SYSTEM_ADMIN_INDEX_HTML;
            } else {
                return REDIRECT_SECURE_DASHBOARD_INDEX_HTML;
            }
        }
    }
    
    @RequestMapping(value = "/secure/processAgreement.html", method = RequestMethod.POST)
    public final String processServiceAgreement(
        @ModelAttribute(WebSecurityConstants.WEB_SECURITY_FORM) final WebSecurityForm<ServiceAgreementForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) throws IOException {
        serviceAgreementValidator.validate(webSecurityForm, result);
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final WebUser user = (WebUser) authentication.getPrincipal();
        
        final ServiceAgreement serviceAgreement = (ServiceAgreement) sessionTool.getAttribute(LATEST_SERVICE_AGREEMENT);
        
        if (serviceAgreement == null) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("serviceAgreementNotFound",
                    serviceAgreementValidator.getMessage("error.termsOfService.sign.unsuccessful")));
        }
        
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0 || serviceAgreement == null) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            setLatestServiceAgreement(request, user, model);
            model.put("errorStatus", webSecurityForm.getValidationErrorInfo().getErrorStatus());
            return TERMS;
        }
        
        webSecurityForm.resetToken();
        
        final ServiceAgreementForm form = webSecurityForm.getWrappedObject();
        
        // set customer agreement data.
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final CustomerAgreement customerAgreement = new CustomerAgreement();
        final Customer customer = new Customer();
        final ServiceAgreement agreement = new ServiceAgreement();
        customer.setId(user.getCustomerId());
        agreement.setId(serviceAgreement.getId());
        customerAgreement.setName(form.getName().trim());
        customerAgreement.setTitle(form.getTitle());
        customerAgreement.setOrganization(form.getOrganization());
        final Date currentTime = new Date();
        customerAgreement.setAgreedGmt(currentTime);
        customerAgreement.setIsOverride(false);
        customerAgreement.setLastModifiedGmt(currentTime);
        customerAgreement.setUserAccount(userAccount);
        customerAgreement.setCustomer(customer);
        customerAgreement.setServiceAgreement(agreement);
        
        this.customerService.saveCustomerAgreement(customerAgreement);
        sessionTool.removeAttribute(LATEST_SERVICE_AGREEMENT);
        
        /* send the email notification. */
        try {
            mailerService.sendServiceAgreementAccepted(customerAgreement, userAccount.getCustomer().getName());
        } catch (Exception e) {
            LOG.error("Failed to send the Service Agreement email notification.", e);
        }
        
        user.setIsAgreementSigned(true);
        
        if (user.isComplexPassword() && !userAccount.isIsPasswordTemporary()) {
            if (userAccount.getCustomer().getCustomerType().getId() == WebCoreConstants.CUSTOMER_TYPE_DPT) {
                return REDIRECT_SYSTEM_ADMIN_INDEX_HTML;
            } else {
                return REDIRECT_SECURE_DASHBOARD_INDEX_HTML;
            }
        } else {
            return REDIRECT_SECURE_PASSWORD_HTML;
        }
    }
    
    @RequestMapping(value = "/secure/serviceAgreementDecline.html")
    public final String serviceAgreementDecline(final HttpServletRequest request, final HttpServletResponse response) {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final LogoutHandler logoutHandler = new SecurityContextLogoutHandler();
        saveActivityLog(((WebUser) authentication.getPrincipal()).getUserAccountId(), false);
        logoutHandler.logout(request, response, authentication);
        
        return "redirect:http://digitalpaytech.com";
    }
    
    private void setLatestServiceAgreement(final HttpServletRequest request, final WebUser user, final ModelMap model) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        final ServiceAgreement latestServiceAgreement = this.serviceAgreementService.findLatestServiceAgreement();
        model.put(AGREEMENT_CONTENT, new String(latestServiceAgreement.getContent()));
        model.put(WebSecurityConstants.WEB_SECURITY_FORM, createForm(user));
        sessionTool.setAttribute(LATEST_SERVICE_AGREEMENT, latestServiceAgreement);
    }
    
    private WebSecurityForm<ServiceAgreementForm> createForm(final WebUser user) {
        final String primaryKey = null;
        
        final ServiceAgreementForm serviceAgreementForm = new ServiceAgreementForm();
        serviceAgreementForm.setCustomerId(user.getCustomerId());
        final WebSecurityForm<ServiceAgreementForm> webSecurityForm = new WebSecurityForm<ServiceAgreementForm>(primaryKey, serviceAgreementForm);
        // Default set as create action
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        
        return webSecurityForm;
    }
    
    private void saveActivityLog(final Integer userAccountId, final boolean isSessionTimeout) {
        
        final ActivityLogin activityLogin = new ActivityLogin();
        final LoginResultType loginResultType = new LoginResultType();
        final UserAccount userAccount = new UserAccount();
        userAccount.setId(userAccountId);
        loginResultType
                .setId(isSessionTimeout ? WebSecurityConstants.LOGIN_RESULT_TYPE_TIMEOUT_LOGOUT : WebSecurityConstants.LOGIN_RESULT_TYPE_USER_LOGOUT);
        activityLogin.setLoginResultType(loginResultType);
        activityLogin.setUserAccount(userAccount);
        activityLogin.setActivityGmt(DateUtil.getCurrentGmtDate());
        
        this.activityLoginService.saveActivityLogin(activityLogin);
    }
}
