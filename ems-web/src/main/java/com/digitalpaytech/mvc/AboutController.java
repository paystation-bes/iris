package com.digitalpaytech.mvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class AboutController {

    @RequestMapping(value = "/secure/about.html")
    public final String processAbout(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        return "/about";
    }
}
