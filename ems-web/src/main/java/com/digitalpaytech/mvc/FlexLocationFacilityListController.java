package com.digitalpaytech.mvc;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.digitalpaytech.domain.FlexLocationFacility;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.dto.FlexLocationFacilitySearchCriteria;
import com.digitalpaytech.mvc.support.LocationIntegrationSearchForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.FlexWSService;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.support.WebObjectId;

@Controller
public class FlexLocationFacilityListController {
    
    protected static final String FORM_SEARCH = "flexLocationFacilitySearchForm";
    
    @Autowired
    protected FlexWSService flexWSService;
    
    @Autowired
    protected CustomerService customerService;
    
    protected FlexLocationFacilitySearchCriteria populateFlexLocationFacilitySearchCriteria(HttpServletRequest request, LocationIntegrationSearchForm form, Integer customerId,
        boolean isSystemAdmin) {
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        FlexLocationFacilitySearchCriteria criteria = new FlexLocationFacilitySearchCriteria();
        criteria.setCustomerId(customerId);
        criteria.setLocationId(keyMapping.getKey(form.getLocationRandomId(), Location.class, Integer.class));
        criteria.setFlexLocationFacilityId(keyMapping.getKey(form.getItemId(), FlexLocationFacility.class, Integer.class));
        
        WebObjectId selectedId = keyMapping.getKeyObject(form.getSelectedId());
        if (selectedId != null) {
            if (Location.class.isAssignableFrom(selectedId.getObjectType())) {
                criteria.setSelectedLocationId((Integer) selectedId.getId());
            } else {
                throw new RuntimeException("UnExpected Object Type for Location Flex Facility selector: " + selectedId.getObjectType());
            }
        }
        
        if (StringUtils.isNotBlank(form.getLocationRandomId()) && (criteria.getLocationId() == null)) {
            throw new RuntimeException("Invalid Location Random ID for Customer: " + customerId);
        }
        if (StringUtils.isNotBlank(form.getItemId()) && (criteria.getFlexLocationFacilityId() == null)) {
            throw new RuntimeException("Invalid FlexLocationFacilty Random ID for Customer: " + customerId);
        }
        
        if (form.getPage() != null) {
            criteria.setPage(form.getPage());
        }
        
        criteria.setItemsPerPage(WebCoreConstants.FLEX_LOCATION_FACILITIES_PER_PAGE);
        
        if ((form.getDataKey() != null) && (form.getDataKey().length() > 0)) {
            try {
                criteria.setMaxUpdatedTime(new Date(Long.parseLong(form.getDataKey())));
            } catch (NumberFormatException nfe) {
                // DO NOTHING
            }
        }
        
        if (criteria.getMaxUpdatedTime() == null) {
            criteria.setMaxUpdatedTime(new Date());
        }        
        
        
        return criteria;
    }
    
    @ModelAttribute(FORM_SEARCH)
    public WebSecurityForm<LocationIntegrationSearchForm> initPosSearchForm(HttpServletRequest request) {
        return new WebSecurityForm<LocationIntegrationSearchForm>(null, new LocationIntegrationSearchForm());
    }        
}
