package com.digitalpaytech.mvc.pda;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PaystationSetting;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.paystation.LicencePlateInfo;
import com.digitalpaytech.dto.paystation.SpaceInfo;
import com.digitalpaytech.mvc.pda.support.PDASearchForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.PaystationSettingService;
import com.digitalpaytech.service.paystation.LicencePlateInfoService;
import com.digitalpaytech.service.paystation.SpaceInfoService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.pda.PDASearchValidator;

@Controller
public class PDASearchController {
    public static final String FORM_SEARCH = "pdaSearchEditForm";
    public static final String SPACE_INFO_LIST = "spaceInfoList";
    public static final String DISPLAY_VALUE = "displayValue";
    public static final String SEARCH_MAIN_PDA = "/pda/searchMainPDA";
    public static final String PLATE_SEARCH_PDA = "/pda/plateSearchPDA";
    
    @Autowired
    private LocationService locationService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private SpaceInfoService spaceInfoService;
    
    @Autowired
    private LicencePlateInfoService licencePlateInfoService;
    
    @Autowired
    private PaystationSettingService paystationSettingService;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    @Autowired
    private PDASearchValidator pdaSearchValidator;
    
    @ModelAttribute(FORM_SEARCH)
    public final WebSecurityForm<PDASearchForm> prepareForm(final HttpServletRequest request) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final PDASearchForm searchForm = new PDASearchForm();
        
        final int searchType = Integer.parseInt(request.getParameter("searchType"));
        searchForm.setSearchType(searchType);
        
        searchForm.setLocations(this.locationService.findChildLocationsByCustomerId(userAccount.getCustomer().getId()));
        if (searchForm.getLocations() != null) {
            for (Location loc : searchForm.getLocations()) {
                loc.setRandomId(keyMapping.getRandomString(loc, WebCoreConstants.ID_LOOK_UP_NAME));
            }
        }
        
        searchForm.setPaystationSettingNames(this.getPayStationSettingNames(userAccount.getCustomer().getId()));
        
        return new WebSecurityForm<PDASearchForm>(null, searchForm, SessionTool.getInstance(request).locatePostToken(FORM_SEARCH));
    }
    
    @RequestMapping(value = "/pda/searchForm.html", method = RequestMethod.GET)
    public final String initStallForm(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_SEARCH) final WebSecurityForm<PDASearchForm> secureForm) {
        /* validate if login user has enough permission. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_PDA_ACCESS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        model.put(FORM_SEARCH, secureForm);
        return SEARCH_MAIN_PDA;
    }
    
    /**
     * This method shows search results for parking spaces. It retrieves space info
     * based on either location, paystation settings or customer id.
     * 
     * @param webSecurityForm
     *            WebSecurityForm that contains most of the parameters that drive the search
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return vm URL (/pda/searchMainPDA.vm)
     */
    @SuppressWarnings("deprecation")
    @RequestMapping(value = "/pda/searchStall.html", method = RequestMethod.POST)
    public final String showStallQueryResults(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<PDASearchForm> webSecurityForm,
        final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate if login user has enough permission. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_PDA_ACCESS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final Integer customerId = userAccount.getCustomer().getId();
        
        final PDASearchForm searchForm = webSecurityForm.getWrappedObject();
        
        List<SpaceInfo> spaceInfoList = new ArrayList<SpaceInfo>();
        String displayValue = null;
        
        this.pdaSearchValidator.validate(webSecurityForm, keyMapping, true, false);
        
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
        } else {
            webSecurityForm.resetToken();
            
            final int querySpaceBy = getQuerySpaceBy(customerId, -1);
            final Integer locationId = (Integer) keyMapping.getKey(searchForm.getLocationRandomId());
            final Date currentDate = DateUtil.getCurrentGmtDate();
            switch (searchForm.getSearchType()) {
                case WebCoreConstants.SEARCH_BY_VALID_STALL:
                    if (WebCoreConstants.LOCATION.equals(searchForm.getSearchBy())) {
                        if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_LATEST_EXPIRY_TIME) {
                            spaceInfoList.add(this.spaceInfoService
                                    .getValidByLocationPurchaseExpirySpaceNumberOrderByLatestExpiry(locationId, searchForm.getStartStallAsNumber(),
                                                                                                    currentDate, customerId));
                        } else {
                            spaceInfoList.add(this.spaceInfoService.getValidByLocationPurchaseExpirySpaceNumber(locationId,
                                                                                                                searchForm.getStartStallAsNumber(),
                                                                                                                currentDate, customerId));
                        }
                        displayValue = this.locationService.findLocationById(locationId).getName();
                        
                    } else if (WebCoreConstants.PAYSTATION.equals(searchForm.getSearchBy())) {
                        spaceInfoList.add(this.spaceInfoService.getValidByPaystationSettingPurchaseExpirySpaceNumber(searchForm
                                .getPaystationSettingName(), searchForm.getStartStallAsNumber(), currentDate, customerId));
                        displayValue = this.customerService.findCustomer(customerId).getName();
                        
                    } else if (WebCoreConstants.CUSTOMER.equals(searchForm.getSearchBy())) {
                        if (querySpaceBy == PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME) {
                            spaceInfoList.add(this.spaceInfoService.getValidByCustomerPurchaseExpirySpaceNumberOrderByLatestExpiry(searchForm
                                    .getStartStallAsNumber(), currentDate, customerId));
                        } else {
                            spaceInfoList.add(this.spaceInfoService.getValidByCustomerPurchaseExpirySpaceNumber(searchForm.getStartStallAsNumber(),
                                                                                                                currentDate, customerId));
                        }
                        displayValue = this.customerService.findCustomer(customerId).getName();
                    }
                    break;
                case WebCoreConstants.SEARCH_BY_VALID_STALL_RANGE:
                    if (WebCoreConstants.LOCATION.equals(searchForm.getSearchBy())) {
                        spaceInfoList = this.spaceInfoService.getSpaceInfoListByLocationAndSpaceRangeForPosDate(customerId, locationId,
                                                                                                                searchForm.getStartStallAsNumber(),
                                                                                                                searchForm.getEndStallAsNumber(),
                                                                                                                PaystationConstants.STALL_TYPE_VALID,
                                                                                                                currentDate, true);
                        displayValue = this.locationService.findLocationById(locationId).getName();
                    } else if (WebCoreConstants.PAYSTATION.equals(searchForm.getSearchBy())) {
                        spaceInfoList = this.spaceInfoService
                                .getSpaceInfoListByPaystationSettingAndSpaceRangeForPosDate(customerId, searchForm.getPaystationSettingName(),
                                                                                            searchForm.getStartStallAsNumber(),
                                                                                            searchForm.getEndStallAsNumber(),
                                                                                            PaystationConstants.STALL_TYPE_VALID, currentDate, true);
                        displayValue = this.customerService.findCustomer(customerId).getName();
                    } else if (WebCoreConstants.CUSTOMER.equals(searchForm.getSearchBy())) {
                        spaceInfoList = this.spaceInfoService.getSpaceInfoListByCustomerAndSpaceRangeForPosDate(customerId,
                                                                                                                searchForm.getStartStallAsNumber(),
                                                                                                                searchForm.getEndStallAsNumber(),
                                                                                                                PaystationConstants.STALL_TYPE_VALID,
                                                                                                                currentDate, true);
                        displayValue = this.customerService.findCustomer(customerId).getName();
                    }
                    break;
                case WebCoreConstants.SEARCH_BY_EXPIRED_STALL:
                    if (WebCoreConstants.LOCATION.equals(searchForm.getSearchBy())) {
                        spaceInfoList = this.spaceInfoService
                                .getSpaceInfoListByLocationAndSpaceRangeForPosDate(customerId, locationId, searchForm.getStartStallAsNumber(),
                                                                                   searchForm.getStartStallAsNumber(),
                                                                                   PaystationConstants.STALL_TYPE_EXPIRED, currentDate, true);
                        displayValue = this.locationService.findLocationById(locationId).getName();
                    } else if (WebCoreConstants.PAYSTATION.equals(searchForm.getSearchBy())) {
                        spaceInfoList = this.spaceInfoService
                                .getSpaceInfoListByPaystationSettingAndSpaceRangeForPosDate(customerId, searchForm.getPaystationSettingName(),
                                                                                            searchForm.getStartStallAsNumber(),
                                                                                            searchForm.getStartStallAsNumber(),
                                                                                            PaystationConstants.STALL_TYPE_EXPIRED, currentDate, true);
                        displayValue = this.customerService.findCustomer(customerId).getName();
                    } else if (WebCoreConstants.CUSTOMER.equals(searchForm.getSearchBy())) {
                        spaceInfoList = this.spaceInfoService
                                .getSpaceInfoListByCustomerAndSpaceRangeForPosDate(customerId, searchForm.getStartStallAsNumber(),
                                                                                   searchForm.getStartStallAsNumber(),
                                                                                   PaystationConstants.STALL_TYPE_EXPIRED, currentDate, true);
                        displayValue = this.customerService.findCustomer(customerId).getName();
                    }
                    break;
                case WebCoreConstants.SEARCH_BY_EXPIRED_STALL_RANGE:
                    if (WebCoreConstants.LOCATION.equals(searchForm.getSearchBy())) {
                        spaceInfoList = this.spaceInfoService
                                .getSpaceInfoListByLocationAndSpaceRangeForPosDatePDA(customerId, locationId, searchForm.getStartStallAsNumber(),
                                                                                      searchForm.getEndStallAsNumber(),
                                                                                      PaystationConstants.STALL_TYPE_EXPIRED, currentDate, true);
                        displayValue = this.locationService.findLocationById(locationId).getName();
                    } else if (WebCoreConstants.PAYSTATION.equals(searchForm.getSearchBy())) {
                        spaceInfoList = this.spaceInfoService
                                .getSpaceInfoListByPaystationSettingAndSpaceRangeForPosDatePDA(customerId, searchForm.getPaystationSettingName(),
                                                                                               searchForm.getStartStallAsNumber(),
                                                                                               searchForm.getEndStallAsNumber(),
                                                                                               PaystationConstants.STALL_TYPE_EXPIRED, currentDate,
                                                                                               true);
                        displayValue = this.customerService.findCustomer(customerId).getName();
                    } else if (WebCoreConstants.CUSTOMER.equals(searchForm.getSearchBy())) {
                        spaceInfoList = this.spaceInfoService
                                .getSpaceInfoListByCustomerAndSpaceRangeForPosDatePDA(customerId, searchForm.getStartStallAsNumber(),
                                                                                      searchForm.getEndStallAsNumber(),
                                                                                      PaystationConstants.STALL_TYPE_EXPIRED, currentDate, true);
                        displayValue = this.customerService.findCustomer(customerId).getName();
                    }
                    break;
                default:
                    break;
            }
        }
        
        final SimpleDateFormat format = new SimpleDateFormat(WebCoreConstants.DATETIME_FORMAT);
        for (SpaceInfo spi : spaceInfoList) {
            if (spi != null) {
                spi.setStartDateLocal(format.format(DateUtil.changeTimeZone(spi.getStartDate(), spi.getTimeZone())));
                spi.setEndDateLocal(format.format(DateUtil.changeTimeZone(spi.getEndDate(), spi.getTimeZone())));
            }
        }
        
        model.put(SPACE_INFO_LIST, spaceInfoList);
        model.put(DISPLAY_VALUE, displayValue);
        model.put(FORM_SEARCH, webSecurityForm);
        
        return SEARCH_MAIN_PDA;
    }
    
    @RequestMapping(value = "/pda/searchPlateForm.html", method = RequestMethod.GET)
    public final String initPlateForm(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_SEARCH) final WebSecurityForm<PDASearchForm> secureForm) {
        /* validate if login user has enough permission. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_PDA_ACCESS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        model.put(FORM_SEARCH, secureForm);
        return PLATE_SEARCH_PDA;
    }
    
    /**
     * This method shows search results for license plates. It retrieves space info
     * based on either location, paystation settings or customer id.
     * 
     * @param webSecurityForm
     *            WebSecurityForm that contains most of the parameters that drive the search
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return vm URL (/pda/plateSearchPDA.vm)
     */
    @RequestMapping(value = "/pda/searchPlate.html", method = RequestMethod.POST)
    public final String showPlateQueryResults(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<PDASearchForm> webSecurityForm,
        final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate if login user has enough permission. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_PDA_ACCESS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final Integer customerId = userAccount.getCustomer().getId();
        
        final PDASearchForm searchForm = webSecurityForm.getWrappedObject();
        
        this.pdaSearchValidator.validate(webSecurityForm, keyMapping, true, false);
        final Integer locationId = (Integer) keyMapping.getKey(searchForm.getLocationRandomId());
        List<LicencePlateInfo> licensePlateList = new ArrayList<LicencePlateInfo>();
        String displayValue = null;
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
        } else {
            final int querySpaceBy = getQuerySpaceBy(customerId, -1);
            switch (searchForm.getSearchType()) {
                case WebCoreConstants.SEARCH_BY_VALID_PLATE:
                    if (WebCoreConstants.LOCATION.equals(searchForm.getSearchBy())) {
                        licensePlateList = this.licencePlateInfoService.getValidPlateListByLocation(customerId, locationId);
                        displayValue = this.locationService.findLocationById(locationId).getName();
                    } else if (WebCoreConstants.CUSTOMER.equals(searchForm.getSearchBy())) {
                        licensePlateList = this.licencePlateInfoService.getValidPlateListByCustomer(customerId, querySpaceBy);
                        displayValue = this.customerService.findCustomer(customerId).getName();
                    }
                    break;
                case WebCoreConstants.SEARCH_BY_EXPIRED_PLATE:
                    if (WebCoreConstants.LOCATION.equals(searchForm.getSearchBy())) {
                        licensePlateList = this.licencePlateInfoService.getExpiredPlateListByLocationWithinXMinutes(customerId, locationId, Integer
                                .parseInt(searchForm.getExpiredMinutesBack()));
                        displayValue = this.locationService.findLocationById(locationId).getName();
                    } else if (WebCoreConstants.CUSTOMER.equals(searchForm.getSearchBy())) {
                        licensePlateList = this.licencePlateInfoService.getExpiredPlateListByCustomerWithinXMinutes(customerId, Integer
                                .parseInt(searchForm.getExpiredMinutesBack()));
                        displayValue = this.customerService.findCustomer(customerId).getName();
                    }
                    break;
                case WebCoreConstants.SEARCH_BY_PLATE:
                    if (WebCoreConstants.LOCATION.equals(searchForm.getSearchBy())) {
                        licensePlateList.add(this.licencePlateInfoService.getValidPlateByLocationAndPlate(customerId, searchForm.getLicensePlateNo(),
                                                                                                          locationId, querySpaceBy));
                        displayValue = this.locationService.findLocationById(locationId).getName();
                    } else if (WebCoreConstants.CUSTOMER.equals(searchForm.getSearchBy())) {
                        licensePlateList.add(this.licencePlateInfoService.getValidPlateByCustomerAndPlate(customerId, searchForm.getLicensePlateNo(),
                                                                                                          querySpaceBy));
                        displayValue = this.customerService.findCustomer(customerId).getName();
                    }
                    break;
                default:
                    break;
            }
        }
        
        final String timeZone = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE)
                .getPropertyValue();
        final SimpleDateFormat format = new SimpleDateFormat(WebCoreConstants.DATETIME_FORMAT);
        for (LicencePlateInfo lpi : licensePlateList) {
            if (lpi != null) {
                lpi.setPermitBeginLocal(format.format(DateUtil.changeTimeZone(lpi.getPermitBeginGmt(), timeZone)));
                lpi.setPermitExpireLocal(format.format(DateUtil.changeTimeZone(lpi.getPermitExpireGmt(), timeZone)));
            }
        }
        
        model.put(SPACE_INFO_LIST, licensePlateList);
        model.put(DISPLAY_VALUE, displayValue);
        model.put(FORM_SEARCH, webSecurityForm);
        
        return PLATE_SEARCH_PDA;
    }
    
    private List<String> getPayStationSettingNames(final Integer customerId) {
        final List<String> pssNames = new ArrayList<String>();
        for (PaystationSetting pss : this.paystationSettingService.findPaystationSettingsByCustomerId(customerId)) {
            pssNames.add(pss.getName());
        }
        return pssNames;
    }
    
    private int getQuerySpaceBy(final int customerId, final int defaultQuerySpaceById) {
        final CustomerProperty customerPropertyQuerySpacesBy = (CustomerProperty) this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_QUERY_SPACES_BY);
        if (customerPropertyQuerySpacesBy != null) {
            return Integer.parseInt(customerPropertyQuerySpacesBy.getPropertyValue());
        }
        return defaultQuerySpaceById;
    }
}
