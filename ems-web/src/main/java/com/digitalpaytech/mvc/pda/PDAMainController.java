package com.digitalpaytech.mvc.pda;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
public class PDAMainController {
	@RequestMapping(value = "/pda/main.html")
	public String showMenu(HttpServletRequest request, HttpServletResponse response) {
		String result = WebCoreConstants.RESPONSE_FALSE;
		if(canAccess(request, response)) {
			result = "/pda/main";
		}
		
		return result;
	}
	
	private boolean canAccess(HttpServletRequest request, HttpServletResponse response) {
		boolean result = true;
		if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_PDA_ACCESS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            result = false;
        }
		
		return result;
	}
}
