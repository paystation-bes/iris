package com.digitalpaytech.mvc.pda.support;

import java.io.Serializable;
import java.util.List;

import com.digitalpaytech.domain.Location;

public class PDASearchForm implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4178826682009912055L;

    private int searchType;    
    private Integer locationId;
    private String locationRandomId;     
    private String startStall;
    private String endStall;
    private Integer startStallAsNumber;
    private Integer endStallAsNumber;    
    private String paystationSettingName;
    private String expiredMinutesBack;
    private String licensePlateNo;
    private String searchBy;    
    private List<String> paystationSettingNames;
    private List<Location> locations;
    
    public int getSearchType() {
        return searchType;
    }

    public void setSearchType(Integer searchType) {
        this.searchType = searchType;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public String getLocationRandomId() {
        return locationRandomId;
    }

    public void setLocationRandomId(String locationRandomId) {
        this.locationRandomId = locationRandomId;
    }

    public String getStartStall() {
        return startStall;
    }

    public void setStartStall(String startStall) {
        this.startStall = startStall;
    }

    public String getEndStall() {
        return endStall;
    }

    public void setEndStall(String endStall) {
        this.endStall = endStall;
    }

    public String getSearchBy() {
        return searchBy;
    }

    public void setSearchBy(String searchBy) {
        this.searchBy = searchBy;
    }

    public String getPaystationSettingName() {
        return paystationSettingName;
    }

    public void setPaystationSettingName(String paystationSettingName) {
        this.paystationSettingName = paystationSettingName;
    }

    public String getExpiredMinutesBack() {
        return expiredMinutesBack;
    }

    public void setExpiredMinutesBack(String expiredMinutesBack) {
        this.expiredMinutesBack = expiredMinutesBack;
    }

    public String getLicensePlateNo() {
        return licensePlateNo;
    }

    public void setLicensePlateNo(String licensePlateNo) {
        this.licensePlateNo = licensePlateNo;
    }
    
    public List<String> getPaystationSettingNames() {
        return paystationSettingNames;
    }
    public void setPaystationSettingNames(List<String> paystationSettingNames) {
        this.paystationSettingNames = paystationSettingNames;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    public Integer getStartStallAsNumber() {
        return startStallAsNumber;
    }

    public void setStartStallAsNumber(Integer startStallAsNumber) {
        this.startStallAsNumber = startStallAsNumber;
    }

    public Integer getEndStallAsNumber() {
        return endStallAsNumber;
    }

    public void setEndStallAsNumber(Integer endStallAsNumber) {
        this.endStallAsNumber = endStallAsNumber;
    }    
    
}
