package com.digitalpaytech.mvc;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import com.digitalpaytech.mvc.support.ControllerStatus;
import com.digitalpaytech.util.TLSUtils;
import com.digitalpaytech.util.WebCoreConstants;

@Controller
public class HomeController implements ApplicationContextAware {
    
    private ApplicationContext ctx;
    
    @Autowired
    private TLSUtils tlsUtils;
    
    @RequestMapping(value = "/index.html")
    public Object showHome(final HttpServletRequest request, final HttpServletResponse response) {
        
        final Map<String, Object> model = new HashMap<String, Object>();
        prepareErrors(model, request, response);
        
        return this.tlsUtils.redirectIfBadHost(request, new ModelAndView("index", model));
    }
    
    @RequestMapping(value = "/pda/index.html")
    public ModelAndView showPDAHome(final HttpServletRequest request, final HttpServletResponse response) {
        final Map<String, Object> model = new HashMap<String, Object>();
        prepareErrors(model, request, response);
        
        return new ModelAndView("pda/index", model);
    }
    
    private void prepareErrors(final Map<String, Object> model, final HttpServletRequest request, final HttpServletResponse response) {
        final ControllerStatus status = this.ctx.getBean(ControllerStatus.class);
        
        AuthenticationException authenticationException =
                (AuthenticationException) WebUtils.getSessionAttribute(request, WebAttributes.AUTHENTICATION_EXCEPTION);
        
        final String loginAction = request.getParameter("error");
        status.clearErrors();
        
        if (StringUtils.isBlank(loginAction)) {
            if (authenticationException != null) {
                WebUtils.setSessionAttribute(request, WebAttributes.AUTHENTICATION_EXCEPTION, null);
            } else {
                authenticationException = null;
            }
        }
        
        if (authenticationException != null) {
            model.put("authenticationException", authenticationException.getMessage());
        } else {
            model.put("authenticationException", "");
        }
        
        boolean invalidSessFlag = false;
        final HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
            invalidSessFlag = true;
        }
        
        if (loginAction != null) {
            if (loginAction.equals(WebCoreConstants.ERROR_SESSIONINVALID)) {
                status.addError("error.session.invalid");
            } else if (loginAction.equals(WebCoreConstants.ERROR_INSUFFICIENTPERMISSION)) {
                status.addError("error.user.insufficientPermission");
            } else if (loginAction.equals(WebCoreConstants.ERROR_FEATURENOTENABLED)) {
                status.addError("error.user.featureNotEnabled");
            } else if (loginAction.equals(WebCoreConstants.ERROR_ACCESSDENIED)) {
                status.addError("error.session.accessDenied");
            } else {
                status.addError("error.common.undefined");
            }
            
        } else if (loginAction == null && session != null && invalidSessFlag) {
            /*
             * This situation happens when session has been invalidated and UI refreshes the page, a session will be created by SessionManager first
             * then invalidated again in this method.
             */
            status.addError("error.session.invalid");
        }
        
        model.put("status", status);
        
        response.addHeader("session.invalid", Boolean.TRUE.toString());
    }
    
    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
        this.ctx = applicationContext;
    }
}
