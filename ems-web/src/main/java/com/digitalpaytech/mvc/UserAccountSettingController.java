package com.digitalpaytech.mvc;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerEmail;
import com.digitalpaytech.domain.CustomerRole;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.domain.UserStatusType;
import com.digitalpaytech.dto.ChildCustomerInfo;
import com.digitalpaytech.dto.customeradmin.RoleSettingInfo;
import com.digitalpaytech.dto.customeradmin.RoleSettingInfoWrapper;
import com.digitalpaytech.dto.customeradmin.UserSettingDetails;
import com.digitalpaytech.dto.customeradmin.UserSettingInfo;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.UserAccountEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.ActivityLogService;
import com.digitalpaytech.service.CustomerRoleService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.RoleService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.util.DashboardUtil;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObject;
import com.digitalpaytech.validation.UserSettingValidator;

/**
 * This class handles the User setting request.
 * 
 * @author Brian Kim
 * 
 */
@Controller
public class UserAccountSettingController {
    
    private static Logger log = Logger.getLogger(UserAccountSettingController.class);
    
    @Autowired
    protected ActivityLogService activityLogService;
    
    @Autowired
    protected UserAccountService userAccountService;
    
    @Autowired
    protected CustomerRoleService customerRoleService;
    
    @Autowired
    protected CustomerService customerService;
    
    @Autowired
    protected EntityService entityService;
    
    @Autowired
    protected RoleService roleService;
    
    @Autowired
    private UserSettingValidator userSettingValidator;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    public final void setUserAccountService(final UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }
    
    public final void setCustomerRoleService(final CustomerRoleService customerRoleService) {
        this.customerRoleService = customerRoleService;
    }
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final void setEntityService(final EntityService entityService) {
        this.entityService = entityService;
    }
    
    public final void setActivityLogService(final ActivityLogService activityLogService) {
        this.activityLogService = activityLogService;
    }
    
    public final void setUserSettingValidator(final UserSettingValidator userSettingValidator) {
        this.userSettingValidator = userSettingValidator;
    }
    
    public final void setPasswordEncoder(final PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }
    
    public final void setRoleService(final RoleService roleService) {
        this.roleService = roleService;
    }
    
    /**
     * This method retrieves necessary data for input form from database and prepares for the form for adding,
     * editing user.
     * 
     * @param request
     *            HttpServletRequest object
     * @param userAccount
     *            UserAccount object
     * @param userSelectedRoleIdList
     *            current user's role id list
     * @return Collection of RoleSettingInfo object
     */
    private List<RoleSettingInfo> populateRoleInfo(final HttpServletRequest request, final Customer customer,
        final Collection<Integer> userSelectedRoleIdList, final Customer parentCustomer) {
        
        final Collection<WebObject> rolesObjList = findCustomerRolesByCustomerId(request, customer.getId());
        final List<RoleSettingInfo> roleSettingInfo = new ArrayList<RoleSettingInfo>();
        for (WebObject obj : rolesObjList) {
            final CustomerRole role = (CustomerRole) obj.getWrappedObject();
            final RoleSettingInfo roleSetting;
            if (userSelectedRoleIdList == null) {
                roleSetting = new RoleSettingInfo(obj.getPrimaryKey().toString(), role.getRole().getName(), StandardConstants.STRING_ACTIVE,
                        role.getRole().getCustomerType().getId(), role.getRole().getIsFromParent());
            } else {
                roleSetting = new RoleSettingInfo(obj.getPrimaryKey().toString(), role.getRole().getName(),
                        userSelectedRoleIdList.contains(role.getRole().getId()) ? StandardConstants.STRING_SELECTED : StandardConstants.STRING_ACTIVE,
                        role.getRole().getCustomerType().getId(), role.getRole().getIsFromParent());
            }
            if (parentCustomer != null) {
                if (role.getRole().getIsFromParent()) {
                    roleSetting.setCustomerName(parentCustomer.getName());
                } else {
                    roleSetting.setCustomerName(customer.getName());
                }
            }
            roleSettingInfo.add(roleSetting);
        }
        return roleSettingInfo;
    }
    
    /**
     * This method retrieves necessary data for input form from database and prepares for the form for adding
     * child customer info.
     * 
     * @param request
     *            HttpServletRequest object
     * @param userAccount
     *            UserAccount object
     * @param userSelectedRoleIdList
     *            current user's role id list
     * @return Collection of ChildCustomerInfo objects
     */
    private List<ChildCustomerInfo> populateChildCustomerInfo(final HttpServletRequest request, final Integer customerId,
        final Collection<Integer> userSelectedChildCustomerIdList, final Collection<UserAccount> userSelectedChildCustomerList) {
        
        final Collection<WebObject> childCustomerObjList = findChildCustomers(request, customerId);
        final List<ChildCustomerInfo> childCustomerInfo = new ArrayList<ChildCustomerInfo>();
        for (WebObject obj : childCustomerObjList) {
            final Customer customer = (Customer) obj.getWrappedObject();
            if (userSelectedChildCustomerIdList == null) {
                childCustomerInfo
                        .add(new ChildCustomerInfo(obj.getPrimaryKey().toString(), customer.getName(), StandardConstants.STRING_ACTIVE, false));
            } else {
                childCustomerInfo.add(new ChildCustomerInfo(
                        obj.getPrimaryKey().toString(), customer.getName(), userSelectedChildCustomerIdList.contains(customer.getId())
                                ? StandardConstants.STRING_SELECTED : StandardConstants.STRING_ACTIVE,
                        this.findIsDefault(userSelectedChildCustomerList, customer.getId())));
            }
        }
        
        return childCustomerInfo;
    }
    
    private boolean findIsDefault(final Collection<UserAccount> userSelectedChildCustomerList, final Integer customerId) {
        for (final UserAccount ua : userSelectedChildCustomerList) {
            if (ua.getCustomer().getId().equals(customerId)) {
                return ua.getIsDefaultAlias();
            }
        }
        return false;
    }
    
    /**
     * This method will retrieve customer's user account information.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @param urlOnSuccess
     *            URI when process is successful.
     * @return urlOnSuccess if the process succeeds, null if fails.
     */
    protected final String getUserListByCustomer(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        final String urlOnSuccess) {
        
        /* make an instance for user account form. */
        final WebSecurityForm<UserAccountEditForm> userAccountEditForm = new WebSecurityForm<UserAccountEditForm>(null, new UserAccountEditForm());
        
        /*
         * retrieve customer's user accounts and convert them into UserSettingList object wrapped with
         * WebObject.
         */
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final Customer customer = userAccount.getCustomer();
        Integer creatorCustomerId = null;
        
        if (userAccount.getUserAccount() != null) {
            creatorCustomerId = userAccount.getUserAccount().getCustomer().getId();
        }
        final Collection<UserSettingInfo> userAccountList =
                this.prepareUserAccountList(request, customer, creatorCustomerId, customer.getParentCustomer());
        
        /* put user account lists in the model for UI. */
        model.put("userAccountEditForm", userAccountEditForm);
        model.put("userAccountList", userAccountList);
        
        return urlOnSuccess;
    }
    
    /**
     * This method will view User Detail information on the page. When "Edit User" is clicked, it will also be
     * called from front-end logic automatically after calling formUserAccount() method.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param isSysAdmin
     *            flag to know whether customer is a system admin or not
     * @return JSON string having user detail information. "false" in case of any exception or validation
     *         failure.
     */
    protected final String viewUserDetails(final HttpServletRequest request, final HttpServletResponse response, final boolean isSysAdmin) {
        
        /* validate the randomized userAccountID from request parameter. */
        final String actualUserAccountId = this.validateUserAccountId(request);
        if (actualUserAccountId.equals(WebCoreConstants.RESPONSE_FALSE)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Integer userAccountId = new Integer(actualUserAccountId);
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final UserSettingDetails userSettingDetails =
                this.prepareUserAccountDetailsForJSON(request, userAccountId, userAccount.getCustomer(), isSysAdmin);
        
        if (userSettingDetails.getIsDeleted()) {
            final MessageInfo message = new MessageInfo();
            message.setError(true);
            message.setToken(null);
            message.addMessage(this.commonControllerHelper.getMessage(ErrorConstants.REQUESTED_ITEM_DELETED));
            return WidgetMetricsHelper.convertToJson(message, WebCoreConstants.UI_MESSAGES, true);
        }
        
        /* convert UserSettingDetails to JSON string. */
        return WidgetMetricsHelper.convertToJson(userSettingDetails, "userSettingDetails", true);
    }
    
    /**
     * This method will be called when "Add User" is clicked. It will set the user form and list of Role
     * information.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return JSON string which returns a role list.
     * 
     */
    protected final String formUserAccount(final HttpServletRequest request, final HttpServletResponse response) {
        
        /* retrieve user's role information. */
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final RoleSettingInfoWrapper roleSettingInfoWrapper = new RoleSettingInfoWrapper();
        roleSettingInfoWrapper
                .setRoleSettingInfos(this.populateRoleInfo(request, userAccount.getCustomer(), null, userAccount.getCustomer().getParentCustomer()));
        roleSettingInfoWrapper.setChildCustomerInfos(this.populateChildCustomerInfo(request, userAccount.getCustomer().getId(), null, null));
        roleSettingInfoWrapper.setAllChilds(userAccount.isIsAllChilds());
        
        return WidgetMetricsHelper.convertToJson(roleSettingInfoWrapper, "roleSettingInfoWrapper", true);
    }
    
    /**
     * This method saves user account information in UserAccount table.
     * 
     * @param webSecurityForm
     *            user input form
     * @param result
     *            Error object
     * @param response
     *            HttpServletResponse object
     * @return "true" + ":" + token string value if process succeeds, JSON error message if process fails
     */
    protected String saveUser(final WebSecurityForm<UserAccountEditForm> webSecurityForm, final BindingResult result,
        final HttpServletResponse response) {
        
        /* validate user input. */
        this.userSettingValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        final UserAccountEditForm userAccountEditForm = webSecurityForm.getWrappedObject();
        
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        webSecurityForm.resetToken();
        
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_CREATE) {
            /* Add User. */
            addUser(userAccountEditForm);
        } else if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
            /* Edit User. */
            editUser(userAccountEditForm);
        }
        
        return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(":").append(webSecurityForm.getInitToken()).toString();
    }
    
    /**
     * This method deletes user account information by setting user status type to 2 (deleted)
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return "true" string value if process succeeds, "false" string value if process fails.
     */
    protected String deleteUser(final HttpServletRequest request, final HttpServletResponse response) {
        
        /* validate the randomized user account ID from request parameter. */
        final String actualUserAccountId = this.validateUserAccountId(request);
        if (actualUserAccountId.equals(WebCoreConstants.RESPONSE_FALSE)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final Date lastModifiedGmt = new Date();
        
        final Integer userAccountId = new Integer(actualUserAccountId);
        final UserAccount account = this.userAccountService.findUserAccountAndEvict(userAccountId);
        final UserStatusType type = new UserStatusType();
        /* set user status type to 2 (deleted) */
        type.setId(WebCoreConstants.USER_STATUS_TYPE_DELETED);
        account.setUserStatusType(type);
        account.setLastModifiedByUserId(userAccount.getId());
        account.setLastModifiedGmt(lastModifiedGmt);
        account.setArchiveDate(lastModifiedGmt);
        
        /* delete user account by setting user status type to 2. */
        this.userAccountService.updateUserAccount(account);
        
        // Hard-delete UserRole records from database to dis-associate with Role.
        this.userAccountService.deleteUserRoles(account);
        
        final Collection<UserAccount> aliasUserAccounts = this.userAccountService.findAliasUserAccountsByUserAccountId(account.getId());
        
        if (aliasUserAccounts != null) {
            for (UserAccount aliasUserAccount : aliasUserAccounts) {
                aliasUserAccount.setUserStatusType(type);
                aliasUserAccount.setLastModifiedByUserId(userAccount.getId());
                aliasUserAccount.setLastModifiedGmt(lastModifiedGmt);
                aliasUserAccount.setArchiveDate(lastModifiedGmt);
                this.userAccountService.deleteUserRoles(aliasUserAccount);
            }
        }
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    /**
     * This method hides Collection with the primary key and return back.
     * 
     * @param request
     *            HttpServletRequest object
     * @param collection
     *            Collection instance
     * @param name
     *            name of the property to hide
     * @return Collection of WebObject
     */
    @SuppressWarnings("unchecked")
    private Collection<WebObject> hideObjectByName(final HttpServletRequest request, final Collection<?> collection, final String name) {
        
        /* make randomised key. */
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        return keyMapping.hideProperties(collection, name);
    }
    
    /**
     * This method retrieves customer's user account list and wraps them with WebObject.
     * 
     * @param request
     *            HttpServletRequest object
     * @param customerId
     *            customer ID
     * @param userStatusTypeId
     *            user status type ID
     * @return Collection of UserSettingInfo
     */
    private Collection<UserSettingInfo> prepareUserAccountList(final HttpServletRequest request, final Customer customer,
        final Integer creatorCustomerId, final Customer parentCustomer) {
        final Collection<UserAccount> userAccounts;
        
        userAccounts = this.userAccountService.findUserAccountsByCustomerIdAndUserAccountTypeId(customer.getId());
        
        final Collection<WebObject> webObjs = hideObjectByName(request, userAccounts, WebCoreConstants.ID_LOOK_UP_NAME);
        final Collection<UserSettingInfo> lists = new ArrayList<UserSettingInfo>();
        
        final UserAccount currAccount = WebSecurityUtil.getUserAccount();
        
        try {
            for (WebObject obj : webObjs) {
                final UserAccount account = (UserAccount) obj.getWrappedObject();
                final boolean readOnly = currAccount.getId().equals(account.getId()) || (account.getUserAccount() != null) ? true : false;
                final UserSettingInfo userInfo = new UserSettingInfo(obj.getPrimaryKey().toString(),
                        URLDecoder.decode(account.getFirstName(), WebSecurityConstants.URL_ENCODING_LATIN1),
                        URLDecoder.decode(account.getLastName(), WebSecurityConstants.URL_ENCODING_LATIN1),
                        account.getUserStatusType().getId() == WebCoreConstants.USER_STATUS_TYPE_ENABLED ? true : false, readOnly,
                        account.isIsAliasUser());
                
                if (parentCustomer != null) {
                    if (account.isIsAliasUser()) {
                        userInfo.setCustomerName(parentCustomer.getName());
                    } else {
                        userInfo.setCustomerName(customer.getName());
                    }
                }
                lists.add(userInfo);
            }
        } catch (UnsupportedEncodingException e) {
            log.error("+++ Failed to encode user name when updating new user.", e);
        }
        
        return lists;
    }
    
    /**
     * This method prepares UserSettingDetails information for JSON string.
     * 
     * @param userAccountId
     *            user account ID
     * @return UserSettingDetails object
     */
    private UserSettingDetails prepareUserAccountDetailsForJSON(final HttpServletRequest request, final Integer userAccountId,
        final Customer customer, final boolean isSysAdmin) {
        
        final UserAccount userAccount = this.userAccountService.findUserAccount(userAccountId);
        final UserSettingDetails detail = new UserSettingDetails();
        try {
            detail.setRandomId(request.getParameter(FormFieldConstants.PARAMETER_USER_ACCOUNT_ID));
            detail.setFirstName(URLDecoder.decode(userAccount.getRealFirstName(), WebSecurityConstants.URL_ENCODING_LATIN1));
            detail.setLastName(URLDecoder.decode(userAccount.getRealLastName(), WebSecurityConstants.URL_ENCODING_LATIN1));
            detail.setUserName(URLDecoder.decode(userAccount.getRealUserName(), WebSecurityConstants.URL_ENCODING_LATIN1));
            if (userAccount.getCustomerEmail() != null) {
                detail.setEmailAddress(URLDecoder.decode(userAccount.getCustomerEmail().getEmail(), WebSecurityConstants.URL_ENCODING_LATIN1));
            }
        } catch (UnsupportedEncodingException e) {
            log.error("+++ Failed to encode user name when preparing user details.", e);
        }
        
        detail.setStatus(userAccount.getUserStatusType().getId() == WebCoreConstants.USER_STATUS_TYPE_ENABLED ? true : false);
        detail.setIsDeleted(userAccount.getUserStatusType().getId() == WebCoreConstants.USER_STATUS_TYPE_DELETED ? true : false);
        detail.setIsAllChilds(userAccount.isIsAllChilds());
        detail.setIsAlias(userAccount.isIsAliasUser());
        
        /* retrieve role information and set up in UserSettingDetails object. */
        final Set<UserRole> roles = userAccount.getUserRoles();
        Collections.sort(new ArrayList<UserRole>(roles), new Comparator<UserRole>() {
            public int compare(final UserRole r1, final UserRole r2) {
                return r1.getRole().getId() - r2.getRole().getId();
            }
        });
        
        final Collection<Integer> userSelectedRoleIdList = new ArrayList<Integer>();
        for (UserRole role : roles) {
            userSelectedRoleIdList.add(role.getRole().getId());
        }
        
        final Collection<UserAccount> userSelectedChildCustomerList = this.userAccountService.findAliasUserAccountsByUserAccountId(userAccountId);
        final Collection<Integer> userSelectedChildCustomerIdList = new ArrayList<Integer>();
        for (UserAccount ua : userSelectedChildCustomerList) {
            userSelectedChildCustomerIdList.add(ua.getCustomer().getId());
        }
        
        /* retrieve activity log information and set up in UserSettingDetails object. */
        /*
         * This is not for 7.0 but for 7.X if (!isSysAdmin) { Collection<ActivityLog> activityLogs =
         * activityLogService.findActivityLogsByUserAccountId(userAccountId); Collection<ActivityLogDetail>
         * activityLogDetails = new ArrayList<ActivityLogDetail>(); for (ActivityLog log : activityLogs) {
         * activityLogDetails.add(new ActivityLogDetail(log.getActivityType().getName(),
         * log.getActivityType().getDescription(), new Timestamp(log .getActivityGmt().getTime()))); }
         * detail.setActivityLogList(activityLogDetails); }
         */
        
        detail.setRoleList(this.populateRoleInfo(request, customer, userSelectedRoleIdList, customer.getParentCustomer()));
        detail.setChildCustomerInfos(this.populateChildCustomerInfo(request, customer.getId(), userSelectedChildCustomerIdList,
                                                                    userSelectedChildCustomerList));
        
        return detail;
    }
    
    /**
     * This method validates user input randomized user account ID.
     * 
     * @param request
     *            HttpServletRequest object
     * @return actual user account ID if process succeeds, "false" string if process fails.
     */
    private String validateUserAccountId(final HttpServletRequest request) {
        
        /* validate the randomized user account ID from request parameter. */
        final String randomizedUserAccountId = request.getParameter(FormFieldConstants.PARAMETER_USER_ACCOUNT_ID);
        if (StringUtils.isBlank(randomizedUserAccountId) || !DashboardUtil.validateRandomId(randomizedUserAccountId)) {
            log.warn("### Invalid userAccountID in the request. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        /* validate the actual user account ID. */
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Object actualUserAccountId = keyMapping.getKey(randomizedUserAccountId);
        if (actualUserAccountId == null || !DashboardUtil.validateDBIntegerPrimaryKey(actualUserAccountId.toString())) {
            log.warn("### Invalid userAccountID in the request. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return actualUserAccountId.toString();
    }
    
    /**
     * This method retrieves default Role information which is necessary for adding user.
     * 
     * @param request
     *            HttpServletRequest object
     * @param customerId
     *            customer ID
     * @return wrapped Role information
     */
    protected final Collection<WebObject> findCustomerRolesByCustomerId(final HttpServletRequest request, final Integer customerId) {
        
        final Collection<CustomerRole> roles = this.customerRoleService.findCustomerRoleByCustomerId(customerId);
        return hideObjectByName(request, roles, "role.id");
    }
    
    /**
     * This method retrieves default Role information which is necessary for adding user.
     * 
     * @param request
     *            HttpServletRequest object
     * @param customerId
     *            customer ID
     * @return wrapped Role information
     */
    protected final Collection<WebObject> findChildCustomers(final HttpServletRequest request, final Integer customerId) {
        
        final Collection<Customer> childCusts = this.customerService.findAllChildCustomers(customerId);
        return hideObjectByName(request, childCusts, "id");
    }
    
    /**
     * This method adds new user to UserAccount table.
     * 
     * @param form
     *            UserAccountEditForm object
     */
    private void addUser(final UserAccountEditForm form) {
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final Date lastModifiedGmt = new Date();
        
        /* set up new UserAccount instance. */
        final UserAccount newUserAccount = new UserAccount();
        
        final UserStatusType type = new UserStatusType();
        if (form.getStatus()) {
            type.setId(WebCoreConstants.USER_STATUS_TYPE_ENABLED);
        } else {
            type.setId(WebCoreConstants.USER_STATUS_TYPE_DISABLED);
        }
        
        newUserAccount.setUserStatusType(type);
        newUserAccount.setCustomer(userAccount.getCustomer());
        
        try {
            newUserAccount.setUserName(URLEncoder.encode(form.getUserName(), WebSecurityConstants.URL_ENCODING_LATIN1));
            newUserAccount.setFirstName(URLEncoder.encode(form.getFirstName(), WebSecurityConstants.URL_ENCODING_LATIN1));
            newUserAccount.setLastName(URLEncoder.encode(form.getLastName(), WebSecurityConstants.URL_ENCODING_LATIN1));
            if (!StringUtils.isBlank(form.getEmailAddress())) {
                newUserAccount.setCustomerEmail(new CustomerEmail(userAccount.getCustomer(),
                        URLEncoder.encode(form.getEmailAddress(), WebSecurityConstants.URL_ENCODING_LATIN1)));
            }
            
        } catch (UnsupportedEncodingException e) {
            log.error("+++ Failed to encode user name when creating new user.", e);
        }
        
        final String salt = RandomStringUtils.randomAlphanumeric(WebSecurityConstants.RANDOMKEY_MIN_HEX_LENGTH);
        final String encodedPassword = this.passwordEncoder.encodePassword(form.getUserPassword(), salt);
        newUserAccount.setPassword(encodedPassword);
        newUserAccount.setPasswordSalt(salt);
        newUserAccount.setIsPasswordTemporary(true);
        newUserAccount.setLastModifiedGmt(lastModifiedGmt);
        newUserAccount.setLastModifiedByUserId(userAccount.getLastModifiedByUserId());
        
        /* set up new User Role. */
        final Set<UserRole> userRoles = new HashSet<UserRole>();
        final Collection<Integer> roleIds = form.getRoleIds();
        for (Integer id : roleIds) {
            final Role role = new Role();
            role.setId(id);
            userRoles.add(new UserRole(role, newUserAccount, userAccount.getId(), lastModifiedGmt));
        }
        
        newUserAccount.setUserRoles(userRoles);
        newUserAccount.setIsAllChilds(form.getIsAllChilds());
        this.userAccountService.saveUserAccountAndUserRole(newUserAccount);
        
        final Collection<Integer> childCustomerIds = form.getChildCustomerIds();
        if (childCustomerIds != null) {
            
            final Set<Role> childRolesFromParent = new HashSet<Role>();
            if (newUserAccount.getUserRoles() != null) {
                for (UserRole childUserRoleparent : newUserAccount.getUserRoles()) {
                    final Role childRole = this.roleService.findRoleByRoleIdAndEvict(childUserRoleparent.getRole().getId());
                    if (childRole.getCustomerType().getId() == WebCoreConstants.CUSTOMER_TYPE_CHILD) {
                        childRolesFromParent.add(childRole);
                    }
                }
            }
            
            for (Integer id : childCustomerIds) {
                final Customer childCustomer = new Customer();
                childCustomer.setId(id);
                final UserAccount childUserAccount = new UserAccount(childCustomer, userAccount.getUserStatusType(),
                        newUserAccount.getUserName() + id, newUserAccount.getFirstName(), newUserAccount.getLastName(), WebCoreConstants.NOT_USED,
                        userAccount.getPasswordSalt(), false, newUserAccount.getArchiveDate(), new Date(), userAccount.getLastModifiedByUserId());
                
                childUserAccount.setIsAliasUser(true);
                childUserAccount.setUserAccount(newUserAccount);
                
                final Set<UserRole> childUserRoles = new HashSet<UserRole>();
                final Set<CustomerRole> childCustomerRoles = new HashSet<CustomerRole>();
                if (childRolesFromParent != null) {
                    for (Role childRole : childRolesFromParent) {
                        childUserRoles.add(new UserRole(childRole, childUserAccount, userAccount.getId(), lastModifiedGmt));
                        childCustomerRoles.add(new CustomerRole(childRole, childUserAccount.getCustomer(), childUserAccount, lastModifiedGmt));
                    }
                }
                childUserAccount.setUserRoles(childUserRoles);
                
                childUserAccount.setIsDefaultAlias(childUserAccount.getCustomer().getId() == form.getDefaultAliasCustomerId() ? true : false);
                
                this.userAccountService.saveUserAccountAndUserRole(childUserAccount);
                this.roleService.saveCustomerRoles(childCustomerRoles);
            }
        }
    }
    
    /**
     * This method updates existing user to UserAccount table.
     * 
     * @param form
     *            UserAccountEditForm object
     */
    private void editUser(final UserAccountEditForm form) {
        
        final UserAccount account = WebSecurityUtil.getUserAccount();
        final Date lastModifiedGmt = new Date();
        
        final Integer userAccountId = form.getUserAccountId();
        final UserAccount userAccount = this.userAccountService.findUserAccountAndEvict(userAccountId);
        
        final UserStatusType type = new UserStatusType();
        if (form.getStatus()) {
            type.setId(WebCoreConstants.USER_STATUS_TYPE_ENABLED);
        } else {
            type.setId(WebCoreConstants.USER_STATUS_TYPE_DISABLED);
        }
        
        userAccount.setUserStatusType(type);
        userAccount.setCustomer(account.getCustomer());
        
        try {
            userAccount.setUserName(URLEncoder.encode(form.getUserName(), WebSecurityConstants.URL_ENCODING_LATIN1));
            userAccount.setFirstName(URLEncoder.encode(form.getFirstName(), WebSecurityConstants.URL_ENCODING_LATIN1));
            userAccount.setLastName(URLEncoder.encode(form.getLastName(), WebSecurityConstants.URL_ENCODING_LATIN1));
            if (!StringUtils.isBlank(form.getEmailAddress())) {
                userAccount.setCustomerEmail(new CustomerEmail(userAccount.getCustomer(),
                        URLEncoder.encode(form.getEmailAddress(), WebSecurityConstants.URL_ENCODING_LATIN1)));
            }
        } catch (UnsupportedEncodingException e) {
            log.error("+++ Failed to encode user name when updating new user.", e);
        }
        
        userAccount.setLastModifiedGmt(lastModifiedGmt);
        userAccount.setLastModifiedByUserId(account.getLastModifiedByUserId());
        
        if (!StringUtils.isBlank(form.getUserPassword())) {
            final String salt = RandomStringUtils.randomAlphanumeric(WebSecurityConstants.RANDOMKEY_MIN_HEX_LENGTH);
            final String encodedPassword = this.passwordEncoder.encodePassword(form.getUserPassword(), salt);
            userAccount.setPassword(encodedPassword);
            userAccount.setPasswordSalt(salt);
            userAccount.setIsPasswordTemporary(true);
        }
        
        /* set up updated User Role. */
        final Set<UserRole> userRoles = new HashSet<UserRole>();
        final Collection<Integer> roleIds = form.getRoleIds();
        for (Integer id : roleIds) {
            final Role role = new Role();
            role.setId(id);
            userRoles.add(new UserRole(role, userAccount, userAccount.getId(), lastModifiedGmt));
        }
        userAccount.setUserRoles(userRoles);
        this.userAccountService.updateUserAccountAndUserRoles(userAccount);
        
        final Collection<Integer> childCustomerIds = form.getChildCustomerIds();
        if (childCustomerIds != null) {
            
            final Set<Role> childRolesFromParent = new HashSet<Role>();
            if (userAccount.getUserRoles() != null) {
                for (UserRole childUserRoleparent : userAccount.getUserRoles()) {
                    final Role childRole = this.roleService.findRoleByRoleIdAndEvict(childUserRoleparent.getRole().getId());
                    if (childRole.getCustomerType().getId() == WebCoreConstants.CUSTOMER_TYPE_CHILD) {
                        childRolesFromParent.add(childRole);
                    }
                }
            }
            
            for (Integer id : childCustomerIds) {
                final UserAccount uaExists = this.userAccountService.findAliasUserAccountByUserAccountIdAndCustomerId(userAccount.getId(), id);
                final Set<UserRole> childUserRoles = new HashSet<UserRole>();
                if (uaExists == null) {
                    final Customer childCustomer = new Customer();
                    childCustomer.setId(id);
                    final UserAccount childUserAccount = new UserAccount(childCustomer, userAccount.getUserStatusType(),
                            userAccount.getUserName() + id, userAccount.getFirstName(), userAccount.getLastName(), WebCoreConstants.NOT_USED,
                            userAccount.getPasswordSalt(), false, userAccount.getArchiveDate(), new Date(), userAccount.getLastModifiedByUserId());
                    childUserAccount.setIsAliasUser(true);
                    childUserAccount.setUserAccount(userAccount);
                    final Set<CustomerRole> childCustomerRoles = new HashSet<CustomerRole>();
                    if (childRolesFromParent != null) {
                        for (Role childRole : childRolesFromParent) {
                            childUserRoles.add(new UserRole(childRole, childUserAccount, userAccount.getId(), lastModifiedGmt));
                            childCustomerRoles.add(new CustomerRole(childRole, childUserAccount.getCustomer(), childUserAccount, lastModifiedGmt));
                        }
                    }
                    childUserAccount.setUserRoles(childUserRoles);
                    childUserAccount
                            .setIsDefaultAlias(childUserAccount.getCustomer().getId().equals(form.getDefaultAliasCustomerId()) ? true : false);
                    this.userAccountService.saveUserAccountAndUserRole(childUserAccount);
                    this.roleService.saveCustomerRoles(childCustomerRoles);
                } else {
                    final Set<CustomerRole> childCustomerRoles = new HashSet<CustomerRole>();
                    if (childRolesFromParent != null) {
                        for (Role childRole : childRolesFromParent) {
                            childUserRoles.add(new UserRole(childRole, uaExists, userAccount.getId(), lastModifiedGmt));
                            childCustomerRoles.add(new CustomerRole(childRole, uaExists.getCustomer(), uaExists, lastModifiedGmt));
                        }
                    }
                    uaExists.setUserRoles(childUserRoles);
                    uaExists.setIsDefaultAlias(uaExists.getCustomer().getId().equals(form.getDefaultAliasCustomerId()) ? true : false);
                    
                    uaExists.setUserName(userAccount.getUserName() + id);
                    uaExists.setFirstName(userAccount.getFirstName());
                    uaExists.setLastName(userAccount.getLastName());
                    uaExists.setUserStatusType(userAccount.getUserStatusType());
                    
                    this.userAccountService.updateUserAccountAndUserRoles(uaExists);
                    this.roleService.saveCustomerRoles(childCustomerRoles);
                }
                
            }
        }
        
        final Collection<UserAccount> aliasUserAccounts = this.userAccountService.findAliasUserAccountsByUserAccountId(userAccount.getId());
        if (aliasUserAccounts != null) {
            for (UserAccount ua : aliasUserAccounts) {
                final Integer customerId = ua.getCustomer().getId();
                if (childCustomerIds == null || !childCustomerIds.contains(customerId)) {
                    final UserStatusType uat = new UserStatusType();
                    uat.setId(WebCoreConstants.USER_STATUS_TYPE_DELETED);
                    ua.setUserStatusType(uat);
                    this.userAccountService.updateUserAccount(ua);
                    this.userAccountService.deleteUserRoles(ua);
                }
            }
        }
        
    }
}
