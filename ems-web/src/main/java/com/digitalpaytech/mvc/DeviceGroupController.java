package com.digitalpaytech.mvc;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;
import java.util.concurrent.ArrayBlockingQueue;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.digitalpaytech.client.FacilitiesManagementClient;
import com.digitalpaytech.client.dto.DeviceGroupFilter;
import com.digitalpaytech.client.dto.fms.BossKeyProgress;
import com.digitalpaytech.client.dto.fms.BulkSchedule;
import com.digitalpaytech.client.dto.fms.DeviceGroup;
import com.digitalpaytech.client.dto.fms.IrisOverrideSettings;
import com.digitalpaytech.client.dto.fms.ScheduleConfiguration;
import com.digitalpaytech.client.dto.fms.UpgradeDisplayGroup;
import com.digitalpaytech.client.util.CommunicationException;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.MapEntry;
import com.digitalpaytech.dto.PaginatedList;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.ribbon.ClientFactory;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.DeviceGroupConstants;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.RestCoreConstants;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StableDateUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.util.json.JSONConfigurationBean;
import com.digitalpaytech.validation.customeradmin.BulkScheduleValidator;
import com.digitalpaytech.validation.customeradmin.DeviceGroupSearchValidator;
import com.digitalpaytech.validation.customeradmin.DeviceGroupValidator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.netflix.hystrix.exception.HystrixBadRequestException;
import com.netflix.hystrix.exception.HystrixRuntimeException;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.EmptyByteBuf;

@Controller
public class DeviceGroupController {
    private static final String BULK_SCHEDULE_ERROR_LABEL = "label.settings.locations.payStationConfiguration.bulk.schedule.error";
    
    private static final String SCHEDULE_IN_PAST_LABEL = "label.settings.locations.payStationConfiguration.schedule.in.past";
    
    private static final String INVALID_SCHEDULE_FORMAT_LABEL = "label.settings.locations.payStationConfiguration.invalid.schedule.format";
    
    private static final Logger LOG = Logger.getLogger(DeviceGroupController.class);
    
    @Autowired
    protected ClientFactory clientFactory;
    
    @Autowired
    protected CommonControllerHelper commonControllerHelper;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private DeviceGroupValidator editValidator;
    
    @Autowired
    private BulkScheduleValidator bulkScheduleValidator;
    
    @Autowired
    private DeviceGroupSearchValidator searchValidator;
    
    @Autowired
    private MailerService mailerService;
    
    private JSON json;
    
    private TypeReference<List<DeviceGroup>> groupListType = new TypeReference<List<DeviceGroup>>() {
    };
    
    private TypeReference<List<UpgradeDisplayGroup>> upgradeDisplayGroupListType = new TypeReference<List<UpgradeDisplayGroup>>() {
    };
    
    @PostConstruct
    public final void init() {
        final JSONConfigurationBean jsonConfig = new JSONConfigurationBean(true, false, false);
        this.json = new JSON(jsonConfig);
    }
    
    protected final String performPage(final WebSecurityForm<DeviceGroup> editForm, final WebSecurityForm<DeviceGroupFilter> searchForm,
        final WebSecurityForm<BulkSchedule> bulkScheduleForm, final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model, final String urlOnSuccess) {
        model.put(DeviceGroupConstants.FORM_EDIT, editForm);
        model.put(DeviceGroupConstants.FORM_SEARCH, searchForm);
        model.put(DeviceGroupConstants.FORM_BULK_SCHEDULE, bulkScheduleForm);
        
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        final PlacementMapBordersInfo mapBorders = this.pointOfSaleService.findMapBordersByCustomerId(customerId);
        model.put("mapBorders", mapBorders);
        
        return urlOnSuccess;
    }
    
    protected final String performList(final WebSecurityForm<DeviceGroupFilter> searchForm, final BindingResult bindingResult,
        final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        String result;
        
        this.searchValidator.validate(searchForm, bindingResult);
        searchForm.resetToken();
        if (searchForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            result = WidgetMetricsHelper.convertToJson(searchForm.getValidationErrorInfo(), ErrorConstants.ERROR_STATUS, true);
        } else {
            final DeviceGroupFilter filter = searchForm.getWrappedObject();
            if (filter.getStatus() == null || filter.getStatus().isEmpty()) {
                filter.setStatus(WebCoreConstants.ONLINE_PAYSTATION_CONFIGURATION_STATUS_ALL);
            }
            if (filter.getLastAccessed() == null) {
                filter.setLastAccessed(System.currentTimeMillis());
            }
            
            final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
            final Customer customer = this.customerService.findCustomer(customerId);
            
            final FacilitiesManagementClient client = this.clientFactory.from(FacilitiesManagementClient.class);
            try {
                final String jsonList =
                        client.groupList(String.valueOf(customer.getId()), filter.getStatus(), filter.getPage(), filter.getLastAccessed()).execute()
                                .toString(Charset.defaultCharset());
                
                final List<DeviceGroup> objList = this.json.deserialize(jsonList, this.groupListType);
                randomizeGroupIdsInDeviceGroup(request, objList);
                final PaginatedList<DeviceGroup> groupsList =
                        new PaginatedList<DeviceGroup>(objList, filter.getLastAccessed().toString(), searchForm.getInitToken(), filter.getPage());
                
                result = WidgetMetricsHelper.convertToJson(groupsList, DeviceGroupConstants.MODEL_GROUPS_LIST, true);
            } catch (JsonException je) {
                LOG.error(DeviceGroupConstants.FAILED_TO_DESERIALIZE_DEVICE_GROUP_LIST_JSON, je);
                result = WidgetMetricsHelper.convertToJson(errorMessage(searchForm.getInitToken(), ErrorConstants.MS_COMM_FAILURE),
                                                           WebCoreConstants.UI_MESSAGES, true);
            } catch (HystrixRuntimeException hre) {
                LOG.error(DeviceGroupConstants.COMMUNICATION_FAILURE_WITH_FMSERVER, hre);
                result = WidgetMetricsHelper.convertToJson(errorMessage(searchForm.getInitToken(), ErrorConstants.MS_COMM_FAILURE),
                                                           WebCoreConstants.UI_MESSAGES, true);
            }
        }
        
        return result;
    }
    
    public final String performlistUpgradeGroups(final HttpServletRequest request, final HttpServletResponse response) {
        
        String result;
        
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        final Customer customer = this.customerService.findCustomer(customerId);
        try {
            final FacilitiesManagementClient facilitiesManagementClient = this.clientFactory.from(FacilitiesManagementClient.class);
            
            final String jsonList =
                    facilitiesManagementClient.listUpgradeGroups(String.valueOf(customer.getId())).execute().toString(Charset.defaultCharset());
            final List<UpgradeDisplayGroup> groupList = this.json.deserialize(jsonList, this.upgradeDisplayGroupListType);
            randomizeGroupIdsInUpgradeDisplayGroup(request, groupList);
            
            final Map<String, List<UpgradeDisplayGroup>> groupFullListResult = new HashMap<String, List<UpgradeDisplayGroup>>(1);
            groupFullListResult.put(DeviceGroupConstants.MODEL_GROUPS_LIST, groupList);
            result = this.json.serialize(groupFullListResult);
        } catch (JsonException je) {
            LOG.error(DeviceGroupConstants.FAILED_TO_DESERIALIZE_DEVICE_GROUP_LIST_JSON, je);
            result = WidgetMetricsHelper.convertToJson(errorMessage(null, ErrorConstants.MS_COMM_FAILURE), WebCoreConstants.UI_MESSAGES, true);
        } catch (HystrixRuntimeException hre) {
            LOG.error(DeviceGroupConstants.COMMUNICATION_FAILURE_WITH_FMSERVER, hre);
            result = WidgetMetricsHelper.convertToJson(errorMessage(null, ErrorConstants.MS_COMM_FAILURE), WebCoreConstants.UI_MESSAGES, true);
        }
        return result;
        
    }
    
    public final String autocompleteGroups(final HttpServletRequest request, final HttpServletResponse response) {
        
        String result;
        
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        final Customer customer = this.customerService.findCustomer(customerId);
        final String search = request.getParameter("search");
        try {
            final String encodedSearch = URLEncoder.encode(search, StandardConstants.HTTP_UTF_8_ENCODE_CHARSET);
            final FacilitiesManagementClient facilitiesManagementClient = this.clientFactory.from(FacilitiesManagementClient.class);
            
            final String jsonList = facilitiesManagementClient.groupNameAutoComplete(String.valueOf(customer.getId()), encodedSearch).execute()
                    .toString(Charset.defaultCharset());
            final List<DeviceGroup> groupList = this.json.deserialize(jsonList, this.groupListType);
            randomizeGroupIdsInDeviceGroup(request, groupList);
            final Map<String, List<DeviceGroup>> groupSearchResult = new HashMap<String, List<DeviceGroup>>(1);
            groupSearchResult.put("groupSearchResult", groupList);
            result = this.json.serialize(groupSearchResult);
        } catch (JsonException je) {
            LOG.error(DeviceGroupConstants.FAILED_TO_DESERIALIZE_DEVICE_GROUP_LIST_JSON, je);
            result = WidgetMetricsHelper.convertToJson(errorMessage(null, ErrorConstants.MS_COMM_FAILURE), WebCoreConstants.UI_MESSAGES, true);
        } catch (HystrixRuntimeException hre) {
            LOG.error(DeviceGroupConstants.COMMUNICATION_FAILURE_WITH_FMSERVER, hre);
            result = WidgetMetricsHelper.convertToJson(errorMessage(null, ErrorConstants.MS_COMM_FAILURE), WebCoreConstants.UI_MESSAGES, true);
        } catch (UnsupportedEncodingException uee) {
            LOG.error("URL encoding failed", uee);
            result = WidgetMetricsHelper.convertToJson(errorMessage(null, ErrorConstants.MS_COMM_FAILURE), WebCoreConstants.UI_MESSAGES, true);
        }
        return result;
        
    }
    
    @SuppressWarnings("unchecked")
    protected final String performDetails(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String randomGroupId = request.getParameter(FormFieldConstants.PARAMETER_GROUP_ID);
        final String groupId = (String) keyMapping.getKey(randomGroupId);
        
        final Customer loginCustomer = this.customerService.findCustomer(this.commonControllerHelper.resolveCustomerId(request, response));
        
        final FacilitiesManagementClient facilitiesManagementClient = this.clientFactory.from(FacilitiesManagementClient.class);
        final String responseJsonString = facilitiesManagementClient.groupDetails(groupId).execute().toString(Charset.defaultCharset());
        
        String updatedJson;
        
        try {
            final Map<String, Object> groupDetails = (Map<String, Object>) this.json.deserialize(responseJsonString, Map.class);
            final List<Map<String, Object>> devices = (List<Map<String, Object>>) groupDetails.get(DeviceGroupConstants.DEVICE_LIST);
            final Map<Integer, Integer> idUpgradeStatusMap = new HashMap<Integer, Integer>();
            final List<Object> unrecognizedDevices = new ArrayList<Object>(devices.size());
            for (Map<String, Object> device : devices) {
                final String serialNumber = (String) device.get(DeviceGroupConstants.DEVICE_ID);
                final PointOfSale pointOfSale =
                        this.pointOfSaleService.findPointOfSaleBySerialNumberAndCustomerId(serialNumber, loginCustomer.getId());
                if (pointOfSale != null) {
                    
                    final String randId = keyMapping.getRandomString(PointOfSale.class, pointOfSale.getId());
                    device.put(DeviceGroupConstants.DEVICE_ID, randId);
                    device.put(DeviceGroupConstants.PAY_STATION_NAME, pointOfSale.getName());
                    device.put(DeviceGroupConstants.SERIAL_NUMBER, serialNumber);
                    device.put(DeviceGroupConstants.IS_POS_ACTIVATED, pointOfSale.getPosStatus().isIsActivated());
                    device.put(DeviceGroupConstants.IS_POS_DECOMMISSIONED, pointOfSale.getPosStatus().isIsDecommissioned());
                    device.put(DeviceGroupConstants.IS_POS_HIDDEN, !pointOfSale.getPosStatus().isIsVisible());
                    
                    final Paystation paystation = pointOfSale.getPaystation();
                    if (paystation != null) {
                        device.put(DeviceGroupConstants.PAYSTATION_TYPE, paystation.getPaystationType().getId());
                    }
                    
                    if (pointOfSale.getLatitude() != null && pointOfSale.getLongitude() != null) {
                        final Integer severity = "completed".equals(device.get(DeviceGroupConstants.UPGRADE_STATUS)) ? 0 : 1;
                        idUpgradeStatusMap.put(pointOfSale.getId(), severity);
                    }
                } else {
                    LOG.warn("Serial number from facilities management was not found in Iris -> serialNumber: " + serialNumber);
                    unrecognizedDevices.add(device);
                }
            }
            
            if (!unrecognizedDevices.isEmpty()) {
                devices.removeAll(unrecognizedDevices);
                groupDetails.put(DeviceGroupConstants.UNRECOGNIZED_DEVICES, unrecognizedDevices);
            }
            groupDetails.put(DeviceGroupConstants.DEVICE_LIST, devices);
            
            if (groupDetails.containsKey(FormFieldConstants.SETTINGS_RECEIVED_DATE)) {
                final String relativeRecDateStr =
                        convertToCustomerTimeZoneString((String) groupDetails.get(FormFieldConstants.SETTINGS_RECEIVED_DATE));
                final boolean result = groupDetails.replace(FormFieldConstants.SETTINGS_RECEIVED_DATE,
                                                            (String) groupDetails.get(FormFieldConstants.SETTINGS_RECEIVED_DATE), relativeRecDateStr);
                if (LOG.isDebugEnabled()) {
                    debugDateStrings((String) groupDetails.get(FormFieldConstants.SETTINGS_RECEIVED_DATE), relativeRecDateStr, result);
                }
            }
            if (groupDetails.containsKey(FormFieldConstants.SCHEDULE_DATE)) {
                final String relativeScheduleStr = convertToCustomerTimeZoneString((String) groupDetails.get(FormFieldConstants.SCHEDULE_DATE));
                final boolean result = groupDetails.replace(FormFieldConstants.SCHEDULE_DATE,
                                                            (String) groupDetails.get(FormFieldConstants.SCHEDULE_DATE), relativeScheduleStr);
                if (LOG.isDebugEnabled()) {
                    debugDateStrings((String) groupDetails.get(FormFieldConstants.SCHEDULE_DATE), relativeScheduleStr, result);
                }
            }
            
            Map<Integer, MapEntry> mapEntries = new HashMap<>();
            if (!idUpgradeStatusMap.isEmpty()) {
                mapEntries = buildMapInfo(idUpgradeStatusMap);
            }
            groupDetails.put(DeviceGroupConstants.MAP_INFO, mapEntries.values());
            groupDetails.put(FormFieldConstants.PARAMETER_GROUP_ID,
                             keyMapping.getRandomString(DeviceGroup.class, groupDetails.get(FormFieldConstants.PARAMETER_GROUP_ID)));
            
            updatedJson = this.json.serialize(groupDetails);
            
        } catch (JsonException | ParseException je) {
            LOG.error(DeviceGroupConstants.FAILED_TO_DESERIALIZE_DEVICE_GROUP_LIST_JSON, je);
            updatedJson = WidgetMetricsHelper.convertToJson(errorMessage(null, ErrorConstants.MS_COMM_FAILURE), WebCoreConstants.UI_MESSAGES, true);
        } catch (HystrixRuntimeException hre) {
            LOG.error(DeviceGroupConstants.COMMUNICATION_FAILURE_WITH_FMSERVER, hre);
            updatedJson = WidgetMetricsHelper.convertToJson(errorMessage(null, ErrorConstants.MS_COMM_FAILURE), WebCoreConstants.UI_MESSAGES, true);
        } catch (IllegalArgumentException iae) {
            LOG.error("Unknown Paystation Serial Number", iae);
            updatedJson = WidgetMetricsHelper.convertToJson(errorMessage(null, ErrorConstants.MS_COMM_FAILURE), WebCoreConstants.UI_MESSAGES, true);
        }
        return updatedJson;
    }
    
    @SuppressWarnings("checkstyle:illegalcatch")
    protected final String sendCardProcessingConfiguration(final WebSecurityForm<DeviceGroup> webSecurityForm, final HttpServletRequest request,
        final boolean canSchedule) {
        
        final DeviceGroup deviceGroup = webSecurityForm.getWrappedObject();
        if (deviceGroup == null || StringUtils.isBlank(deviceGroup.getGroupId())) {
            LOG.error(DeviceGroupConstants.CONFIGURATION_GROUP_ERROR_LOG + deviceGroup);
            return prepareFailedResponse(DeviceGroupConstants.CONFIGURATION_GROUP_ERROR_KEY);
        }
        
        final String originalGroupId = (String) SessionTool.getInstance(request).getKeyMapping().getKey(deviceGroup.getGroupId());
        
        /*
         * "forced" means that the update is being done by someone with schedule permission.
         * If someone has only manage permission, and they try to update settings when the group is in the scheduled state,
         * the update will be refused by FMS.
         * Should only pass "true" if the user has Schedule permission.
         */
        IrisOverrideSettings irisOverrideSettings = null;
        if (deviceGroup != null && deviceGroup.getGroupConfiguration() != null && deviceGroup.getGroupConfiguration().isCardProcessingOverride()) {
            irisOverrideSettings =
                    new IrisOverrideSettings(true, canSchedule, deviceGroup.getGroupConfiguration().getIrisSettings().getCardProcessing());
        } else {
            irisOverrideSettings = new IrisOverrideSettings(false, canSchedule, null);
        }
        
        final FacilitiesManagementClient client = this.clientFactory.from(FacilitiesManagementClient.class);
        try {
            final String resp = client.updateDeviceGroup(originalGroupId, irisOverrideSettings).execute().toString(Charset.defaultCharset());
            LOG.info("Successfully called 'updateDeviceGroup' for originalGroupId: " + originalGroupId + ", response: " + resp);
        } catch (Exception e) {
            /*
             * The underlining exceptions are HystrixBadRequestException and com.digitalpaytech.client.util.CommunicationException but
             * since those are wrapped by RuntimeException, I have to catch all.
             */
            LOG.error("sendCardProcessingConfiguration error, FMS response: " + e.getMessage(), e);
            final String resp = causedByScheduledOrIncompleteStatus(e, DeviceGroupConstants.FAILED_CARD_PROCESSING_SCHEDULED_CANNOT_UPDATE_ERROR_KEY,
                                                                    DeviceGroupConstants.FAILED_TO_CONFIG_CARD_PROCESSING_ERROR_KEY);
            return prepareFailedResponse(resp);
        }
        return prepareSuccessfulResponse(webSecurityForm.getInitToken());
    }
    
    protected final String sendBulkSchedule(final WebSecurityForm<BulkSchedule> webSecurityForm, final HttpServletRequest request,
        final boolean canManage) {
        
        final BulkSchedule bulkSchedule = webSecurityForm.getWrappedObject();
        
        if (bulkSchedule == null) {
            LOG.error(DeviceGroupConstants.MALFORMED_BULK_SCHEDULE_REQUEST);
            return prepareFailedResponse(DeviceGroupConstants.CONFIGURATION_GROUP_ERROR_KEY);
        }
        if (StringUtils.isBlank(bulkSchedule.getGroupIds())) {
            LOG.error(DeviceGroupConstants.BULK_SCHEDULE_NO_GROUP_SELECTED);
            return prepareFailedResponse(DeviceGroupConstants.BULK_SCHEDULE_NO_GROUP_SELECTED_ERROR_KEY);
        }
        
        if (!this.bulkScheduleValidator.validateScheduleFormat(webSecurityForm, bulkSchedule.getScheduleDate(), bulkSchedule.getScheduleTime())) {
            webSecurityForm.addErrorStatus(new FormErrorStatus(FormFieldConstants.SCHEDULE_DATE,
                    this.bulkScheduleValidator.getMessage(INVALID_SCHEDULE_FORMAT_LABEL)));
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), ErrorConstants.ERROR_STATUS, true);
        } else if (!this.bulkScheduleValidator.validateScheduleInFuture(WebSecurityUtil.getCustomerTimeZone(), webSecurityForm,
                                                                        bulkSchedule.getScheduleDate(), bulkSchedule.getScheduleTime())) {
            webSecurityForm.addErrorStatus(new FormErrorStatus(FormFieldConstants.SCHEDULE_DATE,
                    this.bulkScheduleValidator.getMessage(SCHEDULE_IN_PAST_LABEL)));
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), ErrorConstants.ERROR_STATUS, true);
        }
        
        final String scheduleDateTime = getDateTime(bulkSchedule.getScheduleDate(), bulkSchedule.getScheduleTime());
        final ScheduleConfiguration conf = new ScheduleConfiguration(true, canManage, scheduleDateTime);
        final FacilitiesManagementClient client = this.clientFactory.from(FacilitiesManagementClient.class);
        
        final RandomKeyMapping randomKeyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        for (String groupId : bulkSchedule.getGroupIds().split(WebCoreConstants.COMMA)) {
            final String originalGroupId = (String) randomKeyMapping.getKey(groupId.trim());
            
            if (StringUtils.isBlank(originalGroupId)) {
                LOG.error("Bulk Schedule: bad randomId: " + groupId);
                return prepareFailedResponse(BULK_SCHEDULE_ERROR_LABEL);
            }
            
            try {
                final String resp = client.sendSchedule(originalGroupId, conf).execute().toString(Charset.defaultCharset());
                LOG.debug("Bulk Schedule: Successfully called 'sendSchedule' for originalGroupId: " + originalGroupId + ", response: " + resp);
            } catch (CommunicationException e) {
                LOG.error("bulkSchedule error, FMS response: " + e.getMessage(), e);
                return prepareFailedResponse(BULK_SCHEDULE_ERROR_LABEL);
                
            }
            
        }
        
        return prepareSuccessfulResponse(webSecurityForm.getInitToken());
    }
    
    @SuppressWarnings("checkstyle:illegalcatch")
    protected final String sendSchedule(final WebSecurityForm<DeviceGroup> webSecurityForm, final HttpServletRequest request,
        final boolean canManage) {
        
        final DeviceGroup deviceGroup = webSecurityForm.getWrappedObject();
        if (deviceGroup == null || StringUtils.isBlank(deviceGroup.getGroupId())) {
            LOG.error(DeviceGroupConstants.CONFIGURATION_GROUP_ERROR_LOG + deviceGroup);
            return prepareFailedResponse(DeviceGroupConstants.CONFIGURATION_GROUP_ERROR_KEY);
        }
        
        ScheduleConfiguration conf = null;
        
        final String unsch = request.getParameter(FormFieldConstants.PARAMETER_UNSCHEDULE_CHECK);
        
        if (isUnschedule(deviceGroup, unsch)) {
            conf = new ScheduleConfiguration(false, canManage);
        } else {
            if (!this.editValidator.validateScheduleFormat(webSecurityForm, deviceGroup.getScheduleDate(), deviceGroup.getScheduleTime())) {
                webSecurityForm.addErrorStatus(new FormErrorStatus(FormFieldConstants.SCHEDULE_DATE,
                        this.editValidator.getMessage(INVALID_SCHEDULE_FORMAT_LABEL)));
                return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), ErrorConstants.ERROR_STATUS, true);
            } else if (!this.editValidator.validateScheduleInFuture(WebSecurityUtil.getCustomerTimeZone(), webSecurityForm,
                                                                    deviceGroup.getScheduleDate(), deviceGroup.getScheduleTime())) {
                webSecurityForm
                        .addErrorStatus(new FormErrorStatus(FormFieldConstants.SCHEDULE_DATE, this.editValidator.getMessage(SCHEDULE_IN_PAST_LABEL)));
                return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), ErrorConstants.ERROR_STATUS, true);
            }
            
            final String scheduleDateTime =
                    getDateTime(webSecurityForm.getWrappedObject().getScheduleDate(), webSecurityForm.getWrappedObject().getScheduleTime());
            conf = new ScheduleConfiguration(true, canManage, scheduleDateTime);
        }
        
        final String originalGroupId = (String) SessionTool.getInstance(request).getKeyMapping().getKey(deviceGroup.getGroupId());
        
        if (StringUtils.isBlank(originalGroupId)) {
            LOG.error("DeviceGroup group name: " + deviceGroup.getGroupName() + " produced incorrect random groupId: " + deviceGroup.getGroupId());
            return prepareFailedResponse(DeviceGroupConstants.CONFIGURATION_GROUP_ERROR_KEY);
        }
        
        final FacilitiesManagementClient client = this.clientFactory.from(FacilitiesManagementClient.class);
        try {
            final String resp = client.sendSchedule(originalGroupId, conf).execute().toString(Charset.defaultCharset());
            LOG.info("Successfully called 'sendSchedule' for originalGroupId: " + originalGroupId + ", response: " + resp);
        } catch (Exception e) {
            /*
             * The underlining exceptions are HystrixBadRequestException and com.digitalpaytech.client.util.CommunicationException but
             * since those are wrapped by RuntimeException, I have to catch all.
             */
            
            LOG.error("sendSchedule error, FMS response: " + e.getMessage(), e);
            final String respMsgKey =
                    causedByScheduledOrIncompleteStatus(e, DeviceGroupConstants.FAILED_CARD_PROCESSING_SCHEDULED_CANNOT_UPDATE_ERROR_KEY,
                                                        DeviceGroupConstants.FAILED_TO_CONFIG_SCHEDULE_ERROR_KEY);
            return prepareFailedResponse(respMsgKey);
        }
        return prepareSuccessfulResponse(webSecurityForm.getInitToken());
    }
    
    public final String generate(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        String result;
        
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        final Customer customer = this.customerService.findCustomer(customerId);
        final FacilitiesManagementClient client = this.clientFactory.from(FacilitiesManagementClient.class);
        
        try {
            result = client.bossKeyGenerate(String.valueOf(customer.getId())).execute().toString(Charset.defaultCharset());
        } catch (HystrixRuntimeException hre) {
            LOG.error(DeviceGroupConstants.COMMUNICATION_FAILURE_WITH_FMSERVER, hre);
            result = WidgetMetricsHelper.convertToJson(errorMessage(null, ErrorConstants.MS_COMM_FAILURE), WebCoreConstants.UI_MESSAGES, true);
        }
        
        return result;
    }
    
    public final String progress(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        final Customer customer = this.customerService.findCustomer(customerId);
        final FacilitiesManagementClient client = this.clientFactory.from(FacilitiesManagementClient.class);
        
        try {
            final String result = client.bossKeyProgress(String.valueOf(customer.getId())).execute().toString(Charset.defaultCharset());
            final BossKeyProgress progress = this.json.deserialize(result, BossKeyProgress.class);
            
            if (progress.getDateGenerated() != null) {
                progress.setDateGenerated(convertToCustomerTimeZoneString(progress.getDateGenerated()));
            }
            return this.json.serialize(progress);
            
        } catch (HystrixRuntimeException | JsonException | ParseException hre) {
            LOG.error(DeviceGroupConstants.COMMUNICATION_FAILURE_WITH_FMSERVER, hre);
            return WidgetMetricsHelper.convertToJson(errorMessage(null, ErrorConstants.MS_COMM_FAILURE), WebCoreConstants.UI_MESSAGES, true);
        }
        
    }
    
    //check style doesn't understand lambdas 
    @SuppressWarnings({ "checkstyle:indentation" })
    protected final void download(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        final Customer customer = this.customerService.findCustomer(customerId);
        
        final String fileStreamExMessage = "Exception while streaming file from FMS for customer " + customerId;
        
        final FacilitiesManagementClient client = this.clientFactory.from(FacilitiesManagementClient.class);
        final ArrayBlockingQueue<ByteBuf> blockingQueue = new ArrayBlockingQueue<>(128);
        final EmptyByteBuf signalHalt = new EmptyByteBuf(ByteBufAllocator.DEFAULT);
        
        try {
            response.setHeader("Content-Disposition", "attachment; filename=\"BOSS_KEY.zip\"");
            response.setContentType("application/zip");
            
            // this thread puts the received bytes into the queue 
            client.bossKeyDownload(String.valueOf(customer.getId())).observe().subscribe(byteBuf -> {
                if (byteBuf != null && !(byteBuf instanceof EmptyByteBuf)) {
                    blockingQueuePut(byteBuf, blockingQueue, response);
                }
            }, exception -> {
                LOG.error(fileStreamExMessage + customerId, exception);
                blockingQueuePut(signalHalt, blockingQueue, response);
            }, () -> {
                LOG.info("Completed download of boss key for customer " + customerId);
                blockingQueuePut(signalHalt, blockingQueue, response);
            });
            
            // this thread polls the bytes off of the queue and places them into the response
            ByteBuf bytesFromQueue = blockingQueue.take();
            while (!(bytesFromQueue instanceof EmptyByteBuf)) {
                final byte[] bytes;
                if (bytesFromQueue.hasArray()) {
                    bytes = bytesFromQueue.array();
                } else {
                    bytes = new byte[bytesFromQueue.readableBytes()];
                    bytesFromQueue.getBytes(bytesFromQueue.readerIndex(), bytes);
                }
                response.getOutputStream().write(bytes, 0, bytes.length);
                bytesFromQueue.release();
                bytesFromQueue = blockingQueue.take();
            }
            
        } catch (HystrixRuntimeException hre) {
            LOG.error(DeviceGroupConstants.COMMUNICATION_FAILURE_WITH_FMSERVER, hre);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        } catch (IOException | InterruptedException e) {
            LOG.error("Exception while downloading boss key for customer " + customerId, e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        
    }
    
    protected final MessageInfo errorMessage(final String token, final String messageKey) {
        final MessageInfo message = new MessageInfo();
        message.setError(true);
        message.setToken(token);
        message.addMessage(this.commonControllerHelper.getMessage(messageKey));
        return message;
    }
    
    @ModelAttribute(DeviceGroupConstants.FORM_EDIT)
    public final WebSecurityForm<DeviceGroup> initEditForm(final HttpServletRequest request) {
        return new WebSecurityForm<DeviceGroup>((Object) null, new DeviceGroup(),
                SessionTool.getInstance(request).locatePostToken(DeviceGroupConstants.FORM_EDIT));
    }
    
    @ModelAttribute(DeviceGroupConstants.FORM_SEARCH)
    public final WebSecurityForm<DeviceGroupFilter> initSearchForm(final HttpServletRequest request) {
        return new WebSecurityForm<DeviceGroupFilter>((Object) null, new DeviceGroupFilter(),
                SessionTool.getInstance(request).locatePostToken(DeviceGroupConstants.FORM_SEARCH));
    }
    
    @ModelAttribute(DeviceGroupConstants.FORM_BULK_SCHEDULE)
    public final WebSecurityForm<BulkSchedule> initBulkScheduleForm(final HttpServletRequest request) {
        return new WebSecurityForm<BulkSchedule>((Object) null, new BulkSchedule(),
                SessionTool.getInstance(request).locatePostToken(DeviceGroupConstants.FORM_BULK_SCHEDULE));
    }
    
    /**
     * Check if exceptions are caused by com.digitalpaytech.client.util.CommunicationException: Update cannot be forced.
     * 
     * @param exception
     *            the most outer exception.
     * @param cannotUpdateMessageKey
     *            Specific error message key for 'cannot update'.
     * @param generalMessageKey
     *            general error message key.
     */
    public final String causedByScheduledOrIncompleteStatus(final Throwable exception, final String cannotUpdateMessageKey,
        final String generalMessageKey) {
        
        // Unfortunately the exceptions weren't "suppressed" and getSuppressed doesn't apply, I need to use 'getCause'.
        if (exception.getCause() != null && exception.getCause() instanceof HystrixBadRequestException && exception.getCause().getCause() != null
            && exception.getCause().getCause() instanceof CommunicationException
            && ((CommunicationException) exception.getCause().getCause()).getResponseStatus() == RestCoreConstants.FAILURE_INCORRECT_CREDENTIALS) {
            return cannotUpdateMessageKey;
        }
        return generalMessageKey;
    }
    
    private String getDateTime(final String scheduleDate, final String scheduleTime) {
        Date schDate = null;
        try {
            schDate = this.editValidator.getScheduleDate(WebSecurityUtil.getCustomerTimeZone(), scheduleDate, scheduleTime);
        } catch (ParseException pe) {
            LOG.error("Cannot create schedule date object, schedule date: " + scheduleDate + ", time: " + scheduleTime, pe);
            return prepareFailedResponse(DeviceGroupConstants.CONFIGURATION_GROUP_ERROR_KEY);
        }
        final DateFormat dateFormat = new SimpleDateFormat(DateUtil.FMS_SCHEDULE_DATE_TIME_FORMAT);
        return DateUtil.createDateString(dateFormat, schDate);
        
    }
    
    private boolean isUnschedule(final DeviceGroup deviceGroup, final String unsch) {
        return StringUtils.isBlank(deviceGroup.getScheduleDate()) && StringUtils.isBlank(deviceGroup.getScheduleTime())
               || (StringUtils.isNotBlank(unsch) && Boolean.TRUE.toString().equalsIgnoreCase(unsch));
    }
    
    private Map<Integer, MapEntry> buildMapInfo(final Map<Integer, Integer> idUpgradeStatusMap) {
        final Map<Integer, MapEntry> mapEntries = pointOfSaleService
                .findPointOfSaleMapEntryDetails(idUpgradeStatusMap.keySet(), TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone()));
        
        final Iterator<Map.Entry<Integer, MapEntry>> iterator = mapEntries.entrySet().iterator();
        while (iterator.hasNext()) {
            final Map.Entry<Integer, MapEntry> pair = (Entry<Integer, MapEntry>) iterator.next();
            final int severity = idUpgradeStatusMap.get(pair.getKey());
            final MapEntry mapEntry = pair.getValue();
            if (mapEntry != null) {
                mapEntry.setSeverity(severity);
                mapEntry.setAlertIcon(severity);
            }
        }
        return mapEntries;
    }
    
    private String prepareFailedResponse(final String errorMsgKey) {
        final StringBuilder bdr = new StringBuilder("FMS returned ERROR response, error message is: ").append(errorMsgKey);
        LOG.error(bdr.toString());
        this.mailerService.sendAdminErrorAlert("FMS returned ERROR DeviceGroup", bdr.toString(), null);
        return WidgetMetricsHelper.convertToJson(errorMessage(null, errorMsgKey), WebCoreConstants.UI_MESSAGES, true);
    }
    
    private String prepareSuccessfulResponse(final String initToken) {
        return WebCoreConstants.RESPONSE_TRUE + WebCoreConstants.COLON + initToken;
    }
    
    private List<DeviceGroup> randomizeGroupIdsInDeviceGroup(final HttpServletRequest request, final List<DeviceGroup> deviceGroups) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        for (DeviceGroup dg : deviceGroups) {
            dg.setGroupId(keyMapping.getRandomString(dg, FormFieldConstants.PARAMETER_GROUP_ID));
        }
        return deviceGroups;
    }
    
    private List<UpgradeDisplayGroup> randomizeGroupIdsInUpgradeDisplayGroup(final HttpServletRequest request,
        final List<UpgradeDisplayGroup> upgradeDisplayGroups) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        for (UpgradeDisplayGroup upgGroup : upgradeDisplayGroups) {
            final DeviceGroup dg = new DeviceGroup();
            dg.setGroupId(upgGroup.getGroupId());
            upgGroup.setGroupId(keyMapping.getRandomString(dg, FormFieldConstants.PARAMETER_GROUP_ID));
        }
        return upgradeDisplayGroups;
    }
    
    private void blockingQueuePut(final ByteBuf byteBuf, final ArrayBlockingQueue<ByteBuf> blockingQueue, final HttpServletResponse response) {
        try {
            blockingQueue.put(byteBuf);
        } catch (InterruptedException e) {
            LOG.error(DeviceGroupConstants.COMMUNICATION_FAILURE_WITH_FMSERVER, e);
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }
    
    private String convertToCustomerTimeZoneString(final String dateStringUTC) throws ParseException {
        final DateFormat dateFormat = new SimpleDateFormat(StableDateUtil.ISO_DATE_TIME_FORMAT, Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getTimeZone(StableDateUtil.UTC));
        final Date dateGeneratedUtc = dateFormat.parse(dateStringUTC);
        dateFormat.setTimeZone(TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone()));
        return dateFormat.format(dateGeneratedUtc);
    }
    
    private static void debugDateStrings(final String before, final String after, final boolean result) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("Before/after date string: ").append(before).append(" / ").append(after).append(", result: ").append(result);
        LOG.debug(bdr.toString());
    }
}
