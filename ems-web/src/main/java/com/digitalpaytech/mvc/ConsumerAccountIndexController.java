package com.digitalpaytech.mvc;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;


@Controller
public class ConsumerAccountIndexController {    
    
    /**
     * This method will look at the permissions in the WebUser and pass control
     * to the first tab that the user has permissions for.
     * 
     * @return Will return the method for definedAlerts, or error if no
     *         permissions exist.
     */
    @RequestMapping(value = "/secure/accounts/index.html", method = RequestMethod.GET)
    public String getFirstPermittedTab(HttpServletRequest request, HttpServletResponse response, ModelMap model) {

        if (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_CONSUMER_ACCOUNTS)
                || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_CONSUMER_ACCOUNTS)) {
            return "redirect:/secure/accounts/consumerAccounts/index.html";
        } else if (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_COUPONS)
                || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_COUPONS)) {
            return "redirect:/secure/accounts/coupons/index.html";
        } else if (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_CUSTOM_CARDS)
                || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_CUSTOM_CARDS)) {
            return "redirect:/secure/accounts/customerCards/index.html";
        }  else {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
    }   
}
