package com.digitalpaytech.mvc;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import com.digitalpaytech.util.DateUtil;

public class FormBinder {
    @InitBinder
    public void initBinder(final WebDataBinder webDataBinder) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(DateUtil.UI_FORMAT);
        dateFormat.setLenient(false);
        
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
}
