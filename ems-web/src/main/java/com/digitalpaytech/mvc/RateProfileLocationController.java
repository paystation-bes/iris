package com.digitalpaytech.mvc;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.RateProfile;
import com.digitalpaytech.domain.RateProfileLocation;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.helper.RateProfileHelper;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.RateProfileLocationForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.RateProfileLocationService;
import com.digitalpaytech.service.RateProfileService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.PooledResource;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.RateProfileLocationValidator;

@Controller
public class RateProfileLocationController {
    public static final String FORM_EDIT_PROFILE = "rateProfileLocationForm";
    public static final String FORM_EDIT_LOCATION = "rateProfileLocationFormForLocation";
    public static final String LIST_RATE_PROFILE_LOCATION = "rateProfileLocationList";
    
    @Autowired
    private RateProfileLocationService rateProfileLocationService;
    
    @Autowired
    private RateProfileService rateProfileService;
    
    @Autowired
    private LocationService locationService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    @Autowired
    private RateProfileLocationValidator rateProfileLocationValidator;
    
    @Autowired
    private RateProfileHelper rateProfileHelper;
    
    public final String listByRateProfileSuper(final HttpServletRequest request, final HttpServletResponse response, final String rateProfileRandomId) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final Integer rateProfileId = keyMapping.getKey(rateProfileRandomId, RateProfile.class, Integer.class);
        if (rateProfileId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final List<RateProfileLocationForm> formList = this.rateProfileHelper.aggregateRateProfileLocationList(this.rateProfileLocationService
                .findByRateProfileId(rateProfileId), this.rateProfileHelper.getRateProfileSessionData(request, rateProfileRandomId), keyMapping);
        
        return WidgetMetricsHelper.convertToJson(formList, LIST_RATE_PROFILE_LOCATION, true);
    }
    
    public final String listByLocationSuper(final HttpServletRequest request, final HttpServletResponse response, final String locationRandomId) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final Integer locationId = keyMapping.getKey(locationRandomId, Location.class, Integer.class);
        if (locationId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        
        final List<RateProfileLocationForm> formList = this.rateProfileHelper
                .aggregateRateProfileLocationList(this.rateProfileLocationService.findPublishedByLocationId(locationId, customerId),
                                                  this.rateProfileHelper.getLocationSessionData(request, locationRandomId), keyMapping);
        
        return WidgetMetricsHelper.convertToJson(formList, LIST_RATE_PROFILE_LOCATION, true);
    }
    
    public final String deleteFromRateProfileSuper(final HttpServletRequest request, final HttpServletResponse response,
        final String rateProfileRandomId, final String randomId) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final Integer rateProfileLocationId = keyMapping.getKey(randomId, RateProfileLocation.class, Integer.class);
        if (rateProfileLocationId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return deleteRateProfileLocation(this.rateProfileHelper.createRateProfileSessionDataIfAbsent(request, rateProfileRandomId),
                                         rateProfileLocationId, randomId, keyMapping);
    }
    
    public final String deleteFromLocationSuper(final HttpServletRequest request, final HttpServletResponse response, final String locationRandomId,
        final String randomId) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final Integer rateProfileLocationId = keyMapping.getKey(randomId, RateProfileLocation.class, Integer.class);
        if (rateProfileLocationId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return deleteRateProfileLocation(this.rateProfileHelper.createLocationSessionDataIfAbsent(request, locationRandomId), rateProfileLocationId,
                                         randomId, keyMapping);
    }
    
    public final String saveToRateProfileSuper(final HttpServletRequest request, final HttpServletResponse response,
        final WebSecurityForm<RateProfileLocationForm> webSecurityForm, final BindingResult result) {
        this.rateProfileLocationValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        return saveRateProfileLocation(this.rateProfileHelper.createRateProfileSessionDataIfAbsent(request, webSecurityForm.getWrappedObject()
                .getRateProfileRandomId()), webSecurityForm, request);
    }
    
    public final String saveToLocationSuper(final HttpServletRequest request, final HttpServletResponse response,
        final WebSecurityForm<RateProfileLocationForm> webSecurityForm, final BindingResult result) {
        this.rateProfileLocationValidator.validateInLocationPage(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        return saveRateProfileLocation(this.rateProfileHelper.createLocationSessionDataIfAbsent(request, webSecurityForm.getWrappedObject()
                .getLocationRandomId()), webSecurityForm, request);
    }
    
    public final String saveAssignedToLocationSuper(final HttpServletRequest request, final HttpServletResponse response,
        final String locationRandomId) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        final RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        
        final Integer locationId = keyMapping.getKey(locationRandomId, Location.class, Integer.class);
        if (locationId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Date now = new Date();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final Customer customer = this.customerService.findCustomer(this.commonControllerHelper.resolveCustomerId(request, response));
        
        final Map<String, RateProfileLocationForm> sessionData = this.rateProfileHelper.getLocationSessionData(request, locationRandomId);
        final List<RateProfileLocation> updatedRates = new ArrayList<RateProfileLocation>(sessionData.size());
        for (String randomId : sessionData.keySet()) {
            final RateProfileLocation domain = this.rateProfileHelper.populate(sessionData.get(randomId), new RateProfileLocation(), keyMapping,
                                                                               customer);
            domain.setLastModifiedByUserId(userAccount.getId());
            domain.setLastModifiedGmt(now);
            
            updatedRates.add(domain);
        }
        
        this.rateProfileLocationService.saveAll(updatedRates, customer.getId());
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    @ModelAttribute(FORM_EDIT_PROFILE)
    public final WebSecurityForm<RateProfileLocationForm> initEditProfileForm(final HttpServletRequest request) {
        return new WebSecurityForm<RateProfileLocationForm>((Object) null, new RateProfileLocationForm(), SessionTool.getInstance(request)
                .locatePostToken(FORM_EDIT_PROFILE));
    }
    
    @ModelAttribute(FORM_EDIT_LOCATION)
    public final WebSecurityForm<RateProfileLocationForm> initEditLocationForm(final HttpServletRequest request) {
        return new WebSecurityForm<RateProfileLocationForm>((Object) null, new RateProfileLocationForm(), SessionTool.getInstance(request)
                .locatePostToken(FORM_EDIT_LOCATION));
    }
    
    private String deleteRateProfileLocation(final Map<String, RateProfileLocationForm> sessionData, final int rateProfileLocationId,
        final String randomId, final RandomKeyMapping keyMapping) {
        if (rateProfileLocationId < 0) {
            sessionData.remove(randomId);
        } else {
            RateProfileLocationForm form = sessionData.get(randomId);
            if (form == null) {
                final RateProfileLocation domain = this.rateProfileLocationService.findById(rateProfileLocationId);
                if (domain == null) {
                    return WebCoreConstants.RESPONSE_FALSE;
                }
                
                final PooledResource<DateFormat> dateFormat = DateUtil.takeDateFormat(DateUtil.DATE_UI_FORMAT, "GMT");
                form = this.rateProfileHelper.populate(domain, new RateProfileLocationForm(), keyMapping, dateFormat.get());
                dateFormat.close();
                
                sessionData.put(randomId, form);
            }
            
            form.setIsDeleted(true);
        }
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    private String saveRateProfileLocation(final Map<String, RateProfileLocationForm> sessionData,
        final WebSecurityForm<RateProfileLocationForm> webSecurityForm, final HttpServletRequest request) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        final RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        
        final RateProfileLocationForm form = webSecurityForm.getWrappedObject();
        if (StringUtils.isBlank(form.getRandomId())) {
            while (StringUtils.isBlank(form.getRandomId())) {
                form.setRandomId(keyMapping.getRandomString(RateProfileLocation.class, sessionTool.getTemporaryIntId()));
                if (sessionData.containsKey(form.getRandomId())) {
                    form.setRandomId((String) null);
                }
            }
        }
        
        sessionData.put(form.getRandomId(), form);
        
        return new StringBuilder().append(WebCoreConstants.RESPONSE_TRUE).append(WebCoreConstants.COLON).append(webSecurityForm.getInitToken())
                .append(WebCoreConstants.COLON).append(form.getRandomId()).toString();
    }
}
