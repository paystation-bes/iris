package com.digitalpaytech.mvc.exception;

import org.springframework.security.core.AuthenticationException;

public class ServiceAgreementException extends AuthenticationException {

	private static final long serialVersionUID = 986157419267600692L;

	public ServiceAgreementException(String msg) {
		super(msg);
	}
	
	public ServiceAgreementException(String msg, Throwable t) {
        super(msg, t);
    }
}
