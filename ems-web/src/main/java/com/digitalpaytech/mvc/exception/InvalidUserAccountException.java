package com.digitalpaytech.mvc.exception;

import java.util.List;

public class InvalidUserAccountException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7490879291254044568L;
	private List<String> msgKeys;
	
	public InvalidUserAccountException() {
	}
	
	public InvalidUserAccountException(String msg) {
		super(msg);
	}
	
	public InvalidUserAccountException(String msg, Throwable t) {
		super(msg, t);
	}
	
	public InvalidUserAccountException(Throwable t) {
		super(t);
	}
	
	public InvalidUserAccountException(String msg, Throwable t, List<String> msgKeys) {
		this(msg, t);
		this.msgKeys = msgKeys;
	}
	
	public InvalidUserAccountException(String msg, List<String> msgKeys) {
		this(msg);
		this.msgKeys = msgKeys;
	}
	
	public List<String> getMessageKeys() {
		return msgKeys;
	}
}
