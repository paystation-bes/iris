package com.digitalpaytech.mvc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.domain.AlertClassType;
import com.digitalpaytech.domain.CardProcessMethodType;
import com.digitalpaytech.domain.CitationType;
import com.digitalpaytech.domain.CollectionType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.RevenueType;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.Section;
import com.digitalpaytech.domain.Tab;
import com.digitalpaytech.domain.TransactionType;
import com.digitalpaytech.domain.UnifiedRate;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetChartType;
import com.digitalpaytech.domain.WidgetFilterType;
import com.digitalpaytech.domain.WidgetLimitType;
import com.digitalpaytech.domain.WidgetMetricType;
import com.digitalpaytech.domain.WidgetRangeType;
import com.digitalpaytech.domain.WidgetSubsetMember;
import com.digitalpaytech.domain.WidgetTierType;
import com.digitalpaytech.dto.AlertWidgetType;
import com.digitalpaytech.dto.CardProcMethodType;
import com.digitalpaytech.dto.CitatType;
import com.digitalpaytech.dto.CollType;
import com.digitalpaytech.dto.DisplayType;
import com.digitalpaytech.dto.FilterType;
import com.digitalpaytech.dto.LocationType;
import com.digitalpaytech.dto.MerchType;
import com.digitalpaytech.dto.MetricType;
import com.digitalpaytech.dto.OrganizationType;
import com.digitalpaytech.dto.RangeType;
import com.digitalpaytech.dto.RateType;
import com.digitalpaytech.dto.RevType;
import com.digitalpaytech.dto.RouteType;
import com.digitalpaytech.dto.TabHeading;
import com.digitalpaytech.dto.TierType;
import com.digitalpaytech.dto.TxnType;
import com.digitalpaytech.dto.WidgetMetrics;
import com.digitalpaytech.dto.WidgetSettingsOptions;
import com.digitalpaytech.dto.comparator.SettingsTypeComparator;
import com.digitalpaytech.exception.WebWidgetServiceException;
import com.digitalpaytech.mvc.exception.InvalidUserAccountException;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.mvc.support.WidgetSettingsForm;
import com.digitalpaytech.service.CitationTypeService;
import com.digitalpaytech.service.DashboardService;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.util.DashboardUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.WebWidgetUtil;
import com.digitalpaytech.util.WidgetConstants;
import com.digitalpaytech.validation.WidgetSettingsValidator;

/**
 * WidgetSettingsController gets the request for retrieving, editing, and
 * applying dashboard widget settings and pass them back to the frond end UI.
 * 
 * @author David Clarke
 */
@Controller
@SessionAttributes({ WebSecurityConstants.WEB_SECURITY_FORM })
public class WidgetSettingsController extends CommonLandingController {
    
    private static Logger log = Logger.getLogger(WidgetSettingsController.class);
    
    @Autowired
    private EntityService entityService;
    @Autowired
    private DashboardService dashboardService;
    @Autowired
    private WidgetSettingsValidator widgetSettingsValidator;
    @Autowired
    private MerchantAccountService merchantAccountService;
    @Autowired
    private CitationTypeService citationTypeService;
    /* Injects DashboardController for 'loadTabTreeToMapIfNecessary' method */
    @Autowired
    private DashboardController dashboardController;
    
    private WidgetMetrics widgetMetrics;
    private WidgetMetrics widgetMetricsParent;
    
    private Map<Integer, String> widgetTierTypesMap;
    private Map<Integer, String> widgetDurationTypesMap;
    private Map<Integer, String> widgetMetricTypesMap;
    private Map<Integer, String> widgetFilterTypesMap;
    private Map<Integer, String> widgetChartTypesMap;
    private Map<Integer, Integer> widgetLimitTypesMap;
    
    public final void setDashboardService(final DashboardService dashboardService) {
        this.dashboardService = dashboardService;
    }
    
    public final void setEntityService(final EntityService entityService) {
        this.entityService = entityService;
    }
    
    public final void setMerchantAccountService(final MerchantAccountService merchantAccountService) {
        this.merchantAccountService = merchantAccountService;
    }
    
    public final void setCitationTypeService(final CitationTypeService citationTypeService) {
        this.citationTypeService = citationTypeService;
    }
    
    public final void setDashboardController(final DashboardController dashboardController) {
        this.dashboardController = dashboardController;
    }
    
    public final void setWidgetSettingsValidator(final WidgetSettingsValidator widgetSettingsValidator) {
        this.widgetSettingsValidator = widgetSettingsValidator;
    }
    
    public final void setWidgetMetrics(final WidgetMetrics widgetMetrics) {
        this.widgetMetrics = widgetMetrics;
    }
    
    public final void setWidgetMetricsParent(final WidgetMetrics widgetMetricsParent) {
        this.widgetMetricsParent = widgetMetricsParent;
    }
    
    public final void setWidgetTierTypesMap(final Map<Integer, String> widgetTierTypesMap) {
        this.widgetTierTypesMap = widgetTierTypesMap;
    }
    
    public final void setWidgetFilterTypesMap(final Map<Integer, String> widgetFilterTypesMap) {
        this.widgetFilterTypesMap = widgetFilterTypesMap;
    }
    
    @PostConstruct
    private void loadWidgetComponents() {
        WidgetMetricsHelper.registerComponents();
        final String child = "/widgetmetrics.xml";
        this.widgetMetrics = WidgetMetricsHelper.loadWidgetMetricsXml(child, WidgetController.class.getResourceAsStream(child));
        
        final String parent = "/widgetmetricsParent.xml";
        this.widgetMetricsParent = WidgetMetricsHelper.loadWidgetMetricsXml(parent, WidgetController.class.getResourceAsStream(parent));
        log.info("widgetmetrics.xml, widgetmetricsParent.xml are loaded, and reigstered widget components");
        
        //Counts number of widget configurations, not necessary, just an interesting factoid
        /*
         * System.out.println("++++++++++++++++++++++++++++++++++++++++");
         * int count = 0;
         * for(Metric metric: widgetMetrics.getMetrics()){
         * for(Range range: metric.getRanges()){
         * String[] typeArray = range.getType().split(",");
         * int rangeCount = 0;
         * for(Filter filter: range.getFilters()){
         * for(Tier1 tier1: filter.getTier1s()){
         * for(Tier2 tier2: tier1.getTier2s()){
         * for(Tier3 tier3: tier2.getTier3s()){
         * for(Display display: tier3.getDisplays()){
         * String[] displayTypeArray = display.getType().split(",");
         * rangeCount += displayTypeArray.length;
         * }
         * }
         * }
         * }
         * }
         * count += rangeCount * (typeArray.length);
         * }
         * }
         * System.out.println(count);
         * System.out.println("++++++++++++++++++++++++++++++++++++++++");
         */
    }
    
    /**
     * Validates widgetId and returns template location.
     * 
     * @return String Widget Settings VM file location.
     */
    @RequestMapping(value = "/secure/dashboard/showWidgetSettings.html", method = RequestMethod.GET)
    public final String showWidgetSettings(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String strWidgetId = request.getParameter("widgetID");
        if (!DashboardUtil.validateRandomId(strWidgetId)) {
            log.warn("+++ Invalid randomised widgetID in MainLandingController.getActualWidgetData() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }
        
        final SessionTool sessionTool = SessionTool.getInstance(request);
        sessionTool.setAttribute(WebCoreConstants.SESSION_WIDGET_ID_CURRENT, strWidgetId);
        setNewWidgetSettingsForm(request, model);
        
        return "/dashboard/include/widgetSettings";
    }
    
    /**
     * Resets widget settings options back to default.
     * 
     * @param request
     *            HttpServletReqeust For checking if WidgetSettingsOptions
     *            object is in the session.
     * @param model
     * @return String
     */
    @ResponseBody
    @RequestMapping(value = "/secure/dashboard/resetWidgetSettings.html", method = RequestMethod.GET)
    public final String resetWidgetSettingsOptions(final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String json = resetWidgetSettingsData(request);
        return json;
    }
    
    /**
     * Prepares options for default Widget Settings page if widgetId is the only parameter.
     * 
     * @param request
     * @return String JSON representation of WidgetSettingsOptions
     * @throws InvalidUserAccountException
     */
    @ResponseBody
    @RequestMapping(value = "/secure/dashboard/updateWidgetSettings.html", method = RequestMethod.GET)
    public final String updateWidgetSettingsOptions(final HttpServletRequest request, final HttpServletResponse response) throws InvalidUserAccountException {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        validateTokenAndMetrics(request);
        validateWidgetRandomId(request);
        
        loadWidgetDurationMapIfNecessary();
        loadWidgetTierTypesMapIfNecessary();
        loadWidgetFilterTypesMapIfNecessary();
        loadWidgetMetricTypesMapIfNecessary();
        loadWidgetChartTypesMapIfNecessary();
        
        this.widgetSettingsValidator.setWidgetMetrics(getWidgetMetrics());
        final String json = resetWidgetSettingsData(request);
        return json;
    }
    
    /**
     * Prepares options for default Widget Settings page using the widgetId parameter and
     * widgetSettingsForm passed in a WebSecurityForm
     * 
     * @param webSecurityForm
     *            WebSecurityConstants WidgetSettingsForm is encapsulated within.
     * @param result
     * @param request
     * @param model
     * @return String JSON representation of WidgetSettingsOptions
     */
    @ResponseBody
    @RequestMapping(value = "/secure/dashboard/updateWidgetSettings.html", method = RequestMethod.POST)
    public final String updateWidgetSettingsOptions(@ModelAttribute(WebSecurityConstants.WEB_SECURITY_FORM) final WebSecurityForm<WidgetSettingsForm> webSecurityForm
            , final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        final WidgetSettingsForm widgetSettingsForm = webSecurityForm.getWrappedObject();
        final String wRandomId = widgetSettingsForm.getRandomId();
        if (StringUtils.isBlank(wRandomId)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        final Widget widget = getWidgetFromWidgetRandomId(request, wRandomId);
        if (widget == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        this.widgetSettingsValidator.validate(webSecurityForm, result, widget, false);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        //      if (result.getErrorCount() > 0) {
        //          webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
        //          webSecurityForm.resetToken();
        //            //TODO Remove SessionToken
        ////            model.put(WebSecurityConstants.SESSION_TOKEN, sessionTool.getAttribute(WebSecurityConstants.SESSION_TOKEN));
        //          model.put("errorField", webSecurityForm.getErrorField());
        //          return WebCoreConstants.RESPONSE_FALSE;
        //      }
        
        final RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        WidgetSettingsOptions settingsOptions = (WidgetSettingsOptions) sessionTool.getAttribute(WebCoreConstants.SESSION_WIDGET_SETTINGS_OPTIONS);
        settingsOptions = this.widgetSettingsValidator.updateWidgetSettingsOptions(webSecurityForm, settingsOptions, result, keyMapping);
        
        sessionTool.setAttribute(WebCoreConstants.SESSION_WIDGET_SETTINGS_OPTIONS, settingsOptions);
        final String json = WidgetMetricsHelper.convertToJson(settingsOptions, "widgetSettingsOptions", true);
        return json;
    }
    
    /**
     * applyWidgetSettings method is called when user selects all options in
     * Widget Settings page and clicks Apply button to submit the form. Validate
     * the form using WidgetSettingsValidator and updates Widget object if
     * everything is ok.
     * 
     * @param webSecurityForm
     *            WebSecurityConstants WidgetSettingsForm is encapsulated
     *            within.
     * @return String Returns WebCoreConstants.RESPONSE_FALSE if it's invalid.
     * @throws InvalidUserAccountException
     *             Tokens in request and session are not the same.
     */
    @ResponseBody
    @RequestMapping(value = "/secure/dashboard/applyWidgetSettings.html", method = RequestMethod.POST)
    public final String applyWidgetSettings(@ModelAttribute(WebSecurityConstants.WEB_SECURITY_FORM) final WebSecurityForm<WidgetSettingsForm> webSecurityForm
            , final BindingResult result, final HttpServletRequest request, final HttpServletResponse response
            , final ModelMap model) throws InvalidUserAccountException {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        validateTokenAndMetrics(request);
        
        loadWidgetDurationMapIfNecessary();
        loadWidgetTierTypesMapIfNecessary();
        loadWidgetFilterTypesMapIfNecessary();
        loadWidgetMetricTypesMapIfNecessary();
        loadWidgetChartTypesMapIfNecessary();
        loadWidgetLimitTypesMapIfNecessary();
        
        final WidgetSettingsForm widgetSettingsForm = webSecurityForm.getWrappedObject();
        final String wRandomId = widgetSettingsForm.getRandomId();
        if (StringUtils.isBlank(wRandomId)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        final Widget widget = getWidgetFromWidgetRandomId(request, wRandomId);
        if (widget == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        //***********************
        //TODO: remove after UI returns proper chart type for map widget
        if (widgetSettingsForm.getMetricName().equals(this.widgetMetricTypesMap.get(WidgetConstants.METRIC_TYPE_MAP))) {
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            widgetSettingsForm.setChartType(keyMapping.getRandomString(this.dashboardService.findObjectByClassTypeAndId(WidgetChartType.class,
                                                                                                                   WidgetConstants.CHART_TYPE_MAP), "id"));
        }
        //***********************
        
        this.widgetSettingsValidator.setWidgetMetrics(getWidgetMetrics());
        this.widgetSettingsValidator.validate(webSecurityForm, result, widget, true);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        //      SessionTool sessionTool = SessionTool.getInstance(request);
        //      if (result.getErrorCount() > 0 || !webSecurityForm.getErrorField().isEmpty()) {
        //          webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
        //          webSecurityForm.resetToken();
        //            //TODO Remove SessionToken
        ////            model.put(WebSecurityConstants.SESSION_TOKEN, sessionTool.getAttribute(WebSecurityConstants.SESSION_TOKEN));
        //          model.put("errorField", webSecurityForm.getErrorField());
        //          return WebCoreConstants.RESPONSE_FALSE;
        //      }
        
        updateWidgetObject(request, webSecurityForm.getWrappedObject());
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    /**
     * Resets WidgetSettingsOptions properties and populates with default data.
     * The default data will have statuses set to SELECTED, ACTIVE, INACTIVE, or DISABLED based on the current settings in the widget
     * WidgetSettingsOptions will then be placed in session and a JSON response will be created and returned.
     * 
     * @return String Default WidgetSettingsOption data in JSON document
     */
    private String resetWidgetSettingsData(final HttpServletRequest request) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        WidgetSettingsOptions settingsOptions = (WidgetSettingsOptions) sessionTool.getAttribute(WebCoreConstants.SESSION_WIDGET_SETTINGS_OPTIONS);
        if (settingsOptions != null) {
            settingsOptions.reset();
        }
        settingsOptions = getDefaultWidgetSettingsOptions(request, settingsOptions);
        sessionTool.setAttribute(WebCoreConstants.SESSION_WIDGET_SETTINGS_OPTIONS, settingsOptions);
        
        final String json = WidgetMetricsHelper.convertToJson(settingsOptions, "widgetSettingsOptions", true);
        return json;
    }
    
    public final ModelMap setNewWidgetSettingsForm(final HttpServletRequest request, final ModelMap model) {
        // public WebSecurityForm(Object primaryKey, Object wrappedObject)
        final SessionTool sessionTool = SessionTool.getInstance(request);
        final RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        final String widgetId = (String) sessionTool.getAttribute(WebCoreConstants.SESSION_WIDGET_ID_CURRENT);
        
        final WidgetSettingsForm widgetSettingsForm = new WidgetSettingsForm();
        widgetSettingsForm.setRandomId(widgetId);
        
        if (!isInteger(String.valueOf(keyMapping.getKey(widgetId)))) {
            //Widget is temporary!
            final Section sec = getSectionFromWidgetRandomId(request, widgetId);
            widgetSettingsForm.setMetricName(sec.getWidget(widgetId).getWidgetMetricType().getName());
        } else {
            widgetSettingsForm.setMetricName(this.dashboardService.findWidgetById((Integer) keyMapping.getKey(widgetId)).getWidgetMetricType().getName());
        }
        
        final WebSecurityForm<WidgetSettingsForm> secForm = new WebSecurityForm<WidgetSettingsForm>(null, widgetSettingsForm);
        secForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        //TODO Remove SessionToken
        //      model.put(WebSecurityConstants.SESSION_TOKEN, sessionTool.getAttribute(WebSecurityConstants.SESSION_TOKEN));
        model.put(WebSecurityConstants.WEB_SECURITY_FORM, secForm);
        return model;
    }
    
    /**
     * 1. Gets Widget out from Tab, Section, using Tab & Widget randomId.
     * 2. Puts data from WidgetSettingsForm to Widget.
     * 3. Places back Tab into java.util.Map.
     */
    private void updateWidgetObject(final HttpServletRequest request, final WidgetSettingsForm widgetSettingsForm) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        final RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        
        final WidgetSettingsOptions settingsOptions = (WidgetSettingsOptions) sessionTool.getAttribute(WebCoreConstants.SESSION_WIDGET_SETTINGS_OPTIONS);
        
        loadWidgetDurationMapIfNecessary();
        loadWidgetTierTypesMapIfNecessary();
        loadWidgetFilterTypesMapIfNecessary();
        loadWidgetMetricTypesMapIfNecessary();
        loadWidgetChartTypesMapIfNecessary();
        loadWidgetLimitTypesMapIfNecessary();
        
        final Section sec = getSectionFromWidgetRandomId(request, widgetSettingsForm.getRandomId());
        
        // Sets user selection back to Widget.
        sec.getWidget(widgetSettingsForm.getRandomId()).setName(widgetSettingsForm.getWidgetName());
        sec.getWidget(widgetSettingsForm.getRandomId()).setLastModifiedGmt(new GregorianCalendar().getTime());
        
        if (isIntegerOrFloat(widgetSettingsForm.getTargetValue()) && !widgetSettingsForm.getTargetValue().isEmpty()) {
            //TODO: widget trend amount is an int, should it be a float? turnover uses precise numbers; ex. 1.75            
            try {
                sec.getWidget(widgetSettingsForm.getRandomId()).setTrendAmount((long) (Float.valueOf(widgetSettingsForm.getTargetValue()) * 100));
                sec.getWidget(widgetSettingsForm.getRandomId()).setIsTrendByLocation(false);
            } catch (NumberFormatException e) {
                sec.getWidget(widgetSettingsForm.getRandomId()).setIsTrendByLocation(false);
            }
        } else if (widgetSettingsForm.getTargetValue().equals("true")) {
            sec.getWidget(widgetSettingsForm.getRandomId()).setTrendAmount(0L);
            sec.getWidget(widgetSettingsForm.getRandomId()).setIsTrendByLocation(true);
        } else {
            sec.getWidget(widgetSettingsForm.getRandomId()).setTrendAmount(0L);
            sec.getWidget(widgetSettingsForm.getRandomId()).setIsTrendByLocation(false);
        }
        
        /* metric type should not be changing in current implementation */
        //int metricId = (Integer) keyMapping.getKey(widgetSettingsForm.getMetricName());
        //WidgetMetricType newMetric = dashboardService.findObjectByClassTypeAndId(WidgetMetricType.class, metricId);
        //sec.getWidget(widgetSettingsForm.getRandomId()).setWidgetMetricType(newMetric);
        
        final int rangeId = (Integer) keyMapping.getKey(widgetSettingsForm.getRange());
        final WidgetRangeType newRange = this.dashboardService.findObjectByClassTypeAndId(WidgetRangeType.class, rangeId);
        sec.getWidget(widgetSettingsForm.getRandomId()).setWidgetRangeType(newRange);
        
        /* tier 1 type should only change if the filter type is not "All" or "Subset" or the widget is a map */
        final int tier1Id = (Integer) keyMapping.getKey(widgetSettingsForm.getTier1());
        final WidgetTierType newTier1 = this.dashboardService.findObjectByClassTypeAndId(WidgetTierType.class, tier1Id);
        sec.getWidget(widgetSettingsForm.getRandomId()).setWidgetTierTypeByWidgetTier1Type(newTier1);
        
        final Widget widget = sec.getWidget(widgetSettingsForm.getRandomId());
        final Set<WidgetSubsetMember> widgetSubsetMembers = new HashSet<WidgetSubsetMember>();
        
        int filterId = 0;
        if (widgetSettingsForm.getFilter().isEmpty()) {
            filterId = WidgetConstants.FILTER_TYPE_ALL;
        } else {
            filterId = (Integer) keyMapping.getKey(widgetSettingsForm.getFilter());
        }
        WidgetFilterType newFilter = this.dashboardService.findObjectByClassTypeAndId(WidgetFilterType.class, filterId);
        sec.getWidget(widgetSettingsForm.getRandomId()).setWidgetFilterType(newFilter);
        
        final int subsetFilterId = WidgetConstants.FILTER_TYPE_SUBSET;
        newFilter = this.dashboardService.findObjectByClassTypeAndId(WidgetFilterType.class, subsetFilterId);
        
        if (widgetSettingsForm.getTier1Attr().length() > 0 && widgetSettingsForm.getTier1Attr().length() < subsetLength(settingsOptions, tier1Id)) {
            final String[] randomTier1AttrIds = widgetSettingsForm.getTier1Attr().split(",");
            widgetSubsetMembers.addAll(setTierSubsetSelection(widget, tier1Id, randomTier1AttrIds, keyMapping));
            sec.getWidget(widgetSettingsForm.getRandomId()).setIsSubsetTier1(true);
            sec.getWidget(widgetSettingsForm.getRandomId()).setWidgetFilterType(newFilter);
        } else {
            sec.getWidget(widgetSettingsForm.getRandomId()).setIsSubsetTier1(false);
        }
        
        if ((widgetSettingsForm.getTier2() != null) && (widgetSettingsForm.getTier2().trim().length() > 0)) {
            final int tier2Id = (Integer) keyMapping.getKey(widgetSettingsForm.getTier2());
            final WidgetTierType newTier2 = this.dashboardService.findObjectByClassTypeAndId(WidgetTierType.class, tier2Id);
            sec.getWidget(widgetSettingsForm.getRandomId()).setWidgetTierTypeByWidgetTier2Type(newTier2);
            
            if (widgetSettingsForm.getTier2Attr().length() > 0 && widgetSettingsForm.getTier2Attr().length() < subsetLength(settingsOptions, tier2Id)) {
                final String[] randomTier2AttrIds = widgetSettingsForm.getTier2Attr().split(",");
                widgetSubsetMembers.addAll(setTierSubsetSelection(widget, tier2Id, randomTier2AttrIds, keyMapping));
                sec.getWidget(widgetSettingsForm.getRandomId()).setIsSubsetTier2(true);
                sec.getWidget(widgetSettingsForm.getRandomId()).setWidgetFilterType(newFilter);
            } else {
                sec.getWidget(widgetSettingsForm.getRandomId()).setIsSubsetTier2(false);
            }
        }
        
        if ((widgetSettingsForm.getTier3() != null) && (widgetSettingsForm.getTier3().trim().length() > 0)) {
            final int tier3Id = (Integer) keyMapping.getKey(widgetSettingsForm.getTier3());
            final WidgetTierType newTier3 = this.dashboardService.findObjectByClassTypeAndId(WidgetTierType.class, tier3Id);
            sec.getWidget(widgetSettingsForm.getRandomId()).setWidgetTierTypeByWidgetTier3Type(newTier3);
            
            if (widgetSettingsForm.getTier3Attr().length() > 0 && widgetSettingsForm.getTier2Attr().length() < subsetLength(settingsOptions, tier3Id)) {
                final String[] randomTier3AttrIds = widgetSettingsForm.getTier3Attr().split(",");
                widgetSubsetMembers.addAll(setTierSubsetSelection(widget, tier3Id, randomTier3AttrIds, keyMapping));
                sec.getWidget(widgetSettingsForm.getRandomId()).setIsSubsetTier3(true);
                sec.getWidget(widgetSettingsForm.getRandomId()).setWidgetFilterType(newFilter);
            } else {
                sec.getWidget(widgetSettingsForm.getRandomId()).setIsSubsetTier3(false);
            }
        }
        
        //Override the current subset members
        final List<WidgetSubsetMember> oldWidgetSubsetMembers = sec.getWidget(widgetSettingsForm.getRandomId()).getWidgetSubsetMembers(); 
        final List<WidgetSubsetMember> newWidgetSubsetMembers = new ArrayList<WidgetSubsetMember>();
        //Use the same subsetmember objects that are still selected
        for (WidgetSubsetMember oldSubsetMember : oldWidgetSubsetMembers) {
            boolean removeMember = true;
            for (WidgetSubsetMember subsetMember : widgetSubsetMembers) {
                if (subsetMember.getMemberId() == oldSubsetMember.getMemberId()
                    && subsetMember.getWidgetTierType().getId().equals(oldSubsetMember.getWidgetTierType().getId())) {
                    newWidgetSubsetMembers.add(oldSubsetMember);
                    removeMember = false;
                    break;
                }
            }
            if (removeMember && oldSubsetMember.getId() != null) {
                oldSubsetMember.setWidget(null);
                @SuppressWarnings("unchecked")
                List<Integer> subsetMemberIds = (List<Integer>) sessionTool.getAttribute(WebCoreConstants.SESSION_SUBSET_MEMBER_IDS_TO_BE_DELETED);
                if (subsetMemberIds == null) {
                    subsetMemberIds = new ArrayList<Integer>();
                }
                subsetMemberIds.add(oldSubsetMember.getId());
                sessionTool.setAttribute(WebCoreConstants.SESSION_SUBSET_MEMBER_IDS_TO_BE_DELETED, subsetMemberIds);
            }
        }
        for (WidgetSubsetMember subsetMember : widgetSubsetMembers) {
            boolean newMember = true;
            for (WidgetSubsetMember newSubsetMember : newWidgetSubsetMembers) {
                if (newSubsetMember.getMemberId() == subsetMember.getMemberId()
                    && newSubsetMember.getWidgetTierType().getId().equals(subsetMember.getWidgetTierType().getId())) {
                    newMember = false;
                }
            }
            if (newMember) {
                newWidgetSubsetMembers.add(subsetMember);
            }
        }
        sec.getWidget(widgetSettingsForm.getRandomId()).setWidgetSubsetMembers(newWidgetSubsetMembers);
        
        final int chartId = (Integer) keyMapping.getKey(widgetSettingsForm.getChartType());
        final WidgetChartType newChart = this.dashboardService.findObjectByClassTypeAndId(WidgetChartType.class, chartId);
        sec.getWidget(widgetSettingsForm.getRandomId()).setWidgetChartType(newChart);
        
        if (!widgetSettingsForm.getFilter().isEmpty()
            && ((Integer) keyMapping.getKey(widgetSettingsForm.getFilter()) == WidgetConstants.FILTER_TYPE_TOP || (Integer) keyMapping
                    .getKey(widgetSettingsForm.getFilter()) == WidgetConstants.FILTER_TYPE_BOTTOM)) {
            final int lengthId = this.widgetLimitTypesMap.get(widgetSettingsForm.getLength());
            final WidgetLimitType newLimit = this.dashboardService.findObjectByClassTypeAndId(WidgetLimitType.class, lengthId);
            sec.getWidget(widgetSettingsForm.getRandomId()).setWidgetLimitType(newLimit);
        }
        
        final Tab tab = getTabFromTabHeading(request);
        @SuppressWarnings("unchecked")
        final Map<String, Tab> tabTreeMap = (Map<String, Tab>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        tabTreeMap.put(tab.getRandomId(), tab);
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, tabTreeMap);
    }
    
    /**
     * Compares sessionToken and makes sure WidgetMetrics is loaded.
     * 
     * @throws InvalidUserAccountException
     *             Tokens in request and session are not the same.
     */
    private void validateTokenAndMetrics(final HttpServletRequest request) throws InvalidUserAccountException {
        // Compares sessionToken.
        if (!WidgetSettingsValidator.isSameSessionToken(request, request.getSession(false))) {
            throw new InvalidUserAccountException("error.common.invalid.posttoken");
        }
        // Makes sure WidgetMetrics is loaded.
        if (this.widgetMetrics == null) {
            throw new WebWidgetServiceException("WidgetMetrics object is NULL");
        }
    }
    
    /**
     * Compares if widgetId in HttpServletRequest and HttpSession are the same
     */
    private void validateWidgetRandomId(final HttpServletRequest request) throws WebWidgetServiceException {
        // Makes sure widgetIDs in HttpSession and HttpServletRequest are the same.
        final SessionTool sessionTool = SessionTool.getInstance(request);
        final String wid = (String) sessionTool.getAttribute(WebCoreConstants.SESSION_WIDGET_ID_CURRENT);
        if (!DashboardUtil.isSameRandomId(request.getParameter("widgetID"), wid)) {
            throw new WebWidgetServiceException("widgetIDs in the request and session are NOT the same.");
        }
    }
    
    /**
     * Retrieves data from WidgetRangeType database table if widgetSettingsValidator durationMap doesn't exist.
     * 
     * @return Map<String, Integer> Populated Map with data from WidgetRangeType database table.
     */
    private void loadWidgetDurationMapIfNecessary() {
        if (this.widgetDurationTypesMap == null) {
            this.widgetDurationTypesMap = new HashMap<Integer, String>(12);
            WidgetRangeType type;
            final Iterator<WidgetRangeType> rangeTypeIter = this.entityService.loadAll(WidgetRangeType.class).iterator();
            while (rangeTypeIter.hasNext()) {
                type = rangeTypeIter.next();
                this.widgetDurationTypesMap.put(type.getId(), type.getName());
            }
            this.widgetSettingsValidator.setDurationMap(this.widgetDurationTypesMap);
        }
    }
    
    /**
     * Retrieves data from WidgetTierType database table if widgetTierTypesMap is null.
     * 
     * @return Map<String, Integer> Populated Map with data from WidgetTierType database table.
     */
    private void loadWidgetTierTypesMapIfNecessary() {
        if (this.widgetTierTypesMap == null) {
            this.widgetTierTypesMap = new HashMap<Integer, String>();
            WidgetTierType type;
            final Iterator<WidgetTierType> iter = this.entityService.loadAll(WidgetTierType.class).iterator();
            while (iter.hasNext()) {
                type = iter.next();
                this.widgetTierTypesMap.put(type.getId(), type.getName());
            }
            this.widgetSettingsValidator.setTierMap(this.widgetTierTypesMap);
        }
    }
    
    /**
     * Retrieves data from WidgetFilterType database table if widgetFilterTypesMap is null.
     * 
     * @return Map<String, Integer> Populated Map with data from WidgetFilterType database table.
     */
    private void loadWidgetFilterTypesMapIfNecessary() {
        if (this.widgetFilterTypesMap == null) {
            this.widgetFilterTypesMap = new HashMap<Integer, String>();
            WidgetFilterType type;
            final Iterator<WidgetFilterType> iter = this.entityService.loadAll(WidgetFilterType.class).iterator();
            while (iter.hasNext()) {
                type = iter.next();
                this.widgetFilterTypesMap.put(type.getId(), type.getName());
            }
            this.widgetSettingsValidator.setFilterMap(this.widgetFilterTypesMap);
        }
    }
    
    /**
     * Retrieves data from WidgetMetricType database table if widgetMetricTypesMap is null.
     * 
     * @return Map<String, Integer> Populated Map with data from WidgetMetricType database table.
     */
    private void loadWidgetMetricTypesMapIfNecessary() {
        if (this.widgetMetricTypesMap == null) {
            this.widgetMetricTypesMap = new HashMap<Integer, String>();
            WidgetMetricType type;
            final Iterator<WidgetMetricType> iter = this.entityService.loadAll(WidgetMetricType.class).iterator();
            while (iter.hasNext()) {
                type = iter.next();
                this.widgetMetricTypesMap.put(type.getId(), type.getName());
            }
            this.widgetSettingsValidator.setMetricMap(this.widgetMetricTypesMap);
        }
    }
    
    /**
     * Retrieves data from WidgetChartType database table if widgetChartTypesMap is null.
     * 
     * @return Map<String, Integer> Populated Map with data from WidgetChartType database table.
     */
    private void loadWidgetChartTypesMapIfNecessary() {
        if (this.widgetChartTypesMap == null) {
            this.widgetChartTypesMap = new HashMap<Integer, String>();
            WidgetChartType type;
            final Iterator<WidgetChartType> iter = this.entityService.loadAll(WidgetChartType.class).iterator();
            while (iter.hasNext()) {
                type = iter.next();
                this.widgetChartTypesMap.put(type.getId(), type.getName());
            }
            this.widgetSettingsValidator.setChartMap(this.widgetChartTypesMap);
        }
    }
    
    /**
     * Retrieves data from WidgetLimitType database table if widgetLimitTypesMap is null.
     * 
     * @return Map<String, Integer> Populated Map with data from WidgetChartType database table.
     */
    private void loadWidgetLimitTypesMapIfNecessary() {
        if (this.widgetLimitTypesMap == null) {
            this.widgetLimitTypesMap = new HashMap<Integer, Integer>();
            WidgetLimitType type;
            final Iterator<WidgetLimitType> iter = this.entityService.loadAll(WidgetLimitType.class).iterator();
            while (iter.hasNext()) {
                type = iter.next();
                this.widgetLimitTypesMap.put(new Integer(type.getRowLimit()), type.getId());
            }
        }
    }
    
    /**
     * Returns the number of attributes found in the settingsOptions using the specified tier Id.
     * Used in applyWidgetSettings to help determine if the isSubset flag of the widget should be selected
     */
    private int subsetLength(final WidgetSettingsOptions settingsOptions, final int tierId) {
        int length = 0;
        if (tierId == WidgetConstants.TIER_TYPE_ORG) {
            length = settingsOptions.getOrganizationTypes().size();
        } else if (tierId == WidgetConstants.TIER_TYPE_LOCATION) {
            length = settingsOptions.getLocationTypes().size();
        } else if (tierId == WidgetConstants.TIER_TYPE_PARENT_LOCATION) {
            length = settingsOptions.getParentLocationTypes().size();
        } else if (tierId == WidgetConstants.TIER_TYPE_RATE) {
            length = settingsOptions.getRateTypes().size();
        } else if (tierId == WidgetConstants.TIER_TYPE_REVENUE_TYPE) {
            length = settingsOptions.getRevenueTypes().size();
        } else if (tierId == WidgetConstants.TIER_TYPE_ROUTE) {
            length = settingsOptions.getRouteTypes().size();
        } else if (tierId == WidgetConstants.TIER_TYPE_TRANSACTION_TYPE) {
            length = settingsOptions.getTransactionTypes().size();
        } else if (tierId == WidgetConstants.TIER_TYPE_ORG) {
            length = settingsOptions.getOrganizationTypes().size();
        } else if (tierId == WidgetConstants.TIER_TYPE_COLLECTION_TYPE) {
            length = settingsOptions.getCollTypes().size();
        } else if (tierId == WidgetConstants.TIER_TYPE_MERCHANT_ACCOUNT) {
            length = settingsOptions.getMerchTypes().size();
        } else if (tierId == WidgetConstants.TIER_TYPE_ALERT_TYPE) {
            length = settingsOptions.getAlertTypes().size();
        } else if (tierId == WidgetConstants.TIER_TYPE_CARD_PROCESS_METHOD_TYPE) {
            length = settingsOptions.getCardProcessMethodTypes().size();
        } else if (tierId == WidgetConstants.TIER_TYPE_CITATION_TYPE) {
            length = settingsOptions.getCitatTypes().size();
        }
        
      //length - 1 to account for number of commas
        return (length * WebCoreConstants.RANDOMKEY_MIN_HEX_LENGTH) + (length - 1);
    }
    
    /**
     * Returns a set of new WidgetSubsetMember objects to be stored in a saved widget based on the randomTierAttrIds passed
     * Used in applyWidgetSettings to set up the subset objects for a widget
     */
    private Set<WidgetSubsetMember> setTierSubsetSelection(final Widget widget, final int tierId
                                                           , final String[] randomTierAttrIds, final RandomKeyMapping keyMapping) {
        
        final Set<WidgetSubsetMember> widgetSubsetMembers = new HashSet<WidgetSubsetMember>();
        
        for (String randomTierAttrId : randomTierAttrIds) {
            final WidgetSubsetMember widgetSubsetMember = new WidgetSubsetMember();
            
            int tierAttrId;
            final Object obj = keyMapping.getKey(randomTierAttrId);
            if (obj instanceof Byte) {
                tierAttrId = ((Byte) obj).intValue();
            } else {
                tierAttrId = (Integer) obj;
            }
            String memberName = null;
            
            if (tierId == WidgetConstants.TIER_TYPE_LOCATION) {
                memberName = this.dashboardService.findLocationById(tierAttrId).getName();
            } else if (tierId == WidgetConstants.TIER_TYPE_PARENT_LOCATION) {
                memberName = this.dashboardService.findLocationById(tierAttrId).getName();
            } else if (tierId == WidgetConstants.TIER_TYPE_RATE) {
                memberName = this.dashboardService.findUnifiedRateById(tierAttrId).getName();
            } else if (tierId == WidgetConstants.TIER_TYPE_REVENUE_TYPE) {
                memberName = this.dashboardService.findRevenueTypeById((byte) tierAttrId).getName();
            } else if (tierId == WidgetConstants.TIER_TYPE_ROUTE) {
                memberName = this.dashboardService.findRouteById(tierAttrId).getName();
            } else if (tierId == WidgetConstants.TIER_TYPE_TRANSACTION_TYPE) {
                memberName = this.dashboardService.findTransactionTypeById(tierAttrId).getName();
            } else if (tierId == WidgetConstants.TIER_TYPE_ORG) {
                memberName = this.dashboardService.findCustomerById(tierAttrId).getName();
            } else if (tierId == WidgetConstants.TIER_TYPE_COLLECTION_TYPE) {
                memberName = this.dashboardService.findCollectionTypeById(tierAttrId).getName();
            } else if (tierId == WidgetConstants.TIER_TYPE_MERCHANT_ACCOUNT) {
                memberName = this.dashboardService.findMerchantAccountById(tierAttrId).getUIName();
            } else if (tierId == WidgetConstants.TIER_TYPE_ALERT_TYPE) {
                memberName = this.dashboardService.findAlertClassTypeById((byte) tierAttrId).getName();
            } else if (tierId == WidgetConstants.TIER_TYPE_CARD_PROCESS_METHOD_TYPE) {
                memberName = this.dashboardService.findCardProcessMethodTypeById((byte) tierAttrId).getName();
            } else if (tierId == WidgetConstants.TIER_TYPE_CITATION_TYPE) {
                memberName = this.dashboardService.findCitationTypeById(tierAttrId).getName();
            } else {
                continue;
            }
            
            widgetSubsetMember.setWidget(widget);
            widgetSubsetMember.setWidgetTierType(this.dashboardService.findWidgetTierTypeByTierTypeId(tierId));
            widgetSubsetMember.setMemberId(tierAttrId);
            widgetSubsetMember.setMemberName(memberName);
            widgetSubsetMember.setLastModifiedGmt(new Date());
            
            widgetSubsetMembers.add(widgetSubsetMember);
        }
        return widgetSubsetMembers;
    }
    
    /**
     * Small method to determine if the passed string is an Integer
     * Used in applyWidgetSettings to test if the targetValue is an Integer (could be a "true" string for target by location)
     */
    private boolean isInteger(final String string) {
        try {
            Integer.valueOf(string);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    
    protected final boolean isIntegerOrFloat(final String value) {
        if (value == null || value.trim().length() == 0) {
            return false;
        }
        for (int i = 0; i < value.length(); i++) {
            final char character = value.charAt(i);
            if (!Character.isDigit(character) && character != '.') {
                return false;
            }
        }
        return true;
    }
    
    /**
     * returns list of subSet Id's or null for the specified widgetId and widgetTierTypeId
     */
    private List<Integer> findSubsetMemberIds(final Widget widget, final int widgetTierTypeId) {
        
        final List<WidgetSubsetMember> subsetMembers = new ArrayList<WidgetSubsetMember>();
        
        //      List<WidgetSubsetMember> subsetMembers = dashboardService.findSubsetMemberByWidgetIdTierTypeId(widgetId,widgetTierTypeId);
        
        for (WidgetSubsetMember subsetMember : widget.getWidgetSubsetMembers()) {
            if (subsetMember.getWidgetTierType().getId().equals(widgetTierTypeId)) {
                subsetMembers.add(subsetMember);
            }
        }
        
        if (subsetMembers != null && subsetMembers.size() > 0) {
            final List<Integer> subsetMemberIds = new ArrayList<Integer>();
            for (WidgetSubsetMember subsetMember : subsetMembers) {
                subsetMemberIds.add(subsetMember.getMemberId());
            }
            return subsetMemberIds;
        }
        return null;
    }
    
    private Widget getWidgetFromWidgetRandomId(final HttpServletRequest request, final String widgetRandomId) {
        final Section sec = getSectionFromWidgetRandomId(request, widgetRandomId);
        if (sec != null) {
            return sec.getWidget(widgetRandomId);
        }
        return null;
    }
    
    private Section getSectionFromWidgetRandomId(final HttpServletRequest request, final String widgetRandomId) {
        // Finds Tab randomId using TabHeading, current tab.
        final Tab tab = getTabFromTabHeading(request);
        Section sec = null;
        final Iterator<Section> iter = tab.getSections().iterator();
        while (iter.hasNext()) {
            sec = iter.next();
            if (sec.getWidget(widgetRandomId) != null) {
                break;
            }
        }
        return sec;
    }
    
    private Tab getTabFromTabHeading(final HttpServletRequest request) {
        // Finds Tab randomId using TabHeading, current tab.
        String tabRandomId = null;
        final SessionTool sessionTool = SessionTool.getInstance(request);
        @SuppressWarnings("unchecked")
        final List<TabHeading> tabHeadings = (List<TabHeading>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_HEADINGS);
        for (TabHeading tabHeading : tabHeadings) {
            if (tabHeading.getCurrent().equals(WebCoreConstants.RESPONSE_TRUE)) {
                tabRandomId = tabHeading.getId();
                break;
            }
        }
        if (tabRandomId == null) {
            final String err = "Unable to prepare widget data, current Tab randomId in TabHeadings is null, check TabHeading.getCurrent and tabHeading.getId.";
            log.error(err);
            throw new WebWidgetServiceException(err);
        }
        
        // Loads tab tree into Map if it doesn't exist.
        final  Map<String, Tab> tabTreeMap = loadTabTreeToMapIfNecessary(request, tabRandomId);
        final Tab tab = tabTreeMap.get(tabRandomId);
        if (tab == null) {
            throw new WebWidgetServiceException("Unable to load Tab object using randomId: " + tabRandomId);
        }
        return tab;
    }
    
    // Looks in tabtreemap for the passed widgetRandomId, returns null if not
    // found.
    protected final Widget findWidgetFromMapIfExists(final HttpServletRequest request, final String widgetRandomId) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        @SuppressWarnings("unchecked")
        final List<TabHeading> tabHeadings = (List<TabHeading>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_HEADINGS);
        @SuppressWarnings("unchecked")
        final Map<String, Tab> tabTreeMap = (Map<String, Tab>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        
        String tabRandomId = null;
        Tab tab = null;
        for (TabHeading tabHeading : tabHeadings) {
            if (tabHeading.getCurrent().equals(WebCoreConstants.RESPONSE_TRUE)) {
                tabRandomId = tabHeading.getId();
                break;
            }
        }
        if (tabTreeMap != null) {
            tab = tabTreeMap.get(tabRandomId);
            if (tab == null) {
                return null;
            }
        } else {
            return null;
        }
        for (Section sec : tab.getSections()) {
            if (sec.getWidget(widgetRandomId) != null) {
                return sec.getWidget(widgetRandomId);
            }
        }
        return null;
    }
    
    private boolean isParentOrganization(final UserAccount userAccount) {
        if (userAccount.getCustomer().isIsParent()) {
            return true;
        }
        return false;
    }
    
    /**
     * Loads data from the following tables: - WidgetMetricType - WidgetTierType- Location - Route - RevenueType - TransactionType
     */
    private WidgetSettingsOptions getDefaultWidgetSettingsOptions(final HttpServletRequest request, final WidgetSettingsOptions settings) {
        WidgetSettingsOptions resultSettings;
        if (settings == null) {
            resultSettings = new WidgetSettingsOptions();
        } else {
            resultSettings = settings;
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final SettingsTypeComparator comparator = new SettingsTypeComparator();
        
        final String wRandomId = request.getParameter("widgetID");
        if (StringUtils.isBlank(wRandomId)) {
            return null;
        }
        
        final Widget widget = getWidgetFromWidgetRandomId(request, wRandomId);
        
        // Constructs list of customer Id's, including the current customer. Used for both child and parent customers       
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final List<Integer> customerIds = new ArrayList<Integer>();
        customerIds.add(userAccount.getCustomer().getId());
        if (userAccount.getCustomer().isIsParent()) {
          //Remove the parent customers Id from the list
            customerIds.remove(0);
            for (Customer childCustomer : this.dashboardService.findAllChildCustomers(userAccount.getCustomer().getId())) {
                customerIds.add(childCustomer.getId());
            }
        }
        
        // 1. Loads locations from database and copies to locationTypes.
        if (resultSettings.getLocationTypes() == null) {
            final List<Location> locations = new ArrayList<Location>();
            for (Integer id : customerIds) {
                locations.addAll(this.dashboardService.findLocationsByCustomerId(id));
            }
            final Iterator<Location> iter = locations.iterator();
            while (iter.hasNext()) {
                final Location loc = iter.next();
                loc.setRandomId(keyMapping.getRandomString(loc, WebCoreConstants.ID_LOOK_UP_NAME));
            }
            
            //create parent and child lists based on each locations properties
            final List<Location> childLocations = new ArrayList<Location>();
            final List<Location> parentLocations = new ArrayList<Location>();
            for (Location location : locations) {
                if ((location.getIsUnassigned() && (locations.size() > 1)) || location.getIsDeleted()) {
                    continue;
                } else if (location.getIsParent()) {
                    parentLocations.add(location);
                } else {
                    childLocations.add(location);
                }
            }
            
            // Copy property values from the origin Location beans to the destination LocationType beans.
            final List<LocationType> childLocationTypes = WebWidgetUtil.copyToLocationTypes(childLocations);
            Collections.sort(childLocationTypes, comparator);
            final List<LocationType> parentLocationTypes = WebWidgetUtil.copyToLocationTypes(parentLocations);
            Collections.sort(parentLocationTypes, comparator);
            
            // find any location subsets for this widget, if found make them selected in the locationType list
            final List<Integer> subsetChildLocationIds = findSubsetMemberIds(widget, WidgetConstants.TIER_TYPE_LOCATION);
            if (subsetChildLocationIds != null) {
                for (LocationType locationType : childLocationTypes) {
                    if (subsetChildLocationIds.contains(keyMapping.getKey(locationType.getRandomId()))) {
                        locationType.setStatus(WebCoreConstants.SELECTED);
                    }
                }
                // if no sub selections are found, if locations is selected, make all location Types status selected.
            } else if (widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_LOCATION
                       || widget.getWidgetTierTypeByWidgetTier2Type().getId() == WidgetConstants.TIER_TYPE_LOCATION
                       || widget.getWidgetTierTypeByWidgetTier3Type().getId() == WidgetConstants.TIER_TYPE_LOCATION) {
                for (LocationType locationType : childLocationTypes) {
                    locationType.setStatus(WebCoreConstants.SELECTED);
                }
            }
            
            final List<Integer> subsetParentLocationIds = findSubsetMemberIds(widget, WidgetConstants.TIER_TYPE_PARENT_LOCATION);
            if (subsetParentLocationIds != null) {
                for (LocationType locationType : parentLocationTypes) {
                    if (subsetParentLocationIds.contains(keyMapping.getKey(locationType.getRandomId()))) {
                        locationType.setStatus(WebCoreConstants.SELECTED);
                    }
                }
            } else if (widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_PARENT_LOCATION
                       || widget.getWidgetTierTypeByWidgetTier2Type().getId() == WidgetConstants.TIER_TYPE_PARENT_LOCATION
                       || widget.getWidgetTierTypeByWidgetTier3Type().getId() == WidgetConstants.TIER_TYPE_PARENT_LOCATION) {
                for (LocationType locationType : parentLocationTypes) {
                    locationType.setStatus(WebCoreConstants.SELECTED);
                }
            }
            resultSettings.setLocationTypes(childLocationTypes);
            resultSettings.setParentLocationTypes(parentLocationTypes);
        }
        // 2. WidgetMetricTypes to MetricTypes.
        if (resultSettings.getMetricTypes() == null) {
            final List<WidgetMetricType> widgetMetricTypes = this.entityService.loadAll(WidgetMetricType.class);
            final Iterator<WidgetMetricType> iter = widgetMetricTypes.iterator();
            while (iter.hasNext()) {
                final WidgetMetricType metricType = iter.next();
                metricType.setRandomId(keyMapping.getRandomString(metricType, WebCoreConstants.ID_LOOK_UP_NAME));
            }
            // Copies property values from the origin WidgetMetricType beans to the destination MetricType beans.
            final List<MetricType> metricTypes = WebWidgetUtil.copyToMetricTypes(widgetMetricTypes);
            Collections.sort(metricTypes, comparator);
            String metricTypeRandomId = null;
            
            //For initial release, metric types cannot be changed. All metric types default to DISABLED
            for (MetricType metricType : metricTypes) {
                metricType.setStatus(WebCoreConstants.DISABLED);
                if (widget.getWidgetMetricType().getName().equals(metricType.getName())) {
                    metricTypeRandomId = metricType.getRandomId();
                }
            }
            resultSettings.setMetricTypes(metricTypes);
            
            // Defines selected metric type.
            resultSettings.setMetricType(metricTypeRandomId);
        }
        // 3. WidgetTierTypes to tierTypes 1, 2 and 3
        if (resultSettings.getTier1Types() == null) {
            
            final boolean parentOrganizationFlag = isParentOrganization(userAccount);
            
            final List<WidgetTierType> widgetTier1Types = this.entityService.loadAll(WidgetTierType.class);
            final Iterator<WidgetTierType> iter = widgetTier1Types.iterator();
            //            String tier1RandomId = null;
            //            String tier2RandomId = null;
            //            String tier3RandomId = null;
            
            while (iter.hasNext()) {
                final WidgetTierType tierType = iter.next();
                if (tierType.getId() == WidgetConstants.TIER_TYPE_ORG && !userAccount.getCustomer().isIsParent()) {
                    iter.remove();
                    continue;
                }
                
                tierType.setRandomId(keyMapping.getRandomString(tierType, WebCoreConstants.ID_LOOK_UP_NAME));
                
                //                if (widget.getWidgetTierTypeByWidgetTier1Type().getId().equals(tierType.getId())) {
                //                    tier1RandomId = tierType.getRandomId();
                //                }
                //                if (widget.getWidgetTierTypeByWidgetTier2Type().getId().equals(tierType.getId())) {
                //                    tier2RandomId = tierType.getRandomId();
                //                }
                //                if (widget.getWidgetTierTypeByWidgetTier3Type().getId().equals(tierType.getId())) {
                //                    tier3RandomId = tierType.getRandomId();
                //                }
            }
            
            // Copies property values from the origin WidgetTierType beans to the destination MetricType beans.
            final List<TierType> tier1Types = WebWidgetUtil.copyToTierTypes(widgetTier1Types, parentOrganizationFlag);
            final List<TierType> tier2Types = new ArrayList<TierType>(tier1Types.size());
            final List<TierType> tier3Types = new ArrayList<TierType>(tier1Types.size());
            Collections.sort(tier1Types, comparator);
            Collections.sort(tier2Types, comparator);
            Collections.sort(tier3Types, comparator);
            
            try {
                // Clones tier1Types and sets to tier2Types and tier3Types.
                for (TierType tier1Type : tier1Types) {
                    tier2Types.add((TierType) tier1Type.clone());
                    tier3Types.add((TierType) tier1Type.clone());
                }
            } catch (CloneNotSupportedException cnse) {
                log.error("getDefaultWidgetSettingsOptions, CANNOT clone and create widgetTier2Types, widgetTier3Types", cnse);
            }
            
            resultSettings.setTier1Types(tier1Types);
            resultSettings.setTier2Types(tier2Types);
            resultSettings.setTier3Types(tier3Types);
        }
        // 4. RouteTypes
        if (resultSettings.getRouteTypes() == null) {
            final List<Route> routes = new ArrayList<Route>();
            for (Integer id : customerIds) {
                routes.addAll(this.dashboardService.findRoutesByCustomerId(id));
            }
            final Iterator<Route> iter = routes.iterator();
            while (iter.hasNext()) {
                final Route rou = iter.next();
                rou.setRandomId(keyMapping.getRandomString(rou, WebCoreConstants.ID_LOOK_UP_NAME));
            }
            // Copies property values from the origin Routes beans to the destination RouteType beans.      
            final List<RouteType> routeTypes = WebWidgetUtil.copyToRouteTypes(routes);
            Collections.sort(routeTypes, comparator);
            
            // find any route subsets for this widget, if found make them selected in the routeType list
            final List<Integer> subsetRouteIds = findSubsetMemberIds(widget, WidgetConstants.TIER_TYPE_ROUTE);
            if (subsetRouteIds != null) {
                for (RouteType routeType : routeTypes) {
                    if (subsetRouteIds.contains(keyMapping.getKey(routeType.getRandomId()))) {
                        routeType.setStatus(WebCoreConstants.SELECTED);
                    }
                }
            } else if (widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_ROUTE
                     || widget.getWidgetTierTypeByWidgetTier2Type().getId() == WidgetConstants.TIER_TYPE_ROUTE
                     || widget.getWidgetTierTypeByWidgetTier3Type().getId() == WidgetConstants.TIER_TYPE_ROUTE) {
                // if no sub selections are found, if route Type is selected, make all route Types status selected.
                for (RouteType routeType : routeTypes) {
                    routeType.setStatus(WebCoreConstants.SELECTED);
                }
            }
            resultSettings.setRouteTypes(routeTypes);
        }
        // 5. RevTypes
        if (resultSettings.getRevenueTypes() == null) {
            final List<RevenueType> revenueTypes = this.entityService.loadAll(RevenueType.class);
            for (RevenueType revType : revenueTypes) {
                revType.setRandomId(keyMapping.getRandomString(RevenueType.class, (int) revType.getId()));
            }
            for (RevenueType revType : revenueTypes) {
                if (revType.getParentRevenueTypeId() == null) {
                    revType.setParentRandomId("");
                } else {
                    revType.setParentRandomId(this.dashboardService.findRevenueTypeById(revType.getParentRevenueTypeId()).getRandomId());
                }
            }
            // Copies property values from the origin RevenueType beans to the destination RevType beans.
            final List<RevType> revTypes = WebWidgetUtil.copyToRevTypes(revenueTypes);
            Collections.sort(revTypes, comparator);
            
            // find any revenue subsets for this widget, if found make them selected in the revenueType list
            final List<Integer> subsetRevIds = findSubsetMemberIds(widget, WidgetConstants.TIER_TYPE_REVENUE_TYPE);
            if (subsetRevIds != null) {
                for (RevType revType : revTypes) {
                    if (subsetRevIds.contains(keyMapping.getKey(revType.getRandomId()))) {
                        revType.setStatus(WebCoreConstants.SELECTED);
                    }
                }
            } else if (widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_REVENUE_TYPE
                     || widget.getWidgetTierTypeByWidgetTier2Type().getId() == WidgetConstants.TIER_TYPE_REVENUE_TYPE
                     || widget.getWidgetTierTypeByWidgetTier3Type().getId() == WidgetConstants.TIER_TYPE_REVENUE_TYPE) {
                // if no sub selections are found, if revenue Type is selected, make all revenue Types status selected.
                for (RevType revType : revTypes) {
                    revType.setStatus(WebCoreConstants.SELECTED);
                }
            }
            resultSettings.setRevenueTypes(revTypes);
        }
        // 6. TxnTypes
        if (resultSettings.getTransactionTypes() == null) {
            final List<TransactionType> transactionTypes = this.entityService.loadAll(TransactionType.class);
            final Iterator<TransactionType> iter = transactionTypes.iterator();
            while (iter.hasNext()) {
                final TransactionType txType = iter.next();
                txType.setRandomId(keyMapping.getRandomString(txType, WebCoreConstants.ID_LOOK_UP_NAME));
            }
            // Copies property values from the origin TransactionType beans to the destination TxnType beans.
            final List<TxnType> txnTypes = WebWidgetUtil.copyToTxnTypes(transactionTypes);
            Collections.sort(txnTypes, comparator);
            
            // find any transaction subsets for this widget, if found make them selected in the transactionType list
            final List<Integer> subsetTxnIds = findSubsetMemberIds(widget, WidgetConstants.TIER_TYPE_TRANSACTION_TYPE);
            if (subsetTxnIds != null) {
                for (TxnType txnType : txnTypes) {
                    if (subsetTxnIds.contains(keyMapping.getKey(txnType.getRandomId()))) {
                        txnType.setStatus(WebCoreConstants.SELECTED);
                    }
                }
            } else if (widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_TRANSACTION_TYPE
                     || widget.getWidgetTierTypeByWidgetTier2Type().getId() == WidgetConstants.TIER_TYPE_TRANSACTION_TYPE
                     || widget.getWidgetTierTypeByWidgetTier3Type().getId() == WidgetConstants.TIER_TYPE_TRANSACTION_TYPE) {
                // if no sub selections are found, if transaction type is selected, make all transaction Types status selected.
                for (TxnType txnType : txnTypes) {
                    txnType.setStatus(WebCoreConstants.SELECTED);
                }
            }
            resultSettings.setTransactionTypes(txnTypes);
        }
        // 7. FilterType
        if (resultSettings.getFilterTypes() == null) {
            final List<WidgetFilterType> widgetFilterTypes = this.entityService.loadAll(WidgetFilterType.class);
            final Iterator<WidgetFilterType> iter = widgetFilterTypes.iterator();
            while (iter.hasNext()) {
                final WidgetFilterType wType = iter.next();
                wType.setRandomId(keyMapping.getRandomString(wType, WebCoreConstants.ID_LOOK_UP_NAME));
            }
            // Copies property values from the origin WidgetFilterType beans to the destination FilterType beans.
            final List<FilterType> filterTypes = WebWidgetUtil.copyToFilterTypes(widgetFilterTypes);
            Collections.sort(filterTypes, comparator);
            for (FilterType filter : filterTypes) {
                filter.setStatus(WebCoreConstants.DISABLED);
            }
            resultSettings.setFilterTypes(filterTypes);
            
            //set current FilterType type to selected       
            String filterTypeRandomId = this.widgetFilterTypesMap.get(WidgetConstants.FILTER_TYPE_ALL);
            if (widget.getWidgetFilterType() != null && widget.getWidgetFilterType().getId() != WidgetConstants.FILTER_TYPE_SUBSET) {
                for (WidgetFilterType filter : widgetFilterTypes) {
                    if (filter.getId() == widget.getWidgetFilterType().getId()) {
                        widget.getWidgetFilterType().setRandomId(filter.getRandomId());
                        filterTypeRandomId = filter.getRandomId();
                        break;
                    }
                }
            }
            resultSettings.setFilterType(filterTypeRandomId);
        }
        // 8. DisplayType / ChartType
        if (resultSettings.getDisplayTypes() == null) {
            
            final List<WidgetChartType> widgetChartTypes = this.entityService.loadAll(WidgetChartType.class);
            final Iterator<WidgetChartType> iter = widgetChartTypes.iterator();
            String chartTypeRandomId = null;
            
            while (iter.hasNext()) {
                final WidgetChartType wcType = iter.next();
                wcType.setRandomId(keyMapping.getRandomString(wcType, WebCoreConstants.ID_LOOK_UP_NAME));
                if (widget.getWidgetChartType().getId() == wcType.getId()) {
                    chartTypeRandomId = wcType.getRandomId();
                }
            }
            // Copies property values from the origin WidgetChartType beans to the destination DisplayType beans.
            final List<DisplayType> chartTypes = WebWidgetUtil.copyToDisplayTypes(widgetChartTypes);
            Collections.sort(chartTypes, comparator);
            resultSettings.setDisplayTypes(chartTypes);
            
            //set current ChartType type to selected            
            resultSettings.setChartType(chartTypeRandomId);
        }
        // 9. Rate is from database table and copies to rateTypes.
        if (resultSettings.getRateTypes() == null) {
            final List<UnifiedRate> rates = new ArrayList<UnifiedRate>();
            for (Integer id : customerIds) {
                rates.addAll(this.dashboardService.findUnifiedRatesByCustomerId(id, WebCoreConstants.DEFAULT_PAST_NUMBER_OF_MONTHS));
            }
            final Iterator<UnifiedRate> iter = rates.iterator();
            while (iter.hasNext()) {
                final UnifiedRate r = iter.next();
                r.setRandomId(keyMapping.getRandomString(r, WebCoreConstants.ID_LOOK_UP_NAME));
            }
            // Copies property values from the origin Rate beans to the destination RateType beans.
            final List<RateType> rateTypes = WebWidgetUtil.copyToRateTypes(rates);
            Collections.sort(rateTypes, comparator);
            
            // find any rate subsets for this widget, if found make them selected in the rateType list
            final List<Integer> subsetRateIds = findSubsetMemberIds(widget, WidgetConstants.TIER_TYPE_RATE);
            if (subsetRateIds != null) {
                for (RateType rateType : rateTypes) {
                    if (subsetRateIds.contains(keyMapping.getKey(rateType.getRandomId()))) {
                        rateType.setStatus(WebCoreConstants.SELECTED);
                    }
                }
            }
            // if no sub selections are found, if Rate is selected, make all rate Types status selected.
            else if (widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_RATE
                     || widget.getWidgetTierTypeByWidgetTier2Type().getId() == WidgetConstants.TIER_TYPE_RATE
                     || widget.getWidgetTierTypeByWidgetTier3Type().getId() == WidgetConstants.TIER_TYPE_RATE) {
                for (RateType rateType : rateTypes) {
                    rateType.setStatus(WebCoreConstants.SELECTED);
                }
            }
            resultSettings.setRateTypes(rateTypes);
        }
        // 10. Range
        if (resultSettings.getRangeTypes() == null) {
            final List<WidgetRangeType> ranges = this.entityService.loadAll(WidgetRangeType.class);
            final Iterator<WidgetRangeType> iter = ranges.iterator();
            String rangeTypeRandomId = null;
            
            while (iter.hasNext()) {
                final WidgetRangeType wrType = iter.next();
                wrType.setRandomId(keyMapping.getRandomString(wrType, WebCoreConstants.ID_LOOK_UP_NAME));
                if (widget.getWidgetRangeType().getId() == wrType.getId()) {
                    rangeTypeRandomId = wrType.getRandomId();
                }
            }
            // Copies property values from the origin Rate beans to the destination RateType beans.
            final List<RangeType> rangeTypes = WebWidgetUtil.copyToRangeTypes(ranges);
            //Ranges are sorted in database order, sorting alphabetically may not make sense - Collections.sort(rangeTypes, comparator);
            resultSettings.setRangeTypes(rangeTypes);
            
            //set current Range type to selected            
            resultSettings.setRangeType(rangeTypeRandomId);
        }
        
        // Prepare all options
        this.widgetSettingsValidator.prepareOptions(widget, resultSettings);
        
        // 11. widget name
        resultSettings.setWidgetName(widget.getName());
        
        // 12. widget trend value
        if (widget.isIsTrendByLocation()) {
            resultSettings.setTargetValue("true");
        } else if (widget.getTrendAmount() != null && widget.getTrendAmount() > 0) {
            final Float dollarValue = new BigDecimal(widget.getTrendAmount()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP).floatValue();
            resultSettings.setTargetValue(dollarValue.toString());
        } else {
            resultSettings.setTargetValue("false");
        }
        
        // 13. widget length value (for top and bottom N revenue widgets)
        if (widget.getWidgetFilterType().getId() == WidgetConstants.FILTER_TYPE_TOP
            || widget.getWidgetFilterType().getId() == WidgetConstants.FILTER_TYPE_BOTTOM) {
            resultSettings.setLength(Short.toString(widget.getWidgetLimitType().getRowLimit()));
        }
        
        //14. Organization (child customers) and convert 'orgId' value from plain customer id to random id. 
        if (resultSettings.getOrganizationTypes() == null) {
            resultSettings = getSettingsOptionsOrganizationType(resultSettings, userAccount.getCustomer().isIsParent(), customerIds, keyMapping, widget);
        }
        
        //15. Collection types
        if (resultSettings.getCollTypes() == null) {
            final List<CollectionType> collectionTypes = this.entityService.loadAll(CollectionType.class);
            final Iterator<CollectionType> iter = collectionTypes.iterator();
            while (iter.hasNext()) {
                final CollectionType cltnType = iter.next();
                cltnType.setRandomId(keyMapping.getRandomString(cltnType, WebCoreConstants.ID_LOOK_UP_NAME));
            }
            // Copies property values from the origin collectionType beans to the destination collType beans.
            final List<CollType> collTypes = WebWidgetUtil.copyToCollTypes(collectionTypes);
            Collections.sort(collTypes, comparator);
            
            // find any collection subsets for this widget, if found make them selected in the collectionType list
            final List<Integer> subsetCollIds = findSubsetMemberIds(widget, WidgetConstants.TIER_TYPE_COLLECTION_TYPE);
            if (subsetCollIds != null) {
                for (CollType cltnType : collTypes) {
                    if (subsetCollIds.contains(keyMapping.getKey(cltnType.getRandomId()))) {
                        cltnType.setStatus(WebCoreConstants.SELECTED);
                    }
                }
            } else if (widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_COLLECTION_TYPE
                     || widget.getWidgetTierTypeByWidgetTier2Type().getId() == WidgetConstants.TIER_TYPE_COLLECTION_TYPE
                     || widget.getWidgetTierTypeByWidgetTier3Type().getId() == WidgetConstants.TIER_TYPE_COLLECTION_TYPE) {
                // if no sub selections are found, if collection Type is selected, make all collection Types status selected.
                for (CollType cltnType : collTypes) {
                    cltnType.setStatus(WebCoreConstants.SELECTED);
                }
            }
            resultSettings.setCollTypes(collTypes);
        }
        
        //16. Merchant Account Types
        if (resultSettings.getMerchTypes() == null) {
            
            final List<MerchantAccount> merchantAccounts = new ArrayList<MerchantAccount>();
            
            for (Integer id : customerIds) {
                merchantAccounts.addAll(this.merchantAccountService.findValidMerchantAccountsByCustomerId(id));
            }
            final Iterator<MerchantAccount> iter = merchantAccounts.iterator();
            while (iter.hasNext()) {
                final MerchantAccount m = iter.next();
                m.setRandomId(keyMapping.getRandomString(m, WebCoreConstants.ID_LOOK_UP_NAME));
            }
            // Copies property values from the origin collectionType beans to the destination collType beans.
            final List<MerchType> merchTypes = WebWidgetUtil.copyToMerchTypes(merchantAccounts);
            Collections.sort(merchTypes, comparator);
            
            // find any collection subsets for this widget, if found make them selected in the collectionType list
            final List<Integer> subsetMerchIds = findSubsetMemberIds(widget, WidgetConstants.TIER_TYPE_MERCHANT_ACCOUNT);
            if (subsetMerchIds != null) {
                for (MerchType merchType : merchTypes) {
                    if (subsetMerchIds.contains(keyMapping.getKey(merchType.getRandomId()))) {
                        merchType.setStatus(WebCoreConstants.SELECTED);
                    }
                }
            } else if (widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_MERCHANT_ACCOUNT
                     || widget.getWidgetTierTypeByWidgetTier2Type().getId() == WidgetConstants.TIER_TYPE_MERCHANT_ACCOUNT
                     || widget.getWidgetTierTypeByWidgetTier3Type().getId() == WidgetConstants.TIER_TYPE_MERCHANT_ACCOUNT) {
                // if no sub selections are found, if collection Type is selected, make all collection Types status selected.
                for (MerchType merchType : merchTypes) {
                    merchType.setStatus(WebCoreConstants.SELECTED);
                }
            }
            resultSettings.setMerchTypes(merchTypes);
        }
        // 17. AlertTypes
        if (resultSettings.getAlertTypes() == null) {
            final List<AlertClassType> alertClassTypes = this.entityService.loadAll(AlertClassType.class);
            
            for (AlertClassType alertClassType : alertClassTypes) {
                alertClassType.setRandomId(keyMapping.getRandomString(AlertClassType.class, (int) alertClassType.getId()));
            }
            
            // Copies property values from the origin TransactionType beans to the destination TxnType beans.
            final List<AlertWidgetType> alertTypes = WebWidgetUtil.copyToAlertTypes(alertClassTypes);
            Collections.sort(alertTypes, comparator);
            
            // find any transaction subsets for this widget, if found make them selected in the transactionType list
            final List<Integer> subsetAlertIds = findSubsetMemberIds(widget, WidgetConstants.TIER_TYPE_ALERT_TYPE);
            if (subsetAlertIds != null) {
                for (AlertWidgetType alertType : alertTypes) {
                    if (subsetAlertIds.contains(keyMapping.getKey(alertType.getRandomId()))) {
                        alertType.setStatus(WebCoreConstants.SELECTED);
                    }
                }
            } else if (widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_ALERT_TYPE
                     || widget.getWidgetTierTypeByWidgetTier2Type().getId() == WidgetConstants.TIER_TYPE_ALERT_TYPE
                     || widget.getWidgetTierTypeByWidgetTier3Type().getId() == WidgetConstants.TIER_TYPE_ALERT_TYPE) {
                // if no sub selections are found, if transaction type is selected, make all transaction Types status selected.
                for (AlertWidgetType alertType : alertTypes) {
                    alertType.setStatus(WebCoreConstants.SELECTED);
                }
            }
            resultSettings.setAlertTypes(alertTypes);
        }
        // 18. CardProcMethodTypes
        if (resultSettings.getCardProcessMethodTypes() == null) {
            final List<CardProcessMethodType> cardProcessMethodTypes = this.dashboardService.findSettledCardProcessMethodTypes();
            final Iterator<CardProcessMethodType> iter = cardProcessMethodTypes.iterator();
            while (iter.hasNext()) {
                final CardProcessMethodType cpmType = iter.next();
                cpmType.setRandomId(keyMapping.getRandomString(cpmType, WebCoreConstants.ID_LOOK_UP_NAME));
            }
            // Copies property values from the origin CardProcessMethodType beans to the destination CardProcMethodType beans.
            final List<CardProcMethodType> cdProcTypes = WebWidgetUtil.copyCardProcMethodTypes(cardProcessMethodTypes);
            Collections.sort(cdProcTypes, comparator);
            
            // find any card processing method type subsets for this widget, if found make them selected in the list
            final List<Integer> subsetCardProcIds = findSubsetMemberIds(widget, WidgetConstants.TIER_TYPE_CARD_PROCESS_METHOD_TYPE);
            if (subsetCardProcIds != null) {
                for (CardProcMethodType cardProMethodType : cdProcTypes) {
                    if (subsetCardProcIds.contains(keyMapping.getKey(cardProMethodType.getRandomId()))) {
                        cardProMethodType.setStatus(WebCoreConstants.SELECTED);
                    }
                }
            } else if (widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_CARD_PROCESS_METHOD_TYPE
                     || widget.getWidgetTierTypeByWidgetTier2Type().getId() == WidgetConstants.TIER_TYPE_CARD_PROCESS_METHOD_TYPE
                     || widget.getWidgetTierTypeByWidgetTier3Type().getId() == WidgetConstants.TIER_TYPE_CARD_PROCESS_METHOD_TYPE) {
                // if no sub selections are found, if card processing method type is selected, make all types status selected.
                for (CardProcMethodType cardProMethodType : cdProcTypes) {
                    cardProMethodType.setStatus(WebCoreConstants.SELECTED);
                }
            }
            resultSettings.setCardProcessMethodTypes(cdProcTypes);
        }
        //19. Citation Types
        if (resultSettings.getCitatTypes() == null) {
            
            final List<CitationType> citationTypes = new ArrayList<CitationType>();
            
            for (Integer id : customerIds) {
                citationTypes.addAll(this.citationTypeService.findCitationTypeByCustomerId(id));
            }
            final Iterator<CitationType> iter = citationTypes.iterator();
            while (iter.hasNext()) {
                final CitationType citationType = iter.next();
                citationType.setRandomId(keyMapping.getRandomString(citationType, WebCoreConstants.ID_LOOK_UP_NAME));
            }
            // Copies property values from the origin CitationType beans to the destination citatType beans.
            final List<CitatType> citatTypes = WebWidgetUtil.copyToCitatTypes(citationTypes);
            Collections.sort(citatTypes, comparator);
            
            // find any citation subsets for this widget, if found make them selected in the citationType list
            final List<Integer> subsetCitatIds = findSubsetMemberIds(widget, WidgetConstants.TIER_TYPE_CITATION_TYPE);
            if (subsetCitatIds != null) {
                for (CitatType citatType : citatTypes) {
                    if (subsetCitatIds.contains(keyMapping.getKey(citatType.getRandomId()))) {
                        citatType.setStatus(WebCoreConstants.SELECTED);
                    }
                }
            } else if (widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_CITATION_TYPE
                     || widget.getWidgetTierTypeByWidgetTier2Type().getId() == WidgetConstants.TIER_TYPE_CITATION_TYPE
                     || widget.getWidgetTierTypeByWidgetTier3Type().getId() == WidgetConstants.TIER_TYPE_CITATION_TYPE) {
               // if no sub selections are found, if collection Type is selected, make all collection Types status selected.
                for (CitatType citatType : citatTypes) {
                    citatType.setStatus(WebCoreConstants.SELECTED);
                }
            }
            resultSettings.setCitatTypes(citatTypes);
        }
        
        // Prepare all options
        this.widgetSettingsValidator.prepareOptions(widget, resultSettings);
        
        return resultSettings;
    }
    
    private WidgetSettingsOptions getSettingsOptionsOrganizationType(final WidgetSettingsOptions settingsOptions, final boolean parentFlag
            , final List<Integer> customerIds, final RandomKeyMapping keyMapping, final Widget widget) {
        
        final SettingsTypeComparator comparator = new SettingsTypeComparator();
        
        final List<Customer> customers = new ArrayList<Customer>();
        for (Integer id : customerIds) {
            customers.add(this.dashboardService.findObjectByClassTypeAndId(Customer.class, id));
        }
        final Iterator<Customer> iter = customers.iterator();
        while (iter.hasNext()) {
            final Customer c = iter.next();
            c.setRandomId(keyMapping.getRandomString(c, WebCoreConstants.ID_LOOK_UP_NAME));
        }
        // Copies property values from the origin Customer beans to the destination OrganizationType beans.
        final List<OrganizationType> orgTypes = WebWidgetUtil.copyToOrganizationTypes(customers);
        Collections.sort(orgTypes, comparator);
        
        // find any rate organizations for this widget, if found make them selected in the organizationType list
        final List<Integer> subsetOrganizationIds = findSubsetMemberIds(widget, WidgetConstants.TIER_TYPE_ORG);
        if (subsetOrganizationIds != null) {
            for (OrganizationType orgType : orgTypes) {
                if (subsetOrganizationIds.contains(keyMapping.getKey(orgType.getRandomId()))) {
                    orgType.setStatus(WebCoreConstants.SELECTED);
                }
            }
        } else if (widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_ORG
                 || widget.getWidgetTierTypeByWidgetTier2Type().getId() == WidgetConstants.TIER_TYPE_ORG
                 || widget.getWidgetTierTypeByWidgetTier3Type().getId() == WidgetConstants.TIER_TYPE_ORG) {
            // if no sub selections are found, if organizations is selected, make all location Types status selected.
            for (OrganizationType orgType : orgTypes) {
                orgType.setStatus(WebCoreConstants.SELECTED);
            }
        } else if (parentFlag) {
            for (OrganizationType orgType : orgTypes) {
                orgType.setStatus(WebCoreConstants.SELECTED);
            }
        }
        settingsOptions.setOrganizationTypes(orgTypes);
        return convertIdsToRandomIds(customers, settingsOptions);
    }
    
    /**
     * Convert customer id to randomId in LocationType, RouteType and RateType.
     * 
     * @param customers
     *            Customer objects with randomIds
     * @param settingsOptions
     *            WidgetSettingsOptions with populated LocationType, RouteType and RateType using plain customer ids.
     * @return WidgetSettingsOptions WidgetSettingsOptions object with populated LocationType, RouteType and RateType using random ids.
     */
    private WidgetSettingsOptions convertIdsToRandomIds(final List<Customer> customers, final WidgetSettingsOptions settingsOptions) {
        //----------- LocationType ---------------------------------------------------
        final List<LocationType> locTypes = settingsOptions.getLocationTypes();
        final List<LocationType> newLocTypes = new ArrayList<LocationType>(locTypes.size());
        final Iterator<LocationType> locTypesIter = locTypes.iterator();
        while (locTypesIter.hasNext()) {
            final LocationType locType = locTypesIter.next();
            locType.setOrgId(findCustomerRandomIdById(Integer.parseInt(locType.getOrgId()), customers));
            newLocTypes.add(locType);
        }
        settingsOptions.setLocationTypes(newLocTypes);

        //----------- ParentLocationType ---------------------------------------------------
        final List<LocationType> parentLocTypes = settingsOptions.getParentLocationTypes();
        final List<LocationType> newParentLocTypes = new ArrayList<LocationType>(parentLocTypes.size());
        final Iterator<LocationType> parentLocTypesIter = parentLocTypes.iterator();
        while (parentLocTypesIter.hasNext()) {
            final LocationType locType = parentLocTypesIter.next();
            locType.setOrgId(findCustomerRandomIdById(Integer.parseInt(locType.getOrgId()), customers));
            newParentLocTypes.add(locType);
        }
        settingsOptions.setParentLocationTypes(newParentLocTypes);

        //----------- RouteType ---------------------------------------------------
        final List<RouteType> rouTypes = settingsOptions.getRouteTypes();
        final List<RouteType> newRouTypes = new ArrayList<RouteType>(rouTypes.size());
        final Iterator<RouteType> rouTypesIter = rouTypes.iterator();
        while (rouTypesIter.hasNext()) {
            final RouteType rouType = rouTypesIter.next();
            rouType.setOrgId(findCustomerRandomIdById(Integer.parseInt(rouType.getOrgId()), customers));
            newRouTypes.add(rouType);
        }
        settingsOptions.setRouteTypes(newRouTypes);
        
        //----------- RateType ---------------------------------------------------
        final List<RateType> raTypes = settingsOptions.getRateTypes();
        final List<RateType> newRaTypes = new ArrayList<RateType>(raTypes.size());
        final Iterator<RateType> raTypesIter = raTypes.iterator();
        while (raTypesIter.hasNext()) {
            final RateType raType = raTypesIter.next();
            raType.setOrgId(findCustomerRandomIdById(Integer.parseInt(raType.getOrgId()), customers));
            newRaTypes.add(raType);
        }
        settingsOptions.setRateTypes(newRaTypes);
        return settingsOptions;
    }
    
    private String findCustomerRandomIdById(final Integer id, final List<Customer> customers) {
        Customer cust;
        final Iterator<Customer> iter = customers.iterator();
        while (iter.hasNext()) {
            cust = iter.next();
            if (cust.getId().intValue() == id.intValue()) {
                return cust.getRandomId();
            }
        }
        return null;
    }
    
    /**
     * @return WidgetMetrics If customer is a parent organization, return WidgetMetrics object with 'widgetmetricsParent.xml'. Otherwise return
     *         with 'widgetmetrics.xml' data.
     */
    // This is for the sake of unit-test
    @SuppressWarnings("checkstyle:designforextension")
    public WidgetMetrics getWidgetMetrics() {
        if (WebSecurityUtil.getUserAccount().getCustomer().isIsParent()) {
            return this.widgetMetricsParent;
        }
        return this.widgetMetrics;
    }
}
