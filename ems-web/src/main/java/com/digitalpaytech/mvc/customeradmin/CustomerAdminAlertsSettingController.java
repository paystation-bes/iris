package com.digitalpaytech.mvc.customeradmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.mvc.AlertsSettingController;
import com.digitalpaytech.mvc.support.AlertEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

/**
 * This class handles the Alert setting request in customer admin Settings -> Alerts menu.
 * 
 * @author Brian Kim
 */
@Controller
@SessionAttributes({ "alertEditForm" })
public class CustomerAdminAlertsSettingController extends AlertsSettingController {
    
    /**
     * This method will look at the permissions in the WebUser and pass control
     * to the first tab that the user has permissions for.
     * 
     * @return Will return the method for definedAlerts, or error if no
     *         permissions exist.
     */
    @RequestMapping(value = "/secure/settings/alerts/index.html")
    public final String getFirstPermittedTab(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasValidCustomerSubscription(WebCoreConstants.SUBSCRIPTION_TYPE_ALERTS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        if (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS)
            || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT)) {
            return getAlertList(request, response, model);
        } else {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
    }
    
    /**
     * This method will retrieve defined alert list and list of route type and
     * display them in Settings -> Alerts main page.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return target vm file URI which is mapped to Settings->Alerts->Defined
     *         Alerts (/settings/alerts/index.vm)
     */
    @RequestMapping(value = "/secure/settings/alerts/definedAlerts.html", method = RequestMethod.GET)
    public final String getAlertList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasValidCustomerSubscription(WebCoreConstants.SUBSCRIPTION_TYPE_ALERTS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS)
            && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        return super.getAlertListSuper(request, response, model, "/settings/alerts/definedAlerts");
    }
    
    /**
     * This method will sort the alert list based on user input and convert them
     * into JSON list and pass back to UI.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return string having sorted alert list information. "false" in case of
     *         any exception or validation failure.
     */
    @RequestMapping(value = "/secure/settings/alerts/sortAlertList.html", method = RequestMethod.GET)
    public final String sortAlertList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasValidCustomerSubscription(WebCoreConstants.SUBSCRIPTION_TYPE_ALERTS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS)
            && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.sortAlertListSuper(request, response, model);
    }
    
    /**
     * This method will view Alert Detail information on the page. When
     * "Edit Alert" is clicked, it will also be called from front-end logic
     * automatically after calling getAlertForm() method.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return JSON string having alert detail information. "false" in case of
     *         any exception or validation failure.
     */
    @RequestMapping(value = "/secure/settings/alerts/viewAlertDetails.html", method = RequestMethod.GET)
    @ResponseBody
    public final String viewAlertDetails(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasValidCustomerSubscription(WebCoreConstants.SUBSCRIPTION_TYPE_ALERTS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS)
            && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        return super.viewAlertDetailsSuper(request, response, model);
    }
    
    /**
     * This method save user defined alert information.
     * 
     * @param webSecurityForm
     *            User input form
     * @param result
     *            BindingResult object to keep errors.
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return "true" string value if process succeeds, JSON string if fails.
     */
    @RequestMapping(value = "/secure/settings/alerts/saveAlert.html", method = RequestMethod.POST)
    @ResponseBody
    public final String saveAlert(@ModelAttribute("alertEditForm") final WebSecurityForm<AlertEditForm> webSecurityForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasValidCustomerSubscription(WebCoreConstants.SUBSCRIPTION_TYPE_ALERTS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.saveAlertSuper(webSecurityForm, result, request, response, model);
    }
    
    /**
     * This method deletes User Defined Alert information.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return "true" string value if process succeeds, "false" string value if
     *         process fails.
     */
    @RequestMapping(value = "/secure/settings/alerts/deleteAlert.html", method = RequestMethod.GET)
    @ResponseBody
    public final String deleteAlert(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasValidCustomerSubscription(WebCoreConstants.SUBSCRIPTION_TYPE_ALERTS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.deleteAlertSuper(request, response, model);
    }
    
    /**
     * This method sends the test notification email.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param webSecurityForm
     *            User input form
     * @param result
     *            BindingResult object to keep errors.
     * @return "true" string value if process succeeds, JSON string value if
     *         process fails.
     */
    @RequestMapping(value = "/secure/settings/alerts/testNotification.html", method = RequestMethod.GET)
    @ResponseBody
    public final String testNotification(final HttpServletRequest request, final HttpServletResponse response,
        @ModelAttribute("alertEditForm") final WebSecurityForm<AlertEditForm> webSecurityForm, final BindingResult result) {
        
        if (!WebSecurityUtil.hasValidCustomerSubscription(WebCoreConstants.SUBSCRIPTION_TYPE_ALERTS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.testNotificationSuper(request, response, webSecurityForm, result);
    }
}
