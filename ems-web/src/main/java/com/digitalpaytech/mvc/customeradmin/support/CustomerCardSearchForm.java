package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;

import com.digitalpaytech.constants.SortOrder;

public class CustomerCardSearchForm implements Serializable {
    private static final long serialVersionUID = 2201649621631437377L;
    
    public enum SortColumn {
    	CARD_NUMBER("cardNumber"),
    	CARD_TYPE("customerCardType.id"),
    	VALID_FROM("cardBeginGmt"),
    	VALID_TO("cardExpireGmt"),
    	RESTRICTED("isForNonOverlappingUse"),
    	GRACE_PERIOD("gracePeriodMinutes");
    	
    	private String[] actualAttribute;
    	
    	SortColumn(String... actualAttribute) {
    		this.actualAttribute = actualAttribute;
    	}
    	
    	public String[] getActualAttribute() {
    		return actualAttribute;
    	}
    }
    
    private String customerCardRandomIds;
    
    private String filterValue;
    
    private Integer page;
    private Integer itemsPerPage;
    private SortColumn column;
    private SortOrder order;
    private String dataKey;
    
    private String targetRandomId;
    
    private boolean showOnlySelectedCards;
    
    private String ignoreConsumerRandomId;
    private String ignoreRandomIds;
    
    public CustomerCardSearchForm() {
        
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public SortColumn getColumn() {
        return column;
    }

    public void setColumn(SortColumn column) {
        this.column = column;
    }

    public SortOrder getOrder() {
        return order;
    }

    public void setOrder(SortOrder order) {
        this.order = order;
    }

	public Integer getItemsPerPage() {
		return itemsPerPage;
	}

	public void setItemsPerPage(Integer itemsPerPage) {
		this.itemsPerPage = itemsPerPage;
	}

	public String getCustomerCardRandomIds() {
		return customerCardRandomIds;
	}

	public void setCustomerCardRandomIds(String customerCardRandomIds) {
		this.customerCardRandomIds = customerCardRandomIds;
	}

	public String getDataKey() {
		return dataKey;
	}

	public void setDataKey(String dataKey) {
		this.dataKey = dataKey;
	}

	public boolean isShowOnlySelectedCards() {
		return showOnlySelectedCards;
	}

	public void setShowOnlySelectedCards(boolean showOnlySelectedCards) {
		this.showOnlySelectedCards = showOnlySelectedCards;
	}

	public String getFilterValue() {
		return filterValue;
	}

	public void setFilterValue(String filterValue) {
		this.filterValue = filterValue;
	}

	public String getTargetRandomId() {
		return targetRandomId;
	}

	public void setTargetRandomId(String targetRandomId) {
		this.targetRandomId = targetRandomId;
	}

	public String getIgnoreRandomIds() {
		return ignoreRandomIds;
	}

	public void setIgnoreRandomIds(String ignoreRandomIds) {
		this.ignoreRandomIds = ignoreRandomIds;
	}

	public String getIgnoreConsumerRandomId() {
		return ignoreConsumerRandomId;
	}

	public void setIgnoreConsumerRandomId(String ignoreConsumerRandomId) {
		this.ignoreConsumerRandomId = ignoreConsumerRandomId;
	}
}
