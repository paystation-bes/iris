package com.digitalpaytech.mvc.customeradmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebPermissionUtil;

@Controller
public class CustomerSettingsIndexController {
	
	/**
	 * This method will look at the permissions in the WebUser and pass control
	 * to the first tab that the user has permissions for.
	 * 
	 * @return Will return the method for routeDetails, or error if no permissions exist.
	 */
	@RequestMapping(value = "/secure/settings/index.html")
	public String getFirstPermittedTab(HttpServletRequest request, HttpServletResponse response, ModelMap model) {

		if (WebPermissionUtil.hasCustomerSettingsGlobalTabPermissions()) {
			return "redirect:/secure/settings/global/index.html";
		} 
		else if (WebPermissionUtil.hasCustomerSettingsLocationsTabPermissions()) {
			return "redirect:/secure/settings/locations/index.html";
		} 
		else if (WebPermissionUtil.hasCustomerSettingsRoutesTabPersmissions()) {
			return "redirect:/secure/settings/routes/index.html";
		} 
		else if (WebPermissionUtil.hasCustomerSettingsAlertsTabPersmissions()) {
			return "redirect:/secure/settings/alerts/index.html";
		} 
		else if (WebPermissionUtil.hasCustomerSettingsUsersTabPersmissions()) {
			return "redirect:/secure/settings/users/index.html";
		} 
		else if (WebPermissionUtil.hasCustomerSettingsCardSettingsPersmissions()) {
			return "redirect:/secure/settings/cardSettings/index.html";
		} 
		else {
		    return "redirect:/secure/settings/global/sysNotification.html";
		}
	}
}
