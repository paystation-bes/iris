package com.digitalpaytech.mvc.customeradmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.mvc.RateRateProfileController;
import com.digitalpaytech.mvc.support.RateRateProfileEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
public class CustomerAdminRateRateProfileController extends RateRateProfileController {
    /**
     * This method will retrieve rate profile list
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return JSON object with rate profile list
     * 
     */
    @RequestMapping(value = "/secure/settings/rates/rateRateProfileList.html", method = RequestMethod.GET)
    @ResponseBody
    public final String getRateRateProfileList(final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!checkPermissions(false)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.getList(request, response);
    }
    
    @RequestMapping(value = "/secure/settings/rates/addRateRateProfile.html", method = RequestMethod.GET)
    public final String addRateRateProfile(@ModelAttribute(FORM_EDIT) final WebSecurityForm<RateRateProfileEditForm> webSecurityForm, final ModelMap model,
                                           final HttpServletRequest request, final HttpServletResponse response) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.add(webSecurityForm, model, request, response, "/settings/include/rateRateProfile");
    }
    
    @RequestMapping(value = "/secure/settings/rates/editRateRateProfile.html", method = RequestMethod.GET)
    public final String editRateRateProfile(@ModelAttribute(FORM_EDIT) final WebSecurityForm<RateRateProfileEditForm> webSecurityForm,
                                            @RequestParam("rateRateProfileID") final String randomId,
                                            @RequestParam("rateProfileID") final String rateProfileRandomId, final ModelMap model,
                                            final HttpServletRequest request, final HttpServletResponse response) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.edit(webSecurityForm, randomId, rateProfileRandomId, model, request, response, "/settings/include/rateRateProfile");
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/settings/rates/saveRateRateProfile.html", method = RequestMethod.POST)
    public final String saveRateRateProfile(@ModelAttribute(FORM_EDIT) final WebSecurityForm<RateRateProfileEditForm> webSecurityForm,
                                            final BindingResult result, final HttpServletRequest request, final HttpServletResponse response) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.save(webSecurityForm, result, request, response);
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/settings/rates/rescheduleRateRateProfile.html", method = RequestMethod.POST)
    public final String rescheduleRateRateProfile(@ModelAttribute(FORM_SCHEDULE) final WebSecurityForm<RateRateProfileEditForm> webSecurityForm,
                                                  final BindingResult result, final HttpServletRequest request, final HttpServletResponse response) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.reschedule(webSecurityForm, result, request, response);
    }
    
    @RequestMapping(value = "/secure/settings/rates/deleteRateRateProfile.html", method = RequestMethod.GET)
    @ResponseBody
    public final String deleteRateProfile(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.delete(request, response, model);
    }
    
    @ModelAttribute(FORM_EDIT)
    public final WebSecurityForm<RateRateProfileEditForm> initEditForm(final HttpServletRequest request) {
        return new WebSecurityForm<RateRateProfileEditForm>((Object) null, new RateRateProfileEditForm(), SessionTool.getInstance(request)
                .locatePostToken(FORM_EDIT));
    }
    
    @ModelAttribute(FORM_SCHEDULE)
    public final WebSecurityForm<RateRateProfileEditForm> initScheduleForm(final HttpServletRequest request) {
        return new WebSecurityForm<RateRateProfileEditForm>((Object) null, new RateRateProfileEditForm(), SessionTool.getInstance(request)
                .locatePostToken(FORM_SCHEDULE));
    }
    
    private boolean checkPermissions(final boolean isManageOnly) {
        if (!WebSecurityUtil.hasValidCustomerSubscription(WebCoreConstants.SUBSCRIPTION_TYPE_ONLINE_RATE_CONFIGURATION)) {
            return false;
        }
        
        if (isManageOnly) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_RATES)) {
                return false;
            }
        } else {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_RATES)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_RATES)) {
                return false;
            }
        }
        return true;
    }
    
}
