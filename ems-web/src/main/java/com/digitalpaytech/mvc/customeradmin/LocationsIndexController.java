package com.digitalpaytech.mvc.customeradmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebPermissionUtil;

@Controller
public class LocationsIndexController {
		
	/**
	 * This method will look at the permissions in the WebUser and pass control
	 * to the first tab that the user has permissions for.
	 * 
	 * @return Will return the method for locationDetails, extendByPhone,
	 *         payStationPlacement, or payStationList depending on permissions,
	 *         or error if no permissions exist.
	 */
	@RequestMapping(value = "/secure/settings/locations/index.html")
	public String getFirstPermittedTab(HttpServletRequest request, HttpServletResponse response, ModelMap model) {

		if (WebPermissionUtil.hasCustomerSettingsLocationsDetailsTabPermissions()) 
		{
			return "redirect:/secure/settings/locations/locationDetails.html";
		} 
		else if (WebPermissionUtil.hasCustomerSettingsExtendByPhoneTabPermissions()) 
		{
			return "redirect:/secure/settings/locations/extendByPhone.html";
		}  
		else if (WebPermissionUtil.hasCustomerSettingsPayStationListPermissions()) 
		{
			return "redirect:/secure/settings/locations/payStationList.html";
		}
		else if (WebPermissionUtil.hasCustomerSettingsPayStationSettings()) {
			return "redirect:/secure/settings/locations/payStationSettings.html";
		}
		else if (WebPermissionUtil.hasCustomerSettingsPayStationPlacementPermissions()) 
		{
			return "redirect:/secure/settings/locations/payStationPlacement.html";
		}
		else {
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			return WebCoreConstants.RESPONSE_FALSE;
		}		
	}
}
