package com.digitalpaytech.mvc.customeradmin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.constants.SortOrder;
import com.digitalpaytech.domain.Consumer;
import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerCard;
import com.digitalpaytech.domain.EmailAddress;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.PaginatedList;
import com.digitalpaytech.dto.customeradmin.ConsumerSearchCriteria;
import com.digitalpaytech.dto.customeradmin.CouponDTO;
import com.digitalpaytech.dto.customeradmin.CustomerAdminConsumerAccountInfo;
import com.digitalpaytech.dto.customeradmin.CustomerCardDTO;
import com.digitalpaytech.mvc.customeradmin.support.CouponSearchForm;
import com.digitalpaytech.mvc.customeradmin.support.CustomerAdminConsumerAccountEditForm;
import com.digitalpaytech.mvc.customeradmin.support.CustomerCardSearchForm;
import com.digitalpaytech.mvc.helper.AccountBeanHelper;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.ConsumerService;
import com.digitalpaytech.service.CouponService;
import com.digitalpaytech.service.CustomerCardService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.customeradmin.CustomerAdminConsumerAccountsValidator;

@Controller
public class CustomerAdminConsumerAccountsController {
    public static final String FORM_EDIT = "consumerEditForm";
    
    @Autowired
    private ConsumerService consumerService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private CouponService couponService;
    
    @Autowired
    private CustomerCardService customerCardService;
    
    @Autowired
    private CustomerAdminConsumerAccountsValidator validator;
    
    @Autowired
    private AccountBeanHelper accBeanHelper;
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    /**
     * This method will retrieve a list of consumer accounts for the current user (customer)
     * 
     * @param request
     * @param response
     * @param model
     * @return Model containing current customer consumer information in list
     *         form, and location edit form and returns target URL which is
     *         mapped to Accounts -> Accounts (/settings/consumerAccounts/index.html)
     */
    @RequestMapping(value = "/secure/accounts/consumerAccounts/index.html")
    public final String consumerList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_EDIT) final WebSecurityForm<CustomerAdminConsumerAccountEditForm> webSecurityForm,
        @ModelAttribute(CouponController.FORM_SEARCH) final WebSecurityForm<CouponSearchForm> couponSearchForm,
        @ModelAttribute(CustomerCardController.FORM_SEARCH) final WebSecurityForm<CustomerCardSearchForm> customerCardSearchForm) {
        if (!WebSecurityUtil.permissionAudit(response, WebSecurityConstants.PERMISSION_VIEW_CONSUMER_ACCOUNTS,
                                             WebSecurityConstants.PERMISSION_MANAGE_CONSUMER_ACCOUNTS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        final CustomerAdminConsumerAccountEditForm customerAdminConsumerAccountEditForm = webSecurityForm.getWrappedObject();
        model.put(FORM_EDIT, webSecurityForm);
        model.put(CouponController.FORM_SEARCH, couponSearchForm);
        model.put(CustomerCardController.FORM_SEARCH, customerCardSearchForm);
        
        final Date timeStamp = new Date();
        customerAdminConsumerAccountEditForm.setDataKey(timeStamp.getTime());
        final List<CustomerAdminConsumerAccountInfo> consumerList = this.prepareConsumerAccountList(webSecurityForm, request);
        
        model.put("consumerList", WidgetMetricsHelper.convertToJson(consumerList, "Consumers", true));
        
        return "/accounts/consumerAccounts/index";
    }
    
    private CustomerAdminConsumerAccountEditForm.SortColumn defaultIfNull(final CustomerAdminConsumerAccountEditForm.SortColumn column) {
        return (column == null) ? CustomerAdminConsumerAccountEditForm.SortColumn.lastName : column;
    }
    
    private SortOrder defaultIfNull(final SortOrder order) {
        return (order == null) ? SortOrder.ASC : order;
    }
    
    private List<CustomerAdminConsumerAccountInfo> prepareConsumerAccountList(
        final WebSecurityForm<CustomerAdminConsumerAccountEditForm> webSecurityForm, final HttpServletRequest request) {
        final CustomerAdminConsumerAccountEditForm form = webSecurityForm.getWrappedObject();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final Integer customerId = userAccount.getCustomer().getId();
        
        Collection<Consumer> consumers;
        
        if (form == null || form.getFilterValue() == null || form.getFilterValue().length() == 0) {
            consumers = this.consumerService.findConsumerByCustomerId(customerId, null, form.getItemsPerPage(), defaultIfNull(form.getColumn())
                    .toString(), defaultIfNull(form.getOrder()).toString(), form.getDataKey());
        } else {
            consumers = this.consumerService.findConsumerByCustomerIdAndFilterValue(customerId, form.getFilterValue(), null, form.getItemsPerPage(),
                                                                                    defaultIfNull(form.getColumn()).toString(),
                                                                                    defaultIfNull(form.getOrder()).toString(), form.getDataKey());
        }
        
        final List<CustomerAdminConsumerAccountInfo> consumerList = new ArrayList<CustomerAdminConsumerAccountInfo>();
        for (Consumer consumer : consumers) {
            final String randomConsumerId = keyMapping.getRandomString(consumer, WebCoreConstants.ID_LOOK_UP_NAME);
            final EmailAddress emailAddress = consumer.getEmailAddress();
            String emailAddressEmail = null;
            if (emailAddress != null) {
                emailAddressEmail = emailAddress.getEmail();
            }
            consumerList.add(new CustomerAdminConsumerAccountInfo(randomConsumerId, consumer.getFirstName(), consumer.getLastName(),
                    emailAddressEmail, consumer.getDescription()));
        }
        
        return consumerList;
    }
    
    /**
     * This method will retrieve a certain page from the list of consumer accounts for the current user (customer)
     * 
     * @param request
     * @param response
     * @param model
     * @return List of consumer accounts as a json string
     * 
     */
    @RequestMapping(value = "/secure/accounts/consumerAccountsPage/index.html")
    @ResponseBody
    public final String consumerListPage(@ModelAttribute(FORM_EDIT) final WebSecurityForm<CustomerAdminConsumerAccountEditForm> webSecurityForm,
        final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!WebSecurityUtil.permissionAudit(response, WebSecurityConstants.PERMISSION_VIEW_CONSUMER_ACCOUNTS,
                                             WebSecurityConstants.PERMISSION_MANAGE_CONSUMER_ACCOUNTS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        final CustomerAdminConsumerAccountEditForm customerAdminConsumerAccountEditForm = webSecurityForm.getWrappedObject();
        if (customerAdminConsumerAccountEditForm.getDataKey() == null) {
            final Date timeStamp = new Date();
            customerAdminConsumerAccountEditForm.setDataKey(timeStamp.getTime());
        }
        
        final PaginatedList<CustomerAdminConsumerAccountInfo> consumerList = this.prepareConsumerAccountListPage(webSecurityForm, request);
        consumerList.setToken(webSecurityForm.getInitToken());
        return WidgetMetricsHelper.convertToJson(consumerList, "Consumers", true);
    }
    
    /**
     * This method retrieves a list from a certain page from the list of consumer accounts for the current user (customer)
     * 
     * @param request
     * @param response
     * @param model
     * @return list of consumer accounts
     */
    private PaginatedList<CustomerAdminConsumerAccountInfo> prepareConsumerAccountListPage(
        final WebSecurityForm<CustomerAdminConsumerAccountEditForm> webSecurityForm, final HttpServletRequest request) {
        final CustomerAdminConsumerAccountEditForm form = webSecurityForm.getWrappedObject();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final Integer customerId = userAccount.getCustomer().getId();
        
        Collection<Consumer> consumers;
        if (form == null || form.getFilterValue() == null || form.getFilterValue().length() == 0) {
            consumers = this.consumerService.findConsumerByCustomerId(customerId, form.getPage(), form.getItemsPerPage(),
                                                                      defaultIfNull(form.getColumn()).toString(), defaultIfNull(form.getOrder())
                                                                              .toString(), form.getDataKey());
        } else {
            consumers = this.consumerService.findConsumerByCustomerIdAndFilterValue(customerId, form.getFilterValue(), form.getPage(), form
                    .getItemsPerPage(), defaultIfNull(form.getColumn()).toString(), defaultIfNull(form.getOrder()).toString(), form.getDataKey());
        }
        
        final List<CustomerAdminConsumerAccountInfo> consumerInfoList = new ArrayList<CustomerAdminConsumerAccountInfo>();
        for (Consumer consumer : consumers) {
            final String randomConsumerId = keyMapping.getRandomString(consumer, WebCoreConstants.ID_LOOK_UP_NAME);
            final EmailAddress emailAddress = consumer.getEmailAddress();
            String emailAddressEmail = null;
            if (emailAddress != null) {
                emailAddressEmail = emailAddress.getEmail();
            }
            consumerInfoList.add(new CustomerAdminConsumerAccountInfo(randomConsumerId, consumer.getFirstName(), consumer.getLastName(),
                    emailAddressEmail, consumer.getDescription()));
        }
        
        final PaginatedList<CustomerAdminConsumerAccountInfo> consumerList = new PaginatedList<CustomerAdminConsumerAccountInfo>();
        consumerList.setElements(consumerInfoList);
        if (form.getDataKey() != null) {
            consumerList.setDataKey(form.getDataKey().toString());
        }
        
        return consumerList;
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/accounts/consumerAccountsPage/locateItemPage.html", method = RequestMethod.POST)
    public final String getPageNumber(@ModelAttribute(FORM_EDIT) final WebSecurityForm<CustomerAdminConsumerAccountEditForm> secureForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response) {
        String resultJson = WebCoreConstants.RESPONSE_FALSE;
        if (!WebSecurityUtil.permissionAudit(response, WebSecurityConstants.PERMISSION_VIEW_CONSUMER_ACCOUNTS,
                                             WebSecurityConstants.PERMISSION_MANAGE_CONSUMER_ACCOUNTS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        } else {
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            final CustomerAdminConsumerAccountEditForm form = secureForm.getWrappedObject();
            
            final ConsumerSearchCriteria criteria = createSearchCriteria(form, request);
            int targetPage = -1;
            final Integer targetId = keyMapping.getKey(form.getTargetRandomId(), Consumer.class, Integer.class);
            if (targetId != null) {
                criteria.setMaxUpdatedTime(new Date());
                form.setDataKey(criteria.getMaxUpdatedTime().getTime());
                
                targetPage = this.consumerService.findRecordPage(targetId, criteria);
            }
            
            if (targetPage > 0) {
                criteria.setPage(targetPage);
                
                final PaginatedList<CustomerAdminConsumerAccountInfo> consumersList = prepareConsumerAccountListPage(secureForm, request);
                consumersList.setPage(targetPage);
                
                resultJson = WidgetMetricsHelper.convertToJson(consumersList, "Consumers", true);
            }
        }
        
        return resultJson;
    }
    
    private ConsumerSearchCriteria createSearchCriteria(final CustomerAdminConsumerAccountEditForm form, final HttpServletRequest request) {
        final ConsumerSearchCriteria result = new ConsumerSearchCriteria();
        result.setCustomerId(WebSecurityUtil.getUserAccount().getCustomer().getId());
        
        if ((form.getFilterValue() != null) && (form.getFilterValue().length() > 0)) {
            final String filterVal = StandardConstants.STRING_PERCENTAGE + form.getFilterValue() + StandardConstants.STRING_PERCENTAGE;
            result.setFirstName(filterVal);
            result.setLastName(filterVal);
            result.setFullName(filterVal);
            result.setEmailAddress(filterVal);
        }
        
        result.setPage((form.getPage() == null) ? 1 : form.getPage());
        if (form.getItemsPerPage() == null) {
            result.setItemsPerPage(WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT);
        } else if (form.getItemsPerPage() > WebCoreConstants.MAX_RESULT_COUNT) {
            result.setItemsPerPage(WebCoreConstants.MAX_RESULT_COUNT);
        } else {
            result.setItemsPerPage(form.getItemsPerPage());
        }
        
        result.setOrderColumn(defaultIfNull(form.getColumn()).getActualAttribute());
        result.setOrderDesc((defaultIfNull(form.getOrder()) == SortOrder.ASC) ? false : true);
        
        if (form.getDataKey() != null) {
            result.setMaxUpdatedTime(new Date(form.getDataKey()));
        }
        
        return result;
    }
    
    /**
     * This method will view Consumer Account Detail information on the page.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return JSON string having consumer detail information. "false" in case of
     *         any exception or validation failure.
     */
    @RequestMapping(value = "/secure/accounts/viewConsumerAccountDetails.html", method = RequestMethod.GET)
    @ResponseBody
    public final String viewAlertDetails(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.permissionAudit(response, WebSecurityConstants.PERMISSION_VIEW_CONSUMER_ACCOUNTS,
                                             WebSecurityConstants.PERMISSION_MANAGE_CONSUMER_ACCOUNTS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        final Integer actualConsumerId = (Integer) CustomerAdminConsumerAccountsValidator.validateRandomIdNoForm(request, "consumerId");
        if (actualConsumerId == WebCoreConstants.SHORT_RECORD_NOT_FOUND) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final Consumer consumer = this.consumerService.findConsumerById(actualConsumerId);
        if (consumer == null) {
            final MessageInfo message = new MessageInfo();
            message.setError(true);
            message.setToken(null);
            message.addMessage(this.commonControllerHelper.getMessage(ErrorConstants.REQUESTED_ITEM_DELETED));
            return WidgetMetricsHelper.convertToJson(message, WebCoreConstants.UI_MESSAGES, true);
        }

        final CustomerAdminConsumerAccountEditForm consumerAccountDetails = this.prepareConsumerAccountDetails(request, consumer);
        final List<Coupon> coupons = this.couponService.findCouponByConsumerId(actualConsumerId);
        consumerAccountDetails.setCoupons(new ArrayList<CouponDTO>(coupons.size()));
        for (Coupon coupon : coupons) {
            final CouponDTO couponDTO = new CouponDTO();
            this.accBeanHelper.populate(coupon, couponDTO, keyMapping, true, true);
            
            consumerAccountDetails.getCoupons().add(couponDTO);
        }
        
        final List<CustomerCard> customCards = this.customerCardService.findCustomerCardByConsumerId(actualConsumerId);
        consumerAccountDetails.setCustomCards(new ArrayList<CustomerCardDTO>(customCards.size()));
        for (CustomerCard card : customCards) {
            final CustomerCardDTO cardDTO = new CustomerCardDTO();
            this.accBeanHelper.populate(card, cardDTO, keyMapping, true);
            consumerAccountDetails.getCustomCards().add(cardDTO);
        }
        
        return WidgetMetricsHelper.convertToJson(consumerAccountDetails, "consumerAccountDetails", true);
    }
    
    /**
     * This method prepares defined alert details to display alert details on
     * the page.
     * 
     * @param consumerId
     *            consumer ID
     * @return ConsumerAccountDetails object
     */
    private CustomerAdminConsumerAccountEditForm prepareConsumerAccountDetails(final HttpServletRequest request, final Consumer consumer) {
        final CustomerAdminConsumerAccountEditForm consumerAccountEditForm = new CustomerAdminConsumerAccountEditForm();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        consumerAccountEditForm.setRandomId(request.getParameter("consumerId"));
        String randomEmailAddressId = null;
        if (consumer.getEmailAddress() != null) {
            randomEmailAddressId = keyMapping.getRandomString(EmailAddress.class, consumer.getEmailAddress().getId());
            consumerAccountEditForm.setEmailAddressRandomId(randomEmailAddressId);
            consumerAccountEditForm.setEmailAddress(consumer.getEmailAddress().getEmail());
        }
        consumerAccountEditForm.setFirstName(consumer.getFirstName());
        consumerAccountEditForm.setLastName(consumer.getLastName());
        consumerAccountEditForm.setDescription(consumer.getDescription());
        
        List<String> couponRandomIds = new ArrayList<String>();
        for (Coupon coupon : consumer.getCoupons()) {
            couponRandomIds.add(keyMapping.getRandomString(Coupon.class, coupon.getId()));
        }
        
        if (couponRandomIds.size() == 0) {
            couponRandomIds = null;
        }
        consumerAccountEditForm.setCouponRandomIds(couponRandomIds);
        
        List<String> customerCardRandomIds = new ArrayList<String>();
        for (CustomerCard customerCard : consumer.getCustomerCards()) {
            customerCardRandomIds.add(keyMapping.getRandomString(CustomerCard.class, customerCard.getId()));
        }
        
        if (customerCardRandomIds.size() == 0) {
            customerCardRandomIds = null;
        }
        consumerAccountEditForm.setCustomCardRandomIds(customerCardRandomIds);
        
        return consumerAccountEditForm;
    }
    
    /**
     * This method save user defined consumer information.
     * 
     * @param webSecurityForm
     *            User input form
     * @param result
     *            BindingResult object to keep errors.
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return "true" string value if process succeeds, JSON string if fails.
     */
    @RequestMapping(value = "/secure/accounts/saveConsumerAccounts.html", method = RequestMethod.POST)
    @ResponseBody
    public final String saveConsumerAccount(@ModelAttribute(FORM_EDIT) final WebSecurityForm<CustomerAdminConsumerAccountEditForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!WebSecurityUtil.permissionAudit(response, WebSecurityConstants.PERMISSION_VIEW_CONSUMER_ACCOUNTS,
                                             WebSecurityConstants.PERMISSION_MANAGE_CONSUMER_ACCOUNTS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        this.validator.validate(webSecurityForm, result, keyMapping);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        webSecurityForm.resetToken();
        
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_CREATE) {
            /* Add Alert. */
            return addConsumer(request, webSecurityForm);
        } else if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
            /* Edit Alert. */
            return editConsumer(request, webSecurityForm);
        } else {
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
    }
    
    /**
     * This method save alert information in CustomerAlertType table.
     * 
     * @param request
     *            HttpServletRequest object
     * @param webSecurityForm
     *            user input form
     * @return "true" + form token value if process succeeds, "false" string if
     *         process fails.
     */
    private String addConsumer(final HttpServletRequest request, final WebSecurityForm<CustomerAdminConsumerAccountEditForm> webSecurityForm) {
        
        final CustomerAdminConsumerAccountEditForm consumerAcctForm = webSecurityForm.getWrappedObject();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final Consumer consumer = new Consumer();
        final Date currentTime = new Date();
        Customer customer = null;
        
        if (userAccount.getCustomer().getCustomerType().getId() == WebCoreConstants.CUSTOMER_TYPE_DPT) {
            final Integer customerId = userAccount.getCustomer().getId();
            if (customerId < 1) {
                return WebCoreConstants.RESPONSE_FALSE;
            }
            customer = this.customerService.findCustomer(customerId);
            
        } else {
            customer = userAccount.getCustomer();
        }
        
        consumer.setCustomer(customer);
        consumer.setFirstName(consumerAcctForm.getFirstName());
        consumer.setLastName(consumerAcctForm.getLastName());
        consumer.setDescription(consumerAcctForm.getDescription());
        consumer.setLastModifiedGmt(currentTime);
        consumer.setLastModifiedByUserId(userAccount.getId());
        
        if (consumerAcctForm.getCouponIds() != null) {
            final Set<Coupon> consumerCoupons = new HashSet<Coupon>();
            for (Integer couponId : consumerAcctForm.getCouponIds()) {
                final Coupon coupon = this.couponService.findCouponById(couponId);
                consumerCoupons.add(coupon);
            }
            consumer.setCoupons(consumerCoupons);
        }
        
        if (consumerAcctForm.getCustomCardIds() != null) {
            final Set<CustomerCard> customerCards = new HashSet<CustomerCard>();
            for (Integer customerCardId : consumerAcctForm.getCustomCardIds()) {
                final CustomerCard customerCard = this.customerCardService.getCustomerCard(customerCardId);
                customerCards.add(customerCard);
            }
            consumer.setCustomerCards(customerCards);
        }
        
        if (consumerAcctForm.getEmailAddressObj() == null && !StringUtils.isEmpty(consumerAcctForm.getEmailAddress())) {
            final EmailAddress email = new EmailAddress();
            email.setEmail(consumerAcctForm.getEmailAddress());
            email.setLastModifiedGmt(currentTime);
            consumer.setEmailAddress(email);
        }
        
        this.consumerService.createConsumer(consumer);
        
        return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(StandardConstants.STRING_COLON).append(webSecurityForm.getInitToken())
                .toString();
    }
    
    /**
     * This method updates Consumer Account information.
     * 
     * @param request
     *            HttpServletRequest object
     * @param webSecurityForm
     *            user input form.
     * @return "true" value if process succeeds, "false" string if process
     *         fails.
     */
    private String editConsumer(final HttpServletRequest request, final WebSecurityForm<CustomerAdminConsumerAccountEditForm> webSecurityForm) {
        
        final CustomerAdminConsumerAccountEditForm editForm = webSecurityForm.getWrappedObject();
        //        RandomKeyMapping keyMapping = SessionUtil.getRandomKeyMapping(request);
        final Consumer consumer = this.consumerService.findConsumerById(editForm.getConsumerId());
        
        consumer.setFirstName(editForm.getFirstName());
        consumer.setLastName(editForm.getLastName());
        consumer.setDescription(editForm.getDescription());
        
        //        Set<Coupon> consumerCoupons = new HashSet<Coupon>();
        //        List<String> couponRandomIds = new ArrayList<String>();
        //        if (editForm.getCouponIds() != null){
        //            for (Integer couponId : editForm.getCouponIds()) {
        //                Coupon coupon = this.couponService.findCouponById(couponId);
        //                consumerCoupons.add(coupon);                
        //                couponRandomIds.add(keyMapping.getRandomString(coupon, WebCoreConstants.ID_LOOK_UP_NAME));
        //            }
        //        }
        //        consumer.setCoupons(consumerCoupons);
        //        editForm.setCouponRandomIds(couponRandomIds);
        //        
        //        
        //        Set<CustomerCard> customerCards = new HashSet<CustomerCard>();
        //        List<String> customerCardRandomIds = new ArrayList<String>();
        //        if (editForm.getCustomCardIds() != null){
        //            for (Integer customerCardId : editForm.getCustomCardIds()) {
        //                CustomerCard customerCard = customerCardService.getCustomerCard(customerCardId);
        //                customerCard.setConsumer(consumer);                
        //                customerCards.add(customerCard);
        //                customerCardRandomIds.add(keyMapping.getRandomString(customerCard, WebCoreConstants.ID_LOOK_UP_NAME));
        //            }
        //        }
        //        consumer.setCustomerCards(customerCards);
        //        editForm.setCustomCardRandomIds(customerCardRandomIds);
        
        //        List <Coupon> existingCoupons = this.couponService.findCouponByConsumerId(consumer.getId());
        //        List <CustomerCard> existingCustomerCards = customerCardService.findCustomerCardByConsumerId(consumer.getId());
        
        if (consumer.getEmailAddress() == null) {
            if (editForm.getEmailAddressObj() != null) {
                consumer.setEmailAddress(editForm.getEmailAddressObj());
            } else {
                if (!StringUtils.isEmpty(editForm.getEmailAddress())) {
                    final EmailAddress emailAddress = new EmailAddress();
                    emailAddress.setEmail(editForm.getEmailAddress());
                    emailAddress.setLastModifiedGmt(new Date());
                    consumer.setEmailAddress(emailAddress);
                }
            }
        } else {
            if (!StringUtils.isEmpty(editForm.getEmailAddress())) {
                if (!editForm.getEmailAddress().equals(consumer.getEmailAddress().getEmail())) {
                    final EmailAddress emailAddress = new EmailAddress();
                    emailAddress.setEmail(editForm.getEmailAddress());
                    emailAddress.setLastModifiedGmt(new Date());
                    consumer.setEmailAddress(emailAddress);
                }
            }
        }
        
        this.consumerService.updateConsumer(consumer, editForm.getEmailAddress(), editForm.getCouponIds(), editForm.getCustomCardIds());
        return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(StandardConstants.STRING_COLON).append(webSecurityForm.getInitToken())
                .toString();
    }
    
    /**
     * This method deletes a Consumer Account and it's attached EmailAddress, ConsumerCoupon records
     * and sets the consumer id of any CustomerCards that refer to it back to null.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return "true" string value if process succeeds, "false" string value if
     *         process fails.
     */
    @RequestMapping(value = "/secure/accounts/deleteConsumer.html", method = RequestMethod.GET)
    @ResponseBody
    public final String deleteConsumer(final HttpServletRequest request, final HttpServletResponse response) {
        if (!WebSecurityUtil.permissionAudit(response, WebSecurityConstants.PERMISSION_VIEW_CONSUMER_ACCOUNTS,
                                             WebSecurityConstants.PERMISSION_MANAGE_CONSUMER_ACCOUNTS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        final Integer consumerId = (Integer) CustomerAdminConsumerAccountsValidator.validateRandomIdNoForm(request, "consumerId");
        if (consumerId == WebCoreConstants.SHORT_RECORD_NOT_FOUND) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        final Consumer consumer = this.consumerService.findConsumerById(consumerId);
        final boolean returnVal = this.consumerService.deleteConsumer(consumer);
        return Boolean.valueOf(returnVal).toString();
    }
    
    @ModelAttribute(FORM_EDIT)
    public final WebSecurityForm<CustomerAdminConsumerAccountEditForm> initEditForm(final HttpServletRequest request) {
        return new WebSecurityForm<CustomerAdminConsumerAccountEditForm>((Object) null, new CustomerAdminConsumerAccountEditForm(), SessionTool
                .getInstance(request).locatePostToken(FORM_EDIT));
    }
    
    @ModelAttribute(CouponController.FORM_SEARCH)
    public final WebSecurityForm<CouponSearchForm> initCouponSearchForm(final HttpServletRequest request) {
        return new WebSecurityForm<CouponSearchForm>((Object) null, new CouponSearchForm(), SessionTool.getInstance(request)
                .locatePostToken(CouponController.FORM_SEARCH));
    }
    
    @ModelAttribute(CustomerCardController.FORM_SEARCH)
    public final WebSecurityForm<CustomerCardSearchForm> initCustomerCardSearchForm(final HttpServletRequest request) {
        return new WebSecurityForm<CustomerCardSearchForm>((Object) null, new CustomerCardSearchForm(), SessionTool.getInstance(request)
                .locatePostToken(CustomerCardController.FORM_SEARCH));
    }
}
