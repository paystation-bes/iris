package com.digitalpaytech.mvc.customeradmin.support.io.dto;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

public class BadCardCsvDTO implements Serializable {
    
    private static final long serialVersionUID = -6022884389258713644L;
    
    private static final int CARD_TYPE_POS = 0;
    private static final int CARD_NUMBER_POS = 1;
    private static final int INTERNAL_KEY_POS = 2;
    private static final int EXPIRY_POS = 3;
    private static final int COMMENT_POS = 4;
    
    private String cardType;
    private String cardNumber;
    private String internalKey;
    private String expiryDate;
    private String comment;
    
    public BadCardCsvDTO() {
    }
    
    public BadCardCsvDTO(String[] tokens) {
        this.cardType = StringUtils.stripToNull(tokens[CARD_TYPE_POS]);
        this.cardNumber = StringUtils.stripToNull(tokens[CARD_NUMBER_POS]);
        this.internalKey = StringUtils.stripToNull(tokens[INTERNAL_KEY_POS]);
        this.expiryDate = StringUtils.stripToNull(tokens[EXPIRY_POS]);
        this.comment = StringUtils.stripToNull(tokens[COMMENT_POS]);
    }
    
    public String getCardType() {
        return cardType;
    }
    
    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }
    
    public String getCardNumber() {
        return cardNumber;
    }
    
    public void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }
    
    public String getExpiryDate() {
        return expiryDate;
    }
    
    public void setExpiryDate(final String expiryDate) {
        this.expiryDate = expiryDate;
    }
    
    public String getComment() {
        return comment;
    }
    
    public void setComment(final String comment) {
        this.comment = comment;
    }
    
    public String getInternalKey() {
        return internalKey;
    }
    
    public void setInternalKey(final String internalKey) {
        this.internalKey = internalKey;
    }
}
