package com.digitalpaytech.mvc.customeradmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.mvc.LocationDetailsController;
import com.digitalpaytech.mvc.customeradmin.support.LocationEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
@SessionAttributes({ WebSecurityConstants.WEB_SECURITY_FORM })
public class CustomerAdminLocationDetailsController extends LocationDetailsController {
    
    /**
     * This method will retrieve all customer's location information, format
     * that information into a hierarchical tree and puts it in the model as
     * "locations" and returns locations/index.vm. Will put a blank
     * locationEditForm into the model
     * 
     * @param request
     * @param response
     * @param model
     * @return Model containing current customer location information in tree
     *         form, and location edit form and returns target URL which is
     *         mapped to Settings->Locations (/settings/locations/index.html)
     */
    @RequestMapping(value = "/secure/settings/locations/locationDetails.html")
    public final String locationList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper role to view it. */
        // Moved here because LocationList is used in other pages as well 
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS)) {
                if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                    && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                    return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
                }
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
            }
        }
        
        return super.locationList(request, response, model, "/settings/locations/locationDetails");
    }
    
    /**
     * This method will return a JSON document holding information for the edit
     * page to the UI. This will be used for adding new locations, to retrieve
     * the drop down lists of pay stations and locations
     * 
     * @param request
     * @param response
     * @param model
     * @return Returns JSON of locations and pay stations
     */
    @RequestMapping(value = "/secure/settings/locations/form.html")
    @ResponseBody
    public final String getLocationEditForm(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)
            && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.getLocationEditFormSuper(request, response, model);
    }
    
    /**
     * This method takes a locationId as input through request parameter
     * "locationID" and gathers details associated to that location to send to
     * the UI for displaying location details, or inserting into the location
     * edit form for editing
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON string having location detail information. "false" in case
     *         of any exception or validation failure.
     */
    @RequestMapping(value = "/secure/settings/locations/info.html")
    @ResponseBody
    public final String getLocationDetails(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.getLocationDetailsSuper(request, response, model);
    }
    
    /**
     * This method takes a WebSecurityForm from the model and after validating
     * the contents of the location edit form, saves the location information to
     * the database. It may also save pay station information, child location
     * information, and open close times.
     * 
     * @param webSecurityForm
     * @param result
     * @param request
     * @param response
     * @param model
     * @return A response of true for a successful save, JSON document for
     *         validation errors, or null for database records not found
     */
    @RequestMapping(value = "/secure/settings/locations/saveLocation.html", method = RequestMethod.POST)
    @ResponseBody
    public final String saveLocation(@ModelAttribute(WebSecurityConstants.WEB_SECURITY_FORM) final WebSecurityForm<LocationEditForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.saveLocationSuper(webSecurityForm, result, request, response, model);
    }
    
    /**
     * This method verifies that a location can be deleted. It checks for
     * children and permits, and returns true or false.
     * 
     * @param request
     * @param response
     * @param model
     * @return "true" or "false" depending on successful validation
     */
    @RequestMapping(value = "/secure/settings/locations/verifyDeleteLocation.html")
    @ResponseBody
    public final String verifyDeleteLocation(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.verifyDeleteLocationSuper(request, response, model);
    }
    
    /**
     * This method takes a location Id and deletes it from the database if it
     * passes validation from verifyDeleteLocation and if the suer has valid
     * permissions
     * 
     * @param request
     * @param response
     * @param model
     * @return True or false depending on success of the deletion.
     */
    @RequestMapping(value = "/secure/settings/locations/deleteLocation.html")
    @ResponseBody
    public final String deleteLocation(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.deleteLocationSuper(request, response, model);
    }
    
    /**
     * This method will just clear out Session Data.
     * 
     * @param request
     * @param response
     * @param locationRandomId
     * @param model
     * @return True or false depending on success of the deletion.
     */
    @RequestMapping(value = "/secure/settings/locations/cancelForm.html", method = RequestMethod.GET)
    @ResponseBody
    public final String cancelLocationForm(final HttpServletRequest request, final HttpServletResponse response,
        @RequestParam("locationRandomId") final String locationRandomId, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.cancelLocationFormSuper(request, response, locationRandomId, model);
    }
    
    /**
     * This method will just clear out Session Data.
     * 
     * @param request
     * @param response
     * @param locationRandomId
     * @param startDate
     * @param endDate
     * @param model
     * @return True or false depending on success of the deletion.
     */
    @RequestMapping(value = "/secure/settings/locations/listRateRateProfiles.html", method = RequestMethod.GET)
    @ResponseBody
    protected final String listRateRateProfiles(final HttpServletRequest request, final HttpServletResponse response,
        @RequestParam("locationID") final String locationRandomId, @RequestParam("startDate") final String startDateStr,
        @RequestParam("endDate") final String endDateStr, final ModelMap model) {
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        // TODO Auto-generated method stub
        return super.listRateRateProfilesSuper(request, response, locationRandomId, startDateStr, endDateStr, model);
    }
    
    /**
     * This method takes a locationId as input through request parameter
     * "locationID" and gathers details associated to that location to send to
     * the UI for displaying location pay station geo-point, or inserting
     * into the location geo-point edit form for editing
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON string having location geo-point detail information. "false" in case
     *         of any exception or validation failure.
     */
    @RequestMapping(value = "/secure/settings/locations/geopoint.html")
    public @ResponseBody String getLocationGeoPoint(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.getLocationGeopoint(request, response, model);
    }
}
