package com.digitalpaytech.mvc.customeradmin.support.io.dto;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

public class CustomerCardCsvDTO implements Serializable {
	private static final long serialVersionUID = -3639436096466382695L;
	
	private String cardType;
	private String cardNumber;
	private String startDate;
	private String endDate;
	private String location;
	private String pointOfSale;
	private String comment;
	private String restricted;
	private String gracePeriod;
	private String maxNumOfUses;
	
	public CustomerCardCsvDTO() {
		
	}
	
	public CustomerCardCsvDTO(String[] tokens) {
		this.cardType = StringUtils.stripToNull(tokens[0]);
		this.cardNumber = StringUtils.stripToNull(tokens[1]);
		this.startDate = StringUtils.stripToNull(tokens[2]);
		this.endDate = StringUtils.stripToNull(tokens[3]);
		this.location = StringUtils.stripToNull(tokens[4]);
		this.pointOfSale = StringUtils.stripToNull(tokens[5]);
		this.comment = StringUtils.stripToNull(tokens[6]);
		this.restricted = StringUtils.stripToNull(tokens[7]);
		this.gracePeriod = StringUtils.stripToNull(tokens[8]);
		this.maxNumOfUses = StringUtils.stripToNull(tokens[9]);
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPointOfSale() {
		return pointOfSale;
	}

	public void setPointOfSale(String pointOfSale) {
		this.pointOfSale = pointOfSale;
	}

	public String getRestricted() {
		return restricted;
	}

	public void setRestricted(String restricted) {
		this.restricted = restricted;
	}

	public String getGracePeriod() {
		return gracePeriod;
	}

	public void setGracePeriod(String gracePeriod) {
		this.gracePeriod = gracePeriod;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getMaxNumOfUses() {
		return maxNumOfUses;
	}

	public void setMaxNumOfUses(String maxNumOfUses) {
		this.maxNumOfUses = maxNumOfUses;
	}
}
