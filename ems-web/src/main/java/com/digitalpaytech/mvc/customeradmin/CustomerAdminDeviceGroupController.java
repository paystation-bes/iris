package com.digitalpaytech.mvc.customeradmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.client.dto.DeviceGroupFilter;
import com.digitalpaytech.client.dto.fms.BulkSchedule;
import com.digitalpaytech.client.dto.fms.DeviceGroup;
import com.digitalpaytech.mvc.DeviceGroupController;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.DeviceGroupConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebPermissionUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
public class CustomerAdminDeviceGroupController extends DeviceGroupController {
    @RequestMapping(value = "/secure/settings/payStations/configGroup.html", method = RequestMethod.GET)
    public final String page(@ModelAttribute(DeviceGroupConstants.FORM_EDIT) final WebSecurityForm<DeviceGroup> editForm,
        @ModelAttribute(DeviceGroupConstants.FORM_SEARCH) final WebSecurityForm<DeviceGroupFilter> searchForm,
        @ModelAttribute(DeviceGroupConstants.FORM_BULK_SCHEDULE) final WebSecurityForm<BulkSchedule> bulkScheduleForm,
        final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        String uri = null;
        if (subscribed() && canView()) {
            uri = super.performPage(editForm, searchForm, bulkScheduleForm, request, response, model, "/settings/locations/configGroup");
        } else {
            uri = WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
        }
        
        return uri;
    }
    
    @RequestMapping(value = "/secure/settings/payStations/configGroupList.html", method = RequestMethod.POST)
    @ResponseBody
    public final String list(@ModelAttribute(DeviceGroupConstants.FORM_SEARCH) final WebSecurityForm<DeviceGroupFilter> searchForm,
        final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        String result = null;
        if (subscribed() && canView()) {
            result = super.performList(searchForm, bindingResult, request, response, model);
        } else {
            result = WebCoreConstants.RESPONSE_FALSE;
        }
        
        return result;
    }
    
    @RequestMapping(value = "/secure/settings/payStations/upgradeGroups.html", method = RequestMethod.GET)
    @ResponseBody
    public final String listUpgradeGroups(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        String result = null;
        if (subscribed() && canView()) {
            result = super.performlistUpgradeGroups(request, response);
        } else {
            result = WebCoreConstants.RESPONSE_FALSE;
        }
        
        return result;
    }
    
    @RequestMapping(value = "/secure/settings/payStations/configGroupDetails.html", method = RequestMethod.GET)
    @ResponseBody
    public final String details(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        String result = null;
        if (subscribed() && canView()) {
            result = super.performDetails(request, response, model);
        } else {
            result = WebCoreConstants.RESPONSE_FALSE;
        }
        
        return result;
    }
    
    @RequestMapping(value = "/secure/settings/payStations/configGroupDetailSave.html", method = RequestMethod.POST)
    @ResponseBody
    public final String saveConfigGroupDetail(final @ModelAttribute(DeviceGroupConstants.FORM_EDIT) WebSecurityForm<DeviceGroup> webSecurityForm,
        final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        /*
         * @ TODO We should not sending the schedule date to fms if User doesn't have permission.
         * This is ok without having to send the error message back to the UI because UI has already hide the schedule date.
         * If there are any modifications to schedule date, it means that the user is hacking Iris.
         * We don't have to show any kind of error message in that case.
         */
        String mgeResp = null;
        if (subscribed() && canManage()) {
            mgeResp = super.sendCardProcessingConfiguration(webSecurityForm, request, canSchedule());
            // sendCardProcessingConfiguration failed and returns the JSON response.
            if (!mgeResp.startsWith(WebCoreConstants.RESPONSE_TRUE)) {
                return mgeResp;
            }
        }
        
        String schResp = null;
        if (subscribed() && canSchedule()) {
            schResp = super.sendSchedule(webSecurityForm, request, canManage());
            // sendSchedule failed and returns the JSON response.
            if (!schResp.startsWith(WebCoreConstants.RESPONSE_TRUE)) {
                return schResp;
            }
        } else {
            // No schedule permission, return manage response.
            return mgeResp;
        }
        return schResp;
    }
    
    @RequestMapping(value = "/secure/settings/payStations/bulkSchedule.html", method = RequestMethod.POST)
    @ResponseBody
    public final String bulkSchedule(final @ModelAttribute(DeviceGroupConstants.FORM_BULK_SCHEDULE) WebSecurityForm<BulkSchedule> webSecurityForm,
        final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        String result = null;
        
        if (subscribed() && canSchedule()) {
            result = super.sendBulkSchedule(webSecurityForm, request, canManage());
        } else {
            result = WebCoreConstants.RESPONSE_FALSE;
        }
        
        return result;
    }
    
    @RequestMapping(value = "/secure/settings/payStations/configGroupSearch.html", method = RequestMethod.GET)
    @ResponseBody
    public final String autocomplete(final HttpServletRequest request, final HttpServletResponse response) {
        String result = null;
        if (subscribed() && canView()) {
            result = super.autocompleteGroups(request, response);
        } else {
            result = WebCoreConstants.RESPONSE_FALSE;
        }
        return result;
    }
    
    @RequestMapping(value = "/secure/settings/payStations/bossKeyGenerate.html", method = RequestMethod.PUT)
    @ResponseBody
    public final String generateBossKey(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        final String result;
        if (subscribed() && canView() && canSchedule()) {
            result = super.generate(request, response, model);
        } else {
            result = WebCoreConstants.RESPONSE_FALSE;
        }
        return result;
    }
    
    @RequestMapping(value = "/secure/settings/payStations/bossKeyProgress.html", method = RequestMethod.GET)
    @ResponseBody
    public final String progressBossKey(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        final String result;
        if (subscribed() && canView() && canSchedule()) {
            result = super.progress(request, response, model);
        } else {
            result = WebCoreConstants.RESPONSE_FALSE;
        }
        return result;
    }
    
    @RequestMapping(value = "/secure/settings/payStations/bossKeyDownload.html", method = RequestMethod.GET)
    public final void downloadBossKey(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (subscribed() && canView() && canSchedule()) {
            super.download(request, response, model);
        } else {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
        }
    }
    
    private boolean subscribed() {
        return WebSecurityUtil.hasValidCustomerSubscription(WebCoreConstants.SUBSCRIPTION_TYPE_ONLINE_PAYSTATION_CONFIGURATION);
    }
    
    private boolean canView() {
        return WebPermissionUtil.hasCustomerSettingsConfigGroupView();
    }
    
    private boolean canManage() {
        return WebPermissionUtil.hasCustomerSettingsConfigGroupManage();
    }
    
    private boolean canSchedule() {
        return WebPermissionUtil.hasCustomerSettingsConfigGroupSchedule();
    }
}
