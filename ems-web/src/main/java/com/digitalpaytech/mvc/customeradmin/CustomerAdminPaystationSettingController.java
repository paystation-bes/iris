package com.digitalpaytech.mvc.customeradmin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.SettingsFile;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.customeradmin.CustomerAdminPayStationSettingInfo;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.PaystationSettingService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.ReportingUtil;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebPermissionUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
public class CustomerAdminPaystationSettingController {
    
    protected static Logger log = Logger.getLogger(CustomerAdminPaystationSettingController.class);
    
    @Autowired
    protected PointOfSaleService pointOfSaleService;
    @Autowired
    protected PosServiceStateService posServiceStateService;
    @Autowired
    protected PaystationSettingService paystationSettingService;
    @Autowired
    protected ReportingUtil reportingUtil;
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final void setPosServiceStateService(final PosServiceStateService posServiceStateService) {
        this.posServiceStateService = posServiceStateService;
    }
    
    public final void setPaystationSettingService(final PaystationSettingService paystationSettingService) {
        this.paystationSettingService = paystationSettingService;
    }
    
    public final void setReportingUtil(final ReportingUtil reportingUtil) {
        this.reportingUtil = reportingUtil;
    }
    
    /**
     * This method returns all SettingFiles for the customer
     * 
     * @param request
     * @param response
     * @param model
     * @return Model attributes: moduleFilterList - List of name/randomId pairs
     *         of modules used to filed results. locationRouteInfo - A
     *         LocationRouteFilterInfo object containing list of locations and
     *         routes used to filter alerts. activeAlerts - An
     *         AlertCentreAlertInfo List of information for current active
     *         alerts. Returns "/alerts/index.vm"
     */
    @RequestMapping(value = "/secure/settings/locations/payStationSettings.html")
    public String getPaystationSettingList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATION_SETTINGS)
            && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATION_SETTINGS)
            && !WebPermissionUtil.hasCustomerSettingsConfigGroupView()) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Customer customer = userAccount.getCustomer();
        final String timezone = WebSecurityUtil.getCustomerTimeZone();
        
        final List<CustomerAdminPayStationSettingInfo> paystationSettingList =
                this.paystationSettingService.findCustomerAdminPayStationSettingInfoListByCustomerId(customer.getId(), timezone, keyMapping);
        
        final PlacementMapBordersInfo mapBorders = this.pointOfSaleService.findMapBordersByCustomerId(customer.getId());
        model.put("paystationSettingList", paystationSettingList);
        model.put("mapBorders", mapBorders);
        
        return "/settings/locations/payStationSettings";
    }
    
    /**
     * This method takes request parameter "settingsFileId" and returns pay
     * station setting information with a list of paystations that use the selected setting
     * 
     * @param request
     * @param response
     * @param model
     * @return Returns JSON formatted CustomerAdminPayStationSettingInfo object.
     *         Returns false if Id is not valid or requested pay station has no
     *         alerts
     */
    @RequestMapping(value = "/secure/settings/locations/payStationSettingsDetail.html", method = RequestMethod.GET)
    public @ResponseBody String getPayStationSettingsDetail(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATION_SETTINGS)
            && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATION_SETTINGS)
            && !WebPermissionUtil.hasCustomerSettingsConfigGroupView()) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Customer customer = userAccount.getCustomer();
        final String timezone = WebSecurityUtil.getCustomerTimeZone();
        
        final String strSettingsFileId = request.getParameter("settingsFileId");
        
        CustomerAdminPayStationSettingInfo paystationSetting = null;
        //        List<CustomerAdminPayStationSettingPayStationInfo> paystationList = new ArrayList<CustomerAdminPayStationSettingPayStationInfo>();
        
        if (WebCoreConstants.NOT_MAINTAINED_PAYSTATION_SETTING.equals(strSettingsFileId)) {
            paystationSetting =
                    this.paystationSettingService.findNoSettingCustomerAdminPayStationSettingInfoByCustomerId(customer.getId(), timezone, keyMapping);
            
        } else {
            
            final Integer settingsFileId =
                    verifyRandomIdAndReturnActual(response, keyMapping, strSettingsFileId,
                                                  "+++ Invalid str_settingsFileId in CustomerAdminPaystationSettingController.getPayStationSettingsDetail() method. +++");
            if (settingsFileId == null) {
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            final SettingsFile settingsFile = this.paystationSettingService.findSettingsFile(settingsFileId);
            if (settingsFile == null) {
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            paystationSetting =
                    this.paystationSettingService.findCustomerAdminPayStationSettingInfoBySettingFileId(settingsFileId, timezone, keyMapping);
            
        }
        
        return WidgetMetricsHelper.convertToJson(paystationSetting, "paystationSetting", true);
    }
    
    /**
     * This method takes request parameter "settingsFileId" and deletes SettingsFileContent from DB
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return "true" string value if process succeeds, "false" string value if
     *         process fails.
     */
    @RequestMapping(value = "/secure/settings/locations/deletePayStationSettings.html", method = RequestMethod.GET)
    public @ResponseBody String deleteSettingsFileContent(final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATION_SETTINGS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        if (!((WebUser) WebSecurityUtil.getAuthentication().getPrincipal()).isSystemAdmin() && !WebSecurityUtil.isCustomerMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + ":" + this.reportingUtil.getPropertyEN("error.customer.notMigrated");
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strSettingsFileId = request.getParameter("settingsFileId");
        
        if (WebCoreConstants.NOT_MAINTAINED_PAYSTATION_SETTING.equals(strSettingsFileId)) {
            return WebCoreConstants.RESPONSE_FALSE;
        } else {
            
            final Integer settingsFileId =
                    verifyRandomIdAndReturnActual(response, keyMapping, strSettingsFileId,
                                                  "+++ Invalid str_settingsFileId in CustomerAdminPaystationSettingController.getPayStationSettingsDetail() method. +++");
            if (settingsFileId == null) {
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            final SettingsFile settingsFile = this.paystationSettingService.findSettingsFile(settingsFileId);
            if (settingsFile == null) {
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            this.paystationSettingService.deleteSettingsFileContent(settingsFile);
            final List<PosServiceState> posServiceStateList = this.posServiceStateService.findPosServiceStateByNextSettingsFileId(settingsFileId);
            if (posServiceStateList != null) {
                for (PosServiceState posServiceState : posServiceStateList) {
                    posServiceState.setIsNewPaystationSetting(false);
                    posServiceState.setSettingsFile(null);
                    this.posServiceStateService.updatePosServiceState(posServiceState);
                }
            }
        }
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    protected final Integer verifyRandomIdAndReturnActual(final HttpServletResponse response, final RandomKeyMapping keyMapping,
        final String str_randomLocationId, final String message) {
        
        final String[] msgs = new String[] { message, message };
        return WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, str_randomLocationId, msgs);
    }
    
}