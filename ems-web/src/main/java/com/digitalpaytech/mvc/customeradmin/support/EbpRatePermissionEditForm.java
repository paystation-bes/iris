package com.digitalpaytech.mvc.customeradmin.support;

import java.util.List;
import java.util.Iterator;

import javax.persistence.Transient;

import com.digitalpaytech.domain.DayOfWeek;
import com.digitalpaytech.domain.ExtensibleRate;
import com.digitalpaytech.domain.ExtensibleRateDayOfWeek;
import com.digitalpaytech.domain.ParkingPermission;
import com.digitalpaytech.domain.ParkingPermissionDayOfWeek;
import com.digitalpaytech.util.WebCoreConstants;

public class EbpRatePermissionEditForm implements java.io.Serializable {   
       
    private static final long serialVersionUID = -1942121330784334367L;
    // ExtensibleRate
	private String extensibleRateFlag;
	private String extensibleRateTypeId;
	private String extensibleRateId;
	
    @Transient
    private transient int extensibleRateRealId;
	
	private List<ExtensibleRate> extensibleRates;
	private String rateValue;
	private String rateServiceFee;
	private String minExtensionMinutes;	
	
	// ParkingPermission
	private String parkingPermissionFlag;
	private String parkingPermissionTypeId;
	private String parkingPermissionId;
	private List<ParkingPermission> parkingPermissions;
	private String permissionStatus;
	private String maxDurationMinutes;
	
    @Transient
    private transient int parkingPermissionRealId; 	
	
	private String name;
	private String activeFlag = Boolean.TRUE.toString();
	private String locationId;
	private String startTime;
	private String endTime;
	private String sunday;
	private String monday;
	private String tuesday;
	private String wednesday;
	private String thursday;
	private String friday;
	private String saturday;
	private String randomId;
	private String creationDateStr;
	private String warningPeriod;
	// For validating overlapped hours & minutes.
	private List<DayOfWeek> daysOfWeek;
	
	
	public EbpRatePermissionEditForm() {
	}
	
	
	/**
	 * Constructor for editing existing Policy Details.
	 * @param parkingPermission ParkingPermission domain object that maps to a ParkingPermission table record.
	 */
	public EbpRatePermissionEditForm(ParkingPermission parkingPermission) {
		// Policy Type, Max. Duration, Policy Name
		parkingPermissionId = parkingPermission.getRandomId();
		parkingPermissionFlag = Boolean.TRUE.toString();
		parkingPermissionTypeId = String.valueOf(parkingPermission.getParkingPermissionType().getId());
		permissionStatus = Boolean.valueOf(parkingPermission.isIsLimitedOrUnlimited()).toString();
		maxDurationMinutes = String.valueOf(parkingPermission.getMaxDurationMinutes());
		name = parkingPermission.getName();
		locationId = parkingPermission.getLocation().getRandomId();

		// Start Time (hh:mm) 
		StringBuilder bdr = new StringBuilder();
		bdr.append(parkingPermission.getBeginHourLocal()).append(WebCoreConstants.COLON).append(parkingPermission.getBeginMinuteLocal());
		startTime = bdr.toString();
		// End Time (hh:mm)
		bdr.replace(0, bdr.length(), "");
		bdr.append(parkingPermission.getEndHourLocal()).append(WebCoreConstants.COLON).append(parkingPermission.getEndMinuteLocal());
		endTime = bdr.toString();
		
		// Days of Week
		ParkingPermissionDayOfWeek parkDow = null;
		Iterator<ParkingPermissionDayOfWeek> iter = parkingPermission.getParkingPermissionDayOfWeeks().iterator();
		while (iter.hasNext()) {
			parkDow = iter.next();
			populateDaysOfWeek(parkDow.getDayOfWeek());
		}
	}

	/**
	 * Constructor for editing existing Rate Details.
	 * @param extensibleRate ExtensibleRate domain object that maps to a ExtensibleRate table record.
	 */
	public EbpRatePermissionEditForm(ExtensibleRate extensibleRate) {
		// Hourly Rate, Service Fee, Min. Extension, Rate name
		extensibleRateId = extensibleRate.getRandomId();
		extensibleRateFlag = Boolean.TRUE.toString();
		extensibleRateTypeId = String.valueOf(extensibleRate.getExtensibleRateType().getId());
		rateValue = String.valueOf(extensibleRate.getRateAmount());
		rateServiceFee = String.valueOf(extensibleRate.getServiceFeeAmount());
		minExtensionMinutes = String.valueOf(extensibleRate.getMinExtensionMinutes());
		name = extensibleRate.getName();
		locationId = extensibleRate.getLocation().getRandomId();
		
		// Start Time (hh:mm) 
		StringBuilder bdr = new StringBuilder();
		bdr.append(extensibleRate.getBeginHourLocal()).append(WebCoreConstants.COLON).append(extensibleRate.getBeginMinuteLocal());
		startTime = bdr.toString();
		// End Time (hh:mm)
		bdr.replace(0, bdr.length(), "");
		bdr.append(extensibleRate.getEndHourLocal()).append(WebCoreConstants.COLON).append(extensibleRate.getEndMinuteLocal());
		endTime = bdr.toString();
		
		// Days of Week
		ExtensibleRateDayOfWeek extRateDow = null;
		Iterator<ExtensibleRateDayOfWeek> iter = extensibleRate.getExtensibleRateDayOfWeeks().iterator();
		while (iter.hasNext()) {
			extRateDow = iter.next();
			populateDaysOfWeek(extRateDow.getDayOfWeek());
		}
	}
	
	private void populateDaysOfWeek(DayOfWeek dayofweek) {
		if (dayofweek.getDayOfWeek() == WebCoreConstants.SUNDAY) {
			sunday = Boolean.TRUE.toString();
		} else if (dayofweek.getDayOfWeek() == WebCoreConstants.MONDAY) {
			monday = Boolean.TRUE.toString();
		} else if (dayofweek.getDayOfWeek() == WebCoreConstants.TUESDAY) {
			tuesday = Boolean.TRUE.toString();
		} else if (dayofweek.getDayOfWeek() == WebCoreConstants.WEDNESDAY) {
			wednesday = Boolean.TRUE.toString();
		} else if (dayofweek.getDayOfWeek() == WebCoreConstants.THURSDAY) {
			thursday = Boolean.TRUE.toString();
		} else if (dayofweek.getDayOfWeek() == WebCoreConstants.FRIDAY) {
			friday = Boolean.TRUE.toString();
		} else if (dayofweek.getDayOfWeek() == WebCoreConstants.SATURDAY) {
			saturday = Boolean.TRUE.toString();
		}
	}
	

	public List<ParkingPermission> getParkingPermissions() {
    	return parkingPermissions;
    }
	public void setParkingPermissions(List<ParkingPermission> parkingPermissions) {
    	this.parkingPermissions = parkingPermissions;
    }
	public String getLocationId() {
    	return locationId;
    }
	public void setLocationId(String locationId) {
    	this.locationId = locationId;
    }
	public String getStartTime() {
    	return startTime;
    }
	public void setStartTime(String startTime) {
    	this.startTime = startTime;
    }
	public String getEndTime() {
    	return endTime;
    }
	public void setEndTime(String endTime) {
    	this.endTime = endTime;
    }
	public String getSunday() {
    	return sunday;
    }
	public void setSunday(String sunday) {
    	this.sunday = sunday;
    }
	public String getMonday() {
    	return monday;
    }
	public void setMonday(String monday) {
    	this.monday = monday;
    }
	public String getTuesday() {
    	return tuesday;
    }
	public void setTuesday(String tuesday) {
    	this.tuesday = tuesday;
    }
	public String getWednesday() {
    	return wednesday;
    }
	public void setWednesday(String wednesday) {
    	this.wednesday = wednesday;
    }
	public String getThursday() {
    	return thursday;
    }
	public void setThursday(String thursday) {
    	this.thursday = thursday;
    }
	public String getFriday() {
    	return friday;
    }
	public void setFriday(String friday) {
    	this.friday = friday;
    }
	public String getSaturday() {
    	return saturday;
    }
	public void setSaturday(String saturday) {
    	this.saturday = saturday;
    }
	public String getRateValue() {
    	return rateValue;
    }
	public void setRateValue(String rateValue) {
    	this.rateValue = rateValue;
    }
	public String getRateServiceFee() {
    	return rateServiceFee;
    }
	public void setRateServiceFee(String rateServiceFee) {
    	this.rateServiceFee = rateServiceFee;
    }
	public String getCreationDateStr() {
    	return creationDateStr;
    }
	public void setCreationDateStr(String creationDateStr) {
    	this.creationDateStr = creationDateStr;
    }
	public String getExtensibleRateFlag() {
    	return extensibleRateFlag;
    }
	public void setExtensibleRateFlag(String extensibleRateFlag) {
    	this.extensibleRateFlag = extensibleRateFlag;
    }
	public String getParkingPermissionFlag() {
    	return parkingPermissionFlag;
    }
	public void setParkingPermissionFlag(String parkingPermissionFlag) {
    	this.parkingPermissionFlag = parkingPermissionFlag;
    }
	public String getExtensibleRateTypeId() {
    	return extensibleRateTypeId;
    }
	public void setExtensibleRateTypeId(String extensibleRateTypeId) {
    	this.extensibleRateTypeId = extensibleRateTypeId;
    }
	public String getExtensibleRateId() {
    	return extensibleRateId;
    }
	public void setExtensibleRateId(String extensibleRateId) {
    	this.extensibleRateId = extensibleRateId;
    }
	public String getParkingPermissionTypeId() {
    	return parkingPermissionTypeId;
    }
	public void setParkingPermissionTypeId(String parkingPermissionTypeId) {
    	this.parkingPermissionTypeId = parkingPermissionTypeId;
    }
	public String getParkingPermissionId() {
    	return parkingPermissionId;
    }
	public void setParkingPermissionId(String parkingPermissionId) {
    	this.parkingPermissionId = parkingPermissionId;
    }
	public String getActive() {
    	return activeFlag;
    }
	public void setActive(String activeFlag) {
    	this.activeFlag = activeFlag;
    }
	public String getMinExtensionMinutes() {
    	return minExtensionMinutes;
    }
	public void setMinExtensionMinutes(String minExtensionMinutes) {
    	this.minExtensionMinutes = minExtensionMinutes;
    }
	public List<ExtensibleRate> getExtensibleRates() {
    	return extensibleRates;
    }
	public void setExtensibleRates(List<ExtensibleRate> extensibleRates) {
    	this.extensibleRates = extensibleRates;
    }
	public String getName() {
    	return name;
    }
	public void setName(String name) {
    	this.name = name;
    }
	public String getPermissionStatus() {
    	return permissionStatus;
    }
	public void setPermissionStatus(String permissionStatus) {
    	this.permissionStatus = permissionStatus;
    }
	public String getMaxDurationMinutes() {
    	return maxDurationMinutes;
    }
	public void setMaxDurationMinutes(String maxDurationMinutes) {
    	this.maxDurationMinutes = maxDurationMinutes;
    }
	public String getRandomId() {
    	return randomId;
    }
	public void setRandomId(String randomId) {
    	this.randomId = randomId;
    }
	public String getWarningPeriod() {
    	return warningPeriod;
    }
	public void setWarningPeriod(String warningPeriod) {
    	this.warningPeriod = warningPeriod;
    }
    public List<DayOfWeek> getDaysOfWeeks() {
        return daysOfWeek;
    }
    public void setDaysOfWeeks(List<DayOfWeek> daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }


    public int getExtensibleRateRealId() {
        return extensibleRateRealId;
    }


    public void setExtensibleRateRealId(int extensibleRateRealId) {
        this.extensibleRateRealId = extensibleRateRealId;
    }


    public int getParkingPermissionRealId() {
        return parkingPermissionRealId;
    }


    public void setParkingPermissionRealId(int parkingPermissionRealId) {
        this.parkingPermissionRealId = parkingPermissionRealId;
    }
}
