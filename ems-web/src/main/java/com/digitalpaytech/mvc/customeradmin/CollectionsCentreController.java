package com.digitalpaytech.mvc.customeradmin;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.CustomerMobileDevice;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.MobileLicense;
import com.digitalpaytech.domain.MobileSessionToken;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosBalance;
import com.digitalpaytech.domain.PosCollection;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportLocationValue;
import com.digitalpaytech.domain.ReportType;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserAccountRoute;
import com.digitalpaytech.dto.ActiveAlertSearchCriteria;
import com.digitalpaytech.dto.AdhocReportInfo;
import com.digitalpaytech.dto.AlertMapEntry;
import com.digitalpaytech.dto.CollectionSearchCriteria;
import com.digitalpaytech.dto.MobileTokenMapEntry;
import com.digitalpaytech.dto.PaginatedList;
import com.digitalpaytech.dto.SummarySearchCriteria;
import com.digitalpaytech.dto.customeradmin.CollectionSummaryEntry;
import com.digitalpaytech.dto.customeradmin.CollectionsCentreCollectionInfo;
import com.digitalpaytech.dto.customeradmin.CollectionsCentrePayStationInfo;
import com.digitalpaytech.dto.customeradmin.CollectionsCentrePosInfo;
import com.digitalpaytech.dto.customeradmin.CollectionsCentreSummaryInfo;
import com.digitalpaytech.dto.customeradmin.LocationRouteFilterInfo;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.dto.customeradmin.SeverityValuePair;
import com.digitalpaytech.mvc.customeradmin.support.CollectionCentreFilterForm;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.AlertsService;
import com.digitalpaytech.service.CollectionSummaryConfigService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerAlertTypeService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.MobileLicenseService;
import com.digitalpaytech.service.MobileSessionTokenService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.PosEventCurrentService;
import com.digitalpaytech.service.ReportCollectionFilterTypeService;
import com.digitalpaytech.service.ReportDefinitionService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.paystation.PosCollectionService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.RelativeDateTime;
import com.digitalpaytech.util.support.WebObjectId;
import com.digitalpaytech.validation.customeradmin.CollectionCentreFilterValidator;

@Controller
public class CollectionsCentreController {
    public static final String FORM_COLLECTIONS = "collectionsForm";
    
    private static Logger log = Logger.getLogger(CollectionsCentreController.class);
    
    @Autowired
    protected MobileLicenseService mobileLicenseService;
    @Autowired
    protected MobileSessionTokenService mobileSessionTokenService;
    @Autowired
    protected UserAccountService userAccountService;
    @Autowired
    protected LocationService locationService;
    @Autowired
    protected RouteService routeService;
    @Autowired
    protected PointOfSaleService pointOfSaleService;
    @Autowired
    protected AlertsService alertsService;
    @Autowired
    protected CustomerAdminService customerAdminService;
    @Autowired
    protected CustomerAlertTypeService customerAlertTypeService;
    @Autowired
    protected PosCollectionService posCollectionService;
    @Autowired
    protected CollectionCentreFilterValidator collectionCentreFilterValidator;
    @Autowired
    protected CommonControllerHelper commonControllerHelper;
    @Autowired
    protected ReportDefinitionService reportDefinitionService;
    @Autowired
    private ReportCollectionFilterTypeService reportCollectionFilterTypeService;
    @Autowired
    private CollectionSummaryConfigService collectionSummaryConfigService;
    @Autowired
    private PosEventCurrentService posEventCurrentService;
    
    public final void setLocationService(final LocationService locationService) {
        this.locationService = locationService;
    }
    
    public final void setRouteService(final RouteService routeService) {
        this.routeService = routeService;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final void setAlertsService(final AlertsService alertsService) {
        this.alertsService = alertsService;
    }
    
    public final void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    public final void setCustomerAlertTypeService(final CustomerAlertTypeService customerAlertTypeService) {
        this.customerAlertTypeService = customerAlertTypeService;
    }
    
    public final void setPosCollectionService(final PosCollectionService posCollectionService) {
        this.posCollectionService = posCollectionService;
    }
    
    public final void setReportCollectionFilterTypeService(final ReportCollectionFilterTypeService reportCollectionFilterTypeService) {
        this.reportCollectionFilterTypeService = reportCollectionFilterTypeService;
    }
    
    public final void setCollectionCentreFilterValidator(final CollectionCentreFilterValidator collectionCentreFilterValidator) {
        this.collectionCentreFilterValidator = collectionCentreFilterValidator;
    }
    
    public final void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }
    
    public final void setCollectionSummaryConfigService(final CollectionSummaryConfigService collectionSummaryConfigService) {
        this.collectionSummaryConfigService = collectionSummaryConfigService;
    }
    
    @RequestMapping(value = "/secure/collections/index.html")
    public final String collectionsCentreIndex(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_COLLECTIONS) final WebSecurityForm<CollectionCentreFilterForm> webSecurityForm) {
        
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_COLLECTION_STATUS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Customer customer = userAccount.getCustomer();
        
        final LocationRouteFilterInfo locationRouteInfo = this.commonControllerHelper
                .createLocationRouteFilter(customer.getId(), keyMapping, WebCoreConstants.ROUTE_TYPE_COLLECTIONS);
        
        final PlacementMapBordersInfo mapBorders = this.pointOfSaleService.findMapBordersByCustomerId(customer.getId());
        model.put("mapBorders", mapBorders);
        
        model.put("locationFilter", locationRouteInfo.getLocations());
        model.put("routeFilter", locationRouteInfo.getRoutes());
        
        model.put(FORM_COLLECTIONS, webSecurityForm);
        
        return "/collections/index";
    }
    
    @RequestMapping(value = "/secure/collections/viewCollectionsMade.html", method = RequestMethod.POST)
    @ResponseBody
    public final String viewCollectionsMade(@ModelAttribute(FORM_COLLECTIONS) final WebSecurityForm<CollectionCentreFilterForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_COLLECTION_STATUS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        /* validate user input. */
        this.collectionCentreFilterValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final CollectionCentreFilterForm filter = (CollectionCentreFilterForm) webSecurityForm.getWrappedObject();
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final TimeZone customerTimeZone = TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone());
        
        final CollectionSearchCriteria criteria = new CollectionSearchCriteria();
        criteria.setCustomerId(userAccount.getCustomer().getId());
        if (filter.getId() != null) {
            if (filter.isLocationOrRoute()) {
                criteria.setRouteId(filter.getId());
            } else {
                criteria.setLocationId(filter.getId());
            }
        }
        criteria.setCollectionTypeIds(new ArrayList<Integer>());
        if (filter.isBills()) {
            criteria.getCollectionTypeIds().add(WebCoreConstants.COLLECTION_TYPE_BILL);
        }
        if (filter.isCoin()) {
            criteria.getCollectionTypeIds().add(WebCoreConstants.COLLECTION_TYPE_COIN);
        }
        if (filter.isCredit()) {
            criteria.getCollectionTypeIds().add(WebCoreConstants.COLLECTION_TYPE_CARD);
        }
        if (filter.isTotal()) {
            criteria.getCollectionTypeIds().add(WebCoreConstants.COLLECTION_TYPE_ALL);
        }
        
        if (filter.getDataKey() == null) {
            filter.setDataKey(System.currentTimeMillis());
        }
        
        final Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(filter.getDataKey());
        criteria.setMaxCollectionDate(cal.getTime());
        
        cal.add(Calendar.DAY_OF_YEAR, -90);
        criteria.setMinCollectionDate(cal.getTime());
        
        if (filter.getTargetId() == null) {
            criteria.setPage((filter.getPageNumber() == null) ? 1 : filter.getPageNumber());
        } else {
            criteria.setTargetId(keyMapping.getKey(filter.getTargetId(), PosCollection.class, Integer.class));
            if (criteria.getTargetId() == null) {
                criteria.setPage(-1);
            } else {
                criteria.setPage(this.posCollectionService.locatePosCollectionPageByCriteria(criteria));
            }
            
            if (criteria.getPage() < 0) {
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        final PaginatedList<CollectionsCentreCollectionInfo> collectionsList = new PaginatedList<CollectionsCentreCollectionInfo>(
                this.posCollectionService.findPosCollectionByCriteria(criteria, customerTimeZone), Long.toString(criteria.getMaxCollectionDate()
                        .getTime()), webSecurityForm.getInitToken(), criteria.getPage());
        
        return WidgetMetricsHelper.convertToJson(collectionsList, "collectionsList", true);
    }
    
    @RequestMapping(value = "/secure/collections/viewCollectionUserMap.html", method = RequestMethod.POST)
    @ResponseBody
    public final String viewCollectionUserMap(final HttpServletRequest request, final HttpServletResponse response) {
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_COLLECTION_STATUS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final int customerId = userAccount.getCustomer().getId();
        final Map<Integer, String> statusNames = new HashMap<Integer, String>();
        statusNames.put(WebCoreConstants.USER_ACCOUNT_MOBILE_STATUS_ACTIVE,
                        this.commonControllerHelper.getMessage("label.collections.users.status.active"));
        statusNames.put(WebCoreConstants.USER_ACCOUNT_MOBILE_STATUS_LOGGED_IN,
                        this.commonControllerHelper.getMessage("label.collections.users.status.loggedIn"));
        statusNames.put(WebCoreConstants.USER_ACCOUNT_MOBILE_STATUS_LOGGED_OUT,
                        this.commonControllerHelper.getMessage("label.collections.users.status.loggedOut"));
        final List<MobileTokenMapEntry> entries = this.mobileSessionTokenService.findMobileSessionTokensMap(customerId,
                                                                                                            WebCoreConstants.MOBILE_APP_TYPE_COLLECT,
                                                                                                            new Date(),
                                                                                                            WebSecurityUtil.getCustomerTimeZone(),
                                                                                                            statusNames);
        
        return WidgetMetricsHelper.convertToJson(entries, "UserList", true);
    }
    
    @RequestMapping(value = "/secure/collections/viewCollectionUserList.html", method = RequestMethod.POST)
    @ResponseBody
    public final String viewCollectionUserList(final HttpServletRequest request, final HttpServletResponse response) {
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_COLLECTION_STATUS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final Customer customer = userAccount.getCustomer();
        
        final String pageNoStr = request.getParameter("wrappedObject.pageNumber");
        int pageNo = 1;
        if ((pageNoStr != null) && (pageNoStr.length() > 0)) {
            try {
                pageNo = Integer.parseInt(pageNoStr);
            } catch (NumberFormatException nfe) {
                // DO NOTHING.
            }
        }
        
        final PaginatedList<MobileTokenMapEntry> entries = new PaginatedList<MobileTokenMapEntry>();
        final String dataKeyStr = request.getParameter("wrappedObject.dataKey");
        long dataKey = -1;
        if ((dataKeyStr != null) && (dataKeyStr.length() > 0)) {
            try {
                dataKey = Long.parseLong(dataKeyStr);
            } catch (NumberFormatException nfe) {
                // DO NOTHING.
            }
        }
        
        if (dataKey <= 0) {
            dataKey = System.currentTimeMillis();
        }
        
        if (pageNo != 1) {
            entries.setElements(new ArrayList<MobileTokenMapEntry>());
        } else {
            final List<MobileSessionToken> mobileSessinTokens = this.mobileSessionTokenService
                    .findProvisionedMobileSessionTokenByCustomerAndApplicationByPage(customer.getId(), WebCoreConstants.MOBILE_APP_TYPE_COLLECT,
                                                                                     pageNo, new Date(dataKey));
            
            final Map<Integer, String> statusNames = new HashMap<Integer, String>();
            statusNames.put(WebCoreConstants.USER_ACCOUNT_MOBILE_STATUS_ACTIVE,
                            this.commonControllerHelper.getMessage("label.collections.users.status.active"));
            statusNames.put(WebCoreConstants.USER_ACCOUNT_MOBILE_STATUS_LOGGED_OUT,
                            this.commonControllerHelper.getMessage("label.collections.users.status.loggedOut"));
            statusNames.put(WebCoreConstants.USER_ACCOUNT_MOBILE_STATUS_LOGGED_IN,
                            this.commonControllerHelper.getMessage("label.collections.users.status.loggedIn"));
            
            entries.setElements(this.mobileSessionTokenService.findRecentUsersForTokens(mobileSessinTokens, WebSecurityUtil.getCustomerTimeZone(),
                                                                                        statusNames));
        }
        
        entries.setDataKey(Long.toString(dataKey));
        entries.setPage(pageNo);
        
        return WidgetMetricsHelper.convertToJson(entries, "userAccountList", true);
    }
    
    @RequestMapping(value = "/secure/collections/viewCollectionUserDetail.html", method = RequestMethod.GET)
    @ResponseBody
    public final String viewCollectionUserDetail(final HttpServletRequest request, final HttpServletResponse response) {
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_COLLECTION_STATUS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        final String tokenRandomId = request.getParameter("randomId");
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final int tokenId = this
                .verifyRandomIdAndReturnActual(response, keyMapping, tokenRandomId,
                                               "+++ Invalid randomised license in CollectionCentreController.viewCollectionUserDetail() method. +++");
        
        final MobileSessionToken token = this.mobileSessionTokenService.findMobileSessionTokenByIdJoin(tokenId);
        final UserAccount user = token.getUserAccount();
        final MobileLicense license = token.getMobileLicense();
        final CustomerMobileDevice device = license.getCustomerMobileDevice();
        final MobileTokenMapEntry entry = new MobileTokenMapEntry();
        if (token.getExpiryDate().before(new Date())) {
            entry.setStatus(this.commonControllerHelper.getMessage("label.collections.users.status.loggedOut"));
            entry.setStatusId(WebCoreConstants.USER_ACCOUNT_MOBILE_STATUS_LOGGED_OUT);
        } else {
            entry.setStatus(this.commonControllerHelper.getMessage("label.collections.users.status.loggedIn"));
            entry.setStatusId(WebCoreConstants.USER_ACCOUNT_MOBILE_STATUS_LOGGED_IN);
            for (UserAccountRoute uar : user.getUserAccountRoutes()) {
                if (uar.getMobileApplicationType().getId().intValue() == WebCoreConstants.MOBILE_APP_TYPE_COLLECT) {
                    entry.setAssignedRouteName(uar.getRoute().getName());
                    entry.setStatus(this.commonControllerHelper.getMessage("label.collections.users.status.active"));
                    entry.setStatusId(WebCoreConstants.USER_ACCOUNT_MOBILE_STATUS_ACTIVE);
                    break;
                }
            }
        }
        
        try {
            entry.setUserName(URLDecoder.decode(user.getUserName(), WebSecurityConstants.URL_ENCODING_LATIN1));
        } catch (UnsupportedEncodingException e) {
            entry.setUserName(user.getUserName());
        }
        entry.setFirstName(user.getFirstName());
        entry.setLastName(user.getLastName());
        entry.setUserRandomId(new WebObjectId(UserAccount.class, user.getId()).toString());
        entry.setLastCommunicated(new RelativeDateTime(token.getLastModifiedGmt(), WebSecurityUtil.getCustomerTimeZone()));
        entry.setLoggedInSince(new RelativeDateTime(token.getCreationDate(), WebSecurityUtil.getCustomerTimeZone()));
        entry.setDeviceName(device.getName());
        entry.setDeviceUid(device.getMobileDevice().getUid());
        return WidgetMetricsHelper.convertToJson(entry, "user", true);
    }
    
    @RequestMapping(value = "/secure/collections/viewCollectionsMadeMap.html", method = RequestMethod.POST)
    @ResponseBody
    public final String viewCollectionsMadeMap(@ModelAttribute(FORM_COLLECTIONS) final WebSecurityForm<CollectionCentreFilterForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response) {
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_COLLECTION_STATUS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        /* validate user input. */
        this.collectionCentreFilterValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        final CollectionCentreFilterForm filter = (CollectionCentreFilterForm) webSecurityForm.getWrappedObject();
        if (filter.getDataKey() == null) {
            filter.setDataKey(System.currentTimeMillis());
        }
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final Customer customer = userAccount.getCustomer();
        final List<Integer> collectionTypesList = new LinkedList<Integer>();
        if (filter.isBills()) {
            collectionTypesList.add(WebCoreConstants.COLLECTION_TYPE_BILL);
        }
        if (filter.isCoin()) {
            collectionTypesList.add(WebCoreConstants.COLLECTION_TYPE_COIN);
        }
        if (filter.isCredit()) {
            collectionTypesList.add(WebCoreConstants.COLLECTION_TYPE_CARD);
        }
        if (filter.isTotal()) {
            collectionTypesList.add(WebCoreConstants.COLLECTION_TYPE_ALL);
        }
        final Object[] collectionTypes = new Object[collectionTypesList.size()];
        int i = 0;
        for (Integer type : collectionTypesList) {
            collectionTypes[i] = type;
            i++;
        }
        final CustomerProperty timezoneProperty = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customer.getId(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        String timezoneString;
        if (timezoneProperty == null) {
            timezoneString = WebCoreConstants.SERVER_TIMEZONE_GMT;
        } else {
            timezoneString = timezoneProperty.getPropertyValue();
        }
        final Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(filter.getDataKey());
        final Date maxDate = cal.getTime();
        final TimeZone timezone = TimeZone.getTimeZone(timezoneString);
        final Calendar calendar = Calendar.getInstance(timezone);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        final Date minDate = calendar.getTime();
        final ActiveAlertSearchCriteria criteria = this.createSearchCriteria(new ActiveAlertSearchCriteria(), filter, userAccount);
        final List<AlertMapEntry> collectionsList = this.alertsService.findActivePointOfSalesByRecentCollections(criteria, collectionTypes, minDate,
                                                                                                                 maxDate);
        
        return WidgetMetricsHelper.convertToJson(collectionsList, "collectionsList", true);
    }
    
    @RequestMapping(value = "/secure/collections/viewCollectionAlertsMap.html", method = RequestMethod.POST)
    @ResponseBody
    public final String viewCollectionAlertsMap(@ModelAttribute(FORM_COLLECTIONS) final WebSecurityForm<CollectionCentreFilterForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_COLLECTION_STATUS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        /* validate user input. */
        this.collectionCentreFilterValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        final CollectionCentreFilterForm filter = (CollectionCentreFilterForm) webSecurityForm.getWrappedObject();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final ActiveAlertSearchCriteria searchCriteria = this.createSearchCriteria(new ActiveAlertSearchCriteria(), filter, userAccount);
        searchCriteria.setPlaced(true);
        
        final List<AlertMapEntry> collectionsList = this.alertsService.findPointOfSalesWithAlertMap(searchCriteria);
        
        return WidgetMetricsHelper.convertToJson(collectionsList, "collectionsList", true);
    }
    
    @RequestMapping(value = "/secure/collections/payStationCollectionsMade.html", method = RequestMethod.GET)
    @ResponseBody
    public final String viewPayStationCollectionsMade(final HttpServletRequest request, final HttpServletResponse response) {
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_COLLECTION_STATUS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final CustomerProperty timezoneProperty = this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(userAccount
                .getCustomer().getId(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        String timezoneString;
        if (timezoneProperty == null) {
            timezoneString = WebCoreConstants.SERVER_TIMEZONE_GMT;
        } else {
            timezoneString = timezoneProperty.getPropertyValue();
        }
        final TimeZone timezone = TimeZone.getTimeZone(timezoneString);
        final Calendar calendar = Calendar.getInstance(timezone);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        final Date minDate = calendar.getTime();
        
        final Calendar cal = Calendar.getInstance();
        final Date maxDate = cal.getTime();
        
        final String strRandomPosId = request.getParameter("pointOfSaleId");
        final Integer posId = verifyRandomIdAndReturnActual(response, keyMapping, strRandomPosId,
                                                            "+ Invalid randomised posId in CollectionCentreController.viewClearedCollectionAlerts() method. +");
        
        final CollectionSearchCriteria criteria = new CollectionSearchCriteria();
        criteria.setMinCollectionDate(minDate);
        criteria.setMaxCollectionDate(maxDate);
        criteria.setPointOfSaleId(posId);
        criteria.setCustomerId(userAccount.getCustomer().getId());
        final TimeZone customerTimeZone = TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone());
        final CollectionsCentrePosInfo info = this.posCollectionService.findPosInfoByCriteria(criteria, customerTimeZone);
        return WidgetMetricsHelper.convertToJson(info, "collectionsList", true);
        
    }
    
    @RequestMapping(value = "/secure/collections/viewCollectionAlerts.html", method = RequestMethod.POST)
    @ResponseBody
    public final String viewCollectionAlerts(@ModelAttribute(FORM_COLLECTIONS) final WebSecurityForm<CollectionCentreFilterForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_COLLECTION_STATUS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        /* validate user input. */
        this.collectionCentreFilterValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final CollectionCentreFilterForm filter = (CollectionCentreFilterForm) webSecurityForm.getWrappedObject();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final ActiveAlertSearchCriteria searchCriteria = this.createSearchCriteria(new ActiveAlertSearchCriteria(), filter, userAccount);
        searchCriteria.setPlaced(false);
        
        if (filter.getDataKey() == null) {
            filter.setDataKey(System.currentTimeMillis());
        }
        
        searchCriteria.setMaxAlertGmt(new Date(filter.getDataKey()));
        if (filter.getTargetId() != null) {
            searchCriteria.setTargetId(keyMapping.getKey(filter.getTargetId(), PointOfSale.class, Integer.class));
            if (searchCriteria.getTargetId() == null) {
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        if (searchCriteria.getTargetId() != null) {
            searchCriteria.setPage(this.alertsService.locatePageContainsPointOfSaleWithAlert(searchCriteria));
            if (searchCriteria.getPage() < 0) {
                return WebCoreConstants.RESPONSE_FALSE;
            }
        } else if (filter.getPageNumber() != null) {
            searchCriteria.setPage(filter.getPageNumber());
        }
        
        final List<AlertMapEntry> collectionsList = this.alertsService.findPointOfSalesWithAlertDetail(searchCriteria,
                                                                                                       WebSecurityUtil.getCustomerTimeZone());
        
        return WidgetMetricsHelper.convertToJson(new PaginatedList<AlertMapEntry>(collectionsList, Long.toString(searchCriteria.getMaxAlertGmt()
                                                         .getTime()), webSecurityForm.getInitToken(), searchCriteria.getPage()), "alertsList", true);
    }
    
    @RequestMapping(value = "/secure/alerts/collectionAlertsDetailReport.html", method = RequestMethod.POST)
    @ResponseBody
    public final String payStationAlertsDetailReport(
        @ModelAttribute(FORM_COLLECTIONS) final WebSecurityForm<CollectionCentreFilterForm> webSecurityForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_COLLECTION_STATUS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        /* validate user input. */
        this.collectionCentreFilterValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        final CollectionCentreFilterForm filter = (CollectionCentreFilterForm) webSecurityForm.getWrappedObject();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final MessageInfo messages = new MessageInfo();
        final AdhocReportInfo ari = this.reportDefinitionService.runAdhocReport(createAdhocReportDefinition(filter, userAccount),
                                                                                WebSecurityUtil.getCustomerTimeZone());
        if (ari.getReportHistoryId() != null) {
            return WidgetMetricsHelper.convertToJson(ari, "report", true);
        } else {
            final StringBuilder checkBackURI = new StringBuilder(CustomerAdminReportingController.URI_ADHOC_REPORT_STATUS);
            checkBackURI.append(".html?");
            ari.appendAsParameterString(checkBackURI);
            
            messages.checkback(checkBackURI.toString(), 3000);
            return WidgetMetricsHelper.convertToJson(messages, "messages", true);
        }
    }
    
    /*
     * private SummarySearchCriteria createCollectionSummarySearchCriteria(final CollectionCentreFilterForm filter, final UserAccount userAccount) {
     * final int alertThresholdArraySize = 8;
     * final SummarySearchCriteria criteria = new SummarySearchCriteria();
     * criteria.setCustomerId(userAccount.getCustomer().getId());
     * if (filter.getId() != null) {
     * if (filter.isSeverity()) {
     * criteria.setSeverityIds(new ArrayList<Integer>(1));
     * criteria.getSeverityIds().add(filter.getId());
     * } else if (!filter.isLocationOrRoute()) {
     * criteria.setLocationIds(new ArrayList<Integer>(1));
     * criteria.getLocationIds().add(filter.getId());
     * } else {
     * criteria.setRouteIds(new ArrayList<Integer>(1));
     * criteria.getRouteIds().add(filter.getId());
     * }
     * }
     * criteria.setExclusiveOrderBy(filter.getColumn());
     * criteria.setExclusiveOrderDir(filter.getOrder());
     * criteria.setAlertThresholdTypeIds(new ArrayList<Short>(alertThresholdArraySize));
     * if (filter.isBills()) {
     * criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_COUNT);
     * criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_DOLLARS);
     * }
     * if (filter.isCoin()) {
     * criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_COUNT);
     * criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_DOLLARS);
     * }
     * if (filter.isCredit()) {
     * criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_COUNT);
     * criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_DOLLARS);
     * }
     * if (filter.isTotal()) {
     * criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_RUNNING_TOTAL_DOLLAR);
     * }
     * if (filter.isOverdue()) {
     * criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_OVERDUE_COLLECTION);
     * }
     * // If none is selected, all selected.
     * if (criteria.getAlertThresholdTypeIds().size() <= 0) {
     * criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_COUNT);
     * criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_DOLLARS);
     * criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_COUNT);
     * criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_DOLLARS);
     * criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_COUNT);
     * criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_DOLLARS);
     * criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_RUNNING_TOTAL_DOLLAR);
     * criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_OVERDUE_COLLECTION);
     * }
     * return criteria;
     * }
     */
    
    @SuppressWarnings("unchecked")
    private <T> T createSearchCriteria(final ActiveAlertSearchCriteria criteria, final CollectionCentreFilterForm filter,
        final UserAccount userAccount) {
        final int alertThresholdArraySize = 8;
        
        criteria.setCustomerId(userAccount.getCustomer().getId());
        
        if (filter.getId() != null) {
            if (!filter.isLocationOrRoute()) {
                criteria.setLocationIds(new ArrayList<Integer>(1));
                criteria.getLocationIds().add(filter.getId());
            } else {
                criteria.setRouteIds(new ArrayList<Integer>(1));
                criteria.getRouteIds().add(filter.getId());
            }
        }
        
        if (filter.isSeverity()) {
            criteria.setSeverityIds(new ArrayList<Integer>(1));
            criteria.getSeverityIds().add(filter.getSeverityId());
        }
        
        criteria.setAlertThresholdTypeIds(new ArrayList<Integer>(alertThresholdArraySize));
        if (filter.isBills()) {
            criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_COUNT);
            criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_DOLLARS);
        }
        if (filter.isCoin()) {
            criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_COUNT);
            criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_DOLLARS);
        }
        if (filter.isCredit()) {
            criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_COUNT);
            criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_DOLLARS);
        }
        if (filter.isTotal()) {
            criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_RUNNING_TOTAL_DOLLAR);
        }
        if (filter.isOverdue()) {
            criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_OVERDUE_COLLECTION);
        }
        
        // If none is selected, all selected.
        if (criteria.getAlertThresholdTypeIds().size() <= 0) {
            criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_COUNT);
            criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_DOLLARS);
            criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_COUNT);
            criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_DOLLARS);
            criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_COUNT);
            criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_DOLLARS);
            criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_RUNNING_TOTAL_DOLLAR);
            criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_OVERDUE_COLLECTION);
        }
        
        return (T) criteria;
    }
    
    @RequestMapping(value = "/secure/collections/payStationCollectionsNeeded.html")
    @ResponseBody
    public final String getPayStationCollectionsNeeded(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_COLLECTION_STATUS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        final String strRandomPointOfSaleId = request.getParameter("pointOfSaleId");
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final Integer pointOfSaleId = verifyRandomIdAndReturnActual(response,
                                                                    keyMapping,
                                                                    strRandomPointOfSaleId,
                                                                    "+++ Invalid randomised payStationId in "
                                                                            + "CollectionCentreController.getPayStationCollectionsNeeded() method. +++");
        if (pointOfSaleId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final CollectionsCentrePayStationInfo payStationInfo = new CollectionsCentrePayStationInfo();
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        final PosBalance pointOfSaleBalance = this.pointOfSaleService.findPOSBalanceByPointOfSaleId(pointOfSale.getId());
        
        final List<PosEventCurrent> posEventCurrentList = this.posEventCurrentService.findCollectionCentreDetailByPointOfSaleId(pointOfSaleId);
        for (PosEventCurrent posEventCurrent : posEventCurrentList) {
            final CustomerAlertType alertType = posEventCurrent.getCustomerAlertType();
            payStationInfo.setSeverity(alertType.getAlertThresholdType().getId(), posEventCurrent.getEventSeverityType().getId());
        }
        
        payStationInfo.setRunningTotalDetails(pointOfSaleBalance, lastSeen(pointOfSaleBalance.getLastCollectionGmt()).toString());
        
        final List<Route> routeList = this.routeService.findRoutesByPointOfSaleIdAndRouteTypeId(pointOfSale.getId(),
                                                                                                WebCoreConstants.ROUTE_TYPE_COLLECTIONS);
        
        payStationInfo.setPayStationDetails(pointOfSale, lastSeen(pointOfSale.getId()).toString(), routeList);
        
        return WidgetMetricsHelper.convertToJson(payStationInfo, "payStationInfo", true);
    }
    
    @RequestMapping(value = "/secure/collections/payStationRecentCollections.html")
    @ResponseBody
    public final String getPayStationRecentCollections(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_COLLECTION_STATUS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        final String strRandomPOSCollectionId = request.getParameter("collectionId");
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final Integer collectionId = verifyRandomIdAndReturnActual(response,
                                                                   keyMapping,
                                                                   strRandomPOSCollectionId,
                                                                   "+++ Invalid randomised collectionId in "
                                                                           + "CollectionCentreController.getPayStationCollectionsNeeded() method. +++");
        if (collectionId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final CollectionsCentrePayStationInfo payStationInfo = new CollectionsCentrePayStationInfo();
        
        final PosCollection posCollection = this.posCollectionService.findPosCollectionByPosCollectionId(collectionId);
        
        final String timeZone = WebSecurityUtil.getCustomerTimeZone();
        
        payStationInfo.setPOSCollectionaDetails(posCollection);
        
        final List<Route> routeList = this.routeService.findRoutesByPointOfSaleIdAndRouteTypeId(posCollection.getPointOfSale().getId(),
                                                                                                WebCoreConstants.ROUTE_TYPE_COLLECTIONS);
        
        payStationInfo.setPayStationDetails(posCollection.getPointOfSale(), DateUtil.getRelativeTimeString(posCollection.getEndGmt(), timeZone),
                                            routeList);
        
        return WidgetMetricsHelper.convertToJson(payStationInfo, "payStationInfo", true);
    }
    
    @RequestMapping(value = "/secure/collections/posSummary.html")
    public final String getPosSummary(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_COLLECTIONS) final WebSecurityForm<CollectionCentreFilterForm> webSecurityForm) {
        
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_COLLECTION_STATUS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final List<SeverityValuePair> severityFilter = new ArrayList<SeverityValuePair>();
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Customer customer = userAccount.getCustomer();
        
        for (EventSeverityType severityType : this.alertsService.findAllEventSeverityTypes()) {
            if (severityType.getId() != WebCoreConstants.SEVERITY_CLEAR) {
                severityFilter.add(new SeverityValuePair(severityType.getName(), keyMapping.getRandomString(severityType,
                                                                                                            WebCoreConstants.ID_LOOK_UP_NAME)));
            }
        }
        
        final LocationRouteFilterInfo locationRouteInfo = this.commonControllerHelper
                .createLocationRouteFilter(customer.getId(), keyMapping, WebCoreConstants.ROUTE_TYPE_COLLECTIONS);
        
        model.put("locationFilter", locationRouteInfo.getLocations());
        model.put("routeFilter", locationRouteInfo.getRoutes());
        model.put("severityFilter", severityFilter);
        
        model.put(FORM_COLLECTIONS, webSecurityForm);
        
        return "/collections/summary";
    }
    
    @RequestMapping(value = "/secure/collections/posSummaryList.html")
    @ResponseBody
    public final String getPosSummaryList(@ModelAttribute(FORM_COLLECTIONS) final WebSecurityForm<CollectionCentreFilterForm> webSecurityForm,
        final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_COLLECTION_STATUS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        /* validate user input. */
        this.collectionCentreFilterValidator.validate(webSecurityForm, bindingResult);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        final CollectionCentreFilterForm filter = (CollectionCentreFilterForm) webSecurityForm.getWrappedObject();
        if (filter.getDataKey() == null) {
            filter.setDataKey((new Date()).getTime());
        }
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final SummarySearchCriteria searchCriteria = createSearchCriteria(new SummarySearchCriteria(), filter, userAccount);
        searchCriteria.setExclusiveOrderBy(filter.getColumn());
        searchCriteria.setExclusiveOrderDir(filter.getOrder());
        
        if (filter.getPage() != null) {
            searchCriteria.setPage(filter.getPage());
        }
        
        final String timeZoneId = WebSecurityUtil.getCustomerTimeZone();
        final List<CollectionSummaryEntry> collectionsList = this.alertsService.findPointOfSalesWithBalanceAndAlertSeverity(searchCriteria,
                                                                                                                            timeZoneId);
        
        final List<CollectionsCentreSummaryInfo> summaryInfoList = populateSummaryInfoList(collectionsList, request, response, timeZoneId);
        
        final PaginatedList<CollectionsCentreSummaryInfo> result = new PaginatedList<CollectionsCentreSummaryInfo>();
        result.setElements(summaryInfoList);
        if (filter.getDataKey() != null) {
            result.setDataKey(Long.toString(filter.getDataKey()));
        }
        return WidgetMetricsHelper.convertToJson(result, "collectionsList", true);
    }
    
    private List<CollectionsCentreSummaryInfo> populateSummaryInfoList(final List<CollectionSummaryEntry> collectionsList,
        final HttpServletRequest request, final HttpServletResponse response, final String timeZoneId) {
        
        final List<CollectionsCentreSummaryInfo> summaryInfoList = new LinkedList<CollectionsCentreSummaryInfo>();
        
        final List<Integer> alertThresholdTypeIdList = new ArrayList<Integer>();
        alertThresholdTypeIdList.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_COUNT);
        alertThresholdTypeIdList.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_DOLLARS);
        alertThresholdTypeIdList.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_COUNT);
        alertThresholdTypeIdList.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_DOLLARS);
        alertThresholdTypeIdList.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_COUNT);
        alertThresholdTypeIdList.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_DOLLARS);
        alertThresholdTypeIdList.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_RUNNING_TOTAL_DOLLAR);
        alertThresholdTypeIdList.add(WebCoreConstants.ALERT_THRESHOLD_TYPE_OVERDUE_COLLECTION);
        
        for (CollectionSummaryEntry collection : collectionsList) {
            
            final CollectionsCentreSummaryInfo collectionCentreSummaryInfo = new CollectionsCentreSummaryInfo();
            collectionCentreSummaryInfo.setPosRandomId(collection.getPosRandomId().toString());
            collectionCentreSummaryInfo.setPosName(collection.getPointOfSaleName());
            collectionCentreSummaryInfo.setSerial(collection.getSerial());
            collectionCentreSummaryInfo.setLocationName(collection.getLocationName());
            collectionCentreSummaryInfo.setLocationRandomId(collection.getLocationRandomId().toString());
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            
            final Integer pointOfSaleId = verifyRandomIdAndReturnActual(response,
                                                                        keyMapping,
                                                                        collection.getPosRandomId().toString(),
                                                                        "+++ Invalid randomised payStationId in "
                                                                                + "CollectionCentreController.getPayStationCollectionsNeeded() method. +++");
            
            String routeName = "";
            String routeRandomId = "";
            if (collection.getRoutes() != null && !collection.getRoutes().isEmpty()) {
                routeName = "Multiple";
                if (collection.getRoutes().size() < 2) {
                    routeName = collection.getRoutes().get(0).getName();
                    routeRandomId = keyMapping.getRandomString(collection.getRoutes().get(0), "id");
                }
            }
            collectionCentreSummaryInfo.setRouteName(routeName);
            collectionCentreSummaryInfo.setRouteRandomId(routeRandomId);
            
            final List<PosEventCurrent> posEventCurrentList = this.posEventCurrentService.findCollectionCentreDetailByPointOfSaleId(pointOfSaleId);
            for (PosEventCurrent posEventCurrent : posEventCurrentList) {
                collectionCentreSummaryInfo.setSeverity(posEventCurrent);
            }
            
            collectionCentreSummaryInfo.setRunningTotalDetails(collection, timeZoneId);
            
            summaryInfoList.add(collectionCentreSummaryInfo);
        }
        
        return summaryInfoList;
    }
    
    protected final Integer verifyRandomIdAndReturnActual(final HttpServletResponse response, final RandomKeyMapping keyMapping,
        final String strRandomLocationId, final String message) {
        
        final String[] msgs = new String[] { message, message };
        return WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, strRandomLocationId, msgs);
    }
    
    private RelativeDateTime lastSeen(final int pointOfSaleId) {
        final String timeZone = WebSecurityUtil.getCustomerTimeZone();
        final PosHeartbeat heartbeat = this.pointOfSaleService.findPosHeartbeatByPointOfSaleId(pointOfSaleId);
        
        return new RelativeDateTime(heartbeat.getLastHeartbeatGmt(), timeZone);
    }
    
    private RelativeDateTime lastSeen(final Date date) {
        final String timeZone = WebSecurityUtil.getCustomerTimeZone();
        
        return new RelativeDateTime(date, timeZone);
    }
    
    @ModelAttribute(FORM_COLLECTIONS)
    public final WebSecurityForm<CollectionCentreFilterForm> initCollectionsForm(final HttpServletRequest request) {
        return new WebSecurityForm<CollectionCentreFilterForm>((Object) null, new CollectionCentreFilterForm(), SessionTool.getInstance(request)
                .locatePostToken(FORM_COLLECTIONS));
    }
    
    protected final ReportDefinition createAdhocReportDefinition(final CollectionCentreFilterForm filter, final UserAccount userAccount) {
        final ReportDefinition result = new ReportDefinition();
        
        result.setCustomer(new Customer(userAccount.getCustomer().getId()));
        if (!filter.isLocationOrRoute()) {
            result.setLocationFilterTypeId(ReportingConstants.REPORT_FILTER_LOCATION_LOCATION_TREE);
            
            final ReportLocationValue rlv = new ReportLocationValue();
            rlv.setReportDefinition(result);
            rlv.setObjectId((filter.getId() == null) ? 0 : filter.getId());
            rlv.setObjectType(Location.class.getName());
            
            result.getReportLocationValues().add(rlv);
        } else {
            result.setLocationFilterTypeId(ReportingConstants.REPORT_FILTER_LOCATION_ROUTE_TREE);
            
            final ReportLocationValue rlv = new ReportLocationValue();
            rlv.setReportDefinition(result);
            rlv.setObjectId((filter.getId() == null) ? 0 : filter.getId());
            rlv.setObjectType(Route.class.getName());
            
            result.getReportLocationValues().add(rlv);
        }
        
        setReportDefinitionCollectionFlags(filter, result);
        
        result.setIsCsvOrPdf(true);
        result.setIsDetailsOrSummary(true);
        result.setUserAccount(userAccount);
        result.setLastModifiedByUserId(userAccount.getId());
        result.setReportType(new ReportType(ReportingConstants.REPORT_TYPE_COLLECTION_SUMMARY));
        result.setIsOnlyVisible(true);
        result.setGroupByFilterTypeId(ReportingConstants.REPORT_FILTER_GROUP_BY_LOCATION);
        
        return result;
    }
    
    private void setReportDefinitionCollectionFlags(final CollectionCentreFilterForm filter, final ReportDefinition reportDefinition) {
        short collectionFilter = 0;
        if (filter.isTotal()) {
            collectionFilter += ReportingConstants.COLLECTION_FILTER_BITMASK_RUNNING_TOTAL_DOLLAR;
        }
        if (filter.isCoin()) {
            collectionFilter += ReportingConstants.COLLECTION_FILTER_BITMASK_COIN_CANISTER_COUNT;
            collectionFilter += ReportingConstants.COLLECTION_FILTER_BITMASK_COIN_CANISTER_DOLLARS;
        }
        if (filter.isBills()) {
            collectionFilter += ReportingConstants.COLLECTION_FILTER_BITMASK_BILL_STACKER_COUNT;
            collectionFilter += ReportingConstants.COLLECTION_FILTER_BITMASK_BILL_STACKER_DOLLARS;
        }
        if (filter.isCredit()) {
            collectionFilter += ReportingConstants.COLLECTION_FILTER_BITMASK_UNSETTLED_CREDIT_CARD_COUNT;
            collectionFilter += ReportingConstants.COLLECTION_FILTER_BITMASK_UNSETTLED_CREDIT_CARD_DOLLARS;
        }
        if (filter.isOverdue()) {
            collectionFilter += ReportingConstants.COLLECTION_FILTER_BITMASK_OVERDUE_COLLECTION;
        }
        if (collectionFilter != 0) {
            reportDefinition.setReportCollectionFilterType(this.reportCollectionFilterTypeService.getReportCollectionFilterType(collectionFilter));
        }
    }
}
