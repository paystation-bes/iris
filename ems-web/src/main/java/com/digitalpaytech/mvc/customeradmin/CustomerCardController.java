package com.digitalpaytech.mvc.customeradmin;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.constants.SortOrder;
import com.digitalpaytech.domain.Consumer;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerCard;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.JsonResponse;
import com.digitalpaytech.dto.PaginatedList;
import com.digitalpaytech.dto.customeradmin.CustomerCardDTO;
import com.digitalpaytech.dto.customeradmin.CustomerCardSearchCriteria;
import com.digitalpaytech.exception.InvalidCSVDataException;
import com.digitalpaytech.mvc.customeradmin.support.CustomerCardForm;
import com.digitalpaytech.mvc.customeradmin.support.CustomerCardSearchForm;
import com.digitalpaytech.mvc.customeradmin.support.CustomerCardUploadForm;
import com.digitalpaytech.mvc.customeradmin.support.io.CustomerCardCsvReader;
import com.digitalpaytech.mvc.helper.AccountBeanHelper;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.WebObjectCacheManager;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.BatchProcessingException;
import com.digitalpaytech.service.CustomerCardService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.ServiceHelper;
import com.digitalpaytech.service.kpi.KPIListenerService;
import com.digitalpaytech.util.CustomerCardTypeWithLineNumbers;
import com.digitalpaytech.util.DashboardUtil;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.LocationWithLineNumbers;
import com.digitalpaytech.util.PointOfSaleWithLineNumbers;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.csv.CsvProcessingError;
import com.digitalpaytech.util.support.LookAheadMap;
import com.digitalpaytech.validation.customeradmin.CustomerCardSearchValidator;
import com.digitalpaytech.validation.customeradmin.CustomerCardUploadValidator;
import com.digitalpaytech.validation.customeradmin.CustomerCardValidator;

/**
 * 
 * 
 * @author wittawatv
 * 
 */

@Controller
public class CustomerCardController {
    public static final String FORM_SEARCH = "customerCardSearchForm";
    public static final String FORM_EDIT = "customerCardForm";
    public static final String FORM_UPLOAD = "customerCardUploadForm";
    public static final String MODEL_CUSTOMER_CARDS_JSON = "customerCardJSON";
    public static final String MODEL_CARD_TYPE_FILTER = "cardTypeFilter";
    public static final String MODEL_CARD_TYPE_FILTER_JSON = "cardTypeFilterJSON";
    public static final String MODEL_LOCATION_TREE = "locationTree";
    
    public static final String JSON_WRAPPED_OBJECT_NAME = "customerCards";
    public static final String JSON_LOCATION_TREE = "locationTree";
    public static final String JSON_CARD_TYPE_FILTER = "cardTypeFilter";
    public static final String JSON_CARD_DTO = "card";
    
    public static final String PARAM_CARD_ID = "cardId";
    
    public static final Integer DEFAULT_ITEMS_PER_PAGE = 25;
    
    @Autowired
    private CustomerCardService customerCardService;
    
    @Autowired
    private CustomerCardTypeService customerCardTypeService;
    
    @Autowired
    private LocationService locationService;
    
    @Autowired
    private CustomerCardSearchValidator searchValidator;
    @Autowired
    private CustomerCardValidator validator;
    @Autowired
    private CustomerCardUploadValidator uploadValidator;
    
    @Autowired
    private CustomerCardCsvReader csvReader;
    
    @Autowired
    private AccountBeanHelper accountBeanHelper;
    
    @Autowired
    private KPIListenerService kpiListenerService;
    
    @Autowired
    private WebObjectCacheManager webObjectCacheManager;
    
    @Autowired
    private EmsPropertiesService emsprops;
    
    @Autowired
    private ServiceHelper serviceHelper;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    public CustomerCardController() {
        
    }
    
    private boolean canView(final HttpServletRequest request, final HttpServletResponse response) {
        boolean result = true;
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_CUSTOM_CARDS)
            && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_CUSTOM_CARDS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            result = false;
        }
        
        return result;
    }
    
    private boolean canManage(final HttpServletRequest request, final HttpServletResponse response) {
        boolean result = true;
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_CUSTOM_CARDS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            result = false;
        }
        
        return result;
    }
    
    private boolean canViewForSelection(final HttpServletRequest request, final HttpServletResponse response) {
        boolean result = true;
        if ((!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_CONSUMER_ACCOUNTS))
            && (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_CONSUMER_ACCOUNTS))
            && (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_CUSTOM_CARDS))
            && (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_CUSTOM_CARDS))) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            result = false;
        }
        
        return result;
    }
    
    @RequestMapping(value = "/secure/accounts/customerCards/index.html", method = RequestMethod.GET)
    public final String showCustomerCards(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_SEARCH) final WebSecurityForm<CustomerCardSearchForm> secSearchForm,
        @ModelAttribute(FORM_EDIT) final WebSecurityForm<CustomerCardForm> secCardForm,
        @ModelAttribute(FORM_UPLOAD) final WebSecurityForm<CustomerCardUploadForm> secImportForm) {
        String resultVM = "/accounts/customerCards/index";
        if (!canView(request, response)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                resultVM = WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            resultVM = WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        } else {
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            final Integer customerId = WebSecurityUtil.getUserAccount().getCustomer().getId();
            
            model.put(FORM_SEARCH, secSearchForm);
            model.put(FORM_EDIT, secCardForm);
            model.put(FORM_UPLOAD, secImportForm);
            
            final CustomerCardSearchCriteria criteria = createSearchCriteria(secSearchForm.getWrappedObject(), request);
            final PaginatedList<CustomerCardDTO> cardsList =
                    prepareCustomerCardList(this.customerCardService.findCustomerCard(criteria), keyMapping, criteria);
            
            model.put(MODEL_CUSTOMER_CARDS_JSON, WidgetMetricsHelper.convertToJson(cardsList, JSON_WRAPPED_OBJECT_NAME, true));
            
            model.put(MODEL_LOCATION_TREE,
                      WidgetMetricsHelper
                              .convertToJson(this.locationService.getLocationPayStationTreeByCustomerId(customerId, true, false, true, keyMapping),
                                             JSON_LOCATION_TREE, false));
            
            final List<FilterDTO> cardTypeFilters =
                    this.customerCardTypeService.getValueCardTypeFilters(new ArrayList<FilterDTO>(), customerId, false, keyMapping);
            model.put(MODEL_CARD_TYPE_FILTER, cardTypeFilters);
            model.put(MODEL_CARD_TYPE_FILTER_JSON, WidgetMetricsHelper.convertToJson(cardTypeFilters, JSON_CARD_TYPE_FILTER, true));
        }
        
        return resultVM;
    }
    
    @RequestMapping(value = "/secure/accounts/customerCards/search.html", method = RequestMethod.POST)
    @ResponseBody
    public final String searchCustomerCards(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<CustomerCardSearchForm> webSecurityForm,
        final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        String result = WebCoreConstants.RESPONSE_FALSE;
        if (canView(request, response)) {
            searchValidator.validate(webSecurityForm, bindingResult);
            if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
                /* return the error status with JSON format to UI. */
                return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
            }
            
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            final CustomerCardSearchForm form = webSecurityForm.getWrappedObject();
            final CustomerCardSearchCriteria criteria = createSearchCriteria(form, request);
            
            final PaginatedList<CustomerCardDTO> cardsList =
                    prepareCustomerCardList(this.customerCardService.findCustomerCard(criteria), keyMapping, criteria);
            
            result = WidgetMetricsHelper.convertToJson(cardsList, JSON_WRAPPED_OBJECT_NAME, true);
        }
        
        return result;
    }
    
    @RequestMapping(value = "/secure/accounts/customerCards/searchUnused.html", method = RequestMethod.POST)
    @ResponseBody
    public final String searchUnusedCustomerCards(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<CustomerCardSearchForm> webSecurityForm,
        final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        String result = WebCoreConstants.RESPONSE_FALSE;
        if (canViewForSelection(request, response)) {
            searchValidator.validate(webSecurityForm, bindingResult);
            if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
                /* return the error status with JSON format to UI. */
                return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
            }
            
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            final CustomerCardSearchForm form = webSecurityForm.getWrappedObject();
            final CustomerCardSearchCriteria criteria = createSearchCriteria(form, request);
            criteria.setUnusedCard(true);
            criteria.setValidForDate(new Date());
            
            final PaginatedList<CustomerCardDTO> cardsList =
                    prepareCustomerCardList(this.customerCardService.findCustomerCard(criteria), keyMapping, criteria);
            
            result = WidgetMetricsHelper.convertToJson(cardsList, JSON_WRAPPED_OBJECT_NAME, true);
        }
        
        return result;
    }
    
    @RequestMapping(value = "/secure/accounts/customerCards/searchConsumerCards.html", method = RequestMethod.POST)
    @ResponseBody
    public final String searchConsumerCards(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<CustomerCardSearchForm> webSecurityForm,
        final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        String result = WebCoreConstants.RESPONSE_FALSE;
        if (canViewForSelection(request, response)) {
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            final CustomerCardSearchForm form = webSecurityForm.getWrappedObject();
            
            searchValidator.validate(webSecurityForm, bindingResult);
            //webSecurityForm.resetToken();
            if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
                /* return the error status with JSON format to UI. */
                return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
            }
            
            List<CustomerCard> cards = null;
            
            final CustomerCardSearchCriteria criteria = createSearchCriteria(webSecurityForm.getWrappedObject(), request);
            final List<Integer> cardIds = retrieveCustomerCardIds(form, keyMapping);
            if ((cardIds.size() > 0) && ((criteria.getPage() == null) || (criteria.getPage() <= 1))) {
                final Integer itemsPerPage = criteria.getItemsPerPage();
                criteria.setItemsPerPage(null);
                criteria.setCardIds(cardIds);
                
                cards = this.customerCardService.findCustomerCard(criteria);
                
                criteria.setItemsPerPage(itemsPerPage);
                criteria.setCardIds(null);
            }
            
            if (!form.isShowOnlySelectedCards()) {
                criteria.setIgnoredCardIds(cardIds);
                criteria.setUnusedCard(true);
                final List<CustomerCard> unselectedCards = this.customerCardService.findCustomerCard(criteria);
                if (cards == null) {
                    cards = unselectedCards;
                } else {
                    cards.addAll(unselectedCards);
                }
            }
            
            final PaginatedList<CustomerCardDTO> cardsList = prepareCustomerCardList(cards, keyMapping, criteria);
            
            result = WidgetMetricsHelper.convertToJson(cardsList, JSON_WRAPPED_OBJECT_NAME, true);
        }
        
        return result;
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/accounts/customerCards/locateItemPage.html", method = RequestMethod.POST)
    public final String getPageNumber(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<CustomerCardSearchForm> secureForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response) {
        String resultJson = WebCoreConstants.RESPONSE_FALSE;
        if (canView(request, response)) {
            searchValidator.validate(secureForm, result);
            if (secureForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
                resultJson = WidgetMetricsHelper.convertToJson(secureForm.getValidationErrorInfo(), "errorStatus", true);
            } else {
                final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
                final CustomerCardSearchForm form = secureForm.getWrappedObject();
                
                final CustomerCardSearchCriteria criteria = createSearchCriteria(form, request);
                int targetPage = -1;
                final Integer targetId = keyMapping.getKey(form.getTargetRandomId(), CustomerCard.class, Integer.class);
                if (targetId != null) {
                    criteria.setMaxUpdatedTime(new Date());
                    targetPage = this.customerCardService.findRecordPage(targetId, criteria);
                }
                
                if (targetPage > 0) {
                    criteria.setPage(targetPage);
                    
                    final PaginatedList<CustomerCardDTO> customerCardsList =
                            prepareCustomerCardList(this.customerCardService.findCustomerCard(criteria), keyMapping, criteria);
                    customerCardsList.setPage(targetPage);
                    
                    resultJson = WidgetMetricsHelper.convertToJson(customerCardsList, JSON_WRAPPED_OBJECT_NAME, true);
                }
            }
        }
        
        return resultJson;
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/accounts/customerCards/viewCard.html", method = RequestMethod.GET)
    public final String viewCustomerCardDetails(@ModelAttribute(FORM_EDIT) final WebSecurityForm<CustomerCardForm> webSecurityForm,
        final HttpServletRequest request, final HttpServletResponse response) {
        String result = WebCoreConstants.RESPONSE_FALSE;
        if (canView(request, response)) {
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            final Integer actualCardId = this.validateCardId(request);
            if (actualCardId != null) {
                final CustomerCardDTO dto = new CustomerCardDTO();
                final CustomerCard card = this.customerCardService.getCustomerCard(actualCardId);
                
                if (card.isIsDeleted()) {
                    final MessageInfo message = new MessageInfo();
                    message.setError(true);
                    message.setToken(null);
                    message.addMessage(this.commonControllerHelper.getMessage(ErrorConstants.REQUESTED_ITEM_DELETED));
                    return WidgetMetricsHelper.convertToJson(message, WebCoreConstants.UI_MESSAGES, true);
                }
                
                this.accountBeanHelper.populate(card, dto, keyMapping, false);
                
                result = WidgetMetricsHelper.convertToJson(dto, JSON_CARD_DTO, true);
            }
        }
        
        return result;
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/accounts/customerCards/saveCard.html", method = RequestMethod.POST)
    public final String saveCustomerCard(@ModelAttribute(FORM_EDIT) final WebSecurityForm<CustomerCardForm> webSecurityForm,
        final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        String result = WebCoreConstants.RESPONSE_FALSE;
        if (canManage(request, response)) {
            
            if (!((WebUser) WebSecurityUtil.getAuthentication().getPrincipal()).isSystemAdmin()
                && !this.validator.validateMigrationStatus(webSecurityForm)) {
                return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
            }
            
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            
            validator.validate(webSecurityForm, bindingResult);
            webSecurityForm.resetToken();
            if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() <= 0) {
                final CustomerCardForm form = webSecurityForm.getWrappedObject();
                CustomerCard po = null;
                if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_CREATE) {
                    po = new CustomerCard();
                    po.setCardNumber(form.getCardNumber().trim());
                    
                    final CustomerCardType customerCardType = new CustomerCardType();
                    customerCardType.setId((Integer) keyMapping.getKey(form.getCardTypeRandomId()));
                    po.setCustomerCardType(customerCardType);
                    
                    this.accountBeanHelper.populate(form, po, keyMapping);
                    
                    po.setAddedGmt(po.getLastModifiedGmt());
                    po.setIsDeleted(false);
                } else if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
                    final Integer customerCardId = (Integer) keyMapping.getKey(form.getRandomId());
                    po = this.customerCardService.getCustomerCard(customerCardId);
                    if (po == null) {
                        throw new RuntimeException("Could not locate customer card: " + customerCardId);
                    }
                    
                    this.accountBeanHelper.populate(form, po, keyMapping);
                }
                
                final CustomerCard duplicatedCard = this.customerCardService
                        .findCustomerCardByCustomerIdAndCardNumber(WebSecurityUtil.getUserAccount().getCustomer().getId(), po.getCardNumber());
                
                if ((duplicatedCard != null) && ((po.getId() == null) || (!po.getId().equals(duplicatedCard.getId())))) {
                    webSecurityForm.addErrorStatus(
                                                   new FormErrorStatus("cardType, cardNumber",
                                                           validator.getMessage("error.common.duplicated",
                                                                                new Object[] { validator.getMessage("label.customerCard"),
                                                                                    validator.getMessage("label.connector.and", new Object[] {
                                                                                        validator
                                                                                                .getMessage("label.customerCard.cardNumber"),
                                                                                        validator.getMessage("label.customerCard.cardType") }) })));
                } else {
                    if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_CREATE) {
                        this.customerCardService.createCustomerCard(po);
                    } else {
                        this.customerCardService.updateCustomerCard(po);
                    }
                }
            }
            
            if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() <= 0) {
                result = WebCoreConstants.RESPONSE_TRUE + ":" + webSecurityForm.getInitToken();
            } else {
                /* return the error status with JSON format to UI. */
                result = WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
            }
        }
        
        return result;
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/accounts/customerCards/deleteCardVerify.html", method = RequestMethod.GET)
    public final String deleteCustomerCardVerify(final HttpServletRequest request, final HttpServletResponse response) {
        String result = WebCoreConstants.RESPONSE_FALSE;
        if (canManage(request, response)) {
            if (!((WebUser) WebSecurityUtil.getAuthentication().getPrincipal()).isSystemAdmin() && !WebSecurityUtil.isCustomerMigrated()) {
                return WebCoreConstants.RESPONSE_FALSE + ":" + this.searchValidator.getMessage("error.customer.notMigrated");
            }
            
            final Integer actualCardId = this.validateCardId(request);
            if (actualCardId != null) {
                final CustomerCard card = customerCardService.getCustomerCard(actualCardId);
                if (card != null) {
                    final JsonResponse<Object> msgObj = new JsonResponse<Object>(true);
                    if (card.getConsumer() != null) {
                        msgObj.setMessage(this.validator.getMessage("alert.accounts.deleteAssignedCustomCardConfirm"));
                    }
                    
                    result = WidgetMetricsHelper.convertToJson(msgObj, "result", true);
                }
            }
        }
        
        return result;
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/accounts/customerCards/deleteCard.html", method = RequestMethod.GET)
    public final String deleteCustomerCard(final HttpServletRequest request, final HttpServletResponse response) {
        String result = WebCoreConstants.RESPONSE_FALSE;
        if (canManage(request, response)) {
            if (!((WebUser) WebSecurityUtil.getAuthentication().getPrincipal()).isSystemAdmin() && !WebSecurityUtil.isCustomerMigrated()) {
                return WebCoreConstants.RESPONSE_FALSE + ":" + this.searchValidator.getMessage("error.customer.notMigrated");
            }
            
            final Integer actualCardId = this.validateCardId(request);
            if (actualCardId == null) {
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            final CustomerCard card = customerCardService.getCustomerCard(actualCardId);
            customerCardService.deleteCustomerCard(card);
            
            result = WebCoreConstants.RESPONSE_TRUE;
        }
        
        return result;
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/accounts/customerCards/deleteAllCard.html", method = RequestMethod.GET)
    public final String deleteAllCustomerCard(final HttpServletRequest request, final HttpServletResponse response) {
        String result = WebCoreConstants.RESPONSE_FALSE;
        if (canManage(request, response)) {
            if (!((WebUser) WebSecurityUtil.getAuthentication().getPrincipal()).isSystemAdmin() && !WebSecurityUtil.isCustomerMigrated()) {
                return WebCoreConstants.RESPONSE_FALSE + ":" + this.searchValidator.getMessage("error.customer.notMigrated");
            }
            
            final MessageInfo messages = new MessageInfo();
            
            final UserAccount user = WebSecurityUtil.getUserAccount();
            final Integer customerId = user.getCustomer().getId();
            final int cardsCnt = customerCardService.countByCustomerId(customerId);
            if (cardsCnt == 0) {
                messages.addMessage(this.searchValidator.getMessage("alert.form.nothingToDelete",
                                                                    this.searchValidator.getMessage("link.nav.accounts.customerCards")));
                messages.setError(true);
            } else {
                final String statusKey = webObjectCacheManager.createProcessKey(request, user);
                this.customerCardService.deleteCustomerCardsByCustomerIdAsync(customerId, statusKey);
                
                int processMillis = this.emsprops.getProperty(EmsPropertiesService.CSV_UPLOAD_PROCTIME_PER_K, Integer.class, 1000) * cardsCnt / 1000;
                if (processMillis < 1000) {
                    processMillis = 1000;
                }
                
                messages.checkback(WebCoreConstants.URI_BATCH_PROCESS_STATUS + "?statusKey=" + statusKey, processMillis);
            }
            
            result = WidgetMetricsHelper.convertToJson(messages, "messages", true);
        }
        
        return result;
    }
    
    @RequestMapping(value = "/secure/accounts/customerCards/importCards.html", method = RequestMethod.POST)
    public final ResponseEntity<String> importCustomerCards(
        @ModelAttribute(FORM_UPLOAD) final WebSecurityForm<CustomerCardUploadForm> webSecurityForm, final BindingResult bindingResult,
        final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        String result = WebCoreConstants.RESPONSE_FALSE;
        if (canManage(request, response)) {
            final int updated = 0;
            final MessageInfo messages = new MessageInfo();
            
            final UserAccount user = WebSecurityUtil.getUserAccount();
            final Customer customer = user.getCustomer();
            
            if (!((WebUser) WebSecurityUtil.getAuthentication().getPrincipal()).isSystemAdmin()
                && !this.uploadValidator.validateMigrationStatus(webSecurityForm)) {
                result = WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
            } else {
                if (!this.uploadValidator.handleMultipartExceptions(webSecurityForm, request)) {
                    this.uploadValidator.validate(webSecurityForm, bindingResult);
                    if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() <= 0) {
                        final CustomerCardUploadForm form = webSecurityForm.getWrappedObject();
                        
                        final HashMap<String, Object> context = new HashMap<String, Object>(4);
                        context.put(CustomerCardCsvReader.CTXKEY_CUSTOMER, user.getCustomer());
                        
                        final TimeZone customerTimeZone = TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone());
                        context.put(CustomerCardCsvReader.CTXKEY_CUSTOMER_TIME_ZONE, customerTimeZone);
                        
                        final LookAheadMap<String, CustomerCardType> cardTypeLookAheadMap =
                                new LookAheadMap<String, CustomerCardType>(new CustomerCardTypeWithLineNumbers.Helper());
                        context.put(CustomerCardCsvReader.CTXKEY_CARD_TYPES, cardTypeLookAheadMap);
                        
                        final LookAheadMap<String, Location> locationLookAheadMap =
                                new LookAheadMap<String, Location>(new LocationWithLineNumbers.Helper());
                        context.put(CustomerCardCsvReader.CTXKEY_LOCATIONS, locationLookAheadMap);
                        
                        final LookAheadMap<String, PointOfSale> posLookAheadMap =
                                new LookAheadMap<String, PointOfSale>(new PointOfSaleWithLineNumbers.Helper());
                        context.put(CustomerCardCsvReader.CTXKEY_POINT_OF_SALES, posLookAheadMap);
                        
                        final LookAheadMap<String, PointOfSale> locPosLookAheadMap =
                                new LookAheadMap<String, PointOfSale>(new PointOfSaleWithLineNumbers.Helper());
                        context.put(CustomerCardCsvReader.CTXKEY_LOCATIONS_N_POSES, locPosLookAheadMap);
                        
                        try {
                            final ArrayList<CsvProcessingError> errors = new ArrayList<CsvProcessingError>();
                            final ArrayList<CsvProcessingError> warnings = new ArrayList<CsvProcessingError>();
                            
                            if (kpiListenerService.getKPIService() != null) {
                                kpiListenerService.getKPIService().addUIFileSize(form.getFile().getSize());
                            }
                            
                            final int maxSize =
                                    emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.MAX_UPLOAD_FILE_SIZE_IN_BYTES,
                                                                               EmsPropertiesService.DEFAULT_MAX_UPLOAD_FILE_SIZE_IN_BYTES);
                            
                            this.csvReader.processFile(errors, warnings, form.getFile(), context, maxSize);
                            
                            // Pay debts on CustomerCardType
                            if (cardTypeLookAheadMap.lookAheadKeySet().size() > 0) {
                                final List<CustomerCardType> cctsList = this.customerCardTypeService
                                        .getDetachedValueCardTypes(customer.getId(), cardTypeLookAheadMap.lookAheadKeySet());
                                for (CustomerCardType cct : cctsList) {
                                    final String cctSig = cct.getName().toLowerCase();
                                    cardTypeLookAheadMap.put(cctSig, cct);
                                }
                                
                                for (String cctSig : cardTypeLookAheadMap.lookAheadKeySet()) {
                                    final String errorMsg =
                                            this.uploadValidator.getMessage("error.common.invalid",
                                                                            this.uploadValidator.getMessage("label.settings.cardSettings.CardType"));
                                    final CustomerCardTypeWithLineNumbers cctNums =
                                            (CustomerCardTypeWithLineNumbers) cardTypeLookAheadMap.get(cctSig);
                                    if (cctNums.getLineNumbers().size() <= 0) {
                                        errors.add(new CsvProcessingError(errorMsg, 0));
                                    } else {
                                        for (Integer lineNum : cctNums.getLineNumbers()) {
                                            errors.add(new CsvProcessingError(errorMsg, lineNum));
                                        }
                                    }
                                }
                            }
                            
                            // Pay debts on Locations
                            if (locationLookAheadMap.lookAheadKeySet().size() > 0) {
                                final List<Location> locsList = this.locationService
                                        .getDetachedLocationsByLowerCaseNames(customer.getId(), locationLookAheadMap.lookAheadKeySet());
                                for (Location loc : locsList) {
                                    final String locSig = loc.getName().toLowerCase();
                                    if (locationLookAheadMap.lookAheadKeySet().contains(locSig)) {
                                        locationLookAheadMap.put(locSig, loc);
                                    } else {
                                        final String warningMsg = this.uploadValidator
                                                .getMessage("error.common.ambiguous", this.uploadValidator.getMessage("label.customerCard.location"),
                                                            loc.getName());
                                        final LocationWithLineNumbers locNums = (LocationWithLineNumbers) locationLookAheadMap.get(locSig);
                                        if (locNums.getLineNumbers().size() <= 0) {
                                            errors.add(new CsvProcessingError(warningMsg, 0, true));
                                        } else {
                                            for (Integer lineNum : locNums.getLineNumbers()) {
                                                errors.add(new CsvProcessingError(warningMsg, lineNum, true));
                                            }
                                        }
                                    }
                                }
                                
                                for (String locSig : locationLookAheadMap.lookAheadKeySet()) {
                                    final String errorMsg = this.uploadValidator
                                            .getMessage("error.common.invalid", this.uploadValidator.getMessage("label.customerCard.location"));
                                    final LocationWithLineNumbers locNums = (LocationWithLineNumbers) locationLookAheadMap.get(locSig);
                                    if (locNums.getLineNumbers().size() <= 0) {
                                        errors.add(new CsvProcessingError(errorMsg, 0));
                                    } else {
                                        for (Integer lineNum : locNums.getLineNumbers()) {
                                            errors.add(new CsvProcessingError(errorMsg, lineNum));
                                        }
                                    }
                                }
                            }
                            
                            // Pay debts on PointOfSales
                            if (posLookAheadMap.lookAheadKeySet().size() > 0) {
                                final List<PointOfSale> posesList = this.pointOfSaleService
                                        .getDetachedPointOfSaleByLowerCaseNames(customer.getId(), posLookAheadMap.lookAheadKeySet(), (String) null);
                                for (PointOfSale pos : posesList) {
                                    final String posSig = pos.getName().toLowerCase();
                                    if (posLookAheadMap.lookAheadKeySet().contains(posSig)) {
                                        posLookAheadMap.put(posSig, pos);
                                    } else {
                                        final String warningMsg = this.uploadValidator
                                                .getMessage("error.common.ambiguous",
                                                            this.uploadValidator.getMessage("label.customerCard.pointOfSale"), pos.getName());
                                        final PointOfSaleWithLineNumbers posNums = (PointOfSaleWithLineNumbers) posLookAheadMap.get(posSig);
                                        if (posNums.getLineNumbers().size() <= 0) {
                                            errors.add(new CsvProcessingError(warningMsg, 0, true));
                                        } else {
                                            for (Integer lineNum : posNums.getLineNumbers()) {
                                                errors.add(new CsvProcessingError(warningMsg, lineNum, true));
                                            }
                                        }
                                    }
                                }
                                
                                for (String posSig : posLookAheadMap.lookAheadKeySet()) {
                                    final String errorMsg = this.uploadValidator
                                            .getMessage("error.common.invalid", this.uploadValidator.getMessage("label.customerCard.pointOfSale"));
                                    final PointOfSaleWithLineNumbers posNums = (PointOfSaleWithLineNumbers) posLookAheadMap.get(posSig);
                                    if (posNums.getLineNumbers().size() <= 0) {
                                        errors.add(new CsvProcessingError(errorMsg, 0));
                                    } else {
                                        for (Integer lineNum : posNums.getLineNumbers()) {
                                            errors.add(new CsvProcessingError(errorMsg, lineNum));
                                        }
                                    }
                                }
                            }
                            
                            // Pay debts on PointOfSales (locPos)
                            if (locPosLookAheadMap.lookAheadKeySet().size() > 0) {
                                final List<PointOfSale> posesList = this.pointOfSaleService
                                        .getDetachedPointOfSaleByLowerCaseNames(customer.getId(), locPosLookAheadMap.lookAheadKeySet(),
                                                                                CustomerCardCsvReader.SEPARATOR_LOC_POS);
                                for (PointOfSale pos : posesList) {
                                    final String posSig = pos.getLocation().getName().toLowerCase() + CustomerCardCsvReader.SEPARATOR_LOC_POS
                                                          + pos.getName().toLowerCase();
                                    if (locPosLookAheadMap.lookAheadKeySet().contains(posSig)) {
                                        locPosLookAheadMap.put(posSig, pos);
                                    } else {
                                        final String warningMsg = this.uploadValidator
                                                .getMessage("error.common.ambiguous",
                                                            this.uploadValidator.getMessage("label.customerCard.pointOfSale"), pos.getName());
                                        final PointOfSaleWithLineNumbers posNums = (PointOfSaleWithLineNumbers) locPosLookAheadMap.get(posSig);
                                        if (posNums.getLineNumbers().size() <= 0) {
                                            errors.add(new CsvProcessingError(warningMsg, 0, true));
                                        } else {
                                            for (Integer lineNum : posNums.getLineNumbers()) {
                                                errors.add(new CsvProcessingError(warningMsg, lineNum, true));
                                            }
                                        }
                                    }
                                }
                                
                                for (String posSig : locPosLookAheadMap.lookAheadKeySet()) {
                                    final String errorMsg = this.uploadValidator
                                            .getMessage("error.common.invalid", this.uploadValidator.getMessage("label.customerCard.pointOfSale"));
                                    final PointOfSaleWithLineNumbers posNums = (PointOfSaleWithLineNumbers) locPosLookAheadMap.get(posSig);
                                    if (posNums.getLineNumbers().size() <= 0) {
                                        errors.add(new CsvProcessingError(errorMsg, 0));
                                    } else {
                                        for (Integer lineNum : posNums.getLineNumbers()) {
                                            errors.add(new CsvProcessingError(errorMsg, lineNum));
                                        }
                                    }
                                }
                            }
                            
                            if (errors.size() > 0) {
                                final Map<String, StringBuilder> errorsGroup = CsvProcessingError.groupErrors(errors);
                                for (String errorMessage : errorsGroup.keySet()) {
                                    webSecurityForm.addErrorStatus(new FormErrorStatus("file",
                                            errorMessage + " [lines# " + errorsGroup.get(errorMessage).toString() + "]"));
                                }
                            } else {
                                final String statusKey = webObjectCacheManager.createProcessKey(request, user);
                                
                                if (warnings.size() <= 0) {
                                    messages.setRequireManualClose(false);
                                } else {
                                    this.serviceHelper.getProcessStatus(statusKey).warn();
                                    messages.setRequireManualClose(true);
                                    
                                    final Map<String, StringBuilder> errorsGroup = CsvProcessingError.groupErrors(warnings);
                                    for (String errorMessage : errorsGroup.keySet()) {
                                        messages.addMessage(errorMessage + " [lines# " + errorsGroup.get(errorMessage).toString() + "]");
                                    }
                                }
                                
                                @SuppressWarnings("unchecked")
                                final Collection<CustomerCard> cardsList =
                                        (Collection<CustomerCard>) context.get(CustomerCardCsvReader.CTXKEY_RESULT);
                                
                                int processMillis = this.emsprops.getProperty(EmsPropertiesService.CSV_UPLOAD_PROCTIME_PER_K, Integer.class, 1000)
                                                    * cardsList.size() / 1000;
                                if (processMillis < 1000) {
                                    processMillis = 1000;
                                }
                                
                                this.customerCardService.processBatchAddCustomerCardsAsync(statusKey, customer.getId(), cardsList,
                                                                                           form.getImportMode(), user.getId());
                                messages.checkback(WebCoreConstants.URI_BATCH_PROCESS_STATUS + "?statusKey=" + statusKey, processMillis);
                            }
                        } catch (InvalidCSVDataException icsvde) {
                            webSecurityForm.addErrorStatus(new FormErrorStatus("file", icsvde.getMessage()));
                        } catch (BatchProcessingException bpe) {
                            webSecurityForm.addErrorStatus(new FormErrorStatus("file", bpe.getMessage()));
                        }
                    }
                }
                
                if ((updated > 0) && (messages.hasMessages())) {
                    messages.prependMessage(this.uploadValidator.getMessage("error.common.bulkupdate.success.with.fail",
                                                                            this.uploadValidator.getMessage("label.customerCard")));
                }
                
                if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
                    result = WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
                } else if ((!messages.hasMessages()) && (messages.getPollingURI() == null)) {
                    result = WebCoreConstants.RESPONSE_TRUE;
                } else {
                    result = WidgetMetricsHelper.convertToJson(messages, "messages", true);
                }
            }
        }
        
        final HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.TEXT_HTML);
        
        return new ResponseEntity<String>(WebCoreUtil.cleanupIframeJSON(result), responseHeaders, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/secure/accounts/customerCards/exportCards.html", method = RequestMethod.GET)
    public final void exportCustomCards(final HttpServletRequest request, final HttpServletResponse response) {
        if (canManage(request, response)) {
            final UserAccount userAccount = WebSecurityUtil.getUserAccount();
            final Customer customer = userAccount.getCustomer();
            final Collection<CustomerCard> cards = customerCardService.findCustomerCardByCustomerId(customer.getId());
            
            final TimeZone customerTimeZone = TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone());
            final SimpleDateFormat fmt = new SimpleDateFormat(WebCoreConstants.FORMAT_YYYYMMDD);
            fmt.setTimeZone(customerTimeZone);
            final String fileName = this.uploadValidator.getMessage("label.customerCard.export.fileName", customer.getId(), customer.getName(),
                                                                    fmt.format(new Date()));
            
            response.setHeader("Content-Type", "text/csv");
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
            response.setHeader("Cache-Control", "public");
            response.setHeader("Pragma", "public");
            
            final HashMap<String, Object> context = new HashMap<String, Object>(2);
            context.put(CustomerCardCsvReader.CTXKEY_CUSTOMER, customer);
            context.put(CustomerCardCsvReader.CTXKEY_CUSTOMER_TIME_ZONE, customerTimeZone);
            
            PrintWriter writer = null;
            try {
                writer = response.getWriter();
                this.csvReader.createCsv(writer, cards, context);
                
            } catch (IOException e) {
                response.setStatus(HttpStatus.EXPECTATION_FAILED.value());
                return;
            } finally {
                if (writer != null) {
                    writer.flush();
                    writer.close();
                }
            }
        }
    }
    
    private Integer validateCardId(final HttpServletRequest request) {
        
        /* validate the randomized customer bad card ID from request parameter. */
        final String randomizedCardId = request.getParameter(PARAM_CARD_ID);
        if (StringUtils.isBlank(randomizedCardId) || !DashboardUtil.validateRandomId(randomizedCardId)) {
            return null;
        }
        
        /* validate the actual customer bad card ID. */
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer actualCardId = keyMapping.getKey(randomizedCardId, CustomerCard.class, Integer.class);
        if (actualCardId == null || !DashboardUtil.validateDBIntegerPrimaryKey(actualCardId.toString())) {
            return null;
        }
        
        return actualCardId;
    }
    
    private List<Integer> retrieveCustomerCardIds(final CustomerCardSearchForm form, final RandomKeyMapping keyMapping) {
        final ArrayList<Integer> cardIds = new ArrayList<Integer>();
        if ((form.getCustomerCardRandomIds() != null) && (form.getCustomerCardRandomIds().length() > 0)) {
            final StringTokenizer tokenizer = new StringTokenizer(form.getCustomerCardRandomIds(), ",");
            while (tokenizer.hasMoreTokens()) {
                cardIds.add((Integer) keyMapping.getKey(tokenizer.nextToken().trim()));
            }
        }
        
        return cardIds;
    }
    
    private PaginatedList<CustomerCardDTO> prepareCustomerCardList(final List<CustomerCard> poList, final RandomKeyMapping keyMapping,
        final CustomerCardSearchCriteria criteria) {
        final PaginatedList<CustomerCardDTO> result = new PaginatedList<CustomerCardDTO>();
        
        final ArrayList<CustomerCardDTO> dtoList = new ArrayList<CustomerCardDTO>(poList.size());
        for (CustomerCard po : poList) {
            final CustomerCardDTO dto = new CustomerCardDTO();
            this.accountBeanHelper.populate(po, dto, keyMapping, true);
            
            dtoList.add(dto);
        }
        
        result.setElements(dtoList);
        
        if (criteria.getMaxUpdatedTime() != null) {
            result.setDataKey(Long.toString(criteria.getMaxUpdatedTime().getTime()));
        }
        
        return result;
    }
    
    private CustomerCardSearchCriteria createSearchCriteria(final CustomerCardSearchForm form, final HttpServletRequest request) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final CustomerCardSearchCriteria result = new CustomerCardSearchCriteria();
        result.setCustomerId(WebSecurityUtil.getUserAccount().getCustomer().getId());
        if ((form.getFilterValue() != null) && (form.getFilterValue().trim().length() > 0)) {
            final ArrayList<String> cardNumber = new ArrayList<String>(2);
            result.setCardNumber(cardNumber);
            
            final ArrayList<String> cardType = new ArrayList<String>(2);
            result.setCardTypeName(cardType);
            
            Matcher m = Pattern.compile("^([0-9]+)([^0-9]*)$").matcher(form.getFilterValue());
            if (m.matches()) {
                cardNumber.add(m.group(1));
                final String buffer = m.group(2).trim();
                if (buffer.length() > 0) {
                    cardType.add(buffer);
                    cardType.add(form.getFilterValue());
                }
            } else {
                m = Pattern.compile("^([^0-9]*)([0-9]+)$").matcher(form.getFilterValue());
                if (m.matches()) {
                    cardNumber.add(m.group(2));
                    final String buffer = m.group(1).trim();
                    if (buffer.length() > 0) {
                        cardType.add(buffer);
                        cardType.add(form.getFilterValue());
                    }
                } else {
                    cardType.add(form.getFilterValue());
                }
            }
        }
        
        if (!form.isShowOnlySelectedCards()) {
            result.setPage((form.getPage() == null) ? 1 : form.getPage());
            if (form.getItemsPerPage() == null) {
                result.setItemsPerPage(WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT);
            } else if (form.getItemsPerPage() > WebCoreConstants.MAX_RESULT_COUNT) {
                result.setItemsPerPage(WebCoreConstants.MAX_RESULT_COUNT);
            } else {
                result.setItemsPerPage(form.getItemsPerPage());
            }
            
            if ((form.getDataKey() != null) && (form.getDataKey().length() > 0)) {
                try {
                    result.setMaxUpdatedTime(new Date(Long.parseLong(form.getDataKey())));
                } catch (NumberFormatException nfe) {
                    // DO NOTHING.
                }
            }
        }
        
        if (form.getIgnoreConsumerRandomId() != null) {
            result.setIgnoredConsumerId(keyMapping.getKey(form.getIgnoreConsumerRandomId(), Consumer.class, Integer.class));
        }
        
        if (form.getIgnoreRandomIds() != null) {
            result.setIgnoredCardIds(keyMapping.getAllKeys(form.getIgnoreRandomIds(), CustomerCard.class, Integer.class));
        }
        
        if ((form.getColumn() != null)) {
            result.setOrderColumn(form.getColumn().getActualAttribute());
        }
        if (form.getOrder() != null) {
            result.setOrderDesc((form.getOrder() == SortOrder.ASC) ? false : true);
        }
        
        if (result.getOrderColumn() == null) {
            result.setOrderColumn(CustomerCardSearchForm.SortColumn.CARD_NUMBER.getActualAttribute());
        }
        
        if (result.getMaxUpdatedTime() == null) {
            result.setMaxUpdatedTime(new Date());
        }
        
        return result;
    }
    
    @ModelAttribute(FORM_SEARCH)
    public final WebSecurityForm<CustomerCardSearchForm> initSearchForm(final HttpServletRequest request) {
        return new WebSecurityForm<CustomerCardSearchForm>((Object) null, new CustomerCardSearchForm(),
                SessionTool.getInstance(request).locatePostToken(FORM_SEARCH));
    }
    
    @ModelAttribute(FORM_EDIT)
    public final WebSecurityForm<CustomerCardForm> initEditForm(final HttpServletRequest request) {
        return new WebSecurityForm<CustomerCardForm>((Object) null, new CustomerCardForm(),
                SessionTool.getInstance(request).locatePostToken(FORM_EDIT));
    }
    
    @ModelAttribute(FORM_UPLOAD)
    public final WebSecurityForm<CustomerCardUploadForm> initUploadForm(final HttpServletRequest request) {
        return new WebSecurityForm<CustomerCardUploadForm>((Object) null, new CustomerCardUploadForm(),
                SessionTool.getInstance(request).locatePostToken(FORM_UPLOAD));
    }
}
