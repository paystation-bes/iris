package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;

import javax.persistence.Transient;

public class CouponForm implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 4155742267819058650L;
    private String randomId;
    private String customerRandomId;
    private String couponCode;
    private String percentDiscount;
    private String dollarDiscount;
    private String discountType;
    private String discountValue;
    private String startDate;
    private String endDate;
    private Character numUsesType;
    private String numUses;
    private String locationRandomId;
    private String stallRange;
    private String description;
    private boolean pndEnabled;
    private boolean pbsEnabled;
    private String useStartDate;
    private String useEndDate;
    private String useNumUses;
    private String validRegionString;
    private String usePercentDiscount;
    private String useDollarDiscount;
    private String useDiscount;
    private String useAvailableFor;
    private boolean validForNumOfDay;
    
    @Transient
    private transient Integer id;
    
    @Transient
    private transient Integer customerId;
    
    @Transient
    private transient Integer locationId;

    
    public CouponForm() {
    }


    public String getCustomerRandomId() {
        return customerRandomId;
    }
    public void setCustomerRandomId(String customerRandomId) {
        this.customerRandomId = customerRandomId;
    }
    public String getRandomId() {
        return randomId;
    }
    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }
    public String getCouponCode() {
        return couponCode;
    }
    public void setCouponCode(String couponCode) {
       this.couponCode = couponCode;
    }
    public void setLocationRandomId(String locationRandomId) {
        this.locationRandomId = locationRandomId;
    }
    public String getPercentDiscount() {
        return percentDiscount;
    }
    public void setPercentDiscount(String percentDiscount) {
        this.percentDiscount = percentDiscount;
    }
    public String getDollarDiscount() {
        return dollarDiscount;
    }
    public void setDollarDiscount(String dollarDiscount) {
        this.dollarDiscount = dollarDiscount;
    }
    public String getDiscountType() {
        return discountType;
    }
    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }
    public String getDiscountValue() {
        return discountValue;
    }
    public void setDiscountValue(String discountValue) {
        this.discountValue = discountValue;
    }
    public String getStartDate() {
        return startDate;
    }
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    public String getEndDate() {
        return endDate;
    }
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    public String getNumUses() {
        return numUses;
    }
    public void setNumUses(String numUses) {
        this.numUses = numUses;
    }
    public String getLocationRandomId() {
        return locationRandomId;
    }
    public void setLocationId(String locationId) {
        this.locationRandomId = locationId;
    }
    public String getStallRange() {
        return stallRange;
    }
    public void setStallRange(String stallRange) {
        this.stallRange = stallRange;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public boolean isPndEnabled() {
        return pndEnabled;
    }
    public void setPndEnabled(boolean pndEnabled) {
        this.pndEnabled = pndEnabled;
    }
    public boolean isPbsEnabled() {
        return pbsEnabled;
    }
    public void setPbsEnabled(boolean pbsEnabled) {
        this.pbsEnabled = pbsEnabled;
    }
    public String getUseStartDate() {
        return useStartDate;
    }
    public void setUseStartDate(String useStartDate) {
        this.useStartDate = useStartDate;
    }
    public String getUseEndDate() {
        return useEndDate;
    }
    public void setUseEndDate(String useEndDate) {
        this.useEndDate = useEndDate;
    }
    public String getUseNumUses() {
        return useNumUses;
    }
    public void setUseNumUses(String useNumUses) {
        this.useNumUses = useNumUses;
    }
    public String getValidRegionString() {
        return validRegionString;
    }
    public void setValidRegionString(String validRegionString) {
        this.validRegionString = validRegionString;
    }
    public String getUsePercentDiscount() {
        return usePercentDiscount;
    }
    public void setUsePercentDiscount(String usePercentDiscount) {
        this.usePercentDiscount = usePercentDiscount;
    }
    public String getUseDollarDiscount() {
        return useDollarDiscount;
    }
    public void setUseDollarDiscount(String useDollarDiscount) {
        this.useDollarDiscount = useDollarDiscount;
    }
    public String getUseDiscount() {
        return useDiscount;
    }
    public void setUseDiscount(String useDiscount) {
        this.useDiscount = useDiscount;
    }
    public String getUseAvailableFor() {
        return useAvailableFor;
    }
    public void setUseAvailableFor(String useAvailableFor) {
        this.useAvailableFor = useAvailableFor;
    }
    public boolean isValidForNumOfDay() {
        return validForNumOfDay;
    }
    public void setValidForNumOfDay(boolean validForNumOfDay) {
        this.validForNumOfDay = validForNumOfDay;
    }


    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public Integer getCustomerId() {
        return customerId;
    }


    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }


    public Integer getLocationId() {
        return locationId;
    }


    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }


	public Character getNumUsesType() {
		return numUsesType;
	}


	public void setNumUsesType(Character numUsesType) {
		this.numUsesType = numUsesType;
	}
}
