package com.digitalpaytech.mvc.customeradmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.mvc.MobileDeviceController;
import com.digitalpaytech.mvc.customeradmin.support.ActivityLogFilterForm;
import com.digitalpaytech.mvc.customeradmin.support.AppTypeFilterForm;
import com.digitalpaytech.mvc.customeradmin.support.MobileDeviceEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
public class CustomerAdminMobileDeviceController extends MobileDeviceController {

	@Override
	@RequestMapping(value = "/secure/settings/devices/index.html")
	public String mobileDeviceControllerIndex(HttpServletRequest request,
			HttpServletResponse response, ModelMap model) {
		// TODO index
		// EMS 4922
		if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_MOBILEDEVICE)
				&& !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_MOBILEDEVICE)) {
		    if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD) && 
                    !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) { 
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }            
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
		}
		return super.mobileDeviceControllerIndex(request, response, model);
	}

	@Override
	@RequestMapping(value = "/secure/settings/devices/deviceList.html")
	public @ResponseBody
	String getDeviceList(
			HttpServletRequest request,
			HttpServletResponse response,
			@ModelAttribute(FORM_APPTYPE) WebSecurityForm<AppTypeFilterForm> webSecurityForm,
			BindingResult result, ModelMap model) {
		// TODO getDeviceList
		// EMS 4922
		if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_MOBILEDEVICE)
				&& !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_MOBILEDEVICE)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD) && 
                    !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) { 
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }            
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
		}
		return super.getDeviceList(request, response, webSecurityForm, result,
				model);
	}

	@Override
	@RequestMapping(value = "/secure/settings/devices/deviceDetail.html")
	public @ResponseBody
	String getDeviceDetail(HttpServletRequest request,
			HttpServletResponse response) {
		// TODO getDeviceDetail
		// EMS 4922
		if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_MOBILEDEVICE)
				&& !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_MOBILEDEVICE)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD) && 
                    !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) { 
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }            
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
		}
		return super.getDeviceDetail(request, response);
	}

	@Override
	@RequestMapping(value = "/secure/settings/devices/deviceHistory.html")
	public @ResponseBody
	String getDeviceHistory(HttpServletRequest request, HttpServletResponse response, @ModelAttribute(FORM_ACTIVITY) WebSecurityForm<ActivityLogFilterForm> webSecurityForm,BindingResult result, ModelMap model) {
		// TODO getDeviceHistory
		// EMS 4922
		if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_MOBILEDEVICE)
				&& !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_MOBILEDEVICE)) {
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			return WebCoreConstants.RESPONSE_FALSE;
		}
		return super.getDeviceHistory(request, response, webSecurityForm, result, model);
	}

	@Override
	@RequestMapping(value = "/secure/settings/devices/saveDevice.html", method = RequestMethod.POST)
	public @ResponseBody
	String saveDevice(HttpServletRequest request, HttpServletResponse response,
	                  @ModelAttribute(FORM_DEVICE) WebSecurityForm<MobileDeviceEditForm> webSecurityForm, BindingResult result) {
		// TODO saveDevice
		// EMS 4922
		if (!WebSecurityUtil
				.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_MOBILEDEVICE)) {
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			return WebCoreConstants.RESPONSE_FALSE;
		}
		return super.saveDevice(request, response, webSecurityForm, result);
	}

	@Override
	@RequestMapping(value = "/secure/settings/devices/lockDevice.html")
	public @ResponseBody
	String lockDevice(HttpServletRequest request, HttpServletResponse response) {
		// TODO lockDevice
		// EMS 4922
		if (!WebSecurityUtil
				.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_MOBILEDEVICE)) {
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			return WebCoreConstants.RESPONSE_FALSE;
		}
		return super.lockDevice(request, response);
	}

	@Override
	@RequestMapping(value = "/secure/settings/devices/releaseDevice.html")
	public @ResponseBody
	String releaseDevice(HttpServletRequest request,
			HttpServletResponse response) {
		// TODO releaseDevice
		// EMS 4922
		if (!WebSecurityUtil
				.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_MOBILEDEVICE)) {
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			return WebCoreConstants.RESPONSE_FALSE;
		}
		return super.releaseDevice(request, response);
	}

	@Override
	@RequestMapping(value = "/secure/settings/devices/unLockDevice.html")
	public @ResponseBody
	String unLockDevice(HttpServletRequest request, HttpServletResponse response) {
		if (!WebSecurityUtil
				.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_MOBILEDEVICE)) {
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			return WebCoreConstants.RESPONSE_FALSE;
		}
		return super.unLockDevice(request, response);
	}

	@Override
	@ModelAttribute(FORM_DEVICE)
	public WebSecurityForm<MobileDeviceEditForm> initMobileDeviceForm(
			HttpServletRequest request) {
		return super.initMobileDeviceForm(request);
	}

	@Override
	@ModelAttribute(FORM_APPTYPE)
	public WebSecurityForm<AppTypeFilterForm> initAppTypeFilterForm(
			HttpServletRequest request) {
		return super.initAppTypeFilterForm(request);
	}
	
	@Override
	@ModelAttribute(FORM_ACTIVITY)
	public WebSecurityForm<ActivityLogFilterForm> initActivityForm(HttpServletRequest request){
	    return super.initActivityForm(request);
	}
}
