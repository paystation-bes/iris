package com.digitalpaytech.mvc.customeradmin.support.io;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.dto.comparator.CouponComparator;
import com.digitalpaytech.mvc.customeradmin.support.io.dto.CouponCsvDTO;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.util.LocationWithLineNumbers;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.csv.CsvProcessingError;
import com.digitalpaytech.util.csv.CsvProcessor;
import com.digitalpaytech.util.csv.CsvParserUtils;
import com.digitalpaytech.util.csv.CsvWriter;
import com.digitalpaytech.validation.customeradmin.CouponCsvValidator;

@Component("couponCsvReader")
@Transactional(readOnly = true)
public class CouponCsvReader extends CsvProcessor<Coupon> {
    public static final String CTXKEY_LOCATION = "locationsMap";
    
    private static final String[] COLUMN_HEADERS = { "CouponCode", "Description", "PercentDiscount", "DollarDiscount", "StartDate", "EndDate",
        "NumUses", "DailySingleUse", "LocationName", "EnablePND/PBL", "EnablePayByStall", "StallRange", };
    
    private static final int NUM_COLUMNS = COLUMN_HEADERS.length;
    private static final String FMT_DATE = "yyyy-MM-dd";
    
    private static final String CTXKEY_DATEFMT = "dateFormat";
    
    @Autowired
    private CouponCsvValidator validator;
    
    @Autowired
    private LocationService locationService;
    
    public CouponCsvReader() {
        super(COLUMN_HEADERS);
    }
    
    @Override
    protected final void init(final Map<String, Object> context, final List<CsvProcessingError> errors, final List<CsvProcessingError> warnings) {
        context.put(CTXKEY_RESULT, new TreeSet<Coupon>(new CouponComparator()));
        
        final SimpleDateFormat dateFmt = new SimpleDateFormat(FMT_DATE + " HH:mm:ss");
        dateFmt.setLenient(false);
        //dateFmt.setTimeZone((TimeZone) context.get(CTXKEY_CUSTOMER_TIME_ZONE));
        context.put(CTXKEY_DATEFMT, dateFmt);
        
        super.init(context, errors, warnings);
    }
    
    @Override
    protected final void initForCreate(final Map<String, Object> context) {
        final SimpleDateFormat dateFmt = new SimpleDateFormat(FMT_DATE);
        //dateFmt.setTimeZone((TimeZone) context.get(CTXKEY_CUSTOMER_TIME_ZONE));
        context.put(CTXKEY_DATEFMT, dateFmt);
        
        super.initForCreate(context);
    }
    
    @Override
    protected final void processLine(final List<CsvProcessingError> errors, final List<CsvProcessingError> warnings, final String line,
        final int lineNum, final Map<String, Object> context, final boolean foundErrors) {
        @SuppressWarnings("unchecked")
        final Set<Coupon> result = (Set<Coupon>) context.get(CTXKEY_RESULT);
        
        final String[] tokens = CsvParserUtils.split(line);
        if (lineNum == 1) {
            CsvParserUtils.verifyHeaders(errors, tokens, COLUMN_HEADERS, NUM_COLUMNS);
        } else {
            if (line.trim().length() > 0) {
                if (tokens.length < NUM_COLUMNS) {
                    errors.add(new CsvProcessingError(
                            this.validator.getMessage("error.common.csv.import.insufficient.column", this.validator.getMessage("label.coupon"))));
                } else {
                    final CouponCsvDTO dto = new CouponCsvDTO(tokens);
                    this.validator.validateRow(errors, warnings, dto, context);
                    if (errors.size() <= 0) {
                        final Coupon coupon = createCoupon(errors, warnings, dto, lineNum, context);
                        if (!result.add(coupon)) {
                            warnings.add(new CsvProcessingError(
                                    this.validator.getMessage("error.common.import.duplicated.record", this.validator.getMessage("label.coupon"),
                                                              this.validator.getMessage("label.coupon.number"), dto.getCouponCode())));
                        }
                    }
                }
            }
        }
    }
    
    private Coupon createCoupon(final List<CsvProcessingError> errors, final List<CsvProcessingError> warnings, final CouponCsvDTO dto,
        final int lineNumber, final Map<String, Object> context) {
        final Customer customer = (Customer) context.get(CTXKEY_CUSTOMER);
        final DateFormat dateFmt = (DateFormat) context.get(CTXKEY_DATEFMT);
        @SuppressWarnings("unchecked")
        final Map<String, Location> locationsMap = (Map<String, Location>) context.get(CTXKEY_LOCATION);
        
        final Coupon coupon = new Coupon();
        coupon.setCustomer(customer);
        // Coupon Code
        coupon.setCoupon(dto.getCouponCode().toUpperCase());
        
        // Description
        coupon.setDescription(dto.getDescription());
        
        // Discount
        if (dto.getPercentDiscount() != null) {
            coupon.setPercentDiscount(new Short(dto.getPercentDiscount()));
        }
        if (dto.getDollarDiscount() != null) {
            coupon.setDollarDiscountAmount((int) Math.round(new Double(dto.getDollarDiscount()) * 100));
        }
        
        if ((coupon.getPercentDiscount() > 0) && (coupon.getDollarDiscountAmount() > 0)) {
            errors.add(new CsvProcessingError(this.validator.getMessage("label.coupon.import.ambiguousDiscount")));
        }
        
        // Start Date
        if (dto.getStartDate() == null) {
            coupon.setStartDateLocal(WebCoreConstants.NO_START_DATE);
        } else {
            
            try {
                coupon.setStartDateLocal(dateFmt.parse(dto.getStartDate() + " 00:00:00"));
            } catch (ParseException pe) {
                errors.add(new CsvProcessingError(
                        this.validator.getMessage("error.common.invalid", this.validator.getMessage("label.coupon.startDate"))));
            }
            
        }
        
        // End Date
        if (dto.getEndDate() == null) {
            coupon.setEndDateLocal(WebCoreConstants.NO_END_DATE);
        } else {
            
            try {
                coupon.setEndDateLocal(dateFmt.parse(dto.getEndDate() + " 23:59:59"));
            } catch (ParseException pe) {
                errors.add(new CsvProcessingError(
                        this.validator.getMessage("error.common.invalid", this.validator.getMessage("label.coupon.endDate"))));
            }
        }
        
        // Swap the Dates if necessary
        if ((coupon.getStartDateLocal() != null) && (coupon.getEndDateLocal() != null)) {
            if (coupon.getStartDateLocal().after(coupon.getEndDateLocal())) {
                final Date startDate = coupon.getStartDateLocal();
                coupon.setStartDateLocal(coupon.getEndDateLocal());
                coupon.setEndDateLocal(startDate);
            }
        }
        
        // Num Uses
        if ((dto.getNumUses() == null) || WebCoreConstants.UNLIMITED.equalsIgnoreCase(dto.getNumUses())) {
            coupon.setNumberOfUsesRemaining(WebCoreConstants.COUPON_USES_UNLIMITED);
        } else {
            coupon.setNumberOfUsesRemaining(new Short(dto.getNumUses()));
        }
        
        // Daily Single Use
        String dailySingleUse = "false";
        if (dto.getDailySingleUse() != null) {
            dailySingleUse = dto.getDailySingleUse().toLowerCase();
        }
        
        if ("true".equals(dailySingleUse) || "yes".equals(dailySingleUse) || "on".equals(dailySingleUse)) {
            coupon.setValidForNumOfDay(1);
        } else {
            coupon.setValidForNumOfDay(0);
        }
        
        // Location
        if (dto.getLocationName() != null) {
            final String locSig = dto.getLocationName().toLowerCase();
            final Location loc = locationsMap.get(locSig);
            
            coupon.setLocation(loc);
            if (loc instanceof LocationWithLineNumbers) {
                ((LocationWithLineNumbers) loc).getLineNumbers().add(lineNumber);
            }
        }
        
        // PND/PBL
        String pnd = "false";
        if (dto.getPnd() != null) {
            pnd = dto.getPnd().toLowerCase();
        }
        
        if ("true".equals(pnd) || "yes".equals(pnd) || "on".equals(pnd)) {
            coupon.setIsPndEnabled(true);
        } else {
            coupon.setIsPndEnabled(false);
        }
        
        // PBS
        String pbs = "false";
        if (dto.getPbs() != null) {
            pbs = dto.getPbs().toLowerCase();
        }
        
        if ("true".equals(pbs) || "yes".equals(pbs) || "on".equals(pbs)) {
            coupon.setIsPbsEnabled(true);
        } else {
            coupon.setIsPbsEnabled(false);
        }
        
        // Stall Range
        if (coupon.isIsPbsEnabled()) {
            coupon.setSpaceRange(dto.getSpaceRange());
        } else {
            coupon.setSpaceRange(WebCoreConstants.EMPTY_STRING);
        }
        
        return coupon;
    }
    
    @Override
    //{ "CouponCode", "Description", "PercentDiscount", "DollarDiscount"
    // , "StartDate", "EndDate", "NumUses","DailySingleUse", "LocationName"
    // , "EnablePND/PBL", "EnablePayByStall", "StallRange" }
    protected final void createLine(final CsvWriter writer, final Coupon data, final int lineNumber, final Map<String, Object> context)
        throws IOException {
        final DateFormat dateFmt = (DateFormat) context.get(CTXKEY_DATEFMT);
        
        writer.writeColumn(data.getCoupon());
        writer.writeColumn(data.getDescription());
        if (data.getDollarDiscountAmount() != 0) {
            writer.writeColumn();
            writer.writeColumn(WebCoreUtil.formatBase100Value(data.getDollarDiscountAmount()));
        } else {
            writer.writeColumn(data.getPercentDiscount());
            writer.writeColumn();
        }
        
        writer.writeColumn(((data.getStartDateLocal() == null) || (WebCoreConstants.NO_START_DATE.equals(data.getStartDateLocal()))) ? ""
                : dateFmt.format(data.getStartDateLocal()));
        writer.writeColumn(((data.getEndDateLocal() == null) || (WebCoreConstants.NO_END_DATE.equals(data.getEndDateLocal()))) ? ""
                : dateFmt.format(data.getEndDateLocal()));
        if ((data.getNumberOfUsesRemaining() == null) || (data.getNumberOfUsesRemaining() == WebCoreConstants.COUPON_USES_UNLIMITED)) {
            writer.writeColumn(WebCoreConstants.UNLIMITED);
        } else {
            writer.writeColumn(data.getNumberOfUsesRemaining());
        }
        
        writer.writeColumn(data.getValidForNumOfDay() > 0);
        writer.writeColumn((data.getLocation() == null) ? "" : data.getLocation().getName());
        writer.writeColumn(data.isIsPndEnabled());
        if (data.isIsPbsEnabled()) {
            writer.writeColumn(true);
            writer.writeColumn(data.getSpaceRange());
        } else {
            writer.writeColumn(false);
            writer.writeColumn();
        }
    }
}
