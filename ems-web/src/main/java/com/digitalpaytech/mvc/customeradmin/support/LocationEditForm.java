package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Transient;

import com.digitalpaytech.dto.WidgetMapInfo;
import com.digitalpaytech.mvc.support.RateProfileLocationForm;
import com.digitalpaytech.util.WebCoreConstants;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

public class LocationEditForm implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 9015339862977419688L;
    private Boolean isParent;
    private Boolean isUnassigned;
    private String randomId;
    private String name;
    private String description;
    private String parentLocation;
    private String parentLocationId;
    private Integer permitIssueTypeId;
    private String capacity;
    private String targetMonthlyRevenue;
    
    private WidgetMapInfo mapInfo;
    
    @XStreamImplicit(itemFieldName = "payStations")
    private List<LocationFormEntry> payStations;
    @XStreamImplicit(itemFieldName = "childLocations")
    private List<LocationFormEntry> childLocations;
    @XStreamImplicit(itemFieldName = "locationOpen")
    private List<Integer> locationOpen;
    @XStreamImplicit(itemFieldName = "locationClose")
    private List<Integer> locationClose;
    @XStreamImplicit(itemFieldName = "openEntireDay")
    private List<Boolean> openEntireDay;
    @XStreamImplicit(itemFieldName = "closedEntireDay")
    private List<Boolean> closedEntireDay;
    @XStreamImplicit(itemFieldName = "facilities")
    private List<LocationFormFacilityEntry> facilities;
    @XStreamImplicit(itemFieldName = "properties")
    private List<LocationFormPropertyEntry> properties;
    @XStreamImplicit(itemFieldName = "lots")
    private List<LocationFormLotEntry> lots;
    
    @XStreamImplicit(itemFieldName = "rateProfilesList")
    private List<RateProfileLocationForm> rateProfilesList;
    
    @XStreamOmitField
    private List<String> selectedPayStationRandomIds;
    @XStreamOmitField
    private List<String> selectedFacilityRandomIds;
    @XStreamOmitField
    private List<String> selectedPropertyRandomIds;
    @XStreamOmitField
    private List<String> selectedLotRandomIds;
    @XStreamOmitField
    private List<String> selectedChildLocationRandomIds;
    
    @Transient
    private transient int pointOfSaleId;
    
    @Transient
    private transient int locationId;
    
    //TODO fix naming and reflect to UI
    @Transient
    private transient int parentLocationRealId;
    
    @Transient
    private transient List<Integer> childLocationIdList;
    @Transient
    private transient List<Integer> pointOfSaleIdList;
    @Transient
    private transient List<Integer> selectedFacilityIdList;
    @Transient
    private transient List<Integer> selectedPropertyIdList;
    @Transient
    private transient List<Integer> selectedLotIdList;
    
    public LocationEditForm() {
        this.targetMonthlyRevenue = "0";
        this.isParent = false;
        this.isUnassigned = false;
        this.payStations = new ArrayList<LocationFormEntry>();
        this.childLocations = new ArrayList<LocationFormEntry>();
        this.facilities = new ArrayList<LocationFormFacilityEntry>();
        this.properties = new ArrayList<LocationFormPropertyEntry>();
        this.lots = new ArrayList<LocationFormLotEntry>();
        this.name = "";
        this.description = "";
    }
    
    public final List<Integer> getLocationOpen() {
        return this.locationOpen;
    }
    
    public final void setLocationOpen(final List<Integer> locationOpen) {
        this.locationOpen = locationOpen;
    }
    
    public final List<Integer> getLocationClose() {
        return this.locationClose;
    }
    
    public final void setLocationClose(final List<Integer> locationClose) {
        this.locationClose = locationClose;
    }
    
    public final List<Boolean> getOpenEntireDay() {
        return this.openEntireDay;
    }
    
    public final void setOpenEntireDay(final List<Boolean> openEntireDay) {
        this.openEntireDay = openEntireDay;
    }
    
    public final List<RateProfileLocationForm> getRateProfilesList() {
        return this.rateProfilesList;
    }
    
    public final void setRateProfilesList(final List<RateProfileLocationForm> rateProfilesList) {
        this.rateProfilesList = rateProfilesList;
    }
    
    public final List<Boolean> getClosedEntireDay() {
        return this.closedEntireDay;
    }
    
    public final void setClosedEntireDay(final List<Boolean> closedEntireDay) {
        this.closedEntireDay = closedEntireDay;
    }
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final boolean getIsParent() {
        return this.isParent;
    }
    
    public final boolean isIsParent() {
        return this.isParent;
    }
    
    public final boolean isParent() {
        return this.isParent;
    }
    
    public final void setIsParent(final boolean isParent) {
        this.isParent = isParent;
    }
    
    public final boolean getIsUnassigned() {
        return this.isUnassigned;
    }
    
    public final boolean isIsUnassigned() {
        return this.isUnassigned;
    }
    
    public final boolean isUnassigned() {
        return this.isUnassigned;
    }
    
    public final void setIsUnassigned(final boolean isUnassigned) {
        this.isUnassigned = isUnassigned;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    public final String getDescription() {
        return this.description;
    }
    
    public final void setDescription(final String description) {
        this.description = description;
    }
    
    public final String getParentLocation() {
        return this.parentLocation;
    }
    
    public final void setParentLocation(final String parentLocation) {
        this.parentLocation = parentLocation;
    }
    
    public final String getParentLocationId() {
        return this.parentLocationId;
    }
    
    public final void setParentLocationId(final String parentLocationId) {
        this.parentLocationId = parentLocationId;
    }
    
    public final Integer getPermitIssueTypeId() {
        return this.permitIssueTypeId;
    }
    
    public final void setPermitIssueTypeId(final Integer permitIssueTypeId) {
        this.permitIssueTypeId = permitIssueTypeId;
    }
    
    public final String getCapacity() {
        return this.capacity;
    }
    
    public final void setCapacity(final String capacity) {
        this.capacity = capacity;
    }
    
    public final void setTargetMonthlyRevenue(final String targetMonthlyRevenue) {
        this.targetMonthlyRevenue = targetMonthlyRevenue;
    }
    
    public final String getTargetMonthlyRevenue() {
        return this.targetMonthlyRevenue;
        //.multiply(new BigDecimal(100)).setScale(0).intValue();
    }
    
    public final WidgetMapInfo getMapInfo() {
        return this.mapInfo;
    }
    
    public final void setMapInfo(final WidgetMapInfo mapInfo) {
        this.mapInfo = mapInfo;
    }
    
    public final List<LocationFormEntry> getPayStations() {
        return this.payStations;
    }
    
    public final void setPayStations(final List<LocationFormEntry> payStations) {
        this.payStations = payStations;
    }
    
    public final void addPayStation(final String name, final String randomId, final String status, final Integer payStationType,
        final Integer severity) {
        final LocationFormEntry entry = new LocationFormEntry();
        entry.setName(name);
        entry.setRandomId(randomId);
        entry.setStatus(status);
        entry.setPayStationType(payStationType);
        entry.setSeverity(severity);
        this.payStations.add(entry);
    }
    
    public final void addPayStation(final String name, final String serialNumber, final String randomId, final String status,
        final Integer payStationType, final Integer severity) {
        final LocationFormEntry entry = new LocationFormEntry();
        entry.setName(name);
        entry.setSerialNumber(serialNumber);
        entry.setRandomId(randomId);
        entry.setStatus(status);
        entry.setPayStationType(payStationType);
        entry.setSeverity(severity);
        this.payStations.add(entry);
    }
    
    public final void addFacility(final String code, final String description, final String randomId, final String status) {
        final LocationFormFacilityEntry entry = new LocationFormFacilityEntry();
        entry.setCode(code);
        entry.setDescription(description);
        entry.setRandomId(randomId);
        entry.setStatus(status);
        this.facilities.add(entry);
    }
    
    public final void addProperty(final int proUid, final String proName, final String randomId, final String status) {
        final LocationFormPropertyEntry entry = new LocationFormPropertyEntry();
        entry.setUid(Integer.toString(proUid));
        entry.setName(proName);
        entry.setRandomId(randomId);
        entry.setStatus(status);
        this.properties.add(entry);
    }
    
    public final void addLot(final String lotName, final String randomId, final String status) {
        final LocationFormLotEntry entry = new LocationFormLotEntry();
        entry.setName(lotName);
        entry.setRandomId(randomId);
        entry.setStatus(status);
        this.lots.add(entry);
    }
    
    public final List<LocationFormEntry> getChildLocations() {
        return this.childLocations;
    }
    
    public final void setChildLocations(final List<LocationFormEntry> childLocations) {
        this.childLocations = childLocations;
    }
    
    public final void addChildLocation(final String name, final String randomId, final String status) {
        final LocationFormEntry entry = new LocationFormEntry();
        entry.setName(name);
        entry.setRandomId(randomId);
        entry.setStatus(status);
        this.childLocations.add(entry);
    }
    
    public final List<String> getSelectedPayStationRandomIds() {
        return this.selectedPayStationRandomIds;
    }
    
    public final void setSelectedPayStationRandomIds(final List<String> selectedPayStationRandomIds) {
        this.selectedPayStationRandomIds = selectedPayStationRandomIds;
    }
    
    public final List<String> getSelectedChildLocationRandomIds() {
        return this.selectedChildLocationRandomIds;
    }
    
    public final void setSelectedChildLocationRandomIds(final List<String> selectedChildLocationRandomIds) {
        this.selectedChildLocationRandomIds = selectedChildLocationRandomIds;
    }
    
    public final int getPointOfSaleId() {
        return this.pointOfSaleId;
    }
    
    public final void setPointOfSaleId(final int pointOfSaleId) {
        this.pointOfSaleId = pointOfSaleId;
    }
    
    public final int getLocationId() {
        return this.locationId;
    }
    
    public final void setLocationId(final int locationId) {
        this.locationId = locationId;
    }
    
    public final List<Integer> getChildLocationIdList() {
        return this.childLocationIdList;
    }
    
    public final void setChildLocationIdList(final List<Integer> childLocationIdList) {
        this.childLocationIdList = childLocationIdList;
    }
    
    public final List<Integer> getPointOfSaleIdList() {
        return this.pointOfSaleIdList;
    }
    
    public final void setPointOfSaleIdList(final List<Integer> pointOfSaleIdList) {
        this.pointOfSaleIdList = pointOfSaleIdList;
    }
    
    public final int getParentLocationRealId() {
        return this.parentLocationRealId;
    }
    
    public final void setParentLocationRealId(final int parentLocationRealId) {
        this.parentLocationRealId = parentLocationRealId;
    }
    
    public final List<String> getSelectedFacilityRandomIds() {
        return this.selectedFacilityRandomIds;
    }
    
    public final void setSelectedFacilityRandomIds(final List<String> selectedFacilityRandomIds) {
        this.selectedFacilityRandomIds = selectedFacilityRandomIds;
    }
    
    public final List<String> getSelectedPropertyRandomIds() {
        return this.selectedPropertyRandomIds;
    }
    
    public final void setSelectedPropertyRandomIds(final List<String> selectedPropertyRandomIds) {
        this.selectedPropertyRandomIds = selectedPropertyRandomIds;
    }
    
    public final List<String> getSelectedLotRandomIds() {
        return this.selectedLotRandomIds;
    }
    
    public final void setSelectedLotRandomIds(final List<String> selectedLotRandomIds) {
        this.selectedLotRandomIds = selectedLotRandomIds;
    }
    
    public final List<Integer> getSelectedFacilityIdList() {
        return this.selectedFacilityIdList;
    }
    
    public final List<Integer> getSelectedPropertyIdList() {
        return this.selectedPropertyIdList;
    }
    
    public final List<Integer> getSelectedLotIdList() {
        return this.selectedLotIdList;
    }
    
    public final void setSelectedFacilityIdList(final List<Integer> selectedFacilityIdList) {
        this.selectedFacilityIdList = selectedFacilityIdList;
    }
    
    public final void setSelectedPropertyIdList(final List<Integer> selectedPropertyIdList) {
        this.selectedPropertyIdList = selectedPropertyIdList;
    }
    
    public final void setSelectedLotIdList(final List<Integer> selectedLotIdList) {
        this.selectedLotIdList = selectedLotIdList;
    }
    
    public static class LocationFormEntry implements Serializable {
        
        private static final long serialVersionUID = 8433589385993586412L;
        private String name;
        private String serialNumber;
        private String randomId;
        private String status;
        private Integer payStationType;
        private Integer severity;
        
        LocationFormEntry() {
        }
        
        public final String getName() {
            return this.name;
        }
        
        public final void setName(final String name) {
            this.name = name;
        }
        
        public final String getSerialNumber() {
            return this.serialNumber;
        }
        
        public final void setSerialNumber(final String serialNumber) {
            this.serialNumber = serialNumber;
        }
        
        public final String getRandomId() {
            return this.randomId;
        }
        
        public final void setRandomId(final String randomId) {
            this.randomId = randomId;
        }
        
        public final String getStatus() {
            return this.status;
        }
        
        public final void setStatus(final String status) {
            this.status = status;
        }
        
        public final Integer getPayStationType() {
            return this.payStationType;
        }
        
        public final void setPayStationType(final Integer payStationType) {
            this.payStationType = payStationType;
        }
        
        public final Integer getSeverity() {
            return this.severity;
        }
        
        public final void setSeverity(final Integer severity) {
            this.severity = severity;
        }
    }
    
    public static class LocationFormFacilityEntry implements Serializable {
        
        private static final long serialVersionUID = 8433589385993586412L;
        private String code;
        private String description;
        private String randomId;
        private String status;
        
        LocationFormFacilityEntry() {
        }
        
        public final String getRandomId() {
            return this.randomId;
        }
        
        public final void setRandomId(final String randomId) {
            this.randomId = randomId;
        }
        
        public final String getStatus() {
            return this.status;
        }
        
        public final void setStatus(final String status) {
            this.status = status;
        }
        
        public final String getCode() {
            return this.code;
        }
        
        public final void setCode(final String code) {
            this.code = code;
        }
        
        public final String getDescription() {
            return this.description;
        }
        
        public final void setDescription(final String description) {
            this.description = description;
        }
    }
    
    public static class LocationFormPropertyEntry implements Serializable {
        
        private static final long serialVersionUID = 8433589385993586412L;
        private String uid;
        private String name;
        private String randomId;
        private String status;
        
        LocationFormPropertyEntry() {
        }
        
        public final String getRandomId() {
            return this.randomId;
        }
        
        public final void setRandomId(final String randomId) {
            this.randomId = randomId;
        }
        
        public final String getStatus() {
            return this.status;
        }
        
        public final void setStatus(final String status) {
            this.status = status;
        }
        
        public final String getUid() {
            return this.uid;
        }
        
        public final void setUid(final String uid) {
            this.uid = uid;
        }
        
        public final String getName() {
            return this.name;
        }
        
        public final void setName(final String name) {
            this.name = name;
        }
    }
    
    public static class LocationFormLotEntry implements Serializable {
        
        private static final long serialVersionUID = -1914499772797768093L;
        private String name;
        private String randomId;
        private String status;
        
        LocationFormLotEntry() {
        }
        
        public final String getRandomId() {
            return this.randomId;
        }
        
        public final void setRandomId(final String randomId) {
            this.randomId = randomId;
        }
        
        public final String getStatus() {
            return this.status;
        }
        
        public final void setStatus(final String status) {
            this.status = status;
        }
        
        public final String getName() {
            return this.name;
        }
        
        public final void setName(final String name) {
            this.name = name;
        }
    }
    
}
