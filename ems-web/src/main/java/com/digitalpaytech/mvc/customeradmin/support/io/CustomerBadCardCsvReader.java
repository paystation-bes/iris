package com.digitalpaytech.mvc.customeradmin.support.io;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerBadCard;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.dto.comparator.CustomerBadCardComparator;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.mvc.customeradmin.support.io.dto.CustomerBadCardCsvDTO;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.crypto.CryptoAlgorithmFactory;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CustomerCardTypeWithLineNumbers;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.crypto.CryptoConstants;
import com.digitalpaytech.util.csv.CsvProcessingError;
import com.digitalpaytech.util.csv.CsvProcessor;
import com.digitalpaytech.util.csv.CsvParserUtils;
import com.digitalpaytech.util.csv.CsvWriter;
import com.digitalpaytech.validation.customeradmin.CardManagementCsvValidator;

@Component("customerBadCardCsvReader")
public class CustomerBadCardCsvReader extends CsvProcessor<CustomerBadCard> {
    public static final String CTXKEY_LOCKED_CARD_TYPES = "lockedCardTypes";
    public static final String CTXKEY_CARD_TYPES = "cardTypes";

    private static final String[] COLUMN_HEADERS = { "CardType", "CardNumber", "InternalKey", "ExpiryDate", "Comment" };
    private static final int NUM_COLUMNS = COLUMN_HEADERS.length;
    private static final int FOUR_DIGITS = 4;
    
    @Autowired
    private CustomerCardTypeService customerCardTypeService;
    
    @Autowired
    private CryptoService cryptoService;
    
    @Autowired
    @Qualifier("cardManagementCsvValidator")
    private CardManagementCsvValidator validator;
    
    @Autowired
    private CryptoAlgorithmFactory cryptoAlgorithmFactory;
    
    public void setCustomerCardTypeService(CustomerCardTypeService customerCardTypeService) {
        this.customerCardTypeService = customerCardTypeService;
    }
    
    public void setCryptoService(CryptoService cryptoService) {
        this.cryptoService = cryptoService;
    }
    
    public CustomerBadCardCsvReader() {
        super(COLUMN_HEADERS);
    }
    
    public void setCryptoAlgorithmFactory(CryptoAlgorithmFactory cryptoAlgorithmFactory) {
        this.cryptoAlgorithmFactory = cryptoAlgorithmFactory;
    }
    
    /**
     * Extract the list of bad credit cards from the csv file.
     * 
     * @param multipartFile
     * @param customerId
     * @return
     * @throws InvalidCSVDataException
     * @throws CryptoException
     */
    /*
     * public Collection<CustomerBadCard> extractCustomerBadCards(
     * MultipartFile multipartFile, Integer customerId)
     * throws InvalidCSVDataException, CryptoException {
     * ArrayList<CustomerBadCard> bannedCards = new ArrayList<CustomerBadCard>();
     * processFile(multipartFile, new Object[] { customerId, bannedCards });
     * return bannedCards;
     * }
     */
    
    @Override
    protected void init(Map<String, Object> context, List<CsvProcessingError> errors, List<CsvProcessingError> warnings) {
    	Customer customer = (Customer) context.get(CTXKEY_CUSTOMER);
    	
    	context.put(CTXKEY_RESULT, new TreeSet<CustomerBadCard>(new CustomerBadCardComparator()));
    	
    	@SuppressWarnings("unchecked")
		Map<String, CustomerCardType> cardTypesHash = (Map<String, CustomerCardType>) context.get(CTXKEY_CARD_TYPES);
        
        List<CustomerCardType> cardTypesList = this.customerCardTypeService.getDetachedBannableCardTypes(customer.getId());
        for (CustomerCardType cardType : cardTypesList) {
            cardTypesHash.put(cardType.getName().toLowerCase(), cardType);
        }
    	
        super.init(context, errors, warnings);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    protected void processLine(List<CsvProcessingError> errors, List<CsvProcessingError> warnings, String line, int lineNum, Map<String, Object> context, boolean foundErrors) {
    	TreeSet<CustomerBadCard> result = (TreeSet<CustomerBadCard>) context.get(CTXKEY_RESULT);
		
        String[] tokens = CsvParserUtils.split(line);
        if (lineNum == 1) {
            CsvParserUtils.verifyHeaders(errors, tokens, COLUMN_HEADERS, NUM_COLUMNS);
        } else {
            if (line.trim().length() > 0) {
            	if(tokens.length < NUM_COLUMNS) {
            		errors.add(new CsvProcessingError(validator.getMessage("error.common.csv.import.insufficient.column",
                            validator.getMessage("label.cardManagement.bannedCard"))));
            	}
            	else {
            		CustomerBadCardCsvDTO dto = new CustomerBadCardCsvDTO(tokens);
                    validator.validateRow(errors, warnings, dto, context);
                    if (errors.size() <= 0) {
                        CustomerBadCard badCC = createCustomerBadCard(errors, warnings, dto, context, lineNum);
                        if (errors.size() <= 0) {
                        	if(!result.add(badCC)) {
                        		warnings.add(new CsvProcessingError(validator.getMessage("error.common.import.duplicated.record"
                						, validator.getMessage("label.cardManagement.bannedCard")
                						, validator.getMessage("label.connector.and"
                								, validator.getMessage("label.settings.cardSettings.CardType")
                								, validator.getMessage("label.settings.cardSettings.CardNumber"))
                						, validator.getMessage("label.connector.and"
                								, badCC.getCustomerCardType().getName()
                								, createMaskForCreditCard(badCC)))));
                        	}
                        }
                    }
            	}
            }
        }
    }
    
    private CustomerBadCard createCustomerBadCard(List<CsvProcessingError> errors, List<CsvProcessingError> warnings, CustomerBadCardCsvDTO dto, Map<String, ?> context, int lineNumber) {
    	@SuppressWarnings("unchecked")
		Map<String, CustomerCardType> cardTypesMap = (Map<String, CustomerCardType>) context.get(CTXKEY_CARD_TYPES);
    	
        CustomerBadCard customerBadCard = new CustomerBadCard();
        
        customerBadCard.setSource(WebCoreConstants.BADCARD_SOURCE_IMPORT);
        
        CustomerCardType cardType = cardTypesMap.get(dto.getCardType().toLowerCase());
        if(cardType instanceof CustomerCardTypeWithLineNumbers) {
        	CustomerCardTypeWithLineNumbers cctNums = (CustomerCardTypeWithLineNumbers) cardType;
        	cctNums.getLineNumbers().add(lineNumber);
        }
        
        customerBadCard.setCustomerCardType(cardType);
        
        Integer cardTypeId = (customerBadCard.getCustomerCardType().getCardType() == null) ? null : customerBadCard.getCustomerCardType().getCardType().getId();
        if ((cardTypeId == null) || (cardTypeId != CardProcessingConstants.CARD_TYPE_CREDIT_CARD)) {
            customerBadCard.setCardNumberOrHash(dto.getCardNumber());
            if (dto.getCardNumber().length() > FOUR_DIGITS) {
                customerBadCard.setLast4digitsOfCardNumber(new Short(dto.getCardNumber().substring(dto.getCardNumber().length() - FOUR_DIGITS)));
            } else {
                customerBadCard.setLast4digitsOfCardNumber(new Short(dto.getCardNumber()));
            }
        } else {
        	try {
	            if (dto.getCardNumber().toLowerCase().contains("x")) {
	            	errors.add(new CsvProcessingError(validator.getMessage("error.bannedcard.import.require.internalKey")));
	            	if(dto.getInternalKey() == null) {
	            		errors.add(new CsvProcessingError(validator.getMessage("error.bannedcard.import.require.internalKey")));
	            	}
	            	else {
	            		customerBadCard.setCardNumberOrHash(dto.getInternalKey());
	            		customerBadCard.setCardData("existing:" + cryptoService.encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL,
	                    		dto.getCardNumber() + "=" + dto.getExpiryDate()));
	            	}
	            } else {
	                if (!customerCardTypeService.isCreditCardData(dto.getCardNumber())) {
	                    errors.add(new CsvProcessingError(validator.getMessage("error.common.invalid",
	                                                                           validator.getMessage("label.settings.cardSettings.CardNumber"))));
	                } else {
	                    customerBadCard.setCardNumberOrHash(this.cryptoAlgorithmFactory.getSha1Hash(dto.getCardNumber(), CryptoConstants.HASH_BAD_CREDIT_CARD));
	                    customerBadCard.setCardData(cryptoService.encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL,
	                    		dto.getCardNumber() + "=" + dto.getExpiryDate()));
	                }
	            }
        	}
        	catch(CryptoException ce) {
        		errors.add(new CsvProcessingError(validator.getMessage("error.common.encryption.fail", validator.getMessage("label.settings.cardSettings.CardNumber"))));
        	}
            
            customerBadCard.setLast4digitsOfCardNumber(new Short(dto.getCardNumber().substring(dto.getCardNumber().length() - 4)));
            customerBadCard.setCardExpiry(new Short(dto.getExpiryDate().substring(2) + dto.getExpiryDate().substring(0, 2)));
        }
        
        customerBadCard.setComment(dto.getComment());
        
        return customerBadCard;
    }
    
	@Override
	protected void createLine(CsvWriter writer, CustomerBadCard data,
			int lineNumber, Map<String, Object> context) throws IOException {
		CustomerCardType cct = data.getCustomerCardType();
		writer.writeColumn(cct.getName());
		if(cct.getCardType().getId() == CardProcessingConstants.CARD_TYPE_CREDIT_CARD) {
			writer.writeColumn(createMaskForCreditCard(data));
			writer.writeColumn(data.getCardNumberOrHash());
        } else {
        	writer.writeColumn(data.getCardNumberOrHash());
        	writer.writeColumn();
        }
		
		String expiry = (data.getCardExpiry() == null) ? null : data.getCardExpiry().toString();
		writer.writeColumn((data.getCardExpiry() == null) ? "" : expiry.substring(2).concat(expiry.substring(0, 2)));
		
		writer.writeColumn(data.getComment());
	}
    
    private String createMaskForCreditCard(CustomerBadCard badCard) {
    	StringBuilder bdr = new StringBuilder();
    	if (WebCoreUtil.checkIfAllDigits(badCard.getCardNumberOrHash())) {
    		bdr.append(badCard.getCardNumberOrHash());
    	} else {
    		bdr.append("XXXXXXXXXXXX");
        	if (badCard.getLast4digitsOfCardNumber() != null) {
        		bdr.append(badCard.getLast4digitsOfCardNumber());
        	}    		
    	}
    	return bdr.toString();
    }
}
