package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class MobileDeviceEditForm implements Serializable{
    
    private static final long serialVersionUID = 348369803016743841L;
    private String randomId;
    private String description;
    private String name;
    private boolean isLocked;
    private List<String> releasedApps;
    private Long timeStamp;
    
    public MobileDeviceEditForm(){
        description = "";
        name = "";
        isLocked = false;
        releasedApps = new LinkedList<String>();
    }
    public MobileDeviceEditForm(String description, String name, boolean isLocked, List<String> releasedApps){
        this.description = description;
        this.name = name;
        this.isLocked = isLocked;
        this.releasedApps = releasedApps;
    }
    
    public String getRandomId(){
        return randomId;
    }
    public void setRandomId(String randomId){
        this.randomId = randomId;
    }
    public String getDescription(){
        return description;
    }
    public void setDescription(String description){
        this.description = description;
    }
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public boolean getIsLocked(){
        return isLocked;
    }
    public void setIsLocked(boolean isLocked){
        this.isLocked = isLocked;
    }
    public List<String> getReleasedApps(){
        return releasedApps;
    }
    public void setReleasedApps(List<String> releasedApps){
        this.releasedApps = releasedApps;
    }
    public Long getTimeStamp(){
        return timeStamp;
    }
    public void setTimeStamp(Long timeStamp){
        this.timeStamp = timeStamp;
    }
}
