package com.digitalpaytech.mvc.customeradmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.mvc.PayStationPlacementController;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
public class CustomerAdminPayStationPlacementController extends PayStationPlacementController {
    
    /**
     * This method creates a list of Id's of routes and locations which have pay
     * stations for the current user and puts them into the model as
     * "filterValues" to be used in the UI to filter pay stations
     * 
     * @param request
     * @param response
     * @param model
     * @return List of locations and routes stored in the model as attribute
     *         "filterValues" used for filtering pay stations. Target vm file
     *         URI which is mapped to Settings->payStationPlacement form
     *         (/settings/locations/payStationPlacement.vm)
     */
    @RequestMapping(value = "/secure/settings/locations/payStationPlacement.html")
    public String placedPayStations(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATION_PLACEMENT)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD) && 
                    !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) { 
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }            
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        return super.placedPayStations(request, response, model, "/settings/locations/payStationPlacement");
    }
    
    /**
     * This method takes an optional location or route Id and creates a list of
     * pay station details. These details contain map information (longitude and
     * latitude).
     * 
     * @param request
     * @param response
     * @param model
     * @return Pay station map information WidgetMapInfo object converted into
     *         JSON
     */
    @RequestMapping(value = "/secure/settings/locations/payStationPlacement/payStations.html")
    public @ResponseBody
    String payStations(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)
                && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATION_PLACEMENT)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.payStations(request, response, model);
    }
    
    /**
     * This method takes a pay station Id as input as well as longitude and
     * latitude. It then updates the pay station in the database to these new
     * coordinates if they pass simple longitude and latitude math validations
     * 
     * @param request
     * @param response
     * @param model
     * @return "true" string value if process succeeds, "false" is process fails
     */
    @RequestMapping(value = "/secure/settings/locations/payStationPlacement/pinPayStation.html")
    public @ResponseBody
    String pinPayStation(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATION_PLACEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.pinPayStation(request, response, model);
    }
    
}
