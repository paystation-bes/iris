package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;

import com.digitalpaytech.util.PaystationConstants;

public class GlobalPreferencesEditForm implements Serializable {
    private static final long serialVersionUID = -3257383378256854009L;
    private String currentTimeZone;
    private Integer querySpacesBy;
    
    public GlobalPreferencesEditForm() {
        this.currentTimeZone = "";
        this.querySpacesBy = PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME;
    }
    
    public GlobalPreferencesEditForm(final String currentTimeZone, final Integer querySpacesBy) {
        super();
        this.currentTimeZone = currentTimeZone;
        this.querySpacesBy = querySpacesBy;
    }
    
    public final String getCurrentTimeZone() {
        return this.currentTimeZone;
    }
    
    public final void setCurrentTimeZone(final String currentTimeZone) {
        this.currentTimeZone = currentTimeZone;
    }
    
    public final Integer getQuerySpacesBy() {
        return this.querySpacesBy;
    }
    
    public final void setQuerySpacesBy(final Integer querySpacesBy) {
        this.querySpacesBy = querySpacesBy;
    }
}
