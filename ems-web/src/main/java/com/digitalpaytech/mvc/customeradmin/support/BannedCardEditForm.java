package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;
import java.util.Date;

import com.digitalpaytech.annotation.StoryAlias;

/**
 * This form handles a user input form for banned card.
 * 
 * @author Brian Kim
 *
 */
@StoryAlias("banned card form")
public class BannedCardEditForm implements Serializable {
	public static final String ALIAS_CARD_EXPIRY = "card expiry";
    public static final String ALIAS_CARD_NUMBER = "card number";
    /**
	 * 
	 */
	private static final long serialVersionUID = 8103479984960295472L;
    private static final String ALIAS_CARD_TYPE_ID = "card type id";
	private String randomCustomerBadCardId;
	private Integer customerBadCardId;
	private String cardNumber;
	private String cardType;
	private Integer cardTypeId;
	private String cardExpiry;
	private String comment;
	private Date dateAdded;
	private Date lastModified;

	public String getRandomCustomerBadCardId() {
		return randomCustomerBadCardId;
	}

	public void setRandomCustomerBadCardId(String randomCustomerBadCardId) {
		this.randomCustomerBadCardId = randomCustomerBadCardId;
	}

	public Integer getCustomerBadCardId() {
		return customerBadCardId;
	}

	public void setCustomerBadCardId(Integer customerBadCardId) {
		this.customerBadCardId = customerBadCardId;
	}

	@StoryAlias(ALIAS_CARD_NUMBER)
	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	@StoryAlias("card type")
	public String getCardType() {
		return cardType;
	}
	
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	@StoryAlias(ALIAS_CARD_TYPE_ID)
	public Integer getCardTypeId() {
		return cardTypeId;
	}

	public void setCardTypeId(Integer cardTypeId) {
		this.cardTypeId = cardTypeId;
	}

	@StoryAlias(ALIAS_CARD_EXPIRY)
	public String getCardExpiry() {
		return cardExpiry;
	}

	public void setCardExpiry(String cardExpiry) {
		this.cardExpiry = cardExpiry;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getDateAdded() {
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}
}
