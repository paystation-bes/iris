package com.digitalpaytech.mvc.customeradmin;

import java.util.ArrayList;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.MapEntry;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
public class MapController {
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @RequestMapping(value = "/secure/map/payStationDetail.html")
    @ResponseBody
    public String getMapWidgetDetail(final HttpServletRequest request, final HttpServletResponse response) {
        MapEntry entry = null;
        
        final String randomId = request.getParameter("psID");
        final Integer id = SessionTool.getInstance(request).getKeyMapping().getKey(randomId, PointOfSale.class, Integer.class);
        if (id != null) {
            ArrayList<Integer> ids = new ArrayList<Integer>(1);
            ids.add(id);
            
            Map<Integer, MapEntry> buffer = this.pointOfSaleService.findPointOfSaleMapEntryDetails(ids,
                                                                                                   TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone()));
            if (buffer.size() > 0) {
                entry = buffer.get(id);
            }
        }
        
        if (entry == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return WidgetMetricsHelper.convertToJson(entry, "mapEntry", true);
    }
}
