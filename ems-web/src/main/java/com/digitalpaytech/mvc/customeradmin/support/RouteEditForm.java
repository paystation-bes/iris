package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RouteEditForm implements Serializable {

	private static final long serialVersionUID = -8958097781826824879L;
	private String randomizedRouteId;
	private Integer routeId;
	private String routeName;
	private String randomizedRouteTypeId;
	private Integer routeTypeId;
	private List<String> randomizedPointOfSaleIds;
	private List<Integer> pointOfSaleIds;

	public RouteEditForm() {
		randomizedPointOfSaleIds = new ArrayList<String>();
		pointOfSaleIds = new ArrayList<Integer>();
	}
	
	public String getRandomizedRouteId() {
		return randomizedRouteId;
	}

	public void setRandomizedRouteId(String randomizedRouteId) {
		this.randomizedRouteId = randomizedRouteId;
	}

	public Integer getRouteId() {
		return routeId;
	}

	public void setRouteId(Integer routeId) {
		this.routeId = routeId;
	}

	public String getRouteName() {
		return routeName;
	}

	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}

	public String getRandomizedRouteTypeId() {
		return randomizedRouteTypeId;
	}

	public void setRandomizedRouteTypeId(String randomizedRouteTypeId) {
		this.randomizedRouteTypeId = randomizedRouteTypeId;
	}

	public Integer getRouteTypeId() {
		return routeTypeId;
	}

	public void setRouteTypeId(Integer routeTypeId) {
		this.routeTypeId = routeTypeId;
	}

	public List<String> getRandomizedPointOfSaleIds() {
		return randomizedPointOfSaleIds;
	}

	public void setRandomizedPointOfSaleIds(
			List<String> randomizedPointOfSaleIds) {
		this.randomizedPointOfSaleIds = randomizedPointOfSaleIds;
	}

	public List<Integer> getPointOfSaleIds() {
		return pointOfSaleIds;
	}

	public void setPointOfSaleIds(List<Integer> pointOfSaleIds) {
		this.pointOfSaleIds = pointOfSaleIds;
	}
}
