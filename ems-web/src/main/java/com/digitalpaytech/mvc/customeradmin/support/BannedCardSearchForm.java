package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;

import com.digitalpaytech.constants.SortOrder;
import com.digitalpaytech.domain.CustomerCardType;

/**
 * This form handles a user input form for banned card.
 * 
 * @author Brian Kim
 *
 */
public class BannedCardSearchForm implements Serializable {
    private static final String CARD_NUMBER_OR_HASH_AS_INT = "cardNumberOrHashAsInt";
    public enum SortColumn {
        CARD_NUMBER(CARD_NUMBER_OR_HASH_AS_INT),
        CARD_TYPE("cct.name", CARD_NUMBER_OR_HASH_AS_INT),
        CARD_EXPIRY("cardExpiry", CARD_NUMBER_OR_HASH_AS_INT),
        ADDED_DATE("addedGmt", CARD_NUMBER_OR_HASH_AS_INT),
        SOURCE("source", CARD_NUMBER_OR_HASH_AS_INT);
        
        private String[] actualAttributes;
        
        SortColumn(final String... actualAttributes) {
            this.actualAttributes = actualAttributes;
        }
        public String[] getActualAttributes() {
            return this.actualAttributes;
        }
    }
    private static final long serialVersionUID = 413205830616011809L;
    private String filterCardNumber;
    // this is the Random id
    private String filterCardTypeId; 
    private Integer cardTypeId;
    private Integer customerCardTypeId;
    private Integer page;
    private Integer itemsPerPage;
    private SortColumn column;
    private SortOrder order;
    private String dataKey;
    private CustomerCardType customerCardType;
    
    public BannedCardSearchForm() {
    }

    public final String getFilterCardNumber() {
        return this.filterCardNumber;
    }
    public final void setFilterCardNumber(final String filterCardNumber) {
        this.filterCardNumber = filterCardNumber;
    }
    public final String getFilterCardTypeId() {
        return this.filterCardTypeId;
    }
    public final void setFilterCardTypeId(final String filterCardTypeId) {
        this.filterCardTypeId = filterCardTypeId;
    }
    public final Integer getCardTypeId() {
        return this.cardTypeId;
    }
    public final void setCardTypeId(final Integer cardTypeId) {
        this.cardTypeId = cardTypeId;
    }
    public final Integer getPage() {
        return this.page;
    }
    public final void setPage(final Integer page) {
        this.page = page;
    }
    public final Integer getItemsPerPage() {
        return this.itemsPerPage;
    }
    public final void setItemsPerPage(final Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }
    public final SortColumn getColumn() {
        return this.column;
    }
    public final void setColumn(final SortColumn column) {
        this.column = column;
    }
    public final SortOrder getOrder() {
        return this.order;
    }
    public final void setOrder(final SortOrder order) {
        this.order = order;
    }
    public final String getDataKey() {
        return this.dataKey;
    }
    public final void setDataKey(final String dataKey) {
        this.dataKey = dataKey;
    }
    public final CustomerCardType getCustomerCardType() {
        return this.customerCardType;
    }
    public final void setCustomerCardType(final CustomerCardType customerCardType) {
        this.customerCardType = customerCardType;
    }
    public final Integer getCustomerCardTypeId() {
        return this.customerCardTypeId;
    }
    public final void setCustomerCardTypeId(final Integer customerCardTypeId) {
        this.customerCardTypeId = customerCardTypeId;
    }
}
