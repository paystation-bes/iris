package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

public class CustomerCardUploadForm implements Serializable {
	private static final long serialVersionUID = 3339700179216787775L;
	
	private String importMode;
	private MultipartFile file;
	
	public CustomerCardUploadForm() {
		
	}

	public String getImportMode() {
		return importMode;
	}

	public void setImportMode(String importMode) {
		this.importMode = importMode;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}
}
