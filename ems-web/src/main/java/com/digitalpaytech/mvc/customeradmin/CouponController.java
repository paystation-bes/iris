package com.digitalpaytech.mvc.customeradmin;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.constants.SortOrder;
import com.digitalpaytech.domain.Consumer;
import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.util.HibernateConstants;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.JsonResponse;
import com.digitalpaytech.dto.PaginatedList;
import com.digitalpaytech.dto.customeradmin.CouponDTO;
import com.digitalpaytech.dto.customeradmin.CouponSearchCriteria;
import com.digitalpaytech.exception.InvalidCSVDataException;
import com.digitalpaytech.mvc.customeradmin.support.CouponForm;
import com.digitalpaytech.mvc.customeradmin.support.CouponSearchForm;
import com.digitalpaytech.mvc.customeradmin.support.CouponUploadForm;
import com.digitalpaytech.mvc.customeradmin.support.io.CouponCsvReader;
import com.digitalpaytech.mvc.helper.AccountBeanHelper;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.WebObjectCacheManager;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.BatchProcessingException;
import com.digitalpaytech.service.CouponService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.ServiceHelper;
import com.digitalpaytech.service.kpi.KPIListenerService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.LabelConstants;
import com.digitalpaytech.util.LocationWithLineNumbers;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.csv.CsvProcessingError;
import com.digitalpaytech.util.support.LookAheadMap;
import com.digitalpaytech.validation.customeradmin.CouponSearchValidator;
import com.digitalpaytech.validation.customeradmin.CouponUploadValidator;
import com.digitalpaytech.validation.customeradmin.CouponValidator;

@Controller
public class CouponController {
    protected static final String FORM_SEARCH = "couponSearchForm";
    protected static final String FORM_EDIT = "couponForm";
    protected static final String FORM_UPLOAD = "couponUploadForm";
    
    private static final String COUPON_LIST = "couponsList";
    private static final String LOCATION_TREE = "locationTree";
    
    private static final String LINES_BEGINNING = " [lines# ";
    
    @Autowired
    private CouponService couponService;
    
    @Autowired
    private CouponSearchValidator searchValidator;
    
    @Autowired
    private CouponCsvReader couponCsvReaderNew;
    
    @Autowired
    private CouponValidator couponValidator;
    
    @Autowired
    private CouponUploadValidator couponUploadValidator;
    
    @Autowired
    private LocationService locationService;
    
    @Autowired
    private AccountBeanHelper accountBeanHelper;
    
    @Autowired
    private EmsPropertiesService emsprops;
    
    @Autowired
    private WebObjectCacheManager webObjectCacheManager;
    
    @Autowired
    private KPIListenerService kpiListenerService;
    
    @Autowired
    private ServiceHelper serviceHelper;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    public CouponController() {
        
    }
    
    @RequestMapping(value = "/secure/accounts/coupons/index.html", method = RequestMethod.GET)
    public final String showCoupons(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_SEARCH) final WebSecurityForm<CouponSearchForm> secSearchForm,
        @ModelAttribute(FORM_EDIT) final WebSecurityForm<CouponForm> secForm,
        @ModelAttribute(FORM_UPLOAD) final WebSecurityForm<CouponUploadForm> secUploadForm) {
        String resultVM = "/accounts/coupons/index";
        //
        if (!canView(response)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                resultVM = WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            resultVM = WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
            
        } else {
            final UserAccount user = WebSecurityUtil.getUserAccount();
            
            final Integer customerId = WebSecurityUtil.getUserAccount().getCustomer().getId();
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            
            final CouponForm couponForm = secForm.getWrappedObject();
            couponForm.setCustomerRandomId(keyMapping.getRandomString(user.getCustomer(), WebCoreConstants.ID_LOOK_UP_NAME));
            
            model.put(FORM_SEARCH, secSearchForm);
            model.put(FORM_EDIT, secForm);
            model.put(FORM_UPLOAD, secUploadForm);
            
            final CouponSearchCriteria criteria = createSearchCriteria(secSearchForm.getWrappedObject(), request);
            criteria.setHideOfflineCoupon(true);
            
            final PaginatedList<CouponDTO> couponList = prepareCoupons(this.couponService.findCouponByCriteria(criteria), keyMapping, criteria);
            
            model.put("couponListJSON", WidgetMetricsHelper.convertToJson(couponList, COUPON_LIST, true));
            
            model.put(LOCATION_TREE, WidgetMetricsHelper
                    .convertToJson(this.locationService.getLocationTreeByCustomerId(customerId, true, false, keyMapping), LOCATION_TREE, false));
            
            final List<FilterDTO> possibleStatuses = new ArrayList<FilterDTO>(3);
            possibleStatuses.add(new FilterDTO(CouponSearchForm.Status.ALL.toString(),
                    this.couponValidator.getMessage(LabelConstants.LABEL_COUPON_SEARCH_WITH_DOT + CouponSearchForm.Status.ALL)));
            possibleStatuses.add(new FilterDTO(CouponSearchForm.Status.ACTIVE.toString(),
                    this.couponValidator.getMessage(LabelConstants.LABEL_COUPON_SEARCH_WITH_DOT + CouponSearchForm.Status.ACTIVE)));
            possibleStatuses.add(new FilterDTO(CouponSearchForm.Status.EXPIRED.toString(),
                    this.couponValidator.getMessage(LabelConstants.LABEL_COUPON_SEARCH_WITH_DOT + CouponSearchForm.Status.EXPIRED)));
            
            model.put("couponStatusTypesJSON", WidgetMetricsHelper.convertToJson(possibleStatuses, "couponStatusTypes", false));
        }
        
        return resultVM;
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/accounts/coupons/searchConsumerCoupons.html", method = RequestMethod.POST)
    public final String showConsumerCoupons(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<CouponSearchForm> secureForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response) {
        String resultJson = WebCoreConstants.RESPONSE_FALSE;
        if (canViewCouponsList(response)) {
            this.searchValidator.validate(secureForm, result);
            //            secureForm.resetToken();
            if (secureForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
                resultJson = WidgetMetricsHelper.convertToJson(secureForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
            } else {
                final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
                final CouponSearchForm form = secureForm.getWrappedObject();
                
                List<Coupon> coupons = null;
                
                final CouponSearchCriteria criteria = createSearchCriteria(form, request);
                final List<Integer> selectedCouponIds = retrieveCouponIds(form, keyMapping);
                if (selectedCouponIds.size() > 0 && (criteria.getPage() == null || criteria.getPage() <= 1)) {
                    final Integer itemsPerPage = criteria.getItemsPerPage();
                    criteria.setItemsPerPage(null);
                    criteria.setCouponIds(selectedCouponIds);
                    
                    coupons = this.couponService.findCouponByCriteria(criteria);
                    
                    criteria.setItemsPerPage(itemsPerPage);
                    criteria.setCouponIds(null);
                }
                
                if (!form.isShowOnlySelectedCoupons()) {
                    criteria.setUnusedCoupon(true);
                    final List<Coupon> unselectedCoupons = this.couponService.findCouponByCriteria(criteria);
                    if (coupons == null) {
                        coupons = unselectedCoupons;
                    } else {
                        coupons.addAll(unselectedCoupons);
                    }
                }
                
                criteria.setHideOfflineCoupon(true);
                
                final PaginatedList<CouponDTO> couponsList = prepareCoupons(coupons, keyMapping, criteria);
                
                resultJson = WidgetMetricsHelper.convertToJson(couponsList, COUPON_LIST, true);
            }
        }
        
        return resultJson;
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/accounts/coupons/searchCoupons.html", method = RequestMethod.POST)
    public final String searchCoupons(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<CouponSearchForm> secureForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response) {
        String resultJson = WebCoreConstants.RESPONSE_FALSE;
        if (canViewCouponsList(response)) {
            this.searchValidator.validate(secureForm, result);
            if (secureForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
                resultJson = WidgetMetricsHelper.convertToJson(secureForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
            } else {
                final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
                final CouponSearchForm form = secureForm.getWrappedObject();
                
                final CouponSearchCriteria criteria = createSearchCriteria(form, request);
                criteria.setDisableFilterValueAsDiscount(true);
                if (form.getStatus() != null) {
                    if (form.getStatus() == CouponSearchForm.Status.ACTIVE) {
                        final String timeZone = WebSecurityUtil.getCustomerTimeZone();
                        criteria.setValidForDate(DateUtil.convertTimeZone(timeZone, DateUtil.GMT, new Date()));
                    } else if (form.getStatus() == CouponSearchForm.Status.EXPIRED) {
                        final String timeZone = WebSecurityUtil.getCustomerTimeZone();
                        criteria.setLastValidDate(DateUtil.convertTimeZone(timeZone, DateUtil.GMT, new Date()));
                    }
                }
                
                final PaginatedList<CouponDTO> couponsList = prepareCoupons(this.couponService.findCouponByCriteria(criteria), keyMapping, criteria);
                
                resultJson = WidgetMetricsHelper.convertToJson(couponsList, COUPON_LIST, true);
            }
        }
        
        return resultJson;
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/accounts/coupons/searchUnusedCoupons.html", method = RequestMethod.POST)
    public final String searchUnusedCoupons(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<CouponSearchForm> secureForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response) {
        String resultJson = WebCoreConstants.RESPONSE_FALSE;
        if (canViewCouponsList(response)) {
            this.searchValidator.validate(secureForm, result);
            if (secureForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
                resultJson = WidgetMetricsHelper.convertToJson(secureForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
            } else {
                final String timeZone = WebSecurityUtil.getCustomerTimeZone();
                
                final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
                final CouponSearchForm form = secureForm.getWrappedObject();
                
                final CouponSearchCriteria criteria = createSearchCriteria(form, request);
                criteria.setUnusedCoupon(true);
                criteria.setValidForDate(DateUtil.convertTimeZone(timeZone, DateUtil.GMT, new Date()));
                
                final PaginatedList<CouponDTO> couponsList = prepareCoupons(this.couponService.findCouponByCriteria(criteria), keyMapping, criteria);
                
                resultJson = WidgetMetricsHelper.convertToJson(couponsList, COUPON_LIST, true);
            }
        }
        
        return resultJson;
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/accounts/coupons/locateItemPage.html", method = RequestMethod.POST)
    public final String getPageNumber(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<CouponSearchForm> secureForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response) {
        String resultJson = WebCoreConstants.RESPONSE_FALSE;
        if (canViewCouponsList(response)) {
            this.searchValidator.validate(secureForm, result);
            if (secureForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
                resultJson = WidgetMetricsHelper.convertToJson(secureForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
            } else {
                final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
                final CouponSearchForm form = secureForm.getWrappedObject();
                
                final CouponSearchCriteria criteria = createSearchCriteria(form, request);
                criteria.setDisableFilterValueAsDiscount(true);
                int targetPage = -1;
                final Integer targetId = keyMapping.getKey(form.getTargetRandomId(), Coupon.class, Integer.class);
                if (targetId != null) {
                    criteria.setMaxUpdatedTime(new Date());
                    targetPage = this.couponService.findRecordPage(targetId, criteria);
                }
                
                if (targetPage > 0) {
                    criteria.setPage(targetPage);
                    
                    final PaginatedList<CouponDTO> couponsList =
                            prepareCoupons(this.couponService.findCouponByCriteria(criteria), keyMapping, criteria);
                    couponsList.setPage(targetPage);
                    
                    resultJson = WidgetMetricsHelper.convertToJson(couponsList, COUPON_LIST, true);
                }
            }
        }
        
        return resultJson;
    }
    
    private boolean canView(final HttpServletResponse response) {
        boolean result = true;
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_COUPONS)
            && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_COUPONS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            result = false;
        }
        
        return result;
    }
    
    private boolean canManage(final HttpServletResponse response) {
        boolean result = true;
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_COUPONS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            result = false;
        }
        
        return result;
    }
    
    private boolean canViewCouponsList(final HttpServletResponse response) {
        return WebSecurityUtil.permissionAudit(response, WebSecurityConstants.PERMISSION_VIEW_COUPONS, WebSecurityConstants.PERMISSION_MANAGE_COUPONS,
                                               WebSecurityConstants.PERMISSION_VIEW_CONSUMER_ACCOUNTS,
                                               WebSecurityConstants.PERMISSION_MANAGE_CONSUMER_ACCOUNTS);
    }
    
    private PaginatedList<CouponDTO> prepareCoupons(final List<Coupon> couponsList, final RandomKeyMapping keyMapping,
        final CouponSearchCriteria criteria) {
        final List<CouponDTO> dtosList = new ArrayList<CouponDTO>(couponsList.size());
        for (Coupon coupon : couponsList) {
            final CouponDTO dto = new CouponDTO();
            this.accountBeanHelper.populate(coupon, dto, keyMapping, true, true);
            
            dtosList.add(dto);
        }
        
        final PaginatedList<CouponDTO> result = new PaginatedList<CouponDTO>();
        result.setElements(dtosList);
        if (criteria.getMaxUpdatedTime() != null) {
            result.setDataKey(Long.toString(criteria.getMaxUpdatedTime().getTime()));
        }
        
        return result;
    }
    
    private List<Integer> retrieveCouponIds(final CouponSearchForm form, final RandomKeyMapping keyMapping) {
        final List<Integer> couponIds = new ArrayList<Integer>();
        if (form.getCouponRandomIds() != null && form.getCouponRandomIds().length() > 0) {
            final StringTokenizer tokenizer = new StringTokenizer(form.getCouponRandomIds(), ",");
            while (tokenizer.hasMoreTokens()) {
                couponIds.add((Integer) keyMapping.getKey(tokenizer.nextToken().trim()));
            }
        }
        
        return couponIds;
    }
    
    private CouponSearchCriteria createSearchCriteria(final CouponSearchForm form, final HttpServletRequest request) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final CouponSearchCriteria result = new CouponSearchCriteria();
        result.setCustomerId(WebSecurityUtil.getUserAccount().getCustomer().getId());
        result.setHideOfflineCoupon(true);
        result.setDisableFilterValueAsDiscount(!form.isQuickAdd());
        
        if (form.getFilterName() != null && form.getFilterName().length() > 0) {
            result.setFilterValue(form.getFilterName());
        }
        
        if (form.getDiscount() != null && form.getDiscount().length() > 0) {
            result.setDiscountValue(form.getDiscount());
        }
        
        // Paginate...
        createPagination(form, result);
        
        if (form.getColumn() == null) {
            form.setColumn(CouponSearchForm.SortColumn.CODE);
        }
        
        if (form.getOrder() == null) {
            form.setOrder(SortOrder.ASC);
        }
        
        result.setOrderColumn(form.getColumn().getActualAttribute());
        result.setOrderDesc(form.getOrder() != SortOrder.ASC);
        
        if (form.getIgnoreConsumerRandomId() != null) {
            result.setIgnoredConsumerId(keyMapping.getKey(form.getIgnoreConsumerRandomId(), Consumer.class, Integer.class));
        }
        
        if (form.getIgnoreRandomIds() != null) {
            result.setIgnoredCouponIds(keyMapping.getAllKeys(form.getIgnoreRandomIds(), Coupon.class, Integer.class));
        }
        
        return result;
    }
    
    private void createPagination(final CouponSearchForm form, final CouponSearchCriteria result) {
        result.setPage((form.getPage() == null) ? 1 : form.getPage());
        if (form.getItemsPerPage() == null) {
            result.setItemsPerPage(WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT);
        } else if (form.getItemsPerPage() > WebCoreConstants.MAX_RESULT_COUNT) {
            result.setItemsPerPage(WebCoreConstants.MAX_RESULT_COUNT);
        } else {
            result.setItemsPerPage(form.getItemsPerPage());
        }
        if (form.getDataKey() != null && form.getDataKey().length() > 0) {
            try {
                result.setMaxUpdatedTime(new Date(Long.parseLong(form.getDataKey())));
            } catch (NumberFormatException nfe) {
                result.setMaxUpdatedTime(new Date());
            }
        } else {
            result.setMaxUpdatedTime(new Date());
        }
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/accounts/coupons/viewCoupon.html", method = RequestMethod.GET)
    public final String viewCoupon(final HttpServletRequest request, final HttpServletResponse response) {
        String resultJson = WebCoreConstants.RESPONSE_FALSE;
        if (canView(response)) {
            final String randomId = request.getParameter(HibernateConstants.ID);
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            final Integer id =
                    WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, randomId,
                                                              new String[] { "+++ Invalid randomised id in viewCoupon - CouponController. +++" });
            
            final CouponDTO dto = new CouponDTO();
            
            Coupon coupon = this.couponService.findCouponById(id);
            
            if (coupon.isIsDeleted()) {
                final MessageInfo message = new MessageInfo();
                message.setError(true);
                message.setToken(null);
                message.addMessage(this.commonControllerHelper.getMessage(ErrorConstants.REQUESTED_ITEM_DELETED));
                return WidgetMetricsHelper.convertToJson(message, WebCoreConstants.UI_MESSAGES, true);
            }
            
            this.accountBeanHelper.populate(coupon, dto, keyMapping, false, false);
            
            resultJson = WidgetMetricsHelper.convertToJson(dto, "coupon", true);
        }
        return resultJson;
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/accounts/coupons/createCoupon.html", method = RequestMethod.POST)
    public final String createCoupon(@ModelAttribute(FORM_EDIT) final WebSecurityForm<CouponForm> secureForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response) {
        String resultJson = WebCoreConstants.RESPONSE_FALSE;
        if (canManage(response)) {
            resultJson = createOrUpdateCoupon(secureForm, result, request, response);
        }
        return resultJson;
        
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/accounts/coupons/updateCoupon.html", method = RequestMethod.POST)
    public final String updateCoupon(@ModelAttribute(FORM_EDIT) final WebSecurityForm<CouponForm> secureForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response) {
        String resultJson = WebCoreConstants.RESPONSE_FALSE;
        if (canManage(response)) {
            resultJson = createOrUpdateCoupon(secureForm, result, request, response);
        }
        return resultJson;
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/accounts/coupons/deleteCouponVerify.html", method = RequestMethod.GET)
    public final String deleteCouponVerify(final HttpServletRequest request, final HttpServletResponse response) {
        String result = WebCoreConstants.RESPONSE_FALSE;
        if (canManage(response)) {
            
            if (!WebSecurityUtil.isCustomerMigrated()) {
                return WebCoreConstants.RESPONSE_FALSE + StandardConstants.STRING_COLON
                       + this.couponValidator.getMessage(LabelConstants.ERROR_CUSTOMER_NOT_MIGRATED);
            }
            
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            final Integer id = WebCoreUtil
                    .verifyRandomIdAndReturnActual(response, keyMapping, request.getParameter(HibernateConstants.ID),
                                                   new String[] { "+++ Invalid randomised id in deleteCouponVerify - CouponController. +++" });
            final Coupon c = this.couponService.findCouponById(id);
            if (c != null) {
                final JsonResponse<Object> msgObj = new JsonResponse<Object>(true);
                if (c.getConsumer() != null) {
                    msgObj.setMessage(this.couponValidator.getMessage("alert.accounts.deleteAssignedCouponConfirm", c.getCoupon()));
                } else {
                    msgObj.setMessage(this.couponValidator.getMessage("alert.accounts.deleteCouponConfirm", c.getCoupon()));
                }
                
                result = WidgetMetricsHelper.convertToJson(msgObj, "result", true);
            }
        }
        
        return result;
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/accounts/coupons/deleteCoupon.html", method = RequestMethod.GET)
    public final String deleteCoupon(@ModelAttribute(FORM_EDIT) final WebSecurityForm<CouponForm> secureForm, final HttpServletRequest request,
        final HttpServletResponse response) {
        String resultJson = WebCoreConstants.RESPONSE_FALSE;
        if (canManage(response)) {
            
            if (!WebSecurityUtil.isCustomerMigrated()) {
                return WebCoreConstants.RESPONSE_FALSE + StandardConstants.STRING_COLON
                       + this.couponValidator.getMessage(LabelConstants.ERROR_CUSTOMER_NOT_MIGRATED);
            }
            
            final String randomId = request.getParameter(HibernateConstants.ID);
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            final Integer id =
                    WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, randomId,
                                                              new String[] { "+++ Invalid randomised id in deleteCoupon - CouponController. +++" });
            final Coupon c = this.couponService.findCouponById(id);
            if (c == null) {
                return WebCoreConstants.RESPONSE_FALSE;
            }
            this.couponService.deleteCoupon(c);
            resultJson = new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(StandardConstants.STRING_COLON).append(secureForm.getInitToken())
                    .toString();
        }
        return resultJson;
    }
    
    @RequestMapping(value = "/secure/accounts/coupons/exportCoupons.html", method = RequestMethod.GET)
    public final void exportCustomCards(final HttpServletRequest request, final HttpServletResponse response) {
        if (canManage(response)) {
            final UserAccount userAccount = WebSecurityUtil.getUserAccount();
            final Customer customer = userAccount.getCustomer();
            final Collection<Coupon> coupons = this.couponService.findCouponsByCustomerId(customer.getId());
            
            final TimeZone customerTimeZone = TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone());
            final SimpleDateFormat fmt = new SimpleDateFormat(WebCoreConstants.FORMAT_YYYYMMDD);
            fmt.setTimeZone(customerTimeZone);
            final String fileName = this.couponUploadValidator.getMessage("label.coupon.export.fileName", customer.getId(), customer.getName(),
                                                                          fmt.format(new Date()));
            
            response.setHeader("Content-Type", "text/csv");
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
            response.setHeader("Cache-Control", StandardConstants.STRING_PUBLIC);
            response.setHeader("Pragma", StandardConstants.STRING_PUBLIC);
            
            final Map<String, Object> context = new HashMap<String, Object>(2);
            context.put(CouponCsvReader.CTXKEY_CUSTOMER, customer);
            context.put(CouponCsvReader.CTXKEY_CUSTOMER_TIME_ZONE, customerTimeZone);
            
            PrintWriter writer = null;
            try {
                writer = response.getWriter();
                this.couponCsvReaderNew.createCsv(writer, coupons, context);
                
            } catch (IOException e) {
                response.setStatus(HttpStatus.EXPECTATION_FAILED.value());
                return;
            } finally {
                if (writer != null) {
                    writer.flush();
                    writer.close();
                }
            }
        }
    }
    
    @RequestMapping(value = "/secure/accounts/coupons/importCoupons.html", method = RequestMethod.POST)
    public final ResponseEntity<String> importCoupons(@ModelAttribute(FORM_UPLOAD) final WebSecurityForm<CouponUploadForm> secureForm,
        final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        String result = WebCoreConstants.RESPONSE_FALSE;
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_COUPONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                result = WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            result = WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        } else if (!WebSecurityUtil.isCustomerMigrated()) {
            result = WebCoreConstants.RESPONSE_FALSE + ":" + this.couponValidator.getMessage("error.customer.notMigrated");
        } else {
            final MessageInfo messages = new MessageInfo();
            secureForm.clearValidationErrors();
            
            final UserAccount user = WebSecurityUtil.getUserAccount();
            final Customer customer = this.customerService.findCustomerDetached(user.getCustomer().getId());
            
            if (!this.couponUploadValidator.handleMultipartExceptions(secureForm, request)) {
                this.couponUploadValidator.validate(secureForm, bindingResult);
                if (secureForm.getValidationErrorInfo().getErrorStatus().size() <= 0) {
                    processCouponImport(customer, secureForm, messages, request);
                }
            }
            
            if (secureForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
                result = WidgetMetricsHelper.convertToJson(secureForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
            } else if (!messages.hasMessages() && messages.getPollingURI() == null) {
                result = WebCoreConstants.RESPONSE_TRUE;
            } else {
                result = WidgetMetricsHelper.convertToJson(messages, "messages", true);
            }
        }
        
        final HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.TEXT_HTML);
        
        return new ResponseEntity<String>(WebCoreUtil.cleanupIframeJSON(result), responseHeaders, HttpStatus.OK);
    }
    
    private void processCouponImport(final Customer customer, final WebSecurityForm<CouponUploadForm> secureForm, final MessageInfo messages,
        final HttpServletRequest request) {
        final CouponUploadForm form = secureForm.getWrappedObject();
        final UserAccount user = WebSecurityUtil.getUserAccount();
        
        final Map<String, Object> context = new HashMap<String, Object>();
        context.put(CouponCsvReader.CTXKEY_CUSTOMER, customer);
        
        final TimeZone customerTimeZone = TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone());
        context.put(CouponCsvReader.CTXKEY_CUSTOMER_TIME_ZONE, customerTimeZone);
        
        final LookAheadMap<String, Location> locationLookAheadMap = new LookAheadMap<String, Location>(new LocationWithLineNumbers.Helper());
        context.put(CouponCsvReader.CTXKEY_LOCATION, locationLookAheadMap);
        
        try {
            final List<CsvProcessingError> errors = new ArrayList<CsvProcessingError>();
            final List<CsvProcessingError> warnings = new ArrayList<CsvProcessingError>();
            if (this.kpiListenerService.getKPIService() != null) {
                this.kpiListenerService.getKPIService().addUIFileSize(form.getFile().getSize());
            }
            
            final int maxSize = emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.MAX_UPLOAD_FILE_SIZE_IN_BYTES,
                                                                           EmsPropertiesService.DEFAULT_MAX_UPLOAD_FILE_SIZE_IN_BYTES);
            
            this.couponCsvReaderNew.processFile(errors, warnings, form.getFile(), context, maxSize);
            
            // Pay debts on Locations
            if (locationLookAheadMap.lookAheadKeySet().size() > 0) {
                final List<Location> locsList =
                        this.locationService.getDetachedLocationsByLowerCaseNames(customer.getId(), locationLookAheadMap.lookAheadKeySet());
                checkLocations(locsList, locationLookAheadMap, errors);
            }
            
            if (errors.size() > 0) {
                final Map<String, StringBuilder> errorsGroup = CsvProcessingError.groupErrors(errors);
                for (String errorMessage : errorsGroup.keySet()) {
                    secureForm.addErrorStatus(new FormErrorStatus(FormFieldConstants.FILE,
                            errorMessage + LINES_BEGINNING + errorsGroup.get(errorMessage).toString() + StandardConstants.STRING_CLOSE_BRACKET));
                }
            } else {
                final String statusKey = this.webObjectCacheManager.createProcessKey(request, user);
                
                if (warnings.size() <= 0) {
                    messages.setRequireManualClose(false);
                } else {
                    this.serviceHelper.getProcessStatus(statusKey).warn();
                    messages.setRequireManualClose(true);
                    
                    final Map<String, StringBuilder> errorsGroup = CsvProcessingError.groupErrors(warnings);
                    for (String errorMessage : errorsGroup.keySet()) {
                        messages.addMessage(errorMessage + LINES_BEGINNING + errorsGroup.get(errorMessage).toString()
                                            + StandardConstants.STRING_CLOSE_BRACKET);
                    }
                }
                
                @SuppressWarnings("unchecked")
                final Collection<Coupon> couponsToAdd = (Collection<Coupon>) context.get(CouponCsvReader.CTXKEY_RESULT);
                
                int processMillis = this.emsprops.getProperty(EmsPropertiesService.CSV_UPLOAD_PROCTIME_PER_K, Integer.class,
                                                              StandardConstants.SECONDS_IN_MILLIS_1)
                                    * couponsToAdd.size() / StandardConstants.SECONDS_IN_MILLIS_1;
                if (processMillis < StandardConstants.SECONDS_IN_MILLIS_1) {
                    processMillis = StandardConstants.SECONDS_IN_MILLIS_1;
                }
                
                this.couponService.processBatchUpdateAsync(statusKey, customer.getId(), couponsToAdd, form.getCreateUpdateOrDelete(), user.getId());
                messages.checkback(WebCoreConstants.URI_BATCH_PROCESS_STATUS + "?statusKey=" + statusKey, processMillis);
            }
        } catch (InvalidCSVDataException icsvde) {
            secureForm.addErrorStatus(new FormErrorStatus(FormFieldConstants.FILE, icsvde.getMessage()));
        } catch (BatchProcessingException bpe) {
            secureForm.addErrorStatus(new FormErrorStatus(FormFieldConstants.FILE, bpe.getMessage()));
        }
        
    }
    
    private void checkLocations(final List<Location> locsList, final LookAheadMap<String, Location> locationLookAheadMap,
        final List<CsvProcessingError> errors) {
        for (Location loc : locsList) {
            final String locSig = loc.getName().toLowerCase();
            if (locationLookAheadMap.lookAheadKeySet().contains(locSig)) {
                locationLookAheadMap.put(locSig, loc);
            } else {
                final String warningMsg = this.couponUploadValidator
                        .getMessage("error.common.ambiguous", this.couponUploadValidator.getMessage(LabelConstants.LABEL_COUPON_LOCATION_NAME),
                                    loc.getName());
                final LocationWithLineNumbers locNums = (LocationWithLineNumbers) loc;
                if (locNums.getLineNumbers().size() <= 0) {
                    errors.add(new CsvProcessingError(warningMsg, 0, true));
                } else {
                    for (Integer lineNum : locNums.getLineNumbers()) {
                        errors.add(new CsvProcessingError(warningMsg, lineNum, true));
                    }
                }
            }
        }
        
        for (String locSig : locationLookAheadMap.lookAheadKeySet()) {
            final String errorMsg = this.couponUploadValidator
                    .getMessage("error.common.invalid", this.couponUploadValidator.getMessage(LabelConstants.LABEL_COUPON_LOCATION_NAME));
            final LocationWithLineNumbers locNums = (LocationWithLineNumbers) locationLookAheadMap.get(locSig);
            if (locNums.getLineNumbers().size() <= 0) {
                errors.add(new CsvProcessingError(errorMsg, 0));
            } else {
                for (Integer lineNum : locNums.getLineNumbers()) {
                    errors.add(new CsvProcessingError(errorMsg, lineNum));
                }
            }
        }
    }
    
    private String createOrUpdateCoupon(final WebSecurityForm<CouponForm> secureForm, final BindingResult result, final HttpServletRequest request,
        final HttpServletResponse response) {
        if (!canViewCouponsList(response)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        if (!this.couponValidator.validateMigrationStatus(secureForm)) {
            return WidgetMetricsHelper.convertToJson(secureForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final Customer customer = userAccount.getCustomer();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        this.couponValidator.validate(secureForm, result, keyMapping);
        secureForm.resetToken();
        if (secureForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            return WidgetMetricsHelper.convertToJson(secureForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        final CouponForm theForm = secureForm.getWrappedObject();
        if (theForm == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Coupon newCoupon = new Coupon();
        this.accountBeanHelper.populate(theForm, newCoupon, keyMapping);
        
        if (newCoupon.getId() == null) {
            final Coupon existingCoupon = this.couponService.findCouponByCustomerIdAndCouponCode(customer.getId(), newCoupon.getCoupon());
            if (existingCoupon != null && (newCoupon.getId() == null || !newCoupon.getId().equals(existingCoupon.getId()))) {
                secureForm.addErrorStatus(new FormErrorStatus("couponCode",
                        this.couponValidator.getMessage("error.common.duplicated", this.couponValidator.getMessage("label.coupon"),
                                                        this.couponValidator.getMessage("label.coupon.number"))));
                return WidgetMetricsHelper.convertToJson(secureForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
            }
        }
        
        final Coupon updatedCoupon = this.couponService.modifyCoupon(newCoupon, userAccount.getId());
        
        return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(StandardConstants.STRING_COLON).append(secureForm.getInitToken())
                .append(StandardConstants.STRING_COLON).append(keyMapping.getRandomString(Coupon.class, updatedCoupon.getId())).toString();
        
    }
    
    public final void setCouponValidator(final CouponValidator couponValidator) {
        this.couponValidator = couponValidator;
    }
    
    public final void setLocationService(final LocationService locationService) {
        this.locationService = locationService;
    }
    
    @ModelAttribute(FORM_SEARCH)
    public final WebSecurityForm<CouponSearchForm> initSearchForm(final HttpServletRequest request) {
        return new WebSecurityForm<CouponSearchForm>((Object) null, new CouponSearchForm(),
                SessionTool.getInstance(request).locatePostToken(FORM_SEARCH));
    }
    
    @ModelAttribute(FORM_EDIT)
    public final WebSecurityForm<CouponForm> initEditForm(final HttpServletRequest request) {
        return new WebSecurityForm<CouponForm>((Object) null, new CouponForm(), SessionTool.getInstance(request).locatePostToken(FORM_EDIT));
    }
    
    @ModelAttribute(FORM_UPLOAD)
    public final WebSecurityForm<CouponUploadForm> initUploadForm(final HttpServletRequest request) {
        return new WebSecurityForm<CouponUploadForm>((Object) null, new CouponUploadForm(),
                SessionTool.getInstance(request).locatePostToken(FORM_UPLOAD));
    }
}
