package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ReportEditForm implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -6645238685669055489L;
    private String randomId;
    private Integer reportType;
    private Boolean isSummary;
    private Integer primaryDateType;
    private Integer primaryMonthBegin;
    private Integer primaryYearBegin;
    private String primaryDateBegin;
    private String primaryTimeBegin;
    private Integer primaryMonthEnd;
    private Integer primaryYearEnd;
    private String primaryDateEnd;
    private String primaryTimeEnd;
    private Integer secondaryDateType;
    private Integer secondaryMonthBegin;
    private Integer secondaryYearBegin;
    private String secondaryDateBegin;
    private String secondaryTimeBegin;
    private Integer secondaryMonthEnd;
    private Integer secondaryYearEnd;
    private String secondaryDateEnd;
    private String secondaryTimeEnd;
    private Boolean showHiddenPayStations;
    private Integer locationType;
    private Integer spaceNumberType;
    private Integer spaceNumberBegin;
    private Integer spaceNumberEnd;
    private Integer cardNumberType;
    private String cardNumberSpecific;
    private String cardType;
    private Integer approvalStatus;
    private String merchantAccount;
    private String licencePlate;
    private Integer ticketNumberType;
    private Integer ticketNumberBegin;
    private Integer ticketNumberEnd;
    private Integer couponNumberType;
    private String couponNumberSpecific;
    private Integer couponType;
    private Integer account;
    private Integer otherParameters;
    private Integer groupBy;
    private Integer sortBy;
    private Boolean isPDF;
    private Boolean isScheduled;
    private String reportTitle;
    private String startDate;
    private String startTime;
    private Integer repeatType;
    private Integer repeatEvery;
    private String srcHistoryRandomId;
    private String consumerRandomId;
    private String accountFilterValue;
    
    private String customerRandomId;
    private Integer customerId;
    
    private String severityType;
    private String deviceType;
    private boolean forCoinCount;
    private boolean forCoinAmount;
    private boolean forBillCount;
    private boolean forBillAmount;
    private boolean forCardCount;
    private boolean forCardAmount;
    private boolean forRunningTotal;
    private boolean forOverdueCollection;
    
    // input lists
    
    private List<String> repeatOn;
    private List<String> emailList;
    
    private List<String> selectedOrgRandomIds;
    private List<String> selectedLocationRandomIds;
    
    private Boolean isSystemAdmin;
    private Boolean isParentAdmin;
    private Boolean isChildAdmin;
    
    public ReportEditForm() {
        selectedLocationRandomIds = new ArrayList<String>();
    }
    
    public String getRandomId() {
        return randomId;
    }
    
    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }
    
    public Integer getReportType() {
        return reportType;
    }
    
    public void setReportType(Integer reportType) {
        this.reportType = reportType;
    }
    
    public Boolean getIsSummary() {
        return isSummary;
    }
    
    public void setIsSummary(Boolean isSummary) {
        this.isSummary = isSummary;
    }
    
    public Integer getPrimaryDateType() {
        return primaryDateType;
    }
    
    public void setPrimaryDateType(Integer primaryDateType) {
        this.primaryDateType = primaryDateType;
    }
    
    public Integer getPrimaryMonthBegin() {
        return primaryMonthBegin;
    }
    
    public void setPrimaryMonthBegin(Integer primaryMonthBegin) {
        this.primaryMonthBegin = primaryMonthBegin;
    }
    
    public Integer getPrimaryYearBegin() {
        return primaryYearBegin;
    }
    
    public void setPrimaryYearBegin(Integer primaryYearBegin) {
        this.primaryYearBegin = primaryYearBegin;
    }
    
    public String getPrimaryDateBegin() {
        return primaryDateBegin;
    }
    
    public void setPrimaryDateBegin(String primaryDateBegin) {
        this.primaryDateBegin = primaryDateBegin;
    }
    
    public String getPrimaryTimeBegin() {
        return primaryTimeBegin;
    }
    
    public void setPrimaryTimeBegin(String primaryTimeBegin) {
        this.primaryTimeBegin = primaryTimeBegin;
    }
    
    public Integer getPrimaryMonthEnd() {
        return primaryMonthEnd;
    }
    
    public void setPrimaryMonthEnd(Integer primaryMonthEnd) {
        this.primaryMonthEnd = primaryMonthEnd;
    }
    
    public Integer getPrimaryYearEnd() {
        return primaryYearEnd;
    }
    
    public void setPrimaryYearEnd(Integer primaryYearEnd) {
        this.primaryYearEnd = primaryYearEnd;
    }
    
    public String getPrimaryDateEnd() {
        return primaryDateEnd;
    }
    
    public void setPrimaryDateEnd(String primaryDateEnd) {
        this.primaryDateEnd = primaryDateEnd;
    }
    
    public String getPrimaryTimeEnd() {
        return primaryTimeEnd;
    }
    
    public void setPrimaryTimeEnd(String primaryTimeEnd) {
        this.primaryTimeEnd = primaryTimeEnd;
    }
    
    public Integer getSecondaryDateType() {
        return secondaryDateType;
    }
    
    public void setSecondaryDateType(Integer secondaryDateType) {
        this.secondaryDateType = secondaryDateType;
    }
    
    public Integer getSecondaryMonthBegin() {
        return secondaryMonthBegin;
    }
    
    public void setSecondaryMonthBegin(Integer secondaryMonthBegin) {
        this.secondaryMonthBegin = secondaryMonthBegin;
    }
    
    public Integer getSecondaryYearBegin() {
        return secondaryYearBegin;
    }
    
    public void setSecondaryYearBegin(Integer secondaryYearBegin) {
        this.secondaryYearBegin = secondaryYearBegin;
    }
    
    public String getSecondaryDateBegin() {
        return secondaryDateBegin;
    }
    
    public void setSecondaryDateBegin(String secondaryDateBegin) {
        this.secondaryDateBegin = secondaryDateBegin;
    }
    
    public String getSecondaryTimeBegin() {
        return secondaryTimeBegin;
    }
    
    public void setSecondaryTimeBegin(String secondaryTimeBegin) {
        this.secondaryTimeBegin = secondaryTimeBegin;
    }
    
    public Integer getSecondaryMonthEnd() {
        return secondaryMonthEnd;
    }
    
    public void setSecondaryMonthEnd(Integer secondaryMonthEnd) {
        this.secondaryMonthEnd = secondaryMonthEnd;
    }
    
    public Integer getSecondaryYearEnd() {
        return secondaryYearEnd;
    }
    
    public void setSecondaryYearEnd(Integer secondaryYearEnd) {
        this.secondaryYearEnd = secondaryYearEnd;
    }
    
    public String getSecondaryDateEnd() {
        return secondaryDateEnd;
    }
    
    public void setSecondaryDateEnd(String secondaryDateEnd) {
        this.secondaryDateEnd = secondaryDateEnd;
    }
    
    public String getSecondaryTimeEnd() {
        return secondaryTimeEnd;
    }
    
    public void setSecondaryTimeEnd(String secondaryTimeEnd) {
        this.secondaryTimeEnd = secondaryTimeEnd;
    }
    
    public Integer getLocationType() {
        return locationType;
    }
    
    public void setLocationType(Integer locationType) {
        this.locationType = locationType;
    }
    
    public Integer getSpaceNumberType() {
        return spaceNumberType;
    }
    
    public void setSpaceNumberType(Integer spaceNumberType) {
        this.spaceNumberType = spaceNumberType;
    }
    
    public Integer getSpaceNumberBegin() {
        return spaceNumberBegin;
    }
    
    public void setSpaceNumberBegin(Integer spaceNumberBegin) {
        this.spaceNumberBegin = spaceNumberBegin;
    }
    
    public Integer getSpaceNumberEnd() {
        return spaceNumberEnd;
    }
    
    public void setSpaceNumberEnd(Integer spaceNumberEnd) {
        this.spaceNumberEnd = spaceNumberEnd;
    }
    
    public Integer getCardNumberType() {
        return cardNumberType;
    }
    
    public void setCardNumberType(Integer cardNumberType) {
        this.cardNumberType = cardNumberType;
    }
    
    public String getCardNumberSpecific() {
        return cardNumberSpecific;
    }
    
    public void setCardNumberSpecific(String cardNumberSpecific) {
        this.cardNumberSpecific = cardNumberSpecific;
    }
    
    public String getCardType() {
        return cardType;
    }
    
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
    
    public Integer getApprovalStatus() {
        return approvalStatus;
    }
    
    public void setApprovalStatus(Integer approvalStatus) {
        this.approvalStatus = approvalStatus;
    }
    
    public String getMerchantAccount() {
        return merchantAccount;
    }
    
    public void setMerchantAccount(String merchantAccount) {
        this.merchantAccount = merchantAccount;
    }
    
    public String getLicencePlate() {
        return licencePlate;
    }
    
    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }
    
    public Integer getTicketNumberType() {
        return ticketNumberType;
    }
    
    public void setTicketNumberType(Integer ticketNumberType) {
        this.ticketNumberType = ticketNumberType;
    }
    
    public Integer getTicketNumberBegin() {
        return ticketNumberBegin;
    }
    
    public void setTicketNumberBegin(Integer ticketNumberBegin) {
        this.ticketNumberBegin = ticketNumberBegin;
    }
    
    public Integer getTicketNumberEnd() {
        return ticketNumberEnd;
    }
    
    public void setTicketNumberEnd(Integer ticketNumberEnd) {
        this.ticketNumberEnd = ticketNumberEnd;
    }
    
    public Integer getCouponNumberType() {
        return couponNumberType;
    }
    
    public void setCouponNumberType(Integer couponNumberType) {
        this.couponNumberType = couponNumberType;
    }
    
    public String getCouponNumberSpecific() {
        return couponNumberSpecific;
    }
    
    public void setCouponNumberSpecific(String couponNumberSpecific) {
        this.couponNumberSpecific = couponNumberSpecific;
    }
    
    public Integer getOtherParameters() {
        return otherParameters;
    }
    
    public void setOtherParameters(Integer otherParameters) {
        this.otherParameters = otherParameters;
    }
    
    public Integer getGroupBy() {
        return groupBy;
    }
    
    public void setGroupBy(Integer groupBy) {
        this.groupBy = groupBy;
    }
    
    public Integer getSortBy() {
        return sortBy;
    }
    
    public void setSortBy(Integer sortBy) {
        this.sortBy = sortBy;
    }
    
    public Boolean getIsPDF() {
        return isPDF;
    }
    
    public void setIsPDF(Boolean isPDF) {
        this.isPDF = isPDF;
    }
    
    public Boolean getIsScheduled() {
        return isScheduled;
    }
    
    public void setIsScheduled(Boolean isScheduled) {
        this.isScheduled = isScheduled;
    }
    
    public String getReportTitle() {
        return reportTitle;
    }
    
    public void setReportTitle(String reportTitle) {
        this.reportTitle = reportTitle;
    }
    
    public String getStartDate() {
        return startDate;
    }
    
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    
    public String getStartTime() {
        return startTime;
    }
    
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
    
    public Integer getRepeatType() {
        return repeatType;
    }
    
    public void setRepeatType(Integer repeatType) {
        this.repeatType = repeatType;
    }
    
    public Integer getRepeatEvery() {
        return repeatEvery;
    }
    
    public void setRepeatEvery(Integer repeatEvery) {
        this.repeatEvery = repeatEvery;
    }
    
    public List<String> getRepeatOn() {
        return repeatOn;
    }
    
    public void setRepeatOn(List<String> repeatOn) {
        this.repeatOn = repeatOn;
    }
    
    public List<String> getEmailList() {
    	return emailList;
    }
    
    public void setEmailList(List<String> emailList) {
        this.emailList = emailList;
    }
    
    

    public Integer getCouponType() {
        return couponType;
    }

    public void setCouponType(Integer couponType) {
        this.couponType = couponType;
    }

    public String getSrcHistoryRandomId() {
        return srcHistoryRandomId;
    }

    public void setSrcHistoryRandomId(String srcHistoryRandomId) {
        this.srcHistoryRandomId = srcHistoryRandomId;
    }

    public List<String> getSelectedOrgRandomIds() {
        return selectedOrgRandomIds;
    }

    public void setSelectedOrgRandomIds(List<String> selectedOrgRandomIds) {
        this.selectedOrgRandomIds = selectedOrgRandomIds;
    }

    public Boolean getIsSystemAdmin() {
        return isSystemAdmin;
    }

    public void setIsSystemAdmin(Boolean isSystemAdmin) {
        this.isSystemAdmin = isSystemAdmin;
    }

    public Boolean getIsParentAdmin() {
        return isParentAdmin;
    }

    public void setIsParentAdmin(Boolean isParentAdmin) {
        this.isParentAdmin = isParentAdmin;
    }

    public Boolean getIsChildAdmin() {
        return isChildAdmin;
    }

    public void setIsChildAdmin(Boolean isChildAdmin) {
        this.isChildAdmin = isChildAdmin;
    }

    public List<String> getSelectedLocationRandomIds() {
        return selectedLocationRandomIds;
    }

    public void setSelectedLocationRandomIds(List<String> selectedLocationRandomIds) {
        this.selectedLocationRandomIds = selectedLocationRandomIds;
    }

    public Boolean getShowHiddenPayStations() {
        return showHiddenPayStations;
    }

    public void setShowHiddenPayStations(Boolean showHiddenPayStations) {
        this.showHiddenPayStations = showHiddenPayStations;
    }

    public String getCustomerRandomId() {
        return customerRandomId;
    }

    public void setCustomerRandomId(String customerRandomId) {
        this.customerRandomId = customerRandomId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }
    
    public String getSeverityType() {
        return severityType;
    }

    public void setSeverityType(String severityType) {
        this.severityType = severityType;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public boolean isForCoinCount() {
        return forCoinCount;
    }

    public void setForCoinCount(boolean forCoinCount) {
        this.forCoinCount = forCoinCount;
    }

    public boolean isForCoinAmount() {
        return forCoinAmount;
    }

    public void setForCoinAmount(boolean forCoinAmount) {
        this.forCoinAmount = forCoinAmount;
    }

    public boolean isForBillCount() {
        return forBillCount;
    }

    public void setForBillCount(boolean forBillCount) {
        this.forBillCount = forBillCount;
    }

    public boolean isForBillAmount() {
        return forBillAmount;
    }

    public void setForBillAmount(boolean forBillAmount) {
        this.forBillAmount = forBillAmount;
    }

    public boolean isForCardCount() {
        return forCardCount;
    }

    public void setForCardCount(boolean forCardCount) {
        this.forCardCount = forCardCount;
    }

    public boolean isForCardAmount() {
        return forCardAmount;
    }

    public void setForCardAmount(boolean forCardAmount) {
        this.forCardAmount = forCardAmount;
    }

    public boolean isForRunningTotal() {
        return forRunningTotal;
    }

    public void setForRunningTotal(boolean forRunningTotal) {
        this.forRunningTotal = forRunningTotal;
    }

    public boolean isForOverdueCollection() {
        return forOverdueCollection;
    }

    public void setForOverdueCollection(boolean forOverdueCollection) {
        this.forOverdueCollection = forOverdueCollection;
    }

    public Integer getAccount() {
        return account;
    }

    public void setAccount(Integer account) {
        this.account = account;
    }

    public String getConsumerRandomId() {
        return consumerRandomId;
    }

    public void setConsumerRandomId(String consumerRandomId) {
        this.consumerRandomId = consumerRandomId;
    }

    public String getAccountFilterValue() {
        return accountFilterValue;
    }

    public void setAccountFilterValue(String accountFilterValue) {
        this.accountFilterValue = accountFilterValue;
    }
}
