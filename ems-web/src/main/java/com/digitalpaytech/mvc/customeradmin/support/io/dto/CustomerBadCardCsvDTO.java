package com.digitalpaytech.mvc.customeradmin.support.io.dto;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

public class CustomerBadCardCsvDTO implements Serializable {
	private static final long serialVersionUID = -4666115428719883601L;
	
	private String cardType;
	private String cardNumber;
	private String internalKey;
	private String expiryDate;
	private String comment;
	
	public CustomerBadCardCsvDTO() {
		
	}
	
	public CustomerBadCardCsvDTO(String[] tokens) {
		this.cardType = StringUtils.stripToNull(tokens[0]);
		if(this.cardType != null) {
			this.cardType = this.cardType.toLowerCase();
		}
		
        this.cardNumber = StringUtils.stripToNull(tokens[1]);
        this.internalKey = StringUtils.stripToNull(tokens[2]);
        this.expiryDate = StringUtils.stripToNull(tokens[3]);
        this.comment = StringUtils.stripToNull(tokens[4]);
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getInternalKey() {
		return internalKey;
	}

	public void setInternalKey(String internalKey) {
		this.internalKey = internalKey;
	}
}
