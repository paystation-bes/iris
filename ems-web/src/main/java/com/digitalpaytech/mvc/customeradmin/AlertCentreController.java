package com.digitalpaytech.mvc.customeradmin;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.EventDeviceType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.EventType;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.domain.PosEventHistory;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportLocationValue;
import com.digitalpaytech.domain.ReportType;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.ActiveAlertSearchCriteria;
import com.digitalpaytech.dto.AdhocReportInfo;
import com.digitalpaytech.dto.AlertMapEntry;
import com.digitalpaytech.dto.AlertSearchCriteria;
import com.digitalpaytech.dto.PaginatedList;
import com.digitalpaytech.dto.SummarySearchCriteria;
import com.digitalpaytech.dto.comparator.AlertCentreAlertDateComparator;
import com.digitalpaytech.dto.comparator.AlertCentreAlertPayStationComparator;
import com.digitalpaytech.dto.comparator.AlertCentreAlertSeverityComparator;
import com.digitalpaytech.dto.customeradmin.AlertCentreAlertInfo;
import com.digitalpaytech.dto.customeradmin.AlertCentrePayStationInfo;
import com.digitalpaytech.dto.customeradmin.AlertCentreSummaryInfo;
import com.digitalpaytech.dto.customeradmin.AlertInfoEntry;
import com.digitalpaytech.dto.customeradmin.AlertSummaryEntry;
import com.digitalpaytech.dto.customeradmin.LocationRouteFilterInfo;
import com.digitalpaytech.dto.customeradmin.ModuleValuePair;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.dto.customeradmin.SeverityValuePair;
import com.digitalpaytech.mvc.customeradmin.support.AlertCentreFilterForm;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.AlertsService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.MobileLicenseService;
import com.digitalpaytech.service.MobileSessionTokenService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.PosAlertStatusService;
import com.digitalpaytech.service.PosEventCurrentService;
import com.digitalpaytech.service.PosEventHistoryService;
import com.digitalpaytech.service.ReportDefinitionService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.customeradmin.AlertCentreFilterValidator;

@Controller
public class AlertCentreController {
    public static final String FORM_ALERTS = "alertsForm";
    public static final String LIST_ALERTS = "alertsList";
    public static final String INFO_PAYSTATION = "payStationInfo";
    public static final int SECONDS_3 = 3000;
    public static final int DAYS_90 = 90;
    
    protected static final Logger LOGGER = Logger.getLogger(AlertCentreController.class);
    @Autowired
    protected MobileSessionTokenService mobileSessionTokenService;
    @Autowired
    protected MobileLicenseService mobileAppService;
    @Autowired
    protected LocationService locationService;
    @Autowired
    protected RouteService routeService;
    @Autowired
    protected PointOfSaleService pointOfSaleService;
    @Autowired
    protected AlertsService alertsService;
    @Autowired
    protected CustomerAdminService customerAdminService;
    @Autowired
    protected AlertCentreFilterValidator alertCentreFilterValidator;
    @Autowired
    protected CommonControllerHelper commonControllerHelper;
    @Autowired
    protected ReportDefinitionService reportDefinitionService;
    @Autowired
    private PosAlertStatusService posAlertStatusService;
    @Autowired
    private PosEventCurrentService posEventCurrentService;
    @Autowired
    private PosEventHistoryService posEventHistoryService;
    @Autowired
    private MessageHelper messageHelper;
    @Autowired
    private UserAccountService userAccountService;
    
    public final void setLocationService(final LocationService locationService) {
        this.locationService = locationService;
    }
    
    public final void setRouteService(final RouteService routeService) {
        this.routeService = routeService;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final void setAlertsService(final AlertsService alertsService) {
        this.alertsService = alertsService;
    }
    
    public final void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    public final void setPosAlertStatusService(final PosAlertStatusService posAlertStatusService) {
        this.posAlertStatusService = posAlertStatusService;
    }
    
    public final void setAlertCentreFilterValidator(final AlertCentreFilterValidator alertCentreFilterValidator) {
        this.alertCentreFilterValidator = alertCentreFilterValidator;
    }
    
    public final void setPosEventCurrentService(final PosEventCurrentService posEventCurrentService) {
        this.posEventCurrentService = posEventCurrentService;
    }
    
    public void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }
    
    public final void setMessageHelper(final MessageHelper messageHelper) {
        this.messageHelper = messageHelper;
    }
    
    /**
     * This method returns information for filtering active alerts based on
     * module, location or route. It then returns all unfiltered currently
     * active alerts.
     * 
     * @param request
     * @param response
     * @param model
     * @return Model attributes: moduleFilterList - List of name/randomId pairs
     *         of modules used to filed results. locationRouteInfo - A
     *         LocationRouteFilterInfo object containing list of locations and
     *         routes used to filter alerts. activeAlerts - An
     *         AlertCentreAlertInfo List of information for current active
     *         alerts. Returns "/alerts/index.vm"
     */
    @RequestMapping(value = "/secure/alerts/index.html")
    public final String alertCentreIndex(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_ALERTS) final WebSecurityForm<AlertCentreFilterForm> webSecurityForm) {
        
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_MAINTENANCE_CENTER)
            && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_CLEAR_ACTIVE_ALERTS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Customer customer = userAccount.getCustomer();
        
        //        List<PointOfSale> pointOfSaleList = pointOfSaleService.findPointOfSalesByCustomerId(userAccount.getCustomer().getId());
        
        final LocationRouteFilterInfo locationRouteInfo = this.commonControllerHelper
                .createLocationRouteFilter(customer.getId(), keyMapping, WebCoreConstants.ROUTE_TYPE_MAINTENANCE);
        
        final List<ModuleValuePair> moduleFilter = new ArrayList<ModuleValuePair>();
        final List<SeverityValuePair> severityFilter = new ArrayList<SeverityValuePair>();
        
        for (EventDeviceType module : this.alertsService.findAllEventDeviceTypes()) {
            moduleFilter.add(new ModuleValuePair(keyMapping.getRandomString(module, WebCoreConstants.ID_LOOK_UP_NAME), this.messageHelper
                    .getMessage(module.getName())));
        }
        
        for (EventSeverityType severityType : this.alertsService.findAllEventSeverityTypes()) {
            if (severityType.getId() != WebCoreConstants.SEVERITY_CLEAR) {
                severityFilter.add(new SeverityValuePair(severityType.getName(), keyMapping.getRandomString(severityType,
                                                                                                            WebCoreConstants.ID_LOOK_UP_NAME)));
            }
        }
        
        final PlacementMapBordersInfo mapBorders = this.pointOfSaleService.findMapBordersByCustomerId(customer.getId());
        model.put("mapBorders", mapBorders);
        model.put("moduleFilter", moduleFilter);
        model.put("severityFilter", severityFilter);
        model.put("locationFilter", locationRouteInfo.getLocations());
        model.put("routeFilter", locationRouteInfo.getRoutes());
        
        model.put(FORM_ALERTS, webSecurityForm);
        
        //        model.put("alertsList", WidgetMetricsHelper.convertToJson(alertsList, "alertsList", true));
        
        return "/alerts/index";
    }
    
    //TODO as per EMS 4915 this should be renamed to viewPayStationAlertsCleared.html
    @RequestMapping(value = "/secure/alerts/filterActiveAlerts.html", method = RequestMethod.POST)
    @ResponseBody
    public final String getActiveAlertsByFilter(@ModelAttribute(FORM_ALERTS) final WebSecurityForm<AlertCentreFilterForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_MAINTENANCE_CENTER)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_CLEAR_ACTIVE_ALERTS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        /* validate user input. */
        this.alertCentreFilterValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final TimeZone customerTimeZone = TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone());
        final AlertCentreFilterForm filter = webSecurityForm.getWrappedObject();
        if (filter.getDataKey() == null) {
            filter.setDataKey(System.currentTimeMillis());
        }
        
        final AlertSearchCriteria criteria = prepareHistoryCriteria(filter, userAccount);
        criteria.setOnlyPayStation(true);
        
        final Calendar clearedGmt = Calendar.getInstance();
        clearedGmt.setTime(criteria.getMaxClearedTime());
        clearedGmt.add(Calendar.DAY_OF_YEAR, WebCoreConstants.OLDEST_DAY_FOR_HISTORY_ALERT);
        criteria.setMinClearedTime(clearedGmt.getTime());
        
        final List<AlertInfoEntry> buffer = this.alertsService.findHistory(criteria, customerTimeZone, (String) null, (String) null);
        final List<AlertCentreAlertInfo> alertsList = new ArrayList<AlertCentreAlertInfo>(buffer == null ? 0 : buffer.size());
        if (buffer != null) {
            for (AlertInfoEntry entry : buffer) {
                alertsList.add(new AlertCentreAlertInfo(entry));
            }
        }
        
        final PaginatedList<AlertCentreAlertInfo> paginatedAlertsList = new PaginatedList<AlertCentreAlertInfo>();
        paginatedAlertsList.setElements(alertsList);
        paginatedAlertsList.setDataKey(Long.toString(filter.getDataKey()));
        
        return WidgetMetricsHelper.convertToJson(paginatedAlertsList, LIST_ALERTS, true);
    }
    
    private AlertSearchCriteria prepareHistoryCriteria(final AlertCentreFilterForm filter, final UserAccount userAccount) {
        final AlertSearchCriteria criteria = new AlertSearchCriteria();
        criteria.setIsAlertCentre(true);
        criteria.setCustomerId(userAccount.getCustomer().getId());
        if (filter.getId() != null) {
            if (!filter.isLocationOrRoute()) {
                criteria.setLocationId(filter.getId());
            } else {
                criteria.setRouteId(filter.getId());
            }
        }
        criteria.setSeverityId(filter.getSeverityId());
        criteria.setEventDeviceId(filter.getEventDeviceId());
        
        criteria.setAlertClassTypeIds(new ArrayList<Byte>());
        criteria.getAlertClassTypeIds().add(WebCoreConstants.ALERT_CLASS_TYPE_COMMUNICATION);
        criteria.getAlertClassTypeIds().add(WebCoreConstants.ALERT_CLASS_TYPE_PAY_STATION_ALERT);
        
        criteria.setActive(filter.isAlert());
        criteria.setPage(filter.getPageNumber());
        
        final Calendar clearedGmt = Calendar.getInstance();
        clearedGmt.setTimeInMillis(filter.getDataKey());
        criteria.setMaxClearedTime(clearedGmt.getTime());
        
        return criteria;
    }
    
    @RequestMapping(value = "/secure/alerts/viewPayStationAlertsMap.html", method = RequestMethod.POST)
    @ResponseBody
    public final String viewPayStationAlertsMap(@ModelAttribute(FORM_ALERTS) final WebSecurityForm<AlertCentreFilterForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_MAINTENANCE_CENTER)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_CLEAR_ACTIVE_ALERTS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        /* validate user input. */
        this.alertCentreFilterValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        final AlertCentreFilterForm filter = webSecurityForm.getWrappedObject();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final ActiveAlertSearchCriteria searchCriteria = createActiveAlertSearchCriteria(filter, userAccount);
        searchCriteria.setPlaced(true);
        
        final List<AlertMapEntry> alertsList = this.alertsService.findPointOfSalesWithAlertMap(searchCriteria);
        return WidgetMetricsHelper.convertToJson(alertsList, LIST_ALERTS, true);
    }
    
    @RequestMapping(value = "/secure/alerts/viewPayStationClearedAlertsMap.html", method = RequestMethod.POST)
    @ResponseBody
    public final String viewPayStationClearedAlertsMap(@ModelAttribute(FORM_ALERTS) final WebSecurityForm<AlertCentreFilterForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_MAINTENANCE_CENTER)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_CLEAR_ACTIVE_ALERTS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final TimeZone customerTimeZone = TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone());
        final AlertCentreFilterForm filter = webSecurityForm.getWrappedObject();
        if (filter.getDataKey() == null) {
            filter.setDataKey(System.currentTimeMillis());
        }
        
        final AlertSearchCriteria criteria = prepareHistoryCriteria(filter, userAccount);
        final Calendar clearedGmt = Calendar.getInstance();
        clearedGmt.setTime(criteria.getMaxClearedTime());
        clearedGmt.add(Calendar.DAY_OF_YEAR, WebCoreConstants.OLDEST_DAY_FOR_HISTORY_ALERT_MAP);
        criteria.setMinClearedTime(clearedGmt.getTime());
        
        List<AlertMapEntry> mapList = this.alertsService.findPointOfSaleWithClearedPSAlert(criteria, customerTimeZone);
        
        /* validate user input. */
        //        this.alertCentreFilterValidator.validate(webSecurityForm, result);
        //        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
        //            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
        //            webSecurityForm.resetToken();
        //            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        //        }
        //        
        //        final AlertCentreFilterForm filter = webSecurityForm.getWrappedObject();
        //        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        //        
        //        final ActiveAlertSearchCriteria searchCriteria = createActiveAlertSearchCriteria(filter, userAccount);
        //        searchCriteria.setPlaced(true);
        //        
        //        final List<AlertMapEntry> alertsList = this.alertsService.findPointOfSalesWithAlertMap(searchCriteria);
        return WidgetMetricsHelper.convertToJson(mapList, LIST_ALERTS, true);
    }
    
    @RequestMapping(value = "/secure/alerts/viewPayStationAlertsDetail.html", method = RequestMethod.POST)
    @ResponseBody
    public final String viewPayStationAlertsDetail(@ModelAttribute(FORM_ALERTS) final WebSecurityForm<AlertCentreFilterForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_MAINTENANCE_CENTER)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_CLEAR_ACTIVE_ALERTS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        /* validate user input. */
        this.alertCentreFilterValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final AlertCentreFilterForm filter = webSecurityForm.getWrappedObject();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final ActiveAlertSearchCriteria searchCriteria = createActiveAlertSearchCriteria(filter, userAccount);
        searchCriteria.setPlaced(false);
        if (filter.getDataKey() == null) {
            filter.setDataKey(System.currentTimeMillis());
        }
        
        searchCriteria.setMaxAlertGmt(new Date(filter.getDataKey()));
        if (filter.getTargetId() != null) {
            searchCriteria.setTargetId(keyMapping.getKey(filter.getTargetId(), PointOfSale.class, Integer.class));
            if (searchCriteria.getTargetId() == null) {
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        if (searchCriteria.getTargetId() != null) {
            searchCriteria.setPage(this.alertsService.locatePageContainsPointOfSaleWithAlert(searchCriteria));
            if (searchCriteria.getPage() < 0) {
                return WebCoreConstants.RESPONSE_FALSE;
            }
        } else if (filter.getPageNumber() != null) {
            searchCriteria.setPage(filter.getPageNumber());
        }
        
        final List<AlertMapEntry> alertsList = this.alertsService.findPointOfSalesWithAlertDetail(searchCriteria,
                                                                                                  WebSecurityUtil.getCustomerTimeZone());
        
        return WidgetMetricsHelper.convertToJson(new PaginatedList<AlertMapEntry>(alertsList, Long
                                                         .toString(searchCriteria.getMaxAlertGmt().getTime()), webSecurityForm.getInitToken(),
                                                         searchCriteria.getPage()), LIST_ALERTS, true);
    }
    
    @RequestMapping(value = "/secure/alerts/payStationAlertsDetailReport.html", method = RequestMethod.POST)
    @ResponseBody
    public final String payStationAlertsDetailReport(@ModelAttribute(FORM_ALERTS) final WebSecurityForm<AlertCentreFilterForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_MAINTENANCE_CENTER)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_CLEAR_ACTIVE_ALERTS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        /* validate user input. */
        this.alertCentreFilterValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        final AlertCentreFilterForm filter = webSecurityForm.getWrappedObject();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final MessageInfo messages = new MessageInfo();
        final AdhocReportInfo ari = this.reportDefinitionService.runAdhocReport(createAdhocReportDefinition(filter, userAccount),
                                                                                WebSecurityUtil.getCustomerTimeZone());
        if (ari.getReportHistoryId() != null) {
            return WidgetMetricsHelper.convertToJson(ari, "report", true);
        } else {
            final StringBuilder checkBackURI = new StringBuilder(CustomerAdminReportingController.URI_ADHOC_REPORT_STATUS);
            checkBackURI.append(".html?");
            ari.appendAsParameterString(checkBackURI);
            
            messages.checkback(checkBackURI.toString(), SECONDS_3);
            return WidgetMetricsHelper.convertToJson(messages, "messages", true);
        }
    }
    
    /**
     * This method takes request parameter "pointOfSaleId" and returns pay
     * station information and all current active alerts for that pay station
     * 
     * @param request
     * @param response
     * @param model
     * @return model attribute "activeAlerts" - A AlertCentreAlertInfoList
     *         object listing all currently active alerts for the requested pay
     *         station. Returns JSON formatted AlertCentrePayStationInfo object.
     *         Returns false if Id is not valid or requested pay station has no
     *         alerts
     */
    @RequestMapping(value = "/secure/alerts/payStationActiveAlerts.html")
    @ResponseBody
    public final String getAlertCentrePayStationActiveAlerts(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_MAINTENANCE_CENTER)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_CLEAR_ACTIVE_ALERTS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        final String strRandomPointOfSaleId = request.getParameter("pointOfSaleId");
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final Integer pointOfSaleId = verifyRandomIdAndReturnActual(response,
                                                                    keyMapping,
                                                                    strRandomPointOfSaleId,
                                                                    "+++ Invalid pointOfSaleId in "
                                                                            + "AlertCentreController.getAlertCentrePayStationActiveAlerts() method. +++");
        if (pointOfSaleId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        List<AlertCentreAlertInfo> alertsList = new ArrayList<AlertCentreAlertInfo>();
        
        final List<PosEventCurrent> posEventCurrentList = this.posEventCurrentService.findMaintenanceCentreDetailByPointOfSaleId(pointOfSaleId);
        
        if (posEventCurrentList == null || posEventCurrentList.isEmpty()) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String timeZone = WebSecurityUtil.getCustomerTimeZone();
        
        for (PosEventCurrent posEventCurrent : posEventCurrentList) {
            
            final int severity = posEventCurrent.getEventSeverityType().getId();
            final StringBuilder eventDescription = new StringBuilder(this.messageHelper.getMessage(posEventCurrent.getEventType().getDescription()));
            if (posEventCurrent.getEventActionType() != null) {
                eventDescription.append(StandardConstants.STRING_EMPTY_SPACE).append(posEventCurrent.getEventActionType().getName());
            }
            boolean isClearable = posEventCurrent.getCustomerAlertType() == null;
            
            alertsList.add(new AlertCentreAlertInfo(posEventCurrent, eventDescription.toString(), severity, timeZone, posEventCurrent.getAlertGmt(),
                    keyMapping, this.messageHelper, isClearable));
            //            }
        }
        
        final AlertCentrePayStationInfo payStationInfo = new AlertCentrePayStationInfo();
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        
        final PosHeartbeat heartbeat = this.pointOfSaleService.findPosHeartbeatByPointOfSaleId(pointOfSaleId);
        final String lastSeen = heartbeat.getLastHeartbeatGmt() == null ? "" : DateUtil.getRelativeTimeString(heartbeat.getLastHeartbeatGmt(),
                                                                                                              timeZone);
        payStationInfo.setLastSeen(lastSeen);
        
        final List<Route> routeList = this.routeService.findRoutesByPointOfSaleIdAndRouteTypeId(pointOfSaleId,
                                                                                                WebCoreConstants.ROUTE_TYPE_MAINTENANCE);
        List<String> routeNameList = null;
        if (routeList != null && !routeList.isEmpty()) {
            routeNameList = new ArrayList<String>();
            for (Route route : routeList) {
                routeNameList.add(route.getName());
            }
        }
        payStationInfo.setLocation(pointOfSale.getLocation().getName());
        payStationInfo.setName(pointOfSale.getName());
        payStationInfo.setRoute(routeNameList);
        payStationInfo.setSerial(pointOfSale.getSerialNumber());
        payStationInfo.setActiveAlerts(alertsList);
        payStationInfo.setPayStationType(pointOfSale.getPaystation().getPaystationType().getId());
        
        alertsList = formatAlerts(alertsList);
        return WidgetMetricsHelper.convertToJson(payStationInfo, INFO_PAYSTATION, true);
    }
    
    @RequestMapping(value = "/secure/alerts/resolvedAlertMap.html")
    @ResponseBody
    public final String getAlertCentreResolvedAlertMap(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS)
            && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        final String strPosId = request.getParameter("pointOfSaleId");
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer posId = keyMapping.getKey(strPosId, PointOfSale.class, Integer.class);
        if (posId == null) {
            LOGGER.warn("+++ Invalid PointOfSaleId in AlertCentreController.getAlertCentreResolvedAlertMap() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String timeZone = WebSecurityUtil.getCustomerTimeZone();
        
        final List<AlertCentreAlertInfo> alertsList = new ArrayList<AlertCentreAlertInfo>();
        
        final List<Route> routeList = this.routeService.findRoutesByPointOfSaleIdAndRouteTypeId(posId, WebCoreConstants.ROUTE_TYPE_MAINTENANCE);
        final List<String> routeNameList = new ArrayList<String>();
        for (Route route : routeList) {
            routeNameList.add(route.getName());
        }
        
        final AlertCentrePayStationInfo payStationInfo = preparePayStationInfo(posId, routeList, timeZone);
        payStationInfo.setActiveAlerts(alertsList);
        
        final AlertSearchCriteria criteria = new AlertSearchCriteria();
        criteria.setIsAlertCentre(true);
        criteria.setCustomerId(userAccount.getCustomer().getId());
        criteria.setPointOfSaleId(posId);
        criteria.setActive(false);
        
        final Calendar clearedTime = Calendar.getInstance();
        criteria.setMaxClearedTime(clearedTime.getTime());
        clearedTime.add(Calendar.DATE, WebCoreConstants.OLDEST_DAY_FOR_HISTORY_ALERT);
        criteria.setMinClearedTime(clearedTime.getTime());
        
        criteria.setItemsPerPage(2);
        criteria.setPage(1);
        
        List<AlertInfoEntry> resolvedList = this.alertsService.findHistory(criteria, TimeZone.getTimeZone(timeZone), (String) null, (String) null);
        for (AlertInfoEntry infoEntry : resolvedList) {
            final AlertCentreAlertInfo resolved = new AlertCentreAlertInfo(infoEntry);
            resolved.setRoute(routeNameList);
            
            payStationInfo.getResolvedAlerts().add(resolved);
        }
        
        return WidgetMetricsHelper.convertToJson(payStationInfo, INFO_PAYSTATION, false);
    }
    
    /**
     * This method takes request parameter "posAlertId" and returns details about
     * the resolved alert
     * 
     * @param request
     * @param response
     * @param model
     * @return Returns JSON formatted AlertCentrePayStationInfo object.
     *         Returns false if Id is not valid or requested pay station has no
     *         alerts
     */
    @RequestMapping(value = "/secure/alerts/resolvedAlert.html")
    @ResponseBody
    public final String getAlertCentreResolvedAlert(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_MAINTENANCE_CENTER)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_CLEAR_ACTIVE_ALERTS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        final String strPosAlertId = request.getParameter("posAlertId");
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final BigInteger tmpId = keyMapping.getKey(strPosAlertId, PosEventHistory.class, BigInteger.class);
        if (tmpId == null) {
            LOGGER.warn("+++ Invalid PosEventHistoryId in AlertCentreController.getAlertCentreResolvedAlert() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Long[] buff = RandomKeyMapping.unpackLong(tmpId);
        //TODO debug new query 
        final PosEventHistory posEventHistory = this.posEventHistoryService.findById(buff[1]);
        final PosEventHistory posEventHistoryClear = this.posEventHistoryService.findById(buff[0]);
        if ((posEventHistory == null) || (posEventHistoryClear == null)) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String timeZone = WebSecurityUtil.getCustomerTimeZone();
        
        final EventType eventType = posEventHistory.getEventType();
        final UserAccount userAccount = this.userAccountService.findUserAccount(posEventHistoryClear.getLastModifiedByUserId());
        
        final List<AlertCentreAlertInfo> alertsList = new ArrayList<AlertCentreAlertInfo>();
        
        final List<Route> routeList = this.routeService.findRoutesByPointOfSaleIdAndRouteTypeId(posEventHistory.getPointOfSale().getId(),
                                                                                                WebCoreConstants.ROUTE_TYPE_MAINTENANCE);
        
        alertsList.add(new AlertCentreAlertInfo(posEventHistory, posEventHistoryClear, this.messageHelper.getMessage(eventType.getDescription()),
                timeZone, routeList, this.messageHelper, userAccount.getFullName()));
        
        final AlertCentrePayStationInfo payStationInfo = preparePayStationInfo(posEventHistory.getPointOfSale().getId(), routeList, timeZone);
        payStationInfo.setActiveAlerts(alertsList);
        
        return WidgetMetricsHelper.convertToJson(payStationInfo, INFO_PAYSTATION, true);
    }
    
    private AlertCentrePayStationInfo preparePayStationInfo(final Integer posId, final List<Route> routeList, final String timeZone) {
        final AlertCentrePayStationInfo payStationInfo = new AlertCentrePayStationInfo();
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(posId);
        
        final PosHeartbeat heartbeat = this.pointOfSaleService.findPosHeartbeatByPointOfSaleId(posId);
        final String lastSeen = heartbeat.getLastHeartbeatGmt() == null ? "" : DateUtil.getRelativeTimeString(heartbeat.getLastHeartbeatGmt(),
                                                                                                              timeZone);
        payStationInfo.setLastSeen(lastSeen);
        
        final List<String> routeNameList = new ArrayList<String>();
        for (Route route : routeList) {
            routeNameList.add(route.getName());
        }
        
        payStationInfo.setLocation(pointOfSale.getLocation().getName());
        payStationInfo.setName(pointOfSale.getName());
        payStationInfo.setRoute(routeNameList);
        payStationInfo.setSerial(pointOfSale.getSerialNumber());
        payStationInfo.setPayStationType(pointOfSale.getPaystation().getPaystationType().getId());
        
        return payStationInfo;
    }
    
    /**
     * This method takes request parameter "activePosAlertId" and clears the alert
     * 
     * @param request
     * @param response
     * @param model
     * @return TRUE if successful
     * 
     */
    @RequestMapping(value = "/secure/alerts/clearAlert.html")
    @ResponseBody
    public final String clearActiveAlert(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_CLEAR_ACTIVE_ALERTS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        if (!WebSecurityUtil.isCustomerMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + ":" + this.alertCentreFilterValidator.getMessage("error.customer.notMigrated");
        }
        
        return this.commonControllerHelper.clearActiveAlert(request, response, true);
        
    }
    
    @SuppressWarnings("unchecked")
    private List<AlertCentreAlertInfo> formatAlerts(final List<AlertCentreAlertInfo> alertsList) {
        
        if (alertsList != null && !alertsList.isEmpty()) {
            final ComparatorChain chain = new ComparatorChain();
            
            //Sort severity highest to lowest
            chain.addComparator(new AlertCentreAlertSeverityComparator(), true);
            
            //Sort alert time stamps most recent to oldest
            chain.addComparator(new AlertCentreAlertDateComparator(), true);
            
            //Sort pay stations alphabetically
            chain.addComparator(new AlertCentreAlertPayStationComparator());
            
            Collections.sort(alertsList, chain);
        }
        
        return alertsList;
    }
    
    protected final Integer verifyRandomIdAndReturnActual(final HttpServletResponse response, final RandomKeyMapping keyMapping,
        final String strRandomLocationId, final String message) {
        
        final String[] msgs = new String[] { message, message };
        return WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, strRandomLocationId, msgs);
    }
    
    protected final ActiveAlertSearchCriteria createActiveAlertSearchCriteria(final AlertCentreFilterForm filter, final UserAccount userAccount) {
        final ActiveAlertSearchCriteria criteria = new ActiveAlertSearchCriteria();
        
        criteria.setCustomerId(userAccount.getCustomer().getId());
        if (filter.getId() != null) {
            if (!filter.isLocationOrRoute()) {
                criteria.setLocationIds(new ArrayList<Integer>(1));
                criteria.getLocationIds().add(filter.getId());
            } else {
                criteria.setRouteIds(new ArrayList<Integer>(1));
                criteria.getRouteIds().add(filter.getId());
            }
        }
        
        criteria.setSeverityIds(new ArrayList<Integer>());
        if (filter.getSeverityId() != null) {
            criteria.getSeverityIds().add(filter.getSeverityId());
        } else {
            criteria.setSeverityIds(new ArrayList<Integer>());
            criteria.getSeverityIds().add(WebCoreConstants.SEVERITY_CRITICAL);
            criteria.getSeverityIds().add(WebCoreConstants.SEVERITY_MAJOR);
            criteria.getSeverityIds().add(WebCoreConstants.SEVERITY_MINOR);
        }
        
        if (filter.getEventDeviceId() != null) {
            criteria.setDeviceTypeIds(new ArrayList<Integer>(1));
            criteria.getDeviceTypeIds().add(filter.getEventDeviceId());
        }
        
        criteria.setAlertThresholdTypeIds(new ArrayList<Integer>(2));
        criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_LAST_SEEN_INTERVAL_HOUR);
        criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_PAYSTATION);
        criteria.setIsPaystationAlertSelected(true);
        
        return criteria;
    }
    
    protected final ReportDefinition createAdhocReportDefinition(final AlertCentreFilterForm filter, final UserAccount userAccount) {
        final ReportDefinition result = new ReportDefinition();
        
        result.setCustomer(new Customer(userAccount.getCustomer().getId()));
        if (!filter.isLocationOrRoute()) {
            result.setLocationFilterTypeId(ReportingConstants.REPORT_FILTER_LOCATION_LOCATION_TREE);
            
            final ReportLocationValue rlv = new ReportLocationValue();
            rlv.setReportDefinition(result);
            rlv.setObjectId((filter.getId() == null) ? 0 : filter.getId());
            rlv.setObjectType(Location.class.getName());
            
            result.getReportLocationValues().add(rlv);
        } else {
            result.setLocationFilterTypeId(ReportingConstants.REPORT_FILTER_LOCATION_ROUTE_TREE);
            
            final ReportLocationValue rlv = new ReportLocationValue();
            rlv.setReportDefinition(result);
            rlv.setObjectId((filter.getId() == null) ? 0 : filter.getId());
            rlv.setObjectType(Route.class.getName());
            
            result.getReportLocationValues().add(rlv);
        }
        
        if (filter.getSeverityId() != null) {
            result.setEventSeverityTypeId(filter.getSeverityId());
        }
        
        if (filter.getEventDeviceId() != null) {
            result.setEventDeviceTypeId(filter.getEventDeviceId());
        }
        
        result.setIsCsvOrPdf(true);
        result.setIsDetailsOrSummary(true);
        result.setUserAccount(userAccount);
        result.setLastModifiedByUserId(userAccount.getId());
        result.setReportType(new ReportType(ReportingConstants.REPORT_TYPE_MAINTENANCE_SUMMARY));
        result.setIsOnlyVisible(true);
        result.setGroupByFilterTypeId(ReportingConstants.REPORT_FILTER_GROUP_BY_LOCATION);
        
        return result;
    }
    
    @RequestMapping(value = "/secure/alerts/posSummary.html")
    public final String getPosSummary(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_ALERTS) final WebSecurityForm<AlertCentreFilterForm> webSecurityForm) {
        
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_MAINTENANCE_CENTER)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_CLEAR_ACTIVE_ALERTS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final List<SeverityValuePair> severityFilter = new ArrayList<SeverityValuePair>();
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Customer customer = userAccount.getCustomer();
        
        for (EventSeverityType severityType : this.alertsService.findAllEventSeverityTypes()) {
            if (severityType.getId() != WebCoreConstants.SEVERITY_CLEAR) {
                severityFilter.add(new SeverityValuePair(severityType.getName(), keyMapping.getRandomString(severityType,
                                                                                                            WebCoreConstants.ID_LOOK_UP_NAME)));
            }
        }
        
        final LocationRouteFilterInfo locationRouteInfo = this.commonControllerHelper
                .createLocationRouteFilter(customer.getId(), keyMapping, WebCoreConstants.ROUTE_TYPE_MAINTENANCE);
        
        model.put("locationFilter", locationRouteInfo.getLocations());
        model.put("routeFilter", locationRouteInfo.getRoutes());
        model.put("severityFilter", severityFilter);
        
        model.put(FORM_ALERTS, webSecurityForm);
        
        return "/alerts/summary";
    }
    
    @RequestMapping(value = "/secure/alerts/posSummaryList.html")
    @ResponseBody
    public final String getPosSummaryList(@ModelAttribute(FORM_ALERTS) final WebSecurityForm<AlertCentreFilterForm> webSecurityForm,
        final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_MAINTENANCE_CENTER)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_CLEAR_ACTIVE_ALERTS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        /* validate user input. */
        this.alertCentreFilterValidator.validate(webSecurityForm, bindingResult);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        final AlertCentreFilterForm filter = (AlertCentreFilterForm) webSecurityForm.getWrappedObject();
        if (filter.getDataKey() == null) {
            filter.setDataKey((new Date()).getTime());
        }
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final SummarySearchCriteria searchCriteria = createSearchCriteria(new SummarySearchCriteria(), filter, userAccount);
        searchCriteria.setExclusiveOrderBy(filter.getColumn());
        searchCriteria.setExclusiveOrderDir(filter.getOrder());
        
        if (filter.getPage() != null) {
            searchCriteria.setPage(filter.getPage());
        }
        
        final List<AlertSummaryEntry> alertList = this.alertsService.findPointOfSalesForAlertSummary(searchCriteria,
                                                                                                     WebSecurityUtil.getCustomerTimeZone());
        
        final List<AlertCentreSummaryInfo> summaryInfoList = populateSummaryInfoList(alertList, request, response);
        
        final PaginatedList<AlertCentreSummaryInfo> result = new PaginatedList<AlertCentreSummaryInfo>();
        result.setElements(summaryInfoList);
        if (filter.getDataKey() != null) {
            result.setDataKey(Long.toString(filter.getDataKey()));
        }
        return WidgetMetricsHelper.convertToJson(result, "alertList", true);
    }
    
    @SuppressWarnings("unchecked")
    private <T> T createSearchCriteria(final SummarySearchCriteria criteria, final AlertCentreFilterForm filter, final UserAccount userAccount) {
        
        criteria.setCustomerId(userAccount.getCustomer().getId());
        
        if (filter.getId() != null) {
            if (!filter.isLocationOrRoute()) {
                criteria.setLocationIds(new ArrayList<Integer>(1));
                criteria.getLocationIds().add(filter.getId());
            } else {
                criteria.setRouteIds(new ArrayList<Integer>(1));
                criteria.getRouteIds().add(filter.getId());
            }
        }
        
        if (filter.getSeverityId() != null) {
            criteria.setSeverityIds(new ArrayList<Integer>(1));
            criteria.getSeverityIds().add(filter.getSeverityId());
        }
        
        criteria.setAlertThresholdTypeIds(new ArrayList<Integer>(1));
        criteria.getAlertThresholdTypeIds().add(WebCoreConstants.ALERT_THRESHOLD_TYPE_PAYSTATION);
        criteria.setIsPaystationAlertSelected(true);
        
        return (T) criteria;
    }
    
    private List<AlertCentreSummaryInfo> populateSummaryInfoList(final List<AlertSummaryEntry> alertList, final HttpServletRequest request,
        final HttpServletResponse response) {
        
        final List<AlertCentreSummaryInfo> summaryInfoList = new LinkedList<AlertCentreSummaryInfo>();
        
        for (AlertSummaryEntry alert : alertList) {
            
            final AlertCentreSummaryInfo alertCentreSummaryInfo = new AlertCentreSummaryInfo();
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            
            alertCentreSummaryInfo.setPosRandomId(alert.getPosRandomId().toString());
            alertCentreSummaryInfo.setPosName(alert.getPointOfSaleName());
            alertCentreSummaryInfo.setSerial(alert.getSerial());
            alertCentreSummaryInfo.setLocationName(alert.getLocationName());
            alertCentreSummaryInfo.setLocationRandomId(alert.getLocationRandomId().toString());
            
            String routeName = "";
            String routeRandomId = "";
            if (alert.getRoutes() != null && !alert.getRoutes().isEmpty()) {
                routeName = "Multiple";
                if (alert.getRoutes().size() < 2) {
                    routeName = alert.getRoutes().get(0).getName();
                    routeRandomId = keyMapping.getRandomString(alert.getRoutes().get(0), "id");
                }
            }
            alertCentreSummaryInfo.setRouteName(routeName);
            alertCentreSummaryInfo.setRouteRandomId(routeRandomId);
            
            alertCentreSummaryInfo.setBatteryLevel(alert.getBatteryLevel());
            alertCentreSummaryInfo.setBatteryVoltage(alert.getBatteryVoltage());
            alertCentreSummaryInfo.setLastSeen(alert.getLastSeenDate());
            alertCentreSummaryInfo.setPaperLevel(alert.getPaperLevel());
            alertCentreSummaryInfo.setPayStationSeverity(alert.getSeverity());
            alertCentreSummaryInfo.setLastSeenSeverity(alert.getLastSeenSeverity());
            
            summaryInfoList.add(alertCentreSummaryInfo);
        }
        
        return summaryInfoList;
    }
    
    @ModelAttribute(FORM_ALERTS)
    public final WebSecurityForm<AlertCentreFilterForm> initAlertsForm(final HttpServletRequest request) {
        return new WebSecurityForm<AlertCentreFilterForm>((Object) null, new AlertCentreFilterForm(), SessionTool.getInstance(request)
                .locatePostToken(FORM_ALERTS));
    }
}
