package com.digitalpaytech.mvc.customeradmin.support.io.dto;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

public class CouponCsvDTO implements Serializable {
	private static final long serialVersionUID = -8429859856829725798L;
	
	private String couponCode;
	private String description;
	private String percentDiscount;
	private String dollarDiscount;
	private String startDate;
	private String endDate;
	private String numUses;
	private String dailySingleUse;
	private String locationName;
	private String pnd;
	private String pbs;
	private String spaceRange;
	
	public CouponCsvDTO() {
		
	}
	
	public CouponCsvDTO(String[] tokens) {
		this.couponCode = StringUtils.stripToNull(tokens[0]);
		this.description = StringUtils.stripToNull(tokens[1]);
		this.percentDiscount = StringUtils.stripToNull(tokens[2]);
		this.dollarDiscount = StringUtils.stripToNull(tokens[3]);
		this.startDate = StringUtils.stripToNull(tokens[4]);
		this.endDate = StringUtils.stripToNull(tokens[5]);
		this.numUses = StringUtils.stripToNull(tokens[6]);
		this.dailySingleUse = StringUtils.stripToNull(tokens[7]);
		this.locationName = StringUtils.stripToNull(tokens[8]);
		this.pnd = StringUtils.stripToNull(tokens[9]);
		this.pbs = StringUtils.stripToNull(tokens[10]);
		this.spaceRange = StringUtils.stripToNull(tokens[11]);
	}

	public String getCouponCode() {
		return couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPercentDiscount() {
		return percentDiscount;
	}

	public void setPercentDiscount(String percentDiscount) {
		this.percentDiscount = percentDiscount;
	}

	public String getDollarDiscount() {
		return dollarDiscount;
	}

	public void setDollarDiscount(String dollarDiscount) {
		this.dollarDiscount = dollarDiscount;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getNumUses() {
		return numUses;
	}

	public void setNumUses(String numUses) {
		this.numUses = numUses;
	}

	public String getDailySingleUse() {
		return dailySingleUse;
	}

	public void setDailySingleUse(String dailySingleUse) {
		this.dailySingleUse = dailySingleUse;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getPnd() {
		return pnd;
	}

	public void setPnd(String pnd) {
		this.pnd = pnd;
	}

	public String getPbs() {
		return pbs;
	}

	public void setPbs(String pbs) {
		this.pbs = pbs;
	}

	public String getSpaceRange() {
		return spaceRange;
	}

	public void setSpaceRange(String spaceRange) {
		this.spaceRange = spaceRange;
	}
}
