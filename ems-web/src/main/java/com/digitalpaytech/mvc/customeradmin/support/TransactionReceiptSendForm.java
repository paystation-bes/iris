package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;

public class TransactionReceiptSendForm implements Serializable {
    
    private static final long serialVersionUID = -9125066944252249218L;
    
    private String purchaseRandomId;
    private String emailAddress;
    
    public String getPurchaseRandomId() {
        return purchaseRandomId;
    }
    
    public void setPurchaseRandomId(String purchaseRandomId) {
        this.purchaseRandomId = purchaseRandomId;
    }
    
    public String getEmailAddress() {
        return emailAddress;
    }
    
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
