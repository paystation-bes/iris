package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;
import org.springframework.web.multipart.MultipartFile;

public class LicensePlateUploadForm implements Serializable {
    private static final long serialVersionUID = 4458003499502293763L;
    private MultipartFile file;

    public final MultipartFile getFile() {
        return this.file;
    }

    public final void setFile(final MultipartFile file) {
        this.file = file;
    }
}
