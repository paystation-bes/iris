package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;
import org.springframework.web.multipart.MultipartFile;

public class CouponUploadForm implements Serializable {
    private static final long serialVersionUID = 2972332707258904595L;
    
    private String createUpdateOrDelete;
    private MultipartFile file;
    private String statusKey;
    
    public CouponUploadForm() {
    	
    }

    public String getCreateUpdateOrDelete() {
        return createUpdateOrDelete;
    }
    
    public void setCreateUpdateOrDelete(String createUpdateOrDelete) {
        this.createUpdateOrDelete = createUpdateOrDelete;
    }
    
    public MultipartFile getFile() {
        return file;
    }
    
    public void setFile(MultipartFile file) {
        this.file = file;
    }
    
	public String getStatusKey() {
		return statusKey;
	}
	
	public void setStatusKey(String statusKey) {
		this.statusKey = statusKey;
	}
}
