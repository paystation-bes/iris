package com.digitalpaytech.mvc.customeradmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.mvc.FlexWSUserAccountController;
import com.digitalpaytech.mvc.support.FlexHelper;
import com.digitalpaytech.mvc.support.FlexWSUserAccountForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
public class CustomerFlexAdminWSUserAccountController extends FlexWSUserAccountController {
    @Override
    @ResponseBody
    @RequestMapping(value = "/secure/settings/global/saveFlexCredentials.html", method = RequestMethod.POST)
    public final String save(@ModelAttribute(FlexHelper.FORM_CREDENTIALS) final WebSecurityForm<FlexWSUserAccountForm> form
            , final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response) {
        if (!subscribed() || !canManage()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.save(form, bindingResult, request, response);
    }

    @Override
    @ResponseBody
    @RequestMapping(value = "/secure/settings/global/testFlexCredentials.html", method = RequestMethod.GET)
    public final String test(@RequestParam(required = false) final String booleanMode, final HttpServletRequest request, final HttpServletResponse response) {
        if (!subscribed() || !canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.test(booleanMode, request, response);
    }

    @Override
    @ResponseBody
    @RequestMapping(value = "/secure/settings/global/testNewFlexCredentials.html", method = RequestMethod.POST)
    protected final String testNew(@ModelAttribute(FlexHelper.FORM_CREDENTIALS) final WebSecurityForm<FlexWSUserAccountForm> form
            , final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response) {
        if (!subscribed() || !canManage()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.testNew(form, bindingResult, request, response);
    }
    
    private boolean subscribed() {
        return WebSecurityUtil.hasValidCustomerSubscription(WebCoreConstants.SUBSCRIPTION_TYPE_FLEX_INTEGRATION);
    }
    
    private boolean canManage() {
        return WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_FLEX);
    }
    
    private boolean canView() {
        return WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_FLEX) || canManage();
    }
}
