package com.digitalpaytech.mvc.customeradmin.support.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerBadCard;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.dto.BadCardData;
import com.digitalpaytech.exception.InvalidCSVDataException;
import com.digitalpaytech.mvc.customeradmin.support.io.dto.BadCardCsvDTO;
import com.digitalpaytech.service.CustomerBadCardService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.LinkBadCardService;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.LabelConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.csv.CsvParserUtils;
import com.digitalpaytech.util.csv.CsvProcessingError;

@Component("badCardCsvProcessor")
public class BadCardCsvProcessor {
    private static final Logger LOG = Logger.getLogger(BadCardCsvProcessor.class);
    private static final String[] COLUMN_HEADERS = { "CardType", "CardNumber", "InternalKey", "ExpiryDate", "Comment" };
    private static final int NUM_COLUMNS = COLUMN_HEADERS.length;
    private static final int CARD_TYPE_POS = 0;
    private static final int CARD_NUMBER_POS = 1;
    private static final int EXPIRY_POS = 3;
    private static final int COMMENT_POS = 4;
    
    private static final String CARD_TYPE_CREDIT_CARD = "Credit Card";
    
    private static final int FOUR_DIGITS = 4;
    
    /* Used to skip the Deletion step executed by CustomerBadCardService.processBatchUpdate */
    private static final String SKIP_REPLACE_MODE = "SKIP_REPLACE";
    
    @Autowired
    @Qualifier("badCardManagementCsvValidator")
    private BadCardManagementCsvValidator validator;
    
    @Autowired
    private LinkBadCardService linkBadCardService;
    
    @Autowired
    private CustomerBadCardService customerBadCardService;
    
    @Autowired
    private CustomerCardTypeService customerCardTypeService;
    
    private Map<String, CustomerCardType> cachedCustomerCardTypes = new HashMap<String, CustomerCardType>();
    
    public List<CsvProcessingError> validate(final InputStream inputStream, final Customer customer) throws IOException {
        
        final List<CsvProcessingError> fileErrors = new ArrayList<>();
        
        int lineNumber = 0;
        try (LineNumberReader lineReader = new LineNumberReader(new InputStreamReader(inputStream))) {
            CsvParserUtils.verifyHeaders(fileErrors, CsvParserUtils.split(lineReader.readLine()), COLUMN_HEADERS, NUM_COLUMNS);
            
            String line = lineReader.readLine();
            while (line != null) {
                lineNumber = lineReader.getLineNumber();
                if (!line.isEmpty()) {
                    final String[] tokens = CsvParserUtils.split(line);
                    if (tokens.length < NUM_COLUMNS) {
                        this.validator.addCsvProcessingError(fileErrors, lineNumber, ErrorConstants.CSV_INSUFFICENT_COLUMNS,
                                                             getMessage(LabelConstants.CARD_MANAGEMENT_BANNED_CARD));
                    } else {
                        final String inputCardType = StringUtils.stripToNull(tokens[CARD_TYPE_POS]);
                        final String cardNumber = StringUtils.stripToNull(tokens[CARD_NUMBER_POS]);
                        final String expiry = StringUtils.stripToNull(tokens[EXPIRY_POS]);
                        final String comment = StringUtils.stripToNull(tokens[COMMENT_POS]);
                        
                        this.validator.validateBadCardCsvRow(fileErrors, inputCardType, cardNumber, expiry, comment, lineNumber, customer,
                                                             this.cachedCustomerCardTypes);
                        
                    }
                }
                line = lineReader.readLine();
            }
            
        }
        
        if (LOG.isDebugEnabled()) {
            LOG.debug(String.format("Validation of BadCard CSV file for customer %d completed. %d lines will be processed.", customer.getId(),
                                    lineNumber));
        }
        return fileErrors;
    }
    
    public void process(final InputStream inputStream, final Customer customer, final int userAccountId) throws IOException, InvalidCSVDataException {
        
        final Map<String, BadCardData> badCardsLink = new HashMap<String, BadCardData>();
        final Map<String, CustomerBadCard> badCardsIris = new HashMap<String, CustomerBadCard>();
        
        try (LineNumberReader lineReader = new LineNumberReader(new InputStreamReader(inputStream))) {
            // skip headers
            lineReader.readLine();
            
            String line = lineReader.readLine();
            while (line != null) {
                if (StringUtils.isNotBlank(line)) {
                    final String[] tokens = CsvParserUtils.split(line);
                    
                    final BadCardCsvDTO badCardCsvDTO = new BadCardCsvDTO(tokens);
                    
                    if (badCardCsvDTO.getCardType().equals(CARD_TYPE_CREDIT_CARD)) {
                        badCardsLink.put(badCardCsvDTO.getCardType() + badCardCsvDTO.getCardNumber(), createBadCardData(badCardCsvDTO, customer));
                    } else {
                        badCardsIris.put(badCardCsvDTO.getCardType() + badCardCsvDTO.getCardNumber(),
                                         createCustomerBadCard(badCardCsvDTO, customer.getId(), userAccountId));
                    }
                }
                line = lineReader.readLine();
            }
            
            processLink(badCardsLink, customer);
            processIris(badCardsIris);
        }
    }
    
    private void processIris(final Map<String, CustomerBadCard> badCardsIris) throws InvalidCSVDataException {
        this.customerBadCardService.createCustomerBadCard(badCardsIris);
    }
    
    private void processLink(final Map<String, BadCardData> badCardsLink, final Customer customer) {
        this.linkBadCardService.addBadCard(badCardsLink, customer.getUnifiId());
    }
    
    private BadCardData createBadCardData(final BadCardCsvDTO dto, final Customer customer) {
        return new BadCardData(dto.getCardNumber(), dto.getExpiryDate(), dto.getCardType(), dto.getComment(), customer,
                WebCoreConstants.BADCARD_SOURCE_IMPORT);
    }
    
    private CustomerBadCard createCustomerBadCard(final BadCardCsvDTO dto, final int customerId, final int userId) throws InvalidCSVDataException {
        
        final CustomerCardType cardType = determineCustomerCardType(customerId, dto.getCardType());
        
        if (cardType == null) {
            throw new InvalidCSVDataException(
                    String.format("Unable to determine CustomerCardType for [customerId = %d] with [CardType = %s]", customerId, dto.getCardType()));
        }
        
        final CustomerBadCard customerBadCard = new CustomerBadCard();
        customerBadCard.setSource(WebCoreConstants.BADCARD_SOURCE_IMPORT);
        customerBadCard.setCustomerCardType(cardType);
        customerBadCard.setCardNumberOrHash(dto.getCardNumber());
        
        if (dto.getCardNumber().length() > FOUR_DIGITS) {
            customerBadCard.setLast4digitsOfCardNumber(new Short(dto.getCardNumber().substring(dto.getCardNumber().length() - FOUR_DIGITS)));
        } else {
            customerBadCard.setLast4digitsOfCardNumber(new Short(dto.getCardNumber()));
        }
        customerBadCard.setComment(dto.getComment());
        
        final Date currentTime = new Date();
        customerBadCard.setAddedGmt(currentTime);
        customerBadCard.setLastModifiedGmt(currentTime);
        customerBadCard.setLastModifiedByUserId(userId);
        
        return customerBadCard;
    }
    
    private CustomerCardType determineCustomerCardType(final int customerId, final String cardTypeName) {
        
        CustomerCardType customerCardType = null;
        customerCardType = this.cachedCustomerCardTypes.get(cardTypeName.toLowerCase(Locale.getDefault()));
        
        if (customerCardType != null) {
            return customerCardType;
        }
        
        final List<CustomerCardType> cardTypesList =
                this.customerCardTypeService.getDetachedBannableCardTypes(customerId, Arrays.asList(cardTypeName.toLowerCase(Locale.getDefault())));
        
        if (cardTypesList != null && !cardTypesList.isEmpty()) {
            customerCardType = cardTypesList.get(0);
        }
        
        return customerCardType;
    }
    
    private String getMessage(final String messageKey, final Object... args) {
        return this.validator.getMessage(messageKey, args);
    }
}
