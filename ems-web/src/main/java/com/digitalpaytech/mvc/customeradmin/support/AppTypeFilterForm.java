package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;

public class AppTypeFilterForm implements Serializable{

    private static final long serialVersionUID = 3636425352149642768L;
    private String appType;
    private Integer page;
    private Long timeStamp;
    
    public String getAppType(){
        return appType;
    }
    public void setAppType(String appType){
        this.appType = appType;
    }
    public Integer getPage(){
        return page;
    }
    public void setPage(Integer page){
        this.page = page;
    }
    public Long getTimeStamp(){
        return timeStamp;
    }
    public void setTimeStamp(Long timeStamp){
        this.timeStamp = timeStamp;
    }
}
