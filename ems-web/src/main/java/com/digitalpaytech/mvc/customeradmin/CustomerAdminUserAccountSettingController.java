package com.digitalpaytech.mvc.customeradmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.UserAccountSettingController;
import com.digitalpaytech.mvc.support.UserAccountEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

/**
 * This class handles the User Account setting request in Settings->Users menu.
 * 
 * @author Brian Kim
 * 
 */
@Controller
@SessionAttributes({ "userAccountEditForm" })
public class CustomerAdminUserAccountSettingController extends UserAccountSettingController {
    
    /**
     * This method will retrieve customer's user account information and display
     * them in Settings->Users main page.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return target vm file URI which is mapped to Settings->Users
     *         (/settings/users/index.vm)
     */
    @RequestMapping(value = "/secure/settings/users/userAccount.html", method = RequestMethod.GET)
    public String getUserListByCustomer(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES)) {
                if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD) && 
                        !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) { 
                    return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
                }            
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
            }
        }
        
        return super.getUserListByCustomer(request, response, model, "/settings/users/userAccount");
    }
    
    /**
     * This method will view User Detail information on the page. When
     * "Edit User" is clicked, it will also be called from front-end logic
     * automatically after calling getUserAccountForm() method.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return JSON string having user detail information. "false" in case of
     *         any exception or validation failure.
     */
    @RequestMapping(value = "/secure/settings/users/viewUserDetails.html", method = RequestMethod.GET)
    public @ResponseBody
    String viewUserDetails(HttpServletRequest request, HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES)) {
                if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD) && 
                        !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) { 
                    return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
                }            
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
            }
        }
        
        return super.viewUserDetails(request, response, false);
    }
    
    /**
     * This method will be called when "Add User" is clicked. It will set the
     * user form and list of Role information.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return JSON string which returns a role list.
     */
    @RequestMapping(value = "/secure/settings/users/form.html", method = RequestMethod.GET)
    public @ResponseBody
    String getUserAccountForm(HttpServletRequest request, HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.formUserAccount(request, response);
    }
    
    /**
     * This method saves user account information in UserAccount table.
     * 
     * @param webSecurityForm
     *            user input form
     * @param result
     *            Error object
     * @param response
     *            HttpServletResponse object
     * @return "true" + ":" + token string value if process succeeds, JSON error
     *         message if process fails
     */
    @RequestMapping(value = "/secure/settings/users/saveUser.html", method = RequestMethod.POST)
    public @ResponseBody
    String saveUser(@ModelAttribute("userAccountEditForm") WebSecurityForm<UserAccountEditForm> webSecurityForm, BindingResult result,
                    HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.saveUser(webSecurityForm, result, response);
    }
    
    /**
     * This method deletes user account information by setting user status type
     * to 2 (deleted)
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return "true" string value if process succeeds, "false" string value if
     *         process fails.
     */
    @RequestMapping(value = "/secure/settings/users/deleteUser.html", method = RequestMethod.GET)
    public @ResponseBody
    String deleteUser(HttpServletRequest request, HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.deleteUser(request, response);
    }
    
    /**
     * This method changes the currently active user to an alias of the parent customer user to access child customer's workspace.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return TRUE if successful, FALSE if next user is not an alias of the same parent user
     */
    @RequestMapping(value = "/secure/settings/users/changeUser.html", method = RequestMethod.GET)
    @ResponseBody
    public final String changeUser(final HttpServletRequest request, final HttpServletResponse response) {
        
        final String userAccountRandomId = request.getParameter("randomId");
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer userAccountId = (Integer) keyMapping.getKey(userAccountRandomId);
        
        final UserAccount currentUserAccount = WebSecurityUtil.getUserAccount();
        final UserAccount nextUserAccount = this.userAccountService.findUserAccount(userAccountId);
        if (currentUserAccount.getRealUserId().intValue() != nextUserAccount.getRealUserId().intValue()) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        WebSecurityUtil.changeUser(userAccountId);
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
}
