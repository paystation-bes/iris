package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;

import javax.persistence.Transient;

public class CollectionCentreFilterForm implements Serializable {
    
    private static final long serialVersionUID = 4216875635917425084L;
    private boolean coin;
    private boolean bills;
    private boolean credit;
    private boolean total;
    private boolean overdue;
    private String randomId;
    private String severityRandomId;
    private boolean isLocationOrRoute;
    private boolean isSeverity;
    private boolean isAlert;
    private Integer page;
    private Integer pageNumber;
    private String targetId;
    private Long timeStamp;
    private String column;
    private String order;
    private Long dataKey;
    
    @Transient
    private transient Integer id;
    @Transient
    private transient Integer severityId;

    public CollectionCentreFilterForm() {
        this.coin = false;
        this.bills = false;
        this.credit = false;
        this.total = false;
        this.overdue = false;
    }
    
    public CollectionCentreFilterForm(final boolean coin, final boolean bills, final boolean credit, final boolean total) {
        this.coin = coin;
        this.bills = bills;
        this.credit = credit;
        this.total = total;        
    }
    
    public final boolean isCoin() {
        return this.coin;
    }
    
    public final void setCoin(final boolean coin) {
        this.coin = coin;
    }
    
    public final boolean isBills() {
        return this.bills;
    }
    
    public final void setBills(final boolean bills) {
        this.bills = bills;
    }
    
    public final boolean isCredit() {
        return this.credit;
    }
    
    public final void setCredit(final boolean credit) {
        this.credit = credit;
    }
    
    public final boolean isTotal() {
        return this.total;
    }
    
    public final void setTotal(final boolean total) {
        this.total = total;
    }

    public final boolean isOverdue() {
        return this.overdue;
    }
    
    public final void setOverdue(final boolean overdue) {
        this.overdue = overdue;
    }

    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final Integer getId() {
        return this.id;
    }
    
    public final void setId(final Integer id) {
        this.id = id;
    }
    
    public final boolean isLocationOrRoute() {
        return this.isLocationOrRoute;
    }
    
    public final void setLocationOrRoute(final boolean locationOrRoute) {
        this.isLocationOrRoute = locationOrRoute;
    }
    
    public final boolean isAlert() {
        return this.isAlert;
    }
    
    public final void setAlert(final boolean alert) {
        this.isAlert = alert;
    }
    
    public final boolean getIsAlert() {
        return this.isAlert;
    }
    
    public final void setIsAlert(final boolean isAlert) {
        this.isAlert = isAlert;
    }
    
    public final Integer getPage() {
        return this.page;
    }
    
    public final void setPage(final Integer page) {
        this.page = page;
    }
    
    public final Long getTimeStamp() {
        return this.timeStamp;
    }
    
    public final void setTimeStamp(final Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public final String getColumn() {
        return this.column;
    }

    public final void setColumn(final String column) {
        this.column = column;
    }

    public final String getOrder() {
        return this.order;
    }

    public final void setOrder(final String order) {
        this.order = order;
    }

    public final Long getDataKey() {
        return this.dataKey;
    }

    public final void setDataKey(final Long dataKey) {
        this.dataKey = dataKey;
    }

    public final String getSeverityRandomId() {
        return this.severityRandomId;
    }

    public final void setSeverityRandomId(final String severityRandomId) {
        this.severityRandomId = severityRandomId;
    }

    public final Integer getPageNumber() {
        return this.pageNumber;
    }

    public final void setPageNumber(final Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public final String getTargetId() {
        return this.targetId;
    }

    public final void setTargetId(final String targetId) {
        this.targetId = targetId;
    }

    public final boolean isSeverity() {
        return this.isSeverity;
    }

    public final void setSeverity(final boolean severity) {
        this.isSeverity = severity;
    }

    public final Integer getSeverityId() {
        return this.severityId;
    }

    public final void setSeverityId(final Integer severityId) {
        this.severityId = severityId;
    }
    
}
