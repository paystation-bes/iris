package com.digitalpaytech.mvc.customeradmin;

import java.text.DateFormat;
import java.util.Collection;
import java.util.TimeZone;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.cardprocessing.CardProcessingManager;
import com.digitalpaytech.cardprocessing.CardProcessorFactory;
import com.digitalpaytech.domain.CPSData;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.dto.cps.Tokens;
import com.digitalpaytech.dto.customeradmin.CardRefundDetails;
import com.digitalpaytech.dto.customeradmin.TransactionReceiptDetails;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.mvc.customeradmin.support.CardRefundForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.PaymentCardService;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.service.PurchaseEmailAddressService;
import com.digitalpaytech.service.cps.CPSDataService;
import com.digitalpaytech.service.cps.CoreCPSService;
import com.digitalpaytech.service.crypto.CryptoAlgorithmFactory;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DashboardUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.PooledResource;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.TLSUtils;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.crypto.CryptoConstants;
import com.digitalpaytech.validation.CardNumberValidator;
import com.digitalpaytech.validation.customeradmin.CardRefundValidator;

/**
 * This controller handles user requests in Card Management menu.
 * (Card Management -> Card Refund)
 * 
 * @author Brian Kim
 * 
 */
@Controller
@SuppressWarnings({ "PMD.ExcessiveImports", "PMD.GodClass" })
public class CardRefundController {
    private static final Logger LOG = Logger.getLogger(CardRefundController.class);
    private static final String TRANSACTION_RECEIPT_DETAILS = "transactionReceiptDetails";
    private static final String ERROR_REFUND_TRANSACTION_FAIL = "error.refund.transaction.fail";
    private static final String REFUND_TRANSACTION = "refundTransaction";
    private static final String FORM_REFUND = "cardRefundDetailForm";
    private static final String PTID = "ptID";
    private static final String SESSION_KEY_CARD_NUMBER = "refundCardNumber";
    private static final String SESSION_KEY_CARD_EXPIRY = "refundCardExpiry";
    private static final String INVALID_PTID = "### Invalid ptID in the request. ###";
    
    @Autowired
    private CardProcessingManager cardProcessingManager;
    
    @Autowired
    private CardProcessorFactory cardProcessorFactory;
    
    @Autowired
    private ProcessorTransactionService processorTransactionService;
    
    @Autowired
    private PaymentCardService paymentCardService;
    
    @Autowired
    private CardRefundValidator cardRefundValidator;
    
    @Autowired
    private CardNumberValidator<CardRefundForm> cardValidator;
    
    @Autowired
    private PurchaseEmailAddressService purchaseEmailAddressService;
    
    @Autowired
    private CryptoAlgorithmFactory cryptoAlgorithmFactory;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    @Autowired
    private CPSDataService cpsDataService;
    
    @Autowired
    private CoreCPSService coreCPSService;
    
    @Autowired
    private TLSUtils tlsUtils;
    
    public void setMerchantAccountService(final MerchantAccountService merchantAccountService) {
        this.merchantAccountService = merchantAccountService;
    }
    
    public void setProcessorTransactionService(final ProcessorTransactionService processorTransactionService) {
        this.processorTransactionService = processorTransactionService;
    }
    
    public void setPaymentCardService(final PaymentCardService paymentCardService) {
        this.paymentCardService = paymentCardService;
    }
    
    public void setCardRefundValidator(final CardRefundValidator cardRefundValidator) {
        this.cardRefundValidator = cardRefundValidator;
    }
    
    /**
     * This method will view the refund detail when refund button is clicked
     * from the transaction list page.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return string vm URL (/cardManagement/refundDetailForm.vm) if process
     *         succeeds, Null with 400 or 401 if fails.
     */
    @RequestMapping(value = "/secure/cardManagement/refundDetailForm.html", method = RequestMethod.GET)
    public String showRefundDetails(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_REFUND) final WebSecurityForm<CardRefundForm> cardRefundDetailForm) {
        
        /* validate if login user has enough permission. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_ISSUE_REFUNDS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        /* validate the randomized routeID from request parameter. */
        final String actualPtId = this.validateProcessorTransactionId(request);
        if (actualPtId.equals(WebCoreConstants.RESPONSE_FALSE)) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }
        
        /* initialize the CardRefundForm. */
        final CardRefundForm refundForm = cardRefundDetailForm.getWrappedObject();
        cardRefundDetailForm.setActionFlag(WebSecurityConstants.ACTION_REFUND);
        
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        /* prepare refund transaction detail. */
        final CardRefundDetails cardRefundDetails = this.prepareCardRefundDetails(new Long(actualPtId));
        refundForm.setRandomProcessorTransactionId(request.getParameter(PTID));
        refundForm.setRefundCardExpiry((String) sessionTool.getAttribute(SESSION_KEY_CARD_EXPIRY));
        refundForm.setRefundAmount(cardRefundDetails.getCardPaidAmount());
        
        model.put(FORM_REFUND, cardRefundDetailForm);
        model.put("cardRefundDetails", cardRefundDetails);
        
        return "/cardManagement/refundDetailForm";
    }
    
    /**
     * This method executes the actual credit card refund process.
     * 
     * @param webSecurityForm
     *            User input form
     * @param result
     *            BindingResult object to hold validation errors
     * @param request
     *            HttpServletRequest object
     * @return JSON string value (refund transaction list)
     * @throws CryptoException 
     */
    @RequestMapping(value = "/secure/cardManagement/processRefund.html", method = RequestMethod.POST)
    @ResponseBody
    public String processRefund(@ModelAttribute(FORM_REFUND) final WebSecurityForm<CardRefundForm> webSecurityForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response) throws CryptoException {
        
        String timeZoneStr = WebSecurityUtil.getCustomerTimeZone();
        if (timeZoneStr == null) {
            timeZoneStr = WebCoreConstants.SERVER_TIMEZONE_GMT;
        }
        
        // validate if login user has enough permission.
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_ISSUE_REFUNDS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        if (this.tlsUtils.shouldRedirect(request)) {
            LOG.error("A user attempted to do a refund from a host that was indicated they should not be able to acces refunds from");
            webSecurityForm.resetToken();
            webSecurityForm.addErrorStatus(new FormErrorStatus("postToken", this.cardRefundValidator.getMessage("error.common.user.not.redirected")));
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }

        if (!this.cardRefundValidator.validateMigrationStatus(webSecurityForm)) {
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        // validate user input.
        this.cardRefundValidator.validateProcessorTransaction(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        final CardRefundForm form = webSecurityForm.getWrappedObject();
        final CPSData cpsData = this.cpsDataService.findCPSDataByProcessorTransactionIdAndRefundTokenIsNull(form.getProcessorTransactionId());
        
        // isCpsTransaction is for the Scala microservices.
        final boolean isCpsTransaction = cpsData != null;
        form.setIsCPS(isCpsTransaction);
        
        /* execute refund transaction process. */
        final ProcessorTransaction original = this.processorTransactionService.findProcessorTransactionById(form.getProcessorTransactionId(), true);
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final MerchantAccount merchantAccount = this.merchantAccountService.findById(original.getMerchantAccount().getId());
        
        if (merchantAccount != null && merchantAccount.getTerminalToken() != null
            && (merchantAccount.getIsLink()
                || merchantAccount.getMerchantStatusType().getId() == WebCoreConstants.MERCHANT_STATUS_TYPE_MIGRATED_ID)) {
            // IRIS-3739 Refund support for transactions that happened in Iris before the
            // merchant moved to CPS
            // find and use the new migrated link merchant account
            final MerchantAccount linkMerchantAccount =
                    this.merchantAccountService.findByTerminalTokenAndIsLink(original.getMerchantAccount().getTerminalToken());
            if (form != null && StringUtils.isNotBlank(form.getCardNumber()) && StringUtils.isNotBlank(form.getRefundCardExpiry())
                && merchantAccount != null && merchantAccount.getMerchantStatusType() != null
                && merchantAccount.getMerchantStatusType().getId() == WebCoreConstants.MERCHANT_STATUS_TYPE_MIGRATED_ID && !isCpsTransaction) {
                // push to CoreCPS
                final String chargeToken = pushTransactionToCPS(form, original, linkMerchantAccount);
                // add CPSData
                if (StringUtils.isNotBlank(chargeToken)) {
                    this.cpsDataService.save(new CPSData(original, chargeToken));
                }
            }
            
            return processLinkRefund(webSecurityForm, keyMapping, original, linkMerchantAccount);
        } else {
            return processIrisRefund(webSecurityForm, keyMapping, result, original, isCpsTransaction ? cpsData.getChargeToken() : null,
                                     isCpsTransaction);
        }
    }
    
    private String pushTransactionToCPS(final CardRefundForm form, final ProcessorTransaction original, final MerchantAccount merchantAccount) {
        final String accountNumber = CardProcessingUtil.createAccountNumber(form.getCardNumber(), form.getRefundCardExpiry());
        final Tokens tokens = this.coreCPSService.sendProcessedTransactionToCoreCPS(original.getPurchase(), original, merchantAccount, accountNumber);
        final UUID chargeToken = tokens.getToken(Tokens.TokenTypes.CHARGE_TOKEN);
        return chargeToken == null ? null : chargeToken.toString();
    }
    
    private String processLinkRefund(final WebSecurityForm<CardRefundForm> webSecurityForm, final RandomKeyMapping keyMapping,
        final ProcessorTransaction original, final MerchantAccount merchantAccount) throws CryptoException {
        final Collection<ProcessorTransaction> existing =
                this.processorTransactionService.findProcessorTransactionByPurchaseIdAndIsApprovedAndIsRefund(original.getPurchase().getId());
        
        if (existing == null || existing.isEmpty()) {
            final ProcessorTransaction procTrans = this.cardProcessingManager.processLinkCPSTransactionRefund(original, merchantAccount);
            
            if (procTrans == null || !procTrans.isIsApproved()) {
                webSecurityForm
                        .addErrorStatus(new FormErrorStatus(REFUND_TRANSACTION, this.cardRefundValidator.getMessage(ERROR_REFUND_TRANSACTION_FAIL)));
                return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
                
            }
            
            final Purchase purchase = original.getPurchase();
            final TransactionReceiptDetails detail = this.purchaseEmailAddressService.getTransactionReceiptDetails(purchase);
            detail.setPurchaseRandomId(keyMapping.getRandomString(purchase, WebCoreConstants.ID_LOOK_UP_NAME));
            return WidgetMetricsHelper.convertToJson(detail, TRANSACTION_RECEIPT_DETAILS, true);
        } else {
            webSecurityForm
                    .addErrorStatus(new FormErrorStatus(REFUND_TRANSACTION, this.cardRefundValidator.getMessage("error.refund.duplicate.refund")));
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
    }
    
    // isCpsTransaction is for the Scala microservices.
    private String processIrisRefund(final WebSecurityForm<CardRefundForm> webSecurityForm, final RandomKeyMapping keyMapping,
        final BindingResult result, final ProcessorTransaction pt, final String chargeToken, final Boolean isCpsTransaction) {
        
        final CardRefundForm form = webSecurityForm.getWrappedObject();
        
        this.cardRefundValidator.validate(webSecurityForm, result);
        // validate session data
        if (this.cardProcessorFactory.isProcessorPaused(pt.getMerchantAccount().getProcessor().getId())) {
            webSecurityForm
                    .addErrorStatus(new FormErrorStatus("", this.cardRefundValidator.getMessage("error.cardManagement.cardRefund.processorPaused")));
        } else if (!isCpsTransaction) {
            // The only time user will see error message about invalid card number here should be when they are hacking our system !
            this.cardValidator.validateCreditCardNumber(webSecurityForm, SESSION_KEY_CARD_NUMBER, "label.settings.cardSettings.CardNumber",
                                                        form.getCardNumber());
            
        }
        
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            // return the error status with JSON format to UI.
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        String cardNumber = form.getCardNumber();
        webSecurityForm.resetToken();
        
        // Validate the data
        if (!isCpsTransaction) {
            try {
                final String cardExpiry = form.getRefundCardExpiry();
                final String cardHash = this.cryptoAlgorithmFactory.getSha1Hash(CardProcessingUtil.createAccountNumber(cardNumber, cardExpiry),
                                                                                CryptoConstants.HASH_CREDIT_CARD_PROCESSING);
                final String cardHashInPT = pt.getCardHash();
                if (StringUtils.isNotBlank(cardHash) && !cardHash.equals(cardHashInPT)) {
                    LOG.error("!! ProcessorTransaction id: " + pt.getId() 
                        + ", is cardHashInPT null? " + cardHashInPT + ", doesn't match with input hash: " + cardHash);
                    webSecurityForm.addErrorStatus(new FormErrorStatus("cardNumber",
                            this.cardRefundValidator.getMessage("error.refund.carddetails.incorrect")));
                    return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL,
                                                             true);
                }
            } catch (CryptoException e) {
                LOG.error("Error while decrypting cardData from Session !", e);
                cardNumber = null;
                return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
            }
        }
        
        final ProcessorTransaction resultPT;
        
        if (isCpsTransaction) {
            resultPT = this.cardProcessingManager.processCPSTransactionRefund(pt, chargeToken);
        } else {
            resultPT = this.cardProcessingManager.processTransactionRefund(pt, cardNumber, form.getRefundCardExpiry(), false);
        }
        
        /* resultPT will be null if the refund process fails. */
        if (resultPT == null || !resultPT.isIsApproved() && resultPT.getCPSResponseMessage() == null) {
            webSecurityForm
                    .addErrorStatus(new FormErrorStatus(REFUND_TRANSACTION, this.cardRefundValidator.getMessage(ERROR_REFUND_TRANSACTION_FAIL)));
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
            
        } else if (!resultPT.isIsApproved() && resultPT.getCPSResponseMessage() != null) {
            webSecurityForm.addErrorStatus(new FormErrorStatus(REFUND_TRANSACTION, resultPT.getCPSResponseMessage()));
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        final Purchase purchase = pt.getPurchase();
        final TransactionReceiptDetails detail = this.purchaseEmailAddressService.getTransactionReceiptDetails(purchase);
        detail.setPurchaseRandomId(keyMapping.getRandomString(purchase, WebCoreConstants.ID_LOOK_UP_NAME));
        return WidgetMetricsHelper.convertToJson(detail, TRANSACTION_RECEIPT_DETAILS, true);
    }
    
    /**
     * This method prepares card refund detail information when refund button is clicked.
     * 
     * @param processorTransactionId
     *            ProcessorTransaction ID
     * @return CardRefundDetails object
     */
    private CardRefundDetails prepareCardRefundDetails(final Long processorTransactionId) {
        TimeZone timeZone = null;
        if (WebSecurityUtil.getCustomerTimeZone() == null) {
            timeZone = TimeZone.getTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT);
        } else {
            timeZone = TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone());
        }
        
        /* retrieve ProcessorTransaction and initialize CardRefundDetails instance to return to UI. */
        final ProcessorTransaction pt = this.processorTransactionService.findTransactionPosPurchaseById(processorTransactionId);
        final CardRefundDetails cardRefundDetails = new CardRefundDetails();
        
        if (pt != null) {
            final PointOfSale pos = pt.getPointOfSale();
            if (pos != null) {
                cardRefundDetails.setPaystationName(pos.getName());
                cardRefundDetails.setPaystationSerialNumber(pos.getSerialNumber());
            }
            
            final PooledResource<DateFormat> dateFmt = DateUtil.takeDateFormat(WebCoreConstants.DATE_FORMAT, timeZone);
            
            cardRefundDetails.setPurchasedDate(dateFmt.get().format(pt.getPurchasedDate()));
            cardRefundDetails.setStatusName(pt.getProcessorTransactionType().getName());
            cardRefundDetails.setAuthorizationNumber(pt.getSanitizedAuthorizationNumber());
            cardRefundDetails.setReferenceNumber(pt.getReferenceNumber());
            cardRefundDetails.setSettlementDate(dateFmt.get().format(pt.getProcessingDate()));
            cardRefundDetails.setCardNumber(CardProcessingUtil.covertLast4DigitsOfCardNumberToString(pt.getLast4digitsOfCardNumber()));
            
            /* retrieve Purchase. */
            final String refundableAmount = WebCoreUtil.formatBase100Value(pt.getAmount());
            final Purchase purchase = pt.getPurchase();
            if (purchase == null) {
                cardRefundDetails.setTicketNumber(pt.getTicketNumber());
                cardRefundDetails.setCardPaidAmount(refundableAmount);
            } else {
                /* retrieve PaymentCard. */
                final PaymentCard paymentCard =
                        this.paymentCardService.findPaymentCardByPurchaseIdAndProcessorTransactionId(purchase.getId(), pt.getId());
                if (paymentCard != null) {
                    cardRefundDetails.setPurchaseId(paymentCard.getPurchase().getId());
                    cardRefundDetails.setCardType(paymentCard.getCreditCardType().getName());
                }
                
                if (purchase.getPaystationSetting() != null) {
                    cardRefundDetails.setPaystationSettingName(purchase.getPaystationSetting().getName());
                }
                
                if (purchase.getTransactionType() != null) {
                    cardRefundDetails.setTransactionType(purchase.getTransactionType().getName());
                }
                if (purchase.getPaymentType() != null) {
                    cardRefundDetails.setPaymentType(purchase.getPaymentType().getName());
                }
                
                cardRefundDetails.setChargedAmount(WebCoreUtil.formatBase100Value(purchase.getChargedAmount()));
                cardRefundDetails.setCashPaidAmount(WebCoreUtil.formatBase100Value(purchase.getCashPaidAmount()));
                cardRefundDetails.setCardPaidAmount(refundableAmount);
                cardRefundDetails.setExcessPaymentAmount(WebCoreUtil.formatBase100Value(purchase.getExcessPaymentAmount()));
                cardRefundDetails.setChangeDispensedAmount(WebCoreUtil.formatBase100Value(purchase.getChangeDispensedAmount()));
                if (purchase.getUnifiedRate() != null) {
                    cardRefundDetails.setRate(purchase.getUnifiedRate().getName());
                }
                // TODO set refund slip ???
                
                final Permit permit = purchase.getFirstPermit();
                if (permit != null) {
                    cardRefundDetails.setExpiryDate(dateFmt.get().format(permit.getPermitExpireGmt()));
                    if (permit.getLicencePlate() != null) {
                        cardRefundDetails.setPlateNumber(permit.getLicencePlate().getNumber());
                    }
                    cardRefundDetails.setSpaceNumber(permit.getSpaceNumber());
                    cardRefundDetails.setTicketNumber(permit.getPermitNumber());
                    cardRefundDetails.setPermitType(permit.getPermitType().getName());
                }
            }
            
            dateFmt.close();
        }
        
        return cardRefundDetails;
    }
    
    /**
     * This method validates user input randomized processor transaction ID.
     * 
     * @param request
     *            HttpServletRequest object
     * @return actual processor transaction ID string value if process succeeds,
     *         "false" string if process fails.
     */
    private String validateProcessorTransactionId(final HttpServletRequest request) {
        /* validate the randomized ptID from request parameter. */
        final String randomizedPtId = request.getParameter(PTID);
        if (StringUtils.isBlank(randomizedPtId) || !DashboardUtil.validateRandomId(randomizedPtId)) {
            LOG.warn(INVALID_PTID);
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        /* validate the actual ptID. */
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Object actualPtId = keyMapping.getKey(randomizedPtId);
        if (actualPtId == null || !DashboardUtil.validateDBIntegerPrimaryKey(actualPtId.toString())) {
            LOG.warn(INVALID_PTID);
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return actualPtId.toString();
    }
    
    @ModelAttribute(FORM_REFUND)
    public WebSecurityForm<CardRefundForm> initRefundForm(final HttpServletRequest request) {
        return new WebSecurityForm<CardRefundForm>((Object) null, new CardRefundForm(),
                SessionTool.getInstance(request).locatePostToken(FORM_REFUND));
    }
}
