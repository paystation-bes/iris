package com.digitalpaytech.mvc.customeradmin;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.constants.SortOrder;
import com.digitalpaytech.domain.CardRetryTransaction;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerBadCard;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.BadCardData;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.comparator.BannedCardComparator;
import com.digitalpaytech.dto.customeradmin.BannedCard;
import com.digitalpaytech.dto.customeradmin.BannedCardInfo;
import com.digitalpaytech.dto.customeradmin.BannedCardList;
import com.digitalpaytech.dto.customeradmin.CustomerBadCardSearchCriteria;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.InvalidCSVDataException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.mvc.customeradmin.support.BannedCardEditForm;
import com.digitalpaytech.mvc.customeradmin.support.BannedCardSearchForm;
import com.digitalpaytech.mvc.customeradmin.support.BannedCardUploadForm;
import com.digitalpaytech.mvc.customeradmin.support.io.BadCardCsvProcessor;
import com.digitalpaytech.mvc.customeradmin.support.io.CustomerBadCardCsvReader;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.BannedCardService;
import com.digitalpaytech.service.BatchProcessingException;
import com.digitalpaytech.service.CardRetryTransactionService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerBadCardService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.LinkBadCardService;
import com.digitalpaytech.service.crypto.CryptoAlgorithmFactory;
import com.digitalpaytech.service.kpi.KPIListenerService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DashboardUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.TLSUtils;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.crypto.CryptoConstants;
import com.digitalpaytech.util.csv.CsvProcessingError;
import com.digitalpaytech.validation.customeradmin.CardManagementEditValidator;
import com.digitalpaytech.validation.customeradmin.CardManagementSearchValidator;
import com.digitalpaytech.validation.customeradmin.CardManagementUploadValidator;

/**
 * This controller takes care of user request from customer admin card
 * management page.
 * 
 * @author Brian Kim
 * 
 */
@Controller
@SuppressWarnings({ "PMD.ExcessiveImports", "PMD.GodClass", "PMD.TooManyFields" })
public class CardManagementController {
    private static final String FILE_PARAM_NAME = "file";
    private static final String FORM_SEARCH = "bannedCardSearchForm";
    private static final String FORM_EDIT = "bannedCardEditForm";
    private static final String FORM_UPLOAD = "bannedCardUploadForm";
    private static final String COLUMN_NAME_CARD_EXPIRY = "CardExpiry";
    private static final String COLUMN_NAME_LAST_4_DIGITS_OF_CARD_NUMBER = "Last4DigitsOfCardNumber";
    private static final String CUSTOMER_BAD_CARD_ID_PARAMETER = "customerBadCardID";
    
    private static final Logger LOG = Logger.getLogger(CardManagementController.class);
    
    private Map<String, String> customerBadCardBannedCardMapping;
    
    @Autowired
    private LinkBadCardService linkBadCardService;
    
    @Autowired
    private BannedCardService bannedCardService;
    
    @Autowired
    private CardRetryTransactionService cardRetryTransactionService;
    
    @Autowired
    private CustomerBadCardService customerBadCardService;
    
    @Autowired
    private CustomerCardTypeService customerCardTypeService;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    @Autowired
    private CryptoService cryptoService;
    
    @Autowired
    private CardManagementSearchValidator cardManagementSearchValidator;
    
    @Autowired
    private CardManagementEditValidator cardManagementEditValidator;
    
    @Autowired
    private CardManagementUploadValidator uploadValidator;
    
    @Autowired
    private CustomerBadCardCsvReader badCreditCardCsvReader;
    
    @Autowired
    private EmsPropertiesService emsprops;
    
    @Autowired
    private KPIListenerService kpiListenerService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private CryptoAlgorithmFactory cryptoAlgorithmFactory;
    
    @Autowired
    private BadCardCsvProcessor badCardCsvProcessor;
    
    @Autowired
    private TLSUtils tlsUtils;
    
    @PostConstruct
    private void createMapping() {
        this.customerBadCardBannedCardMapping = new HashMap<>(StandardConstants.CONSTANT_5);
        this.customerBadCardBannedCardMapping.put("cardNumberOrHashAsInt", COLUMN_NAME_LAST_4_DIGITS_OF_CARD_NUMBER);
        this.customerBadCardBannedCardMapping.put("cct.name", "CardType");
        this.customerBadCardBannedCardMapping.put(CardRetryTransaction.COLUMN_NAME_CARD_EXPIRY, COLUMN_NAME_CARD_EXPIRY);
        this.customerBadCardBannedCardMapping.put("addedGmt", "AddedGMT");
        this.customerBadCardBannedCardMapping.put("source", "Source");
        
    }
    
    public final void setCardRetryTransactionService(final CardRetryTransactionService cardRetryTransactionService) {
        this.cardRetryTransactionService = cardRetryTransactionService;
    }
    
    public final void setLinkBadCardService(final LinkBadCardService linkBadCardService) {
        this.linkBadCardService = linkBadCardService;
    }
    
    public final void setCustomerBadCardService(final CustomerBadCardService customerBadCardService) {
        this.customerBadCardService = customerBadCardService;
    }
    
    public final void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    public final void setCryptoService(final CryptoService cryptoService) {
        this.cryptoService = cryptoService;
    }
    
    public final void setCardManagementSearchValidator(final CardManagementSearchValidator cardManagementSearchValidator) {
        this.cardManagementSearchValidator = cardManagementSearchValidator;
    }
    
    public final void setCardManagementEditValidator(final CardManagementEditValidator cardManagementEditValidator) {
        this.cardManagementEditValidator = cardManagementEditValidator;
    }
    
    public final void setBadCreditCardCsvReader(final CustomerBadCardCsvReader badCreditCardCsvReader) {
        this.badCreditCardCsvReader = badCreditCardCsvReader;
    }
    
    public final void setCustomerCardTypeService(final CustomerCardTypeService customerCardTypeService) {
        this.customerCardTypeService = customerCardTypeService;
    }
    
    public final void setCryptoAlgorithmFactory(final CryptoAlgorithmFactory cryptoAlgorithmFactory) {
        this.cryptoAlgorithmFactory = cryptoAlgorithmFactory;
    }
    
    /**
     * This method shows card management main search page. It retrieves card
     * type list and returns back to UI with user input form.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return vm URL (/cardManagement/index.vm) string value if process
     *         succeeds, Null with 401 if fails.
     * @throws CryptoException
     */
    @RequestMapping(value = "/secure/cardManagement/index.html", method = RequestMethod.GET)
    public String showBannedCardFilter(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_SEARCH) final WebSecurityForm<BannedCardSearchForm> bannedCardSearchForm,
        @ModelAttribute(FORM_EDIT) final WebSecurityForm<BannedCardEditForm> bannedCardEditForm,
        @ModelAttribute(FORM_UPLOAD) final WebSecurityForm<BannedCardUploadForm> bannedCardUploadForm) throws CryptoException {
        
        /* validate if login user has enough permission. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_BANNED_CARDS)
            && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_BANNED_CARDS)) {
            if (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_ISSUE_REFUNDS)) {
                return "redirect:/secure/cardManagement/cardRefund.html";
            } else {
                if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                    && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                    return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
                }
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
            }
        }
        
        final Customer customer = WebSecurityUtil.getUserAccount().getCustomer();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        /* initialize the BannedCardSearchForm. */
        bannedCardSearchForm.setActionFlag(WebSecurityConstants.ACTION_SEARCH);
        
        /* retrieve card type list. */
        List<FilterDTO> cardTypeList = new ArrayList<FilterDTO>();
        cardTypeList.add(new FilterDTO("0", "All"));
        
        cardTypeList = this.customerCardTypeService.getBannableCardTypeFilters(cardTypeList, customer.getId(), keyMapping);
        
        model.put(FORM_EDIT, bannedCardEditForm);
        model.put(FORM_SEARCH, bannedCardSearchForm);
        model.put(FORM_UPLOAD, bannedCardUploadForm);
        
        model.put("cardTypeList", cardTypeList);
        
        return "/cardManagement/index";
    }
    
    /**
     * This method searches & retrieves CustomerBadCard information and passes
     * JSON string to UI.
     * 
     * @param webSecurityForm
     *            User input form
     * @param result
     *            BindingResult object to hold error status.
     * @param request
     *            HttpServletRequest object
     * @return JSON string of BannedCardInfo list if process succeeds,
     *         errorStatus JSON string if fails.
     * @throws CryptoException
     *             this exception could occur while creating card hash value.
     */
    @RequestMapping(value = "/secure/cardManagement/searchBannedCard.html", method = RequestMethod.POST)
    public @ResponseBody String searchBannedCard(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<BannedCardSearchForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response)
        throws CryptoException, InvalidDataException {
        
        /* validate if login user has enough permission. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_BANNED_CARDS)
            && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_BANNED_CARDS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        /* validate search filter. */
        this.cardManagementSearchValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        final BannedCardSearchForm form = webSecurityForm.getWrappedObject();
        if (form.getCustomerCardTypeId() != null) {
            final CustomerCardType cct = this.customerCardTypeService.getCardType(form.getCustomerCardTypeId());
            form.setCustomerCardType(cct);
            form.setCustomerCardTypeId(cct.getId());
            form.setCardTypeId(cct.getCardType().getId());
        }
        this.cardManagementSearchValidator.validateCardNumber(webSecurityForm);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        if (this.tlsUtils.shouldRedirect(request) && StringUtils.isNotBlank(form.getFilterCardNumber())
            && form.getFilterCardNumber().length() > CardProcessingConstants.CREDIT_CARD_LAST_4_DIGITS_LENGTH) {
            webSecurityForm.addAllErrorStatus(Collections.singletonList(new FormErrorStatus("FilterCardNumber",
                    this.cardManagementSearchValidator.getMessage("error.common.user.not.redirected"))));
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        /* retrieve all CustomerBadCard list. */
        final BannedCardList bannedCardListInfo = this.prepareBannedCardSearchResult(request, form, null);
        bannedCardListInfo.setToken(webSecurityForm.getInitToken());
        
        /* convert CardRefundListInfo collection to JSON string. */
        return WidgetMetricsHelper.convertToJson(bannedCardListInfo, "bannedCardList", true);
        
    }
    
    /**
     * This method shows the detailed CustomerBadCard information when "View" is
     * clicked on the page. When "Edit" is clicked, it will also be called from
     * front-end logic automatically after calling getBannedCardForm() method.
     * 
     * @param request
     *            HttpServletRequest object.
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return JSON string of BannedCardInfo object if process succeeds, "false"
     *         string if fails.
     */
    @RequestMapping(value = "/secure/cardManagement/viewBannedCardDetail.html", method = RequestMethod.GET)
    public String viewBannedCardDetail(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate if login user has enough permission. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_BANNED_CARDS)
            && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_BANNED_CARDS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        final String actualBadCardId = this.validateCustomerBadCardId(request);
        if (actualBadCardId.equals(WebCoreConstants.RESPONSE_FALSE)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final BannedCardInfo bannedCardInfo = this.prepareBannedCardDetails(new Integer(actualBadCardId));
        
        /* convert BannedCardInfo collection to JSON string. */
        final String json = WidgetMetricsHelper.convertToJson(bannedCardInfo, "bannedCardInfo", true);
        if (StringUtils.isBlank(json)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return json;
    }
    
    /**
     * This method will be called when "Add" or "Edit" is clicked in order to
     * display user input form.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return vm URL (/cardManagement/bannedCardForm.vm) if process succeeds,
     *         Null with 401 if fails.
     */
    @RequestMapping(value = "/secure/cardManagement/bannedCardForm.html", method = RequestMethod.GET)
    public String getBannedCardForm(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate if login user has enough permission. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_BANNED_CARDS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        if (!WebSecurityUtil.isCustomerMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + ":" + this.cardManagementEditValidator.getMessage("error.customer.notMigrated");
        }
        
        return "/cardManagement/bannedCardForm";
    }
    
    /**
     * This method saves banned card in CustomerBadCard table.
     * 
     * @param webSecurityForm
     *            User input WebSecurityForm form
     * @param result
     *            BindingResult object holding error status.
     * @param request
     *            HttpServletRequest object
     * @return JSON string value (updated new BannedCardInfo object list) if
     *         process succeeds, errorStatus JSON string if fails.
     * @throws CryptoException
     *             It occurs when hashing the card number.
     */
    @RequestMapping(value = "/secure/cardManagement/saveBannedCard.html", method = RequestMethod.POST)
    public @ResponseBody String saveBannedCard(@ModelAttribute(FORM_EDIT) final WebSecurityForm<BannedCardEditForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response) throws CryptoException {
        
        /* validate if login user has enough permission. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_BANNED_CARDS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        if (!this.cardManagementEditValidator.validateMigrationStatus(webSecurityForm)) {
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        /* validate user input. */
        this.cardManagementEditValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        webSecurityForm.resetToken();
        
        /* retrieve all BannedCardEditForm list. */
        final BannedCardEditForm form = webSecurityForm.getWrappedObject();
        
        if (this.tlsUtils.shouldRedirect(request) && form.getCardTypeId() == CardProcessingConstants.CARD_TYPE_CREDIT_CARD) {
            webSecurityForm.addAllErrorStatus(Collections.singletonList(new FormErrorStatus("CardTypeId",
                    this.cardManagementSearchValidator.getMessage("error.common.user.not.redirected"))));
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final Customer customer = userAccount.getCustomer();
        
        final CustomerCardType customerCardType = this.customerCardTypeService.getCardType(form.getCardTypeId());
        
        if (customerCardType.getCardType().getId() == CardProcessingConstants.CARD_TYPE_CREDIT_CARD) {
            return addLinkBadCard(webSecurityForm, form, customer);
        } else {
            return addIrisBadCard(webSecurityForm, form, userAccount, customer.getId());
        }
    }
    
    private String addIrisBadCard(final WebSecurityForm<BannedCardEditForm> webSecurityForm, final BannedCardEditForm form,
        final UserAccount userAccount, final int customerId) throws CryptoException {
        boolean duplicated = false;
        final Collection<CustomerBadCard> badCardsList =
                this.customerBadCardService.getCustomerBadCards(customerId, form.getCardNumber(), form.getCardTypeId());
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_CREATE) {
            duplicated = !badCardsList.isEmpty();
            if (!duplicated) {
                /* Add CustomerBadCard. */
                addCustomerBadCard(form, userAccount);
            }
        } else if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
            final Iterator<CustomerBadCard> itr = badCardsList.iterator();
            while ((!duplicated) && (itr.hasNext())) {
                final CustomerBadCard dbBad = itr.next();
                duplicated = !dbBad.getId().equals(form.getCustomerBadCardId());
            }
            
            if (!duplicated) {
                /* Edit CustomerBadCard. */
                editCustomerBadCard(form, userAccount);
            }
        }
        
        if (duplicated) {
            webSecurityForm.addErrorStatus(
                                           new FormErrorStatus("cardType, cardNumber",
                                                   this.cardManagementEditValidator.getMessage("error.common.duplicated", new Object[] {
                                                       this.cardManagementEditValidator.getMessage("label.cardManagement.bannedCard"),
                                                       this.cardManagementEditValidator.getMessage("label.settings.cardSettings.CardNumber") })));
            
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        } else {
            /* convert CardRefundListInfo collection to JSON string. */
            return WebCoreConstants.RESPONSE_TRUE + ":" + webSecurityForm.getInitToken();
        }
    }
    
    private String addLinkBadCard(final WebSecurityForm<BannedCardEditForm> webSecurityForm, final BannedCardEditForm form, final Customer customer)
        throws CryptoException {
        
        final BadCardData badCardData = new BadCardData(form.getCardNumber(), form.getCardExpiry(), form.getCardTypeId(), form.getComment(), customer,
                WebCoreConstants.BADCARD_SOURCE_MANUAL);
        
        if (StringUtils.isNumeric(badCardData.getCardNumber()) && (badCardData.getExpiry() == null || StringUtils.isNumeric(badCardData.getExpiry()))
            && this.linkBadCardService.addBadCard(badCardData)) {
            return WebCoreConstants.RESPONSE_TRUE + ":" + webSecurityForm.getInitToken();
        }
        
        webSecurityForm.addErrorStatus(new FormErrorStatus("cardType, cardNumber",
                this.cardManagementEditValidator.getMessage("error.cardManagement.add.banned.card.fail")));
        return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        
    }
    
    /**
     * This method receives a request to delete banned card and then checks if the parameter [customerBadCardID] matches a UUID pattern AND customer has a Link
     * Merchant Account, in this case, send the deletion to [Bad Card Microservice], otherwise, deletes from CustomerBadCard table. By design,
     * the process will delete the physical data from table.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return "true" string value if process succeeds, "false" if fails.
     * @throws CryptoException
     */
    @ResponseBody
    @RequestMapping(value = "/secure/cardManagement/deleteBannedCard.html", method = RequestMethod.GET)
    public final String deleteBannedCard(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model)
        throws CryptoException {
        
        /* validate if login user has enough permission. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_BANNED_CARDS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        if (!WebSecurityUtil.isCustomerMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + ":" + this.cardManagementEditValidator.getMessage("error.customer.notMigrated");
        }
        
        final Customer customer = this.customerService.findCustomerDetached(WebSecurityUtil.getUserAccount().getCustomer().getId());
        
        final String badCardUuid = this.validaterBadCardUUID(request);
        final boolean isBadCardUuid = badCardUuid.equals(WebCoreConstants.RESPONSE_FALSE) ? Boolean.FALSE : Boolean.TRUE;
        
        if (isBadCardUuid) {
            if (customer.getUnifiId() == null) {
                LOG.error("Error UnifiId is null");
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            return processDeleteLinkBadCard(badCardUuid, request, customer);
        } else {
            return processDeleteIrisBadCard(request);
        }
    }
    
    private String processDeleteIrisBadCard(final HttpServletRequest request) {
        final String actualBadCardId = this.validateCustomerBadCardId(request);
        if (actualBadCardId.equals(WebCoreConstants.RESPONSE_FALSE)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final CustomerBadCard customerBadCard = this.customerBadCardService.findCustomerBadCardById(Integer.parseInt(actualBadCardId), true);
        if (customerBadCard != null) {
            deleteIrisBadCard(customerBadCard);
        } else {
            
            final CardRetryTransaction cardRetry = this.cardRetryTransactionService.findCardRetryTransaction(Long.parseLong(actualBadCardId));
            if (cardRetry != null) {
                cardRetry.setIgnoreBadCard(true);
                this.cardRetryTransactionService.updateCardRetryTransaction(cardRetry);
            } else {
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    private String processDeleteLinkBadCard(final String badCardUUID, final HttpServletRequest request, final Customer customer)
        throws CryptoException {
        
        final boolean deleteLinkBadCard = deleteLinkBadCard(badCardUUID, customer);
        if (!deleteLinkBadCard) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    private final void deleteIrisBadCard(final CustomerBadCard customerBadCard) {
        this.customerBadCardService.deleteCustomerBadCard(customerBadCard);
    }
    
    private final boolean deleteLinkBadCard(final String UUID, final Customer customer) throws CryptoException {
        final BadCardData badCardData = new BadCardData();
        badCardData.setId(UUID);
        badCardData.setCustomer(customer);
        badCardData.setSource(WebCoreConstants.BADCARD_SOURCE_MANUAL);
        
        return this.linkBadCardService.deleteBadCard(badCardData);
    }
    
    /**
     * This method is invoked when "Import" is clicked from the page.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return vm URL (/cardManagement/importBannedCardForm) string value if
     *         process succeeds, Null with 401 if fails.
     */
    @RequestMapping(value = "/secure/cardManagement/importBannedCardForm.html", method = RequestMethod.GET)
    public String getBannedCardImportForm(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate if login user has enough permission. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_BANNED_CARDS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        if (!WebSecurityUtil.isCustomerMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + ":" + this.cardManagementEditValidator.getMessage("error.customer.notMigrated");
        }
        
        /* initialize the BannedCardUploadForm. */
        final WebSecurityForm<BannedCardUploadForm> bannedCardUploadForm =
                new WebSecurityForm<BannedCardUploadForm>(null, new BannedCardUploadForm());
        
        model.put(FORM_UPLOAD, bannedCardUploadForm);
        
        return "/cardManagement/importBannedCardForm";
    }
    
    /**
     * This method processes the import of banned card and inserts card
     * information to CustomerBadCard table.
     * 
     * @param webSecurityForm
     *            Banned card upload form
     * @param request
     *            HttpServletRequest object
     * @return JSON string value
     */
    @RequestMapping(value = "/secure/cardManagement/importBannedCard.html", method = RequestMethod.POST)
    public ResponseEntity<String> importBannedCard(@ModelAttribute(FORM_UPLOAD) final WebSecurityForm<BannedCardUploadForm> webSecurityForm,
        final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model)
        throws CryptoException {
        String result = WebCoreConstants.RESPONSE_FALSE;
        /* validate if login user has enough permission. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_BANNED_CARDS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
        } else if (!this.uploadValidator.validateMigrationStatus(webSecurityForm)) {
            result = WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        } else {
            final MessageInfo messages = new MessageInfo();
            
            final UserAccount userAccount = WebSecurityUtil.getUserAccount();
            final Customer customer = this.customerService.findCustomerDetached(WebSecurityUtil.getUserAccount().getCustomer().getId());
            
            if (!this.uploadValidator.handleMultipartExceptions(webSecurityForm, request)) {
                this.uploadValidator.validate(webSecurityForm, bindingResult);
                if (webSecurityForm.getValidationErrorInfo().getErrorStatus().isEmpty()) {
                    
                    final BannedCardUploadForm form = webSecurityForm.getWrappedObject();
                    
                    if (this.kpiListenerService.getKPIService() != null) {
                        this.kpiListenerService.getKPIService().addUIFileSize(form.getFile().getSize());
                    }
                    
                    final int maxFileSize = this.emsprops.getPropertyValueAsInt(EmsPropertiesService.MAX_UPLOAD_FILE_SIZE_IN_BYTES,
                                                                                
                                                                                EmsPropertiesService.DEFAULT_MAX_UPLOAD_FILE_SIZE_IN_BYTES);
                    
                    processBadCardCSV(webSecurityForm, messages, customer, userAccount, maxFileSize);
                    
                }
                
                if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
                    result = WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
                } else if (!messages.hasMessages() && messages.getPollingURI() == null) {
                    result = WebCoreConstants.RESPONSE_TRUE;
                } else {
                    result = WidgetMetricsHelper.convertToJson(messages, "messages", true);
                }
            }
        }
        final HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.TEXT_HTML);
        
        return new ResponseEntity<String>(WebCoreUtil.cleanupIframeJSON(result), responseHeaders, HttpStatus.OK);
    }
    
    private void processBadCardCSV(final WebSecurityForm<BannedCardUploadForm> webSecurityForm, final MessageInfo messages, final Customer customer,
        final UserAccount userAccount, final int maxSize) {
        
        final BannedCardUploadForm bannedCardUploadForm = webSecurityForm.getWrappedObject();
        
        if (bannedCardUploadForm.getFile().getSize() > maxSize) {
            webSecurityForm.addErrorStatus(new FormErrorStatus(FILE_PARAM_NAME,
                    this.uploadValidator.getMessage(ErrorConstants.IMPORT_SIZE_LIMIT_EXCEEDED, maxSize / StandardConstants.BYTES_PER_MEGABYTE)));
        } else {
            messages.setRequireManualClose(true);
            
            InputStream validationInputStream = null;
            InputStream processingInputStream = null;
            try {
                validationInputStream = bannedCardUploadForm.getFile().getInputStream();
                final List<CsvProcessingError> errors = this.badCardCsvProcessor.validate(validationInputStream, customer);
                if (errors.isEmpty()) {
                    final BannedCardUploadForm form = webSecurityForm.getWrappedObject();
                    
                    /* If Replace mode is on, Clean Data on Link and Iris */
                    if (form.getRecordStatus().equals(FormFieldConstants.REPLACE_MODE)) {
                        
                        this.linkBadCardService.deleteBadCardsbyCustomerId(customer.getUnifiId());
                        this.customerBadCardService.deleteByCustomerId(customer.getId(), null);
                    }
                    
                    processingInputStream = bannedCardUploadForm.getFile().getInputStream();
                    this.badCardCsvProcessor.process(processingInputStream, customer, userAccount.getId());
                    
                    messages.addMessage(this.uploadValidator.getMessage("label.cardManagement.bannedCard.importForm.savedMsg"));
                    messages.setRequireManualClose(false);
                    
                } else {
                    final Map<String, StringBuilder> errorsGroup = CsvProcessingError.groupErrors(errors);
                    errorsGroup.keySet().forEach(error -> webSecurityForm
                            .addErrorStatus(new FormErrorStatus(FILE_PARAM_NAME, error + formatLineNumberError(errorsGroup.get(error).toString()))));
                    
                    messages.setRequireManualClose(true);
                }
                
            } catch (IOException e) {
                LOG.error("Error opening bad card file", e);
                webSecurityForm.addErrorStatus(new FormErrorStatus(FILE_PARAM_NAME, this.uploadValidator.getMessage(ErrorConstants.COMMON_IMPORT)));
                
            } catch (BatchProcessingException bpe) {
                LOG.error(bpe.getMessage());
                webSecurityForm.addErrorStatus(new FormErrorStatus(FILE_PARAM_NAME, bpe.getMessage()));
                
            } catch (InvalidCSVDataException icde) {
                LOG.error(icde.getMessage());
                webSecurityForm.addErrorStatus(new FormErrorStatus(FILE_PARAM_NAME, icde.getMessage()));
                
            } finally {
                try {
                    if (validationInputStream != null) {
                        validationInputStream.close();
                    }
                    if (processingInputStream != null) {
                        processingInputStream.close();
                    }
                    
                } catch (IOException e) {
                    LOG.error("Error closing input stream for bad card file", e);
                }
            }
            
        }
    }
    
    private String formatLineNumberError(final String errorMessage) {
        return " [lines# " + errorMessage + "]";
    }
    
    /**
     * This method is NO LONGER being used !
     * 
     * This method processes the export of banned card lists.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     */
    @Deprecated
    @RequestMapping(value = "/secure/cardManagement/exportBannedCard.html", method = RequestMethod.GET)
    public void exportBannedCard(final HttpServletRequest request, final HttpServletResponse response) {
        
        /* validate if login user has enough permission. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_BANNED_CARDS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return;
        }
        
        /* retrieve banned card info of login customer. */
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final Customer customer = userAccount.getCustomer();
        final CustomerBadCardSearchCriteria criteria = createSearchCriteria((BannedCardSearchForm) null, request);
        final Collection<CustomerBadCard> badCards = this.customerBadCardService.findCustomerBadCards(criteria);
        
        final SimpleDateFormat fmt = new SimpleDateFormat(WebCoreConstants.FORMAT_YYYYMMDD);
        fmt.setTimeZone(TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone()));
        final String fileName = this.uploadValidator.getMessage("label.cardManagement.bannedCard.export.fileName", customer.getId(),
                                                                customer.getName(), fmt.format(new Date()));
        
        response.setHeader("Content-Type", "text/csv");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
        response.setHeader("Cache-Control", "public");
        response.setHeader("Pragma", "public");
        
        PrintWriter writer = null;
        try {
            writer = response.getWriter();
            
            final Map<String, Object> context = new HashMap<String, Object>(2);
            
            this.badCreditCardCsvReader.createCsv(writer, badCards, context);
            
        } catch (IOException e) {
            response.setStatus(HttpStatus.EXPECTATION_FAILED.value());
            return;
        } finally {
            if (writer != null) {
                writer.flush();
                writer.close();
            }
        }
    }
    
    /**
     * It retrieves banned card information based on the search input for the
     * banned card and prepare for data to be passed to UI.
     * 
     * @param request
     *            HttpServletRequest object
     * @param cardNumber
     *            Banned card number
     * @param cardTypeId
     *            Card Type ID
     * @param customerId
     *            Customer ID
     * @return Collection of BannedCardInfo objects
     * @throws CryptoException
     *             it occurs when hashing the card number.
     */
    private BannedCardList prepareBannedCardSearchResult(final HttpServletRequest request, final BannedCardSearchForm form,
        final Collection<String> failedImportCardList) throws CryptoException {
        
        final Set<BannedCard> bannedCards = new TreeSet<>(new BannedCardComparator());
        
        final CustomerBadCardSearchCriteria criteria = createSearchCriteria(form, request);
        
        /* customerCardType == null, user selected Card Type [All] */
        if (criteria.getCustomerCardType() == null) {
            if (StringUtils.isNotBlank(criteria.getCardNumber())) {
                Set<BannedCard> tmpBannedCards = searchOnIris(criteria);
                
                if (tmpBannedCards.isEmpty()) {
                    tmpBannedCards = searchOnLinkBadCard(criteria);
                } else {
                    setCustomerBadCardSearchCriteriaOrderColumn(criteria);            
                }
                
                bannedCards.addAll(tmpBannedCards);
            } else {
                bannedCards.addAll(searchOnIris(criteria));
                bannedCards.addAll(searchOnLinkBadCard(criteria));
            }
            bannedCards.addAll(this.bannedCardService.findBannedCardsFromCardRetry(criteria));
        } else if (criteria.getCardType() == CardProcessingConstants.CARD_TYPE_CREDIT_CARD) {
            bannedCards.addAll(searchOnLinkBadCard(criteria));
            bannedCards.addAll(this.bannedCardService.findBannedCardsFromCardRetry(criteria));
        } else {
            bannedCards.addAll(searchOnIris(criteria));
        }
        
        final BannedCardList result = mapToBannedCardList(request, failedImportCardList, bannedCards, criteria);
        
        return result;
    }
    
    private BannedCardList mapToBannedCardList(final HttpServletRequest request, final Collection<String> failedImportCardList,
        final Set<BannedCard> list, final CustomerBadCardSearchCriteria criteria) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final BannedCardList result = new BannedCardList(criteria.getMaxUpdatedTime().getTime(), new ArrayList<BannedCardInfo>(list.size()));
        for (BannedCard card : list) {
            final BannedCardInfo bannedCardInfo = prepareBannedCardInfo(new BannedCardInfo(), card);
            
            final boolean isUUID = StringUtils.isNotBlank(card.getUuid());
            final String randomBadCardId = (isUUID) ? card.getUuid() : keyMapping.getRandomString(CustomerBadCard.class, card.getId());
            
            bannedCardInfo.setRandomBadCardId(randomBadCardId);
            bannedCardInfo.setImportFailedCardNumber(failedImportCardList);
            
            result.getBannedCardInfo().add(bannedCardInfo);
        }
        return result;
    }
    
    private Set<BannedCard> searchOnIris(final CustomerBadCardSearchCriteria criteria) {
        final Set<BannedCard> bannedCards = new LinkedHashSet<>();
        bannedCards.addAll(mapToBannedCardSet(this.customerBadCardService.findCustomerBadCards(criteria)));
        return bannedCards;
    }
    
    private void setCustomerBadCardSearchCriteriaOrderColumn(final CustomerBadCardSearchCriteria criteria) {
        criteria.setOrderColumn(new String[] { this.customerBadCardBannedCardMapping.get(criteria.getOrderColumn()[0]) });        
    }
    
    private Set<BannedCard> searchOnLinkBadCard(final CustomerBadCardSearchCriteria criteria) throws CryptoException {
        final Set<BannedCard> bannedCards = new LinkedHashSet<>();
        
        final Customer customer = this.customerService.findCustomerDetached(WebSecurityUtil.getUserAccount().getCustomer().getId());
        
        setCustomerBadCardSearchCriteriaOrderColumn(criteria);
        
        if (customer.getUnifiId() != null) {
            bannedCards.addAll(this.linkBadCardService.findBannedCards(customer.getUnifiId(), criteria));
        } else {
            bannedCards.addAll(Collections.emptySet());
        }
        return bannedCards;
    }
    
    private CustomerBadCardSearchCriteria createSearchCriteria(final BannedCardSearchForm form, final HttpServletRequest request) {
        final CustomerBadCardSearchCriteria result = new CustomerBadCardSearchCriteria();
        result.setCustomerId(WebSecurityUtil.getUserAccount().getCustomer().getId());
        
        BannedCardSearchForm searchForm = form;
        if (searchForm == null) {
            searchForm = new BannedCardSearchForm();
        } else {
            result.setCardNumber(searchForm.getFilterCardNumber());
            result.setCardType(searchForm.getCardTypeId());
            result.setCustomerCardType(searchForm.getCustomerCardTypeId());
            if (searchForm.getDataKey() != null) {
                result.setMaxUpdatedTime(new Date(Long.parseLong(searchForm.getDataKey())));
            }
        }
        
        result.setPage((searchForm.getPage() == null) ? 1 : searchForm.getPage());
        if (searchForm.getItemsPerPage() == null) {
            result.setItemsPerPage(WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT);
        } else if (searchForm.getItemsPerPage() > WebCoreConstants.MAX_RESULT_COUNT) {
            result.setItemsPerPage(WebCoreConstants.MAX_RESULT_COUNT);
        } else {
            result.setItemsPerPage(searchForm.getItemsPerPage());
        }
        
        if (searchForm.getDataKey() != null && searchForm.getDataKey().length() > 0) {
            try {
                result.setMaxUpdatedTime(new Date(Long.parseLong(searchForm.getDataKey())));
            } catch (NumberFormatException nfe) {
                // DO NOTHING.
            }
        }
        
        if (searchForm.getColumn() != null) {
            result.setOrderColumn(searchForm.getColumn().getActualAttributes());
        }
        if (searchForm.getOrder() != null) {
            result.setOrderDesc((searchForm.getOrder() == SortOrder.ASC) ? false : true);
        }
        
        if (result.getOrderColumn() == null) {
            result.setOrderColumn(BannedCardSearchForm.SortColumn.CARD_TYPE.getActualAttributes());
        }
        
        if (result.getMaxUpdatedTime() == null) {
            result.setMaxUpdatedTime(new Date());
        }
        
        final Customer customer = this.customerService.findCustomerDetached(WebSecurityUtil.getUserAccount().getCustomer().getId());
        final CustomerProperty retryBeforeBan = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customer.getId(),
                                                                        WebCoreConstants.CUSTOMER_PROPERTY_TYPE_CREDIT_CARD_OFFLINE_RETRY);
        
        if (retryBeforeBan == null) {
            result.setMaxRetry(0);
        } else {
            result.setMaxRetry(Integer.parseInt(retryBeforeBan.getPropertyValue()));
        }
        
        return result;
    }
    
    private BannedCardInfo prepareBannedCardInfo(final BannedCardInfo bannedCardInfo, final CustomerBadCard customerBadCard) {
        final BannedCard bannedCard = new BannedCard();
        bannedCard.setId(customerBadCard.getId());
        bannedCard.setAddedGMT(customerBadCard.getAddedGmt());
        bannedCard.setCardExpiry(customerBadCard.getCardExpiry());
        bannedCard.setLast4DigitsOfCardNumber(customerBadCard.getLast4digitsOfCardNumber());
        bannedCard.setCardNumber(customerBadCard.getCardNumberOrHash());
        bannedCard.setComment(customerBadCard.getComment());
        bannedCard.setCardData(customerBadCard.getCardData());
        bannedCard.setSource(customerBadCard.getSource());
        bannedCard.setCardType(customerBadCard.getCustomerCardType().getCardType().getName());
        bannedCard.setCardTypeId(customerBadCard.getCustomerCardType().getCardType().getId());
        bannedCard.setCustomer(customerBadCard.getCustomerCardType().getCustomer());
        bannedCard.setNumRetries(0);
        return prepareBannedCardInfo(bannedCardInfo, bannedCard);
    }
    
    private BannedCardInfo prepareBannedCardInfo(final BannedCardInfo bannedCardInfo, final BannedCard badCard) {
        if (CardProcessingConstants.CARD_TYPE_CREDIT_CARD == badCard.getCardTypeId()
            || CardProcessingConstants.CARD_TYPE_UNKNOWN == badCard.getCardTypeId()) {
            if (badCard.getLast4DigitsOfCardNumber() == null && badCard.getCardData() != null) {
                try {
                    final String decrypted = this.cryptoService.decryptData(badCard.getCardData());
                    if (decrypted != null) {
                        final String[] parts = decrypted.split(WebCoreConstants.EQUAL_SIGN);
                        if (parts.length > 0 && parts[0].length() > StandardConstants.CONSTANT_4) {
                            badCard.setLast4DigitsOfCardNumber(new Short(parts[0].substring(parts[0].length() - StandardConstants.CONSTANT_4)));
                            this.customerBadCardService.updateLast4DigitsOfCardNumber(badCard.getId(), badCard.getLast4DigitsOfCardNumber());
                        }
                    }
                } catch (CryptoException ce) {
                    LOG.error("Could not decrypt card data for BadCard: " + badCard.getId(), ce);
                }
            }
            
            bannedCardInfo.setCardNumber("XXXXXXXXXXXX" + ((badCard.getLast4DigitsOfCardNumber() == null) ? ""
                    : CardProcessingUtil.covertLast4DigitsOfCardNumberToString(badCard.getLast4DigitsOfCardNumber().intValue())));
        } else {
            bannedCardInfo.setCardNumber(badCard.getCardNumber());
        }
        
        String expiryDate;
        if (badCard.getCardExpiry() == null) {
            expiryDate = WebCoreConstants.N_A_STRING;
        } else {
            expiryDate = switchMMYY(badCard.getCardExpiry());
        }
        
        bannedCardInfo.setCardExpiry(expiryDate);
        bannedCardInfo.setCardType(badCard.getCardType());
        bannedCardInfo.setComment(badCard.getComment());
        bannedCardInfo.setSource((badCard.getSource() == null) ? WebCoreConstants.N_A_STRING : badCard.getSource());
        if (badCard.getAddedGMT() != null) {
            bannedCardInfo.setDateAdded(DateUtil.getRelativeTimeString(badCard.getAddedGMT(), WebSecurityUtil.getCustomerTimeZone()));
        }
        return bannedCardInfo;
    }
    
    private BannedCard mapToBannedCard(final CustomerBadCard customerBadCard) {
        final BannedCard bannedCard = new BannedCard();
        
        bannedCard.setId(customerBadCard.getId());
        bannedCard.setAddedGMT(customerBadCard.getAddedGmt());
        bannedCard.setCardExpiry(customerBadCard.getCardExpiry());
        bannedCard.setLast4DigitsOfCardNumber(customerBadCard.getLast4digitsOfCardNumber());
        bannedCard.setCardNumber(customerBadCard.getCardNumberOrHash());
        bannedCard.setComment(customerBadCard.getComment());
        bannedCard.setCardData(customerBadCard.getCardData());
        bannedCard.setSource(customerBadCard.getSource());
        bannedCard.setCardType(customerBadCard.getCustomerCardType().getName());
        bannedCard.setCardTypeId(customerBadCard.getCustomerCardType().getCardType().getId());
        bannedCard.setCustomer(customerBadCard.getCustomerCardType().getCustomer());
        bannedCard.setNumRetries(0);
        
        return bannedCard;
    }
    
    private Set<BannedCard> mapToBannedCardSet(final Collection<CustomerBadCard> customerBadCards) {
        final Set<BannedCard> bannedCards = new TreeSet<>(new BannedCardComparator());
        
        for (CustomerBadCard customerBadCard : customerBadCards) {
            final BannedCard bannedCard = mapToBannedCard(customerBadCard);
            bannedCards.add(bannedCard);
        }
        
        return bannedCards;
    }
    
    /**
     * This method validates user input randomized customer bad card ID.
     * 
     * @param request
     *            HttpServletRequest object
     * @return actual customer bad card ID if process succeeds, "false" string
     *         if process fails.
     */
    private String validateCustomerBadCardId(final HttpServletRequest request) {
        
        /* validate the randomized customer bad card ID from request parameter. */
        final String randomizedCardId = request.getParameter(CUSTOMER_BAD_CARD_ID_PARAMETER);
        if (StringUtils.isBlank(randomizedCardId) || !DashboardUtil.validateRandomId(randomizedCardId)) {
            LOG.warn("### Invalid customerBadCardID in the request. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        /* validate the actual customer bad card ID. */
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Object actualCardId = keyMapping.getKey(randomizedCardId);
        if (actualCardId == null || !DashboardUtil.validateDBIntegerPrimaryKey(actualCardId.toString())) {
            LOG.warn("### Invalid roleID in the request. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return actualCardId.toString();
    }
    
    /**
     * This method extracts the parameter [customerBadCardID] and verify if it's a valid UUID.
     * 
     * @param request
     *            HttpServletRequest object
     * @return UUID if process succeeds, "false" string
     *         if process fails.
     */
    private String validaterBadCardUUID(final HttpServletRequest request) {
        
        final String uuidFromRequestParameter = request.getParameter("customerBadCardID");
        
        if (StringUtils.isBlank(uuidFromRequestParameter) || !isUUID(uuidFromRequestParameter)) {
            LOG.warn("### Invalid UUID in the request. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return uuidFromRequestParameter;
    }
    
    private boolean isUUID(final String id) {
        return CardProcessingConstants.UUID_PATTERN.matcher(id).matches();
    }
    
    /**
     * This method prepares banned card detail information by converting
     * CustomerBadCard domain object to BannedCardInfo object to pass back to
     * UI.
     * 
     * @param id
     *            CustomerBadCard Id
     * @return BannedCardInfo object
     */
    private BannedCardInfo prepareBannedCardDetails(final Integer id) {
        final CustomerBadCard badCard = this.customerBadCardService.findCustomerBadCardById(id, false);
        BannedCardInfo result = new BannedCardInfo();
        if (badCard != null) {
            result = prepareBannedCardInfo(result, badCard);
        }
        
        return result;
    }
    
    /**
     * This method inserts new CustomerBadCard record.
     * 
     * @param form
     *            BannedCardEditForm object
     * @param userAccount
     *            UserAccount object of login user
     * @throws CryptoException
     *             it occurs when hashing the card number.
     */
    private void addCustomerBadCard(final BannedCardEditForm form, final UserAccount userAccount) throws CryptoException {
        
        final CustomerBadCard customerBadCard = new CustomerBadCard();
        
        final CustomerCardType customerCardType = this.customerCardTypeService.getCustomerCardTypeById(form.getCardTypeId());
        customerBadCard.setCustomerCardType(customerCardType);
        customerBadCard.setCardNumberOrHash(form.getCardNumber());
        
        if (form.getCardNumber().length() >= StandardConstants.CONSTANT_4) {
            customerBadCard.setLast4digitsOfCardNumber(new Short(
                    form.getCardNumber().substring(form.getCardNumber().length() - StandardConstants.CONSTANT_4, form.getCardNumber().length())));
        } else {
            customerBadCard.setLast4digitsOfCardNumber(new Short(form.getCardNumber()));
        }
        
        if ((customerCardType != null) && (customerCardType.getCardType() != null)
            && (customerCardType.getCardType().getId() == CardProcessingConstants.CARD_TYPE_CREDIT_CARD)) {
            final String mmyy = form.getCardExpiry();
            customerBadCard.setCardExpiry(new Short(new StringBuilder(mmyy.substring(2)).append(mmyy.substring(0, 2)).toString()));
        }
        
        customerBadCard.setSource(WebCoreConstants.BADCARD_SOURCE_MANUAL);
        customerBadCard.setComment(form.getComment());
        
        final Date currentTime = new Date();
        customerBadCard.setAddedGmt(currentTime);
        customerBadCard.setLastModifiedGmt(currentTime);
        customerBadCard.setLastModifiedByUserId(userAccount.getId());
        
        this.customerBadCardService.createCustomerBadCard(customerBadCard);
    }
    
    /**
     * This method updates existing CustomerBadCard record.
     * 
     * @param form
     *            BannedCardEditForm object
     * @param userAccount
     *            UserAccount object of login user
     * @throws CryptoException
     *             it occurs when hashing the card number.
     */
    private void editCustomerBadCard(final BannedCardEditForm form, final UserAccount userAccount) throws CryptoException {
        final CustomerBadCard customerBadCard = this.customerBadCardService.findCustomerBadCardById(form.getCustomerBadCardId(), true);
        
        customerBadCard
                .setCardNumberOrHash((customerBadCard.getCustomerCardType().getCardType().getId() == CardProcessingConstants.CARD_TYPE_CREDIT_CARD)
                        ? this.cryptoAlgorithmFactory.getSha1Hash(form.getCardNumber(), CryptoConstants.HASH_BAD_CREDIT_CARD) : form.getCardNumber());
        
        if (form.getCardNumber().length() >= 4) {
            customerBadCard.setLast4digitsOfCardNumber(new Short(
                    form.getCardNumber().substring(form.getCardNumber().length() - 4, form.getCardNumber().length())));
        } else {
            customerBadCard.setLast4digitsOfCardNumber(new Short(form.getCardNumber()));
        }
        
        final CustomerCardType cardType = this.customerCardTypeService.getCardType(form.getCardTypeId());
        if (cardType.getCardType().getId() == CardProcessingConstants.CARD_TYPE_CREDIT_CARD) {
            final String mmyy = form.getCardExpiry();
            customerBadCard.setCardExpiry(new Short(new StringBuilder(mmyy.substring(2)).append(mmyy.substring(0, 2)).toString()));
            customerBadCard.setCardData(this.cryptoService
                    .encryptData(CryptoService.PURPOSE_CREDIT_CARD_LOCAL,
                                 new StringBuilder(form.getCardNumber()).append("=").append(form.getCardExpiry()).toString()));
        }
        customerBadCard.setComment(form.getComment());
        customerBadCard.setLastModifiedGmt(new Date());
        customerBadCard.setLastModifiedByUserId(userAccount.getId());
        
        this.customerBadCardService.updateCustomerBadCard(customerBadCard);
    }
    
    public void setUploadValidator(final CardManagementUploadValidator uploadValidator) {
        this.uploadValidator = uploadValidator;
    }
    
    @ModelAttribute(FORM_SEARCH)
    public WebSecurityForm<BannedCardSearchForm> initSearchForm(final HttpServletRequest request) {
        return new WebSecurityForm<BannedCardSearchForm>((Object) null, new BannedCardSearchForm(),
                SessionTool.getInstance(request).locatePostToken(FORM_SEARCH));
        
    }
    
    @ModelAttribute(FORM_UPLOAD)
    public WebSecurityForm<BannedCardUploadForm> initUploadForm(final HttpServletRequest request) {
        return new WebSecurityForm<BannedCardUploadForm>((Object) null, new BannedCardUploadForm(),
                SessionTool.getInstance(request).locatePostToken(FORM_UPLOAD));
    }
    
    @ModelAttribute(FORM_EDIT)
    public WebSecurityForm<BannedCardEditForm> initEditForm(final HttpServletRequest request) {
        return new WebSecurityForm<BannedCardEditForm>((Object) null, new BannedCardEditForm(),
                SessionTool.getInstance(request).locatePostToken(FORM_EDIT));
    }
    
    /*
     * e.g. mmyy -> yymm, 0118 -> 1801
     */
    private String switchMMYY(final Short mmyy) {
        final DecimalFormat formatter = new DecimalFormat(StandardConstants.STRING_FOUR_ZEROS);
        final String mmyyString = formatter.format(mmyy.shortValue());
        return mmyyString.substring(StandardConstants.CONSTANT_2) + mmyyString.substring(StandardConstants.CONSTANT_0, StandardConstants.CONSTANT_2);
    }
}
