package com.digitalpaytech.mvc.customeradmin;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.constants.DateRangeOption;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.EmailAddress;
import com.digitalpaytech.domain.FlexLocationFacility;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.PurchaseEmailAddress;
import com.digitalpaytech.dto.PaginatedList;
import com.digitalpaytech.dto.PermitInfo;
import com.digitalpaytech.dto.customeradmin.EmailAddressHistoryDetail;
import com.digitalpaytech.dto.customeradmin.LocationRouteFilterInfo;
import com.digitalpaytech.dto.customeradmin.TransactionDetail;
import com.digitalpaytech.dto.customeradmin.TransactionReceiptDetails;
import com.digitalpaytech.dto.customeradmin.TransactionReceiptSearchCriteria;
import com.digitalpaytech.dto.dataset.PermitQueryDataSet.PermitFlexRecord;
import com.digitalpaytech.exception.T2FlexException;
import com.digitalpaytech.mvc.PayStationDetailController;
import com.digitalpaytech.mvc.cache.PermitCacheManager;
import com.digitalpaytech.mvc.customeradmin.support.TransactionReceiptSearchForm;
import com.digitalpaytech.mvc.customeradmin.support.TransactionReceiptSendForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.ConsumerService;
import com.digitalpaytech.service.CustomerSubscriptionService;
import com.digitalpaytech.service.FlexWSService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.PurchaseEmailAddressService;
import com.digitalpaytech.service.PurchaseService;
import com.digitalpaytech.service.T2FlexWsService;
import com.digitalpaytech.service.TransactionDetailService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.FlexConstants;
import com.digitalpaytech.util.LabelConstants;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.PooledResource;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.TLSUtils;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.cache.RedisAggregatedCache;
import com.digitalpaytech.util.support.WebObjectId;
import com.digitalpaytech.validation.customeradmin.TransactionReceiptSendValidator;
import com.digitalpaytech.validation.customeradmin.TransactionSearchValidator;

/**
 * @author Amanda Chong
 */

@Controller("transactionReceiptController")
public class TransactionReceiptController extends PayStationDetailController {
    public static final String FORM_SEARCH = "transactionReceiptSearchForm";
    public static final String FORM_SEND = "transactionReceiptSendForm";
    
    private static final long INTERVAL_MILLISECS = 300000L;
    
    private static final Set<DateRangeOption> DATE_OPTIONS_TRANSACTION;
    static {
        final Set<DateRangeOption> buffer = new LinkedHashSet<DateRangeOption>(20);
        buffer.add(DateRangeOption.DATE_TIME_RANGE);
        buffer.add(DateRangeOption.TODAY);
        buffer.add(DateRangeOption.YESTERDAY);
        buffer.add(DateRangeOption.LAST_24_HOURS);
        buffer.add(DateRangeOption.THIS_WEEK);
        buffer.add(DateRangeOption.LAST_WEEK);
        buffer.add(DateRangeOption.LAST_7_DAYS);
        buffer.add(DateRangeOption.THIS_MONTH);
        buffer.add(DateRangeOption.LAST_30_DAYS);
        
        DATE_OPTIONS_TRANSACTION = Collections.unmodifiableSet(buffer);
    }
    
    private static final String FMT_CONTAINS_EXPRESSION = "%{0}%";
    
    private static final Logger LOG = Logger.getLogger(TransactionReceiptController.class);
    
    @Autowired
    private CustomerSubscriptionService customerSubscriptionService;
    
    @Autowired
    private LocationService locationService;
    @Autowired
    private TransactionSearchValidator transactionSearchValidator;
    @Autowired
    private TransactionReceiptSendValidator transactionReceiptSendValidator;
    @Autowired
    private PurchaseEmailAddressService purchaseEmailAddressService;
    @Autowired
    private ConsumerService consumerService;
    @Autowired
    private PurchaseService purchaseService;
    @Autowired
    private TransactionDetailService transactionDetailService;
    @Autowired
    private MessageHelper messageHelper;
    @Autowired
    private T2FlexWsService t2FlexWsService;
    @Autowired
    private FlexWSService flexWSService;    
    
    
    @Autowired
    private PermitCacheManager permitCacheManager;
    
    @Autowired
    private TLSUtils tlsUtils;
    
    @ModelAttribute(FORM_SEARCH)
    public final WebSecurityForm<TransactionReceiptSearchForm> initSearchForm(final HttpServletRequest request) {
        return new WebSecurityForm<TransactionReceiptSearchForm>((Object) null, new TransactionReceiptSearchForm(), SessionTool.getInstance(request)
                .locatePostToken(FORM_SEARCH));
    }
    
    @ModelAttribute(FORM_SEND)
    public final WebSecurityForm<TransactionReceiptSendForm> initSendForm(final HttpServletRequest request) {
        return new WebSecurityForm<TransactionReceiptSendForm>((Object) null, new TransactionReceiptSendForm(), SessionTool.getInstance(request)
                .locatePostToken(FORM_SEND));
    }
    
    @RequestMapping(value = "/secure/reporting/transactionReceipts/index.html", method = RequestMethod.GET)
    public final String showTransactionReceiptFilter(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_SEARCH) final WebSecurityForm<TransactionReceiptSearchForm> transactionReceiptSearchForm,
        @ModelAttribute(FORM_SEND) final WebSecurityForm<TransactionReceiptSendForm> transactionReceiptSendForm, final BindingResult result) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_TRANSACTION_REPORTS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                    && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) { 
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }            
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Customer customer = WebSecurityUtil.getUserAccount().getCustomer();
        transactionReceiptSearchForm.setActionFlag(WebSecurityConstants.ACTION_SEARCH);
        transactionReceiptSendForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        
        getLocationRouteFilters(request, response, model, customer.getId(), keyMapping);
        model.put("txDateOptions", DATE_OPTIONS_TRANSACTION);
        model.put(FORM_SEARCH, transactionReceiptSearchForm);
        model.put(FORM_SEND, transactionReceiptSendForm);
        
        return "/reporting/transactionReceipts/index";
    }
    
    private void getLocationRouteFilters(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        final int customerId, final RandomKeyMapping keyMapping) {
        
        final List<Location> locations = this.locationService.findLocationsByCustomerId(customerId);
        final LocationRouteFilterInfo filterValues = this.commonControllerHelper.createLocationRouteFilter(customerId, keyMapping);
        final LocationRouteFilterInfo validValues = new LocationRouteFilterInfo();
        
        if (locations != null) {
            for (Location location : locations) {
                if (!location.getIsParent()) {
                    location.setRandomId(keyMapping.getRandomString(location, WebCoreConstants.ID_LOOK_UP_NAME));
                    if (location.getLocation() != null) {
                        validValues.addLocation(location.getName(), location.getRandomId(), location.getLocation().getName(), location.getIsParent());
                    } else {
                        validValues.addLocation(location.getName(), location.getRandomId(), "", location.getIsParent());
                    }
                }
            }
        }
        
        model.put("filterValues", filterValues);
        model.put("validValues", validValues);
    }
    
    @RequestMapping(value = "/secure/reporting/transactionReceipts/transactionSearch.html", method = RequestMethod.POST)
    @ResponseBody
    public final String searchTransactionReceipts(
        @ModelAttribute(FORM_SEARCH) final WebSecurityForm<TransactionReceiptSearchForm> transactionReceiptSearchForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_TRANSACTION_REPORTS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final TimeZone customerTimeZone = TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone());
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        this.transactionSearchValidator.validate(transactionReceiptSearchForm, result);
        
        if (transactionReceiptSearchForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            return WidgetMetricsHelper.convertToJson(transactionReceiptSearchForm.getValidationErrorInfo(), ErrorConstants.ERROR_STATUS, true);
        }
        
        final TransactionReceiptSearchForm form = transactionReceiptSearchForm.getWrappedObject();
        final TransactionReceiptSearchCriteria criteria = createSearchCriteria(form, request);
        
        final PooledResource<DateFormat> format = DateUtil.takeDateFormat(WebCoreConstants.DATE_FORMAT, customerTimeZone);
        final PooledResource<DateFormat> timeFormat = DateUtil.takeDateFormat(WebCoreConstants.TIME_UI_FORMAT_TODAY, customerTimeZone);
        
        final int firstRecord = (criteria.getPage() - 1) * criteria.getItemsPerPage();
        final int lastRecord = firstRecord + criteria.getItemsPerPage();
        
        final RedisAggregatedCache<TransactionDetail> cache = this.permitCacheManager.getCache(criteria);
        callIrisIfNeedMoreData(cache, criteria, keyMapping, format, timeFormat, firstRecord, lastRecord);
        callFlexIfNeedMoreData(cache, criteria, keyMapping, format, timeFormat, firstRecord, lastRecord);
        
        final PaginatedList<TransactionDetail> transactionList = new PaginatedList<TransactionDetail>();
        transactionList.setElements(cache.range(firstRecord, criteria.getItemsPerPage()));
        
        final String doesNotExpire = this.messageHelper.getMessage(LabelConstants.LABEL_REPORTING_TRANSACTIONSEARCH_NOEXPIRYDATE);
        for (TransactionDetail txDtl : transactionList.getElements()) {
            if (txDtl.getIsFlex() != null && txDtl.getIsFlex().booleanValue()) {
                // modify the display of expire date for records having year 9999 (or 10000 cause of GMT transform)
                final Calendar cal = Calendar.getInstance();
                cal.setTime(txDtl.getExpiryDate());
                final int year = cal.get(Calendar.YEAR);
                if (year >= FlexConstants.PERMIT_NO_EXPIRY_YEAR) {
                    txDtl.setPermitExpiryDate(doesNotExpire);
                }
                txDtl.setPurchaseRandomId(keyMapping.getRandomString(PermitFlexRecord.class, txDtl.getPermitId()));
            } else {
                txDtl.setPurchaseRandomId(keyMapping.getRandomString(Purchase.class, txDtl.getPermitId()));
            }
        }
        
        transactionList.setDataKey(Long.toString(criteria.getMaxLastUpdatedTime().getTime()));
        
        return WidgetMetricsHelper.convertToJson(transactionList, "transactionList", true);
    }
    
    private void callIrisIfNeedMoreData(final RedisAggregatedCache<TransactionDetail> cache, final TransactionReceiptSearchCriteria criteria
            , final RandomKeyMapping keyMapping, final PooledResource<DateFormat> format, final PooledResource<DateFormat> timeFormat
            , final int firstRecord, final int lastRecord) {
        if (!cache.isSaturated(PermitCacheManager.KEYTYPE_IRIS, firstRecord, lastRecord)) {
            final List<TransactionDetail> txDetails = retrieveIrisPermits(criteria, keyMapping, format.get(), timeFormat.get());
            
            cache.put(this.permitCacheManager.transform(txDetails));
            
            if (txDetails.size() < criteria.getItemsPerPage()) {
                cache.saturate(PermitCacheManager.KEYTYPE_IRIS);
            }
        }
    }
    
    private void callFlexIfNeedMoreData(final RedisAggregatedCache<TransactionDetail> cache, final TransactionReceiptSearchCriteria criteria
            , final RandomKeyMapping keyMapping, final PooledResource<DateFormat> format, final PooledResource<DateFormat> timeFormat
            , final int firstRecord, final int lastRecord) {
        if ((this.shouldCallFlex(criteria)) && (!cache.isSaturated(PermitCacheManager.KEYTYPE_FLEX, firstRecord, lastRecord))) {
            List<TransactionDetail> t2TransactionDetailList = null;
            try {
                final PermitInfo permitInfo = new PermitInfo(criteria.getCustomerId(),
                                                             criteria.getLicencePlateNumber(),
                                                             criteria.getStartDateTime(),
                                                             criteria.getEndDateTime(),
                                                             criteria.getMaxLastUpdatedTime(),
                                                             firstRecord + 1,
                                                             lastRecord, criteria.getLocationId());
                
                final List<FlexLocationFacility> flfList = this.flexWSService.findFlexLocationFacilityByLocationId(criteria.getLocationId());
                
                if (criteria.getLocationId() == null || !flfList.isEmpty()) {
                    t2TransactionDetailList = this.t2FlexWsService.getPermits(permitInfo, format.get(), keyMapping, flfList);                
                    cache.put(this.permitCacheManager.transform(t2TransactionDetailList));                
                    if (t2TransactionDetailList.size() < criteria.getItemsPerPage()) {
                        cache.saturate(PermitCacheManager.KEYTYPE_FLEX);
                    }
                }
            } catch (T2FlexException t2fe) {
                LOG.error(t2fe);
                t2TransactionDetailList = new ArrayList<TransactionDetail>();
            }
        }
    }
    
    @RequestMapping(value = "/secure/reporting/transactionReceipts/receiptDetails.html", method = RequestMethod.GET)
    @ResponseBody
    public final String showReceiptDetails(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_TRANSACTION_REPORTS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                    && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) { 
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }            
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final String purchaseRandomId = request.getParameter("transactionId");
        
        final WebObjectId objId = keyMapping.getKeyObject(purchaseRandomId);
        if (objId == null || objId.getId() == null) {
            LOG.warn("+++ Invalid transactionId in PayStationListController.payStationDetailsReportsTransactionDetails() method. +++");
            return null;
        }
        
        TransactionReceiptDetails detail = null;
        try (PooledResource<DateFormat> format = DateUtil.takeDateFormat(WebCoreConstants.DATE_FORMAT, DateUtil.GMT)) {
            if (objId.getObjectType() != null
                && objId.getObjectType().getName().toLowerCase().indexOf(PermitFlexRecord.class.getName().toLowerCase()) != -1) {
                
                // public TransactionDetail getFlexPermit(final Integer customerId, final Integer permitId) {
                final TransactionDetail txDetail = this.permitCacheManager
                        .getFlexPermit(WebSecurityUtil.getUserAccount().getCustomer().getId(), (Long) objId.getId());
                detail = this.t2FlexWsService.populateTransactionReceiptDetails(txDetail, keyMapping);
            } else {
                final Long purchaseId = Long.parseLong(objId.getId().toString());
                if (purchaseId == null) {
                    return null;
                }
                
                final Purchase purchase = this.purchaseService.findPurchaseById(purchaseId, false);
                if (purchase == null) {
                    response.setStatus(HttpStatus.NOT_FOUND.value());
                    return WebCoreConstants.RESPONSE_FALSE;
                }
                
                detail = this.purchaseEmailAddressService.getTransactionReceiptDetails(purchase);
                detail.setPurchaseRandomId(keyMapping.getRandomString(purchase, WebCoreConstants.ID_LOOK_UP_NAME));
                
                Collection<ProcessorTransaction> pts = this.processorTransactionService.findProcessorTransactionByPurchaseId(purchase.getId());
                if (pts != null && !pts.isEmpty()) {
                    ProcessorTransaction pt = pts.iterator().next();
                    detail.setProcessorTransactionRandomId(keyMapping.getRandomString(pt, WebCoreConstants.ID_LOOK_UP_NAME));
                    //detail.setPurchaseRandomId(keyMapping.getRandomString(pt, WebCoreConstants.ID_LOOK_UP_NAME));
                }
               
                final String timeZone = this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(purchase.getCustomer().getId(),
                    WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE).getPropertyValue();
                
                final List<EmailAddressHistoryDetail> emailAddressHistoryDetailList = this.purchaseEmailAddressService
                        .findEmailAddressHistoryDetail(purchaseId);
                for (EmailAddressHistoryDetail emailAddressHistoryDetail : emailAddressHistoryDetailList) {
                    try {
                        emailAddressHistoryDetail.setSentEmailDate(format.get().format(DateUtil.changeTimeZone(
                            DateUtil.getDatabaseDateFormat().parse(emailAddressHistoryDetail.getSentEmailDate()), timeZone)));
                    } catch (ParseException e) {
                        LOG.warn("Unable to parse", e);
                        e.printStackTrace();
                    }
                }
                detail.setEmailAddressHistoryDetailList(emailAddressHistoryDetailList);
            }
        }
        if (this.tlsUtils.shouldRedirect(request)) {
            detail.setIsRefundable(false);
        }
        
        return WidgetMetricsHelper.convertToJson(detail, "transactionReceiptDetails", true);
    }
    
    @RequestMapping(value = "/secure/reporting/transactionReceipts/sendReceipt.html", method = RequestMethod.POST)
    @ResponseBody
    public final String sendReceipt(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_SEND) final WebSecurityForm<TransactionReceiptSendForm> transactionReceiptSendForm, final BindingResult result) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_TRANSACTION_REPORTS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        this.transactionReceiptSendValidator.validate(transactionReceiptSendForm, result);
        if (transactionReceiptSendForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            transactionReceiptSendForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            transactionReceiptSendForm.resetToken();
            return WidgetMetricsHelper.convertToJson(transactionReceiptSendForm.getValidationErrorInfo(), ErrorConstants.ERROR_STATUS, true);
        }
        transactionReceiptSendForm.resetToken();
        
        final String emailAddressString = transactionReceiptSendForm.getWrappedObject().getEmailAddress();
        final List<EmailAddress> emailAddressList = this.consumerService.findEmailAddressByEmail(emailAddressString);
        
        EmailAddress emailAddress = null;
        if (emailAddressList == null || emailAddressList.isEmpty()) {
            emailAddress = new EmailAddress(emailAddressString, new Date());
            this.purchaseEmailAddressService.saveEmailAddress(emailAddress);
        } else {
            emailAddress = emailAddressList.get(0);
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final String purchaseRandomId = transactionReceiptSendForm.getWrappedObject().getPurchaseRandomId();
        final Long purchaseID = (Long) keyMapping.getKey(purchaseRandomId);
        
        final Purchase purchase = this.purchaseService.findPurchaseById(purchaseID, false);
        final PurchaseEmailAddress purchaseEmailAddress = new PurchaseEmailAddress(purchase, emailAddress, new Date(), false);
        this.purchaseEmailAddressService.savePurchaseEmail(purchaseEmailAddress);
        
        return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(":").append(transactionReceiptSendForm.getInitToken()).toString();
    }
    
    protected final List<TransactionDetail> retrieveIrisPermits(final TransactionReceiptSearchCriteria criteria, final RandomKeyMapping keyMapping,
                                                                final DateFormat format, final DateFormat timeFormat) {
        final List<TransactionDetail> transactionDetailList = this.transactionDetailService.findTransactionDetailByCriteria(criteria);
        
        final String timeZone = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(WebSecurityUtil.getUserAccount().getCustomer().getId()
                                                                        , WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE).getPropertyValue();
        
        for (TransactionDetail transactionDetail : transactionDetailList) {
            assignCouponNumber(transactionDetail);
            
            assignCardInfo(transactionDetail);
            
            final Purchase purchase = new Purchase();
            purchase.setId(Long.parseLong(transactionDetail.getPurchaseRandomId()));
            transactionDetail.setPurchaseRandomId(keyMapping.getRandomString(purchase, WebCoreConstants.ID_LOOK_UP_NAME));
            
            assignDates(transactionDetail, format, timeFormat, timeZone);
        }
        
        return transactionDetailList;
    }
    
    private void assignCouponNumber(final TransactionDetail transactionDetail) {
        if ((transactionDetail.isIsCouponOffline() != null) && transactionDetail.isIsCouponOffline() 
                && (StringUtils.isNotBlank(transactionDetail.getCouponNumber()))) {
            if (transactionDetail.getCouponNumber().endsWith("+")) {
                transactionDetail.setCouponNumber(transactionDetail.getCouponNumber().substring(1, transactionDetail.getCouponNumber().length() - 1));
            } else {
                transactionDetail.setCouponNumber(transactionDetail.getCouponNumber().substring(1));
            }
        }
    }
    
    private void assignCardInfo(final TransactionDetail transactionDetail) {
        if (StringUtils.isNotEmpty(transactionDetail.getCardNumber())) {
            transactionDetail.setCardNumber(WebCoreUtil.createStarredCardNumber(transactionDetail.getCardNumber(), false));
            if (((transactionDetail.getIsCreditCard() != null) && transactionDetail.getIsCreditCard().booleanValue())
                || ((transactionDetail.getIsPasscard() != null) && transactionDetail.getIsPasscard().booleanValue())) {
                transactionDetail.setLast4DigitsOfCardNumber(WebCoreUtil.createStarredCardNumber(transactionDetail.getCardNumber(), false));
            }
        }
        
        if (!StringUtils.isBlank(transactionDetail.getLast4DigitsOfCardNumber())) {
            transactionDetail.setLast4DigitsOfCardNumber(WebCoreUtil.createStarredCardNumber(transactionDetail.getLast4DigitsOfCardNumber(), false));
        }
    }
    
    private void assignDates(final TransactionDetail transactionDetail, final DateFormat format
            , final DateFormat timeFormat, final String timeZone) {
        if (transactionDetail.getPurchaseDateGmt() != null) {
            transactionDetail.setPurchaseDate(format.format(transactionDetail.getPurchaseDateGmt()));
        }
        if (transactionDetail.getPermitExpiryDateGmt() != null) {
            transactionDetail.setPermitExpiryDate(format.format(transactionDetail.getPermitExpiryDateGmt()));
        }
        
        final Calendar purchaseDateCal = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
        purchaseDateCal.setTime(transactionDetail.getPurchaseDateGmt());
        
        if (!transactionDetail.getIsCancelled() && !transactionDetail.getIsTest()) {
            final Calendar permitExpiryDateCal = (Calendar) purchaseDateCal.clone();
            permitExpiryDateCal.setTime(transactionDetail.getPermitExpiryDateGmt());
            
            if (purchaseDateCal.get(Calendar.YEAR) == permitExpiryDateCal.get(Calendar.YEAR)
                && purchaseDateCal.get(Calendar.DAY_OF_YEAR) == permitExpiryDateCal.get(Calendar.DAY_OF_YEAR)) {
                transactionDetail.setPermitExpiryDate(timeFormat.format(transactionDetail.getPermitExpiryDateGmt()));
                
                assignPurchaseDateSameYear(transactionDetail, format, timeFormat, timeZone);
            } else {
                transactionDetail.setPurchaseDate(format.format(transactionDetail.getPurchaseDateGmt()));
                transactionDetail.setPermitExpiryDate(format.format(transactionDetail.getPermitExpiryDateGmt()));
            }
        } else {
            assignPurchaseDateSameYear(transactionDetail, format, timeFormat, timeZone);
        }
    }
    
    private void assignPurchaseDateSameYear(final TransactionDetail transactionDetail, final DateFormat format
            , final DateFormat timeFormat, final String timeZone) {
        final Calendar todayCal = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
        final Calendar purchaseDateCal = Calendar.getInstance(TimeZone.getTimeZone(timeZone));
        final String time = timeFormat.format(transactionDetail.getPurchaseDateGmt());
        
        purchaseDateCal.setTime(transactionDetail.getPurchaseDateGmt());
        
        if (purchaseDateCal.get(Calendar.YEAR) == todayCal.get(Calendar.YEAR)
            && purchaseDateCal.get(Calendar.DAY_OF_YEAR) == todayCal.get(Calendar.DAY_OF_YEAR)) {
            transactionDetail.setPurchaseDate(this.messageHelper.getMessage("label.reporting.today") + WebCoreConstants.EMPTY_SPACE + time);
        } else if (purchaseDateCal.get(Calendar.YEAR) == todayCal.get(Calendar.YEAR)
                   && purchaseDateCal.get(Calendar.DAY_OF_YEAR) == todayCal.get(Calendar.DAY_OF_YEAR) - 1) {
            transactionDetail.setPurchaseDate(this.messageHelper.getMessage("label.reporting.yesterday") + WebCoreConstants.EMPTY_SPACE + time);
        } else {
            transactionDetail.setPurchaseDate(format.format(transactionDetail.getPurchaseDateGmt()));
        }
    }
    
    protected final boolean shouldCallFlex(final TransactionReceiptSearchCriteria criteria) {
        final boolean hasFlexIntegration = this.customerSubscriptionService
                .hasOneOfSubscriptions(criteria.getCustomerId(), WebCoreConstants.SUBSCRIPTION_TYPE_FLEX_INTEGRATION);
        
        return  hasFlexIntegration && StringUtils.isBlank(criteria.getCardNumber())
                && StringUtils.isBlank(criteria.getSpaceNumber())
                && StringUtils.isBlank(criteria.getCouponNumber())
                && StringUtils.isBlank(criteria.getEmailAddress())
                && StringUtils.isBlank(criteria.getMobileNumber());
    }
    
    private TransactionReceiptSearchCriteria createSearchCriteria(final TransactionReceiptSearchForm form, final HttpServletRequest request) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final TransactionReceiptSearchCriteria criteria = new TransactionReceiptSearchCriteria();
        criteria.setCustomerId(WebSecurityUtil.getUserAccount().getCustomer().getId());
        criteria.setCardNumber(nullIfEmpty(form.getCardNumber()));
        criteria.setLicencePlateNumber(criteriaContains(form.getPlateNumber()));
        criteria.setSpaceNumber(nullIfEmpty(form.getSpaceNumber()));
        criteria.setEmailAddress(criteriaContains(form.getEmailAddress(), true));
        
        final String couponNumber = form.getCouponNumber();
        if (couponNumber != null && couponNumber.length() > 0) {
            criteria.setCouponNumber(MessageFormat.format(FMT_CONTAINS_EXPRESSION, couponNumber.replaceAll("\\*", "%")));
        }
        
        criteria.setMobileNumber(criteriaContains(form.getMobileNumber()));
        
        final String locationRandomId = form.getLocationRandomId();
        final Integer locationId = (Integer) keyMapping.getKey(locationRandomId);
        if (locationId != null) {
            criteria.setLocationId(locationId);
        }
        
        criteria.setStartDateTime(form.getStartDateTime());
        criteria.setEndDateTime(form.getEndDateTime());
        
        // Paginate...
        
        criteria.setPage((form.getPage() == null) ? 1 : form.getPage());
        if (form.getItemsPerPage() == null) {
            criteria.setItemsPerPage(WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT);
        } else if (form.getItemsPerPage() > WebCoreConstants.MAX_RESULT_COUNT) {
            criteria.setItemsPerPage(WebCoreConstants.MAX_RESULT_COUNT);
        } else {
            criteria.setItemsPerPage(form.getItemsPerPage());
        }
        
        long dataKey;
        if (form.getDataKey() == null) {
            dataKey = (System.currentTimeMillis() / INTERVAL_MILLISECS) * INTERVAL_MILLISECS;
        } else {
            dataKey = form.getDataKey();
        }
        
        criteria.setMaxLastUpdatedTime(new Date(dataKey));
        
        return criteria;
    }
    
    private String nullIfEmpty(final String in) {
        String result = null;
        if ((in != null) && (in.length() > 0)) {
            result = in;
        }
        
        return result;
    }
    
    private String criteriaContains(final String in) {
        return criteriaContains(in, false);
    }
    
    private String criteriaContains(final String in, final boolean lowerCase) {
        String result = null;
        if ((in != null) && (in.length() > 0)) {
            result = MessageFormat.format(FMT_CONTAINS_EXPRESSION, (!lowerCase) ? in : in.toLowerCase());
        }
        
        return result;
    }
}
