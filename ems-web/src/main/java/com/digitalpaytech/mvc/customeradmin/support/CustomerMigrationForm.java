package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class CustomerMigrationForm implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 935861702366066151L;
    private List<String> migrationContacts;
    private String scheduleDate;
    
    private Date scheduleRealDate;
    
    public final List<String> getMigrationContacts() {
        return this.migrationContacts;
    }
    
    public final void setMigrationContacts(final List<String> migrationContacts) {
        this.migrationContacts = migrationContacts;
    }
    
    public final String getScheduleDate() {
        return this.scheduleDate;
    }
    
    public final void setScheduleDate(final String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }
    
    public final Date getScheduleRealDate() {
        return this.scheduleRealDate;
    }
    
    public final void setScheduleRealDate(final Date scheduleRealDate) {
        this.scheduleRealDate = scheduleRealDate;
    }
    
}
