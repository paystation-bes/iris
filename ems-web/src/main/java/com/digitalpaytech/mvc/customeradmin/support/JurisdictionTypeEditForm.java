package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

import com.digitalpaytech.util.PaystationConstants;

public class JurisdictionTypeEditForm implements Serializable {
    private static final long serialVersionUID = 810607895989570975L;
    private Integer jurisdictionTypePreferred;
    private Integer jurisdictionTypeLimited;
    private String preferredParkersStatus;
    private MultipartFile file;

    public JurisdictionTypeEditForm() {
        this.jurisdictionTypePreferred = PaystationConstants.PAYSTATION_JURISDICTION_PREFERRED_BY_CUSTOMER;
        this.jurisdictionTypeLimited = PaystationConstants.PAYSTATION_JURISDICTION_LIMITED_BY_CUSTOMER;
    }
    
    public final Integer getJurisdictionTypePreferred() {
        return this.jurisdictionTypePreferred;
    }
    
    public final void setJurisdictionTypePreferred(final Integer jurisdictionTypePreferred) {
        this.jurisdictionTypePreferred = jurisdictionTypePreferred;
    }
    
    public final Integer getJurisdictionTypeLimited() {
        return this.jurisdictionTypeLimited;
    }
    
    public final void setJurisdictionTypeLimited(final Integer jurisdictionTypeLimited) {
        this.jurisdictionTypeLimited = jurisdictionTypeLimited;
    }

    public final String getPreferredParkersStatus() {
        return this.preferredParkersStatus;
    }

    public final void setPreferredParkersStatus(final String preferredParkersStatus) {
        this.preferredParkersStatus = preferredParkersStatus;
    }

    public final MultipartFile getFile() {
        return this.file;
    }

    public final void setFile(final MultipartFile file) {
        this.file = file;
    }
}
