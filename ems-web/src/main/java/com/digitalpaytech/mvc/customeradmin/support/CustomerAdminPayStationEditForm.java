package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;

import javax.persistence.Transient;

import com.digitalpaytech.dto.MapEntry;

public class CustomerAdminPayStationEditForm implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -1129060488838417556L;
    private String randomId;
    private String name;
    private String locationRandomId;
    private Boolean active;
    private Boolean hidden;
    private String locationName;
    private String parentLocationName;
    private String serialNumber;
    private String lastSeen;
    private MapEntry mapInfo;
    private Boolean isDecommissioned;
    private Integer alertCount;
    private Integer alertSeverity;
    private Boolean isLinux;
    
    @Transient
    private transient int pointOfSaleId;
    @Transient
    private transient int locationId;
    
    public CustomerAdminPayStationEditForm() {
    }
    
    public String getRandomId() {
        return randomId;
    }
    
    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getLocationRandomId() {
        return locationRandomId;
    }
    
    public void setLocationRandomId(String locationRandomId) {
        this.locationRandomId = locationRandomId;
    }
    
    public Boolean getActive() {
        return active;
    }
    
    public void setActive(Boolean active) {
        this.active = active;
    }
    
    public Boolean getHidden() {
        return hidden;
    }
    
    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }
    
    public String getLocationName() {
        return locationName;
    }
    
    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
    
    public String getParentLocationName() {
        return parentLocationName;
    }
    
    public void setParentLocationName(String parentLocationName) {
        this.parentLocationName = parentLocationName;
    }
    
    public String getSerialNumber() {
        return serialNumber;
    }
    
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
    
    public String getLastSeen() {
        return lastSeen;
    }
    
    public void setLastSeen(String lastSeen) {
        this.lastSeen = lastSeen;
    }
    
    public MapEntry getMapInfo() {
        return mapInfo;
    }
    
    public void setMapInfo(MapEntry mapInfo) {
        this.mapInfo = mapInfo;
    }
    
    public int getPointOfSaleId() {
        return pointOfSaleId;
    }
    
    public void setPointOfSaleId(int pointOfSaleId) {
        this.pointOfSaleId = pointOfSaleId;
    }
    
    public int getLocationId() {
        return locationId;
    }
    
    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }
    
    public Boolean getIsDecommissioned() {
        return isDecommissioned;
    }
    
    public void setIsDecommissioned(Boolean isDecommissioned) {
        this.isDecommissioned = isDecommissioned;
    }
    
    public Integer getAlertCount() {
        return alertCount;
    }
    
    public void setAlertCount(Integer alertCount) {
        this.alertCount = alertCount;
    }
    
    public Integer getAlertSeverity() {
        return alertSeverity;
    }
    
    public void setAlertSeverity(Integer alertSeverity) {
        this.alertSeverity = alertSeverity;
    }

    public Boolean getIsLinux() {
        return isLinux;
    }

    public void setIsLinux(Boolean isLinux) {
        this.isLinux = isLinux;
    }
}
