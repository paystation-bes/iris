package com.digitalpaytech.mvc.customeradmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.mvc.RateController;
import com.digitalpaytech.mvc.support.RateEditForm;
import com.digitalpaytech.mvc.support.RateSearchForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
@SessionAttributes({ "rateEditForm" })
public class CustomerAdminRateController extends RateController {
    
    @RequestMapping(value = "/secure/settings/rates/index.html", method = RequestMethod.GET)
    public final String getFirstPermittedTab(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasValidCustomerSubscription(WebCoreConstants.SUBSCRIPTION_TYPE_ONLINE_RATE_CONFIGURATION)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        if (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_RATES)
            || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_RATES)) {
            return viewRateDetails(request, response, model);
        } else {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD) && 
                    !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) { 
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }            
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
    }
    
    @RequestMapping(value = "/secure/settings/rates/rateDetails.html", method = RequestMethod.GET)
    public final String viewRateDetails(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!checkPermissions(false)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD) && 
                    !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) { 
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }            
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        return super.view(request, response, model, "/settings/rates/rateDetails");
    }
    
    @RequestMapping(value = "/secure/settings/rates/searchRate.html", method = RequestMethod.POST)
    @ResponseBody
    public final String searchRate(final HttpServletRequest request, final HttpServletResponse response) {
        if (!checkPermissions(false)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.search(request, response);
    }
    
    @RequestMapping(value = "/secure/settings/rates/rateList.html", method = RequestMethod.GET)
    @ResponseBody
    public final String getRateList(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<RateSearchForm> form, final HttpServletRequest request,
                                    final HttpServletResponse response) {
        if (!checkPermissions(false)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.getList(form, request, response);
    }
    
    @RequestMapping(value = "/secure/settings/rates/locatePageWithRate.html", method = RequestMethod.POST)
    @ResponseBody
    public final String locatePageWithRate(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<RateSearchForm> form, final HttpServletRequest request,
                                           final HttpServletResponse response) {
        if (!checkPermissions(false)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.locatePageWith(form, request, response);
    }
    
    @RequestMapping(value = "/secure/settings/rates/rateInfo.html", method = RequestMethod.GET)
    @ResponseBody
    public final String getRateInfo(final HttpServletRequest request, final HttpServletResponse response) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.getInfo(request, response);
    }
    
    @RequestMapping(value = "/secure/settings/rates/saveRateDetails.html", method = RequestMethod.POST)
    @ResponseBody
    public final String saveRate(@ModelAttribute("rateEditForm") final WebSecurityForm<RateEditForm> webSecurityForm, final BindingResult result,
                                 final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.save(webSecurityForm, result, request, response, model);
    }
    
    @RequestMapping(value = "/secure/settings/rates/verifyDeleteRate.html", method = RequestMethod.GET)
    @ResponseBody
    public final String verifyDeleteRate(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.verifyDelete(request, response);
    }
    
    @RequestMapping(value = "/secure/settings/rates/deleteRate.html", method = RequestMethod.GET)
    @ResponseBody
    public final String deleteRate(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.delete(request, response);
    }
    
    private boolean checkPermissions(final boolean isManageOnly) {
        if (!WebSecurityUtil.hasValidCustomerSubscription(WebCoreConstants.SUBSCRIPTION_TYPE_ONLINE_RATE_CONFIGURATION)) {
            return false;
        }
        
        if (isManageOnly) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_RATES)) {
                return false;
            }
        } else {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_RATES)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_RATES)) {
                return false;
            }
        }
        return true;
    }
    
}
