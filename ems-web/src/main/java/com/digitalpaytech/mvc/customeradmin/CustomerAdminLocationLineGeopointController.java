package com.digitalpaytech.mvc.customeradmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.mvc.LocationLineGeopointController;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
public class CustomerAdminLocationLineGeopointController extends LocationLineGeopointController {
    
    @RequestMapping(value = "/secure/settings/locations/saveLocationGeoParkingArea.html", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public final String saveLocationGeoParkingArea(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        return super.saveLocationGeoParkingArea(request, response, model);
    }
}
