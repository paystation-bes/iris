package com.digitalpaytech.mvc.customeradmin.support.io;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.CustomerCard;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.comparator.CustomerCardComparator;
import com.digitalpaytech.mvc.customeradmin.support.io.dto.CustomerCardCsvDTO;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.util.CustomerCardTypeWithLineNumbers;
import com.digitalpaytech.util.LocationWithLineNumbers;
import com.digitalpaytech.util.PointOfSaleWithLineNumbers;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.csv.CsvProcessingError;
import com.digitalpaytech.util.csv.CsvProcessor;
import com.digitalpaytech.util.csv.CsvParserUtils;
import com.digitalpaytech.util.csv.CsvWriter;
import com.digitalpaytech.validation.customeradmin.CustomerCardCsvValidator;

@Component("customerCardCsvReader")
public class CustomerCardCsvReader extends CsvProcessor<CustomerCard> {
    public static final String CTXKEY_CARD_TYPES = "cardTypes";
    public static final String CTXKEY_LOCATIONS = "locations";
    public static final String CTXKEY_POINT_OF_SALES = "pointOfSales";
    public static final String CTXKEY_LOCATIONS_N_POSES = "locationsPoses";
    
    public static final String SEPARATOR_LOC_POS = "-";
    
    private static final String[] COLUMN_HEADERS = { "CardType", "CardNumber", "StartDate", "EndDate", "LocationName", "PayStationName",
        "Description", "RestrictCard", "GraceMinutes", "maxNumberOfUses" };
    private static final int NUM_COLUMNS = COLUMN_HEADERS.length;
    private static final String FMT_DATE = "yyyy-MM-dd";
    
    private static final String CTXKEY_DATEFMT = "dateFormat";
    
    @Autowired
    private CustomerCardCsvValidator validator;
    
    @Autowired
    private CustomerCardTypeService customerCardTypeService;
    
    @Autowired
    private LocationService locationService;
    
    @Autowired
    private PointOfSaleService posService;
    
    public CustomerCardCsvReader() {
        super(COLUMN_HEADERS);
    }
    
    @Override
    protected void init(Map<String, Object> context, List<CsvProcessingError> errors, List<CsvProcessingError> warnings) {
        context.put(CTXKEY_RESULT, new TreeSet<CustomerCard>(new CustomerCardComparator()));
        
        SimpleDateFormat dateFmt = new SimpleDateFormat(FMT_DATE + " HH:mm:ss");
        dateFmt.setTimeZone((TimeZone) context.get(CTXKEY_CUSTOMER_TIME_ZONE));
        dateFmt.setLenient(false);
        context.put(CTXKEY_DATEFMT, dateFmt);
        
        super.init(context, errors, warnings);
    }
    
    @Override
    protected void initForCreate(Map<String, Object> context) {
        SimpleDateFormat dateFmt = new SimpleDateFormat(FMT_DATE);
        dateFmt.setTimeZone((TimeZone) context.get(CTXKEY_CUSTOMER_TIME_ZONE));
        context.put(CTXKEY_DATEFMT, dateFmt);
        super.initForCreate(context);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    protected void processLine(List<CsvProcessingError> errors, List<CsvProcessingError> warnings, String line, int lineNum,
        Map<String, Object> context, boolean foundErrors) {
        TreeSet<CustomerCard> result = (TreeSet<CustomerCard>) context.get(CTXKEY_RESULT);
        
        String[] tokens = CsvParserUtils.split(line);
        if (lineNum == 1) {
            CsvParserUtils.verifyHeaders(errors, tokens, COLUMN_HEADERS, NUM_COLUMNS);
        } else {
            if (line.trim().length() > 0) {
                if (tokens.length < NUM_COLUMNS) {
                    errors.add(new CsvProcessingError(
                            validator.getMessage("error.common.csv.import.insufficient.column", validator.getMessage("label.customerCard"))));
                } else {
                    CustomerCardCsvDTO dto = new CustomerCardCsvDTO(tokens);
                    validator.validateRow(errors, warnings, dto, context);
                    if (errors.size() <= 0) {
                        CustomerCard card = createCustomerCard(errors, warnings, dto, lineNum, context);
                        if (!result.add(card)) {
                            CsvProcessingError csvProcErr = new CsvProcessingError(validator
                                    .getMessage("error.common.import.duplicated.record", validator.getMessage("label.customerCard"),
                                                validator.getMessage("label.connector.and", validator.getMessage("label.customerCard.cardType"),
                                                                     validator.getMessage("label.customerCard.cardNumber")),
                                                validator.getMessage("label.connector.and", dto.getCardType(), dto.getCardNumber())));
                            
                            warnings.add(csvProcErr);
                        }
                    }
                }
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    private CustomerCard createCustomerCard(List<CsvProcessingError> errors, List<CsvProcessingError> warnings, CustomerCardCsvDTO dto,
        int lineNumber, Map<String, ?> context) {
        DateFormat dateFmt = (DateFormat) context.get(CTXKEY_DATEFMT);
        Map<String, CustomerCardTypeWithLineNumbers> cardTypes = (Map<String, CustomerCardTypeWithLineNumbers>) context.get(CTXKEY_CARD_TYPES);
        Map<String, LocationWithLineNumbers> locations = (Map<String, LocationWithLineNumbers>) context.get(CTXKEY_LOCATIONS);
        Map<String, PointOfSaleWithLineNumbers> poses = (Map<String, PointOfSaleWithLineNumbers>) context.get(CTXKEY_POINT_OF_SALES);
        Map<String, PointOfSaleWithLineNumbers> locPoses = (Map<String, PointOfSaleWithLineNumbers>) context.get(CTXKEY_LOCATIONS_N_POSES);
        
        CustomerCard card = new CustomerCard();
        
        CustomerCardType cardType = cardTypes.get(dto.getCardType().toLowerCase());
        
        card.setCustomerCardType(cardType);
        if (cardType instanceof CustomerCardTypeWithLineNumbers) {
            ((CustomerCardTypeWithLineNumbers) cardType).getLineNumbers().add(lineNumber);
        }
        
        card.setCardNumber(dto.getCardNumber());
        
        if (dto.getStartDate() == null) {
            card.setCardBeginGmt(WebCoreConstants.NO_START_DATE);
        } else {
            try {
                card.setCardBeginGmt(dateFmt.parse(dto.getStartDate() + " 00:00:00"));
            } catch (ParseException pe) {
                errors.add(new CsvProcessingError(
                        validator.getMessage("error.common.invalid", validator.getMessage("label.customerCard.validFrom"))));
            }
        }
        
        if (dto.getEndDate() == null) {
            card.setCardExpireGmt(WebCoreConstants.NO_END_DATE);
        } else {
            try {
                card.setCardExpireGmt(dateFmt.parse(dto.getEndDate() + " 23:59:59"));
            } catch (ParseException pe) {
                errors.add(new CsvProcessingError(validator.getMessage("error.common.invalid", validator.getMessage("label.customerCard.validTo"))));
            }
        }
        
        if ((card.getCardBeginGmt() != null) && (card.getCardExpireGmt() != null)) {
            if (card.getCardBeginGmt().after(card.getCardExpireGmt())) {
                errors.add(new CsvProcessingError(
                        validator.getMessage("error.common.endAfterCurrent", validator.getMessage("label.customerCard.validTo"),
                                             validator.getMessage("label.customerCard.validFrom"))));
            }
            
            Date now = new Date();
            if (now.after(card.getCardExpireGmt())) {
                errors.add(new CsvProcessingError(
                        validator.getMessage("error.common.endAfterCurrent", validator.getMessage("label.customerCard.validTo"),
                                             validator.getMessage("label.coupon.currentDate"))));
            }
        }
        
        if (dto.getPointOfSale() == null) {
            if (dto.getLocation() != null) {
                Location loc = locations.get(dto.getLocation().toLowerCase());
                
                card.setLocation(loc);
                if (loc instanceof LocationWithLineNumbers) {
                    ((LocationWithLineNumbers) loc).getLineNumbers().add(lineNumber);
                }
            }
        } else {
            PointOfSale pos = null;
            if (dto.getLocation() == null) {
                pos = poses.get(dto.getPointOfSale().toLowerCase());
                
            } else {
                pos = locPoses.get(dto.getLocation().toLowerCase() + SEPARATOR_LOC_POS + dto.getPointOfSale().toLowerCase());
            }
            
            card.setPointOfSale(pos);
            if (pos instanceof PointOfSaleWithLineNumbers) {
                ((PointOfSaleWithLineNumbers) pos).getLineNumbers().add(lineNumber);
            }
        }
        
        card.setComment(dto.getComment());
        
        card.setIsForNonOverlappingUse(false);
        if (dto.getRestricted() != null) {
            String restricted = dto.getRestricted().toLowerCase();
            card.setIsForNonOverlappingUse("true".equals(restricted) || "yes".equals(restricted) || "on".equals(restricted));
        }
        
        if (card.isIsForNonOverlappingUse()) {
            if (dto.getGracePeriod() != null) {
                card.setGracePeriodMinutes(new Short(dto.getGracePeriod()));
            }
        }
        
        card.setMaxNumberOfUses((dto.getMaxNumOfUses() == null) ? null : new Short(dto.getMaxNumOfUses()));
        
        return card;
    }
    
    @Override
    protected void createLine(CsvWriter writer, CustomerCard data, int lineNumber, Map<String, Object> context) throws IOException {
        DateFormat dateFmt = (DateFormat) context.get(CTXKEY_DATEFMT);
        
        writer.writeColumn(data.getCustomerCardType().getName());
        writer.writeColumn(data.getCardNumber());
        
        writer.writeColumn(((data.getCardBeginGmt() == null) || (WebCoreConstants.NO_START_DATE.equals(data.getCardBeginGmt()))) ? null
                : dateFmt.format(data.getCardBeginGmt()));
        writer.writeColumn(((data.getCardExpireGmt() == null) || (WebCoreConstants.NO_END_DATE.equals(data.getCardExpireGmt()))) ? null
                : dateFmt.format(data.getCardExpireGmt()));
        
        Location loc = data.getLocation();
        PointOfSale pos = data.getPointOfSale();
        if (pos != null) {
            loc = pos.getLocation();
        } else if (loc != null) {
            pos = null;
        }
        
        writer.writeColumn((loc == null) ? null : loc.getName());
        writer.writeColumn((pos == null) ? null : pos.getName());
        
        writer.writeColumn(data.getComment());
        
        writer.writeColumn(data.isIsForNonOverlappingUse());
        writer.writeColumn((data.isIsForNonOverlappingUse()) ? null : data.getGracePeriodMinutes());
        
        writer.writeColumn(data.getMaxNumberOfUses());
    }
}
