package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;

import javax.persistence.Transient;

public class AlertCentreFilterForm implements Serializable {
    
    private static final long serialVersionUID = 8686415924256595313L;
    private String randomId;
    private boolean isLocationOrRoute;
    private boolean isAlert;
    private Integer pageNumber;
    private Integer page;
    private String targetId;
    private Long timeStamp;
    private String severityRandomId;
    private String eventDeviceRandomId;
    private String column;
    private String order;
    private Long dataKey;
    
    @Transient
    private transient Integer id;
    @Transient
    private transient Integer severityId;
    @Transient
    private transient Integer eventDeviceId;
    
    public AlertCentreFilterForm() {
        this.isLocationOrRoute = false;
    }
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final boolean isLocationOrRoute() {
        return this.isLocationOrRoute;
    }
    
    public final boolean getLocationOrRoute() {
        return this.isLocationOrRoute;
    }
    
    public final void setLocationOrRoute(final boolean locationOrRoute) {
        this.isLocationOrRoute = locationOrRoute;
    }
    
    public final boolean isAlert() {
        return this.isAlert;
    }
    
    public final boolean getIsAlert() {
        return this.isAlert;
    }
    
    public final void setAlert(final boolean alert) {
        this.isAlert = alert;
    }
    
    public final void setIsAlert(final boolean isAlert) {
        this.isAlert = isAlert;
    }
    
    public final Integer getPageNumber() {
        return this.pageNumber;
    }
    
    public final void setPageNumber(final Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public final Integer getPage() {
        return this.page;
    }
    
    public final void setPage(final Integer page) {
        this.page = page;
    }

    public final Long getTimeStamp() {
        return this.timeStamp;
    }
    
    public final void setTimeStamp(final Long timeStamp) {
        this.timeStamp = timeStamp;
    }
    
    public final String getSeverityRandomId() {
        return this.severityRandomId;
    }
    
    public final void setSeverityRandomId(final String severityRandomId) {
        this.severityRandomId = severityRandomId;
    }
    
    public final String getEventDeviceRandomId() {
        return this.eventDeviceRandomId;
    }
    
    public final void setEventDeviceRandomId(final String eventDeviceRandomId) {
        this.eventDeviceRandomId = eventDeviceRandomId;
    }
    
    public final Integer getId() {
        return this.id;
    }
    
    public final void setId(final Integer id) {
        this.id = id;
    }
    
    public final Integer getSeverityId() {
        return this.severityId;
    }
    
    public final void setSeverityId(final Integer severityId) {
        this.severityId = severityId;
    }
    
    public final Integer getEventDeviceId() {
        return this.eventDeviceId;
    }
    
    public final void setEventDeviceId(final Integer eventDeviceId) {
        this.eventDeviceId = eventDeviceId;
    }
    
    public final String getColumn() {
        return this.column;
    }
    
    public final void setColumn(final String column) {
        this.column = column;
    }
    
    public final String getOrder() {
        return this.order;
    }
    
    public final void setOrder(final String order) {
        this.order = order;
    }
    
    public final Long getDataKey() {
        return this.dataKey;
    }
    
    public final void setDataKey(final Long dataKey) {
        this.dataKey = dataKey;
    }
    
    public final String getTargetId() {
        return this.targetId;
    }

    public final void setTargetId(final String targetId) {
        this.targetId = targetId;
    }
}
