package com.digitalpaytech.mvc.customeradmin.support.io.dto;

import java.io.Serializable;

public class SensorHistoryCsvDTO implements Serializable {
    
    private static final long serialVersionUID = -5017170962580478649L;
    private String name;
    private String date;
    private String value;
    private String unit;
    private String chargeState;
    
    public SensorHistoryCsvDTO() {
        
    }
    
    public SensorHistoryCsvDTO(String name, String date, String value, String unit) {
        super();
        this.name = name;
        this.date = date;
        this.value = value;
        this.unit = unit;
    }
    
    public SensorHistoryCsvDTO(String name, String date, String value, String unit, String chargeState) {
        super();
        this.name = name;
        this.date = date;
        this.value = value;
        this.unit = unit;
        this.chargeState = chargeState;
    }
    
    public void setDate(String date) {
        this.date = date;
    }
    
    public String getDate() {
        return date;
    }
    
    public void setValue(String value) {
        this.value = value;
    }
    
    public String getValue() {
        return value;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public void setUnit(String unit) {
        this.unit = unit;
    }
    
    public String getUnit() {
        return unit;
    }
    
    public String getChargeState() {
        return chargeState;
    }
    
    public void setChargeState(String chargeState) {
        this.chargeState = chargeState;
    }
}
