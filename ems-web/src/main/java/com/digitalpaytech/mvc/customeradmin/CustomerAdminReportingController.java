package com.digitalpaytech.mvc.customeradmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.dto.AdhocReportInfo;
import com.digitalpaytech.mvc.ReportingController;
import com.digitalpaytech.mvc.customeradmin.support.ReportEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

/*
 * This structure is based off of the UI mock ups as of July 13
 * 
 */

@Controller
public class CustomerAdminReportingController extends ReportingController {
    private static Logger logger = Logger.getLogger(CustomerAdminReportingController.class);
    
    @ModelAttribute(FORM_EDIT)
    public WebSecurityForm<ReportEditForm> initReportForm(HttpServletRequest request) {
        ReportEditForm reportForm = new ReportEditForm();
        return new WebSecurityForm<ReportEditForm>((Object) null, reportForm, SessionTool.getInstance(request).locatePostToken(FORM_EDIT));
    }
    
    /***********************************************************************************
     * ReportLists
     ***********************************************************************************/
    
    /**
     * This method will look at the permissions in the WebUser and pass control to the Reporting landing page if allowed.
     * If the customer does not subscribe to the reports module, then no, there should be no access to either reports or permit lookup.
     * 
     * @return Will return the page for pending/scheduled/ finished report lists or error if no permissions exist.
     */
    @RequestMapping(value = "/secure/reporting/index.html", method = RequestMethod.GET)
    public String getReportLists(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!WebSecurityUtil.hasValidCustomerSubscription(WebCoreConstants.SUBSCRIPTION_TYPE_STANDARD_REPORT)
            || !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_REPORTS)) {
            if (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_TRANSACTION_REPORTS)) {
                return "redirect:/secure/reporting/transactionReceipts/index.html";
            } else {
                if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                    && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                    return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
                }
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
            }
        }
        return super.getReportLists(request, response, model, "/reporting/index");
    }
    
    /**
     * This method will look at the permissions in the WebUser and pass control to the Reporting landing page if allowed.
     * 
     * @return Will return the page for pending/scheduled/ finished report lists or error if no permissions exist.
     */
    @RequestMapping(value = "/secure/reporting/unreadReports.html", method = RequestMethod.GET)
    public String getUnreadReportNumber(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.getUnreadReportNumber(request, response, model);
    }
    
    /**
     * This method will look at the permissions in the WebUser and pass control to the Reporting landing page if allowed.
     * 
     * @return Will return the page for pending/scheduled/ finished report lists or error if no permissions exist.
     */
    @RequestMapping(value = "/secure/reporting/sortReportsList.html", method = RequestMethod.GET)
    public String sortReports(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.sortReports(request, response, model, "/include/reporting/sortReportsList");
    }
    
    /**
     * This method will look at the permissions in the WebUser and pass control to the Reporting landing page if allowed.
     * 
     * @return Will return the page for pending/scheduled/ finished report lists or error if no permissions exist.
     */
    @RequestMapping(value = "/secure/reporting/sortReportDefinitionsList.html", method = RequestMethod.GET)
    public String sortReportDefinitions(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.sortReportDefinitions(request, response, model, "/include/reporting/sortReportDefinitionsList");
    }
    
    /**
     * This method takes a reportQueue Id and modifies the status to Cancelled if the user has valid permissions. If the report is launched before the user
     * tries to cancel it response status is set to HTTP_STATUS_CODE_483.
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON object of Pending Reports and Completed Reports lists.
     */
    @RequestMapping(value = "/secure/reporting/cancel.html", method = RequestMethod.GET)
    public String cancelQueuedReport(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.cancelQueuedReport(request, response, model, "/include/reporting/sortReportsList");
    }
    
    /**
     * This method takes a reportHistory Id (coming from Pending Reports) or reportRepository Id (coming from Completed Reports) and creates a new reportQueue
     * entry using the reportDefinition of the reportHistory item if the user has permission.
     * 
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON object of Pending Reports and Completed Reports lists.
     */
    // TODO is status or type check needed? 
    @RequestMapping(value = "/secure/reporting/rerun.html", method = RequestMethod.GET)
    public String rerunReport(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.rerunReport(request, response, model, "/include/reporting/sortReportsList");
    }
    
    /**
     * This method takes a reportDefinition Id and creates a new reportQueue entry if the user has permission.
     * 
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON object of Pending Reports and Completed Reports lists.
     */
    @RequestMapping(value = "/secure/reporting/runNow.html", method = RequestMethod.GET)
    public String runNow(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.runNow(request, response, model, "/include/reporting/sortReportsList");
    }
    
    /**
     * This method takes a reportRepository Id and deletes the content of the report from reportRepository and reportContent tables if the user has permission.
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON object of Pending Reports and Completed Reports lists.
     */
    // TODO is status or type check needed? 
    @RequestMapping(value = "/secure/reporting/delete.html", method = RequestMethod.GET)
    public String deleteReport(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.deleteReport(request, response, model, "/include/reporting/sortReportsList");
    }
    
    /**
     * This method takes a reportRepository Id and deletes the content of the report from reportRepository and reportContent tables if the user has permission.
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON object of Pending Reports and Completed Reports lists.
     */
    // TODO is status or type check needed? 
    @RequestMapping(value = "/secure/reporting/viewReport.html", method = RequestMethod.GET)
    public @ResponseBody String viewReport(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.viewReport(request, response, model);
    }
    
    /**
     * This method takes a reportDefinition Id and changes the status to Deleted if the user has permission.
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON object of Scheduled Reports list.
     */
    // TODO is status or type check needed? 
    @RequestMapping(value = "/secure/reporting/deleteDefinition.html", method = RequestMethod.GET)
    public String deleteReportDefinition(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.deleteReportDefinition(request, response, model, "/include/reporting/sortReportDefinitionsList");
    }
    
    /***********************************************************************************
     * new/edit Report
     ***********************************************************************************/
    
    /**
     * This method will look at the permissions in the WebUser and pass control to the new Report page if allowed.
     * 
     * @return Will return the page for new reports or error if no permissions exist.
     */
    @RequestMapping(value = "/secure/reporting/newReport.html", method = RequestMethod.GET)
    public String newReport(HttpServletRequest request, HttpServletResponse response, ModelMap model,
        @ModelAttribute(FORM_EDIT) WebSecurityForm<ReportEditForm> secForm) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.newReport(request, response, model, secForm, "/reporting/newReport");
    }
    
    /**
     * This method will look at the permissions in the WebUser and depending on the report type selected create the data needed to create the new report
     * settings if the user has permission.
     * 
     * @return Will return /reporting/formReport.vm with all filters in the model or error if no permissions exist.
     */
    @ResponseBody
    @RequestMapping(value = "/secure/reporting/formReport.html", method = RequestMethod.GET)
    public String newReportSettings(@ModelAttribute(FORM_EDIT) WebSecurityForm<ReportEditForm> secForm, BindingResult result,
        HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.newReportSettings(secForm, result, request, response, model);
    }
    
    /**
     * This method takes a WebSecurityForm from the model and after validating the contents of the report edit form, saves the report definition information to
     * the database.
     * 
     * @param webSecurityForm
     * @param result
     * @param request
     * @param response
     * @param model
     * @return A response of true for a successful save, JSON document for
     *         validation errors, or null for database records not found
     */
    @RequestMapping(value = "/secure/reporting/saveReport.html", method = RequestMethod.POST)
    public @ResponseBody String saveReport(@ModelAttribute(FORM_EDIT) WebSecurityForm<ReportEditForm> webSecurityForm, BindingResult result,
        HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.saveReport(webSecurityForm, result, request, response, model);
    }
    
    /**
     * This method will open the report settings page look at the permissions in the WebUser and depending on the report type selected create the data needed to
     * create the new report
     * settings if the user has permission.
     * 
     * @return Will return /reporting/editReport.vm with all filters in the model or error if no permissions exist.
     */
    @RequestMapping(value = "/secure/reporting/editReport.html", method = RequestMethod.GET)
    public String editReportSettings(HttpServletRequest request, HttpServletResponse response, ModelMap model,
        @ModelAttribute(FORM_EDIT) WebSecurityForm<ReportEditForm> secForm) {
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.editReportSettings(request, response, model, secForm, "/reporting/newReport");
    }
    
    @RequestMapping(value = "/secure/reporting/recreateReport.html", method = RequestMethod.GET)
    public String recreateReportSettings(HttpServletRequest request, HttpServletResponse response, ModelMap model,
        @ModelAttribute(FORM_EDIT) WebSecurityForm<ReportEditForm> secForm) {
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.recreateReportSettings(request, response, model, secForm, "/reporting/newReport");
    }
    
    @RequestMapping(value = "/secure/reporting/fixReport.html", method = RequestMethod.GET)
    public String fixReportSettings(HttpServletRequest request, HttpServletResponse response, ModelMap model,
        @ModelAttribute(FORM_EDIT) WebSecurityForm<ReportEditForm> secForm) {
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.fixReportSettings(request, response, model, secForm, "/reporting/newReport");
    }
    
    @RequestMapping(value = "/secure/reporting/deleteHistory.html", method = RequestMethod.GET)
    public String deleteReportHistory(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.deleteReportHistory(request, response, model, "/include/reporting/sortReportsList");
    }
    
    @RequestMapping(value = "/secure/reporting/getOrganizationFilters.html", method = RequestMethod.GET)
    @ResponseBody
    public String getAdditionalFilters(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.getAdditionalFilters(request, response, model);
    }
    
    @RequestMapping(value = URI_ADHOC_REPORT_STATUS, method = RequestMethod.GET)
    @ResponseBody
    public String getAdhocReportStatus(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        AdhocReportInfo ari = super.retrieveAdhocReportInfo(request);
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)
            && !super.hasUserPermissionForAdhocReport(ari)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.getAdhocReportStatus(request, response, model, ari);
    }
    
    @RequestMapping(value = URI_ADHOC_REPORT_VIEW, method = RequestMethod.GET)
    @ResponseBody
    public String viewAdhocReport(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)
            && !super.hasUserPermissionForAdhocReport(request)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
        }
        
        return super.viewAdhocReport(request, response, model);
    }
    
    @RequestMapping(value = "/secure/reporting/searchConsumers.html", method = RequestMethod.POST)
    @ResponseBody
    public String searchConsumers(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)
            && !super.hasUserPermissionForAdhocReport(request)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
        }
        return super.searchConsumerForAccountName(request, response, model);
    }
    
}
