package com.digitalpaytech.mvc.customeradmin;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.customeradmin.support.CustomerMigrationForm;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.CustomerMigrationService;
import com.digitalpaytech.service.CustomerMigrationStatusTypeService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.LabelConstants;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.customeradmin.CustomerMigrationValidator;

/**
 * This class handles user request to migrate all data from old EMS to new EMS
 * when user clicks "migrate me" button on the page.
 * 
 * @author Brian Kim
 *         
 */
@Controller
@SessionAttributes(CustomerMigrationController.FORM_EDIT)
public class CustomerMigrationController implements MessageSourceAware {
    
    public static final String FORM_EDIT = "customerMigrationForm";
    private static Logger log = Logger.getLogger(CustomerMigrationController.class);
    
    @Autowired
    private CustomerMigrationService customerMigrationService;
    
    @Autowired
    private CustomerMigrationStatusTypeService customerMigrationStatusTypeService;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private CustomerMigrationValidator customerMigrationValidator;
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    public final void setCustomerMigrationService(final CustomerMigrationService customerMigrationService) {
        this.customerMigrationService = customerMigrationService;
    }
    
    public final void setCustomerMigrationStatusTypeService(final CustomerMigrationStatusTypeService customerMigrationStatusTypeService) {
        this.customerMigrationStatusTypeService = customerMigrationStatusTypeService;
    }
    
    public final void setMailerService(final MailerService mailerService) {
        this.mailerService = mailerService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final void setCustomerMigrationValidator(final CustomerMigrationValidator customerMigrationValidator) {
        this.customerMigrationValidator = customerMigrationValidator;
    }
    
    public final void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }
    
    @Override
    public final void setMessageSource(final MessageSource messageSource) {
        this.messageSource = messageSource;
    }
    
    /**
     * This method will returns customer migration setting form and displays
     * customer migration page.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return velocity vm file name (/settings/viewMigration.vm) if process
     *         succeeds, NULL with 401 response status if fails.
     */
    @RequestMapping(value = "/secure/settings/viewMigration.html", method = RequestMethod.GET)
    @ResponseBody
    public final String validateMigration(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.isCustomerAdmin()) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final CustomerMigration customerMigration =
                this.customerMigrationService.findCustomerMigrationByCustomerId(userAccount.getCustomer().getId());
                
        final Date earliestMigrationDate = this.commonControllerHelper.getEarliestMigrationDate(customerMigration.getCustomerBoardedGmt());
        final boolean isReadyForMigration = this.commonControllerHelper
                .getReadyForMigration(customerMigration.getCustomerMigrationValidationStatusType().getId(), earliestMigrationDate);
                
        if (!isReadyForMigration) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append(WebCoreConstants.RESPONSE_FALSE).append(StandardConstants.STRING_COLON)
                    .append(this.commonControllerHelper.getMessage("error.customer.notReady"));
            return bdr.toString();
        }
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    @RequestMapping(value = "/secure/settings/include/viewMigration.html", method = RequestMethod.GET)
    public final String showMigrationPage(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate if login user is the admin user for the customer. */
        if (!WebSecurityUtil.isCustomerAdmin()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        /* retrieve customer email and set in the form. */
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        String emailAddress = null;
        if (userAccount.getCustomerEmail() != null) {
            try {
                emailAddress = URLDecoder.decode(userAccount.getCustomerEmail().getEmail(), WebSecurityConstants.URL_ENCODING_LATIN1);
            } catch (UnsupportedEncodingException uee) {
                emailAddress = null;
            }
        }
        
        final CustomerMigrationForm form = new CustomerMigrationForm();
        final WebSecurityForm<CustomerMigrationForm> webSecurityForm = new WebSecurityForm<CustomerMigrationForm>(null, form);
        model.put(FORM_EDIT, webSecurityForm);
        model.put("migrationScheduleTimeRange", this.commonControllerHelper.getRequestTimeRangeString());
        
        return "/settings/include/viewMigration";
    }
    
    /**
     * This method stores user migration schedule in the database after user
     * input validation.
     * 
     * @param webSecurityForm
     *            User Input Form
     * @param result
     *            BindingResult object to hold validation errors
     * @return "true" if process succeeds, JSON string if fails.
     */
    @RequestMapping(value = "/secure/settings/scheduleMigration.html", method = RequestMethod.POST)
    @ResponseBody
    public final String scheduleMigration(@ModelAttribute(FORM_EDIT) final WebSecurityForm<CustomerMigrationForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate if login user is the admin user for the customer. */
        if (!WebSecurityUtil.isCustomerAdmin()) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        this.customerMigrationValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        /* store user migration schedule. */
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final CustomerMigrationForm form = webSecurityForm.getWrappedObject();
        try {
            final List<String> emailAddressList = new ArrayList<String>();
            try {
                for (String email : form.getMigrationContacts()) {
                    emailAddressList.add(URLEncoder.encode(email, WebSecurityConstants.URL_ENCODING_LATIN1));
                }
                form.setMigrationContacts(emailAddressList);
            } catch (UnsupportedEncodingException uee) {
                return WidgetMetricsHelper.convertToJson(
                                                         new FormErrorStatus(FormFieldConstants.CUSTOMER_MIGRATION,
                                                                 this.messageSource.getMessage(LabelConstants.ERROR_CUSTOMER_MIGRATION_FAIL, null,
                                                                                               LocaleContextHolder.getLocale())),
                                                         WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
                                                         
            }
            
            //TODO CustomerMigration fix
            if (saveCustomerMigration(userAccount, form)) {
                // send emails to IT
                final String itEmailAddress = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.CUSTOMER_MIGRATION_ALERT_EMAIL,
                                                                                         EmsPropertiesService.CUSTOMER_MIGRATION_ALERT_EMAIL_DEFAULT);
                                                                                         
                this.mailerService
                        .sendMessage(itEmailAddress,
                                     this.messageSource
                                             .getMessage("customermigration.email.admin.migrationrequested.title",
                                                         new Object[] { userAccount.getCustomer().getId() }, LocaleContextHolder.getLocale()),
                                     this.messageSource.getMessage("customermigration.email.admin.migrationrequested.content",
                                                                   new Object[] { userAccount.getCustomer().getId(),
                                                                       userAccount.getCustomer().getName(), },
                                                                   LocaleContextHolder.getLocale()),
                                     null);
                                     
                // send email to Customer
                final StringBuilder emailAddresses = new StringBuilder();
                for (String email : form.getMigrationContacts()) {
                    emailAddresses.append(email);
                    emailAddresses.append(StandardConstants.STRING_COMMA);
                }
                
                this.mailerService.sendMessage(URLDecoder.decode(emailAddresses.toString(), WebSecurityConstants.URL_ENCODING_LATIN1),
                                               this.messageSource.getMessage("customermigration.email.customer.migrationrequested.title",
                                                                             new Object[] { userAccount.getCustomer().getName() },
                                                                             LocaleContextHolder.getLocale()),
                                               this.messageSource.getMessage("customermigration.email.customer.migrationrequested.content",
                                                                             new Object[] { userAccount.getCustomer().getName() },
                                                                             LocaleContextHolder.getLocale()),
                                               null);
                return WebCoreConstants.RESPONSE_TRUE;
            } else {
                return WidgetMetricsHelper.convertToJson(
                                                         new FormErrorStatus(FormFieldConstants.CUSTOMER_MIGRATION,
                                                                 this.messageSource.getMessage(LabelConstants.ERROR_CUSTOMER_MIGRATION_FAIL, null,
                                                                                               LocaleContextHolder.getLocale())),
                                                         WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
            }
        } catch (UnsupportedEncodingException uee) {
            log.error("\r\nException occurred in scheduleMigration, ", uee);
            
            return WidgetMetricsHelper.convertToJson(
                                                     new FormErrorStatus(FormFieldConstants.CUSTOMER_MIGRATION,
                                                             this.messageSource.getMessage(LabelConstants.ERROR_CUSTOMER_MIGRATION_FAIL, null,
                                                                                           LocaleContextHolder.getLocale())),
                                                     WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
    }
    
    /**
     * This method saves customer migration user input.
     * 
     * @param form
     *            User Input Form
     * @param userAccount
     *            logged-in UserAccount object
     * @return true boolean if insert succeeds, false if fails.
     */
    private boolean saveCustomerMigration(final UserAccount userAccount, final CustomerMigrationForm form) {
        final Date currentTime = DateUtil.getCurrentGmtDate();
        final CustomerMigration customerMigration =
                this.customerMigrationService.findCustomerMigrationByCustomerId(userAccount.getCustomer().getId());
                
        if (log.isDebugEnabled()) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("\r\nCustomerId: ").append(userAccount.getCustomer().getId()).append(", CustomerMigration object: ").append(customerMigration);
            log.debug(bdr.toString());
        }
        
        customerMigration.setCustomer(userAccount.getCustomer());
        customerMigration.setUserAccount(userAccount);
        customerMigration.setCustomerMigrationStatusType(this.customerMigrationStatusTypeService
                .findCustomerMigrationStatusType(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_REQUESTED));
        customerMigration.setMigrationRequestedGmt(currentTime);
        customerMigration.setMigrationScheduledGmt(form.getScheduleRealDate());
        customerMigration.setLastModifiedGmt(currentTime);
        customerMigration.setLastModifiedByUserId(userAccount.getId().intValue());
        final Integer id = (Integer) this.customerMigrationService.updateCustomerMigration(customerMigration, form.getMigrationContacts());
        
        if (log.isDebugEnabled()) {
            log.debug("saveCustomerMigration, id: " + id);
        }
        return id != null && id.intValue() > 0;
    }
    
}
