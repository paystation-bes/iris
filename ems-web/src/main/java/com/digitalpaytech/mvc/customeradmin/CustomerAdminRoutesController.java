package com.digitalpaytech.mvc.customeradmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.RoutesController;
import com.digitalpaytech.mvc.customeradmin.support.RouteEditForm;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.customeradmin.RouteSettingValidator;

/**
 * This class handles the Route setting request in Settings menu.
 * 
 * @author Brian Kim
 * 
 */
@Controller
@SessionAttributes({ "routeEditForm" })
public class CustomerAdminRoutesController extends RoutesController {
	
    @Autowired
    private RouteSettingValidator routeSettingValidator;
    
    /**
     * This method will look at the permissions in the WebUser and pass control
     * to the first tab that the user has permissions for.
     * 
     * @return Will return the method for routeDetails, or error if no
     *         permissions exist.
     */
    @RequestMapping(value = "/secure/settings/routes/index.html")
    public String getFirstPermittedTab(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_ROUTES)
            || WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES)) {
            return getRouteListByCustomer(request, response, model);
        } else {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD) && 
                    !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) { 
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }            
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
    }
    
    /**
     * This method will retrieve all customer's route information and display
     * them in Settings->Routes main page.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return target vm file URI which is mapped to Settings->Routes
     *         (/settings/routes/index.vm)
     */
    @RequestMapping(value = "/secure/settings/routes/routeDetails.html", method = RequestMethod.GET)
    public String getRouteListByCustomer(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        /* get customer information from security framework session. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_ROUTES)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        UserAccount userAccount = WebSecurityUtil.getUserAccount();
        Customer customer = userAccount.getCustomer();
        
        return super.getRouteListByCustomer(request, response, model, customer, "/settings/routes/routeDetails");
    }
    
    /**
     * This method will sort the route list based on user input and convert them
     * into JSON list and pass back to UI.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return Velocity file URL (/settings/include/sortRouteList.vm). "false"
     *         in case of any exception or validation failure.
     */
    @RequestMapping(value = "/secure/settings/routes/sortRouteList.html", method = RequestMethod.GET)
    public String sortRouteList(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        /* get customer information from security framework session. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_ROUTES)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        UserAccount userAccount = WebSecurityUtil.getUserAccount();
        Customer customer = userAccount.getCustomer();
        
        return super.sortRouteList(request, response, model, customer, "/settings/include/sortRouteList");
    }
    
    /**
     * This method will view Route Detail information on the page. When
     * "Edit Route" is clicked, it will also be called from front-end logic
     * automatically after calling getRouteForm() method.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return JSON string having route detail information. "false" in case of
     *         any exception or validation failure.
     */
    @RequestMapping(value = "/secure/settings/routes/viewRoutesDetails.html", method = RequestMethod.GET)
    public @ResponseBody
    String viewRoutesDetails(HttpServletRequest request, HttpServletResponse response) {
        
        /* validate user account if it has proper permission to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_ROUTES)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.viewRoutesDetails(request, response, WebSecurityUtil.getUserAccount().getCustomer().getId());
    }
    
    /**
     * This method will be called when "Add Route" is clicked.
     * It will return list of all pay stations with JSON format.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return String JSON string containing all pay station list.
     */
    @RequestMapping(value = "/secure/settings/routes/form.html", method = RequestMethod.GET)
    public @ResponseBody
    String getRouteForm(HttpServletRequest request, HttpServletResponse response) {
        
        /* get customer information from security framework session. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        UserAccount userAccount = WebSecurityUtil.getUserAccount();
        Customer customer = userAccount.getCustomer();
        
        return super.getRouteForm(request, response, customer);
    }
    
    /**
     * This method saves Route information in database when save button is
     * clicked from UI.
     * 
     * @param webSecurityForm
     *            user input form
     * @param result
     *            Error object
     * @param response
     *            HttpServletResponse object
     * @return "true" string value if process succeeds, JSON error message if
     *         process fails
     */
    @RequestMapping(value = "/secure/settings/routes/saveRoute.html", method = RequestMethod.POST)
    public @ResponseBody
    String saveRoute(@ModelAttribute("routeEditForm") WebSecurityForm<RouteEditForm> webSecurityForm, BindingResult result, HttpServletResponse response) {
        
        /* validate user account if it has proper permission to view it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.saveRoute(webSecurityForm, result, response, WebSecurityUtil.getUserAccount().getCustomer());
    }
    
    
    /**
     * Verify whether this route can be deleted from database
     * 
     * @param request
     * @param response
     * @return {@link MessageInfo} in JSON format or "false" if user doesn't have permission to delete route.
     */
    @RequestMapping(value = "/secure/settings/routes/verifyDeleteRoute.html", method = RequestMethod.GET)
    public @ResponseBody String verifyDeleteRoute(HttpServletRequest request, HttpServletResponse response) {
    	if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return routeSettingValidator.verifyDeleteRoute(request);
    }
    
    /**
     * This method deletes Route information and it updates the status flag in
     * Route table.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return "true" string value if process succeeds, "false" string value if
     *         process fails.
     */
    @RequestMapping(value = "/secure/settings/routes/deleteRoute.html", method = RequestMethod.GET)
    public @ResponseBody
    String deleteRoute(HttpServletRequest request, HttpServletResponse response) {
        
        /* validate user account if it has proper permission to do it. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.deleteRoute(request, response);
    }
}
