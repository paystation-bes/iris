package com.digitalpaytech.mvc.customeradmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.mvc.PayStationDetailController;
import com.digitalpaytech.mvc.support.PayStationCollectionFilterForm;
import com.digitalpaytech.mvc.support.PayStationRecentActivityFilterForm;
import com.digitalpaytech.mvc.support.PayStationTransactionFilterForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
@SessionAttributes({ "recentActivityFilterForm", "transactionFilterForm", "collectionFilterForm" })
public class CustomerAdminPayStationDetailController extends PayStationDetailController {
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the battery voltage from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the batteryVoltage of the requested pay station in a JSON
     *         document
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/batteryVoltage.html")
    @ResponseBody
    public final String payStationDetailsCurrentStatusBatteryVoltage(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.payStationDetailsCurrentStatusBatteryVoltageSuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the sensor info trend from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the sensor info trend of the requested pay station in a JSON
     *         document
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/batteryVoltageTrend.html")
    @ResponseBody
    public final String payStationDetailsBatteryVoltageTrend(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.payStationDetailsBatteryVoltageTrendSuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the input current from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the inputCurrent of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/inputCurrent.html")
    @ResponseBody
    public final String payStationDetailsCurrentStatusInputCurrent(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.payStationDetailsCurrentStatusInputCurrentSuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the input current from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the inputCurrent trend of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/inputCurrentTrend.html")
    @ResponseBody
    public final String payStationDetailsInputCurrentTrend(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.payStationDetailsInputCurrentTrendSuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the ambient temperature from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the ambientTemperature of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/ambientTemperature.html")
    @ResponseBody
    public final String payStationDetailsCurrentStatusAmbientTemperature(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.payStationDetailsCurrentStatusAmbientTemperatureSuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the ambient temperature trend from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the ambientTemperature trend of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/ambientTemperatureTrend.html")
    @ResponseBody
    public final String payStationDetailsAmbientTemperatureTrend(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.payStationDetailsAmbientTemperatureTrendSuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the humidity from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the humidity of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/relativeHumidity.html")
    @ResponseBody
    public final String payStationDetailsCurrentStatusRelativeHumidity(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.payStationDetailsCurrentStatusRelativeHumiditySuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the humidity trend from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the humidity trend of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/relativeHumidityTrend.html")
    @ResponseBody
    public final String payStationDetailsRelativeHumidityTrend(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.payStationDetailsRelativeHumidityTrendSuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the controllerTemperature from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the controllerTemperature of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/controllerTemperature.html")
    @ResponseBody
    public final String payStationDetailsCurrentStatusControllerTemperature(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.payStationDetailsCurrentStatusControllerTemperatureSuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the controllerTemperature trend from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the controllerTemperature trend of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/controllerTemperatureTrend.html")
    @ResponseBody
    public final String payStationDetailsControllerTemperatureTrend(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.payStationDetailsControllerTemperatureTrendSuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the system load from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the controllerTemperature of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/systemLoad.html")
    @ResponseBody
    public final String payStationDetailsCurrentStatusSystemLoad(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        /* validate user account if it has proper role to view stations. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)
            && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStationDetailsSensorHistorySuper(request, response, model);
    }
    
    /**
     * This method retrieves and returns filter information for the alert
     * section of pay station details
     * 
     * @param request
     * @param response
     * @param model
     * @return alert information of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/alertFilter.html")
    public final String payStationDetailsAlertsFilter(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper role to view alerts. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.payStationDetailsAlertsFilterSuper(request, response, model, "/settings/locations/include/alertFilter");
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns alert information for the requested pay station
     * 
     * @param request
     * @param response
     * @param model
     * @return alert information of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/alerts.html", method = RequestMethod.GET)
    @ResponseBody
    public final String payStationDetailsAlerts(
        @ModelAttribute("recentActivityFilterForm") final WebSecurityForm<PayStationRecentActivityFilterForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response) {
        
        /* validate user account if it has proper role to manage pay stations. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        return super.payStationDetailsAlertsSuper(webSecurityForm, result, request, response);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns alert History as a CSV File for the requested pay station
     * 
     * @param request
     * @param response
     * @param model
     * @return alert information of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/alertsHistory.html")
    @ResponseBody
    public final String payStationDetailsAlertsHistory(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper role to manage pay stations. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.payStationDetailsAlertsHistorySuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns pay station details in a PayStationDetails object
     * 
     * @param request
     * @param response
     * @param model
     * @return A PayStationDetails object populated with the details of the
     *         requested pay station in a JSON document
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/information.html")
    @ResponseBody
    public final String payStationDetailsInformation(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.payStationDetailsInformationSuper(request, response, model, false);
    }
    
    /**
     * This method retrieves and returns filter information for the transaction report
     * section of pay station details
     * 
     * @param request
     * @param response
     * @param model
     * @return alert information of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/transactionFilter.html")
    public final String payStationDetailsTransactionFilter(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        /* validate user account if it has proper role to view alerts. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.payStationDetailsTransactionFilterSuper(request, response, model, "/settings/locations/include/transactionFilter");
    }
    
    /**
     * This method takes a payStationID request parameter input, and returns
     * data listing transaction reports for the last 90 days
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON objects collectionsReports and transactionReports
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/viewTransactionReports.html")
    @ResponseBody
    public final String viewTransactionReports(
        @ModelAttribute("transactionFilterForm") final WebSecurityForm<PayStationTransactionFilterForm> webSecurityForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.viewTransactionReportsSuper(webSecurityForm, result, request, response);
    }
    
    /**
     * This method retrieves and returns filter information for the collection report
     * section of pay station details
     * 
     * @param request
     * @param response
     * @param model
     * @return alert information of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/collectionFilter.html")
    public final String payStationDetailsCollectionFilter(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        /* validate user account if it has proper role to view alerts. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                    && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                    return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
                }
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
            }
        }
        
        return super.payStationDetailsCollectionFilterSuper(request, response, model, "/settings/locations/include/collectionFilter");
    }
    
    /**
     * This method takes a payStationID request parameter input, and returns
     * data listing collection reports for the last 90 days
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON objects collectionsReports and transactionReports
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/viewCollectionReports.html")
    @ResponseBody
    public final String viewCollectionReports(
        @ModelAttribute("collectionFilterForm") final WebSecurityForm<PayStationCollectionFilterForm> webSecurityForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                    && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                    return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
                }
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
            }
        }
        
        return super.viewCollectionReportsSuper(webSecurityForm, result, request, response);
    }
    
    @RequestMapping(value = "/secure/settings/locations/payStationList/printCollectionReport.html")
    @ResponseBody
    public final String printCollectionReport(
        @ModelAttribute("collectionFilterForm") final WebSecurityForm<PayStationCollectionFilterForm> webSecurityForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                    && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                    return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
                }
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
            }
        }
        
        return super.printCollectionReportSuper(webSecurityForm, result, request, response);
    }
    
    /**
     * This method takes a transaction report random ID as input which is taken
     * from the output from payStationDetailsReprots(). It then returns vm file URL
     * string "transactionReport" with details for that transaction
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/payStationDetails/transactionReport.html")
    public final String payStationDetailsReportsTransactionDetails(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                    && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                    return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
                }
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
            }
        }
        
        return super.payStationDetailsReportsTransactionDetails(request, response, model, "/settings/locations/include/transactionReportDetail");
    }
    
    /**
     * This method takes a collection report random ID as input which is taken
     * from the output from payStationDetailsReprots(). It then returns vm file URL
     * string "collectionReport" with details for that collection
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/payStationDetails/collectionReport.html")
    public final String payStationDetailsReportsCollectionDetails(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_COLLECTION_STATUS)) {
                    if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                        && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                        return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
                    }
                    return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
                }
            }
        }
        
        return super.payStationDetailsReportsCollectionsDetails(request, response, model, "/settings/locations/include/collectionReportDetail");
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/settings/locations/payStationList/payStationDetails/exportCollectionReport.html")
    public final String exportCollectionReport(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_COLLECTION_STATUS)) {
                    if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                        && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                        return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
                    }
                    return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
                }
            }
        }
        
        return super.exportCollectionReport(request, response);
    }
    
    /**
     * This method takes the request parameter "payStationId" and "sensorInfoTypeId'
     * returns the 90 day history for that sensor Type
     * 
     * @param request
     * @param response
     * @param model
     * @return the list of Sensor History of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/sensorHistory.html")
    @ResponseBody
    public final String payStationDetailsSensorHistory(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        super.payStationDetailsSensorHistorySuper(request, response, model);
        return WebCoreConstants.EMPTY_STRING;
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns alert information for the requested pay station
     * 
     * @param request
     * @param response
     * @param model
     * @return alert information of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList/clearAlert.html", method = RequestMethod.GET)
    @ResponseBody
    public final String clearStationDetailsAlert(final HttpServletRequest request, final HttpServletResponse response) {
        
        /* validate user account if it has proper role to manage pay stations. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_CLEAR_ACTIVE_ALERTS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.clearActiveAlert(request, response);
    }
    
}
