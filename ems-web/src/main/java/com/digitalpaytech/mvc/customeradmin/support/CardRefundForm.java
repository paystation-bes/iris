package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryLookup;
import com.digitalpaytech.domain.ProcessorTransaction;

@StoryAlias(CardRefundForm.ALIAS)
public class CardRefundForm implements Serializable {
    
    private static final String ALIAS_CARD_NUMBER = "card number";
    public static final String ALIAS = "card refund form";
    public static final String ALIAS_PROCESSOR_TRANSACTION = "processor transaction";
    public static final String ALIAS_REFUND_CARD_EXPIRY = "refund card expire";
    public static final String ALIAS_REFUND_AMOUNT = "refund amount";
    public static final String ALIAS_EMV = "emv";
    
    private static final long serialVersionUID = -775790929876469565L;
    
    private transient String randomProcessorTransactionId;
    private transient Long processorTransactionId;
    private transient String refundCardExpiry;
    private transient String cardNumber;
    private String refundAmount;
    private Boolean isCPS;
    
    public CardRefundForm() {
        
    }
    
    @StoryAlias(ALIAS_REFUND_CARD_EXPIRY)
    public String getRefundCardExpiry() {
        return this.refundCardExpiry;
    }
    
    public void setRefundCardExpiry(final String refundCardExpiry) {
        this.refundCardExpiry = refundCardExpiry;
    }
    
    @StoryAlias(ALIAS_REFUND_AMOUNT)
    public String getRefundAmount() {
        return this.refundAmount;
    }
    
    public void setRefundAmount(final String refundAmount) {
        this.refundAmount = refundAmount;
    }
    
    public String getRandomProcessorTransactionId() {
        return this.randomProcessorTransactionId;
    }
    
    public void setRandomProcessorTransactionId(final String randomProcessorTransactionId) {
        this.randomProcessorTransactionId = randomProcessorTransactionId;
    }
    
    @StoryAlias(ProcessorTransaction.ALIAS)
    @StoryLookup(type = ProcessorTransaction.ALIAS, searchAttribute = ProcessorTransaction.ALIAS_AUTHORIZATION_NUMBER, returnAttribute = ProcessorTransaction.ALIAS_ID)
    public Long getProcessorTransactionId() {
        return this.processorTransactionId;
    }
    
    public void setProcessorTransactionId(final Long processorTransactionId) {
        this.processorTransactionId = processorTransactionId;
    }
    
    @StoryAlias(ALIAS_EMV)
    public final Boolean getIsCPS() {
        return this.isCPS;
    }
    
    public final void setIsCPS(final Boolean isCPS) {
        this.isCPS = isCPS;
    }
    
    @StoryAlias(ALIAS_CARD_NUMBER)
    public String getCardNumber() {
        return this.cardNumber;
    }
    
    public void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }
}
