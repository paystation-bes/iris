package com.digitalpaytech.mvc.customeradmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.mvc.ExtendByPhoneController;
import com.digitalpaytech.mvc.customeradmin.support.EbpRatePermissionEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

/**
 * LocationsExtendByPhoneController manages displaying Extend-By-Phone Overview and show/edit/delete ExtensibleRate and ParkingPermission.
 * 
 * @author Allen Liang
 */

@Controller
@SessionAttributes({ WebSecurityConstants.WEB_SECURITY_FORM })
public class CustomerAdminExtendByPhoneController extends ExtendByPhoneController {
    
    /**
     * The list of Locations contains child locations only, not parent.
     */
    @RequestMapping(value = "/secure/settings/locations/extendByPhone.html")
    public String showExtendByPhoneLocationList(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS)
            && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_RATES_AND_POLICIES)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_RATES_AND_POLICIES)) {
                if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD) && 
                        !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) { 
                    return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
                }            
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
            }
        }
        
        return super.showExtendByPhoneLocationList(request, response, model);
    }
    
    /**
     * Retrieves current ExtensibleRate & ParkingPermission records.
     * 
     * @return String Path to overview.vm
     */
    @RequestMapping(value = "/secure/settings/locations/extendByPhone/overview.html")
    public String showExtendByPhoneLocationOverview(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_RATES_AND_POLICIES)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_RATES_AND_POLICIES)) {
                if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD) && 
                        !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) { 
                    return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
                }            
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;            }
        }
        
        return super.showExtendByPhoneLocationOverview(request, response, model);
    }
    
    /**
     * For showing 'Rate'.
     */
    @RequestMapping(value = "/secure/settings/locations/extendByPhone/rate.html")
    public String rate(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_RATES_AND_POLICIES)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_RATES_AND_POLICIES)) {
                if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD) && 
                        !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) { 
                    return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
                }            
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
            }
        }
        
        return super.rateList(request, response, model);
    }
    
    /**
     * For showing 'Policy'.
     */
    @RequestMapping(value = "/secure/settings/locations/extendByPhone/policy.html")
    public String policy(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_RATES_AND_POLICIES)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_RATES_AND_POLICIES)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.policyList(request, response, model);
    }
    
    /**
     * For showing 'Current Rates' and 'Rate Details'.
     */
    @RequestMapping(value = "/secure/settings/locations/extendByPhone/viewRate.html")
    public @ResponseBody
    String viewRate(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_RATES_AND_POLICIES)
            && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_RATES_AND_POLICIES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.viewEditRate(request, response, model);
    }
    
    /**
     * For showing 'Current Policies' and 'Policy Details'.
     */
    @RequestMapping(value = "/secure/settings/locations/extendByPhone/viewPolicy.html")
    public @ResponseBody
    String viewPolicyList(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_RATES_AND_POLICIES)
            && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_RATES_AND_POLICIES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.viewEditPolicy(request, response, model);
    }
    
    /**
     * Creates new or updates existing ExtensibleRate or ParkingPermission using input EbpRatePermissionEditForm and stores it in the database.
     * 
     * @return String 'true' or 'false' response.
     */
    @RequestMapping(value = "/secure/settings/locations/extendByPhone/saveExtendByPhone.html", method = RequestMethod.POST)
    public @ResponseBody
    String saveOrUpdate(@ModelAttribute(WebSecurityConstants.WEB_SECURITY_FORM) WebSecurityForm<EbpRatePermissionEditForm> webSecurityForm, BindingResult result,
                        HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_RATES_AND_POLICIES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.saveOrUpdate(webSecurityForm, result, request, response, model);
    }
    
    @RequestMapping(value = "/secure/settings/locations/extendByPhone/deleteRate.html")
    public @ResponseBody
    String deleteRate(HttpServletRequest request, HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_RATES_AND_POLICIES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.deleteRate(request, response);
    }
    
    @RequestMapping(value = "/secure/settings/locations/extendByPhone/deletePolicy.html")
    public @ResponseBody
    String deletePolicy(HttpServletRequest request, HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_RATES_AND_POLICIES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.deletePolicy(request, response);
    }
    
    @RequestMapping(value = "/secure/settings/locations/extendByPhoneEditWarningPeriod.html", method = RequestMethod.POST)
    public @ResponseBody
    String saveWarningPeriod(@ModelAttribute(WebSecurityConstants.WEB_SECURITY_FORM) WebSecurityForm<EbpRatePermissionEditForm> webSecurityForm, BindingResult result,
                             HttpServletRequest request, HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_RATES_AND_POLICIES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.saveWarningPeriod(webSecurityForm, result, request, response);
    }
}
