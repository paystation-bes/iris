package com.digitalpaytech.mvc.customeradmin.support.io;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.service.CardTypeService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.LinkBadCardService;
import com.digitalpaytech.service.LinkCardTypeService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.LabelConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.csv.CsvProcessingError;
import com.digitalpaytech.validation.customeradmin.CardManagementCsvValidator;

@Component("badCardManagementCsvValidator")
public class BadCardManagementCsvValidator extends CardManagementCsvValidator {
    
    private static final String CARD_EXPIRY = "cardExpiry";
    private static final String CARD_NUMBER = "cardNumber";
    
    @Autowired
    private LinkBadCardService linkBadCardService;
    
    @Autowired
    private LinkCardTypeService linkCardTypeService;
    
    @Autowired
    private CardTypeService cardTypeService;
    
    @Autowired
    private CustomerCardTypeService customerCardTypeService;
    
    public void addCsvProcessingError(final List<CsvProcessingError> errors, final int lineNumber, final String errorTypeKey, final Object... args) {
        errors.add(new CsvProcessingError(getMessage(errorTypeKey, args), lineNumber));
    }
    
    public void addDuplicateWarning(final List<CsvProcessingError> warnings, final String cardType, final String cardNumber, final int lineNumber) {
        warnings.add(new CsvProcessingError(
                getMessage(ErrorConstants.DUPLICATED_IMPORT_RECORD, getMessage(LabelConstants.CARD_MANAGEMENT_BANNED_CARD),
                           getMessage(LabelConstants.AND, getMessage(LabelConstants.CARD_SETTINGS_CARD_TYPE),
                                      getMessage(LabelConstants.CARD_SETTINGS_CARD_NUMBER)),
                           getMessage(LabelConstants.AND, cardType, createMaskForCreditCard(cardNumber))),
                lineNumber));
    }
    
    @SuppressWarnings("PMD.UseObjectForClearerAPI")
    public void validateBadCardCsvRow(final List<CsvProcessingError> fileErrors, final String inputCardType, final String cardNumber,
        final String expiry, final String comment, final int lineNumber, final Customer customer, final Map<String, CustomerCardType> cachedCustomerCardTypes) {
        final List<CsvProcessingError> rowErrors = new ArrayList<>();
        
        final boolean validCardNumber = validateRequired(rowErrors, CARD_NUMBER, LabelConstants.CARD_SETTINGS_CARD_NUMBER, cardNumber) != null;
        
        if (validCardNumber) {
            final CustomerCardType customerCardType = determineCustomerCardType(inputCardType, customer.getId(), cachedCustomerCardTypes);
            final boolean validCardType = customerCardType != null;
            
            if (validCardType) {
                verifyCardDetails(rowErrors, cardNumber, customerCardType, expiry);
            } else {
                rowErrors.add(new CsvProcessingError(this.getMessage(ErrorConstants.INVALID, this.getMessage(LabelConstants.CARD_SETTINGS_CARD_TYPE)),
                        lineNumber));
            }
        }
        
        validateExternalDescription(rowErrors, LabelConstants.CARD_MANAGEMENT_COMMENT, LabelConstants.CARD_MANAGEMENT_COMMENT, comment,
                                    WebCoreConstants.VALIDATION_MIN_LENGTH_1);
        
        rowErrors.forEach(error -> {
            error.setLineNumber(lineNumber);
            fileErrors.add(error);
        });
    }
    
    private String createMaskForCreditCard(final String cardNumber) {
        return StringUtils.repeat(CardProcessingConstants.MASK, cardNumber.length() - CardProcessingConstants.LAST_4_DIGITS_LENGTH)
               + cardNumber.substring(cardNumber.length() - CardProcessingConstants.LAST_4_DIGITS_LENGTH, cardNumber.length());
    }
    
    private void verifyCardDetails(final List<CsvProcessingError> rowErrors, final String cardNumber, final CustomerCardType customerCardType,
        final String expiry) {
        
        final int cardMinLen;
        final int cardMaxLen;
        
        if (customerCardType.getCardType() == null || customerCardType.getCardType().getId() == WebCoreConstants.CARD_TYPE_VALUE_CARD) {
            cardMinLen = CardProcessingConstants.VALUE_CARD_MIN_LENGTH;
            cardMaxLen = CardProcessingConstants.BAD_VALUE_CARD_MAX_LENGTH;
            
            validateNumberStringLength(rowErrors, CARD_NUMBER, LabelConstants.CARD_SETTINGS_CARD_NUMBER, cardNumber, cardMinLen, cardMaxLen);
            validateNumber(rowErrors, CARD_NUMBER, LabelConstants.CARD_SETTINGS_CARD_NUMBER, cardNumber);
            
        } else if (customerCardType.getCardType().getId() == WebCoreConstants.CARD_TYPE_SMART_CARD) {
            cardMinLen = CardProcessingConstants.SMART_CARD_MIN_LENGTH;
            cardMaxLen = CardProcessingConstants.BAD_SMART_CARD_MAX_LENGTH;
            
            validateNumberStringLength(rowErrors, CARD_NUMBER, LabelConstants.CARD_SETTINGS_CARD_NUMBER, cardNumber, cardMinLen, cardMaxLen);
            validateNumber(rowErrors, CARD_NUMBER, LabelConstants.CARD_SETTINGS_CARD_NUMBER, cardNumber);
            
        } else {
            cardMinLen = CardProcessingConstants.CREDIT_CARD_MIN_LENGTH;
            cardMaxLen = CardProcessingConstants.CREDIT_CARD_MAX_LENGTH;
            
            /* Credit Card number is been validated previously by validateBadCardCsvRow */
            validateExpiryDate(rowErrors, CARD_EXPIRY, expiry);
            validateCreditCardNumber(rowErrors, CARD_NUMBER, cardNumber, cardMinLen, cardMaxLen);
        }
    }
    
    private void validateExpiryDate(final List<CsvProcessingError> rowErrors, final String fieldName, final String expiry) {
        validateRequired(rowErrors, fieldName, LabelConstants.CARD_SETTINGS_EXPIRY, expiry);
        validateNumber(rowErrors, fieldName, LabelConstants.CARD_SETTINGS_EXPIRY, expiry);
        validateStringLength(rowErrors, CARD_EXPIRY, LabelConstants.CARD_SETTINGS_EXPIRY, expiry,
                             CardProcessingConstants.LAST_4_DIGITS_LENGTH);
    }
    
    private void validateCreditCardNumber(final List<CsvProcessingError> rowErrors, final String fieldName, final String cardNumber,
        final int cardMinLen, final int cardMaxLen) {
        
        final boolean isValidRequired = validateRequired(rowErrors, fieldName, LabelConstants.CARD_SETTINGS_CARD_NUMBER, cardNumber) != null;
        final boolean isValidNumber = validateNumber(rowErrors, fieldName, LabelConstants.CARD_SETTINGS_CARD_NUMBER, cardNumber) != null;
        final boolean isValidNumberStringLength = validateNumberStringLength(rowErrors, fieldName, LabelConstants.CARD_SETTINGS_CARD_NUMBER,
                                                                             cardNumber,
                                                                       cardMinLen, cardMaxLen) != null;
        if (isValidRequired && isValidNumber && isValidNumberStringLength) {
            final String creditCardTypeName = this.linkCardTypeService.getCreditCardTypeName(cardNumber);
            
            if (StringUtils.isBlank(creditCardTypeName)) {
                addError(rowErrors, fieldName, this.getMessage(ErrorConstants.INVALID, this.getMessage(LabelConstants.CARD_SETTINGS_CARD_NUMBER)));
            }
        }
    }
    
    public CustomerCardType determineCustomerCardType(final String cardTypeName, final int customerId, final Map<String, CustomerCardType> cachedCustomerCardTypes) {
        if (WebCoreConstants.BADCARD_CSV_PROCESSOR_TYPE_CREDIT_CARD.equals(cardTypeName)) {
            final CardType cardType = this.cardTypeService.getCardTypesMap().get(WebCoreConstants.CARD_TYPE_CREDIT_CARD);
            if (cardType != null) {
                final CustomerCardType customerCardType = new CustomerCardType();
                customerCardType.setCardType(cardType);
                return customerCardType;
            }
        } else {
            final CustomerCardType cachedCustomerCardType = cachedCustomerCardTypes.get(cardTypeName.toLowerCase(Locale.getDefault()));
            
            if (cachedCustomerCardType != null) {
                return cachedCustomerCardType;
            }
            
            final List<CustomerCardType> cardTypes = this.customerCardTypeService
                    .getDetachedBannableCardTypes(customerId, Arrays.asList(cardTypeName.toLowerCase(Locale.getDefault())));
            
            if (cardTypes != null && !cardTypes.isEmpty()) {
                final CustomerCardType customerCardType = cardTypes.get(0);
                cachedCustomerCardTypes.put(cardTypeName.toLowerCase(Locale.getDefault()), customerCardType);
                return customerCardType;
            }
        }
        return null;
    }
}
