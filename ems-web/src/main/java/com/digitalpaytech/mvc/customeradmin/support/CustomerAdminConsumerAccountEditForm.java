package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Transient;

import com.digitalpaytech.constants.SortOrder;
import com.digitalpaytech.domain.EmailAddress;
import com.digitalpaytech.dto.customeradmin.CouponDTO;
import com.digitalpaytech.dto.customeradmin.CustomerCardDTO;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class CustomerAdminConsumerAccountEditForm implements Serializable {       
    
    /**
     * 
     */
    private static final long serialVersionUID = -687996622324045233L;
    
    public enum SortColumn {
    	firstName("firstName", "lastName"),
    	lastName("lastName", "firstName"),
    	emailAddress("emailAddr.email", "lastName", "firstName")
    	;
    	
    	private String[] actualAttribute;
		
		SortColumn(String... actualAttribute) {
			this.actualAttribute = actualAttribute;
		}
		
		public String[] getActualAttribute() {
			return actualAttribute;
		}
    }
    
    private String randomId;
    private String firstName;
    private String lastName;
    private String description;  
    private String emailAddressRandomId;    
    private String emailAddress;
    private Date lastModifedByGMT;
    private Integer lastModifiedByUserId;
    
    @XStreamImplicit(itemFieldName = "couponRandomIds")
    private List<String> couponRandomIds;
    @XStreamImplicit(itemFieldName = "customCardRandomIds")
    private List<String> customCardRandomIds;
    
    private String filterValue; 
    private SortOrder order;
    private SortColumn column;    
    private Integer page;
    private String numOfRecord;
    private Integer itemsPerPage;
    private Long dataKey;
    
    private String targetRandomId;
    
    @Transient
    private transient int consumerId;
    @Transient
    private transient int emailAddressId;
    @Transient
    private transient EmailAddress emailAddressObj;    
    @Transient
    private transient List<Integer> couponIds;
    @Transient
    private transient List<Integer> customCardIds;
    
    @XStreamImplicit(itemFieldName = "customCards")
    private List<CustomerCardDTO> customCards;
    @XStreamImplicit(itemFieldName = "coupons")
    private List<CouponDTO> coupons;
    
    public String getRandomId() {
        return randomId;
    }
    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getEmailAddressRandomId() {
        return emailAddressRandomId;
    }
    public void setEmailAddressRandomId(String emailAddressRandomId) {
        this.emailAddressRandomId = emailAddressRandomId;
    }
    public Date getLastModifedByGMT() {
        return lastModifedByGMT;
    }
    public void setLastModifedByGMT(Date lastModifedByGMT) {
        this.lastModifedByGMT = lastModifedByGMT;
    }
    public Integer getLastModifiedByUserId() {
        return lastModifiedByUserId;
    }
    public void setLastModifiedByUserId(Integer lastModifiedByUserId) {
        this.lastModifiedByUserId = lastModifiedByUserId;
    }
    public int getConsumerId() {
        return consumerId;
    }
    public void setConsumerId(int consumerId) {
        this.consumerId = consumerId;
    }
    public EmailAddress getEmailAddressObj() {
        return emailAddressObj;
    }    
    public int getEmailAddressId() {
        return emailAddressId;
    }
    public void setEmailAddressId(int emailAddressId) {
        this.emailAddressId = emailAddressId;
    }
    public String getEmailAddress() {
        return emailAddress;
    }
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    public List<String> getCouponRandomIds() {
        return couponRandomIds;
    }
    public void setCouponRandomIds(List<String> couponRandomIds) {
        this.couponRandomIds = couponRandomIds;
    }
    public List<String> getCustomCardRandomIds() {
        return customCardRandomIds;
    }
    public void setCustomCardRandomIds(List<String> customCardRandomIds) {
        this.customCardRandomIds = customCardRandomIds;
    }
    public List<Integer> getCouponIds() {
        return couponIds;
    }
    public void setCouponIds(List<Integer> couponIds) {
        this.couponIds = couponIds;
    }
    public List<Integer> getCustomCardIds() {
        return customCardIds;
    }
    public void setCustomCardIds(List<Integer> customCardIds) {
        this.customCardIds = customCardIds;
    }
    public SortOrder getOrder() {
        return order;
    }
    public void setOrder(SortOrder order) {
        this.order = order;
    }
    public SortColumn getColumn() {
        return column;
    }
    public void setColumn(SortColumn column) {
        this.column = column;
    }
    public String getNumOfRecord() {
        return numOfRecord;
    }
    public void setNumOfRecord(String numOfRecord) {
        this.numOfRecord = numOfRecord;
    }
    public String getFilterValue() {
        return filterValue;
    }
    public void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }
    public Integer getPage() {
        return page;
    }
    public void setPage(Integer page) {
        this.page = page;
    }
    public Integer getItemsPerPage() {
        return itemsPerPage;
    }
    public void setItemsPerPage(Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }
    public Long getDataKey() {
        return dataKey;
    }
    public void setDataKey(Long dataKey) {
        this.dataKey = dataKey;
    }
    public void setEmailAddressObj(EmailAddress emailAddressObj) {
        this.emailAddressObj = emailAddressObj;
    }
	public String getTargetRandomId() {
		return targetRandomId;
	}
	public void setTargetRandomId(String targetRandomId) {
		this.targetRandomId = targetRandomId;
	}
	public List<CustomerCardDTO> getCustomCards() {
		return customCards;
	}
	public void setCustomCards(List<CustomerCardDTO> customCards) {
		this.customCards = customCards;
	}
	public List<CouponDTO> getCoupons() {
		return coupons;
	}
	public void setCoupons(List<CouponDTO> coupons) {
		this.coupons = coupons;
	}
    
}
