package com.digitalpaytech.mvc.customeradmin;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.client.dto.llps.PreferredParkerFile;
import com.digitalpaytech.domain.CaseCustomerAccount;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.domain.CustomerMigrationValidationStatus;
import com.digitalpaytech.domain.CustomerMigrationValidationType;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.FlexWSUserAccount;
import com.digitalpaytech.domain.LicensePlateFileUploadStatus;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.Notification;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.BatchProcessStatus;
import com.digitalpaytech.dto.NotificationDetails;
import com.digitalpaytech.dto.NotificationListInfo;
import com.digitalpaytech.dto.SearchCustomerResultSetting;
import com.digitalpaytech.dto.customeradmin.GlobalConfiguration;
import com.digitalpaytech.dto.customermigration.MigrationValidationInfo;
import com.digitalpaytech.mvc.customeradmin.support.GlobalPreferencesEditForm;
import com.digitalpaytech.mvc.customeradmin.support.JurisdictionTypeEditForm;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.FlexHelper;
import com.digitalpaytech.mvc.support.FlexWSUserAccountForm;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.WebObjectCacheManager;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.CaseService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerMigrationService;
import com.digitalpaytech.service.CustomerMigrationValidationStatusService;
import com.digitalpaytech.service.FlexWSService;
import com.digitalpaytech.service.LicensePlateFileUploadStatusService;
import com.digitalpaytech.service.LicensePlateService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.NotificationService;
import com.digitalpaytech.util.CustomerAdminUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.LabelConstants;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.PooledResource;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.AlertSettingValidator;
import com.digitalpaytech.validation.customeradmin.GlobalPreferencesValidator;
import com.digitalpaytech.validation.customeradmin.JurisdictionTypeValidator;
import com.digitalpaytech.validation.customeradmin.QuerySpaceByValidator;

@Controller
@SuppressWarnings({ "PMD.ExcessiveImports", "PMD.GodClass", "PMD.CouplingBetweenObjects", "PMD.TooManyFields" })
public class GlobalController {
    
    public static final String FORM_FLEX_CREDENTIALS = "flexCredentialsForm";
    
    private static final String MESSAGES = "messages";
    private static final String FORM_EDIT = "globalPreferencesEditForm";
    private static final String JURISDICTION_TYPE_EDIT_FORM = "jurisdictionTypeEditForm";
    private static final String CURRENT_NOTIFICATION_INFO = "currentNotificationInfo";
    private static final String PAST_NOTIFICATION_INFO = "pastNotificationInfo";
    private static final String FILE_PATH_NOTIFICATIONLIST = "/include/sortNotificationList";
    private static final String PARAMETER_NAME_ISSYSTEMADMIN = "isSysAdmin";
    
    private static final Logger LOG = Logger.getLogger(GlobalController.class);
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    @Autowired
    private NotificationService notificationService;
    
    @Autowired
    private GlobalPreferencesValidator globalPreferencesValidator;
    
    @Autowired
    private QuerySpaceByValidator querySpacesByValidator;
    
    @Autowired
    private JurisdictionTypeValidator jurisdictionTypeValidator;
    
    @Autowired
    private CustomerMigrationService customerMigrationService;
    
    @Autowired
    private CustomerMigrationValidationStatusService customerMigrationValidationStatusService;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    @Autowired
    private CaseService caseService;
    
    @Autowired
    private FlexWSService flexWSService;
    
    @Autowired
    private FlexHelper flexHelper;
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    @Autowired
    private WebObjectCacheManager webObjectCacheManager;
    
    @Autowired
    private LicensePlateService licensePlateService;
    
    @Autowired
    private LicensePlateFileUploadStatusService licensePlateFileUploadStatusService;
    
    public final CustomerAdminService getCustomerAdminService() {
        return this.customerAdminService;
    }
    
    public final NotificationService getNotificationService() {
        return this.notificationService;
    }
    
    public final GlobalPreferencesValidator getGlobalPreferencesValidator() {
        return this.globalPreferencesValidator;
    }
    
    public final QuerySpaceByValidator getQuerySpacesByValidator() {
        return this.querySpacesByValidator;
    }
    
    public final CustomerMigrationService getCustomerMigrationService() {
        return this.customerMigrationService;
    }
    
    public final CustomerMigrationValidationStatusService getCustomerMigrationValidationStatusService() {
        return this.customerMigrationValidationStatusService;
    }
    
    public final MerchantAccountService getMerchantAccountService() {
        return this.merchantAccountService;
    }
    
    public final CaseService getCaseService() {
        return this.caseService;
    }
    
    public final FlexWSService getFlexWSService() {
        return this.flexWSService;
    }
    
    public final FlexHelper getFlexHelper() {
        return this.flexHelper;
    }
    
    public final CommonControllerHelper getCommonControllerHelper() {
        return this.commonControllerHelper;
    }
    
    public final WebObjectCacheManager getWebObjectCacheManager() {
        return this.webObjectCacheManager;
    }
    
    public final void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    public final void setNotificationService(final NotificationService notificationService) {
        this.notificationService = notificationService;
    }
    
    public final void setGlobalPreferencesValidator(final GlobalPreferencesValidator globalPreferencesValidator) {
        this.globalPreferencesValidator = globalPreferencesValidator;
    }
    
    public final void setQuerySpacesByValidator(final QuerySpaceByValidator querySpacesByValidator) {
        this.querySpacesByValidator = querySpacesByValidator;
    }
    
    public final void setCustomerMigrationService(final CustomerMigrationService customerMigrationService) {
        this.customerMigrationService = customerMigrationService;
    }
    
    public final void setCustomerMigrationValidationStatusService(
        final CustomerMigrationValidationStatusService customerMigrationValidationStatusService) {
        this.customerMigrationValidationStatusService = customerMigrationValidationStatusService;
    }
    
    public final void setMerchantAccountService(final MerchantAccountService merchantAccountService) {
        this.merchantAccountService = merchantAccountService;
    }
    
    public final void setCaseService(final CaseService caseService) {
        this.caseService = caseService;
    }
    
    public final void setFlexWSService(final FlexWSService flexWSService) {
        this.flexWSService = flexWSService;
    }
    
    public final void setFlexHelper(final FlexHelper flexHelper) {
        this.flexHelper = flexHelper;
    }
    
    public final void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }
    
    public final JurisdictionTypeValidator getJurisdictionTypeValidator() {
        return this.jurisdictionTypeValidator;
    }
    
    public final void setJurisdictionTypeValidator(final JurisdictionTypeValidator jurisdictionTypeValidator) {
        this.jurisdictionTypeValidator = jurisdictionTypeValidator;
    }
    
    @ModelAttribute(FORM_EDIT)
    public final WebSecurityForm<GlobalPreferencesEditForm> initEditForm(final HttpServletRequest request) {
        return new WebSecurityForm<GlobalPreferencesEditForm>((Object) null, new GlobalPreferencesEditForm(),
                SessionTool.getInstance(request).locatePostToken(FORM_EDIT));
    }
    
    @ModelAttribute(JURISDICTION_TYPE_EDIT_FORM)
    public final WebSecurityForm<JurisdictionTypeEditForm> initJurisdictionTypeEditForm(final HttpServletRequest request) {
        return new WebSecurityForm<JurisdictionTypeEditForm>((Object) null, new JurisdictionTypeEditForm(),
                SessionTool.getInstance(request).locatePostToken(JURISDICTION_TYPE_EDIT_FORM));
    }
    
    @ModelAttribute(FORM_FLEX_CREDENTIALS)
    public final WebSecurityForm<FlexWSUserAccountForm> initFlexCredentialsForm(final HttpServletRequest request) {
        return new WebSecurityForm<FlexWSUserAccountForm>((Object) null, new FlexWSUserAccountForm(),
                SessionTool.getInstance(request).locatePostToken(FORM_FLEX_CREDENTIALS));
    }
    
    /**
     * This method will look at the permissions in the WebUser and pass control
     * to the first tab that the user has permissions for.
     * 
     * @return Will return the method for Settings or SystemNotifications
     *         depending on permissions, or error if no permissions exist.
     */
    
    //This is the initial tab loaded for customer settings - "/settings/index.html"
    @RequestMapping(value = "/secure/settings/global/index.html", method = RequestMethod.GET)
    public final String getFirstPermittedTab(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_EDIT) final WebSecurityForm<GlobalPreferencesEditForm> webSecurityForm,
        @ModelAttribute(FlexHelper.FORM_CREDENTIALS) final WebSecurityForm<FlexWSUserAccountForm> flexCredentialsForm,
        @ModelAttribute(JURISDICTION_TYPE_EDIT_FORM) final WebSecurityForm<JurisdictionTypeEditForm> jurisdictionTypeEditForm) {
        if (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_SYSTEM_SETTINGS)) {
            return getGlobalPreferences(request, response, model, webSecurityForm, flexCredentialsForm, jurisdictionTypeEditForm);
        } else {
            return getSystemNotification(request, response, model);
        }
    }
    
    /*
     * ========================================================================================================================
     * "Settings" tab
     * ========================================================================================================================
     */
    
    //This is the initial tab loaded for customer settings - "/settings/index.html"
    @RequestMapping(value = "/secure/settings/global/settings.html", method = RequestMethod.GET)
    public final String getGlobalPreferences(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_EDIT) final WebSecurityForm<GlobalPreferencesEditForm> webSecurityForm,
        @ModelAttribute(FlexHelper.FORM_CREDENTIALS) final WebSecurityForm<FlexWSUserAccountForm> flexCredentialsForm,
        @ModelAttribute(JURISDICTION_TYPE_EDIT_FORM) final WebSecurityForm<JurisdictionTypeEditForm> jurisdictionTypeEditForm) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_SYSTEM_SETTINGS)
            && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_ACCESS_DIGITAL_API)) {
            return getSystemNotification(request, response, model);
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        GlobalConfiguration globalCfg = getGlobalConfiguration();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        globalCfg = this.customerAdminService.findGlobalPreferences(userAccount.getCustomer(), globalCfg);
        
        if (WebSecurityUtil.isCustomerAdmin()) {
            final CustomerMigration customerMigration =
                    this.customerMigrationService.findCustomerMigrationByCustomerId(userAccount.getCustomer().getId());
            if (customerMigration != null) {
                this.commonControllerHelper.setModelObjectsForMigration(customerMigration, model,
                                                                        TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone()));
            }
        }
        
        model.put(WebCoreConstants.GLOBAL_CONFIGURATION, globalCfg);
        model.put(FORM_EDIT, webSecurityForm);
        
        final FlexWSUserAccount flexAccount = this.flexWSService.findFlexWSUserAccountByCustomerId(userAccount.getCustomer().getId());
        this.flexHelper.transform(flexAccount, flexCredentialsForm.getWrappedObject(), keyMapping);
        model.put(FlexHelper.FORM_CREDENTIALS, flexCredentialsForm);
        
        if (WebSecurityUtil.hasValidCustomerSubscription(WebCoreConstants.SUBSCRIPTION_TYPE_CASE_INTEGRATION)) {
            addCaseAccount(userAccount.getCustomer().getId(), keyMapping, model);
        }
        
        model.put(JURISDICTION_TYPE_EDIT_FORM, jurisdictionTypeEditForm);
        
        return "/settings/global/settings";
    }
    
    @RequestMapping(value = "/secure/settings/global/migrationTaskStatus.html", method = RequestMethod.GET)
    public final String getMigrationTaskStatus(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_SYSTEM_SETTINGS)
            && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_ACCESS_DIGITAL_API)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        if (!WebSecurityUtil.isCustomerAdmin()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final CustomerMigration customerMigration =
                this.customerMigrationService.findCustomerMigrationByCustomerId(userAccount.getCustomer().getId());
        
        final List<CustomerMigrationValidationStatus> validationStatusList =
                this.customerMigrationValidationStatusService.findUserFacingValidationsByCustomerId(userAccount.getCustomer().getId());
        
        model.put("validationList", getMigrationValidationInfoList(customerMigration, validationStatusList));
        
        return "/settings/include/migrationTaskList";
    }
    
    @RequestMapping(value = "/secure/settings/global/saveTimeZone.html", method = RequestMethod.POST)
    @ResponseBody
    public final String saveTimeZone(@ModelAttribute(FORM_EDIT) final WebSecurityForm<GlobalPreferencesEditForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_SYSTEM_SETTINGS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        if (!this.globalPreferencesValidator.validateMigrationStatus(webSecurityForm)) {
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        this.globalPreferencesValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        final GlobalPreferencesEditForm globalPreferencesEditForm = webSecurityForm.getWrappedObject();
        final String timezone = globalPreferencesEditForm.getCurrentTimeZone();
        
        CustomerAdminUtil.setCustomerPropertyTypes(this.customerAdminService.getCustomerPropertyTypes());
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        // if the customer has a trial subscription, update the end date to the UTC equivalent of 23:59:59 their local time
        final Customer customer = userAccount.getCustomer();
        
        String previousTimeZone = WebCoreConstants.SERVER_TIMEZONE_GMT;
        final CustomerProperty timezoneProperty = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customer.getId(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        if (timezoneProperty != null) {
            previousTimeZone = timezoneProperty.getPropertyValue();
        }
        if (customer.getCustomerStatusType().getId() == WebCoreConstants.CUSTOMER_STATUS_TYPE_TRIAL) {
            Date newTrialExpiryGmt = null;
            newTrialExpiryGmt = DateUtil.convertTimeZone(previousTimeZone, timezone, customer.getTrialExpiryGmt());
            customer.setTrialExpiryGmt(newTrialExpiryGmt);
        }
        
        final CustomerProperty updatedProperty = updateProperty(userAccount, timezone, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        this.customerAdminService.saveOrUpdateCustomerProperty(updatedProperty);
        
        final WebUser webUser = (WebUser) WebSecurityUtil.getAuthentication().getPrincipal();
        webUser.setIsMetric(!(timezone != null && timezone.startsWith("US/")));
        webUser.setCustomerTimeZone(timezone);
        
        final List<MerchantAccount> merchantAccountList = this.merchantAccountService.findAllMerchantAccountsByCustomerId(customer.getId());
        if (merchantAccountList != null) {
            for (MerchantAccount merchantAccount : merchantAccountList) {
                merchantAccount.setCloseQuarterOfDay(DateUtil.convertCloseTime(merchantAccount.getCloseQuarterOfDay(), previousTimeZone, timezone));
                this.merchantAccountService.updateMerchantAccount(merchantAccount);
            }
        }
        
        return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(StandardConstants.STRING_COLON).append(webSecurityForm.getInitToken())
                .toString();
    }
    
    @RequestMapping(value = "/secure/settings/global/saveQuerySpacesBy.html", method = RequestMethod.POST)
    @ResponseBody
    public final String saveQuerySpacesBy(@ModelAttribute(FORM_EDIT) final WebSecurityForm<GlobalPreferencesEditForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_SYSTEM_SETTINGS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        if (!this.querySpacesByValidator.validateMigrationStatus(webSecurityForm)) {
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        this.querySpacesByValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        final GlobalPreferencesEditForm globalPreferencesEditForm = webSecurityForm.getWrappedObject();
        CustomerAdminUtil.setCustomerPropertyTypes(this.customerAdminService.getCustomerPropertyTypes());
        
        final CustomerProperty updatedProperty =
                updateProperty(WebSecurityUtil.getUserAccount(), String.valueOf(globalPreferencesEditForm.getQuerySpacesBy()),
                               WebCoreConstants.CUSTOMER_PROPERTY_TYPE_QUERY_SPACES_BY);
        this.customerAdminService.saveOrUpdateCustomerProperty(updatedProperty);
        
        return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(StandardConstants.STRING_COLON).append(webSecurityForm.getInitToken())
                .toString();
    }
    
    @RequestMapping(value = "/secure/settings/global/saveJurisdictionType.html", method = RequestMethod.POST)
    @ResponseBody
    public String saveJurisdictionType(@ModelAttribute(JURISDICTION_TYPE_EDIT_FORM) final WebSecurityForm<JurisdictionTypeEditForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_SYSTEM_SETTINGS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final UserAccount ua = WebSecurityUtil.getUserAccount();
        final String origPreferredValue = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(ua.getCustomer().getId().intValue(),
                                                                        WebCoreConstants.CUSTOMER_PROPERTY_TYPE_JURISDICTION_TYPE_PREFERRED)
                .getPropertyValue();
        
        if (!this.jurisdictionTypeValidator.handleMultipartExceptions(webSecurityForm, request)) {
            this.jurisdictionTypeValidator.validate(webSecurityForm, result);
            this.jurisdictionTypeValidator.validatePreferredParkerImportForm(webSecurityForm, Integer.parseInt(origPreferredValue), result, true);
        }
        
        if (!webSecurityForm.getValidationErrorInfo().getErrorStatus().isEmpty()) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WebCoreUtil.cleanupIframeJSON(WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(),
                                                                                   WebCoreConstants.JSON_ERROR_STATUS_LABEL, true));
        }
        
        final JurisdictionTypeEditForm jurisdictionTypeEditForm = webSecurityForm.getWrappedObject();
        CustomerAdminUtil.setCustomerPropertyTypes(this.customerAdminService.getCustomerPropertyTypes());
        
        final CustomerProperty updatedPreferred = updateProperty(ua, String.valueOf(jurisdictionTypeEditForm.getJurisdictionTypePreferred()),
                                                                 WebCoreConstants.CUSTOMER_PROPERTY_TYPE_JURISDICTION_TYPE_PREFERRED);
        
        final CustomerProperty updatedLimited = updateProperty(ua, String.valueOf(jurisdictionTypeEditForm.getJurisdictionTypeLimited()),
                                                               WebCoreConstants.CUSTOMER_PROPERTY_TYPE_JURISDICTION_TYPE_LIMITED);
        
        final boolean isFileEmpty = this.jurisdictionTypeValidator.isJurisdictionTypeFileEmpty(webSecurityForm);
        
        final boolean isPreferredDisabled =
                this.licensePlateService.compareJurisdictionType(PaystationConstants.PAYSTATION_JURISDICTION_PREFERRED_DISABLED, updatedPreferred);
        
        final boolean isPreferredSame = this.licensePlateService.compareJurisdictionType(Integer.parseInt(origPreferredValue), updatedPreferred);
        
        if (isFileEmpty && (isPreferredSame || isPreferredDisabled)) {
            saveOrUpdateCustomerProperties(updatedPreferred, updatedLimited);
            return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(StandardConstants.STRING_COLON).append(webSecurityForm.getInitToken())
                    .toString();
        } else {
            final String uploadResult = uploadLicensePlates(webSecurityForm);
            if (uploadResult.equals(WebCoreConstants.RESPONSE_TRUE)) {
                saveOrUpdateCustomerProperties(updatedPreferred, updatedLimited);
                return new StringBuilder(uploadResult).append(StandardConstants.STRING_COLON).append(webSecurityForm.getInitToken()).toString();
            }
            return uploadResult;
        }
    }
    
    private void saveOrUpdateCustomerProperties(final CustomerProperty updatedPreferred, final CustomerProperty updatedLimited) {
        this.customerAdminService.saveOrUpdateCustomerProperty(updatedPreferred);
        this.customerAdminService.saveOrUpdateCustomerProperty(updatedLimited);
    }
    
    private String uploadLicensePlates(@ModelAttribute(JURISDICTION_TYPE_EDIT_FORM) final WebSecurityForm<JurisdictionTypeEditForm> webSecurityForm) {
        
        final Customer customer = WebSecurityUtil.getUserAccount().getCustomer();
        
        final int customerId = customer.getId().intValue();
        
        final CustomerProperty prefByLocationProp = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId,
                                                                        WebCoreConstants.CUSTOMER_PROPERTY_TYPE_JURISDICTION_TYPE_PREFERRED);
        
        if (this.licensePlateService.compareJurisdictionType(PaystationConstants.PAYSTATION_JURISDICTION_PREFERRED_DISABLED, prefByLocationProp)) {
            return buildErrorMessageInfo(webSecurityForm, "error.llps.cannot.upload.when.preferred.none");
        }
        
        final LicensePlateFileUploadStatus existingStatus = this.licensePlateFileUploadStatusService
                .findByCustomerIdAndStatus(customer.getId(), LicensePlateFileUploadStatus.ProcessStatus.IN_PROCESS.toString());
        
        if (existingStatus != null) {
            return buildErrorMessageInfo(webSecurityForm, "error.llps.other.file.in.progress");
        }
        
        final JurisdictionTypeEditForm jurisdictionTypeEditForm = webSecurityForm.getWrappedObject();
        final MultipartFile multipartFile = jurisdictionTypeEditForm.getFile();
        
        if (invalidCsvFileParams(multipartFile)) {
            return buildErrorMessageInfo(webSecurityForm, "error.llps.invalid.csv.file");
        }
        
        CustomerAdminUtil.setCustomerPropertyTypes(this.customerAdminService.getCustomerPropertyTypes());
        
        final boolean isByLocation = this.licensePlateService
                .compareJurisdictionType(PaystationConstants.PAYSTATION_JURISDICTION_PREFERRED_BY_LOCATION, prefByLocationProp);
        
        final LicensePlateFileUploadStatus licensePlateFileUploadStatus =
                new LicensePlateFileUploadStatus(null, customer, multipartFile.getOriginalFilename(),
                        LicensePlateFileUploadStatus.ProcessStatus.IN_PROCESS.toString(), null, WebSecurityUtil.getUserAccount().getRealUserId());
        
        this.licensePlateFileUploadStatusService.save(licensePlateFileUploadStatus);
        
        try {
            @SuppressWarnings({"PMD.AvoidCatchingGenericException"}) 
            final PreferredParkerFile preferredParkerFile =
                    this.licensePlateService.uploadLicensePlates(customer.getUnifiId(), isByLocation, multipartFile);
            
            final List<String> fileErrors = preferredParkerFile.getErrorRows();
            final String concatErrors = WebCoreUtil.concatErrors(fileErrors, LicensePlateFileUploadStatus.ERROR_STRING_MAX_LEN);
            
            licensePlateFileUploadStatus.setFileId(preferredParkerFile.getId());
            licensePlateFileUploadStatus.setStatus(preferredParkerFile.getProcessStatus().toString());
            licensePlateFileUploadStatus.setError(concatErrors);
            
            this.licensePlateFileUploadStatusService.update(licensePlateFileUploadStatus);
            
            return WebCoreConstants.RESPONSE_TRUE;
            
            // catch all exception to prevent HystrixBadRequestException if Discovery Cilent fails to convert exception
        } catch (Exception ex) {
            final String errMsg = "Unable to send license plate file to llps";
            LOG.error(errMsg, ex);
            licensePlateFileUploadStatus.setStatus(LicensePlateFileUploadStatus.ProcessStatus.ERROR_OUT.toString());
            licensePlateFileUploadStatus.setError(errMsg);
            
            this.licensePlateFileUploadStatusService.update(licensePlateFileUploadStatus);
            return buildErrorMessageInfo(webSecurityForm, "error.llps.failed.to.contact.llps");
        }
        
    }
    
    /*
     * ========================================================================================================================
     * "System Notification" tab
     * ========================================================================================================================
     */
    @RequestMapping(value = "/secure/settings/global/sysNotification.html", method = RequestMethod.GET)
    public final String getSystemNotification(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* get the NotificationListInfo DTO for current and past notifications. */
        final Collection<NotificationListInfo> currentNotificationInfo = this.prepareCurrentNotificationInfo(request);
        final Collection<NotificationListInfo> pastNotificationInfo = this.preparePastNotificationInfo(request);
        
        /* put DTOs in model. */
        model.put(CURRENT_NOTIFICATION_INFO, currentNotificationInfo);
        model.put(PAST_NOTIFICATION_INFO, pastNotificationInfo);
        
        return "/settings/global/sysNotification";
    }
    
    @RequestMapping(value = "/secure/settings/global/sortCurrentNotificationList.html", method = RequestMethod.GET)
    public final String sortCurrentNotificationList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* get the NotificationListInfo DTO for current and past notifications. */
        final Collection<NotificationListInfo> currentNotificationInfo = this.prepareCurrentNotificationInfo(request);
        
        /* put DTOs in model. */
        model.put(CURRENT_NOTIFICATION_INFO, currentNotificationInfo);
        model.put(PARAMETER_NAME_ISSYSTEMADMIN, false);
        
        return FILE_PATH_NOTIFICATIONLIST;
    }
    
    @RequestMapping(value = "/secure/settings/global/sortPastNotificationList.html", method = RequestMethod.GET)
    public final String sortPastNotificationList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        final Collection<NotificationListInfo> pastNotificationInfo = this.preparePastNotificationInfo(request);
        
        /* put DTOs in model. */
        model.put(PAST_NOTIFICATION_INFO, pastNotificationInfo);
        model.put(PARAMETER_NAME_ISSYSTEMADMIN, false);
        
        return FILE_PATH_NOTIFICATIONLIST;
    }
    
    /**
     * This method retrieves notification detail information and return back to UI.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return JSON string having user detail information. "false" in case of any exception or validation failure.
     */
    @RequestMapping(value = "/secure/settings/global/sysNotificationDetails.html", method = RequestMethod.GET)
    @ResponseBody
    public final String viewSystemNotificationDetails(final HttpServletRequest request, final HttpServletResponse response) {
        
        /* validate the randomized notificationID from request parameter. */
        final Object actualNotificationId = AlertSettingValidator.validateRandomIdNoForm(request, "notificationID");
        if (actualNotificationId.equals(WebCoreConstants.RECORD_NOT_FOUND)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Integer notificationId = (Integer) actualNotificationId;
        final NotificationDetails notificationDetails = this.prepareNotificationDetailsForJSON(notificationId);
        
        /* convert NotificationDetails to JSON string. */
        final String json = WidgetMetricsHelper.convertToJson(notificationDetails, "notificationDetails", true);
        if (StringUtils.isBlank(json)) {
            LOG.warn("### Blank JSON response for Notification Details. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return json;
    }
    
    @RequestMapping(value = WebCoreConstants.URI_BATCH_PROCESS_STATUS)
    @ResponseBody
    public final String batchProcessStatus(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        final MessageInfo messages = new MessageInfo();
        
        final String statusKey = request.getParameter("statusKey");
        if (statusKey == null || statusKey.length() <= 0) {
            messages.setError(true);
        } else {
            final BatchProcessStatus status = this.webObjectCacheManager.getImmutableProcessStatus(statusKey);
            
            if (!status.isError()) {
                messages.setRequireManualClose(false);
            }
            
            if (status.isDone()) {
                messages.setError(status.isError());
                if (status.getMessage() != null) {
                    messages.addMessage(status.getMessage());
                }
            } else {
                messages.checkback(WebCoreConstants.URI_BATCH_PROCESS_STATUS + "?statusKey=" + statusKey, StandardConstants.SECONDS_IN_MILLIS_3);
            }
        }
        
        return WidgetMetricsHelper.convertToJson(messages, MESSAGES, true);
    }
    
    private boolean invalidCsvFileParams(final MultipartFile multipartFile) {
        return multipartFile == null || multipartFile.isEmpty() || StringUtils.isEmpty(multipartFile.getName())
               || StringUtils.isEmpty(multipartFile.getContentType()) || StringUtils.isEmpty(multipartFile.getOriginalFilename())
               || !multipartFile.getOriginalFilename().toLowerCase(Locale.getDefault()).endsWith(WebCoreConstants.CSV_FILE_NAME_EXTENSION);
    }
    
    private CustomerProperty updateProperty(final UserAccount userAccount, final String valueString, final int customerPropertyTypeId) {
        
        final CustomerProperty originalProperty = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(userAccount.getCustomer().getId().intValue(), customerPropertyTypeId);
        
        if (originalProperty == null) {
            final CustomerProperty property = new CustomerProperty();
            property.setCustomer(userAccount.getCustomer());
            property.setCustomerPropertyType(CustomerAdminUtil.findCustomerPropertyType(customerPropertyTypeId));
            property.setPropertyValue(valueString);
            property.setLastModifiedGmt(new Date());
            property.setLastModifiedByUserId(userAccount.getId());
            return property;
        }
        if (!originalProperty.getPropertyValue().equals(valueString)) {
            originalProperty.setPropertyValue(valueString);
            originalProperty.setLastModifiedGmt(new Date());
            originalProperty.setLastModifiedByUserId(userAccount.getId());
        }
        return originalProperty;
    }
    
    private GlobalConfiguration getGlobalConfiguration() {
        
        return new GlobalConfiguration();
    }
    
    /**
     * This method returns notification details for JSON format.
     * 
     * @param notificationId
     *            Notification ID
     * @return NotificationDetails object
     */
    private NotificationDetails prepareNotificationDetailsForJSON(final Integer notificationId) {
        
        final Notification notification = this.notificationService.findNotificationById(notificationId, false);
        final NotificationDetails details = new NotificationDetails();
        // BeanUtils.copyProperties(notification, details);
        details.setTitle(notification.getTitle());
        details.setMessage(notification.getMessage());
        details.setMessageUrl(notification.getMessageUrl());
        
        // The time zone might have to be Customer Time Zone. Default is used because the original code used default.
        final PooledResource<DateFormat> format = DateUtil.takeDateFormat(DateUtil.DATE_ONLY_FORMAT_WIDGET, TimeZone.getDefault());
        
        if (notification.getBeginGmt() != null) {
            details.setBeginGmt(new Timestamp(notification.getBeginGmt().getTime()));
            details.setBeginTime(format.get().format(notification.getBeginGmt()));
        }
        
        if (notification.getEndGmt() != null) {
            details.setEndGmt(new Timestamp(notification.getEndGmt().getTime()));
            details.setEndTime(format.get().format(notification.getEndGmt()));
        }
        
        format.close();
        
        return details;
    }
    
    /**
     * This method prepares a list of current NotificationListInfo DTO objects to pass it to UI.
     * 
     * @param request
     *            HttpServletRequest object
     * @return collection of NotificationListInfo
     */
    private Collection<NotificationListInfo> prepareCurrentNotificationInfo(final HttpServletRequest request) {
        final List<Notification> currentNotifications = this.notificationService.findNotificationByEffectiveDate();
        return hideNotificationId(SessionTool.getInstance(request).getKeyMapping(), currentNotifications);
    }
    
    /**
     * This method prepares a list of past NotificationListInfo DTO objects to pass it to UI.
     * 
     * @param request
     *            HttpServletRequest object
     * @return collection of NotificationListInfo
     */
    private Collection<NotificationListInfo> preparePastNotificationInfo(final HttpServletRequest request) {
        final List<Notification> pastNotifications = this.notificationService.findNotificationInPast();
        return hideNotificationId(SessionTool.getInstance(request).getKeyMapping(), pastNotifications);
    }
    
    //PMD.AvoidInstantiatingObjectsInLoops: Required
    @SuppressWarnings({ "PMD.AvoidInstantiatingObjectsInLoops" })
    private List<NotificationListInfo> hideNotificationId(final RandomKeyMapping keyMapping, final List<Notification> notificationsList) {
        final List<NotificationListInfo> result = new ArrayList<NotificationListInfo>(notificationsList.size());
        for (Notification notification : notificationsList) {
            result.add(new NotificationListInfo(keyMapping.getRandomString(Notification.class, notification.getId()), notification.getTitle(),
                    notification.getBeginGmt()));
        }
        
        return result;
    }
    
    //PMD.AvoidInstantiatingObjectsInLoops: Required
    @SuppressWarnings({ "PMD.AvoidInstantiatingObjectsInLoops" })
    private List<MigrationValidationInfo> getMigrationValidationInfoList(final CustomerMigration customerMigration,
        final List<CustomerMigrationValidationStatus> validationStatusList) {
        List<MigrationValidationInfo> validationList = null;
        if (validationStatusList != null && !validationStatusList.isEmpty()) {
            validationList = new ArrayList<MigrationValidationInfo>(validationStatusList.size());
            for (CustomerMigrationValidationStatus validationStatus : validationStatusList) {
                final CustomerMigrationValidationType validationType = validationStatus.getCustomerMigrationValidationType();
                final MigrationValidationInfo validation = new MigrationValidationInfo();
                validation.setName(this.commonControllerHelper.getMessage(validationType.getName()));
                validation.setWarning(this.commonControllerHelper.getMessage(validationType.getWarning()));
                validation.setUrl(this.commonControllerHelper.getMessage(validationType.getUrl()));
                validation.setIsBlocking(validationType.isIsBlocking());
                validation.setIsPassed(validationStatus.isIsPassed());
                validationList.add(validation);
            }
            if (customerMigration.getCustomerMigrationValidationStatusType()
                    .getId() == WebCoreConstants.CUSTOMER_MIGRATION_VALIDATION_STATUS_TYPE_BLOCKED) {
                final MigrationValidationInfo validation = new MigrationValidationInfo();
                validation.setName(this.commonControllerHelper.getMessage(LabelConstants.CM_LABEL_ALLBLOCKERSPASSFORCUSTOMER));
                validation.setWarning(this.commonControllerHelper.getMessage(LabelConstants.CM_WARNING_ALLBLOCKERSPASSFORCUSTOMER));
                validation.setUrl(this.commonControllerHelper.getMessage(LabelConstants.CM_URL_ALLBLOCKERSPASSFORCUSTOMER));
                validation.setIsBlocking(true);
                validation.setIsPassed(false);
                validationList.add(validation);
            }
        }
        
        return validationList;
    }
    
    private void addCaseAccount(final Integer customerId, final RandomKeyMapping keyMapping, final ModelMap model) {
        final CaseCustomerAccount caseCustomerAccount = this.caseService.findCaseCustomerAccountByCustomerId(customerId);
        if (caseCustomerAccount != null) {
            final SearchCustomerResultSetting searchCustomerResultSetting = new SearchCustomerResultSetting();
            searchCustomerResultSetting.setName(caseCustomerAccount.getCaseAccountName());
            searchCustomerResultSetting.setRandomId(keyMapping.getRandomString(CaseCustomerAccount.class, caseCustomerAccount.getId()));
            model.put("caseCustomer", searchCustomerResultSetting);
            
        }
    }
    
    private String buildErrorMessageInfo(final WebSecurityForm<?> form, final String messageKey) {
        final MessageInfo messageInfo = new MessageInfo();
        messageInfo.addMessage(this.commonControllerHelper.getMessage(messageKey));
        messageInfo.setRequireConfirmation(true);
        messageInfo.setError(true);
        form.resetToken();
        messageInfo.setToken(form.getInitToken());
        
        return WidgetMetricsHelper.convertToJson(messageInfo, MESSAGES, true);
    }
    
}
