package com.digitalpaytech.mvc.customeradmin;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.domain.AuthorizationType;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerPropertyType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.customeradmin.support.CardSettingsEditForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerBadCardService;
import com.digitalpaytech.service.CustomerCardService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.util.CustomerAdminUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.customeradmin.CardSettingsValidator;

@Controller
@SessionAttributes({WebSecurityConstants.WEB_SECURITY_FORM})
public class CardSettingsController {
	
	@Autowired
	protected CustomerAdminService customerAdminService;
	
	@Autowired
	protected CustomerCardTypeService customerCardTypeService;	

	@Autowired
	private EntityService entityService;

	@Autowired
	private CardSettingsValidator cardSettingsValidator;
	
	@Autowired
	private CustomerCardService customerCardService;
	
	@Autowired
	private CustomerBadCardService customerBadCardService;

	private static final String AUTHORIZATION_TYPES = "authTypes";
	
	public void setCustomerAdminService(CustomerAdminService customerAdminService) {
		this.customerAdminService = customerAdminService;
	}
	
	public void setCustomerCardTypeService(CustomerCardTypeService customerCardTypeService) {
		this.customerCardTypeService = customerCardTypeService;
	}	
	
	public void setCardSettingsValidator(CardSettingsValidator cardSettingsValidator) {
    	this.cardSettingsValidator = cardSettingsValidator;
    }

	public void setEntityService(EntityService entityService) {
    	this.entityService = entityService;
    }

	public void setCustomerCardService(CustomerCardService customerCardService) {
		this.customerCardService = customerCardService;
	}

	public void setCustomerBadCardService(
			CustomerBadCardService customerBadCardService) {
		this.customerBadCardService = customerBadCardService;
	}
	
    /**
	 * This method will look at the permissions in the WebUser and pass control
	 * to the first tab that the user has permissions for.
	 * 
	 * @return Will return the method for definedAlerts, or error if no
	 *         permissions exist.
	 */
	@RequestMapping(value = "/secure/settings/cardSettings/index.html")
	public String getFirstPermittedTab(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		
		if (WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_SETUP_CREDIT_CARD_PROCESSING)
			|| WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_CONFIGURE_CUSTOM_CARDS)) 
		{
			return getCardSettingsAndTypesList(request, response, model);
		}
		else{
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD) && 
                    !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) { 
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }            
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD; 			
		}			
	}
	
	//========================================================================================================================
	// "Settings" tab
	//========================================================================================================================

	@RequestMapping(value = "/secure/settings/cardSettings/settings.html")
	public String getCardSettingsAndTypesList(HttpServletRequest request, HttpServletResponse response, ModelMap model) {

	    if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_SETUP_CREDIT_CARD_PROCESSING)
	            && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_CONFIGURE_CUSTOM_CARDS)) {
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			return WebCoreConstants.RESPONSE_FALSE;
		}
		
		CustomerAdminUtil.setCustomerPropertyTypes(customerAdminService.getCustomerPropertyTypes());
        UserAccount userAccount = WebSecurityUtil.getUserAccount();

		//--------------------------------------------------------------------------
		// Card Settings
		//--------------------------------------------------------------------------
        
        // Credit Card Offline Retry
        CustomerProperty prop = findCustomerProperty(userAccount.getCustomer(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_CREDIT_CARD_OFFLINE_RETRY);
		String ccOfflineRetry = "";
		if (prop != null) {
			ccOfflineRetry = prop.getPropertyValue();
		}

		// Max Offline Retry
		prop = findCustomerProperty(userAccount.getCustomer(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_MAX_OFFLINE_RETRY);
		String maxOfflineRetry = "";
		if (prop != null) {
			maxOfflineRetry = prop.getPropertyValue();
		}

		model.put("creditCardOfflineRetry", ccOfflineRetry);
		model.put("maxUserAccounts", maxOfflineRetry);
	
		
		//--------------------------------------------------------------------------
		// Card Types
		//--------------------------------------------------------------------------		
		List<CustomerCardType> types = getCardTypesList(request, userAccount.getCustomer().getId());
		model.put("customerCardTypes", types);
		
//		SessionTool sessionTool = SessionTool.getInstance(request);
        WebSecurityForm<CardSettingsEditForm> webSecurityForm = new WebSecurityForm<CardSettingsEditForm>(null, new CardSettingsEditForm());
        model.put(WebSecurityConstants.WEB_SECURITY_FORM, webSecurityForm);
        model.put(AUTHORIZATION_TYPES, getAuthorizationTypes(request));
        //TODO Remove SessionToken
//        model.put(WebSecurityConstants.SESSION_TOKEN, sessionTool.getAttribute(WebSecurityConstants.SESSION_TOKEN));
        
		return "/settings/cardSettings/settings";
	}
	
	
	@RequestMapping(value = "/secure/settings/cardSettings/saveCardSettings.html", method = RequestMethod.POST)
	public @ResponseBody String saveCardSettings(@ModelAttribute(WebSecurityConstants.WEB_SECURITY_FORM) WebSecurityForm<CardSettingsEditForm> webSecurityForm,
											 	 BindingResult result, 
											 	 HttpServletRequest request,
											 	 HttpServletResponse response, 
											 	 ModelMap model) {

		if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_SETUP_CREDIT_CARD_PROCESSING)) {
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			return WebCoreConstants.RESPONSE_FALSE;
		}
		
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();

        cardSettingsValidator.validate(webSecurityForm, result, keyMapping);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
		webSecurityForm.resetToken();
		
		CardSettingsEditForm theForm = webSecurityForm.getWrappedObject();
		
		UserAccount userAcct = WebSecurityUtil.getUserAccount();
		
		// CUSTOMER_PROPERTY_TYPE_CREDIT_CARD_OFFLINE_RETRY
		CustomerProperty ccRetryProp = 
				populateCustomerProperty(userAcct.getCustomer(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_CREDIT_CARD_OFFLINE_RETRY, theForm.getCreditCardOfflineRetry());
		customerAdminService.saveOrUpdateCustomerProperty(ccRetryProp);
		
		// CUSTOMER_PROPERTY_TYPE_MAX_OFFLINE_RETRY
		CustomerProperty maxRetryProp = 
				populateCustomerProperty(userAcct.getCustomer(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_MAX_OFFLINE_RETRY, theForm.getMaxOfflineRetry());
		customerAdminService.saveOrUpdateCustomerProperty(maxRetryProp);
		
		return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(":").append(webSecurityForm.getInitToken()).toString();
	}

	
	private CustomerProperty populateCustomerProperty(Customer customer, int customerPropTypeId, String propValue) {
		CustomerProperty custProp = findCustomerProperty(customer, customerPropTypeId);
		if (custProp == null) {
			custProp = new CustomerProperty();
			custProp.setCustomerPropertyType(CustomerAdminUtil.findCustomerPropertyType(customerPropTypeId));
		}
		custProp.setCustomer(customer);
		custProp.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
		custProp.setPropertyValue(propValue);
		return custProp;
	}
	
	
	private CustomerProperty findCustomerProperty(Customer customer, int customerPropertyTypeId) {

		CustomerPropertyType type = CustomerAdminUtil.findCustomerPropertyType(customerPropertyTypeId);
		if (type == null) {
			StringBuilder bdr = new StringBuilder();
			bdr.append("customerPropertyTypeId: ").append(customerPropertyTypeId).append(" doesn't exist in customerAdminService.getCustomerPropertyTypes, size: ");
			bdr.append(customerAdminService.getCustomerPropertyTypes().size());
			throw new RuntimeException(bdr.toString());
		}	
		CustomerProperty prop = CustomerAdminUtil.findCustomerProperty(type.getId(), customer.getCustomerProperties().iterator());
		return prop;
	}
	
	
	private List<CustomerCardType> getCardTypesList(HttpServletRequest request, int customerId) {
		List<CustomerCardType> types = customerCardTypeService.findByCustomerIdIsLocked(customerId, false);
		RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
		if(types != null){
			Iterator<CustomerCardType> iter = types.iterator();
			while (iter.hasNext()) {
				CustomerCardType cardType = iter.next();
				cardType.setRandomId(keyMapping.getRandomString(cardType, WebCoreConstants.ID_LOOK_UP_NAME));
				
				AuthorizationType authType = cardType.getAuthorizationType();
				authType.setRandomId(keyMapping.getRandomString(authType, WebCoreConstants.ID_LOOK_UP_NAME));
			}
		}
		return types;	
	}

	
	/**
	 * Tries to get AuthorizationTypes from user's session first, if it doesn't exist it will retrieve data from the database
	 * and set it to the attribute.
	 * @param request
	 * @return List<AuthorizationType> Populated list of AuthorizationType objects with randomIds.
	 */
	private List<AuthorizationType> getAuthorizationTypes(HttpServletRequest request) {
		SessionTool sessionTool = SessionTool.getInstance();
		
		@SuppressWarnings("unchecked")
		List<AuthorizationType> authTypes = (List<AuthorizationType>) sessionTool.getAttribute(AUTHORIZATION_TYPES);
		if (authTypes == null) {
			authTypes = entityService.loadAll(AuthorizationType.class);
			
			RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
			Iterator<AuthorizationType> iter = authTypes.iterator();
			while (iter.hasNext()) {
				AuthorizationType authType = iter.next();
				authType.setRandomId(keyMapping.getRandomString(authType, WebCoreConstants.ID_LOOK_UP_NAME));
			}
			// bReplace to 'true' so it will already set attribute value.
			sessionTool.setAttribute(AUTHORIZATION_TYPES, authTypes); 
		}
		return authTypes;
	}
	
	@RequestMapping(value = "/secure/settings/cardSettings/saveCardType.html", method = RequestMethod.POST)
	public @ResponseBody String saveCardType(@ModelAttribute(WebSecurityConstants.WEB_SECURITY_FORM) WebSecurityForm<CardSettingsEditForm> webSecurityForm,
											 BindingResult result, 
											 HttpServletRequest request, 
											 HttpServletResponse response, 
											 ModelMap model) {
		
		/* validate user account if it has proper role to view it. */
		if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_CONFIGURE_CUSTOM_CARDS)) {
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			return WebCoreConstants.RESPONSE_FALSE;
		}
		
        if (!this.cardSettingsValidator.validateMigrationStatus(webSecurityForm)) {
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }

		RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
		// Sets Customer to the form for the validator.
		CardSettingsEditForm form = webSecurityForm.getWrappedObject();
		webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_UPDATE);
		if(StringUtils.isEmpty(form.getCustomerCardTypeRandomId())){
			webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
		}
		
		form.setCustomerId(WebSecurityUtil.getUserAccount().getCustomer().getId());
		cardSettingsValidator.validate(webSecurityForm, result, keyMapping);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
		cardSettingsValidator.validateAuthorizationType(webSecurityForm, result, getAuthorizationTypes(request), keyMapping);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }

		webSecurityForm.resetToken();			
				
		UserAccount userAccount = WebSecurityUtil.getUserAccount();
		
        // Check if new CustomerCardType name already exists in the database.
		if(cardSettingsValidator.isExistCardTypeName(userAccount.getCustomer().getId(), form.getCardTypeName(), form.getCustomerCardTypeId())) {
			webSecurityForm.addErrorStatus(new FormErrorStatus("cardType", this.cardSettingsValidator.getMessage("alert.settings.cardSettings.CustomerCardType.already.exists", form.getCardTypeName())));
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
		}
		
		// Everything is ok, save CustomerCardType now.		
		CustomerCardType custCardType = new CustomerCardType();
		custCardType.setId(form.getCustomerCardTypeId());
		custCardType.setCustomer(userAccount.getCustomer());
		custCardType.setCardType(getCardType(WebCoreConstants.VALUE_CARD));
		custCardType.setAuthorizationType(getAuthorizationType(request, form.getAuthorizeTypeId()));
		custCardType.setName(form.getCardTypeName().trim());
		custCardType.setTrack2pattern(form.getTrack2Pattern());
		custCardType.setDescription(form.getDescription());		
		custCardType.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
		custCardType.setLastModifiedByUserId(userAccount.getCustomer().getId());
		
		if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
			this.customerCardTypeService.updateCardType(custCardType);
		}
		else {
			this.customerCardTypeService.createCustomerCardType(custCardType);
		}
		
		return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(":").append(webSecurityForm.getInitToken()).toString();	
	}


	private CardType getCardType(int cardTypeId) {
		CardType cardType = null;
		Iterator<CardType> iter = customerAdminService.getCardTypes().iterator();
		while (iter.hasNext()) {
			cardType = iter.next();
			if (cardType.getId() == cardTypeId) {
				break;
			}
		}
		return cardType;
	}
	
	private AuthorizationType getAuthorizationType(HttpServletRequest request, int authTypeId) {
		AuthorizationType authType = null;
		Iterator<AuthorizationType> iter = getAuthorizationTypes(request).iterator();
		while (iter.hasNext()) {
			authType = iter.next();
			if (authType.getId() == authTypeId) {
				return authType;
			}
		}
		return null;
	}
	
	private CustomerCardType getCustomerCarCardType(int customerCardTypeId, Iterator<CustomerCardType> customerCardTypeIter) {
		CustomerCardType type;
		while (customerCardTypeIter.hasNext()) {
			type = customerCardTypeIter.next();
			if (type.getId() == customerCardTypeId) {
				return type;
			}
		}
		return null;
	}
	

	@RequestMapping(value = "/secure/settings/cardSettings/editCardType.html")
	public @ResponseBody String editCardType(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_CONFIGURE_CUSTOM_CARDS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
	    
	    String selectedRandomId = request.getParameter("id");
		
		UserAccount userAccount = WebSecurityUtil.getUserAccount();
		List<CustomerCardType> types = getCardTypesList(request, userAccount.getCustomer().getId());
		CustomerCardType customerCardType = null;
		Iterator<CustomerCardType> iter = types.iterator();
		while (iter.hasNext()) {
			customerCardType = iter.next();
			if (customerCardType.getRandomId().equals(selectedRandomId)) {
				break;
			}
		}
		
		model.put(AUTHORIZATION_TYPES, getAuthorizationTypes(request));
		CardSettingsEditForm form = new CardSettingsEditForm();
		if (customerCardType != null) {
    		form.setCustomerCardTypeRandomId(customerCardType.getRandomId());
    		form.setAuthorizationMethod(customerCardType.getAuthorizationType().getName());
    		form.setAuthorizationMethodRandomId(findAuthorizationTypeRandomId(getAuthorizationTypes(request), customerCardType.getAuthorizationType().getName()));
    		form.setCardTypeName(customerCardType.getName());
    		form.setTrack2Pattern(customerCardType.getTrack2pattern());
    		form.setDescription(customerCardType.getDescription());
		}
		return WidgetMetricsHelper.convertToJson(form, "cardSettingsEditForm", true);	
	}
	
	
	private String findAuthorizationTypeRandomId(List<AuthorizationType> authTypes, String authTypeName) {
	    AuthorizationType type;
	    Iterator<AuthorizationType> iter = authTypes.iterator();
	    while (iter.hasNext()) {
	        type = iter.next();
	        if (type.getName().equalsIgnoreCase(authTypeName)) {
	            return type.getRandomId();
	        }
	    }
	    return null;
	}

	@ResponseBody
	@RequestMapping(value = "/secure/settings/cardSettings/deleteCardType.html", method = RequestMethod.GET)
	public String deleteCardType(HttpServletRequest request, HttpServletResponse response, ModelMap model) {

		if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_CONFIGURE_CUSTOM_CARDS)) {
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			return WebCoreConstants.RESPONSE_FALSE;
		}
		
        if (!WebSecurityUtil.isCustomerMigrated()) {
            StringBuilder bdr = new StringBuilder();
            bdr.append(WebCoreConstants.RESPONSE_FALSE).append(":").append(this.cardSettingsValidator.getMessage("error.customer.notMigrated"));
            return bdr.toString();
        }

		String selectedRandomId = request.getParameter("id");
		RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
		Object obj = keyMapping.getKey(selectedRandomId);
		
		int id = 0;
		// Validates CustomerCardType id.
		if (obj instanceof String) {
    		String idStr = (String) obj;
			if (StringUtils.isBlank(idStr)) {
    			return WebCoreConstants.RESPONSE_FALSE;
    		}
    		if (!WebCoreUtil.checkIfAllDigits(idStr)) {
    			return WebCoreConstants.RESPONSE_FALSE;
    		}
    		id = Integer.parseInt((String) obj);
    		
		} else if (obj instanceof Integer && ((Integer) obj).intValue() <= 0) {
			return WebCoreConstants.RESPONSE_FALSE;
		
		} else if (obj instanceof Integer) {
			id = (Integer) obj;
		}
		
		UserAccount userAccount = WebSecurityUtil.getUserAccount();
		List<CustomerCardType> list = getCardTypesList(request, userAccount.getCustomer().getId());
		CustomerCardType cardType = getCustomerCarCardType(id, list.iterator());
		
		if (cardType == null) {
		    return WebCoreConstants.RESPONSE_FALSE;
		}
		
		ArrayList<Integer> checkIds = new ArrayList<Integer>(2);
		checkIds.add(cardType.getId());
		if((cardType.getParentCustomerCardType() != null) && (this.customerCardTypeService.countChildType(cardType.getParentCustomerCardType().getId()) <= 1)) {
			checkIds.add(cardType.getParentCustomerCardType().getId());
		}
		
		MessageInfo message = new MessageInfo();
		if (this.customerCardService.countByCustomerCardType(checkIds) > 0) {
			message.addMessage(this.cardSettingsValidator.getMessage("alert.settings.cardSettings.CustomerCard.isActive"));
		    message.setError(true);
		}
		if (this.customerBadCardService.countByCustomerCardType(checkIds) > 0) {
			message.addMessage(this.cardSettingsValidator.getMessage("alert.settings.cardSettings.CustomerBadCard.isActive"));
		    message.setError(true);
		}
		
		// Need to confirm there is no active Customer Card (IsDeleted = true or card expired) before 'soft-delete' CustomerCardType.
		if (message.isError()) {
		    return WidgetMetricsHelper.convertToJson(message, "messages", true);
		}
		
		this.customerCardTypeService.deleteCardType(cardType);
		
		return WebCoreConstants.RESPONSE_TRUE;	
	}
}
