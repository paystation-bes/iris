package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;

public class CustomerCardForm implements Serializable {
    private static final long serialVersionUID = -34967988887946918L;
    
    /* Modifiable */
    private String randomId;
    private String cardNumber;
    private String startValidDate;
    private String expireDate;
    private String maxNumOfUses;
    private String comment;
    
    private Boolean isRestricted;
    private String gracePeriod;
    
    private String cardTypeRandomId;
    private String locationRandomId;
    
    /* Read Only */
    private String assignedTo;
    private Integer remainingNumOfUses;
    
    public CustomerCardForm() {
        
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getStartValidDate() {
        return startValidDate;
    }

    public void setStartValidDate(String beginValidDate) {
        this.startValidDate = beginValidDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getMaxNumOfUses() {
        return maxNumOfUses;
    }

    public void setMaxNumOfUses(String maxNumOfUses) {
        this.maxNumOfUses = maxNumOfUses;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getGracePeriod() {
        return gracePeriod;
    }

    public void setGracePeriod(String gracePeriod) {
        this.gracePeriod = gracePeriod;
    }

    public Integer getRemainingNumOfUses() {
        return remainingNumOfUses;
    }

    public void setRemainingNumOfUses(Integer remainingNumOfUses) {
        this.remainingNumOfUses = remainingNumOfUses;
    }

	public String getCardTypeRandomId() {
		return cardTypeRandomId;
	}

	public void setCardTypeRandomId(String cardTypeRandomId) {
		this.cardTypeRandomId = cardTypeRandomId;
	}

	public String getLocationRandomId() {
		return locationRandomId;
	}

	public void setLocationRandomId(String locationRandomId) {
		this.locationRandomId = locationRandomId;
	}

	public String getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	public String getRandomId() {
		return randomId;
	}

	public void setRandomId(String randomId) {
		this.randomId = randomId;
	}

	public Boolean getIsRestricted() {
		return isRestricted;
	}

	public void setIsRestricted(Boolean isRestricted) {
		this.isRestricted = isRestricted;
	}
}
