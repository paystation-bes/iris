package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;
import java.util.Date;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.constants.DateRangeOption;
@StoryAlias(TransactionReceiptSearchForm.ALIAS)
public class TransactionReceiptSearchForm implements Serializable {
    
    public static final String ALIAS = "transaction receipt search form";
    public static final String ALIAS_CARD_NUMBER = "card number";
    public static final String ALIAS_START_DATE_TIME = "start date time";
    public static final String ALIAS_END_DATE_TIME = "end date time";
    public static final String ALIAS_START_DATE = "start date";
    public static final String ALIAS_END_DATE = "end date";
    public static final String ALIAS_DATE_TYPE = "date type";
        
    private static final long serialVersionUID = -6503291861650644895L;
    
    private DateRangeOption dateType;
    private String startDate;
    private String startTime;
    private String endDate;
    private String endTime;
    private Date startDateTime;
    private Date endDateTime;
    private String cardNumber;
    private String plateNumber;
    private String emailAddress;
    private String spaceNumber;
    private String mobileNumber;
    private String couponNumber;
    private String locationRandomId;
    
    private Integer page;
    private Integer itemsPerPage;
    private Long dataKey;
    
    public TransactionReceiptSearchForm() {
        
    }
    
    public final String getLocationRandomId() {
        return this.locationRandomId;
    }
    
    public final void setLocationRandomId(final String locationRandomId) {
        this.locationRandomId = locationRandomId;
    }
    
    public final Integer getPage() {
        return this.page;
    }
    
    public final void setPage(final Integer page) {
        this.page = page;
    }
    
    public final String getCouponNumber() {
        return this.couponNumber;
    }
    
    public final void setCouponNumber(final String couponNumber) {
        this.couponNumber = couponNumber;
    }
    
    public final String getMobileNumber() {
        return this.mobileNumber;
    }
    
    public final void setMobileNumber(final String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
    
    public final Integer getItemsPerPage() {
        return this.itemsPerPage;
    }
    
    public final void setItemsPerPage(final Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }
    
    public final String getPlateNumber() {
        return this.plateNumber;
    }
    
    public final void setPlateNumber(final String plateNumber) {
        this.plateNumber = plateNumber;
    }
    
    public final String getSpaceNumber() {
        return this.spaceNumber;
    }
    
    public final void setSpaceNumber(final String spaceNumber) {
        this.spaceNumber = spaceNumber;
    }
    
    public final String getEmailAddress() {
        return this.emailAddress;
    }
    
    public final void setEmailAddress(final String emailAddress) {
        this.emailAddress = emailAddress;
    }
    
    @StoryAlias(ALIAS_START_DATE)
    public final String getStartDate() {
        return this.startDate;
    }
    
    public final void setStartDate(final String startDate) {
        this.startDate = startDate;
    }
    
    @StoryAlias(ALIAS_END_DATE)
    public final String getEndDate() {
        return this.endDate;
    }
    
    public final void setEndDate(final String endDate) {
        this.endDate = endDate;
    }
    
    @StoryAlias(ALIAS_START_DATE_TIME)
    public final Date getStartDateTime() {
        return this.startDateTime;
    }
    
    public final void setStartDateTime(final Date startDateTime) {
        this.startDateTime = startDateTime;
    }
    
    @StoryAlias(ALIAS_END_DATE_TIME)
    public final Date getEndDateTime() {
        return this.endDateTime;
    }
    
    public final void setEndDateTime(final Date endDateTime) {
        this.endDateTime = endDateTime;
    }
    
    @StoryAlias(ALIAS_CARD_NUMBER)
    public final String getCardNumber() {
        return this.cardNumber;
    }
    
    public final void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }
    
    @StoryAlias(ALIAS_DATE_TYPE)
    public final DateRangeOption getDateType() {
        return this.dateType;
    }
    
    public final void setDateType(final DateRangeOption dateType) {
        this.dateType = dateType;
    }
    
    public final String getStartTime() {
        return this.startTime;
    }
    
    public final void setStartTime(final String startTime) {
        this.startTime = startTime;
    }
    
    public final String getEndTime() {
        return this.endTime;
    }
    
    public final void setEndTime(final String endTime) {
        this.endTime = endTime;
    }

    public final Long getDataKey() {
        return this.dataKey;
    }

    public final void setDataKey(final Long dataKey) {
        this.dataKey = dataKey;
    }
}
