package com.digitalpaytech.mvc.customeradmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.mvc.RateProfileController;
import com.digitalpaytech.mvc.RateProfileLocationController;
import com.digitalpaytech.mvc.RateRateProfileController;
import com.digitalpaytech.mvc.support.RateProfileEditForm;
import com.digitalpaytech.mvc.support.RateProfileLocationForm;
import com.digitalpaytech.mvc.support.RateProfileSearchForm;
import com.digitalpaytech.mvc.support.RateRateProfileEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
@SessionAttributes({ RateProfileController.FORM_EDIT })
public class CustomerAdminRateProfileController extends RateProfileController {
    @RequestMapping(value = "/secure/settings/rates/rateProfile.html", method = RequestMethod.GET)
    public final String viewRateProfile(@ModelAttribute(RateRateProfileController.FORM_SCHEDULE)
                                        final WebSecurityForm<RateRateProfileEditForm> rescheduleForm,
                                        @ModelAttribute(RateProfileLocationController.FORM_EDIT_PROFILE)
                                        final WebSecurityForm<RateProfileLocationForm> locationForm,
                                        final HttpServletRequest request, final HttpServletResponse response,
                                        final ModelMap model) {
        if (!checkPermissions(false)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD) && 
                    !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) { 
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }            
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
        return super.view(rescheduleForm, locationForm, request, response, model, "/settings/rates/rateProfile");
    }
    
    @RequestMapping(value = "/secure/settings/rates/searchRateProfile.html", method = RequestMethod.POST)
    @ResponseBody
    public final String searchRate(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!checkPermissions(false)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.search(request, response);
    }
    
    @RequestMapping(value = "/secure/settings/rates/rateProfileList.html", method = RequestMethod.POST)
    @ResponseBody
    public final String getRateProfileList(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<RateProfileSearchForm> form, final HttpServletRequest request,
                                           final HttpServletResponse response) {
        if (!checkPermissions(false)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.getList(form, request, response);
    }
    
    @RequestMapping(value = "/secure/settings/rates/locatePageWithRateProfile.html", method = RequestMethod.POST)
    @ResponseBody
    public final String locatePageWithRateProfile(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<RateProfileSearchForm> form,
                                                  final HttpServletRequest request, final HttpServletResponse response) {
        if (!checkPermissions(false)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.locatePageWith(form, request, response);
    }
    
    @RequestMapping(value = "/secure/settings/rates/addRateProfile.html", method = RequestMethod.GET)
    @ResponseBody
    public final String addRateProfile(@ModelAttribute(FORM_EDIT) final WebSecurityForm<RateProfileEditForm> webSecurityForm, final HttpServletRequest request,
                                       final HttpServletResponse response) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.add(webSecurityForm, request, response);
    }
    
    @RequestMapping(value = "/secure/settings/rates/editRateProfile.html", method = RequestMethod.GET)
    @ResponseBody
    public final String editRateProfile(@ModelAttribute(FORM_EDIT) final WebSecurityForm<RateProfileEditForm> webSecurityForm,
                                        final HttpServletRequest request, final HttpServletResponse response) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.edit(webSecurityForm, request, response);
    }
    
    @RequestMapping(value = "/secure/settings/rates/saveRateProfile.html", method = RequestMethod.POST)
    @ResponseBody
    public final String saveRateProfile(@ModelAttribute(FORM_EDIT) final WebSecurityForm<RateProfileEditForm> webSecurityForm, final BindingResult result,
                                        final HttpServletRequest request, final HttpServletResponse response) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.save(webSecurityForm, result, request, response);
    }
    
    @RequestMapping(value = "/secure/settings/rates/validateRateProfile.html", method = RequestMethod.POST)
    @ResponseBody
    public final String validateRateProfile(@ModelAttribute(FORM_EDIT) final WebSecurityForm<RateProfileEditForm> webSecurityForm, final BindingResult result,
                                            final HttpServletRequest request, final HttpServletResponse response) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.validate(webSecurityForm, result, request, response);
    }
    
    @RequestMapping(value = "/secure/settings/rates/publishRateProfile.html", method = RequestMethod.POST)
    @ResponseBody
    public final String publishRateProfile(@ModelAttribute(FORM_EDIT) final WebSecurityForm<RateProfileEditForm> webSecurityForm, final BindingResult result,
                                           final HttpServletRequest request, final HttpServletResponse response) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.publish(webSecurityForm, result, request, response);
    }
    
    @RequestMapping(value = "/secure/settings/rates/cancelRateProfile.html", method = RequestMethod.GET)
    @ResponseBody
    public final String cancelRateProfile(final HttpServletRequest request, final HttpServletResponse response) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.cancel(request, response);
    }
    
    @RequestMapping(value = "/secure/settings/rates/verifyDeleteRateProfile.html", method = RequestMethod.GET)
    @ResponseBody
    public final String verifyDeleteRateProfile(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.verifyDelete(request, response);
    }
    
    @RequestMapping(value = "/secure/settings/rates/deleteRateProfile.html", method = RequestMethod.GET)
    @ResponseBody
    public final String deleteRateProfile(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        return super.delete(request, response);
    }
    
    private boolean checkPermissions(final boolean isManageOnly) {
        if (!WebSecurityUtil.hasValidCustomerSubscription(WebCoreConstants.SUBSCRIPTION_TYPE_ONLINE_RATE_CONFIGURATION)) {
            return false;
        }
        
        if (isManageOnly) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_RATES)) {
                return false;
            }
        } else {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_RATES)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_RATES)) {
                return false;
            }
        }
        return true;
    }
}
