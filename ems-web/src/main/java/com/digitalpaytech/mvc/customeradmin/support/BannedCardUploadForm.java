package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

/**
 * This class holds file upload form for importing banned card lists.
 * 
 * @author Brian Kim
 * 
 */
public class BannedCardUploadForm implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 811170009124460874L;
	private MultipartFile file;
	private String recordStatus;

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String getRecordStatus() {
		return recordStatus;
	}

	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}

}
