package com.digitalpaytech.mvc.customeradmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.dto.FlexLocationPropertySearchCriteria;
import com.digitalpaytech.dto.FlexLocationPropertySelectorDTO;
import com.digitalpaytech.dto.PaginatedList;
import com.digitalpaytech.mvc.FlexLocationPropertyListController;
import com.digitalpaytech.mvc.support.LocationIntegrationSearchForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
@SessionAttributes({ "LocationIntegrationSearchForm" })
public class CustomerAdminFlexLocationPropertyListController extends FlexLocationPropertyListController {
    
    @ResponseBody
    @RequestMapping(value = "/secure/flexLocationPropertySelectors.html", method = RequestMethod.GET)
    public final String flexLocationPropertySelectors(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<LocationIntegrationSearchForm> form,
        final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS)
            && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final FlexLocationPropertySearchCriteria criteria = super.populateFlexLocationPropertySearchCriteria(request, form.getWrappedObject(),
                                                                                                             WebSecurityUtil.getUserAccount()
                                                                                                                     .getCustomer().getId(), false);
        
        final PaginatedList<FlexLocationPropertySelectorDTO> result = new PaginatedList<FlexLocationPropertySelectorDTO>();
        result.setElements(this.flexWSService.findFlexLocationPropertySelector(criteria));
        result.setDataKey(Long.toString(criteria.getMaxUpdatedTime().getTime(), WebCoreConstants.PAGED_LIST_MAXUPDATETIME_LONG_LENGTH));
        
        return WidgetMetricsHelper.convertToJson(result, "flexLocationPropertyList", true);
    }
    
}
