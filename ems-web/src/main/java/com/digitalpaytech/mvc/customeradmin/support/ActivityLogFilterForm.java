package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Transient;

import com.digitalpaytech.domain.ActivityType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

public class ActivityLogFilterForm implements Serializable {
    
    private static final long serialVersionUID = -6807778563686016905L;
    private List<FilterEntry> userAccounts;
    private List<FilterEntry> logTypes;
    private List<String> dateRanges;
    
    private String selectedUserAccounts;
    private String selectedLogTypes;
    private String selectedDateRange;
    private String customDateStart;
    private String customDateEnd;
    
    @Transient
    private Integer selectedUserAccountsRealValue;
    @Transient
    private Integer selectedLogTypesRealValue;
    
    private String order;
    private String column;
    
    private String page;
    private String numOfRecord;
    
    private Long dataKey;
    
    public ActivityLogFilterForm() {
        userAccounts = new ArrayList<FilterEntry>();
        userAccounts.add(new FilterEntry("All", "All"));
        logTypes = new ArrayList<FilterEntry>();
        setUpDateRange();
    }
    
    //TODO: get these from a database table?
    private void setUpDateRange() {
        dateRanges = new ArrayList<String>();
        dateRanges.add("All");
        dateRanges.add("Last 24 Hours");
        dateRanges.add("This Week");
        dateRanges.add("Last Week");
        dateRanges.add("Last 2 Weeks");
        dateRanges.add("Last Month");
        dateRanges.add("Last 3 Months");
        dateRanges.add("Last 6 Months");
        dateRanges.add("Last Year");
        dateRanges.add("Custom Date Range");
    }
    
    public String getOrder() {
        return order;
    }
    
    public void setOrder(String order) {
        this.order = order;
    }
    
    public String getColumn() {
        return column;
    }
    
    public void setColumn(String column) {
        this.column = column;
    }
    
    public String getPage() {
        return page;
    }
    
    public void setPage(String page) {
        this.page = page;
    }
    
    public String getNumOfRecord() {
        return numOfRecord;
    }
    
    public void setNumOfRecord(String numOfRecord) {
        this.numOfRecord = numOfRecord;
    }
    
    public List<FilterEntry> getUserAccounts() {
        return userAccounts;
    }
    
    public void setUserAccounts(List<FilterEntry> userAccounts) {
        this.userAccounts = userAccounts;
    }
    
    public void addUserAccount(UserAccount userAccount) throws UnsupportedEncodingException {
        RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        this.userAccounts.add(new FilterEntry(URLDecoder.decode(userAccount.getRealFirstName(), WebSecurityConstants.URL_ENCODING_LATIN1), keyMapping
                .getRandomString(userAccount, WebCoreConstants.ID_LOOK_UP_NAME)));
    }
    
    public List<FilterEntry> getLogTypes() {
        return logTypes;
    }
    
    public void setLogTypes(List<FilterEntry> logTypes) {
        this.logTypes = logTypes;
    }
    
    public void addLogType(ActivityType activityType) {
        RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        this.logTypes.add(new FilterEntry(activityType.getName(), keyMapping.getRandomString(activityType, WebCoreConstants.ID_LOOK_UP_NAME)));
    }
    
    public List<String> getDateRanges() {
        return dateRanges;
    }
    
    public void setDateRanges(List<String> dateRanges) {
        this.dateRanges = dateRanges;
    }
    
    public String getSelectedUserAccounts() {
        return selectedUserAccounts;
    }
    
    public void setSelectedUserAccounts(String selectedUserAccounts) {
        this.selectedUserAccounts = selectedUserAccounts;
    }
    
    public String getSelectedDateRange() {
        return selectedDateRange;
    }
    
    public void setSelectedDateRange(String selectedDateRange) {
        this.selectedDateRange = selectedDateRange;
    }
    
    public String getSelectedLogTypes() {
        return selectedLogTypes;
    }
    
    public void setSelectedLogTypes(String selectedLogTypes) {
        this.selectedLogTypes = selectedLogTypes;
    }
    
    public String getCustomDateStart() {
        return customDateStart;
    }
    
    public void setCustomDateStart(String customDateStart) {
        this.customDateStart = customDateStart;
    }
    
    public String getCustomDateEnd() {
        return customDateEnd;
    }
    
    public void setCustomDateEnd(String customDateEnd) {
        this.customDateEnd = customDateEnd;
    }
    
    public static class FilterEntry implements Serializable {
        /**
		 * 
		 */
        private static final long serialVersionUID = -7556720767090360705L;
        private String name;
        private String randomId;
        
        private FilterEntry(String name, String randomId) {
            this.name = name;
            this.randomId = randomId;
        }
        
        public String getName() {
            return name;
        }
        
        public void setName(String name) {
            this.name = name;
        }
        
        public String getRandomId() {
            return randomId;
        }
        
        public void setRandomId(String randomId) {
            this.randomId = randomId;
        }
    }
    
    public Integer getSelectedUserAccountsRealValue() {
        return selectedUserAccountsRealValue;
    }
    
    public void setSelectedUserAccountsRealValue(Integer selectedUserAccountsRealValue) {
        this.selectedUserAccountsRealValue = selectedUserAccountsRealValue;
    }
    
    public Integer getSelectedLogTypesRealValue() {
        return selectedLogTypesRealValue;
    }
    
    public void setSelectedLogTypesRealValue(Integer selectedLogTypesRealValue) {
        this.selectedLogTypesRealValue = selectedLogTypesRealValue;
    }
    
    public Long getDataKey() {
        return dataKey;
    }
    
    public void setDataKey(Long dataKey) {
        this.dataKey = dataKey;
    }
}
