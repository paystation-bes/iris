package com.digitalpaytech.mvc.customeradmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.mvc.ActivityLogController;
import com.digitalpaytech.mvc.customeradmin.support.ActivityLogFilterForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
@SessionAttributes({ WebSecurityConstants.WEB_SECURITY_FORM })
public class CustomerAdminActivityLogController extends ActivityLogController{

	@RequestMapping(value = "/secure/settings/global/recentActivity.html")
	public String getGlobalRecentActivity(HttpServletResponse response, HttpServletRequest request, ModelMap model) {		
        
	    if (!WebSecurityUtil.permissionAudit(response, WebSecurityConstants.PERMISSION_VIEW_SYSTEM_ACTIVITIES)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }

        return super.getGlobalRecentActivity(response, request, model);
	}
	
/*	@RequestMapping(value = "/secure/settings/alerts/recentActivity.html")
	public String getAlertsRecentActivity(HttpServletRequest request, ModelMap model) {		
		return super.getAlertsRecentActivity(request, model);
	}
	
	@RequestMapping(value = "/secure/settings/cardSettings/recentActivity.html")
	public String getCardSettingsRecentActivity(HttpServletRequest request, ModelMap model) {
		return super.getCardSettingsRecentActivity(request, model);
	}
	
	@RequestMapping(value = "/secure/settings/locations/recentActivity.html")
	public String getLocationsRecentActivity(HttpServletRequest request, ModelMap model) {
		return super.getLocationsRecentActivity(request, model);
	}
	
	@RequestMapping(value = "/secure/settings/routes/recentActivity.html")
	public String getRoutesRecentActivity(HttpServletRequest request, ModelMap model) {
		return super.getRoutesRecentActivity(request, model);
	}
	
	@RequestMapping(value = "/secure/settings/users/recentActivity.html")
	public String getUsersRecentActivity(HttpServletRequest request, ModelMap model) {
		return super.getUsersRecentActivity(request, model);
	}*/
	
	@RequestMapping(value = "/secure/settings/getActivityLog.html")
	public @ResponseBody String getActivityLog(@ModelAttribute(WebSecurityConstants.WEB_SECURITY_FORM) WebSecurityForm<ActivityLogFilterForm> webSecurityForm,
					BindingResult result, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
		
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_SYSTEM_ACTIVITIES)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD) && 
                    !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) { 
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
            }            
            return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
        }
        
		return super.getActivityLog(webSecurityForm, result, request, response, model);
	}	
}
