package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;
import org.apache.commons.lang.StringUtils;

public class CardSettingsEditForm implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -4943169600173705053L;
    private String creditCardOfflineRetry;
	private String maxOfflineRetry;
    private String customerCardTypeRandomId;
    private Integer customerCardTypeId;
    private String cardTypeName;
    private String track2Pattern;
    private String authorizationMethod;
    private Integer authorizeTypeId;
    private String authorizationMethodRandomId;
    private String formType;
    private String description;
    private Integer customerId;
    
	public CardSettingsEditForm() {
		creditCardOfflineRetry = "";
		maxOfflineRetry = "";
	}

	public String getCreditCardOfflineRetry() {
		return creditCardOfflineRetry;
	}
	public void setCreditCardOfflineRetry(String creditCardOfflineRetry) {
		this.creditCardOfflineRetry = creditCardOfflineRetry;
	}
	public String getMaxOfflineRetry() {
		return maxOfflineRetry;
	}
	public void setMaxOfflineRetry(String maxOfflineRetry) {
		this.maxOfflineRetry = maxOfflineRetry;
	}
    public String getCardTypeName() {
        return cardTypeName;
    }
    public void setCardTypeName(String cardTypeName) {
        this.cardTypeName = cardTypeName;
    }
    public String getTrack2Pattern() {
        return track2Pattern;
    }
    public void setTrack2Pattern(String track2Pattern) {
        this.track2Pattern = track2Pattern;
    }
    public String getAuthorizationMethod() {
        return authorizationMethod;
    }
    public void setAuthorizationMethod(String authorizationMethod) {
        this.authorizationMethod = authorizationMethod;
    }
    public String getCustomerCardTypeRandomId() {
        return customerCardTypeRandomId;
    }
    public void setCustomerCardTypeRandomId(String customerCardTypeRandomId) {
        this.customerCardTypeRandomId = customerCardTypeRandomId;
    }
    public String getAuthorizationMethodRandomId() {
        return authorizationMethodRandomId;
    }
    public void setAuthorizationMethodRandomId(String authorizationMethodRandomId) {
        this.authorizationMethodRandomId = authorizationMethodRandomId;
    }
    public String getFormType() {
        return formType;
    }
    public void setFormType(String formType) {
        this.formType = formType;
    }
    public Integer getCustomerId() {
        return customerId;
    }
    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public boolean isAllEmpty() {
        if (StringUtils.isBlank(creditCardOfflineRetry) && StringUtils.isBlank(maxOfflineRetry)
            && StringUtils.isBlank(customerCardTypeRandomId) && StringUtils.isBlank(cardTypeName)
            && StringUtils.isBlank(track2Pattern) && StringUtils.isBlank(authorizationMethod)) {
            return true;
        }
        return false;
    }

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getCustomerCardTypeId() {
		return customerCardTypeId;
	}

	public void setCustomerCardTypeId(Integer customerCardTypeId) {
		this.customerCardTypeId = customerCardTypeId;
	}

	public Integer getAuthorizeTypeId() {
		return authorizeTypeId;
	}

	public void setAuthorizeTypeId(Integer authorizeTypeId) {
		this.authorizeTypeId = authorizeTypeId;
	}
}
