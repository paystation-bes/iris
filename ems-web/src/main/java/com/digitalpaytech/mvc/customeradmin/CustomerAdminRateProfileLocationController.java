package com.digitalpaytech.mvc.customeradmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.mvc.RateProfileLocationController;
import com.digitalpaytech.mvc.support.RateProfileLocationForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
public class CustomerAdminRateProfileLocationController extends RateProfileLocationController {
    @RequestMapping(value = "/secure/settings/rates/listLocations.html", method = RequestMethod.GET)
    @ResponseBody
    public final String listByRateProfile(final HttpServletRequest request, final HttpServletResponse response,
        @RequestParam("rateProfileRandomId") final String rateProfileRandomId) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.listByRateProfileSuper(request, response, rateProfileRandomId);
    }
    
    @RequestMapping(value = "/secure/settings/rates/listProfilesAssignedToLocation.html", method = RequestMethod.GET)
    @ResponseBody
    public final String listByLocation(final HttpServletRequest request, final HttpServletResponse response,
        @RequestParam("locationRandomId") final String locationRandomId) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.listByLocationSuper(request, response, locationRandomId);
    }
    
    @RequestMapping(value = "/secure/settings/rates/deleteLocation.html", method = RequestMethod.GET)
    @ResponseBody
    public final String deleteFromRateProfile(final HttpServletRequest request, final HttpServletResponse response,
        @RequestParam("rateProfileRandomId") final String rateProfileRandomId, @RequestParam("randomId") final String randomId) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.deleteFromRateProfileSuper(request, response, rateProfileRandomId, randomId);
    }
    
    @RequestMapping(value = "/secure/settings/rates/deleteProfileFromLocation.html", method = RequestMethod.GET)
    @ResponseBody
    public final String deleteFromLocation(final HttpServletRequest request, final HttpServletResponse response,
        @RequestParam("locationRandomId") final String randomId, @RequestParam("randomId") final String rateProfileLocationRandomId) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.deleteFromLocationSuper(request, response, randomId, rateProfileLocationRandomId);
    }
    
    @RequestMapping(value = "/secure/settings/rates/saveLocation.html", method = RequestMethod.POST)
    @ResponseBody
    public final String saveToRateProfile(final HttpServletRequest request, final HttpServletResponse response,
        @ModelAttribute(FORM_EDIT_PROFILE) final WebSecurityForm<RateProfileLocationForm> webSecurityForm, final BindingResult result) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.saveToRateProfileSuper(request, response, webSecurityForm, result);
    }
    
    @RequestMapping(value = "/secure/settings/rates/assignProfileToLocation.html", method = RequestMethod.POST)
    @ResponseBody
    public final String saveToLocation(final HttpServletRequest request, final HttpServletResponse response,
        @ModelAttribute(FORM_EDIT_LOCATION) final WebSecurityForm<RateProfileLocationForm> webSecurityForm, final BindingResult result) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.saveToLocationSuper(request, response, webSecurityForm, result);
    }
    
    @RequestMapping(value = "/secure/settings/rates/saveAssignedProfile.html", method = RequestMethod.GET)
    @ResponseBody
    public final String saveAssignedToLocation(final HttpServletRequest request, final HttpServletResponse response,
        @RequestParam("locationRandomId") final String locationRandomId) {
        if (!checkPermissions(true)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.saveAssignedToLocationSuper(request, response, locationRandomId);
    }
    
    private boolean checkPermissions(final boolean isManageOnly) {
        if (!WebSecurityUtil.hasValidCustomerSubscription(WebCoreConstants.SUBSCRIPTION_TYPE_ONLINE_RATE_CONFIGURATION)) {
            return false;
        }
        
        if (isManageOnly) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_RATES)) {
                return false;
            }
        } else {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_RATES)
                && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_RATES)) {
                return false;
            }
        }
        return true;
    }
}
