package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;
import java.util.List;

import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.ReportUIFilterType;
import com.digitalpaytech.dto.customeradmin.LocationTree;

public class ReportFilter {
	private List<ReportUIFilterType> primaryDateFilters;
    private List<ReportUIFilterType> secondaryDateFilters;
    private List<ReportUIFilterType> spaceNumberFilters;
    private List<ReportUIFilterType> locationTypeFilters;
    private List<ReportUIFilterType> cardNumberFilters;
    private List<ReportUIFilterType> approvalStatusFilters;
    private List<ReportUIFilterType> ticketNumberFilters;
    private List<ReportUIFilterType> couponNumberFilters;
    private List<ReportUIFilterType> couponTypeFilters;
    private List<ReportUIFilterType> otherParameterFilters;
    private List<ReportUIFilterType> groupByFilters;
    private List<ReportUIFilterType> sortByFilters;
    private List<ReportUIFilterType> repeatTypeFilters;
    
    private LocationTree organizationTree;
    private LocationTree locationPOSTree;
    private LocationTree routePOSTree;
    
    private List<FilterDTO> merchantAccountListFilters;
    private List<FilterDTO> cardTypeListFilters;
    private List<FilterDTO> severityTypeListFilters;
    private List<FilterDTO> deviceTypeListFilters;
    
    public List<ReportUIFilterType> getPrimaryDateFilters() {
        return primaryDateFilters;
    }
    
    public void setPrimaryDateFilters(List<ReportUIFilterType> primaryDateFilters) {
        this.primaryDateFilters = primaryDateFilters;
    }
    
    public List<ReportUIFilterType> getSecondaryDateFilters() {
        return secondaryDateFilters;
    }
    
    public void setSecondaryDateFilters(List<ReportUIFilterType> secondaryDateFilters) {
        this.secondaryDateFilters = secondaryDateFilters;
    }
    
    public List<ReportUIFilterType> getSpaceNumberFilters() {
        return spaceNumberFilters;
    }
    
    public void setSpaceNumberFilters(List<ReportUIFilterType> spaceNumberFilters) {
        this.spaceNumberFilters = spaceNumberFilters;
    }
    
    public List<ReportUIFilterType> getLocationTypeFilters() {
        return locationTypeFilters;
    }
    
    public void setLocationTypeFilters(List<ReportUIFilterType> locationTypeFilters) {
        this.locationTypeFilters = locationTypeFilters;
    }
    
    public List<ReportUIFilterType> getCardNumberFilters() {
        return cardNumberFilters;
    }
    
    public void setCardNumberFilters(List<ReportUIFilterType> cardNumberFilters) {
        this.cardNumberFilters = cardNumberFilters;
    }
    
    public List<ReportUIFilterType> getApprovalStatusFilters() {
        return approvalStatusFilters;
    }
    
    public void setApprovalStatusFilters(List<ReportUIFilterType> approvalStatusFilters) {
        this.approvalStatusFilters = approvalStatusFilters;
    }
    
    public List<ReportUIFilterType> getTicketNumberFilters() {
        return ticketNumberFilters;
    }
    
    public void setTicketNumberFilters(List<ReportUIFilterType> ticketNumberFilters) {
        this.ticketNumberFilters = ticketNumberFilters;
    }
    
    public List<ReportUIFilterType> getCouponNumberFilters() {
        return couponNumberFilters;
    }
    
    public void setCouponNumberFilters(List<ReportUIFilterType> couponNumberFilters) {
        this.couponNumberFilters = couponNumberFilters;
    }
    
    public List<ReportUIFilterType> getOtherParameterFilters() {
        return otherParameterFilters;
    }
    
    public void setOtherParameterFilters(List<ReportUIFilterType> otherParameterFilters) {
        this.otherParameterFilters = otherParameterFilters;
    }
    
    public List<ReportUIFilterType> getGroupByFilters() {
        return groupByFilters;
    }
    
    public void setGroupByFilters(List<ReportUIFilterType> groupByFilters) {
        this.groupByFilters = groupByFilters;
    }
    
    public List<ReportUIFilterType> getSortByFilters() {
        return sortByFilters;
    }
    
    public void setSortByFilters(List<ReportUIFilterType> sortByFilters) {
        this.sortByFilters = sortByFilters;
    }
    
    public List<FilterDTO> getMerchantAccountListFilters() {
        return merchantAccountListFilters;
    }
    
    public void setMerchantAccountListFilters(List<FilterDTO> merchantAccountListFilters) {
        this.merchantAccountListFilters = merchantAccountListFilters;
    }
    
    public List<FilterDTO> getCardTypeListFilters() {
        return cardTypeListFilters;
    }
    
    public void setCardTypeListFilters(List<FilterDTO> cardTypeListFilters) {
        this.cardTypeListFilters = cardTypeListFilters;
    }
    
    public List<ReportUIFilterType> getRepeatTypeFilters() {
        return repeatTypeFilters;
    }
    
    public void setRepeatTypeFilters(List<ReportUIFilterType> repeatTypeFilters) {
        this.repeatTypeFilters = repeatTypeFilters;
    }
    
    public List<ReportUIFilterType> getCouponTypeFilters() {
        return couponTypeFilters;
    }

    public void setCouponTypeFilters(List<ReportUIFilterType> couponTypeFilters) {
        this.couponTypeFilters = couponTypeFilters;
    }
    
    public LocationTree getOrganizationTree() {
		return organizationTree;
	}

	public void setOrganizationTree(LocationTree organizationTree) {
		this.organizationTree = organizationTree;
	}
    
    public LocationTree getLocationPOSTree() {
		return locationPOSTree;
	}

	public void setLocationPOSTree(LocationTree locationPOSTree) {
		this.locationPOSTree = locationPOSTree;
	}

	public LocationTree getRoutePOSTree() {
		return routePOSTree;
	}

	public void setRoutePOSTree(LocationTree routePOSTree) {
		this.routePOSTree = routePOSTree;
	}
	
	public List<FilterDTO> getSeverityTypeListFilters() {
		return severityTypeListFilters;
	}

	public void setSeverityTypeListFilters(List<FilterDTO> severityTypeListFilters) {
		this.severityTypeListFilters = severityTypeListFilters;
	}

	public List<FilterDTO> getDeviceTypeListFilters() {
		return deviceTypeListFilters;
	}

	public void setDeviceTypeListFilters(List<FilterDTO> deviceTypeListFilters) {
		this.deviceTypeListFilters = deviceTypeListFilters;
	}
	
	public static class ReportFormUIEntry implements Serializable {
		private static final long serialVersionUID = 8084931767879982763L;
		
		private String randomId;
        private String text;
        private boolean isSelected;
        private boolean merchantAcctIsDeleted;
        
        public ReportFormUIEntry() {
        }
        
        public ReportFormUIEntry(String randomId, String text, boolean isSelected) {
            this.randomId = randomId;
            this.text = text;
            this.isSelected = isSelected;
            this.merchantAcctIsDeleted = false;
        }        
        
        public ReportFormUIEntry(String randomId, String text, boolean isSelected, boolean isDeleted) {
            this.randomId = randomId;
            this.text = text;
            this.isSelected = isSelected;
            this.merchantAcctIsDeleted = isDeleted;
        }
        
        public String getRandomId() {
            return randomId;
        }
        
        public void setRandomId(String randomId) {
            this.randomId = randomId;
        }
        
        public String getText() {
            return text;
        }
        
        public void setText(String text) {
            this.text = text;
        }
        
        public boolean isSelected() {
            return isSelected;
        }
        
        public void setSelected(boolean isSelected) {
            this.isSelected = isSelected;
        }

		public boolean getMerchantAcctIsDeleted() {
			return merchantAcctIsDeleted;
		}

		public void setMerchantAcctIsDeleted(boolean merchantAcctIsDeleted) {
			this.merchantAcctIsDeleted = merchantAcctIsDeleted;
		}
    }
}
