package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;


public class LocationLineGeopointEditForm implements Serializable {
    private static final long serialVersionUID = 7704591797708561021L;
    
    private String locationRandomId;
    private String jsonString;
    
    public final String getLocationRandomId() {
        return this.locationRandomId;
    }

    public final void setLocationRandomId(final String locationRandomId) {
        this.locationRandomId = locationRandomId;
    }
    
    public final String getJsonString() {
        return this.jsonString;
    }

    public final void setJsonString(final String jsonString) {
        this.jsonString = jsonString;
    }
    
}
