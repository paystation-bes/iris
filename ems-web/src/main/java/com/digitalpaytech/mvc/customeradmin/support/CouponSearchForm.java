package com.digitalpaytech.mvc.customeradmin.support;

import java.io.Serializable;

import com.digitalpaytech.constants.SortOrder;

public class CouponSearchForm implements Serializable {
	private static final long serialVersionUID = 706351577428800367L;
	
	public enum SortColumn {
	    //TODO: Mos, is there a reason why "couponAsInt" is used for ordering?
//		CODE("couponAsInt", "coupon"),
	    CODE("coupon"),
		DESCRIPTION("description"),
		DISCOUNT("percentDiscount", "dollarDiscountAmount"),
		START_DATE("startDateLocal"),
		END_DATE("endDateLocal"),
		REMAINING("numberOfUsesRemaining"),
		DAILY_SINGLE_USE("validForNumOfDay"),
		OFFLINE("isOffline")
		;
		
		private String[] actualAttribute;
		
		SortColumn(String... actualAttribute) {
			this.actualAttribute = actualAttribute;
		}
		
		public String[] getActualAttribute() {
			return actualAttribute;
		}
	}
	
	public enum Status {
		ALL,
		ACTIVE,
		EXPIRED
	}
	
	private String filterName;
	private String discount;
	private Status status;
	
	private Integer page;
	private Integer itemsPerPage;
	private SortColumn column;
	private SortOrder order;
	private boolean showOnlySelectedCoupons;
	private boolean quickAdd;
	public boolean isQuickAdd() {
		return quickAdd;
	}

	public void setQuickAdd(boolean quickAdd) {
		this.quickAdd = quickAdd;
	}

	private String couponRandomIds;
	private String dataKey;
	
	private String targetRandomId;
	
	private String ignoreConsumerRandomId;
	private String ignoreRandomIds;
	
	public CouponSearchForm() {
		
	}
	
	public String getFilterName() {
		return filterName;
	}

	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public SortColumn getColumn() {
		return column;
	}

	public void setColumn(SortColumn column) {
		this.column = column;
	}

	public SortOrder getOrder() {
		return order;
	}

	public void setOrder(SortOrder order) {
		this.order = order;
	}

	public Integer getItemsPerPage() {
		return itemsPerPage;
	}

	public void setItemsPerPage(Integer itemsPerPage) {
		this.itemsPerPage = itemsPerPage;
	}

	public String getCouponRandomIds() {
		return couponRandomIds;
	}

	public void setCouponRandomIds(String couponRandomIds) {
		this.couponRandomIds = couponRandomIds;
	}

	public String getDataKey() {
		return dataKey;
	}

	public void setDataKey(String dataKey) {
		this.dataKey = dataKey;
	}

	public boolean isShowOnlySelectedCoupons() {
		return showOnlySelectedCoupons;
	}

	public void setShowOnlySelectedCoupons(boolean showOnlySelectedCoupons) {
		this.showOnlySelectedCoupons = showOnlySelectedCoupons;
	}

	public String getTargetRandomId() {
		return targetRandomId;
	}

	public void setTargetRandomId(String targetRandomId) {
		this.targetRandomId = targetRandomId;
	}

	public String getIgnoreRandomIds() {
		return ignoreRandomIds;
	}

	public void setIgnoreRandomIds(String ignoreRandomIds) {
		this.ignoreRandomIds = ignoreRandomIds;
	}

	public String getIgnoreConsumerRandomId() {
		return ignoreConsumerRandomId;
	}

	public void setIgnoreConsumerRandomId(String ignoreConsumerRandomId) {
		this.ignoreConsumerRandomId = ignoreConsumerRandomId;
	}

	public String getDiscount() {
		return discount;
	}

	public void setDiscount(String discount) {
		this.discount = discount;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
}
