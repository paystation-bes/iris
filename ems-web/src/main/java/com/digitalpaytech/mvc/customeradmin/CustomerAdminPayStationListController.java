package com.digitalpaytech.mvc.customeradmin;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.client.dto.fms.DeviceUser;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosStatus;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.PaginatedList;
import com.digitalpaytech.dto.PayStationSelectorDTO;
import com.digitalpaytech.dto.PointOfSaleSearchCriteria;
import com.digitalpaytech.dto.customeradmin.PaystationListInfo;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.mvc.PayStationListController;
import com.digitalpaytech.mvc.customeradmin.support.CustomerAdminPayStationEditForm;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.PointOfSaleSearchForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebPermissionUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.customeradmin.CustomerAdminPayStationValidator;

@Controller
@SessionAttributes({ "payStationEditForm" })
public class CustomerAdminPayStationListController extends PayStationListController {
    private static final Logger LOG = Logger.getLogger(CustomerAdminPayStationListController.class);
    
    @Autowired
    protected CustomerAdminPayStationValidator customerAdminPayStationValidator;
    
    public final void setCustomerAdminPayStationValidator(final CustomerAdminPayStationValidator customerAdminPayStationValidator) {
        this.customerAdminPayStationValidator = customerAdminPayStationValidator;
    }
    
    /**
     * This method creates a list of Id's of routes and locations which have pay
     * stations for the current user and puts them into the model as
     * "filterValues" to be used in the UI to filter pay stations
     * 
     * @param request
     * @param response
     * @param model
     * @return List of locations and routes stored in the model as attribute
     *         "filterValues" used for filtering pay stations. Target vm file
     *         URI which is mapped to Settings->pay station list form
     *         (/settings/locations/paystationList.vm)
     */
    @RequestMapping(value = "/secure/settings/locations/payStationList.html")
    public String payStationList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                if (WebPermissionUtil.hasCustomerSettingsConfigGroupView()) {
                    return "redirect:/secure/settings/payStations/configGroup.html";
                } else if (WebPermissionUtil.hasCustomerSettingsPayStationSettings()) {
                    return "redirect:/secure/settings/locations/payStationSettings.html";
                } else if (WebPermissionUtil.hasCustomerSettingsPayStationPlacementPermissions()) {
                    return "redirect:/secure/settings/locations/payStationPlacement.html";
                } else {
                    if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                        && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                        return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
                    }
                    return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
                }
            }
        }
        
        final CustomerAdminPayStationEditForm payStationEditForm = new CustomerAdminPayStationEditForm();
        final WebSecurityForm<CustomerAdminPayStationEditForm> webSecurityForm =
                new WebSecurityForm<CustomerAdminPayStationEditForm>(null, payStationEditForm);
        model.put("payStationEditForm", webSecurityForm);
        
        return super.payStationList(request, response, model, "/settings/locations/payStationList", true);
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/settings/locations/payStationList/searchPos.html", method = RequestMethod.POST)
    public final String searchPos(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        /* get customer information from security framework session. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.searchPos(request, response);
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/listPayStations.html", method = RequestMethod.GET)
    public final String listPayStations(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<PointOfSaleSearchForm> form,
        final HttpServletRequest request, final HttpServletResponse response) {
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        final PointOfSaleSearchCriteria criteria =
                super.populatePosSearchCriteria(request, form.getWrappedObject(), WebSecurityUtil.getUserAccount().getCustomer().getId(), false);
        criteria.setShowDeactivated(true);
        criteria.setShowDecommissioned(true);
        criteria.setShowHidden(true);
        
        final PaginatedList<PaystationListInfo> result = new PaginatedList<PaystationListInfo>();
        result.setElements(this.pointOfSaleService.findPaystation(criteria));
        result.setDataKey(Long.toString(criteria.getMaxUpdatedTime().getTime(), 10));
        
        return WidgetMetricsHelper.convertToJson(result, "payStationsList", true);
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/locatePageWithPayStations.html", method = RequestMethod.GET)
    public final String locatePageWithPayStation(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<PointOfSaleSearchForm> form,
        final HttpServletRequest request, final HttpServletResponse response) {
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        String resultJson = WebCoreConstants.RESPONSE_FALSE;
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final PointOfSaleSearchCriteria criteria =
                super.populatePosSearchCriteria(request, form.getWrappedObject(), WebSecurityUtil.getUserAccount().getCustomer().getId(), false);
        criteria.setShowDeactivated(true);
        criteria.setShowDecommissioned(true);
        criteria.setShowHidden(true);
        
        int targetPage = -1;
        final Integer targetId = keyMapping.getKey(form.getWrappedObject().getTargetRandomId(), PointOfSale.class, Integer.class);
        if (targetId != null) {
            criteria.setMaxUpdatedTime(new Date());
            targetPage = this.pointOfSaleService.findPayStationPage(targetId, criteria);
        }
        
        if (targetPage > 0) {
            criteria.setPage(targetPage);
            
            final PaginatedList<PaystationListInfo> result = new PaginatedList<PaystationListInfo>();
            result.setElements(this.pointOfSaleService.findPaystation(criteria));
            result.setDataKey(Long.toString(criteria.getMaxUpdatedTime().getTime(), 10));
            result.setPage(targetPage);
            
            resultJson = WidgetMetricsHelper.convertToJson(result, "payStationsList", true);
        }
        
        return resultJson;
    }
    
    @ResponseBody
    @RequestMapping(value = "/secure/payStationSelectors.html", method = RequestMethod.GET)
    public final String payStationSelectors(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<PointOfSaleSearchForm> form,
        final HttpServletRequest request, final HttpServletResponse response) {
        final PointOfSaleSearchCriteria criteria =
                super.populatePosSearchCriteria(request, form.getWrappedObject(), WebSecurityUtil.getUserAccount().getCustomer().getId(), false);
        criteria.setShowDeactivated(true);
        criteria.setShowDecommissioned(false);
        criteria.setShowHidden(false);
        final PaginatedList<PayStationSelectorDTO> result = new PaginatedList<PayStationSelectorDTO>();
        result.setElements(this.pointOfSaleService.findPayStationSelector(criteria));
        result.setDataKey(Long.toString(criteria.getMaxUpdatedTime().getTime(), 10));
        
        return WidgetMetricsHelper.convertToJson(result, "payStationsList", true);
    }
    
    /**
     * This method takes a pay station id as input and returns map information
     * and payStationDetails.vm
     * 
     * @param request
     * @param response
     * @param model
     * @return A MapEntry object with the requested payStationId's coordinates
     *         in the model as attribute "mapInfo". Target vm file URI which is
     *         mapped to Settings->payStationDetails
     *         (/settings/location/payStationDetails.vm)
     */
    @ResponseBody
    @RequestMapping(value = "/secure/settings/locations/payStationList/payStationDetails.html")
    public String payStationDetailPage(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
                if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)
                    && !WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                    return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
                }
                return WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_DASHBOARD;
            }
        }
        
        return super.payStationDetailPageSuper(request, response, model);
    }
    
    /**
     * Saves changes made to a pay station using the passed model attribute
     * "payStationEditForm" WebSecurityForm
     * 
     * @param webSecurityForm
     * @param result
     * @param request
     * @param response
     * @param model
     * @return "true" string value if process succeeds, JSON error message if
     *         process fails
     */
    @ResponseBody
    @RequestMapping(value = "/secure/settings/locations/payStationList/savePayStation.html", method = RequestMethod.POST)
    public String savePayStation(@ModelAttribute("payStationEditForm") final WebSecurityForm<CustomerAdminPayStationEditForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        /* validate user account if it has proper role to manage pay stations. */
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        if (!this.customerAdminPayStationValidator.validateMigrationStatus(webSecurityForm)) {
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        /* validate user input. */
        this.customerAdminPayStationValidator.validate(webSecurityForm, result, keyMapping);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        webSecurityForm.resetToken();
        
        final CustomerAdminPayStationEditForm form = webSecurityForm.getWrappedObject();
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(form.getPointOfSaleId());
        final Location location = this.locationService.findLocationById(form.getLocationId());
        
        if (pointOfSale == null || location == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return null;
        }
        
        final PosStatus pointOfSaleStatus = this.pointOfSaleService.findPointOfSaleStatusByPOSId(pointOfSale.getId(), true);
        
        final Boolean visible = !form.getHidden();
        final Boolean isActive = form.getActive();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        if (pointOfSaleStatus.isIsActivated() != isActive || pointOfSaleStatus.isIsVisible() != visible) {
            
            if (pointOfSale.isIsLinux() != null && pointOfSale.isIsLinux().booleanValue()) {
                // If a pay station is activated/deactivated update user status in Link.
                final Integer unifiId = super.customerAdminService.findCustomerByCustomerId(userAccount.getCustomer().getId()).getUnifiId();
                
                boolean hasUpdated;
                try {
                    hasUpdated =
                            super.systemAdminService.updateUserInMitreId(pointOfSale, isActive, new DeviceUser(pointOfSale.getSerialNumber(), 
                                unifiId, isActive ? WebCoreConstants.TERMINAL_ENABLED : WebCoreConstants.TERMINAL_DISABLED, 
                                WebCoreConstants.PAY_STATION));
                } catch (InvalidDataException ide) {
                    LOG.error("Failed to update device on Link", ide);
                    hasUpdated = false;
                }
                if (!hasUpdated) {
                    return errorMessage(webSecurityForm.getInitToken(), ErrorConstants.MS_COMM_FAILURE);
                }
            }
            
            final Date now = DateUtil.getCurrentGmtDate();
            if (pointOfSaleStatus.isIsActivated() != isActive) {
                pointOfSaleStatus.setIsActivated(isActive);
                pointOfSaleStatus.setIsActivatedGmt(now);
                super.insertNewPosDate(pointOfSale, WebCoreConstants.POS_DATE_TYPE_ACTIVATED, userAccount, isActive, null);
                if (pointOfSaleStatus.isIsBillableMonthlyOnActivation()) {
                    pointOfSaleStatus.setIsBillableMonthlyOnActivation(isActive);
                    pointOfSaleStatus.setIsBillableMonthlyOnActivationGmt(now);
                    super.insertNewPosDate(pointOfSale, WebCoreConstants.POS_DATE_TYPE_BILLABLE_MONTHLY_ON_ACTIVATION, userAccount, isActive, null);
                }
                
                resetAlerts(isActive, pointOfSale.getId(), userAccount, false);
            }
            if (pointOfSaleStatus.isIsVisible() != visible) {
                pointOfSaleStatus.setIsVisible(visible);
                pointOfSaleStatus.setIsVisibleGmt(now);
                super.insertNewPosDate(pointOfSale, WebCoreConstants.POS_DATE_TYPE_VISIBLE, userAccount, visible, null);
            }
            this.customerAdminService.saveOrUpdatePointOfSaleStatus(pointOfSaleStatus);
            
        }
        
        pointOfSale.setName(form.getName().trim());
        pointOfSale.setLocation(location);
        
        this.customerAdminService.saveOrUpdatePointOfSale(pointOfSale);
        
        return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(":").append(webSecurityForm.getInitToken()).toString();
    }
    
    private String errorMessage(final String token, final String messageKey) {
        final MessageInfo message = new MessageInfo();
        message.setError(true);
        message.setToken(token);
        message.addMessage(this.commonControllerHelper.getMessage(messageKey));
        return WidgetMetricsHelper.convertToJson(message, WebCoreConstants.UI_MESSAGES, true);
    }
}
