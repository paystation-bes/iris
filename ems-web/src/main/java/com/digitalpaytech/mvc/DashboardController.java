package com.digitalpaytech.mvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Section;
import com.digitalpaytech.domain.Tab;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetSubsetMember;
import com.digitalpaytech.dto.TabHeading;
import com.digitalpaytech.dto.TabsGroup;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.util.DashboardUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.WidgetConstants;

@Controller
public class DashboardController extends CommonLandingController {
    
    private static Logger log = Logger.getLogger(DashboardController.class);
    
    @Autowired
    protected EntityService entityService;
    @Autowired
    protected UserAccountService userAccountService;
    
    public void setEntityService(EntityService entityService) {
        this.entityService = entityService;
    }
    
    public void setUserAccountService(UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }
    
    //========================================================================================================================
    // Dashboard
    //========================================================================================================================
    @RequestMapping(value = "/secure/dashboard/editDashboard.html")
    public @ResponseBody String editDashboard(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        SessionTool.getInstance(request).getKeyMapping();
        if (SessionTool.getInstance(request).getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP) == null) {
            Map<String, Tab> map = new ConcurrentHashMap<String, Tab>();
            SessionTool.getInstance(request).setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, map);
        }
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    @RequestMapping(value = "/secure/dashboard/saveDashboard.html")
    public @ResponseBody String saveDashboard(HttpServletRequest request, HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        SessionTool sessionTool = SessionTool.getInstance(request);
        
        @SuppressWarnings("unchecked")
        Map<String, Tab> tabTreeMap = (Map<String, Tab>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        @SuppressWarnings("unchecked")
        List<TabHeading> tabHeadings = (List<TabHeading>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_HEADINGS);
        
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        WebUser webUser = (WebUser) authentication.getPrincipal();
        UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        int tabId = -1;
        
        //if the user has defaultdashboard, make sure all default tabs are in the map with the users ID
        if (userAccount.getIsDefaultDashboard()) {
            
            for (Entry<String, Tab> entry : tabTreeMap.entrySet()) {
                Tab tab = entry.getValue();
                if (tab.isToBeDeleted()) {
                    tab.setId(null);
                }
            }
            
            for (TabHeading tabHeading : tabHeadings) {
                if (!tabTreeMap.containsKey(tabHeading.getId())) {
                    //tab is not in the map
                    tabId = Integer.parseInt(keyMapping.getKey(tabHeading.getId()).toString());
                    Tab tab = dashboardService.findTabByIdEager(tabId);
                    
                    tab = DashboardUtil.setRandomIds(request, tab);
                    if (tab.getUserAccount().getId() == WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID) {
                        tab = removeMasterList(request, tab, userAccount.getCustomer().isIsParent());
                    }
                    
                    tab.setId(null);
                    
                    tab.setUserAccount(userAccount);
                    for (Section section : tab.getSections()) {
                        for (Widget widget : section.getWidgets()) {
                            widget.setId(null);
                            widget.setWidgetListType(dashboardService.getWidgetListTypeById(WidgetConstants.LIST_TYPE_USER));
                            widget.setUserAccountId(userAccount.getId());
                            widget.setCustomerId(webUser.getCustomerId());
                            if (userAccount.getCustomer().getParentCustomer() != null) {
                                widget.setParentCustomerId(userAccount.getCustomer().getParentCustomer().getId());
                            } else {
                                widget.setParentCustomerId(new Integer(0));
                            }
                            for (WidgetSubsetMember subsetMember : widget.getWidgetSubsetMembers()) {
                                this.entityService.evict(subsetMember);
                                subsetMember.setId(null);
                                subsetMember.setWidget(widget);
                            }
                        }
                        section.setId(null);
                        section.setUserAccountId(userAccount.getId());
                    }
                    
                    tabTreeMap.put(tabHeading.getId(), tab);
                } else {
                    //tab is already in the map, 
                    Tab tab = tabTreeMap.get(tabHeading.getId());
                    
                    tab = removeMasterList(request, tab, userAccount.getCustomer().isIsParent());
                    tab.setId(null);
                    
                    tab.setUserAccount(userAccount);
                    for (Section section : tab.getSections()) {
                        for (Widget widget : section.getWidgets()) {
                            widget.setId(null);
                            widget.setWidgetListType(dashboardService.getWidgetListTypeById(WidgetConstants.LIST_TYPE_USER));
                            widget.setUserAccountId(userAccount.getId());
                            widget.setCustomerId(webUser.getCustomerId());
                            if (userAccount.getCustomer().getParentCustomer() != null) {
                                widget.setParentCustomerId(userAccount.getCustomer().getParentCustomer().getId());
                            } else {
                                widget.setParentCustomerId(new Integer(0));
                            }
                            for (WidgetSubsetMember subsetMember : widget.getWidgetSubsetMembers()) {
                                this.entityService.evict(subsetMember);
                                subsetMember.setId(null);
                                subsetMember.setWidget(widget);
                            }
                        }
                        section.setId(null);
                        section.setUserAccountId(userAccount.getId());
                    }
                }
            }
        }
        
        //        if(tabId == -1) { // ToDo: Remove this if statement and integrate with existing logic 
        //            tabId = 0;
        //            for (TabHeading tabHeading : tabHeadings) {
        //                if (tabHeading.getCurrent().equals(WebCoreConstants.RESPONSE_TRUE) && tabHeading.getOriginalId() != null) {
        //                    tabId = tabHeading.getOriginalId();
        //                    break;
        //                }
        //            }
        //        }
        
        //        int index = 0, currentTabIndex = 0;
        //        for (TabHeading tabHeading : tabHeadings) {
        //            if (tabHeading.getCurrent().equals(WebCoreConstants.RESPONSE_TRUE)) {
        //                tabHeading.setCurrent(WebCoreConstants.RESPONSE_FALSE);
        //                currentTabIndex = index;
        //                break;
        //            }
        ////            if (tabHeading.getOriginalId() != null && tabHeading.getOriginalId().intValue() == tabId) {
        ////                tabHeading.setCurrent(WebCoreConstants.RESPONSE_TRUE);
        ////                currentTabIndex = index;
        ////            }
        //            index++;
        //        }
        
        //        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, tabHeadings);
        //        DashboardUtil.setCurrentTabCookie(request, response, currentTabIndex); 
        
        @SuppressWarnings("unchecked")
        TabsGroup tabsGroup = new TabsGroup(tabTreeMap, (List<Integer>) sessionTool.getAttribute(WebCoreConstants.SESSION_SECTION_IDS_TO_BE_DELETED),
                (List<Integer>) sessionTool.getAttribute(WebCoreConstants.SESSION_WIDGET_IDS_TO_BE_DELETED),
                (List<Integer>) sessionTool.getAttribute(WebCoreConstants.SESSION_SUBSET_MEMBER_IDS_TO_BE_DELETED), userAccount);
        dashboardService.saveTabs(tabsGroup, userAccount);
        
        reloadDashboard(request, response);
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    @RequestMapping(value = "/secure/dashboard/cancelDashboard.html")
    public @ResponseBody String reloadDashboard(HttpServletRequest request, HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        SessionTool sessionTool = SessionTool.getInstance(request);
        
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        @SuppressWarnings("unchecked")
        Map<String, Tab> tabTreeMap = (Map<String, Tab>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        @SuppressWarnings("unchecked")
        List<TabHeading> tabHeadings = (List<TabHeading>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_HEADINGS);
        
        //find tabHeading which is current
        int id = WebCoreConstants.NEW_TAB_ID;
        for (TabHeading tabHeading : tabHeadings) {
            if (tabHeading.getCurrent().equals(WebCoreConstants.RESPONSE_TRUE) && tabHeading.getOriginalId() != null) {
                id = tabHeading.getOriginalId();
                break;
            }
        }
        
        //        keyMapping.clear();
        tabTreeMap.clear();
        
        sessionTool.removeAttribute(WebCoreConstants.SESSION_SECTION_IDS_TO_BE_DELETED);
        sessionTool.removeAttribute(WebCoreConstants.SESSION_WIDGET_IDS_TO_BE_DELETED);
        sessionTool.removeAttribute(WebCoreConstants.SESSION_SUBSET_MEMBER_IDS_TO_BE_DELETED);
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        WebUser webUser = (WebUser) authentication.getPrincipal();
        
        tabHeadings = getTabs(request, webUser.getUserAccountId());
        
        int currentTabIndex = 0;
        int index = 0;
        for (TabHeading tabHeading : tabHeadings) {
            if (tabHeading.getCurrent().equals(WebCoreConstants.RESPONSE_TRUE)) {
                if (id == WebCoreConstants.NEW_TAB_ID) {
                    currentTabIndex = index;
                } else {
                    tabHeading.setCurrent(WebCoreConstants.RESPONSE_FALSE);
                }
            }
            if (tabHeading.getOriginalId() == id) {
                tabHeading.setCurrent(WebCoreConstants.RESPONSE_TRUE);
                currentTabIndex = index;
            }
            index++;
        }
        
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, tabTreeMap);
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, tabHeadings);
        DashboardUtil.setCurrentTabCookie(request, response, currentTabIndex);
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    @RequestMapping(value = "/secure/dashboard/resetDashboard.html")
    public @ResponseBody String resetDashboard(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        SessionTool sessionTool = SessionTool.getInstance(request);
        
        @SuppressWarnings("unchecked")
        Map<String, Tab> tabTreeMap = (Map<String, Tab>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        @SuppressWarnings("unchecked")
        List<TabHeading> tabHeadings = (List<TabHeading>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_HEADINGS);
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        //if user isDefaultDashboard == true, return true
        if (WebSecurityUtil.getUserAccount().getIsDefaultDashboard()) {
            reloadDashboard(request, response);
            return WebCoreConstants.RESPONSE_TRUE;
        }
        
        if (tabTreeMap != null) {
            tabTreeMap.clear();
        }
        
        tabHeadings.clear();
        
        //retrieve all users tabs from database
        
        //delete all tab trees starting at the widget level		
        dashboardService.deleteTabsByUserId(userAccount.getId());
        userAccount.setLastModifiedByUserId(userAccount.getId());
        userAccount.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        userAccount.setIsDefaultDashboard(true);
        userAccountService.updateUserAccount(userAccount);
        
        // fill tabHeadings with default tabs
        tabHeadings = getTabs(request, userAccount.getId());
        
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, tabTreeMap);
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, tabHeadings);
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    @RequestMapping(value = "/secure/dashboard/setCurrentTab.html")
    public @ResponseBody String setCurrentTab(HttpServletRequest request, HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        SessionTool sessionTool = SessionTool.getInstance(request);
        
        @SuppressWarnings("unchecked")
        List<TabHeading> tabHeadings = (List<TabHeading>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_HEADINGS);
        String tabId = request.getParameter("tabID");
        
        if (!DashboardUtil.validateRandomId(tabId)) {
            log.warn("### Invalid tabID in the request. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        if ((tabId != null) && (tabId.trim().length() > 0)) {
            int index = 0, currentTabIndex = 0;
            for (TabHeading tabHeading : tabHeadings) {
                if (tabHeading.getCurrent().equals(WebCoreConstants.RESPONSE_TRUE)) {
                    tabHeading.setCurrent(WebCoreConstants.RESPONSE_FALSE);
                }
                if (tabHeading.getId().equals(tabId)) {
                    tabHeading.setCurrent(WebCoreConstants.RESPONSE_TRUE);
                    currentTabIndex = index;
                }
                index++;
            }
            
            sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, tabHeadings);
            DashboardUtil.setCurrentTabCookie(request, response, currentTabIndex);
        }
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    //========================================================================================================================
    // Tabs
    //========================================================================================================================
    
    /**
     * createNewTab and addTab are called sequentially by Velocity and it would create a default Tab and Section, place into
     * tabTreeMap, and then put 'tempID' into request.
     */
    @RequestMapping(value = "/secure/dashboard/newTab.html")
    public @ResponseBody String createNewTab(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        SessionTool sessionTool = SessionTool.getInstance(request);
        
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        @SuppressWarnings("unchecked")
        Map<String, Tab> tabTreeMap = (Map<String, Tab>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        @SuppressWarnings("unchecked")
        List<TabHeading> tabHeadings = (List<TabHeading>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_HEADINGS);
        
        if (tabTreeMap == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        //        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        //        WebUser webUser = (WebUser) authentication.getPrincipal();
        
        /* create tab and empty section */
        Tab tab = new Tab();
        int orderNumber = tabHeadings.size() + 1;
        tab.setOrderNumber(orderNumber);
        tab.setLastModifiedGmt(new Date());
        tab.setUserAccount(WebSecurityUtil.getUserAccount());
        tab.setName("Untitled");
        
        createNewSection(tab, keyMapping);
        
        tab.setRandomId(DashboardUtil.getTemporaryId(WebCoreConstants.TEMP_TAB_ID_PREFIX));
        tab.setRandomId(keyMapping.getRandomString(tab, WebCoreConstants.RANDOM_ID_LOOK_UP_NAME));
        
        /* add tabTree to map & tab to tabHeadings */
        tabTreeMap.put(tab.getRandomId(), tab);
        tabHeadings.add(new TabHeading(tab));
        
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, tabTreeMap);
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, tabHeadings);
        
        return tab.getRandomId();
    }
    
    @RequestMapping(value = "/secure/dashboard/addTab.html")
    public String addTab(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        String tempId = request.getParameter("id");
        model.put("tempID", tempId);
        
        return "/dashboard/include/addTab";
    }
    
    @RequestMapping(value = "/secure/dashboard/tabOrder.html")
    public @ResponseBody String updateTabOrder(HttpServletRequest request, ModelMap model, HttpServletResponse response) throws Exception {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        SessionTool sessionTool = SessionTool.getInstance(request);
        
        @SuppressWarnings("unchecked")
        List<TabHeading> tabHeadings = (List<TabHeading>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_HEADINGS);
        String[] tabIdList = request.getParameterValues("tabID[]");
        
        if (tabIdList.length != tabHeadings.size()) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        if (updateTabOrder(tabIdList, request, response)) {
            return WebCoreConstants.RESPONSE_TRUE;
        } else {
            return WebCoreConstants.RESPONSE_FALSE;
        }
    }
    
    @RequestMapping(value = "/secure/dashboard/editTab.html")
    public @ResponseBody String editTab(HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        String tabId = request.getParameter("tabID");
        String tabName = request.getParameter("tabName");
        
        if (tabName == null || tabName.matches(WebCoreConstants.REGEX_WHITE_SPACE_ONLY) || tabName.isEmpty()) {
            tabName = WebCoreConstants.DEFAULT_TAB_NAME;
        }
        tabName = tabName.trim();
        
        if (!DashboardUtil.validateRandomId(tabId)) {
            log.warn("### Invalid tabID in the request. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        SessionTool sessionTool = SessionTool.getInstance(request);
        
        @SuppressWarnings("unchecked")
        Map<String, Tab> tabTreeMap = (Map<String, Tab>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        @SuppressWarnings("unchecked")
        List<TabHeading> tabHeadings = (List<TabHeading>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_HEADINGS);
        
        if (tabTreeMap.containsKey(tabId)) {
            tabTreeMap.get(tabId).setName(tabName);
        } else {
            RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
            int id = Integer.parseInt(keyMapping.getKey(tabId).toString());
            
            Tab tab = dashboardService.findTabByIdEager(id);
            // Generates randomId for Tab, Sections and Widgets.
            tab = DashboardUtil.setRandomIds(request, tab);
            tab.setName(tabName);
            tabTreeMap.put(tab.getRandomId(), tab);
        }
        
        for (TabHeading heading : tabHeadings) {
            if (heading.getId().equals(tabId)) {
                heading.setName(tabName);
                break;
            }
        }
        
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, tabTreeMap);
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, tabHeadings);
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    @RequestMapping(value = "/secure/dashboard/deleteTab.html")
    public @ResponseBody String deleteTab(HttpServletRequest request, ModelMap model, HttpServletResponse response) throws Exception {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        String tabId = request.getParameter("id");
        
        if (!DashboardUtil.validateRandomId(tabId)) {
            log.warn("### Invalid tabID in the request. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        SessionTool sessionTool = SessionTool.getInstance(request);
        @SuppressWarnings("unchecked")
        Map<String, Tab> tabTreeMap = (Map<String, Tab>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        @SuppressWarnings("unchecked")
        List<TabHeading> tabHeadings = (List<TabHeading>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_HEADINGS);
        
        Tab tab = null;
        
        if (tabTreeMap.containsKey(tabId)) {
            tabTreeMap.get(tabId).setToBeDeleted(true);
            tab = tabTreeMap.get(tabId);
        } else {
            RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
            int id = Integer.parseInt(keyMapping.getKey(tabId).toString());
            
            tab = dashboardService.findTabByIdEager(id);
            tab = DashboardUtil.setRandomIds(request, tab);
            tab.setToBeDeleted(true);
            tabTreeMap.put(tab.getRandomId(), tab);
        }
        
        for (TabHeading tabHeading : tabHeadings) {
            if (tabHeading.getOriginalId().intValue() == tab.getId().intValue()) {
                tabHeadings.remove(tabHeading);
                break;
            }
        }
        List<String> tabIdList = new ArrayList<String>();
        for (TabHeading tabHeading : tabHeadings) {
            tabIdList.add(tabHeading.getId());
        }
        String[] idList = new String[tabIdList.size()];
        tabIdList.toArray(idList);
        
        updateTabOrder(idList, request, response);
        
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, tabTreeMap);
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, tabHeadings);
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    private Boolean updateTabOrder(String[] tabIdList, HttpServletRequest request, HttpServletResponse response) {
        SessionTool sessionTool = SessionTool.getInstance(request);
        
        @SuppressWarnings("unchecked")
        Map<String, Tab> tabTreeMap = (Map<String, Tab>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        @SuppressWarnings("unchecked")
        List<TabHeading> tabHeadings = (List<TabHeading>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_HEADINGS);
        
        String currentTabId = null;
        for (TabHeading tabHeading : tabHeadings) {
            if (tabHeading.getCurrent().equals("true")) {
                currentTabId = tabHeading.getId();
                break;
            }
        }
        
        tabHeadings.clear();
        
        if (tabTreeMap == null) {
            return false;
        }
        
        //go through tabIdList and rebuild tabHeadings in the new order
        for (int n = 0; n < tabIdList.length; n++) {
            
            if (!DashboardUtil.validateRandomId(tabIdList[n])) {
                log.warn("### Invalid tabID in the request. ###");
                return false;
            }
            
            if (tabTreeMap.containsKey(tabIdList[n])) {
                tabTreeMap.get(tabIdList[n]).setOrderNumber(n + 1);
                tabHeadings.add(new TabHeading(tabTreeMap.get(tabIdList[n])));
            } else {
                RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
                int id = Integer.parseInt(keyMapping.getKey(tabIdList[n]).toString());
                
                Tab tab = dashboardService.findTabByIdEager(id);
                tab.setOrderNumber(n + 1);
                tab = DashboardUtil.setRandomIds(request, tab);
                tab.setRandomId(tabIdList[n]);
                tabTreeMap.put(tab.getRandomId(), tab);
                tabHeadings.add(new TabHeading(tab));
            }
        }
        
        //reset the current tab after rebuilding tabHeadings in new order
        int currentTabIndex = 0;
        if (currentTabId == null && !tabHeadings.isEmpty()) {
            tabHeadings.get(0).setCurrent("true");
        } else {
            for (TabHeading tabHeading : tabHeadings) {
                if (tabHeading.getId().equals(currentTabId)) {
                    tabHeading.setCurrent("true");
                    break;
                }
                currentTabIndex++;
            }
        }
        
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, tabTreeMap);
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, tabHeadings);
        DashboardUtil.setCurrentTabCookie(request, response, currentTabIndex);
        return true;
    }
    
    //========================================================================================================================
    // Sections
    //========================================================================================================================	
    
    /**
     * Creates a new Section with default layout id (2) and puts into Tab's sections.
     * 
     * @param tab
     *            Tab object which would have a new Section in the list.
     * @param keyMapping
     *            RandomKeyMapping For generating a random id.
     * @return Section New Section with default settings.
     */
    private Section createNewSection(Tab tab, RandomKeyMapping keyMapping) {
        
        Section section = new Section();
        section.setUserAccountId(tab.getUserAccount().getId());
        section.setTab(tab);
        section.setLayoutId(WebCoreConstants.DEFAULT_SECTION_LAYOUT_ID);
        section.setOrderNumber(tab.getSections().size() + 1);
        section.setLastModifiedGmt(new GregorianCalendar().getTime());
        
        section.setRandomId(DashboardUtil.getTemporaryId(WebCoreConstants.TEMP_SECTION_ID_PREFIX));
        section.setRandomId(keyMapping.getRandomString(section, WebCoreConstants.RANDOM_ID_LOOK_UP_NAME));
        
        section.prepareSectionColumns();
        
        tab.addSection(section);
        
        return section;
    }
    
    /**
     * Creates a new Section and put temporary section id in response. When a new tab is called, a default section would be created as well. 'addSection' would
     * be an addition of default section. Default layoutId is 2.
     * 
     * For a New Section:
     * Ajax /secure/dashboard/addSection.html?id=<TabID> returns /secure/dashboard/include/tabSection.vm.
     */
    @RequestMapping(value = "/secure/dashboard/addSection.html", method = RequestMethod.GET)
    public String addSection(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        // Random tab id
        String tabIdStr = request.getParameter("id");
        if (StringUtils.isBlank(tabIdStr)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        // Loads tab tree into Map if it doesn't exist.
        Map<String, Tab> tabTreeMap = loadTabTreeToMapIfNecessary(request, tabIdStr);
        
        Tab tab = tabTreeMap.get(tabIdStr);
        if (tab == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        // createNewSection would create Section and assigns to Tab.
        SessionTool sessionTool = SessionTool.getInstance(request);
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        Section newSection = createNewSection(tab, keyMapping);
        tabTreeMap.put(tabIdStr, tab);
        
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, tabTreeMap);
        model.put("section", newSection);
        
        return "/dashboard/include/tabSection";
    }
    
    /**
     * Update for the sectionOrder.html the request will be sending via POST
     * tabID = "current Tab ID"
     * section[] = "Array of Section ID's in order of placement on the page"
     * 
     * Returns "true" or "false".
     * 
     * @param request
     * @return String boolean response in String format.
     */
    @RequestMapping(value = "/secure/dashboard/sectionOrder.html", method = RequestMethod.POST)
    public @ResponseBody String updateSectionOrder(HttpServletRequest request, HttpServletResponse response) {
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        String tabIdStr = request.getParameter("tabID");
        if (StringUtils.isBlank(tabIdStr)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        // section[] ="Array of Section ID's in order of placement on the page".
        List<String> secIdsList = getIdsFromRequestArrayParameters(request, "section[]");
        
        // Loads tab tree into Map if it doesn't exist.
        Map<String, Tab> tabTreeMap = loadTabTreeToMapIfNecessary(request, tabIdStr);
        Tab tab = tabTreeMap.get(tabIdStr);
        
        for (int i = 0; i < secIdsList.size(); i++) {
            tab.getSection(secIdsList.get(i)).setOrderNumber(i + 1);
        }
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    /**
     * e.g. parameter name 'colID[]', colID[] = "array of colum 'ids'", for example colID[]=1&colID[]=2 , will delete all widgets in the Second and third
     * columns.
     * parameter name 'sectionID[]', sectionID[] ="Array of Section ID's in order of placement on the page".
     * 
     * @param parameterName
     *            e.g. 'colID[]', 'sectionID[]'
     * @return List<String> ids.
     */
    protected List<String> getIdsFromRequestArrayParameters(HttpServletRequest request, String parameterName) {
        
        List<String> idsList = new ArrayList<String>();
        
        /*
         * getParameterValues returns an array of String objects containing all of the values the given request parameter has,
         * or null if the parameter does not exist.
         */
        String[] values = request.getParameterValues(parameterName);
        if (isValidIds(values)) {
            idsList.addAll(Arrays.asList(values));
        }
        return idsList;
    }
    
    private boolean isValidIds(String[] values) {
        if (values == null) {
            return false;
        }
        for (String value : values) {
            if (StringUtils.isBlank(value)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * For the editSection.html in GET request query string:
     * layoutID= "newLayoutID"
     * sectionID= "currentSectionID"
     * tabID= "currentTabID"
     * 
     * @param request
     * @return String boolean response in String format.
     */
    @RequestMapping(value = "/secure/dashboard/editSection.html", method = RequestMethod.GET)
    public @ResponseBody String editSection(HttpServletRequest request, HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        String tabIdStr = request.getParameter("tabID");
        String layoutIdStr = request.getParameter("layoutID");
        String sectionId = request.getParameter("sectionID");
        
        if (StringUtils.isBlank(tabIdStr) || StringUtils.isBlank(layoutIdStr) || StringUtils.isBlank(sectionId)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        // Returns 'false' if layoutId is not a positive number.
        if (!DashboardUtil.isPositiveInteger(layoutIdStr)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        // Loads tab tree into Map if it doesn't exist.
        Map<String, Tab> tabTreeMap = loadTabTreeToMapIfNecessary(request, tabIdStr);
        
        int layoutId = Integer.parseInt(layoutIdStr);
        Tab tab = tabTreeMap.get(tabIdStr);
        tab.getSection(sectionId).setLayoutId(layoutId);
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    /**
     * For the clearColumn.html which will delete all widgets within the specified columns. The query string will be via a Get request:
     * sectionID= "currentSectionID"
     * tabID= "currentTabID"
     * colID[] = "array of colum 'ids'" for example colID[]=1&
     * 
     * Returns "true" or "false".
     * 
     * @param request
     * @return String boolean response in String format.
     */
    @RequestMapping(value = "/secure/dashboard/clearColumn.html", method = RequestMethod.GET)
    public @ResponseBody String clearColumn(HttpServletRequest request, HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        SessionTool sessionTool = SessionTool.getInstance(request);
        
        // The current section id & tab id.
        String sectionIdStr = request.getParameter("sectionID");
        String tabIdStr = request.getParameter("tabID");
        if (StringUtils.isBlank(sectionIdStr) || StringUtils.isBlank(tabIdStr)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        // colID[] = "array of colum 'ids'", for example colID[]=1&colID[]=2 , will delete all widgets in the Second and third columns.
        List<String> colIdsList = getIdsFromRequestArrayParameters(request, "colID[]");
        
        // There is no column to be deleted.
        if (colIdsList.isEmpty()) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        // Loads tab tree into Map if it doesn't exist.
        Map<String, Tab> tabTreeMap = loadTabTreeToMapIfNecessary(request, tabIdStr);
        Tab tab = tabTreeMap.get(tabIdStr);
        
        List<Widget> newSectionWidgetList = new ArrayList<Widget>();
        @SuppressWarnings("unchecked")
        List<Integer> widgetIdsToBeRemoved = (List<Integer>) sessionTool.getAttribute(WebCoreConstants.SESSION_WIDGET_IDS_TO_BE_DELETED);
        if (widgetIdsToBeRemoved == null) {
            widgetIdsToBeRemoved = new ArrayList<Integer>();
        }
        for (Widget widget : tab.getSection(sectionIdStr).getWidgets()) {
            if (colIdsList.contains(String.valueOf(widget.getColumnId()))) {
                widgetIdsToBeRemoved.add(widget.getId());
            } else {
                newSectionWidgetList.add(widget);
            }
        }
        sessionTool.setAttribute(WebCoreConstants.SESSION_WIDGET_IDS_TO_BE_DELETED, widgetIdsToBeRemoved);
        
        tab.getSection(sectionIdStr).removeSectionColumns(colIdsList.toArray(new String[colIdsList.size()]));
        tab.getSection(sectionIdStr).setWidgets(newSectionWidgetList);
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    /**
     * Loads tab tree into Map if it doesn't exist.
     * 
     * @param request
     *            HttpServletRequest with Map<String, Tab> in the session.
     * @param tabId
     *            String random Tab id.
     * @return Map<String, Tab> Map with requested Tab object in Map.
     */
    Map<String, Tab> loadTabTreeToMapIfNecessary(HttpServletRequest request, String tabId) {
        SessionTool sessionTool = SessionTool.getInstance(request);
        @SuppressWarnings("unchecked")
        Map<String, Tab> tabTreeMap = (Map<String, Tab>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        if (!tabTreeMap.containsKey(tabId)) {
            RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
            int id = Integer.parseInt(keyMapping.getKey(tabId).toString());
            
            Tab tab = dashboardService.findTabByIdEager(id);
            // Generates randomId for Tab, Sections and Widgets.
            tab = DashboardUtil.setRandomIds(request, tab);
            tabTreeMap.put(tab.getRandomId(), tab);
        }
        return tabTreeMap;
    }
    
    /**
     * For the deleteSection.html which will delete a Section with specified tabID and sectionID.
     * 
     * @param request
     *            HttpServletRequest with tabID and sectionID parameter.
     * @return String boolean response in String format.
     */
    @RequestMapping(value = "/secure/dashboard/deleteSection.html")
    public @ResponseBody String deleteSection(HttpServletRequest request, HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        SessionTool sessionTool = SessionTool.getInstance(request);
        
        // The current section id & tab id.
        String sectionIdStr = request.getParameter("sectionID");
        String tabIdStr = request.getParameter("tabID");
        if (StringUtils.isBlank(tabIdStr) || StringUtils.isBlank(sectionIdStr)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        // Loads tab tree into Map if it doesn't exist.
        Map<String, Tab> tabTreeMap = loadTabTreeToMapIfNecessary(request, tabIdStr);
        Tab tab = tabTreeMap.get(tabIdStr);
        Section sec = tab.deleteSection(sectionIdStr);
        
        if (sec.getId() != null && sec.getId() > 0) {
            @SuppressWarnings("unchecked")
            List<Integer> sectionIds = (List<Integer>) sessionTool.getAttribute(WebCoreConstants.SESSION_SECTION_IDS_TO_BE_DELETED);
            if (sectionIds == null) {
                sectionIds = new ArrayList<Integer>();
            }
            sectionIds.add(sec.getId());
            sessionTool.setAttribute(WebCoreConstants.SESSION_SECTION_IDS_TO_BE_DELETED, sectionIds);
        }
        return WebCoreConstants.RESPONSE_TRUE;
    }
}
