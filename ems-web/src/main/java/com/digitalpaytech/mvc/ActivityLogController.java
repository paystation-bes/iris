package com.digitalpaytech.mvc;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.cxf.common.util.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.digitalpaytech.domain.ActivityLog;
import com.digitalpaytech.domain.ActivityType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.ListResultDTO;
import com.digitalpaytech.dto.customeradmin.ActivityLogEntry;
import com.digitalpaytech.mvc.customeradmin.CustomerAdminActivityLogController;
import com.digitalpaytech.mvc.customeradmin.support.ActivityLogFilterForm;
import com.digitalpaytech.mvc.customeradmin.support.ActivityLogFilterForm.FilterEntry;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.PropertiesRetrieval;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.ActivityLogService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.util.ActivityTypeConstants;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.customeradmin.ActivityLogFormValidator;

@Controller
public class ActivityLogController {
    
    private static Logger log = Logger.getLogger(CustomerAdminActivityLogController.class);
    
    @Autowired
    protected CustomerAdminService customerAdminService;
    @Autowired
    protected ActivityLogService activityLogService;
    @Autowired
    protected ActivityLogFormValidator activityLogFormValidator;
    @Autowired
    private PropertiesRetrieval propertiesRetrieval;
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    // Lazy loading in isValidDayRange method.
    private String[] dateRangesDefinitions;
    
    public final void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    public final void setActivityLogService(final ActivityLogService activityLogService) {
        this.activityLogService = activityLogService;
    }
    
    public final void setActivityLogFormValidator(final ActivityLogFormValidator activityLogFormValidator) {
        this.activityLogFormValidator = activityLogFormValidator;
    }
    
    public final void setPropertiesRetrieval(final PropertiesRetrieval propertiesRetrieval) {
        this.propertiesRetrieval = propertiesRetrieval;
    }
    
    public final void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }
    
    public String getGlobalRecentActivity(final HttpServletResponse response, final HttpServletRequest request, final ModelMap model) {
        final ActivityLogFilterForm filterForm = new ActivityLogFilterForm();
        
        /*
         * Pass null to setUpFilterForm in next release to retrieve all activity
         * types for Global recent activity tab
         */
        setUpFilterForm(request, model, filterForm, ActivityTypeConstants.LOGIN);
        //setUpFilterForm(request, model, filterForm, null);
        
        return "/settings/global/recentActivity";
    }
    
    /*
     * public String getAlertsRecentActivity(HttpServletRequest request,ModelMap model) {
     * ActivityLogFilterForm filterForm = new ActivityLogFilterForm();
     * setUpFilterForm(request, model, filterForm, ActivityTypeConstants.ALERT_CONFIG);
     * return "/settings/alerts/recentActivity";
     * }
     * public String getCardSettingsRecentActivity(HttpServletRequest request, ModelMap model) {
     * ActivityLogFilterForm filterForm = new ActivityLogFilterForm();
     * setUpFilterForm(request, model, filterForm, ActivityTypeConstants.CARD_MANAGEMENT_CONFIG);
     * return "/settings/cardSettings/recentActivity";
     * }
     * public String getLocationsRecentActivity(HttpServletRequest request, ModelMap model) {
     * ActivityLogFilterForm filterForm = new ActivityLogFilterForm();
     * setUpFilterForm(request, model, filterForm, ActivityTypeConstants.LOCATION_CONFIG);
     * setUpFilterForm(request, model, filterForm, ActivityTypeConstants.PAY_STATION_CONFIG);
     * setUpFilterForm(request, model, filterForm, ActivityTypeConstants.EXTEND_BY_PHONE_CONFIG);
     * return "/settings/locations/recentActivity";
     * }
     * public String getRoutesRecentActivity(HttpServletRequest request, ModelMap model) {
     * ActivityLogFilterForm filterForm = new ActivityLogFilterForm();
     * setUpFilterForm(request, model, filterForm, ActivityTypeConstants.ROUTE_CONFIG);
     * return "/settings/routes/recentActivity";
     * }
     * public String getUsersRecentActivity(HttpServletRequest request,ModelMap model) {
     * ActivityLogFilterForm filterForm = new ActivityLogFilterForm();
     * setUpFilterForm(request, model, filterForm, ActivityTypeConstants.USER_CONFIG);
     * setUpFilterForm(request, model, filterForm, ActivityTypeConstants.ROLE_CONFIG);
     * return "/settings/users/recentActivity";
     * }
     */
    
    private ModelMap setUpFilterForm(final HttpServletRequest request, final ModelMap model, final ActivityLogFilterForm filterForm,
        final Integer parentActivityTypeId) {
        
        //null is for global activity, all activity types
        if (parentActivityTypeId == null) {
            for (ActivityType activityType : this.activityLogService.findAllChildActivityTypes()) {
                filterForm.addLogType(activityType);
            }
        } else {
            for (ActivityType activityType : this.activityLogService.findActivityTypesByParentActivityTypeId(parentActivityTypeId)) {
                filterForm.addLogType(activityType);
            }
        }
        
        final SessionTool sessionTool = SessionTool.getInstance(request);
        final RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final List<UserAccount> userAccounts = this.customerAdminService.getUserAccountsWithAliasUsersByCustomerId(userAccount.getCustomer().getId());
        
        final List<String> userIds = new ArrayList<String>();
        for (FilterEntry entries : filterForm.getUserAccounts()) {
            userIds.add(entries.getRandomId());
        }
        
        try {
            for (UserAccount user : userAccounts) {
                if (!userIds.contains(keyMapping.getRandomString(user, WebCoreConstants.ID_LOOK_UP_NAME))) {
                    filterForm.addUserAccount(user);
                }
            }
        } catch (UnsupportedEncodingException e) {
            log.error("+++ Failed to encode user name when adding user in activity log.", e);
        }
        
        final WebSecurityForm<ActivityLogFilterForm> secForm = new WebSecurityForm<ActivityLogFilterForm>(null, filterForm);
        secForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        model.put(WebSecurityConstants.WEB_SECURITY_FORM, secForm);
        return model;
    }
    
    public String getActivityLog(
        @ModelAttribute(WebSecurityConstants.WEB_SECURITY_FORM) final WebSecurityForm<ActivityLogFilterForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        final HttpSession session = request.getSession(false);
        if (session == null) {
            log.warn("### Invalid session in the request. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final ActivityLogFilterForm activityLogFilter = webSecurityForm.getWrappedObject();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        this.activityLogFormValidator.validate(webSecurityForm, result, keyMapping);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        final String timeZone = WebSecurityUtil.getCustomerTimeZone();
        //sorting the activity log  
        List<ActivityLog> activityLogs = getActivityLogByFilter(activityLogFilter, keyMapping, timeZone);
        if (activityLogs == null || activityLogs.isEmpty()) {
            activityLogs = new ArrayList<ActivityLog>(1);
            //            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final ListResultDTO<ActivityLogEntry> customerActivityLog = new ListResultDTO<ActivityLogEntry>();
        customerActivityLog.setDataKey(activityLogFilter.getDataKey());
        
        for (ActivityLog activityLog : activityLogs) {
            final String activityMessage = findActivityMessageByActivityTypeId(activityLog);
            ActivityLogEntry logEntry;
            try {
                logEntry = new ActivityLogEntry(activityLog, timeZone, activityMessage);
            } catch (UnsupportedEncodingException e) {
                return WebCoreConstants.RESPONSE_FALSE;
            }
            customerActivityLog.getData().add(logEntry);
        }
        
        return WidgetMetricsHelper.convertToJson(customerActivityLog, "customerActivityLog", false);
    }
    
    private List<ActivityLog> getActivityLogByFilter(final ActivityLogFilterForm activityLogFilter, final RandomKeyMapping keyMapping,
        final String timeZone) {
        
        Calendar start = new GregorianCalendar();
        Calendar end = new GregorianCalendar();
        
        final List<Integer> userIds = new ArrayList<Integer>();
        if (activityLogFilter.getSelectedUserAccounts() == null || activityLogFilter.getSelectedUserAccounts().contains("All")
            || StringUtils.isEmpty(activityLogFilter.getSelectedUserAccounts()) || "[]".equals(activityLogFilter.getSelectedUserAccounts())) {
            for (FilterEntry userValuePair : activityLogFilter.getUserAccounts()) {
                userIds.add((Integer) keyMapping.getKey(userValuePair.getRandomId()));
            }
        } else {
            userIds.add(activityLogFilter.getSelectedUserAccountsRealValue());
        }
        
        final List<Integer> logTypes = new ArrayList<Integer>();
        for (FilterEntry logTypeValuePair : activityLogFilter.getLogTypes()) {
            logTypes.add((Integer) keyMapping.getKey(logTypeValuePair.getRandomId()));
        }
        /*
         * For this release, we do not have a filter for log type
         * for(String randomLogId: activityLogFilter.getSelectedLogTypes()){
         * logTypes.add((Integer) keyMapping.getKey(randomLogId));
         * }
         */
        
        if (activityLogFilter.getDataKey() == null) {
            activityLogFilter.setDataKey((new Date()).getTime());
        }
        
        if (isValidDayRange(activityLogFilter.getSelectedDateRange())
            && activityLogFilter.getSelectedDateRange().equalsIgnoreCase(this.commonControllerHelper
                                                                                 .getMessage("label.date.range.option.LAST_24_HOURS"))) {
            
            start.setTime(DateUtil.getCurrentGmtDate());
            final Calendar[] cals = DateUtil.getFirstLast24Hrs();
            start.setTime(cals[0].getTime());
            end.setTime(cals[1].getTime());
            
        } else if (isValidDayRange(activityLogFilter.getSelectedDateRange())
                   && activityLogFilter.getSelectedDateRange().equalsIgnoreCase(this.commonControllerHelper
                                                                                        .getMessage("label.date.range.option.THIS_WEEK"))) {
            
            final Calendar[] cals = DateUtil.getFirstLastDayofThisWeeks();
            start.setTime(cals[0].getTime());
            end.setTime(cals[1].getTime());
            
        } else if (isValidDayRange(activityLogFilter.getSelectedDateRange())
                   && activityLogFilter.getSelectedDateRange().equalsIgnoreCase(this.commonControllerHelper
                                                                                        .getMessage("label.date.range.option.LAST_WEEK"))) {
            
            final Calendar[] cals = DateUtil.getFirstLastDayofLastWeeks(1);
            start.setTime(cals[0].getTime());
            end.setTime(cals[1].getTime());
            
        } else if (isValidDayRange(activityLogFilter.getSelectedDateRange())
                   && activityLogFilter.getSelectedDateRange().equalsIgnoreCase(this.commonControllerHelper
                                                                                        .getMessage("label.date.range.option.LAST_2_WEEKS"))) {
            
            final Calendar[] cals = DateUtil.getFirstLastDayofLastWeeks(2);
            start.setTime(cals[0].getTime());
            end.setTime(cals[1].getTime());
            
        } else if (isValidDayRange(activityLogFilter.getSelectedDateRange())
                   && activityLogFilter.getSelectedDateRange().equalsIgnoreCase(this.commonControllerHelper
                                                                                        .getMessage("label.date.range.option.LAST_MONTH"))) {
            
            final Calendar[] cals = DateUtil.getFirstLastOfLastMonths(1);
            start.setTime(cals[0].getTime());
            end.setTime(cals[1].getTime());
            
        } else if (isValidDayRange(activityLogFilter.getSelectedDateRange())
                   && activityLogFilter.getSelectedDateRange().equalsIgnoreCase(this.commonControllerHelper
                                                                                        .getMessage("label.date.range.option.LAST_3_MONTHS"))) {
            final Calendar[] cals = DateUtil.getFirstLastOfLastMonths(3);
            start.setTime(cals[0].getTime());
            end.setTime(cals[1].getTime());
            
        } else if (isValidDayRange(activityLogFilter.getSelectedDateRange())
                   && activityLogFilter.getSelectedDateRange().equalsIgnoreCase(this.commonControllerHelper
                                                                                        .getMessage("label.date.range.option.LAST_6_MONTHS"))) {
            final Calendar[] cals = DateUtil.getFirstLastOfLastMonths(6);
            start.setTime(cals[0].getTime());
            end.setTime(cals[1].getTime());
            
        } else if (isValidDayRange(activityLogFilter.getSelectedDateRange())
                   && activityLogFilter.getSelectedDateRange().equalsIgnoreCase(this.commonControllerHelper
                                                                                        .getMessage("label.date.range.option.LAST_YEAR"))) {
            final Calendar[] cals = DateUtil.getLastYear();
            start.setTime(cals[0].getTime());
            end.setTime(cals[1].getTime());
            
        } else {
            start = getStartTime(activityLogFilter);
            end = getEndTime(activityLogFilter);
        }
        
        if (start == null || end == null) {
            return null;
        }
        
        if (!activityLogFilter.getSelectedDateRange().equalsIgnoreCase(this.commonControllerHelper
                                                                               .getMessage("label.date.range.option.LAST_24_HOURS"))
            && !activityLogFilter.getSelectedDateRange().equalsIgnoreCase(this.commonControllerHelper.getMessage("label.all"))) {
            final int offset = TimeZone.getTimeZone(timeZone).getOffset(start.getTimeInMillis()) / WebCoreConstants.ONE_SECOND_IN_MILLISECONDS;
            start.add(Calendar.SECOND, -offset);
            end.add(Calendar.SECOND, -offset);
        }
        
        final List<ActivityLog> list = this.activityLogService.findActivityLogWithCriteria(start, end, userIds, logTypes,
                                                                                           activityLogFilter.getOrder(),
                                                                                           activityLogFilter.getColumn(),
                                                                                           Integer.parseInt(activityLogFilter.getPage()),
                                                                                           Integer.parseInt(activityLogFilter.getNumOfRecord()));
        return list;
    }
    
    private Calendar getEndTime(final ActivityLogFilterForm filter) {
        final String dateRange = filter.getSelectedDateRange();
        Calendar endCal = new GregorianCalendar();
        if (dateRange == null || dateRange.equalsIgnoreCase(this.commonControllerHelper.getMessage("label.all"))) {
            endCal.setTimeInMillis(filter.getDataKey());
        } else {
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat(WebCoreConstants.DATE_UI_FORMAT);
                endCal.setTime(sdf.parse(filter.getCustomDateEnd()));
                endCal = DateUtil.setEndOfDayHourMinuteSecond(endCal);
                if (endCal.after(filter.getDataKey())) {
                    endCal.setTimeInMillis(filter.getDataKey());
                }
            } catch (ParseException e) {
                endCal.setTimeInMillis(filter.getDataKey());
            }
        }
        return endCal;
    }
    
    private Calendar getStartTime(final ActivityLogFilterForm filter) {
        final String dateRange = filter.getSelectedDateRange();
        Calendar startDate = new GregorianCalendar();
        startDate.setTime(DateUtil.getCurrentGmtDate());
        // All
        if (dateRange == null || dateRange.equals(this.commonControllerHelper.getMessage("label.all"))) {
            startDate.setTimeInMillis(0);
        } else if (dateRange.isEmpty() && !filter.getCustomDateStart().isEmpty()) {
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat(WebCoreConstants.DATE_UI_FORMAT);
                startDate.setTime(sdf.parse(filter.getCustomDateStart()));
                startDate = DateUtil.setBeginOfDayHourMinuteSecond(startDate);
            } catch (ParseException e) {
                return null;
            }
        } else {
            startDate.setTimeInMillis(0);
        }
        return startDate;
    }
    
    /**
     * Check if dateRange is:
     * - last 24 hours
     * - last week
     * - last 2 weeks
     * - last month
     * - last 3 months
     * - last 6 months
     * - last year
     * 
     * @param dateRange
     *            Date range user selected.
     * @return boolean true if it's in the list.
     */
    private boolean isValidDayRange(final String dateRange) {
        if (this.dateRangesDefinitions == null) {
            this.dateRangesDefinitions = new String[] { this.commonControllerHelper.getMessage("label.date.range.option.LAST_24_HOURS"),
                this.commonControllerHelper.getMessage("label.date.range.option.THIS_WEEK"),
                this.commonControllerHelper.getMessage("label.date.range.option.LAST_WEEK"),
                this.commonControllerHelper.getMessage("label.date.range.option.LAST_2_WEEKS"),
                this.commonControllerHelper.getMessage("label.date.range.option.LAST_MONTH"),
                this.commonControllerHelper.getMessage("label.date.range.option.LAST_3_MONTHS"),
                this.commonControllerHelper.getMessage("label.date.range.option.LAST_6_MONTHS"),
                this.commonControllerHelper.getMessage("label.date.range.option.LAST_YEAR") };
        }
        return isValidOption(this.dateRangesDefinitions, dateRange);
    }
    
    private boolean isValidOption(final String[] dateRangesDefinitionArray, final String dateRange) {
        for (String ranDef : dateRangesDefinitionArray) {
            if (ranDef.equalsIgnoreCase(dateRange)) {
                return true;
            }
        }
        return false;
    }
    
    private String findActivityMessageByActivityTypeId(final ActivityLog activitylog) {
        
        final ActivityType activityType = this.activityLogService.findActivityTypeByActivityId(activitylog.getActivityType().getId());
        
        final StringBuilder keyBldr = new StringBuilder();
        keyBldr.append("activity.message.");
        keyBldr.append(activityType.getMessagePropertiesKey());
        
        final String[] args = getActivityMessageArgs(activitylog);
        if (activitylog.getUserAccount().isIsAliasUser()) {
            keyBldr.append(".alias");
        }
        
        final String message = this.propertiesRetrieval.getPropertyEN(keyBldr.toString(), args);
        
        return message;
    }
    
    private String[] getActivityMessageArgs(final ActivityLog activityLog) {
        
        switch (activityLog.getActivityType().getParentActivityType().getId()) {
            case ActivityTypeConstants.LOGIN:
                return activityTypeLoginArguments(activityLog);
                
                /*
                 * case ActivityTypeConstants.BOSS:
                 * return activityTypeBOSSArguments(log);
                 * case ActivityTypeConstants.COUPON_CONFIG:
                 * return activityTypeCouponArguments(log);
                 * case ActivityTypeConstants.CARD_MANAGEMENT_CONFIG:
                 * return activityTypeCardManagementArguments(log);
                 * case ActivityTypeConstants.LOCATION_CONFIG:
                 * return activityTypeLocationArguments(log);
                 * case ActivityTypeConstants.PAY_STATION_CONFIG:
                 * return activityTypePayStationArguments(log);
                 * case ActivityTypeConstants.ROUTE_CONFIG:
                 * return activityTypeRouteArguments(log);
                 * case ActivityTypeConstants.ALERT_CONFIG:
                 * return activityTypeAlertArguments(log);
                 * case ActivityTypeConstants.EXTEND_BY_PHONE_CONFIG:
                 * return activityTypeExtendByPhoneArguments(log);
                 * case ActivityTypeConstants.USER_CONFIG:
                 * return activityTypeUserArguments(log);
                 * case ActivityTypeConstants.ROLE_CONFIG:
                 * return activityTypeRoleArguments(log);
                 * case ActivityTypeConstants.REPORTING_CONFIG:
                 * return activityTypeReportingArguments(log);
                 * case ActivityTypeConstants.COMPANY_PREFERENCES_CONFIG:
                 * return activityTypeCompanyPreferencesArguments(log);
                 * case ActivityTypeConstants.DPT_ADMIN:
                 * return activityTypeDPTAdminArguments(log);
                 */
            default:
                break;
        }
        return null;
    }
    
    private String[] activityTypeLoginArguments(final ActivityLog activityLog) {
        //%user name% logged in.
        //%user name% logged out.
        //%user name% failed to log in.		
        final String[] args = { activityLog.getUserAccount().getRealUserName() };
        return args;
    }
    
    /*
     * private String[] activityTypeBOSSArguments(ActivityLog log) {
     * if(Arrays.asList(ActivityTypeConstants.BOSS_CONFIG_TYPES).contains(log.getActivityType().getId())){
     * //%user name% uploaded settings for %number of pay stations affected% pay stations
     * //%user name% uploaded transactions for %number of pay stations affected% pay stations
     * //TODO: BOSSLog changeLog = findBOSSLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getNumberOfPayStationsAffected(),
     * };
     * return args;
     * }
     * else if(log.getActivityType().getId() == ActivityTypeConstants.MOVE_PAY_STATION){
     * //%user name% downloaded a %bad card list type% bad card list
     * //TODO: BOSSLog changeLog = findBOSSLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getBadCardListType(),
     * };
     * return args;
     * }
     * return null;
     * }
     * private String[] activityTypeCouponArguments(ActivityLog log) {
     * if(Arrays.asList(ActivityTypeConstants.COUPON_IMORT_EXPORT_TYPES).contains(log.getActivityType().getId())){
     * //%user name% exported a list of %number of coupons% coupons
     * //%user name% imported a list of %number of coupons% coupons
     * //TODO: CouponConfigLog changeLog = findCouponConfigLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getNumberOfCoupons(),
     * };
     * return args;
     * }
     * else if(Arrays.asList(ActivityTypeConstants.COUPON_CONFIG_TYPES).contains(log.getActivityType().getId())){
     * //%user name% created coupon %coupon number%
     * //%user name% modified coupon %coupon number%
     * //%user name% deleted coupon %coupon number%
     * //TODO: CouponConfigLog changeLog = findCouponConfigLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getCouponNumber(),
     * };
     * return args;
     * }
     * return null;
     * }
     * private String[] activityTypeCardManagementArguments(ActivityLog log) {
     * if(Arrays.asList(ActivityTypeConstants.CARD_IMPORT_EXPORT_TYPES).contains(log.getActivityType().getId())){
     * //%user name% exported a list of %number of cards% cards
     * //%user name% imported a list of %number of cards% cards
     * //TODO: CardConfigLog changeLog = findCardConfigLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getNumberOfCards()
     * };
     * return args;
     * }
     * else if(Arrays.asList(ActivityTypeConstants.CARD_CONFIG_TYPES).contains(log.getActivityType().getId())){
     * //%user name% created card %card number%
     * //%user name% modified card %card number%
     * //%user name% deleted card %card number%
     * //%user name% banned card %card number%
     * //%user name% modified banned card %card number%
     * //%user name% deleted banned card %card number%
     * //TODO: CardConfigLog changeLog = findCardConfigLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getCardNumber()
     * };
     * return args;
     * }
     * else if(Arrays.asList(ActivityTypeConstants.CUSTOM_CARD_CONFIG_TYPES).contains(log.getActivityType().getId())){
     * //%user name% created a new %custom card type% type
     * //%user name% modified a %custom card type% type
     * //%user name% deleted a %custom card type% type
     * //TODO: CardConfigLog changeLog = findCardConfigLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getCardType()
     * };
     * return args;
     * }
     * else if(log.getActivityType().getId() == ActivityTypeConstants.CREDIT_CARD_REFUND){
     * //%user name% refunded credit card transaction number %original transaction number%
     * //TODO: CardConfigLog changeLog = findCardConfigLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getTransactionNumber()
     * };
     * return args;
     * }
     * return null;
     * }
     * private String[] activityTypeLocationArguments(ActivityLog log) {
     * if(Arrays.asList(ActivityTypeConstants.LOCATION_CONFIG_TYPES).contains(log.getActivityType().getId())){
     * //%user name% created location %location name%
     * //%user name% modified location %location name%
     * //%user name% deleted location %location name%
     * //TODO: LocationLog changeLog = findLocationLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getLocationName()
     * };
     * return args;
     * }
     * else if(log.getActivityType().getId() == ActivityTypeConstants.ASSIGN_PAY_STATION_TO_LOCATION){
     * //%user name% assigned pay station %pay station name% to location %location name%
     * //TODO: LocationLog changeLog = findLocationLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getPayStationName(),
     * //changeLog.getLocationName()
     * };
     * return args;
     * }
     * return null;
     * }
     * private String[] activityTypePayStationArguments(ActivityLog log) {
     * if(Arrays.asList(ActivityTypeConstants.PAY_STATION_CONFIG_TYPES).contains(log.getActivityType().getId())){
     * //%user name% set pay station %pay station name% to hidden
     * //%user name% set pay station %pay station name% to visible
     * //%user name% activated pay station %pay station name%
     * //%user name% deactivated Pay Station %pay station name%
     * //%user name% edited settings for Pay Station %pay station name%
     * //%user name% created Pay Station %pay station name%
     * //TODO: PayStationLog changeLog = findPayStationLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getPayStationName()
     * };
     * return args;
     * }
     * else if(log.getActivityType().getId() == ActivityTypeConstants.MOVE_PAY_STATION){
     * //%user name% moved Pay Station %pay station name% from %original location% to %new location%
     * //TODO: LocationLog changeLog = findLocationLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getPayStationName(),
     * //changeLog.getOriginalLocation(),
     * //changeLog.getNewLocation()
     * };
     * return args;
     * }
     * else if(Arrays.asList(ActivityTypeConstants.PAY_STATION_DOWNLOAD_TYPES).contains(log.getActivityType().getId())){
     * //Pay Station %pay station name% downloaded new configuration
     * //Pay Station %pay station name% downloaded a new encryption key
     * //TODO: PayStationLog changeLog = findPayStationLogByLogId(log.getEntityLogId());
     * String args[] = {
     * //changeLog.getPayStationName()
     * };
     * return args;
     * }
     * else if(log.getActivityType().getId() == ActivityTypeConstants.SOFTWARE_UPDATED){
     * //Pay Station %pay station name% was upgraded from %previous software version% to %new software version%
     * //TODO: PayStationLog changeLog = findPayStationLogByLogId(log.getEntityLogId());
     * String args[] = {
     * //changeLog.getPayStationName()
     * //changeLog.getPreviousSoftwareVersion()
     * //changeLog.getNewSoftwareVersion()
     * };
     * return args;
     * }
     * return null;
     * }
     * private String[] activityTypeRouteArguments(ActivityLog log) {
     * if(log.getActivityType().getId() == ActivityTypeConstants.ADD_ROUTE){
     * //%user name% created a %route type% route called %route name%
     * //TODO: RouteLog changeLog = findRouteLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getRouteType(),
     * //changeLog.getRouteName(),
     * };
     * return args;
     * }
     * else if(Arrays.asList(ActivityTypeConstants.ROUTE_CONFIG_TYPES).contains(log.getActivityType().getId())){
     * //%user name% modified the %route name% route
     * //%user name% deleted the %route name% route
     * //TODO: RouteLog changeLog = findRouteLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getRouteName(),
     * };
     * return args;
     * }
     * else if(log.getActivityType().getId() == ActivityTypeConstants.ASSIGN_PAY_STATION_TO_ROUTE){
     * //%user name% assigned Pay Station %pay station name% to the %route name% route
     * //TODO: RouteLog changeLog = findRouteLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getPayStationName(),
     * //changeLog.getRouteName(),
     * };
     * return args;
     * }
     * return null;
     * }
     * private String[] activityTypeAlertArguments(ActivityLog log) {
     * if(log.getActivityType().getId() == ActivityTypeConstants.ADD_ROUTE){
     * //%user name% created a %alert type% alert named %alert name%
     * //TODO: AlertLog changeLog = findAlertLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getAlertType(),
     * //changeLog.getAlertName(),
     * };
     * return args;
     * }
     * else if(Arrays.asList(ActivityTypeConstants.ALERT_CONFIG_TYPES).contains(log.getActivityType().getId())){
     * //%user name% modified the alert %alert name%
     * //%user name% deleted the alert %alert name%
     * //TODO: AlertLog changeLog = findAlertLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getAlertName(),
     * };
     * return args;
     * }
     * return null;
     * }
     * private String[] activityTypeExtendByPhoneArguments(ActivityLog log) {
     * if(Arrays.asList(ActivityTypeConstants.RATE_CONFIG_TYPES).contains(log.getActivityType().getId())){
     * //%user name% created an Extend-by-Phone rate named %rate name% for %location name%
     * //%user name% modified the rate %rate name% for %location name%
     * //%user name% deleted the rate %rate name% for %location name%
     * //TODO: ExtendByPhoneLog changeLog = findExtendByPhoneLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getRateName(),
     * //changeLog.getLocationName(),
     * };
     * return args;
     * }
     * else if(Arrays.asList(ActivityTypeConstants.POLICY_CONFIG_TYPES).contains(log.getActivityType().getId())){
     * //%user name% created an Extend-by-Phone policy named %policy name% for %location name%
     * //%user name% modified the policy %policy name% for %location name%
     * //%user name% deleted the policy %policy name% for %location name%
     * //TODO: ExtendByPhoneLog changeLog = findExtendByPhoneLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getPolicyName(),
     * //changeLog.getLocationName(),
     * };
     * return args;
     * }
     * return null;
     * }
     * private String[] activityTypeUserArguments(ActivityLog log) {
     * if(Arrays.asList(ActivityTypeConstants.USER_CONFIG_TYPES).contains(log.getActivityType().getId())){
     * //%user name% created a user account for %user first name% %user last name%
     * //%user name% modified %user first name% %user last name%’s user account
     * //%user name% deleted %user first name% %user last name%’s user account
     * UserAccountLog changeLog = findUserAccountLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * changeLog.getFirstName(),
     * changeLog.getLastName()};
     * return args;
     * }
     * return null;
     * }
     * private String[] activityTypeRoleArguments(ActivityLog log) {
     * if(Arrays.asList(ActivityTypeConstants.ROLE_CONFIG_TYPES).contains(log.getActivityType().getId())){
     * //%user name% created the %role name% role
     * //%user name% modified the %role name% role
     * //%user name% deleted the %role name% role
     * //TODO: RoleLog changeLog = findRoleLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getRolename(),
     * };
     * return args;
     * }
     * else if(Arrays.asList(ActivityTypeConstants.ROLE_ASSIGN_CONFIG_TYPES).contains(log.getActivityType().getId())){
     * //%user name% assigned %user first name% %user last name% to the %role name% role
     * //%user name% removed %user first name% %user last name% from the %role name% role
     * //TODO: RoleLog changeLog = findRoleLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getFirstName(),
     * //changeLog.getLastName(),
     * //changeLog.getRoleName()
     * };
     * return args;
     * }
     * return null;
     * }
     * private String[] activityTypeReportingArguments(ActivityLog log) {
     * if(Arrays.asList(ActivityTypeConstants.REPORTING_CONFIG_TYPES).contains(log.getActivityType().getId())){
     * //%user name% created the %report name% scheduled report
     * //%user name% modified the %report name% scheduled report
     * //%user name% deleted the %report name% scheduled report
     * //TODO: ReportLog changeLog = findReportLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getReportName(),
     * };
     * return args;
     * }
     * return null;
     * }
     * private String[] activityTypeCompanyPreferencesArguments(ActivityLog log) {
     * if(log.getActivityType().getId() == ActivityTypeConstants.EDIT_VALUE){
     * //%user name% modified %company preference name%
     * //TODO: CompanyPreferenceLog changeLog = findCompanyPreferenceLogByLogId(log.getEntityLogId());
     * String args[] = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getCompanyPreferenceName(),
     * };
     * return args;
     * }
     * return null;
     * }
     * private String[] activityTypeDPTAdminArguments(ActivityLog log) {
     * if(Arrays.asList(ActivityTypeConstants.DPT_PROCESS_TYPES).contains(log.getActivityType().getId())){
     * //%user name% triggered a key rotation
     * //%user name% triggered a Credit Card Retry
     * //TODO: DPTLog changeLog = findDPTLogId(log.getEntityLogId());
     * String[] notificationArgs = {
     * log.getUserAccount().getUserName(),
     * };
     * return notificationArgs;
     * }
     * else if(Arrays.asList(ActivityTypeConstants.DPT_SERVER_CONFIG_TYPES).contains(log.getActivityType().getId())){
     * //%user name% processed uploaded files on %application server cluster name%
     * //%user name% promoted %application server cluster name% to primary status
     * //%user name% triggered a shutdown of %application server cluster name%
     * //TODO: ServerConfigLog changeLog = findServerConfigLogId(log.getEntityLogId());
     * String[] notificationArgs = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getAppServerClusterName()
     * };
     * return notificationArgs;
     * }
     * else if(Arrays.asList(ActivityTypeConstants.NOTIFICATION_TYPES).contains(log.getActivityType().getId())){
     * //%user name% created a System Notification named %alert notification title%
     * //%user name% modified %alert notification title%
     * //%user name% deleted %alert notification title%
     * //%user name% archived %alert notification title%
     * NotificationLog changeLog = findNotificationLogByLogId(log.getEntityLogId());
     * String[] notificationArgs = {
     * log.getUserAccount().getUserName(),
     * changeLog.getTitle()
     * };
     * return notificationArgs;
     * }
     * else if(log.getActivityType().getId() == ActivityTypeConstants.ADD_MERCHANT_ACCOUNT){
     * //%user name% created merchant account %merchant account name% for %EMS customer Name%
     * //TODO: MerchantAccountLog changeLog = findMerchantAccountLogId(log.getEntityLogId());
     * String[] notificationArgs = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getMerchantAccountName()
     * //changeLog.getEMSCustomerName()
     * };
     * return notificationArgs;
     * }
     * else if(Arrays.asList(ActivityTypeConstants.MERCHANT_ACCOUNT_CONFIG_TYPES).contains(log.getActivityType().getId())){
     * //%user name% modified %ems customer name%’s merchant account %merchant account name%
     * //%user name% deleted %ems customer name%’s merchant account %merchant account name%
     * //TODO: MerchantAccountLog changeLog = findMerchantAccountLogId(log.getEntityLogId());
     * String[] notificationArgs = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getEMSCustomerName()
     * //changeLog.getMerchantAccountName()
     * };
     * return notificationArgs;
     * }
     * else if(Arrays.asList(ActivityTypeConstants.DPT_API_TYPES).contains(log.getActivityType().getId())){
     * //%user name% created a token for the %api name% API
     * //%user name% modified a token for the %api name% API
     * //%user name% deleted a token for the %api name% API
     * //TODO: APILog changeLog = findAPILogByLogId(log.getEntityLogId());
     * String[] notificationArgs = {
     * log.getUserAccount().getUserName(),
     * //changeLog.getAPIName()
     * };
     * return notificationArgs;
     * }
     * return null;
     * }
     */
}
