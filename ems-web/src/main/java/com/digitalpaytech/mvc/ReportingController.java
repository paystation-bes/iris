package com.digitalpaytech.mvc;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Consumer;
import com.digitalpaytech.domain.CreditCardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.CustomerEmail;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.EmailAddress;
import com.digitalpaytech.domain.EventDeviceType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.ReportContent;
import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportEmail;
import com.digitalpaytech.domain.ReportHistory;
import com.digitalpaytech.domain.ReportLocationValue;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.domain.ReportRepeatType;
import com.digitalpaytech.domain.ReportRepository;
import com.digitalpaytech.domain.ReportStatusType;
import com.digitalpaytech.domain.ReportType;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.AdhocReportInfo;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.ReportSearchCriteria;
import com.digitalpaytech.dto.ReportUIFilterType;
import com.digitalpaytech.dto.SearchConsumerResult;
import com.digitalpaytech.dto.SearchConsumerResultSetting;
import com.digitalpaytech.dto.customeradmin.LocationTree;
import com.digitalpaytech.dto.customeradmin.ReportDefinitionInfo;
import com.digitalpaytech.dto.customeradmin.ReportDefinitionInfo.DefinitionInfo;
import com.digitalpaytech.dto.customeradmin.ReportQueueAndRepositoryInfo;
import com.digitalpaytech.dto.customeradmin.ReportQueueInfo;
import com.digitalpaytech.dto.customeradmin.ReportQueueInfo.QueueInfo;
import com.digitalpaytech.dto.customeradmin.ReportRepositoryInfo;
import com.digitalpaytech.dto.customeradmin.ReportRepositoryInfo.RepositoryInfo;
import com.digitalpaytech.mvc.customeradmin.support.ReportEditForm;
import com.digitalpaytech.mvc.customeradmin.support.ReportFilter;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.ConsumerService;
import com.digitalpaytech.service.CreditCardTypeService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.ReportCollectionFilterTypeService;
import com.digitalpaytech.service.ReportDefinitionService;
import com.digitalpaytech.service.ReportHistoryService;
import com.digitalpaytech.service.ReportQueueService;
import com.digitalpaytech.service.ReportRepositoryService;
import com.digitalpaytech.service.ReportTypeService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.ReportingUtil;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.RelativeDateTime;
import com.digitalpaytech.util.support.WebObjectId;
import com.digitalpaytech.validation.customeradmin.ReportValidator;

/*
 * This structure is based off of the UI mock ups as of July 13
 * 
 */

@Controller
public class ReportingController {
    public static final String DEFAULT_REPORT_SCHEDULE_TIME = "03:00";
    public static final String REPORTING_SUBCLASS_SYSTEM_ADMIN = "SystemAdminReportingController";
    public static final String REPORTING_SUBCLASS_CUSTOMER_SYSTEM_ADMIN = "SystemAdminCustomerReportingController";
    
    public static final String FORM_EDIT = WebSecurityConstants.WEB_SECURITY_FORM;
    public static final String FORM_REPORT = "reportForm";
    
    public static final String URI_ADHOC_REPORT_STATUS = "/secure/reporting/adhocStatus";
    public static final String URI_ADHOC_REPORT_VIEW = "/secure/reporting/viewAdhocReport";
    
    public static final String PARAM_REPORT_TYPE_ID = "reportTypeID";
    public static final String PARAM_REPOSITORY_ID = "repositoryID";
    public static final String PARAM_QUEUE_ID = "queueID";
    public static final String PARAM_DEFINITION_ID = "definitionID";
    public static final String PARAM_HISTORY_ID = "historyId";
    
    public static final String MODEL_DEFINITION_INFO_LIST = "definitionInfoList";
    public static final String MODEL_QUEUE_INFO_LIST = "queueAndRepositoryInfoList";
    
    protected static final Map<String, Class<?>> MAP_LOCPOS_OBJECT_TYPE = new HashMap<String, Class<?>>(4);
    static {
        MAP_LOCPOS_OBJECT_TYPE.put(Location.class.getName(), Location.class);
        MAP_LOCPOS_OBJECT_TYPE.put(PointOfSale.class.getName(), PointOfSale.class);
    }
    
    private static final String REPORT_FILTER = "ReportFilters";
    
    private static final String SEPARATOR_TIME = " : ";
    private static final String FMT_UI_DATE = WebCoreConstants.DATE_UI_FORMAT;
    private static final String FMT_UI_TIME = "HH:mm";
    private static final String FMT_REPORT_TIME = "HH:mm:ss";
    private static final String FMT_UI_DATE_TIME = FMT_UI_DATE + SEPARATOR_TIME + FMT_UI_TIME;
    private static final String FMT_REPORT_DATE_TIME = FMT_UI_DATE + SEPARATOR_TIME + FMT_REPORT_TIME;
    
    private static final Logger LOG = Logger.getLogger(ReportingController.class);
    
    private static Map<Integer, int[]> permissionsByType = new HashMap<Integer, int[]>();
    static {
        permissionsByType.put((int) ReportingConstants.REPORT_TYPE_MAINTENANCE_SUMMARY, new int[] {
            WebSecurityConstants.PERMISSION_ALERTS_MANAGEMENT, WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS, });
        permissionsByType.put((int) ReportingConstants.REPORT_TYPE_COLLECTION_SUMMARY, new int[] {
            WebSecurityConstants.PERMISSION_COLLECTIONS_MANAGEMENT, WebSecurityConstants.PERMISSION_VIEW_COLLECTION_STATUS, });
    }
    
    @Autowired
    private ReportingUtil reportingUtil;
    
    @Autowired
    private ReportDefinitionService reportDefinitionService;
    @Autowired
    private ReportQueueService reportQueueService;
    @Autowired
    private ReportHistoryService reportHistoryService;
    @Autowired
    private ReportRepositoryService reportRepositoryService;
    @Autowired
    private ReportTypeService reportTypeService;
    @Autowired
    private CreditCardTypeService creditCardTypeService;
    @Autowired
    private CustomerCardTypeService customerCardTypeService;
    @Autowired
    private MerchantAccountService merchantAccountService;
    @Autowired
    private LocationService locationService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private CustomerAdminService customerAdminService;
    @Autowired
    private ConsumerService consumerService;
    @Autowired
    private ReportCollectionFilterTypeService reportCollectionFilterTypeService;
    
    @Autowired
    private ReportValidator reportValidator;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    public final void setReportDefinitionService(final ReportDefinitionService reportDefinitionService) {
        this.reportDefinitionService = reportDefinitionService;
    }
    
    public final void setReportQueueService(final ReportQueueService reportQueueService) {
        this.reportQueueService = reportQueueService;
    }
    
    public final void setConsumerService(final ConsumerService consumerService) {
        this.consumerService = consumerService;
    }
    
    public final void setReportHistoryService(final ReportHistoryService reportHistoryService) {
        this.reportHistoryService = reportHistoryService;
    }
    
    public final void setReportRepositoryService(final ReportRepositoryService reportRepositoryService) {
        this.reportRepositoryService = reportRepositoryService;
    }
    
    public final void setReportTypeService(final ReportTypeService reportTypeService) {
        this.reportTypeService = reportTypeService;
    }
    
    public final void setCreditCardTypeService(final CreditCardTypeService creditCardTypeService) {
        this.creditCardTypeService = creditCardTypeService;
    }
    
    public final void setCustomerCardTypeService(final CustomerCardTypeService customerCardTypeService) {
        this.customerCardTypeService = customerCardTypeService;
    }
    
    public final void setMerchantAccountService(final MerchantAccountService merchantAccountService) {
        this.merchantAccountService = merchantAccountService;
    }
    
    public final void setLocationService(final LocationService locationService) {
        this.locationService = locationService;
    }
    
    protected final CustomerAdminService getCustomerAdminService() {
        return this.customerAdminService;
    }
    
    public final void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    protected final CustomerService getCustomerService() {
        return this.customerService;
    }
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final void setReportValidator(final ReportValidator reportValidator) {
        this.reportValidator = reportValidator;
    }
    
    public final void setReportingUtil(final ReportingUtil reportingUtil) {
        this.reportingUtil = reportingUtil;
    }
    
    public final EmsPropertiesService getEmsPropertiesService() {
        return this.emsPropertiesService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final void setReportCollectionFilterTypeService(final ReportCollectionFilterTypeService reportCollectionFilterTypeService) {
        this.reportCollectionFilterTypeService = reportCollectionFilterTypeService;
    }
    
    protected final CommonControllerHelper getCommonControllerHelper() {
        return this.commonControllerHelper;
    }
    
    public final void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }
    
    /***********************************************************************************
     * ReportLists
     ***********************************************************************************/
    
    /**
     * This method will look at the permissions in the WebUser and pass control to the Reporting landing page if allowed.
     * 
     * @return Will return the page for pending/scheduled/ finished report lists or error if no permissions exist.
     */
    public final String getReportLists(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model, final String url) {
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final SessionTool sessionTool = SessionTool.getInstance(request);
        final RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        
        final ReportQueueAndRepositoryInfo queueAndRepositoryInfoList = createReportQueueAndRepositoryInfo(userAccount, request, response, keyMapping);
        
        model.put(MODEL_DEFINITION_INFO_LIST, getReportDefinitionInfo(userAccount.getId(), resolveCustomerId(request, response), keyMapping));
        model.put(MODEL_QUEUE_INFO_LIST, queueAndRepositoryInfoList);
        
        return url;
        
    }
    
    /**
     * This method will look at the permissions in the WebUser and pass control to the Reporting landing page if allowed.
     * 
     * @return Will return the page for pending/scheduled/ finished report lists or error if no permissions exist.
     */
    @SuppressWarnings("checkstyle:designforextension")
    public String getUnreadReportNumber(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final int numberOfUnreadReports = this.reportRepositoryService.countUnreadReportsByUserAccountId(userAccount.getId());
        
        return String.valueOf(numberOfUnreadReports);
    }
    
    /**
     * This method will look at the permissions in the WebUser and pass control to the Reporting landing page if allowed.
     * 
     * @return Will return the page for pending/scheduled/ finished report lists or error if no permissions exist.
     */
    public final String sortReports(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model, final String url) {
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final ReportQueueAndRepositoryInfo queueAndRepositoryInfoList = createReportQueueAndRepositoryInfo(userAccount, request, response, keyMapping);
        model.put(MODEL_QUEUE_INFO_LIST, queueAndRepositoryInfoList);
        
        return url;
    }
    
    /**
     * This method will look at the permissions in the WebUser and pass control to the Reporting landing page if allowed.
     * 
     * @return Will return the page for pending/scheduled/ finished report lists or error if no permissions exist.
     */
    public final String sortReportDefinitions(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model, final String url) {
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        model.put(MODEL_DEFINITION_INFO_LIST, getReportDefinitionInfo(userAccount.getId(), resolveCustomerId(request, response), keyMapping));
        
        return url;
    }
    
    /**
     * This method takes a reportQueue Id and modifies the status to Cancelled if the user has valid permissions. If the report is launched before the user
     * tries to cancel it response status is set to HTTP_STATUS_CODE_483.
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON object of Pending Reports and Completed Reports lists.
     */
    public final String cancelQueuedReport(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model, final String url) {
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strQueueId = request.getParameter(PARAM_QUEUE_ID);
        final Integer queueId = (Integer) keyMapping.getKey(strQueueId);
        if (queueId == null) {
            LOG.warn("+++ Invalid randomised queueID in ReportingController.cancelQueuedReport() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final boolean isCancelled = this.reportQueueService.cancelReport(queueId);
        if (!isCancelled) {
            response.setHeader(WebCoreConstants.HEADER_CUSTOM_STATUS_CODE, "" + WebCoreConstants.HTTP_STATUS_CODE_483);
        }
        
        final ReportQueueAndRepositoryInfo queueAndRepositoryInfoList = createReportQueueAndRepositoryInfo(userAccount, request, response, keyMapping);
        
        //        return WidgetMetricsHelper.convertToJson(queueAndRepositoryInfoList, "ReportQueueAndRepositoryInfo", true);
        model.put(MODEL_QUEUE_INFO_LIST, queueAndRepositoryInfoList);
        return url;
    }
    
    /**
     * This method takes a reportHistory Id (coming from Pending Reports) or reportRepository Id (coming from Completed Reports) and creates a new reportQueue
     * entry using the reportDefinition of the reportHistory item if the user has permission.
     * 
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON object of Pending Reports and Completed Reports lists.
     */
    // TODO is status or type check needed? 
    @SuppressWarnings("checkstyle:designforextension")
    public String rerunReport(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model, final String url) {
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strRepositoryId = request.getParameter(PARAM_REPOSITORY_ID);
        final String strHistoryId = request.getParameter(PARAM_QUEUE_ID);
        Integer historyId = null;
        boolean updateHistoryStatus = false;
        
        if (strRepositoryId != null) {
            final Integer repositoryId = (Integer) keyMapping.getKey(strRepositoryId);
            if (repositoryId == null) {
                LOG.warn("+++ Invalid randomised repositoryID in ReportingController.rerunReport() method. +++");
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            final ReportRepository reportRepository = this.reportRepositoryService.getReportRepository(repositoryId);
            
            historyId = reportRepository.getReportHistory().getId();
            
        } else if (strHistoryId != null) {
            historyId = (Integer) keyMapping.getKey(strHistoryId);
            if (historyId == null) {
                LOG.warn("+++ Invalid randomised queueID in ReportingController.rerunReport() method. +++");
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            updateHistoryStatus = true;
        }
        
        final ReportHistory reportHistory = this.reportHistoryService.getReportHistory(historyId);
        final ReportDefinition reportDefinition = this.reportDefinitionService.getReportDefinition(reportHistory.getReportDefinition().getId());
        final String timeZone = resolveTimeZone(reportDefinition.getCustomer().getId());
        final ReportQueue reportQueue = this.reportingUtil.createReportQueueEntry(reportDefinition, timeZone, true);
        
        try {
            this.reportQueueService.saveReportQueue(reportQueue);
        } catch (DataIntegrityViolationException dive) {
            final Throwable th = dive.getCause().getCause();
            if (!th.getMessage().contains("idx_reportqueue_reportdefinition")) {
                throw dive;
            }
        }
        if (updateHistoryStatus) {
            final ReportStatusType reportStatusType = new ReportStatusType();
            reportStatusType.setId(ReportingConstants.REPORT_STATUS_RERAN);
            reportHistory.setReportStatusType(reportStatusType);
            this.reportHistoryService.updateReportHistory(reportHistory);
        }
        
        final ReportQueueAndRepositoryInfo queueAndRepositoryInfoList = createReportQueueAndRepositoryInfo(userAccount, request, response, keyMapping);
        
        //        return WidgetMetricsHelper.convertToJson(queueAndRepositoryInfoList, "ReportQueueAndRepositoryInfo", true);
        model.put(MODEL_QUEUE_INFO_LIST, queueAndRepositoryInfoList);
        return url;
    }
    
    /**
     * This method takes a reportDefinition Id and creates a new reportQueue entry if the user has permission.
     * 
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON object of Pending Reports and Completed Reports lists.
     */
    public final String runNow(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model, final String url) {
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strDefinitionId = request.getParameter(PARAM_DEFINITION_ID);
        final Integer definitionId = (Integer) keyMapping.getKey(strDefinitionId);
        if (definitionId == null) {
            LOG.warn("+++ Invalid randomised definitionID in ReportingController.runNow() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final ReportDefinition reportDefinition = this.reportDefinitionService.getReportDefinition(definitionId);
        final String timeZone = resolveTimeZone(reportDefinition.getCustomer().getId());
        final ReportQueue reportQueue = this.reportingUtil.createReportQueueEntry(reportDefinition, timeZone, true);
        
        try {
            this.reportQueueService.saveReportQueue(reportQueue);
        } catch (DataIntegrityViolationException dive) {
            final Throwable th = dive.getCause().getCause();
            if (!th.getMessage().contains("idx_reportqueue_reportdefinition")) {
                throw dive;
            }
        }
        
        final ReportQueueAndRepositoryInfo queueAndRepositoryInfoList = createReportQueueAndRepositoryInfo(userAccount, request, response, keyMapping);
        
        //        return WidgetMetricsHelper.convertToJson(queueAndRepositoryInfoList, "ReportQueueAndRepositoryInfo", true);
        model.put(MODEL_QUEUE_INFO_LIST, queueAndRepositoryInfoList);
        return url;
    }
    
    /**
     * This method takes a reportRepository Id and deletes the content of the report from reportRepository and reportContent tables if the user has permission.
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON object of Pending Reports and Completed Reports lists.
     */
    // TODO is status or type check needed? 
    @SuppressWarnings("checkstyle:designforextension")
    public String deleteReport(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model, final String url) {
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strRepositoryId = request.getParameter(PARAM_REPOSITORY_ID);
        final Integer repositoryId = (Integer) keyMapping.getKey(strRepositoryId);
        if (repositoryId == null) {
            LOG.warn("+++ Invalid randomised repositoryID in ReportingController.deleteReport() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        this.reportRepositoryService.deleteReportRepository(repositoryId);
        
        final ReportQueueAndRepositoryInfo queueAndRepositoryInfoList = createReportQueueAndRepositoryInfo(userAccount, request, response, keyMapping);
        
        //        return WidgetMetricsHelper.convertToJson(queueAndRepositoryInfoList, "ReportQueueAndRepositoryInfo", true);
        model.put(MODEL_QUEUE_INFO_LIST, queueAndRepositoryInfoList);
        return url;
    }
    
    /**
     * This method takes a reportRepository Id and deletes the content of the report from reportRepository and reportContent tables if the user has permission.
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON object of Pending Reports and Completed Reports lists.
     */
    // TODO is status or type check needed? 
    @SuppressWarnings("checkstyle:designforextension")
    public String viewReport(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strRepositoryId = request.getParameter(PARAM_REPOSITORY_ID);
        final Integer repositoryId = (Integer) keyMapping.getKey(strRepositoryId);
        if (repositoryId == null) {
            LOG.warn("+++ Invalid randomised repositoryID in ReportingController.viewReport() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final ReportContent reportContent = this.reportRepositoryService.findReportContentByReportRepositoryId(repositoryId);
        final byte[] fileContent = reportContent.getContent();
        
        if (fileContent == null) {
            LOG.warn("+++ Content file empty for repositoryId =" + repositoryId + " in ReportingController.viewReport() method.  +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
            
        }
        
        final ReportRepository reportRepository = this.reportRepositoryService.getReportRepository(repositoryId);
        
        try {
            response.reset();            
            if (reportRepository.getReportHistory().getReportDefinition().isIsCsvOrPdf()) {
                response.setHeader("Content-Disposition", "attachment; filename=\"" + reportRepository.getFileName() + "\"");
                response.setContentType("application/pdf");
            } else {
                response.setHeader("Content-Disposition", "attachment; filename=\"" + reportRepository.getFileName() + ".zip\"");
                response.setContentType("application/x-zip-compressed");
            }
            final ServletOutputStream outStream = response.getOutputStream();
            
            outStream.write(fileContent);
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            LOG.warn("+++ PDF could not be created for fileName =" + reportRepository.getFileName()
                        + " in ReportingController.viewReport() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        reportRepository.setIsReadByUser(true);
        this.reportRepositoryService.saveOrUpdateReportRepository(reportRepository);
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    /**
     * This method takes a reportDefinition Id and changes the status to Deleted if the user has permission.
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON object of Scheduled Reports list.
     */
    // TODO is status or type check needed? 
    @SuppressWarnings("checkstyle:designforextension")
    public String deleteReportDefinition(final HttpServletRequest request, final HttpServletResponse response
            , final ModelMap model, final String url) {
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strDefinitionId = request.getParameter(PARAM_DEFINITION_ID);
        final Integer definitionId = (Integer) keyMapping.getKey(strDefinitionId);
        if (definitionId == null) {
            LOG.warn("+++ Invalid repositoryID in ReportingController.deleteDefinition() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final ReportDefinition reportDefinition = this.reportDefinitionService.getReportDefinition(definitionId);
        final ReportStatusType deletedStatus = new ReportStatusType();
        deletedStatus.setId(ReportingConstants.REPORT_STATUS_DELETED);
        reportDefinition.setReportStatusType(deletedStatus);
        this.reportDefinitionService.updateReportDefinition(reportDefinition);
        
        //        return WidgetMetricsHelper.convertToJson(definitionInfoList, "ReportDefinitionInfo", true);
        model.put(MODEL_DEFINITION_INFO_LIST, getReportDefinitionInfo(userAccount.getId(), resolveCustomerId(request, response), keyMapping));
        return url;
    }
    
    private ReportDefinitionInfo getReportDefinitionInfoList(final List<ReportDefinition> definitionList, final RandomKeyMapping keyMapping
            , final String timeZone) {
        final ReportDefinitionInfo definitionInfo = new ReportDefinitionInfo();
        final List<DefinitionInfo> definitionInfoList = definitionInfo.getDefinitionList();
        for (ReportDefinition definition : definitionList) {
            final DefinitionInfo newDefinitionInfo = new DefinitionInfo();
            newDefinitionInfo.setRandomId(keyMapping.getRandomString(definition, WebCoreConstants.ID_LOOK_UP_NAME));
            newDefinitionInfo.setTitle(definition.getTitle());
            //            newDefinitionInfo.setNextScheduledTime(formatDateString(definition.getScheduledToRunGmt(), true));
            newDefinitionInfo.setNextScheduledTime(DateUtil.getRelativeTimeString(definition.getScheduledToRunGmt(), timeZone));
            newDefinitionInfo.setNextScheduledMs(definition.getScheduledToRunGmt().getTime());
            definitionInfoList.add(newDefinitionInfo);
        }
        return definitionInfo;
    }
    
    private ReportQueueInfo getReportQueueInfoList(final List<ReportQueue> queueList, final List<ReportHistory> unfinishedHistoryList,
            final RandomKeyMapping keyMapping, final TimeZone timeZone) {
        final ReportQueueInfo queueInfo = new ReportQueueInfo();
        final List<QueueInfo> queueInfoList = queueInfo.getQueueList();
        for (ReportQueue queue : queueList) {
            final QueueInfo newQueueInfo = new QueueInfo();
            newQueueInfo.setRandomId(keyMapping.getRandomString(queue, WebCoreConstants.ID_LOOK_UP_NAME));
            newQueueInfo.setTitle(queue.getReportDefinition().getTitle());
            newQueueInfo.setStatus(queue.getReportStatusType().getId());
            newQueueInfo.setTime(new RelativeDateTime(queue.getExecutionBeginGmt(), timeZone));
            queueInfoList.add(newQueueInfo);
        }
        for (ReportHistory history : unfinishedHistoryList) {
            final QueueInfo newQueueInfo = new QueueInfo();
            newQueueInfo.setRandomId(keyMapping.getRandomString(history, WebCoreConstants.ID_LOOK_UP_NAME));
            newQueueInfo.setTitle(history.getReportDefinition().getTitle());
            newQueueInfo.setStatus(history.getReportStatusType().getId());
            newQueueInfo.setStatusExplanation(history.getStatusExplanation());
            newQueueInfo.setTime(new RelativeDateTime(history.getExecutionBeginGmt(), timeZone));
            queueInfoList.add(newQueueInfo);
            
        }
        return queueInfo;
    }
    
    private ReportRepositoryInfo getReportRepositoryInfoList(final List<ReportRepository> repositoryList, final RandomKeyMapping keyMapping
            , final String timeZone) {
        final ReportRepositoryInfo repositoryInfo = new ReportRepositoryInfo();
        final List<RepositoryInfo> repositoryInfoList = repositoryInfo.getRepositoryList();
        for (ReportRepository repository : repositoryList) {
            final RepositoryInfo newRepositoryInfo = new RepositoryInfo();
            newRepositoryInfo.setRandomId(keyMapping.getRandomString(repository, WebCoreConstants.ID_LOOK_UP_NAME));
            newRepositoryInfo.setTitle(repository.getReportHistory().getReportDefinition().getTitle());
            //            newRepositoryInfo.setCreatedTime(formatDateString(repository.getAddedGmt(), false));
            newRepositoryInfo.setCreatedTime(DateUtil.getRelativeTimeString(repository.getAddedGmt(), timeZone));
            newRepositoryInfo.setCreatedMs(repository.getAddedGmt().getTime());
            newRepositoryInfo.setStatus(repository.getReportHistory().getReportStatusType().getId());
            newRepositoryInfo.setToBeDeleted(repository.isIsSoonToBeDeleted());
            newRepositoryInfo.setReadByUser(repository.isIsReadByUser());
            repositoryInfoList.add(newRepositoryInfo);
        }
        return repositoryInfo;
    }
    
    private ReportQueueAndRepositoryInfo createReportQueueAndRepositoryInfo(final UserAccount userAccount, final HttpServletRequest request,
        final HttpServletResponse response, final RandomKeyMapping keyMapping) {
        
        final String reportingSubClass = this.getClass().getSimpleName();
        final Integer customerId = resolveCustomerId(request, response);
        final TimeZone timezone = TimeZone.getTimeZone(resolveTimeZone(customerId));
        
        final ReportQueueAndRepositoryInfo result = new ReportQueueAndRepositoryInfo();
        result.setReportQueueInfo(new ReportQueueInfo());
        result.setReportRepositoryInfo(new ReportRepositoryInfo());
        
        final ReportSearchCriteria searchCriteria = new ReportSearchCriteria();
        if (((WebUser) WebSecurityUtil.getAuthentication().getPrincipal()).isSystemAdmin()
            && REPORTING_SUBCLASS_SYSTEM_ADMIN.equals(reportingSubClass)) {
            searchCriteria.setSysadmin(true);
            searchCriteria.setUserAccountId(userAccount.getId());
            searchCriteria.setCustomerId(customerId);
        } else if (((WebUser) WebSecurityUtil.getAuthentication().getPrincipal()).isSystemAdmin()
                   && REPORTING_SUBCLASS_CUSTOMER_SYSTEM_ADMIN.equals(reportingSubClass)) {
            searchCriteria.setSysadmin(true);
            searchCriteria.setCustomerId(customerId);
        } else {
            searchCriteria.setSysadmin(false);
            searchCriteria.setUserAccountId(userAccount.getId());
        }
        
        result.getReportQueueInfo().setQueueList(this.reportQueueService.findQueuedReports(searchCriteria, keyMapping, timezone));
        result.getReportQueueInfo().getQueueList().addAll(this.reportHistoryService.findFailedReports(searchCriteria, keyMapping, timezone));
        
        result.getReportRepositoryInfo().setRepositoryList(this.reportRepositoryService.findCompletedReports(searchCriteria, keyMapping, timezone));
        
        return result;
    }
    
    /***********************************************************************************
     * new/edit Report
     ***********************************************************************************/
    
    /**
     * This method will look at the permissions in the WebUser and pass control to the new Report page if allowed.
     * 
     * @return Will return the page for new reports or error if no permissions exist.
     */
    public final String newReport(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model
            , final WebSecurityForm<ReportEditForm> secForm, final String url) {
        final Integer customerId = resolveCustomerId(request, response);
        final Customer customer = this.customerService.findCustomer(customerId);
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final ReportEditForm form = secForm.getWrappedObject();
        prepareForm(form, (ReportDefinition) null, customer.getId(), keyMapping);
        prepareCommonFilters(request, response, model);
        
        model.put(FORM_EDIT, secForm);
        model.put(FORM_REPORT, secForm);
        
        return url;
    }
    
    /**
     * This method will look at the permissions in the WebUser and depending on the report type selected create the data needed to create the new report
     * settings if the user has permission.
     * 
     * @return Will return /reporting/formReport.vm with all filters in the model or error if no permissions exist.
     */
    @SuppressWarnings("checkstyle:designforextension")
    public String newReportSettings(final WebSecurityForm<ReportEditForm> secForm, final BindingResult result, final HttpServletRequest request,
        final HttpServletResponse response, final ModelMap model) {
        Integer reportType = null;
        try {
            // Lazy
            reportType = new Integer(request.getParameter(PARAM_REPORT_TYPE_ID));
        } catch (NullPointerException npe) {
            LOG.debug(npe.getMessage());
        } catch (NumberFormatException nfe) {
            LOG.debug(nfe.getMessage());
        }
        
        if (reportType == null) {
            LOG.warn("+++ Invalid strReportTypeId in ReportingController.newReportSettings() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Integer customerId = resolveCustomerId(request, response);
        final Customer customer = this.customerService.findCustomer(customerId);
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final ReportEditForm reportEditForm = secForm.getWrappedObject();
        reportEditForm.setReportType(reportType);
        final ReportFilter filters = new ReportFilter();
        
        prepareFilters(filters, reportType, customer, keyMapping);
        
        return WidgetMetricsHelper.convertToJson(filters, REPORT_FILTER, false);
    }
    
    /**
     * This method takes a WebSecurityForm from the model and after validating the contents of the report edit form, saves the report definition information to
     * the database.
     * 
     * @param webSecurityForm
     * @param result
     * @param request
     * @param response
     * @param model
     * @return A response of true for a successful save, JSON document for
     *         validation errors, or null for database records not found
     */
    public String saveReport(final WebSecurityForm<ReportEditForm> webSecurityForm, final BindingResult result, final HttpServletRequest request,
        final HttpServletResponse response, final ModelMap model) {
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final ReportEditForm form = webSecurityForm.getWrappedObject();
        
        final Integer primaryDateType = form.getPrimaryDateType();
        if (primaryDateType != null) {
            if (ReportingConstants.REPORT_FILTER_DATE_DATE_TIME == primaryDateType.intValue()) {
                String primaryTimeBegin = form.getPrimaryTimeBegin();
                form.setPrimaryTimeBegin(primaryTimeBegin.split(WebCoreConstants.COLON).length == 3 ? primaryTimeBegin : primaryTimeBegin + WebCoreConstants.COLON + WebCoreConstants.ZERO_SECONDS);
                String primaryTimeEnd = form.getPrimaryTimeEnd();
                form.setPrimaryTimeEnd(primaryTimeEnd.split(WebCoreConstants.COLON).length == 3 ? primaryTimeEnd : primaryTimeEnd + WebCoreConstants.COLON + WebCoreConstants.ZERO_SECONDS);
            }
        }
        
        final Integer secondaryDateType = form.getSecondaryDateType();
        if (secondaryDateType != null) {
            if (ReportingConstants.REPORT_FILTER_DATE_DATE_TIME == secondaryDateType.intValue()) {
                String secondaryTimeBegin = form.getSecondaryTimeBegin();
                form.setSecondaryTimeBegin(secondaryTimeBegin.split(WebCoreConstants.COLON).length == 3 ? secondaryTimeBegin
                        : secondaryTimeBegin + WebCoreConstants.COLON + WebCoreConstants.ZERO_SECONDS);
                String secondaryTimeEnd = form.getSecondaryTimeEnd();
                form.setSecondaryTimeEnd(secondaryTimeBegin.split(WebCoreConstants.COLON).length == 3 ? secondaryTimeEnd : secondaryTimeEnd + WebCoreConstants.COLON + WebCoreConstants.ZERO_SECONDS);
            }
        }
        
        /* validate user input. */
        this.reportValidator.validate(webSecurityForm, result);
        if (!webSecurityForm.getValidationErrorInfo().getErrorStatus().isEmpty()) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        webSecurityForm.resetToken();
        
        /* Prevent User from saving Group By when it is not PDF Report */
        if (!form.getIsPDF()) {
            form.setGroupBy((Integer) null);
        }
        
        if (form.getReportType() == ReportingConstants.REPORT_TYPE_TRANSACTION_RATE) {
            form.setIsSummary(false);
        } else if ((form.getReportType() == ReportingConstants.REPORT_TYPE_TRANSACTION_RATE_SUMMARY)
                   || (form.getReportType() == ReportingConstants.REPORT_TYPE_STALL_REPORTS)
                   || (form.getReportType() == ReportingConstants.REPORT_TYPE_PAY_STATION_SUMMARY)) {
            form.setIsSummary(true);
        }
        
        final Integer customerId = form.getCustomerId() != null ? form.getCustomerId() : userAccount.getCustomer().getId();
        final Customer customer = this.customerService.findCustomer(customerId);
        final String timeZone = resolveTimeZone(customerId);
        
        final String strRandomLocationId = form.getRandomId();
        
        ReportDefinition originalReportDefinition = null;
        
        if (strRandomLocationId != null && !strRandomLocationId.isEmpty()) {
            //this is editing an existing location  
            final int reportDefinitionId = verifyRandomIdAndReturnActual(response, keyMapping, strRandomLocationId);
            
            originalReportDefinition = this.reportDefinitionService.getReportDefinition(reportDefinitionId);
            if (originalReportDefinition == null) {
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return null;
            }
            originalReportDefinition.setLastModifiedGmt(new Date());
            originalReportDefinition.setLastModifiedByUserId(userAccount.getId());
            final ReportStatusType reportStatusType = new ReportStatusType();
            reportStatusType.setId(ReportingConstants.REPORT_STATUS_DELETED);
            originalReportDefinition.setReportStatusType(reportStatusType);
        }
        
        final ReportDefinition reportDefinition = createReportDefinition(customer, userAccount, keyMapping, form, timeZone);
        
        final DateFormat dateTimeFmt = new SimpleDateFormat(FMT_UI_DATE_TIME);
        dateTimeFmt.setTimeZone(TimeZone.getTimeZone(timeZone));
        
        final List<ReportLocationValue> reportLocationValueList = createReportLocationValueList(keyMapping, form);
        
        final Set<ReportEmail> emailList = addEmails(form.getEmailList(), reportDefinition, userAccount, customer, new Date());
        
        Integer accountFilterTypeId = null;
        if (!StringUtils.isBlank(form.getConsumerRandomId())) {
            accountFilterTypeId = verifyRandomIdAndReturnActual(response, keyMapping, form.getConsumerRandomId());
            final Consumer consumer = this.consumerService.findConsumerById(accountFilterTypeId);
            if (consumer != null) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append(WebCoreUtil.returnEmptyIfBlank(consumer.getFirstName())).append(CardProcessingConstants.SPACE_CHARACTER);
                bdr.append(WebCoreUtil.returnEmptyIfBlank(consumer.getLastName()));
                
                reportDefinition.setAccountNameFilter(bdr.toString());
            }
        } else {
            reportDefinition.setAccountNameFilter(this.commonControllerHelper.getMessage("label.all"));
        }
        
        reportDefinition.setAccountFilterTypeId(accountFilterTypeId);
        // Run now report...
        if (ReportingConstants.REPORT_REPEAT_TYPE_ONLINE != reportDefinition.getReportRepeatType().getId()) {
            this.reportDefinitionService.saveOrUpdateReportDefinition(reportDefinition, originalReportDefinition, reportLocationValueList, emailList);
        } else {
            this.reportDefinitionService.saveOrUpdateReportDefinitionForOnline(reportDefinition, originalReportDefinition, reportLocationValueList,
                                                                               emailList, timeZone);
        }
        
        if ((form.getSrcHistoryRandomId() != null) && (form.getSrcHistoryRandomId().length() > 0)) {
            final Integer historyId = (Integer) keyMapping.getKey(form.getSrcHistoryRandomId());
            if (historyId != null) {
                this.reportHistoryService.deleteReportHistory(historyId);
            }
        }
        
        return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(":").append(webSecurityForm.getInitToken()).toString();
    }
    
    protected final Integer verifyRandomIdAndReturnActual(final HttpServletResponse response, final RandomKeyMapping keyMapping,
        final String strRandomReportDefinitionId) {
        
        final String[] msgs = new String[] { "+++ Invalid randomised ReportDefinitionID in ReportingController. +++",
            "+++ Invalid ReportDefinitionID in ReportingController method. +++" };
        return WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, strRandomReportDefinitionId, msgs);
    }
    
    private List<ReportLocationValue> createReportLocationValueList(final RandomKeyMapping keyMapping, final ReportEditForm form) {
        Integer locationType = form.getLocationType();
        final List<ReportLocationValue> result = new ArrayList<ReportLocationValue>();
        
        if ((form.getSelectedOrgRandomIds() != null) && (!form.getSelectedOrgRandomIds().isEmpty())
            && (!"".equals(form.getSelectedOrgRandomIds().get(0)))) {
            for (String randomId : form.getSelectedOrgRandomIds()) {
                final ReportLocationValue reportLocationValue = new ReportLocationValue();
                if ("0".equals(randomId)) {
                    reportLocationValue.setObjectId(0);
                } else {
                    reportLocationValue.setObjectId(keyMapping.getKey(randomId, Customer.class, Integer.class));
                }
                
                reportLocationValue.setObjectType(Customer.class.getName());
                
                result.add(reportLocationValue);
            }
        }
        
        if (locationType == null) {
            locationType = ReportingConstants.REPORT_FILTER_LOCATION_LOCATION_TREE;
        }
        
        final List<String> strRandomIdList = form.getSelectedLocationRandomIds();
        for (String strRandomId : strRandomIdList) {
            final ReportLocationValue reportLocationValue = new ReportLocationValue();
            if ("0".equals(strRandomId)) {
                Class<?> objType = null;
                if (ReportingConstants.REPORT_FILTER_LOCATION_LOCATION_TREE == locationType) {
                    objType = Location.class;
                }
                if (ReportingConstants.REPORT_FILTER_LOCATION_ROUTE_TREE == locationType) {
                    objType = Route.class;
                } else if (ReportingConstants.REPORT_FILTER_LOCATION_PAY_STATION_TREE == locationType) {
                    objType = PointOfSale.class;
                }
                
                if (objType == null) {
                    throw new IllegalArgumentException("Invalid Location Type: " + locationType);
                }
                
                reportLocationValue.setObjectId(0);
                reportLocationValue.setObjectType(objType.getName());
            } else {
                final WebObjectId id = keyMapping.getKeyObject(strRandomId);
                reportLocationValue.setObjectId(Integer.parseInt(id.getId().toString()));
                reportLocationValue.setObjectType(id.getObjectType().getName());
            }
            
            result.add(reportLocationValue);
        }
        
        return result;
    }
    
    private ReportDefinition createReportDefinition(final Customer customer, final UserAccount userAccount, final RandomKeyMapping keyMapping
            , final ReportEditForm form, final String customerTimeZone) {
        final ReportDefinition reportDefinition = new ReportDefinition();
        reportDefinition.setCustomer(customer);
        reportDefinition.setUserAccount(userAccount);
        
        if (customer.getId().intValue() != userAccount.getCustomer().getId().intValue()
            || userAccount.getCustomer().getCustomerType().getId() == WebCoreConstants.CUSTOMER_TYPE_DPT) {
            reportDefinition.setIsSystemAdmin(true);
        }
        
        final TimeZone timeZone = TimeZone.getTimeZone(customerTimeZone);
        final DateFormat dateTimeFmt = new SimpleDateFormat(FMT_UI_DATE_TIME);
        final DateFormat reportDateTimeFmt = new SimpleDateFormat(FMT_REPORT_DATE_TIME);
        dateTimeFmt.setTimeZone(timeZone);
        reportDateTimeFmt.setTimeZone(timeZone);
        
        final Calendar cal = Calendar.getInstance(timeZone);
        
        final ReportType reportType = new ReportType();
        reportType.setId(form.getReportType().shortValue());
        reportDefinition.setReportType(reportType);
        
        reportDefinition.setIsDetailsOrSummary(form.getIsSummary());
        
        final Integer primaryDateType = form.getPrimaryDateType();
        if (primaryDateType != null) {
            reportDefinition.setPrimaryDateFilterTypeId(primaryDateType);
            if (ReportingConstants.REPORT_FILTER_DATE_DATE_TIME == primaryDateType.intValue()) {
                try {
                    reportDefinition.setPrimaryDateRangeBeginGmt(reportDateTimeFmt.parse(form.getPrimaryDateBegin() + SEPARATOR_TIME + form.getPrimaryTimeBegin()));
                    reportDefinition.setPrimaryDateRangeEndGmt(reportDateTimeFmt.parse(form.getPrimaryDateEnd() + SEPARATOR_TIME + form.getPrimaryTimeEnd()));
                } catch (ParseException pe) {
                    throw new IllegalStateException("Un-expected incorrect date/time string !");
                }
            } else if (ReportingConstants.REPORT_FILTER_DATE_MONTH == primaryDateType.intValue()) {
                cal.set(form.getPrimaryYearBegin(), form.getPrimaryMonthBegin(), 1, 0, 0, 0);
                cal.set(Calendar.MILLISECOND, 0);
                reportDefinition.setPrimaryDateRangeBeginGmt(cal.getTime());
                
                cal.set(form.getPrimaryYearEnd(), form.getPrimaryMonthEnd(), 1, 0, 0, 0);
                cal.set(Calendar.MILLISECOND, 0);
                cal.add(Calendar.MONTH, 1);
                cal.add(Calendar.MILLISECOND, -1);
                reportDefinition.setPrimaryDateRangeEndGmt(cal.getTime());
            }
        }
        
        final Integer secondaryDateType = form.getSecondaryDateType();
        
        if (secondaryDateType != null) {
            reportDefinition.setSecondaryDateFilterTypeId(secondaryDateType);
            if (ReportingConstants.REPORT_FILTER_DATE_DATE_TIME == secondaryDateType.intValue()) {
                try {
                    reportDefinition.setSecondaryDateRangeBeginGmt(reportDateTimeFmt.parse(form.getSecondaryDateBegin() + SEPARATOR_TIME
                                                                                     + form.getSecondaryTimeBegin()));
                    reportDefinition.setSecondaryDateRangeEndGmt(reportDateTimeFmt.parse(form.getSecondaryDateEnd() + SEPARATOR_TIME + form.getSecondaryTimeEnd()));
                } catch (ParseException pe) {
                    throw new IllegalStateException("Un-expected incorrect date/time string!");
                }
            } else if (ReportingConstants.REPORT_FILTER_DATE_MONTH == secondaryDateType.intValue()) {
                cal.set(form.getSecondaryYearBegin(), form.getSecondaryMonthBegin(), 1, 0, 0, 0);
                cal.set(Calendar.MILLISECOND, 0);
                reportDefinition.setSecondaryDateRangeBeginGmt(cal.getTime());
                
                cal.set(form.getSecondaryYearEnd(), form.getSecondaryMonthEnd(), 1, 0, 0, 0);
                cal.set(Calendar.MILLISECOND, 0);
                cal.add(Calendar.MONTH, 1);
                cal.add(Calendar.MILLISECOND, -1);
                reportDefinition.setSecondaryDateRangeEndGmt(cal.getTime());
            }
        }
        
        reportDefinition.setSpaceNumberFilterTypeId(form.getSpaceNumberType());
        reportDefinition.setSpaceNumberBeginFilter(form.getSpaceNumberBegin());
        reportDefinition.setSpaceNumberEndFilter(form.getSpaceNumberEnd());
        if (ReportingConstants.REPORT_TYPE_PAYSTATION_INVENTORY != reportType.getId()) {
            reportDefinition.setIsOnlyVisible((form.getShowHiddenPayStations() == null) ? true : !form.getShowHiddenPayStations());
        } else {
            reportDefinition.setIsOnlyVisible(false);
        }
        if (form.getLocationType() == null) {
            reportDefinition.setLocationFilterTypeId(ReportingConstants.REPORT_FILTER_LOCATION_LOCATION_TREE);
        } else {
            reportDefinition.setLocationFilterTypeId(form.getLocationType());
        }
        
        reportDefinition.setCardNumberFilterTypeId(form.getCardNumberType());
        reportDefinition.setCardNumberSpecificFilter(form.getCardNumberSpecific());
        final String strRandomCardTypeId = form.getCardType();
        if (strRandomCardTypeId != null) {
            if ("0".equals(strRandomCardTypeId)) {
                reportDefinition.setCardTypeId(0);
            } else {
                final Object actualCardTypeId = keyMapping.getKey(strRandomCardTypeId);
                if (actualCardTypeId != null) {
                    final int cardTypeId = Integer.parseInt(actualCardTypeId.toString());
                    reportDefinition.setCardTypeId(cardTypeId);
                }
            }
        }
        reportDefinition.setApprovalStatusFilterTypeId(form.getApprovalStatus());
        final String strRandomMerchantAccountId = form.getMerchantAccount();
        if (strRandomMerchantAccountId != null) {
            if ("0".equals(strRandomMerchantAccountId)) {
                reportDefinition.setCardMerchantAccountId(0);
            } else {
                final Object actualMerchantAccountId = keyMapping.getKey(strRandomMerchantAccountId);
                if (actualMerchantAccountId != null) {
                    final int merchantAccountId = Integer.parseInt(actualMerchantAccountId.toString());
                    reportDefinition.setCardMerchantAccountId(merchantAccountId);
                }
            }
        }
        reportDefinition.setLicencePlateFilter(form.getLicencePlate());
        reportDefinition.setTicketNumberFilterTypeId(form.getTicketNumberType());
        reportDefinition.setTicketNumberBeginFilter(form.getTicketNumberBegin());
        reportDefinition.setTicketNumberEndFilter(form.getTicketNumberEnd());
        reportDefinition.setCouponNumberFilterTypeId(form.getCouponNumberType());
        reportDefinition.setCouponNumberSpecificFilter(form.getCouponNumberSpecific());
        reportDefinition.setCouponTypeFilterTypeId(form.getCouponType());
        reportDefinition.setOtherParametersTypeFilterTypeId(form.getOtherParameters());
        reportDefinition.setGroupByFilterTypeId(form.getGroupBy());
        reportDefinition.setSortByFilterTypeId(form.getSortBy());
        reportDefinition.setIsCsvOrPdf(form.getIsPDF());
        
        reportDefinition.setEventDeviceTypeId(keyMapping.getKey(form.getDeviceType(), EventDeviceType.class, Integer.class));
        reportDefinition.setEventSeverityTypeId(keyMapping.getKey(form.getSeverityType(), EventSeverityType.class, Integer.class));
        
        setReportDefinitionCollectionFlags(form, reportDefinition);
        
        final ReportStatusType reportStatus = new ReportStatusType();
        reportStatus.setId(ReportingConstants.REPORT_STATUS_READY);
        reportDefinition.setReportStatusType(reportStatus);
        
        reportDefinition.setCreatedGmt(new Date());
        reportDefinition.setLastModifiedGmt(new Date());
        reportDefinition.setLastModifiedByUserId(userAccount.getId());
        
        // Schedule
        /* Determin Start Time */
        String reportStartTime = form.getStartTime();
        if ((reportStartTime == null) || (reportStartTime.trim().length() <= 0)) {
            if (this.emsPropertiesService != null) {
                reportStartTime = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.REPORTS_QUEUE_TIME);
            } else {
                reportStartTime = DEFAULT_REPORT_SCHEDULE_TIME;
            }
        }
        
        reportDefinition.setReportRepeatTimeOfDay(reportStartTime);
        
        final Date determinedScheduleDate = determineScheduleDate(reportDefinition);
        
        final ReportRepeatType repeatType = new ReportRepeatType();
        
        if (!form.getIsScheduled()) {
            repeatType.setId(ReportingConstants.REPORT_REPEAT_TYPE_ONLINE);
            reportDefinition.setReportRepeatType(repeatType);
            reportDefinition.setTitle(this.reportTypeService.getText(form.getReportType()));
            if (determinedScheduleDate == null) {
                reportDefinition.setScheduledToRunGmt(new Date());
            } else {
                reportDefinition.setScheduledToRunGmt(determinedScheduleDate);
            }
        } else {
            repeatType.setId(form.getRepeatType());
            reportDefinition.setReportRepeatType(repeatType);
            reportDefinition.setTitle(form.getReportTitle());
            if (form.getRepeatOn() != null && !form.getRepeatOn().isEmpty()) {
                final StringBuilder repeatOn = new StringBuilder();
                for (String repeat : form.getRepeatOn()) {
                    repeatOn.append(repeat);
                    repeatOn.append(WebCoreConstants.COMMA);
                }
                
                repeatOn.deleteCharAt(repeatOn.lastIndexOf(WebCoreConstants.COMMA));
                reportDefinition.setReportRepeatDays(repeatOn.toString());
            }
            
            reportDefinition.setReportRepeatFrequency(form.getRepeatEvery().byteValue());
            
            if (determinedScheduleDate != null) {
                reportDefinition.setScheduledToRunGmt(determinedScheduleDate);
            } else {
                final Calendar now = Calendar.getInstance();
                now.setTimeZone(timeZone);
                if ((form.getStartDate() == null) || (form.getStartDate().trim().length() <= 0)) {
                    final DateFormat dateFmt = new SimpleDateFormat(FMT_UI_DATE);
                    dateFmt.setTimeZone(timeZone);
                    
                    form.setStartDate(dateFmt.format(now.getTime()));
                }
                
                final Calendar scheduledTime = Calendar.getInstance();
                scheduledTime.setTimeZone(timeZone);
                try {
                    scheduledTime.setTime(dateTimeFmt.parse(form.getStartDate() + SEPARATOR_TIME + reportStartTime));
                    //                    long millisecsToRun = scheduledTime.getTime().getTime() - now.getTime().getTime();
                    //                    if ((millisecsToRun > 0) && ((millisecsToRun >= 60000L) || (now.get(Calendar.MINUTE) != scheduledTime.get(Calendar.MINUTE)))) {
                    reportDefinition.setScheduledToRunGmt(scheduledTime.getTime());
                    //                    } else {
                    //                        reportDefinition.setScheduledToRunGmt(scheduledTime.getTime());
                    //                        this.reportingUtil.calculateNextReportDate(reportDefinition, scheduledTime.getTime(), timeZone.getDisplayName());
                    //                        scheduledTime.set(Calendar.DAY_OF_YEAR, now.get(Calendar.DAY_OF_YEAR) + 1);
                    //                    }
                } catch (ParseException pe) {
                    throw new IllegalStateException("Un-expected incorrect date/time string!");
                }
            }
            
            final Calendar now = Calendar.getInstance();
            now.setTimeZone(timeZone);
            if (!(reportDefinition.getReportRepeatType().getId() == ReportingConstants.REPORT_REPEAT_TYPE_DAILY && now.getTime()
                    .before(reportDefinition.getScheduledToRunGmt()))) {
                this.reportingUtil.calculateNextReportDate(reportDefinition, reportDefinition.getScheduledToRunGmt(), customerTimeZone, true);
            }
            
        }
        
        return reportDefinition;
    }
    
    private Date determineScheduleDate(final ReportDefinition def) {
        Date result = null;
        
        int dateType = -1;
        if (def.getPrimaryDateFilterTypeId() != null) {
            dateType = def.getPrimaryDateFilterTypeId().intValue();
            if ((ReportingConstants.REPORT_FILTER_DATE_DATE_TIME == dateType) || (ReportingConstants.REPORT_FILTER_DATE_MONTH == dateType)) {
                result = def.getPrimaryDateRangeEndGmt();
            }
        }
        
        if (def.getSecondaryDateFilterTypeId() != null) {
            dateType = def.getSecondaryDateFilterTypeId().intValue();
            if ((ReportingConstants.REPORT_FILTER_DATE_DATE_TIME == dateType) || (ReportingConstants.REPORT_FILTER_DATE_MONTH == dateType)) {
                if ((result == null) || ((def.getSecondaryDateRangeEndGmt() != null) && (result.before(def.getSecondaryDateRangeEndGmt())))) {
                    result = def.getSecondaryDateRangeEndGmt();
                }
            }
        }
        
        return result;
    }
    
    /**
     * This method will open the report settings page look at the permissions in the WebUser and depending on the report type selected create the data needed to
     * create the new report
     * settings if the user has permission.
     * 
     * @return Will return /reporting/editReport.vm with all filters in the model or error if no permissions exist.
     */
    public final String editReportSettings(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        final WebSecurityForm<ReportEditForm> secForm, final String url) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strDefinitionId = request.getParameter(PARAM_DEFINITION_ID);
        final Integer reportDefinitionId = (Integer) keyMapping.getKey(strDefinitionId);
        if (reportDefinitionId == null) {
            LOG.warn("+++ Invalid definitionID in ReportingController.editReport() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return prepareReportDefinitionForEdit(request, response, model, reportDefinitionId, strDefinitionId, (String) null, secForm, url);
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    public String recreateReportSettings(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        final WebSecurityForm<ReportEditForm> secForm, final String url) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strRepositoryId = request.getParameter(PARAM_REPOSITORY_ID);
        final Integer repositoryId = (Integer) keyMapping.getKey(strRepositoryId);
        if (repositoryId == null) {
            LOG.warn("+++ Invalid repositoryID in ReportingController.recreateReportSettings() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final ReportRepository reportRepository = this.reportRepositoryService.getReportRepository(repositoryId);
        final ReportDefinition reportDefinition = reportRepository.getReportHistory().getReportDefinition();
        return prepareReportDefinitionForEdit(request, response, model, reportDefinition.getId(),
                                              keyMapping.getRandomString(reportDefinition, WebCoreConstants.ID_LOOK_UP_NAME), (String) null, secForm,
                                              url);
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    public String fixReportSettings(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        final WebSecurityForm<ReportEditForm> secForm, final String url) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strHistoryId = request.getParameter(PARAM_QUEUE_ID);
        final Integer historyId = (Integer) keyMapping.getKey(strHistoryId);
        if (historyId == null) {
            LOG.warn("+++ Invalid historyID in ReportingController.fixReportSettings() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final ReportHistory history = this.reportHistoryService.getReportHistory(historyId);
        final ReportDefinition reportDefinition = history.getReportDefinition();
        return prepareReportDefinitionForEdit(request, response, model, reportDefinition.getId(),
                                              keyMapping.getRandomString(reportDefinition, WebCoreConstants.ID_LOOK_UP_NAME), strHistoryId, secForm,
                                              url);
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    public String deleteReportHistory(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model, final String url) {
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strHistoryId = request.getParameter(PARAM_QUEUE_ID);
        final Integer historyId = (Integer) keyMapping.getKey(strHistoryId);
        if (historyId == null) {
            LOG.warn("+++ Invalid historyID in ReportingController.deleteReportHistory() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        this.reportHistoryService.deleteReportHistory(historyId);
        
        final ReportQueueAndRepositoryInfo queueAndRepositoryInfoList = createReportQueueAndRepositoryInfo(userAccount, request, response, keyMapping);
        
        //        return WidgetMetricsHelper.convertToJson(queueAndRepositoryInfoList, "ReportQueueAndRepositoryInfo", true);
        model.put(MODEL_QUEUE_INFO_LIST, queueAndRepositoryInfoList);
        return url;
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    public String getAdditionalFilters(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        Integer reportType = null;
        Integer customerId = null;
        try {
            // Doing this because I am lazy :D.
            reportType = new Integer(request.getParameter(PARAM_REPORT_TYPE_ID));
            customerId = keyMapping.getKey(request.getParameter("organizationID"), Customer.class, Integer.class);
        } catch (NullPointerException npe) {
            LOG.debug(npe.getMessage());
        } catch (NumberFormatException nfe) {
            LOG.debug(nfe.getMessage());
        }
        
        if ((reportType == null) || (customerId == null)) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final ReportFilter filters = new ReportFilter();
        prepareCustomerFilters(filters, reportType, customerId, keyMapping);
        return WidgetMetricsHelper.convertToJson(filters, REPORT_FILTER, false);
    }
    
    protected final AdhocReportInfo retrieveAdhocReportInfo(final HttpServletRequest request) {
        WebObjectId reportDefinitionId = null;
        Long queuedGmt = null;
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String params = request.getParameter(AdhocReportInfo.PARAM_ID);
        if (params != null) {
            final String[] buff = params.split("_");
            if (buff.length >= 2) {
                reportDefinitionId = keyMapping.getKeyObject(buff[0]);
                
                try {
                    queuedGmt = Long.parseLong(buff[1]);
                } catch (NumberFormatException nfe) {
                    LOG.debug(nfe.getMessage());
                }
            }
        }
        
        AdhocReportInfo result = null;
        if ((reportDefinitionId != null) && (queuedGmt != null) && (ReportDefinition.class.equals(reportDefinitionId.getObjectType()))) {
            result = new AdhocReportInfo(reportDefinitionId, queuedGmt);
        }
        
        return result;
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    protected String getAdhocReportStatus(final HttpServletRequest request, final HttpServletResponse response
            , final ModelMap model, final AdhocReportInfo ari) {
        final MessageInfo messages = new MessageInfo();
        messages.setError(true);
        
        if ((ari == null) || (ari.getReportDefinitionId() == null) || (!ReportDefinition.class.equals(ari.getReportDefinitionId().getObjectType()))) {
            messages.addMessage(this.reportingUtil.getPropertyEN("error.reporting.adhoc.invalid.params"));
        } else {
            boolean incompleted = true;
            
            final ReportHistory rh = this.reportHistoryService.findRecentlyExecutedReport(ari);
            if (rh != null) {
                switch (rh.getReportStatusType().getId()) {
                    case ReportingConstants.REPORT_STATUS_COMPLETED:
                        ari.setReportHistoryId(new WebObjectId(ReportHistory.class, rh.getId()));
                        incompleted = false;
                        break;
                    case ReportingConstants.REPORT_STATUS_FAILED:
                        messages.addMessage(this.reportingUtil.getPropertyEN("error.reporting.adhoc.invalid.failed"));
                        incompleted = false;
                        break;
                    case ReportingConstants.REPORT_STATUS_CANCELLED:
                        messages.addMessage(this.reportingUtil.getPropertyEN("error.reporting.adhoc.invalid.cancelled"));
                        incompleted = false;
                        break;
                    case ReportingConstants.REPORT_STATUS_DELETED:
                        messages.addMessage(this.reportingUtil.getPropertyEN("error.reporting.adhoc.invalid.deleted"));
                        incompleted = false;
                        break;
                    default:
                        break;
                }
            }
            
            if (incompleted) {
                messages.setError(false);
                
                final StringBuilder callbackURI = new StringBuilder(URI_ADHOC_REPORT_STATUS);
                callbackURI.append(".html?");
                callbackURI.append(AdhocReportInfo.PARAM_ID);
                callbackURI.append("=");
                callbackURI.append(request.getParameter(AdhocReportInfo.PARAM_ID));
                
                messages.checkback(callbackURI.toString(), 3000);
            }
        }
        
        if ((ari == null) || (ari.getReportHistoryId() == null)) {
            return WidgetMetricsHelper.convertToJson(messages, "messages", true);
        } else {
            return WidgetMetricsHelper.convertToJson(ari, "report", true);
        }
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    protected String viewAdhocReport(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        try {
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            
            final String strHistoryId = request.getParameter(PARAM_HISTORY_ID);
            final Integer historyId = (Integer) keyMapping.getKey(strHistoryId);
            
            final ReportRepository reportRepository = this.reportRepositoryService.findByReportHistoryId(historyId);
            final ReportContent reportContent = this.reportRepositoryService.findReportContentByReportRepositoryId(reportRepository.getId());
            final byte[] fileContent = reportContent.getContent();
            
            response.reset();
            response.setHeader("Content-Disposition", "attachment; filename=\"" + reportRepository.getFileName() + "\"");
            if (reportRepository.getReportHistory().getReportDefinition().isIsCsvOrPdf()) {
                response.setContentType("application/pdf");
            } else {
                response.setContentType("text/csv");
            }
            
            final ServletOutputStream outStream = response.getOutputStream();
            
            outStream.write(fileContent);
            outStream.flush();
            outStream.close();
            
            reportRepository.setIsReadByUser(true);
            this.reportRepositoryService.saveOrUpdateReportRepository(reportRepository);
            
            return WebCoreConstants.RESPONSE_TRUE;
        } catch (Exception e) {
            LOG.error("+++ Could not be created report method. +++", e);
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            
            return WebCoreConstants.RESPONSE_FALSE;
        }
    }
    
    protected final boolean hasUserPermissionForAdhocReport(final AdhocReportInfo ari) {
        boolean result = false;
        
        if ((ari != null) && (ari.getReportDefinitionId() != null)) {
            final ReportDefinition rd = this.reportDefinitionService.getReportDefinition((Integer) ari.getReportDefinitionId().getId());
            if (rd != null) {
                result = hasUserPermissionForAdhocReport((int) rd.getReportType().getId());
            }
        }
        
        return result;
    }
    
    protected final boolean hasUserPermissionForAdhocReport(final HttpServletRequest request) {
        boolean result = false;
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strHistoryId = request.getParameter(PARAM_HISTORY_ID);
        final Integer historyId = (Integer) keyMapping.getKey(strHistoryId);
        
        final ReportDefinition rd = this.reportDefinitionService.getReportDefinitionFromHistory(historyId);
        if (rd != null) {
            result = hasUserPermissionForAdhocReport((int) rd.getReportType().getId());
        }
        
        return result;
    }
    
    protected final boolean hasUserPermissionForAdhocReport(final Integer reportTypeId) {
        boolean result = false;
        
        final int[] perms = permissionsByType.get(reportTypeId);
        if (perms != null) {
            int idx = -1;
            while ((!result) && (++idx < perms.length)) {
                result = WebSecurityUtil.hasUserValidPermission(perms[idx]);
            }
        }
        
        return result;
    }
    
    private String prepareReportDefinitionForEdit(final HttpServletRequest request, final HttpServletResponse response
            , final ModelMap model, final int reportDefinitionId, final String randomDefinitionId, final String randomHistoryId
            , final WebSecurityForm<ReportEditForm> secForm, final String url) {
        final Integer customerId = resolveCustomerId(request, response);
        final Customer customer = this.customerService.findCustomer(customerId);
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final ReportDefinition reportDefinition = this.reportDefinitionService.getReportDefinition(reportDefinitionId);
        if (reportDefinition == null) {
            LOG.warn("+++ Invalid reportDefinitionId in ReportingController.editReportSettings() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final ReportEditForm reportEditForm = secForm.getWrappedObject();
        final ReportFilter filters = new ReportFilter();
        
        prepareForm(reportEditForm, reportDefinition, customer.getId(), keyMapping);
        prepareCommonFilters(request, response, model);
        prepareFilters(filters, reportEditForm.getReportType(), customer, keyMapping);
        
        reportEditForm.setSrcHistoryRandomId(randomHistoryId);
        
        model.put(FORM_EDIT, secForm);
        
        model.put("reportFilters", WidgetMetricsHelper.convertToJson(filters, REPORT_FILTER, false));
        model.put(FORM_REPORT, secForm);
        
        return url;
    }
    
    /**
     * This method gets a set of user defined email addresses
     * 
     * @param newEmails
     *            user's input email addresses
     * @param definition
     *            ReportDefinition object
     * @param userAccount
     *            UserAccount object
     * @param customer
     *            Customer object
     * @param currentTime
     *            current server time for update
     * @return Set of AlertEmail object
     */
    private Set<ReportEmail> addEmails(final List<String> newEmails, final ReportDefinition definition, final UserAccount userAccount, final Customer customer,
                                       final Date currentTime) {
        final Set<ReportEmail> existingEmails = new HashSet<ReportEmail>();
        if (newEmails != null) {
            try {
                for (String em : newEmails) {
                    final String email = URLEncoder.encode(em, WebSecurityConstants.URL_ENCODING_LATIN1);
                    boolean exist = false;
                    for (ReportEmail re : existingEmails) {
                        if (URLDecoder.decode(re.getCustomerEmail().getEmail(), WebSecurityConstants.URL_ENCODING_LATIN1).equals(email)) {
                            exist = true;
                            break;
                        }
                    }
                    if (exist) {
                        continue;
                    } else {
                        final CustomerEmail customerEmail = new CustomerEmail(customer, email, currentTime, userAccount.getId(), false);
                        final ReportEmail reportEmail = new ReportEmail(definition, customerEmail, currentTime, userAccount.getId());
                        reportEmail.setCustomerEmail(customerEmail);
                        existingEmails.add(reportEmail);
                    }
                }
            } catch (UnsupportedEncodingException e) {
                LOG.error("+++ Failed to encode and decode email when updating report.", e);
            }
        }
        
        return existingEmails;
    }
    
    private void prepareForm(final ReportEditForm form, final ReportDefinition reportDefinition, final Integer customerId, final RandomKeyMapping keyMapping) {
        
        final CustomerProperty timeZoneProperty = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        final TimeZone timeZone = TimeZone.getTimeZone(timeZoneProperty.getPropertyValue());
        final Calendar cal = Calendar.getInstance(timeZone);
        
        final DateFormat dateFmt = new SimpleDateFormat(WebCoreConstants.DATE_UI_FORMAT);
        dateFmt.setTimeZone(timeZone);
        
        final DateFormat timeFmt = new SimpleDateFormat(FMT_UI_TIME);
        timeFmt.setTimeZone(timeZone);
        
        if (reportDefinition == null) {
            defaultValues(form, cal, dateFmt, timeFmt);
        } else {
            populateForm(form, reportDefinition, keyMapping, cal, dateFmt, timeFmt);
        }
    }
    
    private void defaultValues(final ReportEditForm reportEditForm, final Calendar now, final DateFormat dateFmt, final DateFormat timeFmt) {
        // Default Date Options
        final String nowDateStr = dateFmt.format(now.getTime());
        
        reportEditForm.setRepeatEvery(1);
        
        // Default Schedule Time
        String defaultScheduleTime = DEFAULT_REPORT_SCHEDULE_TIME;
        if (this.emsPropertiesService != null) {
            defaultScheduleTime = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.REPORTS_QUEUE_TIME);
            if (defaultScheduleTime.length() == 4) {
                defaultScheduleTime = "0" + defaultScheduleTime;
            }
        }
        
        reportEditForm.setPrimaryDateType(ReportingConstants.REPORT_FILTER_DATE_TODAY);
        reportEditForm.setSecondaryDateType(ReportingConstants.REPORT_FILTER_DATE_TODAY);
        reportEditForm.setStartTime(defaultScheduleTime);
        
        // Default Schedule Date
        final String nowTimeStr = timeFmt.format(now.getTime());
        if (nowTimeStr.compareTo(defaultScheduleTime) < 0) {
            reportEditForm.setStartDate(nowDateStr);
        } else {
            now.add(Calendar.DAY_OF_YEAR, 1);
            reportEditForm.setStartDate(dateFmt.format(now.getTime()));
            now.add(Calendar.DAY_OF_YEAR, -1);
        }
        
        // Location Default
        reportEditForm.setSelectedLocationRandomIds(new ArrayList<String>(1));
        reportEditForm.getSelectedLocationRandomIds().add("0");
        
        reportEditForm.setSelectedOrgRandomIds(new ArrayList<String>(1));
        reportEditForm.getSelectedOrgRandomIds().add("0");
    }
    
    private void populateForm(final ReportEditForm form, final ReportDefinition reportDefinition, final RandomKeyMapping keyMapping, final Calendar cal, final DateFormat dateFmt,
        final DateFormat timeFmt) {
        if (reportDefinition.getReportRepeatType().getId() > ReportingConstants.REPORT_REPEAT_TYPE_ONLINE) {
            form.setIsScheduled(true);
            form.setRepeatType(reportDefinition.getReportRepeatType().getId());
            form.setReportTitle(reportDefinition.getTitle());
            form.setStartDate(dateFmt.format(reportDefinition.getScheduledToRunGmt()));
            form.setStartTime(reportDefinition.getReportRepeatTimeOfDay());
            if (reportDefinition.getReportRepeatFrequency() != null) {
                form.setRepeatEvery((int) reportDefinition.getReportRepeatFrequency().byteValue());
            }
            
            if (reportDefinition.getReportRepeatDays() != null) {
                form.setRepeatOn(Arrays.asList(reportDefinition.getReportRepeatDays().split(WebCoreConstants.COMMA)));
            }
        } else {
            form.setIsScheduled(false);
        }
        
        form.setRandomId(keyMapping.getRandomString(ReportDefinition.class, reportDefinition.getId()));
        form.setReportType((int) reportDefinition.getReportType().getId());
        form.setIsSummary(reportDefinition.isIsDetailsOrSummary());
        
        final Integer primaryDateType = reportDefinition.getPrimaryDateFilterTypeId();
        if (primaryDateType != null) {
            form.setPrimaryDateType(primaryDateType);
            if (ReportingConstants.REPORT_FILTER_DATE_DATE_TIME == primaryDateType.intValue()) {
                form.setPrimaryDateBegin(dateFmt.format(reportDefinition.getPrimaryDateRangeBeginGmt()));
                form.setPrimaryTimeBegin(timeFmt.format(reportDefinition.getPrimaryDateRangeBeginGmt()));
                form.setPrimaryDateEnd(dateFmt.format(reportDefinition.getPrimaryDateRangeEndGmt()));
                form.setPrimaryTimeEnd(timeFmt.format(reportDefinition.getPrimaryDateRangeEndGmt()));
            } else if (ReportingConstants.REPORT_FILTER_DATE_MONTH == primaryDateType.intValue()) {
                cal.setTime(reportDefinition.getPrimaryDateRangeBeginGmt());
                form.setPrimaryMonthBegin(cal.get(Calendar.MONTH));
                form.setPrimaryYearBegin(cal.get(Calendar.YEAR));
                
                cal.setTime(reportDefinition.getPrimaryDateRangeEndGmt());
                form.setPrimaryMonthEnd(cal.get(Calendar.MONTH));
                form.setPrimaryYearEnd(cal.get(Calendar.YEAR));
            }
        }
        
        final Integer secondaryDateType = reportDefinition.getSecondaryDateFilterTypeId();
        if (secondaryDateType != null) {
            form.setSecondaryDateType(secondaryDateType);
            if (ReportingConstants.REPORT_FILTER_DATE_DATE_TIME == secondaryDateType.intValue()) {
                form.setSecondaryDateBegin(dateFmt.format(reportDefinition.getSecondaryDateRangeBeginGmt()));
                form.setSecondaryTimeBegin(timeFmt.format(reportDefinition.getSecondaryDateRangeBeginGmt()));
                form.setSecondaryDateEnd(dateFmt.format(reportDefinition.getSecondaryDateRangeEndGmt()));
                form.setSecondaryTimeEnd(timeFmt.format(reportDefinition.getSecondaryDateRangeEndGmt()));
            } else if (ReportingConstants.REPORT_FILTER_DATE_MONTH == secondaryDateType.intValue()) {
                cal.setTime(reportDefinition.getSecondaryDateRangeBeginGmt());
                form.setSecondaryMonthBegin(cal.get(Calendar.MONTH));
                form.setSecondaryYearBegin(cal.get(Calendar.YEAR));
                
                cal.setTime(reportDefinition.getSecondaryDateRangeEndGmt());
                form.setSecondaryMonthEnd(cal.get(Calendar.MONTH));
                form.setSecondaryYearEnd(cal.get(Calendar.YEAR));
            }
        }
        
        form.setSpaceNumberType(reportDefinition.getSpaceNumberFilterTypeId());
        form.setSpaceNumberBegin(reportDefinition.getSpaceNumberBeginFilter());
        form.setSpaceNumberEnd(reportDefinition.getSpaceNumberEndFilter());
        form.setCardNumberType(reportDefinition.getCardNumberFilterTypeId());
        form.setCardNumberSpecific(reportDefinition.getCardNumberSpecificFilter());
        form.setApprovalStatus(reportDefinition.getApprovalStatusFilterTypeId());
        form.setLicencePlate(reportDefinition.getLicencePlateFilter());
        form.setTicketNumberType(reportDefinition.getTicketNumberFilterTypeId());
        form.setTicketNumberBegin(reportDefinition.getTicketNumberBeginFilter());
        form.setTicketNumberEnd(reportDefinition.getTicketNumberEndFilter());
        form.setCouponNumberType(reportDefinition.getCouponNumberFilterTypeId());
        form.setCouponNumberSpecific(reportDefinition.getCouponNumberSpecificFilter());
        form.setCouponType(reportDefinition.getCouponTypeFilterTypeId());
        form.setAccount(reportDefinition.getAccountFilterTypeId());
        
        if (reportDefinition.getAccountFilterTypeId() != null) {
            form.setConsumerRandomId(keyMapping.getRandomString(Consumer.class, reportDefinition.getAccountFilterTypeId()));
            form.setAccountFilterValue(reportDefinition.getAccountNameFilter());
        }
        
        form.setOtherParameters(reportDefinition.getOtherParametersTypeFilterTypeId());
        form.setGroupBy(reportDefinition.getGroupByFilterTypeId());
        form.setSortBy(reportDefinition.getSortByFilterTypeId());
        form.setIsPDF(reportDefinition.isIsCsvOrPdf());
        
        form.setMerchantAccount("0");
        if ((reportDefinition.getCardMerchantAccountId() != null) && (reportDefinition.getCardMerchantAccountId() != 0)) {
            form.setMerchantAccount(keyMapping.getRandomString(MerchantAccount.class, reportDefinition.getCardMerchantAccountId()));
        }
        
        form.setCardType("0");
        if ((reportDefinition.getCardTypeId() != null) && (reportDefinition.getCardTypeId() != 0)) {
            if (isCreditCardReport(reportDefinition.getReportType().getId())) {
                form.setCardType(keyMapping.getRandomString(CreditCardType.class, reportDefinition.getCardTypeId()));
            } else {
                form.setCardType(keyMapping.getRandomString(CustomerCardType.class, reportDefinition.getCardTypeId()));
            }
        }
        
        form.setShowHiddenPayStations(!reportDefinition.isIsOnlyVisible());
        form.setLocationType(reportDefinition.getLocationFilterTypeId());
        if ((reportDefinition.getReportLocationValues() != null) && (!reportDefinition.getReportLocationValues().isEmpty())) {
            form.setSelectedOrgRandomIds(new ArrayList<String>());
            form.setSelectedLocationRandomIds(new ArrayList<String>());
            for (ReportLocationValue rlv : reportDefinition.getReportLocationValues()) {
                Class<?> objType = null;
                try {
                    objType = Class.forName(rlv.getObjectType());
                } catch (ClassNotFoundException cnfe) {
                    throw new IllegalStateException("Could not load domain class for Object type: " + rlv.getObjectType());
                }
                
                String randomId = "0";
                if (rlv.getObjectId() != 0) {
                    randomId = keyMapping.getRandomString(objType, rlv.getObjectId());
                }
                
                if (objType == Customer.class) {
                    form.getSelectedOrgRandomIds().add(randomId);
                } else {
                    form.getSelectedLocationRandomIds().add(randomId);
                }
            }
        }
        
        form.setDeviceType("");
        if (reportDefinition.getEventDeviceTypeId() != null) {
            form.setDeviceType(keyMapping.getRandomString(EventDeviceType.class, reportDefinition.getEventDeviceTypeId()));
        }
        
        // 0 Also mean clear ! but we are not include that in the report anyway !
        form.setSeverityType("0");
        if ((reportDefinition.getEventSeverityTypeId() != null) && (reportDefinition.getEventSeverityTypeId() != 0)) {
            form.setSeverityType(keyMapping.getRandomString(EventSeverityType.class, reportDefinition.getEventSeverityTypeId()));
        }
        
        setFormCollectionFlags(form, reportDefinition);
        
        final List<String> emailsList = this.reportDefinitionService.findEmailsByReportDefinitionId(reportDefinition.getId());
        if (emailsList != null) {
            try {
                for (int emailIdx = -1; ++emailIdx < emailsList.size();) {
                    emailsList.set(emailIdx, URLDecoder.decode(emailsList.get(emailIdx), WebSecurityConstants.URL_ENCODING_LATIN1));
                }
                
                form.setEmailList(emailsList);
            } catch (UnsupportedEncodingException e) {
                LOG.error("+++ Failed to decode email when retrieving report.", e);
            }
        }
    }
    
    private boolean isCreditCardReport(final short reportType) {
        return (reportType == ReportingConstants.REPORT_TYPE_TRANSACTION_CREDIT_CARD)
                || (reportType == ReportingConstants.REPORT_TYPE_TRANSACTION_CC_PROCESSING)
                || (reportType == ReportingConstants.REPORT_TYPE_TRANSACTION_CC_REFUND)
                || (reportType == ReportingConstants.REPORT_TYPE_TRANSACTION_CC_RETRY);
    }
    
    private void setFormCollectionFlags(final ReportEditForm form, final ReportDefinition reportDefinition) {
        if (reportDefinition.getReportCollectionFilterType() != null) {
            final short collectionFilter = reportDefinition.getReportCollectionFilterType().getId();
            form.setForRunningTotal((collectionFilter & ReportingConstants.COLLECTION_FILTER_BITMASK_RUNNING_TOTAL_DOLLAR)
                                    == ReportingConstants.COLLECTION_FILTER_BITMASK_RUNNING_TOTAL_DOLLAR);
            form.setForCoinCount((collectionFilter & ReportingConstants.COLLECTION_FILTER_BITMASK_COIN_CANISTER_COUNT)
                                 == ReportingConstants.COLLECTION_FILTER_BITMASK_COIN_CANISTER_COUNT);
            form.setForCoinAmount((collectionFilter & ReportingConstants.COLLECTION_FILTER_BITMASK_COIN_CANISTER_DOLLARS)
                                  == ReportingConstants.COLLECTION_FILTER_BITMASK_COIN_CANISTER_DOLLARS);
            form.setForBillCount((collectionFilter & ReportingConstants.COLLECTION_FILTER_BITMASK_BILL_STACKER_COUNT)
                                 == ReportingConstants.COLLECTION_FILTER_BITMASK_BILL_STACKER_COUNT);
            form.setForBillAmount((collectionFilter & ReportingConstants.COLLECTION_FILTER_BITMASK_BILL_STACKER_DOLLARS)
                                  == ReportingConstants.COLLECTION_FILTER_BITMASK_BILL_STACKER_DOLLARS);
            form.setForCardCount((collectionFilter & ReportingConstants.COLLECTION_FILTER_BITMASK_UNSETTLED_CREDIT_CARD_COUNT)
                                 == ReportingConstants.COLLECTION_FILTER_BITMASK_UNSETTLED_CREDIT_CARD_COUNT);
            form.setForCardAmount((collectionFilter & ReportingConstants.COLLECTION_FILTER_BITMASK_UNSETTLED_CREDIT_CARD_DOLLARS)
                                  == ReportingConstants.COLLECTION_FILTER_BITMASK_UNSETTLED_CREDIT_CARD_DOLLARS);
            form.setForOverdueCollection((collectionFilter & ReportingConstants.COLLECTION_FILTER_BITMASK_OVERDUE_COLLECTION)
                                         == ReportingConstants.COLLECTION_FILTER_BITMASK_OVERDUE_COLLECTION);
        }
    }
    
    private void setReportDefinitionCollectionFlags(final ReportEditForm form, final ReportDefinition reportDefinition) {
        short collectionFilter = 0;
        if (form.isForRunningTotal()) {
            collectionFilter += ReportingConstants.COLLECTION_FILTER_BITMASK_RUNNING_TOTAL_DOLLAR;
        }
        if (form.isForCoinCount()) {
            collectionFilter += ReportingConstants.COLLECTION_FILTER_BITMASK_COIN_CANISTER_COUNT;
        }
        if (form.isForCoinAmount()) {
            collectionFilter += ReportingConstants.COLLECTION_FILTER_BITMASK_COIN_CANISTER_DOLLARS;
        }
        if (form.isForBillCount()) {
            collectionFilter += ReportingConstants.COLLECTION_FILTER_BITMASK_BILL_STACKER_COUNT;
        }
        if (form.isForBillAmount()) {
            collectionFilter += ReportingConstants.COLLECTION_FILTER_BITMASK_BILL_STACKER_DOLLARS;
        }
        if (form.isForCardCount()) {
            collectionFilter += ReportingConstants.COLLECTION_FILTER_BITMASK_UNSETTLED_CREDIT_CARD_COUNT;
        }
        if (form.isForCardAmount()) {
            collectionFilter += ReportingConstants.COLLECTION_FILTER_BITMASK_UNSETTLED_CREDIT_CARD_DOLLARS;
        }
        if (form.isForOverdueCollection()) {
            collectionFilter += ReportingConstants.COLLECTION_FILTER_BITMASK_OVERDUE_COLLECTION;
        }
        if (collectionFilter != 0) {
            reportDefinition.setReportCollectionFilterType(this.reportCollectionFilterTypeService.getReportCollectionFilterType(collectionFilter));
        }
    }
    
    private void prepareCommonFilters(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        final Integer customerId = resolveCustomerId(request, response);
        final Customer customer = this.customerService.findCustomer(customerId);
        
        int reportTypeFilterType = ReportingConstants.REPORT_FILTER_TYPE_REPORT_TYPE_CHILD_CUSTADMIN;
        if ((customer != null) && (customer.getCustomerType() != null)) {
            if (WebCoreConstants.CUSTOMER_TYPE_PARENT == customer.getCustomerType().getId()) {
                reportTypeFilterType = ReportingConstants.REPORT_FILTER_TYPE_REPORT_TYPE_PARENT_CUSTADMIN;
            } else if (WebCoreConstants.CUSTOMER_TYPE_DPT == customer.getCustomerType().getId()) {
                reportTypeFilterType = ReportingConstants.REPORT_FILTER_TYPE_REPORT_TYPE_SYSADMIN;
            }
        }
        
        List<ReportUIFilterType> reportTypeFilterList = this.reportingUtil.getFilterList(ReportingConstants.REPORT_TYPE_COMMON, reportTypeFilterType);
        if (WebSecurityUtil.getUserAccount().getCustomer().getCustomerType() != null
            && WebCoreConstants.CUSTOMER_TYPE_DPT != WebSecurityUtil.getUserAccount().getCustomer().getCustomerType().getId()) {
            for (int i = reportTypeFilterList.size() - 1; 0 <= i; i--) {
                if (reportTypeFilterList.get(i).getValue() == ReportingConstants.REPORT_TYPE_TRANSACTION_DELAY) {
                    reportTypeFilterList = new ArrayList<ReportUIFilterType>(reportTypeFilterList);
                    reportTypeFilterList.remove(i);
                    break;
                }
            }
        }
        final List<ReportUIFilterType> repeatTypeFilterList = this.reportingUtil.getFilterList(ReportingConstants.REPORT_TYPE_COMMON,
                                                                                         ReportingConstants.REPORT_FILTER_TYPE_REPORT_REPEAT_TYPE);
        model.put("reportTypeFilterList", reportTypeFilterList);
        model.put("repeatTypeFilterList", repeatTypeFilterList);
    }
    
    private void prepareFilters(final ReportFilter filters, final Integer reportType, final Customer customer, final RandomKeyMapping keyMapping) {
        final int reportTypeId = reportType.intValue();
        final boolean needOrgTree = (customer.getCustomerType() != null) && (WebCoreConstants.CUSTOMER_TYPE_PARENT == customer.getCustomerType().getId());
        
        filters.setPrimaryDateFilters(this.reportingUtil.getFilterList(reportType, ReportingConstants.REPORT_FILTER_TYPE_PRIMARY_DATE));
        filters.setSecondaryDateFilters(this.reportingUtil.getFilterList(reportType, ReportingConstants.REPORT_FILTER_TYPE_SECONDARY_DATE));
        filters.setSpaceNumberFilters(this.reportingUtil.getFilterList(reportType, ReportingConstants.REPORT_FILTER_TYPE_SPACE_NUMBER));
        
        filters.setCardNumberFilters(this.reportingUtil.getFilterList(reportType, ReportingConstants.REPORT_FILTER_TYPE_CARD_NUMBER));
        filters.setApprovalStatusFilters(this.reportingUtil.getFilterList(reportType, ReportingConstants.REPORT_FILTER_TYPE_APPROVAL_STATUS));
        filters.setTicketNumberFilters(this.reportingUtil.getFilterList(reportType, ReportingConstants.REPORT_FILTER_TYPE_TICKET_NUMBER));
        filters.setCouponNumberFilters(this.reportingUtil.getFilterList(reportType, ReportingConstants.REPORT_FILTER_TYPE_COUPON_NUMBER));
        filters.setCouponTypeFilters(this.reportingUtil.getFilterList(reportType, ReportingConstants.REPORT_FILTER_TYPE_COUPON_TYPE));
        filters.setOtherParameterFilters(this.reportingUtil.getFilterList(reportType, ReportingConstants.REPORT_FILTER_TYPE_OTHER_PARAMETERS));
        filters.setGroupByFilters(this.reportingUtil.getFilterList(reportType, ReportingConstants.REPORT_FILTER_TYPE_GROUP_BY));
        filters.setSortByFilters(this.reportingUtil.getFilterList(reportType, ReportingConstants.REPORT_FILTER_TYPE_SORT_BY));
        filters.setRepeatTypeFilters(this.reportingUtil.getFilterList(reportType, ReportingConstants.REPORT_FILTER_TYPE_REPORT_REPEAT_TYPE));
        
        if (ReportingConstants.REPORT_TYPE_STALL_REPORTS != reportTypeId) {
            filters.setLocationTypeFilters(this.reportingUtil.getFilterList(reportType, ReportingConstants.REPORT_FILTER_TYPE_LOCATION));
        }
        if (needOrgTree) {
            filters.setOrganizationTree(prepareTreeRoot(this.customerService.getCustomerTree(customer.getId(), keyMapping), "0", "All Organizations"));
            prepareCustomerFilters(filters, reportTypeId, null, keyMapping);
        } else {
            prepareCustomerFilters(filters, reportTypeId, customer.getId(), keyMapping);
        }
    }
    
    private void prepareCustomerFilters(final ReportFilter filters, final int reportType, final Integer customerId, final RandomKeyMapping keyMapping) {
        LocationTree locationRoot = null;
        if (customerId == null) {
            locationRoot = new LocationTree();
        } else {
            locationRoot = this.locationService.getLocationPayStationTreeByCustomerId(customerId, true, false, false, keyMapping);
        }
        
        filters.setLocationPOSTree(prepareTreeRoot(locationRoot, "0", "All Locations"));
        
        if (ReportingConstants.REPORT_TYPE_STALL_REPORTS != reportType) {
            LocationTree routeRoot = null;
            if (customerId == null) {
                routeRoot = new LocationTree();
            } else {
                routeRoot = this.locationService.getRoutePayStationTreeByCustomerId(customerId, false, false, false, keyMapping);
            }
            
            filters.setRoutePOSTree(prepareTreeRoot(routeRoot, "0", "All Routes"));
        }
        
        switch (reportType) {
            case ReportingConstants.REPORT_TYPE_TRANSACTION_VALUE_CARD:
                prepareCustomerCardTypeFilter(filters, customerId, keyMapping);
                prepareMerchantAccountFilter(filters, customerId, keyMapping);
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_CREDIT_CARD:
                prepareCreditCardTypeFilter(filters, customerId, keyMapping);
                prepareMerchantAccountFilter(filters, customerId, keyMapping);
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_CC_PROCESSING:
                prepareCreditCardTypeFilter(filters, customerId, keyMapping);
                prepareMerchantAccountFilter(filters, customerId, keyMapping);
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_CC_RETRY:
                prepareCreditCardTypeFilter(filters, customerId, keyMapping);
                break;
            case ReportingConstants.REPORT_TYPE_TRANSACTION_CC_REFUND:
                prepareCreditCardTypeFilter(filters, customerId, keyMapping);
                prepareMerchantAccountFilter(filters, customerId, keyMapping);
                break;
            case ReportingConstants.REPORT_TYPE_MAINTENANCE_SUMMARY:
                prepareSeverityTypeFilter(filters, keyMapping);
                prepareDeviceTypeFilter(filters, keyMapping);
                break;
            default:
                break;
        }
    }
    
    private void prepareCreditCardTypeFilter(final ReportFilter filters, final Integer customerId, final RandomKeyMapping keyMapping) {
        List<FilterDTO> cardTypes = createFilterList(new FilterDTO("0", WebCoreConstants.ALL_STRING));
        cardTypes = this.creditCardTypeService.getFilters(cardTypes, keyMapping, true);
        
        filters.setCardTypeListFilters(cardTypes);
    }
    
    private void prepareCustomerCardTypeFilter(final ReportFilter filters, final Integer customerId, final RandomKeyMapping keyMapping) {
        List<FilterDTO> cardTypes = createFilterList(new FilterDTO("0", WebCoreConstants.ALL_STRING));
        if (customerId != null) {
            cardTypes = this.customerCardTypeService.getValueCardTypeFiltersWithDeletedFlag(cardTypes, customerId, true, keyMapping);
        }
        
        filters.setCardTypeListFilters(cardTypes);
    }
    
    private void prepareSeverityTypeFilter(final ReportFilter filters, final RandomKeyMapping keyMapping) {
        filters.setSeverityTypeListFilters(this.reportingUtil.getSeverityFilters(createFilterList(new FilterDTO("0", WebCoreConstants.ALL_STRING)), keyMapping));
    }
    
    private void prepareDeviceTypeFilter(final ReportFilter filters, final RandomKeyMapping keyMapping) {
        filters.setDeviceTypeListFilters(this.reportingUtil.getModuleFilters(createFilterList(new FilterDTO("", WebCoreConstants.ALL_STRING)), keyMapping));
    }
    
    private void prepareMerchantAccountFilter(final ReportFilter filters, final Integer customerId, final RandomKeyMapping keyMapping) {
        List<FilterDTO> maList = createFilterList(new FilterDTO("0", WebCoreConstants.ALL_STRING));
        if (customerId != null) {
            maList = this.merchantAccountService.getMerchantAccountFiltersByCustomerId(maList, customerId, keyMapping);
        }
        
        filters.setMerchantAccountListFilters(maList);
    }
    
    private LocationTree prepareTreeRoot(final LocationTree root, final String rootValue, final String rootLabel) {
        LocationTree result = null;
        if ((root.getChildren() != null) && (!root.getChildren().isEmpty())) {
            result = root;
            result.setRandomId(rootValue);
            result.setName(rootLabel);
        }
        
        return result;
    }
    
    private List<FilterDTO> createFilterList(final FilterDTO defaultFilter) {
        final List<FilterDTO> result = new ArrayList<FilterDTO>();
        if (defaultFilter != null) {
            result.add(defaultFilter);
        }
        
        return result;
    }
    
    private Integer resolveCustomerId(final HttpServletRequest request, final HttpServletResponse response) {
        final Customer customer = WebSecurityUtil.getUserAccount().getCustomer();
        
        Integer customerId = WebCoreUtil
                .verifyRandomIdAndReturnActualWithoutError(response, SessionTool.getInstance(request).getKeyMapping(),
                                                           request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID),
                                                           new String[] { "+++ Invalid randomised customerID in LocationsController. +++" });
        if (customerId == null) {
            customerId = customer.getId();
            
        }
        
        return customerId;
    }
    
    private String resolveTimeZone(final Integer customerId) {
        String timeZone = null;
        final Customer customer = WebSecurityUtil.getUserAccount().getCustomer();
        if ((customer.getCustomerType() == null) || (customer.getCustomerType().getId() != WebCoreConstants.CUSTOMER_TYPE_DPT)) {
            
            timeZone = WebSecurityUtil.getCustomerTimeZone();
            ;
        } else {
            final CustomerProperty timeZoneProperty = this.customerAdminService
                    .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
            timeZone = timeZoneProperty.getPropertyValue();
        }
        
        return timeZone;
    }
    
    private ReportDefinitionInfo getReportDefinitionInfo(final Integer userAccountId, final Integer customerId, final RandomKeyMapping keyMapping) {
        List<ReportDefinition> definitionList = null;
        final String reportingSubClass = this.getClass().getSimpleName();
        if (((WebUser) WebSecurityUtil.getAuthentication().getPrincipal()).isSystemAdmin()
            && REPORTING_SUBCLASS_SYSTEM_ADMIN.equals(reportingSubClass)) {
            definitionList = this.reportDefinitionService.findScheduledReportsByCustomerIdAndUserAccountId(userAccountId, customerId);
        } else if (((WebUser) WebSecurityUtil.getAuthentication().getPrincipal()).isSystemAdmin()
                   && REPORTING_SUBCLASS_CUSTOMER_SYSTEM_ADMIN.equals(reportingSubClass)) {
            definitionList = this.reportDefinitionService.findScheduledReportsByCustomerId(customerId);
        } else {
            definitionList = this.reportDefinitionService.findScheduledReportsByUserAccountId(userAccountId);
            
        }
        return getReportDefinitionInfoList(definitionList, keyMapping, resolveTimeZone(customerId));
        
    }
    
    /**
     * The search of Account Name in Coupon Summary Report should search against First, Last Names and Email after the user has at least typed
     * 3 characters into this field. The business side should be a list of
     * - Account Random ID
     * - First Name
     * - Last Name
     * - Email Address
     * 
     * @return String JSON document of SearchConsumerResult object. Returns empty string if no result is found.
     */
    public String searchConsumerForAccountName(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        Integer customerId;
        final Customer customer = WebSecurityUtil.getUserAccount().getCustomer();
        if ((customer.getCustomerType() == null) || (customer.getCustomerType().getId() != WebCoreConstants.CUSTOMER_TYPE_DPT)) {
            customerId = customer.getId();
        } else {
            customerId = WebCoreUtil.verifyRandomIdAndReturnActual(response, SessionTool.getInstance(request).getKeyMapping(),
                                                                   request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID),
                                                                   new String[] { "+++ Invalid randomised customerID in ReportingController. +++" });
        }
        
        if (customerId == null) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        String search = request.getParameter("search");
        if (StringUtils.isNotBlank(search)) {
            search = search.trim();
            if (!search.matches(WebCoreConstants.REGEX_PAYSTATION_TEXT)) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            final List<Consumer> consumers = this.consumerService.findConsumersByFirstLastNameOrEmail(customerId, search);
            final SearchConsumerResult searchConsumerResult = convertConsumerResult(consumers, keyMapping);
            return WidgetMetricsHelper.convertToJson(searchConsumerResult, "searchConsumerResult", true);
        }
        return WebCoreConstants.EMPTY_STRING;
    }
    
    private SearchConsumerResult convertConsumerResult(final List<Consumer> consumersList, final RandomKeyMapping keyMapping) {
        final SearchConsumerResult result = new SearchConsumerResult();
        for (Consumer consumer : consumersList) {
            result.addSearchConsumerResult(new SearchConsumerResultSetting(keyMapping.getRandomString(Consumer.class, consumer.getId()), consumer
                    .getFirstName(), consumer.getLastName(), returnEmptyStringIfNull(consumer.getEmailAddress())));
        }
        return result;
    }
    
    private String returnEmptyStringIfNull(final EmailAddress emailAddress) {
        if (emailAddress == null) {
            return WebCoreConstants.EMPTY_STRING;
        }
        if (StringUtils.isBlank(emailAddress.getEmail())) {
            return WebCoreConstants.EMPTY_STRING;
        }
        return emailAddress.getEmail();
    }
}
