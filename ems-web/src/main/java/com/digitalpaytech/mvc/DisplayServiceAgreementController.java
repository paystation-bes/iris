package com.digitalpaytech.mvc;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAgreement;
import com.digitalpaytech.domain.ServiceAgreement;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerAgreementService;
import com.digitalpaytech.service.ServiceAgreementService;

@Controller
public class DisplayServiceAgreementController {

	@Autowired
	private ServiceAgreementService serviceAgreementService;
	
	@Autowired
	private CustomerAgreementService customerAgreementService;
	
	@Autowired
	private CustomerAdminService customerAdminService;
	
	public void setServiceAgreementService(
			ServiceAgreementService serviceAgreementService) {
		this.serviceAgreementService = serviceAgreementService;
	}

	public void setCustomerAgreementService(
			CustomerAgreementService customerAgreementService) {
		this.customerAgreementService = customerAgreementService;
	}

	public void setCustomerAdminService(
			CustomerAdminService customerAdminService) {
		this.customerAdminService = customerAdminService;
	}

	@RequestMapping(value = "/secure/termsofService.html")
	public String showServiceAgreement(HttpServletRequest request, ModelMap model) {
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		WebUser webUser = (WebUser) authentication.getPrincipal();
		Integer customerId = webUser.getCustomerId();
		
		Customer customer = customerAdminService.findCustomerByCustomerId(customerId);
		if(customer.getParentCustomer() != null){
			customerId = customer.getParentCustomer().getId();
		}
		
		ServiceAgreement serviceAgreement = serviceAgreementService.findLatestServiceAgreementByCustomerId(customerId);
		CustomerAgreement customerAgreement = customerAgreementService.findCustomerAgreementByCustomerId(customerId);
		
		model.put("agreementContent", new String(serviceAgreement.getContent()));
		model.put("customerAgreementName", customerAgreement.getName());
		model.put("customerAgreementTitle", customerAgreement.getTitle());
		model.put("customerAgreementOrganization", customerAgreement.getOrganization());
		
		return "termsofService";
	}
}
