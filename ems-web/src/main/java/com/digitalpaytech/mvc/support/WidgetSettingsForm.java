package com.digitalpaytech.mvc.support;

public class WidgetSettingsForm implements java.io.Serializable {
	/**
     * 
     */
    private static final long serialVersionUID = -2270412057463147078L;
	private String randomId;
	private String sectionRandomId;
	private String widgetName;
	private String metricName;
	private String range;
	private String filter;
	private Integer length; // used for N values in top and bottom filter types
	private String tier1;
	private String tier2;
	private String tier3;
	private String tier1Attr;
	private String tier2Attr;
	private String tier3Attr;
	private String chartType;
	private String targetValue;

	
	public WidgetSettingsForm() {
	    this.widgetName = "";
	    this.filter = "";
	    this.range = "";
	    this.length = 0;
	    this.tier1 = "";
	    this.tier2 = "";
	    this.tier3 = "";
	    this.tier1Attr = "";
	    this.tier2Attr = "";
	    this.tier3Attr = "";
	    this.chartType = "";
	    this.targetValue = "";
	}
	
	public WidgetSettingsForm(String randomId, String sectionRandomId, String widgetName, String filter, String range,
			Integer length, String tier1, String tier2, String tier3,
            String tier1Attr, String tier2Attr, String tier3Attr,
            String targetValue, String chartType) {
	    this.randomId = randomId;
	    this.sectionRandomId = sectionRandomId;
		this.widgetName = widgetName;
	    this.filter = filter;
	    this.range = range;
	    this.length = length;
	    this.tier1 = tier1;
	    this.tier2 = tier2;
	    this.tier3 = tier3;
	    this.tier1Attr = tier1Attr;
	    this.tier2Attr = tier2Attr;
	    this.tier3Attr = tier3Attr;
	    this.targetValue = targetValue;
	    this.chartType = chartType;
    }

	@Override
	public String toString() {
		StringBuilder bdr = new StringBuilder();
		
	    bdr.append("randomId: ").append(randomId).append("widgetName: ").append(widgetName).append(", filter: ").append(filter).append("\n");
	    bdr.append("length: ").append(length).append(", tier1: ").append(tier1).append(", tier2: ").append(tier2).append(", tier3: ").append(tier3).append("\n"); 
	    bdr.append("tier1Attr: ").append(tier1Attr).append(", tier2Attr: ").append(tier2Attr).append(", tier3Attr: ").append(tier3Attr).append("\n");
	    bdr.append("targetValue: ").append(targetValue).append(", chartType: ").append(chartType).append("\n");
	    bdr.append("range: ").append(range).append(", metricName: ").append(metricName).append(", targetValue: ").append(targetValue);
	    return bdr.toString();
	}
	
	public String getWidgetName() {
    	return widgetName;
    }
	public void setWidgetName(String widgetName) {
    	this.widgetName = widgetName;
    }
	public String getFilter() {
    	return filter;
    }
	public void setFilter(String filter) {
    	this.filter = filter;
    }
	public String getRange() {
    	return range;
    }
	public void setRange(String range) {
    	this.range = range;
    }
	public Integer getLength() {
    	return length;
    }
	public void setLength(Integer length) {
    	this.length = length;
    }
	public String getTier1() {
    	return tier1;
    }
	public void setTier1(String tier1) {
    	this.tier1 = tier1;
    }
	public String getTier2() {
    	return tier2;
    }
	public void setTier2(String tier2) {
    	this.tier2 = tier2;
    }
	public String getTier3() {
    	return tier3;
    }
	public void setTier3(String tier3) {
    	this.tier3 = tier3;
    }
	public String getTier1Attr() {
    	return tier1Attr;
    }
	public void setTier1Attr(String tier1Attr) {
    	this.tier1Attr = tier1Attr;
    }
	public String getTier2Attr() {
    	return tier2Attr;
    }
	public void setTier2Attr(String tier2Attr) {
    	this.tier2Attr = tier2Attr;
    }
	public String getTier3Attr() {
    	return tier3Attr;
    }
	public void setTier3Attr(String tier3Attr) {
    	this.tier3Attr = tier3Attr;
    }
	public String getChartType() {
    	return chartType;
    }
	public void setChartType(String chartType) {
    	this.chartType = chartType;
    }
	public String getMetricName() {
    	return metricName;
    }
	public void setMetricName(String metricName) {
    	this.metricName = metricName;
    }
	public String getRandomId() {
    	return randomId;
    }
	public void setRandomId(String randomId) {
    	this.randomId = randomId;
    }
	public String getSectionRandomId() {
    	return sectionRandomId;
    }
	public void setSectionRandomId(String sectionRandomId) {
    	this.sectionRandomId = sectionRandomId;
    }
	public String getTargetValue() {
    	return targetValue;
    }
	public void setTargetValue(String targetValue) {
    	this.targetValue = targetValue;
    }
}
