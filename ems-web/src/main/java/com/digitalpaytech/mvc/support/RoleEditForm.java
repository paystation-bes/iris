package com.digitalpaytech.mvc.support;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public class RoleEditForm implements Serializable {
    
    private static final long serialVersionUID = -2565286456227470802L;
    private String randomizedRoleId;
    private Integer roleId;
    private String roleName;
    private boolean statusEnabled;
    private Integer typeId;
    private List<String> randomizedPermissionIds;
    private Collection<Integer> permissionIds;
    
    public final String getRandomizedRoleId() {
        return this.randomizedRoleId;
    }
    
    public final void setRandomizedRoleId(final String randomizedRoleId) {
        this.randomizedRoleId = randomizedRoleId;
    }
    
    public final Integer getRoleId() {
        return this.roleId;
    }
    
    public final void setRoleId(final Integer roleId) {
        this.roleId = roleId;
    }
    
    public final String getRoleName() {
        return this.roleName;
    }
    
    public final void setRoleName(final String roleName) {
        this.roleName = roleName;
    }
    
    public final boolean getStatusEnabled() {
        return this.statusEnabled;
    }
    
    public final void setStatusEnabled(final boolean statusEnabled) {
        this.statusEnabled = statusEnabled;
    }
    
    public final Integer getTypeId() {
        return this.typeId;
    }
    
    public final void setTypeId(final Integer typeId) {
        this.typeId = typeId;
    }
    
    public final List<String> getRandomizedPermissionIds() {
        return this.randomizedPermissionIds;
    }
    
    public final void setRandomizedPermissionIds(final List<String> randomizedPermissionIds) {
        this.randomizedPermissionIds = randomizedPermissionIds;
    }
    
    public final Collection<Integer> getPermissionIds() {
        return this.permissionIds;
    }
    
    public final void setPermissionIds(final Collection<Integer> permissionIds) {
        this.permissionIds = permissionIds;
    }
}
