package com.digitalpaytech.mvc.support;

import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.FlexWSUserAccount;
import com.digitalpaytech.util.RandomKeyMapping;

@Component("flexCredentialsHelper")
public class FlexHelper {
    public static final String FORM_CREDENTIALS = "flexCredentialsForm";
    
    private static final String MASK_PASSWD = "********";
    
    public FlexHelper() {
        
    }
    
    public final FlexWSUserAccountForm transform(final FlexWSUserAccount flexAccount, final FlexWSUserAccountForm flexAccountForm
            , final RandomKeyMapping keyMapping) {
        if (flexAccount == null) {
            flexAccountForm.setMissing(true);
        } else {
            flexAccountForm.setMissing(false);
            
            flexAccountForm.setRandomId(keyMapping.getRandomString(FlexWSUserAccount.class, flexAccount.getId()));
            
            flexAccountForm.setUrl(flexAccount.getUrl());
            flexAccountForm.setUsername(flexAccount.getUsername());
            flexAccountForm.setPassword(MASK_PASSWD);
        }
        
        return flexAccountForm;
    }
    
    public final FlexWSUserAccount transform(final FlexWSUserAccountForm flexAccountForm, final FlexWSUserAccount flexAccount) {
        flexAccount.setUrl(flexAccountForm.getUrl());
        flexAccount.setUsername(flexAccountForm.getUsername());
        if (hasPasswordChanged(flexAccountForm)) {
            flexAccount.setPassword(flexAccountForm.getPassword());
        }
        
        return flexAccount;
    }
    
    public boolean hasPasswordChanged(final FlexWSUserAccountForm flexAccountForm) {
        return !MASK_PASSWD.equals(flexAccountForm.getPassword());
    }
}
