package com.digitalpaytech.mvc.support;

import java.io.Serializable;

public class ChangePasswordForm implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4587715076380001695L;
	private String newPassword;
	private String newPasswordAgain;
	
	public ChangePasswordForm() {
	}
	
	public ChangePasswordForm(String newPassword, String newPasswordAgain) {
		this.newPassword = newPassword;
		this.newPasswordAgain = newPasswordAgain;
	}
	
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	public String getNewPasswordAgain() {
		return newPasswordAgain;
	}
	public void setNewPasswordAgain(String newPasswordAgain) {
		this.newPasswordAgain = newPasswordAgain;
	}
}
