package com.digitalpaytech.mvc.support;

import java.io.Serializable;
import java.util.List;

import com.digitalpaytech.util.PostToken;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.mvc.support.ValidationErrorInfo;

public class WebSecurityForm<W extends Serializable> extends WebSecurityBaseActionForm {
	
	private static final long serialVersionUID = -7222634132075174106L;
	private W wrappedObject;
	private PostToken initToken;
	private String postToken = null;
	private ValidationErrorInfo validationErrorInfo;

	public WebSecurityForm() {
		super();
		
		this.initToken = new PostToken();
		this.wrappedObject = null;
		this.validationErrorInfo = new ValidationErrorInfo(this.initToken);
		resetToken();
	}

	public WebSecurityForm(Object primaryKey, W wrappedObject) {
		super(primaryKey);
		
		this.initToken = new PostToken();
		this.wrappedObject = wrappedObject;
		this.validationErrorInfo = new ValidationErrorInfo(this.initToken);
		resetToken();
	}
	
	public WebSecurityForm(Object primaryKey, W wrappedObject, PostToken token) {
		super(primaryKey);
		
		this.initToken = token;
		this.wrappedObject = wrappedObject;
		this.validationErrorInfo = new ValidationErrorInfo(token);
	}

	public void resetToken() {
		initToken.reset();
		postToken = null;
		createAction = WebSecurityConstants.ACTION_FAILURE;
		updateAction = WebSecurityConstants.ACTION_FAILURE;		
	}

	public W getWrappedObject() {
		return wrappedObject;
	}

	public void setWrappedObject(W wrappedObject) {
		this.wrappedObject = wrappedObject;
	}

	public String getPostToken() {
		return postToken;
	}

	public void setPostToken(String postToken) {
		this.postToken = postToken;
	}

	public String getInitToken() {
		return initToken.toString();
	}

	// For unit test ONLY.
	public void setInitToken(String initToken) {
		this.initToken = new PostToken(initToken);
	}

	public boolean isCorrectPostToken(String token) {
		return this.initToken.matches(token);
	}

	public boolean isCorrectPostToken() {
		return isCorrectPostToken(this.postToken);
	}
	
	public ValidationErrorInfo getValidationErrorInfo() {
		return validationErrorInfo;
	}
	
	public void clearValidationErrors() {
		validationErrorInfo.clear();
	}
	
	public void setErrorStatus(List<FormErrorStatus> errorStatus) {
		this.validationErrorInfo.setErrorStatus(errorStatus);
	}
	
	public void addErrorStatus(FormErrorStatus status) {
		this.validationErrorInfo.addErrorStatus(status);
	}
	
	public void addAllErrorStatus(List<FormErrorStatus> statusList) {
		this.validationErrorInfo.addAllErrorStatus(statusList);
	}
	
	public void removeAllErrorStatus() {
		this.validationErrorInfo.getErrorStatus().clear();
	}
	
	public void removeErrorStatus(int index) {
		this.validationErrorInfo.removeErrorStatus(index);
	}
}
