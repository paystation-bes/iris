package com.digitalpaytech.mvc.support;

import java.io.Serializable;

public class RateProfileSearchForm implements Serializable {
    
    private static final long serialVersionUID = -2309616108460323786L;
    
    private Integer permitIssueTypeId;
    private Boolean isPublished;
    private String itemId;
    private String selectedId;
    
    private Integer page;
    private Integer itemsPerPage;
    private String dataKey;
    
    private String targetRandomId;
    
    public RateProfileSearchForm() {
        
    }
    
    public final Integer getPermitIssueTypeId() {
        return this.permitIssueTypeId;
    }
    
    public final void setPermitIssueTypeId(final Integer permitIssueTypeId) {
        this.permitIssueTypeId = permitIssueTypeId;
    }
    
    public final Boolean getIsPublished() {
        return this.isPublished;
    }
    
    public final void setIsPublished(final Boolean isPublished) {
        this.isPublished = isPublished;
    }
    
    public final Integer getPage() {
        return this.page;
    }
    
    public final void setPage(final Integer page) {
        this.page = page;
    }
    
    public final Integer getItemsPerPage() {
        return this.itemsPerPage;
    }
    
    public final void setItemsPerPage(final Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }
    
    public final String getDataKey() {
        return this.dataKey;
    }
    
    public final void setDataKey(final String dataKey) {
        this.dataKey = dataKey;
    }
    
    public final String getItemId() {
        return this.itemId;
    }
    
    public final void setItemId(final String itemId) {
        this.itemId = itemId;
    }
    
    public final String getSelectedId() {
        return this.selectedId;
    }
    
    public final void setSelectedId(final String selectedId) {
        this.selectedId = selectedId;
    }
    
    public final String getTargetRandomId() {
        return this.targetRandomId;
    }
    
    public final void setTargetRandomId(final String targetRandomId) {
        this.targetRandomId = targetRandomId;
    }
}
