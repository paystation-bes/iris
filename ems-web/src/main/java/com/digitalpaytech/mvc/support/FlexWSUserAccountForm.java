package com.digitalpaytech.mvc.support;

import java.io.Serializable;

public class FlexWSUserAccountForm implements Serializable {
    private static final long serialVersionUID = -5963783283069339313L;
    
    private String randomId;
    
    private String username;
    private String password;
    private String url;
    
    private boolean missing;
    
    public FlexWSUserAccountForm() {
        
    }

    public final String getRandomId() {
        return this.randomId;
    }

    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }

    public final String getUsername() {
        return this.username;
    }

    public final void setUsername(final String username) {
        this.username = username;
    }

    public final String getPassword() {
        return this.password;
    }

    public final void setPassword(final String password) {
        this.password = password;
    }

    public final String getUrl() {
        return this.url;
    }

    public final void setUrl(final String url) {
        this.url = url;
    }

    public final boolean isMissing() {
        return this.missing;
    }

    public final void setMissing(final boolean missing) {
        this.missing = missing;
    }
}
