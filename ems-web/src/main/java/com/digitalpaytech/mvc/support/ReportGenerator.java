package com.digitalpaytech.mvc.support;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.PooledResource;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.type.WhenNoDataTypeEnum;
import net.sf.jasperreports.engine.util.JRLoader;

@Component("reportGenerator")
public class ReportGenerator {
    public static final String PARAM_REPORT_TITLE = "ReportTitle";
    public static final String PARAM_REPORT_FILE_NAME = "ReportFileName";
    public static final String PARAM_PRINTED_DATE = "PrintedDate";
    public static final String PARAM_CUSTOMER_TIME_ZONE = "TimeZone";
    
    public static final String DEFAULT_REPORT_FILE_NAME = "Report";
    
    public final void generatePDF(final HttpServletResponse response, final String jasperFile, final Map<String, Object> parameters
        , final Object data) throws IOException {
        final List<Object> list = new ArrayList<Object>(1);
        list.add(data);
        
        generatePDF(response, jasperFile, parameters, list);
    }
    
    public final void generatePDF(final HttpServletResponse response, final String jasperFile, final Map<String, Object> parameters
            , final Collection<?> data) throws IOException {
        final Map<String, Object> reportParams = prepareReportParameters(parameters);
        
        String outputName = (String) parameters.get(PARAM_REPORT_FILE_NAME);
        if (outputName == null) {
            outputName = DEFAULT_REPORT_FILE_NAME;
        }
        
        outputName = URLEncoder.encode(outputName, "UTF-8");
        
        response.reset();
        response.setHeader("Content-Disposition", "attachment; filename=\"" + outputName + ".pdf\"");
        response.setContentType("application/pdf");
        
        final ServletOutputStream outStream = response.getOutputStream();
        outStream.flush();
        
        try {
            final JasperPrint jasperPrint = JasperFillManager.fillReport(loadReport(jasperFile), reportParams, wrap(data));
            JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
        } catch (JRException jre) {
            throw new IOException("Failed to generate report", jre);
        }
        
        outStream.flush();
        outStream.close();
    }

    protected final JasperReport loadReport(final String jasperFile) throws IOException, JRException {
        final JasperReport jasperReport = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(jasperFile));
        jasperReport.setWhenNoDataType(WhenNoDataTypeEnum.ALL_SECTIONS_NO_DETAIL);
        
        return jasperReport;
    }
    
    protected final Map<String, Object> prepareReportParameters(final Map<String, Object> parameters) {
        String timeZoneId = null;
        
        Map<String, Object> params = parameters;
        if (params == null) {
            params = new HashMap<String, Object>(2);
        } else {
            timeZoneId = (String) parameters.get(PARAM_CUSTOMER_TIME_ZONE);
        }
        
        if (timeZoneId == null) {
            timeZoneId = DateUtil.GMT;
        }
        
        final PooledResource<DateFormat> dateFmt = DateUtil.takeDateFormat(DateUtil.REPORTS_OUTPUT_FORMAT, timeZoneId);
        params.put(PARAM_PRINTED_DATE, dateFmt.get().format(new Date()));
        dateFmt.close();
        
        return parameters;
    }
    
    protected final JRDataSource wrap(final Collection<?> data) {
        return new JRBeanCollectionDataSource(data);
    }
}
