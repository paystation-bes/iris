package com.digitalpaytech.mvc.support;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.stereotype.Component;

@Component("propertiesRetrieval")
public class PropertiesRetrieval implements MessageSourceAware  {

	@Autowired
	private MessageSource messageSource;
	
	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;		
	}

	public String getPropertyEN(String messageKey, String[] args){		
		return messageSource.getMessage(messageKey, args, null);				
	}

	public String getProperty(String messageKey, String[] args, Locale locale){		
		return messageSource.getMessage(messageKey, args, locale);	
	}
}
