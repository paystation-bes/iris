package com.digitalpaytech.mvc.support;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Transient;

public class RateProfileEditForm implements Serializable {
    
    private static final long serialVersionUID = -7475762143303976788L;
    private String randomId;
    private String name;
    private Integer permitIssueTypeId;
    
    @Transient
    private transient Integer rateProfileId;
    
    private int tempRateId;
    
    public RateProfileEditForm() {
        
    }
    
    public RateProfileEditForm(String randomId) {
        this.randomId = randomId;
    }
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final String getName() {
        return this.name;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    public final Integer getPermitIssueTypeId() {
        return this.permitIssueTypeId;
    }
    
    public final void setPermitIssueTypeId(final Integer permitIssueTypeId) {
        this.permitIssueTypeId = permitIssueTypeId;
    }
    
    public final Integer getRateProfileId() {
        return this.rateProfileId;
    }
    
    public final void setRateProfileId(final Integer rateProfileId) {
        this.rateProfileId = rateProfileId;
    }
    
    public final int generateTemporaryRateId() {
        return --this.tempRateId;
    }
    
}
