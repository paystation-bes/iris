package com.digitalpaytech.mvc.support;

import java.io.Serializable;
import java.util.List;

public class AlertEditForm implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -2818839383707909115L;
    private String randomizedAlertId;
    private Integer alertId;
    private String alertName;
    private boolean status;
    private Short alertTypeId;
    private String alertThresholdExceed;
    private Short alertThresholdExceedDisplay;
    private String randomizedRouteId;
    private Integer routeId;
    private List<String> alertContacts;
    private boolean isDelayed;
    private int delayedByMinutes;
    
    public String getRandomizedAlertId() {
        return this.randomizedAlertId;
    }
    
    public void setRandomizedAlertId(final String randomizedAlertId) {
        this.randomizedAlertId = randomizedAlertId;
    }
    
    public Integer getAlertId() {
        return this.alertId;
    }
    
    public void setAlertId(final Integer alertId) {
        this.alertId = alertId;
    }
    
    public String getAlertName() {
        return this.alertName;
    }
    
    public void setAlertName(final String alertName) {
        this.alertName = alertName;
    }
    
    public boolean getStatus() {
        return this.status;
    }
    
    public void setStatus(final boolean status) {
        this.status = status;
    }
    
    public Short getAlertTypeId() {
        return this.alertTypeId;
    }
    
    public void setAlertTypeId(final Short alertTypeId) {
        this.alertTypeId = alertTypeId;
    }
    
    public String getAlertThresholdExceed() {
        return this.alertThresholdExceed;
    }
    
    public void setAlertThresholdExceed(final String alertThresholdExceed) {
        this.alertThresholdExceed = alertThresholdExceed;
    }
    
    public String getRandomizedRouteId() {
        return this.randomizedRouteId;
    }
    
    public void setRandomizedRouteId(final String randomizedRouteId) {
        this.randomizedRouteId = randomizedRouteId;
    }
    
    public Integer getRouteId() {
        return this.routeId;
    }
    
    public void setRouteId(final Integer routeId) {
        this.routeId = routeId;
    }
    
    public List<String> getAlertContacts() {
        return this.alertContacts;
    }
    
    public void setAlertContacts(final List<String> alertContacts) {
        this.alertContacts = alertContacts;
    }
    
    public Short getAlertThresholdExceedDisplay() {
        return this.alertThresholdExceedDisplay;
    }
    
    public void setAlertThresholdExceedDisplay(final Short alertThresholdExceedDisplay) {
        this.alertThresholdExceedDisplay = alertThresholdExceedDisplay;
    }
    
    public boolean getIsDelayed() {
        return this.isDelayed;
    }
    
    public void setIsDelayed(final boolean isDelayed) {
        this.isDelayed = isDelayed;
    }
    
    public int getDelayedByMinutes() {
        return this.delayedByMinutes;
    }
    
    public void setDelayedByMinutes(final int delayedByMinutes) {
        this.delayedByMinutes = delayedByMinutes;
    }
}
