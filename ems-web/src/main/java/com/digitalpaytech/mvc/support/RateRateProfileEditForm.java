package com.digitalpaytech.mvc.support;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Transient;

import com.digitalpaytech.domain.Rate;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias(value = "RateRateProfileEditForm")
public class RateRateProfileEditForm implements Serializable {
    
    private static final long serialVersionUID = 7922744760011464087L;
    private String rateRateProfileRandomId;
    private String rateProfileRandomId;
    private String rateRandomId;
    private String rateName;
    private String rateTypeName;
    private boolean useRateBlending;
    private boolean useOverride;
    private String description;
    private String availabilityStartDate;
    private String availabilityEndDate;
    private String availabilityStartTime;
    private String availabilityEndTime;
    private Integer flatRateType;
    private Integer numberOfDays;
    private Integer numberOfHours;
    private Integer numberOfMins;
    private boolean useMaxExpiry;
    private Integer expiryDayType;
    private String expiryTime;
    private boolean useAdvPurchase;
    private boolean useScheduled = true;
    private boolean useSunday;
    private boolean useMonday;
    private boolean useTuesday;
    private boolean useWednesday;
    private boolean useThursday;
    private boolean useFriday;
    private boolean useSaturday;
    
    private boolean isToBeDeleted;
    
    @Transient
    private Date startDate;
    @Transient
    private Date endDate;
    @Transient
    private Date startTime;
    @Transient
    private Date endTime;
    @Transient
    private transient Rate rate;
    
    public RateRateProfileEditForm() {
        
    }
    
    public final String getRateRateProfileRandomId() {
        return this.rateRateProfileRandomId;
    }
    
    public final void setRateRateProfileRandomId(final String rateRateProfileRandomId) {
        this.rateRateProfileRandomId = rateRateProfileRandomId;
    }
    
    public final String getRateProfileRandomId() {
        return this.rateProfileRandomId;
    }
    
    public final void setRateProfileRandomId(final String rateProfileRandomId) {
        this.rateProfileRandomId = rateProfileRandomId;
    }
    
    public final String getRateRandomId() {
        return this.rateRandomId;
    }
    
    public final void setRateRandomId(final String rateRandomId) {
        this.rateRandomId = rateRandomId;
    }
    
    public final String getRateName() {
        return this.rateName;
    }
    
    public final void setRateName(final String rateName) {
        this.rateName = rateName;
    }
    
    public final String getRateTypeName() {
        return this.rateTypeName;
    }
    
    public final void setRateTypeName(final String rateTypeName) {
        this.rateTypeName = rateTypeName;
    }
    
    public final boolean getUseRateBlending() {
        return this.useRateBlending;
    }
    
    public final void setUseRateBlending(final boolean useRateBlending) {
        this.useRateBlending = useRateBlending;
    }
    
    public final boolean getUseOverride() {
        return this.useOverride;
    }
    
    public final void setUseOverride(final boolean useOverride) {
        this.useOverride = useOverride;
    }
    
    public final String getDescription() {
        return this.description;
    }
    
    public final void setDescription(final String description) {
        this.description = description;
    }
    
    public final String getAvailabilityStartDate() {
        return this.availabilityStartDate;
    }
    
    public final void setAvailabilityStartDate(final String availabilityStartDate) {
        this.availabilityStartDate = availabilityStartDate;
    }
    
    public final String getAvailabilityEndDate() {
        return this.availabilityEndDate;
    }
    
    public final void setAvailabilityEndDate(final String availabilityEndDate) {
        this.availabilityEndDate = availabilityEndDate;
    }
    
    public final String getAvailabilityStartTime() {
        return this.availabilityStartTime;
    }
    
    public final void setAvailabilityStartTime(final String availabilityStartTime) {
        this.availabilityStartTime = availabilityStartTime;
    }
    
    public final String getAvailabilityEndTime() {
        return this.availabilityEndTime;
    }
    
    public final void setAvailabilityEndTime(final String availabilityEndTime) {
        this.availabilityEndTime = availabilityEndTime;
    }
    
    public final Integer getFlatRateType() {
        return this.flatRateType;
    }
    
    public final void setFlatRateType(final Integer flatRateType) {
        this.flatRateType = flatRateType;
    }
    
    public final Integer getNumberOfDays() {
        return this.numberOfDays;
    }
    
    public final void setNumberOfDays(final Integer numberOfDays) {
        this.numberOfDays = numberOfDays;
    }
    
    public final Integer getNumberOfHours() {
        return this.numberOfHours;
    }
    
    public final void setNumberOfHours(final Integer numberOfHours) {
        this.numberOfHours = numberOfHours;
    }
    
    public final Integer getNumberOfMins() {
        return this.numberOfMins;
    }
    
    public final void setNumberOfMins(final Integer numberOfMins) {
        this.numberOfMins = numberOfMins;
    }
    
    public final boolean getUseMaxExpiry() {
        return this.useMaxExpiry;
    }
    
    public final void setUseMaxExpiry(final boolean useMaxExpiry) {
        this.useMaxExpiry = useMaxExpiry;
    }
    
    public final Integer getExpiryDayType() {
        return this.expiryDayType;
    }
    
    public final void setExpiryDayType(final Integer expiryDayType) {
        this.expiryDayType = expiryDayType;
    }
    
    public final String getExpiryTime() {
        return this.expiryTime;
    }
    
    public final void setExpiryTime(final String expiryTime) {
        this.expiryTime = expiryTime;
    }
    
    public final boolean getUseAdvPurchase() {
        return this.useAdvPurchase;
    }
    
    public final void setUseAdvPurchase(final boolean useAdvPurchase) {
        this.useAdvPurchase = useAdvPurchase;
    }
    
    public final boolean getUseScheduled() {
        return this.useScheduled;
    }
    
    public final void setUseScheduled(final boolean useScheduled) {
        this.useScheduled = useScheduled;
    }
    
    public final boolean getUseSunday() {
        return this.useSunday;
    }
    
    public final void setUseSunday(final boolean useSunday) {
        this.useSunday = useSunday;
    }
    
    public final boolean getUseMonday() {
        return this.useMonday;
    }
    
    public final void setUseMonday(final boolean useMonday) {
        this.useMonday = useMonday;
    }
    
    public final boolean getUseTuesday() {
        return this.useTuesday;
    }
    
    public final void setUseTuesday(final boolean useTuesday) {
        this.useTuesday = useTuesday;
    }
    
    public final boolean getUseWednesday() {
        return this.useWednesday;
    }
    
    public final void setUseWednesday(final boolean useWednesday) {
        this.useWednesday = useWednesday;
    }
    
    public final boolean getUseThursday() {
        return this.useThursday;
    }
    
    public final void setUseThursday(final boolean useThursday) {
        this.useThursday = useThursday;
    }
    
    public final boolean getUseFriday() {
        return this.useFriday;
    }
    
    public final void setUseFriday(final boolean useFriday) {
        this.useFriday = useFriday;
    }
    
    public final boolean getUseSaturday() {
        return this.useSaturday;
    }
    
    public final void setUseSaturday(final boolean useSaturday) {
        this.useSaturday = useSaturday;
    }
    
    public final Date getStartDate() {
        return this.startDate;
    }
    
    public final void setStartDate(final Date startDate) {
        this.startDate = startDate;
    }
    
    public final Date getEndDate() {
        return this.endDate;
    }
    
    public final void setEndDate(final Date endDate) {
        this.endDate = endDate;
    }
    
    public final Date getStartTime() {
        return this.startTime;
    }
    
    public final void setStartTime(final Date startTime) {
        this.startTime = startTime;
    }
    
    public final Date getEndTime() {
        return this.endTime;
    }
    
    public final void setEndTime(final Date endTime) {
        this.endTime = endTime;
    }
    
    public final boolean getIsToBeDeleted() {
        return this.isToBeDeleted;
    }
    
    public final void setIsToBeDeleted(final boolean isToBeDeleted) {
        this.isToBeDeleted = isToBeDeleted;
    }
    
    public final Rate getRate() {
        return this.rate;
    }
    
    public final void setRate(final Rate rate) {
        this.rate = rate;
    }
    
}
