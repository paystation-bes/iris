package com.digitalpaytech.mvc.support;

import java.io.Serializable;

import javax.persistence.Transient;

import com.digitalpaytech.constants.SortOrder;

public class PayStationRecentActivityFilterForm implements Serializable {
    
    private static final long serialVersionUID = -4415028300270036003L;
    private String randomId;
    private String moduleRandomId;
    private String severityRandomId;
    private String status;
    private Integer page;
    private SortColumn column;
    private SortOrder order;
    
    @Transient
    private transient Integer pointOfSaleId;
    @Transient
    private transient Integer moduleId;
    @Transient
    private transient Integer severityId;
    
    public PayStationRecentActivityFilterForm() {
    }
    
    public PayStationRecentActivityFilterForm(String moduleRandomId, String severityRandomId, Integer page, SortColumn column, SortOrder order) {
        super();
        this.moduleRandomId = moduleRandomId;
        this.severityRandomId = severityRandomId;
        this.page = page;
        this.column = column;
        this.order = order;
    }
    
    public String getRandomId() {
        return randomId;
    }
    
    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }
    
    public String getModuleRandomId() {
        return moduleRandomId;
    }
    
    public void setModuleRandomId(String moduleRandomId) {
        this.moduleRandomId = moduleRandomId;
    }
    
    public String getSeverityRandomId() {
        return severityRandomId;
    }
    
    public void setSeverityRandomId(String severityRandomId) {
        this.severityRandomId = severityRandomId;
    }
    
    public String getStatus() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    
    public Integer getPage() {
        return page;
    }
    
    public void setPage(Integer page) {
        this.page = page;
    }
    
    public SortColumn getColumn() {
        return column;
    }
    
    public void setColumn(SortColumn column) {
        this.column = column;
    }
    
    public SortOrder getOrder() {
        return order;
    }
    
    public void setOrder(SortOrder order) {
        this.order = order;
    }
    
    public Integer getModuleId() {
        return moduleId;
    }
    
    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }
    
    public Integer getPointOfSaleId() {
        return pointOfSaleId;
    }
    
    public void setPointOfSaleId(Integer pointOfSaleId) {
        this.pointOfSaleId = pointOfSaleId;
    }
    
    public Integer getSeverityId() {
        return severityId;
    }
    
    public void setSeverityId(Integer severityId) {
        this.severityId = severityId;
    }
    
    public enum SortColumn {
        severity, alertDate, resolvedDate, alertStatus;
    }
    
}
