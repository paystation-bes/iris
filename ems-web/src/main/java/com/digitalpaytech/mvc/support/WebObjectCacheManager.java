package com.digitalpaytech.mvc.support;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.BatchProcessStatus;
import com.digitalpaytech.dto.WidgetCacheKey;
import com.digitalpaytech.dto.WidgetData;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WidgetConstants;
import com.digitalpaytech.util.cache.ObjectCache;
import com.digitalpaytech.util.cache.ObjectTTL;

/**
 * WebObjectCacheManager uses Ehcache to cache objects with specific live/idle times.
 * See 'EMS 7.0 Software Requirements Specification.doc', Table 16 Widget Query Cache Times for details.
 * 
 * @author Allen Liang
 */
@Component("WebObjectCacheManager")
public final class WebObjectCacheManager {
    public static final int TTL_STANDARD = 900;
    public static final int TTL_SHORT = 300;
    
    private static Logger log = Logger.getLogger(WebObjectCacheManager.class);
    
    //====================================================================================================
    // Widget Query Cache Times
    //====================================================================================================
    /** 1-Now, 2-Today, 3-Yesterday, 4-Last 24 Hours */
    private static final int[] LAST_EQUALS_TWO_DAYS_RANGE_TYPE_IDS = { 1, 2, 3, 4 };
    private static final int[] CITATION_MAP_SHORT_CACHE_IDS = { 1, 2, 4 };
    
    @Autowired
    @Qualifier("ObjectCache")
    private ObjectCache cache;
    
    public WebObjectCacheManager() {
        
    }
    
    // This is for testing only and will be replaced when spring autowired the ObjectCache.
    public WebObjectCacheManager(final ObjectCache cache) {
        this.cache = cache;
    }
    
    @PostConstruct
    public void init() {
        // Creates a cache for WidgetData.
        final CacheTimer standardRangeTimer = new RangeSpecificTTL(LAST_EQUALS_TWO_DAYS_RANGE_TYPE_IDS, TTL_STANDARD, TTL_SHORT);
        final CacheTimer mapRangeTimer = new RangeSpecificTTL(CITATION_MAP_SHORT_CACHE_IDS, TTL_STANDARD, TTL_SHORT);
        final CacheTimer shortTTL = new ConstantTTL(TTL_SHORT);
        final CacheTimer standardTTL = new ConstantTTL(TTL_STANDARD);
        
        final Map<Integer, CacheTimer> widgetCacheTimerMap = new HashMap<Integer, CacheTimer>(30);
        widgetCacheTimerMap.put(WidgetConstants.METRIC_TYPE_REVENUE, standardRangeTimer);
        widgetCacheTimerMap.put(WidgetConstants.METRIC_TYPE_PURCHASES, standardRangeTimer);
        widgetCacheTimerMap.put(WidgetConstants.METRIC_TYPE_TURNOVER, standardRangeTimer);
        widgetCacheTimerMap.put(WidgetConstants.METRIC_TYPE_UTILIZATION, standardRangeTimer);
        widgetCacheTimerMap.put(WidgetConstants.METRIC_TYPE_PAID_OCCUPANCY, shortTTL);
        widgetCacheTimerMap.put(WidgetConstants.METRIC_TYPE_ACTIVE_ALERTS, shortTTL);
        widgetCacheTimerMap.put(WidgetConstants.METRIC_TYPE_COLLECTIONS, standardRangeTimer);
        widgetCacheTimerMap.put(WidgetConstants.METRIC_TYPE_SETTLED_CARD, standardTTL);
        widgetCacheTimerMap.put(WidgetConstants.METRIC_TYPE_MAP, shortTTL);
        widgetCacheTimerMap.put(WidgetConstants.METRIC_TYPE_AVERAGE_PRICE, standardTTL);
        widgetCacheTimerMap.put(WidgetConstants.METRIC_TYPE_AVERAGE_REVENUE, standardTTL);
        widgetCacheTimerMap.put(WidgetConstants.METRIC_TYPE_AVERAGE_DURATION, standardTTL);
        widgetCacheTimerMap.put(WidgetConstants.METRIC_TYPE_CARD_PROCESSING_REV, standardTTL);
        widgetCacheTimerMap.put(WidgetConstants.METRIC_TYPE_CARD_PROCESSING_TX, standardTTL);
        widgetCacheTimerMap.put(WidgetConstants.METRIC_TYPE_CITATIONS, standardTTL);
        widgetCacheTimerMap.put(WidgetConstants.METRIC_TYPE_CITATION_MAP, mapRangeTimer);
        widgetCacheTimerMap.put(WidgetConstants.METRIC_TYPE_PHYSICAL_OCCUPANCY, standardRangeTimer);
        widgetCacheTimerMap.put(WidgetConstants.METRIC_TYPE_COMPARE_OCCUPANCY, standardRangeTimer);
        widgetCacheTimerMap.put(WidgetConstants.METRIC_TYPE_OCCUPANCY_MAP, shortTTL);
        
        this.cache.registerTTL(new ObjectTTL(WidgetData.class) {
            @Override
            public int ttlInSecs(final String key, final Object obj) {
                int ttl = -1;
                if (obj instanceof WidgetData) {
                    final WidgetData w = (WidgetData) obj;
                    final CacheTimer timer = widgetCacheTimerMap.get(w.getMetricTypeId());
                    if (timer != null) {
                        ttl = timer.calculateTTL(w);
                    }
                }
                
                if (ttl == -1) {
                    ttl = TTL_STANDARD;
                }
                
                return ttl;
            }
        });
    }
    
    /**
     * Dynamic cache configuration without using ehcache.xml file.
     * 
     * @param widgetData
     *            New WidgetData that will be put into ehcache.
     */
    public void cacheWidgetData(final WidgetCacheKey key, final WidgetData widgetData) {
        this.cache.put(WidgetData.class, key.getIdentity(), widgetData);
        if (log.isDebugEnabled()) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Puts new WidgetData to cache, key is: ").append(key);
            log.debug(bdr.toString());
        }
    }
    
    public WidgetData getWidgetData(final WidgetCacheKey key) {
        final WidgetData result = (key == null) ? null : this.cache.get(WidgetData.class, key.getIdentity());
        if (log.isDebugEnabled()) {
            if (result != null) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append("Gets WidgetData from cache, key is: ").append(key);
                log.debug(bdr.toString());
            }
        }
        
        return result;
    }
    
    public boolean isWidgetDataInCache(final WidgetCacheKey key) {
        return (key == null) ? false : this.cache.has(WidgetData.class, key.getIdentity());
    }
    
    public void removeWidgetDataFromCache(final WidgetCacheKey key) {
        if (key != null) {
            this.cache.remove(WidgetData.class, key.getIdentity());
            if (log.isDebugEnabled()) {
                final StringBuilder bdr = new StringBuilder();
                bdr.append("Removing WidgetData from cache, key is: ").append(key);
                log.debug(bdr.toString());
            }
        }
    }
    
    public String createProcessKey(final HttpServletRequest request, final UserAccount user) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final StringBuilder keyBuffer = new StringBuilder();
        keyBuffer.append(keyMapping.getRandomString(UserAccount.class, user.getId()));
        
        final BatchProcessStatus stat = new BatchProcessStatus();
        stat.start();
        
        String key;
        do {
            keyBuffer.append("_");
            keyBuffer.append(this.cache.generateUnique(BatchProcessStatus.class));
            
            key = keyBuffer.toString();
        } while (this.cache.putIfAbsent(BatchProcessStatus.class, key, stat) != null);
        
        return key;
    }
    
    public BatchProcessStatus getImmutableProcessStatus(final String key) {
        return this.cache.get(BatchProcessStatus.class, key);
    }
    
    public void removeProcessStatus(final String key) {
        this.cache.remove(BatchProcessStatus.class, key);
    }
    
    interface CacheTimer {
        int calculateTTL(WidgetData widgetData);
    }
    
    private static final class ConstantTTL implements CacheTimer {
        private int ttl;
        
        private ConstantTTL(final int ttl) {
            this.ttl = ttl;
        }

        @Override
        public int calculateTTL(final WidgetData widgetData) {
            return this.ttl;
        }
    }
    
    private static final class RangeSpecificTTL implements CacheTimer {
        private Set<Integer> specialRange;
        private int standardTTL;
        private int specialTTL;
        
        private RangeSpecificTTL(final int[] specialRangeIds, final int standardTTL, final int specialTTL) {
            this.standardTTL = standardTTL;
            this.specialTTL = specialTTL;
            
            this.specialRange = new HashSet<Integer>(specialRangeIds.length);
            for (int rangeId : specialRangeIds) {
                this.specialRange.add(rangeId);
            }
        }
        
        @Override
        public int calculateTTL(final WidgetData widgetData) {
            return this.specialRange.contains(widgetData.getRangeTypeId()) ? this.specialTTL : this.standardTTL;
        }
    }
}
