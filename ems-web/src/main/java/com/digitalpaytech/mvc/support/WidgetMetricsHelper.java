package com.digitalpaytech.mvc.support;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.digitalpaytech.dto.AdhocReportInfo;
import com.digitalpaytech.dto.AlertMapEntry;
import com.digitalpaytech.dto.ChildCustomerInfo;
import com.digitalpaytech.dto.Display;
import com.digitalpaytech.dto.Filter;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.ListResultDTO;
import com.digitalpaytech.dto.Metric;
import com.digitalpaytech.dto.MobileAppInfo;
import com.digitalpaytech.dto.MobileLicenseInfo;
import com.digitalpaytech.dto.MobileLicenseListInfo;
import com.digitalpaytech.dto.MobileSubscriptionInfo;
import com.digitalpaytech.dto.MobileTokenMapEntry;
import com.digitalpaytech.dto.NotificationDetails;
import com.digitalpaytech.dto.PaginatedList;
import com.digitalpaytech.dto.PayStationSelectorDTO;
import com.digitalpaytech.dto.Range;
import com.digitalpaytech.dto.ReportUIFilterType;
import com.digitalpaytech.dto.SearchConsumerResult;
import com.digitalpaytech.dto.SearchCustomerResult;
import com.digitalpaytech.dto.Tier1;
import com.digitalpaytech.dto.Tier2;
import com.digitalpaytech.dto.Tier3;
import com.digitalpaytech.dto.TrendData;
import com.digitalpaytech.dto.WidgetData;
import com.digitalpaytech.dto.WidgetMasterList;
import com.digitalpaytech.dto.WidgetMetrics;
import com.digitalpaytech.dto.WidgetSeries;
import com.digitalpaytech.dto.WidgetSettingsOptions;
import com.digitalpaytech.dto.customeradmin.AlertCentreAlertInfo;
import com.digitalpaytech.dto.customeradmin.AlertCentrePayStationInfo;
import com.digitalpaytech.dto.customeradmin.AlertInfo;
import com.digitalpaytech.dto.customeradmin.BannedCardList;
import com.digitalpaytech.dto.customeradmin.CardRefundListInfo;
import com.digitalpaytech.dto.customeradmin.CollectionsCentreCollectionInfo;
import com.digitalpaytech.dto.customeradmin.CollectionsCentreMinimalInfo;
import com.digitalpaytech.dto.customeradmin.CollectionsCentrePayStationInfo;
import com.digitalpaytech.dto.customeradmin.CollectionsCentrePosInfo;
import com.digitalpaytech.dto.customeradmin.CouponDTO;
import com.digitalpaytech.dto.customeradmin.CustomerActivityLog;
import com.digitalpaytech.dto.customeradmin.CustomerAdminConsumerAccountInfo;
import com.digitalpaytech.dto.customeradmin.CustomerAdminPayStationSettingInfo;
import com.digitalpaytech.dto.customeradmin.CustomerAdminPayStationSettingPayStationInfo;
import com.digitalpaytech.dto.customeradmin.CustomerCardDTO;
import com.digitalpaytech.dto.customeradmin.DefinedAlertDetails;
import com.digitalpaytech.dto.customeradmin.DeviceHistoryInfo;
import com.digitalpaytech.dto.customeradmin.EmailAddressHistoryDetail;
import com.digitalpaytech.dto.customeradmin.LocationPOSTree;
import com.digitalpaytech.dto.customeradmin.LocationRouteFilterInfo;
import com.digitalpaytech.dto.customeradmin.LocationTree;
import com.digitalpaytech.dto.customeradmin.MobileDeviceInfo;
import com.digitalpaytech.dto.customeradmin.PayStationDetails;
import com.digitalpaytech.dto.customeradmin.PayStationListCollectionReportList;
import com.digitalpaytech.dto.customeradmin.PayStationListHistoryNote;
import com.digitalpaytech.dto.customeradmin.PayStationListTransactionReportList;
import com.digitalpaytech.dto.customeradmin.PaystationListInfo;
import com.digitalpaytech.dto.customeradmin.RateListInfo;
import com.digitalpaytech.dto.customeradmin.ReportDefinitionInfo;
import com.digitalpaytech.dto.customeradmin.ReportQueueAndRepositoryInfo;
import com.digitalpaytech.dto.customeradmin.ReportQueueInfo;
import com.digitalpaytech.dto.customeradmin.ReportRepositoryInfo;
import com.digitalpaytech.dto.customeradmin.RoleSettingDetails;
import com.digitalpaytech.dto.customeradmin.RouteDetails;
import com.digitalpaytech.dto.customeradmin.SensorLogGraphData;
import com.digitalpaytech.dto.customeradmin.SortedSettingListInfo;
import com.digitalpaytech.dto.customeradmin.TaxDetail;
import com.digitalpaytech.dto.customeradmin.TransactionReceiptDetails;
import com.digitalpaytech.dto.customeradmin.TransactionReceiptSearchCriteria;
import com.digitalpaytech.dto.customeradmin.UserSettingDetails;
import com.digitalpaytech.dto.customermigration.CustomerMigrationData;
import com.digitalpaytech.dto.customermigration.RecentBoardedMigratedCustomerList;
import com.digitalpaytech.dto.customermigration.ScheduleMigrationInfo;
import com.digitalpaytech.dto.systemadmin.CardProcessingQueueStatus;
import com.digitalpaytech.dto.systemadmin.ClusterMemberInfo;
import com.digitalpaytech.mvc.PostTokenConverter;
import com.digitalpaytech.mvc.customeradmin.support.CustomerAdminConsumerAccountEditForm;
import com.digitalpaytech.mvc.customeradmin.support.CustomerAdminPayStationEditForm;
import com.digitalpaytech.mvc.customeradmin.support.LocationEditForm;
import com.digitalpaytech.mvc.customeradmin.support.ReportEditForm;
import com.digitalpaytech.mvc.systemadmin.support.SysAdminPayStationEditForm;
import com.digitalpaytech.mvc.systemadmin.support.SysAdminPayStationEditForm.ListEntry;
import com.digitalpaytech.util.KeyValuePair;
import com.digitalpaytech.util.KeyValuePairString;
import com.digitalpaytech.util.support.RelativeDateTimeConverter;
import com.digitalpaytech.util.support.SubscriptionEntry;
import com.digitalpaytech.util.support.ValueProxyConverter;
import com.digitalpaytech.util.support.WebObjectIdConverter;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import com.thoughtworks.xstream.io.json.JsonHierarchicalStreamDriver;

public final class WidgetMetricsHelper {
    
    private static final Logger LOGGER = Logger.getLogger(WidgetMetricsHelper.class);
    private static Map<String, WidgetMetrics> metricsMap;
    private static XStream flatXstream;
    private static XStream hierarchicalXstream;
    
    private WidgetMetricsHelper() {
    }
    
    static {
        System.setProperty("jettison.mapped.typeconverter.class", "com.digitalpaytech.util.JSONPrimitiveConverter");
        
        flatXstream = new XStream(new JettisonMappedXmlDriver());
        hierarchicalXstream = new XStream(new JsonHierarchicalStreamDriver());
        
        final WebObjectIdConverter randomConverter = new WebObjectIdConverter();
        flatXstream.registerConverter(randomConverter);
        hierarchicalXstream.registerConverter(randomConverter);
        
        final RelativeDateTimeConverter relativeDateConverter = new RelativeDateTimeConverter();
        flatXstream.registerConverter(relativeDateConverter);
        hierarchicalXstream.registerConverter(relativeDateConverter);
        
        final PostTokenConverter postTokenConverter = new PostTokenConverter();
        flatXstream.registerConverter(postTokenConverter);
        hierarchicalXstream.registerConverter(postTokenConverter);
        
        final ValueProxyConverter valueProxyConverter = new ValueProxyConverter();
        flatXstream.registerConverter(valueProxyConverter);
        hierarchicalXstream.registerConverter(valueProxyConverter);
        
        // To hold child WidgetMetrics and parent WidgetMetrics.
        metricsMap = new HashMap<String, WidgetMetrics>(2);
    }
    
    /**
     * Loads metric xml file (e.g. widgetmetric.xml) only once by using PostConstruct annotation during the application is starting.
     * 
     * @param String
     *            metricsXmlFileName Metric xml file name, e.g. "/widgetmetrics.xml", "/widgetmetricsParent.xml
     * @param inputStream
     *            Metric xml file input stream. The method would return 'false' if inputStream is null.
     * @return boolean 'true' if file is successfully loaded.
     */
    public static WidgetMetrics loadWidgetMetricsXml(final String metricsXmlFileName, final InputStream inputStream) {
        
        if (inputStream == null) {
            return null;
        }
        
        if (metricsMap.get(metricsXmlFileName.toLowerCase()) != null) {
            return metricsMap.get(metricsXmlFileName.toLowerCase());
        }
        
        final XStream xstream = new XStream();
        xstream.processAnnotations(WidgetMetrics.class);
        xstream.processAnnotations(Metric.class);
        xstream.processAnnotations(Range.class);
        xstream.processAnnotations(Filter.class);
        xstream.processAnnotations(Tier1.class);
        xstream.processAnnotations(Tier2.class);
        xstream.processAnnotations(Tier3.class);
        xstream.processAnnotations(Display.class);
        
        final WidgetMetrics widgetMetrics = (WidgetMetrics) xstream.fromXML(inputStream);
        metricsMap.put(metricsXmlFileName, widgetMetrics);
        
        return widgetMetrics;
    }
    
    public static void registerComponents() {
        flatXstream.alias("WidgetSeries", WidgetSeries.class);
        hierarchicalXstream.alias("WidgetSeries", WidgetSeries.class);
        
        flatXstream.processAnnotations(WidgetData.class);
        hierarchicalXstream.processAnnotations(WidgetData.class);
        
        flatXstream.processAnnotations(WidgetMasterList.class);
        hierarchicalXstream.processAnnotations(WidgetMasterList.class);
        
        flatXstream.processAnnotations(WidgetSettingsOptions.class);
        hierarchicalXstream.processAnnotations(WidgetSettingsOptions.class);
        
        flatXstream.alias("FormErrorStatus", FormErrorStatus.class);
        hierarchicalXstream.alias("FormErrorStatus", FormErrorStatus.class);
        
        flatXstream.alias("LocationTree", LocationTree.class);
        hierarchicalXstream.alias("LocationTree", LocationTree.class);
        flatXstream.processAnnotations(LocationTree.class);
        hierarchicalXstream.processAnnotations(LocationTree.class);
        
        flatXstream.alias("LocationPOSTree", LocationPOSTree.class);
        hierarchicalXstream.alias("LocationPOSTree", LocationPOSTree.class);
        flatXstream.processAnnotations(LocationPOSTree.class);
        hierarchicalXstream.processAnnotations(LocationPOSTree.class);
        
        flatXstream.processAnnotations(RouteDetails.class);
        hierarchicalXstream.processAnnotations(RouteDetails.class);
        
        flatXstream.processAnnotations(UserSettingDetails.class);
        hierarchicalXstream.processAnnotations(UserSettingDetails.class);
        
        flatXstream.processAnnotations(RoleSettingDetails.class);
        hierarchicalXstream.processAnnotations(RoleSettingDetails.class);
        
        flatXstream.processAnnotations(LocationRouteFilterInfo.class);
        hierarchicalXstream.processAnnotations(LocationRouteFilterInfo.class);
        
        flatXstream.processAnnotations(DefinedAlertDetails.class);
        hierarchicalXstream.processAnnotations(DefinedAlertDetails.class);
        
        flatXstream.processAnnotations(SortedSettingListInfo.class);
        hierarchicalXstream.processAnnotations(SortedSettingListInfo.class);
        
        flatXstream.processAnnotations(SensorLogGraphData.class);
        hierarchicalXstream.processAnnotations(SensorLogGraphData.class);
        
        flatXstream.processAnnotations(AlertInfo.class);
        hierarchicalXstream.processAnnotations(AlertInfo.class);
        
        flatXstream.processAnnotations(NotificationDetails.class);
        hierarchicalXstream.processAnnotations(NotificationDetails.class);
        
        flatXstream.processAnnotations(ClusterMemberInfo.class);
        hierarchicalXstream.processAnnotations(ClusterMemberInfo.class);
        
        flatXstream.processAnnotations(LocationEditForm.class);
        hierarchicalXstream.processAnnotations(LocationEditForm.class);
        
        flatXstream.processAnnotations(CardRefundListInfo.class);
        hierarchicalXstream.processAnnotations(CardRefundListInfo.class);
        
        flatXstream.processAnnotations(AlertCentrePayStationInfo.class);
        hierarchicalXstream.processAnnotations(AlertCentrePayStationInfo.class);
        
        flatXstream.processAnnotations(BannedCardList.class);
        hierarchicalXstream.processAnnotations(BannedCardList.class);
        
        flatXstream.processAnnotations(CustomerActivityLog.class);
        hierarchicalXstream.processAnnotations(CustomerActivityLog.class);
        
        flatXstream.processAnnotations(CustomerAdminPayStationEditForm.class);
        hierarchicalXstream.processAnnotations(CustomerAdminPayStationEditForm.class);
        
        flatXstream.processAnnotations(SysAdminPayStationEditForm.class);
        hierarchicalXstream.processAnnotations(SysAdminPayStationEditForm.class);
        
        flatXstream.processAnnotations(ListEntry.class);
        hierarchicalXstream.processAnnotations(ListEntry.class);
        
        flatXstream.processAnnotations(ReportDefinitionInfo.class);
        hierarchicalXstream.processAnnotations(ReportDefinitionInfo.class);
        
        flatXstream.processAnnotations(ReportQueueInfo.class);
        hierarchicalXstream.processAnnotations(ReportQueueInfo.class);
        
        flatXstream.processAnnotations(ReportRepositoryInfo.class);
        hierarchicalXstream.processAnnotations(ReportRepositoryInfo.class);
        
        flatXstream.processAnnotations(ReportQueueAndRepositoryInfo.class);
        hierarchicalXstream.processAnnotations(ReportQueueAndRepositoryInfo.class);
        
        flatXstream.processAnnotations(ReportUIFilterType.class);
        hierarchicalXstream.processAnnotations(ReportUIFilterType.class);
        
        flatXstream.processAnnotations(ReportEditForm.class);
        hierarchicalXstream.processAnnotations(ReportEditForm.class);
        
        flatXstream.processAnnotations(PayStationDetails.class);
        hierarchicalXstream.processAnnotations(PayStationDetails.class);
        
        flatXstream.processAnnotations(PayStationListTransactionReportList.class);
        hierarchicalXstream.processAnnotations(PayStationListTransactionReportList.class);
        
        flatXstream.processAnnotations(PayStationListCollectionReportList.class);
        hierarchicalXstream.processAnnotations(PayStationListCollectionReportList.class);
        
        flatXstream.processAnnotations(MessageInfo.class);
        hierarchicalXstream.processAnnotations(MessageInfo.class);
        
        flatXstream.processAnnotations(ValidationErrorInfo.class);
        hierarchicalXstream.processAnnotations(ValidationErrorInfo.class);
        
        flatXstream.processAnnotations(CardProcessingQueueStatus.class);
        hierarchicalXstream.processAnnotations(CardProcessingQueueStatus.class);
        
        flatXstream.processAnnotations(SearchCustomerResult.class);
        hierarchicalXstream.processAnnotations(SearchCustomerResult.class);
        
        flatXstream.processAnnotations(SubscriptionEntry.class);
        hierarchicalXstream.processAnnotations(SubscriptionEntry.class);
        
        flatXstream.alias("SubscriptionEntry", SubscriptionEntry.class);
        hierarchicalXstream.alias("SubscriptionEntry", SubscriptionEntry.class);
        
        flatXstream.processAnnotations(CollectionsCentrePayStationInfo.class);
        hierarchicalXstream.processAnnotations(CollectionsCentrePayStationInfo.class);
        
        flatXstream.processAnnotations(AlertCentreAlertInfo.class);
        hierarchicalXstream.processAnnotations(AlertCentreAlertInfo.class);
        flatXstream.alias("AlertCentreAlertInfo", AlertCentreAlertInfo.class);
        hierarchicalXstream.alias("AlertCentreAlertInfo", AlertCentreAlertInfo.class);
        
        flatXstream.processAnnotations(CollectionsCentreCollectionInfo.class);
        hierarchicalXstream.processAnnotations(CollectionsCentreCollectionInfo.class);
        flatXstream.alias("CollectionsCentreCollectionInfo", CollectionsCentreCollectionInfo.class);
        hierarchicalXstream.alias("CollectionsCentreCollectionInfo", CollectionsCentreCollectionInfo.class);
        
        flatXstream.processAnnotations(CustomerAdminConsumerAccountInfo.class);
        hierarchicalXstream.processAnnotations(CustomerAdminConsumerAccountInfo.class);
        
        flatXstream.processAnnotations(CustomerAdminPayStationSettingInfo.class);
        hierarchicalXstream.processAnnotations(CustomerAdminPayStationSettingInfo.class);
        
        flatXstream.processAnnotations(CustomerAdminPayStationSettingPayStationInfo.class);
        hierarchicalXstream.processAnnotations(CustomerAdminPayStationSettingPayStationInfo.class);
        flatXstream.alias("CustomerAdminPayStationSettingPayStationInfo", CustomerAdminPayStationSettingPayStationInfo.class);
        hierarchicalXstream.alias("CustomerAdminPayStationSettingPayStationInfo", CustomerAdminPayStationSettingPayStationInfo.class);
        
        flatXstream.processAnnotations(CustomerAdminConsumerAccountEditForm.class);
        hierarchicalXstream.processAnnotations(CustomerAdminConsumerAccountEditForm.class);
        
        flatXstream.alias("Filter", FilterDTO.class);
        hierarchicalXstream.alias("Filter", FilterDTO.class);
        
        flatXstream.processAnnotations(ListResultDTO.class);
        hierarchicalXstream.processAnnotations(ListResultDTO.class);
        
        flatXstream.processAnnotations(PaginatedList.class);
        hierarchicalXstream.processAnnotations(PaginatedList.class);
        
        flatXstream.processAnnotations(CouponDTO.class);
        hierarchicalXstream.processAnnotations(CouponDTO.class);
        
        flatXstream.processAnnotations(CustomerCardDTO.class);
        hierarchicalXstream.processAnnotations(CustomerCardDTO.class);
        
        flatXstream.processAnnotations(PaystationListInfo.class);
        hierarchicalXstream.processAnnotations(PaystationListInfo.class);
        
        flatXstream.processAnnotations(PayStationSelectorDTO.class);
        hierarchicalXstream.processAnnotations(PayStationSelectorDTO.class);
        
        flatXstream.processAnnotations(AlertMapEntry.class);
        hierarchicalXstream.processAnnotations(AlertMapEntry.class);
        flatXstream.alias("alertEntry", AlertMapEntry.class);
        hierarchicalXstream.alias("alertEntry", AlertMapEntry.class);
        
        flatXstream.processAnnotations(EmailAddressHistoryDetail.class);
        hierarchicalXstream.processAnnotations(EmailAddressHistoryDetail.class);
        
        flatXstream.processAnnotations(TransactionReceiptDetails.class);
        hierarchicalXstream.processAnnotations(TransactionReceiptDetails.class);
        
        flatXstream.processAnnotations(TaxDetail.class);
        hierarchicalXstream.processAnnotations(TaxDetail.class);
        
        flatXstream.processAnnotations(TrendData.class);
        hierarchicalXstream.processAnnotations(TrendData.class);
        
        flatXstream.processAnnotations(CollectionsCentrePosInfo.class);
        hierarchicalXstream.processAnnotations(CollectionsCentrePosInfo.class);
        flatXstream.alias("paystation", CollectionsCentrePosInfo.class);
        hierarchicalXstream.alias("paystation", CollectionsCentrePosInfo.class);
        
        flatXstream.processAnnotations(CollectionsCentreMinimalInfo.class);
        hierarchicalXstream.processAnnotations(CollectionsCentreMinimalInfo.class);
        flatXstream.alias("collection", CollectionsCentreMinimalInfo.class);
        hierarchicalXstream.alias("collection", CollectionsCentreMinimalInfo.class);
        
        flatXstream.processAnnotations(MobileDeviceInfo.class);
        hierarchicalXstream.processAnnotations(MobileDeviceInfo.class);
        flatXstream.alias("device", MobileDeviceInfo.class);
        hierarchicalXstream.alias("device", MobileDeviceInfo.class);
        
        flatXstream.processAnnotations(MobileAppInfo.class);
        hierarchicalXstream.processAnnotations(MobileAppInfo.class);
        flatXstream.alias("app", MobileAppInfo.class);
        hierarchicalXstream.alias("app", MobileAppInfo.class);
        
        flatXstream.processAnnotations(MobileLicenseListInfo.class);
        hierarchicalXstream.processAnnotations(MobileLicenseListInfo.class);
        flatXstream.alias("device", MobileLicenseListInfo.class);
        hierarchicalXstream.alias("device", MobileLicenseListInfo.class);
        
        flatXstream.processAnnotations(MobileTokenMapEntry.class);
        hierarchicalXstream.processAnnotations(MobileTokenMapEntry.class);
        flatXstream.alias("user", MobileTokenMapEntry.class);
        hierarchicalXstream.alias("user", MobileTokenMapEntry.class);
        
        flatXstream.processAnnotations(MobileSubscriptionInfo.class);
        hierarchicalXstream.processAnnotations(MobileSubscriptionInfo.class);
        flatXstream.alias("mobileSubscription", MobileSubscriptionInfo.class);
        hierarchicalXstream.alias("mobileSubscription", MobileSubscriptionInfo.class);
        
        flatXstream.processAnnotations(DeviceHistoryInfo.class);
        hierarchicalXstream.processAnnotations(DeviceHistoryInfo.class);
        flatXstream.alias("deviceHistory", DeviceHistoryInfo.class);
        hierarchicalXstream.alias("deviceHistory", DeviceHistoryInfo.class);
        
        flatXstream.processAnnotations(MobileLicenseInfo.class);
        hierarchicalXstream.processAnnotations(MobileLicenseInfo.class);
        flatXstream.alias("mobileLicense", MobileLicenseInfo.class);
        hierarchicalXstream.alias("mobileLicense", MobileLicenseInfo.class);
        
        flatXstream.processAnnotations(AdhocReportInfo.class);
        hierarchicalXstream.processAnnotations(AdhocReportInfo.class);
        
        flatXstream.processAnnotations(PayStationListHistoryNote.class);
        hierarchicalXstream.processAnnotations(PayStationListHistoryNote.class);
        
        flatXstream.processAnnotations(SearchConsumerResult.class);
        hierarchicalXstream.processAnnotations(SearchConsumerResult.class);
        
        flatXstream.processAnnotations(RateListInfo.class);
        hierarchicalXstream.processAnnotations(RateListInfo.class);
        
        flatXstream.processAnnotations(RateRateProfileEditForm.class);
        hierarchicalXstream.processAnnotations(RateRateProfileEditForm.class);
        
        flatXstream.processAnnotations(ChildCustomerInfo.class);
        hierarchicalXstream.processAnnotations(ChildCustomerInfo.class);
        
        flatXstream.processAnnotations(TransactionReceiptSearchCriteria.class);
        hierarchicalXstream.processAnnotations(TransactionReceiptSearchCriteria.class);
        
        flatXstream.processAnnotations(ScheduleMigrationInfo.class);
        hierarchicalXstream.processAnnotations(ScheduleMigrationInfo.class);
        flatXstream.alias("scheduleMigrationInfo", ScheduleMigrationInfo.class);
        hierarchicalXstream.alias("scheduleMigrationInfo", ScheduleMigrationInfo.class);
        
        flatXstream.processAnnotations(CustomerMigrationData.class);
        hierarchicalXstream.processAnnotations(CustomerMigrationData.class);
        
        flatXstream.processAnnotations(RecentBoardedMigratedCustomerList.class);
        hierarchicalXstream.processAnnotations(RecentBoardedMigratedCustomerList.class);
        
        flatXstream.processAnnotations(ScheduleMigrationInfo.class);
        hierarchicalXstream.processAnnotations(RecentBoardedMigratedCustomerList.class);
        
        flatXstream.alias("KeyValuePair", KeyValuePair.class);
        flatXstream.processAnnotations(KeyValuePair.class);
        hierarchicalXstream.processAnnotations(KeyValuePair.class);
        
        flatXstream.alias("KeyValuePairString", KeyValuePairString.class);
        flatXstream.processAnnotations(KeyValuePairString.class);
        hierarchicalXstream.processAnnotations(KeyValuePairString.class);
        
    }
    
    /**
     * Converts Java objects to JSON document representation.
     * 
     * @param obj
     *            Object contains all data/sub-objects.
     * @param openTagName
     *            String open tag name, e.g. 'widgetSettingsOptions'.
     * @param flatOut
     *            boolean Return String will be in one line if flatOut is true, otherwise return value has hierarchical structure.
     * @return String JSON document.
     */
    public static String convertToJson(final Object obj, final String openTagName, final boolean flatOut) {
        XStream xstream;
        if (flatOut) {
            xstream = flatXstream;
            
        } else {
            xstream = hierarchicalXstream;
        }
        xstream.setMode(XStream.NO_REFERENCES);
        xstream.alias(openTagName, obj.getClass());
        final String json = xstream.toXML(obj);
        
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(json);
        }
        return json;
    }
    
}
