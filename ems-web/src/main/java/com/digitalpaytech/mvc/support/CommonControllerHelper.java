package com.digitalpaytech.mvc.support;

import java.util.Date;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.domain.EventType;
import com.digitalpaytech.dto.customeradmin.LocationRouteFilterInfo;
import com.digitalpaytech.util.RandomKeyMapping;

public interface CommonControllerHelper {
    
    LocationRouteFilterInfo createLocationRouteFilter(Integer customerId, RandomKeyMapping keyMapping);
    
    LocationRouteFilterInfo createLocationRouteFilter(Integer customerId, RandomKeyMapping keyMapping, Integer routeTypeId);
    
    String getMessage(String key);
    
    String getMessage(String key, Object... args);
    
    Integer resolveCustomerId(HttpServletRequest request, HttpServletResponse response);
    
    TimeZone resolveTimeZone(Integer customerId);
    
    Date getEarliestMigrationDate(Date customerBoardedGmt);
    
    boolean getReadyForMigration(Byte migrationValidationStatus, Date earliestMigrationDate);
    
    void setModelObjectsForMigration(CustomerMigration customerMigration, ModelMap model, TimeZone timeZone);
    
    Customer insertCustomerInModelForSystemAdmin(HttpServletRequest request, ModelMap model);
    
    void setModelObjectsForMigrationLight(CustomerMigration customerMigration, ModelMap model);
    
    String getRequestTimeRangeString();
    
    String clearActiveAlert(HttpServletRequest request, HttpServletResponse response, boolean isActive);
    
    String createClearEvent(Integer eventActionTypeId, EventType eventType, Integer userAccountId, Integer pointOfSaleId);
    
    Integer verifyRandomIdAndReturnActual(HttpServletResponse response, RandomKeyMapping keyMapping, String strRandomLocationID, String message);
    
    Long verifyRandomIdAndReturnActualLong(HttpServletResponse response, RandomKeyMapping keyMapping, String strRandomLocationID, String message);
}
