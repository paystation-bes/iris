package com.digitalpaytech.mvc.support;

import java.io.Serializable;
import java.util.Date;

public class RateProfileLocationForm implements Serializable {
    private static final long serialVersionUID = -4558618807971678084L;
    
    private String randomId;
    
    private String locationRandomId;
    private String rateProfileRandomId;
    
    private String startDate;
    private String endDate;
    private String spaceRange;
    
    private String rateProfileName;
    
    /* The following field are for business side only */
    private Date startDateObj;
    private Date endDateObj;
    
    private boolean isDeleted;
    
    public RateProfileLocationForm() {
        
    }
    
    public String getRandomId() {
        return randomId;
    }
    
    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }
    
    public String getLocationRandomId() {
        return locationRandomId;
    }
    
    public void setLocationRandomId(String locationRandomId) {
        this.locationRandomId = locationRandomId;
    }
    
    public String getRateProfileRandomId() {
        return rateProfileRandomId;
    }
    
    public void setRateProfileRandomId(String rateProfileRandomId) {
        this.rateProfileRandomId = rateProfileRandomId;
    }
    
    public String getRateProfileName() {
        return rateProfileName;
    }

    public void setRateProfileName(String rateProfileName) {
        this.rateProfileName = rateProfileName;
    }

    public String getStartDate() {
        return startDate;
    }
    
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    
    public String getEndDate() {
        return endDate;
    }
    
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    
    public String getSpaceRange() {
        return spaceRange;
    }
    
    public void setSpaceRange(String spaceRange) {
        this.spaceRange = spaceRange;
    }
    
    public Date getStartDateObj() {
        return startDateObj;
    }
    
    public void setStartDateObj(Date startDateObj) {
        this.startDateObj = startDateObj;
    }
    
    public Date getEndDateObj() {
        return endDateObj;
    }
    
    public void setEndDateObj(Date endDateObj) {
        this.endDateObj = endDateObj;
    }
    
    public boolean isIsDeleted() {
        return isDeleted;
    }
    
    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
}
