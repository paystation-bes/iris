package com.digitalpaytech.mvc.support;

import java.io.Serializable;

public class LocationIntegrationSearchForm implements Serializable {    
    private static final long serialVersionUID = 256884079688740251L;    
    
    private String locationRandomId;    
    private String itemId;
    private String selectedId;
    
    private Integer page;
    private Integer itemsPerPage;
    private String dataKey;
    
    private String targetRandomId;

    public final String getLocationRandomId() {
        return this.locationRandomId;
    }

    public final void setLocationRandomId(final String locationRandomId) {
        this.locationRandomId = locationRandomId;
    }

    public final String getItemId() {
        return this.itemId;
    }

    public final void setItemId(final String itemId) {
        this.itemId = itemId;
    }

    public final String getSelectedId() {
        return this.selectedId;
    }

    public final void setSelectedId(final String selectedId) {
        this.selectedId = selectedId;
    }

    public final Integer getPage() {
        return this.page;
    }

    public final void setPage(final Integer page) {
        this.page = page;
    }

    public final Integer getItemsPerPage() {
        return this.itemsPerPage;
    }

    public final void setItemsPerPage(final Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    public final String getDataKey() {
        return this.dataKey;
    }

    public final void setDataKey(final String dataKey) {
        this.dataKey = dataKey;
    }

    public final String getTargetRandomId() {
        return this.targetRandomId;
    }

    public final void setTargetRandomId(final String targetRandomId) {
        this.targetRandomId = targetRandomId;
    }    
    
}
