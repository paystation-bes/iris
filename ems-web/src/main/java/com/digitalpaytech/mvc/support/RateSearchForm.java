package com.digitalpaytech.mvc.support;

import java.io.Serializable;

public class RateSearchForm implements Serializable {
    
    private static final long serialVersionUID = -2309616108460323786L;

    private Byte rateTypeId;
    private String itemId;
    private String selectedId;
    
    private Integer page;
    private Integer itemsPerPage;
    private String dataKey;
    
    private String targetRandomId;
    
    public RateSearchForm() {
        
    }
    
    public final Byte getRateTypeId() {
        return this.rateTypeId;
    }
    
    public final void setRateTypeId(final Byte rateTypeId) {
        this.rateTypeId = rateTypeId;
    }
    
    public final Integer getPage() {
        return this.page;
    }
    
    public final void setPage(final Integer page) {
        this.page = page;
    }
    
    public final Integer getItemsPerPage() {
        return this.itemsPerPage;
    }
    
    public final void setItemsPerPage(final Integer itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }
    
    public final String getDataKey() {
        return this.dataKey;
    }
    
    public final void setDataKey(final String dataKey) {
        this.dataKey = dataKey;
    }
    
    public final String getItemId() {
        return this.itemId;
    }
    
    public final void setItemId(final String itemId) {
        this.itemId = itemId;
    }
    
    public final String getSelectedId() {
        return this.selectedId;
    }
    
    public final void setSelectedId(final String selectedId) {
        this.selectedId = selectedId;
    }
    
    public final String getTargetRandomId() {
        return this.targetRandomId;
    }
    
    public final void setTargetRandomId(final String targetRandomId) {
        this.targetRandomId = targetRandomId;
    }
}
