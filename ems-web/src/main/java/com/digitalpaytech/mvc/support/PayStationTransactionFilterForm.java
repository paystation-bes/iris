package com.digitalpaytech.mvc.support;

import java.io.Serializable;

import javax.persistence.Transient;

import com.digitalpaytech.constants.SortOrder;

public class PayStationTransactionFilterForm implements Serializable {
    
    private static final long serialVersionUID = -1554039492532909196L;
    private String randomId;
    private Integer page;
    private SortColumn column;
    private SortOrder order;
    
    @Transient
    private transient Integer pointOfSaleId;
    
    public PayStationTransactionFilterForm() {
    }
    
    public PayStationTransactionFilterForm(Integer page, SortColumn column, SortOrder order) {
        super();
        this.page = page;
        this.column = column;
        this.order = order;
    }
    
    public String getRandomId() {
        return randomId;
    }
    
    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }
    
    
    public Integer getPage() {
        return page;
    }
    
    public void setPage(Integer page) {
        this.page = page;
    }
    
    public SortColumn getColumn() {
        return column;
    }
    
    public void setColumn(SortColumn column) {
        this.column = column;
    }
    
    public SortOrder getOrder() {
        return order;
    }
    
    public void setOrder(SortOrder order) {
        this.order = order;
    }
    
    public Integer getPointOfSaleId() {
        return pointOfSaleId;
    }
    
    public void setPointOfSaleId(Integer pointOfSaleId) {
        this.pointOfSaleId = pointOfSaleId;
    }
    
    public enum SortColumn {
        date, type, paymentType, amount;
    }
    
}
