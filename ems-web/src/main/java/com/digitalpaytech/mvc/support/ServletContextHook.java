package com.digitalpaytech.mvc.support;

import java.util.Map;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.stat.Statistics;
import org.quartz.Scheduler;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.concurrent.ExecutorConfigurationSupport;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.digitalpaytech.util.EMSThreadManager;

/**
 * Web Application Shutdown Hook to terminate Quartz's Schedulers and Executor used/injected by Spring.
 * 
 * @author wittawatv
 * 
 */
public class ServletContextHook implements ServletContextListener {
    private static Logger logger = Logger.getLogger(ServletContextHook.class);
    
    public ServletContextHook() {
        
    }
    
    @Override
    public final void contextInitialized(final ServletContextEvent sce) {
        logger.info("Starting Iris...");
    }
    
    @Override
    public final void contextDestroyed(final ServletContextEvent sce) {
        logger.info("Stopping Iris...");
        final ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(sce.getServletContext());
        if (ctx == null) {
            logger.info("...Unable to locate ApplicationContext, have to trust that spring turned things off properly...");
        } else {
            // Commented codes are things that spring has already done !
            shutdownExecutors(ctx);
            shutdownSchedulers(ctx);
            
            /* Shutdown all known Thread(s) */
            EMSThreadManager.getInstance().shutdown();
            
            shutdownSessionFactories(ctx);
        }
        
        logger.info("Iris is gracefully terminated !");
    }
    
    /* Close Executor(s) injected by Spring */
    private void shutdownExecutors(final ApplicationContext ctx) {
        final Map<String, ExecutorConfigurationSupport> executors = ctx.getBeansOfType(ExecutorConfigurationSupport.class);
        for (String name : executors.keySet()) {
            try {
                executors.get(name).shutdown();
                logger.info("Terminated Executor: " + name);
            } catch (Exception e) {
                logger.warn("Error while terminating Executor: " + name);
            }
        }
    }
    
    /* Close Scheduler(s) injected by Spring */
    private void shutdownSchedulers(final ApplicationContext ctx) {
        final Map<String, Scheduler> schedulers = ctx.getBeansOfType(Scheduler.class);
        for (String name : schedulers.keySet()) {
            final Scheduler sched = schedulers.get(name);
            
            try {
                sched.shutdown(true);
                logger.info("Terminated Scheduler: " + name);
            } catch (Exception e) {
                logger.warn("Error while terminating Scheduler: " + name);
            }
        }
    }
    
    private void shutdownSessionFactories(final ApplicationContext ctx) {
        final Map<String, SessionFactory> sessionFctries = ctx.getBeansOfType(SessionFactory.class);
        
        /* Close all SessionFactory(s) */
        for (String name : sessionFctries.keySet()) {
            try {
                final SessionFactory sessionFctry = sessionFctries.get(name);
                if (sessionFctry.isClosed()) {
                    logger.info("SessionFactory \"" + name + "\" is already closed !");
                } else {
                    final Statistics stats = sessionFctry.getStatistics();
                    final long remainedOpenSessions = stats.getSessionOpenCount() - stats.getSessionCloseCount();
                    if (remainedOpenSessions > 0) {
                        logger.warn("Found " + remainedOpenSessions
                                    + " opened Hibernate Session(s), this might cause resources leak [SessionFactory: " + name + "] !");
                    } else {
                        sessionFctries.get(name).close();
                        logger.info("Closed SessionFactory: " + name);
                    }
                }
            } catch (Exception e) {
                logger.warn("Error while closing SessionFactory: " + name);
            }
        }
    }
    
}
