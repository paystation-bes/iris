package com.digitalpaytech.mvc.support.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.domain.CustomerMigrationValidationStatus;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.EventType;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.domain.PosEventHistory;
import com.digitalpaytech.domain.PosMacAddress;
import com.digitalpaytech.domain.PosSensorState;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.customeradmin.CustomerDetails;
import com.digitalpaytech.dto.customeradmin.LocationRouteFilterInfo;
import com.digitalpaytech.dto.queue.QueueEvent;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.scheduling.queue.alert.PosEventCurrentProcessor;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerMigrationService;
import com.digitalpaytech.service.CustomerMigrationValidationStatusService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.PosEventCurrentService;
import com.digitalpaytech.service.PosEventHistoryService;
import com.digitalpaytech.service.PosMacAddressService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.service.queue.QueueEventService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.LabelConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityUtil;

@Component("commonControllerHelper")
public class CommonControllerHelperImpl implements CommonControllerHelper, MessageSourceAware {
    
    private static final int WAIT_FOR_MIGRATION_DATE = 0;
    private static final int READY_BUT_BLOCKED = 1;
    private static final int READY_WITH_NOTIFICATION = 2;
    private static final int READY_ALL_CLEAR = 3;
    private static final int REQUESTED = 4;
    private static final int SCHEDULED = 5;
    private static final int NOT_BOARDED = 6;
    private static final int RUNNING = 7;
    private static final int MIGRATED = 8;
    private static final int FAILED = 9;
    
    private static final int SCHEDULE_MIN_HOUR_9 = 9;
    private static final int SCHEDULE_MAX_HOUR_16 = 16;
    
    @Autowired
    protected PointOfSaleService pointOfSaleService;
    @Autowired
    protected LocationService locationService;
    @Autowired
    protected CustomerService customerService;
    @Autowired
    protected CustomerAdminService customerAdminService;
    @Autowired
    protected RouteService routeService;
    @Autowired
    protected PosMacAddressService posMacAddressService;
    @Autowired
    private CustomerMigrationService customerMigrationService;
    @Autowired
    private CustomerMigrationValidationStatusService customerMigrationValidationStatusService;
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    @Autowired
    private QueueEventService queueEventService;
    @Autowired
    private PosEventCurrentService posEventCurrentService;
    @Autowired
    private PosEventCurrentProcessor posEventCurrentProcessor;
    @Autowired
    private PosEventHistoryService posEventHistoryService;
    
    private MessageSourceAccessor messageAccessor;
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final void setLocationService(final LocationService locationService) {
        this.locationService = locationService;
    }
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    public final void setRouteService(final RouteService routeService) {
        this.routeService = routeService;
    }
    
    public final void setCustomerMigrationService(final CustomerMigrationService customerMigrationService) {
        this.customerMigrationService = customerMigrationService;
    }
    
    public final void setCustomerMigrationValidationStatusService(
        final CustomerMigrationValidationStatusService customerMigrationValidationStatusService) {
        this.customerMigrationValidationStatusService = customerMigrationValidationStatusService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final MessageSourceAccessor getMessageAccessor() {
        return this.messageAccessor;
    }
    
    public final void setMessageAccessor(final MessageSourceAccessor messageAccessor) {
        this.messageAccessor = messageAccessor;
    }
    
    @Override
    public final void setMessageSource(final MessageSource messageSource) {
        this.messageAccessor = new MessageSourceAccessor(messageSource);
    }
    
    @Override
    public LocationRouteFilterInfo createLocationRouteFilter(final Integer customerId, final RandomKeyMapping keyMapping) {
        
        final LocationRouteFilterInfo locationRouteInfo = new LocationRouteFilterInfo();
        
        final List<Location> usedLocationList = this.locationService.findLocationsWithPointOfSalesByCustomerId(customerId);
        for (Location location : usedLocationList) {
            location.setRandomId(keyMapping.getRandomString(location, WebCoreConstants.ID_LOOK_UP_NAME));
            if (location.getLocation() == null) {
                locationRouteInfo.addLocation(location.getName(), location.getRandomId(), "", location.getIsParent());
            } else {
                locationRouteInfo.addLocation(location.getName(), location.getRandomId(), location.getLocation().getName(), location.getIsParent());
            }
        }
        final List<Route> usedRouteList = this.routeService.findRoutesWithPointOfSalesByCustomerId(customerId);
        for (Route route : usedRouteList) {
            route.setRandomId(keyMapping.getRandomString(route, WebCoreConstants.ID_LOOK_UP_NAME));
            locationRouteInfo.addRoute(route.getName(), route.getRandomId());
        }
        
        return locationRouteInfo;
    }
    
    @Override
    public LocationRouteFilterInfo createLocationRouteFilter(final Integer customerId, final RandomKeyMapping keyMapping, final Integer routeTypeId) {
        
        final LocationRouteFilterInfo locationRouteInfo = new LocationRouteFilterInfo();
        
        final List<Location> usedLocationList = this.locationService.findLocationsWithPointOfSalesByCustomerId(customerId);
        for (Location location : usedLocationList) {
            location.setRandomId(keyMapping.getRandomString(location, WebCoreConstants.ID_LOOK_UP_NAME));
            if (location.getLocation() == null) {
                locationRouteInfo.addLocation(location.getName(), location.getRandomId(), "", location.getIsParent());
            } else {
                locationRouteInfo.addLocation(location.getName(), location.getRandomId(), location.getLocation().getName(), location.getIsParent());
            }
        }
        final List<Route> usedRouteList = this.routeService.findRoutesWithPointOfSalesByCustomerIdAndRouteType(customerId, routeTypeId);
        for (Route route : usedRouteList) {
            route.setRandomId(keyMapping.getRandomString(route, WebCoreConstants.ID_LOOK_UP_NAME));
            locationRouteInfo.addRoute(route.getName(), route.getRandomId());
        }
        
        return locationRouteInfo;
    }
    
    @Override
    public String getMessage(final String key) {
        return this.messageAccessor.getMessage(key);
    }
    
    @Override
    public String getMessage(final String code, final Object... args) {
        return this.messageAccessor.getMessage(code, args);
    }
    
    @Override
    public final Integer resolveCustomerId(final HttpServletRequest request, final HttpServletResponse response) {
        Integer customerId;
        final Customer customer = WebSecurityUtil.getUserAccount().getCustomer();
        if (customer.getCustomerType() == null || customer.getCustomerType().getId() != WebCoreConstants.CUSTOMER_TYPE_DPT) {
            customerId = customer.getId();
        } else {
            customerId = WebCoreUtil
                    .verifyRandomIdAndReturnActual(response, SessionTool.getInstance(request).getKeyMapping(),
                                                   request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID),
                                                   new String[] { "+++ Invalid randomised customerID in CommonControllerHelper. +++" });
        }
        return customerId;
    }
    
    @Override
    public final TimeZone resolveTimeZone(final Integer customerId) {
        String timeZone = null;
        final Customer customer = WebSecurityUtil.getUserAccount().getCustomer();
        if (customer.getCustomerType() == null || customer.getCustomerType().getId() != WebCoreConstants.CUSTOMER_TYPE_DPT) {
            
            timeZone = WebSecurityUtil.getCustomerTimeZone();
        } else {
            final CustomerProperty timeZoneProperty = this.customerAdminService
                    .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
            timeZone = timeZoneProperty.getPropertyValue();
        }
        
        return TimeZone.getTimeZone(timeZone);
    }
    
    @Override
    public final Date getEarliestMigrationDate(final Date customerBoardedGmt) {
        Date earliestMigrationDate = null;
        if (customerBoardedGmt != null) {
            final long waitTime = this.emsPropertiesService.getPropertyValueAsLong(EmsPropertiesService.CUSTOMER_MIGRATION_WAIT_TIME,
                                                                                   EmsPropertiesService.CUSTOMER_MIGRATION_WAIT_TIME_DEFAULT);
            
            earliestMigrationDate = new Date(customerBoardedGmt.getTime() + waitTime);
            
        }
        return earliestMigrationDate;
    }
    
    @Override
    public final boolean getReadyForMigration(final Byte migrationValidationStatus, final Date earliestMigrationDate) {
        boolean showMigrationButton = false;
        
        final boolean isPassedEarliestDate = earliestMigrationDate.before(DateUtil.getCurrentGmtDate());
        
        if (isPassedEarliestDate && migrationValidationStatus != null
            && migrationValidationStatus.intValue() != WebCoreConstants.CUSTOMER_MIGRATION_VALIDATION_STATUS_TYPE_BLOCKED) {
            showMigrationButton = true;
        }
        
        return showMigrationButton;
    }
    
    @Override
    public Customer insertCustomerInModelForSystemAdmin(final HttpServletRequest request, final ModelMap model) {
        final String custRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer customerId = (Integer) keyMapping.getKey(custRandomId);
        
        final Customer customer = this.customerAdminService.findCustomerByCustomerId(customerId);
        customer.setRandomId(keyMapping.getRandomString(customer, WebCoreConstants.ID_LOOK_UP_NAME));
        
        final String timeZone = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customer.getId(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE)
                .getPropertyValue();
        
        final CustomerDetails customerDetails = new CustomerDetails(customer, null, null, null, null, null, null, null, null,
                timeZone == null || !timeZone.startsWith("US/"));
        model.put("customerDetails", customerDetails);
        
        final CustomerMigration customerMigration = this.customerMigrationService.findCustomerMigrationByCustomerId(customerId);
        if (customerMigration != null) {
            setModelObjectsForMigrationLight(customerMigration, model);
        }
        return customer;
    }
    
    @Override
    public final String getRequestTimeRangeString() {
        final TimeZone customerTimeZone = TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone());
        final TimeZone dptTimeZone = TimeZone.getTimeZone(WebCoreConstants.SYSTEM_ADMIN_UI_TIMEZONE);
        
        final Calendar start = Calendar.getInstance(dptTimeZone);
        final Calendar end = Calendar.getInstance(dptTimeZone);
        start.set(Calendar.HOUR_OF_DAY, SCHEDULE_MIN_HOUR_9);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 0);
        start.set(Calendar.MILLISECOND, 0);
        
        end.set(Calendar.HOUR_OF_DAY, SCHEDULE_MAX_HOUR_16);
        end.set(Calendar.MINUTE, 0);
        end.set(Calendar.SECOND, 0);
        end.set(Calendar.MILLISECOND, 0);
        
        final String startTimeStr = DateUtil.format(start.getTime(), DateUtil.TIME_WITH_AMPM_FORMAT, customerTimeZone);
        final String endTimeStr = DateUtil.format(end.getTime(), DateUtil.TIME_WITH_AMPM_FORMAT, customerTimeZone);
        
        return getMessage(LabelConstants.LABEL_SETTINGS_GLOBALPREF_MIGRATION_TIMERANGE, startTimeStr, endTimeStr);
    }
    
    @Override
    public final void setModelObjectsForMigration(final CustomerMigration customerMigration, final ModelMap model, final TimeZone timeZone) {
        
        boolean showMigrationButton = false;
        boolean isMigrationRequested = false;
        String earliestMigrationDate = null;
        String migrationRequestedDate = null;
        String migrationScheduledDate = null;
        String migrationDate = null;
        String migrationScheduleTimeRange = null;
        
        final int customerMigrationStatusType = customerMigration.getCustomerMigrationStatusType().getId();
        
        if (customerMigrationStatusType == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_BOARDED) {
            final Date earliestMigrationDateAsDate = getEarliestMigrationDate(customerMigration.getCustomerBoardedGmt());
            earliestMigrationDate = DateUtil.createMigrationDateOnlyString(earliestMigrationDateAsDate, timeZone);
            final byte customerMigrationValidationStatus = customerMigration.getCustomerMigrationValidationStatusType() == null
                    ? WebCoreConstants.CUSTOMER_MIGRATION_VALIDATION_STATUS_TYPE_BLOCKED : customerMigration
                            .getCustomerMigrationValidationStatusType().getId();
            showMigrationButton = getReadyForMigration(customerMigrationValidationStatus, earliestMigrationDateAsDate);
        } else if (customerMigrationStatusType == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMPLETED
                   || customerMigrationStatusType == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMMUNICATION_CHECKED
                   || customerMigrationStatusType == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_ALL_PAYSTATIONS_MIGRATED) {
            migrationDate = DateUtil.createMigrationDateTimeString(customerMigration.getMigrationEndGmt(), timeZone);
        } else if (customerMigrationStatusType != WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_NOT_BOARDED) {
            migrationRequestedDate = DateUtil.createMigrationDateTimeString(customerMigration.getMigrationRequestedGmt(), timeZone);
            isMigrationRequested = true;
            if (customerMigrationStatusType == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_REQUESTED) {
                migrationScheduledDate = DateUtil.createMigrationDateOnlyString(customerMigration.getMigrationScheduledGmt(), timeZone);
                migrationScheduleTimeRange = getRequestTimeRangeString();
            } else {
                migrationScheduledDate = DateUtil.createMigrationDateTimeString(customerMigration.getMigrationScheduledGmt(), timeZone);
            }
        }
        
        setModelObjectsForMigrationLight(customerMigration, model);
        
        model.put("showMigrationButton", showMigrationButton);
        model.put("isMigrationRequested", isMigrationRequested);
        model.put("earliestMigrationDate", earliestMigrationDate);
        model.put("migrationRequestedDate", migrationRequestedDate);
        model.put("migrationScheduledDate", migrationScheduledDate);
        model.put("migrationDate", migrationDate);
        model.put("migrationScheduleTimeRange", migrationScheduleTimeRange);
        
    }
    
    @Override
    public final void setModelObjectsForMigrationLight(final CustomerMigration customerMigration, final ModelMap model) {
        
        boolean isMigrated = false;
        final int customerMigrationStatusType = customerMigration.getCustomerMigrationStatusType().getId();
        
        if (customerMigrationStatusType == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMPLETED
            || customerMigrationStatusType == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMMUNICATION_CHECKED
            || customerMigrationStatusType == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_ALL_PAYSTATIONS_MIGRATED) {
            isMigrated = true;
        }
        model.put("isMigrated", isMigrated);
        model.put("migrationAvailabilityStatus", getMigrationAvailabilityStatus(customerMigration));
        
    }
    
    private int getMigrationAvailabilityStatus(final CustomerMigration customerMigration) {
        int migrationAvailabilityStatus = WAIT_FOR_MIGRATION_DATE;
        Date earliestMigrationDate = null;
        final int customerMigrationStatusType = customerMigration.getCustomerMigrationStatusType().getId();
        
        if (customerMigrationStatusType == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_NOT_BOARDED) {
            migrationAvailabilityStatus = NOT_BOARDED;
        } else if (customerMigrationStatusType == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_BOARDED) {
            earliestMigrationDate = getEarliestMigrationDate(customerMigration.getCustomerBoardedGmt());
            migrationAvailabilityStatus = getBoardedMigrationAvailabilityStatus(customerMigration, earliestMigrationDate);
        } else if (customerMigrationStatusType == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMPLETED
                   || customerMigrationStatusType == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMMUNICATION_CHECKED
                   || customerMigrationStatusType == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_ALL_PAYSTATIONS_MIGRATED) {
            migrationAvailabilityStatus = MIGRATED;
        } else {
            if (customerMigrationStatusType == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_CANCELLED
                || customerMigrationStatusType == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SUSPENDED) {
                migrationAvailabilityStatus = FAILED;
            } else if (customerMigrationStatusType == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_REQUESTED) {
                migrationAvailabilityStatus = REQUESTED;
            } else if (customerMigrationStatusType == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SCHEDULED) {
                migrationAvailabilityStatus = SCHEDULED;
            } else {
                migrationAvailabilityStatus = RUNNING;
            }
        }
        return migrationAvailabilityStatus;
    }
    
    private int getBoardedMigrationAvailabilityStatus(final CustomerMigration customerMigration, final Date earliestMigrationDate) {
        int migrationAvailabilityStatus = 0;
        if (earliestMigrationDate.after(DateUtil.getCurrentGmtDate())) {
            migrationAvailabilityStatus = WAIT_FOR_MIGRATION_DATE;
        } else {
            final List<CustomerMigrationValidationStatus> validationStatusList = this.customerMigrationValidationStatusService
                    .findUserFacingValidationsByCustomerId(customerMigration.getCustomer().getId());
            boolean hasNotifications = false;
            boolean isBlocked = false;
            for (CustomerMigrationValidationStatus validationStatus : validationStatusList) {
                if (!validationStatus.isIsPassed()) {
                    hasNotifications = true;
                    if (validationStatus.getCustomerMigrationValidationType().isIsBlocking()) {
                        isBlocked = true;
                    }
                }
            }
            if (isBlocked) {
                migrationAvailabilityStatus = READY_BUT_BLOCKED;
            } else if (hasNotifications) {
                migrationAvailabilityStatus = READY_WITH_NOTIFICATION;
            } else {
                migrationAvailabilityStatus = READY_ALL_CLEAR;
            }
        }
        return migrationAvailabilityStatus;
    }
    
    @Override
    public final String clearActiveAlert(final HttpServletRequest request, final HttpServletResponse response, final boolean isActive) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        PosEventHistory activePosEventHistory = null;
        PosEventCurrent activePosEventCurrent = null;
        Integer eventActionTypeId = null;
        
        EventType eventType = null;
        PointOfSale pointOfSale = null;
        
        if (isActive) {
            final String strPosEventCurrentId = request.getParameter("activePosAlertId");
            if (strPosEventCurrentId == null) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            final Long posecId = verifyRandomIdAndReturnActualLong(response, keyMapping, strPosEventCurrentId,
                                                                   "+++ Invalid posEventCurrentId in CommonControllerHelper.clearActiveAlert() method. +++");
            
            activePosEventCurrent = this.posEventCurrentService.findActivePosEventCurrentById(posecId);
            
            if (activePosEventCurrent == null) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            if (activePosEventCurrent.getCustomerAlertType() != null) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            if (activePosEventCurrent.getEventActionType() != null) {
                eventActionTypeId = activePosEventCurrent.getEventActionType().getId();
            }
            eventType = activePosEventCurrent.getEventType();
            pointOfSale = activePosEventCurrent.getPointOfSale();
        } else {
            final String strPosEventHistoryId = request.getParameter("posAlertRandomId");
            if (strPosEventHistoryId == null) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            final Long posehId = verifyRandomIdAndReturnActualLong(response, keyMapping, strPosEventHistoryId,
                                                                   "+++ Invalid posEventHistoryId in CommonControllerHelper.clearActiveAlert() method. +++");
            
            activePosEventHistory = this.posEventHistoryService.findActivePosEventHistoryById(posehId);
            
            if (activePosEventHistory == null) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            if (activePosEventHistory.getCustomerAlertType() != null) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            if (activePosEventHistory.getEventActionType() != null) {
                eventActionTypeId = activePosEventHistory.getEventActionType().getId();
            }
            eventType = activePosEventHistory.getEventType();
            pointOfSale = activePosEventHistory.getPointOfSale();
            
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        return createClearEvent(eventActionTypeId, eventType, userAccount.getId(), pointOfSale.getId());
    }
    
    @Override
    public final String createClearEvent(final Integer eventActionTypeId, final EventType eventType, final Integer userAccountId,
        final Integer pointOfSaleId) {
        final Date now = DateUtil.getCurrentGmtDate();
        final QueueEvent queueEventCurrent = new QueueEvent();
        queueEventCurrent.setCreatedGMT(now);
        queueEventCurrent.setCustomerAlertTypeId(null);
        queueEventCurrent.setEventActionTypeId(eventActionTypeId);
        queueEventCurrent.setEventSeverityTypeId(WebCoreConstants.SEVERITY_CLEAR);
        queueEventCurrent.setEventTypeId(eventType.getId());
        queueEventCurrent.setIsActive(false);
        queueEventCurrent.setIsNotification(eventType.isIsNotification());
        queueEventCurrent.setLastModifiedByUserId(userAccountId);
        queueEventCurrent.setPointOfSaleId(pointOfSaleId);
        queueEventCurrent.setTimestampGMT(now);
        
        this.posEventCurrentProcessor.processQueueEvent(queueEventCurrent);
        
        fixPOSSensorState(queueEventCurrent);
        
        this.queueEventService.createEventQueue(queueEventCurrent.getPointOfSaleId(), null, null, eventType, WebCoreConstants.SEVERITY_CLEAR,
                                                queueEventCurrent.getEventActionTypeId(), queueEventCurrent.getTimestampGMT(), null, false,
                                                eventType.isIsNotification(), userAccountId, WebCoreConstants.QUEUE_TYPE_HISTORICAL_EVENT);
        
        return WebCoreConstants.RESPONSE_TRUE;
        
    }
    
    private void fixPOSSensorState(final QueueEvent queueEvent) {
        final byte eventTypeId = queueEvent.getEventTypeId();
        final PosSensorState posSensorState = this.pointOfSaleService.findPosSensorStateByPointOfSaleId(queueEvent.getPointOfSaleId());
        boolean isUpdate = false;
        switch (eventTypeId) {
            case WebCoreConstants.EVENT_TYPE_BATTERY_LEVEL:
                posSensorState.setBattery1Level((byte) WebCoreConstants.LEVEL_NORMAL);
                posSensorState.setBattery1Voltage(WebCoreConstants.BATTERY_LOW_VOLTAGE_THRESHOLD);
                isUpdate = true;
                break;
            case WebCoreConstants.EVENT_TYPE_PRINTER_PAPERSTATUS:
                posSensorState.setPrinterPaperLevel((byte) WebCoreConstants.LEVEL_NORMAL);
                posSensorState.setLastPaperLevelGmt(DateUtil.getCurrentGmtDate());
                isUpdate = true;
                break;
            case WebCoreConstants.EVENT_TYPE_PRINTER_PAPERJAM:
                posSensorState.setPrinterStatus((byte) WebCoreConstants.LEVEL_NORMAL);
                posSensorState.setLastPrinterStatusGmt(DateUtil.getCurrentGmtDate());
                isUpdate = true;
                break;
            default:
                break;
        }
        if (isUpdate) {
            this.pointOfSaleService.updatePosSensorState(posSensorState);
        }
        
        if (eventTypeId == WebCoreConstants.EVENT_TYPE_DUPLICATE_SERIALNUMBER) {
            final PosMacAddress posMacAddress = this.posMacAddressService.findPosMacAddressByPointOfSaleId(queueEvent.getPointOfSaleId());
            if (posMacAddress != null) {
                posMacAddress.setAlertCount((short) 0);
                this.posMacAddressService.saveOrUpdate(posMacAddress, false);
            }
        }
    }
    
    public final Integer verifyRandomIdAndReturnActual(final HttpServletResponse response, final RandomKeyMapping keyMapping,
        final String strRandomLocationID, final String message) {
        
        final String[] msgs = new String[] { message, message };
        return WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, strRandomLocationID, msgs);
    }
    
    public final Long verifyRandomIdAndReturnActualLong(final HttpServletResponse response, final RandomKeyMapping keyMapping,
        final String strRandomLocationID, final String message) {
        
        final String[] msgs = new String[] { message, message };
        return WebCoreUtil.verifyRandomIdAndReturnActualLong(response, keyMapping, strRandomLocationID, msgs);
    }
    
}
