package com.digitalpaytech.mvc.support;

import java.io.Serializable;

public class FormErrorStatus implements Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -6197914433346155973L;
    private String errorFieldName;
	private String message;

	public FormErrorStatus() {
		
	}
	
	public FormErrorStatus(String errorFieldName, String message) {
		this.errorFieldName = errorFieldName;
		this.message = message;
	}
	
	public String getErrorFieldName() {
		return errorFieldName;
	}

	public void setErrorFieldName(String errorFieldName) {
		this.errorFieldName = errorFieldName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}