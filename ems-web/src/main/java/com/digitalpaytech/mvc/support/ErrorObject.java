package com.digitalpaytech.mvc.support;

import java.io.Serializable;

public class ErrorObject implements Serializable
{
	private static final long serialVersionUID = 1872228981764504308L;
	private String code;
	private Object[] args;
	private String defaultMesssage;
	
	public ErrorObject()
	{
		// Default Constructor
	}
	public ErrorObject(String code, Object[] args, String defaultMessage)
	{
		this.code = code;
		this.args = args;
		this.defaultMesssage = defaultMessage;
	}
	public ErrorObject(String code, Object[] args)
	{
		this.code = code;
		this.args = args;
		this.defaultMesssage = null;
	}
	
	public ErrorObject(String code)
	{
		this.code = code;
		this.args = null;
		this.defaultMesssage = null;
	}
	
	public Object[] getArgs()
	{
		return args;
	}
	public void setArgs(Object[] args)
	{
		this.args = args;
	}
	public String getCode()
	{
		return code;
	}
	public void setCode(String code)
	{
		this.code = code;
	}
	public String getDefaultMesssage()
	{
		return defaultMesssage;
	}
	public void setDefaultMesssage(String defaultMesssage)
	{
		this.defaultMesssage = defaultMesssage;
	}
	
	
}
