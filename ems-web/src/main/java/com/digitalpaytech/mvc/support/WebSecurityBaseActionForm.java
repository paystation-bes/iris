package com.digitalpaytech.mvc.support;

import com.digitalpaytech.util.WebSecurityConstants;

public abstract class WebSecurityBaseActionForm extends WebSecurityBaseForm {
	
	private static final long serialVersionUID = -9090336681203934508L;
	private int actionFlag = WebSecurityConstants.ACTION_UPDATE;
	protected Boolean createAction = WebSecurityConstants.ACTION_FAILURE;
	protected Boolean updateAction = WebSecurityConstants.ACTION_FAILURE;

	public WebSecurityBaseActionForm() {
		super();
	}

	public WebSecurityBaseActionForm(Object primaryKey) {
		super(primaryKey);
	}

	public int getActionFlag() {
		return actionFlag;
	}

	public void setActionFlag(int actionFlag) {
		this.actionFlag = actionFlag;
	}

	public Boolean getCreateAction() {
		return createAction;
	}

	public void setCreateAction(Boolean createAction) {
		this.createAction = createAction;
	}

	public Boolean getUpdateAction() {
		return updateAction;
	}

	public void setUpdateAction(Boolean updateAction) {
		this.updateAction = updateAction;
	}
}
