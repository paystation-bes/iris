package com.digitalpaytech.mvc.support;

import java.io.Serializable;

public class PointOfSaleSearchForm implements Serializable {
	private static final long serialVersionUID = 901602635160024758L;
	
	private String locationRandomId;
	private String routeRandomId;
	private String itemId;
	private String selectedId;
	
	private Integer page;
	private Integer itemsPerPage;
	private String dataKey;
	
	private String targetRandomId;
	
	public PointOfSaleSearchForm() {
		
	}

	public String getLocationRandomId() {
		return locationRandomId;
	}

	public void setLocationRandomId(String locationRandomId) {
		this.locationRandomId = locationRandomId;
	}

	public String getRouteRandomId() {
		return routeRandomId;
	}

	public void setRouteRandomId(String routeRandomId) {
		this.routeRandomId = routeRandomId;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getItemsPerPage() {
		return itemsPerPage;
	}

	public void setItemsPerPage(Integer itemsPerPage) {
		this.itemsPerPage = itemsPerPage;
	}

	public String getDataKey() {
		return dataKey;
	}

	public void setDataKey(String dataKey) {
		this.dataKey = dataKey;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getSelectedId() {
		return selectedId;
	}

	public void setSelectedId(String selectedId) {
		this.selectedId = selectedId;
	}

	public String getTargetRandomId() {
		return targetRandomId;
	}

	public void setTargetRandomId(String targetRandomId) {
		this.targetRandomId = targetRandomId;
	}
}
