package com.digitalpaytech.mvc.support;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.digitalpaytech.util.PostToken;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class ValidationErrorInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5423271648559992870L;
	private PostToken token;
	
	@XStreamImplicit(itemFieldName="errorStatus")
	private List<FormErrorStatus> errorStatus;	

	public ValidationErrorInfo() {
		this((PostToken) null);
	}
	
	public ValidationErrorInfo(PostToken token) {
		this.token = token;
		this.errorStatus = new ArrayList<FormErrorStatus>();
	}
	
	public ValidationErrorInfo(PostToken token, List<FormErrorStatus> errorStatus) {
		this.token = token;
		this.errorStatus = errorStatus;
	}
	
	public PostToken getToken() {
		return token;
	}

	public void setToken(PostToken token) {
		this.token = token;
	}

	public List<FormErrorStatus> getErrorStatus() {
		return errorStatus;
	}

	public void setErrorStatus(List<FormErrorStatus> errorStatus) {
		this.errorStatus = errorStatus;
	}

	public void addErrorStatus(FormErrorStatus status) {
		this.errorStatus.add(status);
	}
	
	public void addAllErrorStatus(List<FormErrorStatus> statusList) {
		this.errorStatus.addAll(statusList);
	}

	public void removeErrorStatus(int index) {
		this.errorStatus.remove(index);
	}
	
	public void clear() {
		this.errorStatus.clear();
	}
}
