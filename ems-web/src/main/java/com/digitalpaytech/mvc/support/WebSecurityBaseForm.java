package com.digitalpaytech.mvc.support;

import java.io.Serializable;

public abstract class WebSecurityBaseForm implements Serializable {

	private static final long serialVersionUID = -8414341540223445856L;
	private Object primaryKey;

	public WebSecurityBaseForm() {
		this.primaryKey = null;
	}

	public WebSecurityBaseForm(Object primaryKey) {
		this.primaryKey = primaryKey;
	}

	public Object getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(Object primaryKey) {
		this.primaryKey = primaryKey;
	}
}
