package com.digitalpaytech.mvc.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

@Component("controllerStatus")
@Scope("prototype")
public class ControllerStatus implements MessageSourceAware {
	private MessageSourceAccessor messageAccessor;

	private List<ErrorObject> errorObjects = new ArrayList<ErrorObject>();

	private String[] errorMessages;

	public void setMessageAccessor(MessageSourceAccessor messageAccessor) {
		this.messageAccessor = messageAccessor;
	}

	public MessageSourceAccessor getMessageAccessor() {
		return messageAccessor;
	}

	public void addError(String errorCode) {
		errorObjects.add(new ErrorObject(errorCode));
	}

	public void addError(String errorCode, Object[] args) {
		errorObjects.add(new ErrorObject(errorCode, args));
	}

	public void addError(String errorCode, Object[] args, String defaultMessage) {
		errorObjects.add(new ErrorObject(errorCode, args, defaultMessage));
	}

	public void clearErrors() {
		errorObjects.clear();
		errorMessages = null;
	}

	/**
	 * Return the error codes for the field or object, if any. Returns an empty
	 * array instead of null if none.
	 */
	public List<ErrorObject> getErrors() {
		return errorObjects;
	}

	/**
	 * Return the first error codes for the field or object, if any.
	 */
	public String getErrorCode() {
		return (this.errorObjects.size() > 0 ? (String) this.errorObjects
				.get(0).getCode() : "");
	}

	/**
	 * Return the resolved error messages for the field or object, if any.
	 * Returns an empty array instead of null if none.
	 */
	public String[] getErrorMessages() {
		initErrorMessages();
		return this.errorMessages;
	}

	/**
	 * Return the first error message for the field or object, if any.
	 */
	public String getErrorMessage() {
		initErrorMessages();
		return (this.errorMessages.length > 0 ? this.errorMessages[0] : "");
	}

	/**
	 * Extract the error messages from the ObjectError list.
	 */
	private void initErrorMessages() throws NoSuchMessageException {
		if (this.errorMessages == null) {
			if (this.errorObjects.size() > 0) {
				this.errorMessages = new String[this.errorObjects.size()];
				for (int i = 0; i < this.errorObjects.size(); i++) {
					ErrorObject errorObj = this.errorObjects.get(i);
					if ((errorObj.getArgs() == null)
							&& (errorObj.getDefaultMesssage() == null))
						this.errorMessages[i] = this.messageAccessor
								.getMessage(errorObj.getCode());
					else if (errorObj.getDefaultMesssage() == null) {
						this.errorMessages[i] = this.messageAccessor
								.getMessage(errorObj.getCode(),
										errorObj.getArgs());
					} else {
						this.errorMessages[i] = this.messageAccessor
								.getMessage(errorObj.getCode(),
										errorObj.getArgs(),
										errorObj.getDefaultMesssage());
					}
				}
			}
		}
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		messageAccessor = new MessageSourceAccessor(messageSource);
	}
}
