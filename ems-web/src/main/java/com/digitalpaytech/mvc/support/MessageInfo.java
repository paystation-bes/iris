package com.digitalpaytech.mvc.support;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamImplicit;

public class MessageInfo implements Serializable {
	private static final long serialVersionUID = 7434581862137951826L;

	private String token;
	private boolean requireConfirmation = false;
	private boolean requireManualClose = true;
	private boolean error = false;
	
	private String pollingURI;
	private int waitMillisecs;
	
	@XStreamImplicit(itemFieldName="messages")
	private List<String> messages;
	
	public MessageInfo() {
		this.messages = new ArrayList<String>();
	}
	
	public MessageInfo(String token, String... messages) {
		this(token, false, messages);
	}
	
	public MessageInfo(boolean requireConfirmation, String... messages) {
		this((String) null, requireConfirmation, messages);
	}
	
	public MessageInfo(String token, boolean requireConfirmation, String... messages) {
		this.token = token;
		this.requireConfirmation = requireConfirmation;
		if((messages != null) && (messages.length > 0)) {
			this.messages = Arrays.asList(messages);
		}
		else {
			this.messages = new ArrayList<String>();
		}
	}
	
	public void checkback(String pollingURI, int millisecs) {
		this.pollingURI = pollingURI;
		this.waitMillisecs = millisecs;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	public boolean hasMessages() {
		return !this.messages.isEmpty();
	}
	
	public void addMessage(String message) {
		this.messages.add(message);
	}
	
	public void prependMessage(String message) {
		this.messages.add(0, message);
	}

	public List<String> getMessages() {
		return messages;
	}
	
	public void clearMessages() {
		this.messages.clear();
	}

	public boolean isRequireConfirmation() {
		return requireConfirmation;
	}

	public void setRequireConfirmation(boolean requireConfirmation) {
		this.requireConfirmation = requireConfirmation;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public boolean isRequireManualClose() {
		return requireManualClose;
	}

	public void setRequireManualClose(boolean requireManualClose) {
		this.requireManualClose = requireManualClose;
	}
	
	public String getPollingURI() {
		return pollingURI;
	}

	public int getWaitMillisecs() {
		return waitMillisecs;
	}
}
