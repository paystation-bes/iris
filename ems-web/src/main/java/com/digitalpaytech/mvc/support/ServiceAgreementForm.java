package com.digitalpaytech.mvc.support;

import java.io.Serializable;

public class ServiceAgreementForm implements Serializable {
	private static final long serialVersionUID = -863358141701077897L;
	private int customerId;
	private String name;
	private String title;
	private String organization;

	public ServiceAgreementForm() {
		this.name = "";
		this.title = "";
		this.organization = "";
	}
	
	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}
}
