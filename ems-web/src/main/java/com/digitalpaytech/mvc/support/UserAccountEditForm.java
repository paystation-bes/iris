package com.digitalpaytech.mvc.support;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public class UserAccountEditForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5442506525128704717L;
	private String randomizedUserAccountId;
	private Integer userAccountId;
	private String firstName;
	private String lastName;
	private String userName;
	private String emailAddress;
	private boolean status;
	private String userPassword;
	private String passwordConfirm;
	private List<String> randomizedRoleIds;
	private List<String> randomizedChildCustomerIds;
	private boolean isAllChilds;
	private Collection<Integer> roleIds;
	private Collection<Integer> childCustomerIds;
	private String defaultAliasCustomerRandomId;
	private Integer defaultAliasCustomerId;

	public String getRandomizedUserAccountId() {
		return randomizedUserAccountId;
	}

	public void setRandomizedUserAccountId(String randomizedUserAccountId) {
		this.randomizedUserAccountId = randomizedUserAccountId;
	}

	public Integer getUserAccountId() {
		return userAccountId;
	}

	public void setUserAccountId(Integer userAccountId) {
		this.userAccountId = userAccountId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public List<String> getRandomizedRoleIds() {
		return randomizedRoleIds;
	}

	public void setRandomizedRoleIds(List<String> randomizedRoleIds) {
		this.randomizedRoleIds = randomizedRoleIds;
	}

	public Collection<Integer> getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(Collection<Integer> roleIds) {
		this.roleIds = roleIds;
	}

	public List<String> getRandomizedChildCustomerIds() {
		return randomizedChildCustomerIds;
	}

	public void setRandomizedChildCustomerIds(List<String> randomizedChildCustomerIds) {
		this.randomizedChildCustomerIds = randomizedChildCustomerIds;
	}

	public boolean getIsAllChilds() {
		return isAllChilds;
	}

	public void setIsAllChilds(boolean isAllChilds) {
		this.isAllChilds = isAllChilds;
	}

	public Collection<Integer> getChildCustomerIds() {
		return childCustomerIds;
	}

	public void setChildCustomerIds(Collection<Integer> childCustomerIds) {
		this.childCustomerIds = childCustomerIds;
	}
	
    public String getDefaultAliasCustomerRandomId() {
        return defaultAliasCustomerRandomId;
    }

    public void setDefaultAliasCustomerRandomId(String defaultAliasCustomerRandomId) {
        this.defaultAliasCustomerRandomId = defaultAliasCustomerRandomId;
    }

    public Integer getDefaultAliasCustomerId() {
        return defaultAliasCustomerId;
    }

    public void setDefaultAliasCustomerId(Integer defaultAliasCustomerId) {
        this.defaultAliasCustomerId = defaultAliasCustomerId;
    }	
}
