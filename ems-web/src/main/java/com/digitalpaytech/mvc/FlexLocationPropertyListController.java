package com.digitalpaytech.mvc;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.digitalpaytech.domain.FlexLocationProperty;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.dto.FlexLocationPropertySearchCriteria;
import com.digitalpaytech.mvc.support.LocationIntegrationSearchForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.FlexWSService;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.support.WebObjectId;

public class FlexLocationPropertyListController {
    
    protected static final String FORM_SEARCH = "LocationIntegrationSearchForm";
    
    @Autowired
    protected FlexWSService flexWSService;
    
    protected FlexLocationPropertySearchCriteria populateFlexLocationPropertySearchCriteria(HttpServletRequest request,
        LocationIntegrationSearchForm form, Integer customerId, boolean isSystemAdmin) {
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        FlexLocationPropertySearchCriteria criteria = new FlexLocationPropertySearchCriteria();
        criteria.setCustomerId(customerId);
        criteria.setLocationId(keyMapping.getKey(form.getLocationRandomId(), Location.class, Integer.class));
        criteria.setFlexLocationPropertyId(keyMapping.getKey(form.getItemId(), FlexLocationProperty.class, Integer.class));
        
        WebObjectId selectedId = keyMapping.getKeyObject(form.getSelectedId());
        if (selectedId != null) {
            if (Location.class.isAssignableFrom(selectedId.getObjectType())) {
                criteria.setSelectedLocationId((Integer) selectedId.getId());
            } else {
                throw new RuntimeException("UnExpected Object Type for Location Flex Property selector: " + selectedId.getObjectType());
            }
        }
        
        if (StringUtils.isNotBlank(form.getLocationRandomId()) && (criteria.getLocationId() == null)) {
            throw new RuntimeException("Invalid Location Random ID for Customer: " + customerId);
        }
        if (StringUtils.isNotBlank(form.getItemId()) && (criteria.getFlexLocationPropertyId() == null)) {
            throw new RuntimeException("Invalid FlexLocationProperty Random ID for Customer: " + customerId);
        }
        
        if (form.getPage() != null) {
            criteria.setPage(form.getPage());
        }
        
        criteria.setItemsPerPage(WebCoreConstants.FLEX_LOCATION_PROPERTIES_PER_PAGE);
        
        if ((form.getDataKey() != null) && (form.getDataKey().length() > 0)) {
            try {
                criteria.setMaxUpdatedTime(new Date(Long.parseLong(form.getDataKey())));
            } catch (NumberFormatException nfe) {
                // DO NOTHING
            }
        }
        
        if (criteria.getMaxUpdatedTime() == null) {
            criteria.setMaxUpdatedTime(new Date());
        }
        
        return criteria;
    }
    
    @ModelAttribute(FORM_SEARCH)
    public WebSecurityForm<LocationIntegrationSearchForm> initPosSearchForm(HttpServletRequest request) {
        return new WebSecurityForm<LocationIntegrationSearchForm>(null, new LocationIntegrationSearchForm());
    }
}
