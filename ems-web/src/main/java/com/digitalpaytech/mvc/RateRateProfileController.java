package com.digitalpaytech.mvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;

import com.digitalpaytech.domain.Rate;
import com.digitalpaytech.domain.RateProfile;
import com.digitalpaytech.domain.RateRateProfile;
import com.digitalpaytech.mvc.helper.RateProfileHelper;
import com.digitalpaytech.mvc.support.RateRateProfileEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.PermitIssueTypeService;
import com.digitalpaytech.service.RateProfileService;
import com.digitalpaytech.service.RateRateProfileService;
import com.digitalpaytech.service.RateService;
import com.digitalpaytech.util.DashboardUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.validation.RateRateProfileValidator;

/**
 * This class handles the Alert setting request.
 * 
 * @author Brian Kim
 */
@Controller
public class RateRateProfileController {
    public static final String FORM_EDIT = "rateRateProfileEditForm";
    public static final String FORM_SCHEDULE = "rateRateProfileScheduleForm";
    private static final String PARAMETER_RATE_PROFILE_OBJECT_ID = "rateProfileID";
    private static final String PARAMETER_OBJECT_ID = "rateRateProfileID";
    
    private static Logger logger = Logger.getLogger(RateRateProfileController.class);
    
    @Autowired
    protected RateService rateService;
    
    @Autowired
    protected RateProfileService rateProfileService;
    
    @Autowired
    protected RateRateProfileService rateRateProfileService;
    
    @Autowired
    protected PermitIssueTypeService permitIssueTypeService;
    
    @Autowired
    protected RateRateProfileValidator rateRateProfileValidator;
    
    @Autowired
    protected RateProfileHelper rateProfileHelper;
    
    public final void setRateService(final RateService rateService) {
        this.rateService = rateService;
    }
    
    public final void setRateProfileService(final RateProfileService rateProfileService) {
        this.rateProfileService = rateProfileService;
    }
    
    public final void setRateRateProfileService(final RateRateProfileService rateRateProfileService) {
        this.rateRateProfileService = rateRateProfileService;
    }
    
    public final void setPermitIssueTypeService(final PermitIssueTypeService permitIssueTypeService) {
        this.permitIssueTypeService = permitIssueTypeService;
    }
    
    public final void setRateRateProfileValidator(final RateRateProfileValidator rateRateProfileValidator) {
        this.rateRateProfileValidator = rateRateProfileValidator;
    }
    
    protected final String getList(final HttpServletRequest request, final HttpServletResponse response) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strRateProfileId = request.getParameter(PARAMETER_RATE_PROFILE_OBJECT_ID);
        final Integer rateProfileId = keyMapping.getKey(strRateProfileId, RateProfile.class, Integer.class);
        
        final String startDateStr = request.getParameter("startDate");
        if (startDateStr == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        final String endDateStr = request.getParameter("endDate");
        if (endDateStr == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        List<RateRateProfileEditForm> result;
        @SuppressWarnings("unchecked")
        Map<String, RateRateProfileEditForm> ratesMap = (Map<String, RateRateProfileEditForm>) SessionTool.getInstance(request)
                .getAttribute(WebCoreConstants.SESSION_RATE_RATE_PROFILE_MAP_ATTRIBUTE_NAME + strRateProfileId);
        if (ratesMap == null) {
            ratesMap = new HashMap<String, RateRateProfileEditForm>();
        }
        if (rateProfileId == null) {
            result = this.rateProfileHelper.populateRateRateProfileEditFormList(new ArrayList<RateRateProfile>(), ratesMap, startDateStr, endDateStr,
                                                                                keyMapping);
        } else {
            final RateProfile rateProfile = this.rateProfileService.findRateProfileById(rateProfileId);
            if (rateProfile == null) {
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            final boolean isSingleDay = startDateStr.equals(endDateStr);
            final Date startDate = DateUtil.createStartDate(startDateStr, TimeZone.getTimeZone(WebCoreConstants.GMT));
            final Date endDate = DateUtil.createEndDate(endDateStr, TimeZone.getTimeZone(WebCoreConstants.GMT));
            
            final List<RateRateProfile> rateRateProfileList = this.rateRateProfileService.findActiveRatesByRateProfileIdAndDateRange(rateProfileId,
                                                                                                                                     startDate,
                                                                                                                                     endDate,
                                                                                                                                     isSingleDay);
            
            result = this.rateProfileHelper.populateRateRateProfileEditFormList(rateRateProfileList, ratesMap, startDateStr, endDateStr, keyMapping);
        }
        
        return WidgetMetricsHelper.convertToJson(result, "rateRateProfileListInfo", true);
    }
    
    protected final String add(final WebSecurityForm<RateRateProfileEditForm> webSecurityForm, final ModelMap model,
                               final HttpServletRequest request, final HttpServletResponse response, final String urlOnSucceed) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strRateProfileId = request.getParameter(PARAMETER_RATE_PROFILE_OBJECT_ID);
        
        final RateRateProfileEditForm form = webSecurityForm.getWrappedObject();
        form.setRateRateProfileRandomId(keyMapping.getRandomString(RateRateProfile.class,
                                                                   new Integer(DashboardUtil
                                                                           .getTemporaryId(WebCoreConstants.TEMP_RATE_RATE_PROFILE_ID_PREFIX))));
        form.setRateProfileRandomId(strRateProfileId);
        final WebSecurityForm<RateRateProfileEditForm> secureForm = new WebSecurityForm<RateRateProfileEditForm>();
        secureForm.setWrappedObject(new RateRateProfileEditForm());
        model.put(FORM_SCHEDULE, secureForm);
        
        return urlOnSucceed;
    }
    
    protected final String edit(final WebSecurityForm<RateRateProfileEditForm> webSecurityForm, final String randomId,
                                final String rateProfileRandomId, final ModelMap model, final HttpServletRequest request,
                                final HttpServletResponse response, final String urlOnSucceed) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        @SuppressWarnings("unchecked")
        final Map<String, RateRateProfileEditForm> ratesMap = (Map<String, RateRateProfileEditForm>) SessionTool.getInstance(request)
                .getAttribute(WebCoreConstants.SESSION_RATE_RATE_PROFILE_MAP_ATTRIBUTE_NAME + rateProfileRandomId);
        
        try {
            final RateRateProfileEditForm existingForm = retrieveRateRateProfileForEdit(ratesMap, randomId, keyMapping);
            webSecurityForm.setWrappedObject(existingForm);
            
            final WebSecurityForm<RateRateProfileEditForm> secureForm = new WebSecurityForm<RateRateProfileEditForm>();
            secureForm.setWrappedObject(new RateRateProfileEditForm());
            model.put(FORM_SCHEDULE, secureForm);
        } catch (RetrieveException re) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return urlOnSucceed;
    }
    
    protected final String save(final WebSecurityForm<RateRateProfileEditForm> webSecurityForm, final BindingResult result,
                                final HttpServletRequest request, final HttpServletResponse response) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        this.rateRateProfileValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        webSecurityForm.resetToken();
        
        final RateRateProfileEditForm form = webSecurityForm.getWrappedObject();
        
        form.setRate(this.rateService.findRateById(keyMapping.getKey(form.getRateRandomId(), Rate.class, Integer.class)));
        if (form.getRate() != null) {
            form.setRateName(form.getRate().getName());
        }
        
        @SuppressWarnings("unchecked")
        final Map<String, RateRateProfileEditForm> ratesMap = (Map<String, RateRateProfileEditForm>) SessionTool.getInstance(request)
                .getAttribute(WebCoreConstants.SESSION_RATE_RATE_PROFILE_MAP_ATTRIBUTE_NAME + form.getRateProfileRandomId());
        ratesMap.put(form.getRateRateProfileRandomId(), form);
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    protected final String reschedule(final WebSecurityForm<RateRateProfileEditForm> webSecurityForm, final BindingResult result,
                                      final HttpServletRequest request, final HttpServletResponse response) {
        
        this.rateRateProfileValidator.validateReschedule(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        boolean success = false;
        webSecurityForm.resetToken();
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final RateRateProfileEditForm form = webSecurityForm.getWrappedObject();
        
        @SuppressWarnings("unchecked")
        final Map<String, RateRateProfileEditForm> ratesMap = (Map<String, RateRateProfileEditForm>) SessionTool.getInstance(request)
                .getAttribute(WebCoreConstants.SESSION_RATE_RATE_PROFILE_MAP_ATTRIBUTE_NAME + form.getRateProfileRandomId());
        
        RateRateProfileEditForm existingForm = null;
        try {
            existingForm = retrieveRateRateProfileForEdit(ratesMap, form.getRateRateProfileRandomId(), keyMapping);
        } catch (RetrieveException re) {
            logger.info("@ TODO");
        }
        
        if (existingForm != null) {
            existingForm.setUseSunday(form.getUseSunday());
            existingForm.setUseMonday(form.getUseMonday());
            existingForm.setUseTuesday(form.getUseTuesday());
            existingForm.setUseWednesday(form.getUseWednesday());
            existingForm.setUseThursday(form.getUseThursday());
            existingForm.setUseFriday(form.getUseFriday());
            existingForm.setUseSaturday(form.getUseSaturday());
            if (!existingForm.getUseScheduled()) {
                final int startMins = DateUtil.parseMinutes(form.getAvailabilityStartTime(), WebCoreConstants.COLON);
                final int endMins = DateUtil.parseMinutes(form.getAvailabilityEndTime(), WebCoreConstants.COLON);
                if ((startMins == 0) && (endMins == 0)) {
                    existingForm.setAvailabilityStartTime((String) null);
                    existingForm.setAvailabilityEndTime((String) null);
                } else {
                    existingForm.setAvailabilityStartTime(DateUtil.formateMinutes(startMins));
                    existingForm.setAvailabilityEndTime(DateUtil.formateMinutes(endMins));
                }
            } else {
                existingForm.setAvailabilityStartDate(form.getAvailabilityStartDate());
                existingForm.setAvailabilityStartTime(form.getAvailabilityStartTime());
                existingForm.setAvailabilityEndDate(form.getAvailabilityEndDate());
                existingForm.setAvailabilityEndTime(form.getAvailabilityEndTime());
            }
            
            existingForm.setStartDate(form.getStartDate());
            existingForm.setStartTime(form.getStartTime());
            existingForm.setEndDate(form.getEndDate());
            existingForm.setEndTime(form.getEndTime());
            
            ratesMap.put(form.getRateRateProfileRandomId(), existingForm);
            success = true;
        }
        
        return (success ? WebCoreConstants.RESPONSE_TRUE : WebCoreConstants.RESPONSE_FALSE) + WebCoreConstants.COLON + webSecurityForm.getInitToken();
    }
    
    protected final String delete(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        final String strRateProfileId = request.getParameter(PARAMETER_RATE_PROFILE_OBJECT_ID);
        @SuppressWarnings("unchecked")
        final Map<String, RateRateProfileEditForm> ratesMap = (Map<String, RateRateProfileEditForm>) SessionTool.getInstance(request)
                .getAttribute(WebCoreConstants.SESSION_RATE_RATE_PROFILE_MAP_ATTRIBUTE_NAME + strRateProfileId);
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strRateRateProfileId = request.getParameter(PARAMETER_OBJECT_ID);
        if (!ratesMap.containsKey(strRateRateProfileId)) {
            final Integer rateRateProfileId = WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, strRateRateProfileId, new String[] {
                "Invalid randomised rateRateProfileID in RateRateProfileController.delete().",
                "+++ Invalid randomised rateRateProfileID in RateRateProfileController.delete(). +++", });
            if ((rateRateProfileId == null) && (!ratesMap.containsKey(strRateRateProfileId))) {
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            final RateRateProfile rateRateProfile = this.rateRateProfileService.findRateRateProfileById(rateRateProfileId);
            if ((rateRateProfile == null) && (!ratesMap.containsKey(strRateRateProfileId))) {
                response.setStatus(HttpStatus.NOT_FOUND.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        //TODO determine what to do with the exception
        try {
            final RateRateProfileEditForm rateToBeDeleted = retrieveRateRateProfileForEdit(ratesMap, strRateRateProfileId, keyMapping);
            
            final Integer rrpId = keyMapping.getKey(rateToBeDeleted.getRateRateProfileRandomId(), RateRateProfile.class, Integer.class);
            if (rrpId == null) {
                throw new IllegalStateException("Invalid RateRateProfileId !");
            }
            
            if (rrpId < 0) {
                ratesMap.remove(strRateRateProfileId);
            } else {
                rateToBeDeleted.setIsToBeDeleted(true);
                ratesMap.put(rateToBeDeleted.getRateRateProfileRandomId(), rateToBeDeleted);
                
            }
        } catch (RetrieveException re) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    private RateRateProfileEditForm retrieveRateRateProfileForEdit(final Map<String, RateRateProfileEditForm> ratesMap, final String randomId,
                                                                   final RandomKeyMapping keyMapping) throws RetrieveException {
        RateRateProfileEditForm result = null;
        if (ratesMap != null) {
            result = ratesMap.get(randomId);
        }
        if (result == null) {
            final Integer rateRateProfileId = keyMapping.getKey(randomId, RateRateProfile.class, Integer.class);
            if (rateRateProfileId == null) {
                throw new RetrieveException("@ TODO1");
            }
            
            final RateRateProfile rrp = this.rateRateProfileService.findRateRateProfileById(rateRateProfileId);
            if (rrp == null) {
                throw new RetrieveException("@ TODO2");
            }
            
            result = this.rateProfileHelper.populateRateRateProfileEditFormComplete(rrp, keyMapping);
        }
        
        return result;
    }
    
    private class RetrieveException extends Exception {
        private static final long serialVersionUID = 6469410542591074363L;
        
        RetrieveException(final String message) {
            super(message);
        }
    }
    
}
