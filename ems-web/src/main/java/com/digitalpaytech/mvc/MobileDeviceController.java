package com.digitalpaytech.mvc;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;

import com.digitalpaytech.domain.CustomerMobileDevice;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.MobileAppHistory;
import com.digitalpaytech.domain.MobileApplicationType;
import com.digitalpaytech.domain.MobileLicense;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.customeradmin.AppTypeFilterInfo;
import com.digitalpaytech.dto.customeradmin.DeviceHistoryInfo;
import com.digitalpaytech.dto.customeradmin.MobileDeviceInfo;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.mvc.customeradmin.support.ActivityLogFilterForm;
import com.digitalpaytech.mvc.customeradmin.support.AppTypeFilterForm;
import com.digitalpaytech.mvc.customeradmin.support.MobileDeviceEditForm;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.CustomerMobileDeviceService;
import com.digitalpaytech.service.CustomerSubscriptionService;
import com.digitalpaytech.service.MobileAppHistoryService;
import com.digitalpaytech.service.MobileApplicationTypeService;
import com.digitalpaytech.service.MobileLicenseService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.RelativeDateTime;
import com.digitalpaytech.util.support.WebObjectId;
import com.digitalpaytech.validation.customeradmin.ActivityLogFormValidator;
import com.digitalpaytech.validation.customeradmin.AppTypeFilterValidator;

public class MobileDeviceController {
    public static final String FORM_DEVICE = "deviceForm";
    public static final String FORM_APPTYPE = "appTypeFilterForm";
    public static final String FORM_ACTIVITY = "activityForm";
    
    @Autowired
    MobileAppHistoryService mobileAppHistoryService;
    
    @Autowired
    MobileLicenseService mobileLicenseService;
    
    @Autowired
    CustomerMobileDeviceService customerMobileDeviceService;
    
    @Autowired
    AppTypeFilterValidator appTypeFilterValidator;
    
    @Autowired
    CustomerSubscriptionService customerSubscriptionService;
    
    @Autowired
    MobileApplicationTypeService mobileApplicationTypeService;
    
    @Autowired
    ActivityLogFormValidator activityLogFormValidator;
    
    @Autowired
    PointOfSaleService pointOfSaleService;
    
    @Autowired
    CommonControllerHelper commonControllerHelper;
    
    public String mobileDeviceControllerIndex(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        //TODO index
        //EMS 4922    
        //  "/secure/settings/devices/index.html" mapped to 
        //  WEB-INF\templates\settings\devices\deviceList.vm 
        UserAccount userAccount = WebSecurityUtil.getUserAccount();
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        Integer customerId = userAccount.getCustomer().getId(); //TODO this only works for customer admin - potentially sys admin could use this page - not set up yet (3 apr 2014)
        PlacementMapBordersInfo mapBorders = pointOfSaleService.findMapBordersByCustomerId(customerId);
        model.put("mapBorders", mapBorders);
        model.put("appTypeFilter", this.createAppTypeFilterInfo(customerId, keyMapping));
        model.put(FORM_ACTIVITY, this.initActivityForm(request));
        return "/settings/devices/deviceList";
    }
    
    public String getDeviceList(HttpServletRequest request, HttpServletResponse response, WebSecurityForm<AppTypeFilterForm> webSecurityForm,
        BindingResult result, ModelMap model) {
        //TODO getDeviceList
        //EMS 4922
        //a paginated ajax request with a Json response of all devices activated or locked from a 
        //Mobile App. This will also accept a variable for APP type so that the list can be filtered 
        //by different APP type or for the customer as a whole. The list should be sorted by status 
        //with Active then locked devices then sorted by the Friendly Name of the Device. The JSON 
        //should include the random ID, Friendly Name, Status, application assigned to and count 
        //of applications it is assigned to in case of Multiple licenses on a single device.   
        this.appTypeFilterValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        AppTypeFilterForm filter = (AppTypeFilterForm) webSecurityForm.getWrappedObject();
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String appTypeString = filter.getAppType();
        UserAccount userAccount = WebSecurityUtil.getUserAccount();
        Integer customerId = userAccount.getCustomer().getId();
        List<MobileDeviceInfo> devices = new LinkedList<MobileDeviceInfo>();
        
        if ("0".equals(appTypeString)) { // means UI is asking for blocked devices
            MobileDeviceInfo deviceInfo = null;
            for (CustomerMobileDevice device : customerMobileDeviceService.findBlockedMobileDevicesByCustomer(customerId, filter.getPage(),
                WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT)) {
                deviceInfo = new MobileDeviceInfo();
                deviceInfo.setApplicationCount(0);
                deviceInfo.setName(device.getName());
                deviceInfo.setStatusId(WebCoreConstants.MOBILE_DEVICE_STATUS_BLOCKED);
                deviceInfo.setStatus(commonControllerHelper.getMessage("label.mobile.device.status.blocked"));
                deviceInfo.setDescription(device.getDescription());
                deviceInfo.setRandomId(keyMapping.getRandomString(CustomerMobileDevice.class, device.getId()));
                devices.add(deviceInfo);
            }
        } else {
            Integer applicationId = null;
            MobileDeviceInfo deviceInfo = null;
            if (StringUtils.isBlank(appTypeString)) {
                // FILTERING FOR ALL APPLICATIONS
                for (CustomerMobileDevice device : customerMobileDeviceService.findCustomerMobileDeviceByCustomer(customerId, filter.getPage(),
                    WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT)) {
                    deviceInfo = new MobileDeviceInfo();
                    deviceInfo.setRandomId(new WebObjectId(CustomerMobileDevice.class, device.getId()).toString());
                    deviceInfo.setApplicationCount(device.getMobileLicenses().size());
                    if (device.getMobileLicenses().size() > 0) {
                        // DEVICE IS USED BY AT LEAST ONE LICENSE
                        Iterator<MobileLicense> i = device.getMobileLicenses().iterator();
                        deviceInfo.setApplication(i.next().getMobileApplicationType().getName());
                        deviceInfo.setStatus(commonControllerHelper.getMessage("label.mobile.device.status.provisioned"));
                        deviceInfo.setStatusId(WebCoreConstants.MOBILE_DEVICE_STATUS_PROVISIONED);
                    } else {
                        // DEVICE HAS NO LICENSES
                        if (device.getIsBlocked() == 0) {
                            deviceInfo.setStatus(commonControllerHelper.getMessage("label.mobile.device.status.inactive"));
                            deviceInfo.setStatusId(WebCoreConstants.MOBILE_DEVICE_STATUS_INACTIVE);
                        } else {
                            deviceInfo.setStatusId(WebCoreConstants.MOBILE_DEVICE_STATUS_BLOCKED);
                            deviceInfo.setStatus(commonControllerHelper.getMessage("label.mobile.device.status.blocked"));
                        }
                    }
                    deviceInfo.setName(device.getName());
                    devices.add(deviceInfo);
                }
            } else {
                // FILTERING FOR SPECIFIC APP
                applicationId = this.verifyRandomIdAndReturnActual(response, keyMapping, appTypeString,
                    "Invalid app type random id in MobileDeviceController.getDeviceList()");
                for (CustomerMobileDevice device : customerMobileDeviceService.findCustomerMobileDeviceByCustomerAndApplication(customerId,
                    applicationId, filter.getPage(), WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT)) {
                    deviceInfo = new MobileDeviceInfo();
                    deviceInfo.setRandomId(new WebObjectId(CustomerMobileDevice.class, device.getId()).toString());
                    deviceInfo.setApplicationCount(device.getMobileLicenses().size());
                    Iterator<MobileLicense> i = device.getMobileLicenses().iterator(); //TODO should probably only display 1 license (correct) but needs to display a license for that app (not implemented correctly here)
                    deviceInfo.setApplication(i.next().getMobileApplicationType().getName());
                    deviceInfo.setStatus(commonControllerHelper.getMessage("label.mobile.device.status.provisioned"));
                    deviceInfo.setStatusId(WebCoreConstants.MOBILE_DEVICE_STATUS_PROVISIONED);
                    deviceInfo.setName(device.getName());
                    devices.add(deviceInfo);
                }
                //devices.addAll(mobileLicenseService.findMobileDevicesByCustomerAndApplication(customerId, applicationId, filter.getPage(), WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT));
            }
        }
        
        return WidgetMetricsHelper.convertToJson(devices, "deviceList", true);
    }
    
    public String getDeviceDetail(HttpServletRequest request, HttpServletResponse response) {
        //TODO getDeviceDetail
        //EMS 4922
        //returns JSON format of Details on the Device including Firendly Name, Random ID, Description, 
        //and all status items it may have against all mobile Apps including the App Name, App Random Id, 
        //App Label, and status (active, locked or null) 
        String str_deviceId = request.getParameter("randomId");
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        int deviceId = this.verifyRandomIdAndReturnActual(response, keyMapping, str_deviceId,
            "Invalid device random id in CustomerAdminMobileDeviceController.getDeviceDetail()");
        HashMap<Integer, String> statusNames = new HashMap<Integer, String>();
        statusNames.put(WebCoreConstants.MOBILE_DEVICE_STATUS_PROVISIONED,
            commonControllerHelper.getMessage("label.mobile.device.status.provisioned"));
        statusNames.put(WebCoreConstants.MOBILE_DEVICE_STATUS_BLOCKED, commonControllerHelper.getMessage("label.mobile.device.status.blocked"));
        statusNames.put(WebCoreConstants.MOBILE_DEVICE_STATUS_INACTIVE, commonControllerHelper.getMessage("label.mobile.device.status.inactive"));
        return WidgetMetricsHelper.convertToJson(customerMobileDeviceService.findDetailsForCustomerMobileDevice(deviceId, null, statusNames),
            "device", true);
    }
    
    public String getDeviceHistory(HttpServletRequest request, HttpServletResponse response, WebSecurityForm<ActivityLogFilterForm> webSecurityForm,
        BindingResult result, ModelMap model) {
        //TODO getDeviceHistory
        //EMS 4922
        //paginated ajax request for the registration history for each time the device was registered, 
        //released and/or locked, for which App and a time stamp for when it occurred. 
        
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        activityLogFormValidator.validate(webSecurityForm, result, keyMapping);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        ActivityLogFilterForm filter = (ActivityLogFilterForm) webSecurityForm.getWrappedObject();
        
        String str_deviceId = request.getParameter("randomId");
        
        int pageNumber = Integer.parseInt(filter.getPage());
        
        int deviceId = this.verifyRandomIdAndReturnActual(response, keyMapping, str_deviceId,
            "Invalid device random id in CustomerAdminMobileDeviceController.getDeviceHistory()");
        
        List<MobileAppHistory> history = mobileAppHistoryService.getMobileAppHistoryByCustomerMobileDeviceIdAndOrder(deviceId, pageNumber,
            WebCoreConstants.INFINITE_SCROLL_RESULT_COUNT, filter.getColumn(), filter.getOrder());
        
        List<DeviceHistoryInfo> historyInfos = new ArrayList<DeviceHistoryInfo>();
        DeviceHistoryInfo historyInfo;
        for (MobileAppHistory h : history) {
            historyInfo = new DeviceHistoryInfo();
            historyInfo.setDeviceUid(h.getCustomerMobileDevice().getMobileDevice().getUid());
            historyInfo.setTime(new RelativeDateTime(h.getTimestamp(), WebSecurityUtil.getCustomerTimeZone()));
            try {
                historyInfo.setUserName(URLDecoder.decode(h.getUserAccount().getUserName(), WebSecurityConstants.URL_ENCODING_LATIN1));
            } catch (UnsupportedEncodingException e) {
                historyInfo.setUserName(h.getUserAccount().getUserName());
            }
            historyInfo.setFirstName(h.getUserAccount().getFirstName());
            historyInfo.setLastName(h.getUserAccount().getLastName());
            historyInfo.setResult(h.getResult());
            historyInfos.add(historyInfo);
        }
        return WidgetMetricsHelper.convertToJson(historyInfos, "history", true);
    }
    
    public String saveDevice(HttpServletRequest request, HttpServletResponse response, WebSecurityForm<MobileDeviceEditForm> webSecurityForm,
        BindingResult result) {
        //TODO saveDevice
        //EMS 4922
        //form submission to allow for the customer to update the description of the device as well 
        //as the ability to release or lock a device from any one or more apps.
        UserAccount userAccount = WebSecurityUtil.getUserAccount();
        Integer customerId = userAccount.getCustomer().getId();
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        MobileDeviceEditForm form = (MobileDeviceEditForm) webSecurityForm.getWrappedObject();
        Integer deviceId = this.verifyRandomIdAndReturnActual(response, keyMapping, form.getRandomId(),
            "Invalid device random id in CustomerAdminMobileDeviceController.saveDevice()");
        CustomerMobileDevice customerDevice = customerMobileDeviceService.findCustomerMobileDeviceById(deviceId);
        customerDevice.setDescription(form.getDescription());
        customerDevice.setName(form.getName().trim());
        if (form.getIsLocked()) {
            customerMobileDeviceService.blockDevice(customerId, deviceId, userAccount.getId());
        } else {
            customerDevice.setIsBlocked((byte) 0);
        }
        if (form.getReleasedApps() != null) {
            for (String appRandomId : form.getReleasedApps()) {
                if (!StringUtils.isBlank(appRandomId)) {
                    Integer appId = this.verifyRandomIdAndReturnActual(response, keyMapping, appRandomId,
                        "Invalid app random id in CustomerAdminMobileDeviceController.saveDevice()");
                    customerMobileDeviceService.releaseDevice(customerId, deviceId, appId);
                }
            }
        }
        customerMobileDeviceService.save(customerDevice);
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    public String lockDevice(HttpServletRequest request, HttpServletResponse response) {
        //TODO lockDevice
        //EMS 4922
        // a single request with the arguement "deviceID" containing the randomID of the device to 
        //lock the specified device from all apps. 
        String str_deviceId = request.getParameter("deviceID");
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        int deviceId = this.verifyRandomIdAndReturnActual(response, keyMapping, str_deviceId,
            "Invalid device random id in CustomerAdminMobileDeviceController.lockDevice()");
        UserAccount userAccount = WebSecurityUtil.getUserAccount();
        int customerId = userAccount.getCustomer().getId();
        customerMobileDeviceService.blockDevice(customerId, deviceId, userAccount.getId());
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    public String releaseDevice(HttpServletRequest request, HttpServletResponse response) {
        //TODO releaseDevice
        //EMS 4922
        // a single request with the arguement "deviceID" containing the randomID of the device to release 
        // the specified device from all apps. 
        String str_deviceId = request.getParameter("deviceID");
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        int deviceId = this.verifyRandomIdAndReturnActual(response, keyMapping, str_deviceId,
            "Invalid device random id in CustomerAdminMobileDeviceController.releaseDevice()");
        UserAccount userAccount = WebSecurityUtil.getUserAccount();
        int customerId = userAccount.getCustomer().getId();
        customerMobileDeviceService.releaseDevice(customerId, deviceId, userAccount.getId());
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    public String unLockDevice(HttpServletRequest request, HttpServletResponse response) {
        String str_deviceId = request.getParameter("deviceID");
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        int customerMobileDeviceId = this.verifyRandomIdAndReturnActual(response, keyMapping, str_deviceId,
            "Invalid device random id in CustomerAdminMobileDeviceController.releaseDevice()");
        UserAccount userAccount = WebSecurityUtil.getUserAccount();
        int customerId = userAccount.getCustomer().getId();
        customerMobileDeviceService.unBlockDevice(customerId, customerMobileDeviceId);
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    protected Integer verifyRandomIdAndReturnActual(HttpServletResponse response, RandomKeyMapping keyMapping, String str_randomId, String message) {
        String[] msgs = new String[] { message, message };
        return WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, str_randomId, msgs);
    }
    
    public AppTypeFilterInfo createAppTypeFilterInfo(Integer customerId, RandomKeyMapping keyMapping) {
        AppTypeFilterInfo info = new AppTypeFilterInfo();
        List<CustomerSubscription> subscriptions = customerSubscriptionService.findActiveCustomerSubscriptionByCustomerId(customerId, true);
        MobileApplicationType type;
        for (CustomerSubscription subscription : subscriptions) {
            type = subscription.getSubscriptionType().getMobileApplicationType();
            if (type != null) {
                info.addAppType(type.getName(), keyMapping.getRandomString(type, WebCoreConstants.ID_LOOK_UP_NAME), subscription.getLicenseUsed(),
                    subscription.getLicenseCount());
            }
        }
        return info;
    }
    
    public WebSecurityForm<AppTypeFilterForm> initAppTypeFilterForm(HttpServletRequest request) {
        WebSecurityForm<AppTypeFilterForm> webSecurityForm = new WebSecurityForm<AppTypeFilterForm>((Object) null, new AppTypeFilterForm());
        return webSecurityForm;
    }
    
    public WebSecurityForm<MobileDeviceEditForm> initMobileDeviceForm(HttpServletRequest request) {
        WebSecurityForm<MobileDeviceEditForm> form = new WebSecurityForm<MobileDeviceEditForm>((Object) null, new MobileDeviceEditForm(), SessionTool
                .getInstance(request).locatePostToken(FORM_DEVICE));
        return form;
    }
    
    public WebSecurityForm<ActivityLogFilterForm> initActivityForm(HttpServletRequest request) {
        return new WebSecurityForm<ActivityLogFilterForm>((Object) null, new ActivityLogFilterForm(), SessionTool.getInstance(request)
                .locatePostToken(FORM_ACTIVITY));
    }
}
