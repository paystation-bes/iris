package com.digitalpaytech.mvc;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;

import com.digitalpaytech.domain.AlertThresholdType;
import com.digitalpaytech.domain.AlertType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAlertEmail;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.CustomerEmail;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.RandomIdNameSetting;
import com.digitalpaytech.dto.customeradmin.DefinedAlertDetails;
import com.digitalpaytech.dto.customeradmin.SortedSettingListInfo;
import com.digitalpaytech.mvc.support.AlertEditForm;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.AlertTypeService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerAlertTypeService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.PosAlertStatusService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObject;
import com.digitalpaytech.validation.AlertSettingValidator;

/**
 * This class handles the Alert setting request.
 * 
 * @author Brian Kim
 */
@Controller
public class AlertsSettingController implements MessageSourceAware {
    
    private static Logger log = Logger.getLogger(AlertsSettingController.class);
    
    @Autowired
    protected CustomerService customerService;
    
    @Autowired
    protected CustomerAdminService customerAdminService;
    
    @Autowired
    private RouteService routeService;
    
    @Autowired
    private CustomerAlertTypeService customerAlertTypeService;
    
    @Autowired
    private AlertTypeService alertTypeService;
    
    @Autowired
    private AlertSettingValidator alertSettingValidator;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    public final void setRouteService(final RouteService routeService) {
        this.routeService = routeService;
    }
    
    public final void setCustomerAlertTypeService(final CustomerAlertTypeService customerAlertTypeService) {
        this.customerAlertTypeService = customerAlertTypeService;
    }
    
    public final void setAlertTypeService(final AlertTypeService alertTypeService) {
        this.alertTypeService = alertTypeService;
    }
    
    public final void setAlertSettingValidator(final AlertSettingValidator alertSettingValidator) {
        this.alertSettingValidator = alertSettingValidator;
    }
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final void setPosAlertStatusService(final PosAlertStatusService posAlertStatusService) {
    }
    
    public final void setMailerService(final MailerService mailerService) {
        this.mailerService = mailerService;
    }
    
    @Override
    public final void setMessageSource(final MessageSource messageSource) {
        this.messageSource = messageSource;
    }
    
    /**
     * This method will retrieve defined alert list and list of route type.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @param urlOnSucceed
     *            URL when process is successful.
     * @return target vm file URI
     */
    protected final String getAlertListSuper(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        final String urlOnSucceed) {
        
        /* retrieve all route types and set name in AlertSettingListInfo. */
        final List<AlertType> alertTypes = this.alertTypeService.findAllAlertTypes();
        final Collection<AlertType> alertTypeList = new ArrayList<AlertType>();
        alertTypeList.add(new AlertType((short) 0, null, "All", true, new Date(), -1));
        for (AlertType alertType : alertTypes) {
            alertTypeList.add(alertType);
        }
        
        Integer customerId = null;
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        /* initialize SortedSettingListInfo instance. */
        if (userAccount.getCustomer().getCustomerType().getId() == WebCoreConstants.CUSTOMER_TYPE_DPT) {
            final Object actualCustomerId = AlertSettingValidator.validateRandomIdNoForm(request, FormFieldConstants.PARAMETER_CUSTOMER_ID);
            if (actualCustomerId.equals(WebCoreConstants.RECORD_NOT_FOUND)) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return null;
            }
            customerId = (Integer) actualCustomerId;
        } else {
            customerId = userAccount.getCustomer().getId();
        }
        final Collection<SortedSettingListInfo> alertSettingListInfo = this.prepareAlertSettingList(request, 0, customerId);
        
        /* initialize the AlertEditForm. */
        final WebSecurityForm<AlertEditForm> alertEditForm = new WebSecurityForm<AlertEditForm>(null, new AlertEditForm());
        
        model.put("alertTypeList", alertTypeList);
        model.put("alertSettingListInfo", alertSettingListInfo);
        model.put("alertEditForm", alertEditForm);
        model.put("routeList", getRouteList(request, customerId));
        
        return urlOnSucceed;
    }
    
    /**
     * This method will sort the alert list based on user input and convert them
     * into JSON list and pass back to UI.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return Velocity file name (/settings/include/sortAlertList.vm). "false"
     *         in case of any exception or validation failure.
     */
    protected final String sortAlertListSuper(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* prepare SortedSettingListInfo list. */
        Integer customerId = null;
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        if (userAccount.getCustomer().getCustomerType().getId() == WebCoreConstants.CUSTOMER_TYPE_DPT) {
            final Object actualCustomerId = AlertSettingValidator.validateRandomIdNoForm(request, FormFieldConstants.PARAMETER_CUSTOMER_ID);
            if (actualCustomerId.equals(WebCoreConstants.RECORD_NOT_FOUND)) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return null;
            }
            customerId = (Integer) actualCustomerId;
        } else {
            
            customerId = userAccount.getCustomer().getId();
        }
        
        final Collection<SortedSettingListInfo> alertSettingListInfo = this.prepareAlertSettingList(request, 0, customerId);
        
        model.put("alertSettingListInfo", alertSettingListInfo);
        
        return "/settings/include/sortAlertList";
    }
    
    /**
     * This method will view Alert Detail information on the page. When
     * "Edit Alert" is clicked, it will also be called from front-end logic
     * automatically after calling getAlertForm() method.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return JSON string having alert detail information. "false" in case of
     *         any exception or validation failure.
     */
    protected final String viewAlertDetailsSuper(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        /* validate the randomized alertID from request parameter. */
        final Object actualAlertId = AlertSettingValidator.validateRandomIdNoForm(request, "alertID");
        if (actualAlertId.equals(WebCoreConstants.RECORD_NOT_FOUND)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Integer alertId = (Integer) actualAlertId;
        final CustomerAlertType customerAlertType = this.customerAlertTypeService.findCustomerAlertTypeById(alertId);
        
        if (customerAlertType.isIsDeleted()) {
            final MessageInfo message = new MessageInfo();
            message.setError(true);
            message.setToken(null);
            message.addMessage(this.commonControllerHelper.getMessage(ErrorConstants.REQUESTED_ITEM_DELETED));
            return WidgetMetricsHelper.convertToJson(message, WebCoreConstants.UI_MESSAGES, true);
        }
        
        final DefinedAlertDetails definedAlertDetails =
                this.prepareDefinedAlertDetailsForJSON(request, userAccount.getCustomer().getId(), customerAlertType);
        
        /* convert DefinedAlertDetails to JSON string. */
        return WidgetMetricsHelper.convertToJson(definedAlertDetails, "definedAlertDetails", true);
    }
    
    /**
     * This method save user defined alert information.
     * 
     * @param webSecurityForm
     *            User input form
     * @param result
     *            BindingResult object to keep errors.
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return "true" string value if process succeeds, JSON string if fails.
     */
    protected final String saveAlertSuper(final WebSecurityForm<AlertEditForm> webSecurityForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        this.alertSettingValidator.validate(webSecurityForm, result, keyMapping);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        webSecurityForm.resetToken();
        
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_CREATE) {
            /* Add Alert. */
            return addAlert(request, webSecurityForm);
        } else if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
            /* Edit Alert. */
            return editAlert(request, webSecurityForm);
        } else {
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
    }
    
    /**
     * This method deletes User Defined Alert information.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return "true" string value if process succeeds, "false" string value if
     *         process fails.
     */
    protected final String deleteAlertSuper(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        /* validate the randomized alertID from request parameter. */
        
        final Object actualAlertId = AlertSettingValidator.validateRandomIdNoForm(request, "alertID");
        if (actualAlertId.equals(WebCoreConstants.RECORD_NOT_FOUND)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        final CustomerAlertType customerAlertType = this.customerAlertTypeService.findCustomerAlertTypeById((Integer) actualAlertId);
        
        Customer customer = null;
        if (userAccount.getCustomer().getCustomerType().getId() == WebCoreConstants.CUSTOMER_TYPE_DPT) {
            final Object actualCustomerId = AlertSettingValidator.validateRandomIdNoForm(request, FormFieldConstants.PARAMETER_CUSTOMER_ID);
            if (actualCustomerId.equals(WebCoreConstants.RECORD_NOT_FOUND)) {
                return WebCoreConstants.RESPONSE_FALSE;
            }
            customer = this.customerService.findCustomer((Integer) actualCustomerId);
            
        } else {
            customer = userAccount.getCustomer();
        }
        
        customerAlertType.setCustomer(customer);
        customerAlertType.setIsDeleted(true);
        customerAlertType.setIsActive(false);
        customerAlertType.setLastModifiedGmt(new Date());
        customerAlertType.setLastModifiedByUserId(userAccount.getId());
        this.customerAlertTypeService.deleteCustomerAlertType(customerAlertType, userAccount.getId());
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    /**
     * This method sends the test notification email.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param webSecurityForm
     *            User input form
     * @param result
     *            BindingResult object to keep errors.
     * 
     * @return "true" string value if process succeeds, JSON string if fails.
     */
    protected final String testNotificationSuper(final HttpServletRequest request, final HttpServletResponse response,
        final WebSecurityForm<AlertEditForm> webSecurityForm, final BindingResult result) {
        
        webSecurityForm.removeAllErrorStatus();
        final String alertContactStr = request.getParameter("alertContacts");
        final String[] alertContacts = alertContactStr.split("[,]");
        final String alertName = request.getParameter("alertName");
        if (alertContacts == null || alertContacts.length == 0) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("alertContacts",
                    this.alertSettingValidator
                            .getMessage("error.common.required",
                                        new Object[] { this.alertSettingValidator.getMessage("label.settings.alerts.form.notificationEmail") })));
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        for (String email : alertContacts) {
            if (StringUtils.isBlank(email)) {
                webSecurityForm.addErrorStatus(new FormErrorStatus("alertContacts",
                        this.alertSettingValidator
                                .getMessage("error.common.required",
                                            new Object[] { this.alertSettingValidator.getMessage("label.settings.alerts.form.notificationEmail") })));
                break;
            }
            
            if (!email.matches(WebCoreConstants.REGEX_EMAIL)) {
                webSecurityForm.addErrorStatus(new FormErrorStatus("alertContacts",
                        this.alertSettingValidator
                                .getMessage("error.common.invalid",
                                            new Object[] { this.alertSettingValidator.getMessage("label.settings.alerts.form.notificationEmail") })));
                break;
            }
        }
        
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        
        /* send test email. */
        String subject = "";
        if (alertName == null || WebCoreConstants.EMPTY_STRING.equals(alertName)) {
            subject = this.messageSource.getMessage("test.email.notification.subject.template", null, null);
        } else {
            subject = "Test Alert: " + alertName;
        }
        
        final String message = this.messageSource.getMessage("test.email.notification.content.template", null, null);
        for (String email : alertContacts) {
            //mailerService.sendMessage(email, this.messageSource.getMessage("test.email.notification.subject.template", null, null),
            //                          this.messageSource.getMessage("test.email.notification.content.template", null, null), null);
            this.mailerService.sendMessage(email, subject, message, null);
        }
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    /**
     * This method prepares SortedSettingListInfo List.
     * 
     * @param request
     *            HttpServletRequest object
     * @param customerId
     *            customer ID
     * @return Collection of SortedSettingListInfo
     */
    private Collection<SortedSettingListInfo> prepareAlertSettingList(final HttpServletRequest request, final Integer filterType,
        final Integer customerId) {
        final Collection<SortedSettingListInfo> sortedSettingListInfo = new ArrayList<SortedSettingListInfo>();
        Collection<CustomerAlertType> types = null;
        final String sortBy = request.getParameter("sortBy");
        final String orderBy = request.getParameter("orderBy");
        
        if (StringUtils.isBlank(sortBy) || sortBy.equals("name")) {
            if (StringUtils.isBlank(orderBy) || orderBy.equals("asc")) {
                types = this.customerAlertTypeService.findCustomerAlertTypesByCustomerIdOrderByNameAsc(customerId, filterType);
            } else {
                types = this.customerAlertTypeService.findCustomerAlertTypesByCustomerIdOrderByNameDesc(customerId, filterType);
            }
        } else if (sortBy.equals("type")) {
            if (StringUtils.isBlank(orderBy) || orderBy.equals("asc")) {
                types = this.customerAlertTypeService.findCustomerAlertTypesByCustomerIdOrderByAlertTypeAsc(customerId, filterType);
            } else {
                types = this.customerAlertTypeService.findCustomerAlertTypesByCustomerIdOrderByAlertTypeDesc(customerId, filterType);
            }
        } else {
            types = this.customerAlertTypeService.findCustomerAlertTypesByCustomerIdOrderByNameAsc(customerId, filterType);
        }
        
        if (!types.isEmpty()) {
            final Collection<WebObject> webObjs = this.hideObjectById(request, types);
            for (WebObject obj : webObjs) {
                final CustomerAlertType customerAlertType = (CustomerAlertType) obj.getWrappedObject();
                sortedSettingListInfo.add(new SortedSettingListInfo(obj.getPrimaryKey().toString(), customerAlertType.getName(),
                        customerAlertType.getAlertThresholdType().getAlertType().getName(), customerAlertType.isIsActive(),
                        customerAlertType.getAlertThresholdType().getAlertType().getId()));
            }
        }
        
        return sortedSettingListInfo;
    }
    
    /**
     * This method prepares defined alert details to display alert details on
     * the page.
     * 
     * @param customerAlertType
     *            alert ID
     * @return DefinedAlertDetails object
     */
    private DefinedAlertDetails prepareDefinedAlertDetailsForJSON(final HttpServletRequest request, final Integer customerId,
        final CustomerAlertType customerAlertType) {
        final DefinedAlertDetails definedAlertDetails = new DefinedAlertDetails();
        
        definedAlertDetails.setRandomAlertId(request.getParameter("alertID"));
        
        definedAlertDetails.setName(customerAlertType.getName());
        if (customerAlertType.isIsActive()) {
            definedAlertDetails.setStatus(true);
        } else {
            definedAlertDetails.setStatus(false);
        }
        definedAlertDetails.setAlertType(customerAlertType.getAlertThresholdType().getAlertType().getName());
        definedAlertDetails.setAlertTypeId(customerAlertType.getAlertThresholdType().getAlertType().getId());
        definedAlertDetails
                .setAlertThresholdExceedDisplay(this.determineAlertThresholdExceedDisplay(customerAlertType.getAlertThresholdType().getId()));
        definedAlertDetails.setAlertThresholdExceed(new Float(customerAlertType.getThreshold()));
        
        if (customerAlertType.getRoute() == null) {
            definedAlertDetails.setRoute("All Pay Stations");
        } else {
            final Route route = this.routeService.findRouteById(customerAlertType.getRoute().getId());
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            definedAlertDetails.setRoute(route.getName());
            definedAlertDetails.setRandomRouteId(keyMapping.getRandomString(route, WebCoreConstants.ID_LOOK_UP_NAME));
        }
        
        try {
            for (CustomerAlertEmail alertEmail : customerAlertType.getCustomerAlertEmails()) {
                definedAlertDetails.getEmailList()
                        .add(URLDecoder.decode(alertEmail.getCustomerEmail().getEmail(), WebSecurityConstants.URL_ENCODING_LATIN1));
            }
        } catch (UnsupportedEncodingException e) {
            log.error("+++ Failed to encode email name when preparing alert details.", e);
        } finally {
            Collections.sort((List<String>) definedAlertDetails.getEmailList(), new Comparator<String>() {
                public int compare(final String f1, final String f2) {
                    return f1.toString().compareTo(f2.toString());
                }
            });
        }
        
        definedAlertDetails.setIsDelayed(customerAlertType.getIsDelayed());
        definedAlertDetails.setDelayedByMinutes(customerAlertType.getDelayedByMinutes());
        return definedAlertDetails;
    }
    
    /**
     * This method wraps Collection with the primary key and return back.
     * 
     * @param request
     *            HttpServletRequest object
     * @param collection
     *            Collection instance
     * @return Collection of WebObject
     */
    @SuppressWarnings("unchecked")
    private Collection<WebObject> hideObjectById(final HttpServletRequest request, final Collection<?> collection) {
        
        /* make randomised key and set the value to randomId in Route table. */
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        return keyMapping.hideProperties(collection, WebCoreConstants.ID_LOOK_UP_NAME);
    }
    
    /**
     * This method save alert information in CustomerAlertType table.
     * 
     * @param request
     *            HttpServletRequest object
     * @param webSecurityForm
     *            user input form
     * @return "true" + form token value if process succeeds, "false" string if
     *         process fails.
     */
    private String addAlert(final HttpServletRequest request, final WebSecurityForm<AlertEditForm> webSecurityForm) {
        
        final AlertEditForm alertEditForm = webSecurityForm.getWrappedObject();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final CustomerAlertType type = new CustomerAlertType();
        final Date currentTime = new Date();
        Customer customer = null;
        if (userAccount.getCustomer().getCustomerType().getId() == WebCoreConstants.CUSTOMER_TYPE_DPT) {
            final Object actualCustomerId = AlertSettingValidator.validateRandomIdNoForm(request, FormFieldConstants.PARAMETER_CUSTOMER_ID);
            if (actualCustomerId.equals(WebCoreConstants.RECORD_NOT_FOUND)) {
                return WebCoreConstants.RESPONSE_FALSE;
            }
            customer = this.customerService.findCustomer((Integer) actualCustomerId);
            
        } else {
            customer = userAccount.getCustomer();
        }
        
        type.setCustomer(customer);
        
        final int alertThresholdTypeId = this.determineAlertThresholdTypeId(alertEditForm);
        type.setAlertThresholdType(this.alertTypeService.findAlertThresholdTypeById(alertThresholdTypeId));
        if (alertEditForm.getRouteId() != null) {
            type.setRoute(this.routeService.findRouteById(alertEditForm.getRouteId()));
        }
        type.setName(alertEditForm.getAlertName().trim());
        type.setThreshold(Float.parseFloat(alertEditForm.getAlertThresholdExceed()));
        type.setIsActive(alertEditForm.getStatus());
        type.setIsDeleted(false);
        type.setLastModifiedGmt(currentTime);
        type.setLastModifiedByUserId(userAccount.getId());
        type.setIsDelayed(alertEditForm.getIsDelayed());
        type.setDelayedByMinutes(alertEditForm.getDelayedByMinutes());
        
        final Set<CustomerAlertEmail> emails = new HashSet<CustomerAlertEmail>();
        try {
            for (String email : alertEditForm.getAlertContacts()) {
                final CustomerEmail customerEmail = new CustomerEmail(customer, URLEncoder.encode(email, WebSecurityConstants.URL_ENCODING_LATIN1),
                        currentTime, userAccount.getId(), false);
                final CustomerAlertEmail alertEmail = new CustomerAlertEmail(customerEmail, type, currentTime, userAccount.getId(), false);
                alertEmail.setCustomerEmail(customerEmail);
                emails.add(alertEmail);
                //customerEmail.setAlertemails(emails);
            }
        } catch (UnsupportedEncodingException e) {
            log.error("+++ Failed to encode email when creating new alert.", e);
        }
        
        type.setCustomerAlertEmails(emails);
        
        this.customerAlertTypeService.saveCustomerAlertType(type);
        return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(StandardConstants.STRING_COLON).append(webSecurityForm.getInitToken())
                .toString();
    }
    
    /**
     * This method updates determines the AlertThresholdType.alertThresholdTypeId based on the alertTypeId and the
     * value of alertExceedDisplayValue. It bridges the database relation between AlertType and AlertThresholdType.
     * Should probably be changed in the future to have a more direct mapping between the two tables based on the real
     * relationship. This was done to save time by not changing the UI.
     * 
     * @param request
     *            HttpServletRequest object
     * @param webSecurityForm
     *            user input form.
     * @return "true" value if process succeeds, "false" string if process
     *         fails.
     */
    private int determineAlertThresholdTypeId(final AlertEditForm alertEditForm) {
        int alertThresholdId = WebCoreConstants.SHORT_RECORD_NOT_FOUND;
        
        switch (alertEditForm.getAlertTypeId()) {
            case WebCoreConstants.ALERTTYPE_ID_LASTSEENINTERVAL:
                alertThresholdId = WebCoreConstants.ALERT_THRESHOLD_TYPE_LAST_SEEN_INTERVAL_HOUR;
                break;
            case WebCoreConstants.ALERTTYPE_ID_RUNNINGTOTAL:
                alertThresholdId = WebCoreConstants.ALERT_THRESHOLD_TYPE_RUNNING_TOTAL_DOLLAR;
                break;
            case WebCoreConstants.ALERTTYPE_ID_COINCANNISTER:
                alertThresholdId = alertEditForm.getAlertThresholdExceedDisplay() == WebCoreConstants.ALERTTHRESHOLDEXCEEDDISPLAY_COUNT
                        ? WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_COUNT : WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_DOLLARS;
                break;
            case WebCoreConstants.ALERTTYPE_ID_BILLSTACKER:
                alertThresholdId = alertEditForm.getAlertThresholdExceedDisplay() == WebCoreConstants.ALERTTHRESHOLDEXCEEDDISPLAY_COUNT
                        ? WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_COUNT : WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_DOLLARS;
                break;
            case WebCoreConstants.ALERTTYPE_ID_UNSETTLEDCREDITCARD:
                alertThresholdId = alertEditForm.getAlertThresholdExceedDisplay() == WebCoreConstants.ALERTTHRESHOLDEXCEEDDISPLAY_COUNT
                        ? WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_COUNT
                        : WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_DOLLARS;
                break;
            case WebCoreConstants.ALERTTYPE_ID_PAYSTATIONALERT:
                alertThresholdId = WebCoreConstants.ALERT_THRESHOLD_TYPE_PAYSTATION;
                break;
            case WebCoreConstants.ALERTTYPE_ID_LASTSEENCOLLECTION:
                alertThresholdId = WebCoreConstants.ALERT_THRESHOLD_TYPE_OVERDUE_COLLECTION;
                break;
            default:
                break;
        }
        return alertThresholdId;
    }
    
    /**
     * This method updates determines the value for AlertThresholdExceedDisplay which will
     * be (1 = Count) or (2 = Dollar)
     * 
     * @param request
     *            HttpServletRequest object
     * @param webSecurityForm
     *            user input form.
     * @return "true" value if process succeeds, "false" string if process
     *         fails.
     */
    private Short determineAlertThresholdExceedDisplay(final int alertThresholdTypeId) {
        Short alertThresholdExceedDisplay = WebCoreConstants.SHORT_RECORD_NOT_FOUND;
        
        switch (alertThresholdTypeId) {
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_RUNNING_TOTAL_DOLLAR:
                alertThresholdExceedDisplay = WebCoreConstants.ALERTTHRESHOLDEXCEEDDISPLAY_DOLLAR;
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_COUNT:
                alertThresholdExceedDisplay = WebCoreConstants.ALERTTHRESHOLDEXCEEDDISPLAY_COUNT;
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_DOLLARS:
                alertThresholdExceedDisplay = WebCoreConstants.ALERTTHRESHOLDEXCEEDDISPLAY_DOLLAR;
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_COUNT:
                alertThresholdExceedDisplay = WebCoreConstants.ALERTTHRESHOLDEXCEEDDISPLAY_COUNT;
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_DOLLARS:
                alertThresholdExceedDisplay = WebCoreConstants.ALERTTHRESHOLDEXCEEDDISPLAY_DOLLAR;
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_COUNT:
                alertThresholdExceedDisplay = WebCoreConstants.ALERTTHRESHOLDEXCEEDDISPLAY_COUNT;
                break;
            case WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_DOLLARS:
                alertThresholdExceedDisplay = WebCoreConstants.ALERTTHRESHOLDEXCEEDDISPLAY_DOLLAR;
                break;
            default:
                alertThresholdExceedDisplay = WebCoreConstants.SHORT_RECORD_NOT_FOUND;
        }
        return alertThresholdExceedDisplay;
    }
    
    /**
     * This method updates alert information.
     * 
     * @param request
     *            HttpServletRequest object
     * @param webSecurityForm
     *            user input form.
     * @return "true" value if process succeeds, "false" string if process
     *         fails.
     */
    private String editAlert(final HttpServletRequest request, final WebSecurityForm<AlertEditForm> webSecurityForm) {
        
        final AlertEditForm alertEditForm = webSecurityForm.getWrappedObject();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final Date currentTime = new Date();
        
        Customer customer = null;
        if (userAccount.getCustomer().getCustomerType().getId() == WebCoreConstants.CUSTOMER_TYPE_DPT) {
            final Object actualCustomerId = AlertSettingValidator.validateRandomIdNoForm(request, FormFieldConstants.PARAMETER_CUSTOMER_ID);
            if (actualCustomerId.equals(WebCoreConstants.RECORD_NOT_FOUND)) {
                return WebCoreConstants.RESPONSE_FALSE;
            }
            customer = this.customerService.findCustomer((Integer) actualCustomerId);
            
        } else {
            customer = userAccount.getCustomer();
        }
        
        final CustomerAlertType type = new CustomerAlertType();
        type.setId(alertEditForm.getAlertId());
        type.setCustomer(customer);
        
        final int alertThresholdTypeId = this.determineAlertThresholdTypeId(alertEditForm);
        
        type.setAlertThresholdType(new AlertThresholdType(alertThresholdTypeId));
        type.setName(alertEditForm.getAlertName().trim());
        type.setThreshold(Float.parseFloat(alertEditForm.getAlertThresholdExceed()));
        type.setIsActive(alertEditForm.getStatus());
        type.setIsDeleted(false);
        
        if (alertEditForm.getRouteId() != null) {
            type.setRoute(new Route());
            type.getRoute().setId(alertEditForm.getRouteId());
        }
        
        type.setIsDelayed(alertEditForm.getIsDelayed());
        type.setDelayedByMinutes(alertEditForm.getDelayedByMinutes());
        type.setLastModifiedGmt(currentTime);
        type.setLastModifiedByUserId(userAccount.getId());
        
        type.setCustomerAlertEmails(this.addNewEmails(type.getCustomerAlertEmails(), alertEditForm.getAlertContacts(), type, userAccount, customer,
                                                      currentTime));
        
        this.customerAlertTypeService.updateCustomerAlertType(type, alertEditForm.getAlertContacts());
        
        return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(StandardConstants.STRING_COLON).append(webSecurityForm.getInitToken())
                .toString();
    }
    
    /**
     * This method removes old email addresses which are not used according to
     * user's new updated email addresses.
     * 
     * @param existingEmails
     *            existing old email list
     * @param updatedEmails
     *            user's new input email addresses
     * @return Set of AlertEmail object
     */
    private Collection<Integer> removeOldEmails(final CustomerAlertType type, final Collection<String> updatedEmails) {
        final Collection<Integer> result = new ArrayList<Integer>();
        final Set<CustomerAlertEmail> newEmails = new HashSet<CustomerAlertEmail>();
        try {
            for (CustomerAlertEmail ae : type.getCustomerAlertEmails()) {
                boolean isSame = false;
                for (String email : updatedEmails) {
                    email = URLEncoder.encode(email, WebSecurityConstants.URL_ENCODING_LATIN1);
                    if (email.equals(ae.getCustomerEmail().getEmail())) {
                        isSame = true;
                        newEmails.add(ae);
                        break;
                    }
                }
                if (!isSame) {
                    result.add(ae.getId());
                }
            }
        } catch (UnsupportedEncodingException e) {
            log.error("+++ Failed to encode and decode email when updating alert.", e);
        }
        
        type.setCustomerAlertEmails(newEmails);
        return result;
    }
    
    /**
     * This method adds user's new updated email addresses to old email
     * addresses.
     * 
     * @param existingEmails
     *            existing old email addresses
     * @param newEmails
     *            user's input email addresses
     * @param type
     *            CustomerAlertType object
     * @param userAccount
     *            UserAccount object
     * @param customer
     *            Customer object
     * @param currentTime
     *            current server time for update
     * @return Set of AlertEmail object
     */
    private Set<CustomerAlertEmail> addNewEmails(final Set<CustomerAlertEmail> existingEmails, final Collection<String> newEmails,
        final CustomerAlertType type, final UserAccount userAccount, final Customer customer, final Date currentTime) {
        if (newEmails.isEmpty()) {
            existingEmails.removeAll(existingEmails);
        }
        try {
            for (String email : newEmails) {
                email = URLEncoder.encode(email, WebSecurityConstants.URL_ENCODING_LATIN1);
                boolean exist = false;
                for (CustomerAlertEmail ae : existingEmails) {
                    if (ae.getCustomerEmail().getEmail().equals(email)) {
                        exist = true;
                        break;
                    }
                }
                if (exist) {
                    continue;
                } else {
                    final CustomerEmail customerEmail = new CustomerEmail(customer, email, currentTime, userAccount.getId(), false);
                    final CustomerAlertEmail alertEmail = new CustomerAlertEmail(customerEmail, type, currentTime, userAccount.getId(), false);
                    alertEmail.setCustomerEmail(customerEmail);
                    existingEmails.add(alertEmail);
                    //customerEmail.setAlertemails(existingEmails);
                }
            }
        } catch (UnsupportedEncodingException e) {
            log.error("+++ Failed to encode and decode email when updating alert.", e);
        }
        
        return existingEmails;
    }
    
    private Collection<RandomIdNameSetting> getRouteList(final HttpServletRequest request, final Integer customerId) {
        /* retrieve Route infomration. */
        final Collection<Route> routes = this.routeService.findRoutesByCustomerId(customerId);
        final Collection<WebObject> wrappedRoutes = this.hideObjectById(request, routes);
        final Collection<RandomIdNameSetting> routeList = new ArrayList<RandomIdNameSetting>();
        routeList.add(new RandomIdNameSetting("-1", "Select"));
        routeList.add(new RandomIdNameSetting("0", "All Pay Stations"));
        for (WebObject webObj : wrappedRoutes) {
            final Route r = (Route) webObj.getWrappedObject();
            routeList.add(new RandomIdNameSetting(webObj.getPrimaryKey().toString(), r.getName()));
        }
        return routeList;
    }
}
