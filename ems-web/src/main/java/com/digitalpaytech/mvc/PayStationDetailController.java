package com.digitalpaytech.mvc;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.annotation.PostConstruct;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;

import com.digitalpaytech.client.FacilitiesManagementClient;
import com.digitalpaytech.client.dto.FirmwareConfig;
import com.digitalpaytech.client.dto.fms.DeviceGroup;
import com.digitalpaytech.domain.ElavonTransactionDetail;
import com.digitalpaytech.domain.EventDeviceType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.MerchantPOS;
import com.digitalpaytech.domain.ModemSetting;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.PaymentSmartCard;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosBatteryInfo;
import com.digitalpaytech.domain.PosCollection;
import com.digitalpaytech.domain.PosCollectionUser;
import com.digitalpaytech.domain.PosDate;
import com.digitalpaytech.domain.PosSensorInfo;
import com.digitalpaytech.domain.PosSensorState;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.PosStatus;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.RoutePOS;
import com.digitalpaytech.domain.SensorInfoType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.AlertSearchCriteria;
import com.digitalpaytech.dto.PaginatedList;
import com.digitalpaytech.dto.TrendData;
import com.digitalpaytech.dto.customeradmin.AlertInfo;
import com.digitalpaytech.dto.customeradmin.AlertInfoEntry;
import com.digitalpaytech.dto.customeradmin.ModuleValuePair;
import com.digitalpaytech.dto.customeradmin.PayStationDetails;
import com.digitalpaytech.dto.customeradmin.PayStationListCollectionReport;
import com.digitalpaytech.dto.customeradmin.PayStationListCollectionReportDetails;
import com.digitalpaytech.dto.customeradmin.PayStationListCollectionReportList;
import com.digitalpaytech.dto.customeradmin.PayStationListHistoryNote;
import com.digitalpaytech.dto.customeradmin.PayStationListTransactionReport;
import com.digitalpaytech.dto.customeradmin.PayStationListTransactionReportDetails;
import com.digitalpaytech.dto.customeradmin.PayStationListTransactionReportList;
import com.digitalpaytech.dto.customeradmin.SeverityValuePair;
import com.digitalpaytech.dto.customeradmin.StatusValuePair;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.mvc.customeradmin.support.io.dto.SensorHistoryCsvDTO;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.PayStationCollectionFilterForm;
import com.digitalpaytech.mvc.support.PayStationCommentFilterForm;
import com.digitalpaytech.mvc.support.PayStationRecentActivityFilterForm;
import com.digitalpaytech.mvc.support.PayStationTransactionFilterForm;
import com.digitalpaytech.mvc.support.ReportGenerator;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.ribbon.ClientFactory;
import com.digitalpaytech.service.AlertsService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.ElavonTransactionDetailService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.PaymentCardService;
import com.digitalpaytech.service.PaymentSmartCardService;
import com.digitalpaytech.service.PermitService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.PosBatteryInfoService;
import com.digitalpaytech.service.PosDateService;
import com.digitalpaytech.service.PosSensorInfoService;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.service.PurchaseService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.service.SensorInfoTypeService;
import com.digitalpaytech.service.paystation.EventAlertService;
import com.digitalpaytech.service.paystation.PosCollectionService;
import com.digitalpaytech.service.paystation.PosTelemetryService;
import com.digitalpaytech.service.systemadmin.SystemAdminService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.PooledResource;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.SensorUtil;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.TelemetryUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.util.json.JSONConfigurationBean;
import com.digitalpaytech.validation.PayStationCollectionFilterValidator;
import com.digitalpaytech.validation.PayStationCommentFilterValidator;
import com.digitalpaytech.validation.PayStationRecentActivityFilterValidator;
import com.digitalpaytech.validation.PayStationTransactionFilterValidator;
import com.digitalpaytech.validation.customeradmin.AlertCentreFilterValidator;

import io.netty.buffer.ByteBuf;

@Controller
@SuppressWarnings({ "PMD.GodClass", "PMD.TooManyMethods", "PMD.TooManyFields", "PMD.AvoidInstantiatingObjectsInLoops", "PMD.CouplingBetweenObjects",
    "PMD.ExcessiveImports", "PMD.ExcessivePublicCount", "PMD.NcssMethodCount" })
/*
 * "PMD.GodClass" - the fix will involve too many changes to the code and will be lengthy in time
 * "PMD.TooManyMethods" - c'est la vie
 * "PMD.TooManyFields" - c'est la vie
 * "PMD.AvoidInstantiatingObjectsInLoops" - cannot be avoided because we have to have a new object created for every
 * iteration of the loop. See lines: 727, 1743, 733, 1779, 2208, 1082, 1769,
 * 1156, 1759
 * "PMD.CouplingBetweenObjects" - the fix will involve too many changes to the code and will be lengthy in time
 * "PMD.ExcessiveImports" - the fix will involve changing the code structure and fixing GodClass and Coupling
 * "PMD.ExcessivePublicCount" - this is because of all the getters and setters for the numerous fields
 * "PMD.InsufficientStringBufferDeclaration" - cannot define a buffer with fixed length (i.e., a static buffer)
 * because it's not clear to what to set the length to)
 * "PMD.NcssMethodCount" - the fix will involve changes to the code and will be lengthy in time
 */
public class PayStationDetailController {
    
    protected static final Logger LOGGER = Logger.getLogger(PayStationDetailController.class);
    
    private static final String COLLECTION_ID = "collectionId";
    private static final String INVALID_COLLECTION_ID_IN_PAY_STATION_LIST_CONTROLLER = "+++ Invalid collectionId in PayStationListController";
    private static final String PAY_STATION_DETAILS_REPORTS_COLLECTIONS_DETAILS_METHOD = ".payStationDetailsReportsCollectionsDetails() method. +++";
    private static final String STATUS_ACTIVE = "1";
    private static final String STATUS_CLEARED = "0";
    private static final String PAYSTATION_ID = "payStationID";
    private static final String US = "US/";
    private static final String INVALID_PAYSTATIONID_IN_LISTCONTROLLER = "+++ Invalid payStationID in PayStationListController";
    private static final String ERROR_STATUS = "errorStatus";
    private static final String CONTENT_DISPOSITION = "Content-Disposition";
    private static final String ATTACHMENT_FILENAME = "attachment; filename=\"";
    private static final String BACKSLASH_QUOTE = "\"";
    private static final String TEXT_CSV = "text/csv";
    private static final String ACTIVE = "Active";
    private static final String TREND_DATA = "trendData";
    private static final String PAYSTATION_DETAILS_SENSOR_HISTORY = ".payStationDetailsSensorHistory() method. +++";
    
    @Autowired
    protected CommonControllerHelper commonControllerHelper;
    
    @Autowired
    protected AlertCentreFilterValidator alertCentreFilterValidator;
    
    @Autowired
    protected AlertsService alertsService;
    
    @Autowired
    protected RouteService routeService;
    
    @Autowired
    protected PointOfSaleService pointOfSaleService;
    
    @Autowired
    protected CustomerAdminService customerAdminService;
    
    @Autowired
    protected PosSensorInfoService posSensorInfoService;
    
    @Autowired
    protected SystemAdminService systemAdminService;
    
    @Autowired
    protected PosCollectionService posCollectionService;
    
    @Autowired
    protected PurchaseService purchaseService;
    
    @Autowired
    protected EventAlertService eventAlertService;
    
    @Autowired
    protected PermitService permitService;
    
    @Autowired
    protected ProcessorTransactionService processorTransactionService;
    
    @Autowired
    protected PaymentCardService paymentCardService;
    
    @Autowired
    protected MessageHelper messageHelper;
    
    @Autowired
    protected EmsPropertiesService emsPropertiesService;
    
    @Autowired
    protected SensorInfoTypeService sensorInfoTypeService;
    
    @Autowired
    protected PosBatteryInfoService posBatteryInfoService;
    
    @Autowired
    protected PayStationRecentActivityFilterValidator payStationRecentActivityFilterValidator;
    
    @Autowired
    protected PayStationTransactionFilterValidator payStationTransactionFilterValidator;
    
    @Autowired
    protected PayStationCollectionFilterValidator payStationCollectionFilterValidator;
    
    @Autowired
    private PaymentSmartCardService paymentSmartCardService;
    
    @Autowired
    private PosDateService posDateService;
    
    @Autowired
    private PayStationCommentFilterValidator payStationCommentFilterValidator;
    
    @Autowired
    private ReportGenerator reportGenerator;
    
    @Autowired
    private ElavonTransactionDetailService elavonTransactionDetailService;
    
    @Autowired
    private PosTelemetryService posTelemetryService;
    
    @Autowired
    private ClientFactory clientFactory;
    
    private JSON json;
    
    @PostConstruct
    public final void init() {
        final JSONConfigurationBean jsonConfig = new JSONConfigurationBean(true, false, false);
        this.json = new JSON(jsonConfig);
    }
    
    public final AlertCentreFilterValidator getAlertCentreFilterValidator() {
        return this.alertCentreFilterValidator;
    }
    
    public final void setAlertCentreFilterValidator(final AlertCentreFilterValidator alertCentreFilterValidator) {
        this.alertCentreFilterValidator = alertCentreFilterValidator;
    }
    
    public final PosDateService getPosDateService() {
        return this.posDateService;
    }
    
    public final void setPosDateService(final PosDateService posDateService) {
        this.posDateService = posDateService;
    }
    
    public final PayStationCommentFilterValidator getPayStationCommentFilterValidator() {
        return this.payStationCommentFilterValidator;
    }
    
    public final void setPayStationCommentFilterValidator(final PayStationCommentFilterValidator payStationCommentFilterValidator) {
        this.payStationCommentFilterValidator = payStationCommentFilterValidator;
    }
    
    public final CommonControllerHelper getCommonControllerHelper() {
        return this.commonControllerHelper;
    }
    
    public final AlertsService getAlertsService() {
        return this.alertsService;
    }
    
    public final RouteService getRouteService() {
        return this.routeService;
    }
    
    public final PointOfSaleService getPointOfSaleService() {
        return this.pointOfSaleService;
    }
    
    public final CustomerAdminService getCustomerAdminService() {
        return this.customerAdminService;
    }
    
    public final PosSensorInfoService getPosSensorInfoService() {
        return this.posSensorInfoService;
    }
    
    public final SystemAdminService getSystemAdminService() {
        return this.systemAdminService;
    }
    
    public final PosCollectionService getPosCollectionService() {
        return this.posCollectionService;
    }
    
    public final PurchaseService getPurchaseService() {
        return this.purchaseService;
    }
    
    public final EventAlertService getEventAlertService() {
        return this.eventAlertService;
    }
    
    public final PermitService getPermitService() {
        return this.permitService;
    }
    
    public final ProcessorTransactionService getProcessorTransactionService() {
        return this.processorTransactionService;
    }
    
    public final PaymentCardService getPaymentCardService() {
        return this.paymentCardService;
    }
    
    public final MessageHelper getMessageHelper() {
        return this.messageHelper;
    }
    
    public final EmsPropertiesService getEmsPropertiesService() {
        return this.emsPropertiesService;
    }
    
    public final SensorInfoTypeService getSensorInfoTypeService() {
        return this.sensorInfoTypeService;
    }
    
    public final PosBatteryInfoService getPosBatteryInfoService() {
        return this.posBatteryInfoService;
    }
    
    public final PayStationRecentActivityFilterValidator getPayStationRecentActivityFilterValidator() {
        return this.payStationRecentActivityFilterValidator;
    }
    
    public final PayStationTransactionFilterValidator getPayStationTransactionFilterValidator() {
        return this.payStationTransactionFilterValidator;
    }
    
    public final PayStationCollectionFilterValidator getPayStationCollectionFilterValidator() {
        return this.payStationCollectionFilterValidator;
    }
    
    public final PaymentSmartCardService getPaymentSmartCardService() {
        return this.paymentSmartCardService;
    }
    
    public final void setAlertsService(final AlertsService alertsService) {
        this.alertsService = alertsService;
    }
    
    public final void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    public final void setRouteService(final RouteService routeService) {
        this.routeService = routeService;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final void setPosSensorInfoService(final PosSensorInfoService posSensorInfoService) {
        this.posSensorInfoService = posSensorInfoService;
    }
    
    public final void setSystemAdminService(final SystemAdminService systemAdminService) {
        this.systemAdminService = systemAdminService;
    }
    
    public final void setPosCollectionService(final PosCollectionService posCollectionService) {
        this.posCollectionService = posCollectionService;
    }
    
    public final void setPurchaseService(final PurchaseService purchaseService) {
        this.purchaseService = purchaseService;
    }
    
    public final void setEventAlertService(final EventAlertService eventAlertService) {
        this.eventAlertService = eventAlertService;
    }
    
    public final void setPermitService(final PermitService permitService) {
        this.permitService = permitService;
    }
    
    public final void setProcessorTransactionService(final ProcessorTransactionService processorTransactionService) {
        this.processorTransactionService = processorTransactionService;
    }
    
    public final void setPaymentCardService(final PaymentCardService paymentCardService) {
        this.paymentCardService = paymentCardService;
    }
    
    public final void setMessageHelper(final MessageHelper messageHelper) {
        this.messageHelper = messageHelper;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final void setSensorInfoTypeService(final SensorInfoTypeService sensorInfoTypeService) {
        this.sensorInfoTypeService = sensorInfoTypeService;
    }
    
    public final void setPosBatteryInfoService(final PosBatteryInfoService posBatteryInfoService) {
        this.posBatteryInfoService = posBatteryInfoService;
    }
    
    public final void setPayStationRecentActivityFilterValidator(
        final PayStationRecentActivityFilterValidator payStationRecentActivityFilterValidator) {
        this.payStationRecentActivityFilterValidator = payStationRecentActivityFilterValidator;
    }
    
    public final void setPayStationTransactionFilterValidator(final PayStationTransactionFilterValidator payStationTransactionFilterValidator) {
        this.payStationTransactionFilterValidator = payStationTransactionFilterValidator;
    }
    
    public final void setPayStationCollectionFilterValidator(final PayStationCollectionFilterValidator payStationCollectionFilterValidator) {
        this.payStationCollectionFilterValidator = payStationCollectionFilterValidator;
    }
    
    public final void setPaymentSmartCardService(final PaymentSmartCardService paymentSmartCardService) {
        this.paymentSmartCardService = paymentSmartCardService;
    }
    
    public final void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }
    
    public final PosTelemetryService getPosTelemetryService() {
        return this.posTelemetryService;
    }
    
    public final void setPosTelemetryService(final PosTelemetryService posTelemetryService) {
        this.posTelemetryService = posTelemetryService;
    }
    
    //========================================================================================================================
    // "Pay Station Details" sub section
    //========================================================================================================================
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the battery voltage from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the batteryVoltage of the requested pay station in a JSON
     *         document
     */
    public final String payStationDetailsCurrentStatusBatteryVoltageSuper(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strPointOfSaleId = request.getParameter(PAYSTATION_ID);
        final Integer pointOfSaleId =
                this.commonControllerHelper.verifyRandomIdAndReturnActual(response, keyMapping, strPointOfSaleId,
                                                                          INVALID_PAYSTATIONID_IN_LISTCONTROLLER + ".payStationDetailsCurrentStatusBatteryVoltage() "
                                                                                                                  + "method. +++");
        if (pointOfSaleId == null) {
            return null;
        }
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        if (pointOfSale == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final PosSensorState sensorState = this.pointOfSaleService.findPosSensorStateByPointOfSaleId(pointOfSaleId);
        if (sensorState == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        final Float voltage = sensorState.getBattery1Voltage();
        
        return WidgetMetricsHelper.convertToJson(voltage, "batteryVoltage", true);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the input current from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the inputCurrent of the requested pay station in a JSON document
     */
    public final String payStationDetailsCurrentStatusInputCurrentSuper(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strPointOfSaleId = request.getParameter(PAYSTATION_ID);
        final Integer pointOfSaleId = this.commonControllerHelper
                .verifyRandomIdAndReturnActual(response, keyMapping, strPointOfSaleId,
                                               INVALID_PAYSTATIONID_IN_LISTCONTROLLER + ".payStationDetailsCurrentStatusInputCurrent() method. +++");
        if (pointOfSaleId == null) {
            return null;
        }
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        if (pointOfSale == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final PosSensorState sensorState = this.pointOfSaleService.findPosSensorStateByPointOfSaleId(pointOfSaleId);
        if (sensorState == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        final Float current = sensorState.getInputCurrent();
        
        return WidgetMetricsHelper.convertToJson(current, "inputCurrent", true);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the ambient temperature from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the ambientTemperature of the requested pay station in a JSON document
     */
    
    public final String payStationDetailsCurrentStatusAmbientTemperatureSuper(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strPointOfSaleId = request.getParameter(PAYSTATION_ID);
        final Integer pointOfSaleId = this.commonControllerHelper
                .verifyRandomIdAndReturnActual(response, keyMapping,
                                               strPointOfSaleId, INVALID_PAYSTATIONID_IN_LISTCONTROLLER
                                                                 + ".payStationDetailsCurrentStatusAmbientTemperature() method. +++");
        if (pointOfSaleId == null) {
            return null;
        }
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        if (pointOfSale == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final PosSensorState sensorState = this.pointOfSaleService.findPosSensorStateByPointOfSaleId(pointOfSaleId);
        if (sensorState == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String timeZone = this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(pointOfSale.getCustomer().getId(),
                                                                                                                 WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE)
                .getPropertyValue();
        final boolean isMetric = timeZone == null || !timeZone.startsWith(US);
        final Float temp =
                isMetric ? sensorState.getAmbientTemperature() : WebCoreUtil.convertCelsiusToFahrenheit(sensorState.getAmbientTemperature());
        
        return WidgetMetricsHelper.convertToJson(temp, "ambientTemperature", true);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the humidity from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the humidity of the requested pay station in a JSON document
     */
    
    public final String payStationDetailsCurrentStatusRelativeHumiditySuper(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strPointOfSaleId = request.getParameter(PAYSTATION_ID);
        final Integer pointOfSaleId = this.commonControllerHelper
                .verifyRandomIdAndReturnActual(response, keyMapping,
                                               strPointOfSaleId, INVALID_PAYSTATIONID_IN_LISTCONTROLLER
                                                                 + ".payStationDetailsCurrentStatusRelativeHumidity() method. +++");
        if (pointOfSaleId == null) {
            return null;
        }
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        if (pointOfSale == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final PosSensorState sensorState = this.pointOfSaleService.findPosSensorStateByPointOfSaleId(pointOfSaleId);
        if (sensorState == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        final Float humidity = sensorState.getRelativeHumidity();
        
        return WidgetMetricsHelper.convertToJson(humidity, "humidity", true);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the controllerTemperature from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the controllerTemperature of the requested pay station in a JSON document
     */
    
    public final String payStationDetailsCurrentStatusControllerTemperatureSuper(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strPointOfSaleId = request.getParameter(PAYSTATION_ID);
        final Integer pointOfSaleId = this.commonControllerHelper
                .verifyRandomIdAndReturnActual(response, keyMapping,
                                               strPointOfSaleId, INVALID_PAYSTATIONID_IN_LISTCONTROLLER
                                                                 + ".payStationDetailsCurrentStatusControllerTemperature() method. +++");
        if (pointOfSaleId == null) {
            return null;
        }
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        if (pointOfSale == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final PosSensorState sensorState = this.pointOfSaleService.findPosSensorStateByPointOfSaleId(pointOfSaleId);
        if (sensorState == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String timeZone = this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(pointOfSale.getCustomer().getId(),
                                                                                                                 WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE)
                .getPropertyValue();
        final boolean isMetric = timeZone == null || !timeZone.startsWith(US);
        final Float temp =
                isMetric ? sensorState.getControllerTemperature() : WebCoreUtil.convertCelsiusToFahrenheit(sensorState.getControllerTemperature());
        
        return WidgetMetricsHelper.convertToJson(temp, "controllerTemperature", true);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the system load from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the systemLoad of the requested pay station in a JSON document
     */
    
    public final String payStationDetailsCurrentStatusSystemLoadSuper(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strPointOfSaleId = request.getParameter(PAYSTATION_ID);
        final Integer pointOfSaleId = this.commonControllerHelper
                .verifyRandomIdAndReturnActual(response, keyMapping, strPointOfSaleId,
                                               INVALID_PAYSTATIONID_IN_LISTCONTROLLER + ".payStationDetailsCurrentStatusSystemLoad() method. +++");
        if (pointOfSaleId == null) {
            return null;
        }
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        if (pointOfSale == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final PosSensorState sensorState = this.pointOfSaleService.findPosSensorStateByPointOfSaleId(pointOfSaleId);
        if (sensorState == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        final Float load = sensorState.getSystemLoad();
        
        return WidgetMetricsHelper.convertToJson(load, "systemLoad", true);
    }
    
    /**
     * This method will return the contents for the severity and module filters
     * 
     * @param request
     * @param response
     * @param model
     * @return Model object "severityFilter", "moduleFilter" & "statusFilter".
     */
    
    public final String payStationDetailsAlertsFilterSuper(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        final String returnString) {
        
        final List<EventDeviceType> deviceTypes = this.alertsService.findAllEventDeviceTypesExceptPCM();
        final List<ModuleValuePair> moduleFilter = new ArrayList<ModuleValuePair>();
        final List<SeverityValuePair> severityFilter = new ArrayList<SeverityValuePair>();
        final List<StatusValuePair> statusFilter = new ArrayList<StatusValuePair>();
        
        moduleFilter.add(new ModuleValuePair(WebCoreConstants.ALL_STRING, WebCoreConstants.ALL_STRING));
        severityFilter.add(new SeverityValuePair(WebCoreConstants.ALL_STRING, WebCoreConstants.ALL_STRING));
        statusFilter.add(new StatusValuePair(WebCoreConstants.ALL_STRING, WebCoreConstants.ALL_STRING));
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        for (EventDeviceType module : deviceTypes) {
            moduleFilter.add(new ModuleValuePair(keyMapping.getRandomString(module, WebCoreConstants.ID_LOOK_UP_NAME),
                    this.messageHelper.getMessage(module.getName())));
        }
        
        for (EventSeverityType severityType : this.alertsService.findAllEventSeverityTypes()) {
            if (severityType.getId() != WebCoreConstants.SEVERITY_CLEAR) {
                severityFilter.add(new SeverityValuePair(severityType.getName(),
                        keyMapping.getRandomString(severityType, WebCoreConstants.ID_LOOK_UP_NAME)));
            }
        }
        
        statusFilter.add(new StatusValuePair(this.messageHelper.getMessage("label.alerts.active"), STATUS_ACTIVE));
        statusFilter.add(new StatusValuePair(this.messageHelper.getMessage("label.alerts.resolvedAlerts.cleared"), STATUS_CLEARED));
        
        model.put("moduleFilter", moduleFilter);
        model.put("severityFilter", severityFilter);
        model.put("statusFilter", statusFilter);
        
        final WebSecurityForm<PayStationRecentActivityFilterForm> recentActivityFilterForm =
                new WebSecurityForm<PayStationRecentActivityFilterForm>(null, new PayStationRecentActivityFilterForm());
        model.put("recentActivityFilterForm", recentActivityFilterForm);
        
        return returnString;
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns alert information for the requested pay station
     * 
     * @param request
     * @param response
     * @param model
     * @param returnString
     * @return alert information of the requested pay station in a JSON document
     */
    
    public final String payStationDetailsAlertsSuper(final WebSecurityForm<PayStationRecentActivityFilterForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response) {
        
        this.payStationRecentActivityFilterValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), ERROR_STATUS, true);
        }
        
        final PayStationRecentActivityFilterForm form = webSecurityForm.getWrappedObject();
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(form.getPointOfSaleId());
        if (pointOfSale == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final AlertInfo alertInfo = new AlertInfo();
        final TimeZone timeZone = TimeZone.getTimeZone(
                                                       this.customerAdminService
                                                               .getCustomerPropertyByCustomerIdAndCustomerPropertyType(pointOfSale.getCustomer()
                                                                       .getId(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE)
                                                               .getPropertyValue());
        
        final AlertSearchCriteria criteria = new AlertSearchCriteria();
        criteria.setIsAlertCentre(false);
        criteria.setSeverityId(form.getSeverityId());
        criteria.setEventDeviceId(form.getModuleId());
        criteria.setPointOfSaleId(form.getPointOfSaleId());
        criteria.setPage(form.getPage());
        criteria.setFindAll(false);
        
        final String column = form.getColumn() == null ? null : form.getColumn().name();
        final String order = form.getOrder() == null ? null : form.getOrder().name();
        
        if (form.getStatus() == null || WebCoreConstants.EMPTY_STRING.equals(form.getStatus())
            || WebCoreConstants.ALL_STRING.equals(form.getStatus())) {
            criteria.setActive((Boolean) null);
        } else if (form.getStatus().equals(STATUS_ACTIVE)) {
            criteria.setActive(true);
        } else if (!form.getStatus().equals(STATUS_ACTIVE)) {
            criteria.setActive(false);
        }
        
        final Calendar clearedGmt = Calendar.getInstance();
        clearedGmt.add(Calendar.DAY_OF_YEAR, WebCoreConstants.OLDEST_DAY_FOR_HISTORY_ALERT);
        criteria.setMinClearedTime(clearedGmt.getTime());
        
        alertInfo.setAlerts(this.alertsService.findHistory(criteria, timeZone, column, order));
        
        return WidgetMetricsHelper.convertToJson(alertInfo, "alertInfo", true);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns alert information for the requested pay station
     * 
     * @param request
     * @param response
     * @param model
     * @param returnString
     * @return alert information of the requested pay station in a JSON document
     */
    
    public final String payStationDetailsAlertsHistorySuper(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strPointOfSaleId = request.getParameter(PAYSTATION_ID);
        final Integer pointOfSaleId = this.commonControllerHelper
                .verifyRandomIdAndReturnActual(response, keyMapping, strPointOfSaleId,
                                               INVALID_PAYSTATIONID_IN_LISTCONTROLLER + ".payStationDetailsAlerts() method. +++");
        if (pointOfSaleId == null) {
            return null;
        }
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        if (pointOfSale == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final AlertInfo alertInfo = new AlertInfo();
        final TimeZone timeZone = TimeZone.getTimeZone(
                                                       this.customerAdminService
                                                               .getCustomerPropertyByCustomerIdAndCustomerPropertyType(pointOfSale.getCustomer()
                                                                       .getId(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE)
                                                               .getPropertyValue());
        
        final AlertSearchCriteria criteria = new AlertSearchCriteria();
        criteria.setIsAlertCentre(false);
        criteria.setPointOfSaleId(pointOfSaleId);
        criteria.setFindAll(true);
        
        final Calendar clearedGmt = Calendar.getInstance();
        clearedGmt.add(Calendar.DAY_OF_YEAR, WebCoreConstants.OLDEST_DAY_FOR_HISTORY_ALERT);
        criteria.setMinClearedTime(clearedGmt.getTime());
        
        alertInfo.setAlerts(this.alertsService.findHistory(criteria, timeZone, (String) null, (String) null));
        
        final byte[] fileContent = buildAlertHistory(alertInfo, timeZone);
        
        final StringBuilder bdr = new StringBuilder();
        bdr.append(pointOfSale.getName().replace(StandardConstants.STRING_EMPTY_SPACE, StandardConstants.STRING_UNDERSCORE)).append("-Alert_History-")
                .append(DateUtil.createDateString(DateUtil.EXPORT_DATA_DATE_TIME, timeZone, new Date())).append(StandardConstants.STRING_DOT)
                .append(ReportingConstants.REPORTS_FORMAT_CSV);
        final String exportFileName = bdr.toString();
        
        ServletOutputStream outStream = null;
        try {
            response.reset();
            response.setHeader(CONTENT_DISPOSITION, ATTACHMENT_FILENAME + exportFileName + BACKSLASH_QUOTE);
            response.setContentType(TEXT_CSV);
            outStream = response.getOutputStream();
            
            outStream.write(fileContent);
            outStream.flush();
        } catch (IOException ioe) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        } finally {
            try {
                if (outStream != null) {
                    outStream.close();
                }
            } catch (IOException ioe) {
                LOGGER.error("outputStream not closed in PayStationDetailController.payStationDetailsAlertsHistory()");
            }
        }
        
        return WebCoreConstants.RESPONSE_TRUE;
        
    }
    
    @SuppressWarnings("PMD.InsufficientStringBufferDeclaration")
    private byte[] buildAlertHistory(final AlertInfo alertInfo, final TimeZone timeZone) {
        final StringBuilder csvFileContent = new StringBuilder();
        //Header
        csvFileContent.append("Alert,Severity,Alert Time,Resolved Time,Status\n");
        for (AlertInfoEntry alert : alertInfo.getAlerts()) {
            csvFileContent.append(alert.getAlertMessage());
            csvFileContent.append(ReportingConstants.CSV_COMMA);
            csvFileContent.append(alert.getIsInfo() ? "Info" : alert.getSeverityName());
            csvFileContent.append(ReportingConstants.CSV_COMMA);
            csvFileContent.append(DateUtil.convertTimeZoneAndCreateDateString(timeZone.getID(), alert.getAlertDate().getDate()));
            csvFileContent.append(ReportingConstants.CSV_COMMA);
            if (alert.getIsInfo()) {
                csvFileContent.append(WebCoreConstants.N_A_STRING);
                csvFileContent.append(ReportingConstants.CSV_COMMA);
                csvFileContent.append(WebCoreConstants.N_A_STRING);
                
            } else {
                csvFileContent.append(alert.getIsActive() || alert.getResolvedDate() == null ? WebCoreConstants.EMPTY_STRING
                        : DateUtil.convertTimeZoneAndCreateDateString(timeZone.getID(), alert.getResolvedDate().getDate()));
                csvFileContent.append(ReportingConstants.CSV_COMMA);
                csvFileContent.append(alert.getIsActive() ? ACTIVE : "Cleared");
            }
            csvFileContent.append(StandardConstants.STRING_END_OF_LINE);
        }
        
        return csvFileContent.toString().getBytes();
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns pay station details in a PayStationDetails object
     * 
     * @param request
     * @param response
     * @param model
     * @return A PayStationDetails object populated with the details of the
     *         requested pay station in a JSON document
     */
    
    public final String payStationDetailsInformationSuper(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        final boolean sysAdmin) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strPointOfSaleId = request.getParameter(PAYSTATION_ID);
        final Integer pointOfSaleId = this.commonControllerHelper
                .verifyRandomIdAndReturnActual(response, keyMapping, strPointOfSaleId,
                                               INVALID_PAYSTATIONID_IN_LISTCONTROLLER + ".payStationDetailsInformation() method. +++");
        if (pointOfSaleId == null) {
            return null;
        }
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        if (pointOfSale == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final FacilitiesManagementClient fmClient = this.clientFactory.from(FacilitiesManagementClient.class);
        final Future<ByteBuf> deviceGroupInfo =
                fmClient.groupInfoFromDevice(String.valueOf(pointOfSale.getCustomer().getId()), pointOfSale.getSerialNumber()).queue();
        
        final List<MerchantPOS> merchantPoss = this.pointOfSaleService.findMerchPOSByPOSIdNoDeleted(pointOfSaleId);
        String creditCardMerchantAccount = null;
        String valueCardMerchantAccount = null;
        for (MerchantPOS merchantPos : merchantPoss) {
            if (merchantPos.getCardType().getId() == WebCoreConstants.CREDIT_CARD) {
                creditCardMerchantAccount = this.systemAdminService.findMerchantAccountById(merchantPos.getMerchantAccount().getId()).getUIName();
            } else if (merchantPos.getCardType().getId() == WebCoreConstants.VALUE_CARD) {
                valueCardMerchantAccount = this.systemAdminService.findMerchantAccountById(merchantPos.getMerchantAccount().getId()).getName();
            }
        }
        
        final ModemSetting modemSetting = this.pointOfSaleService.findModemSettingByPointOfSaleid(pointOfSaleId);
        String modemSettingsAPNString = null;
        String modemSettingsCCIDString = null;
        String modemSettingsMEIDString = null;
        String modemSettingsCarrierString = null;
        String modemSettingsHardwareManufacturerString = null;
        String modemSettingsHardwareModelString = null;
        String modemSettingsHardwareFirmwareString = null;
        
        if (modemSetting != null) {
            modemSettingsAPNString = modemSetting.getAccessPoint() == null ? WebCoreConstants.EMPTY_STRING : modemSetting.getAccessPoint().getName();
            modemSettingsCCIDString = modemSetting.getCcid();
            modemSettingsCarrierString = modemSetting.getCarrier() == null ? WebCoreConstants.EMPTY_STRING : modemSetting.getCarrier().getName();
            modemSettingsMEIDString = modemSetting.getMeid();
            modemSettingsHardwareManufacturerString = modemSetting.getHardwareManufacturer();
            modemSettingsHardwareModelString = modemSetting.getHardwareModel();
            modemSettingsHardwareFirmwareString = modemSetting.getHardwareFirmware();
        }
        
        final PosStatus posStatus = this.pointOfSaleService.findPointOfSaleStatusByPOSId(pointOfSaleId, true);
        final Date activatedDate = posStatus.getIsActivatedGmt();
        final String activatedDateString =
                DateUtil.getDateFormat().format(DateUtil.changeTimeZone(activatedDate, WebSecurityUtil.getCustomerTimeZone()));
        String status = "Inactive";
        if (posStatus.isIsActivated()) {
            status = ACTIVE;
        }
        
        final List<RoutePOS> routePOSes = this.routeService.findRoutePOSesByPointOfSaleId(pointOfSaleId);
        final List<String> routeNames = new ArrayList<String>();
        final List<String> routeRandomIds = new ArrayList<String>();
        for (RoutePOS routePOS : routePOSes) {
            routeNames.add(routePOS.getRoute().getName());
            routeRandomIds.add(keyMapping.getRandomString(routePOS.getRoute(), WebCoreConstants.ID_LOOK_UP_NAME));
        }
        
        final PosServiceState serviceState = this.pointOfSaleService.findPosServiceStateByPointOfSaleId(pointOfSaleId);
        if (serviceState.isIsNewMerchantAccount()) {
            creditCardMerchantAccount = this.messageHelper.getMessage("label.settings.locations.paystationList.information.updatePending");
        }
        
        final List<String> installedModules = listInstalledModules(serviceState);
        
        final PayStationDetails payStationDetails = new PayStationDetails();
        
        payStationDetails.setName(pointOfSale.getName());
        payStationDetails.setSerialNumber(pointOfSale.getSerialNumber());
        payStationDetails.setControllerSerialNumber(serviceState.getBbserialNumber());
        payStationDetails.setPayStationType(pointOfSale.getPaystation().getPaystationType().getName());
        payStationDetails.setSoftwareVersion(serviceState.getPrimaryVersion());
        payStationDetails.setFirmwareVersion(serviceState.getSecondaryVersion());
        payStationDetails.setCreditCardMerchantAccount(creditCardMerchantAccount);
        payStationDetails.setValueCardMerchantAccount(valueCardMerchantAccount);
        payStationDetails.setLocation(pointOfSale.getLocation().getName());
        payStationDetails.setLocationRandomId(keyMapping.getRandomString(pointOfSale.getLocation(), WebCoreConstants.ID_LOOK_UP_NAME));
        payStationDetails.setSetting(serviceState.getPaystationSettingName());
        if (!routeNames.isEmpty()) {
            payStationDetails.setGroupAssignment(routeNames);
        }
        if (!routeRandomIds.isEmpty()) {
            payStationDetails.setGroupAssignmentRandomIds(routeRandomIds);
        }
        if (!installedModules.isEmpty()) {
            payStationDetails.setInstalledModules(installedModules);
        }
        if (modemSetting != null && modemSetting.getModemType() != null) {
            payStationDetails.setModemType(modemSetting.getModemType().getName());
        }
        payStationDetails.setModemSettingsAPN(modemSettingsAPNString);
        payStationDetails.setModemSettingsCCID(modemSettingsCCIDString);
        payStationDetails.setModemSettingsMEID(modemSettingsMEIDString);
        payStationDetails.setModemSettingsCarrier(modemSettingsCarrierString);
        payStationDetails.setActivationDate(activatedDateString);
        payStationDetails.setStatus(status);
        
        payStationDetails.setTelemetryData(this.posTelemetryService.findActiveByPosId(pointOfSaleId, sysAdmin));
        
        payStationDetails.setModemSettingsHardwareManufacturer(modemSettingsHardwareManufacturerString);
        payStationDetails.setModemSettingsHardwareModel(modemSettingsHardwareModelString);
        payStationDetails.setModemSettingsHardwareFirmware(modemSettingsHardwareFirmwareString);
        
        try {
            final String deviceGroupString = deviceGroupInfo.get().toString(Charset.defaultCharset());
            final Map<String, Object> deviceGroupMap = this.json.deserialize(deviceGroupString);
            payStationDetails.setGroupId(keyMapping.getRandomString(DeviceGroup.class, deviceGroupMap.get(FormFieldConstants.PARAMETER_GROUP_ID)));
            payStationDetails.setGroupName((String) deviceGroupMap.get("groupName"));
        } catch (InterruptedException | ExecutionException e) {
            LOGGER.error("Failed to get group info from facilities management service, " + logUnifiIdSerialNumber(pointOfSale), e);
        } catch (JsonException e) {
            LOGGER.error("Error deserializing response from facilities management service, " + logUnifiIdSerialNumber(pointOfSale), e);
        }
        
        if (pointOfSale.isIsLinux()) {
            try {
                final FirmwareConfig firCfg = this.systemAdminService.getFirmwareConfig(pointOfSale.getSerialNumber());
                int key = StandardConstants.CONSTANT_999;
                if (firCfg != null && firCfg.getUpgradeProgress() != null) {
                    key = firCfg.getUpgradeProgress();
                }
                payStationDetails.setUpgradeStatus(this.messageHelper.getMessage(TelemetryUtil.getUpgradeProgressMessageKey(key)));
            } catch (ApplicationException ae) {
                LOGGER.error(ae);
                payStationDetails.setUpgradeStatus(null);
            }
        }
        return WidgetMetricsHelper.convertToJson(payStationDetails, "payStationDetails", true);
    }
    
    private String logUnifiIdSerialNumber(final PointOfSale pointOfSale) {
        return "unifiId: " + pointOfSale.getCustomer().getUnifiId() + ", serialNumber: " + pointOfSale.getSerialNumber();
    }
    
    /**
     * This method will prepare the page for transaction list
     * 
     * @param request
     * @param response
     * @param model
     * @return Model object "transactionFilterForm".
     */
    
    public final String payStationDetailsTransactionFilterSuper(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model, final String returnString) {
        
        final WebSecurityForm<PayStationTransactionFilterForm> transactionFilterForm =
                new WebSecurityForm<PayStationTransactionFilterForm>(null, new PayStationTransactionFilterForm());
        model.put("transactionFilterForm", transactionFilterForm);
        
        return returnString;
    }
    
    /**
     * This method takes a payStationID request parameter input, and returns
     * data listing transaction reports for the last 90 days
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON objects collectionsReports and transactionReports
     */
    
    public final String viewTransactionReportsSuper(final WebSecurityForm<PayStationTransactionFilterForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response) {
        
        this.payStationTransactionFilterValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), ERROR_STATUS, true);
        }
        
        final PayStationTransactionFilterForm form = webSecurityForm.getWrappedObject();
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(form.getPointOfSaleId());
        if (pointOfSale == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String timeZone = this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(pointOfSale.getCustomer().getId(),
                                                                                                                 WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE)
                .getPropertyValue();
        
        final List<Purchase> purchases =
                this.purchaseService.findPurchasesByPointOfSaleIdLast90Days(pointOfSale.getCustomer().getId(), form.getPointOfSaleId(),
                                                                            form.getOrder().name(), form.getColumn().name(), form.getPage());
        final PayStationListTransactionReportList transactionReports = new PayStationListTransactionReportList();
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        if (purchases != null && !purchases.isEmpty()) {
            for (Purchase purchase : purchases) {
                final PayStationListTransactionReport report = new PayStationListTransactionReport();
                
                report.setRandomId(keyMapping.getRandomString(purchase, WebCoreConstants.ID_LOOK_UP_NAME));
                report.setPaymentType(purchase.getPaymentType().getName());
                report.setType(purchase.getTransactionType().getName());
                report.setAmount(formatToDollars(purchase.getChargedAmount()));
                
                report.setDate(DateUtil.getDateFormat().format(DateUtil.changeTimeZone(purchase.getPurchaseGmt(), timeZone)));
                
                transactionReports.addTransactionReport(report);
            }
        }
        
        return WidgetMetricsHelper.convertToJson(transactionReports, "payStationListTransactionReportList", true);
    }
    
    /**
     * This method will prepare the page for collection list
     * 
     * @param request
     * @param response
     * @param model
     * @return Model object "transactionFilterForm".
     */
    
    public final String payStationDetailsCollectionFilterSuper(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model, final String returnString) {
        
        final WebSecurityForm<PayStationCollectionFilterForm> collectionFilterForm =
                new WebSecurityForm<PayStationCollectionFilterForm>(null, new PayStationCollectionFilterForm());
        model.put("collectionFilterForm", collectionFilterForm);
        
        return returnString;
    }
    
    /**
     * This method takes a payStationID request parameter input, and returns
     * data listing collection reports for the last 90 days
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON objects collectionsReports and transactionReports
     */
    
    public final String viewCollectionReportsSuper(final WebSecurityForm<PayStationCollectionFilterForm> webSecurityForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response) {
        final PayStationListCollectionReportList collectionReports = new PayStationListCollectionReportList();
        String respStr = fillCollectionsReport(collectionReports, (Map<String, Object>) null, webSecurityForm, result, request, response);
        if (respStr == null) {
            respStr = WidgetMetricsHelper.convertToJson(collectionReports, "payStationListCollectionReportList", true);
        }
        
        return respStr;
    }
    
    protected final String printCollectionReportSuper(final WebSecurityForm<PayStationCollectionFilterForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response) {
        final PayStationListCollectionReportList collectionReports = new PayStationListCollectionReportList();
        final Map<String, Object> reportParams = createCollectionReportParams();
        
        webSecurityForm.getWrappedObject().setPage(1);
        String respStr = fillCollectionsReport(collectionReports, reportParams, webSecurityForm, result, request, response);
        if (respStr == null) {
            try {
                this.reportGenerator.generatePDF(response, ReportingConstants.REPORT_JASPER_FILE_COLLECTION_LOG, reportParams,
                                                 collectionReports.getCollectionReports());
                respStr = WebCoreConstants.RESPONSE_TRUE;
            } catch (IOException ioe) {
                LOGGER.error("Failed to print collection report", ioe);
                
                response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
                respStr = WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return respStr;
    }
    
    private Map<String, Object> createCollectionReportParams() {
        final Map<String, Object> params = new HashMap<String, Object>(5);
        params.put(ReportGenerator.PARAM_REPORT_TITLE, this.messageHelper.getMessage(ReportingConstants.REPORT_TYPE_NAME_COLLECTION_LOG));
        params.put(ReportGenerator.PARAM_REPORT_FILE_NAME, this.messageHelper.getMessage(ReportingConstants.REPORT_TYPE_NAME_COLLECTION_LOG));
        
        return params;
    }
    
    private Map<String, Object> createDetailedCollectionReportParams() {
        final Map<String, Object> params = new HashMap<String, Object>(5);
        params.put(ReportGenerator.PARAM_REPORT_TITLE, this.messageHelper.getMessage(ReportingConstants.REPORT_TYPE_NAME_COLLECTION_DETAIL));
        params.put(ReportGenerator.PARAM_REPORT_FILE_NAME, this.messageHelper.getMessage(ReportingConstants.REPORT_TYPE_NAME_COLLECTION_DETAIL));
        
        return params;
    }
    
    private String fillCollectionsReport(final PayStationListCollectionReportList collectionReports, final Map<String, Object> reportParams,
        final WebSecurityForm<PayStationCollectionFilterForm> webSecurityForm, final BindingResult result, final HttpServletRequest request,
        final HttpServletResponse response) {
        this.payStationCollectionFilterValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), ERROR_STATUS, true);
        }
        
        final PayStationCollectionFilterForm form = webSecurityForm.getWrappedObject();
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(form.getPointOfSaleId());
        if (pointOfSale == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String timeZone = this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(pointOfSale.getCustomer().getId(),
                                                                                                                 WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE)
                .getPropertyValue();
        
        final List<PosCollection> posCollections = this.posCollectionService
                .findPosCollectionByPointOfSaleIdLast90Days(form.getPointOfSaleId(), form.getOrder().name(), form.getColumn().name(), form.getPage());
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        if (posCollections != null && !posCollections.isEmpty()) {
            
            for (PosCollection posCollection : posCollections) {
                
                final PayStationListCollectionReport collectionReport = new PayStationListCollectionReport();
                
                collectionReport.setRandomId(keyMapping.getRandomString(posCollection, WebCoreConstants.ID_LOOK_UP_NAME));
                collectionReport.setType(posCollection.getCollectionType().getName());
                // The overview Amount column displays 'Revenue' amount value.
                collectionReport.setAmount(formatToDollars(getRevenue(posCollection)));
                
                collectionReport.setDate(DateUtil.getDateFormat().format(DateUtil.changeTimeZone(posCollection.getEndGmt(), timeZone)));
                collectionReports.addCollectionReport(collectionReport);
            }
        }
        
        if (reportParams != null) {
            reportParams.put(ReportGenerator.PARAM_CUSTOMER_TIME_ZONE, timeZone);
        }
        
        return null;
    }
    
    /**
     * This method takes a transaction report random ID as input which is taken
     * from the output from payStationDetailsReprots(). It then returns vm file URL
     * string "transactionReport" with details for that transaction
     */
    public final String payStationDetailsReportsTransactionDetails(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model, final String urlOnSuccess) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strPurchaseId = request.getParameter("transactionId");
        final Long purchaseId = this.commonControllerHelper
                .verifyRandomIdAndReturnActualLong(response, keyMapping,
                                                   strPurchaseId, "+++ Invalid transactionId in PayStationListController"
                                                                  + ".payStationDetailsReportsTransactionDetails() method. +++");
        if (purchaseId == null) {
            return null;
        }
        
        final Purchase purchase = this.purchaseService.findPurchaseById(purchaseId, false);
        if (purchase == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        //        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        //        WebUser webUser = (WebUser) authentication.getPrincipal();
        final PayStationListTransactionReportDetails transactionReport = new PayStationListTransactionReportDetails();
        
        transactionReport.setLocationName(purchase.getLocation().getName());
        transactionReport.setPayStationName(purchase.getPointOfSale().getName());
        transactionReport.setPayStationSerial(purchase.getPointOfSale().getSerialNumber());
        transactionReport.setTransactionNumber(String.valueOf(purchase.getPurchaseNumber()));
        transactionReport.setLotNumber(purchase.getPaystationSetting().getName());
        
        transactionReport.setTransactionType(purchase.getTransactionType().getName());
        transactionReport.setPaymentType(purchase.getPaymentType().getName());
        
        final SimpleDateFormat format = DateUtil.getDateFormat();
        
        final String timeZone = this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(purchase.getCustomer().getId(),
                                                                                                                 WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE)
                .getPropertyValue();
        
        transactionReport.setPurchasedDate(format.format(DateUtil.changeTimeZone(purchase.getPurchaseGmt(), timeZone)));
        
        final Permit permit = this.permitService.findLatestExpiredPermitByPurchaseId(purchase.getId());
        if (permit == null) {
            if (purchase.getTransactionType().getId().intValue() == ReportingConstants.TRANSACTION_TYPE_CANCELLED
                || purchase.getTransactionType().getId().intValue() == ReportingConstants.TRANSACTION_TYPE_SC_RECHARGE) {
                // There is no Permit record for cancellation or SC Recharge, set expiry date to purchase date.
                transactionReport.setExpiryDate(transactionReport.getPurchasedDate());
            }
            
        } else {
            transactionReport.setExpiryDate(format.format(DateUtil.changeTimeZone(permit.getPermitOriginalExpireGmt(), timeZone)));
            
            transactionReport.setStallNumber(permit.getSpaceNumber());
            transactionReport.setAddTimeNumber(permit.getAddTimeNumber());
            if (permit.getLicencePlate() == null) {
                transactionReport.setLicensePlateNumber(WebCoreConstants.NOT_APPLICABLE);
            } else {
                transactionReport.setLicensePlateNumber(permit.getLicencePlate().getNumber());
            }
            if (purchase.getTransactionType().getId().intValue() == ReportingConstants.TRANSACTION_TYPE_MONTHLY) {
                final Date startDate = DateUtil.createUTCStartOfMonthDate(permit.getPermitExpireGmt(), timeZone);
                transactionReport.setMonthlyStartDate(format.format(DateUtil.changeTimeZone(startDate, timeZone)));
            }
        }
        
        transactionReport.setChargedAmount(formatToDollars(purchase.getChargedAmount()));
        
        transactionReport.setCashPaid(formatToDollars(purchase.getCashPaidAmount()));
        
        setCreditCardPaymentType(transactionReport, purchase, purchaseId);
        
        transactionReport.setExcessPayment(formatToDollars(purchase.getExcessPaymentAmount()));
        transactionReport.setChangeDispensed(formatToDollars(purchase.getChangeDispensedAmount()));
        transactionReport.setRefundSlip(purchase.isIsRefundSlip());
        
        int cardRefundAmount = 0;
        if (purchase.getCardPaidAmount() > 0) {
            final Collection<ProcessorTransaction> pts = this.processorTransactionService.findProcessorTransactionByPurchaseId(purchase.getId());
            if (pts == null || pts.isEmpty()) {
                final PaymentCard paymentCard = this.paymentCardService.findByPurchaseId(purchaseId);
                if (paymentCard != null && paymentCard.getCardType().getId() == WebCoreConstants.CARD_TYPE_VALUE_CARD
                    && paymentCard.getCustomerCard().getCustomerCardType().getAuthorizationType().getId() == WebCoreConstants.AUTH_TYPE_INTERNAL_LIST
                    && purchase.getTransactionType().getId().intValue() == ReportingConstants.TRANSACTION_TYPE_CANCELLED) {
                    cardRefundAmount = paymentCard.getAmount();
                    transactionReport.setAmountRefundedCard(formatToDollars(paymentCard.getAmount()));
                    
                }
                final PaymentSmartCard psc = this.paymentSmartCardService.findByPurchaseId(purchase);
                if (psc != null) {
                    transactionReport.setSmartCardData(psc.getSmartCardData());
                }
                
            } else {
                final ProcessorTransaction pt = pts.iterator().next();
                
                if (pt.getProcessorTransactionType().getId() == CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_NO_REFUNDS
                    && this.processorTransactionService.isIncomplete(pt)) {
                    // Show incomplete Link SnF as offline
                    transactionReport.setCardType(null);
                    transactionReport.setCardNumber(WebCoreConstants.N_A_STRING);
                    
                } else {
                    final PaymentCard paymentCard =
                            this.paymentCardService.findPaymentCardByPurchaseIdAndProcessorTransactionId(purchase.getId(), pt.getId());
                    transactionReport.setProcessorTransactionTypeId(pt.getProcessorTransactionType().getId());
                    // If processorTransactionType is UnClosable or UnRecoverable paymentCard could be null
                    if (paymentCard != null) {
                        
                        processPaymentCard(paymentCard, transactionReport, pt);
                        
                    }
                    
                    String processingDate = "";
                    if (pt.getProcessorTransactionType().isIsRefund()) {
                        final Collection<ProcessorTransaction> refundPts =
                                this.processorTransactionService.findRefundedProcessorTransaction(purchase.getId());
                        if (refundPts != null && !refundPts.isEmpty()) {
                            final ProcessorTransaction refundPt = refundPts.iterator().next();
                            String status = refundPt.getProcessorTransactionType().getName();
                            if (refundPt.getMerchantAccount().getProcessor().getId() == CardProcessingConstants.PROCESSOR_ID_ELAVON_VIACONEX) {
                                final ElavonTransactionDetail etd =
                                        this.elavonTransactionDetailService.findByProcessorTransactionId(refundPt.getId());
                                if (etd != null) {
                                    if (etd.getTransactionSettlementStatusType()
                                            .getId() == CardProcessingConstants.TRANSACTION_SETTLEMENT_STATUS_TYPE_POSTAUTH_ID) {
                                        status = etd.getTransactionSettlementStatusType().getName();
                                    } else if (etd.getTransactionSettlementStatusType()
                                            .getId() == CardProcessingConstants.TRANSACTION_SETTLEMENT_STATUS_TYPE_SETTLED_ID
                                               && etd.getSettlementGmt() != null) {
                                        processingDate = format.format(DateUtil.changeTimeZone(etd.getSettlementGmt(), timeZone));
                                    }
                                }
                            } else {
                                processingDate = format.format(DateUtil.changeTimeZone(refundPt.getProcessingDate(), timeZone));
                            }
                            transactionReport.setStatus(status);
                            transactionReport.setProcessingDate(processingDate);
                            transactionReport.setAmountRefundedCard(formatToDollars(refundPt.getAmount()));
                            cardRefundAmount = refundPt.getAmount();
                        }
                    } else {
                        String status = pt.getProcessorTransactionType().getName();
                        if (pt.getMerchantAccount() != null
                            && pt.getMerchantAccount().getProcessor().getId() == CardProcessingConstants.PROCESSOR_ID_ELAVON_VIACONEX) {
                            final ElavonTransactionDetail etd = this.elavonTransactionDetailService.findByProcessorTransactionId(pt.getId());
                            if (etd != null) {
                                if (etd.getTransactionSettlementStatusType()
                                        .getId() == CardProcessingConstants.TRANSACTION_SETTLEMENT_STATUS_TYPE_POSTAUTH_ID) {
                                    status = etd.getTransactionSettlementStatusType().getName();
                                } else if (etd.getTransactionSettlementStatusType()
                                        .getId() == CardProcessingConstants.TRANSACTION_SETTLEMENT_STATUS_TYPE_SETTLED_ID
                                           && etd.getSettlementGmt() != null) {
                                    processingDate = format.format(DateUtil.changeTimeZone(etd.getSettlementGmt(), timeZone));
                                }
                            }
                        } else {
                            processingDate = format.format(DateUtil.changeTimeZone(pt.getProcessingDate(), timeZone));
                        }
                        transactionReport.setStatus(status);
                        
                        transactionReport.setProcessingDate(processingDate);
                    }
                    
                }
            }
        }
        
        // Refund cash and total refund amount.
        final int cancelledTransaction = purchase.getTransactionType().getId().intValue();
        if (purchase.isIsRefundSlip() && cancelledTransaction == ReportingConstants.TRANSACTION_TYPE_CANCELLED && isPaidByCashBillCoin(purchase)) {
            
            final int cardAmountToAdd = purchase.getCardPaidAmount() - cardRefundAmount;
            final int cashAndCardAmount = purchase.getCashPaidAmount() + cardAmountToAdd;
            final int totalRefundAmt =
                    cashAndCardAmount - purchase.getChargedAmount() - purchase.getChangeDispensedAmount() - purchase.getExcessPaymentAmount();
            
            transactionReport.setAmountRefundedSlip(formatToDollars(totalRefundAmt));
        } else {
            if (purchase.isIsRefundSlip()) {
                final int chargedAmountRemaining = purchase.getChargedAmount() - purchase.getCardPaidAmount();
                final int excessAmount = purchase.getCashPaidAmount() - chargedAmountRemaining;
                transactionReport.setAmountRefundedSlip(formatToDollars(excessAmount));
            } else {
                transactionReport.setAmountRefundedSlip(0f);
            }
        }
        if (purchase.getTransactionType().getId().intValue() != ReportingConstants.TRANSACTION_TYPE_CANCELLED) {
            if (transactionReport.getAmountRefundedSlip() == null) {
                transactionReport.setAmountRefundedSlip(Float.valueOf(STATUS_CLEARED));
            }
            if (transactionReport.getAmountRefundedCard() == null) {
                transactionReport.setAmountRefundedCard(Float.valueOf(STATUS_CLEARED));
            }
        }
        transactionReport.setCouponNumber(purchase.getCoupon() == null ? null : purchase.getCoupon().getCoupon());
        
        model.put("transactionReportDetail", transactionReport);
        return urlOnSuccess;
    }
    
    private void processPaymentCard(final PaymentCard paymentCard, final PayStationListTransactionReportDetails transactionReport,
        final ProcessorTransaction pt) {
        
        if (paymentCard.getCardType().getId() == WebCoreConstants.CREDIT_CARD) {
            
            if (pt.getProcessorTransactionType().getId() == CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_SF_WITH_NO_REFUNDS
                && this.processorTransactionService.isIncomplete(pt)) {
                transactionReport.setCardType(null);
                transactionReport.setCardNumber(WebCoreConstants.N_A_STRING);
                
            } else {
                transactionReport.setCardType(pt.getCardType());
                transactionReport.setCardNumber(WebCoreUtil.createStarredCardNumber(String.valueOf(pt.getLast4digitsOfCardNumber()), true));
                transactionReport.setAuthorizationNumber(pt.getSanitizedAuthorizationNumber());
                transactionReport.setReferenceNumber(pt.getReferenceNumber());
            }
        } else if (paymentCard.getCardType().getId() == WebCoreConstants.VALUE_CARD) {
            transactionReport.setCardType(paymentCard.getCustomerCard().getCustomerCardType().getName());
            transactionReport.setCardNumber(WebCoreUtil.createStarredCardNumber(paymentCard.getCustomerCard().getCardNumber(), true));
        } else if (paymentCard.getCardType().getId() == WebCoreConstants.SMART_CARD) {
            transactionReport.setCardType(paymentCard.getCustomerCard().getCustomerCardType().getName());
            transactionReport.setCardNumber(paymentCard.getCustomerCard().getCardNumber());
        } else {
            transactionReport.setCardType(pt.getCardType());
        }
        
    }
    
    private void setCreditCardPaymentType(final PayStationListTransactionReportDetails transactionReport, final Purchase purchase,
        final Long purchaseId) {
        switch (purchase.getPaymentType().getId()) {
            case WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_SWIPE:
            case WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_SWIPE:
            case WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS:
            case WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CL:
            case WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_CHIP:
            case WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_CH:
            case WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_EXTERNAL:
                transactionReport.setCardPaid(formatToDollars(purchase.getCardPaidAmount()));
                transactionReport.setValueCardPaid(formatToDollars(0));
                transactionReport.setSmartCardPaid(formatToDollars(0));
                break;
            case WebCoreConstants.PAYMENT_TYPE_VALUE_CARD:
            case WebCoreConstants.PAYMENT_TYPE_CASH_VALUE:
                final PaymentCard paymentCard = this.paymentCardService.findByPurchaseId(purchaseId);
                transactionReport.setCardType(paymentCard.getCustomerCard().getCustomerCardType().getName());
                transactionReport.setCardNumber(WebCoreUtil.createStarredCardNumber(paymentCard.getCustomerCard().getCardNumber(), true));
                transactionReport.setCardPaid(formatToDollars(0));
                transactionReport.setValueCardPaid(formatToDollars(purchase.getCardPaidAmount()));
                transactionReport.setSmartCardPaid(formatToDollars(0));
                break;
            case WebCoreConstants.PAYMENT_TYPE_SMART_CARD:
            case WebCoreConstants.PAYMENT_TYPE_CASH_SMART:
                transactionReport.setCardPaid(formatToDollars(0));
                transactionReport.setValueCardPaid(formatToDollars(0));
                transactionReport.setSmartCardPaid(formatToDollars(purchase.getCardPaidAmount()));
                break;
            default:
                break;
        }
        
    }
    
    private boolean isPaidByCashBillCoin(final Purchase purchase) {
        if (purchase.getCashPaidAmount() > 0 || purchase.getBillPaidAmount() > 0 || purchase.getCoinPaidAmount() > 0) {
            return true;
        }
        return false;
    }
    
    /**
     * This method takes a collection report random ID as input which is taken
     * from the output from payStationDetailsReprots(). It then returns vm file URL
     * string "collectionReport" with details for that collection
     */
    
    public String payStationDetailsReportsCollectionsDetails(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model, final String urlOnSuccess) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strCollectionId = request.getParameter(COLLECTION_ID);
        final Integer collectionId = this.commonControllerHelper
                .verifyRandomIdAndReturnActual(response, keyMapping, strCollectionId, INVALID_COLLECTION_ID_IN_PAY_STATION_LIST_CONTROLLER
                                                                                      + PAY_STATION_DETAILS_REPORTS_COLLECTIONS_DETAILS_METHOD);
        if (collectionId == null) {
            return null;
        }
        
        final PosCollection collection = this.posCollectionService.findPosCollectionByPosCollectionId(collectionId);
        if (collection == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        model.put("collectionReportDetail", prepareCollectionReport(collection));
        return urlOnSuccess;
    }
    
    public final String exportCollectionReport(final HttpServletRequest request, final HttpServletResponse response) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strCollectionId = request.getParameter(COLLECTION_ID);
        final Integer collectionId = this.commonControllerHelper
                .verifyRandomIdAndReturnActual(response, keyMapping, strCollectionId, INVALID_COLLECTION_ID_IN_PAY_STATION_LIST_CONTROLLER
                                                                                      + PAY_STATION_DETAILS_REPORTS_COLLECTIONS_DETAILS_METHOD);
        if (collectionId == null) {
            return null;
        }
        
        final PosCollection collection = this.posCollectionService.findPosCollectionByPosCollectionId(collectionId);
        if (collection == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        String respStr;
        final Map<String, Object> reportParams = createDetailedCollectionReportParams();
        
        try {
            this.reportGenerator.generatePDF(response, ReportingConstants.REPORT_JASPER_FILE_COLLECTION_REPORT_DETAIL, reportParams,
                                             prepareCollectionReport(collection));
            respStr = WebCoreConstants.RESPONSE_TRUE;
        } catch (IOException ioe) {
            LOGGER.error("Failed to export collection detail report", ioe);
            
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            respStr = WebCoreConstants.RESPONSE_FALSE;
        }
        
        return respStr;
    }
    
    private PayStationListCollectionReportDetails prepareCollectionReport(final PosCollection collection) {
        final PayStationListCollectionReportDetails collectionReport = new PayStationListCollectionReportDetails();
        
        final String timeZone = this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(collection.getCustomer().getId(),
                                                                                                                 WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE)
                .getPropertyValue();
        
        final PooledResource<DateFormat> format = DateUtil.takeDateFormat(WebCoreConstants.DATETIME_FORMAT, WebCoreConstants.GMT);
        
        collectionReport.setStartDateLocal(format.get().format(DateUtil.changeTimeZone(collection.getStartGmt(), timeZone)));
        collectionReport.setEndDateLocal(format.get().format(DateUtil.changeTimeZone(collection.getEndGmt(), timeZone)));
        collectionReport.setPayStationSettingName(collection.getPaystationSetting().getName());
        if (collection.getNextTicketNumber() > 0) {
            collectionReport.setEndTicketNumber(collection.getNextTicketNumber() - 1);
        } else {
            collectionReport.setEndTicketNumber(0);
        }
        collectionReport.setTicketSold(collection.getTicketsSold());
        collectionReport.setTube1type(collection.getTube1type());
        collectionReport.setTube1amount(formatToDollars(collection.getTube1amount()));
        collectionReport.setTube2type(collection.getTube2type());
        collectionReport.setTube2amount(formatToDollars(collection.getTube2amount()));
        collectionReport.setTube3type(collection.getTube3type());
        collectionReport.setTube3amount(formatToDollars(collection.getTube3amount()));
        collectionReport.setTube4type(collection.getTube4type());
        collectionReport.setTube4amount(formatToDollars(collection.getTube4amount()));
        final int tubeTotal = collection.getTube1amount() + collection.getTube2amount() + collection.getTube3amount() + collection.getTube4amount();
        collectionReport.setTubeTotalAmount(formatToDollars(tubeTotal));
        collectionReport.setReplenishedAmount(formatToDollars(collection.getReplenishedAmount()));
        collectionReport.setOverfillAmount(formatToDollars(collection.getOverfillAmount()));
        collectionReport.setAcceptedFloatAmount(formatToDollars(collection.getAcceptedFloatAmount()));
        final int changerDispensed = collection.getChangeIssuedAmount() - (collection.getHopper1dispensed() + collection.getHopper2dispensed());
        collectionReport.setChangerDispensed(formatToDollars(changerDispensed));
        collectionReport.setTestDispensedChanger(formatToDollars(collection.getTestDispensedChanger()));
        collectionReport.setHopper1type(formatToDollars(collection.getHopper1type()));
        collectionReport.setHopper1current(formatToDollars(collection.getHopper1current()));
        collectionReport.setHopper2type(formatToDollars(collection.getHopper2type()));
        collectionReport.setHopper2current(formatToDollars(collection.getHopper2current()));
        final int hopperTotal = collection.getHopper1current() + collection.getHopper2current();
        collectionReport.setHopperTotalAmount(formatToDollars(hopperTotal));
        collectionReport.setHopper1replenished(formatToDollars(collection.getHopper1replenished()));
        collectionReport.setHopper2replenished(formatToDollars(collection.getHopper2replenished()));
        collectionReport.setHopper1dispensed(formatToDollars(collection.getHopper1dispensed()));
        collectionReport.setHopper2dispensed(formatToDollars(collection.getHopper2dispensed()));
        collectionReport.setTestDispensedHopper1(formatToDollars(collection.getTestDispensedHopper1()));
        collectionReport.setTestDispensedHopper2(formatToDollars(collection.getTestDispensedHopper2()));
        final int cardAmountTotal = collection.getAmexAmount() + collection.getDinersAmount() + collection.getDiscoverAmount()
                                    + collection.getMasterCardAmount() + collection.getVisaAmount() + collection.getJCBAmount();
        collectionReport.setCardAmountTotal(formatToDollars(cardAmountTotal));
        collectionReport.setExcessPaymentAmount(formatToDollars(collection.getExcessPaymentAmount()));
        final int totalCollections = getTotalCollections(collection);
        
        collectionReport.setTotalCollections(formatToDollars(totalCollections));
        collectionReport.setChangeIssuedAmount(formatToDollars(collection.getChangeIssuedAmount()));
        collectionReport.setRefundIssuedAmount(formatToDollars(collection.getRefundIssuedAmount()));
        final int revenue = getRevenue(collection);
        
        collectionReport.setRevenue(formatToDollars(revenue));
        
        collectionReport.setBillAmount1(formatToDollars(collection.getBillAmount1()));
        collectionReport.setBillAmount2(formatToDollars(collection.getBillAmount2()));
        collectionReport.setBillAmount5(formatToDollars(collection.getBillAmount5()));
        collectionReport.setBillAmount10(formatToDollars(collection.getBillAmount10()));
        collectionReport.setBillAmount20(formatToDollars(collection.getBillAmount20()));
        collectionReport.setBillAmount50(formatToDollars(collection.getBillAmount50()));
        collectionReport.setBillTotalAmount(formatToDollars(collection.getBillTotalAmount()));
        
        collectionReport.setCoinAmount5(formatToDollars(collection.getCoinCount05() * StandardConstants.COIN_5));
        collectionReport.setCoinAmount10(formatToDollars(collection.getCoinCount10() * StandardConstants.COIN_10));
        collectionReport.setCoinAmount25(formatToDollars(collection.getCoinCount25() * StandardConstants.COIN_25));
        collectionReport.setCoinAmount100(formatToDollars(collection.getCoinCount100() * StandardConstants.COIN_100));
        collectionReport.setCoinAmount200(formatToDollars(collection.getCoinCount200() * StandardConstants.COIN_200));
        collectionReport.setCoinTotalAmount(formatToDollars(collection.getCoinTotalAmount()));
        
        collectionReport.setAmexAmount(formatToDollars(collection.getAmexAmount()));
        collectionReport.setDinersAmount(formatToDollars(collection.getDinersAmount()));
        collectionReport.setDiscoverAmount(formatToDollars(collection.getDiscoverAmount()));
        collectionReport.setMasterCardAmount(formatToDollars(collection.getMasterCardAmount()));
        collectionReport.setVisaAmount(formatToDollars(collection.getVisaAmount()));
        collectionReport.setJcbAmount(formatToDollars(collection.getJCBAmount()));
        collectionReport.setCardTotalAmount(formatToDollars(collection.getCardTotalAmount()));
        
        collectionReport.setPermitCount(collection.getTicketsSold());
        
        collectionReport.setCollectionDate(format.get().format(DateUtil.changeTimeZone(collection.getEndGmt(), timeZone)));
        
        // Start Date & End Date
        final Date dateTimeGMT = this.posCollectionService.findLastCollectionDate(collection);
        if (dateTimeGMT == null) {
            collectionReport.setPreviousCollection(WebCoreConstants.EMPTY_STRING);
        } else {
            collectionReport.setPreviousCollection(format.get().format(DateUtil.changeTimeZone(dateTimeGMT, timeZone)));
        }
        
        collectionReport.setSmartCardAmount(formatToDollars(collection.getSmartCardAmount()));
        collectionReport.setSmartCardRechargeAmount(formatToDollars(collection.getSmartCardRechargeAmount()));
        collectionReport.setValueCardAmount(formatToDollars(collection.getValueCardAmount()));
        collectionReport.setLocationName(collection.getPointOfSale().getLocation().getName());
        collectionReport.setPayStationName(collection.getPointOfSale().getName());
        collectionReport.setPayStationSerial(collection.getPointOfSale().getSerialNumber());
        collectionReport.setTypeId(collection.getCollectionType().getId());
        collectionReport.setType(collection.getCollectionType().getName());
        collectionReport.setReportNumber(collection.getReportNumber());
        
        final Set<PosCollectionUser> collectionUsers = collection.getPosCollectionUsers();
        if (collectionUsers != null && !collectionUsers.isEmpty()) {
            String userName = null;
            for (PosCollectionUser collectionUser : collectionUsers) {
                if (collectionUser.getUserAccount() != null) {
                    try {
                        userName = URLDecoder.decode(collectionUser.getUserAccount().getFullName(), WebSecurityConstants.URL_ENCODING_LATIN1);
                    } catch (UnsupportedEncodingException uee) {
                        userName = collectionUser.getUserAccount().getFullName();
                    }
                    break;
                }
            }
            collectionReport.setCollectionUserName(userName);
        }
        
        format.close();
        
        return collectionReport;
    }
    
    public final String payStationDetailsSensorHistorySuper(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strPointOfSaleId = request.getParameter(PAYSTATION_ID);
        final Integer pointOfSaleId =
                this.commonControllerHelper.verifyRandomIdAndReturnActual(response, keyMapping, strPointOfSaleId,
                                                                          INVALID_PAYSTATIONID_IN_LISTCONTROLLER + PAYSTATION_DETAILS_SENSOR_HISTORY);
        if (pointOfSaleId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        if (pointOfSale == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String strSensorTypeID = request.getParameter("sensorTypeID");
        if (strSensorTypeID == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        Integer sensorTypeId = null;
        try {
            sensorTypeId = Integer.parseInt(strSensorTypeID);
        } catch (NumberFormatException nfe) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        final SensorInfoType sensorInfoType = this.sensorInfoTypeService.findSensorInfoType(sensorTypeId);
        
        final String timeZone = this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(pointOfSale.getCustomer().getId(),
                                                                                                                 WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE)
                .getPropertyValue();
        
        final TimeZone custTimeZone = TimeZone.getTimeZone(timeZone);
        
        final byte[] fileContent = buildSensorHistory(pointOfSaleId, sensorTypeId, sensorInfoType, timeZone);
        
        final String exportFileName = pointOfSale.getName().replace(StandardConstants.STRING_EMPTY_SPACE, StandardConstants.STRING_UNDERSCORE)
                                      + StandardConstants.STRING_DASH
                                      + sensorInfoType.getName().replace(StandardConstants.STRING_EMPTY_SPACE, StandardConstants.STRING_UNDERSCORE)
                                      + StandardConstants.STRING_DASH
                                      + DateUtil.createDateString(DateUtil.EXPORT_DATA_DATE_TIME, custTimeZone, new Date())
                                      + StandardConstants.STRING_DOT + ReportingConstants.REPORTS_FORMAT_CSV;
        
        ServletOutputStream outStream = null;
        try {
            response.reset();
            response.setHeader(CONTENT_DISPOSITION, ATTACHMENT_FILENAME + exportFileName + BACKSLASH_QUOTE);
            response.setContentType(TEXT_CSV);
            outStream = response.getOutputStream();
            
            outStream.write(fileContent);
            outStream.flush();
        } catch (IOException e) {
            try {
                if (outStream != null) {
                    outStream.close();
                    response.setStatus(HttpStatus.BAD_REQUEST.value());
                    return WebCoreConstants.RESPONSE_FALSE;
                }
            } catch (IOException ioe) {
                LOGGER.error("outputStream not closed in PayStationDetailController.payStationDetailsSensorHistory");
            }
        }
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    private Float formatToDollars(final int i) {
        return new BigDecimal(i).divide(new BigDecimal(StandardConstants.CONSTANT_100), 2, BigDecimal.ROUND_HALF_UP).floatValue();
    }
    
    protected final void insertNewPosDate(final PointOfSale pointOfSale, final int posDateTypeId, final UserAccount userAccount,
        final boolean setting) {
        final PosDate posDate = new PosDate();
        posDate.setPointOfSale(pointOfSale);
        posDate.setPosDateType(this.pointOfSaleService.findPosDateTypeById(posDateTypeId));
        posDate.setCurrentSetting(setting);
        posDate.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        posDate.setLastModifiedByUserId(userAccount);
        posDate.setChangedGmt(DateUtil.getCurrentGmtDate());
        this.pointOfSaleService.saveOrUpdatePOSDate(posDate);
    }
    
    private List<String> listInstalledModules(final PosServiceState serviceState) {
        
        //TODO confirm this list contains all modules
        
        final List<String> installedModules = new ArrayList<String>();
        final List<Integer> moduleIds = new ArrayList<Integer>();
        
        setModuleIds(serviceState, moduleIds);
        
        for (Integer moduleId : moduleIds) {
            installedModules.add(this.messageHelper.getMessage(this.alertsService.findEventDeviceTypeById(moduleId).getName()));
        }
        return installedModules;
    }
    
    private List<Integer> setModuleIds(final PosServiceState serviceState, final List<Integer> moduleIds) {
        
        if (serviceState.isIsBillAcceptor()) {
            moduleIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_BILL_ACCEPTOR);
        }
        if (serviceState.isIsBillStacker()) {
            moduleIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_BILL_STACKER);
        }
        if (serviceState.isIsCardReader()) {
            moduleIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_CARD_READER);
        }
        if (serviceState.isIsCoinAcceptor()) {
            moduleIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_ACCEPTOR);
        }
        if (serviceState.isIsCoinCanister()) {
            moduleIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_CANISTER);
        }
        if (serviceState.getIsCoinChanger() == 1) {
            moduleIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_CHANGER);
        }
        if (serviceState.isIsCoinEscrow()) {
            moduleIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_ESCROW);
        }
        if (serviceState.isIsCoinHopper1()) {
            moduleIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_HOPPER1);
        }
        if (serviceState.isIsCoinHopper2()) {
            moduleIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_HOPPER2);
        }
        if (serviceState.isIsPrinter()) {
            moduleIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_PRINTER);
        }
        if (serviceState.isIsRfidCardReader()) {
            moduleIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_RFID_CARD_READER);
        }
        
        return moduleIds;
        
    }
    
    @SuppressWarnings("PMD.InsufficientStringBufferDeclaration")
    private byte[] buildSensorHistory(final int pointOfSaleId, final int sensorTypeId, final SensorInfoType sensorInfoType, final String timeZone) {
        
        int maxDays = 0;
        try {
            maxDays = this.emsPropertiesService
                    .getPropertyValueAsInt(EmsPropertiesService.ARCHIVE_MAX_DAYS_FOR_POS_SENSOR_INFO_AND_POS_BATTERY_INFO,
                                           EmsPropertiesService.DEFAULT_ARCHIVE_MAX_DAYS_FOR_POS_SENSOR_INFO_AND_POS_BATTERY_INFO);
        } catch (NumberFormatException e) {
            maxDays = EmsPropertiesService.DEFAULT_ARCHIVE_MAX_DAYS_FOR_POS_SENSOR_INFO_AND_POS_BATTERY_INFO;
        }
        
        final Date currentDate = new Date();
        final Date fromDate = DateUtils.addDays(currentDate, -maxDays);
        
        final List<SensorHistoryCsvDTO> sensorHistoryList = new ArrayList<SensorHistoryCsvDTO>();
        
        if (sensorTypeId == WebCoreConstants.SENSOR_INFO_BATTERY_VOLTAGE || sensorTypeId == WebCoreConstants.SENSOR_INFO_BATTERY_2_VOLTAGE) {
            
            final List<PosBatteryInfo> posBatteryInfoList =
                    this.posBatteryInfoService.findAllPosBatteryInfoByPointOfSaleIdsAndFromDate(pointOfSaleId, fromDate);
            
            for (PosBatteryInfo batteryInfo : posBatteryInfoList) {
                sensorHistoryList.add(new SensorHistoryCsvDTO(sensorInfoType.getName(),
                        DateUtil.createDateString(timeZone, batteryInfo.getSensorGmt()),
                        WebCoreConstants.SENSOR_BATTERY_FORMATTER.format(batteryInfo.getBatteryVoltage()), WebCoreConstants.SENSOR_UNIT_VOLTAGE,
                        SensorUtil.getBatteryChargingStateDisplayed(batteryInfo.getInputCurrent(), batteryInfo.getSystemLoad())));
            }
        } else if (sensorTypeId == WebCoreConstants.SENSOR_INFO_AMBIENT_TEMPERATURE
                   || sensorTypeId == WebCoreConstants.SENSOR_INFO_CONTROLLER_TEMPERATURE) {
            
            final boolean isMetric = timeZone == null || !timeZone.startsWith(US);
            final String unitType = isMetric ? WebCoreConstants.SENSOR_UNIT_CELSIUS : WebCoreConstants.SENSOR_UNIT_FAHRENHEIT;
            
            final List<PosSensorInfo> sensorInfoList =
                    this.posSensorInfoService.findPosSensorInfoByPointOfSaleIdAndTypeIdAndStartDate(pointOfSaleId, sensorTypeId, fromDate);
            
            for (PosSensorInfo sensorInfo : sensorInfoList) {
                sensorHistoryList
                        .add(new SensorHistoryCsvDTO(sensorInfoType.getName(), DateUtil.createDateString(timeZone, sensorInfo.getSensorGmt()),
                                WebCoreConstants.SENSOR_TEMPERATURE_FORMATTER
                                        .format(isMetric ? sensorInfo.getAmount() : WebCoreUtil.convertCelsiusToFahrenheit(sensorInfo.getAmount())),
                                unitType));
            }
        } else if (sensorTypeId == WebCoreConstants.SENSOR_INFO_INPUT_CURRENT || sensorTypeId == WebCoreConstants.SENSOR_INFO_SYSTEM_LOAD) {
            final List<PosSensorInfo> sensorInfoList =
                    this.posSensorInfoService.findPosSensorInfoByPointOfSaleIdAndTypeIdAndStartDate(pointOfSaleId, sensorTypeId, fromDate);
            
            for (PosSensorInfo sensorInfo : sensorInfoList) {
                sensorHistoryList
                        .add(new SensorHistoryCsvDTO(sensorInfoType.getName(), DateUtil.createDateString(timeZone, sensorInfo.getSensorGmt()),
                                Float.toString(sensorInfo.getAmount()), WebCoreConstants.SENSOR_UNIT_MILLIAMPS));
            }
        } else if (sensorTypeId == WebCoreConstants.SENSOR_INFO_RELATIVE_HUMIDITY) {
            final List<PosSensorInfo> sensorInfoList =
                    this.posSensorInfoService.findPosSensorInfoByPointOfSaleIdAndTypeIdAndStartDate(pointOfSaleId, sensorTypeId, fromDate);
            
            for (PosSensorInfo sensorInfo : sensorInfoList) {
                sensorHistoryList
                        .add(new SensorHistoryCsvDTO(sensorInfoType.getName(), DateUtil.createDateString(timeZone, sensorInfo.getSensorGmt()),
                                Float.toString(sensorInfo.getAmount()), WebCoreConstants.SENSOR_UNIT_HUMIDITY));
            }
        }
        
        final StringBuilder csvFileContent = new StringBuilder();
        //Header
        csvFileContent.append("Sensor,Timestamp,Value,Units");
        if (sensorTypeId == WebCoreConstants.SENSOR_INFO_BATTERY_VOLTAGE || sensorTypeId == WebCoreConstants.SENSOR_INFO_BATTERY_2_VOLTAGE) {
            csvFileContent.append(",Power Status");
            csvFileContent.append(StandardConstants.STRING_END_OF_LINE);
            for (SensorHistoryCsvDTO sensorHistory : sensorHistoryList) {
                csvFileContent.append(sensorHistory.getName());
                csvFileContent.append(ReportingConstants.CSV_COMMA);
                csvFileContent.append(sensorHistory.getDate());
                csvFileContent.append(ReportingConstants.CSV_COMMA);
                csvFileContent.append(sensorHistory.getValue());
                csvFileContent.append(ReportingConstants.CSV_COMMA);
                csvFileContent.append(sensorHistory.getUnit());
                csvFileContent.append(ReportingConstants.CSV_COMMA);
                csvFileContent.append(sensorHistory.getChargeState());
                csvFileContent.append(StandardConstants.STRING_END_OF_LINE);
            }
        } else {
            csvFileContent.append(StandardConstants.STRING_END_OF_LINE);
            for (SensorHistoryCsvDTO sensorHistory : sensorHistoryList) {
                csvFileContent.append(sensorHistory.getName());
                csvFileContent.append(ReportingConstants.CSV_COMMA);
                csvFileContent.append(sensorHistory.getDate());
                csvFileContent.append(ReportingConstants.CSV_COMMA);
                csvFileContent.append(sensorHistory.getValue());
                csvFileContent.append(ReportingConstants.CSV_COMMA);
                csvFileContent.append(sensorHistory.getUnit());
                csvFileContent.append(StandardConstants.STRING_END_OF_LINE);
            }
        }
        
        return csvFileContent.toString().getBytes();
    }
    
    private int getTotalCollections(final PosCollection collection) {
        final int totalCollections = collection.getAcceptedFloatAmount() + collection.getOverfillAmount() + collection.getCoinTotalAmount()
                                     + collection.getBillTotalAmount() + collection.getCardTotalAmount();
        return totalCollections;
    }
    
    private int getRevenue(final PosCollection collection) {
        final int revenue = getTotalCollections(collection) - collection.getSmartCardRechargeAmount() + collection.getSmartCardAmount()
                            + collection.getAttendantTicketsAmount() - collection.getAttendantDepositAmount() - collection.getChangeIssuedAmount()
                            - collection.getRefundIssuedAmount();
        return revenue;
    }
    
    public final String payStationDetailsBatteryVoltageTrendSuper(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strPointOfSaleId = request.getParameter(PAYSTATION_ID);
        final Integer pointOfSaleId =
                this.commonControllerHelper.verifyRandomIdAndReturnActual(response, keyMapping, strPointOfSaleId,
                                                                          INVALID_PAYSTATIONID_IN_LISTCONTROLLER + PAYSTATION_DETAILS_SENSOR_HISTORY);
        if (pointOfSaleId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        if (pointOfSale == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Date currentDate = new Date();
        final Date fromDate = DateUtils.addDays(currentDate, -WebCoreConstants.PAY_STATION_DETAIL_LIST_MAX_TREND_DAYS);
        final TrendData data = new TrendData();
        
        final List<PosBatteryInfo> posBatteryInfoList =
                this.posBatteryInfoService.findAllPosBatteryInfoByPointOfSaleIdsAndFromDateNoZeroes(pointOfSale.getId(), fromDate);
        
        data.setUnitType(WebCoreConstants.SENSOR_UNIT_VOLTAGE);
        data.getTargetValueList().add(WebCoreConstants.SENSOR_TREND_TARGET_VOLTAGE);
        
        for (PosBatteryInfo batteryInfo : posBatteryInfoList) {
            if (batteryInfo.getBatteryVoltage() > 0) {
                data.getDateTimeStampList().add(batteryInfo.getSensorGmt().getTime());
                data.getDataPointList().add(Float.toString(batteryInfo.getBatteryVoltage()));
            }
        }
        
        return WidgetMetricsHelper.convertToJson(data, TREND_DATA, true);
    }
    
    public final String payStationDetailsInputCurrentTrendSuper(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strPointOfSaleId = request.getParameter(PAYSTATION_ID);
        final Integer pointOfSaleId =
                this.commonControllerHelper.verifyRandomIdAndReturnActual(response, keyMapping, strPointOfSaleId,
                                                                          INVALID_PAYSTATIONID_IN_LISTCONTROLLER + PAYSTATION_DETAILS_SENSOR_HISTORY);
        if (pointOfSaleId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        if (pointOfSale == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Date currentDate = new Date();
        final Date fromDate = DateUtils.addDays(currentDate, -WebCoreConstants.PAY_STATION_DETAIL_LIST_MAX_TREND_DAYS);
        final TrendData data = new TrendData();
        
        final List<PosSensorInfo> sensorInfoList = this.posSensorInfoService
                .findPosSensorInfoByPointOfSaleIdAndTypeIdAndStartDateOrderBySensorGMTDesc(pointOfSaleId, WebCoreConstants.SENSOR_INFO_INPUT_CURRENT,
                                                                                           fromDate);
        
        data.setUnitType(WebCoreConstants.SENSOR_UNIT_MILLIAMPS);
        
        for (PosSensorInfo sensorInfo : sensorInfoList) {
            data.getDateTimeStampList().add(sensorInfo.getSensorGmt().getTime());
            data.getDataPointList().add(Float.toString(sensorInfo.getAmount()));
        }
        
        return WidgetMetricsHelper.convertToJson(data, TREND_DATA, true);
    }
    
    public final String payStationDetailsAmbientTemperatureTrendSuper(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strPointOfSaleId = request.getParameter(PAYSTATION_ID);
        final Integer pointOfSaleId =
                this.commonControllerHelper.verifyRandomIdAndReturnActual(response, keyMapping, strPointOfSaleId,
                                                                          INVALID_PAYSTATIONID_IN_LISTCONTROLLER + PAYSTATION_DETAILS_SENSOR_HISTORY);
        if (pointOfSaleId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        if (pointOfSale == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String timeZone = this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(pointOfSale.getCustomer().getId(),
                                                                                                                 WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE)
                .getPropertyValue();
        
        final Date fromDate = DateUtils.addDays(new Date(), -WebCoreConstants.PAY_STATION_DETAIL_LIST_MAX_TREND_DAYS);
        final TrendData data = new TrendData();
        
        final boolean isMetric = timeZone == null || !timeZone.startsWith(US);
        final List<PosSensorInfo> sensorInfoList =
                this.posSensorInfoService.findPosSensorInfoByPointOfSaleIdAndTypeIdAndStartDateOrderBySensorGMTDesc(pointOfSaleId,
                                                                                                                    WebCoreConstants.SENSOR_INFO_AMBIENT_TEMPERATURE,
                                                                                                                    fromDate);
        
        for (PosSensorInfo sensorInfo : sensorInfoList) {
            data.getDateTimeStampList().add(sensorInfo.getSensorGmt().getTime());
            data.getDataPointList()
                    .add(Float.toString(isMetric ? sensorInfo.getAmount() : WebCoreUtil.convertCelsiusToFahrenheit(sensorInfo.getAmount())));
        }
        
        if (isMetric) {
            data.getTargetValueList().add(WebCoreConstants.SENSOR_TREND_TARGET_TEMPERATURE_HIGH_C);
            data.getTargetValueList().add(WebCoreConstants.SENSOR_TREND_TARGET_TEMPERATURE_LOW_C);
            data.setUnitType(WebCoreConstants.SENSOR_UNIT_CELSIUS);
        } else {
            data.getTargetValueList().add(WebCoreConstants.SENSOR_TREND_TARGET_TEMPERATURE_HIGH_F);
            data.getTargetValueList().add(WebCoreConstants.SENSOR_TREND_TARGET_TEMPERATURE_LOW_F);
            data.setUnitType(WebCoreConstants.SENSOR_UNIT_FAHRENHEIT);
        }
        
        return WidgetMetricsHelper.convertToJson(data, TREND_DATA, true);
    }
    
    public final String payStationDetailsRelativeHumidityTrendSuper(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strPointOfSaleId = request.getParameter(PAYSTATION_ID);
        final Integer pointOfSaleId =
                this.commonControllerHelper.verifyRandomIdAndReturnActual(response, keyMapping, strPointOfSaleId,
                                                                          INVALID_PAYSTATIONID_IN_LISTCONTROLLER + PAYSTATION_DETAILS_SENSOR_HISTORY);
        if (pointOfSaleId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        if (pointOfSale == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Date fromDate = DateUtils.addDays(new Date(), -WebCoreConstants.PAY_STATION_DETAIL_LIST_MAX_TREND_DAYS);
        final TrendData data = new TrendData();
        
        final List<PosSensorInfo> sensorInfoList = this.posSensorInfoService
                .findPosSensorInfoByPointOfSaleIdAndTypeIdAndStartDateOrderBySensorGMTDesc(pointOfSaleId,
                                                                                           WebCoreConstants.SENSOR_INFO_RELATIVE_HUMIDITY, fromDate);
        
        data.getTargetValueList().add(WebCoreConstants.SENSOR_TREND_TARGET_HUMIDITY);
        data.setUnitType(WebCoreConstants.SENSOR_UNIT_HUMIDITY);
        
        for (PosSensorInfo sensorInfo : sensorInfoList) {
            if (sensorInfo.getAmount() > 0 && sensorInfo.getAmount() <= StandardConstants.CONSTANT_100) {
                data.getDateTimeStampList().add(sensorInfo.getSensorGmt().getTime());
                data.getDataPointList().add(Float.toString(sensorInfo.getAmount()));
            }
        }
        
        return WidgetMetricsHelper.convertToJson(data, TREND_DATA, true);
    }
    
    public final String payStationDetailsControllerTemperatureTrendSuper(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strPointOfSaleId = request.getParameter(PAYSTATION_ID);
        final Integer pointOfSaleId =
                this.commonControllerHelper.verifyRandomIdAndReturnActual(response, keyMapping, strPointOfSaleId,
                                                                          INVALID_PAYSTATIONID_IN_LISTCONTROLLER + PAYSTATION_DETAILS_SENSOR_HISTORY);
        if (pointOfSaleId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        if (pointOfSale == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String timeZone = this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(pointOfSale.getCustomer().getId(),
                                                                                                                 WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE)
                .getPropertyValue();
        
        final Date fromDate = DateUtils.addDays(new Date(), -WebCoreConstants.PAY_STATION_DETAIL_LIST_MAX_TREND_DAYS);
        final TrendData data = new TrendData();
        
        final boolean isMetric = timeZone == null || !timeZone.startsWith(US);
        final List<PosSensorInfo> sensorInfoList =
                this.posSensorInfoService.findPosSensorInfoByPointOfSaleIdAndTypeIdAndStartDateOrderBySensorGMTDesc(pointOfSaleId,
                                                                                                                    WebCoreConstants.SENSOR_INFO_CONTROLLER_TEMPERATURE,
                                                                                                                    fromDate);
        data.setUnitType(isMetric ? WebCoreConstants.SENSOR_UNIT_CELSIUS : WebCoreConstants.SENSOR_UNIT_FAHRENHEIT);
        
        for (PosSensorInfo sensorInfo : sensorInfoList) {
            data.getDateTimeStampList().add(sensorInfo.getSensorGmt().getTime());
            data.getDataPointList()
                    .add(Float.toString(isMetric ? sensorInfo.getAmount() : WebCoreUtil.convertCelsiusToFahrenheit(sensorInfo.getAmount())));
        }
        data.getTargetValueList()
                .add(isMetric ? WebCoreConstants.SENSOR_TREND_TARGET_TEMPERATURE_HIGH_C : WebCoreConstants.SENSOR_TREND_TARGET_TEMPERATURE_HIGH_F);
        data.getTargetValueList()
                .add(isMetric ? WebCoreConstants.SENSOR_TREND_TARGET_TEMPERATURE_LOW_C : WebCoreConstants.SENSOR_TREND_TARGET_TEMPERATURE_LOW_F);
        data.setUnitType(isMetric ? WebCoreConstants.SENSOR_UNIT_CELSIUS : WebCoreConstants.SENSOR_UNIT_FAHRENHEIT);
        
        return WidgetMetricsHelper.convertToJson(data, TREND_DATA, true);
    }
    
    public final String payStationDetailsSystemLoadTrendSuper(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strPointOfSaleId = request.getParameter(PAYSTATION_ID);
        final Integer pointOfSaleId =
                this.commonControllerHelper.verifyRandomIdAndReturnActual(response, keyMapping, strPointOfSaleId,
                                                                          INVALID_PAYSTATIONID_IN_LISTCONTROLLER + PAYSTATION_DETAILS_SENSOR_HISTORY);
        if (pointOfSaleId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        if (pointOfSale == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Date fromDate = DateUtils.addDays(new Date(), -WebCoreConstants.PAY_STATION_DETAIL_LIST_MAX_TREND_DAYS);
        final TrendData data = new TrendData();
        
        final List<PosSensorInfo> sensorInfoList = this.posSensorInfoService
                .findPosSensorInfoByPointOfSaleIdAndTypeIdAndStartDateOrderBySensorGMTDesc(pointOfSaleId, WebCoreConstants.SENSOR_INFO_SYSTEM_LOAD,
                                                                                           fromDate);
        
        data.setUnitType(WebCoreConstants.SENSOR_UNIT_MILLIAMPS);
        
        for (PosSensorInfo sensorInfo : sensorInfoList) {
            data.getDateTimeStampList().add(sensorInfo.getSensorGmt().getTime());
            data.getDataPointList().add(Float.toString(sensorInfo.getAmount()));
        }
        
        return WidgetMetricsHelper.convertToJson(data, TREND_DATA, true);
    }
    
    /**
     * This method takes request parameter "activePosAlertId" and clears the alert
     * 
     * @param request
     * @param response
     * @return TRUE if successful
     * 
     */
    
    public String clearActiveAlert(final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!WebSecurityUtil.isCustomerMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + ":" + this.alertCentreFilterValidator.getMessage("error.customer.notMigrated");
        }
        
        return this.commonControllerHelper.clearActiveAlert(request, response, false);
    }
    
    protected final String viewHistoryNotesSuper(final WebSecurityForm<PayStationCommentFilterForm> webSecurityForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response) {
        
        this.payStationCommentFilterValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), ERROR_STATUS, true);
        }
        
        final PayStationCommentFilterForm form = webSecurityForm.getWrappedObject();
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(form.getPointOfSaleId());
        if (pointOfSale == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String timeZone = this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(pointOfSale.getCustomer().getId(),
                                                                                                                 WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE)
                .getPropertyValue();
        
        final List<PosDate> posDateList = this.posDateService.findPosDateByPointOfSale(form.getPointOfSaleId(), form.getOrder().name(),
                                                                                       form.getColumn().name(), form.getPage());
        final List<PayStationListHistoryNote> payStationListHistoryNoteList = new ArrayList<PayStationListHistoryNote>();
        
        for (PosDate posDate : posDateList) {
            final PayStationListHistoryNote payStationListHistoryNote = new PayStationListHistoryNote();
            try {
                payStationListHistoryNote
                        .setUser(URLDecoder.decode(posDate.getLastModifiedByUserId().getRealUserName(), WebSecurityConstants.URL_ENCODING_LATIN1));
                
            } catch (UnsupportedEncodingException e) {
                LOGGER.error("+++ Couldnot decode username for paystation notes +++", e);
            }
            payStationListHistoryNote.setComment(posDate.getComment());
            payStationListHistoryNote.setDate(DateUtil.getDateFormat().format(DateUtil.changeTimeZone(posDate.getChangedGmt(), timeZone)));
            payStationListHistoryNoteList.add(payStationListHistoryNote);
        }
        
        final PaginatedList<PayStationListHistoryNote> noteHistoryList = new PaginatedList<PayStationListHistoryNote>();
        noteHistoryList.setElements(payStationListHistoryNoteList);
        
        return WidgetMetricsHelper.convertToJson(noteHistoryList, "noteHistoryList", true);
    }
}
