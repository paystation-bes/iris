package com.digitalpaytech.mvc;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.LocationLineGeopoint;
import com.digitalpaytech.service.LocationLineGeopointService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.util.DashboardUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.json.JsonSchemaValidator;


/**
 * This class handles the requests from map
 * 
 * @author mikev
 *
 */
@Controller

public class LocationLineGeopointController {
    private static Logger log = Logger.getLogger(LocationLineGeopointController.class);
    
    @Autowired
    private LocationLineGeopointService locationLineGeopointService;
    
    @Autowired
    private LocationService locationService;
    

    public String saveLocationGeoParkingArea(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) { 
        
        final String jsonString = getBody(request);
        JSONObject json;
        JSONObject locationGeoData;
        String randomLocationId;
        String data;
        boolean clearMap = false;
        
        try {
            json = new JSONObject(jsonString);
            locationGeoData = (JSONObject) json.get("locationGeoData");
            randomLocationId = locationGeoData.getString("randomId");
            data = locationGeoData.getString("data");
            if ("clearMap".equals(data)) {
                clearMap = true;
            }
        } catch (JSONException e) {
            log.warn("+++ Invalid JSON object +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }
        
        if (!clearMap) {
            // validate JSON schema
            try {
                final boolean validJson = JsonSchemaValidator.validate(jsonString, WebCoreConstants.GEO_PARKING_AREA_JSON_SCHEMA);
                if (!validJson) {
                    log.warn("+++ Invalid JSON schema in LocationLineGeopointController +++");
                    response.setStatus(HttpStatus.BAD_REQUEST.value());
                    return null;
                }
            } catch (IOException e) {
                log.warn("+++ Invalid JSON schema processing: " + e.getMessage() + " +++");
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return null;
            }
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer locationId = (Integer) keyMapping.getKey(randomLocationId);
        
        if (!DashboardUtil.validateRandomId(randomLocationId)) {
            log.warn("+++ Invalid randomised locationID in LocationLineGeopointController.saveLocationGeoParkingArea() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }
        
        final Location loc = this.locationService.findLocationById(locationId);
        // check first if the location has already a string saved in the DB (in case of one-to-one with locations)
        LocationLineGeopoint line = this.locationLineGeopointService.getLocationLineGeopointByLocationId(locationId);
        if (line != null) {
            line.setJsonstring(data);
        } else {
            line = new LocationLineGeopoint(loc, data);
        }
        if (clearMap) {
            this.locationLineGeopointService.deleteLocationLineGeopoint(line);
        } else {
            this.locationLineGeopointService.saveOrUpdateLocationLineGeopoint(line);
        }
        
        return WebCoreConstants.RESPONSE_TRUE + WebCoreConstants.COLON + randomLocationId;
    }
    
    /**
     * Read Request Body in Spring Bean
     * 
     * @param req - the servlet request
     * @return    - the body of the POST, empty if error
     */
    private String getBody(final HttpServletRequest req) {
        String body = WebCoreConstants.EMPTY_STRING;
        if (WebCoreConstants.HTTP_POST.equals(req.getMethod())) {
            final StringBuilder sb = new StringBuilder();
            BufferedReader bufferedReader = null;
            try {
                bufferedReader =  req.getReader();
                final char[] charBuffer = new char[WebCoreConstants.CHAR_BUFFER_CAPACITY];
                int bytesRead;
                while ((bytesRead = bufferedReader.read(charBuffer)) != -1) {
                    sb.append(charBuffer, 0, bytesRead);
                }
            } catch (IOException ex) {
                // swallow silently -- can't get body, won't
                log.error("+++ Cannot get body in request LocationLineGeopointController.getBody() method. +++");
            } finally {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException ex) {
                        // swallow silently -- can't get body, won't
                        log.error("+++ Cannot get body in request LocationLineGeopointController getBody() method. +++");
                    }
                }
            }
            body = sb.toString();
        }
        
        return body;
    }
}
