package com.digitalpaytech.mvc.systemadmin;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAgreement;
import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantAccountMigration;
import com.digitalpaytech.domain.SubscriptionType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.RandomIdNameSetting;
import com.digitalpaytech.dto.RestAccountInfo;
import com.digitalpaytech.dto.WebServiceEndPointInfo;
import com.digitalpaytech.dto.customeradmin.CustomerDetails;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.support.CustomerEditForm;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerAgreementService;
import com.digitalpaytech.service.CustomerMigrationService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.CustomerSubscriptionService;
import com.digitalpaytech.service.MerchantAccountMigrationService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.RestAccountService;
import com.digitalpaytech.service.SubscriptionTypeService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.WebServiceEndPointService;
import com.digitalpaytech.util.CustomerAdminUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.systemadmin.CustomerValidator;

/**
 * CustomerDetailsController handles requests coming from System Admin, Customer Details page.
 */

@Controller
//TODO remove search form   
//@SessionAttributes({ "customerEditForm", "customerSearchForm" })
@SessionAttributes({ "customerEditForm" })
public class CustomerDetailsController {
    
    private static Logger logger = Logger.getLogger(SystemAdminMainController.class);
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    @Autowired
    private RestAccountService restAccountService;
    
    @Autowired
    private WebServiceEndPointService webServiceEndPointService;
    
    @Autowired
    private CustomerSubscriptionService customerSubscriptionService;
        
    @Autowired
    private UserAccountService userAccountService;
    
    @Autowired
    private CustomerAgreementService customerAgreementService;
    
    @Autowired
    private SubscriptionTypeService subscriptionTypeService;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    @Autowired
    private CustomerMigrationService customerMigrationService;
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    private List<SubscriptionType> subscriptionTypes;
    
    @PostConstruct
    public final void loadSubscriptionTypes() {
        this.subscriptionTypes = this.subscriptionTypeService.loadAll();
        logger.info("SystemAdminMainController, loadSubscriptionTypes loaded, size: " + this.subscriptionTypes.size());
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/customers/customerDetails.html", method = RequestMethod.GET)
    public final String showCustomerDetails(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* get customer information from security framework session. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_CUSTOMER_ADMINISTRATION)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String custRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer customerId = (Integer) keyMapping.getKey(custRandomId);
        
        // Invalid customer id.
        if (customerId == null) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }
        final Customer customer = this.customerService.findCustomer(customerId);
        
        // If child customer mask inactive subscriptions by parent Customer
        final List<CustomerSubscription> custSubs = this.customerSubscriptionService.findCustomerSubscriptionsByCustomerId(customerId, true);
        List<CustomerSubscription> visibleSubs = new ArrayList<CustomerSubscription>();
        if (customer.getParentCustomer() != null) {
            final List<CustomerSubscription> parentSubs =
                    this.customerSubscriptionService.findActiveCustomerSubscriptionByCustomerId(customer.getParentCustomer().getId(), true);
            for (CustomerSubscription custSub : custSubs) {
                for (CustomerSubscription parentSub : parentSubs) {
                    if (parentSub.getSubscriptionType().getId() == custSub.getSubscriptionType().getId()) {
                        visibleSubs.add(custSub);
                        parentSubs.remove(parentSub);
                        break;
                    }
                }
            }
        } else {
            visibleSubs = custSubs;
        }
        final List<MerchantAccount> merchantAccts = this.merchantAccountService.findAllMerchantAccountsByCustomerId(customerId);
        
        this.merchantAccountService.populateMigrationInfo(merchantAccts);
        
        final List<WebServiceEndPointInfo> wsPoints = WebServiceEndPointInfo
                .convertToWebServiceEndPointInfo(this.webServiceEndPointService.findWebServiceEndPointsByCustomer(customerId), keyMapping);
        final List<WebServiceEndPointInfo> privateWsPoints = WebServiceEndPointInfo
                .convertToWebServiceEndPointInfo(this.webServiceEndPointService.findPrivateWebServiceEndPointsByCustomer(customerId), keyMapping);
        final List<RestAccountInfo> restAccts =
                RestAccountInfo.convertToRestAccountInfo(this.restAccountService.findRestAccounts(customerId), keyMapping);
        CustomerAgreement customerAgreement = null;
        if (customer.getParentCustomer() == null) {
            customerAgreement = this.customerAgreementService.findCustomerAgreementByCustomerId(customerId);
        } else {
            customerAgreement = this.customerAgreementService.findCustomerAgreementByCustomerId(customer.getParentCustomer().getId());
        }
        final List<Customer> childCustomers = setRandomIds(keyMapping, customer, customerAgreement, visibleSubs, merchantAccts);
        
        final String timeZone = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customer.getId(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE)
                .getPropertyValue();
        
        final Date trialExpiryLocal = DateUtil.convertTimeZone(timeZone, WebCoreConstants.SERVER_TIMEZONE_GMT, customer.getTrialExpiryGmt());
        final CustomerDetails customerDetails = new CustomerDetails(customer, customer.getParentCustomer(), customerAgreement, visibleSubs,
                merchantAccts, wsPoints, privateWsPoints, restAccts, childCustomers, timeZone == null || !timeZone.startsWith("US/"));
        customerDetails.setTimeZone(timeZone);
        customerDetails.setTrialExpiryLocal(trialExpiryLocal);
        
        final CustomerMigration customerMigration = this.customerMigrationService.findCustomerMigrationByCustomerId(customerId);
        if (customerMigration != null) {
            this.commonControllerHelper.setModelObjectsForMigration(customerMigration, model, WebCoreConstants.SYSTEM_ADMIN_TIMEZONE);
        }
        
        model.put("customerDetails", customerDetails);
        // SessionTool sessionTool = SessionTool.getInstance(request);
        //TODO Remove SessionToken
        //        model.put(WebSecurityConstants.SESSION_TOKEN, sessionTool.getAttribute(WebSecurityConstants.SESSION_TOKEN));
        
        //TODO remove search form   
        //        CustomerSearchForm form = new CustomerSearchForm();
        //        WebSecurityForm webSecurityForm = new WebSecurityForm(null, form);
        //        model.put("customerSearchForm", webSecurityForm);
        
        return "/systemAdmin/workspace/customers/customerDetails";
    }
    
    //--------------------------------------------------------------------------------------
    // Customer Details
    //--------------------------------------------------------------------------------------
    
    /**
     * Prepares data for editing customer information and customer subscriptions.
     */
    @RequestMapping(value = "/systemAdmin/workspace/customers/editCustomer.html", method = RequestMethod.GET)
    public final String editCustomer(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_EDIT_EXISTING_CUSTOMER)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_CREATE_NEW_CUSTOMER)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final SessionTool sessionTool = SessionTool.getInstance(request);
        final RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        final String custRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
        final Integer custId = (Integer) keyMapping.getKey(custRandomId);
        
        final Customer customer = this.customerService.findCustomer(custId);
        
        /* retrieve parent customer list. */
        final Collection<RandomIdNameSetting> parentCustomerList = new ArrayList<RandomIdNameSetting>();
        final Collection<Customer> customers = this.customerService.findAllParentCustomers();
        for (Customer c : customers) {
            parentCustomerList.add(new RandomIdNameSetting(keyMapping.getRandomString(Customer.class, c.getId()), c.getName()));
        }
        
        final CustomerEditForm form = populateCustomerEditForm(customer, keyMapping);
        final WebSecurityForm<CustomerEditForm> customerEditForm = new WebSecurityForm<CustomerEditForm>(null, form);
        customerEditForm.setActionFlag(WebSecurityConstants.ACTION_UPDATE);
        
        model.put("parentCustomerList", parentCustomerList);
        model.put("customerEditForm", customerEditForm);
        
        return "/systemAdmin/workspace/customers/include/customerForm";
    }
    
    /**
     * Populates new CustomerEditForm using data in UserAccount & Customer.
     * 
     * @param userAcct
     *            UserAccount with existing data.
     * @return CustomerEditForm Populated CustomerEditForm object.
     */
    private CustomerEditForm populateCustomerEditForm(final Customer customer, final RandomKeyMapping keyMapping) {
        final CustomerEditForm custForm = new CustomerEditForm();
        
        // Add randomIds to List<SubscriptionType>, create a Map and store in CustomerEditForm.
        this.subscriptionTypes = CustomerAdminUtil.setRandomIds(keyMapping, this.subscriptionTypes);
        
        custForm.setIsParentCompany(customer.isIsParent());
        custForm.setAccountStatus(WebCoreConstants.CUSTOMER_STATUS_TYPE_LABELS[customer.getCustomerStatusType().getId()]);
        custForm.setLegalName(customer.getName());
        custForm.setIsMigrated(customer.isIsMigrated());
        custForm.setUnifiId(customer.getUnifiId());
        if (customer.getParentCustomer() != null) {
            custForm.setRandomParentCustomerId(keyMapping.getRandomString(customer.getParentCustomer(), WebCoreConstants.ID_LOOK_UP_NAME));
        }
        if (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_EDIT_EXISTING_CUSTOMER)) {
            custForm.setIrisCustomerId(customer.getId());
        }
        
        String timeZone = WebCoreConstants.SERVER_TIMEZONE_GMT;
        final CustomerProperty currentCustomerTimeZone = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customer.getId(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        if (currentCustomerTimeZone != null) {
            timeZone = currentCustomerTimeZone.getPropertyValue();
        }
        custForm.setCurrentTimeZone(timeZone);
        custForm.setTimeZones(this.customerAdminService.getTimeZones());
        
        String dateString = "";
        if (customer.getTrialExpiryGmt() != null) {
            final Date localDate = DateUtil.convertTimeZone(timeZone, WebCoreConstants.SERVER_TIMEZONE_GMT, customer.getTrialExpiryGmt());
            dateString = DateUtil.createDateOnlyString(localDate, true);
        }
        custForm.setTrialDateString(dateString);
        UserAccount userAccount = null;
        if (customer.isIsParent()) {
            userAccount = this.userAccountService.findAdminUserAccountByParentCustomerId(customer.getId());
        } else {
            userAccount = this.userAccountService.findAdminUserAccountByChildCustomerId(customer.getId());
        }
        
        try {
            custForm.setUserName(URLDecoder.decode(userAccount.getUserName(), WebSecurityConstants.URL_ENCODING_LATIN1));
        } catch (UnsupportedEncodingException e) {
            logger.error("Username " + userAccount.getUserName() + "could not be decoded.", e);
        }
        
        final List<CustomerSubscription> custSubs = this.customerSubscriptionService.findCustomerSubscriptionsByCustomerId(customer.getId(), true);
        if (custSubs != null && !custSubs.isEmpty()) {
            for (CustomerSubscription custSub : custSubs) {
                custSub.setRandomId(keyMapping.getRandomString(custSub, WebCoreConstants.ID_LOOK_UP_NAME));
            }
        }
        List<SubscriptionType> availableTypeList = new ArrayList<SubscriptionType>();
        if (customer.getParentCustomer() != null) {
            final List<CustomerSubscription> parentActiveCustomerSubscriptionList =
                    this.customerSubscriptionService.findActiveCustomerSubscriptionByCustomerId(customer.getParentCustomer().getId(), true);
            availableTypeList = new ArrayList<SubscriptionType>();
            if (parentActiveCustomerSubscriptionList != null) {
                for (CustomerSubscription customerSubscription : parentActiveCustomerSubscriptionList) {
                    availableTypeList.add(customerSubscription.getSubscriptionType());
                }
            }
        } else {
            availableTypeList.addAll(this.subscriptionTypes);
        }
        
        custForm.setSubscriptionTypeMap(CustomerAdminUtil.createSubscriptionTypeMap(this.subscriptionTypes, availableTypeList, custSubs));
        
        return custForm;
    }
    
    //--------------------------------------------------------------------------------------
    // Merchant Accounts
    //--------------------------------------------------------------------------------------
    
    private List<Customer> setRandomIds(final RandomKeyMapping keyMapping, final Customer cust, final CustomerAgreement customerAgreement,
        final List<CustomerSubscription> custSubs, final List<MerchantAccount> merchantAccts) {
        
        cust.setRandomId(keyMapping.getRandomString(cust, WebCoreConstants.ID_LOOK_UP_NAME));
        
        List<Customer> childCustomers = null;
        if (cust.isIsParent()) {
            childCustomers = this.customerService.findAllChildCustomers(cust.getId());
            for (Customer childCustomer : childCustomers) {
                childCustomer.setRandomId(keyMapping.getRandomString(childCustomer, WebCoreConstants.ID_LOOK_UP_NAME));
            }
            
        } else {
            if (cust.getParentCustomer() != null && StringUtils.isBlank(cust.getParentCustomer().getRandomId())) {
                cust.getParentCustomer().setRandomId(keyMapping.getRandomString(cust.getParentCustomer(), WebCoreConstants.ID_LOOK_UP_NAME));
            }
        }
        if (customerAgreement != null) {
            customerAgreement.setRandomId(keyMapping.getRandomString(customerAgreement, WebCoreConstants.ID_LOOK_UP_NAME));
        }
        if (custSubs != null) {
            for (CustomerSubscription custSub : custSubs) {
                custSub.setRandomId(keyMapping.getRandomString(custSub, WebCoreConstants.ID_LOOK_UP_NAME));
            }
        }
        if (merchantAccts != null) {
            for (MerchantAccount merchantAcct : merchantAccts) {
                merchantAcct.setRandomId(keyMapping.getRandomString(merchantAcct, WebCoreConstants.ID_LOOK_UP_NAME));
                merchantAcct.setIsForValueCard(merchantAcct.getProcessor().isIsForValueCard());
            }
        }
        
        return childCustomers;
    }
    
    public final void setMerchantAccountService(final MerchantAccountService merchantAccountService) {
        this.merchantAccountService = merchantAccountService;
    }
    
    public final void setRestAccountService(final RestAccountService restAccountService) {
        this.restAccountService = restAccountService;
    }
    
    public final void setWebServiceEndPointService(final WebServiceEndPointService webServiceEndPointService) {
        this.webServiceEndPointService = webServiceEndPointService;
    }
    
    public final void setCustomerSubscriptionService(final CustomerSubscriptionService customerSubscriptionService) {
        this.customerSubscriptionService = customerSubscriptionService;
    }
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final void setUserAccountService(final UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }
    
    public final void setCustomerAgreementService(final CustomerAgreementService customerAgreementService) {
        this.customerAgreementService = customerAgreementService;
    }
    
    public final void setSubscriptionTypeService(final SubscriptionTypeService subscriptionTypeService) {
        this.subscriptionTypeService = subscriptionTypeService;
    }
    
    public final void setSubscriptionTypes(final List<SubscriptionType> subscriptionTypes) {
        this.subscriptionTypes = subscriptionTypes;
    }
    
}
