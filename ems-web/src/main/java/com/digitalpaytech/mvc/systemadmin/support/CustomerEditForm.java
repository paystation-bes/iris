package com.digitalpaytech.mvc.systemadmin.support;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.digitalpaytech.annotation.AttributeTransformer;
import com.digitalpaytech.annotation.StorageType;
import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryConstants;
import com.digitalpaytech.annotation.StoryLookup;
import com.digitalpaytech.annotation.StoryTransformer;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.SubscriptionType;
import com.digitalpaytech.domain.TimezoneVId;
import com.digitalpaytech.util.support.SubscriptionEntry;

@StoryAlias(value = CustomerEditForm.ALIAS, code = "customerEditForm")
public final class CustomerEditForm implements Serializable {
    public static final String ALIAS = "Customer Form";
    private static final long serialVersionUID = 9150549086078892770L;
    private boolean isParentCompany;
    private String legalName;
    private String randomCustomerId;
    private Integer customerId;
    private Integer irisCustomerId;
    private String randomParentCustomerId;
    private Integer parentCustomerId;
    private Integer unifiId;
    private String userName;
    private String accountStatus;
    private String trialDateString;
    private String password;
    private String passwordConfirmed;
    private String subscriptionTypeIds;
    private String customerSubscriptionRandomIds;
    private String isActivatedValues;
    private Map<Integer, SubscriptionEntry> subscriptionTypeMap;
    private Boolean hidePassword;
    private Boolean isMigrated;
    private String currentTimeZone;
    private List<TimezoneVId> timeZones;
    
    private boolean reportServices;
    private boolean coupons;
    private boolean textAlerts;
    // Value card = Custom card
    private boolean valueCards;
    private boolean creditCardProcessing;
    private boolean payByCell;
    private boolean extenByPhone;
    private boolean batchCreditCardProcessing;
    private boolean realTimeCampusCardIntegration;
    private boolean smartCardManagement;
    private boolean digitalApiRead;
    private boolean digitalApiWrite;
    
    //	public void setSubscriptionType(int id) {
    //		if (id == WebCoreConstants.SUBSCRIPTION_TYPE_STANDARD_REPORTS) {
    //			reportServices = true;
    //		} else if (id == WebCoreConstants.SUBSCRIPTION_TYPE_ALERTS) {
    //			textAlerts = true;
    //		} else if (id == WebCoreConstants.SUBSCRIPTION_TYPE_REAL_TIME_CREDIT_CARD_PROCESSING) {
    //			creditCardProcessing = true;
    //		} else if (id == WebCoreConstants.SUBSCRIPTION_TYPE_BATCH_CREDIT_CARD_PROCESSING) {
    //			batchCreditCardProcessing = true;
    //		} else if (id == WebCoreConstants.SUBSCRIPTION_TYPE_REAL_TIME_CAMPUS_CARD_INTEGRATION) {
    //			realTimeCampusCardIntegration = true;
    //		} else if (id == WebCoreConstants.SUBSCRIPTION_TYPE_COUPONS) {
    //			coupons = true;
    //		} else if (id == WebCoreConstants.SUBSCRIPTION_TYPE_CUSTOM_CARDS) {
    //			valueCards = true;
    //		} else if (id == WebCoreConstants.SUBSCRIPTION_TYPE_SMART_CARD_MANAGEMENT) {
    //			smartCardManagement = true;
    //		} else if (id == WebCoreConstants.SUBSCRIPTION_TYPE_EXTEND_BY_PHONE) {
    //			extenByPhone = true;
    //		} else if (id == WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_API_READ) {
    //			digitalApiRead = true;
    //		} else if (id == WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_API_WRITE) {
    //			digitalApiWrite = true;
    //		} else if (id == WebCoreConstants.SUBSCRIPTION_TYPE_THIRD_PARTY_PAY_BY_CELL_INTEGRATION) {
    //			payByCell = true;
    //		} 
    //	}
    
    @StoryAlias(value = "parent company", defaultData = "false")
    public boolean getIsParentCompany() {
        return this.isParentCompany;
    }
    
    public void setIsParentCompany(final boolean isParentCompany) {
        this.isParentCompany = isParentCompany;
    }
    
    @StoryAlias(value = "customer name", defaultData = "Test Customer")
    public String getLegalName() {
        return this.legalName;
    }
    
    public void setLegalName(final String legalName) {
        this.legalName = legalName;
    }
    
    public String getRandomCustomerId() {
        return this.randomCustomerId;
    }
    
    public void setRandomCustomerId(final String randomCustomerId) {
        this.randomCustomerId = randomCustomerId;
    }
    
    public Integer getCustomerId() {
        return this.customerId;
    }
    
    public void setCustomerId(final Integer customerId) {
        this.customerId = customerId;
    }
    
    @StoryAlias(value = "parent company name")
    @StoryLookup(type = Customer.ALIAS, searchAttribute = Customer.ALIAS_NAME, returnAttribute = Customer.ALIAS_ID, storage = StorageType.DB)
    @AttributeTransformer(value = "randomKey", params = { CustomerEditForm.ALIAS })
    public String getRandomParentCustomerId() {
        return this.randomParentCustomerId;
    }
    
    public void setRandomParentCustomerId(final String randomParentCustomerId) {
        this.randomParentCustomerId = randomParentCustomerId;
    }
    
    public Integer getParentCustomerId() {
        return this.parentCustomerId;
    }
    
    public void setParentCustomerId(final Integer parentCustomerId) {
        this.parentCustomerId = parentCustomerId;
    }
    
    @StoryAlias(value = "admin user name", defaultData = "testUserName")
    public String getUserName() {
        return this.userName;
    }
    
    public void setUserName(final String userName) {
        this.userName = userName;
    }

    public Integer getUnifiId() {
        return this.unifiId;
    }
    
    public void setUnifiId(final Integer unifiId) {
        this.unifiId = unifiId;
    }

    @StoryAlias(value = "account status", defaultData = "enabled")
    public String getAccountStatus() {
        return this.accountStatus;
    }
    
    public void setAccountStatus(final String accountStatus) {
        this.accountStatus = accountStatus;
    }
    
    @StoryAlias(value = "trial end date", defaultData = "next month")
    @StoryTransformer(value = "dateString", params = { StoryConstants.FORMAT_DATE_TIME_UI, StoryConstants.DEFAULT_TIME_ZONE_UI })
    public String getTrialDateString() {
        return this.trialDateString;
    }
    
    public void setTrialDateString(final String trialDateString) {
        this.trialDateString = trialDateString;
    }
    
    @StoryAlias(value = "password", defaultData = "Password$1")
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(final String password) {
        this.password = password;
    }
    
    @StoryAlias(value = "confirm password", defaultData = "Password$1")
    public String getPasswordConfirmed() {
        return this.passwordConfirmed;
    }
    
    public void setPasswordConfirmed(final String passwordConfirmed) {
        this.passwordConfirmed = passwordConfirmed;
    }
    
    public boolean isReportServices() {
        return this.reportServices;
    }
    
    public void setReportServices(final boolean reportServices) {
        this.reportServices = reportServices;
    }
    
    public boolean isCoupons() {
        return this.coupons;
    }
    
    public void setCoupons(final boolean coupons) {
        this.coupons = coupons;
    }
    
    public boolean isTextAlerts() {
        return this.textAlerts;
    }
    
    public void setTextAlerts(final boolean textAlerts) {
        this.textAlerts = textAlerts;
    }
    
    public boolean isValueCards() {
        return this.valueCards;
    }
    
    public void setValueCards(final boolean valueCards) {
        this.valueCards = valueCards;
    }
    
    public boolean isCreditCardProcessing() {
        return this.creditCardProcessing;
    }
    
    public void setCreditCardProcessing(final boolean creditCardProcessing) {
        this.creditCardProcessing = creditCardProcessing;
    }
    
    public boolean isPayByCell() {
        return this.payByCell;
    }
    
    public void setPayByCell(final boolean payByCell) {
        this.payByCell = payByCell;
    }
    
    public boolean isExtenByPhone() {
        return this.extenByPhone;
    }
    
    public void setExtenByPhone(final boolean extenByPhone) {
        this.extenByPhone = extenByPhone;
    }
    
    public boolean isBatchCreditCardProcessing() {
        return this.batchCreditCardProcessing;
    }
    
    public void setBatchCreditCardProcessing(final boolean batchCreditCardProcessing) {
        this.batchCreditCardProcessing = batchCreditCardProcessing;
    }
    
    public boolean isRealTimeCampusCardIntegration() {
        return this.realTimeCampusCardIntegration;
    }
    
    public void setRealTimeCampusCardIntegration(final boolean realTimeCampusCardIntegration) {
        this.realTimeCampusCardIntegration = realTimeCampusCardIntegration;
    }
    
    public boolean isSmartCardManagement() {
        return this.smartCardManagement;
    }
    
    public void setSmartCardManagement(final boolean smartCardManagement) {
        this.smartCardManagement = smartCardManagement;
    }
    
    public boolean isDigitalApiRead() {
        return this.digitalApiRead;
    }
    
    public void setDigitalApiRead(final boolean digitalApiRead) {
        this.digitalApiRead = digitalApiRead;
    }
    
    public boolean isDigitalApiWrite() {
        return this.digitalApiWrite;
    }
    
    public void setDigitalApiWrite(final boolean digitalApiWrite) {
        this.digitalApiWrite = digitalApiWrite;
    }
    
    public Map<Integer, SubscriptionEntry> getSubscriptionTypeMap() {
        return this.subscriptionTypeMap;
    }
    
    public void setSubscriptionTypeMap(final Map<Integer, SubscriptionEntry> subscriptionTypeMap) {
        this.subscriptionTypeMap = subscriptionTypeMap;
    }
    
    @StoryAlias(value = "subscription type ids", defaultData = "all")
    @StoryLookup(type = SubscriptionType.ALIAS, searchAttribute = SubscriptionType.ALIAS_NAME, returnAttribute = SubscriptionType.ALIAS_ID, aggregate = true)
    public String getSubscriptionTypeIds() {
        return this.subscriptionTypeIds;
    }
    
    public void setSubscriptionTypeIds(final String subscriptionTypeIds) {
        this.subscriptionTypeIds = subscriptionTypeIds;
    }
    
    @StoryAlias(value = "services assigned", defaultData = "all")
    @StoryTransformer(value = "dbBooleanSelector", params = { SubscriptionType.ALIAS, SubscriptionType.ALIAS_NAME })
    public String getIsActivatedValues() {
        return this.isActivatedValues;
    }
    
    public void setIsActivatedValues(final String isActivatedValues) {
        this.isActivatedValues = isActivatedValues;
    }
    
    public String getCustomerSubscriptionRandomIds() {
        return this.customerSubscriptionRandomIds;
    }
    
    public void setCustomerSubscriptionRandomIds(final String customerSubscriptionRandomIds) {
        this.customerSubscriptionRandomIds = customerSubscriptionRandomIds;
    }
    
    public Boolean getHidePassword() {
        return this.hidePassword;
    }
    
    public void setHidePassword(final Boolean hidePassword) {
        this.hidePassword = hidePassword;
    }
    
    public Boolean getIsMigrated() {
        return this.isMigrated;
    }
    
    public void setIsMigrated(final Boolean isMigrated) {
        this.isMigrated = isMigrated;
    }
    
    @StoryAlias(value = "current time zone", defaultData = "Canada/Pacific")
    public String getCurrentTimeZone() {
        return this.currentTimeZone;
    }
    
    public void setCurrentTimeZone(final String currentTimeZone) {
        this.currentTimeZone = currentTimeZone;
    }
    
    public List<TimezoneVId> getTimeZones() {
        return this.timeZones;
    }
    
    public void setTimeZones(final List<TimezoneVId> timeZones) {
        this.timeZones = timeZones;
    }

    public Integer getIrisCustomerId() {
        return this.irisCustomerId;
    }

    public void setIrisCustomerId(final Integer irisCustomerId) {
        this.irisCustomerId = irisCustomerId;
    }
    
}
