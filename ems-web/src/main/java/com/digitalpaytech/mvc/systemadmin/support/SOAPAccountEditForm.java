package com.digitalpaytech.mvc.systemadmin.support;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Transient;

import com.digitalpaytech.domain.WebServiceEndPointType;

public class SOAPAccountEditForm implements Serializable {
    public enum Category {
        STANDARD, PRIVATE
    };
    
    private static final long serialVersionUID = 3972244794670385852L;
    private Integer type;
    private String token;
    private String customerId;
    private String randomId;
    private String comments;
    
    @Transient
    private transient Integer customerRealId;
    
    private List<WebServiceEndPointType> endPointTypeList;
    
    public final Integer getType() {
        return this.type;
    }
    
    public final void setType(final Integer type) {
        this.type = type;
    }
    
    public final String getToken() {
        return this.token;
    }
    
    public final void setToken(final String token) {
        this.token = token;
    }
    
    public final String getComments() {
        return this.comments;
    }
    
    public final void setComments(final String comments) {
        this.comments = comments;
    }
    
    public final String getCustomerId() {
        return this.customerId;
    }
    
    public final void setCustomerId(final String customerId) {
        this.customerId = customerId;
    }
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final List<WebServiceEndPointType> getEndPointTypeList() {
        return this.endPointTypeList;
    }
    
    public final void setEndPointTypeList(final List<WebServiceEndPointType> endPointTypeList) {
        this.endPointTypeList = endPointTypeList;
    }
    
    public final Integer getCustomerRealId() {
        return this.customerRealId;
    }
    
    public final void setCustomerRealId(final Integer customerRealId) {
        this.customerRealId = customerRealId;
    }
}
