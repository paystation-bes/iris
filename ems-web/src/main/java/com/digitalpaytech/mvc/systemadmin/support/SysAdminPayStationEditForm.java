package com.digitalpaytech.mvc.systemadmin.support;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Transient;

import com.digitalpaytech.annotation.AttributeTransformer;
import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryLookup;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.dto.MapEntry;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@StoryAlias(SysAdminPayStationEditForm.ALIAS)
public class SysAdminPayStationEditForm implements Serializable {
    public static final String ALIAS = "pay station edit form";
    public static final String ALIAS_BILLABLE = "billable";
    public static final String ALIAS_LOCATION = "location";
    public static final String ALIAS_CC_MERCHANT_ACCOUNT = "credit card merchant account";
    public static final String ALIAS_POS_ID = "point of sale";
    public static final String ALIAS_LOCATION_SECOND = "location real id";
    public static final String ALIAS_DELETED = "deleted";
    public static final String ALIAS_IS_ACTIVE = "active";
    public static final String ALIAS_IS_LINUX = "linux";
    public static final String ALIAS_IS_MOVE_CUSTOMER = "new customer";
    
    /**
     * 
     */
    private static final long serialVersionUID = -4708779768518866969L;
    
    private String randomId;
    private String name;
    private String serialNumber;
    private String customerRandomId;
    private String currentCustomerRandomId;
    private String creditCardMerchantRandomId;
    private String customCardMerchantRandomId;
    private String locationRandomId;
    private String creditCardMerchantName;
    private String customCardMerchantName;
    private String locationName;
    private String parentLocationName;
    private Boolean isDecommissioned;
    private Integer alertCount;
    private Integer alertSeverity;
    private String comment;
    
    //Used for adding new pay stations
    private List<String> serialNumbers;
    
    @XStreamImplicit(itemFieldName = "payStations")
    private List<ListEntry> payStations;
    @XStreamImplicit(itemFieldName = "locations")
    private List<ListEntry> locations;
    @XStreamImplicit(itemFieldName = "creditCardMerchantAccounts")
    private List<ListEntry> creditCardMerchantAccounts;
    @XStreamImplicit(itemFieldName = "customCardMerchantAccounts")
    private List<ListEntry> customCardMerchantAccounts;
    @XStreamImplicit(itemFieldName = "customers")
    private List<ListEntry> customers;
    @XStreamImplicit(itemFieldName = "linuxes")
    private List<Boolean> linuxes;
    
    private Boolean billable;
    private Boolean bundledData;
    private Boolean active;
    private Boolean hidden;
    private Boolean moveCustomerBoolean;
    
    private Boolean isSerialNumberFixed;
    
    private String lastSeen;
    private MapEntry mapInfo;
    
    private String postToken;
    
    private Boolean updateLocations;
    private Boolean updateCreditMerchant;
    private Boolean updateCustomMerchant;
    
    private Boolean isLinux;
    
    @Transient
    private transient Integer customerRealId;
    @Transient
    private transient Integer moveCustomerRealId;
    @Transient
    private transient Integer creditCardMerchantRealId;
    @Transient
    private transient Integer customCardMerchantRealId;
    @Transient
    private transient Integer locationRealId;
    @Transient
    private transient Integer pointOfSaleRealId;
    @Transient
    private transient boolean deleteCreditCardMerchAcct;
    @Transient
    private transient boolean deleteCustomCardMerchAcct;
    @Transient
    private transient Collection<Integer> payStationRealIds;
    
    public SysAdminPayStationEditForm() {
        this.serialNumbers = new ArrayList<String>();
        this.payStations = new ArrayList<ListEntry>();
        this.locations = new ArrayList<ListEntry>();
        this.customers = new ArrayList<ListEntry>();
        this.linuxes = new ArrayList<>();
        this.creditCardMerchantAccounts = new ArrayList<ListEntry>();
        this.customCardMerchantAccounts = new ArrayList<ListEntry>();
        this.deleteCreditCardMerchAcct = false;
        this.deleteCustomCardMerchAcct = false;
    }
    
    public SysAdminPayStationEditForm(final List<String> serialNumbers, final List<ListEntry> payStations, final List<ListEntry> locations,
            final List<ListEntry> creditCardMerchantAccounts, final List<ListEntry> customCardMerchantAccounts, final List<ListEntry> customers,
            final String serialNumber, final Boolean billable, final Boolean bundledData, final Boolean active, final Boolean hidden) {
        super();
        this.serialNumbers = serialNumbers;
        this.payStations = payStations;
        this.locations = locations;
        this.creditCardMerchantAccounts = creditCardMerchantAccounts;
        this.customCardMerchantAccounts = customCardMerchantAccounts;
        this.serialNumber = serialNumber;
        this.customers = customers;
        this.billable = billable;
        this.bundledData = bundledData;
        this.active = active;
        this.hidden = hidden;
        this.deleteCreditCardMerchAcct = true;
        this.deleteCustomCardMerchAcct = true;
    }
    
    public final List<String> getSerialNumbers() {
        return this.serialNumbers;
    }
    
    public final void setSerialNumbers(final List<String> serialNumbers) {
        this.serialNumbers = serialNumbers;
    }
    
    public final String getParentLocationName() {
        return this.parentLocationName;
    }
    
    public final void setParentLocationName(final String parentLocationName) {
        this.parentLocationName = parentLocationName;
    }
    
    public final List<ListEntry> getPayStations() {
        return this.payStations;
    }
    
    public final void setPayStations(final List<ListEntry> payStations) {
        this.payStations = payStations;
    }
    
    public final void addPayStation(final String name, final String randomId, final boolean isDecommissioned, final boolean active) {
        this.payStations.add(new ListEntry(name, randomId, true, isDecommissioned, active));
    }
    
    public final List<String> getPayStationRandomIds() {
        final List<String> randomIds = new ArrayList<String>();
        for (ListEntry entry : this.payStations) {
            randomIds.add(entry.getRandomId());
        }
        return randomIds;
    }
    
    public final List<String> getPayStationNames() {
        final List<String> randomIds = new ArrayList<String>();
        for (ListEntry entry : this.payStations) {
            randomIds.add(entry.getName());
        }
        return randomIds;
    }
    
    public final List<ListEntry> getLocations() {
        return this.locations;
    }
    
    public final void setLocations(final List<ListEntry> locations) {
        this.locations = locations;
    }
    
    public final void addLocation(final String name, final String randomId, final boolean selected) {
        final List<String> locationNames = new ArrayList<String>();
        for (ListEntry location : this.locations) {
            if (location.getName().equals(name)) {
                location.setSelected(selected || location.isSelected());
                return;
            }
            locationNames.add(location.getName());
        }
        if (!locationNames.contains(name)) {
            this.locations.add(new ListEntry(name, randomId, selected));
        }
    }
    
    public final List<String> getLocationRandomIds() {
        final List<String> randomIds = new ArrayList<String>();
        for (ListEntry entry : this.locations) {
            randomIds.add(entry.getRandomId());
        }
        return randomIds;
    }
    
    public final String getSelectedLocationId() {
        for (ListEntry entry : this.locations) {
            if (entry.isSelected()) {
                return entry.getRandomId();
            }
        }
        return null;
    }
    
    public final List<ListEntry> getCreditCardMerchantAccounts() {
        return this.creditCardMerchantAccounts;
    }
    
    public final void setCreditCardMerchantAccounts(final List<ListEntry> creditCardMerchantAccounts) {
        this.creditCardMerchantAccounts = creditCardMerchantAccounts;
    }
    
    public final void addCreditCardMerchantAccount(final String name, final String randomId, final boolean selected) {
        this.creditCardMerchantAccounts.add(new ListEntry(name, randomId, selected));
    }
    
    public final List<String> getCreditCardMerchantAccountRandomIds() {
        final List<String> randomIds = new ArrayList<String>();
        for (ListEntry entry : this.creditCardMerchantAccounts) {
            randomIds.add(entry.getRandomId());
        }
        return randomIds;
    }
    
    public final String getSelectedCreditCardMerchantAccountId() {
        for (ListEntry entry : this.creditCardMerchantAccounts) {
            if (entry.isSelected()) {
                return entry.getRandomId();
            }
        }
        return null;
    }
    
    public final List<ListEntry> getCustomCardMerchantAccounts() {
        return this.customCardMerchantAccounts;
    }
    
    public final void setCustomCardMerchantAccounts(final List<ListEntry> customCardMerchantAccounts) {
        this.customCardMerchantAccounts = customCardMerchantAccounts;
    }
    
    public final void addCustomCardMerchantAccount(final String name, final String randomId, final boolean selected) {
        this.customCardMerchantAccounts.add(new ListEntry(name, randomId, selected));
    }
    
    public final String getSelectedCustomCardMerchantAccountId() {
        for (ListEntry entry : this.customCardMerchantAccounts) {
            if (entry.isSelected()) {
                return entry.getRandomId();
            }
        }
        return null;
    }
    
    public final List<String> getCustomCardMerchantAccountRandomIds() {
        final List<String> randomIds = new ArrayList<String>();
        for (ListEntry entry : this.customCardMerchantAccounts) {
            randomIds.add(entry.getRandomId());
        }
        return randomIds;
    }
    
    public final List<ListEntry> getCustomers() {
        return this.customers;
    }
    
    public final void setCustomers(final List<ListEntry> customers) {
        this.customers = customers;
    }
    
    public final void addCustomer(final String name, final String randomId, final boolean selected) {
        this.customers.add(new ListEntry(name, randomId, selected));
    }
    
    public final String getSelectedCustomerId() {
        for (ListEntry entry : this.customers) {
            if (entry.isSelected()) {
                return entry.getRandomId();
            }
        }
        return null;
    }
    
    public final List<String> getCustomerRandomIds() {
        final List<String> randomIds = new ArrayList<String>();
        for (ListEntry entry : this.customers) {
            randomIds.add(entry.getRandomId());
        }
        return randomIds;
    }
    
    @StoryAlias(ALIAS_BILLABLE)
    public final Boolean getBillable() {
        return this.billable;
    }
    
    public final void setBillable(final Boolean billable) {
        this.billable = billable;
    }
    
    public final Boolean getBundledData() {
        return this.bundledData;
    }
    
    public final void setBundledData(final Boolean bundledData) {
        this.bundledData = bundledData;
    }
    
    @StoryAlias(ALIAS_IS_ACTIVE)
    public final Boolean getActive() {
        return this.active;
    }
    
    public final void setActive(final Boolean active) {
        this.active = active;
    }
    
    @StoryAlias(value = PointOfSale.ALIAS_SERIAL_NUMBER, dependsOn = ALIAS_POS_ID)
    @StoryLookup(type = PointOfSale.ALIAS, searchAttribute = PointOfSale.ALIAS_SERIAL_NUMBER, returnAttribute = PointOfSale.ALIAS_SERIAL_NUMBER)
    public final String getSerialNumber() {
        return this.serialNumber;
    }
    
    public final void setSerialNumber(final String serialNumber) {
        this.serialNumber = serialNumber;
    }
    
    public final Boolean getHidden() {
        return this.hidden;
    }
    
    public final void setHidden(final Boolean hidden) {
        this.hidden = hidden;
    }
    
    public final String getLastSeen() {
        return this.lastSeen;
    }
    
    public final void setLastSeen(final String lastSeen) {
        this.lastSeen = lastSeen;
    }
    
    public final MapEntry getMapInfo() {
        return this.mapInfo;
    }
    
    public final void setMapInfo(final MapEntry mapInfo) {
        this.mapInfo = mapInfo;
    }
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    @StoryAlias(value = PointOfSale.ALIAS_NAME, dependsOn = ALIAS_POS_ID)
    @StoryLookup(type = PointOfSale.ALIAS, searchAttribute = PointOfSale.ALIAS_NAME, returnAttribute = PointOfSale.ALIAS_NAME)
    public final String getName() {
        return this.name;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    public final String getCustomerRandomId() {
        return this.customerRandomId;
    }
    
    public final void setCustomerRandomId(final String customerRandomId) {
        this.customerRandomId = customerRandomId;
    }
    
    public final String getCurrentCustomerRandomId() {
        return this.currentCustomerRandomId;
    }
    
    public final void setCurrentCustomerRandomId(final String currentCustomerRandomId) {
        this.currentCustomerRandomId = currentCustomerRandomId;
    }
    
    public final String getCreditCardMerchantRandomId() {
        return this.creditCardMerchantRandomId;
    }
    
    public final void setCreditCardMerchantRandomId(final String creditCardMerchantRandomId) {
        this.creditCardMerchantRandomId = creditCardMerchantRandomId;
    }
    
    public final String getCustomCardMerchantRandomId() {
        return this.customCardMerchantRandomId;
    }
    
    public final void setCustomCardMerchantRandomId(final String customCardMerchantRandomId) {
        this.customCardMerchantRandomId = customCardMerchantRandomId;
    }
    
    @StoryAlias(value = ALIAS_LOCATION_SECOND, dependsOn = ALIAS_LOCATION)
    @StoryLookup(type = Location.ALIAS, searchAttribute = Location.ALIAS_LOCATION_NAME, returnAttribute = Location.ALIAS_ID)
    @AttributeTransformer(value = "randomKey", params = { "com.digitalpaytech.domain.Location" })
    public final String getLocationRandomId() {
        return this.locationRandomId;
    }
    
    public final void setLocationRandomId(final String locationRandomId) {
        this.locationRandomId = locationRandomId;
    }
    
    public final String getCreditCardMerchantName() {
        return this.creditCardMerchantName;
    }
    
    public final void setCreditCardMerchantName(final String creditCardMerchantName) {
        this.creditCardMerchantName = creditCardMerchantName;
    }
    
    public final String getCustomCardMerchantName() {
        return this.customCardMerchantName;
    }
    
    public final void setCustomCardMerchantName(final String customCardMerchantName) {
        this.customCardMerchantName = customCardMerchantName;
    }
    
    public final String getLocationName() {
        return this.locationName;
    }
    
    public final void setLocationName(final String locationName) {
        this.locationName = locationName;
    }
    
    public final String getPostToken() {
        return this.postToken;
    }
    
    public final void setPostToken(final String postToken) {
        this.postToken = postToken;
    }
    
    public final Boolean getIsSerialNumberFixed() {
        return this.isSerialNumberFixed;
    }
    
    public final void setIsSerialNumberFixed(final Boolean isSerialNumberFixed) {
        this.isSerialNumberFixed = isSerialNumberFixed;
    }
    
    @StoryAlias(Customer.ALIAS)
    @StoryLookup(type = Customer.ALIAS, searchAttribute = Customer.ALIAS_NAME, returnAttribute = Customer.ALIAS_ID)
    public final Integer getCustomerRealId() {
        return this.customerRealId;
    }
    
    public final void setCustomerRealId(final Integer customerRealId) {
        this.customerRealId = customerRealId;
    }
    
    @StoryAlias(ALIAS_CC_MERCHANT_ACCOUNT)
    @StoryLookup(type = MerchantAccount.ALIAS, searchAttribute = MerchantAccount.ALIAS_NAME, returnAttribute = MerchantAccount.ALIAS_ID)
    public final Integer getCreditCardMerchantRealId() {
        return this.creditCardMerchantRealId;
    }
    
    public final void setCreditCardMerchantRealId(final Integer creditCardMerchantRealId) {
        this.creditCardMerchantRealId = creditCardMerchantRealId;
    }
    
    public final Integer getCustomCardMerchantRealId() {
        return this.customCardMerchantRealId;
    }
    
    public final void setCustomCardMerchantRealId(final Integer customCardMerchantRealId) {
        this.customCardMerchantRealId = customCardMerchantRealId;
    }
    
    @StoryAlias(ALIAS_LOCATION)
    @StoryLookup(type = Location.ALIAS, searchAttribute = Location.ALIAS_LOCATION_NAME, returnAttribute = Location.ALIAS_ID)
    public final Integer getLocationRealId() {
        return this.locationRealId;
    }
    
    public final void setLocationRealId(final Integer locationRealId) {
        this.locationRealId = locationRealId;
    }
    
    @StoryAlias(ALIAS_POS_ID)
    @StoryLookup(type = PointOfSale.ALIAS, searchAttribute = PointOfSale.ALIAS_NAME, returnAttribute = PointOfSale.ALIAS_ID)
    public final Integer getPointOfSaleRealId() {
        return this.pointOfSaleRealId;
    }
    
    public final void setPointOfSaleRealId(final Integer pointOfSaleRealId) {
        this.pointOfSaleRealId = pointOfSaleRealId;
    }
    
    @StoryAlias(ALIAS_DELETED)
    public final boolean isDeleteCreditCardMerchAcct() {
        return this.deleteCreditCardMerchAcct;
    }
    
    public final void setDeleteCreditCardMerchAcct(final boolean deleteCreditCardMerchAcct) {
        this.deleteCreditCardMerchAcct = deleteCreditCardMerchAcct;
    }
    
    public final boolean isDeleteCustomCardMerchAcct() {
        return this.deleteCustomCardMerchAcct;
    }
    
    public final void setDeleteCustomCardMerchAcct(final boolean deleteCustomCardMerchAcct) {
        this.deleteCustomCardMerchAcct = deleteCustomCardMerchAcct;
    }
    
    public final Boolean getMoveCustomerBoolean() {
        return this.moveCustomerBoolean;
    }
    
    public final void setMoveCustomerBoolean(final Boolean moveCustomerBoolean) {
        this.moveCustomerBoolean = moveCustomerBoolean;
    }
    
    public final Boolean getIsDecommissioned() {
        return this.isDecommissioned;
    }
    
    public final void setIsDecommissioned(final Boolean isDecommissioned) {
        this.isDecommissioned = isDecommissioned;
    }
    
    @StoryAlias(ALIAS_IS_MOVE_CUSTOMER)
    @StoryLookup(type = Customer.ALIAS, searchAttribute = Customer.ALIAS_NAME, returnAttribute = Customer.ALIAS_ID)
    public final Integer getMoveCustomerRealId() {
        return this.moveCustomerRealId;
    }
    
    public final void setMoveCustomerRealId(final Integer moveCustomerRealId) {
        this.moveCustomerRealId = moveCustomerRealId;
    }
    
    public final Collection<Integer> getPayStationRealIds() {
        return this.payStationRealIds;
    }
    
    public final void setPayStationRealIds(final Collection<Integer> payStationRealIds) {
        this.payStationRealIds = payStationRealIds;
    }
    
    public final Integer getAlertCount() {
        return this.alertCount;
    }
    
    public final void setAlertCount(final Integer alertCount) {
        this.alertCount = alertCount;
    }
    
    public final Integer getAlertSeverity() {
        return this.alertSeverity;
    }
    
    public final void setAlertSeverity(final Integer alertSeverity) {
        this.alertSeverity = alertSeverity;
    }
    
    public final String getComment() {
        return this.comment;
    }
    
    public final void setComment(final String comment) {
        this.comment = comment;
    }
    
    public final Boolean getUpdateLocations() {
        return this.updateLocations;
    }
    
    public final void setUpdateLocations(final Boolean updateLocations) {
        this.updateLocations = updateLocations;
    }
    
    public final Boolean getUpdateCreditMerchant() {
        return this.updateCreditMerchant;
    }
    
    public final void setUpdateCreditMerchant(final Boolean updateCreditMerchant) {
        this.updateCreditMerchant = updateCreditMerchant;
    }
    
    public final Boolean getUpdateCustomMerchant() {
        return this.updateCustomMerchant;
    }
    
    public final void setUpdateCustomMerchant(final Boolean updateCustomMerchant) {
        this.updateCustomMerchant = updateCustomMerchant;
    }
    
    @StoryAlias(ALIAS_IS_LINUX)
    public final Boolean getIsLinux() {
        return this.isLinux;
    }
    
    public final void setIsLinux(final Boolean isLinux) {
        this.isLinux = isLinux;
    }
    
    public final List<Boolean> getLinuxes() {
        return this.linuxes;
    }
    
    public final void setLinuxes(final List<Boolean> linuxes) {
        this.linuxes = linuxes;
    }
    
    public final void addLinux(final Boolean bLinux) {
        if (bLinux != null && bLinux.booleanValue()) {
            this.linuxes.add(Boolean.TRUE);
        } else {
            this.linuxes.add(Boolean.FALSE);
        }
    }
    
    public static class ListEntry implements Serializable {
        
        private static final long serialVersionUID = -4964358214353344987L;
        
        private String name;
        private String randomId;
        private boolean selected;
        private boolean isDecommissioned;
        private boolean active;
        
        public ListEntry() {
        }
        
        public ListEntry(final String name, final String randomId, final boolean selected) {
            this.name = name;
            this.randomId = randomId;
            this.selected = selected;
            this.isDecommissioned = false;
        }
        
        public ListEntry(final String name, final String randomId, final boolean selected, final boolean isDecommissioned, final boolean active) {
            this.name = name;
            this.randomId = randomId;
            this.selected = selected;
            this.isDecommissioned = isDecommissioned;
            this.active = active;
        }
        
        public final String getName() {
            return this.name;
        }
        
        public final void setName(final String name) {
            this.name = name;
        }
        
        public final String getRandomId() {
            return this.randomId;
        }
        
        public final void setRandomId(final String randomId) {
            this.randomId = randomId;
        }
        
        public final boolean isSelected() {
            return this.selected;
        }
        
        public final void setSelected(final boolean selected) {
            this.selected = selected;
        }
        
        public final boolean isDecommissioned() {
            return this.isDecommissioned;
        }
        
        public final void setDecommissioned(final boolean isDecommissioned) {
            this.isDecommissioned = isDecommissioned;
        }
        
        public final boolean isActive() {
            return this.active;
        }
        
        public final void setActive(final boolean active) {
            this.active = active;
        }
    }
    
}
