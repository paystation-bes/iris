package com.digitalpaytech.mvc.systemadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.mvc.AlertsSettingController;
import com.digitalpaytech.mvc.support.AlertEditForm;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

/**
 * This class handles the Alert setting request in system admin Alerts menu.
 * 
 * @author Brian Kim
 */
@Controller
//TODO remove search form   
//@SessionAttributes({ "alertEditForm", "customerSearchForm" })
@SessionAttributes({ "alertEditForm" })
public class SystemAdminAlertsSettingController extends AlertsSettingController {
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    public final void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }
    
    /**
     * This method will look at the permissions in the WebUser and pass control
     * to the first tab that the user has permissions for.
     * 
     * @return Will return the method for definedAlerts, or error if no
     *         permissions exist.
     */
    @RequestMapping(value = "/systemAdmin/workspace/alerts/index.html")
    public final String getFirstPermittedTab(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS)
            || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT)) {
            //TODO remove search form   
            //            CustomerSearchForm searchForm = new CustomerSearchForm();
            //            WebSecurityForm searchSecurityForm = new WebSecurityForm(null, searchForm);
            //            model.put("customerSearchForm", searchSecurityForm);
            
            this.commonControllerHelper.insertCustomerInModelForSystemAdmin(request, model);
            
            return getAlertList(request, response, model);
        } else {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
    }
    
    /**
     * This method will retrieve defined alert list and list of route type and
     * display them in system admin Alerts main page.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return target vm file URI which is mapped to Alerts in system admin.
     *         (/systemAdmin/alerts.vm)
     */
    @RequestMapping(value = "/systemAdmin/workspace/alerts/definedAlerts.html", method = RequestMethod.GET)
    public final String getAlertList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.getAlertListSuper(request, response, model, "/systemAdmin/workspace/alerts/definedAlerts");
    }
    
    /**
     * This method will sort the alert list based on user input and convert them
     * into JSON list and pass back to UI.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return string having sorted alert list information. "false" in case of
     *         any exception or validation failure.
     */
    @RequestMapping(value = "/systemAdmin/workspace/alerts/sortAlertList.html", method = RequestMethod.GET)
    public final String sortAlertList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.sortAlertListSuper(request, response, model);
    }
    
    /**
     * This method will view Alert Detail information on the page. When
     * "Edit Alert" is clicked, it will also be called from front-end logic
     * automatically after calling getAlertForm() method.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return JSON string having alert detail information. "false" in case of
     *         any exception or validation failure.
     */
    @RequestMapping(value = "/systemAdmin/workspace/alerts/viewAlertDetails.html", method = RequestMethod.GET)
    public @ResponseBody final String viewAlertDetails(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.viewAlertDetailsSuper(request, response, model);
    }
    
    /**
     * This method save user defined alert information.
     * 
     * @param webSecurityForm
     *            User input form
     * @param result
     *            BindingResult object to keep errors.
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return "true" string value if process succeeds, JSON string if fails.
     */
    @RequestMapping(value = "/systemAdmin/workspace/alerts/saveAlert.html", method = RequestMethod.POST)
    public @ResponseBody final String saveAlert(@ModelAttribute("alertEditForm") final WebSecurityForm<AlertEditForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.saveAlertSuper(webSecurityForm, result, request, response, model);
    }
    
    /**
     * This method deletes User Defined Alert information.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return "true" string value if process succeeds, "false" string value if
     *         process fails.
     */
    @RequestMapping(value = "/systemAdmin/workspace/alerts/deleteAlert.html", method = RequestMethod.GET)
    public @ResponseBody final String deleteAlert(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.deleteAlertSuper(request, response, model);
    }
    
    /**
     * This method sends the test notification email.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param webSecurityForm
     *            User input form
     * @param result
     *            BindingResult object to keep errors.
     * @return "true" string value if process succeeds, JSON string if fails.
     */
    @RequestMapping(value = "/systemAdmin/workspace/alerts/testNotification.html", method = RequestMethod.GET)
    public @ResponseBody final String testNotification(final HttpServletRequest request, final HttpServletResponse response,
        @ModelAttribute("alertEditForm") final WebSecurityForm<AlertEditForm> webSecurityForm, final BindingResult result) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.testNotificationSuper(request, response, webSecurityForm, result);
    }
    
}
