package com.digitalpaytech.mvc.systemadmin.support;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Transient;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.RestAccountType;

public class RESTAccountEditForm implements Serializable {

    private static final long serialVersionUID = -8820303944771501175L;
    private Integer type;
    private String key;
    private String accountName;
    private String customerId;
    private String virtualPOSId;
    private String randomId;
    private String comments;
    
    private List<PointOfSale> virtualPSList;
    private List<RestAccountType> restAccountTypeList;    
    
    @Transient
    private transient Integer customerRealId;
    
    @Transient
    private transient Integer restAccountId;
    
    @Transient
    private transient Integer virtualPosRealId; 
    
    public Integer getType() {
        return type;
    }
    
    public void setType(Integer type) {
        this.type = type;
    }
    
    public String getKey() {
        return key;
    }
    
    public void setKey(String key) {
        this.key = key;
    }

    public String getAccountName() {
        return accountName;
    }
    
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getComments(){
        return this.comments;
    }
    
    public void setComments(String comments){
        this.comments = comments;
    }
    
    public String getCustomerId() {
        return customerId;
    }
    
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
    
    public String getVirtualPOSId() {
        return virtualPOSId;
    }
    
    public void setVirtualPOSId(String virtualPOSId) {
        this.virtualPOSId = virtualPOSId;
    }

    public String getRandomId() {
        return randomId;
    }
    
    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }

    public List<PointOfSale> getVirtualPSList() {
        return virtualPSList;
    }

    public void setVirtualPSList(List<PointOfSale> virtualPSList) {
        this.virtualPSList = virtualPSList;
    }

    public List<RestAccountType> getRestAccountTypeList() {
        return restAccountTypeList;
    }

    public void setRestAccountTypeList(List<RestAccountType> restAccountTypeList) {
        this.restAccountTypeList = restAccountTypeList;
    }

    public Integer getRestAccountId() {
        return restAccountId;
    }

    public void setRestAccountId(Integer restAccountId) {
        this.restAccountId = restAccountId;
    }

    public Integer getCustomerRealId() {
        return customerRealId;
    }

    public void setCustomerRealId(Integer customerRealId) {
        this.customerRealId = customerRealId;
    }

    public Integer getVirtualPosRealId() {
        return virtualPosRealId;
    }

    public void setVirtualPosRealId(Integer virtualPosRealId) {
        this.virtualPosRealId = virtualPosRealId;
    }
}
