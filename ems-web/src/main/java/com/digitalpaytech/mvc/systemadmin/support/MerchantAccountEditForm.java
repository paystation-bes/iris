package com.digitalpaytech.mvc.systemadmin.support;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.Transient;

import com.digitalpaytech.annotation.StoryAlias;
import com.digitalpaytech.annotation.StoryLookup;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.support.ProcessorNameFormNamePair;
@StoryAlias(MerchantAccountEditForm.ALIAS) 
public class MerchantAccountEditForm implements Serializable {
    
    public static final String ALIAS_ENABLED = "Enabled";
    public static final String ALIAS_TIME_ZONE = "Time Zone";
    public static final String ALIAS_CLOSE_TIME = "Close Time";
    public static final String ALIAS = "merchant account form";
    public static final String ALIAS_RANDOM_ID = "random id";
    private static final long serialVersionUID = 2996056803852683632L;
    private String processorName;
    private String processorLabel;
    private String name;
    private String customerId;
    
    @Transient
    private transient Integer customerRealId;
    
    @Transient
    private transient Integer merchantAccountId;
    
    private String field1;
    private String field2;
    private String field3;
    private String field4;
    private String field5;
    private String field6;
    private Boolean isEnabled;
    private String randomId;
    private Integer quarterOfTheDay;
    private String timeZone;
    
    private ProcessorNameFormNamePair[][] processors;
    
    private String[] paradataGatewayProcs;
    private String[] authorizeNetGatewayProcs;
    private Map<Integer, String[]> emvProcs;
    
    /**
     * Returns corresponding MerchantStatusTypeId based on "enabled" variable value.
     * 
     * @return int WebCoreConstants.MERCHANT_STATUS_TYPE_ENABLED_ID or WebCoreConstants.MERCHANT_STATUS_TYPE_DISABLED_ID.
     */
    public final int getMerchantStatusTypeIdByEnabledStatus() {
        if (this.isEnabled != null && this.isEnabled) {
            return WebCoreConstants.MERCHANT_STATUS_TYPE_ENABLED_ID;
        }
        return WebCoreConstants.MERCHANT_STATUS_TYPE_DISABLED_ID;
    }
    
    @StoryAlias(Processor.ALIAS)
    @StoryLookup(type = Processor.ALIAS, searchAttribute = Processor.ALIAS_NAME, returnAttribute = Processor.ALIAS_NAME)
    public final String getProcessorName() {
        return this.processorName;
    }
    
    public final void setProcessorName(final String processorName) {
        this.processorName = processorName;
    }

    public final String getProcessorLabel() {
        return this.processorLabel;
    }

    public final void setProcessorLabel(final String processorLabel) {
        this.processorLabel = processorLabel;
    }
    
    @StoryAlias(MerchantAccount.ALIAS_NAME)
    public final String getName() {
        return this.name;
    }
    
    public final void setName(final String name) {
        this.name = name;
    }
    
    @StoryAlias(MerchantAccount.ALIAS_FIELD1)
    public final String getField1() {
        return this.field1;
    }
    
    public final void setField1(final String field1) {
        this.field1 = field1;
    }
    
    @StoryAlias(MerchantAccount.ALIAS_FIELD2)
    public final String getField2() {
        return this.field2;
    }
    
    public final void setField2(final String field2) {
        this.field2 = field2;
    }
    
    @StoryAlias(MerchantAccount.ALIAS_FIELD3)
    public final String getField3() {
        return this.field3;
    }
    
    public final void setField3(final String field3) {
        this.field3 = field3;
    }
    
    @StoryAlias(MerchantAccount.ALIAS_FIELD4)
    public final String getField4() {
        return this.field4;
    }
    
    public final void setField4(final String field4) {
        this.field4 = field4;
    }
    
    @StoryAlias(MerchantAccount.ALIAS_FIELD5)
    public final String getField5() {
        return this.field5;
    }
    
    public final void setField5(final String field5) {
        this.field5 = field5;
    }
    
    public final String getField6() {
        return this.field6;
    }
    
    public final void setField6(final String field6) {
        this.field6 = field6;
    }
    
    @StoryAlias(ALIAS_ENABLED)
    public final Boolean getIsEnabled() {
        return this.isEnabled;
    }
    
    public final void setIsEnabled(final Boolean isEnabled) {
        this.isEnabled = isEnabled;
    }
    
    @StoryAlias(Customer.ALIAS)
    @StoryLookup(type = Customer.ALIAS, searchAttribute = Customer.ALIAS_NAME, returnAttribute = Customer.ALIAS_ID)
    public final String getCustomerId() {
        return this.customerId;
    }
    
    public final void setCustomerId(final String customerId) {
        this.customerId = customerId;
    }
    
    @StoryAlias(value = MerchantAccountEditForm.ALIAS_RANDOM_ID, dependsOn = MerchantAccount.ALIAS_NAME)
    @StoryLookup(type = MerchantAccount.ALIAS, searchAttribute = MerchantAccount.ALIAS_NAME, returnAttribute = MerchantAccount.ALIAS_ID)
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final ProcessorNameFormNamePair[][] getProcessors() {
        return this.processors;
    }
    
    public final void setProcessors(final ProcessorNameFormNamePair[][] processors) {
        this.processors = processors;
    }
    
    public final String[] getParadataGatewayProcs() {
        return this.paradataGatewayProcs;
    }
    
    public final void setParadataGatewayProcs(final String[] paradataGatewayProcs) {
        this.paradataGatewayProcs = paradataGatewayProcs;
    }
    
    public final String[] getAuthorizeNetGatewayProcs() {
        return this.authorizeNetGatewayProcs;
    }
    
    public final void setAuthorizeNetGatewayProcs(final String[] authorizeNetGatewayProcs) {
        this.authorizeNetGatewayProcs = authorizeNetGatewayProcs;
    }
    
    /**
     * Need to follow JavaBean standard and apply camel-case method names. 
     */
    public final Map<Integer, String[]> getEmvProcs() {
        return this.emvProcs;
    }
    
    /**
     * Need to follow JavaBean standard and apply camel-case method names. 
     */
    public final void setEmvProcs(final Map<Integer, String[]> emvProcs) {
        this.emvProcs = emvProcs;
    }
    
    public final Integer getCustomerRealId() {
        return this.customerRealId;
    }
    
    public final void setCustomerRealId(final Integer customerRealId) {
        this.customerRealId = customerRealId;
    }
    
    public final Integer getMerchantAccountId() {
        return this.merchantAccountId;
    }
    
    public final void setMerchantAccountId(final Integer merchantAccountId) {
        this.merchantAccountId = merchantAccountId;
    }
    
    @StoryAlias(ALIAS_CLOSE_TIME)
    public final Integer getQuarterOfTheDay() {
        return this.quarterOfTheDay;
    }
    
    public final void setQuarterOfTheDay(final Integer quarterOfTheDay) {
        this.quarterOfTheDay = quarterOfTheDay;
    }
    
    @StoryAlias(ALIAS_TIME_ZONE)
    public final String getTimeZone() {
        return this.timeZone;
    }
    
    public final void setTimeZone(final String timeZone) {
        this.timeZone = timeZone;
    }
   
}
