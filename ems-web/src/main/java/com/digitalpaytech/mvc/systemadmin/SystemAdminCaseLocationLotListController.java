package com.digitalpaytech.mvc.systemadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.mvc.CaseLocationLotListController;
import com.digitalpaytech.mvc.support.LocationIntegrationSearchForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
public class SystemAdminCaseLocationLotListController extends CaseLocationLotListController {
    
    @ResponseBody
    @RequestMapping(value = "/systemAdmin/workspace/locations/caseLocationLotSelectors.html", method = RequestMethod.GET)
    public final String flexLocationFacilitySelectors(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<LocationIntegrationSearchForm> form,
        final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.caseLocationLotSelectorsSuper(form, request, response);
        
    }
    
}
