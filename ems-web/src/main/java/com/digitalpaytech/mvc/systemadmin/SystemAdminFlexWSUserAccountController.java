package com.digitalpaytech.mvc.systemadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.mvc.FlexWSUserAccountController;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.FlexHelper;
import com.digitalpaytech.mvc.support.FlexWSUserAccountForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
public class SystemAdminFlexWSUserAccountController extends FlexWSUserAccountController {
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    public final CommonControllerHelper getCommonControllerHelper() {
        return this.commonControllerHelper;
    }
    
    public final void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }
    
    @Override
    @RequestMapping(value = "/systemAdmin/workspace/licenses/flexCredentials.html", method = RequestMethod.GET)
    public final String view(@ModelAttribute(FlexHelper.FORM_CREDENTIALS) final WebSecurityForm<FlexWSUserAccountForm> form,
        final HttpServletRequest request, final HttpServletResponse response, final ModelMap model, final String successURL) {
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        this.commonControllerHelper.insertCustomerInModelForSystemAdmin(request, model);
        
        return super.view(form, request, response, model, "systemAdmin/workspace/licenses/flexCredentials");
    }
    
    @Override
    @ResponseBody
    @RequestMapping(value = "/systemAdmin/workspace/licenses/saveFlexCredentials.html", method = RequestMethod.POST)
    public final String save(@ModelAttribute(FlexHelper.FORM_CREDENTIALS) final WebSecurityForm<FlexWSUserAccountForm> form,
        final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response) {
        if (!canManage()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.save(form, bindingResult, request, response);
    }
    
    @Override
    @ResponseBody
    //PMD.JUnit4TestShouldUseTestAnnotation: False positive this is not a junit test
    @SuppressWarnings("PMD.JUnit4TestShouldUseTestAnnotation")
    @RequestMapping(value = "/systemAdmin/workspace/licenses/testFlexCredentials.html", method = RequestMethod.GET)
    public final String test(@RequestParam(required = false) final String booleanMode, final HttpServletRequest request,
        final HttpServletResponse response) {
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.test(booleanMode, request, response);
    }
    
    @Override
    @ResponseBody
    //PMD.JUnit4TestShouldUseTestAnnotation: False positive this is not a junit test
    @SuppressWarnings("PMD.JUnit4TestShouldUseTestAnnotation")
    @RequestMapping(value = "/systemAdmin/workspace/licenses/testNewFlexCredentials.html", method = RequestMethod.POST)
    protected final String testNew(@ModelAttribute(FlexHelper.FORM_CREDENTIALS) final WebSecurityForm<FlexWSUserAccountForm> form,
        final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response) {
        if (!canManage()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.testNew(form, bindingResult, request, response);
    }
    
    private boolean canManage() {
        return WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_MANAGE_FLEX);
    }
    
    private boolean canView() {
        return WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_VIEW_FLEX) || canManage();
    }
}
