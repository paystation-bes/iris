package com.digitalpaytech.mvc.systemadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.support.MobileLicenseEditForm;
import com.digitalpaytech.service.CustomerSubscriptionService;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
public class SystemAdminLicenseController {
    
    @Autowired
    private CustomerSubscriptionService customerSubscriptionService;
    
    @Autowired
    private SystemAdminMobileLicenseController systemAdminMobileLicenseController;
    
    @Autowired
    private SystemAdminDigitalApiController systemAdminDigitalApiController;
    
    @RequestMapping(value = "/systemAdmin/workspace/licenses/index.html")
    public final String licenseControllerIndex(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        // "/systemAdmin/workspace/licenses/index.html" URL maps to 
        // WEB-INF\templates\systemAdmin\workspace\licenses\digitalApi.vm by default 
        // or 
        // WEB-INF\templates\systemAdmin\workspace\licenses\mobileLicenses.vm if the customer does not have Digital API
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final String customerRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
        final Integer customerId = this.verifyRandomIdAndReturnActual(response, keyMapping, customerRandomId,
                                                                      "Invalid customer random id in SystemAdminLicenseController.licenseControllerIndex()");
        for (CustomerSubscription sub : this.customerSubscriptionService.findByCustomerId(customerId, true)) {
            if (sub.getSubscriptionType().getId() == WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_API_READ
                || sub.getSubscriptionType().getId() == WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_API_WRITE) {
                if (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LICENSES)
                    || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LICENSES)) {
                    return this.systemAdminDigitalApiController.digitalApiControllerIndex(request, response, model);
                }
                
            }
        }
        
        for (CustomerSubscription sub : this.customerSubscriptionService.findByCustomerId(customerId, true)) {
            if (sub.getSubscriptionType().getId() == WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_COLLECT) {
                if (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_MOBILEAPP)
                    || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_MOBILEAPP)) {
                    return "redirect:/systemAdmin/workspace/licenses/mobileLicenses.html?customerID=" + customerRandomId;
                }
            }
        }
        
        final WebSecurityForm<MobileLicenseEditForm> form = this.systemAdminMobileLicenseController.initLicenseEditForm(request, response);
        return this.systemAdminMobileLicenseController.licenseIndex(request, response, model, form);
    }
    
    protected final Integer verifyRandomIdAndReturnActual(final HttpServletResponse response, final RandomKeyMapping keyMapping, final String strRandomId,
                                                          final String message) {
        final String[] msgs = new String[] { message, message };
        return WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, strRandomId, msgs);
    }
}
