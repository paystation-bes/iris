package com.digitalpaytech.mvc.systemadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.mvc.UserAccountSettingController;
import com.digitalpaytech.mvc.support.UserAccountEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
@SessionAttributes({ "userAccountEditForm" })
public class SystemAdminUserAccountSettingController extends UserAccountSettingController {
    
    /**
     * This method will retrieve system admin's user account information and
     * display them in Admin User Account main page.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return target vm file URI which is mapped to Settings->Users
     *         (/settings/users/index.vm)
     */
    @RequestMapping(value = "/systemAdmin/adminUserAccount/userAccount.html", method = RequestMethod.GET)
    public final String getAdminUserAccountList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES)) {
            if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.getUserListByCustomer(request, response, model, "/systemAdmin/adminUserAccount/adminUserAccount");
    }
    
    /**
     * This method will view System Admin User Detail information on the page.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return JSON string having user detail information. "false" in case of
     *         any exception or validation failure.
     */
    @RequestMapping(value = "/systemAdmin/adminUserAccount/viewUserDetails.html", method = RequestMethod.GET)
    @ResponseBody
    public final String viewSystemAdminUserDetails(final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES)) {
            if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.viewUserDetails(request, response, true);
    }
    
    /**
     * This method will be called when "Add User" is clicked. It will set the
     * user form and list of Role information.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return JSON string which returns a role list.
     */
    @RequestMapping(value = "/systemAdmin/adminUserAccount/form.html", method = RequestMethod.GET)
    @ResponseBody
    public final String getUserAccountForm(final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.formUserAccount(request, response);
    }
    
    /**
     * This method saves user account information in UserAccount table.
     * 
     * @param webSecurityForm
     *            user input form
     * @param result
     *            Error object
     * @param response
     *            HttpServletResponse object
     * @return "true" + ":" + token string value if process succeeds, JSON error
     *         message if process fails
     */
    @RequestMapping(value = "/systemAdmin/adminUserAccount/saveUser.html", method = RequestMethod.POST)
    @ResponseBody
    public final String saveSystemAdminUser(@ModelAttribute("userAccountEditForm") final WebSecurityForm<UserAccountEditForm> webSecurityForm,
        final BindingResult result, final HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.saveUser(webSecurityForm, result, response);
    }
    
    /**
     * This method deletes user account information by setting user status type
     * to 2 (deleted)
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return "true" string value if process succeeds, "false" string value if
     *         process fails.
     */
    @RequestMapping(value = "/systemAdmin/adminUserAccount/deleteUser.html", method = RequestMethod.GET)
    @ResponseBody
    public final String deleteSystemAdminUser(final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.deleteUser(request, response);
    }
}
