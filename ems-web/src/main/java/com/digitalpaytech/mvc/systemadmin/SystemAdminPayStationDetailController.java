package com.digitalpaytech.mvc.systemadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.mvc.PayStationDetailController;
import com.digitalpaytech.mvc.support.PayStationCollectionFilterForm;
import com.digitalpaytech.mvc.support.PayStationCommentFilterForm;
import com.digitalpaytech.mvc.support.PayStationRecentActivityFilterForm;
import com.digitalpaytech.mvc.support.PayStationTransactionFilterForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
@SessionAttributes({ "recentActivityFilterForm", "transactionFilterForm", "collectionFilterForm" })
public class SystemAdminPayStationDetailController extends PayStationDetailController {
    
    public static final String FORM_COMMENT = "commentFilterForm";
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the battery voltage from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the batteryVoltage of the requested pay station in a JSON
     *         document
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/batteryVoltage.html")
    @ResponseBody
    public final String payStationDetailsCurrentStatusBatteryVoltage(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStationDetailsCurrentStatusBatteryVoltageSuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the sensor info trend from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the sensor info trend of the requested pay station in a JSON
     *         document
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/batteryVoltageTrend.html")
    @ResponseBody
    public final String payStationDetailsBatteryVoltageTrend(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStationDetailsBatteryVoltageTrendSuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the input current from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the inputCurrent of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/inputCurrent.html")
    @ResponseBody
    public final String payStationDetailsCurrentStatusInputCurrent(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStationDetailsCurrentStatusInputCurrentSuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the input current trend from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the inputCurrent trend of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/inputCurrentTrend.html")
    @ResponseBody
    public final String payStationDetailsInputCurrentTrend(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStationDetailsInputCurrentTrendSuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the ambient temperature from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the ambientTemperature of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/ambientTemperature.html")
    @ResponseBody
    public final String payStationDetailsCurrentStatusAmbientTemperature(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStationDetailsCurrentStatusAmbientTemperatureSuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the ambient temperature trend from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the ambientTemperature trend of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/ambientTemperatureTrend.html")
    @ResponseBody
    public final String payStationDetailsCAmbientTemperatureTrend(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStationDetailsAmbientTemperatureTrendSuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the humidity from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the humidity of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/relativeHumidity.html")
    @ResponseBody
    public final String payStationDetailsCurrentStatusRelativeHumidity(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStationDetailsCurrentStatusRelativeHumiditySuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the humidity trend from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the humidity trend of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/relativeHumidityTrend.html")
    @ResponseBody
    public final String payStationDetailsRelativeHumidityTrend(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStationDetailsRelativeHumidityTrendSuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the controllerTemperature from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the controllerTemperature of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/controllerTemperature.html")
    @ResponseBody
    public final String payStationDetailsCurrentStatusControllerTemperature(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStationDetailsCurrentStatusControllerTemperatureSuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the controllerTemperature trend from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the controllerTemperature trend of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/controllerTemperatureTrend.html")
    @ResponseBody
    public final String payStationDetailsControllerTemperatureTrend(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStationDetailsControllerTemperatureTrendSuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the system load from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the system load of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/systemLoad.html")
    @ResponseBody
    public final String payStationDetailsCurrentStatusSystemLoad(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        /* validate user account if it has proper role to view stations. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_VIEW_PAYSTATION)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStationDetailsCurrentStatusSystemLoadSuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns the system load trend from the service state table
     * 
     * @param request
     * @param response
     * @param model
     * @return the system load trend of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/systemLoadTrend.html")
    @ResponseBody
    public final String payStationDetailsSystemLoadTrend(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper role to view stations. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_VIEW_PAYSTATION)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStationDetailsSystemLoadTrendSuper(request, response, model);
    }
    
    /**
     * This method retrieves and returns filter information for the alert
     * section of pay station details
     * 
     * @param request
     * @param response
     * @param model
     * @return alert information of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/alertsFilter.html")
    public final String payStationDetailsAlertsFilter(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper role to view alerts. */
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStationDetailsAlertsFilterSuper(request, response, model, "/systemAdmin/workspace/payStations/include/alertFilter");
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns alert information for the requested pay station
     * 
     * @param request
     * @param response
     * @param model
     * @return alert information of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/alerts.html", method = RequestMethod.GET)
    @ResponseBody
    public final String payStationDetailsAlerts(
        @ModelAttribute("recentActivityFilterForm") final WebSecurityForm<PayStationRecentActivityFilterForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response) {
        
        /* validate user account if it has proper role to manage pay stations. */
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStationDetailsAlertsSuper(webSecurityForm, result, request, response);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns alert information for the requested pay station
     * 
     * @param request
     * @param response
     * @param model
     * @return alert information of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/alertsHistory.html")
    @ResponseBody
    public final String payStationDetailsAlertsHistory(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper role to manage pay stations. */
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStationDetailsAlertsHistorySuper(request, response, model);
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns pay station details in a PayStationDetails object
     * 
     * @param request
     * @param response
     * @param model
     * @return A PayStationDetails object populated with the details of the
     *         requested pay station in a JSON document
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/information.html")
    @ResponseBody
    public final String payStationDetailsInformation(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStationDetailsInformationSuper(request, response, model, true);
    }
    
    /**
     * This method retrieves and returns filter information for the transaction report
     * section of pay station details
     * 
     * @param request
     * @param response
     * @param model
     * @return alert information of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/transactionFilter.html")
    public final String payStationDetailsTransactionFilter(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        /* validate user account if it has proper role to view alerts. */
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStationDetailsTransactionFilterSuper(request, response, model,
                                                             "/systemAdmin/workspace/payStations/include/transactionFilter");
    }
    
    /**
     * This method takes a payStationID request parameter input, and returns
     * data listing transaction reports for the last 90 days
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON objects collectionsReports and transactionReports
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/viewTransactionReports.html")
    @ResponseBody
    public final String viewTransactionReports(
        @ModelAttribute("transactionFilterForm") final WebSecurityForm<PayStationTransactionFilterForm> webSecurityForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.viewTransactionReportsSuper(webSecurityForm, result, request, response);
    }
    
    /**
     * This method retrieves and returns filter information for the collection report
     * section of pay station details
     * 
     * @param request
     * @param response
     * @param model
     * @return alert information of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/collectionFilter.html")
    public final String payStationDetailsCollectionFilter(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        /* validate user account if it has proper role to view alerts. */
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStationDetailsCollectionFilterSuper(request, response, model, "/systemAdmin/workspace/payStations/include/collectionFilter");
    }
    
    /**
     * This method takes a payStationID request parameter input, and returns
     * data listing collection reports for the last 90 days
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON objects collectionsReports and transactionReports
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/viewCollectionReports.html")
    @ResponseBody
    public final String viewCollectionReports(
        @ModelAttribute("collectionFilterForm") final WebSecurityForm<PayStationCollectionFilterForm> webSecurityForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.viewCollectionReportsSuper(webSecurityForm, result, request, response);
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/printCollectionReport.html")
    @ResponseBody
    public final String printCollectionReport(
        @ModelAttribute("collectionFilterForm") final WebSecurityForm<PayStationCollectionFilterForm> webSecurityForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response) {
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.printCollectionReportSuper(webSecurityForm, result, request, response);
    }
    
    /**
     * This method takes a transaction report random ID as input which is taken
     * from the output from payStationDetailsReprots(). It then returns vm file URL
     * string "transactionReport" with details for that transaction
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/transactionReport.html")
    public final String payStationDetailsReportsTransactionDetails(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStationDetailsReportsTransactionDetails(request, response, model,
                                                                "/systemAdmin/workspace/payStations/include/transactionReportDetail");
    }
    
    /**
     * This method takes a collection report random ID as input which is taken
     * from the output from payStationDetailsReprots(). It then returns vm file URL
     * string "collectionReport" with details for that collection
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/collectionReport.html")
    public final String payStationDetailsReportsCollectionDetails(final HttpServletRequest request, final HttpServletResponse response,
        final ModelMap model) {
        
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStationDetailsReportsCollectionsDetails(request, response, model,
                                                                "/systemAdmin/workspace/payStations/include/collectionReportDetail");
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/exportCollectionReport.html")
    public final String exportCollectionReport(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.exportCollectionReport(request, response);
    }
    
    /**
     * This method takes the request parameter "payStationId" and "sensorInfoTypeId'
     * returns the 90 day history for that sensor Type
     * 
     * @param request
     * @param response
     * @param model
     * @return the list of Sensor History of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/sensorHistory.html")
    @ResponseBody
    public final String payStationDetailsSensorHistory(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        super.payStationDetailsSensorHistorySuper(request, response, model);
        return WebCoreConstants.EMPTY_STRING;
        
    }
    
    /**
     * This method takes a payStationID request parameter input, and returns
     * data listing the history of comments for the paystation
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON objects history notes
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails/historyNotes.html")
    @ResponseBody
    public final String viewHistoryNotes(@ModelAttribute(FORM_COMMENT) final WebSecurityForm<PayStationCommentFilterForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.viewHistoryNotesSuper(webSecurityForm, result, request, response);
    }
    
    @ModelAttribute(FORM_COMMENT)
    public final WebSecurityForm<PayStationCommentFilterForm> initSearchForm(final HttpServletRequest request) {
        return new WebSecurityForm<PayStationCommentFilterForm>((Object) null, new PayStationCommentFilterForm(),
                SessionTool.getInstance(request).locatePostToken(FORM_COMMENT));
    }
    
    /**
     * This method takes the request parameter "payStationId" and retrieves and
     * returns alert information for the requested pay station
     * 
     * @param request
     * @param response
     * @param model
     * @return alert information of the requested pay station in a JSON document
     */
    @RequestMapping(value = "/systemAdmin/workspace/locations/payStationList/clearAlert.html", method = RequestMethod.GET)
    @ResponseBody
    public final String clearStationDetailsAlert(final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_CLEAR_ACTIVE_ALERTS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.clearActiveAlert(request, response);
    }
    
    private boolean canView() {
        return WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_VIEW_PAYSTATION) || canManage();
    }
    
    private boolean canManage() {
        return WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_MANAGE_PAYSTATION)
               || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_SET_BILLING_PARAMETERS)
               || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_CREATE_PAYSTATIONS)
               || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_MOVE_PAYSTATION);
    }
}
