package com.digitalpaytech.mvc.systemadmin.support;

import java.io.Serializable;

import com.digitalpaytech.constants.SortOrder;
import com.digitalpaytech.mvc.systemadmin.support.CustomerMigrationRecentlyBoardedFilterForm.SortColumn;

public class CustomerMigrationRecentlyMigratedFilterForm implements Serializable {
    
    private static final long serialVersionUID = -8282292846569037108L;
    private Integer page;
    private SortColumn column;
    private SortOrder order;
    
    public CustomerMigrationRecentlyMigratedFilterForm() {
        this.column = SortColumn.customerName;
        this.order = SortOrder.ASC;
        this.page = 1;
    }
    
    public CustomerMigrationRecentlyMigratedFilterForm(final Integer page, final SortColumn column, final SortOrder order) {
        super();
        this.page = page;
        this.column = column;
        this.order = order;
    }
    
    public final Integer getPage() {
        return this.page;
    }
    
    public final void setPage(final Integer page) {
        this.page = page;
    }
    
    public final SortColumn getColumn() {
        return this.column;
    }
    
    public final void setColumn(final SortColumn column) {
        this.column = column;
    }
    
    public final SortOrder getOrder() {
        return this.order;
    }
    
    public final void setOrder(final SortOrder order) {
        this.order = order;
    }
    
    public enum SortColumn {
        customerName, date, payStationCount;
    }
    
}
