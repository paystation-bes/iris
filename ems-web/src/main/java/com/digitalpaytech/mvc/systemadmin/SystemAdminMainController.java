package com.digitalpaytech.mvc.systemadmin;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerStatusType;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.CustomerType;
import com.digitalpaytech.domain.MobileApplicationType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.SubscriptionType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserDefaultDashboard;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.domain.UserStatusType;
import com.digitalpaytech.dto.RandomIdNameSetting;
import com.digitalpaytech.dto.SearchCustomerResult;
import com.digitalpaytech.dto.SearchCustomerResultSetting;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.mvc.systemadmin.support.CustomerEditForm;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerAlertTypeService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.CustomerSubscriptionService;
import com.digitalpaytech.service.MobileLicenseService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.SubscriptionTypeService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.systemadmin.SystemAdminService;
import com.digitalpaytech.util.CustomerAdminUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebPermissionUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.SubscriptionEntry;
import com.digitalpaytech.util.support.WebObject;
import com.digitalpaytech.util.support.WebObjectId;
import com.digitalpaytech.validation.systemadmin.CustomerValidator;

/**
 * This controller handles back-end logics to display system admin main page.
 * 
 * @author Brian Kim
 * 
 */
@Controller
//TODO remove search form   
//@SessionAttributes({ "customerEditForm", "customerSearchForm" })
@SessionAttributes({ "customerEditForm" })
public class SystemAdminMainController {
    
    private static Logger logger = Logger.getLogger(SystemAdminMainController.class);
    @Autowired
    private CustomerAdminService customerAdminService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private SystemAdminService systemAdminService;
    
    @Autowired
    private UserAccountService userAccountService;
    
    @Autowired
    private SubscriptionTypeService subscriptionTypeService;
    
    @Autowired
    private CustomerValidator customerValidator;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private CustomerSubscriptionService customerSubscriptionService;
    
    @Autowired
    private MobileLicenseService mobileLicenseService;
    
    @Autowired
    private CustomerAlertTypeService customerAlertTypeService;
    
    private List<SubscriptionType> subscriptionTypes;
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final void setCustomerValidator(final CustomerValidator customerValidator) {
        this.customerValidator = customerValidator;
    }
    
    public final void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    public final void setPasswordEncoder(final PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }
    
    public final void setSystemAdminService(final SystemAdminService systemAdminService) {
        this.systemAdminService = systemAdminService;
    }
    
    public final void setSubscriptionTypeService(final SubscriptionTypeService subscriptionTypeService) {
        this.subscriptionTypeService = subscriptionTypeService;
    }
    
    public final void setCustomerSubscriptionService(final CustomerSubscriptionService customerSubscriptionService) {
        this.customerSubscriptionService = customerSubscriptionService;
    }
    
    public final void setUserAccountService(final UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }
    
    public final void setSubscriptionTypes(final List<SubscriptionType> subscriptionTypes) {
        this.subscriptionTypes = subscriptionTypes;
    }
    
    @PostConstruct
    public void loadSubscriptionTypes() {
        this.subscriptionTypes = this.subscriptionTypeService.loadAll();
        logger.info("SystemAdminMainController, loadSubscriptionTypes loaded, size: " + this.subscriptionTypes.size());
    }
    
    /**
     * This method shows System Admin main page with the customer/paystation list.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return target vm file URI which is mapped to System Admin main landing page (/systemAdmin/index.vm)
     */
    @RequestMapping(value = "/systemAdmin/workspaceIndex.html", method = RequestMethod.GET)
    public final String getCustomersAndPaystations(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* get customer information from security framework session. */
        if (!WebPermissionUtil.hasSystemAdminWorkspacePermissions()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        //TODO remove search form   
        //        /* initialize the CustomerSearchForm. */
        //        WebSecurityForm customerSearchForm = new WebSecurityForm(null, new CustomerSearchForm());
        //        customerSearchForm.setActionFlag(WebSecurityConstants.ACTION_SEARCH);
        //        model.put("customerSearchForm", customerSearchForm);
        
        return "/systemAdmin/index";
    }
    
    /**
     * This method is called to search the customers/pay stations by Ajax in system admin main page.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return JSON object string
     */
    @RequestMapping(value = "/systemAdmin/searchCustomerAndPos.html", method = RequestMethod.GET)
    @ResponseBody
    public final String searchCustomerAndPos(final HttpServletRequest request, final HttpServletResponse response) {
        
        /* get customer information from security framework session. */
        if (!WebPermissionUtil.hasSystemAdminWorkspacePermissions()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String search = request.getParameter("search");
        
        final SearchCustomerResult searchCustomerResult = new SearchCustomerResult();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        if (!StringUtils.isBlank(search)) {
            if (!search.matches(WebCoreConstants.REGEX_PRINTABLE_TEXT)) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            if (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_PAYSTATION_ADMINISTRATION)) {
                /* retrieve all pay stations by serial number. */
                if (StringUtils.isNumeric(search)) {
                    final Collection<PointOfSale> posList = this.pointOfSaleService.findAllPointOfSalesBySerialNumberKeyword(search);
                    for (PointOfSale pos : posList) {
                        searchCustomerResult.addPosSearchResult(new SearchCustomerResultSetting(keyMapping.getRandomString(PointOfSale.class,
                                                                                                                           pos.getId()), pos
                                .getSerialNumber(), pos.getCustomer().getName()));
                    }
                }
                
                /* retrieve all pay stations by PointOfSale name. */
                final Collection<PointOfSale> posList = this.pointOfSaleService.findAllPointOfSalesByPosNameKeyword(search);
                for (PointOfSale pos : posList) {
                    if (pos.getName().equals(pos.getSerialNumber())) {
                        continue;
                    }
                    
                    searchCustomerResult.addPosNameSearchResult(new SearchCustomerResultSetting(keyMapping.getRandomString(PointOfSale.class,
                                                                                                                           pos.getId()), pos
                            .getName(), pos.getCustomer().getName()));
                }
            }
            
            /* retrieve all parent and child customers. */
            //                Collection<Customer> customers = this.customerService.findAllParentAndChildCustomersBySearchKeyword(search);
            Collection<Customer> customers = null;
            final String selectOnlyChild = request.getParameter("childCompany");
            if ((selectOnlyChild != null) && (WebCoreConstants.RESPONSE_TRUE.equals(selectOnlyChild.toLowerCase()))) {
                customers = this.customerService.findAllParentAndChildCustomersBySearchKeyword(search, WebCoreConstants.CUSTOMER_TYPE_CHILD);
            } else {
                customers = this.customerService.findAllParentAndChildCustomersBySearchKeyword(search, WebCoreConstants.CUSTOMER_TYPE_PARENT,
                                                                                               WebCoreConstants.CUSTOMER_TYPE_CHILD);
            }
            
            for (Customer customer : customers) {
                final boolean isParent = customer.isIsParent();
                String parentName = null;
                if (customer.getParentCustomer() != null) {
                    parentName = customer.getParentCustomer().getName();
                }
                searchCustomerResult.addCustomerSearchResult(new SearchCustomerResultSetting(keyMapping.getRandomString(Customer.class,
                                                                                                                        customer.getId()), customer
                        .getName(), parentName, isParent));
            }
        }
        
        return WidgetMetricsHelper.convertToJson(searchCustomerResult, "searchCustomerResult", true);
    }
    
    /**
     * This method is called to search the customers/pay stations by Ajax in system admin main page.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return JSON object string
     */
    @RequestMapping(value = "/systemAdmin/searchPos.html", method = RequestMethod.POST)
    @ResponseBody
    public final String searchPos(final HttpServletRequest request, final HttpServletResponse response) {
        
        /* get customer information from security framework session. */
        if (!canViewPayStation()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String search = request.getParameter("search");
        
        final SearchCustomerResult searchCustomerResult = new SearchCustomerResult();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final Integer customerId = keyMapping.getKey(request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID), Customer.class, Integer.class);
        if (customerId == null) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        if (!StringUtils.isBlank(search)) {
            if (!search.matches(WebCoreConstants.REGEX_PAYSTATION_TEXT)) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            if (canViewPayStation()) {
                /* retrieve all pay stations by serial number. */
                if (StringUtils.isNumeric(search)) {
                    final Collection<PointOfSale> posList = this.pointOfSaleService.findPointOfSalesBySerialNumberKeywordWithLimit(search, customerId);
                    for (PointOfSale pos : posList) {
                        searchCustomerResult.addPosSearchResult(new SearchCustomerResultSetting(keyMapping.getRandomString(PointOfSale.class,
                                                                                                                           pos.getId()), pos
                                .getSerialNumber(), pos.getLocation().getName()));
                    }
                }
                
                /* retrieve all pay stations by PointOfSale name. */
                final Collection<PointOfSale> posList = this.pointOfSaleService.findPointOfSalesByPosNameKeywordWithLimit(search, customerId);
                for (PointOfSale pos : posList) {
                    if (pos.getName().equals(pos.getSerialNumber())) {
                        continue;
                    }
                    
                    searchCustomerResult.addPosNameSearchResult(new SearchCustomerResultSetting(keyMapping.getRandomString(PointOfSale.class,
                                                                                                                           pos.getId()), pos
                            .getName(), pos.getLocation().getName()));
                }
            }
        }
        
        return WidgetMetricsHelper.convertToJson(searchCustomerResult, "searchCustomerResult", true);
    }
    
    /**
     * This method is called to search the customers by Ajax for customer selection inputs.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return JSON object string
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/systemAdmin/searchCustomer.html", method = RequestMethod.GET)
    @ResponseBody
    public final String searchCustomer(final HttpServletRequest request, final HttpServletResponse response) {
        
        /* get customer information from security framework session. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_CUSTOMER_ADMINISTRATION)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String search = request.getParameter("search");
        
        final SearchCustomerResult searchCustomerResult = new SearchCustomerResult();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        if (!StringUtils.isBlank(search)) {
            if (!search.matches(WebCoreConstants.REGEX_PRINTABLE_TEXT)) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            /* retrieve all parent and child customers. */
            Collection<Customer> customers = null;
            final String selectOnlyChild = request.getParameter("childCompany");
            if ((selectOnlyChild != null) && ("true".equals(selectOnlyChild.toLowerCase()))) {
                customers = this.customerService.findAllParentAndChildCustomersBySearchKeyword(search, WebCoreConstants.CUSTOMER_TYPE_CHILD);
            } else {
                customers = this.customerService.findAllParentAndChildCustomersBySearchKeyword(search, WebCoreConstants.CUSTOMER_TYPE_PARENT,
                                                                                               WebCoreConstants.CUSTOMER_TYPE_CHILD);
            }
            
            final Collection<WebObject> wrappedCustomers = keyMapping.hideProperties(customers, WebCoreConstants.ID_LOOK_UP_NAME);
            for (WebObject webObj : wrappedCustomers) {
                final Customer customer = (Customer) webObj.getWrappedObject();
                final boolean isParent = customer.isIsParent();
                String parentName = null;
                if (customer.getParentCustomer() != null) {
                    parentName = customer.getParentCustomer().getName();
                }
                searchCustomerResult.addCustomerSearchResult(new SearchCustomerResultSetting(webObj.getPrimaryKey().toString(), customer.getName(),
                        parentName, isParent));
            }
        }
        
        return WidgetMetricsHelper.convertToJson(searchCustomerResult, "searchCustomerResult", true);
    }
    
    /**
     * This method redirects the page to either customer detail or pay station details depending on user input.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return Redirect URL string value
     */
    //    @RequestMapping(value = "/systemAdmin/goDetail.html", method = RequestMethod.POST)
    //    public String goDetails(@ModelAttribute("customerSearchForm") WebSecurityForm webSecurityForm, BindingResult result, HttpServletRequest request,
    //                            HttpServletResponse response, ModelMap model) {
    @RequestMapping(value = "/systemAdmin/goDetail.html", method = RequestMethod.GET)
    public final String goDetails(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* get customer information from security framework session. */
        if (!WebPermissionUtil.hasSystemAdminWorkspacePermissions()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String randomId = request.getParameter("randomId");
        
        final StringBuilder res = new StringBuilder();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final WebObjectId objId = keyMapping.getKeyObject(randomId);
        
        if (objId == null) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
            //            webSecurityForm.addErrorStatus(new FormErrorStatus("search", this.customerSearchValidator
            //                    .getMessage("error.common.invalid", new String[] { 
            //            this.customerSearchValidator.getMessage("label.systemAdmin.customerPayStation") })));
            //            webSecurityForm.resetToken();
            //            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        final Class<?> type = objId.getObjectType();
        if (PointOfSale.class.equals(type)) {
            if (!canViewPayStation()) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            return redirectToPayStationSection(res, keyMapping, randomId);
        } else if (Customer.class.equals(type)) {
            final Set<CustomerSubscription> subscriptions = this.customerSubscriptionService.findByCustomerId((Integer) objId.getId(), true);
            final Set<Integer> subscriptionIds = new HashSet<Integer>(subscriptions.size() * 2);
            for (CustomerSubscription cs : subscriptions) {
                if (cs.isIsEnabled()) {
                    subscriptionIds.add(cs.getSubscriptionType().getId());
                }
            }
            
            if (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_CUSTOMER_ADMINISTRATION)) {
                return res.append("redirect:/systemAdmin/workspace/customers/customerDetails.html?customerID=").append(randomId).toString();
            } else if (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
                // No need to check customer for "Standard Report" Subscription becuase target of these reports are for System Admin.
                return res.append("redirect:/systemAdmin/workspace/reporting/index.html?&customerID=").append(randomId).toString();
            } else if (((WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_MOBILEAPP) || WebSecurityUtil
                    .hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_MOBILEAPP)) && (subscriptionIds
                    .contains(WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_COLLECT)
                                                                                                              || subscriptionIds
                                                                                                                      .contains(WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_MAINTAIN) || subscriptionIds
                    .contains(WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_PATROL)))
                       || (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DIGITAL_API_ADMINISTRATION) && (subscriptionIds
                               .contains(WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_API_READ) || subscriptionIds
                               .contains(WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_API_WRITE)))) {
                return res.append("redirect:/systemAdmin/workspace/licenses/index.html?&customerID=").append(randomId).toString();
            } else if (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_PAYSTATION_ADMINISTRATION)) {
                return redirectToPayStationSection(res, keyMapping, randomId);
            } else if (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS)
                       || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS)) {
                return res.append("redirect:/systemAdmin/workspace/locations/index.html?&customerID=").append(randomId).toString();
            } else if (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_ROUTES)
                       || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES)) {
                return res.append("redirect:/systemAdmin/workspace/routes/index.html?&customerID=").append(randomId).toString();
            } else if (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_ALERTS_MANAGEMENT)
                       && subscriptionIds.contains(WebCoreConstants.SUBSCRIPTION_TYPE_ALERTS)) {
                return res.append("redirect:/systemAdmin/workspace/alerts/index.html?&customerID=").append(randomId).toString();
            } else {
                return "redirect:/systemAdmin/workspaceIndex.html?error=-1";
            }
        } else {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
    }
    
    private String redirectToPayStationSection(final StringBuilder res, final RandomKeyMapping keyMapping, final String randomId) {
        final WebObjectId key = keyMapping.getKeyObject(randomId);
        if (Customer.class.isAssignableFrom(key.getObjectType())) {
            res.append("redirect:/systemAdmin/workspace/payStations/index.html?customerID=").append(randomId);
        } else if (PointOfSale.class.isAssignableFrom(key.getObjectType())) {
            final PointOfSale pos = this.pointOfSaleService.findPointOfSaleById((Integer) key.getId());
            
            res.append("redirect:/systemAdmin/workspace/payStations/index.html?posID=").append(randomId);
            res.append("&customerID=").append(keyMapping.getRandomString(Customer.class, pos.getCustomer().getId()));
        }
        
        return res.toString();
    }
    
    /**
     * This method prepares adding new customer form.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return @return target vm file URI which is mapped to new customer input form (/systemAdmin/workspace/customers/include/customerForm.vm)
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/systemAdmin/workspace/customers/newCustomer.html", method = RequestMethod.GET)
    public final String showNewCustomerForm(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* get customer information from security framework session. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_CREATE_NEW_CUSTOMER)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Collection<RandomIdNameSetting> parentCustomerList = new ArrayList<RandomIdNameSetting>();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        // Add randomIds to List<SubscriptionType>, create a Map and store in CustomerEditForm.
        this.subscriptionTypes = CustomerAdminUtil.setRandomIds(keyMapping, this.subscriptionTypes);
        final CustomerEditForm form = new CustomerEditForm();
        form.setSubscriptionTypeMap(CustomerAdminUtil.createSubscriptionTypeMap(this.subscriptionTypes));
        form.setTimeZones(customerAdminService.getTimeZones());
        /* make an instance for user account form. */
        final WebSecurityForm<CustomerEditForm> customerEditForm = new WebSecurityForm<CustomerEditForm>(null, form);
        customerEditForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        
        /* retrieve parent customer list. */
        final Collection<Customer> customers = this.customerService.findAllParentCustomers();
        final Collection<WebObject> wrappedCustomers = keyMapping.hideProperties(customers, WebCoreConstants.ID_LOOK_UP_NAME);
        for (WebObject obj : wrappedCustomers) {
            final Customer customer = (Customer) obj.getWrappedObject();
            parentCustomerList.add(new RandomIdNameSetting(obj.getPrimaryKey().toString(), customer.getName()));
        }
        
        model.put("parentCustomerList", parentCustomerList);
        model.put("customerEditForm", customerEditForm);
        
        return "/systemAdmin/workspace/customers/include/customerForm";
    }
    
    /**
     * This method saves customer information in Customer table.
     * 
     * @param webSecurityForm
     *            user input form
     * @param result
     *            Error object
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return "true" string value if process succeeds, JSON error message if process fails
     */
    @RequestMapping(value = "/systemAdmin/workspace/customers/saveCustomer.html", method = RequestMethod.POST)
    @ResponseBody
    public final String saveCustomer(@ModelAttribute("customerEditForm") final WebSecurityForm<CustomerEditForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper permission to view it. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_CREATE_NEW_CUSTOMER)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_EDIT_EXISTING_CUSTOMER)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        /* validate user input. */
        final boolean isMigrated = this.customerValidator.validateWithMigrated(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        webSecurityForm.resetToken();
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        // Check if randomIds are set already.
        if (StringUtils.isEmpty(this.subscriptionTypes.get(0).getRandomId())) {
            this.subscriptionTypes = CustomerAdminUtil.setRandomIds(keyMapping, this.subscriptionTypes);
        }
        
        /* create Customer and UserAccount. */
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        return saveCustomerAndUserAccount(webSecurityForm, userAccount, keyMapping, isMigrated);
    }
    
    /**
     * This method gets subscriptiontypeMap depending on parent customers subscriptions.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return json of subscriptionTypeMap
     */
    @RequestMapping(value = "/systemAdmin/workspace/customers/getSubscriptionTypeMap.html", method = RequestMethod.GET)
    @ResponseBody
    public final String getSubscriptionTypeMap(final HttpServletRequest request, final HttpServletResponse response) {
        
        /* validate user account if it has proper permission to view it. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_CREATE_NEW_CUSTOMER)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_EDIT_EXISTING_CUSTOMER)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String parentCustomerRandomId = request.getParameter("parentCustomerID");
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer parentCustomerId = (Integer) keyMapping.getKey(parentCustomerRandomId);
        
        Map<Integer, SubscriptionEntry> subscriptionTypeMap = null;
        if (parentCustomerId != null) {
            
            final List<CustomerSubscription> customerSubscriptionList = this.customerSubscriptionService
                    .findActiveCustomerSubscriptionByCustomerId(parentCustomerId.intValue(), true);
            
            final List<SubscriptionType> availableSubscriptionTypes = new ArrayList<SubscriptionType>();
            if (customerSubscriptionList != null) {
                for (CustomerSubscription customerSubscription : customerSubscriptionList) {
                    availableSubscriptionTypes.add(customerSubscription.getSubscriptionType());
                }
            }
            
            subscriptionTypeMap = CustomerAdminUtil.createAvailableSubscriptionTypeMap(this.subscriptionTypes, availableSubscriptionTypes);
        } else {
            subscriptionTypeMap = CustomerAdminUtil.createSubscriptionTypeMap(this.subscriptionTypes);
        }
        
        final List<SubscriptionEntry> subscriptionList = new ArrayList<SubscriptionEntry>();
        for (SubscriptionEntry subscription : subscriptionTypeMap.values()) {
            subscriptionList.add(subscription);
        }
        //        String str = WidgetMetricsHelper.convertToJson(subscriptionList, "subscriptionList", false);
        return WidgetMetricsHelper.convertToJson(subscriptionList, "subscriptionList", true);
    }
    
    private String saveUserAccountForNotMigratedCustomer(final WebSecurityForm<CustomerEditForm> webSecurityForm,
        final UserAccount lastModifiedUserAccount, final RandomKeyMapping keyMapping) {
        final CustomerEditForm form = webSecurityForm.getWrappedObject();
        
        final Integer customerId = (Integer) keyMapping.getKey(form.getRandomCustomerId());
        final Customer customer = this.customerService.findCustomer(customerId);
        UserAccount userAccount = null;
        if (customer.isIsParent()) {
            userAccount = this.userAccountService.findAdminUserAccountByParentCustomerId(customer.getId());
        } else {
            userAccount = this.userAccountService.findAdminUserAccountByChildCustomerId(customer.getId());
        }
        
        String encodedUserName = null;
        try {
            encodedUserName = URLEncoder.encode(form.getUserName(), WebSecurityConstants.URL_ENCODING_LATIN1);
        } catch (UnsupportedEncodingException e) {
            logger.error("+++ Failed to encode user name when updating new user.", e);
        }
        
        final UserAccount otherUserAccount = this.userAccountService.findUndeletedUserAccount(encodedUserName);
        if (userAccount != null && otherUserAccount != null && otherUserAccount.getId().intValue() != userAccount.getId().intValue()) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("userName", this.customerValidator.getMessage("error.common.duplicated", new Object[] {
                this.customerValidator.getMessage("label.general.user.account"), this.customerValidator.getMessage("label.general.user.name"), })));
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        boolean changeUserAccount = true;
        if ((form.getHidePassword() != null && form.getHidePassword()) && userAccount.getUserName().equals(encodedUserName)) {
            changeUserAccount = false;
        }
        
        if (changeUserAccount) {
            
            final Date currentTime = DateUtil.getCurrentGmtDate();
            
            userAccount.setUserName(encodedUserName);
            userAccount.setLastModifiedGmt(currentTime);
            userAccount.setLastModifiedByUserId(lastModifiedUserAccount.getId());
            userAccount.setCustomer(customer);
            if (form.getHidePassword() == null || !form.getHidePassword()) {
                final String salt = RandomStringUtils.randomAlphanumeric(WebSecurityConstants.RANDOMKEY_MIN_HEX_LENGTH);
                final String encodedPassword = this.passwordEncoder.encodePassword(form.getPassword(), salt);
                userAccount.setPassword(encodedPassword);
                userAccount.setPasswordSalt(salt);
                userAccount.setIsPasswordTemporary(true);
            }
            
            this.userAccountService.updateUserAccount(userAccount);
        }
        
        return WebCoreConstants.RESPONSE_TRUE;
        
    }
    
    private UserAccount getUserAccountForNewCustomer(final CustomerEditForm form, final Date currentTime, final Integer lastModifiedByUserId) {
        /* set up new Customer. */
        final Customer customer = new Customer();
        
        String encodedUserName = null;
        try {
            encodedUserName = URLEncoder.encode(form.getUserName(), WebSecurityConstants.URL_ENCODING_LATIN1);
        } catch (UnsupportedEncodingException e) {
            logger.warn("userName could not be encoded in SystemAdminMainController.getUserAccountForNewCustomer()");
            encodedUserName = form.getUserName();
        }
        UserAccount userAccount = this.userAccountService.findUndeletedUserAccount(encodedUserName);
        if (userAccount != null) {
            return null;
        }
        userAccount = new UserAccount();
        
        /* set up new CustomerSubscription. */
        final Set<CustomerSubscription> customerSubscriptions = new HashSet<CustomerSubscription>();
        if (form.getSubscriptionTypeIds() != null) {
            final String[] subscriptions = form.getSubscriptionTypeIds().split(StandardConstants.STRING_COMMA);
            final String[] isActivatedValues = form.getIsActivatedValues().split(StandardConstants.STRING_COMMA);
            for (int i = 0; i < subscriptions.length; i++) {
                final SubscriptionType type = new SubscriptionType();
                type.setId(Integer.parseInt(subscriptions[i]));
                final boolean isActivated = Boolean.parseBoolean(isActivatedValues[i]);
                if (isActivated) {
                    customerSubscriptions.add(new CustomerSubscription(type, customer, isActivated, currentTime, lastModifiedByUserId));
                }
            }
        }
        customer.setCustomerSubscriptions(customerSubscriptions);
        userAccount.setCustomer(customer);
        return userAccount;
    }
    
    private Set<CustomerSubscription> fixCustomerSubscriptions(final Customer customer, final CustomerEditForm form, final Date currentTime,
        final int lastModifiedByUserId) {
        final Set<CustomerSubscription> customerSubscriptions = customer.getCustomerSubscriptions();
        if (form.getSubscriptionTypeIds() != null) {
            final String[] subscriptions = form.getSubscriptionTypeIds().split(StandardConstants.STRING_COMMA);
            final String[] isActivatedValues = form.getIsActivatedValues().split(StandardConstants.STRING_COMMA);
            final int[] subscriptionTypeInts = new int[subscriptions.length];
            final boolean[] isActivateds = new boolean[subscriptions.length];
            for (int i = 0; i < subscriptions.length; i++) {
                subscriptionTypeInts[i] = Integer.parseInt(subscriptions[i]);
                isActivateds[i] = Boolean.parseBoolean(isActivatedValues[i]);
            }
            //                String[] customerSubscriptionRandomIds = form.getCustomerSubscriptionRandomIds().split(",");
            final List<Customer> childCustomers = this.customerService.findAllChildCustomers(customer.getId());
            for (CustomerSubscription customerSubscription : customerSubscriptions) {
                for (int i = 0; i < subscriptions.length; i++) {
                    if (customerSubscription.getSubscriptionType().getId() == subscriptionTypeInts[i]) {
                        customerSubscription.setIsEnabled(isActivateds[i]);
                        customerSubscription.setLastModifiedGmt(currentTime);
                        customerSubscription.setLastModifiedByUserId(lastModifiedByUserId);
                        
                        for (Customer childCustomer : childCustomers) {
                            final Set<CustomerSubscription> childCustomerSubscriptions = childCustomer.getCustomerSubscriptions();
                            for (CustomerSubscription childCustomerSubscription : childCustomerSubscriptions) {
                                if (childCustomerSubscription.getSubscriptionType().getId() == subscriptionTypeInts[i]
                                    && childCustomerSubscription.getIsEnabled()) {
                                    childCustomerSubscription.setIsEnabled(isActivateds[i]);
                                    childCustomerSubscription.setLastModifiedGmt(currentTime);
                                    childCustomerSubscription.setLastModifiedByUserId(lastModifiedByUserId);
                                }
                                
                                // Disable all the non-default CustomerAlertType if the subscription get revoked. 
                                if (WebCoreConstants.SUBSCRIPTION_TYPE_ALERTS == childCustomerSubscription.getSubscriptionType().getId()) {
                                    if (!childCustomerSubscription.getIsEnabled()) {
                                        this.customerAlertTypeService
                                                .disableNonDefaultAlert(childCustomer.getId(), currentTime, lastModifiedByUserId);
                                    }
                                }
                            }
                        }
                        
                        //update license count and license used count:
                        //if mobile app is not enabled, all licenses for this app should be deleted and count = 0/0
                        //if mobile app is enabled, count the licenses and update the subscription (will only matter the first time it's enabled)
                        final MobileApplicationType mobileApplicationType = customerSubscription.getSubscriptionType().getMobileApplicationType();
                        if (mobileApplicationType != null) {
                            int count = 0;
                            int used = 0;
                            if (customerSubscription.getIsEnabled()) {
                                count = (int) this.mobileLicenseService.countTotalLicensesForCustomerAndType(customer.getId(),
                                                                                                             mobileApplicationType.getId());
                                used = (int) this.mobileLicenseService.countProvisionedLicensesForCustomerAndType(customer.getId(),
                                                                                                                  mobileApplicationType.getId());
                            } else {
                                this.mobileLicenseService.updateLicenseCount(customer.getId(), mobileApplicationType.getId(), 0);
                            }
                            this.customerSubscriptionService.updateLicenseCountAndUsedForCustomer(customer.getId(), customerSubscription
                                    .getSubscriptionType().getId(), count, used, lastModifiedByUserId);
                        }
                        
                        // Disable all the non-default CustomerAlertType if the subscription get revoked. 
                        if (WebCoreConstants.SUBSCRIPTION_TYPE_ALERTS == subscriptionTypeInts[i]) {
                            if (!customerSubscription.getIsEnabled()) {
                                this.customerAlertTypeService.disableNonDefaultAlert(customer.getId(), currentTime, lastModifiedByUserId);
                            }
                        }
                    }
                }
            }
        }
        return customerSubscriptions;
    }
    
    private UserAccount getUserAccountForExistingCustomer(final String randomCustomerId, final CustomerEditForm form, final Date currentTime,
        final Integer lastModifiedByUserId, final RandomKeyMapping keyMapping) {
        UserAccount userAccount = null;
        // Use existing Customer and userAccount
        final Integer customerId = (Integer) keyMapping.getKey(randomCustomerId);
        final Customer customer = this.customerService.findCustomer(customerId);
        if (customer.isIsParent()) {
            userAccount = this.userAccountService.findAdminUserAccountByParentCustomerId(customerId);
        } else {
            userAccount = this.userAccountService.findAdminUserAccountByChildCustomerId(customerId);
        }
        
        if (userAccount == null) {
            return null;
        }
        
        String encodedUserName = null;
        try {
            encodedUserName = URLEncoder.encode(form.getUserName(), WebSecurityConstants.URL_ENCODING_LATIN1);
        } catch (UnsupportedEncodingException e) {
            logger.warn("userName could not be encoded in SystemAdminMainController.getUserAccountForExistingCustomer()");
            encodedUserName = form.getUserName();
        }
        final UserAccount otherUserAccount = this.userAccountService.findUndeletedUserAccount(encodedUserName);
        if (otherUserAccount != null && otherUserAccount.getId().intValue() != userAccount.getId().intValue()) {
            return null;
        }
        
        final Set<CustomerSubscription> customerSubscriptions = fixCustomerSubscriptions(customer, form, currentTime, lastModifiedByUserId);
        
        customer.setCustomerSubscriptions(customerSubscriptions);
        userAccount.setCustomer(customer);
        return userAccount;
    }
    
    private void updateParentUsers(final Customer customer, final Integer originalParentId, final Date currentTime, final int lastModifiedByUserId) {
        if (originalParentId != null) {
            final List<UserAccount> userAccountList = this.userAccountService.findAdminUserAccountsForParent(customer.getId(), originalParentId);
            for (UserAccount userAccount : userAccountList) {
                final UserStatusType userStatusType = new UserStatusType();
                userStatusType.setId(WebCoreConstants.USER_STATUS_TYPE_DELETED);
                userAccount.setUserStatusType(userStatusType);
                userAccount.setArchiveDate(currentTime);
                this.userAccountService.updateUserAccount(userAccount);
            }
        }
        if (customer.getParentCustomer() != null) {
            final List<UserAccount> originalUserAccountList = this.userAccountService.findAdminUserAccountWithIsAllChilds(customer
                    .getParentCustomer().getId());
            for (UserAccount userAccount : originalUserAccountList) {
                
                UserAccount newUserAccount = this.userAccountService.findUserAccount(userAccount.getUserName() + customer.getId());
                // If the new parent customer used to be the parent of this customer before and the user wasn't disabled for some reason disable it now. 
                if (newUserAccount != null) {
                    final UserStatusType userStatusType = new UserStatusType();
                    userStatusType.setId(WebCoreConstants.USER_STATUS_TYPE_DELETED);
                    newUserAccount.setUserStatusType(userStatusType);
                    newUserAccount.setArchiveDate(currentTime);
                    this.userAccountService.updateUserAccount(newUserAccount);
                }
                newUserAccount = new UserAccount(customer, userAccount.getUserStatusType(), userAccount.getUserName() + customer.getId(),
                        userAccount.getFirstName(), userAccount.getLastName(), WebCoreConstants.NOT_USED, userAccount.getPasswordSalt(), false,
                        userAccount.getArchiveDate(), currentTime, lastModifiedByUserId);
                newUserAccount.setCustomerEmail(userAccount.getCustomerEmail());
                newUserAccount.setUserAccount(userAccount);
                newUserAccount.setIsAliasUser(true);
                final UserDefaultDashboard userDefaultDashboard = new UserDefaultDashboard(newUserAccount, true, currentTime, lastModifiedByUserId);
                newUserAccount.setUserDefaultDashboard(userDefaultDashboard);
                
                final Set<UserRole> newUserRoles = new HashSet<UserRole>();
                for (UserRole userRole : userAccount.getUserRoles()) {
                    final UserRole newUserRole = new UserRole(userRole.getRole(), newUserAccount, lastModifiedByUserId, currentTime);
                    newUserRoles.add(newUserRole);
                }
                newUserAccount.setUserRoles(newUserRoles);
                this.userAccountService.saveUserAccountAndUserRole(newUserAccount);
            }
        }
    }
    
    /**
     * This method is called from saveCustomer() and creates new Customer, CustomerProperty, CustomerRole, UserAccount, CustomerSubscription, default Location,
     * and UserRole.
     * 
     * @param form
     *            CustomerEditForm object (user input form)
     * @param lastModifiedUserAccount
     *            lastModified UserAccount
     */
    private String saveCustomerAndUserAccount(final WebSecurityForm<CustomerEditForm> webSecurityForm, final UserAccount lastModifiedUserAccount,
        final RandomKeyMapping keyMapping, final boolean isMigrated) {
        
        final CustomerEditForm form = webSecurityForm.getWrappedObject();
        
        final String randomCustomerId = form.getRandomCustomerId();
        final Integer lastModifiedByUserId = lastModifiedUserAccount.getId();
        UserAccount userAccount = null;
        Customer parentCustomer = null;
        final CustomerType customerType = new CustomerType();
        final CustomerStatusType customerStatusType = new CustomerStatusType();
        final Date currentTime = DateUtil.getCurrentGmtDate();
        final UserStatusType userStatusType = new UserStatusType();
        
        if (!isMigrated) {
            return saveUserAccountForNotMigratedCustomer(webSecurityForm, lastModifiedUserAccount, keyMapping);
        } else {
            
            customerType.setId(form.getIsParentCompany() ? WebCoreConstants.CUSTOMER_TYPE_PARENT : WebCoreConstants.CUSTOMER_TYPE_CHILD);
            Integer parentCustomerId = null;
            if (form.getRandomParentCustomerId() != null && !WebCoreConstants.EMPTY_STRING.equals(form.getRandomParentCustomerId())) {
                parentCustomerId = (Integer) keyMapping.getKey(form.getRandomParentCustomerId());
                parentCustomer = new Customer();
                parentCustomer.setId(parentCustomerId);
            }
            boolean changeUserAccount = true;
            final boolean isNewCustomer = randomCustomerId == null || WebCoreConstants.EMPTY_STRING.equals(randomCustomerId);
            if (isNewCustomer) {
                
                userAccount = getUserAccountForNewCustomer(form, currentTime, lastModifiedByUserId);
                if (userAccount == null) {
                    webSecurityForm.addErrorStatus(new FormErrorStatus("userName", this.customerValidator
                            .getMessage("error.common.duplicated", new Object[] { this.customerValidator.getMessage("label.general.user.account"),
                                this.customerValidator.getMessage("label.general.user.name"), })));
                    return WidgetMetricsHelper
                            .convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
                }
            } else {
                userAccount = getUserAccountForExistingCustomer(randomCustomerId, form, currentTime, lastModifiedByUserId, keyMapping);
                if (userAccount == null) {
                    webSecurityForm.addErrorStatus(new FormErrorStatus("userName", this.customerValidator
                            .getMessage("error.common.duplicated", new Object[] { this.customerValidator.getMessage("label.general.user.account"),
                                this.customerValidator.getMessage("label.general.user.name"), })));
                    return WidgetMetricsHelper
                            .convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
                }
            }
            final Customer customer = userAccount.getCustomer();
            String timeZone = WebCoreConstants.SERVER_TIMEZONE_GMT;
            String formTimeZone = form.getCurrentTimeZone();
            boolean formTZNullFlag = false;
            
            if (formTimeZone == null || formTimeZone.equals(WebCoreConstants.BLANK)
                || formTimeZone.equals(WebCoreConstants.CUSTOMER_TIME_ZONE_NO_SELECTION)) {
                formTZNullFlag = true;
            }
            final Integer id = customer.getId();
            CustomerProperty property = null;
            if (id != null) {
                property = this.customerAdminService
                        .getCustomerPropertyByCustomerIdAndCustomerPropertyType(id, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
                timeZone = property.getPropertyValue();
            }
            
            // If Customer id is null AND no selected time zone, use default timezone GMT.
            if (id == null && formTZNullFlag) {
                formTimeZone = WebCoreConstants.SERVER_TIMEZONE_GMT;
            } else if (id != null && timeZone != null && formTZNullFlag) {
                // If Customer is not null AND CustomerProperty - TimeZone is not null AND no selected time zone, use CustomerProperty - TimeZone.
                formTimeZone = timeZone;
            }
            
            CustomerAdminUtil.createStatusTypes(customer, customerStatusType, userStatusType, form.getAccountStatus(), form.getTrialDateString(),
                                                formTimeZone);
            
            if (property != null && !property.getPropertyValue().equals(formTimeZone)) {
                property.setPropertyValue(formTimeZone);
                property.setLastModifiedGmt(new Date());
                property.setLastModifiedByUserId(WebSecurityUtil.getUserAccount().getId());
                customerAdminService.saveOrUpdateCustomerProperty(property);
            }
            
            boolean isParentChanged = false;
            Integer originalParentId = null;
            if (customer.getParentCustomer() != null) {
                if (parentCustomerId == null || customer.getParentCustomer().getId().intValue() != parentCustomerId.intValue()) {
                    isParentChanged = true;
                    originalParentId = customer.getParentCustomer() != null ? customer.getParentCustomer().getId() : null;
                }
            } else if (parentCustomerId != null) {
                isParentChanged = true;
            }
            
            customer.setCustomerType(customerType);
            customer.setCustomerStatusType(customerStatusType);
            
            customer.setParentCustomer(parentCustomer);
            customer.setName(form.getLegalName().trim());
            customer.setIsParent(form.getIsParentCompany());
            customer.setLastModifiedGmt(currentTime);
            customer.setLastModifiedByUserId(lastModifiedByUserId);
            customer.setUnifiId(form.getUnifiId());
            
            /* set up UserAccount. */
            
            String encodedUserName = null;
            try {
                encodedUserName = URLEncoder.encode(form.getUserName(), WebSecurityConstants.URL_ENCODING_LATIN1);
            } catch (UnsupportedEncodingException e) {
                logger.error("+++ Failed to encode user name when updating new user.", e);
                encodedUserName = form.getUserName();
            }
            
            if (!isNewCustomer && (form.getHidePassword() != null && form.getHidePassword()) && userAccount.getUserName().equals(encodedUserName)
                && userAccount.getUserStatusType().getId() == userStatusType.getId()) {
                changeUserAccount = false;
            }
            
            userAccount.setUserName(encodedUserName);
            userAccount.setUserStatusType(userStatusType);
            userAccount.setLastModifiedGmt(currentTime);
            userAccount.setLastModifiedByUserId(lastModifiedByUserId);
            userAccount.setCustomer(customer);
            if (form.getHidePassword() == null || !form.getHidePassword()) {
                final String salt = RandomStringUtils.randomAlphanumeric(WebSecurityConstants.RANDOMKEY_MIN_HEX_LENGTH);
                final String encodedPassword = this.passwordEncoder.encodePassword(form.getPassword(), salt);
                userAccount.setPassword(encodedPassword);
                userAccount.setPasswordSalt(salt);
                userAccount.setIsPasswordTemporary(true);
            }
            
            if (isNewCustomer) {
                final int newCustomerId = this.systemAdminService.createCustomer(customer, userAccount, formTimeZone);
                if (newCustomerId == 0) {
                    webSecurityForm.addErrorStatus(new FormErrorStatus("newCustomerCreation", this.customerValidator
                            .getMessage("error.customer.create.fail")));
                    return WidgetMetricsHelper
                            .convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
                    
                } else {
                    return WebCoreConstants.RESPONSE_TRUE;
                }
            } else {
                boolean result = false;
                if (changeUserAccount) {
                    result = this.systemAdminService.updateCustomer(customer, userAccount);
                } else {
                    result = this.customerService.update(customer);
                }
                
                if (isParentChanged) {
                    updateParentUsers(customer, originalParentId, currentTime, lastModifiedByUserId);
                }
                
                if (!result) {
                    webSecurityForm.addErrorStatus(new FormErrorStatus("newCustomerCreation", this.customerValidator
                            .getMessage("error.customer.create.fail")));
                    return WidgetMetricsHelper
                            .convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
                } else {
                    return WebCoreConstants.RESPONSE_TRUE;
                }
            }
        }
    }
    
    private boolean canViewPayStation() {
        return WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_VIEW_PAYSTATION) || canManagePayStation();
    }
    
    private boolean canManagePayStation() {
        return WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_MANAGE_PAYSTATION)
               || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_CREATE_PAYSTATIONS)
               || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_SET_BILLING_PARAMETERS)
               || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_MOVE_PAYSTATION);
    }
}
