package com.digitalpaytech.mvc.systemadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.dto.customeradmin.CustomerDetails;
import com.digitalpaytech.mvc.PayStationPlacementController;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
//TODO remove search form   
//@SessionAttributes({ "customerSearchForm" })
public class SystemAdminPayStationPlacementController extends PayStationPlacementController {
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;

    public final void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }

    /**
     * This method creates a list of Id's of routes and locations which have pay
     * stations for the current user and puts them into the model as
     * "filterValues" to be used in the UI to filter pay stations
     * 
     * @param request
     * @param response
     * @param model
     * @return List of locations and routes stored in the model as attribute
     *         "filterValues" used for filtering pay stations. Target vm file
     *         URI which is mapped to Settings->paystationPlacement form
     *         (/settings/locations/paystationPlacement.vm)
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationPlacement.html")
    public final String placedPayStations(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_MANAGE_PAYSTATION_PLACEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        this.commonControllerHelper.insertCustomerInModelForSystemAdmin(request, model);
        
        return super.placedPayStations(request, response, model, "/systemAdmin/workspace/payStations/payStationPlacement");
    }
    
    /**
     * This method takes an optional location or route Id and creates a list of
     * pay station details. These details contain map information (longitude and
     * latitude).
     * 
     * @param request
     * @param response
     * @param model
     * @return Pay station map information WidgetMapInfo object converted into
     *         JSON
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationPlacement/payStations.html")
    @ResponseBody
    public final String payStations(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_PAYSTATION_ADMINISTRATION)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.payStations(request, response, model);
    }
    
    /**
     * This method takes a pay station Id as input as well as longitude and
     * latitude. It then updates the pay station in the database to these new
     * coordinates if they pass simple longitude and latitude math validations
     * 
     * @param request
     * @param response
     * @param model
     * @return "true" string value if process succeeds, "false" is process fails
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationPlacement/pinPayStation.html")
    @ResponseBody
    public final String pinPayStation(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_MANAGE_PAYSTATION_PLACEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.pinPayStation(request, response, model);
    }
    
}
