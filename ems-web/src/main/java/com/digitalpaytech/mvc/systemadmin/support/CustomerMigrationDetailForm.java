package com.digitalpaytech.mvc.systemadmin.support;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class CustomerMigrationDetailForm implements Serializable {
    
    private static final long serialVersionUID = -729038442753820107L;
    private String randomId;
    private String customerName;
    private int status;
    private String boardedDate;
    private String requestedDate;
    private String lastFailureDate;
    private String migrateDate;
    private int onlinePaystations;
    private int communicatedPaystations;
    private int totalPaystations;
    private int usage;
    
    private List<String> migrationContacts;
    private String scheduleDate;
    private String scheduleTime;
    private Date scheduleRealDate;
    private boolean isContacted;
    private boolean isApproved;
    
    public CustomerMigrationDetailForm() {
    }
    
    public final String getRandomId() {
        return this.randomId;
    }
    
    public final void setRandomId(final String randomId) {
        this.randomId = randomId;
    }
    
    public final String getCustomerName() {
        return this.customerName;
    }
    
    public final void setCustomerName(final String customerName) {
        this.customerName = customerName;
    }
    
    public final int getStatus() {
        return this.status;
    }
    
    public final void setStatus(final int status) {
        this.status = status;
    }
    
    public final String getBoardedDate() {
        return this.boardedDate;
    }
    
    public final void setBoardedDate(final String boardedDate) {
        this.boardedDate = boardedDate;
    }
    
    public final String getRequestedDate() {
        return this.requestedDate;
    }
    
    public final void setRequestedDate(final String requestedDate) {
        this.requestedDate = requestedDate;
    }
    
    public final String getLastFailureDate() {
        return this.lastFailureDate;
    }
    
    public final void setLastFailureDate(final String lastFailureDate) {
        this.lastFailureDate = lastFailureDate;
    }
    
    public final String getMigrateDate() {
        return this.migrateDate;
    }
    
    public final void setMigrateDate(final String migrateDate) {
        this.migrateDate = migrateDate;
    }
    
    public final int getOnlinePaystations() {
        return this.onlinePaystations;
    }
    
    public final void setOnlinePaystations(final int onlinePaystations) {
        this.onlinePaystations = onlinePaystations;
    }
    
    public final int getCommunicatedPaystations() {
        return this.communicatedPaystations;
    }
    
    public final void setCommunicatedPaystations(final int communicatedPaystations) {
        this.communicatedPaystations = communicatedPaystations;
    }
    
    public final int getTotalPaystations() {
        return this.totalPaystations;
    }
    
    public final void setTotalPaystations(final int totalPaystations) {
        this.totalPaystations = totalPaystations;
    }
    
    public final int getUsage() {
        return this.usage;
    }
    
    public final void setUsage(final int usage) {
        this.usage = usage;
    }
    
    public final List<String> getMigrationContacts() {
        return this.migrationContacts;
    }
    
    public final void setMigrationContacts(final List<String> migrationContacts) {
        this.migrationContacts = migrationContacts;
    }
    
    public final String getScheduleDate() {
        return this.scheduleDate;
    }
    
    public final void setScheduleDate(final String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }
    
    public final String getScheduleTime() {
        return this.scheduleTime;
    }
    
    public final void setScheduleTime(final String scheduleTime) {
        this.scheduleTime = scheduleTime;
    }
    
    public final Date getScheduleRealDate() {
        return this.scheduleRealDate;
    }
    
    public final void setScheduleRealDate(final Date scheduleRealDate) {
        this.scheduleRealDate = scheduleRealDate;
    }
    
    public final boolean getIsContacted() {
        return this.isContacted;
    }
    
    public final void setIsContacted(final boolean isContacted) {
        this.isContacted = isContacted;
    }
    
    public final boolean getIsApproved() {
        return this.isApproved;
    }
    
    public final void setIsApproved(final boolean isApproved) {
        this.isApproved = isApproved;
    }
    
}
