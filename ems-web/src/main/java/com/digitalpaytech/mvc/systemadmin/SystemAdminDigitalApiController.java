package com.digitalpaytech.mvc.systemadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.digitalpaytech.mvc.DigitalApiController;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
public class SystemAdminDigitalApiController extends DigitalApiController {
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    public final void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/licenses/digitalAPI.html")
    public final String digitalApiControllerIndex(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        //TODO check permissions
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LICENSES)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LICENSES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        this.commonControllerHelper.insertCustomerInModelForSystemAdmin(request, model);
        
        return super.digitalApiControllerIndex(request, response, model);
    }
}
