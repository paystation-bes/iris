package com.digitalpaytech.mvc.systemadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.mvc.ReportingController;
import com.digitalpaytech.mvc.customeradmin.support.ReportEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

/*
 * This structure is based off of the UI mock ups as of July 13
 * 
 */

@Controller
public class SystemAdminCustomerReportingController extends ReportingController {
    private static final Logger LOG = Logger.getLogger(SystemAdminCustomerReportingController.class);
    
    @ModelAttribute(FORM_EDIT)
    public final WebSecurityForm<ReportEditForm> initReportForm(final HttpServletRequest request) {
        final ReportEditForm reportForm = new ReportEditForm();
        return new WebSecurityForm<ReportEditForm>((Object) null, reportForm, SessionTool.getInstance(request).locatePostToken(FORM_EDIT));
    }
    
    /***********************************************************************************
     * ReportLists
     ***********************************************************************************/
    
    /**
     * This method will look at the permissions in the WebUser and pass control to the Reporting landing page if allowed.
     * 
     * @return Will return the page for pending/scheduled/ finished report lists or error if no permissions exist.
     */
    @RequestMapping(value = "/systemAdmin/workspace/reporting/index.html", method = RequestMethod.GET)
    public final String getReportLists(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        getCommonControllerHelper().insertCustomerInModelForSystemAdmin(request, model);
        
        return super.getReportLists(request, response, model, "/systemAdmin/workspace/reporting/index");
    }
    
    /**
     * This method will look at the permissions in the WebUser and pass control to the Reporting landing page if allowed.
     * 
     * @return Will return the page for pending/scheduled/ finished report lists or error if no permissions exist.
     */
    @RequestMapping(value = "/systemAdmin/workspace/reporting/unreadReports.html", method = RequestMethod.GET)
    public final String getUnreadReportNumber(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.getUnreadReportNumber(request, response, model);
    }
    
    /**
     * This method will look at the permissions in the WebUser and pass control to the Reporting landing page if allowed.
     * 
     * @return Will return the page for pending/scheduled/ finished report lists or error if no permissions exist.
     */
    @RequestMapping(value = "/systemAdmin/workspace/reporting/sortReportsList.html", method = RequestMethod.GET)
    public final String sortReports(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.sortReports(request, response, model, "/include/reporting/sortReportsList");
    }
    
    /**
     * This method will look at the permissions in the WebUser and pass control to the Reporting landing page if allowed.
     * 
     * @return Will return the page for pending/scheduled/ finished report lists or error if no permissions exist.
     */
    @RequestMapping(value = "/systemAdmin/workspace/reporting/sortReportDefinitionsList.html", method = RequestMethod.GET)
    public final String sortReportDefinitions(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.sortReportDefinitions(request, response, model, "/include/reporting/sortReportDefinitionsList");
    }
    
    /**
     * This method takes a reportQueue Id and modifies the status to Cancelled if the user has valid permissions. If the report is launched before the user
     * tries to cancel it response status is set to HTTP_STATUS_CODE_483.
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON object of Pending Reports and Completed Reports lists.
     */
    @RequestMapping(value = "/systemAdmin/workspace/reporting/cancel.html", method = RequestMethod.GET)
    public final String cancelQueuedReport(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.cancelQueuedReport(request, response, model, "/include/reporting/sortReportsList");
    }
    
    /**
     * This method takes a reportHistory Id (coming from Pending Reports) or reportRepository Id (coming from Completed Reports) and creates a new reportQueue
     * entry using the reportDefinition of the reportHistory item if the user has permission.
     * 
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON object of Pending Reports and Completed Reports lists.
     */
    // TODO is status or type check needed? 
    @RequestMapping(value = "/systemAdmin/workspace/reporting/rerun.html", method = RequestMethod.GET)
    public final String rerunReport(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.rerunReport(request, response, model, "/include/reporting/sortReportsList");
    }
    
    /**
     * This method takes a reportDefinition Id and creates a new reportQueue entry if the user has permission.
     * 
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON object of Pending Reports and Completed Reports lists.
     */
    @RequestMapping(value = "/systemAdmin/workspace/reporting/runNow.html", method = RequestMethod.GET)
    public final String runNow(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.runNow(request, response, model, "/include/reporting/sortReportsList");
    }
    
    /**
     * This method takes a reportRepository Id and deletes the content of the report from reportRepository and reportContent tables if the user has permission.
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON object of Pending Reports and Completed Reports lists.
     */
    // TODO is status or type check needed? 
    @RequestMapping(value = "/systemAdmin/workspace/reporting/delete.html", method = RequestMethod.GET)
    public final String deleteReport(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.deleteReport(request, response, model, "/include/reporting/sortReportsList");
    }
    
    /**
     * This method takes a reportRepository Id and deletes the content of the report from reportRepository and reportContent tables if the user has permission.
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON object of Pending Reports and Completed Reports lists.
     */
    // TODO is status or type check needed?
    @ResponseBody
    @RequestMapping(value = "/systemAdmin/workspace/reporting/viewReport.html", method = RequestMethod.GET)
    public final String viewReport(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.viewReport(request, response, model);
    }
    
    /**
     * This method takes a reportDefinition Id and changes the status to Deleted if the user has permission.
     * 
     * @param request
     * @param response
     * @param model
     * @return JSON object of Scheduled Reports list.
     */
    // TODO is status or type check needed? 
    @RequestMapping(value = "/systemAdmin/workspace/reporting/deleteDefinition.html", method = RequestMethod.GET)
    public final String deleteReportDefinition(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.deleteReportDefinition(request, response, model, "/include/reporting/sortReportDefinitionsList");
    }
    
    /***********************************************************************************
     * new/edit Report
     ***********************************************************************************/
    
    /**
     * This method will look at the permissions in the WebUser and pass control to the new Report page if allowed.
     * 
     * @return Will return the page for new reports or error if no permissions exist.
     */
    @RequestMapping(value = "/systemAdmin/workspace/reporting/newReport.html", method = RequestMethod.GET)
    public final String newReport(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_EDIT) final WebSecurityForm<ReportEditForm> secForm) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        getCommonControllerHelper().insertCustomerInModelForSystemAdmin(request, model);
        
        return super.newReport(request, response, model, secForm, "/systemAdmin/workspace/reporting/newReport");
    }
    
    /**
     * This method will look at the permissions in the WebUser and depending on the report type selected create the data needed to create the new report
     * settings if the user has permission.
     * 
     * @return Will return /reporting/formReport.vm with all filters in the model or error if no permissions exist.
     */
    @ResponseBody
    @RequestMapping(value = "/systemAdmin/workspace/reporting/formReport.html", method = RequestMethod.GET)
    public final String newReportSettings(@ModelAttribute(FORM_EDIT) final WebSecurityForm<ReportEditForm> secForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.newReportSettings(secForm, result, request, response, model);
    }
    
    /**
     * This method takes a WebSecurityForm from the model and after validating the contents of the report edit form, saves the report definition information to
     * the database.
     * 
     * @param webSecurityForm
     * @param result
     * @param request
     * @param response
     * @param model
     * @return A response of true for a successful save, JSON document for
     *         validation errors, or null for database records not found
     */
    @ResponseBody
    @RequestMapping(value = "/systemAdmin/workspace/reporting/saveReport.html", method = RequestMethod.POST)
    public final String saveReport(@ModelAttribute(FORM_EDIT) final WebSecurityForm<ReportEditForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.saveReport(webSecurityForm, result, request, response, model);
    }
    
    /**
     * This method will open the report settings page look at the permissions in the WebUser and depending on the report type selected create the data needed to
     * create the new report
     * settings if the user has permission.
     * 
     * @return Will return /reporting/editReport.vm with all filters in the model or error if no permissions exist.
     */
    @RequestMapping(value = "/systemAdmin/workspace/reporting/editReport.html", method = RequestMethod.GET)
    public final String editReportSettings(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_EDIT) final WebSecurityForm<ReportEditForm> secForm) {
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        getCommonControllerHelper().insertCustomerInModelForSystemAdmin(request, model);
        
        return super.editReportSettings(request, response, model, secForm, "/systemAdmin/workspace/reporting/newReport");
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/reporting/recreateReport.html", method = RequestMethod.GET)
    public final String recreateReportSettings(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_EDIT) final WebSecurityForm<ReportEditForm> secForm) {
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        getCommonControllerHelper().insertCustomerInModelForSystemAdmin(request, model);
        
        return super.recreateReportSettings(request, response, model, secForm, "/systemAdmin/workspace/reporting/newReport");
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/reporting/fixReport.html", method = RequestMethod.GET)
    public final String fixReportSettings(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_EDIT) final WebSecurityForm<ReportEditForm> secForm) {
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        getCommonControllerHelper().insertCustomerInModelForSystemAdmin(request, model);
        
        return super.fixReportSettings(request, response, model, secForm, "/systemAdmin/workspace/reporting/newReport");
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/reporting/deleteHistory.html", method = RequestMethod.GET)
    public final String deleteReportHistory(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        getCommonControllerHelper().insertCustomerInModelForSystemAdmin(request, model);
        
        return super.deleteReportHistory(request, response, model, "/include/reporting/sortReportsList");
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/reporting/getOrganizationFilters.html", method = RequestMethod.GET)
    @ResponseBody
    public final String getAdditionalFilters(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.getAdditionalFilters(request, response, model);
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/reporting/searchConsumers.html", method = RequestMethod.POST)
    @ResponseBody
    public final String searchConsumers(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
        }
        return super.searchConsumerForAccountName(request, response, model);
    }
    
}
