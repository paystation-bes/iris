package com.digitalpaytech.mvc.systemadmin.exception;

public class InvalidMerchantAccountException extends RuntimeException {
	/**
     * 
     */
    private static final long serialVersionUID = 878225490633682771L;
	public InvalidMerchantAccountException(String msg) {
		super(msg);
	}
	public InvalidMerchantAccountException(Throwable t) {
		super(t);
	}
	public InvalidMerchantAccountException(String msg, Throwable t) {
		super(msg, t);
	}
}
