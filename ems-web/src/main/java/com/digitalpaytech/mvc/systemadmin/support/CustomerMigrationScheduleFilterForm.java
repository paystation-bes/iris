package com.digitalpaytech.mvc.systemadmin.support;

import java.io.Serializable;

public class CustomerMigrationScheduleFilterForm implements Serializable {
    
    private static final long serialVersionUID = -3393633797665851747L;
    private boolean isBoarded;
    private boolean isRequested;
    private boolean isScheduled;
    private boolean isMigrated;
    private boolean isEMS6;
    
    private Integer page;
    private String targetId;
    private Long timeStamp;
    private String column;
    private String order;
    private Long dataKey;
    
    public CustomerMigrationScheduleFilterForm() {
        this.isBoarded = true;
        this.isRequested = true;
        this.isScheduled = true;
        this.isMigrated = false;
        this.isEMS6 = false;
    }
    
    public final boolean getIsBoarded() {
        return this.isBoarded;
    }
    
    public final void setIsBoarded(final boolean isBoarded) {
        this.isBoarded = isBoarded;
    }
    
    public final boolean getIsRequested() {
        return this.isRequested;
    }
    
    public final void setIsRequested(final boolean isRequested) {
        this.isRequested = isRequested;
    }
    
    public final boolean getIsScheduled() {
        return this.isScheduled;
    }
    
    public final void setIsScheduled(final boolean isScheduled) {
        this.isScheduled = isScheduled;
    }
    
    public final boolean getIsMigrated() {
        return this.isMigrated;
    }
    
    public final void setIsMigrated(final boolean isMigrated) {
        this.isMigrated = isMigrated;
    }
    
    public final boolean getIsEMS6() {
        return this.isEMS6;
    }
    
    public final void setIsEMS6(final boolean isEMS6) {
        this.isEMS6 = isEMS6;
    }
    
    public final Integer getPage() {
        return this.page;
    }
    
    public final void setPage(final Integer page) {
        this.page = page;
    }
    
    public final Long getTimeStamp() {
        return this.timeStamp;
    }
    
    public final void setTimeStamp(final Long timeStamp) {
        this.timeStamp = timeStamp;
    }
    
    public final String getColumn() {
        return this.column;
    }
    
    public final void setColumn(final String column) {
        this.column = column;
    }
    
    public final String getOrder() {
        return this.order;
    }
    
    public final void setOrder(final String order) {
        this.order = order;
    }
    
    public final Long getDataKey() {
        return this.dataKey;
    }
    
    public final void setDataKey(final Long dataKey) {
        this.dataKey = dataKey;
    }
    
    public final String getTargetId() {
        return this.targetId;
    }
    
    public final void setTargetId(final String targetId) {
        this.targetId = targetId;
    }
}
