package com.digitalpaytech.mvc.systemadmin;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.domain.Notification;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.NotificationDetails;
import com.digitalpaytech.dto.NotificationListInfo;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.mvc.systemadmin.support.NotificationManagementForm;
import com.digitalpaytech.service.NotificationService;
import com.digitalpaytech.util.DashboardUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObject;
import com.digitalpaytech.validation.systemadmin.NotificationManagementValidator;

/**
 * This class handles the notification management page in system admin.
 * 
 * @author Brian Kim
 * 
 */
@Controller
@SessionAttributes({ "notificationManagementForm" })
public class SystemAdminNotificationManagementController {
    
    private static Logger logger = Logger.getLogger(SystemAdminNotificationManagementController.class);
    
    private static final String FMT_DATE_TIME = WebCoreConstants.DATE_UI_FORMAT + WebCoreConstants.EMPTY_SPACE + WebCoreConstants.TIME_UI_FORMAT;
    
    @Autowired
    private NotificationService notificationService;
    
    @Autowired
    private NotificationManagementValidator notificationManagementValidator;
    
    public final void setNotificationService(final NotificationService notificationService) {
        this.notificationService = notificationService;
    }
    
    public final void setNotificationManagementValidator(final NotificationManagementValidator notificationManagementValidator) {
        this.notificationManagementValidator = notificationManagementValidator;
    }
    
    /**
     * This method shows the notification management main page.
     * 
     * @param request
     *            HttpServletRequest
     * @param response
     *            HttpServletResponse
     * @param model
     *            ModelMap
     * @return target URI (/systemAdmin/notificationManagement.vm) when process succeeds,
     *         null with response status (401) if fails.
     */
    @RequestMapping(value = "/systemAdmin/notifications/index.html", method = RequestMethod.GET)
    public final String getNotificationList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* get customer information from security framework session. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_NOTIFICATIONS)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_NOTIFICATIONS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        /* get the NotificationListInfo DTO for current and past notifications. */
        final NotificationManagementForm form = new NotificationManagementForm();
        final WebSecurityForm<NotificationManagementForm> notificationManagementForm = new WebSecurityForm<NotificationManagementForm>(null, form);
        
        final Collection<NotificationListInfo> currentNotificationInfo = this.prepareCurrentNotificationInfo(request);
        final Collection<NotificationListInfo> pastNotificationInfo = this.preparePastNotificationInfo(request);
        
        /* put DTOs in model. */
        model.put("currentNotificationInfo", currentNotificationInfo);
        model.put("pastNotificationInfo", pastNotificationInfo);
        model.put("notificationManagementForm", notificationManagementForm);
        
        return "/systemAdmin/notification/index";
    }
    
    /**
     * This method retrieves notification detail information and return back to UI.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return JSON string having user detail information. "false" in case of any exception or validation failure.
     */
    @RequestMapping(value = "/systemAdmin/notifications/notificationDetails.html", method = RequestMethod.GET)
    @ResponseBody
    public final String viewNotificationDetails(final HttpServletRequest request, final HttpServletResponse response) {
        
        /* get customer information from security framework session. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_NOTIFICATIONS)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_NOTIFICATIONS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        /* validate the randomized notificationID from request parameter. */
        final String actualNotificationId = this.validateNotificationId(request);
        if (actualNotificationId.equals(WebCoreConstants.RESPONSE_FALSE)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Integer notificationId = new Integer(actualNotificationId);
        final NotificationDetails notificationDetails = this.prepareNotificationDetailsForJSON(notificationId);
        
        /* convert NotificationDetails to JSON string. */
        final String json = WidgetMetricsHelper.convertToJson(notificationDetails, "notificationDetails", true);
        if (StringUtils.isBlank(json)) {
            logger.warn("### Blank JSON response for Notification Details. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return json;
    }
    
    /**
     * This method will be called when "Add Notification" or "Edit Notification" is clicked.
     * It will set the notification form.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return uriOnSuccess if the process succeeds, null if fails.
     * 
     */
    @RequestMapping(value = "/systemAdmin/notifications/notificationForm.html", method = RequestMethod.GET)
    public final String getNotificationForm(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* get customer information from security framework session. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_NOTIFICATIONS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final NotificationManagementForm form = new NotificationManagementForm();
        final WebSecurityForm<NotificationManagementForm> notificationManagementForm = new WebSecurityForm<NotificationManagementForm>(null, form);
        
        if (request.getParameter("notificationID") == null) {
            notificationManagementForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        } else {
            final String id = this.validateNotificationId(request);
            if (id.equals(WebCoreConstants.RESPONSE_FALSE)) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return null;
            } else {
                notificationManagementForm.setActionFlag(WebSecurityConstants.ACTION_UPDATE);
            }
        }
        
        model.put("notificationManagementForm", notificationManagementForm);
        
        return "/systemAdmin/notificationForm";
    }
    
    /**
     * This method saves notification information in Notification table.
     * 
     * @param webSecurityForm
     *            user input form
     * @param result
     *            Error object
     * @param request
     *            HttpServletRequest object
     * @return "true" string value if process succeeds, JSON error message if process fails
     */
    @RequestMapping(value = "/systemAdmin/notifications/saveNotification.html", method = RequestMethod.POST)
    @ResponseBody
    public final String saveNotification(@ModelAttribute("notificationManagementForm") final WebSecurityForm<NotificationManagementForm> webSecurityForm,
                                         final BindingResult result, final HttpServletRequest request, final HttpServletResponse response) {
        
        /* validate user account if it has proper permission to view it. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_NOTIFICATIONS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        /* validate user input. */
        this.notificationManagementValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        webSecurityForm.resetToken();
        
        final NotificationManagementForm notificationManagementForm = (NotificationManagementForm) webSecurityForm.getWrappedObject();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        /* Convert Date/Time */
        final SimpleDateFormat df = new SimpleDateFormat(FMT_DATE_TIME);
        df.setTimeZone(TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone()));
        try {
            if ((notificationManagementForm.getStartDate() != null) && (notificationManagementForm.getStartTime() != null)) {
                notificationManagementForm.setStartDateTime(df.parse(notificationManagementForm.getStartDate() + WebCoreConstants.EMPTY_SPACE
                                                                     + notificationManagementForm.getStartTime()));
            }
            
            if ((notificationManagementForm.getEndDate() != null) && (notificationManagementForm.getEndTime() != null)) {
                notificationManagementForm.setEndDateTime(df.parse(notificationManagementForm.getEndDate() + WebCoreConstants.EMPTY_SPACE
                                                                   + notificationManagementForm.getEndTime()));
            }
        } catch (ParseException pe) {
            throw new IllegalStateException("Un-expected incorrect Date/Time String!");
        }
        
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_CREATE) {
            /* Add Notification. */
            addNotification(notificationManagementForm, userAccount);
        } else if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
            /* Edit Notification. */
            editNotification(notificationManagementForm, userAccount);
        }
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    /**
     * This method deletes notification.
     * 
     * @param request
     *            HttpServletRequest object
     * @return "true" string value if process succeeds, "false" string value if process fails.
     */
    @RequestMapping(value = "/systemAdmin/notifications/deleteNotification.html", method = RequestMethod.GET)
    @ResponseBody
    public final String deleteNotification(final HttpServletRequest request, final HttpServletResponse response) {
        
        /* validate user account if it has proper permission to do it. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_NOTIFICATIONS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        /* validate the randomized notification ID from request parameter. */
        final String actualNotificationId = this.validateNotificationId(request);
        if (actualNotificationId.equals(WebCoreConstants.RESPONSE_FALSE)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Integer notificationId = new Integer(actualNotificationId);
        final Notification notification = this.notificationService.findNotificationById(notificationId, true);
        
        /* set delete flag, last modified GMT, and last modified useraccount id. */
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        notification.setIsDeleted(true);
        notification.setLastModifiedGmt(new Date());
        notification.setUserAccount(userAccount);
        
        this.notificationService.updateNotification(notification);
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    /**
     * This method prepares a list of current NotificationListInfo DTO objects to pass it to UI.
     * 
     * @param request
     *            HttpServletRequest object
     * @return collection of NotificationListInfo
     */
    private Collection<NotificationListInfo> prepareCurrentNotificationInfo(final HttpServletRequest request) {
        
        final Collection<NotificationListInfo> currentNotificationInfo = new ArrayList<NotificationListInfo>();
        final List<Notification> currentNotifications = this.notificationService.findNotificationAllCurrentAndFuture();
        final Collection<WebObject> webObjs = this.hideObjectByName(request, currentNotifications, WebCoreConstants.ID_LOOK_UP_NAME);
        
        for (WebObject obj : webObjs) {
            final Notification notification = (Notification) obj.getWrappedObject();
            currentNotificationInfo.add(new NotificationListInfo(obj.getPrimaryKey().toString(), notification.getTitle(), notification.getBeginGmt()));
        }
        
        return currentNotificationInfo;
    }
    
    /**
     * This method prepares a list of past NotificationListInfo DTO objects to pass it to UI.
     * 
     * @param request
     *            HttpServletRequest object
     * @return collection of NotificationListInfo
     */
    private Collection<NotificationListInfo> preparePastNotificationInfo(final HttpServletRequest request) {
        
        final Collection<NotificationListInfo> pastNotificationInfo = new ArrayList<NotificationListInfo>();
        final List<Notification> pastNotifications = this.notificationService.findNotificationInPast();
        final Collection<WebObject> webObjs = this.hideObjectByName(request, pastNotifications, WebCoreConstants.ID_LOOK_UP_NAME);
        
        for (WebObject obj : webObjs) {
            final Notification notification = (Notification) obj.getWrappedObject();
            pastNotificationInfo.add(new NotificationListInfo(obj.getPrimaryKey().toString(), notification.getTitle(), notification.getBeginGmt()));
        }
        
        return pastNotificationInfo;
    }
    
    /**
     * This method hides Collection with the primary key and return back.
     * 
     * @param request
     *            HttpServletRequest object
     * @param collection
     *            Collection instance
     * @param name
     *            name of the property to hide
     * @return Collection of WebObject
     */
    @SuppressWarnings("unchecked")
    private Collection<WebObject> hideObjectByName(final HttpServletRequest request, final Collection<?> collection, final String name) {
        
        /* make randomised key. */
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        return keyMapping.hideProperties(collection, name);
    }
    
    /**
     * This method validates user input randomized notification ID.
     * 
     * @param request
     *            HttpServletRequest object
     * @return actual notification ID if process succeeds, "false" string if process fails.
     */
    private String validateNotificationId(final HttpServletRequest request) {
        
        /* validate the randomized notification ID from request parameter. */
        final String randomizedNotificationId = request.getParameter("notificationID");
        if (StringUtils.isBlank(randomizedNotificationId) || !DashboardUtil.validateRandomId(randomizedNotificationId)) {
            logger.warn("### Invalid notificationID in the request. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        /* validate the actual user account ID. */
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Object actualNotificationId = keyMapping.getKey(randomizedNotificationId);
        if (actualNotificationId == null || !DashboardUtil.validateDBIntegerPrimaryKey(actualNotificationId.toString())) {
            logger.warn("### Invalid notificationID in the request. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return actualNotificationId.toString();
    }
    
    /**
     * This method returns notification details for JSON format.
     * 
     * @param notificationId
     *            Notification ID
     * @return NotificationDetails object
     */
    private NotificationDetails prepareNotificationDetailsForJSON(final Integer notificationId) {
        final String timezone = WebSecurityUtil.getCustomerTimeZone();
        
        final Notification notification = this.notificationService.findNotificationById(notificationId, false);
        final NotificationDetails details = new NotificationDetails();
        
        details.setTitle(notification.getTitle());
        details.setMessage(notification.getMessage());
        details.setMessageUrl(notification.getMessageUrl());
        
        final SimpleDateFormat fmt = new SimpleDateFormat("HH:mm");
        fmt.setTimeZone(TimeZone.getTimeZone(timezone));
        
        details.setBeginTime(fmt.format(notification.getBeginGmt()));
        details.setEndTime(fmt.format(notification.getEndGmt()));
        
        details.setBeginGmt(new Timestamp(DateUtil.convertTimeZone(timezone, WebCoreConstants.SERVER_TIMEZONE_GMT, notification.getBeginGmt()).getTime()));
        details.setEndGmt(new Timestamp(DateUtil.convertTimeZone(timezone, WebCoreConstants.SERVER_TIMEZONE_GMT, notification.getEndGmt()).getTime()));
        
        return details;
    }
    
    /**
     * This method inserts new Notification.
     * 
     * @param form
     *            User Input Form
     * @param userAccount
     *            UserAccount object
     */
    private void addNotification(final NotificationManagementForm form, final UserAccount userAccount) {
        final Notification notification = new Notification();
        notification.setTitle(form.getTitle());
        notification.setMessage(form.getMessage());
        notification.setMessageUrl(form.getLink());
        notification.setBeginGmt(form.getStartDateTime());
        notification.setEndGmt(form.getEndDateTime());
        notification.setLastModifiedGmt(new Date());
        notification.setUserAccount(userAccount);
        
        this.notificationService.saveNotification(notification);
    }
    
    /**
     * This method updates existing Notification.
     * 
     * @param form
     *            User Input Form
     * @param userAccount
     *            UserAccount object
     */
    private void editNotification(final NotificationManagementForm form, final UserAccount userAccount) {
        
        final Notification notification = this.notificationService.findNotificationById(form.getNotificationId(), true);
        notification.setTitle(form.getTitle());
        notification.setMessage(form.getMessage());
        notification.setMessageUrl(form.getLink());
        notification.setBeginGmt(form.getStartDateTime());
        notification.setEndGmt(form.getEndDateTime());
        notification.setLastModifiedGmt(new Date());
        notification.setUserAccount(userAccount);
        
        this.notificationService.updateNotification(notification);
    }
    
    /**
     * This method sorts current and future notifications.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return sortNotificationList.vm file
     */
    
    @RequestMapping(value = "/systemAdmin/notifications/sortCurrentNotificationList.html", method = RequestMethod.GET)
    public String sortCurrentNotificationList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* get the NotificationListInfo DTO for current and past notifications. */
        final Collection<NotificationListInfo> currentNotificationInfo = this.prepareCurrentNotificationInfo(request);
        
        /* put DTOs in model. */
        model.put("currentNotificationInfo", currentNotificationInfo);
        model.put("isSysAdmin", true);
        
        return "/include/sortNotificationList";
    }
    
    /**
     * This method sorts npast otifications.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return sortNotificationList.vm file
     */
    
    @RequestMapping(value = "/systemAdmin/notifications/sortPastNotificationList.html", method = RequestMethod.GET)
    public String sortPastNotificationList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        final Collection<NotificationListInfo> pastNotificationInfo = this.preparePastNotificationInfo(request);
        
        /* put DTOs in model. */
        model.put("pastNotificationInfo", pastNotificationInfo);
        model.put("isSysAdmin", true);
        
        return "/include/sortNotificationList";
    }
    
}
