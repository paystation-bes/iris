package com.digitalpaytech.mvc.systemadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.client.dto.fms.BulkSchedule;
import com.digitalpaytech.client.dto.fms.DeviceGroup;
import com.digitalpaytech.client.dto.DeviceGroupFilter;
import com.digitalpaytech.mvc.DeviceGroupController;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.DeviceGroupConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
public class SystemAdminDeviceGroupController extends DeviceGroupController {
    @RequestMapping(value = "/systemAdmin/workspace/payStations/configGroup.html", method = RequestMethod.GET)
    public final String page(@ModelAttribute(DeviceGroupConstants.FORM_EDIT) final WebSecurityForm<DeviceGroup> editForm,
                             @ModelAttribute(DeviceGroupConstants.FORM_SEARCH) final WebSecurityForm<DeviceGroupFilter> searchForm,
                             @ModelAttribute(DeviceGroupConstants.FORM_BULK_SCHEDULE) final WebSecurityForm<BulkSchedule> bulkScheduleForm,
                             final HttpServletRequest request,
                             final HttpServletResponse response, 
                             final ModelMap model) {
        String uri = null;
        if (hasAdminView()) {
            this.commonControllerHelper.insertCustomerInModelForSystemAdmin(request, model);
            uri = super.performPage(editForm, searchForm, bulkScheduleForm, request, response, model, "/systemAdmin/workspace/payStations/configGroup");
        } else {
            uri = WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS;
        }
        
        return uri;
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/payStations/configGroupList.html", method = RequestMethod.POST)
    @ResponseBody
    public final String list(@ModelAttribute(DeviceGroupConstants.FORM_SEARCH) final WebSecurityForm<DeviceGroupFilter> searchForm, 
                             final BindingResult bindingResult,
                             final HttpServletRequest request, 
                             final HttpServletResponse response, 
                             final ModelMap model) {
        String result = null;
        if (hasAdminView()) {
            result = super.performList(searchForm, bindingResult, request, response, model);
        } else {
            result = WebCoreConstants.RESPONSE_FALSE;
        }
        
        return result;
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/payStations/configGroupDetails.html", method = RequestMethod.GET)
    @ResponseBody
    public final String details(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        String result = null;
        if (hasAdminView()) {
            result = super.performDetails(request, response, model);
        } else {
            result = WebCoreConstants.RESPONSE_FALSE;
        }
        
        return result;
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/payStations/configGroupSearch.html", method = RequestMethod.GET)
    @ResponseBody
    public final String autocomplete(final HttpServletRequest request, final HttpServletResponse response) {
        String result = null;
        if (hasAdminView()) {
            result = super.autocompleteGroups(request, response);
        } else {
            result = WebCoreConstants.RESPONSE_FALSE;
        }
        return result;
    }
    
    private boolean hasAdminView() {
        return WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_VIEW_PAYSTATION_GROUPS);
    }
    
}