package com.digitalpaytech.mvc.systemadmin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantPOS;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.PosStatus;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.SettingsFile;
import com.digitalpaytech.domain.SettingsFileContent;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.MapEntry;
import com.digitalpaytech.dto.PaginatedList;
import com.digitalpaytech.dto.PayStationSelectorDTO;
import com.digitalpaytech.dto.PointOfSaleSearchCriteria;
import com.digitalpaytech.dto.PosAlertStatusDetailSummary;
import com.digitalpaytech.dto.customeradmin.CustomerDetails;
import com.digitalpaytech.dto.customeradmin.PaystationListInfo;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.mvc.PayStationListController;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.PayStationCommentFilterForm;
import com.digitalpaytech.mvc.support.PointOfSaleSearchForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.mvc.systemadmin.support.SysAdminPayStationEditForm;
import com.digitalpaytech.ribbon.ClientFactory;
import com.digitalpaytech.service.CustomerMigrationService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.LinkCardTypeService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.PaystationSettingService;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.PaystationSettingZipReader;
import com.digitalpaytech.util.support.WebObjectId;
import com.digitalpaytech.validation.systemadmin.PayStationSerialNumberValidator;
import com.digitalpaytech.validation.systemadmin.SysAdminPayStationValidator;

@Controller
@SessionAttributes({ "payStationEditForm" })
public class SystemAdminPayStationListController extends PayStationListController {
    private static final Logger LOG = Logger.getLogger(SystemAdminPayStationListController.class);
    private static final String EDIT_FORM = "payStationEditForm";
    private static final String SEARCH_FORM = "commentFilterForm";
    private static final int DATA_KEY_LENGTH = 10;
    private static final String PAYSTATION_LIST = "payStationsList";
    
    private static final String FAILED_REMOVE_TELEMETRY_OR_MITREID = "Failed to remove from Telemetry and/or Link Mitreid.";
    private static final String FAILED_UPDATE_DEVICE_LCDMS = "Failed to update device on LCDMS";
    
    @Autowired
    protected ClientFactory clientFactory;
    @Autowired
    protected SysAdminPayStationValidator sysAdminPayStationValidator;
    
    @Autowired
    protected PaystationSettingService paystationSettingService;
    
    @Autowired
    protected PayStationSerialNumberValidator payStationSerialNumberValidator;
    
    @Autowired
    private CustomerMigrationService customerMigrationService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    @Autowired
    private PosServiceStateService posServiceStateService;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private LinkCardTypeService linkCardTypeService;
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final void setCustomerMigrationService(final CustomerMigrationService customerMigrationService) {
        this.customerMigrationService = customerMigrationService;
    }
    
    public final void setMerchantAccountService(final MerchantAccountService merchantAccountService) {
        this.merchantAccountService = merchantAccountService;
    }
    
    public final void setSysAdminPayStationValidator(final SysAdminPayStationValidator sysAdminPayStationValidator) {
        this.sysAdminPayStationValidator = sysAdminPayStationValidator;
    }
    
    public final void setPaystationSettingService(final PaystationSettingService paystationSettingService) {
        this.paystationSettingService = paystationSettingService;
    }
    
    public final void setPayStationSerialNumberValidator(final PayStationSerialNumberValidator payStationSerialNumberValidator) {
        this.payStationSerialNumberValidator = payStationSerialNumberValidator;
    }
  
    public final void setMailerService(final MailerService mailerService) {
        this.mailerService = mailerService;
    }
    
    /**
     * This method creates a list of Id's of routes and locations which have pay
     * stations for the current user and puts them into the model as
     * "filterValues" to be used in the UI to filter pay stations
     * 
     * @param request
     * @param response
     * @param model
     *            "filterValues" used for filtering pay stations. Target vm file
     *            URI which is mapped to Settings->pay station list form
     *            (/settings/locations/paystationList.vm)
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationList.html")
    public final String payStationList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final SysAdminPayStationEditForm form = new SysAdminPayStationEditForm();
        final WebSecurityForm<SysAdminPayStationEditForm> webSecurityForm = new WebSecurityForm<SysAdminPayStationEditForm>(null, form);
        model.put(EDIT_FORM, webSecurityForm);
        
        final PayStationCommentFilterForm commentForm = new PayStationCommentFilterForm();
        final WebSecurityForm<PayStationCommentFilterForm> commentFilterForm = new WebSecurityForm<PayStationCommentFilterForm>(null, commentForm);
        model.put(SEARCH_FORM, commentFilterForm);
        
        insertCustomerInModel(request, model);
        
        return super.payStationList(request, response, model, "/systemAdmin/workspace/payStations/payStationList", true);
    }
    
    @ResponseBody
    @RequestMapping(value = "/systemAdmin/workspace/listPayStations.html", method = RequestMethod.GET)
    public final String listPayStations(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<PointOfSaleSearchForm> form,
        final HttpServletRequest request, final HttpServletResponse response) {
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final PointOfSaleSearchCriteria criteria =
                super.populatePosSearchCriteria(request, form.getWrappedObject(), this.commonControllerHelper.resolveCustomerId(request, response),
                                                false);
        criteria.setShowDeactivated(true);
        criteria.setShowDecommissioned(true);
        criteria.setShowHidden(true);
        
        final PaginatedList<PaystationListInfo> result = new PaginatedList<PaystationListInfo>();
        result.setElements(this.pointOfSaleService.findPaystation(criteria));
        result.setDataKey(Long.toString(criteria.getMaxUpdatedTime().getTime(), DATA_KEY_LENGTH));
        
        return WidgetMetricsHelper.convertToJson(result, PAYSTATION_LIST, true);
    }
    
    @ResponseBody
    @RequestMapping(value = "/systemAdmin/workspace/locatePageWithPayStations.html", method = RequestMethod.GET)
    public final String locatePageWithPayStation(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<PointOfSaleSearchForm> form,
        final HttpServletRequest request, final HttpServletResponse response) {
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        String resultJson = WebCoreConstants.RESPONSE_FALSE;
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final PointOfSaleSearchCriteria criteria =
                super.populatePosSearchCriteria(request, form.getWrappedObject(), this.commonControllerHelper.resolveCustomerId(request, response),
                                                false);
        criteria.setShowDeactivated(true);
        criteria.setShowDecommissioned(true);
        criteria.setShowHidden(true);
        
        int targetPage = -1;
        final Integer targetId = keyMapping.getKey(form.getWrappedObject().getTargetRandomId(), PointOfSale.class, Integer.class);
        if (targetId != null) {
            criteria.setMaxUpdatedTime(new Date());
            targetPage = this.pointOfSaleService.findPayStationPage(targetId, criteria);
        }
        
        if (targetPage > 0) {
            criteria.setPage(targetPage);
            
            final PaginatedList<PaystationListInfo> result = new PaginatedList<PaystationListInfo>();
            result.setElements(this.pointOfSaleService.findPaystation(criteria));
            result.setDataKey(Long.toString(criteria.getMaxUpdatedTime().getTime(), DATA_KEY_LENGTH));
            result.setPage(targetPage);
            
            resultJson = WidgetMetricsHelper.convertToJson(result, PAYSTATION_LIST, true);
        }
        
        return resultJson;
    }
    
    @ResponseBody
    @RequestMapping(value = "/systemAdmin/workspace/payStationSelectors.html", method = RequestMethod.GET)
    public final String payStationSelectors(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<PointOfSaleSearchForm> form,
        final HttpServletRequest request, final HttpServletResponse response) {
        if (!canView() && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final PointOfSaleSearchCriteria criteria =
                super.populatePosSearchCriteria(request, form.getWrappedObject(), this.commonControllerHelper.resolveCustomerId(request, response),
                                                false);
        criteria.setShowDeactivated(true);
        criteria.setShowDecommissioned(false);
        criteria.setShowHidden(false);
        
        final PaginatedList<PayStationSelectorDTO> result = new PaginatedList<PayStationSelectorDTO>();
        result.setElements(this.pointOfSaleService.findPayStationSelector(criteria));
        result.setDataKey(Long.toString(criteria.getMaxUpdatedTime().getTime(), DATA_KEY_LENGTH));
        
        return WidgetMetricsHelper.convertToJson(result, PAYSTATION_LIST, true);
    }
    
    /**
     * Returns PayStationEditForm populated with details using the request
     * parameter "payStationIds" and wrapped by WebSecurityForm
     * 
     * @param request
     * @param response
     * @param model
     * @return A PayStationEditForm populated with the requested payStationId's
     *         information stored in the model as attribute
     *         "payStationEditForm". Target vm file URI which is mapped to
     *         Settings->payStationForm (/settings/location/payStationForm.vm)
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/editPayStation.html")
    @ResponseBody
    public final String getPayStationEditForm(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper role to view it. */
        if (!canManage()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        Customer currentCustomer = this.customerService.findCustomer(customerId);
        
        if (!currentCustomer.isIsMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON
                   + this.sysAdminPayStationValidator.getMessage("error.customer.notMigrated.systemAdmin");
        }
        
        final String[] pointOfSaleIds = request.getParameter("payStationID").split(",");
        
        final List<Integer> actualPointOfSaleIds = new ArrayList<Integer>();
        
        final List<Integer> customerIds = new ArrayList<Integer>();
        final List<Integer> locationIds = new ArrayList<Integer>();
        final List<Integer> routeIds = new ArrayList<Integer>();
        for (String randomPointOfSaleId : pointOfSaleIds) {
            if ("0".equals(randomPointOfSaleId)) {
                customerIds.add(customerId);
                
                locationIds.clear();
                routeIds.clear();
                actualPointOfSaleIds.clear();
            } else {
                final WebObjectId buffer = keyMapping.getKeyObject(randomPointOfSaleId);
                if (buffer == null) {
                    response.setStatus(HttpStatus.NOT_FOUND.value());
                    return WebCoreConstants.RESPONSE_FALSE;
                } else if (Customer.class.isAssignableFrom(buffer.getObjectType())) {
                    customerIds.add((Integer) buffer.getId());
                } else if (Location.class.isAssignableFrom(buffer.getObjectType())) {
                    locationIds.add((Integer) buffer.getId());
                } else if (Route.class.isAssignableFrom(buffer.getObjectType())) {
                    routeIds.add((Integer) buffer.getId());
                } else if (PointOfSale.class.isAssignableFrom(buffer.getObjectType())) {
                    actualPointOfSaleIds.add((Integer) buffer.getId());
                } else {
                    response.setStatus(HttpStatus.NOT_FOUND.value());
                    return WebCoreConstants.RESPONSE_FALSE;
                }
            }
        }
        
        final SysAdminPayStationEditForm form = new SysAdminPayStationEditForm();
        currentCustomer = null;
        
        final List<Location> selectedLocation = new ArrayList<Location>();
        
        final List<PointOfSale> posList = pointOfSaleService.findAllPointOfSales(customerIds, locationIds, routeIds, actualPointOfSaleIds);
        
        for (PointOfSale pointOfSale : posList) {
            final PosStatus posStatus = pointOfSale.getPosStatus();
            if (currentCustomer == null) {
                currentCustomer = pointOfSale.getCustomer();
            }
            
            final boolean isDecommissioned = posStatus.isIsDecommissioned() != null ? posStatus.isIsDecommissioned() : false;
            final boolean active = posStatus.isIsActivated() != null ? posStatus.isIsActivated() : false;
            
            form.addPayStation(pointOfSale.getName(), keyMapping.getRandomString(pointOfSale, WebCoreConstants.ID_LOOK_UP_NAME), isDecommissioned,
                               active);
            
            if (!selectedLocation.contains(pointOfSale.getLocation())) {
                selectedLocation.add(pointOfSale.getLocation());
            }
            
            form.addLinux(pointOfSale.isIsLinux());
        }
        //if all pay stations have the same location, we can set the 'is selected' value to true
        if (selectedLocation.size() == 1) {
            form.addLocation(selectedLocation.get(0).getName(), keyMapping.getRandomString(selectedLocation.get(0), WebCoreConstants.ID_LOOK_UP_NAME),
                             true);
        }
        
        final List<Customer> customers = systemAdminService.findAllNonDPTCustomers();
        for (Customer customer : customers) {
            form.addCustomer(customer.getName(), keyMapping.getRandomString(customer, WebCoreConstants.ID_LOOK_UP_NAME), false);
        }
        
        final Integer currentSelectedCreditMerchantPosId = findCurrentlySelectedMerchantAccount(actualPointOfSaleIds, WebCoreConstants.CREDIT_CARD);
        
        final Integer currentSelectedCustomMerchantPosId = findCurrentlySelectedMerchantAccount(actualPointOfSaleIds, WebCoreConstants.VALUE_CARD);
        
        final List<MerchantAccount> merchantAccounts = this.merchantAccountService.findAllMerchantAccountsByCustomerId(currentCustomer.getId());
        
        for (MerchantAccount merchantAccount : merchantAccounts) {
            final String randomId = keyMapping.getRandomString(merchantAccount, WebCoreConstants.ID_LOOK_UP_NAME);
            final String name = merchantAccount.getUIName();
            
            if (currentSelectedCustomMerchantPosId != null && currentSelectedCustomMerchantPosId.equals(merchantAccount.getId())) {
                form.addCustomCardMerchantAccount(name, randomId, true);
            }
            
            if (currentSelectedCreditMerchantPosId != null && currentSelectedCreditMerchantPosId.equals(merchantAccount.getId())) {
                form.addCreditCardMerchantAccount(name, randomId, true);
            }
        }
        final WebSecurityForm<SysAdminPayStationEditForm> webSecurityForm = new WebSecurityForm<SysAdminPayStationEditForm>(null, form);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_UPDATE);
        form.setPostToken(webSecurityForm.getInitToken());
        model.put(EDIT_FORM, webSecurityForm);
        
        return WidgetMetricsHelper.convertToJson(form, "payStationForm", true);
    }
    
    private Integer findCurrentlySelectedMerchantAccount(final List<Integer> pointOfSaleIds, final int cardType) {
        
        Integer currentMerchantId = null;
        final List<MerchantPOS> merchantPoss = new ArrayList<MerchantPOS>();
        
        for (Integer pointOfSaleId : pointOfSaleIds) {
            merchantPoss.addAll(pointOfSaleService.findMerchPOSByPOSIdNoDeleted(pointOfSaleId));
        }
        
        if (merchantPoss == null || merchantPoss.isEmpty()) {
            return null;
        }
        
        if (pointOfSaleIds.size() != merchantPoss.size()) {
            return null;
        }
        
        for (MerchantPOS merchantPos : merchantPoss) {
            if (merchantPos.getCardType().getId() == cardType) {
                if (currentMerchantId == null || currentMerchantId.equals(merchantPos.getMerchantAccount().getId())) {
                    currentMerchantId = merchantPos.getMerchantAccount().getId();
                } else {
                    /*
                     * A null merchantId means there is no single merchant
                     * account that is selected across all selected point of
                     * sales for the selected cardType
                     */
                    currentMerchantId = null;
                    return currentMerchantId;
                }
            }
        }
        
        return currentMerchantId;
    }
    
    /**
     * Saves changes made to pay stations using the passed model attribute
     * "payStationEditForm" WebSecurityForm
     * 
     * @param webSecurityForm
     * @param result
     * @param request
     * @param response
     * @param model
     * @return "true" string value if process succeeds, JSON error message if
     *         process fails
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/savePayStation.html", method = RequestMethod.POST)
    @ResponseBody
    public final String savePayStation(@ModelAttribute(EDIT_FORM) final WebSecurityForm<SysAdminPayStationEditForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        final Date now = DateUtil.getCurrentGmtDate();
        
        if (!canManage()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final SysAdminPayStationEditForm form = webSecurityForm.getWrappedObject();
        
        /* validate user input. */
        this.sysAdminPayStationValidator.validate(webSecurityForm, result, keyMapping);
        
        // Validate "one merchant account with one pay station" for Elavon ViaConex.
        if (form.getCreditCardMerchantRealId() != null
            && WebCoreUtil.isInArray(systemAdminService.findMerchantAccountById(form.getCreditCardMerchantRealId()).getProcessor().getId(),
                                     CardProcessingConstants.ONE_MERCHANT_ACCOUNT_ONE_POS_PROCESSOR_IDS)) {
            
            boolean okFlag = false;
            final Collection<MerchantPOS> mposColl =
                    this.merchantAccountService.findMerchantPosesByMerchantAccountId(form.getCreditCardMerchantRealId());
            // If there is no MerchantPOS with isDeleted = 0 it is good.
            if (mposColl == null || mposColl.isEmpty()) {
                okFlag = true;
            } else if (mposColl.size() == 1) {
                final List<MerchantPOS> mposList = new ArrayList<MerchantPOS>(mposColl);
                final MerchantPOS mpos = mposList.get(0);
                // If it exists but the pointOfSale is the same one you are modifying, it is good as well.
                if (mpos.getPointOfSale().getId().intValue() == form.getPointOfSaleRealId().intValue()) {
                    okFlag = true;
                }
            }
            
            // If MerchantPOS exists for another pointOfSale throw the error.
            if (!okFlag) {
                webSecurityForm.addErrorStatus(new FormErrorStatus("customerRandomId",
                        this.sysAdminPayStationValidator.getMessage("error.paystation.add.fail.merchantAccountInUse")));
            }
        }
        
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        final Customer customer = this.customerService.findCustomer(form.getCustomerRealId());
        
        if (!this.sysAdminPayStationValidator.validateSystemAdminMigrationStatus(webSecurityForm, customer)) {
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        webSecurityForm.resetToken();
        
        boolean moveCustomer = false;
        Customer newCustomer = null;
        if (form.getMoveCustomerRealId() != null) {
            newCustomer = customerAdminService.findCustomerByCustomerId(form.getMoveCustomerRealId());
        }
        
        if (newCustomer != null) {
            if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_MOVE_PAYSTATION)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            moveCustomer = true;
        }
        
        if (moveCustomer) {
            final UserAccount userAccount = WebSecurityUtil.getUserAccount();
            final Boolean isLinux = form.getIsLinux();
            
            if (form.getPayStations().size() > 1) {
                if (hasChangeToLinuxInTheBatch(keyMapping, form)) {
                    return errorMessage(webSecurityForm.getInitToken(), "error.paystation.move.and.upgrade.linux.same.time");
                }
                
                for (String randomId : form.getPayStationRandomIds()) {
                    final Integer pointOfSaleId = (Integer) keyMapping.getKey(randomId);
                    
                    resetAlerts(false, pointOfSaleId, userAccount, true);
                    try {
                        this.systemAdminService.movePointOfSale(pointOfSaleId, customer, newCustomer, userAccount, isLinux);
                        unassignTerminalTokenToLinkPOS(customer.getUnifiId(), pointOfSaleId);
                        
                    } catch (InvalidDataException ide) {
                        return logAndReturnInvalidDataException(webSecurityForm, ide);
                    }
                }
            } else {
                final PointOfSale pointOfSale = pointOfSaleService.findPointOfSaleById(form.getPointOfSaleRealId());
                
                if (isChangeToLinux(pointOfSale, isLinux)) {
                    return errorMessage(webSecurityForm.getInitToken(), "error.paystation.move.and.upgrade.linux.same.time");
                }
                
                resetAlerts(false, form.getPointOfSaleRealId(), userAccount, true);
                
                try {
                    this.systemAdminService.movePointOfSale(form.getPointOfSaleRealId(), customer, newCustomer, userAccount, isLinux);
                    unassignTerminalTokenToLinkPOS(customer.getUnifiId(), form.getPointOfSaleRealId());
                    
                } catch (InvalidDataException ide) {
                    return logAndReturnInvalidDataException(webSecurityForm, ide);
                }
            }
        } else {
            
            MerchantAccount creditCardMerchantAccount = null;
            MerchantAccount customCardMerchantAccount = null;
            Location location = null;
            
            if (form.getCreditCardMerchantRealId() != null) {
                creditCardMerchantAccount = systemAdminService.findMerchantAccountById(form.getCreditCardMerchantRealId());
            }
            
            if (form.getCustomCardMerchantRealId() != null) {
                customCardMerchantAccount = systemAdminService.findMerchantAccountById(form.getCustomCardMerchantRealId());
            }
            
            if (form.getLocationRealId() != null) {
                location = this.locationService.findLocationById(form.getLocationRealId());
            }
            
            if (form.getPayStations().size() > 1) {
                for (Integer pointOfSaleId : form.getPayStationRealIds()) {
                    
                    final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
                    
                    try {
                        this.systemAdminService.addOrUpdateMitreIdAndTelemetryAndLCDMS(pointOfSale, customer, form.getIsLinux(),
                                                                                       pointOfSale.getPosStatus().isIsActivated(),
                                                                                       WebCoreConstants.LINK_ACTION_TYPE_ADD);
                    } catch (InvalidDataException ide) {
                        return logAndReturnInvalidDataException(webSecurityForm, ide);
                    }
                    
                    if (form.getUpdateLocations()) {
                        if (location != null) {
                            pointOfSale.setLocation(location);
                        }
                    }
                    
                    if (form.getUpdateCreditMerchant()) {
                        saveOrUpdateMerchantPoss(creditCardMerchantAccount, WebCoreConstants.CREDIT_CARD, pointOfSale,
                                                 form.isDeleteCreditCardMerchAcct());
                    }
                    if (form.getUpdateCustomMerchant()) {
                        saveOrUpdateMerchantPoss(customCardMerchantAccount, WebCoreConstants.VALUE_CARD, pointOfSale,
                                                 form.isDeleteCustomCardMerchAcct());
                    }
                    pointOfSale.setCustomer(customer);
                    
                    customerAdminService.saveOrUpdatePointOfSale(pointOfSale);
                }
            } else {
                final Integer pointOfSaleId = form.getPointOfSaleRealId();
                
                final PointOfSale pointOfSale = pointOfSaleService.findPointOfSaleById(pointOfSaleId);
                final PosStatus pointOfSaleStatus = pointOfSaleService.findPointOfSaleStatusByPOSId(pointOfSaleId, false);
                final Paystation payStation = pointOfSaleService.findPayStationByPointOfSaleId(pointOfSaleId);
                
                location = locationService.findLocationById((Integer) keyMapping.getKey(form.getLocationRandomId()));
                
                final String pointOfSaleName = form.getName();
                final String pointOfSaleSerialNumber = form.getSerialNumber();
                
                final Boolean isVisible = !form.getHidden();
                final Boolean isActive = form.getActive();
                final Boolean isBillable = form.getBillable();
                final Boolean isBundled = form.getBundledData();
                final Boolean isLinux = form.getIsLinux();
                Boolean isCommentAdded = false;
                boolean recreatedBySerialNumberChange = false;
                final boolean isChangeToLinux = isChangeToLinux(pointOfSale, isLinux);
                final boolean isChangeName = !pointOfSaleName.equals(pointOfSale.getName());
                final boolean isChangeSerialNumber = !pointOfSaleSerialNumber.equals(pointOfSale.getSerialNumber());
                final boolean isChangeActive = pointOfSaleStatus.isIsActivated() != isActive;
                
                final UserAccount userAccount = WebSecurityUtil.getUserAccount();
                final String actionType =
                        (!pointOfSale.isIsLinux()) ? WebCoreConstants.LINK_ACTION_TYPE_ADD : WebCoreConstants.LINK_ACTION_TYPE_UPDATE;
                
                final String oldSerialNumber = pointOfSale.getSerialNumber();
                final String newSerialNumber = pointOfSaleSerialNumber;
                
                pointOfSale.setName(pointOfSaleName);
                pointOfSale.setSerialNumber(pointOfSaleSerialNumber);
                setPaystationType(payStation, pointOfSaleSerialNumber);
                
                if (isChangeSerialNumber) {
                    try {
                        recreatedBySerialNumberChange =
                                this.systemAdminService.recreateMitreIdAndTelemetryAndLCDMS(oldSerialNumber, newSerialNumber, pointOfSale,
                                                                                            pointOfSale.getCustomer(), isActive);
                    } catch (InvalidDataException ide) {
                        final String errorMsgFailedRemoveTelemetryMitreId =
                                "PointOfSale with different serial number. " + FAILED_REMOVE_TELEMETRY_OR_MITREID;
                        LOG.error(errorMsgFailedRemoveTelemetryMitreId, ide);
                        return errorMessage(webSecurityForm.getInitToken(), errorMsgFailedRemoveTelemetryMitreId);
                    }
                }
                
                if (!recreatedBySerialNumberChange && (isChangeActive || (!isChangeActive && isChangeToLinux))) {
                    try {
                        this.systemAdminService.addOrUpdateMitreIdAndTelemetryAndLCDMS(pointOfSale, customer, isLinux, isActive, actionType);
                    } catch (InvalidDataException ide) {
                        return logAndReturnInvalidDataException(webSecurityForm, ide);
                    }
                } else if (!recreatedBySerialNumberChange && isChangeName) {
                    try {
                        this.systemAdminService.updateDeviceInLCDMS(pointOfSale, pointOfSale.getCustomer(), false);
                    } catch (InvalidDataException ide) {
                        final String errorMsgFailedUpdateLCDMS = "PointOfSale with different name. " + FAILED_UPDATE_DEVICE_LCDMS;
                        LOG.error(errorMsgFailedUpdateLCDMS, ide);
                        return errorMessage(webSecurityForm.getInitToken(), errorMsgFailedUpdateLCDMS);
                    }
                }
                
                saveOrUpdateMerchantPoss(creditCardMerchantAccount, WebCoreConstants.CREDIT_CARD, pointOfSale, form.isDeleteCreditCardMerchAcct());
                
                saveOrUpdateMerchantPoss(customCardMerchantAccount, WebCoreConstants.VALUE_CARD, pointOfSale, form.isDeleteCustomCardMerchAcct());
                
                if (pointOfSaleStatus.isIsDigitalConnect() != isBundled || pointOfSaleStatus.isIsBillableMonthlyOnActivation() != isBillable
                    || pointOfSaleStatus.isIsActivated() != isActive || pointOfSaleStatus.isIsVisible() != isVisible) {
                    
                    if (pointOfSaleStatus.isIsDigitalConnect() != isBundled) {
                        pointOfSaleStatus.setIsDigitalConnect(isBundled);
                        pointOfSaleStatus.setIsDigitalConnectGmt(now);
                        super.insertNewPosDate(pointOfSale, WebCoreConstants.POS_DATE_TYPE_DIGITAL_CONNECT, userAccount, isBundled, null);
                    }
                    if (pointOfSaleStatus.isIsBillableMonthlyOnActivation() != isBillable) {
                        pointOfSaleStatus.setIsBillableMonthlyOnActivation(isBillable);
                        pointOfSaleStatus.setIsBillableMonthlyOnActivationGmt(now);
                        super.insertNewPosDate(pointOfSale, WebCoreConstants.POS_DATE_TYPE_BILLABLE_MONTHLY_ON_ACTIVATION, userAccount, isBillable,
                                               null);
                    }
                    if (pointOfSaleStatus.isIsActivated() != isActive) {
                        pointOfSaleStatus.setIsActivated(isActive);
                        pointOfSaleStatus.setIsActivatedGmt(now);
                        super.insertNewPosDate(pointOfSale, WebCoreConstants.POS_DATE_TYPE_ACTIVATED, userAccount, isActive, form.getComment());
                        
                        resetAlerts(isActive, pointOfSale.getId(), userAccount, false);
                        isCommentAdded = true;
                    }
                    if (pointOfSaleStatus.isIsVisible() != isVisible) {
                        pointOfSaleStatus.setIsVisible(isVisible);
                        pointOfSaleStatus.setIsVisibleGmt(now);
                        super.insertNewPosDate(pointOfSale, WebCoreConstants.POS_DATE_TYPE_VISIBLE, userAccount, isVisible, null);
                    }
                    if (!pointOfSaleStatus.isIsBillableMonthlyOnActivation()) {
                        if (isActive && isBillable) {
                            pointOfSaleStatus.setIsBillableMonthlyOnActivation(true);
                            pointOfSaleStatus.setIsBillableMonthlyOnActivationGmt(now);
                            super.insertNewPosDate(pointOfSale, WebCoreConstants.POS_DATE_TYPE_BILLABLE_MONTHLY_ON_ACTIVATION, userAccount, true,
                                                   null);
                        }
                    } else {
                        if (!isActive || !isBillable) {
                            pointOfSaleStatus.setIsBillableMonthlyOnActivation(false);
                            pointOfSaleStatus.setIsBillableMonthlyOnActivationGmt(now);
                            super.insertNewPosDate(pointOfSale, WebCoreConstants.POS_DATE_TYPE_BILLABLE_MONTHLY_ON_ACTIVATION, userAccount, false,
                                                   null);
                        }
                    }
                    pointOfSaleStatus.setLastModifiedByUserId(userAccount.getId());
                    pointOfSaleStatus.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
                    customerAdminService.saveOrUpdatePointOfSaleStatus(pointOfSaleStatus);
                }
                
                pointOfSale.setName(pointOfSaleName);
                pointOfSale.setCustomer(customer);
                pointOfSale.setLocation(location);
                
                if (!isCommentAdded && form.getComment() != null && !form.getComment().equals(WebCoreConstants.EMPTY_STRING)) {
                    super.insertNewPosDate(pointOfSale, WebCoreConstants.POS_DATE_TYPE_COMMENT_ADDED, userAccount, false, form.getComment());
                }
                
                customerAdminService.saveOrUpdatePointOfSale(pointOfSale);
                customerAdminService.saveOrUpdatePaystation(payStation);
            }
        }
        return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(WebCoreConstants.COLON).append(webSecurityForm.getInitToken()).toString();
    }

    private void unassignTerminalTokenToLinkPOS(final Integer unifiId, final Integer pointOfSaleId) {
        final String serialNum = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId).getSerialNumber();
        final List<Integer> cardTypeIds = Arrays.asList(new Integer[] { WebCoreConstants.CREDIT_CARD, WebCoreConstants.VALUE_CARD });
        
        for (Integer cardTypeId : cardTypeIds) {
            this.linkCardTypeService.pushPosTerminalAssignments(unifiId, serialNum, this.systemAdminService.findCardTypeById(cardTypeId), null);
        }
    }
    
    private boolean hasChangeToLinuxInTheBatch(final RandomKeyMapping keyMapping, final SysAdminPayStationEditForm form) {
        final Boolean isLinux = form.getIsLinux();
        
        for (String randomId : form.getPayStationRandomIds()) {
            final Integer pointOfSaleId = (Integer) keyMapping.getKey(randomId);    
            final PointOfSale pointOfSale = pointOfSaleService.findPointOfSaleById(pointOfSaleId);
            
            if (isChangeToLinux(pointOfSale, isLinux) ) {
                return true;
            }                                
        }
        return false;
    }
    
    private boolean isChangeToLinux(final PointOfSale pointOfSale, final Boolean isLinux) {
        return BooleanUtils.isNotTrue(pointOfSale.isIsLinux()) && BooleanUtils.isTrue(isLinux);
    }
    
    private void setPaystationType(final Paystation payStation, final String pointOfSaleSerialNumber) {
        if (pointOfSaleSerialNumber.matches(WebSecurityConstants.SERIAL_NUMBER_INTELLAPAY_REGEX)) {
            payStation.setPaystationType(pointOfSaleService.findPayStationTypeById(WebCoreConstants.PAY_STATION_TYPE_TEST_INTELLEPAY));
        } else if (pointOfSaleSerialNumber.matches(WebSecurityConstants.SERIAL_NUMBER_VIRTUAL_REGEX)) {
            payStation.setPaystationType(pointOfSaleService.findPayStationTypeById(WebCoreConstants.PAY_STATION_TYPE_TEST_VIRTUAL));
        } else if (pointOfSaleSerialNumber.matches(WebSecurityConstants.SERIAL_NUMBER_DPT_INTERNAL_REGEX)) {
            payStation.setPaystationType(pointOfSaleService.findPayStationTypeById(WebCoreConstants.PAY_STATION_TYPE_TEST_DEMO));
        } else if (pointOfSaleSerialNumber.matches(WebSecurityConstants.SERIAL_NUMBER_LUKE_REGEX)) {
            payStation.setPaystationType(pointOfSaleService.findPayStationTypeById(WebCoreConstants.PAY_STATION_TYPE_LUKE));
        } else if (pointOfSaleSerialNumber.matches(WebSecurityConstants.SERIAL_NUMBER_LUKE_RADIUS_REGEX)) {
            payStation.setPaystationType(pointOfSaleService.findPayStationTypeById(WebCoreConstants.PAY_STATION_TYPE_LUKE_RADIUS));
        } else if (pointOfSaleSerialNumber.matches(WebSecurityConstants.SERIAL_NUMBER_SHELBY_REGEX)) {
            payStation.setPaystationType(pointOfSaleService.findPayStationTypeById(WebCoreConstants.PAY_STATION_TYPE_SHELBY));
        } else if (pointOfSaleSerialNumber.matches(WebSecurityConstants.SERIAL_NUMBER_LUKE_2_REGEX)) {
            payStation.setPaystationType(pointOfSaleService.findPayStationTypeById(WebCoreConstants.PAY_STATION_TYPE_LUKE_2));
        } else if (pointOfSaleSerialNumber.matches(WebSecurityConstants.SERIAL_NUMBER_LUKE_B_REGEX)) {
            payStation.setPaystationType(pointOfSaleService.findPayStationTypeById(WebCoreConstants.PAY_STATION_TYPE_LUKE_B));
        } else {
            payStation.setPaystationType(pointOfSaleService.findPayStationTypeById(WebCoreConstants.PAY_STATION_TYPE_NA));
        }
    }
    
    private void saveOrUpdateMerchantPoss(final MerchantAccount cardTypeMerchantAccount, final int cardTypeId, final PointOfSale pointOfSale,
        final boolean deleteAcct) {
        final int pointOfSaleId = pointOfSale.getId();
        final MerchantAccount ma = this.merchantAccountService.findByMerchantPOSPointOfSaleIdAndCardTypeId(pointOfSaleId, cardTypeId);
        final List<Integer> posIdParam = Arrays.asList(pointOfSaleId);
        final CardType cardType = systemAdminService.findCardTypeById(cardTypeId);
        // Check if user selected anything other than 'choose'
        if (cardTypeMerchantAccount != null) {
            // If user did select something, check if the pay station is already assigned to a merchant account
            if (ma == null) {
                // If the pay station is not assigned to anything
                createAndSaveMerchantPOS(cardTypeMerchantAccount, pointOfSaleId, cardType);
            } else {
                // If the user didn't choose the same merchant account that was already assigned to the pay station
                if (!cardTypeMerchantAccount.getId().equals(ma.getId())) {
                    final MerchantPOS merchantPos = this.merchantAccountService
                            .findMerchantPosesByMerchantAccountIdAndCardTypeIdAndPOSId(ma.getId(), cardTypeId, pointOfSaleId);
                    merchantPos.setMerchantAccount(cardTypeMerchantAccount);
                    if (ma.getProcessor().getIsEMV()) {
                        this.posServiceStateService.checkforEMVMerchantAccount(cardTypeMerchantAccount, posIdParam);
                        final Date now = DateUtil.getCurrentGmtDate();
                        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
                        merchantPos.setLastModifiedGmt(now);
                        merchantPos.setLastModifiedByUserId(userAccount.getId());
                        systemAdminService.saveOrUpdateMerchantPos(merchantPos);
                    } else {
                        doUpdateSaveChecks(merchantPos, cardTypeMerchantAccount, cardTypeId);
                    }
                    
                }
            }
        } else if (deleteAcct && ma != null) {
            final MerchantPOS merchantPos =
                    this.merchantAccountService.findMerchantPosesByMerchantAccountIdAndCardTypeIdAndPOSId(ma.getId(), cardTypeId, pointOfSaleId);
            this.posServiceStateService.checkforEMVMerchantAccount(null, posIdParam);
            merchantPos.setIsDeleted(true);
            final Date now = DateUtil.getCurrentGmtDate();
            final UserAccount userAccount = WebSecurityUtil.getUserAccount();
            merchantPos.setLastModifiedGmt(now);
            merchantPos.setLastModifiedByUserId(userAccount.getId());
            systemAdminService.saveOrUpdateMerchantPos(merchantPos);
        }
        
        // Push data to Link.
        if (CardProcessingUtil.isNotNullAndIsLink(cardTypeMerchantAccount)) {
            final UUID terminalToken =
                    cardTypeMerchantAccount.getTerminalToken() == null ? null : UUID.fromString(cardTypeMerchantAccount.getTerminalToken());
            
            this.linkCardTypeService.pushPosTerminalAssignments(this.customerService.findCustomer(pointOfSale
                                                                                                  .getCustomer().getId()).getUnifiId(),
                                                                pointOfSale.getSerialNumber(), cardType, terminalToken);
        } else if (CardProcessingUtil.isNotNullAndIsLink(ma) 
                || pointOfSale.isIsLinux()) {
            // When previous MA is link or Linux PS has non-Link MA, send with null terminal token
            this.linkCardTypeService.pushPosTerminalAssignments(this.customerService.findCustomer(pointOfSale
                                                                                                  .getCustomer().getId()).getUnifiId(),
                                                                pointOfSale.getSerialNumber(), cardType, null);
        }
    }
  
    private void doUpdateSaveChecks(final MerchantPOS merchantPos, final MerchantAccount merchantAccount, final int cardTypeId) {
        final Date now = DateUtil.getCurrentGmtDate();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final List<Integer> posIdParam = Arrays.asList(merchantPos.getPointOfSale().getId());
        merchantPos.setLastModifiedGmt(now);
        merchantPos.setLastModifiedByUserId(userAccount.getId());
        if (cardTypeId == WebCoreConstants.CREDIT_CARD && merchantAccount != null) {
            this.posServiceStateService.checkforEMVMerchantAccount(merchantAccount, posIdParam);
        }
        systemAdminService.saveOrUpdateMerchantPos(merchantPos);
    }
    
    private void createAndSaveMerchantPOS(final MerchantAccount merchantAccount, final Integer pointOfSaleId, final CardType cardType) {
        final int cardTypeId = cardType.getId();
        final MerchantAccount ma = this.merchantAccountService.findByDeletedMerchantPOSPointOfSaleIdAndCardTypeId(pointOfSaleId, cardTypeId);
        final List<Integer> posIdParam = Arrays.asList(pointOfSaleId);
        MerchantPOS merchantPos = null;
        if (ma != null) {
            merchantPos =
                    this.merchantAccountService.findMerchantPosesByMerchantAccountIdAndCardTypeIdAndPOSId(ma.getId(), cardTypeId, pointOfSaleId);
        } else {
            merchantPos = new MerchantPOS();
        }
        final PointOfSale pointOfSale = pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        merchantPos.setIsDeleted(false);
        merchantPos.setPointOfSale(pointOfSale);
        merchantPos.setCardType(cardType);
        merchantPos.setMerchantAccount(merchantAccount);
        merchantPos.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        if (cardTypeId == WebCoreConstants.CREDIT_CARD) {
            this.posServiceStateService.checkforEMVMerchantAccount(merchantAccount, posIdParam);
        }
        systemAdminService.saveOrUpdateMerchantPos(merchantPos);
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/payStations/addPayStationForm.html")
    @ResponseBody
    public final String addPayStationForm(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate user account if it has proper role to view it. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_CREATE_PAYSTATIONS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        final Customer currentCustomer = this.customerService.findCustomer(customerId);
        
        if (!currentCustomer.isIsMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON
                   + this.sysAdminPayStationValidator.getMessage("error.customer.notMigrated.systemAdmin");
        }
        
        final SysAdminPayStationEditForm form = new SysAdminPayStationEditForm();
        final WebSecurityForm<SysAdminPayStationEditForm> webSecurityForm = new WebSecurityForm<SysAdminPayStationEditForm>(null, form);
        model.put(EDIT_FORM, webSecurityForm);
        
        return WebCoreConstants.RESPONSE_TRUE + WebCoreConstants.COLON + webSecurityForm.getInitToken();
    }
    
    /**
     * This method looks at the "serialNumbers" attribute of the passed
     * SysAdminPayStationEditForm and after validation, calls the stored
     * procedure sp_InsertPointOfSale to create new pay station and point of
     * sale records
     * 
     * @param webSecurityForm
     * @param result
     * @param request
     * @param response
     * @param model
     * @return
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/addPayStation.html", method = RequestMethod.POST)
    @ResponseBody
    public final String addPayStation(@ModelAttribute(EDIT_FORM) final WebSecurityForm<SysAdminPayStationEditForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_CREATE_PAYSTATIONS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        final Customer currentCustomer = this.customerService.findCustomer(customerId);
        
        if (!this.sysAdminPayStationValidator.validateSystemAdminMigrationStatus(webSecurityForm, currentCustomer)) {
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        /* validate user input. */
        this.payStationSerialNumberValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        webSecurityForm.resetToken();
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final SysAdminPayStationEditForm form = webSecurityForm.getWrappedObject();
        
        final List<String> failedSerialNumbers = new ArrayList<String>(3);
        final List<String> duplicatedSerialNumbers = new ArrayList<String>(3);
        final List<String> unknownPaystationTypeSerialNumbers = new ArrayList<String>(3);
        
        int saved = 0;
        final String[] serialNumberArray = form.getSerialNumbers().get(0).split("\n");
        for (String serialNumber : serialNumberArray) {
            serialNumber = serialNumber.trim();
            if (WebCoreConstants.EMPTY_STRING.equals(serialNumber)) {
                continue;
            }
            final List<PointOfSale> duplicates = this.pointOfSaleService.findPointOfSaleBySerialNumberNoConditions(serialNumber);
            if (duplicates.size() > 0) {
                duplicatedSerialNumbers.add(serialNumber);
            } else {
                final int reply = systemAdminService.createPointOfSale(serialNumber, customerId, userAccount);
                if (reply == WebCoreConstants.PAY_STATION_CREATION_ERROR_GENERAL) {
                    failedSerialNumbers.add(serialNumber);
                } else if (reply == WebCoreConstants.PAY_STATION_CREATION_ERROR_UNKNOWN_TYPE) {
                    unknownPaystationTypeSerialNumbers.add(serialNumber);
                } else {
                    ++saved;
                    checkForExistingSettingsFile(reply, customerId, serialNumber);
                }
            }
        }
        
        final MessageInfo message = new MessageInfo(webSecurityForm.getInitToken());
        if (!failedSerialNumbers.isEmpty()) {
            message.addMessage(this.sysAdminPayStationValidator.getMessage("error.paystation.add.fail.serial", failedSerialNumbers.toString()));
            message.setError(true);
        }
        if (!duplicatedSerialNumbers.isEmpty()) {
            message.addMessage(this.sysAdminPayStationValidator.getMessage("error.paystation.add.duplicate.serial",
                                                                           duplicatedSerialNumbers.toString()));
            message.setError(true);
        }
        if (!unknownPaystationTypeSerialNumbers.isEmpty()) {
            message.addMessage(this.sysAdminPayStationValidator.getMessage("error.paystation.add.unknowntype.serial",
                                                                           unknownPaystationTypeSerialNumbers.toString()));
            message.setError(true);
        }
        if (message.isError() && saved > 0) {
            message.prependMessage(this.sysAdminPayStationValidator.getMessage("error.common.bulkupdate.success.with.fail",
                                                                               this.sysAdminPayStationValidator.getMessage("label.payStation")));
            message.setError(false);
        }
        
        if (message.getMessages().size() > 0) {
            message.setToken(webSecurityForm.getInitToken().toString());
            return WidgetMetricsHelper.convertToJson(message, "messages", true);
        } else if (!webSecurityForm.getValidationErrorInfo().getErrorStatus().isEmpty()) {
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        } else {
            return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(WebCoreConstants.COLON).append(webSecurityForm.getInitToken()).toString();
        }
    }
    
    private void checkForExistingSettingsFile(final Integer posId, final Integer customerId, final String serialNumber) {
        final List<SettingsFile> settingsList = this.paystationSettingService.findSettingsFileByCustomerIdOrderByUploadGmt(customerId);
        if (settingsList != null) {
            for (SettingsFile settingsFile : settingsList) {
                final SettingsFileContent fileContent = this.paystationSettingService.findSettingsFileContentBySettingsFileId(settingsFile.getId());
                if (fileContent != null) {
                    final PaystationSettingZipReader zipReader = new PaystationSettingZipReader(fileContent.getContent());
                    if (zipReader.isSerialNumberInFile(serialNumber)) {
                        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(posId);
                        pointOfSale.setSettingsFile(settingsFile);
                        this.pointOfSaleService.update(pointOfSale);
                        final PosServiceState pss = this.pointOfSaleService.findPosServiceStateByPointOfSaleId(pointOfSale.getId());
                        pss.setPaystationSettingName(settingsFile.getName());
                        pss.setIsNewPaystationSetting(true);
                        pss.setSettingsFile(settingsFile);
                        this.pointOfSaleService.updatePosServicState(pss);
                        break;
                    }
                }
            }
        }
    }
    
    /**
     * This method takes a pay station id as input and returns map information
     * and payStationDetails.vm
     * 
     * @param request
     * @param response
     * @param model
     * @return A MapEntry object with the requested payStationId's coordinates
     *         in the model as attribute "mapInfo". Target vm file URI which is
     *         mapped to Settings->payStationDetails
     *         (/settings/location/payStationDetails.vm)
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/payStationDetails.html")
    @ResponseBody
    public final String payStationDetailPage(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strPointOfSaleId = request.getParameter("payStationID");
        final Integer pointOfSaleId =
                verifyRandomIdAndReturnActual(response, keyMapping, strPointOfSaleId,
                                              "+++ Invalid payStationID in PayStationListController.payStationDetailPage() method. +++");
        if (pointOfSaleId == null) {
            return null;
        }
        
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        if (pointOfSale == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Paystation payStation = this.pointOfSaleService.findPayStationByPointOfSaleId(pointOfSale.getId());
        
        int criticalCount = 0;
        int majorCount = 0;
        int minorCount = 0;
        int severity = 0;
        
        final PosAlertStatusDetailSummary pasds =
                posAlertStatusService.findPosAlertDetailSummaryByPointOfSaleId(pointOfSale.getId()).get(pointOfSale.getId());
        if (pasds != null) {
            criticalCount = pasds.getTotalCritical();
            majorCount = pasds.getTotalMajor();
            minorCount = pasds.getTotalMinor();
            severity = pasds.getSeverity();
        }
        
        final MapEntry mapInfo = new MapEntry(payStation.getPaystationType().getId(), pointOfSale.getName(), pointOfSale.getLatitude(),
                pointOfSale.getLongitude(), severity, null, null, 0, null, null, null, null);
        
        final Location location = pointOfSale.getLocation();
        location.setRandomId(keyMapping.getRandomString(location, WebCoreConstants.ID_LOOK_UP_NAME));
        final PosStatus pointOfSaleStatus = this.pointOfSaleService.findPointOfSaleStatusByPOSId(pointOfSale.getId(), true);
        final List<MerchantPOS> merchantPosList = this.pointOfSaleService.findMerchPOSByPOSIdNoDeleted(pointOfSaleId);
        MerchantAccount creditCardMerchantAccount = null;
        MerchantAccount customCardMerchantAccount = null;
        
        for (MerchantPOS merchantPos : merchantPosList) {
            if (WebCoreConstants.CREDIT_CARD == merchantPos.getCardType().getId()) {
                creditCardMerchantAccount = merchantPos.getMerchantAccount();
                creditCardMerchantAccount.setRandomId(keyMapping.getRandomString(creditCardMerchantAccount, WebCoreConstants.ID_LOOK_UP_NAME));
            }
            if (WebCoreConstants.VALUE_CARD == merchantPos.getCardType().getId()) {
                customCardMerchantAccount = merchantPos.getMerchantAccount();
                customCardMerchantAccount.setRandomId(keyMapping.getRandomString(customCardMerchantAccount, WebCoreConstants.ID_LOOK_UP_NAME));
            }
        }
        
        final SysAdminPayStationEditForm payStationEditForm = new SysAdminPayStationEditForm();
        payStationEditForm.setRandomId(keyMapping.getRandomString(pointOfSale, WebCoreConstants.ID_LOOK_UP_NAME));
        payStationEditForm.setName(pointOfSale.getName());
        payStationEditForm.setSerialNumber(pointOfSale.getSerialNumber());
        if (creditCardMerchantAccount != null) {
            payStationEditForm.setCreditCardMerchantRandomId(creditCardMerchantAccount.getRandomId());
            payStationEditForm.setCreditCardMerchantName(creditCardMerchantAccount.getUIName());
        }
        if (customCardMerchantAccount != null) {
            payStationEditForm.setCustomCardMerchantRandomId(customCardMerchantAccount.getRandomId());
            payStationEditForm.setCustomCardMerchantName(customCardMerchantAccount.getName());
        }
        payStationEditForm.setLocationRandomId(location.getRandomId());
        payStationEditForm.setLocationName(location.getName());
        payStationEditForm.setCustomerRandomId(keyMapping.getRandomString(pointOfSale.getCustomer(), WebCoreConstants.ID_LOOK_UP_NAME));
        payStationEditForm.setBillable(pointOfSaleStatus.isIsBillableMonthlyOnActivation());
        payStationEditForm.setBundledData(pointOfSaleStatus.isIsDigitalConnect());
        payStationEditForm.setActive(pointOfSaleStatus.isIsActivated());
        payStationEditForm.setHidden(!pointOfSaleStatus.isIsVisible());
        payStationEditForm.setParentLocationName(location.getLocation() == null ? "" : location.getLocation().getName());
        payStationEditForm.setMapInfo(mapInfo);
        payStationEditForm.setIsDecommissioned(pointOfSaleStatus.isIsDecommissioned());
        payStationEditForm.setAlertCount(criticalCount + majorCount + minorCount);
        payStationEditForm.setAlertSeverity(criticalCount > 0 ? WebCoreConstants.SEVERITY_CRITICAL : majorCount > 0 ? WebCoreConstants.SEVERITY_MAJOR
                : minorCount > 0 ? WebCoreConstants.SEVERITY_MINOR : WebCoreConstants.SEVERITY_CLEAR);
        
        final String timeZone = WebSecurityUtil.getCustomerTimeZone();
        // Last seen
        final PosHeartbeat heartbeat = pointOfSaleService.findPosHeartbeatByPointOfSaleId(pointOfSaleId);
        if (heartbeat != null) {
            payStationEditForm.setLastSeen(heartbeat.getLastHeartbeatGmt() == null ? ""
                    : DateUtil.getRelativeTimeString(heartbeat.getLastHeartbeatGmt(), timeZone));
            if (heartbeat.getLastHeartbeatGmt() != null && !heartbeat.getLastHeartbeatGmt().equals(pointOfSaleStatus.getIsProvisionedGmt())) {
                payStationEditForm.setIsSerialNumberFixed(true);
            }
        }
        
        if (pointOfSale.isIsLinux() != null && pointOfSale.isIsLinux().booleanValue()) {
            payStationEditForm.setIsLinux(Boolean.TRUE);
        } else {
            payStationEditForm.setIsLinux(Boolean.FALSE);
        }
        
        final WebSecurityForm<SysAdminPayStationEditForm> webSecurityForm = new WebSecurityForm<SysAdminPayStationEditForm>(null, payStationEditForm);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_UPDATE);
        payStationEditForm.setPostToken(webSecurityForm.getInitToken());
        model.put(EDIT_FORM, webSecurityForm);
        
        return WidgetMetricsHelper.convertToJson(payStationEditForm, "payStationDetails", true);
    }
    
    private void insertCustomerInModel(final HttpServletRequest request, final ModelMap model) {
        final String custRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer customerId = (Integer) keyMapping.getKey(custRandomId);
        
        final Customer customer = this.customerService.findCustomer(customerId);
        customer.setRandomId(keyMapping.getRandomString(customer, WebCoreConstants.ID_LOOK_UP_NAME));
        
        final List<MerchantAccount> ccMerchantAccts = this.merchantAccountService.findMerchantAccounts(customerId, false);
        final List<MerchantAccount> vcMerchantAccts = this.merchantAccountService.findMerchantAccounts(customerId, true);
        
        for (MerchantAccount merchantAccount : ccMerchantAccts) {
            merchantAccount.setRandomId(keyMapping.getRandomString(merchantAccount, WebCoreConstants.ID_LOOK_UP_NAME));
        }
        for (MerchantAccount merchantAccount : vcMerchantAccts) {
            merchantAccount.setRandomId(keyMapping.getRandomString(merchantAccount, WebCoreConstants.ID_LOOK_UP_NAME));
        }
        
        final String timeZone = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customer.getId(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE)
                .getPropertyValue();
        final CustomerDetails customerDetails =
                new CustomerDetails(customer, null, null, null, null, null, null, null, null, timeZone == null || !timeZone.startsWith("US/"));
        customerDetails.setValueCardMerchantAccounts(this.merchantAccountService.populateMigrationInfo(vcMerchantAccts));
        customerDetails.setCreditCardMerchantAccounts(this.merchantAccountService.populateMigrationInfo(ccMerchantAccts));
        model.put("customerDetails", customerDetails);
        
        final CustomerMigration customerMigration = this.customerMigrationService.findCustomerMigrationByCustomerId(customerId);
        if (customerMigration != null) {
            this.commonControllerHelper.setModelObjectsForMigrationLight(customerMigration, model);
        }
        
    }
    
    private boolean canView() {
        return WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_VIEW_PAYSTATION) || canManage();
    }
    
    private boolean canManage() {
        return WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_MANAGE_PAYSTATION)
               || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_CREATE_PAYSTATIONS)
               || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_SET_BILLING_PARAMETERS)
               || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_MOVE_PAYSTATION);
    }
    
    private String errorMessage(final String token, final String messageKey) {
        final String message = this.commonControllerHelper.getMessage(messageKey);
        return WidgetMetricsHelper.convertToJson(createMessageInfo(token, message, true), WebCoreConstants.UI_MESSAGES, true);
    }
    
    private String errorMessage(final String token, final String messageKey, final String message) {
        return WidgetMetricsHelper.convertToJson(createMessageInfo(token, message, true), WebCoreConstants.UI_MESSAGES, true);
    }
    
    private MessageInfo createMessageInfo(final String token, final String message, final boolean isError) {
        final MessageInfo messageInfo = new MessageInfo();
        messageInfo.setError(isError);
        messageInfo.setToken(token);
        messageInfo.addMessage(message);
        return messageInfo;
    }
    
    private String logAndReturnInvalidDataException(final WebSecurityForm<SysAdminPayStationEditForm> webSecurityForm,
        final InvalidDataException ide) {
        LOG.error(ide.getMessage(), ide);
        return errorMessage(webSecurityForm.getInitToken(), null, ide.getMessage());
    }
}
