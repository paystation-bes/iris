package com.digitalpaytech.mvc.systemadmin;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.domain.CustomerMigrationEmail;
import com.digitalpaytech.domain.CustomerMigrationValidationStatus;
import com.digitalpaytech.domain.CustomerMigrationValidationType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.customermigration.MigrationValidationInfo;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.mvc.systemadmin.support.CustomerMigrationDetailForm;
import com.digitalpaytech.service.CustomerMigrationService;
import com.digitalpaytech.service.CustomerMigrationStatusTypeService;
import com.digitalpaytech.service.CustomerMigrationValidationStatusService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.LabelConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.systemadmin.CustomerMigrationDetailValidator;

/**
 * This controller handles back-end logics to display system admin customer
 * migration page.
 * 
 * @author Brian Kim
 * 
 */
@Controller
@SessionAttributes({ "migrationDetailForm" })
public class SystemAdminMigrationDetailController implements MessageSourceAware {
    private static final String FORM_DETAIL_MIGRATION = "migrationDetailForm";
    private static final String CUSTOMER_MIGRATION = "customerMigration";
    private static final Logger LOGGER = Logger.getLogger(SystemAdminMigrationDetailController.class);
    
    @Autowired
    protected CustomerMigrationService customerMigrationService;
    
    @Autowired
    protected CustomerMigrationValidationStatusService customerMigrationValidationStatusService;
    
    @Autowired
    private CustomerMigrationStatusTypeService customerMigrationStatusTypeService;
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    @Autowired
    private CustomerMigrationDetailValidator customerMigrationDetailValidator;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    public final void setCustomerMigrationService(final CustomerMigrationService customerMigrationService) {
        this.customerMigrationService = customerMigrationService;
    }
    
    public final void setCustomerMigrationValidationStatusService(
        final CustomerMigrationValidationStatusService customerMigrationValidationStatusService) {
        this.customerMigrationValidationStatusService = customerMigrationValidationStatusService;
    }
    
    public final void setCustomerMigrationStatusTypeService(final CustomerMigrationStatusTypeService customerMigrationStatusTypeService) {
        this.customerMigrationStatusTypeService = customerMigrationStatusTypeService;
    }
    
    public final void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }
    
    public final void setCustomerMigrationDetailValidator(final CustomerMigrationDetailValidator customerMigrationDetailValidator) {
        this.customerMigrationDetailValidator = customerMigrationDetailValidator;
    }
    
    public final void setMailerService(final MailerService mailerService) {
        this.mailerService = mailerService;
    }
    
    @Override
    public final void setMessageSource(final MessageSource messageSource) {
        this.messageSource = messageSource;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    @RequestMapping(value = "/systemAdmin/migration/customerDetails.html", method = RequestMethod.GET)
    public final String getMigrationCustomerDetails(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_DETAIL_MIGRATION) final WebSecurityForm<CustomerMigrationDetailForm> webSecurityForm) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_CUSTOMER_MIGRATION_STATUS)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_CUSTOMER_MIGRATION_STATUS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        
        final CustomerMigration customerMigration = this.customerMigrationService.findCustomerMigrationByCustomerId(customerId);
        
        webSecurityForm.setWrappedObject(createCustomerMigrationDetailForm(request, customerMigration));
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_UPDATE);
        
        model.put(FORM_DETAIL_MIGRATION, webSecurityForm);
        
        return "/systemAdmin/migration/customerDetails";
    }
    
    @RequestMapping(value = "/systemAdmin/migration/migrationTaskStatus.html", method = RequestMethod.GET)
    public final String getMigrationTaskStatus(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_CUSTOMER_MIGRATION_STATUS)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_CUSTOMER_MIGRATION_STATUS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        
        final List<CustomerMigrationValidationStatus> validationStatusList = this.customerMigrationValidationStatusService
                .findInMigrationValidationsByCustomerId(customerId, false);
        
        model.put("validationList", getMigrationValidationInfoList(validationStatusList));
        
        return "/systemAdmin/migration/include/migrationTaskList";
    }
    
    @RequestMapping(value = "/systemAdmin/migration/saveCustomerDetails.html", method = RequestMethod.POST)
    @ResponseBody
    public final String saveCustomerDetails(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_DETAIL_MIGRATION) final WebSecurityForm<CustomerMigrationDetailForm> webSecurityForm, final BindingResult result) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_CUSTOMER_MIGRATION_STATUS)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_CUSTOMER_MIGRATION_STATUS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        this.customerMigrationDetailValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        /* store user migration schedule. */
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final CustomerMigrationDetailForm form = webSecurityForm.getWrappedObject();
        
        try {
            final List<String> emailAddressList = new ArrayList<String>();
            try {
                for (String email : form.getMigrationContacts()) {
                    emailAddressList.add(URLEncoder.encode(email, WebSecurityConstants.URL_ENCODING_LATIN1));
                }
                form.setMigrationContacts(emailAddressList);
            } catch (UnsupportedEncodingException uee) {
                return WidgetMetricsHelper.convertToJson(new FormErrorStatus(CUSTOMER_MIGRATION, this.messageSource
                                                                 .getMessage(LabelConstants.ERROR_CUSTOMER_MIGRATION_FAIL, null,
                                                                             LocaleContextHolder.getLocale())),
                                                         WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
                
            }
            
            //TODO CustomerMigration fix
            final CustomerMigration customerMigration = saveCustomerMigration(request, userAccount, form);
            if (customerMigration == null) {
                return WidgetMetricsHelper.convertToJson(new FormErrorStatus(CUSTOMER_MIGRATION, this.messageSource
                                                                 .getMessage(LabelConstants.ERROR_CUSTOMER_MIGRATION_FAIL, null,
                                                                             LocaleContextHolder.getLocale())),
                                                         WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
            } else if (customerMigration.getResendScheduleEmail()) {
                // send emails to IT
                final String itEmailAddress = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.CUSTOMER_MIGRATION_ALERT_EMAIL,
                                                                                         EmsPropertiesService.CUSTOMER_MIGRATION_ALERT_EMAIL_DEFAULT);
                
                this.mailerService.sendMessage(itEmailAddress, this.messageSource
                                                       .getMessage("customermigration.email.admin.migrationscheduled.title",
                                                                   new Object[] { customerMigration.getCustomer().getId() },
                                                                   LocaleContextHolder.getLocale()), this.messageSource
                                                       .getMessage("customermigration.email.admin.migrationscheduled.content", new Object[] {
                                                                       customerMigration.getCustomer().getId(),
                                                                       customerMigration.getCustomer().getName(), },
                                                                   LocaleContextHolder.getLocale()),
                                               null);
                
                // send email to Customer
                final StringBuilder emailAddresses = new StringBuilder();
                for (String email : form.getMigrationContacts()) {
                    emailAddresses.append(email);
                    emailAddresses.append(StandardConstants.STRING_COMMA);
                }
                
                this.mailerService.sendMessage(URLDecoder.decode(emailAddresses.toString(), WebSecurityConstants.URL_ENCODING_LATIN1),
                                               this.messageSource.getMessage("customermigration.email.customer.migrationscheduled.title",
                                                                             new Object[] { customerMigration.getCustomer().getName() },
                                                                             LocaleContextHolder.getLocale()), this.messageSource
                                                       .getMessage("customermigration.email.customer.migrationscheduled.content",
                                                                   new Object[] { customerMigration.getCustomer().getName() },
                                                                   LocaleContextHolder.getLocale()), null);
                
            }
            return WebCoreConstants.RESPONSE_TRUE;
        } catch (UnsupportedEncodingException uee) {
            LOGGER.error("\r\nException occurred in scheduleMigration, ", uee);
            
            return WidgetMetricsHelper.convertToJson(new FormErrorStatus(CUSTOMER_MIGRATION, this.messageSource
                                                             .getMessage(LabelConstants.ERROR_CUSTOMER_MIGRATION_FAIL, null,
                                                                         LocaleContextHolder.getLocale())), WebCoreConstants.JSON_ERROR_STATUS_LABEL,
                                                     true);
        }
    }
    
    private CustomerMigrationDetailForm createCustomerMigrationDetailForm(final HttpServletRequest request, final CustomerMigration customerMigration) {
        
        final CustomerMigrationDetailForm form = new CustomerMigrationDetailForm();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        form.setStatus(this.customerMigrationService.getMigrationStatusInt(customerMigration.getCustomerMigrationStatusType().getId()));
        form.setOnlinePaystations(customerMigration.getTotalOnlinePaystationCount());
        form.setCommunicatedPaystations(customerMigration.getCommunicatedPaystationCount());
        form.setTotalPaystations(customerMigration.getTotalPaystationCount());
        
        form.setRandomId(keyMapping.getRandomString(customerMigration.getCustomer(), WebCoreConstants.ID_LOOK_UP_NAME));
        form.setCustomerName(customerMigration.getCustomer().getName());
        
        form.setUsage(customerMigration.getTotalUserLoggedMinutes());
        
        form.setIsApproved(customerMigration.getCustomerMigrationStatusType().getId() >= WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SCHEDULED
                           && customerMigration.getCustomerMigrationStatusType().getId() < WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_BOARDED
                           && customerMigration.getCustomerMigrationStatusType().getId() != WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SUSPENDED
                           && customerMigration.getCustomerMigrationStatusType().getId() != WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_CANCELLED);
        form.setIsContacted(customerMigration.isIsContacted());
        
        setDateValues(form, customerMigration);
        
        form.setMigrationContacts(getMigrationEmailList(customerMigration));
        
        return form;
    }
    
    private void setDateValues(final CustomerMigrationDetailForm form, final CustomerMigration customerMigration) {
        form.setBoardedDate(customerMigration.getCustomerBoardedGmt() == null ? WebCoreConstants.N_A_STRING : DateUtil
                .createUIDateOnlyString(customerMigration.getCustomerBoardedGmt(), WebCoreConstants.SYSTEM_ADMIN_TIMEZONE));
        form.setRequestedDate(customerMigration.getMigrationRequestedGmt() == null ? WebCoreConstants.N_A_STRING : DateUtil
                .createUIDateOnlyString(customerMigration.getMigrationRequestedGmt(), WebCoreConstants.SYSTEM_ADMIN_TIMEZONE));
        form.setMigrateDate(customerMigration.getMigrationEndGmt() == null ? WebCoreConstants.N_A_STRING : DateUtil
                .createUIDateOnlyString(customerMigration.getMigrationEndGmt(), WebCoreConstants.SYSTEM_ADMIN_TIMEZONE));
        form.setLastFailureDate(customerMigration.getMigrationCancelGmt() == null ? WebCoreConstants.N_A_STRING : DateUtil
                .createUIDateOnlyString(customerMigration.getMigrationCancelGmt(), WebCoreConstants.SYSTEM_ADMIN_TIMEZONE));
        
        final DateFormat dateFmt = new SimpleDateFormat(WebCoreConstants.DATE_UI_FORMAT);
        dateFmt.setTimeZone(WebCoreConstants.SYSTEM_ADMIN_TIMEZONE);
        
        final DateFormat timeFmt = new SimpleDateFormat(DateUtil.TIME_UI_FORMAT);
        timeFmt.setTimeZone(WebCoreConstants.SYSTEM_ADMIN_TIMEZONE);
        
        if (customerMigration.getMigrationScheduledGmt() != null) {
            form.setScheduleDate(DateUtil.format(customerMigration.getMigrationScheduledGmt(), DateUtil.DATE_UI_FORMAT,
                                                 WebCoreConstants.SYSTEM_ADMIN_TIMEZONE));
            form.setScheduleTime(DateUtil.format(customerMigration.getMigrationScheduledGmt(), DateUtil.TIME_UI_FORMAT,
                                                 WebCoreConstants.SYSTEM_ADMIN_TIMEZONE));
        }
    }
    
    private List<String> getMigrationEmailList(final CustomerMigration customerMigration) {
        final Set<CustomerMigrationEmail> customerMigrationEmails = customerMigration.getCustomerMigrationEmails();
        final List<String> migrationEmailList = new ArrayList<String>(customerMigrationEmails.size());
        
        for (CustomerMigrationEmail customerMigrationEmail : customerMigrationEmails) {
            try {
                migrationEmailList.add(URLDecoder.decode(customerMigrationEmail.getCustomerEmail().getEmail(),
                                                         WebSecurityConstants.URL_ENCODING_LATIN1));
            } catch (UnsupportedEncodingException uee) {
                migrationEmailList.add(customerMigrationEmail.getCustomerEmail().getEmail());
            }
        }
        
        return migrationEmailList;
    }
    
    private List<MigrationValidationInfo> getMigrationValidationInfoList(final List<CustomerMigrationValidationStatus> validationStatusList) {
        List<MigrationValidationInfo> validationList = null;
        if (validationStatusList != null && !validationStatusList.isEmpty()) {
            validationList = new ArrayList<MigrationValidationInfo>(validationStatusList.size());
            for (CustomerMigrationValidationStatus validationStatus : validationStatusList) {
                final CustomerMigrationValidationType validationType = validationStatus.getCustomerMigrationValidationType();
                final MigrationValidationInfo validation = new MigrationValidationInfo();
                validation.setName(this.commonControllerHelper.getMessage(validationType.getName()));
                validation.setWarning(this.commonControllerHelper.getMessage(validationType.getWarning()));
                validation.setUrl(this.commonControllerHelper.getMessage(validationType.getUrl()));
                validation.setIsBlocking(validationType.isIsBlocking());
                validation.setIsPassed(validationStatus.isIsPassed());
                validationList.add(validation);
            }
        }
        return validationList;
    }
    
    /**
     * This method saves customer migration user input.
     * 
     * @param form
     *            User Input Form
     * @param userAccount
     *            logged-in UserAccount object
     * @return CustomerMigration.
     */
    private CustomerMigration saveCustomerMigration(final HttpServletRequest request, final UserAccount userAccount,
        final CustomerMigrationDetailForm form) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer customerId = (Integer) keyMapping.getKey(form.getRandomId());
        
        final Date currentTime = DateUtil.getCurrentGmtDate();
        final CustomerMigration customerMigration = this.customerMigrationService.findCustomerMigrationByCustomerId(customerId);
        
        if (!form.getScheduleRealDate().equals(customerMigration.getMigrationScheduledGmt())) {
            customerMigration.setResendScheduleEmail(true);
        }
        
        customerMigration.setMigrationScheduledGmt(form.getScheduleRealDate());
        
        if (customerMigration.getCustomerMigrationStatusType().getId() != WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SCHEDULED
            && form.getIsApproved()) {
            customerMigration.setCustomerMigrationStatusType(this.customerMigrationStatusTypeService
                    .findCustomerMigrationStatusType(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SCHEDULED));
            customerMigration.setResendScheduleEmail(true);
        } else if (customerMigration.getCustomerMigrationStatusType().getId() != WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_REQUESTED
                   && !form.getIsApproved()) {
            customerMigration.setCustomerMigrationStatusType(this.customerMigrationStatusTypeService
                    .findCustomerMigrationStatusType(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_REQUESTED));
            customerMigration.setResendScheduleEmail(false);
        } else {
            customerMigration.setResendScheduleEmail(false);
        }
        customerMigration.setIsContacted(form.getIsContacted());
        
        customerMigration.setLastModifiedGmt(currentTime);
        customerMigration.setLastModifiedByUserId(userAccount.getId().intValue());
        
        final Integer id = (Integer) this.customerMigrationService.updateCustomerMigration(customerMigration, form.getMigrationContacts());
        
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("saveCustomerMigration, id: " + id);
        }
        return id == null ? null : customerMigration;
    }
    
    @ModelAttribute(FORM_DETAIL_MIGRATION)
    public final WebSecurityForm<CustomerMigrationDetailForm> initMigrationDetailForm(final HttpServletRequest request) {
        return new WebSecurityForm<CustomerMigrationDetailForm>((Object) null, new CustomerMigrationDetailForm(), SessionTool.getInstance(request)
                .locatePostToken(FORM_DETAIL_MIGRATION));
    }
    
}
