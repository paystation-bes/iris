package com.digitalpaytech.mvc.systemadmin;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerMobileDevice;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.MobileApplicationType;
import com.digitalpaytech.domain.MobileLicense;
import com.digitalpaytech.domain.SubscriptionType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.MobileAppInfo;
import com.digitalpaytech.dto.MobileLicenseListInfo;
import com.digitalpaytech.dto.MobileSubscriptionInfo;
import com.digitalpaytech.dto.customeradmin.CustomerDetails;
import com.digitalpaytech.dto.customeradmin.MobileDeviceInfo;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.mvc.systemadmin.support.MobileLicenseEditForm;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.CustomerSubscriptionService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.MobileLicenseService;
import com.digitalpaytech.service.SubscriptionTypeService;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObjectId;
import com.digitalpaytech.validation.systemadmin.MobileLicenseEditValidator;

@Controller
public class SystemAdminMobileLicenseController {
    // As described in EMS-4919 to meet requirements in 
    // IREQ-19: "The ability for a system admin to manage digital collect licenses for a customer"
    
    public static final String FILTER_LICENSETYPE = "mobileLicenseFilter";
    public static final String FORM_EDIT_LICENSE = "mobileLicenseEditForm";
    
    @Autowired
    private MobileLicenseEditValidator mobileLicenseEditValidator;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private CustomerSubscriptionService customerSubscriptionService;
    
    @Autowired
    private MobileLicenseService mobileLicenseService;
    
    @Autowired
    private SubscriptionTypeService subscriptionTypeService;
    
    @Autowired
    private CustomerAdminService customerAdminService;

    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    public final void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }

    
    @RequestMapping(value = "/systemAdmin/workspace/licenses/mobileLicenses.html")
    public final String licenseIndex(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_EDIT_LICENSE) final WebSecurityForm<MobileLicenseEditForm> webSecurityForm) {
        //"/systemAdmin/workspace/licenses/mobileLicenses.html" maps to 
        //WEB-INF\templates\systemAdmin\workspace\licenses\mobileLicenses.vm 
        //This page we need an object that contains the List of Mobile Apps with Licenses with their Name, a label and 
        //a random ID. The label needs to match the logic we used for widget Types and other elements so that the UI 
        //can use this to identify what APP was chosen as the label does not change but the random ID exists for 
        //communication back to the Business layer for security. 
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_MOBILEAPP)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_MOBILEAPP)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        final String customerRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer customerId = this.verifyRandomIdAndReturnActual(response, keyMapping, customerRandomId,
            "Invalid customer random Id in SystemAdminMobileLicenseController.licenseIndex()");
        this.commonControllerHelper.insertCustomerInModelForSystemAdmin(request, model);
        model.put(FILTER_LICENSETYPE, initFilter(customerId, keyMapping));
        model.put(FORM_EDIT_LICENSE, webSecurityForm);
        return "/systemAdmin/workspace/licenses/mobileLicenses";
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/licenses/viewMobileLicenseDetails.html")
    public final String getMobileLicenseDetails(final HttpServletRequest request, final HttpServletResponse response) {
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_MOBILEAPP)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_MOBILEAPP)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        final String appRandomId = request.getParameter("appID");
        final String customerRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer appId = this.verifyRandomIdAndReturnActual(response, keyMapping, appRandomId,
            "Invalid app Id in SystemAdminMobileLicenseController.getMobileLicenseDetails()");
        final Integer customerId = this.verifyRandomIdAndReturnActual(response, keyMapping, customerRandomId,
            "Invalid customer Id in SystemAdminMobileLicenseController.getMobileLicenseDetails()");
        final int licenseUsed = (int) this.mobileLicenseService.countProvisionedLicensesForCustomerAndType(customerId, appId);
        final int licenseCount = (int) this.mobileLicenseService.countTotalLicensesForCustomerAndType(customerId, appId);
        final List<MobileLicense> keys = this.mobileLicenseService.findProvisionedMobileLicenseByCustomerAndApplication(customerId, appId);
        
        final MobileSubscriptionInfo info = new MobileSubscriptionInfo();
        info.setLicenseCount(licenseCount);
        info.setLicenseUsed(licenseUsed);
        CustomerMobileDevice customerDevice;
        MobileDeviceInfo mobileDeviceInfo;
        for (MobileLicense key : keys) {
            customerDevice = key.getCustomerMobileDevice();
            if (customerDevice != null) {
                mobileDeviceInfo = new MobileDeviceInfo();
                mobileDeviceInfo.setRandomId(new WebObjectId(CustomerMobileDevice.class, customerDevice.getId()).toString());
                mobileDeviceInfo.setDescription(customerDevice.getDescription());
                mobileDeviceInfo.setName(customerDevice.getName());
                info.addDevice(mobileDeviceInfo);
            }
        }
        return WidgetMetricsHelper.convertToJson(info, "licenseDetails", true);
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/licenses/mobileLicenseList.html")
    @ResponseBody
    public final String getMobileLicenseList(final HttpServletRequest request, final HttpServletResponse response) {
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_MOBILEAPP)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_MOBILEAPP)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        final String subTypeRandomId = request.getParameter("appID");
        final String customerRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer customerId = this.verifyRandomIdAndReturnActual(response, keyMapping, customerRandomId,
            "Invalid customer random Id in SystemAdminMobileLicenseController.getMobileLicenseList()");
        Integer subId = null;
        Integer appId = null;
        if (subTypeRandomId != null) {
            subId = this.verifyRandomIdAndReturnActual(response, keyMapping, subTypeRandomId,
                "Invalid appType random Id in SystemAdminMobileLicenseController.getMobileLicenseList()");
        }
        final SubscriptionType subscriptionType = this.subscriptionTypeService.findById(subId);
        final MobileApplicationType appType = subscriptionType.getMobileApplicationType();
        appId = appType.getId();
        final MobileLicenseListInfo list = new MobileLicenseListInfo();
        list.setMobileLicenseInfos(this.mobileLicenseService.findMobileLicenseInfoForCustomerAndApplication(customerId, appId,
            WebSecurityUtil.getCustomerTimeZone()));
        list.setLicenseCount((int) this.mobileLicenseService.countTotalLicensesForCustomerAndType(customerId, appId));
        return WidgetMetricsHelper.convertToJson(list, "licenses", true);
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/licenses/saveLicenseCount.html", method = RequestMethod.POST)
    @ResponseBody
    public final String saveLicenseCount(@ModelAttribute(FORM_EDIT_LICENSE) final WebSecurityForm<MobileLicenseEditForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        //"/systemAdmin/workspace/licenses/saveLicenseCount.html" a form submission to send the count of licenses a 
        //customer will be billed for for the specified APP supplied by the randomID for the APP type.
        
        UserAccount userAccount = WebSecurityUtil.getUserAccount();
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_MOBILEAPP)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        this.mobileLicenseEditValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), "errorStatus", true);
        }
        final MobileLicenseEditForm form = webSecurityForm.getWrappedObject();
        
        final Integer customerId = form.getCustomerRealId();
        final Integer subId = form.getAppRealId();
        final Integer count = form.getLicenseCount();
        int appId = 0;
        final SubscriptionType subscriptionType = this.subscriptionTypeService.findById(subId);
        appId = subscriptionType.getMobileApplicationType().getId();
        this.mobileLicenseService.updateLicenseCount(customerId, appId, count);
        final int used = (int) this.mobileLicenseService.countProvisionedLicensesForCustomerAndType(customerId, appId);
        this.customerSubscriptionService.updateLicenseCountAndUsedForCustomer(customerId, subId, count, used, userAccount.getId());
        return WebCoreConstants.RESPONSE_TRUE + ":" + webSecurityForm.getInitToken();
    }
    
    private Integer verifyRandomIdAndReturnActual(final HttpServletResponse response, final RandomKeyMapping keyMapping, final String strRandomId,
        final String message) {
        final String[] msgs = new String[] { message, message };
        return WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, strRandomId, msgs);
    }
    
    private List<MobileAppInfo> initFilter(final Integer customerId, final RandomKeyMapping keyMapping) {
        final ArrayList<MobileAppInfo> mobileLicenseFilter = new ArrayList<MobileAppInfo>();
        final List<CustomerSubscription> mobileSubscriptions = new LinkedList<CustomerSubscription>();
        for (CustomerSubscription sub : this.customerSubscriptionService.findCustomerSubscriptionsByCustomerId(customerId, true)) {
            if (sub.getSubscriptionType().getMobileApplicationType() != null) {
                mobileSubscriptions.add(sub);
            }
        }
        
        for (CustomerSubscription mobileSub : mobileSubscriptions) {
            final SubscriptionType type = mobileSub.getSubscriptionType();
            mobileLicenseFilter.add(new MobileAppInfo(type.getName(), keyMapping.getRandomString(SubscriptionType.class, type.getId())));
        }
        return mobileLicenseFilter;
    }
    
    @ModelAttribute(FORM_EDIT_LICENSE)
    public final WebSecurityForm<MobileLicenseEditForm> initLicenseEditForm(final HttpServletRequest request, final HttpServletResponse response) {
        return new WebSecurityForm<MobileLicenseEditForm>((Object) null, new MobileLicenseEditForm(), SessionTool.getInstance().locatePostToken(
            FORM_EDIT_LICENSE));
    }
    
}
