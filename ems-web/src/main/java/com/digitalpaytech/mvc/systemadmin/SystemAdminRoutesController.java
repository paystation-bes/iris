package com.digitalpaytech.mvc.systemadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.dto.customeradmin.CustomerDetails;
import com.digitalpaytech.mvc.RoutesController;
import com.digitalpaytech.mvc.customeradmin.support.RouteEditForm;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.customeradmin.RouteSettingValidator;

/**
 * This class handles the Route setting request in Settings menu.
 * 
 * @author Brian Kim
 * 
 */
@Controller
@SessionAttributes({ "routeEditForm" })
public class SystemAdminRoutesController extends RoutesController {
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private RouteSettingValidator routeSettingValidator;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;

    public final void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }

    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    /**
     * This method will retrieve all customer's route information and display
     * them in Settings->Routes main page.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return target vm file URI which is mapped to Settings->Routes
     *         (/settings/routes/index.vm)
     */
    @RequestMapping(value = "/systemAdmin/workspace/routes/index.html", method = RequestMethod.GET)
    public final String getRouteListByCustomer(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* get customer information from security framework session. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_ROUTES)) {
            if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        final Customer customer = this.commonControllerHelper.insertCustomerInModelForSystemAdmin(request, model);
        
        return super.getRouteListByCustomer(request, response, model, customer, "/systemAdmin/workspace/routes/routeDetails");
    }
    
    /**
     * This method will sort the route list based on user input and convert them
     * into JSON list and pass back to UI.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return Velocity file URL (/settings/include/sortRouteList.vm). "false"
     *         in case of any exception or validation failure.
     */
    @RequestMapping(value = "/systemAdmin/workspace/routes/sortRouteList.html", method = RequestMethod.GET)
    public final String sortRouteList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* get customer information from security framework session. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_ROUTES)) {
            if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        final Customer customer = this.customerService.findCustomer(getCustomerId(request));
        
        return super.sortRouteList(request, response, model, customer, "/settings/include/sortRouteList");
    }
    
    /**
     * This method will view Route Detail information on the page. When
     * "Edit Route" is clicked, it will also be called from front-end logic
     * automatically after calling getRouteForm() method.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return JSON string having route detail information. "false" in case of
     *         any exception or validation failure.
     */
    @RequestMapping(value = "/systemAdmin/workspace/routes/viewRoutesDetails.html", method = RequestMethod.GET)
    @ResponseBody
    public final String viewRoutesDetails(final HttpServletRequest request, final HttpServletResponse response) {
        
        /* validate user account if it has proper permission to view it. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_ROUTES)) {
            if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.viewRoutesDetails(request, response, getCustomerId(request));
    }
    
    /**
     * This method will be called when "Add Route" is clicked.
     * It will return list of all pay stations with JSON format.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return String JSON string containing all pay station list.
     */
    @RequestMapping(value = "/systemAdmin/workspace/routes/form.html", method = RequestMethod.GET)
    @ResponseBody
    public final String getRouteForm(final HttpServletRequest request, final HttpServletResponse response) {
        
        /* get customer information from security framework session. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Customer customer = this.customerService.findCustomer(getCustomerId(request));
        
        return super.getRouteForm(request, response, customer);
    }
    
    /**
     * This method saves Route information in database when save button is
     * clicked from UI.
     * 
     * @param webSecurityForm
     *            user input form
     * @param result
     *            Error object
     * @param response
     *            HttpServletResponse object
     * @return "true" string value if process succeeds, JSON error message if
     *         process fails
     */
    @RequestMapping(value = "/systemAdmin/workspace/routes/saveRoute.html", method = RequestMethod.POST)
    @ResponseBody
    public final String saveRoute(@ModelAttribute("routeEditForm") final WebSecurityForm<RouteEditForm> webSecurityForm, final BindingResult result,
                                  final HttpServletRequest request, final HttpServletResponse response) {
        
        /* validate user account if it has proper permission to view it. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Customer customer = this.customerService.findCustomer(getCustomerId(request));
        
        return super.saveRoute(webSecurityForm, result, response, customer);
    }
    
    /**
     * Verify whether this route can be deleted from database
     * 
     * @param request
     * @param response
     * @return {@link MessageInfo} in JSON format or "false" if user doesn't have permission to delete route.
     */
    @RequestMapping(value = "/systemAdmin/workspace/routes/verifyDeleteRoute.html", method = RequestMethod.GET)
    @ResponseBody
    public final String verifyDeleteRoute(final HttpServletRequest request, final HttpServletResponse response) {
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return this.routeSettingValidator.verifyDeleteRoute(request);
    }
    
    /**
     * This method deletes Route information and it updates the status flag in
     * Route table.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return "true" string value if process succeeds, "false" string value if
     *         process fails.
     */
    @RequestMapping(value = "/systemAdmin/workspace/routes/deleteRoute.html", method = RequestMethod.GET)
    @ResponseBody
    public final String deleteRoute(final HttpServletRequest request, final HttpServletResponse response) {
        
        /* validate user account if it has proper permission to do it. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_ROUTES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.deleteRoute(request, response);
    }
    
    private Integer getCustomerId(final HttpServletRequest request) {
        final String custRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        return (Integer) keyMapping.getKey(custRandomId);
    }
    
}
