package com.digitalpaytech.mvc.systemadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebPermissionUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
public class NavigationIndexController {
    
    /**
     * This method will look at the permissions in the WebUser and pass control
     * to the first tab that the user has permissions for.
     * 
     * @return Will return the method for locationDetails, extendByPhone,
     *         payStationPlacement, or payStationList depending on permissions,
     *         or error if no permissions exist.
     */
    @RequestMapping(value = "/systemAdmin/index.html")
    public final String getFirstPermittedTab(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (WebPermissionUtil.hasSystemAdminWorkspacePermissions()) {
            return "redirect:/systemAdmin/workspaceIndex.html";
        } else if (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_REPORTS)) {
            return "redirect:/systemAdmin/reporting/index.html";
        } else if (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES)
                   || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES)) {
            return "redirect:/systemAdmin/adminUserAccount/index.html";
        } else if (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_NOTIFICATIONS)
                   || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_NOTIFICATIONS)) {
            return "redirect:/systemAdmin/notifications/index.html";
        } else if (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_SYSTEM_ACTIVITIES)
                   || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_SERVER_STATUS)
                   || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_SERVER_OPERATIONS)) {
            return "redirect:/systemAdmin/systemStatus/index.html";
        } else {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
    }
}
