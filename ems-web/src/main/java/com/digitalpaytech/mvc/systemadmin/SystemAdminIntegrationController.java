package com.digitalpaytech.mvc.systemadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.digitalpaytech.mvc.FlexWSUserAccountController;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.FlexHelper;
import com.digitalpaytech.mvc.support.FlexWSUserAccountForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
public class SystemAdminIntegrationController extends FlexWSUserAccountController {
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    public final CommonControllerHelper getCommonControllerHelper() {
        return this.commonControllerHelper;
    }
    
    public final void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/licenses/integrations.html", method = RequestMethod.GET)
    public final String view(@ModelAttribute(FlexHelper.FORM_CREDENTIALS) final WebSecurityForm<FlexWSUserAccountForm> form,
        final HttpServletRequest request, final HttpServletResponse response, final ModelMap model, final String successURL) {
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        this.commonControllerHelper.insertCustomerInModelForSystemAdmin(request, model);
        
        return "systemAdmin/workspace/licenses/integrations";
    }
    
    private boolean canManage() {
        return WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_MANAGE_FLEX)
               || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_MANAGE_CASE);
    }
    
    private boolean canView() {
        return WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_VIEW_FLEX)
               || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_VIEW_CASE) || canManage();
    }
}
