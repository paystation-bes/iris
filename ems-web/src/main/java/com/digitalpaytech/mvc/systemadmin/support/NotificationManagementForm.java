package com.digitalpaytech.mvc.systemadmin.support;

import java.io.Serializable;
import java.util.Date;

public class NotificationManagementForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5074823310539727304L;
	private String randomNotificationId;
	private Integer notificationId;
	private String startDate;
	private String startTime;
	private String endDate;
	private String endTime;
	private String title;
	private String message;
	private String link;
	private Date startDateTime;
	private Date endDateTime;

	public String getRandomNotificationId() {
		return randomNotificationId;
	}

	public void setRandomNotificationId(String randomNotificationId) {
		this.randomNotificationId = randomNotificationId;
	}

	public Integer getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Integer notificationId) {
		this.notificationId = notificationId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Date getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(Date startDateTime) {
		this.startDateTime = startDateTime;
	}

	public Date getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(Date endDateTime) {
		this.endDateTime = endDateTime;
	}

}
