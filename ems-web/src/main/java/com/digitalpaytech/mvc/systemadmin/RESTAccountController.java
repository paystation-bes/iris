package com.digitalpaytech.mvc.systemadmin;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.RestAccount;
import com.digitalpaytech.domain.RestAccountType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.mvc.systemadmin.support.RESTAccountEditForm;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.EncryptionService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RestAccountService;
import com.digitalpaytech.service.RestAccountTypeService;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.systemadmin.RESTAccountValidator;

@Controller
@SessionAttributes({ "restAccountEditForm" })
public class RESTAccountController {
    
    private static Logger log = Logger.getLogger(RESTAccountController.class);
    
    @Autowired
    private RestAccountTypeService restAccountTypeService;
    
    @Autowired
    private RestAccountService restAccountService;
    
    @Autowired
    private RESTAccountValidator restAccountValidator;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private EncryptionService encryptionService;
    
    public final void setRestAccountTypeService(final RestAccountTypeService restAccountTypeService) {
        this.restAccountTypeService = restAccountTypeService;
    }
    
    public final void setRestAccountService(final RestAccountService restAccountService) {
        this.restAccountService = restAccountService;
    }
    
    public final void setRestAccountValidator(final RESTAccountValidator restAccountValidator) {
        this.restAccountValidator = restAccountValidator;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final void setEncryptionService(final EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }
    
    /**
     * addMerchantAccount is called when admin user clicks "Add Merchant Account" icon and it will return a list of processors.
     * 
     * @param response
     *            HttpServletRequest
     * @param model
     *            ModelMap it will contain WebCoreConstants.PROCESSOR_NAMES which is a two-dimensional arrays object that first entry of
     *            each array is processor group name, e.g. Direct. Key name is "processors".
     * @return String full template file path.
     */
    @RequestMapping(value = "/systemAdmin/workspace/customers/addRestAccount.html", method = RequestMethod.GET)
    public final String addRestAccount(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LICENSES)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LICENSES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String customerRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer customerId = (Integer) keyMapping.getKey(customerRandomId);
        
        final Customer customer = this.customerService.findCustomer(customerId);
        
        if (!customer.isIsMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON + this.restAccountValidator.getMessage("error.customer.notMigrated.systemAdmin");
        }
        
        final List<RestAccountType> restAccountTypeList = this.restAccountTypeService.loadAll();
        final List<PointOfSale> virtualPosList = this.pointOfSaleService.findVirtualPointOfSalesByCustomerId(customerId);
        for (PointOfSale pos : virtualPosList) {
            pos.setRandomId(keyMapping.getRandomString(pos, WebCoreConstants.ID_LOOK_UP_NAME));
        }
        
        final RESTAccountEditForm form = new RESTAccountEditForm();
        form.setRestAccountTypeList(restAccountTypeList);
        form.setVirtualPSList(virtualPosList);
        
        final WebSecurityForm<RESTAccountEditForm> restAccountEditForm = new WebSecurityForm<RESTAccountEditForm>(null, form);
        restAccountEditForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        
        model.put("restAccountEditForm", restAccountEditForm);
        return "/systemAdmin/workspace/customers/include/restAccountForm";
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/customers/saveRestAccount.html", method = RequestMethod.POST)
    @ResponseBody
    public final String saveRestAccount(@ModelAttribute("restAccountEditForm") final WebSecurityForm<RESTAccountEditForm> webSecurityForm,
                                        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LICENSES)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LICENSES)) {
            webSecurityForm.addErrorStatus(new FormErrorStatus(FormFieldConstants.PARAMETER_CUSTOMER_ID, this.restAccountValidator.getMessage("error.user.insufficientPermission")));
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        this.restAccountValidator.validate(webSecurityForm, result, keyMapping);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        final RESTAccountEditForm form = webSecurityForm.getWrappedObject();
        final Customer customer = this.customerService.findCustomer(form.getCustomerRealId());
        
        if (!this.restAccountValidator.validateSystemAdminMigrationStatus(webSecurityForm, customer)) {
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        webSecurityForm.resetToken();
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(form.getVirtualPosRealId());
        RestAccount restAccount = null;
        
        if (form.getRandomId() == null || form.getRandomId().isEmpty()) {
            final List<RestAccount> restAccounts = this.restAccountService.findNonUniqueRestAccountByAccountName(form.getAccountName());
            if (restAccounts != null && restAccounts.size() > 0) {
                webSecurityForm.addErrorStatus(new FormErrorStatus("accountName", this.restAccountValidator.getMessage("error.restAccount.accountName.inUse")));
                webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
                return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
            }
            
            final RestAccount posRestAccount = this.restAccountService.findRestAccountByPointOfSaleId(form.getVirtualPosRealId());
            if (posRestAccount != null) {
                webSecurityForm
                        .addErrorStatus(new FormErrorStatus("virtualPOSId", this.restAccountValidator.getMessage("error.restAccount.pointOfSale.inUse")));
                webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
                return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
            }
            restAccount = new RestAccount();
        } else {
            restAccount = this.restAccountService.findRestAccountById(form.getRestAccountId());
            if (!restAccount.getAccountName().equals(form.getAccountName())) {
                final RestAccount acctNameRestAccount = this.restAccountService.findRestAccountByAccountName(form.getAccountName());
                if (acctNameRestAccount != null) {
                    webSecurityForm.addErrorStatus(new FormErrorStatus("accountName", this.restAccountValidator
                            .getMessage("error.restAccount.accountName.inUse")));
                    webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
                    return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
                }
            }
            if (restAccount.getPointOfSale().getId().intValue() != form.getVirtualPosRealId().intValue()) {
                final RestAccount posRestAccount = this.restAccountService.findRestAccountByPointOfSaleId(form.getVirtualPosRealId());
                if (posRestAccount != null) {
                    webSecurityForm.addErrorStatus(new FormErrorStatus("virtualPOSId", this.restAccountValidator
                            .getMessage("error.restAccount.pointOfSale.inUse")));
                    webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
                    return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
                }
            }
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final Customer dummyCustomer = new Customer();
        dummyCustomer.setId(form.getCustomerRealId());
        restAccount.setCustomer(dummyCustomer);
        restAccount.setLastModifiedByUserId(userAccount.getId());
        restAccount.setLastModifiedGmt(new Date());
        restAccount.setSecretKey(this.encryptionService.createHMACSecretKey());
        final RestAccountType restAccountType = new RestAccountType();
        restAccountType.setId(form.getType().byteValue());
        restAccount.setRestAccountType(restAccountType);
        restAccount.setAccountName(form.getAccountName());
        //TODO
        //test whether the comments are being saved in the table
        restAccount.setComments(form.getComments());
        restAccount.setPointOfSale(pointOfSale);
        
        this.restAccountService.saveOrUpdate(restAccount);
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    /**
     * Edits the existing merchant account.
     * 
     * @param request
     *            HttpServletRequest object needs to have 'merchantAccountId' parameter which is merchant account random id.
     * @param response
     *            HttpServletResponse for error response.
     * @param model
     *            ModelMap object will contain loaded MerchantAccount object with key name 'merchantAccount'.
     * @return String template full file path.
     */
    
    @RequestMapping(value = "/systemAdmin/workspace/customers/editRestAccount.html", method = RequestMethod.GET)
    public String editRestAccount(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LICENSES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final String customerRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
        final Integer customerId = (Integer) keyMapping.getKey(customerRandomId);
        
        final Customer customer = this.customerService.findCustomer(customerId);
        
        if (!customer.isIsMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON + this.restAccountValidator.getMessage("error.customer.notMigrated.systemAdmin");
        }
        
        final String restAccountRandomId = request.getParameter("restAccountId");
        final String msg = "+++ Invalid merchantAccountId in RestAccountController.updateMerchantAccount() GET method. +++";
        
        final Integer restAccountId = WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, restAccountRandomId, new String[] { msg, msg });
        if (restAccountId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final RestAccount restAccount = this.restAccountService.findRestAccountById(restAccountId);
        restAccount.setRandomId(restAccountRandomId);
        
        final List<RestAccountType> restAccountTypeList = this.restAccountTypeService.loadAll();
        final List<PointOfSale> virtualPosList = this.pointOfSaleService.findVirtualPointOfSalesByCustomerId(restAccount.getCustomer().getId());
        for (PointOfSale pos : virtualPosList) {
            pos.setRandomId(keyMapping.getRandomString(pos, WebCoreConstants.ID_LOOK_UP_NAME));
        }
        
        final RESTAccountEditForm form = populateRESTAccountEditForm(restAccount, keyMapping);
        form.setRestAccountTypeList(restAccountTypeList);
        form.setVirtualPSList(virtualPosList);
        final WebSecurityForm<RESTAccountEditForm> restAccountEditForm = new WebSecurityForm<RESTAccountEditForm>(null, form);
        restAccountEditForm.setActionFlag(WebSecurityConstants.ACTION_UPDATE);
        
        model.put("restAccountEditForm", restAccountEditForm);
        
        return "/systemAdmin/workspace/customers/include/restAccountForm";
    }
    
    private RESTAccountEditForm populateRESTAccountEditForm(final RestAccount restAccount, final RandomKeyMapping keyMapping) {
        final RESTAccountEditForm form = new RESTAccountEditForm();
        final Customer customer = restAccount.getCustomer();
        
        final List<RestAccountType> restAccountTypeList = this.restAccountTypeService.loadAll();
        final List<PointOfSale> virtualPosList = this.pointOfSaleService.findVirtualPointOfSalesByCustomerId(customer.getId());
        for (PointOfSale pos : virtualPosList) {
            pos.setRandomId(keyMapping.getRandomString(pos, WebCoreConstants.ID_LOOK_UP_NAME));
        }
        
        form.setCustomerId(keyMapping.getRandomString(customer, WebCoreConstants.ID_LOOK_UP_NAME));
        form.setRandomId(restAccount.getRandomId());
        form.setType(new Integer(restAccount.getRestAccountType().getId()));
        form.setAccountName(restAccount.getAccountName());
        form.setComments(restAccount.getComments());
        form.setVirtualPOSId(keyMapping.getRandomString(restAccount.getPointOfSale(), WebCoreConstants.ID_LOOK_UP_NAME));
        
        form.setVirtualPSList(virtualPosList);
        form.setRestAccountTypeList(restAccountTypeList);
        return form;
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/customers/deleteRestAccount.html", method = RequestMethod.GET)
    public @ResponseBody
    final String deleteRestAccount(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LICENSES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final String customerRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
        final Integer customerId = (Integer) keyMapping.getKey(customerRandomId);
        
        final Customer customer = this.customerService.findCustomer(customerId);
        
        if (!customer.isIsMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON + this.restAccountValidator.getMessage("error.customer.notMigrated.systemAdmin");
        }
        
        final String restAccountRandomId = request.getParameter("restAccountId");
        final String msg = "+++ Invalid merchantAccountId in RestAccountController.updateMerchantAccount() GET method. +++";
        
        final Integer restAccountId = WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, restAccountRandomId, new String[] { msg, msg });
        if (restAccountId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final RestAccount restAccount = this.restAccountService.findRestAccountById(restAccountId);
        restAccount.setIsDeleted(true);
        this.restAccountService.saveOrUpdate(restAccount);
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
}
