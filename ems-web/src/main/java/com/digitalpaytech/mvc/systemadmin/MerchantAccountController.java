package com.digitalpaytech.mvc.systemadmin;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutionException;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.cardprocessing.CardProcessingManager;
import com.digitalpaytech.cardprocessing.CardProcessorFactory;
import com.digitalpaytech.cardprocessing.cps.CPSProcessor;
import com.digitalpaytech.client.PaymentClient;
import com.digitalpaytech.client.dto.ObjectResponse;
import com.digitalpaytech.client.util.CommunicationException;
import com.digitalpaytech.data.InfoAndMerchantAccount;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.GatewayProcessor;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantAccountMigration;
import com.digitalpaytech.domain.MerchantAccountMigrationStatus;
import com.digitalpaytech.domain.MerchantPOS;
import com.digitalpaytech.domain.MerchantStatusType;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.ProcessorInfo;
import com.digitalpaytech.dto.cps.CPSTransactionDto;
import com.digitalpaytech.dto.customeradmin.GlobalConfiguration;
import com.digitalpaytech.exception.DuplicateMerchantAccountException;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.mvc.dto.TerminalAccount;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.mvc.systemadmin.exception.InvalidMerchantAccountException;
import com.digitalpaytech.mvc.systemadmin.support.MerchantAccountEditForm;
import com.digitalpaytech.ribbon.ClientFactory;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.MerchantAccountMigrationService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.ProcessorService;
import com.digitalpaytech.service.paystation.PosServiceStateService;
import com.digitalpaytech.service.systemadmin.SystemAdminService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DPTResponseCodesMapping;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.ReportingUtil;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.SystemAdminUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.util.json.JSONConfigurationBean;
import com.digitalpaytech.util.support.ProcessorNameFormNamePair;
import com.digitalpaytech.validation.BaseValidator;
import com.digitalpaytech.validation.systemadmin.MerchantAccountValidator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.netflix.hystrix.exception.HystrixBadRequestException;

@Controller
@SessionAttributes({ "merchantAccountEditForm" })
public class MerchantAccountController {
    
    private static final Logger LOG = Logger.getLogger(MerchantAccountController.class);
    private static final int PROCESSOR_ID_ALLIANCE = 6;
    private static final int PROCESSOR_ID_FIRST_DATA_NASHVILLE = 2;
    private static final String INVALID_MERCHANT_ACCOUNT_NO_REASON_MSG_KEY = "error.merchantaccount.invalid.merchantaccount.no.reason";
    private static final String INVALID_MERCHANT_ACCOUNT_MSG_KEY = "error.merchantaccount.invalid.merchantaccount";
    private static final String INVALID_MERCHANT_ACCOUNT_INVALID_CUSTOMER_MSG_KEY = "error.merchantAccount.invalid.customer";
    private static Set<Integer> ignoredField5 = new HashSet<Integer>(4);
    
    static {
        ignoredField5.add(CardProcessingConstants.PROCESSOR_ID_CBORD);
        ignoredField5.add(CardProcessingConstants.PROCESSOR_ID_CBORD_ODYSSEY);
    }
    
    @Autowired
    private MerchantAccountValidator merchantAccountValidator;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    @Autowired
    private ProcessorService processorService;
    
    @Autowired
    private CardProcessingManager cardProcessingManager;
    
    @Autowired
    private CardProcessorFactory cardProcessorFactory;
    
    @Autowired
    private EntityService entityService;
    
    @Autowired
    private SystemAdminService systemAdminService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    @Autowired
    private ReportingUtil reportingUtil;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private CPSProcessor cpsProcessor;
    
    @Autowired
    private ClientFactory clientFactory;
    
    @Autowired
    private MessageHelper messageHelper;
    
    @Autowired
    private PosServiceStateService posServiceStateService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private MerchantAccountMigrationService merchantAccountMigrationService;
    
    private ProcessorNameFormNamePair[][] irisProcessorNames;
    private Map<String, Integer> processorIdsMap;
    private Map<Integer, MerchantStatusType> merchantStatusTypeMap;
    private Map<Integer, String[]> gatewayProcessorsMap;
    
    private JSON json;
    
    @PostConstruct
    private void loadConfigData() {
        loadProcessors();
        loadMerchantStatusType();
        loadGatewayProcessors();
        
        final JSONConfigurationBean jsonConfig = new JSONConfigurationBean(true, false, false);
        this.json = new JSON(jsonConfig);
    }
    
    private void loadProcessors() {
        final Set<Integer> processorIdsSet = this.cardProcessorFactory.getProcessorIds();
        
        final Map<String, Integer> processorIdsMapTemp = new HashMap<String, Integer>(processorIdsSet.size() * 2);
        final Set<ProcessorNameFormNamePair> gateways = new TreeSet<ProcessorNameFormNamePair>();
        final Set<ProcessorNameFormNamePair> credits = new TreeSet<ProcessorNameFormNamePair>();
        final Set<ProcessorNameFormNamePair> values = new TreeSet<ProcessorNameFormNamePair>();
        final Set<ProcessorNameFormNamePair> emvs = new TreeSet<ProcessorNameFormNamePair>();
        for (Integer processorId : processorIdsSet) {
            final ProcessorInfo procInfo = this.cardProcessorFactory.getProcessorInfo(processorId);
            final ProcessorNameFormNamePair pnfnp = new ProcessorNameFormNamePair(procInfo.getLabel(), procInfo.getName());
            
            processorIdsMapTemp.put(procInfo.getName(), procInfo.getId());
            if (procInfo.getFormName().length() > 0 && procInfo.getLabel().length() > 0 
            		&& !procInfo.isLink() && procInfo.isActive()) {
                if (procInfo.isGateway()) {
                    gateways.add(pnfnp);
                } else if (procInfo.isForValueCard()) {
                    values.add(pnfnp);
                } else if (procInfo.isEMV()) {
                    emvs.add(pnfnp);
                } else {
                    credits.add(pnfnp);
                }
            }
        }
        
        this.processorIdsMap = processorIdsMapTemp;
        LOG.info("MerchantAccountController, processorMap loaded, total size: " + this.processorIdsMap.size());
        
        final ProcessorNameFormNamePair[][] processorNamesTemp = new ProcessorNameFormNamePair[4][];
        
        int idx = 0;
        Iterator<ProcessorNameFormNamePair> itr = credits.iterator();
        processorNamesTemp[0] = new ProcessorNameFormNamePair[credits.size() + 1];
        processorNamesTemp[0][idx++] =
                new ProcessorNameFormNamePair(this.reportingUtil.getPropertyEN(WebCoreConstants.DIRECT), WebCoreConstants.EMPTY_STRING);
        while (itr.hasNext()) {
            processorNamesTemp[0][idx++] = itr.next();
        }
        
        idx = 0;
        itr = gateways.iterator();
        processorNamesTemp[1] = new ProcessorNameFormNamePair[gateways.size() + 1];
        processorNamesTemp[1][idx++] =
                new ProcessorNameFormNamePair(this.reportingUtil.getPropertyEN(WebCoreConstants.GATEWAY), WebCoreConstants.EMPTY_STRING);
        while (itr.hasNext()) {
            processorNamesTemp[1][idx++] = itr.next();
        }
        
        idx = 0;
        itr = values.iterator();
        processorNamesTemp[2] = new ProcessorNameFormNamePair[values.size() + 1];
        processorNamesTemp[2][idx++] =
                new ProcessorNameFormNamePair(this.reportingUtil.getPropertyEN(WebCoreConstants.VALUECARD), WebCoreConstants.EMPTY_STRING);
        while (itr.hasNext()) {
            processorNamesTemp[2][idx++] = itr.next();
        }
        
        idx = 0;
        itr = emvs.iterator();
        processorNamesTemp[3] = new ProcessorNameFormNamePair[emvs.size() + 1];
        processorNamesTemp[3][idx++] =
                new ProcessorNameFormNamePair(this.reportingUtil.getPropertyEN(WebCoreConstants.EMV), WebCoreConstants.EMPTY_STRING);
        while (itr.hasNext()) {
            processorNamesTemp[3][idx++] = itr.next();
        }
        
        this.irisProcessorNames = processorNamesTemp;
    }
    
    public final void loadMerchantStatusType() {
        this.merchantStatusTypeMap = new HashMap<Integer, MerchantStatusType>();
        final Iterator<MerchantStatusType> iter = this.entityService.loadAll(MerchantStatusType.class).iterator();
        while (iter.hasNext()) {
            final MerchantStatusType type = iter.next();
            this.merchantStatusTypeMap.put(type.getId(), type);
        }
        LOG.info("MerchantAccountController, merchantStatusTypes loaded");
    }
    
    private void loadGatewayProcessors() {
        loadGatewayProcessorsMap(this.merchantAccountService.findGatewayProcessors());
        
        // -------------- Gateway Processors -----------
        final Iterator<Integer> iter = this.gatewayProcessorsMap.keySet().iterator();
        Integer processorId;
        String[] values;
        final StringBuilder bdr = new StringBuilder();
        LOG.info("MerchantAccountController, Gateway Processors Map size: " + this.gatewayProcessorsMap.size());
        while (iter.hasNext()) {
            processorId = iter.next();
            values = this.gatewayProcessorsMap.get(processorId);
            
            bdr.append("processorId / gateways: ").append(processorId).append(" / ").append(showGatewayProcessorValues(values));
            LOG.debug(bdr.toString());
            LOG.debug("-------------------------------------");
            bdr.replace(0, bdr.length(), "");
        }
    }
    
    private String showGatewayProcessorValues(final String[] values) {
        final StringBuilder bdr = new StringBuilder();
        for (String s : values) {
            bdr.append(s).append(StandardConstants.STRING_COMMA);
        }
        return bdr.toString();
    }
    
    public final Map<Integer, String[]> loadGatewayProcessorsMap(final List<GatewayProcessor> gatewayProcessors) {
        final Map<Integer, List<String>> map = new HashMap<Integer, List<String>>();
        for (GatewayProcessor gateProc : gatewayProcessors) {
            final Integer id = gateProc.getProcessor().getId();
            if (map.containsKey(id)) {
                map.get(id).add(gateProc.getValue());
                
            } else {
                final List<String> values = new ArrayList<String>();
                values.add(gateProc.getValue());
                map.put(id, values);
            }
        }
        
        this.gatewayProcessorsMap = new HashMap<Integer, String[]>(map.size());
        
        final Iterator<Integer> keyIter = map.keySet().iterator();
        while (keyIter.hasNext()) {
            final Integer processorId = keyIter.next();
            final String[] values = map.get(processorId).toArray(new String[map.get(processorId).size()]);
            this.gatewayProcessorsMap.put(processorId, values);
        }
        
        return this.gatewayProcessorsMap;
    }
    
    private Processor findProcessor(final String processorMsgKey) {
        final Integer processorId = this.processorIdsMap.get(processorMsgKey.trim());
        return (processorId == null) ? null : this.processorService.findProcessor(processorId);
    }
    
    /**
     * addMerchantAccount is called when admin user clicks "Add Merchant Account" icon and it will return a list of processors.
     * 
     * @param response
     *            HttpServletRequest
     * @param model
     *            ModelMap it will contain WebCoreConstants.PROCESSOR_NAMES which is a two-dimensional arrays object that first entry of
     *            each array is processor group name, e.g. Direct. Key name is "processors".
     * @return String full template file path.
     */
    @RequestMapping(value = "/systemAdmin/workspace/customers/addMerchantAccount.html", method = RequestMethod.GET)
    public final String addMerchantAccount(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_CREATE_NEW_CUSTOMER)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_EDIT_EXISTING_CUSTOMER)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return null;
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final String customerRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
        final Integer customerId = (Integer) keyMapping.getKey(customerRandomId);
        
        final Customer customer = this.customerService.findCustomer(customerId);
        
        if (!customer.isIsMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON
                   + this.merchantAccountValidator.getMessage("error.customer.notMigrated.systemAdmin");
        }
        
        /*
         * irisProcessorNames is a two-dimensional arrays object that first entry of each array is processor group name, e.g. Direct.
         */
        
        final MerchantAccountEditForm form = new MerchantAccountEditForm();
        form.setProcessors(this.irisProcessorNames);
        form.setParadataGatewayProcs(this.gatewayProcessorsMap.get(CardProcessingConstants.PROCESSOR_ID_PARADATA));
        form.setAuthorizeNetGatewayProcs(this.gatewayProcessorsMap.get(CardProcessingConstants.PROCESSOR_ID_AUTHORIZE_NET));
        // Need to follow JavaBean standard and apply camel-case method names. 
        form.setEmvProcs(this.gatewayProcessorsMap);
        
        final WebSecurityForm<MerchantAccountEditForm> merchantAccountEditForm = new WebSecurityForm<MerchantAccountEditForm>(null, form);
        merchantAccountEditForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        
        GlobalConfiguration globalCfg = new GlobalConfiguration();
        globalCfg = this.customerAdminService.findGlobalPreferencesTimeZoneOnly(customer, globalCfg);
        model.put(WebCoreConstants.GLOBAL_CONFIGURATION, globalCfg);
        
        // Default time requested is 11 PM US/Eastern
        merchantAccountEditForm.getWrappedObject().setQuarterOfTheDay(92);
        merchantAccountEditForm.getWrappedObject().setTimeZone("US/Eastern");
        model.put("merchantAccountEditForm", merchantAccountEditForm);
        return "/systemAdmin/workspace/customers/include/merchantAccountForm";
    }
    
    /**
     * addProcessorInfo method is called using AJAX when a processor is selected. Based on selected processor it would return full file path to
     * the template.
     */
    @RequestMapping(value = "/systemAdmin/workspace/customers/addProcessorInfo.html", method = RequestMethod.GET)
    public final String addProcessorInfo(final HttpServletRequest request) {
        ProcessorInfo procInfo = null;
        
        final String procName = request.getParameter("processorTemplate");
        if (procName != null) {
            final Integer processorId = this.processorIdsMap.get(procName);
            if (processorId != null) {
                procInfo = this.cardProcessorFactory.getProcessorInfo(processorId);
            }
        }
        
        if (procInfo == null) {
            throw new InvalidMerchantAccountException("error.merchantAccount.invalid.processor");
        }
        final StringBuilder bdr = new StringBuilder();
        bdr.append("/systemAdmin/workspace/customers/include/").append(procInfo.getFormName());
        return bdr.toString();
    }
    
    private void printEMVErrorMessage(final int processorId) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("Processor id: ").append(processorId).append(" is NOT credit card but it's (EMV but not CPS) !");
        bdr.append(". Check processor details and implementation, EMV processor should have TerminalToken in MerchantAccount table.");
        LOG.error(bdr.toString());
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/customers/testMerchantAccount.html", method = RequestMethod.GET)
    @ResponseBody
    public final String testMerchantAccount(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_EDIT_EXISTING_CUSTOMER)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_CREATE_NEW_CUSTOMER)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return null;
        }
        
        String resp = WebCoreConstants.RESPONSE_FALSE;
        MerchantAccount merchantAccount = null;
        try {
            merchantAccount = findMerchantAccount(request, response,
                                                  "+++ Invalid merchantAccountId in MerchantAccountController.testMerchantAccount() GET method. +++");

        } catch (InvalidMerchantAccountException imae) {
            LOG.error(imae);
            return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON + imae.getMessage();
        }            
            
        if (SystemAdminUtil.isCreditCardProcessor(merchantAccount.getProcessor()) || SystemAdminUtil.isEMVProcessor(merchantAccount.getProcessor())) {
            
            /*
             * Only test merchant account if it uses credit card. DPTResponseCodesMapping.CC_INVALID_MERCHANT_STRING__7072 is
             * from CardProcessingManager, processTransactionTestCharge when trying to instantiate FirstDataNashvilleProcessor.
             */
            if (SystemAdminUtil.isCreditCardProcessor(merchantAccount.getProcessor()) && !merchantAccount.getIsLink()) {
                resp = this.cardProcessingManager.processTransactionTestCharge(merchantAccount, StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1, null,
                                                                               Track2Card.INTERNAL_TYPE_CREDIT_CARD);
            
            } else if ((SystemAdminUtil.isCreditCardProcessor(merchantAccount.getProcessor()) || SystemAdminUtil.isEMVProcessor(merchantAccount.getProcessor()))
                    && merchantAccount.getIsLink()) {
                resp = this.merchantAccountService.testTransaction(merchantAccount);

            } else if (SystemAdminUtil.isEMVProcessor(merchantAccount.getProcessor()) && merchantAccount.isCPS() && !merchantAccount.getIsLink()) {
                resp = this.cpsProcessor.processTransactionTestCharge(new CPSTransactionDto(merchantAccount));
                
            } else {
                printEMVErrorMessage(merchantAccount.getProcessor().getId());
                throw new InvalidMerchantAccountException("error.merchantAccount.invalid.processor");
            }
            
            if (resp.indexOf(WebCoreConstants.RESPONSE_TRUE) > -1) {
                merchantAccount.setIsValidated(true);
            } else {
                merchantAccount.setIsValidated(false);
            }
            merchantAccount.setReferenceCounter(merchantAccount.getReferenceCounter() + 1);
            this.merchantAccountService.updateMerchantAccount(merchantAccount);
        }
        return resp;
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/customers/saveMerchantAccount.html", method = RequestMethod.POST)
    @ResponseBody
    public final String saveMerchantAccount(@ModelAttribute("merchantAccountEditForm") final WebSecurityForm<MerchantAccountEditForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_CREATE_NEW_CUSTOMER)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_EDIT_EXISTING_CUSTOMER)) {
            webSecurityForm.addErrorStatus(new FormErrorStatus(FormFieldConstants.PARAMETER_CUSTOMER_ID,
                    this.merchantAccountValidator.getMessage("error.user.insufficientPermission")));
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        final MerchantAccountEditForm form = webSecurityForm.getWrappedObject();
        // Finds existing processor.
        final Processor pro = findProcessor(form.getProcessorName());
        if (pro == null || pro.getIsLink()) {
            LOG.error("MerchantAccountController, saveMerchantAccount, cannot find processor in db with name: " + form.getProcessorName());
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.addErrorStatus(new FormErrorStatus("processorName",
                    this.merchantAccountValidator.getMessage("error.merchantAccount.processor.wrong.name")));
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        this.merchantAccountValidator.validate(webSecurityForm, result, pro);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        final Customer customer = this.customerService.findCustomer(form.getCustomerRealId());
        
        if (!this.merchantAccountValidator.validateSystemAdminMigrationStatus(webSecurityForm, customer)) {
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        webSecurityForm.resetToken();
        
        final Integer customerId = form.getCustomerRealId();
        final int merchantAccountId = form.getMerchantAccountId() != null ? form.getMerchantAccountId() : 0;
        // Creates search criteria to find existing MerchantAccount with same procesorId and fields.
        final Map<String, String> criteriaMap = createCriteriaMap(form, pro.getId(), merchantAccountId, customerId);
        ignoreOptionalFields(criteriaMap, pro.getId());
        MerchantAccount existingMerchantAcct = null;
        
        boolean isFault = true;
        
        try {
            //search for an existing merchant account with same criteria (processor fields and credentials)
            //isFault will remain true for following causes: 
            //account already exists for another customer,
            //OR account already exists for this customer with a different id
            existingMerchantAcct = this.merchantAccountService.findActiveByProcessorAndFields(criteriaMap);
            if (existingMerchantAcct != null) {
                if (merchantAccountId != 0) {
                    if (existingMerchantAcct.getId().intValue() == merchantAccountId) {
                        isFault = false;
                        existingMerchantAcct = this.merchantAccountService.findById(merchantAccountId);
                    }
                }
            } else {
                isFault = false;
            }
            
        } catch (DuplicateMerchantAccountException dmae) {
            // display error message if multiple identical accounts already exist in database
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.addErrorStatus(new FormErrorStatus(FormFieldConstants.PARAMETER_CUSTOMER_ID,
                    this.merchantAccountValidator.getMessage(INVALID_MERCHANT_ACCOUNT_INVALID_CUSTOMER_MSG_KEY)));
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        if (isFault) {
            if (existingMerchantAcct.getCustomer().getId().intValue() != customerId.intValue()) {
                // display error message if this account belongs to another customer
                webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
                webSecurityForm.addErrorStatus(new FormErrorStatus(FormFieldConstants.PARAMETER_CUSTOMER_ID,
                        this.merchantAccountValidator.getMessage(INVALID_MERCHANT_ACCOUNT_INVALID_CUSTOMER_MSG_KEY)));
                return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
            } else {
                //display error message if this account already exists with a different id
                webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
                webSecurityForm.addErrorStatus(new FormErrorStatus(FormFieldConstants.PARAMETER_CUSTOMER_ID,
                        this.merchantAccountValidator.getMessage("error.merchantAccount.processor.duplicate")));
                return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
            }
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final MerchantAccount merchantAccount;
        // if no accounts found with identical processor fields and credentials,
        // try to find an existing merchant account that the customer has selected to edit
        if (existingMerchantAcct == null) {
            existingMerchantAcct = this.merchantAccountService.findById(merchantAccountId);
        }
        // create new merchant account only if not modifying existing, or creating a duplicate
        if (existingMerchantAcct == null) {
            MerchantAccount newMerchantAcct = null;
            if (pro.getIsEMV()) {
                try {
                    newMerchantAcct = createNewEMVAccount(userAccount, pro, form, customerId);
                } catch (final RuntimeException re) {
                    if (re.getCause() instanceof HystrixBadRequestException && re.getCause().getCause() != null
                        && re.getCause().getCause() instanceof CommunicationException
                        && ((CommunicationException) re.getCause().getCause()).getResponseStatus() == WebCoreConstants.HTTP_STATUS_CODE_409) {
                        webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
                        webSecurityForm.addErrorStatus(new FormErrorStatus(FormFieldConstants.PARAMETER_CUSTOMER_ID,
                                this.merchantAccountValidator.getMessage("error.merchantAccount.processor.duplicate")));
                        return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL,
                                                                 true);
                    } else {
                        throw re;
                    }
                }
            } else {
                newMerchantAcct = createNewMerchantAccount(userAccount, pro, form, customerId);
                // Validates Alliance & First Data Nashville.
                this.merchantAccountValidator.validateAccountWithProcessorAndField1And2(newMerchantAcct, result);
                
                // Populates 'field3' and 'field4' for Alliance or First Data Nashville.
                final InfoAndMerchantAccount infoMer = preFillMerchantAccountIfNecessary(newMerchantAcct);
                final String nameList = infoMer.getInfo();
                if (nameList != null && nameList.length() > 0) {
                    webSecurityForm.addErrorStatus(new FormErrorStatus("processorName",
                            this.merchantAccountValidator.getMessage("error.merchantAccount.field.inUse")));
                    return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL,
                                                             true);
                }
            }
            
            this.merchantAccountService.saveMerchantAccount(newMerchantAcct);
            merchantAccount = newMerchantAcct;
        } else {
            // update 
            if (pro.getIsEMV()) {
                existingMerchantAcct = updateExistingEMVAccount(userAccount, existingMerchantAcct, form);
            } else {
                existingMerchantAcct = updateExistingMerchantAcct(userAccount, existingMerchantAcct, form);
            }
            
            this.merchantAccountService.updateMerchantAccount(existingMerchantAcct);
            merchantAccount = existingMerchantAcct;
        }
        // It's value card (custom card) or EMV, set it's valid by default.            
        if (pro.isIsForValueCard() && form.getIsEnabled()) {
            merchantAccount.setIsValidated(true);
        }
        // if it's credit card, check validity if enabled
        String resp = WebCoreConstants.RESPONSE_TRUE;
        if (SystemAdminUtil.isCreditCardProcessor(merchantAccount.getProcessor()) || SystemAdminUtil.isEMVProcessor(merchantAccount.getProcessor())) {
            
            // 1st, check if MerchantAccount terminalToken exists and call CPSProcessor if it does, no matter if it's EMV or not.
            if (merchantAccount.isCPS()) {
                resp = this.cpsProcessor.processTransactionTestCharge(new CPSTransactionDto(merchantAccount));
                
                // 2nd, check if Processor isIsForValueCard = 0 && isIsExcluded = 0 & getIsEMV = 0, and calls CardProcessingManager if it's true.
            } else if (SystemAdminUtil.isCreditCardProcessor(merchantAccount.getProcessor())) {
                resp = this.cardProcessingManager.processTransactionTestCharge(merchantAccount, StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1, null,
                                                                               Track2Card.INTERNAL_TYPE_CREDIT_CARD);
                
                // Otherwise print error message and throw InvalidMerchantAccountException.
            } else {
                printEMVErrorMessage(merchantAccount.getProcessor().getId());
                throw new InvalidMerchantAccountException("error.merchantAccount.invalid.processor");
            }
            
            if (resp.indexOf(WebCoreConstants.RESPONSE_TRUE) > -1) {
                merchantAccount.setIsValidated(true);
            } else {
                resp = resp.replaceFirst(WebCoreConstants.RESPONSE_FALSE, WebCoreConstants.RESPONSE_TRUE);
                merchantAccount.setIsValidated(false);
            }
            
            merchantAccount.setReferenceCounter(merchantAccount.getReferenceCounter() + 1);
            this.merchantAccountService.updateMerchantAccount(merchantAccount);
        }
        
        return insertTokenToResponse(webSecurityForm.getInitToken(), resp);
    }
    
    /*
     * Re-format return message from:
     * - "true" to "true:posttoken"
     * - "true:testresultmessage" to "true:posttoken:testresultmessage"
     */
    private String insertTokenToResponse(final String token, final String response) {
        if (StringUtils.isBlank(response)) {
            return response;
        }
        final String[] msgs = response.split(StandardConstants.STRING_COLON);
        StringBuilder bdr = new StringBuilder();
        
        // Place ':token' after a successful 'true' response.
        if (msgs.length == StandardConstants.CONSTANT_1) {
            bdr.append(response).append(StandardConstants.STRING_COLON).append(token);
            return bdr.toString();
        }
        
        for (int i = 0; i < msgs.length; i++) {
            // Place token between the 1st and 2nd element.
            if (i == StandardConstants.CONSTANT_1) {
                bdr.append(token).append(StandardConstants.STRING_COLON);
            }
            bdr.append(msgs[i]).append(StandardConstants.STRING_COLON);
        }
        // Remove the last ':'.
        bdr = bdr.replace(bdr.length() - StandardConstants.CONSTANT_1, bdr.length(), StandardConstants.STRING_EMPTY_STRING);
        return bdr.toString();
    }
    
    private MerchantAccount updateExistingEMVAccount(final UserAccount userAccount, final MerchantAccount existingMerchantAcct,
        final MerchantAccountEditForm form) {
        
        final List<MerchantPOS> merchantPoses =
                new ArrayList<MerchantPOS>(this.merchantAccountService.findMerchantPosesByMerchantAccountId(existingMerchantAcct.getId()));
        final List<Integer> posIds = new ArrayList<Integer>();
        for (MerchantPOS merchantPos : merchantPoses) {
            posIds.add(merchantPos.getPointOfSale().getId());
        }
        
        if (!posIds.isEmpty()) {
            if (existingMerchantAcct.getMerchantStatusType().getId() != form.getMerchantStatusTypeIdByEnabledStatus()) {
                this.posServiceStateService.checkforEMVMerchantAccount(existingMerchantAcct, posIds);
            }
        }
        
        existingMerchantAcct.setName(form.getName().trim());
        existingMerchantAcct
                .setMerchantStatusType(form.getIsEnabled() ? this.merchantStatusTypeMap.get(WebCoreConstants.MERCHANT_STATUS_TYPE_ENABLED_ID)
                        : this.merchantStatusTypeMap.get(WebCoreConstants.MERCHANT_STATUS_TYPE_DISABLED_ID));
        existingMerchantAcct.setCloseQuarterOfDay(DateUtil.convertCloseTime(form.getQuarterOfTheDay().byteValue(), form.getTimeZone(),
                                                                            getCustomerTimeZone(form.getCustomerRealId())));
        existingMerchantAcct.setTimeZone(getProperField(form.getTimeZone()));
        existingMerchantAcct.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        existingMerchantAcct.setLastModifiedByUserId(userAccount.getId());
        
        final TerminalAccount terminal = updateTerminalObject(existingMerchantAcct);
        updateTerminalRequest(terminal, existingMerchantAcct);
        return existingMerchantAcct;
    }
    
    private String getTerminalStatus(final MerchantAccount ma) {
        String statusName = null;
        switch (ma.getMerchantStatusType().getId()) {
            case 0:
                statusName = WebCoreConstants.TERMINAL_DISABLED;
                break;
            case 1:
                statusName = WebCoreConstants.TERMINAL_ENABLED;
                break;
            case 2:
                statusName = WebCoreConstants.TERMINAL_DELETED;
                break;
            default:
                statusName = WebCoreConstants.TERMINAL_INVALID;
                break;
        }
        return statusName;
    }
    
    private TerminalAccount updateTerminalObject(final MerchantAccount ma) {
        final TerminalAccount terminal = new TerminalAccount();
        terminal.setName(ma.getName().trim());
        terminal.setTimeZone(ma.getTimeZone());
        terminal.setTerminalStatus(getTerminalStatus(ma));
        return terminal;
    }
    
    private MerchantAccount updateExistingMerchantAcct(final UserAccount userAccount, final MerchantAccount existingMerchantAcct,
        final MerchantAccountEditForm form) {
        existingMerchantAcct.setName(form.getName().trim());
        existingMerchantAcct.setField1(getProperField(form.getField1()));
        existingMerchantAcct.setField2(getProperField(form.getField2()));
        existingMerchantAcct.setField3(getProperField(form.getField3()));
        existingMerchantAcct.setField4(getProperField(form.getField4()));
        existingMerchantAcct.setField5(getProperField(form.getField5()));
        existingMerchantAcct.setField6(getProperField(form.getField6()));
        existingMerchantAcct
                .setMerchantStatusType(form.getIsEnabled() ? this.merchantStatusTypeMap.get(WebCoreConstants.MERCHANT_STATUS_TYPE_ENABLED_ID)
                        : this.merchantStatusTypeMap.get(WebCoreConstants.MERCHANT_STATUS_TYPE_DISABLED_ID));
        existingMerchantAcct.setCloseQuarterOfDay(DateUtil.convertCloseTime(form.getQuarterOfTheDay().byteValue(), form.getTimeZone(),
                                                                            WebSecurityUtil.getCustomerTimeZone()));
        existingMerchantAcct.setTimeZone(getProperField(form.getTimeZone()));
        existingMerchantAcct.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        existingMerchantAcct.setLastModifiedByUserId(userAccount.getId());
        return existingMerchantAcct;
    }
    
    private MerchantAccount createNewEMVAccount(final UserAccount userAccount, final Processor processor, final MerchantAccountEditForm form,
        final int customerId) {
        final MerchantAccount acct = new MerchantAccount();
        final Customer customer = new Customer();
        customer.setId(customerId);
        acct.setCustomer(customer);
        acct.setProcessor(processor);
        acct.setName(form.getName().trim());
        acct.setIsValidated(true);
        acct.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        acct.setLastModifiedByUserId(userAccount.getId());
        acct.setCloseQuarterOfDay(DateUtil.convertCloseTime(form.getQuarterOfTheDay().byteValue(), form.getTimeZone(),
                                                            getCustomerTimeZone(customerId)));
        acct.setTimeZone(getProperField(form.getTimeZone()));
        final MerchantStatusType merchantStatusType = new MerchantStatusType();
        merchantStatusType
                .setId(form.getIsEnabled() ? WebCoreConstants.MERCHANT_STATUS_TYPE_ENABLED_ID : WebCoreConstants.MERCHANT_STATUS_TYPE_DISABLED_ID);
        acct.setMerchantStatusType(merchantStatusType);
        // Set field1 with Terminal Id data for Inventory and Billing reports.
        acct.setField1(form.getField3());
        final TerminalAccount terminal = createTerminalObject(acct, form, customerId);
        final String terminalToken = createTerminalRequest(terminal);
        acct.setTerminalToken(terminalToken);
        return acct;
    }
    
    private Map<String, String> createField1to6(final MerchantAccount ma, final MerchantAccountEditForm form) {
        final Map<String, String> terminalFields = new HashMap<String, String>();
        terminalFields.put(this.messageHelper.getMessage("label.terminalAccount.field1." + ma.getProcessor().getId()), form.getField1());
        terminalFields.put(this.messageHelper.getMessage("label.terminalAccount.field2." + ma.getProcessor().getId()), form.getField2());
        terminalFields.put(this.messageHelper.getMessage("label.terminalAccount.field3." + ma.getProcessor().getId()), form.getField3());
        terminalFields.put(this.messageHelper.getMessage("label.terminalAccount.field4." + ma.getProcessor().getId()), form.getField4());
        terminalFields.put(this.messageHelper.getMessage("label.terminalAccount.field5." + ma.getProcessor().getId()), form.getField5());
        terminalFields.put(this.messageHelper.getMessage("label.terminalAccount.field6." + ma.getProcessor().getId()), form.getField6());
        return terminalFields;
    }
    
    private TerminalAccount createTerminalObject(final MerchantAccount merchantAccount, final MerchantAccountEditForm form, final int customerId) {
        final TerminalAccount terminal = new TerminalAccount();
        final Customer customer = this.customerService.findCustomer(customerId);
        final Map<String, String> terminalFields = createField1to6(merchantAccount, form);
        terminal.setCustomerName(customer.getName());
        terminal.setName(merchantAccount.getName());
        terminal.setProcessorType(this.messageHelper.getMessage(merchantAccount.getProcessor().getName()));
        terminal.setTerminalConfiguration(terminalFields);
        terminal.setTimeZone(merchantAccount.getTimeZone());
        terminal.setTerminalStatus(getTerminalStatus(merchantAccount));
        return terminal;
    }
    
    private String convertTerminalToJson(final TerminalAccount terminal) {
        String terminalJSON = null;
        try {
            terminalJSON = this.json.serialize(terminal);
            return terminalJSON;
        } catch (JsonException e) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append("Unable to read JSON: ").append(terminal).append(". ");
            LOG.error(bdr.toString(), e);
            return null;
        }
    }
    
    private String updateTerminalRequest(final TerminalAccount terminal, final MerchantAccount merchantAccount) {
        final String terminalCreateJson = convertTerminalToJson(terminal);
        final PaymentClient payService = this.clientFactory.from(PaymentClient.class);
        final String token =
                payService.updateTerminalRequest(merchantAccount.getTerminalToken(), terminalCreateJson).execute().toString(Charset.defaultCharset());
        return token;
    }
    
    private String deleteTerminalRequest(final MerchantAccount merchantAccount) {
        final PaymentClient payService = this.clientFactory.from(PaymentClient.class);
        final String token = payService.deleteTerminalRequest(merchantAccount.getTerminalToken()).execute().toString(Charset.defaultCharset());
        return token;
    }
    
    private String createTerminalRequest(final TerminalAccount terminal) {
        final String terminalCreateJson = convertTerminalToJson(terminal);
        final PaymentClient payService = this.clientFactory.from(PaymentClient.class);
        final String token = payService.createTerminalRequest(terminalCreateJson).execute().toString(Charset.defaultCharset());
        return token;
    }
    
    private void ignoreOptionalFields(final Map<String, String> criteriaMap, final int processorId) {
        if (processorId == CardProcessingConstants.PROCESSOR_ID_ELAVON) {
            /*
             * Elavon Field 4 is "Dynamic DBA prefix length"
             * This field can be set to any of multiple possible values without causing any transactions to fail.
             * Elavon accounts with different DBA prefix lengths should still be considered duplicates as this field does not constitute a credential.
             */
            criteriaMap.remove(MerchantAccountService.FIELD_4);
        } else if (processorId == CardProcessingConstants.PROCESSOR_ID_ELAVON_VIACONEX) {
            /*
             * Elavon viaConex Field 2 is "Merchant ID" which should not be verified
             * because it's included in Terminal ID. Also ignore Field 3, "Close
             * Batch Size" which doesn't need to in the query.
             */
            criteriaMap.remove(MerchantAccountService.FIELD_2);
            criteriaMap.remove(MerchantAccountService.FIELD_3);
        }
    }
    
    /**
     * Edits the existing merchant account.
     * 
     * @param request
     *            HttpServletRequest object needs to have 'merchantAccountId' parameter which is merchant account random id.
     * @param response
     *            HttpServletResponse for error response.
     * @param model
     *            ModelMap object will contain loaded MerchantAccount object with key name 'merchantAccount'.
     * @return String template full file path.
     */
    @RequestMapping(value = "/systemAdmin/workspace/customers/editMerchantAccount.html", method = RequestMethod.GET)
    public final String editMerchantAccount(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_EDIT_EXISTING_CUSTOMER)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_CREATE_NEW_CUSTOMER)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final String custRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
        final Integer custId = (Integer) keyMapping.getKey(custRandomId);
        
        final Customer customer = this.customerService.findCustomer(custId);
        
        if (!customer.isIsMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON
                   + this.merchantAccountValidator.getMessage("error.customer.notMigrated.systemAdmin");
        }
        
        prepareMerchantAccountForm(request, response, model, WebSecurityConstants.ACTION_UPDATE);
        
        GlobalConfiguration globalCfg = new GlobalConfiguration();
        globalCfg = this.customerAdminService.findGlobalPreferencesTimeZoneOnly(customer, globalCfg);
        model.put(WebCoreConstants.GLOBAL_CONFIGURATION, globalCfg);
        
        return "/systemAdmin/workspace/customers/include/merchantAccountForm";
    }
    
    /**
     * Views the existing merchant account.
     * 
     * @param request
     *            HttpServletRequest object needs to have 'merchantAccountId' parameter which is merchant account random id.
     * @param response
     *            HttpServletResponse for error response.
     * @param model
     *            ModelMap object will contain loaded MerchantAccount object with key name 'merchantAccount'.
     * @return String template full file path.
     */
    @RequestMapping(value = "/systemAdmin/workspace/customers/viewMerchantAccount.html", method = RequestMethod.GET)
    public final String viewMerchantAccount(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_CUSTOMER_ADMINISTRATION)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        try {
            prepareMerchantAccountForm(request, response, model, WebSecurityConstants.ACTION_VIEW);
        } catch (InvalidMerchantAccountException imae) {
            LOG.error(imae);
            return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON + imae.getMessage();              
        }
        return "/systemAdmin/workspace/customers/include/merchantAccountDetail";
    }
    
    private void prepareMerchantAccountForm(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        final int actionType) {
        final MerchantAccount merchantAccount =
                findMerchantAccount(request, response,
                                    "+++ Invalid merchantAccountId in MerchantAccountController.editMerchantAccount() GET method. +++");
        
        merchantAccount.setRandomId(request.getParameter("merchantAccountId"));
        final ProcessorInfo procInfo = this.cardProcessorFactory.getProcessorInfo(merchantAccount.getProcessor().getId());
        final ProcessorNameFormNamePair processor = new ProcessorNameFormNamePair(procInfo.getLabel(), procInfo.getName());
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final MerchantAccountEditForm form = new MerchantAccountEditForm();
        
        form.setCustomerId(keyMapping.getRandomString(merchantAccount.getCustomer(), WebCoreConstants.ID_LOOK_UP_NAME));
        if (merchantAccount.getTerminalToken() != null && !merchantAccount.getIsLink()) {
            setTerminalFields(merchantAccount, form);
        } else {
            form.setField1(merchantAccount.getField1());
            form.setField2(merchantAccount.getField2());
            form.setField3(merchantAccount.getField3());
            form.setField4(merchantAccount.getField4());
            form.setField5(merchantAccount.getField5());
            form.setField6(merchantAccount.getField6());
        }
        form.setIsEnabled(merchantAccount.getMerchantStatusType().getId() == WebCoreConstants.MERCHANT_STATUS_TYPE_ENABLED_ID);
        form.setName(merchantAccount.getName());
        form.setProcessorName(procInfo.getName());
        form.setProcessorLabel(procInfo.getLabel());
        form.setRandomId(merchantAccount.getRandomId());
        final int customerId = merchantAccount.getCustomer().getId();
        
        final Integer quarterDay;
        final String tz;
        if (StringUtils.isBlank(merchantAccount.getTimeZone())) {
            final String subject = "MerchantAccount TimeZone is null. Id: " + merchantAccount.getId();
            final String err = subject + ", CustomerId: " 
                    + merchantAccount.getCustomer().getId() + ", TerminalToken: " + merchantAccount.getTerminalToken();
            LOG.error(err);
            this.mailerService.sendAdminErrorAlert(subject, err, null);
            quarterDay = StandardConstants.CONSTANT_0;
            tz = WebCoreConstants.N_A_STRING;
        } else {
            quarterDay = (int) DateUtil.convertCloseTime(merchantAccount.getCloseQuarterOfDay(), 
                                                         getCustomerTimeZone(customerId), 
                                                         merchantAccount.getTimeZone());
            tz = merchantAccount.getTimeZone();
        }
        form.setQuarterOfTheDay(quarterDay);
        form.setTimeZone(tz);
        form.setParadataGatewayProcs(this.gatewayProcessorsMap.get(CardProcessingConstants.PROCESSOR_ID_PARADATA));
        form.setAuthorizeNetGatewayProcs(this.gatewayProcessorsMap.get(CardProcessingConstants.PROCESSOR_ID_AUTHORIZE_NET));
        // Need to follow JavaBean standard and apply camel-case method names.
        form.setEmvProcs(this.gatewayProcessorsMap);
        
        final WebSecurityForm<MerchantAccountEditForm> merchantAccountEditForm = new WebSecurityForm<MerchantAccountEditForm>(null, form);
        merchantAccountEditForm.setActionFlag(WebSecurityConstants.ACTION_UPDATE);
        model.put("processor", processor);
        model.put("merchantAccountEditForm", merchantAccountEditForm);
    }
    
    public final void setTerminalFields(final MerchantAccount merchantAccount, final MerchantAccountEditForm form) {
        String jsonString = null;
        try {
            jsonString = URLEncoder.encode(merchantAccount.getTerminalToken(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            LOG.error("Unable to process JSON Mapping", e);
        }
        
        if (!merchantAccount.getIsLink()) {
            final PaymentClient payService = this.clientFactory.from(PaymentClient.class);
            final String terminalConfiguration = payService.grabTerminalInfo(jsonString).execute().toString(Charset.defaultCharset());
            Map<String, String> terminalConfigMap = new HashMap<String, String>();
            
            try {
                terminalConfigMap = this.json.deserialize(terminalConfiguration, new TypeReference<Map<String, String>>() {
                });
            } catch (JsonException e) {
                LOG.error("Unable to process JSON Mapping", e);
            }
            form.setField1(terminalConfigMap
                    .get(this.messageHelper.getMessage("label.terminalAccount.field1." + merchantAccount.getProcessor().getId())));
            form.setField2(terminalConfigMap
                    .get(this.messageHelper.getMessage("label.terminalAccount.field2." + merchantAccount.getProcessor().getId())));
            form.setField3(terminalConfigMap
                    .get(this.messageHelper.getMessage("label.terminalAccount.field3." + merchantAccount.getProcessor().getId())));
            form.setField4(terminalConfigMap
                    .get(this.messageHelper.getMessage("label.terminalAccount.field4." + merchantAccount.getProcessor().getId())));
            form.setField5(terminalConfigMap
                    .get(this.messageHelper.getMessage("label.terminalAccount.field5." + merchantAccount.getProcessor().getId())));
            form.setField6(terminalConfigMap
                    .get(this.messageHelper.getMessage("label.terminalAccount.field6." + merchantAccount.getProcessor().getId())));
        }
    }
    
    /**
     * Finds MerchantAccount record from database.
     * 
     * @param request
     *            HttpServletRequest object needs to have 'merchantAccountId' parameter which is merchant account random id.
     * @param response
     *            HttpServletResponse for error response.
     * @param errorMessage
     *            String error messages
     * @return MerchantAccount Returns 'null' if couldn't find MerchantAccount from database.
     * @throws InvalidMerchantAccountException
     *             The exception is thrown when it's unable to obtain id from random id or couldn't find record from database.
     */
    private MerchantAccount findMerchantAccount(final HttpServletRequest request, 
                                                final HttpServletResponse response, 
                                                final String errorMessage) {
        
        final String maRandomId = request.getParameter("merchantAccountId");
        final String msg = "+++ Invalid merchantAccountId in MerchantAccountController.updateMerchantAccount() GET method. +++";
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer maId = WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, maRandomId, new String[] { msg, msg });
        if (maId == null) {
            throw new InvalidMerchantAccountException(this.messageHelper.getMessage(INVALID_MERCHANT_ACCOUNT_INVALID_CUSTOMER_MSG_KEY));
        }

        final MerchantAccount linkMA = this.merchantAccountService.findById(maId);
        if (linkMA == null) {
            throw new InvalidMerchantAccountException(this.messageHelper.getMessage(INVALID_MERCHANT_ACCOUNT_INVALID_CUSTOMER_MSG_KEY));
        }

        // Check if it's a Link Processor and call different 'findWithProcessorById'.
        final boolean forTest = CardProcessingUtil.isNotNullAndIsLink(linkMA);
        
        try {
            final MerchantAccount merchantAccount = this.merchantAccountService.findWithProcessorById(maId, forTest);
            if (merchantAccount == null) {
                throw new InvalidMerchantAccountException(this.messageHelper.getMessage(INVALID_MERCHANT_ACCOUNT_NO_REASON_MSG_KEY));
            }
            return merchantAccount;
        } catch (CommunicationException ce) {
            try {
                final String causeJson = ce.getResponseMessage().toCompletableFuture().get();
                final ObjectResponse<?> objRes = this.json.deserialize(causeJson, ObjectResponse.class);
                throw new InvalidMerchantAccountException(this.messageHelper.getMessage(INVALID_MERCHANT_ACCOUNT_MSG_KEY, 
                                                                                        objRes.getStatus().getErrors().get(0).getMessage()),
                                                          ce);
            } catch (InterruptedException | ExecutionException | JsonException e) {
                LOG.error("Unable to retrieve 'responseMessage' from CommunicationException, ", e);
                throw new InvalidMerchantAccountException(this.messageHelper.getMessage(INVALID_MERCHANT_ACCOUNT_NO_REASON_MSG_KEY));
            }
        }
    }
    
    /**
     * Finds MerchantPOS records attached to this merchant account
     * 
     * @param request
     *            HttpServletRequest object needs to have 'merchantAccountId' parameter which is merchant account random id.
     * @param response
     *            HttpServletResponse for error response.
     * @param errorMessage
     *            String error messages
     * @return MerchantPOS Returns 'null' if couldn't find MerchantPOS from database.
     * @throws InvalidMerchantAccountException
     *             The exception is thrown when it's unable to obtain id from random id or couldn't find record from database.
     */
    @RequestMapping(value = "/systemAdmin/workspace/customers/verifyDeleteMerchantAccount.html")
    @ResponseBody
    public final String findAttachedPos(final HttpServletRequest request, final HttpServletResponse response) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final String custRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
        final Integer custId = (Integer) keyMapping.getKey(custRandomId);
        
        final Customer customer = this.customerService.findCustomer(custId);
        
        if (!customer.isIsMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON
                   + this.merchantAccountValidator.getMessage("error.customer.notMigrated.systemAdmin");
        }
        
        final Integer maId = (Integer) BaseValidator.validateRandomIdNoForm(request, "merchantAccountId");
        if (WebCoreConstants.RECORD_NOT_FOUND.equals(maId)) {
            throw new InvalidMerchantAccountException("error.merchantaccount.invalid.randomid");
        }
        String responseVal = "true";
        final Collection<MerchantPOS> merchantPOSs = this.merchantAccountService.findMerchantPosesByMerchantAccountId(maId);
        if (merchantPOSs.size() > 0) {
            responseVal =
                    "The pay stations listed below are attached to this merchant account. Do you still wish to delete this merchant account?\n\n";
            String responseValPt2 = "";
            for (MerchantPOS mpos : merchantPOSs) {
                responseValPt2 += ", " + mpos.getPointOfSale().getSerialNumber();
            }
            responseVal = responseVal + responseValPt2.substring(2);
            
            return WidgetMetricsHelper.convertToJson(new MessageInfo(true, responseVal), "messages", true);
        }
        
        return WebCoreConstants.RESPONSE_TRUE;
        
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/customers/deleteMerchantAccount.html", method = RequestMethod.GET)
    @ResponseBody
    public final String deleteMerchantAccount(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_EDIT_EXISTING_CUSTOMER)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_CREATE_NEW_CUSTOMER)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return null;
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final String custRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
        final Integer custId = (Integer) keyMapping.getKey(custRandomId);
        
        final Customer customer = this.customerService.findCustomer(custId);
        
        if (!customer.isIsMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON
                   + this.merchantAccountValidator.getMessage("error.customer.notMigrated.systemAdmin");
        }
        
        final MerchantAccount merchantAccount =
                findMerchantAccount(request, response,
                                    "+++ Invalid merchantAccountId in MerchantAccountController.deleteMerchantAccount() method. +++");
        final MerchantStatusType merchantStatusType = new MerchantStatusType();
        merchantStatusType.setId(WebCoreConstants.MERCHANT_STATUS_TYPE_DELETED_ID);
        merchantAccount.setMerchantStatusType(merchantStatusType);
        merchantAccount.setIsValidated(false);
        if (merchantAccount.getProcessor().getIsEMV()) {
            
            final List<MerchantPOS> merchantPoses =
                    new ArrayList<MerchantPOS>(this.merchantAccountService.findMerchantPosesByMerchantAccountId(merchantAccount.getId()));
            final List<Integer> posIds = new ArrayList<Integer>();
            for (MerchantPOS merchantPos : merchantPoses) {
                posIds.add(merchantPos.getPointOfSale().getId());
            }
            
            if (!posIds.isEmpty()) {
                this.posServiceStateService.checkforEMVMerchantAccount(merchantAccount, posIds);
            }
            
            updateTerminalObject(merchantAccount);
            final String msg = deleteTerminalRequest(merchantAccount);
            if (StringUtils.isBlank(msg) || !msg.equalsIgnoreCase(this.merchantAccountValidator.getMessage("cps.delete.response.succeed"))) {
                return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON
                       + this.merchantAccountValidator.getMessage("label.SystemErrorPage.ErrorMessage");
            }
        }
        this.merchantAccountService.updateMerchantAccount(merchantAccount);
        deleteAttachedPos(request, response);
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/customers/migrateMerchantAccount.html", method = RequestMethod.GET)
    @ResponseBody
    public final String migrateMerchantAccount(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_EDIT_EXISTING_CUSTOMER)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_CREATE_NEW_CUSTOMER)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return null;
        }
        
        final MerchantAccount merchantAccount =
                findMerchantAccount(request, response,
                                    "+++ Invalid merchantAccountId in MerchantAccountController.migrateMerchantAccount() method. +++");
        
        if (merchantAccount == null || merchantAccount.getMerchantStatusType().getId() == WebCoreConstants.MERCHANT_STATUS_TYPE_DELETED_ID
            || merchantAccount.getMerchantStatusType().getId() == WebCoreConstants.MERCHANT_STATUS_TYPE_MIGRATED_ID
            || !merchantAccount.getIsValidated()) {
            
            final MessageInfo messageInfo = new MessageInfo(false, this.messageHelper.getMessage("message.merchantAccount.migration.error.general"));
            messageInfo.setError(true);
            return WidgetMetricsHelper.convertToJson(messageInfo, "messages", true);
        }
        
        if (!this.processorService.isProcessorReadyForMigration(merchantAccount.getProcessor())) {
            final MessageInfo messageInfo =
                    new MessageInfo(false, this.messageHelper.getMessage("message.merchantAccount.migration.error.processor.not.supported"));
            messageInfo.setError(true);
            return WidgetMetricsHelper.convertToJson(messageInfo, "messages", true);
        }
        
        if (this.merchantAccountMigrationService.postAuthBlockMigration(merchantAccount)) {
            final MessageInfo messageInfo =
                    new MessageInfo(false, this.messageHelper.getMessage("message.merchantAccount.migration.error.post.auth"));
            messageInfo.setError(true);
            return WidgetMetricsHelper.convertToJson(messageInfo, "messages", true);
        }
        
        if (this.merchantAccountMigrationService.reversalsBlockMigraion(merchantAccount)) {
            final MessageInfo messageInfo = new MessageInfo(false, this.messageHelper.getMessage("message.merchantAccount.migration.error.reversal"));
            messageInfo.setError(true);
            return WidgetMetricsHelper.convertToJson(messageInfo, "messages", true);
        }
        
        final int migrationWaitTime =
                this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.MERCHANT_ACCOUNT_MIGRATION_WAIT_TIME_IN_MINUTES,
                                                                EmsPropertiesService.MERCHANT_ACCOUNT_MIGRATION_WAIT_TIME_IN_MINUTES_DEFAULT);
        
        final MerchantAccountMigration merchantAccountMigration = new MerchantAccountMigration();
        merchantAccountMigration.setMerchantAccount(merchantAccount);
        
        final MerchantAccountMigrationStatus merchantAccountMigrationStatus = new MerchantAccountMigrationStatus();
        merchantAccountMigrationStatus.setId(MerchantAccountMigrationStatus.PENDING);
        merchantAccountMigration.setMerchantAccountMigrationStatus(merchantAccountMigrationStatus);
        
        final Calendar calendar = GregorianCalendar.getInstance();
        merchantAccountMigration.setCreatedGMT(calendar.getTime());
        
        calendar.add(Calendar.MINUTE, migrationWaitTime);
        merchantAccountMigration.setDelayUntilGMT(calendar.getTime());
        
        final MerchantAccountMigration existingMerchantAcct = this.merchantAccountMigrationService.findByMerchantAccountId(merchantAccount.getId());
        
        if (existingMerchantAcct != null) {
            return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON
                   + this.merchantAccountValidator.getMessage("message.merchantAccount.migration.error.alreadyRequested");
        }
        
        this.merchantAccountMigrationService.disableMerchantAccountAndStartsMigration(merchantAccountMigration);
        
        return WidgetMetricsHelper.convertToJson(new MessageInfo(false, this.messageHelper.getMessage("message.merchantAccount.migration.started")),
                                                 "messages", true);
    }
    
    private void deleteAttachedPos(final HttpServletRequest request, final HttpServletResponse response) {
        
        final Integer maId = (Integer) BaseValidator.validateRandomIdNoForm(request, "merchantAccountId");
        if (WebCoreConstants.RECORD_NOT_FOUND.equals(maId)) {
            throw new InvalidMerchantAccountException("error.merchantaccount.invalid.randomid");
        }
        final Collection<MerchantPOS> merchantPOSs = this.merchantAccountService.findMerchantPosesByMerchantAccountId(maId);
        for (MerchantPOS mpos : merchantPOSs) {
            mpos.setIsDeleted(true);
            mpos.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
            mpos.setLastModifiedByUserId(WebSecurityUtil.getUserAccount().getId());
            this.systemAdminService.updateMerchantPos(mpos);
        }
    }
    
    /**
     * Creates a java.util.Map that has processor id (MerchantAccountService.PROCESSOR_ID), field1 (MerchantAccountService.FIELD_1) and
     * the additional fields if exist.
     * 
     * @see com.digitalpaytech.service.impl.MerchantAccountServiceImpl#findByProcessorAndFields(Map)
     * @param form
     *            MerchantAccountEditForm contains user's inputs.
     * @param processorId
     *            Processor table primary key.
     * @return Map<String, String> Map object with user's inputs.
     */
    private Map<String, String> createCriteriaMap(final MerchantAccountEditForm form, final int processorId, final int merchantAccountId,
        final int customerId) {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(MerchantAccountService.MERCHANT_ACCOUNT_ID, String.valueOf(merchantAccountId));
        map.put(MerchantAccountService.PROCESSOR_ID, String.valueOf(processorId));
        map.put(MerchantAccountService.CUSTOMER_ID, String.valueOf(customerId));
        map.put(MerchantAccountService.FIELD_1, form.getField1());
        
        final String field2 = getProperField(form.getField2());
        if (field2 != null) {
            map.put(MerchantAccountService.FIELD_2, field2);
        }
        final String field3 = getProperField(form.getField3());
        if (field3 != null) {
            map.put(MerchantAccountService.FIELD_3, field3);
        }
        final String field4 = getProperField(form.getField4());
        if (field4 != null) {
            map.put(MerchantAccountService.FIELD_4, field4);
        }
        if (!ignoredField5.contains(processorId)) {
            final String field5 = getProperField(form.getField5());
            if (field5 != null) {
                map.put(MerchantAccountService.FIELD_5, field5);
            }
        }
        final String field6 = getProperField(form.getField6());
        if (field6 != null) {
            map.put(MerchantAccountService.FIELD_6, field6);
        }
        return map;
    }
    
    /**
     * Creates a new MerchantAccount to save to db.
     * 
     * @param userAccount
     *            Log in UserACcount object.
     * @param processor
     *            Processor object.
     * @param form
     *            MerchantAccountEditForm object with user's inputs.
     * @return MerchantAccount New populated MerchantAccount object.
     */
    private MerchantAccount createNewMerchantAccount(final UserAccount userAccount, final Processor processor, final MerchantAccountEditForm form,
        final int customerId) {
        final MerchantAccount acct = new MerchantAccount();
        final Customer customer = new Customer();
        customer.setId(customerId);
        acct.setCustomer(customer);
        acct.setProcessor(processor);
        acct.setName(form.getName().trim());
        acct.setIsValidated(true);
        final MerchantStatusType merchantStatusType = new MerchantStatusType();
        merchantStatusType
                .setId(form.getIsEnabled() ? WebCoreConstants.MERCHANT_STATUS_TYPE_ENABLED_ID : WebCoreConstants.MERCHANT_STATUS_TYPE_DISABLED_ID);
        acct.setMerchantStatusType(merchantStatusType);
        acct.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
        acct.setLastModifiedByUserId(userAccount.getId());
        acct.setReferenceCounter(0);
        // Field 1 ~ 6
        acct.setField1(form.getField1().trim());
        acct.setField2(getProperField(form.getField2()));
        acct.setField3(getProperField(form.getField3()));
        acct.setField4(getProperField(form.getField4()));
        acct.setField5(getProperField(form.getField5()));
        acct.setField6(getProperField(form.getField6()));
        acct.setCloseQuarterOfDay(DateUtil.convertCloseTime(form.getQuarterOfTheDay().byteValue(), form.getTimeZone(),
                                                            getCustomerTimeZone(customerId)));
        acct.setTimeZone(getProperField(form.getTimeZone()));
        return acct;
    }
    
    /**
     * Verifies if input 'field' is not empty and it's not WebCoreConstants.NOT_APPLICABLE (N/A), otherwise return null.
     * 
     * @param field
     *            String
     * @return String return 'null' if field is empty or null.
     */
    private String getProperField(final String field) {
        if (StringUtils.isNotBlank(field) && !field.equalsIgnoreCase(WebCoreConstants.NOT_APPLICABLE)) {
            return field.trim();
        }
        return null;
    }
    
    private InfoAndMerchantAccount preFillMerchantAccountIfNecessary(final MerchantAccount ma) {
        InfoAndMerchantAccount infoMer = null;
        if (ma.getProcessor() != null
            && (ma.getProcessor().getId() == PROCESSOR_ID_ALLIANCE || ma.getProcessor().getId() == PROCESSOR_ID_FIRST_DATA_NASHVILLE)) {
            infoMer = this.merchantAccountService.preFillMerchantAccount(ma);
            
        } else {
            // Return original MerchantAccount object back to caller.
            infoMer = new InfoAndMerchantAccount(null, ma);
        }
        return infoMer;
    }
    
    /**
     * Check if 'response' is String of "7000 (DPTResponseCodesMapping.CC_APPROVED_VAL__7000)" or
     * "CC Transaction was approved (DPTResponseCodesMapping.CC_APPROVED_STRING__7000)"
     * 
     * @param response
     *            String response comes from the processor
     * @return boolean true if it's valid.
     */
    private boolean isValidMerchantAccount(final String response) {
        if (response.indexOf(String.valueOf(DPTResponseCodesMapping.CC_APPROVED_VAL__7000)) != -1
            || response.indexOf(DPTResponseCodesMapping.mapValueToString(DPTResponseCodesMapping.CC_APPROVED_VAL__7000)) != -1) {
            return true;
        }
        return false;
    }
    
    /**
     * Get customer's TimeZone
     * 
     * @param customerId
     * 
     * @return customer TimeZone
     * 
     */
    private String getCustomerTimeZone(final int customerId) {
        final String timeZone;
        CustomerProperty property = null;
        
        property = this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId,
                                                                                                    WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        timeZone = property.getPropertyValue();
        return timeZone;
    }
    
    public final void setMerchantAccountValidator(final MerchantAccountValidator merchantAccountValidator) {
        this.merchantAccountValidator = merchantAccountValidator;
    }
    
    public final void setProcessorService(final ProcessorService processorService) {
        this.processorService = processorService;
    }
    
    public final void setMerchantAccountService(final MerchantAccountService merchantAccountService) {
        this.merchantAccountService = merchantAccountService;
    }
    
    public final void setEntityService(final EntityService entityService) {
        this.entityService = entityService;
    }
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final void setMerchantStatusTypeMap(final Map<Integer, MerchantStatusType> merchantStatusTypeMap) {
        this.merchantStatusTypeMap = merchantStatusTypeMap;
    }
    
    public final void setCardProcessingManager(final CardProcessingManager cardProcessingManager) {
        this.cardProcessingManager = cardProcessingManager;
    }
    
    public final void setSystemAdminService(final SystemAdminService systemAdminService) {
        this.systemAdminService = systemAdminService;
    }
    
    public final void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    public final void setReportingUtil(final ReportingUtil reportingUtil) {
        this.reportingUtil = reportingUtil;
    }
    
    public final void setMailerService(final MailerService mailerService) {
        this.mailerService = mailerService;
    }
}
