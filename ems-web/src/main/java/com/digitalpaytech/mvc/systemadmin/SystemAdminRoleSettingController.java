package com.digitalpaytech.mvc.systemadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.mvc.RoleSettingController;
import com.digitalpaytech.mvc.support.RoleEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

/**
 * This class handles the System Admin Roles setting request.
 * 
 * @author Brian Kim
 * 
 */
@Controller
@SessionAttributes({ "roleEditForm" })
public class SystemAdminRoleSettingController extends RoleSettingController {
    
    /**
     * This method will retrieve user account's role information and display
     * them in Settings->Users->Roles page.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return target vm file URI which is mapped to Settings->Users->Roles
     *         (/systemAdmin/adminUserRole.vm)
     */
    @RequestMapping(value = "/systemAdmin/adminUserAccount/userRole.html", method = RequestMethod.GET)
    public final String getSystemAdminRoleList(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES)) {
            if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.getRoleList(request, response, model, "/systemAdmin/adminUserAccount/userRole");
    }
    
    /**
     * This method will view System Admin User's Role Detail information on the
     * page.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return JSON string having role detail information. "false" in case of
     *         any exception or validation failure.
     */
    @RequestMapping(value = "/systemAdmin/adminUserAccount/viewRoleDetails.html", method = RequestMethod.GET)
    @ResponseBody
    public final String viewSystemAdminRoleDetails(final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES)) {
            if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return super.viewRoleDetails(request, response);
    }
    
    /**
     * This method will be called when "Add Role" is clicked. It
     * will set the user form and list of Role information.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return JSON string having all permission lists, "false" if fails.
     */
    @RequestMapping(value = "/systemAdmin/adminUserAccount/roleForm.html", method = RequestMethod.GET)
    @ResponseBody
    public final String getSystemAdminUserRoleForm(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.formUserRole(request, response, model);
    }
    
    /**
     * This method saves role information in Role and RolePermission table.
     * 
     * @param webSecurityForm
     *            user input form
     * @param result
     *            Error object
     * @param response
     *            HttpServletResponse object
     * @return "true" + ":" + token string value if process succeeds, JSON error
     *         message if process fails
     */
    @RequestMapping(value = "/systemAdmin/adminUserAccount/saveRole.html", method = RequestMethod.POST)
    @ResponseBody
    public final String saveSystemAdminRole(@ModelAttribute("roleEditForm") final WebSecurityForm<RoleEditForm> webSecurityForm, final BindingResult result,
                                            final HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.saveRole(webSecurityForm, result, response);
    }
    
    /**
     * This method verifies if a role can be deleted
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return "true" string value if process succeeds, "false" string value if
     *         process fails.
     */
    @RequestMapping(value = "/systemAdmin/adminUserAccount/verifyDeleteRole.html", method = RequestMethod.GET)
    @ResponseBody
    public final String verifyDeleteRole(final HttpServletRequest request, final HttpServletResponse response) {
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.verifyDeleteRole(request, response);
        
    }
    
    /**
     * This method deletes role information by setting type to 2 (deleted)
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @return "true" string value if process succeeds, "false" string value if
     *         process fails.
     */
    @RequestMapping(value = "/systemAdmin/adminUserAccount/deleteRole.html", method = RequestMethod.GET)
    @ResponseBody
    public final String deleteSystemAdminRole(final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.deleteRole(request, response);
    }
}
