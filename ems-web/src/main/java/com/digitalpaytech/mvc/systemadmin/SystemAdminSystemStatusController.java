package com.digitalpaytech.mvc.systemadmin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.cardprocessing.CardProcessorFactory;
import com.digitalpaytech.cardprocessing.support.CardTransactionZipProcessor;
import com.digitalpaytech.client.dto.KeyPackage;
import com.digitalpaytech.client.dto.RSAKeyInfo;
import com.digitalpaytech.domain.Cluster;
import com.digitalpaytech.domain.CryptoKey;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.CardProcessorInfo;
import com.digitalpaytech.dto.EncryptionInfo;
import com.digitalpaytech.dto.ProcessorInfo;
import com.digitalpaytech.dto.crypto.HashAlgorithmData;
import com.digitalpaytech.dto.systemadmin.CardProcessingQueueStatus;
import com.digitalpaytech.dto.systemadmin.ClusterMemberInfo;
import com.digitalpaytech.dto.systemadmin.CryptoKeyListInfo;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.scheduling.task.NightlyTransactionRetryProcessingTask;
import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.EncryptionService;
import com.digitalpaytech.service.HashAlgorithmTypeService;
import com.digitalpaytech.service.MailerService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.LabelConstants;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

/**
 * This class handles the system status memu request in the system admin page.
 * (system monitor and server admin tab.)
 * 
 * @author Brian Kim
 * 
 */
@Controller
//PMD: To be refractured later
@SuppressWarnings({ "PMD.GodClass", "PMD.ExcessiveImports" })
public class SystemAdminSystemStatusController {
    
    private static final Logger LOGGER = Logger.getLogger(SystemAdminSystemStatusController.class);
    private static final String PROCESSOR_ID = "processorId";
    private static final String DISPLAY_QUEUE_STATUS = "displayQueueStatus";
    private static final String EMS_MAIN = "EmsMain";
    private static final String MESSAGES = "messages";
    
    @Autowired
    private CardTransactionZipProcessor cardTransactionZipProcessor;
    
    @Autowired
    private EncryptionService encryptionService;
    
    @Autowired
    private ClusterMemberService clusterMemberService;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private CryptoService cryptoService;
    
    @Autowired
    private CardProcessorFactory cardProcessorFactory;
    
    @Autowired
    private NightlyTransactionRetryProcessingTask nightlyTransactionRetryProcessor;
    
    @Autowired
    private MessageHelper messages;
    
    @Autowired
    private HashAlgorithmTypeService hashAlgorithmTypeService;
    
    @Autowired
    private MailerService mailerService;
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    public final CardTransactionZipProcessor getCardTransactionZipProcessor() {
        return this.cardTransactionZipProcessor;
    }
    
    public final void setCardTransactionZipProcessor(final CardTransactionZipProcessor cardTransactionZipProcessor) {
        this.cardTransactionZipProcessor = cardTransactionZipProcessor;
    }
    
    public final EncryptionService getEncryptionService() {
        return this.encryptionService;
    }
    
    public final void setEncryptionService(final EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }
    
    public final ClusterMemberService getClusterMemberService() {
        return this.clusterMemberService;
    }
    
    public final void setClusterMemberService(final ClusterMemberService clusterMemberService) {
        this.clusterMemberService = clusterMemberService;
    }
    
    public final EmsPropertiesService getEmsPropertiesService() {
        return this.emsPropertiesService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final CryptoService getCryptoService() {
        return this.cryptoService;
    }
    
    public final void setCryptoService(final CryptoService cryptoService) {
        this.cryptoService = cryptoService;
    }
    
    public final void setNightlyTransactionRetryProcessor(final NightlyTransactionRetryProcessingTask nightlyTransactionRetryProcessor) {
        this.nightlyTransactionRetryProcessor = nightlyTransactionRetryProcessor;
    }
    
    public final NightlyTransactionRetryProcessingTask getNightlyTransactionRetryProcessor() {
        return this.nightlyTransactionRetryProcessor;
    }
    
    public final CardProcessorFactory getCardProcessorFactory() {
        return this.cardProcessorFactory;
    }
    
    public final void setCardProcessorFactory(final CardProcessorFactory cardProcessorFactory) {
        this.cardProcessorFactory = cardProcessorFactory;
    }
    
    public final MessageHelper getMessages() {
        return this.messages;
    }
    
    public final void setMessages(final MessageHelper messages) {
        this.messages = messages;
    }
    
    public final HashAlgorithmTypeService getHashAlgorithmTypeService() {
        return this.hashAlgorithmTypeService;
    }
    
    public final void setHashAlgorithmTypeService(final HashAlgorithmTypeService hashAlgorithmTypeService) {
        this.hashAlgorithmTypeService = hashAlgorithmTypeService;
    }
    
    public final MailerService getMailerService() {
        return this.mailerService;
    }
    
    public final void setMailerService(final MailerService mailerService) {
        this.mailerService = mailerService;
    }
    
    public final MerchantAccountService getMerchantAccountService() {
        return this.merchantAccountService;
    }
    
    public final void setMerchantAccountService(final MerchantAccountService merchantAccountService) {
        this.merchantAccountService = merchantAccountService;
    }
    
    /**
     * This method will look at the permissions in the WebUser and pass control
     * to the first tab that the user has permissions for.
     * 
     * @return Will return the method for systemMonitor, serverAdmin, or error
     *         if no permissions exist.
     */
    @RequestMapping(value = "/systemAdmin/systemStatus/index.html", method = RequestMethod.GET)
    public final String getFirstPermittedTab(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model)
        throws CryptoException {
        
        if (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_SERVER_STATUS)) {
            return getSystemMonitor(request, response, model);
        } else if (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_SERVER_OPERATIONS)) {
            return getServerAdmin(request, response, model);
        } else {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
    }
    
    /**
     * This method retrieves information on encryption services and display it on system monitor page.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return mapping vm file name (/systemAdmin/systemMonitor.vm) if process succeeds, null with response status 401 if fails.
     */
    @RequestMapping(value = "/systemAdmin/systemStatus/systemMonitor.html", method = RequestMethod.GET)
    public final String getSystemMonitor(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        /* validate if login user has enough permission. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_SERVER_ADMINISTRATION)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        /* retrieve encryption services information. */
        final EncryptionInfo encryptionInfo = this.encryptionService.getEncryptionInfo();
        model.put("encryptionInfo", encryptionInfo);
        model.put("clusterName", this.clusterMemberService.getClusterName());
        model.put("emsVersion", this.emsPropertiesService.getPropertyValue(EmsPropertiesService.EMS_APPLICATION_ID,
                                                                           EmsPropertiesService.DEFAULT_EMS_APPLICATION_ID, true));
        
        return "/systemAdmin/systemStatus/systemMonitor";
    }
    
    /**
     * This method retrieves information on server admin and display it on server admin page.
     * 
     * @param request
     *            HttpServletRequest object
     * @param response
     *            HttpServletResponse object
     * @param model
     *            ModelMap object
     * @return mapping vm file name (/systemAdmin/serverAdmin.vm) if process succeeds, null with response status 401 if fails.
     * @throws CryptoException
     */
    @RequestMapping(value = "/systemAdmin/systemStatus/serverAdmin.html", method = RequestMethod.GET)
    public final String getServerAdmin(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model)
        throws CryptoException {
        
        /* validate if login user has enough permission. */
        if (!(WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_SERVER_STATUS)
              || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_SERVER_OPERATIONS))) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        /* load all cluster member */
        final Collection<Cluster> members = this.clusterMemberService.getAllClusterMembers();
        final Collection<ClusterMemberInfo> clusterMembers = converToClusterMemberInfo(members);
        final boolean isServerRolePrimary = this.isThisClusterRolePrimary(clusterMembers);
        
        /* determine if this queue status is shown on the page or not. */
        if (this.emsPropertiesService.getPropertyValueAsBoolean(EmsPropertiesService.CARD_PROCESSING_QUEUE_STATUS, false, true)) {
            model.put(DISPLAY_QUEUE_STATUS, WebCoreConstants.RESPONSE_TRUE);
        } else {
            model.put(DISPLAY_QUEUE_STATUS, WebCoreConstants.RESPONSE_FALSE);
        }
        
        final CryptoKeyListInfo cryptoKeyListInfo = this.cryptoService.listActiveKeys(CryptoService.PURPOSE_CREDIT_CARD_REMOTE);
        
        model.put("cryptoKeyListInfo", cryptoKeyListInfo);
        model.put("clusterMembers", clusterMembers);
        model.put("currentClusterName", this.clusterMemberService.getClusterName());
        model.put("isServerRolePrimary", isServerRolePrimary);
        
        model.put("cardRetryStatusEntries", createCardProcessingQueueStatus(request));
        model.put("settlementQueueSize", this.nightlyTransactionRetryProcessor.getCardProcessingMaster().getSettlementQueueSize());
        model.put("storeForwardQueueSize", this.nightlyTransactionRetryProcessor.getCardProcessingMaster().getStoreForwardQueueSize());
        model.put("reversalQueueSize", this.nightlyTransactionRetryProcessor.getCardProcessingMaster().getReversalQueueSize());
        
        return "/systemAdmin/systemStatus/serverAdmin";
    }
    
    /**
     * This method promotes the cluster member information (primary or secondary) based on user input.
     * 
     * @param request
     *            HttpServletRequest object
     * @return JSON string of cluster member list if process succeeds, WebCoreConstants.RESPONSE_FALSE if fails.
     */
    @RequestMapping(value = "/systemAdmin/systemStatus/promoteClusterMember.html", method = RequestMethod.GET)
    @ResponseBody
    public final String promoteClusterMember(final HttpServletRequest request) {
        
        /* validate if login user has enough permission. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_SERVER_OPERATIONS)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String newPrimaryServer = request.getParameter("ProcessPromoteServer");
        if (newPrimaryServer != null) {
            if (this.clusterMemberService.isClusterMember(newPrimaryServer)) {
                LOGGER.info("Changing primary server to " + newPrimaryServer);
                this.emsPropertiesService.updatePrimaryServer(newPrimaryServer);
            } else {
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    @RequestMapping(value = "/systemAdmin/systemStatus/processUploadedFile.html", method = RequestMethod.GET)
    @ResponseBody
    public final String processUploadedFile() {
        
        /* validate if login user has enough permission. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_SERVER_OPERATIONS)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Collection<Cluster> members = this.clusterMemberService.getAllClusterMembers();
        final Collection<ClusterMemberInfo> clusterMembers = converToClusterMemberInfo(members);
        final boolean isServerRolePrimary = this.isThisClusterRolePrimary(clusterMembers);
        if (!isServerRolePrimary) {
            final MessageInfo message = new MessageInfo(false, this.messages
                    .getMessageWithKeyParams(LabelConstants.ALERT_OPERATION_NONPRIMARY_NOT_ALLOW, "button.sysAdmin.processUploadedFiles"));
            message.setError(true);
            
            return WidgetMetricsHelper.convertToJson(message, MESSAGES, true);
        }
        
        this.cardTransactionZipProcessor.addUnprocessedFileToQueue();
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    @RequestMapping(value = "/systemAdmin/systemStatus/processCardRetry.html", method = RequestMethod.GET)
    @ResponseBody
    public final String processCardRetry() {
        
        /* validate if login user has enough permission. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_SERVER_OPERATIONS)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Collection<Cluster> members = this.clusterMemberService.getAllClusterMembers();
        final Collection<ClusterMemberInfo> clusterMembers = converToClusterMemberInfo(members);
        final boolean isServerRolePrimary = this.isThisClusterRolePrimary(clusterMembers);
        if (!isServerRolePrimary) {
            final MessageInfo message = new MessageInfo(false,
                    this.messages.getMessageWithKeyParams(LabelConstants.ALERT_OPERATION_NONPRIMARY_NOT_ALLOW, "button.sysAdmin.processCardRetry"));
            message.setError(true);
            
            return WidgetMetricsHelper.convertToJson(message, MESSAGES, true);
        }
        
        /* process nightly transaction retries. */
        this.nightlyTransactionRetryProcessor.processNightlyTransactionRetries();
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    /**
     * This method will process the shut-down of Tomcat cluster by the user request.
     * 
     * @return "true" if the process succeeds, "false" if fails.
     */
    @RequestMapping(value = "/systemAdmin/systemStatus/shutdownTomcat.html", method = RequestMethod.GET)
    @ResponseBody
    public final String shutDownTomcatServer() {
        
        /* validate if login user has enough permission. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_SERVER_OPERATIONS)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        LOGGER.info("%%%% TOMCAT SERVER IS ABOUT TO SHUT DOWN. %%%%");
        
        this.nightlyTransactionRetryProcessor.getCardProcessingMaster().checkProcessingQueueAndStopServer();
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    @RequestMapping(value = "/systemAdmin/systemStatus/refreshQueueStatus.html", method = RequestMethod.GET)
    @ResponseBody
    public final String refreshQueueStatus(final HttpServletRequest request) {
        
        /* validate if login user has enough permission. */
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_SERVER_OPERATIONS)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final CardProcessingQueueStatus cardProcessingQueueStatus = new CardProcessingQueueStatus();
        cardProcessingQueueStatus.setCardRetryStatusEntries(createCardProcessingQueueStatus(request));
        cardProcessingQueueStatus.setSettlementQueueSize(this.nightlyTransactionRetryProcessor.getCardProcessingMaster().getSettlementQueueSize());
        cardProcessingQueueStatus
                .setStoreForwardQueueSize(this.nightlyTransactionRetryProcessor.getCardProcessingMaster().getStoreForwardQueueSize());
        cardProcessingQueueStatus.setReversalQueueSize(this.nightlyTransactionRetryProcessor.getCardProcessingMaster().getReversalQueueSize());
        
        return WidgetMetricsHelper.convertToJson(cardProcessingQueueStatus, "cardProcessingQueueStatus", true);
    }
    
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    private List<CardProcessorInfo> createCardProcessingQueueStatus(final HttpServletRequest request) {
        final Set<Entry<Integer, Integer>> sets = this.nightlyTransactionRetryProcessor.getAllCardRetryQueueSize().entrySet();
        final List<CardProcessorInfo> result = new ArrayList<CardProcessorInfo>(sets.size());
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        for (Entry<Integer, Integer> set : sets) {
            final ProcessorInfo procInfo = this.cardProcessorFactory.getProcessorInfo(set.getKey());
            final boolean isPaused = this.cardProcessorFactory.isProcessorPaused(set.getKey());
            result.add(new CardProcessorInfo(procInfo.getAlias(), String.valueOf(set.getValue()),
                    keyMapping.getRandomString(procInfo, WebCoreConstants.ID_LOOK_UP_NAME), String.valueOf(isPaused), procInfo.isLink()));
        }
        return result;
    }
    
    /**
     * This method converts ClusterMember domain object collection to ClusterMemberInfo object
     * to display in UI.
     * 
     * @param members
     *            collection of ClusterMember domain object
     * @return collection of ClusterMemberInfo object
     */
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    private Collection<ClusterMemberInfo> converToClusterMemberInfo(final Collection<Cluster> members) {
        
        final Collection<ClusterMemberInfo> infos = new ArrayList<ClusterMemberInfo>();
        final String cardProcessingServer =
                this.emsPropertiesService.getPropertyValue(EmsPropertiesService.SCHEDULED_CARD_PROCESS_SERVER, EMS_MAIN, true);
        final String autoCrytoServer =
                this.emsPropertiesService.getPropertyValue(EmsPropertiesService.AUTO_CRYPTO_KEY_PROCESS_SERVER, EMS_MAIN, true);
        if (members != null && !members.isEmpty()) {
            for (Cluster member : members) {
                final boolean primary = cardProcessingServer.equals(member.getName()) && autoCrytoServer.equals(member.getName());
                infos.add(new ClusterMemberInfo(member.getName(), primary));
            }
        }
        return infos;
    }
    
    /**
     * This method checks if the current Tomcat cluster has the private role.
     * 
     * @param clusterMembers
     *            ClusterMemberInfo collection
     * @return true if the current Tomcat cluster has the private role, false if not.
     */
    private boolean isThisClusterRolePrimary(final Collection<ClusterMemberInfo> clusterMembers) {
        boolean result = false;
        final String serverName = this.clusterMemberService.getClusterName();
        for (ClusterMemberInfo info : clusterMembers) {
            if (info.getName().equals(serverName)) {
                result = info.isPrimary();
                break;
            }
        }
        
        return result;
    }
    
    @RequestMapping(value = "/systemAdmin/systemStatus/pauseCardProcessing.html", method = RequestMethod.GET)
    @ResponseBody
    public final String pauseCardProcessing(final HttpServletRequest request) {
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_SERVER_OPERATIONS)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final boolean changedFlag = pauseOrResumeCardProcessing(request, true);
        if (changedFlag) {
            sendNotificationEmail(WebSecurityUtil.getUserAccount().getUserName(), getId(request, request.getParameter(PROCESSOR_ID)),
                                  "card.processor.notifications.email.paused");
            
            return WebCoreConstants.RESPONSE_TRUE;
        }
        return WebCoreConstants.RESPONSE_FALSE;
    }
    
    @RequestMapping(value = "/systemAdmin/systemStatus/resumeCardProcessing.html", method = RequestMethod.GET)
    @ResponseBody
    public final String resumeCardProcessing(final HttpServletRequest request) {
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_SERVER_OPERATIONS)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final boolean changedFlag = pauseOrResumeCardProcessing(request, false);
        if (changedFlag) {
            sendNotificationEmail(WebSecurityUtil.getUserAccount().getUserName(), getId(request, request.getParameter(PROCESSOR_ID)),
                                  "card.processor.notifications.email.resumed");
            
            return WebCoreConstants.RESPONSE_TRUE;
        }
        return WebCoreConstants.RESPONSE_FALSE;
    }
    
    private boolean pauseOrResumeCardProcessing(final HttpServletRequest request, final boolean pauseFlag) {
        boolean okFlag = false;
        final String randomId = request.getParameter(PROCESSOR_ID);
        if (StringUtils.isNotBlank(randomId)) {
            final int id = getId(request, randomId);
            if (pauseFlag) {
                okFlag = this.cardProcessorFactory.pauseProcessor(id);
            } else if (!pauseFlag) {
                okFlag = this.cardProcessorFactory.resumeProcessor(id);
            }
        }
        return okFlag;
    }
    
    private void sendNotificationEmail(final String userAccount, final int processorId, final String pausedOrResumedKey) {
        final String emailAddress = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.PROD_SUPPORT_EMAIL_ADDRESS,
                                                                               EmsPropertiesService.PROD_SUPPORT_EMAIL_ADDRESS_STRING);
        
        final List<String> nameList = this.merchantAccountService.findCustomerNamesByProcessorId(processorId);
        StringBuilder nameBdr = new StringBuilder();
        for (String s : nameList) {
            nameBdr.append(s).append(", ");
        }
        if (!nameList.isEmpty()) {
            nameBdr = nameBdr.replace(nameBdr.lastIndexOf(","), nameBdr.length(), "");
        }
        final StringBuilder bdr = new StringBuilder();
        bdr.append(this.messages
                .getMessage("card.processor.notifications.email",
                            new Object[] { this.messages.getMessage(this.cardProcessorFactory.getProcessorInfo(processorId).getName()),
                                this.messages.getMessage(pausedOrResumedKey), userAccount,
                                DateUtil.getAlertDateString(new Date(), WebCoreConstants.SYSTEM_ADMIN_TIMEZONE), nameBdr.toString(), }));
        
        LOGGER.info(bdr.toString());
        this.mailerService.sendMessage(emailAddress, bdr.toString());
    }
    
    private int getId(final HttpServletRequest request, final String randomId) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        return (Integer) keyMapping.getKey(randomId);
    }
}
