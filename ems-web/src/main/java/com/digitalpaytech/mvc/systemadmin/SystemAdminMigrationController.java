package com.digitalpaytech.mvc.systemadmin;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.domain.CustomerMigration;
import com.digitalpaytech.dto.CustomerMigrationSearchCriteria;
import com.digitalpaytech.dto.PaginatedList;
import com.digitalpaytech.dto.customermigration.CustomerMigrationData;
import com.digitalpaytech.dto.customermigration.RecentBoardedMigratedCustomerList;
import com.digitalpaytech.dto.customermigration.ScheduleMigrationInfo;
import com.digitalpaytech.dto.customermigration.WeeklyMigrationInfo;
import com.digitalpaytech.dto.customermigration.WeeklyMigrationStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.mvc.systemadmin.support.CustomerMigrationRecentlyBoardedFilterForm;
import com.digitalpaytech.mvc.systemadmin.support.CustomerMigrationRecentlyMigratedFilterForm;
import com.digitalpaytech.mvc.systemadmin.support.CustomerMigrationScheduleFilterForm;
import com.digitalpaytech.service.CustomerMigrationService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.systemadmin.CustomerMigrationScheduleFilterValidator;

/**
 * This controller handles back-end logics to display system admin customer
 * migration page.
 * 
 * @author Brian Kim
 * 
 */

@Controller
@SessionAttributes({ "recentlyBoardedFilterForm", "recentlyMigratedFilterForm", "migrationScheduleForm" })
public class SystemAdminMigrationController {
    
    private static final String FORM_MIGRATION = "migrationScheduleForm";
    private static final String FORM_RECENTLY_BOARDED = "recentlyBoardedFilterForm";
    private static final String FORM_RECENTLY_MIGRATED = "recentlyMigratedFilterForm";
    private static final String WIDGET_JSON_OBJECT_NAME = "customerMigrationStatus";
    private static final String WIDGET_JSON_GRAPH_NAME = "customerMigrationData";
    private static final String WIDGET_JSON_CUSTOMER_LIST = "recentBoardedAndMigratedCustomers";
    
    private static final Logger LOGGER = Logger.getLogger(SystemAdminMigrationController.class);
    
    @Autowired
    protected CustomerMigrationService customerMigrationService;
    
    @Autowired
    protected CustomerMigrationScheduleFilterValidator customerMigrationScheduleFilterValidator;
    
    @RequestMapping(value = "/systemAdmin/migration/index.html", method = RequestMethod.GET)
    public final String getMigrationStatus(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_CUSTOMER_MIGRATION_STATUS)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_CUSTOMER_MIGRATION_STATUS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return "/systemAdmin/migration/status";
    }
    
    @RequestMapping(value = "/systemAdmin/migration/schedule.html", method = RequestMethod.GET)
    public final String getMigrationSchedule(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        @ModelAttribute(FORM_MIGRATION) final WebSecurityForm<CustomerMigrationScheduleFilterForm> webSecurityForm) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_CUSTOMER_MIGRATION_STATUS)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_CUSTOMER_MIGRATION_STATUS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final List<CustomerMigration> customerMigrationList = this.customerMigrationService.findThisWeeksSchedule();
        
        model.put("weeklyMigrationStatus", createWeeklyMigrationList(request, customerMigrationList));
        model.put(FORM_MIGRATION, webSecurityForm);
        
        return "/systemAdmin/migration/schedule";
    }
    
    @RequestMapping(value = "/systemAdmin/migration/filterSchedule.html", method = RequestMethod.POST)
    @ResponseBody
    public final String getMigrationScheduleByFilter(
        @ModelAttribute(FORM_MIGRATION) final WebSecurityForm<CustomerMigrationScheduleFilterForm> webSecurityForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_CUSTOMER_MIGRATION_STATUS)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_CUSTOMER_MIGRATION_STATUS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        /* validate user input. */
        this.customerMigrationScheduleFilterValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        final CustomerMigrationScheduleFilterForm filter = webSecurityForm.getWrappedObject();
        if (filter.getDataKey() == null) {
            filter.setDataKey(System.currentTimeMillis());
        }
        
        final CustomerMigrationSearchCriteria criteria = createCriteria(filter);
        
        final PaginatedList<ScheduleMigrationInfo> paginatedMigrationList = new PaginatedList<ScheduleMigrationInfo>(
                createScheduleMigrationList(request,
                                            this.customerMigrationService.findCustomerMigrationByCriteria(criteria,
                                                                                                          WebCoreConstants.SYSTEM_ADMIN_TIMEZONE
                                                                                                                  .getID())), Long.toString(filter
                        .getDataKey()), webSecurityForm.getInitToken(), criteria.getPage());
        
        return WidgetMetricsHelper.convertToJson(paginatedMigrationList, "customerMigrationList", true);
    }
    
    @RequestMapping(value = "/systemAdmin/migration/getCurrentMigrationStatus.html", method = RequestMethod.GET)
    @ResponseBody
    public final String getCurrentMigrationStatus(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_CUSTOMER_MIGRATION_STATUS)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_CUSTOMER_MIGRATION_STATUS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final CustomerMigrationData migrationStatus = this.customerMigrationService.getCurrentCustomerMigrationData();
        return WidgetMetricsHelper.convertToJson(migrationStatus, WIDGET_JSON_OBJECT_NAME, true);
    }
    
    @RequestMapping(value = "/systemAdmin/migration/getCurrentPaystationMigrationStatus.html", method = RequestMethod.GET)
    @ResponseBody
    public final String getCurrentPaystationMigrationStatus(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_CUSTOMER_MIGRATION_STATUS)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_CUSTOMER_MIGRATION_STATUS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final CustomerMigrationData migrationStatus = this.customerMigrationService.getCurrentPaystationMigrationStatus();
        return WidgetMetricsHelper.convertToJson(migrationStatus, WIDGET_JSON_OBJECT_NAME, true);
    }
    
    @RequestMapping(value = "/systemAdmin/migration/getCustomerMigrationData.html", method = RequestMethod.GET)
    @ResponseBody
    public final String getCustomerMigrationData(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_CUSTOMER_MIGRATION_STATUS)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_CUSTOMER_MIGRATION_STATUS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final List<CustomerMigrationData> migrationStatus = this.customerMigrationService.getCustomerMigrationData();
        return WidgetMetricsHelper.convertToJson(migrationStatus, WIDGET_JSON_GRAPH_NAME, true);
    }
    
    @RequestMapping(value = "/systemAdmin/migration/getPayStationMigrationData.html", method = RequestMethod.GET)
    @ResponseBody
    public final String getPayStationMigrationData(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_CUSTOMER_MIGRATION_STATUS)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_CUSTOMER_MIGRATION_STATUS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final List<CustomerMigrationData> migrationStatus = this.customerMigrationService.getCustomerPaystationMigrationData();
        return WidgetMetricsHelper.convertToJson(migrationStatus, WIDGET_JSON_GRAPH_NAME, true);
    }
    
    @RequestMapping(value = "/systemAdmin/migration/getRecentBoardedCustomerList.html", method = RequestMethod.GET)
    @ResponseBody
    public final String getRecentBoardedCustomerList(
        @ModelAttribute(FORM_RECENTLY_BOARDED) final WebSecurityForm<CustomerMigrationRecentlyBoardedFilterForm> webSecurityForm,
        final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_CUSTOMER_MIGRATION_STATUS)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_CUSTOMER_MIGRATION_STATUS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final CustomerMigrationRecentlyBoardedFilterForm form = webSecurityForm.getWrappedObject();
        
        final List<RecentBoardedMigratedCustomerList> boardedCustomers = this.customerMigrationService.getRecentCustomerActivityList(request, form
                .getOrder().name(), form.getColumn().name(), form.getPage(), true);
        
        return WidgetMetricsHelper.convertToJson(boardedCustomers, WIDGET_JSON_CUSTOMER_LIST, true);
    }
    
    @RequestMapping(value = "/systemAdmin/migration/getRecentMigratedCustomerList.html", method = RequestMethod.GET)
    @ResponseBody
    public final String getRecentMigratedCustomerList(
        @ModelAttribute(FORM_RECENTLY_MIGRATED) final WebSecurityForm<CustomerMigrationRecentlyMigratedFilterForm> webSecurityForm,
        final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_CUSTOMER_MIGRATION_STATUS)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_CUSTOMER_MIGRATION_STATUS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final CustomerMigrationRecentlyMigratedFilterForm form = webSecurityForm.getWrappedObject();
        
        final List<RecentBoardedMigratedCustomerList> migratedCustomers = this.customerMigrationService.getRecentCustomerActivityList(request, form
                .getOrder().name(), form.getColumn().name(), form.getPage(), false);
        
        return WidgetMetricsHelper.convertToJson(migratedCustomers, WIDGET_JSON_CUSTOMER_LIST, true);
    }
    
    private WeeklyMigrationStatus createWeeklyMigrationList(final HttpServletRequest request, final List<CustomerMigration> customerMigrationList) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final WeeklyMigrationStatus weeklyMigrationStatus = new WeeklyMigrationStatus();
        final List<WeeklyMigrationInfo> weeklyMigrationInfoList = new ArrayList<WeeklyMigrationInfo>(customerMigrationList.size());
        for (CustomerMigration customerMigration : customerMigrationList) {
            final WeeklyMigrationInfo weeklyMigrationInfo = new WeeklyMigrationInfo();
            weeklyMigrationInfo.setRandomId(keyMapping.getRandomString(customerMigration.getCustomer(), WebCoreConstants.ID_LOOK_UP_NAME));
            weeklyMigrationInfo.setCustomerName(customerMigration.getCustomer().getName());
            weeklyMigrationInfo.setStatus(this.customerMigrationService.getMigrationStatusText(customerMigration.getCustomerMigrationStatusType()
                    .getId()));
            weeklyMigrationInfo.setDayOfWeek(getDayOfWeek(weeklyMigrationStatus, customerMigration));
            weeklyMigrationInfo.setValidationStatus(customerMigration.getCustomerMigrationValidationStatusType() == null
                    ? WebCoreConstants.CUSTOMER_MIGRATION_VALIDATION_STATUS_TYPE_NA : customerMigration.getCustomerMigrationValidationStatusType()
                            .getId());
            
            weeklyMigrationInfoList.add(weeklyMigrationInfo);
        }
        weeklyMigrationStatus.setWeeklyMigrationList(weeklyMigrationInfoList);
        return weeklyMigrationStatus;
    }
    
    private List<ScheduleMigrationInfo> createScheduleMigrationList(final HttpServletRequest request,
        final List<CustomerMigration> customerMigrationList) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final List<ScheduleMigrationInfo> scheduleMigrationInfoList = new ArrayList<ScheduleMigrationInfo>(customerMigrationList.size());
        for (CustomerMigration customerMigration : customerMigrationList) {
            final ScheduleMigrationInfo scheduleMigrationInfo = new ScheduleMigrationInfo();
            scheduleMigrationInfo.setRandomId(keyMapping.getRandomString(customerMigration.getCustomer(), WebCoreConstants.ID_LOOK_UP_NAME));
            scheduleMigrationInfo.setCustomerName(customerMigration.getCustomer().getName());
            scheduleMigrationInfo.setStatus(this.customerMigrationService.getMigrationStatusText(customerMigration.getCustomerMigrationStatusType()
                    .getId()));
            scheduleMigrationInfo.setBoardedDate(customerMigration.getCustomerBoardedGmt() == null ? WebCoreConstants.N_A_STRING : DateUtil
                    .createUIDateOnlyString(customerMigration.getCustomerBoardedGmt(), WebCoreConstants.SYSTEM_ADMIN_TIMEZONE));
            scheduleMigrationInfo.setBoardedTime(customerMigration.getCustomerBoardedGmt() == null ? 0L : customerMigration.getCustomerBoardedGmt()
                    .getTime());
            if (customerMigration.getCustomerMigrationStatusType().getId() < WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMPLETED) {
                scheduleMigrationInfo.setMigrateDate(customerMigration.getMigrationScheduledGmt() == null ? WebCoreConstants.N_A_STRING : DateUtil
                        .createUIDateOnlyString(customerMigration.getMigrationScheduledGmt(), WebCoreConstants.SYSTEM_ADMIN_TIMEZONE));
                scheduleMigrationInfo.setMigrateTime(customerMigration.getMigrationScheduledGmt() == null ? 0L : customerMigration
                        .getMigrationScheduledGmt().getTime());
            } else {
                scheduleMigrationInfo.setMigrateDate(customerMigration.getMigrationEndGmt() == null ? WebCoreConstants.N_A_STRING : DateUtil
                        .createUIDateOnlyString(customerMigration.getMigrationEndGmt(), WebCoreConstants.SYSTEM_ADMIN_TIMEZONE));
                scheduleMigrationInfo.setMigrateTime(customerMigration.getMigrationEndGmt() == null ? 0L : customerMigration.getMigrationEndGmt()
                        .getTime());
            }
            scheduleMigrationInfo.setIsContacted(customerMigration.isIsContacted());
            scheduleMigrationInfo.setUsage(customerMigration.getTotalUserLoggedMinutes());
            scheduleMigrationInfo.setValidationStatus(customerMigration.getCustomerMigrationValidationStatusType() == null
                    ? WebCoreConstants.CUSTOMER_MIGRATION_VALIDATION_STATUS_TYPE_NA : customerMigration.getCustomerMigrationValidationStatusType()
                            .getId());
            scheduleMigrationInfoList.add(scheduleMigrationInfo);
        }
        
        return scheduleMigrationInfoList;
    }
    
    private int getDayOfWeek(final WeeklyMigrationStatus weeklyMigrationStatus, final CustomerMigration customerMigration) {
        Date customerDate = null;
        final int migrationStatus = customerMigration.getCustomerMigrationStatusType().getId();
        if (migrationStatus == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_REQUESTED) {
            customerDate = customerMigration.getMigrationRequestedGmt();
            weeklyMigrationStatus.setRequestedCount(weeklyMigrationStatus.getRequestedCount() + 1);
        } else if (migrationStatus == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SCHEDULED) {
            customerDate = customerMigration.getMigrationScheduledGmt();
            weeklyMigrationStatus.setScheduledCount(weeklyMigrationStatus.getScheduledCount() + 1);
        } else if (migrationStatus == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_CANCELLED
                   || migrationStatus == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SUSPENDED) {
            customerDate = customerMigration.getMigrationCancelGmt();
            weeklyMigrationStatus.setFailedCount(weeklyMigrationStatus.getFailedCount() + 1);
        } else if (migrationStatus >= WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_QUEUED
                   && migrationStatus <= WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_DATA_MIGRATION_CONFIRMED) {
            customerDate = customerMigration.getMigrationStartGmt();
            weeklyMigrationStatus.setRunningCount(weeklyMigrationStatus.getRunningCount() + 1);
        } else if (migrationStatus >= WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMPLETED
                   && migrationStatus <= WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_ALL_PAYSTATIONS_MIGRATED) {
            customerDate = customerMigration.getMigrationEndGmt();
            weeklyMigrationStatus.setMigratedCount(weeklyMigrationStatus.getMigratedCount() + 1);
        } else if (migrationStatus == WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_BOARDED) {
            customerDate = customerMigration.getCustomerBoardedGmt();
            weeklyMigrationStatus.setBoardedCount(weeklyMigrationStatus.getBoardedCount() + 1);
        } else {
            customerDate = customerMigration.getCreatedGmt();
        }
        
        final Calendar customerDateCal = Calendar.getInstance();
        customerDateCal.setTime(customerDate);
        customerDateCal.setTimeZone(WebCoreConstants.SYSTEM_ADMIN_TIMEZONE);
        
        return customerDateCal.get(Calendar.DAY_OF_WEEK);
    }
    
    private CustomerMigrationSearchCriteria createCriteria(final CustomerMigrationScheduleFilterForm form) {
        
        final CustomerMigrationSearchCriteria criteria = new CustomerMigrationSearchCriteria();
        final List<Byte> customerMigrationStatusTypeIdList = new ArrayList<Byte>();
        if (form.getIsBoarded()) {
            customerMigrationStatusTypeIdList.add(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_BOARDED);
        }
        if (form.getIsRequested()) {
            customerMigrationStatusTypeIdList.add(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_REQUESTED);
            customerMigrationStatusTypeIdList.add(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_CANCELLED);
            customerMigrationStatusTypeIdList.add(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SUSPENDED);
        }
        if (form.getIsScheduled()) {
            customerMigrationStatusTypeIdList.add(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_SCHEDULED);
            customerMigrationStatusTypeIdList.add(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_QUEUED);
            customerMigrationStatusTypeIdList.add(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_EMS6_DISABLED);
            customerMigrationStatusTypeIdList.add(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_WAITING_DATA_MIGRATION);
            customerMigrationStatusTypeIdList.add(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_DATA_MIGRATION_CONFIRMED);
        }
        if (form.getIsMigrated()) {
            customerMigrationStatusTypeIdList.add(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMPLETED);
            customerMigrationStatusTypeIdList.add(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_COMMUNICATION_CHECKED);
            customerMigrationStatusTypeIdList.add(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_ALL_PAYSTATIONS_MIGRATED);
        }
        if (form.getIsEMS6()) {
            customerMigrationStatusTypeIdList.add(WebCoreConstants.CUSTOMER_MIGRATION_STATUS_TYPE_NOT_BOARDED);
        }
        criteria.setCustomerMigrationStatusTypeIds(customerMigrationStatusTypeIdList);
        
        criteria.setExclusiveOrderBy(form.getColumn());
        criteria.setExclusiveOrderDir(form.getOrder());
        
        criteria.setPage(form.getPage());
        
        final Calendar clearedGmt = Calendar.getInstance();
        clearedGmt.setTimeInMillis(form.getDataKey());
        
        return criteria;
    }
    
    @ModelAttribute(FORM_MIGRATION)
    public final WebSecurityForm<CustomerMigrationScheduleFilterForm> initMigrationForm(final HttpServletRequest request) {
        return new WebSecurityForm<CustomerMigrationScheduleFilterForm>((Object) null, new CustomerMigrationScheduleFilterForm(), SessionTool
                .getInstance(request).locatePostToken(FORM_MIGRATION));
    }
    
    @ModelAttribute(FORM_RECENTLY_BOARDED)
    public final WebSecurityForm<CustomerMigrationRecentlyBoardedFilterForm> initMigrationFormRecentlyBoarded(final HttpServletRequest request) {
        return new WebSecurityForm<CustomerMigrationRecentlyBoardedFilterForm>((Object) null, new CustomerMigrationRecentlyBoardedFilterForm(),
                SessionTool.getInstance(request).locatePostToken(FORM_RECENTLY_BOARDED));
    }
    
    @ModelAttribute(FORM_RECENTLY_MIGRATED)
    public final WebSecurityForm<CustomerMigrationRecentlyMigratedFilterForm> initMigrationFormRecentlyMigrated(final HttpServletRequest request) {
        return new WebSecurityForm<CustomerMigrationRecentlyMigratedFilterForm>((Object) null, new CustomerMigrationRecentlyMigratedFilterForm(),
                SessionTool.getInstance(request).locatePostToken(FORM_RECENTLY_MIGRATED));
    }
    
}
