package com.digitalpaytech.mvc.systemadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.dto.FlexLocationFacilitySearchCriteria;
import com.digitalpaytech.dto.FlexLocationFacilitySelectorDTO;
import com.digitalpaytech.dto.PaginatedList;
import com.digitalpaytech.mvc.FlexLocationFacilityListController;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.LocationIntegrationSearchForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
public class SystemAdminFlexLocationFacilityController extends FlexLocationFacilityListController {
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    @ResponseBody
    @RequestMapping(value = "/systemAdmin/workspace/locations/flexLocationFacilitySelectors.html", method = RequestMethod.GET)
    public final String flexLocationFacilitySelectors(@ModelAttribute(FORM_SEARCH) final WebSecurityForm<LocationIntegrationSearchForm> form
            , final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS) && 
                !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }        
        
        final FlexLocationFacilitySearchCriteria criteria = super
                .populateFlexLocationFacilitySearchCriteria(request, form.getWrappedObject(), this.commonControllerHelper.resolveCustomerId(request, response), false);
        
        final PaginatedList<FlexLocationFacilitySelectorDTO> result = new PaginatedList<FlexLocationFacilitySelectorDTO>();
        if (WebSecurityUtil.hasSystemAdminUserValidPermission(WebCoreConstants.SUBSCRIPTION_TYPE_FLEX_INTEGRATION)) {
            result.setElements(this.flexWSService.findFlexLocationFacilitySelector(criteria));
            result.setDataKey(Long.toString(criteria.getMaxUpdatedTime().getTime(), WebCoreConstants.PAGED_LIST_MAXUPDATETIME_LONG_LENGTH));
        }
        
        return WidgetMetricsHelper.convertToJson(result, "flexLocationFacilityList", true);
    }
    
}

