package com.digitalpaytech.mvc.systemadmin;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.domain.CaseCustomerAccount;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.SearchCustomerResult;
import com.digitalpaytech.dto.SearchCustomerResultSetting;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.CaseService;
import com.digitalpaytech.service.CaseSynchronizationHelper;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
@SuppressWarnings({ "PMD.ExcessiveImports" })
public class SystemAdminCaseAccountController {
    
    @Autowired
    private CaseService caseService;
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private CaseSynchronizationHelper caseSynchronizationHelper;
    
    public final CaseService getCaseService() {
        return this.caseService;
    }
    
    public final void setCaseService(final CaseService caseService) {
        this.caseService = caseService;
    }
    
    public final CommonControllerHelper getCommonControllerHelper() {
        return this.commonControllerHelper;
    }
    
    public final void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }
    
    public final CustomerService getCustomerService() {
        return this.customerService;
    }
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final CaseSynchronizationHelper getCaseSynchronizationHelper() {
        return this.caseSynchronizationHelper;
    }
    
    public final void setCaseSynchronizationHelper(final CaseSynchronizationHelper caseSynchronizationHelper) {
        this.caseSynchronizationHelper = caseSynchronizationHelper;
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/licenses/viewCaseAccount.html", method = RequestMethod.GET)
    public final String viewCaseAccount(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model,
        final String successURL) {
        if (!canView()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        this.commonControllerHelper.insertCustomerInModelForSystemAdmin(request, model);
        
        final CaseCustomerAccount caseCustomerAccount = this.caseService.findCaseCustomerAccountByCustomerId(this.commonControllerHelper
                .resolveCustomerId(request, response));
        if (caseCustomerAccount != null) {
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            final SearchCustomerResultSetting searchCustomerResultSetting = new SearchCustomerResultSetting();
            searchCustomerResultSetting.setName(caseCustomerAccount.getCaseAccountName());
            searchCustomerResultSetting.setRandomId(keyMapping.getRandomString(CaseCustomerAccount.class, caseCustomerAccount.getId()));
            model.put("caseCustomer", searchCustomerResultSetting);
            
        }
        
        return "systemAdmin/workspace/licenses/caseCredentials";
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/licenses/searchCaseAccounts.html", method = RequestMethod.GET)
    @ResponseBody
    //PMD.AvoidInstantiatingObjectsInLoops: Required
    @SuppressWarnings({ "PMD.AvoidInstantiatingObjectsInLoops" })
    public final String searchCaseAccounts(final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!canManage()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String search = request.getParameter(FormFieldConstants.PARAMETER_SEARCH);
        
        final SearchCustomerResult searchCustomerResult = new SearchCustomerResult();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        if (!StringUtils.isBlank(search)) {
            if (!search.matches(WebCoreConstants.REGEX_PRINTABLE_TEXT)) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            if (this.caseService.isCaseCustomerAccountListEmpty()) {
                this.caseSynchronizationHelper.syncCaseAccounts();
            }
            
            final List<CaseCustomerAccount> accountList = this.caseService.findCaseCustomerAccountByNameKeyword(search);
            for (CaseCustomerAccount caseCustomerAccount : accountList) {
                final SearchCustomerResultSetting searchCustomerResultSetting = new SearchCustomerResultSetting(
                        keyMapping.getRandomString(CaseCustomerAccount.class, caseCustomerAccount.getId()), caseCustomerAccount.getCaseAccountName());
                if (caseCustomerAccount.getCustomer() != null) {
                    searchCustomerResultSetting.setCustomerName(caseCustomerAccount.getCustomer().getName());
                }
                searchCustomerResult.addCustomerSearchResult(searchCustomerResultSetting);
            }
        }
        
        return WidgetMetricsHelper.convertToJson(searchCustomerResult, "searchCustomerResult", true);
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/licenses/saveCaseAccounts.html", method = RequestMethod.GET)
    @ResponseBody
    public final String saveCaseAccounts(final HttpServletRequest request, final HttpServletResponse response) {
        
        if (!canManage()) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final String randomId = request.getParameter(FormFieldConstants.PARAMETER_RANDOM_ID);
        if (randomId == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer caseCustomerAccountId = keyMapping.getKey(randomId, CaseCustomerAccount.class, Integer.class);
        if (caseCustomerAccountId == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final CaseCustomerAccount caseCustomerAccount = this.caseService.findCaseCustomerAccountById(caseCustomerAccountId);
        if (caseCustomerAccount == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final Customer customer = this.customerService.findCustomer(this.commonControllerHelper.resolveCustomerId(request, response));
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final Date currentDate = DateUtil.getCurrentGmtDate();
        
        final CaseCustomerAccount oldCaseCustomerAccount = this.caseService.findCaseCustomerAccountByCustomerId(customer.getId());
        if (oldCaseCustomerAccount != null && oldCaseCustomerAccount.getId().intValue() != caseCustomerAccount.getId().intValue()) {
            oldCaseCustomerAccount.setCustomer(null);
            oldCaseCustomerAccount.setLastModifiedByUserId(userAccount.getId());
            oldCaseCustomerAccount.setLastModifiedGmt(currentDate);
            this.caseService.updateCaseCustomerAccount(oldCaseCustomerAccount);
        }
        
        caseCustomerAccount.setCustomer(customer);
        caseCustomerAccount.setLastModifiedByUserId(userAccount.getId());
        caseCustomerAccount.setLastModifiedGmt(currentDate);
        this.caseService.updateCaseCustomerAccount(caseCustomerAccount);
        
        this.caseSynchronizationHelper.syncCaseFacilities();
        this.caseSynchronizationHelper.syncCaseLots();
        

        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    private boolean canManage() {
        return WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_MANAGE_CASE);
    }
    
    private boolean canView() {
        return WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_VIEW_CASE) || canManage();
    }
}
