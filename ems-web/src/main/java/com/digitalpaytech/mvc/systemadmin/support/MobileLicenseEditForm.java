package com.digitalpaytech.mvc.systemadmin.support;

import java.io.Serializable;

import javax.persistence.Transient;

public class MobileLicenseEditForm implements Serializable{

    private static final long serialVersionUID = 7081963194543820343L;
    
    private String customerId;
    private String appId;
    private int licenseCount;
    @Transient
    private transient Integer customerRealId;
    @Transient
    private transient Integer appRealId;
    public String getCustomerId(){
        return customerId;
    }
    public void setCustomerId(String customerId){
        this.customerId = customerId;
    }
    public String getAppId(){
        return appId;
    }
    public void setAppId(String appId){
        this.appId = appId;
    }
    public int getLicenseCount(){
        return licenseCount;
    }
    public void setLicenseCount(int licenseCount){
        this.licenseCount = licenseCount;
    }
    public Integer getCustomerRealId(){
        return customerRealId;
    }
    public void setCustomerRealId(Integer customerRealId){
        this.customerRealId = customerRealId;
    }
    public Integer getAppRealId(){
        return appRealId;
    }
    public void setAppRealId(Integer appRealId){
        this.appRealId = appRealId;
    }
}
