package com.digitalpaytech.mvc.systemadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
public class SystemAdminPayStationIndexController {
    
    @Autowired
    private SystemAdminPayStationListController payStationListController;
    @Autowired
    private SystemAdminPayStationPlacementController payStationPlacementController;
    
    public final void setCustomerAdminRoleSettingController(final SystemAdminPayStationListController systemAdminPayStationListController) {
        this.payStationListController = systemAdminPayStationListController;
    }
    
    public final void setCustomerAdminUserAccountSettingController(final SystemAdminPayStationPlacementController systemAdminPayStationPlacementController) {
        this.payStationPlacementController = systemAdminPayStationPlacementController;
    }
    
    /**
     * This method will look at the permissions in the WebUser and pass control
     * to the first tab that the user has permissions for.
     * 
     * @return Will return the method for payStationPlacement, or payStationList
     *         depending on permissions, or error if no permissions exist.
     */
    @RequestMapping(value = "/systemAdmin/workspace/payStations/index.html")
    public final String getFirstPermittedTab(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (canView()) {
            return this.payStationListController.payStationList(request, response, model);
        } else if (WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATION_PLACEMENT)
                   || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_MANAGE_PAYSTATION_PLACEMENT)) {
            return this.payStationPlacementController.placedPayStations(request, response, model);
        } else {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
    }
    
    private boolean canView() {
        return WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_VIEW_PAYSTATION)
                || canManage();
    }
    
    private boolean canManage() {
        return WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_MANAGE_PAYSTATION)
                || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_CREATE_PAYSTATIONS)
                || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_SET_BILLING_PARAMETERS)
                || WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_MOVE_PAYSTATION);
    }
}
