package com.digitalpaytech.mvc.systemadmin.support;

import java.io.Serializable;

public class CustomerSearchForm implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -4328826866009912055L;
    private String randomId;

    public CustomerSearchForm() {
        
    }
    
    public CustomerSearchForm(String randomId) {
        this.randomId = randomId;
    }
    
    public String getRandomId() {
        return randomId;
    }

    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }
    
    
}
