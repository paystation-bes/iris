package com.digitalpaytech.mvc.systemadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.digitalpaytech.mvc.ActivityLogController;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

public class SystemAdminActivityLogController extends ActivityLogController {
    
    @RequestMapping(value = "/systemAdmin/global/recentActivity.html")
    public final String getGlobalRecentActivity(final HttpServletResponse response, final HttpServletRequest request, final ModelMap model) {
        
        if (!WebSecurityUtil.permissionAudit(response, WebSecurityConstants.PERMISSION_VIEW_SYSTEM_ACTIVITIES)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return super.getGlobalRecentActivity(response, request, model);
    }
    
    /*
     * @RequestMapping(value = "/systemAdmin/alerts/recentActivity.html")
     * public String getAlertsRecentActivity(HttpServletRequest request, ModelMap model) {
     * return super.getAlertsRecentActivity(request, model);
     * }
     * 
     * @RequestMapping(value = "/systemAdmin/cardSettings/recentActivity.html")
     * public String getCardSettingsRecentActivity(HttpServletRequest request, ModelMap model) {
     * return super.getCardSettingsRecentActivity(request, model);
     * }
     * 
     * @RequestMapping(value = "/systemAdmin/locations/recentActivity.html")
     * public String getLocationsRecentActivity(HttpServletRequest request, ModelMap model) {
     * return super.getLocationsRecentActivity(request, model);
     * }
     * 
     * @RequestMapping(value = "/systemAdmin/routes/recentActivity.html")
     * public String getRoutesRecentActivity(HttpServletRequest request, ModelMap model) {
     * return super.getRoutesRecentActivity(request, model);
     * }
     * 
     * @RequestMapping(value = "/systemAdmin/users/recentActivity.html")
     * public String getUsersRecentActivity(HttpServletRequest request, ModelMap model) {
     * return super.getUsersRecentActivity(request, model);
     * }
     */
    
    //	@RequestMapping(value = "/systemAdmin/getActivityLog.html")
    //	public @ResponseBody String getActivityLog(@ModelAttribute(WebSecurityConstants.WEB_SECURITY_FORM) WebSecurityForm webSecurityForm,
    //					BindingResult result, HttpServletRequest request, HttpServletResponse response, ModelMap model) {
    //		
    //    if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_SYSTEM_ACTIVITIES)) {
    //        response.setStatus(HttpStatus.UNAUTHORIZED.value());
    //        return WebCoreConstants.RESPONSE_FALSE;
    //    }
    //    
    //
    //		return super.getActivityLog(webSecurityForm, result, request, response, model);
    //	}	
}
