package com.digitalpaytech.mvc.systemadmin;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.cxf.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.WebServiceEndPoint;
import com.digitalpaytech.domain.WebServiceEndPointType;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.mvc.systemadmin.support.SOAPAccountEditForm;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.WebServiceEndPointService;
import com.digitalpaytech.service.WebServiceEndPointTypeService;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.systemadmin.SOAPAccountValidator;

@Controller
public class SOAPAccountController {
    public static final String FORM_EDIT = "soapAccountEditForm";
    
    @Autowired
    protected WebServiceEndPointService webServiceEndPointService;
    
    @Autowired
    protected CustomerService customerService;
    
    @Autowired
    protected SOAPAccountValidator soapAccountValidator;
    
    @Autowired
    private WebServiceEndPointTypeService webServiceEndPointTypeService;
    
    public final void setWebServiceEndPointTypeService(final WebServiceEndPointTypeService webServiceEndPointTypeService) {
        this.webServiceEndPointTypeService = webServiceEndPointTypeService;
    }
    
    public final void setWebServiceEndPointService(final WebServiceEndPointService webServiceEndPointService) {
        this.webServiceEndPointService = webServiceEndPointService;
    }
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final void setSoapAccountValidator(final SOAPAccountValidator soapAccountValidator) {
        this.soapAccountValidator = soapAccountValidator;
    }
    
    protected final String addAccount(final SOAPAccountEditForm.Category category, final WebSecurityForm<SOAPAccountEditForm> webSecurityForm,
        final HttpServletRequest request, final HttpServletResponse response, final ModelMap model, final String returnUrl) {
        final boolean isPrivate = category == SOAPAccountEditForm.Category.PRIVATE;
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final String customerRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
        final Integer customerId = (Integer) keyMapping.getKey(customerRandomId);
        
        final Customer customer = this.customerService.findCustomer(customerId);
        
        if (!customer.isIsMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON
                   + this.soapAccountValidator.getMessage("error.customer.notMigrated.systemAdmin");
        }
        
        final List<WebServiceEndPointType> endPointTypeList = isPrivate ? this.webServiceEndPointTypeService.loadAllPrivate()
                : this.webServiceEndPointTypeService.loadAll();
        Collections.sort(endPointTypeList, new Comparator<WebServiceEndPointType>() {
            @Override
            public int compare(final WebServiceEndPointType o1, final WebServiceEndPointType o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        
        webSecurityForm.getWrappedObject().setEndPointTypeList(endPointTypeList);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        
        model.put(FORM_EDIT, webSecurityForm);
        return returnUrl;
    }
    
    protected final String saveAccount(final WebSecurityForm<SOAPAccountEditForm> webSecurityForm, final BindingResult result,
        final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        this.soapAccountValidator.validate(webSecurityForm, result);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
            webSecurityForm.resetToken();
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        final SOAPAccountEditForm form = webSecurityForm.getWrappedObject();
        final Customer customer = this.customerService.findCustomer(form.getCustomerRealId());
        
        if (!this.soapAccountValidator.validateSystemAdminMigrationStatus(webSecurityForm, customer)) {
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        webSecurityForm.resetToken();
        
        final String customerRandomId = form.getCustomerId();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer customerId = (Integer) keyMapping.getKey(customerRandomId);
        
        Integer wsepId = null;
        final String randomId = form.getRandomId();
        if (!StringUtils.isEmpty(randomId)) {
            wsepId = (Integer) keyMapping.getKey(randomId);
        }
        
        WebServiceEndPoint wsep = null;
        if (StringUtils.isEmpty(randomId)) {
            wsep = this.webServiceEndPointService.findWebServiceEndPointByToken(form.getToken());
            if (wsep != null) {
                webSecurityForm.setCreateAction(WebSecurityConstants.ACTION_FAILURE);
                webSecurityForm.addErrorStatus(new FormErrorStatus("token", this.soapAccountValidator
                        .getMessage("error.soapAccount.token.dupplicated")));
                return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
            }
            wsep = new WebServiceEndPoint();
        } else {
            wsep = this.webServiceEndPointService.findWebServiceEndById(wsepId);
        }
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final Customer dummyCustomer = new Customer();
        dummyCustomer.setId(customerId);
        wsep.setCustomer(dummyCustomer);
        wsep.setLastModifiedByUserId(userAccount.getId());
        wsep.setLastModifiedGmt(new Date());
        wsep.setToken(form.getToken());
        wsep.setComments(form.getComments());
        final WebServiceEndPointType wsepType = new WebServiceEndPointType();
        wsepType.setId(form.getType().byteValue());
        wsep.setWebServiceEndPointType(wsepType);
        
        this.webServiceEndPointService.saveOrUpdate(wsep);
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    protected final String editAccount(final WebSecurityForm<SOAPAccountEditForm> webSecurityForm, final HttpServletRequest request,
        final HttpServletResponse response, final ModelMap model, final String returnUrl) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final String customerRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
        final Integer customerId = (Integer) keyMapping.getKey(customerRandomId);
        
        final Customer customer = this.customerService.findCustomer(customerId);
        
        if (!customer.isIsMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON
                   + this.soapAccountValidator.getMessage("error.customer.notMigrated.systemAdmin");
        }
        
        final String wsepRandomId = request.getParameter("soapAccountId");
        final String msg = "+++ Invalid soapAccountId in SoapAccountController.updateSoapAccount() GET method. +++";
        
        final Integer wsepId = WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, wsepRandomId, new String[] { msg, msg });
        if (wsepId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final WebServiceEndPoint wsep = this.webServiceEndPointService.findWebServiceEndById(wsepId);
        wsep.setRandomId(wsepRandomId);
        
        populateSOAPAccountEditForm(webSecurityForm.getWrappedObject(), wsep, keyMapping);
        
        final List<WebServiceEndPointType> endPointTypeList = wsep.getWebServiceEndPointType().getIsPrivate() ? this.webServiceEndPointTypeService
                .loadAllPrivate() : this.webServiceEndPointTypeService.loadAll();
        webSecurityForm.getWrappedObject().setEndPointTypeList(endPointTypeList);
        
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_UPDATE);
        
        model.put("soapAccountEditForm", webSecurityForm);
        
        return returnUrl;
    }
    
    private void populateSOAPAccountEditForm(final SOAPAccountEditForm form, final WebServiceEndPoint wsep, final RandomKeyMapping keyMapping) {
        final Customer customer = wsep.getCustomer();
        
        form.setCustomerId(keyMapping.getRandomString(customer, WebCoreConstants.ID_LOOK_UP_NAME));
        form.setRandomId(wsep.getRandomId());
        form.setToken(wsep.getToken());
        form.setType(new Integer(wsep.getWebServiceEndPointType().getId()));
        form.setComments(wsep.getComments());
    }
    
    protected final String deleteAccount(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final String customerRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
        final Integer customerId = (Integer) keyMapping.getKey(customerRandomId);
        
        final Customer customer = this.customerService.findCustomer(customerId);
        
        if (!customer.isIsMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON
                   + this.soapAccountValidator.getMessage("error.customer.notMigrated.systemAdmin");
        }
        
        final String wsepRandomId = request.getParameter("soapAccountId");
        final String msg = "+++ Invalid soapAccountId in SoapAccountController.updateSoapAccount() GET method. +++";
        
        final Integer wsepId = WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, wsepRandomId, new String[] { msg, msg });
        if (wsepId == null) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final WebServiceEndPoint wsep = this.webServiceEndPointService.findWebServiceEndById(wsepId);
        wsep.setLastModifiedByUserId(userAccount.getId());
        wsep.setLastModifiedGmt(new Date());
        
        this.webServiceEndPointService.delete(wsep);
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    @ModelAttribute(FORM_EDIT)
    public final WebSecurityForm<SOAPAccountEditForm> initEditForm(final HttpServletRequest request) {
        return new WebSecurityForm<SOAPAccountEditForm>((Object) null, new SOAPAccountEditForm(), SessionTool.getInstance(request)
                .locatePostToken(FORM_EDIT));
    }
}
