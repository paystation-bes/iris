package com.digitalpaytech.mvc.systemadmin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.WebServiceEndPoint;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.mvc.systemadmin.support.SOAPAccountEditForm;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
public class APIReadAccountController extends SOAPAccountController {
    private static final Logger LOGGER = Logger.getLogger(APIReadAccountController.class);
    private static final String FORM_URL = "/systemAdmin/workspace/customers/include/soapAccountForm";
    
    @RequestMapping(value = "/systemAdmin/workspace/customers/addSoapAccount.html", method = RequestMethod.GET)
    public final String addSoapAccount(@RequestParam("category") final String category, @ModelAttribute(FORM_EDIT) final WebSecurityForm<SOAPAccountEditForm> webSecurityForm, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LICENSES)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LICENSES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        SOAPAccountEditForm.Category cat = SOAPAccountEditForm.Category.valueOf(category);
        if (cat == null) {
            cat = SOAPAccountEditForm.Category.STANDARD;
        }
        
        return addAccount(cat, webSecurityForm, request, response, model, FORM_URL);
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/customers/saveSoapAccount.html", method = RequestMethod.POST)
    @ResponseBody
    public final String saveSoapAccount(@ModelAttribute(FORM_EDIT) final WebSecurityForm<SOAPAccountEditForm> webSecurityForm,
        final BindingResult result, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_LICENSES)
            && !WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LICENSES)) {
            webSecurityForm.addErrorStatus(new FormErrorStatus(FormFieldConstants.PARAMETER_CUSTOMER_ID, super.soapAccountValidator
                    .getMessage("error.user.insufficientPermission")));
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        return saveAccount(webSecurityForm, result, request, response, model);
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/customers/editSoapAccount.html", method = RequestMethod.GET)
    public final String editSoapAccount(@ModelAttribute(FORM_EDIT) final WebSecurityForm<SOAPAccountEditForm> webSecurityForm, final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LICENSES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return editAccount(webSecurityForm, request, response, model, FORM_URL);
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/customers/deleteSoapAccount.html", method = RequestMethod.GET)
    @ResponseBody
    public final String deleteSoapAccount(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LICENSES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return deleteAccount(request, response, model);
        
    }
    
    @RequestMapping(value = "/systemAdmin/workspace/customers/getNewToken.html", method = RequestMethod.GET)
    @ResponseBody
    public final String getNewToken(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        
        if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_LICENSES)) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final String customerRandomId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
        final Integer customerId = (Integer) keyMapping.getKey(customerRandomId);
        
        final Customer customer = super.customerService.findCustomer(customerId);
        
        if (!customer.isIsMigrated()) {
            return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON
                   + this.soapAccountValidator.getMessage("error.customer.notMigrated.systemAdmin");
        }
        
        final String wsepRandomId = request.getParameter("soapAccountId");
        if (wsepRandomId != null) {
            final String msg = "+++ Invalid soapAccountId in SoapAccountController.updateSoapAccount() GET method. +++";
            
            final Integer wsepId = WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, wsepRandomId, new String[] { msg, msg });
            if (wsepId == null) {
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            final WebServiceEndPoint wsep = super.webServiceEndPointService.findWebServiceEndById(wsepId);
            final String token = RandomStringUtils.randomAlphanumeric(WebCoreConstants.VALIDATION_WSENDPOINT_TOKEN_LENGTH);
            LOGGER.debug("Returned token: " + token);
            wsep.setToken(token);
            super.webServiceEndPointService.saveOrUpdate(wsep);
            
            return WebCoreConstants.RESPONSE_TRUE;
        } else {
            final String token = RandomStringUtils.randomAlphanumeric(WebCoreConstants.VALIDATION_WSENDPOINT_TOKEN_LENGTH);
            LOGGER.debug("Existing token: " + token);
            
            return token;
        }
    }
}
