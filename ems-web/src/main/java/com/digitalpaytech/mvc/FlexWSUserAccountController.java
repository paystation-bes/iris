package com.digitalpaytech.mvc;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.digitalpaytech.domain.FlexWSUserAccount;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.exception.T2FlexException;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.FlexHelper;
import com.digitalpaytech.mvc.support.FlexWSUserAccountForm;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.FlexWSService;
import com.digitalpaytech.service.T2FlexWsService;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.FlexWSUserAccountValidator;

@Controller
public class FlexWSUserAccountController {
    private static Logger log = Logger.getLogger(FlexWSUserAccountController.class);
    
    @Autowired
    private CommonControllerHelper controllerHelper;
    
    @Autowired
    private MessageHelper messageHelper;
    
    @Autowired
    private FlexHelper flexHelper;
    
    @Autowired
    private FlexWSService flexWSService;
    
    @Autowired
    private T2FlexWsService t2FlexWsService;
    
    @Autowired
    private FlexWSUserAccountValidator validator;
    
    @Autowired
    private CustomerService customerService;
    
    @SuppressWarnings("checkstyle:designforextension")
    protected String view(final WebSecurityForm<FlexWSUserAccountForm> form
            , final HttpServletRequest request, final HttpServletResponse response, final ModelMap model
            , final String successURL) {
        final Integer customerId = this.controllerHelper.resolveCustomerId(request, response);
        final FlexWSUserAccount flexAccount = this.flexWSService.findFlexWSUserAccountByCustomerId(customerId);
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        this.flexHelper.transform(flexAccount, form.getWrappedObject(), keyMapping);
        
        model.put(FlexHelper.FORM_CREDENTIALS, form);
        
        return successURL;
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    protected String save(final WebSecurityForm<FlexWSUserAccountForm> form
            , final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response) {
        String result;
        this.validator.validate(form, bindingResult);
        form.resetToken();
        if (form.getValidationErrorInfo().getErrorStatus().size() > 0) {
            result = WidgetMetricsHelper.convertToJson(form.getValidationErrorInfo(), ErrorConstants.ERROR_STATUS, true);
        } else {
            final Date now = new Date();
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            
            final MessageInfo messageInfo = new MessageInfo();
            messageInfo.setError(true);
            
            FlexWSUserAccount flexAccount = locateFlexAccount(form.getWrappedObject().getRandomId(), keyMapping);
            final boolean existed = flexAccount != null;
            if (!existed) {
                flexAccount = new FlexWSUserAccount();
            }
            
            this.flexHelper.transform(form.getWrappedObject(), flexAccount);
            if (checkFlexAvailability(flexAccount, messageInfo)) {
                final UserAccount user = WebSecurityUtil.getUserAccount();
                final Integer customerId = this.controllerHelper.resolveCustomerId(request, response);
                
                flexAccount.setCustomer(this.customerService.findCustomer(customerId));
                flexAccount.setLastModifiedByUserId(user.getId());
                flexAccount.setLastModifiedGmt(now);
                
                this.flexWSService.saveFlexAccount(flexAccount);
                messageInfo.setError(false);
            }
            
            messageInfo.setRequireManualClose(false);
            messageInfo.setToken(form.getInitToken());
            result = WidgetMetricsHelper.convertToJson(messageInfo, "messages", true);
        }
        
        return result;
    }
    
    private FlexWSUserAccount locateFlexAccount(final String randomId, final RandomKeyMapping keyMapping) {
        FlexWSUserAccount flexAccount = null;
        if (randomId != null) {
            final Integer accountId = keyMapping.getKey(randomId, FlexWSUserAccount.class, Integer.class);
            if (accountId != null) {
                flexAccount = this.flexWSService.findById(accountId);
            }
        }
        
        return flexAccount;
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    protected String testNew(final WebSecurityForm<FlexWSUserAccountForm> form
            , final BindingResult bindingResult, final HttpServletRequest request, final HttpServletResponse response) {
        String result;
        
        this.validator.validate(form, bindingResult);
        form.resetToken();
        if (form.getValidationErrorInfo().getErrorStatus().size() > 0) {
            result = WidgetMetricsHelper.convertToJson(form.getValidationErrorInfo(), ErrorConstants.ERROR_STATUS, true);
        } else {
            final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
            
            final MessageInfo messageInfo = new MessageInfo();
            
            FlexWSUserAccount flexAccount = locateFlexAccount(form.getWrappedObject().getRandomId(), keyMapping);
            if (flexAccount == null) {
                flexAccount = new FlexWSUserAccount();
            }
            
            this.flexHelper.transform(form.getWrappedObject(), flexAccount);
            
            checkFlexAvailability(flexAccount, messageInfo);
            
            messageInfo.setToken(form.getInitToken());
            result = WidgetMetricsHelper.convertToJson(messageInfo, "messages", true);
        }
        
        return result;
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    protected String test(final String booleanMode, final HttpServletRequest request, final HttpServletResponse response) {
        final MessageInfo messageInfo = new MessageInfo();
        
        final Integer customerId = this.controllerHelper.resolveCustomerId(request, response);
        final FlexWSUserAccount flexAccount = this.flexWSService.findFlexWSUserAccountByCustomerId(customerId);
        if (flexAccount == null) {
            messageInfo.setError(true);
            messageInfo.addMessage(this.messageHelper.getMessage("alert.systemAdmin.workspace.licenses.flexCredentials.missing"));
        } else {
            checkFlexAvailability(flexAccount, messageInfo);
        }
        
        String result = null;
        if ("1".equals(booleanMode)) {
            result = (messageInfo.isError()) ? WebCoreConstants.RESPONSE_FALSE : WebCoreConstants.RESPONSE_TRUE;
        } else {
            result = WidgetMetricsHelper.convertToJson(messageInfo, "messages", true);
        }
        
        return result;
    }
    
    private boolean checkFlexAvailability(final FlexWSUserAccount flexAccount, final MessageInfo messageInfo) {
        boolean available = false;
        if (flexAccount != null) {
            try {
                available = this.t2FlexWsService.isFlexAvailable(flexAccount);
            } catch (T2FlexException t2fe) {
                log.error("Error connecting to Flex Web Service", t2fe);
            }
        }
        
        if ((!available) && (messageInfo != null)) {
            messageInfo.setError(true);
            messageInfo.addMessage(this.messageHelper.getMessage("error.flexCredentials.save.testFailed"));
        }
        
        return available;
    }
    
    @ModelAttribute(FlexHelper.FORM_CREDENTIALS)
    public final WebSecurityForm<FlexWSUserAccountForm> initForm(final HttpServletRequest request) {
        return new WebSecurityForm<FlexWSUserAccountForm>((Object) null, new FlexWSUserAccountForm()
                    , SessionTool.getInstance(request).locatePostToken(FlexHelper.FORM_CREDENTIALS));
    }
}
