package com.digitalpaytech.mvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.digitalpaytech.util.WebCoreConstants;

@Controller
public class AccessDeniedController {

	@RequestMapping(value = "/accessDenied.html")
	public final String processAccessDenied(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
		return "redirect:/secure/j_spring_security_logout?error=" + WebCoreConstants.ERROR_ACCESSDENIED;
	}
}
