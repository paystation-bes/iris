package com.digitalpaytech.mvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SessionPingController {
	
	@RequestMapping(value = "/secure/validateSession.html", method = RequestMethod.GET)
	public @ResponseBody String validateSession(HttpServletRequest request) {
		
		HttpSession session = request.getSession(false);
		
		if (session == null) {
			return "false";
		}
		
		return "true";
	}
}
