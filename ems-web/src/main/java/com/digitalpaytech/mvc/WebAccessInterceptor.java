package com.digitalpaytech.mvc;

import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Component;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;

@Component
public class WebAccessInterceptor extends HandlerInterceptorAdapter implements MessageSourceAware {
    private MessageSourceAccessor messageAccessor;
    // private LicenseAppService licenseAppService;
    private static Logger logger = Logger.getLogger(WebAccessInterceptor.class);
    
    @Value("${oauth.trustedURLs}")
    private String trustedURLs;
    
    // private EncryptionAppService encryptionAppService;
    
    public void setMessageSource(MessageSource messageSource) {
        messageAccessor = new MessageSourceAccessor(messageSource);
    }
    
    public void setTrustedURLs(final String trustedURLs) {
        this.trustedURLs = trustedURLs;
    }
    
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        boolean bReturn = true;
        
        HttpSession httpSession = request.getSession(false);
        
        //REFERRER CHECK REMOVED AND MOVED TO APACHE 
        
        // check for illegal character in query string
        // form post parameters will be filtered in form binding and validation
        String queryString = request.getQueryString();
        if (queryString != null) {
            try {
                // decode query string first
                // firefox sends in encoded url
                queryString = URLDecoder.decode(request.getQueryString(), "UTF-8");
            } catch (Exception e) {
                // redirect to error page
                if (httpSession != null) {
                    httpSession.invalidate();
                    response.sendRedirect(getContextPath(request) + "/error.html");
                    // }
                } else {
                    response.sendRedirect(getContextPath(request) + getLoginPage(httpSession) + "?error=" + WebCoreConstants.ERROR_SESSIONINVALID);
                }
                return false;
            }
            
            if (queryString.contains("\r") || queryString.contains("\n")) {
                // log the query string with warn level.
                logger.warn("+++ WARNING: Any CR or LF characters in : " + request.getRequestURL().toString() + "?" + queryString + " +++");
                
                // if session exists, expire the session.
                String loginPage = getLoginPage(httpSession);
                if (httpSession != null) {
                    httpSession.invalidate();
                }
                
                // redirect to error page
                response.sendRedirect(getContextPath(request) + loginPage + "?error=" + WebCoreConstants.ERROR_SESSIONINVALID);
                
                return false;
            }
            
            if (queryString.indexOf('<') != -1 || queryString.indexOf('>') != -1) {
                // redirect to error page
                if (httpSession != null) {
                    httpSession.invalidate();
                    response.sendRedirect(getContextPath(request) + "/error.html");
                } else {
                    response.sendRedirect(getContextPath(request) + getLoginPage(httpSession) + "?error=" + WebCoreConstants.ERROR_SESSIONINVALID);
                }
                return false;
            }
        }
        
        response.addHeader("Cache-Control", "max-age=0,no-cache,no-store");
        
        return bReturn;
    }
    
    private String getLoginPage(HttpSession httpSession) {
    	String loginPage = "/index.html";
        if (httpSession != null) {
            String buffer = (String) httpSession.getAttribute(WebCoreConstants.SESSIONKEY_LOGIN_PAGE);
            if (buffer != null) {
                loginPage = buffer;
                if((!loginPage.isEmpty()) && (loginPage.charAt(0) != '/')) {
                	loginPage = "/" + loginPage;
                }
            }
        }
        
        return loginPage;
    }
    
    private String getContextPath(HttpServletRequest request) {
    	String result = request.getContextPath();
    	
    	return result;
    }
    
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        String viewName = null;
        if (modelAndView != null) {
            viewName = modelAndView.getViewName();
        }
        
        if (viewName != null && viewName.startsWith("redirect:") && WebUtils.getSessionAttribute(request, WebAttributes.AUTHENTICATION_EXCEPTION) == null) {
            modelAndView.setViewName(viewName);
        }
        
        if (this.trustedURLs == null || this.trustedURLs.isEmpty()) {
            response.addHeader("X-FRAME-OPTIONS", "SAMEORIGIN");
        } else {
            response.addHeader("X-FRAME-OPTIONS", "ALLOW-FROM " + this.trustedURLs);
        }
        
        super.postHandle(request, response, handler, modelAndView);
    }

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		SessionTool.clear();
		super.afterCompletion(request, response, handler, ex);
	}

	protected MessageSourceAccessor getMessageAccessor() {
		return messageAccessor;
	}
}
