package com.digitalpaytech.mvc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.digitalpaytech.cardprocessing.CardProcessorFactory;
import com.digitalpaytech.dto.ProcessorInfo;
import com.digitalpaytech.scheduling.task.NightlyTransactionRetryProcessingTask;
import com.digitalpaytech.service.kpi.KPIService;
import com.digitalpaytech.util.KPIConstants;
import com.digitalpaytech.util.WebCoreConstants;

/**
 * @author Amanda Chong
 */

public class KPIController {
    public static final String URL_VIEW = "/monitoring/kpi";
    
    public static final String ATTR_KPI_DATA = "kpiData";
    
    @Autowired
    private KPIService kpiService;
    
    @Autowired
    private NightlyTransactionRetryProcessingTask nightlyTransactionRetryProcessingTask;
    
    @Autowired
    private CardProcessorFactory cardProcessorFactory;
    
    /**
     * If name and sql is null, nothing is printed
     * If name is present, the past minutes data is printed
     * If sql is present, the database queries will be executed and everything for the next minute will include the data retrieved from the database
     * 
     * @param name
     *            can be specified to only retrieve entries that contains the string
     * @param sql
     *            time out in ms defined to stop the sql queries from running
     * @param model
     * @return
     */
    @RequestMapping(value = "/monitoring/KPI", method = RequestMethod.GET)
    public final String getKPIInfo(@RequestParam(value = "name", required = false) final String name,
        @RequestParam(value = "sql", required = false) final Integer sql, final ModelMap model) {
        if (this.kpiService == null) {
            model.addAttribute(ATTR_KPI_DATA, "KPI disabled.");
            return URL_VIEW;
        }
        final Map<String, Object> kpiInfo = this.kpiService.getKPIInfo();
        String kpiData = "";
        
        if (sql != null) {
            final Future<Void> future = this.kpiService.runSQL();
            try {
                future.get(sql, TimeUnit.SECONDS);
                kpiData = toString(kpiInfo);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                future.cancel(true);
                kpiData = "SQL exceeded " + sql + " s to run.  Aborted.";
                model.addAttribute(ATTR_KPI_DATA, kpiData);
                return URL_VIEW;
            }
        }
        if (name != null) {
            if ("".equals(name)) {
                gatherCardProcessingData(kpiInfo);
                kpiData = toString(kpiInfo);
            } else {
                try {
                    kpiData = this.kpiService.getValue(name);
                } catch (NullPointerException e) {
                    final StringBuilder kpiDataBuffer = new StringBuilder();
                    final List<String> keys = new ArrayList<String>(kpiInfo.keySet());
                    for (String key : keys) {
                        if (key.contains(name)) {
                            kpiDataBuffer.append(key);
                            kpiDataBuffer.append(WebCoreConstants.EQUAL_SIGN);
                            kpiDataBuffer.append(kpiInfo.get(key));
                            kpiDataBuffer.append(WebCoreConstants.NIX_NEXT_LINE_SYMBOL);
                        }
                    }
                    
                    kpiData = kpiDataBuffer.toString();
                }
                if ("".equals(kpiData)) {
                    kpiData = "No data for " + name;
                }
                model.addAttribute(ATTR_KPI_DATA, kpiData);
                return URL_VIEW;
            }
        }
        model.addAttribute(ATTR_KPI_DATA, kpiData);
        return URL_VIEW;
    }
    
    private String toString(final Map<String, Object> map) {
        final StringBuilder ret = new StringBuilder();
        final Iterator<Entry<String, Object>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            final Map.Entry<String, Object> pair = (Map.Entry<String, Object>) iterator.next();
            ret.append(pair.getKey());
            ret.append(WebCoreConstants.EQUAL_SIGN);
            ret.append(pair.getValue());
            ret.append(WebCoreConstants.NIX_NEXT_LINE_SYMBOL);
        }
        return ret.toString();
    }
    
    // Due to the package hierarchy, these methods can only be called in a controller.
    private void gatherCardProcessingData(final Map<String, Object> map) {
        final int settlement = this.nightlyTransactionRetryProcessingTask.getCardProcessingMaster().getSettlementQueueSize();
        final int storeAndForward = this.nightlyTransactionRetryProcessingTask.getCardProcessingMaster().getStoreForwardQueueSize();
        final int reversal = this.nightlyTransactionRetryProcessingTask.getCardProcessingMaster().getReversalQueueSize();
        int total = settlement + storeAndForward + reversal;
        
        map.put(KPIConstants.CARD_PROCESSING_QUEUE_SIZE_SETTLEMENT, settlement);
        map.put(KPIConstants.CARD_PROCESSING_QUEUE_SIZE_STOREANDFORWARD, storeAndForward);
        map.put(KPIConstants.CARD_PROCESSING_QUEUE_SIZE_REVERSAL, reversal);
        
        final Set<Entry<Integer, Integer>> sets = this.nightlyTransactionRetryProcessingTask.getAllCardRetryQueueSize().entrySet();
        for (Entry<Integer, Integer> set : sets) {
            final ProcessorInfo procInfo = this.cardProcessorFactory.getProcessorInfo(set.getKey());
            map.put(KPIConstants.CARD_PROCESSING_QUEUE_SIZE_CARDRETRY
                            + formatNameIfNecessary(procInfo.getAlias().toLowerCase().charAt(0) + procInfo.getAlias().substring(1)),
                    String.valueOf(set.getValue()));
            total += set.getValue();
        }
        map.put(KPIConstants.CARD_PROCESSING_QUEUE_SIZE_TOTAL, total);
    }
    
    private String formatNameIfNecessary(final String original) {
        for (int i = 0; i < KPIConstants.ORIGINAL_NAMES.length; i++) {
            if (KPIConstants.ORIGINAL_NAMES[i].equalsIgnoreCase(original)) {
                return KPIConstants.NEW_NAMES[i];
            }
        }
        return original;
    }
}
