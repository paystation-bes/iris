package com.digitalpaytech.mvc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.data.SectionColumn;
import com.digitalpaytech.domain.Notification;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.Section;
import com.digitalpaytech.domain.Tab;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.dto.TabHeading;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.NotificationService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.util.DashboardUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Controller
// WebSecurityConstants.WEB_SECURITY_FORM is used in 'showLandingPage', widgetController.setNewWidgetSettingsForm.
@SessionAttributes({ WebSecurityConstants.WEB_SECURITY_FORM })
public class MainLandingController extends CommonLandingController {
    
    private static Logger log = Logger.getLogger(MainLandingController.class);
    @Autowired
    NotificationService notificationService;
    
    @Autowired
    private EntityService entityService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private WidgetSettingsController widgetSettingsController;
    
    public void setNotificationService(NotificationService notificationService) {
        this.notificationService = notificationService;
    }
    
    public void setEntityService(EntityService entityService) {
        this.entityService = entityService;
    }
    
    public void setPointOfSaleService(PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public void setWidgetSettingsController(WidgetSettingsController widgetSettingsController) {
        this.widgetSettingsController = widgetSettingsController;
    }
    
    @RequestMapping(value = "/secure/dashboard/index.html")
    public String showLandingPage(HttpServletRequest request, HttpServletResponse response, ModelMap model) {

        if (isSystemAdministrator()) {
            return "/systemAdmin/index";
        }
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                return "redirect:/secure/settings/global/index.html";
            }
        }

        // retrieve user notification.
        List<Notification> notifications = notificationService.findNotificationByEffectiveDate();
        model.put("notifications", notifications);
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        WebUser webUser = (WebUser) authentication.getPrincipal();
        
        List<TabHeading> tabs = getTabs(request, webUser.getUserAccountId());
        model.put("tabheadings", tabs);
        
        Tab landingTab = loadLandingTab(request, tabs);
        model.put("tabTree", landingTab);
        
        PlacementMapBordersInfo mapBorders = this.pointOfSaleService.findMapBordersByCustomerId(webUser.getCustomerId());
        model.put("mapBorders", mapBorders);
        
        return "/dashboard/index";
    }
    
    @RequestMapping(value = "/secure/dashboard/tab.html")
    public String findTab(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD)) {
            if (!WebSecurityUtil.hasUserValidPermission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD)) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
		SessionTool sessionTool = SessionTool.getInstance(request);
        
        Tab tab = null;
        String tabId = request.getParameter("id");
        
        // No tab id in the request, show default one.
        if (StringUtils.isBlank(tabId) || tabId.equals("undefined")) {
            log.info("Show default tab when there is no tab id in the request, id: " + tabId);
            tab = findDefaultTabEager(request);
            
        } else {
			RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
            String tabKeyId = keyMapping.getKey(tabId).toString();
            
            if (tabKeyId.startsWith(WebCoreConstants.TEMP_TAB_ID_PREFIX)) {
				@SuppressWarnings("unchecked")
				Map<String, Tab> tabTreeMap = (Map<String, Tab>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
                tab = tabTreeMap.get(tabId);
            } else {
                int id = Integer.parseInt(tabKeyId);
                tab = dashboardService.findTabByIdEager(id);
                tab.setRandomId(tabId);
                tab = DashboardUtil.setRandomIds(request, tab);
            }
        }
        
        // If couldn't find any tab data, show default one.
        if (tab == null) {
            log.error("Show default tab when couldn't find a tab with id: " + tabId);
            tab = findDefaultTabEager(request);
        }
        
        // If no section within a tab, show error message.
        if (tab.getSections() == null || tab.getSections().size() == 0) {
            model.put("error.no.section", "error.no.section");
            return "/dashboard/index?error";
        }
        
        if (tab.getUserAccount().getId() == WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID) {
            tab = removeMasterList(request, tab, WebSecurityUtil.getUserAccount().getCustomer().isIsParent());
        }
        
        int index = 0, currentTabIndex = 0;
		@SuppressWarnings("unchecked")
		List<TabHeading> tabHeadings = (List<TabHeading>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_HEADINGS);
        for (TabHeading tabHeading : tabHeadings) {
            if (tabHeading.getCurrent().equals("true")) {
                tabHeading.setCurrent("false");
            }
            if (tabHeading.getId().equals(tabId)) {
                tabHeading.setCurrent("true");
                currentTabIndex = index;
            }
            index++;
        }
		sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, tabHeadings);
        
        List<Section> sectionList = new ArrayList<Section>();
        sectionList = tab.getSections();
        Collections.sort(sectionList, new Comparator<Section>() {
            public int compare(Section s1, Section s2) {
                int diff = s1.getOrderNumber() - s2.getOrderNumber();
                return diff;
            }
        });
        
        for (Section s : sectionList) {
            for (int i = 0; i < WebCoreConstants.DEFAULT_NUMBER_OF_COLUMN; i++) {
                SectionColumn sc = s.getSectionColumn(String.valueOf(i));
                List<Widget> wlist = sc.getWidgets();
                Collections.sort(wlist, new Comparator<Widget>() {
                    public int compare(Widget w1, Widget w2) {
                        return w1.getOrderNumber() - w2.getOrderNumber();
                    }
                });
                sc.setWidgets(i, wlist);
            }
        }
        
        tab.setSections(sectionList);
        
        model.put("tabTree", tab);
        DashboardUtil.setCurrentTabCookie(request, response, currentTabIndex);
        return "/dashboard/include/dashboard";
    }
    
    private Tab findDefaultTabEager(HttpServletRequest request) {
        
        List<TabHeading> tabHeadings = getTabs(request, WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
        // Gets first default tab ('Finance') and associated table information.
        return dashboardService.findTabByIdEager(tabHeadings.get(0).getOriginalId());
    }
    
    /**
     * Obtains UserAccount from SecurityContextHolder && Authentication and calls 'isSystemAdministrator' method to verify the role.
     * 
     * @return boolean
     */
    private boolean isSystemAdministrator() {
        UserAccount userAccount = WebSecurityUtil.getUserAccount();
        return isPredefinedRole(userAccount, WebSecurityConstants.ROLE_ID_SYSTEM_ADMIN, WebSecurityConstants.CUSTOMER_TYPE_ID_SYSTEM_ADMIN);
    }
    
    /**
     * For 'DPT Admin', 'Parent Admin' and 'Child Admin' roles attributes have Role.IsAdmin = 1 and Role.IsLocked = 1 and Role.ClonedRoleId = NULL
     * e.g. For core 'locked' DPT admin role: Role.Id = 1, Role.CustomerTypeId = 1, Role.IsAdmin = 1, Role.IsLocked = 1 and Role.ClonedRoleId = NULL.
     */
    public boolean isPredefinedRole(UserAccount userAccount, int roleId, int customerTypeId) {
        Role role;
        Iterator<Role> roleIter = userAccount.getRoles().iterator();
        while (roleIter.hasNext()) {
            role = roleIter.next();
            if (role.getId() == roleId && role.getCustomerType().getId() == customerTypeId && role.isIsAdmin() && role.isIsLocked()) {
                return true;
            }
        }
        return false;
    }
    
//	private boolean isNullOrEmpty(Set<Role> roles) {
//		if (roles == null || roles.size() == 0) {
//			return true;
//		}
//		return false;
//	}
    
    /**
     * Iterates input TabHeadings and find 'current' TabHeading id.
     * 
     * @return Tab Populated Tab, Section, SectionColumn and Widget for selected landing tab.
     */
    public Tab loadLandingTab(HttpServletRequest request, List<TabHeading> tabHeadings) {
		SessionTool sessionTool = SessionTool.getInstance(request);
        
        // resets temporary changes made in edit mode if the user refreshes the page, or 
        //		exits edit mode without calling calling "cancelDashboard.html"
		sessionTool.removeAttribute(WebCoreConstants.SESSION_SECTION_IDS_TO_BE_DELETED);	
		sessionTool.removeAttribute(WebCoreConstants.SESSION_WIDGET_IDS_TO_BE_DELETED);
		sessionTool.removeAttribute(WebCoreConstants.SESSION_SUBSET_MEMBER_IDS_TO_BE_DELETED);
		sessionTool.removeAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        // *****
        
        int currentTabIdx = DashboardUtil.getCurrentTabFromCookie(request);
		int id = 0, index = 0;
        TabHeading heading;
        Iterator<TabHeading> iter = tabHeadings.iterator();
        while (iter.hasNext()) {
            heading = iter.next();
            if (currentTabIdx == index) {
                heading.setCurrent(Boolean.TRUE.toString());
                id = heading.getOriginalId();
            } else if (heading.getCurrent().equals(Boolean.TRUE.toString())) {
                id = heading.getOriginalId();
            }
            
            index++;
        }
        // This should not happen because TabHeadings should always have a 'current=true' object, see 'getTabs' method.
        if (id == 0) {
            
            id = tabHeadings.get(0).getOriginalId();
        }
        
        Tab tab = dashboardService.findTabByIdEager(id);
        tab = DashboardUtil.setRandomIds(request, tab);
        
        if (tab.getUserAccount().getId() == WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID) {
            tab = removeMasterList(request, tab, WebSecurityUtil.getUserAccount().getCustomer().isIsParent());
        }
        
        List<Section> sectionList = new ArrayList<Section>();
        sectionList = tab.getSections();
        Collections.sort(sectionList, new Comparator<Section>() {
            public int compare(Section s1, Section s2) {
                int diff = s1.getOrderNumber() - s2.getOrderNumber();
                return diff;
            }
        });
        
        for (Section s : sectionList) {
            for (int i = 0; i < WebCoreConstants.DEFAULT_NUMBER_OF_COLUMN; i++) {
                SectionColumn sc = s.getSectionColumn(String.valueOf(i));
                List<Widget> wlist = sc.getWidgets();
                Collections.sort(wlist, new Comparator<Widget>() {
                    public int compare(Widget w1, Widget w2) {
                        return w1.getOrderNumber() - w2.getOrderNumber();
                    }
                });
                sc.setWidgets(i, wlist);
            }
        }
        
        tab.setSections(sectionList);
        
        return tab;
    }
}
