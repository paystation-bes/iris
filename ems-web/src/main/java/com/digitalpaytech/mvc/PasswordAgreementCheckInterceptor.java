package com.digitalpaytech.mvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.util.WebSecurityUtil;

public class PasswordAgreementCheckInterceptor extends HandlerInterceptorAdapter {
    
    public static final String FIRST_PAGE = "/secure/terms.html";
    public static final String SAVE_AGREEMENT_PAGE = "/secure/processAgreement.html";
    public static final String PASSWORD_PAGE = "/secure/password.html";
    public static final String VALIDATE_SESSION = "/secure/validateSession.html";
    public static final String AGREEMENT_DECLINE = "/secure/serviceAgreementDecline.html";
    
    //    private static Logger logger = Logger.getLogger(PasswordAgreementCheckInterceptor.class);
    
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //        boolean bReturn = true;
        
        //        HttpSession httpSession = request.getSession(false);
        Authentication authentication = WebSecurityUtil.getAuthentication();
        /* check referer. */
        if ((authentication != null) && (authentication.getPrincipal() instanceof WebUser)) {
            WebUser user = (WebUser) authentication.getPrincipal();
            String url = request.getRequestURI();
            if (PASSWORD_PAGE.equals(url)) {
                if (!user.getIsAgreementSigned()) {
                    response.sendRedirect(getContextPath(request) + FIRST_PAGE);
                    return false;
                }
            } else if (FIRST_PAGE.equals(url) || VALIDATE_SESSION.equals(url) || SAVE_AGREEMENT_PAGE.equals(url) || AGREEMENT_DECLINE.equals(url)) {
                // ignore
            } else {
                if (!user.getIsAgreementSigned()) {
                    response.sendRedirect(getContextPath(request) + FIRST_PAGE);
                    return false;
                }
                if (user.getIsTemporaryPassword()) {
                    response.sendRedirect(getContextPath(request) + PASSWORD_PAGE);
                    return false;
                }
                
            }
        }
        return true;
    }
    
    private String getContextPath(HttpServletRequest request) {
        String result = request.getContextPath();
        return result;
    }
    
}
