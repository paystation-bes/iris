package com.digitalpaytech.mvc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetSubsetMember;
import com.digitalpaytech.dto.WidgetCacheKey;
import com.digitalpaytech.dto.WidgetMapInfo;
import com.digitalpaytech.dto.customeradmin.LocationRouteFilterInfo;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.WebObjectCacheManager;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.DashboardService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.util.DashboardUtil;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.WidgetConstants;

@Controller
public class PayStationPlacementController {
    
    protected static final Logger log = Logger.getLogger(WidgetController.class);
    
    @Autowired
    protected LocationService locationService;
    @Autowired
    protected CustomerAdminService customerAdminService;
    @Autowired
    protected RouteService routeService;
    @Autowired
    protected PointOfSaleService pointOfSaleService;
    @Autowired
    protected DashboardService dashboardService;
    @Autowired
    protected CommonControllerHelper commonControllerHelper;
    
    @Autowired
    protected WebObjectCacheManager cacheManager;
    
    public void setLocationService(final LocationService locationService) {
        this.locationService = locationService;
    }
    
    public void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    public void setRouteService(final RouteService routeService) {
        this.routeService = routeService;
    }
    
    public void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public void setDashboardService(final DashboardService dashboardService) {
        this.dashboardService = dashboardService;
    }
    
    public void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }
    
    public void setWebObjectCacheManager(final WebObjectCacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }
    
    //========================================================================================================================
    // "Pay Station Placement" tab
    //========================================================================================================================
    
    /**
     * This method creates a list of Id's of routes and locations which have pay
     * stations for the current user and puts them into the model as
     * "filterValues" to be used in the UI to filter pay stations
     * 
     * @param request
     * @param response
     * @param model
     * @return List of locations and routes stored in the model as attribute
     *         "filterValues" used for filtering pay stations. Target vm file
     *         URI which is mapped to Settings->paystationPlacement form
     *         (/settings/locations/paystationPlacement.vm)
     */
    public String placedPayStations(HttpServletRequest request, HttpServletResponse response, ModelMap model, String urlOnSuccess) {
    	RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
    	
        Integer customerId = resolveCustomerId(request, response);
        
        LocationRouteFilterInfo locationRouteInfo = this.commonControllerHelper.createLocationRouteFilter(customerId, keyMapping);
        
        PlacementMapBordersInfo mapBorders = this.pointOfSaleService.findMapBordersByCustomerId(customerId);
        
        model.put("mapBorders", mapBorders);

        model.put("filterValues", locationRouteInfo);
        return urlOnSuccess;
    }
    
    /**
     * This method takes an optional location or route Id and creates a list of
     * pay station details. These details contain map information (longitude and
     * latitude).
     * 
     * @param request
     * @param response
     * @param model
     * @return Pay station map information WidgetMapInfo object converted into
     *         JSON
     */
    public String payStations(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        String str_randomLocationId = request.getParameter("locationID");
        String str_randomRouteId = request.getParameter("routeID");
        
//        UserAccount userAccount = WebSecurityUtil.getUserAccount();
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        List<PointOfSale> pointOfSales = new ArrayList<PointOfSale>();
        Integer customerId = resolveCustomerId(request, response);
        
        if (str_randomLocationId != null) {
            if (!DashboardUtil.validateRandomId(str_randomLocationId)) {
                log.warn("+++ Invalid randomised locationID in PayStationPlacement.payStations() method. +++");
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return null;
            }
            Object actualLocationId = keyMapping.getKey(str_randomLocationId);
            if (!DashboardUtil.validateDBIntegerPrimaryKey(actualLocationId.toString())) {
                log.warn("+++ Invalid locationID in PayStationPlacement.payStations() method. +++");
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return null;
            }
            int locationId = Integer.parseInt(actualLocationId.toString());
            pointOfSales = pointOfSaleService.findPointOfSalesByLocationId(locationId);
        } else if (str_randomRouteId != null) {
            if (!DashboardUtil.validateRandomId(str_randomRouteId)) {
                log.warn("+++ Invalid randomised routeID in PayStationPlacement.payStations() method. +++");
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return null;
            }
            Object actualRouteId = keyMapping.getKey(str_randomRouteId);
            if (!DashboardUtil.validateDBIntegerPrimaryKey(actualRouteId.toString())) {
                log.warn("+++ Invalid routeID in PayStationPlacement.payStations() method. +++");
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return null;
            }
            int routeId = Integer.parseInt(actualRouteId.toString());
            pointOfSales = routeService.findPointOfSalesByRouteId(routeId, false);
        } else {
            pointOfSales = pointOfSaleService.findValidPointOfSaleByCustomerIdOrderByName(customerId);
        }
        
        WidgetMapInfo mapInfo = new WidgetMapInfo();
        
        if (pointOfSales != null) {
            //set Pay Stations randomID and initialize map data
            for (PointOfSale pointOfSale : pointOfSales) {
                if (pointOfSale.getPaystation().getPaystationType().getId() != WebCoreConstants.PAY_STATION_TYPE_TEST_VIRTUAL) {
                    mapInfo.addMapEntry(pointOfSale.getPaystation().getPaystationType().getId(), pointOfSale, pointOfSale.getId(), 0);
                }
            }
        }
        
        return WidgetMetricsHelper.convertToJson(mapInfo, "payStationInformation", true);
    }
    
    /**
     * This method takes a pay station Id as input as well as longitude and
     * latitude. It then updates the pay station in the database to these new
     * coordinates if they pass simple longitude and latitude math validations
     * 
     * @param request
     * @param response
     * @param model
     * @return "true" string value if process succeeds, "false" is process fails
     */
    public String pinPayStation(HttpServletRequest request, HttpServletResponse response, ModelMap model) {
        
        String str_randomPointOfSaleId = request.getParameter("payStationID");
        
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        if (!DashboardUtil.validateRandomId(str_randomPointOfSaleId)) {
            log.warn("+++ Invalid randomised locationID in LocationsController.getLocationDetails() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        Object actualPOSId = keyMapping.getKey(str_randomPointOfSaleId);
        if (!DashboardUtil.validateDBIntegerPrimaryKey(actualPOSId.toString())) {
            log.warn("+++ Invalid locationID in LocationsController.getLocationDetails() method. +++");
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        int pointOfSaleId = Integer.parseInt(actualPOSId.toString());
        
        String latString = request.getParameter("latitude");
        String lngString = request.getParameter("longitude");
        String altString = request.getParameter("altitude");
        
        BigDecimal lat = null;
        BigDecimal lng = null;
        Integer alt = null;
        
        if (!StringUtils.isBlank(latString)) {
            try {
                lat = new BigDecimal(latString);
            } catch (NumberFormatException e) {
                log.warn("+++ NumberFormatException - Invalid latitude format in LocationsController.pinPayStation() method. +++");
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            if (lat.compareTo(new BigDecimal(-90)) < 0 || lat.compareTo(new BigDecimal(90)) > 0) {
                log.warn("+++ Invalid latitude in PayStationPlacement.pinPayStation() method. +++");
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        if (!StringUtils.isBlank(lngString)) {
            try {
                lng = new BigDecimal(lngString);
            } catch (NumberFormatException e) {
                log.warn("+++ NumberFormatException - Invalid longitude format in LocationsController.pinPayStation() method. +++");
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            if (lng.compareTo(new BigDecimal(-180)) < 0 || lng.compareTo(new BigDecimal(180)) > 0) {
                log.warn("+++ Invalid longitude in PayStationPlacement.pinPayStation() method. +++");
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        if (!StringUtils.isBlank(altString)) {
            try {
                alt = new Integer(altString);
            } catch (NumberFormatException e) {
                log.warn("+++ NumberFormatException - Invalid altitude format in LocationsController.pinPayStation() method. +++");
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
        }
        
        PointOfSale pointOfSale = pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        if (pointOfSale == null) {
            log.warn("+++ pointOfSale record not found in PayStationPlacement.pinPayStation() method. +++");
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        pointOfSale.setLatitude(lat);
        pointOfSale.setLongitude(lng);
        pointOfSale.setAltitudeOrLevel(alt);
        
        customerAdminService.saveOrUpdatePointOfSale(pointOfSale);
        
        removeMapWidgetFromCache(pointOfSale.getId());
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    private void removeMapWidgetFromCache(Integer pointOfSaleId) {
        
        UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        List<Widget> mapWidgets = dashboardService.findMapWidgetsByUserAccountId(userAccount.getId());
        
        if (mapWidgets != null && !mapWidgets.isEmpty()) {
            widgetLoop: for (Widget widget : mapWidgets) {
                
                if (widget.isIsSubsetTier1() || widget.isIsSubsetTier2() || widget.isIsSubsetTier3()) {
                    
                    List<PointOfSale> pointOfSales = new ArrayList<PointOfSale>();
                    
                    if (widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_LOCATION
                        || widget.getWidgetTierTypeByWidgetTier2Type().getId() == WidgetConstants.TIER_TYPE_LOCATION
                        || widget.getWidgetTierTypeByWidgetTier3Type().getId() == WidgetConstants.TIER_TYPE_LOCATION) {
                        
                        List<WidgetSubsetMember> subsetMembers = dashboardService.findSubsetMemberByWidgetIdTierTypeId(widget.getId(),
                                                                                                                       WidgetConstants.TIER_TYPE_LOCATION);
                        
                        for (WidgetSubsetMember subsetMember : subsetMembers) {
                            pointOfSales = pointOfSaleService.findPointOfSalesByLocationId(subsetMember.getMemberId());
                        }
                        
                    } else if (widget.getWidgetTierTypeByWidgetTier1Type().getId() == WidgetConstants.TIER_TYPE_ROUTE
                               || widget.getWidgetTierTypeByWidgetTier2Type().getId() == WidgetConstants.TIER_TYPE_ROUTE
                               || widget.getWidgetTierTypeByWidgetTier3Type().getId() == WidgetConstants.TIER_TYPE_ROUTE) {
                        
                        List<WidgetSubsetMember> subsetMembers = dashboardService.findSubsetMemberByWidgetIdTierTypeId(widget.getId(),
                                                                                                                       WidgetConstants.TIER_TYPE_ROUTE);
                        
                        for (WidgetSubsetMember subsetMember : subsetMembers) {
                            pointOfSales = pointOfSaleService.findPointOfSalesByRouteId(subsetMember.getMemberId());
                        }
                        
                    } else {
                        cacheManager.removeWidgetDataFromCache(new WidgetCacheKey(widget, userAccount.getCustomer().getId()));
                        break widgetLoop;
                    }
                    
                    for (PointOfSale pointOfSale : pointOfSales) {
                        if (pointOfSale.getId().equals(pointOfSaleId)) {
                            cacheManager.removeWidgetDataFromCache(new WidgetCacheKey(widget, userAccount.getCustomer().getId()));
                            break widgetLoop;
                        }
                    }
                }
                cacheManager.removeWidgetDataFromCache(new WidgetCacheKey(widget, userAccount.getCustomer().getId()));
            }
        }
    }
    
    protected Integer verifyRandomIdAndReturnActual(HttpServletResponse response, RandomKeyMapping keyMapping, String str_randomLocationId) {
        
        String[] msgs = new String[] { "+++ Invalid randomised locationID in LocationsController.getLocationDetails() method. +++",
                "+++ Invalid locationID in LocationsController.getLocationDetails() method. +++" };
        return WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, str_randomLocationId, msgs);
    }
    
    protected Integer resolveCustomerId(HttpServletRequest request, HttpServletResponse response) {
        Integer customerId;
        Customer customer = WebSecurityUtil.getUserAccount().getCustomer();
        if ((customer.getCustomerType() == null) || (customer.getCustomerType().getId() != WebCoreConstants.CUSTOMER_TYPE_DPT)) {
            customerId = customer.getId();
        } else {
            customerId = WebCoreUtil.verifyRandomIdAndReturnActual(response, SessionTool.getInstance(request).getKeyMapping(), request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID),
                                                                   new String[] { "+++ Invalid randomised customerID in LocationsController. +++" });
        }
        
        return customerId;
    }
}
