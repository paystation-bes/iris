package com.digitalpaytech.mvc.dto;

import java.util.Map;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class TerminalAccount {
    
    private String _id;
    private String name;
    private String customerName;
    private String terminalStatus;
    private String timeZone;
    private String processorType;
    private Map<String, String> terminalConfiguration;
    

    public TerminalAccount() {
        //Empty Constructor
    }
    
    
    public final String getId() {
        return this._id;
    }

    public final void setId(final String id) {
        this._id = id;
    }

    public final String getName() {
        return this.name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final String getCustomerName() {
        return this.customerName;
    }

    public final void setCustomerName(final String customerName) {
        this.customerName = customerName;
    }

    public final String getTerminalStatus() {
        return this.terminalStatus;
    }

    public final void setTerminalStatus(final String terminalStatus) {
        this.terminalStatus = terminalStatus;
    }

    public final String getTimeZone() {
        return this.timeZone;
    }

    public final void setTimeZone(final String timeZone) {
        this.timeZone = timeZone;
    }


    public final String getProcessorType() {
        return this.processorType;
    }


    public final void setProcessorType(final String processorType) {
        this.processorType = processorType;
    }


    public final Map<String, String> getTerminalConfiguration() {
        return this.terminalConfiguration;
    }


    public final void setTerminalConfiguration(final Map<String, String> terminalConfiguration) {
        this.terminalConfiguration = terminalConfiguration;
    }
    
    
   
}
