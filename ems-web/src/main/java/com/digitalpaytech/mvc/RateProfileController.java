package com.digitalpaytech.mvc;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PermitIssueType;
import com.digitalpaytech.domain.Rate;
import com.digitalpaytech.domain.RateProfile;
import com.digitalpaytech.domain.RateProfileLocation;
import com.digitalpaytech.domain.RateRateProfile;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.PaginatedList;
import com.digitalpaytech.dto.SearchCustomerResult;
import com.digitalpaytech.dto.SearchCustomerResultSetting;
import com.digitalpaytech.dto.customeradmin.RateProfileListInfo;
import com.digitalpaytech.exception.RandomKeyMappingException;
import com.digitalpaytech.mvc.helper.RateProfileHelper;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.RateProfileEditForm;
import com.digitalpaytech.mvc.support.RateProfileLocationForm;
import com.digitalpaytech.mvc.support.RateProfileSearchForm;
import com.digitalpaytech.mvc.support.RateRateProfileEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.PermitIssueTypeService;
import com.digitalpaytech.service.RateExpiryDayTypeService;
import com.digitalpaytech.service.RateFlatTypeService;
import com.digitalpaytech.service.RateProfileService;
import com.digitalpaytech.service.RateRateProfileService;
import com.digitalpaytech.service.RateService;
import com.digitalpaytech.util.DashboardUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.RateProfileValidator;

/**
 * This class handles the Alert setting request.
 * 
 * @author Brian Kim
 */
@Controller
public class RateProfileController {
    public static final String FORM_EDIT = "rateProfileEditForm";
    
    protected static final String FORM_SEARCH = "rateProfileSearchForm";
    protected static final String LIST_NAME = "rateProfileListInfo";
    protected static final String PARAMETER_OBJECT_ID = "rateProfileID";
    protected static final String PARAMETER_SEARCH = "search";
    
    private static Logger logger = Logger.getLogger(RateProfileController.class);
    
    @Autowired
    protected RateProfileService rateProfileService;
    
    @Autowired
    protected RateRateProfileService rateRateProfileService;
    
    @Autowired
    protected RateFlatTypeService rateFlatTypeService;
    
    @Autowired
    protected RateExpiryDayTypeService rateExpiryDayTypeService;
    
    @Autowired
    protected PermitIssueTypeService permitIssueTypeService;
    
    @Autowired
    protected RateProfileValidator rateProfileValidator;
    
    @Autowired
    private CommonControllerHelper commonControllerHelper;
    
    @Autowired
    private RateService rateService;
    
    @Autowired
    private LocationService locationService;
    
    public final void setRateProfileService(final RateProfileService rateProfileService) {
        this.rateProfileService = rateProfileService;
    }
    
    public final void setRateRateProfileService(final RateRateProfileService rateRateProfileService) {
        this.rateRateProfileService = rateRateProfileService;
    }
    
    public final void setRateFlatTypeService(final RateFlatTypeService rateFlatTypeService) {
        this.rateFlatTypeService = rateFlatTypeService;
    }
    
    public final void setRateExpiryDayTypeService(final RateExpiryDayTypeService rateExpiryDayTypeService) {
        this.rateExpiryDayTypeService = rateExpiryDayTypeService;
    }
    
    public final void setPermitIssueTypeService(final PermitIssueTypeService permitIssueTypeService) {
        this.permitIssueTypeService = permitIssueTypeService;
    }
    
    public final void setRateProfileValidator(final RateProfileValidator rateProfileValidator) {
        this.rateProfileValidator = rateProfileValidator;
    }
    
    public final void setCommonControllerHelper(final CommonControllerHelper commonControllerHelper) {
        this.commonControllerHelper = commonControllerHelper;
    }
    
    public final void setRateService(final RateService rateService) {
        this.rateService = rateService;
    }
    
    public final void setLocationService(final LocationService locationService) {
        this.locationService = locationService;
    }
    
    @ModelAttribute(RateRateProfileController.FORM_SCHEDULE)
    public final WebSecurityForm<RateRateProfileEditForm> initScheduleForm(final HttpServletRequest request) {
        return new WebSecurityForm<RateRateProfileEditForm>((Object) null, new RateRateProfileEditForm(), SessionTool.getInstance(request)
                .locatePostToken(RateRateProfileController.FORM_SCHEDULE));
    }
    
    protected final String view(final WebSecurityForm<RateRateProfileEditForm> rescheduleForm,
                                final WebSecurityForm<RateProfileLocationForm> locationForm, final HttpServletRequest request,
                                final HttpServletResponse response, final ModelMap model, final String urlOnSucceed) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        
        final List<PermitIssueType> permitIssueTypeList = this.permitIssueTypeService.findAll();
        
        final WebSecurityForm<RateProfileEditForm> rateProfileEditForm = new WebSecurityForm<RateProfileEditForm>(null, new RateProfileEditForm());
        model.put(FORM_EDIT, rateProfileEditForm);
        
        model.put("permitIssueTypeList", permitIssueTypeList);
        model.put("locationList", createLocationLists(customerId, keyMapping));
        model.put("rateProfileLocationForm", locationForm);
        
        rescheduleForm.setWrappedObject(new RateRateProfileEditForm());
        model.put(RateRateProfileController.FORM_SCHEDULE, rescheduleForm);
        
        return urlOnSucceed;
    }
    
    protected final String search(final HttpServletRequest request, final HttpServletResponse response) {
        
        final String search = request.getParameter(PARAMETER_SEARCH);
        
        final SearchCustomerResult searchCustomerResult = new SearchCustomerResult();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        
        if (customerId == null) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        if (!StringUtils.isBlank(search)) {
            if (!search.matches(WebCoreConstants.REGEX_PRINTABLE_TEXT)) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                return WebCoreConstants.RESPONSE_FALSE;
            }
            
            /* retrieve rate by name. */
            final List<RateProfile> rateProfileList = this.rateProfileService.searchRateProfiles(customerId, search);
            for (RateProfile rateProfile : rateProfileList) {
                searchCustomerResult.addPosNameSearchResult(new SearchCustomerResultSetting(keyMapping.getRandomString(RateProfile.class,
                                                                                                                       rateProfile.getId()),
                        rateProfile.getName(), rateProfile.getPermitIssueType().getName()));
            }
            
        }
        
        return WidgetMetricsHelper.convertToJson(searchCustomerResult, "searchRateProfileResult", true);
    }
    
    protected final String getList(final WebSecurityForm<RateProfileSearchForm> form, final HttpServletRequest request,
                                   final HttpServletResponse response) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        
        Date maxUpdateTime = null;
        if (form.getWrappedObject().getPage() == 1) {
            maxUpdateTime = DateUtil.getCurrentGmtDate();
        }
        if ((form.getWrappedObject().getDataKey() != null) && (form.getWrappedObject().getDataKey().length() > 0)) {
            try {
                maxUpdateTime = new Date(Long.parseLong(form.getWrappedObject().getDataKey()));
            } catch (NumberFormatException nfe) {
                logger.info("NumberFormatException in RateProfileController.getList(), currentTime is used for maxUpdateTime");
            }
        }
        
        final List<RateProfile> rateProfileList = this.rateProfileService.findPagedRateProfileList(customerId, form.getWrappedObject()
                .getPermitIssueTypeId(), form.getWrappedObject().getIsPublished(), maxUpdateTime, form.getWrappedObject().getPage());
        
        final PaginatedList<RateProfileListInfo> result = new PaginatedList<RateProfileListInfo>();
        result.setElements(populateRateProfileListInfo(rateProfileList, keyMapping));
        result.setDataKey(Long.toString(maxUpdateTime.getTime(), WebCoreConstants.PAGED_LIST_MAXUPDATETIME_LONG_LENGTH));
        
        return WidgetMetricsHelper.convertToJson(result, LIST_NAME, true);
    }
    
    protected final String locatePageWith(final WebSecurityForm<RateProfileSearchForm> form, final HttpServletRequest request,
                                          final HttpServletResponse response) {
        
        String resultJson = WebCoreConstants.RESPONSE_FALSE;
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        int targetPage = -1;
        final Integer targetId = keyMapping.getKey(form.getWrappedObject().getTargetRandomId(), RateProfile.class, Integer.class);
        final Date maxUpdateTime = new Date();
        if (targetId != null) {
            targetPage = this.rateProfileService.findRateProfilePage(targetId, customerId, form.getWrappedObject().getPermitIssueTypeId(), form
                    .getWrappedObject().getIsPublished(), maxUpdateTime);
        }
        
        if (targetPage > 0) {
            
            final List<RateProfile> rateProfileList = this.rateProfileService.findPagedRateProfileList(customerId, form.getWrappedObject()
                    .getPermitIssueTypeId(), form.getWrappedObject().getIsPublished(), maxUpdateTime, form.getWrappedObject().getPage());
            
            final PaginatedList<RateProfileListInfo> result = new PaginatedList<RateProfileListInfo>();
            result.setElements(populateRateProfileListInfo(rateProfileList, keyMapping));
            result.setDataKey(Long.toString(maxUpdateTime.getTime(), WebCoreConstants.PAGED_LIST_MAXUPDATETIME_LONG_LENGTH));
            
            resultJson = WidgetMetricsHelper.convertToJson(result, LIST_NAME, true);
        }
        
        return resultJson;
    }
    
    protected final String add(final WebSecurityForm<RateProfileEditForm> webSecurityForm, final HttpServletRequest request,
                               final HttpServletResponse response) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final SessionTool sessionTool = SessionTool.getInstance(request);
        final RateProfile rateProfile = createRateProfile(request);
        
        String randomStr = null;
        try {
            randomStr = keyMapping.hideProperty(rateProfile, "randomId").getPrimaryKey().toString();
        } catch (RandomKeyMappingException e) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return null;
        }
        rateProfile.setRandomId(randomStr);
        sessionTool.setAttribute(WebCoreConstants.SESSION_RATE_PROFILE_ATTRIBUTE_NAME + randomStr, rateProfile);
        sessionTool.setAttribute(WebCoreConstants.SESSION_RATE_RATE_PROFILE_MAP_ATTRIBUTE_NAME + randomStr,
                                 new HashMap<String, RateRateProfileEditForm>());
        webSecurityForm.setWrappedObject(new RateProfileEditForm(randomStr));
        
        return rateProfile.getRandomId();
    }
    
    protected final String edit(final WebSecurityForm<RateProfileEditForm> webSecurityForm, final HttpServletRequest request,
                                final HttpServletResponse response) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final SessionTool sessionTool = SessionTool.getInstance(request);
        final String strRateProfileId = request.getParameter(PARAMETER_OBJECT_ID);
        final Integer rateProfileId = WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, strRateProfileId, new String[] {
            "Invalid randomised rateProfileID in RateProfileController.edit().",
            "+++ Invalid randomised rateProfileID in RateProfileController.edit(). +++", });
        if (rateProfileId == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final RateProfile rateProfile = this.rateProfileService.findRateProfileById(rateProfileId);
        if (rateProfile == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        rateProfile.setRandomId(strRateProfileId);
        sessionTool.setAttribute(WebCoreConstants.SESSION_RATE_PROFILE_ATTRIBUTE_NAME + strRateProfileId, rateProfile);
        sessionTool.setAttribute(WebCoreConstants.SESSION_RATE_RATE_PROFILE_MAP_ATTRIBUTE_NAME + strRateProfileId,
                                 new HashMap<String, RateRateProfileEditForm>());
        webSecurityForm.setWrappedObject(populateRateProfileInfo(rateProfile, strRateProfileId, keyMapping));
        
        return WidgetMetricsHelper.convertToJson(webSecurityForm.getWrappedObject(), "rateProfileInfo", true);
    }
    
    protected final String save(final WebSecurityForm<RateProfileEditForm> webSecurityForm, final BindingResult result,
                                final HttpServletRequest request, final HttpServletResponse response) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final RateProfileEditForm form = webSecurityForm.getWrappedObject();
        final RateProfile rateProfile = (RateProfile) SessionTool.getInstance(request)
                .getAttribute(WebCoreConstants.SESSION_RATE_PROFILE_ATTRIBUTE_NAME + form.getRandomId());
        
        final String responseText = prepareRateProfileForSave(request, response, webSecurityForm, result, rateProfile, form);
        if (responseText != null) {
            return responseText;
        }
        
        this.rateProfileService.saveOrUpdateRateProfile(rateProfile, rateProfile.getRateRateProfiles(), rateProfile.getRateProfileLocations());
        
        return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(WebCoreConstants.COLON).append(webSecurityForm.getInitToken())
                .append(WebCoreConstants.COLON).append(keyMapping.getRandomString(rateProfile, WebCoreConstants.ID_LOOK_UP_NAME)).toString();
    }
    
    protected final String validate(final WebSecurityForm<RateProfileEditForm> webSecurityForm, final BindingResult result,
                                    final HttpServletRequest request, final HttpServletResponse response) {
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        
        this.rateProfileValidator.validate(webSecurityForm, result, customerId);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    protected final String publish(final WebSecurityForm<RateProfileEditForm> webSecurityForm, final BindingResult result,
                                   final HttpServletRequest request, final HttpServletResponse response) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final RateProfileEditForm form = webSecurityForm.getWrappedObject();
        final RateProfile rateProfile = (RateProfile) SessionTool.getInstance(request)
                .getAttribute(WebCoreConstants.SESSION_RATE_PROFILE_ATTRIBUTE_NAME + form.getRandomId());
        
        final String responseText = prepareRateProfileForSave(request, response, webSecurityForm, result, rateProfile, form);
        if (responseText != null) {
            return responseText;
        }
        
        rateProfile.setIsPublished(true);
        
        for (RateProfileLocation rpl : rateProfile.getRateProfileLocations()) {
            rpl.setIsPublished(true);
        }
        
        this.rateProfileService.saveOrUpdateRateProfile(rateProfile, rateProfile.getRateRateProfiles(), rateProfile.getRateProfileLocations());
        
        return new StringBuilder(WebCoreConstants.RESPONSE_TRUE).append(WebCoreConstants.COLON).append(webSecurityForm.getInitToken())
                .append(WebCoreConstants.COLON).append(keyMapping.getRandomString(rateProfile, WebCoreConstants.ID_LOOK_UP_NAME)).toString();
    }
    
    protected final String cancel(final HttpServletRequest request, final HttpServletResponse response) {
        cleanupSessionData(request, request.getParameter(PARAMETER_OBJECT_ID));
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    private final void cleanupSessionData(final HttpServletRequest request, final String strRateProfileId) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        
        sessionTool.removeAttribute(WebCoreConstants.SESSION_RATE_PROFILE_ATTRIBUTE_NAME + strRateProfileId);
        sessionTool.removeAttribute(WebCoreConstants.SESSION_RATE_RATE_PROFILE_MAP_ATTRIBUTE_NAME + strRateProfileId);
        sessionTool.removeAttribute(MessageFormat.format(RateProfileHelper.SESSION_KEY_PTTRN_RATE_LOCATION, strRateProfileId));
    }
    
    protected final String verifyDelete(final HttpServletRequest request, final HttpServletResponse response) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final String strRateProfileId = request.getParameter(PARAMETER_OBJECT_ID);
        final Integer rateProfileId = WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, strRateProfileId, new String[] {
            "Invalid randomised rateProfileID in RateProfileController.verifyDelete().",
            "+++ Invalid randomised rateProfileID in RateProfileController.verifyDelete(). +++", });
        if (rateProfileId == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final RateProfile rateProfile = this.rateProfileService.findRateProfileById(rateProfileId);
        if (rateProfile == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        if (rateProfile.isIsPublished()) {
            final Set<RateProfileLocation> rateProfileLocationList = rateProfile.getRateProfileLocations();
            
            if (rateProfileLocationList != null && !rateProfileLocationList.isEmpty()) {
                return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON
                       + this.rateProfileValidator.getMessage("warning.settings.rate.rateProfileAssignedToLocations");
            }
        }
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    protected final String delete(final HttpServletRequest request, final HttpServletResponse response) {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final String strRateProfileId = request.getParameter(PARAMETER_OBJECT_ID);
        final Integer rateProfileId = WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, strRateProfileId, new String[] {
            "Invalid randomised rateProfileID in RateProfileController.delete().",
            "+++ Invalid randomised rateProfileID in RateProfileController.delete(). +++", });
        if (rateProfileId == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        final RateProfile rateProfile = this.rateProfileService.findRateProfileById(rateProfileId);
        if (rateProfile == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        this.rateProfileService.deleteRateProfile(rateProfile, userAccount.getId());
        
        return WebCoreConstants.RESPONSE_TRUE;
    }
    
    private String prepareRateProfileForSave(final HttpServletRequest request, final HttpServletResponse response,
                                             final WebSecurityForm<RateProfileEditForm> webSecurityForm, final BindingResult result,
                                             final RateProfile rateProfile, final RateProfileEditForm form) {
        final SessionTool sessionTool = SessionTool.getInstance(request);
        final RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        final Integer customerId = this.commonControllerHelper.resolveCustomerId(request, response);
        
        this.rateProfileValidator.validate(webSecurityForm, result, customerId);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            webSecurityForm.resetToken();
            /* return the error status with JSON format to UI. */
            return WidgetMetricsHelper.convertToJson(webSecurityForm.getValidationErrorInfo(), WebCoreConstants.JSON_ERROR_STATUS_LABEL, true);
        }
        
        if ((rateProfile != null) && !rateProfile.getRandomId().equals(form.getRandomId())) {
            return WebCoreConstants.RESPONSE_FALSE + WebCoreConstants.COLON
                   + this.rateProfileValidator.getMessage("error.settings.rates.anotherActiveProfile");
        }
        
        webSecurityForm.resetToken();
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        final Date currentTime = new Date();
        
        rateProfile.setName(form.getName().trim());
        rateProfile.setIsPublished(false);
        rateProfile.setPermitIssueType(this.permitIssueTypeService.findPermitIssueTypeById(form.getPermitIssueTypeId()));
        rateProfile.setLastModifiedByUserId(userAccount.getId());
        rateProfile.setLastModifiedGmt(currentTime);
        
        @SuppressWarnings("unchecked")
        final Map<String, RateRateProfileEditForm> ratesMap = (Map<String, RateRateProfileEditForm>) SessionTool.getInstance(request)
                .getAttribute(WebCoreConstants.SESSION_RATE_RATE_PROFILE_MAP_ATTRIBUTE_NAME + form.getRandomId());
        
        rateProfile.setRateRateProfiles(retrieveModifiedRateRateProfileList(ratesMap.values(), rateProfile, userAccount.getId(), currentTime,
                                                                            keyMapping));
        
        rateProfile.setRateProfileLocations(retrieveModifiedRateProfileLocation(request, form.getRandomId(), rateProfile, userAccount, currentTime,
                                                                                keyMapping));
        
        //Remove object from session
        cleanupSessionData(request, form.getRandomId());
        
        return null;
    }
    
    private Set<RateRateProfile> retrieveModifiedRateRateProfileList(final Collection<RateRateProfileEditForm> rateRateProfileEditForms,
                                                                     final RateProfile rateProfile, final Integer userAccountId,
                                                                     final Date currentDate, final RandomKeyMapping keyMapping) {
        
        final Set<RateRateProfile> rateRateProfileList = new HashSet<RateRateProfile>(rateRateProfileEditForms.size());
        for (RateRateProfileEditForm rateRateProfileForm : rateRateProfileEditForms) {
            final RateRateProfile rateRateProfile = populateRateRateProfile(rateRateProfileForm, rateProfile, userAccountId, currentDate, keyMapping);
            rateRateProfileList.add(rateRateProfile);
        }
        
        return rateRateProfileList;
    }
    
    private Set<RateProfileLocation> retrieveModifiedRateProfileLocation(final HttpServletRequest request, final String rateProfileRandomId,
                                                                         final RateProfile rateProfile, final UserAccount userAccount,
                                                                         final Date currentDate, final RandomKeyMapping keyMapping) {
        HashSet<RateProfileLocation> result = null;
        
        @SuppressWarnings("unchecked")
        final Map<String, RateProfileLocationForm> rateProfileLocationForms = (Map<String, RateProfileLocationForm>) SessionTool.getInstance(request)
                .getAttribute(MessageFormat.format(RateProfileHelper.SESSION_KEY_PTTRN_RATE_LOCATION, rateProfileRandomId));
        if (rateProfileLocationForms == null) {
            result = new HashSet<RateProfileLocation>(0);
        } else {
            result = new HashSet<RateProfileLocation>(rateProfileLocationForms.size());
            for (String rplRandomId : rateProfileLocationForms.keySet()) {
                final RateProfileLocation domain = populateRateProfileLocation(rateProfileLocationForms.get(rplRandomId), keyMapping);
                domain.setRateProfile(rateProfile);
                domain.setCustomer(userAccount.getCustomer());
                domain.setLastModifiedByUserId(userAccount.getId());
                domain.setLastModifiedGmt(currentDate);
                
                result.add(domain);
            }
        }
        
        return result;
    }
    
    private RateRateProfile populateRateRateProfile(final RateRateProfileEditForm form, final RateProfile rateProfile, final Integer userAccountId,
                                                    final Date currentDate, final RandomKeyMapping keyMapping) {
        
        RateRateProfile rateRateProfile = null;
        
        final Integer rateRateProfileId = keyMapping.getKey(form.getRateRateProfileRandomId(), RateRateProfile.class, Integer.class);
        if (rateRateProfileId == null) {
            throw new IllegalStateException("Invalid RateRateProfileRandomId !");
        }
        
        if (rateRateProfileId > 0) {
            rateRateProfile = this.rateRateProfileService.findRateRateProfileById(rateRateProfileId);
        } else {
            rateRateProfile = new RateRateProfile();
        }
        
        final Integer rateId = keyMapping.getKey(form.getRateRandomId(), Rate.class, Integer.class);
        if (rateId == null) {
            throw new IllegalStateException("Invalid RateRandomId !");
        }
        
        rateRateProfile.setRate(this.rateService.findRateById(rateId));
        rateRateProfile.setRateProfile(rateProfile);
        rateRateProfile.setDescription(form.getDescription());
        rateRateProfile.setCustomer(rateProfile.getCustomer());
        rateRateProfile.setAvailabilityStartDate(form.getStartDate());
        rateRateProfile.setAvailabilityStartTime(form.getAvailabilityStartTime());
        rateRateProfile.setAvailabilityEndDate(form.getEndDate());
        rateRateProfile.setAvailabilityEndTime(form.getAvailabilityEndTime());
        rateRateProfile.setDescription(form.getDescription());
        rateRateProfile.setRateFlatType(form.getFlatRateType() != null ? this.rateFlatTypeService.findRateFlatTypeById(form.getFlatRateType()
                .byteValue()) : null);
        rateRateProfile.setIsDeleted(form.getIsToBeDeleted());
        rateRateProfile.setDurationNumberOfDays(form.getNumberOfDays() != null ? form.getNumberOfDays().shortValue() : null);
        rateRateProfile.setDurationNumberOfHours(form.getNumberOfHours() != null ? form.getNumberOfHours().shortValue() : null);
        rateRateProfile.setDurationNumberOfMins(form.getNumberOfMins() != null ? form.getNumberOfMins().shortValue() : null);
        
        rateRateProfile.setIsMaximumExpiryEnabled(form.getUseMaxExpiry());
        
        if (form.getUseMaxExpiry()) {
            rateRateProfile.setRateExpiryDayType(this.rateExpiryDayTypeService.findRateExpiryDayTypeById(form.getExpiryDayType().byteValue()));
            rateRateProfile.setExpiryTime(form.getExpiryTime());
        } else {
            rateRateProfile.setRateExpiryDayType(null);
            rateRateProfile.setExpiryTime(null);
            
        }
        
        rateRateProfile.setIsAdvancePurchaseEnabled(form.getUseAdvPurchase());
        rateRateProfile.setIsOverride(form.getUseOverride());
        rateRateProfile.setIsRateBlendingEnabled(form.getUseRateBlending());
        
        rateRateProfile.setIsMonday(form.getUseMonday());
        rateRateProfile.setIsTuesday(form.getUseTuesday());
        rateRateProfile.setIsWednesday(form.getUseWednesday());
        rateRateProfile.setIsThursday(form.getUseThursday());
        rateRateProfile.setIsFriday(form.getUseFriday());
        rateRateProfile.setIsSaturday(form.getUseSaturday());
        rateRateProfile.setIsSunday(form.getUseSunday());
        
        rateRateProfile.setIsScheduled(form.getUseScheduled());
        
        rateRateProfile.setDisplayPriority(WebCoreConstants.DEFAULT_DISPLAY_PRIORITY);
        rateRateProfile.setLastModifiedByUserId(userAccountId);
        rateRateProfile.setLastModifiedGmt(currentDate);
        
        return rateRateProfile;
    }
    
    private RateProfile createRateProfile(final HttpServletRequest request) {
        
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        
        final RateProfile rateProfile = new RateProfile();
        
        rateProfile.setCustomer(userAccount.getCustomer());
        rateProfile.setRandomId(DashboardUtil.getTemporaryId(WebCoreConstants.TEMP_RATE_PROFILE_ID_PREFIX));
        
        return rateProfile;
    }
    
    private RateProfileEditForm populateRateProfileInfo(final RateProfile rateProfile, final String randomId, final RandomKeyMapping keyMapping) {
        final RateProfileEditForm rateProfileInfo = new RateProfileEditForm();
        rateProfileInfo.setRandomId(randomId);
        rateProfileInfo.setName(rateProfile.getName());
        rateProfileInfo.setPermitIssueTypeId(rateProfile.getPermitIssueType().getId());
        
        return rateProfileInfo;
    }
    
    private List<RateProfileListInfo> populateRateProfileListInfo(final List<RateProfile> rateProfileList, final RandomKeyMapping keyMapping) {
        final List<RateProfileListInfo> rateProfileListInfoList = new ArrayList<RateProfileListInfo>();
        for (RateProfile rateProfile : rateProfileList) {
            final RateProfileListInfo rateProfileListInfo = new RateProfileListInfo();
            rateProfileListInfo.setRandomRateId(keyMapping.getRandomString(rateProfile, WebCoreConstants.ID_LOOK_UP_NAME));
            rateProfileListInfo.setName(rateProfile.getName());
            rateProfileListInfo.setPermitIssueTypeId(rateProfile.getPermitIssueType().getId());
            rateProfileListInfo.setPermitIssueTypeName(rateProfile.getPermitIssueType().getName());
            rateProfileListInfo.setIsPublished(rateProfile.isIsPublished());
            rateProfileListInfoList.add(rateProfileListInfo);
        }
        return rateProfileListInfoList;
    }
    
    private RateProfileLocation populateRateProfileLocation(final RateProfileLocationForm form, final RandomKeyMapping keyMapping) {
        final RateProfileLocation domain = new RateProfileLocation();
        
        Integer id = keyMapping.getKey(form.getRandomId(), RateProfileLocation.class, Integer.class);
        if ((id != null) && (id >= 0)) {
            domain.setId(id);
        }
        
        id = keyMapping.getKey(form.getLocationRandomId(), Location.class, Integer.class);
        if (id != null) {
            domain.setLocation(this.locationService.findLocationById(id));
        }
        
        domain.setStartGmt(form.getStartDateObj());
        domain.setEndGmt(form.getEndDateObj());
        domain.setSpaceRange(form.getSpaceRange());
        domain.setIsDeleted(form.isIsDeleted());
        
        return domain;
    }
    
    protected final List<FilterDTO> createLocationLists(final Integer customerId, final RandomKeyMapping keyMapping) {
        final List<Location> locsList = this.locationService.getLocationByCustomerId(customerId, true, true);
        final List<FilterDTO> result = new ArrayList<FilterDTO>(locsList.size());
        for (Location loc : locsList) {
            final FilterDTO filter = new FilterDTO();
            filter.setLabel(loc.getName());
            filter.setValue(keyMapping.getRandomString(Location.class, loc.getId()));
            if (loc.getLocation() != null) {
                filter.setDesc(loc.getLocation().getName());
            }
            
            filter.setExtraData("" + loc.getPermitIssueType().getId());
            
            result.add(filter);
        }
        
        return result;
    }
    
    @ModelAttribute(FORM_SEARCH)
    public final WebSecurityForm<RateProfileSearchForm> initRateProfileSearchForm(final HttpServletRequest request) {
        return new WebSecurityForm<RateProfileSearchForm>(null, new RateProfileSearchForm());
    }
    
    @ModelAttribute(RateProfileLocationController.FORM_EDIT_PROFILE)
    public final WebSecurityForm<RateProfileLocationForm> initEditProfileForm(final HttpServletRequest request) {
        return new WebSecurityForm<RateProfileLocationForm>((Object) null, new RateProfileLocationForm(), SessionTool.getInstance(request)
                .locatePostToken(RateProfileLocationController.FORM_EDIT_PROFILE));
    }
    
    @ModelAttribute(RateProfileLocationController.FORM_EDIT_LOCATION)
    public final WebSecurityForm<RateProfileLocationForm> initEditLocationForm(final HttpServletRequest request) {
        return new WebSecurityForm<RateProfileLocationForm>((Object) null, new RateProfileLocationForm(), SessionTool.getInstance(request)
                .locatePostToken(RateProfileLocationController.FORM_EDIT_LOCATION));
    }
}
