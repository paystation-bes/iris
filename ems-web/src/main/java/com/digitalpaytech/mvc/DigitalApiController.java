package com.digitalpaytech.mvc;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.dto.RestAccountInfo;
import com.digitalpaytech.dto.WebServiceEndPointInfo;
import com.digitalpaytech.dto.customeradmin.CustomerDetails;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.RestAccountService;
import com.digitalpaytech.service.WebServiceEndPointService;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;

public class DigitalApiController {
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private RestAccountService restAccountService;
    
    @Autowired
    private WebServiceEndPointService webServiceEndPointService;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    public String digitalApiControllerIndex(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model) {
        final String randomCustomerId = request.getParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID);
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Integer customerId = this.verifyRandomIdAndReturnActual(response, keyMapping, randomCustomerId,
                                                                      "Invalid customer random id in Digital Api Controller");
        final Customer customer = this.customerService.findCustomer(customerId);
        final List<RestAccountInfo> restAccts = RestAccountInfo.convertToRestAccountInfo(this.restAccountService.findRestAccounts(customerId),
                                                                                         keyMapping);
        final List<WebServiceEndPointInfo> wsPoints = WebServiceEndPointInfo.convertToWebServiceEndPointInfo(this.webServiceEndPointService
                .findWebServiceEndPointsByCustomer(customerId), keyMapping);
        final List<WebServiceEndPointInfo> privateWsPoints = WebServiceEndPointInfo.convertToWebServiceEndPointInfo(this.webServiceEndPointService
                                                                                                             .findPrivateWebServiceEndPointsByCustomer(customerId), keyMapping);
        final String timeZone = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customer.getId(), WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE)
                .getPropertyValue();
        final CustomerDetails customerDetails = new CustomerDetails(customer, null, null, null, null, wsPoints, privateWsPoints, restAccts, null,
                timeZone == null || !timeZone.startsWith("US/"));
        model.put("customerDetails", customerDetails);
        return "/systemAdmin/workspace/licenses/digitalApi";
    }
    
    protected final Integer verifyRandomIdAndReturnActual(final HttpServletResponse response, final RandomKeyMapping keyMapping,
        final String strRandomId, final String message) {
        final String[] msgs = new String[] { message, message };
        return WebCoreUtil.verifyRandomIdAndReturnActual(response, keyMapping, strRandomId, msgs);
    }
}
