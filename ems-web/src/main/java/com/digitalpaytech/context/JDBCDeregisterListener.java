package com.digitalpaytech.context;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

/**
 * This listener deregisters JDBC drivers before Tomcat server shuts down.
 * 
 * @author Brian Kim
 *
 */
public class JDBCDeregisterListener implements ServletContextListener {

	private static Logger logger = Logger.getLogger(JDBCDeregisterListener.class);
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		// TODO Auto-generated method stub

	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		
		Enumeration<java.sql.Driver> drivers = DriverManager.getDrivers();
	    while (drivers.hasMoreElements()) {
	        Driver driver = drivers.nextElement();
	        ClassLoader driverclassLoader = driver.getClass().getClassLoader();
	        ClassLoader thisClassLoader = this.getClass().getClassLoader();
	        if (driverclassLoader != null && thisClassLoader != null &&  driverclassLoader.equals(thisClassLoader)) {
	            try {
	            	logger.warn("Deregistering: " + driver);
	                DriverManager.deregisterDriver(driver);
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}

}
