package com.digitalpaytech.constants;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.digitalpaytech.data.RangeData;

public enum DateRangeOption {
    DATE_RANGE(true, true) {
        @Override
        protected RangeData<Date> performCalculation(final TimeZone timeZone, final Date start, final Date end) {
            return createDateRange(timeZone, start, end);
        }
    },
    DATE_TIME_RANGE(true, true) {
        @Override
        protected RangeData<Date> performCalculation(final TimeZone timeZone, final Date start, final Date end) {
            return createDateRange(timeZone, start, end);
        }
    },
    MONTH_RANGE(true, true) {
        @Override
        protected RangeData<Date> performCalculation(final TimeZone timeZone, final Date start, final Date end) {
            return createDateRange(timeZone, start, end);
        }
    },
    TODAY() {
        @Override
        protected RangeData<Date> performCalculation(final TimeZone timeZone, final Date start, final Date end) {
            final Calendar cal = Calendar.getInstance();
            cal.setTimeZone(timeZone);
            
            return createSingleDateRange(cal, Calendar.DAY_OF_YEAR);
        }
    },
    YESTERDAY() {
        @Override
        protected RangeData<Date> performCalculation(final TimeZone timeZone, final Date start, final Date end) {
            final Calendar cal = Calendar.getInstance();
            cal.setTimeZone(timeZone);
            cal.add(Calendar.DAY_OF_YEAR, -1);
            
            return createSingleDateRange(cal, Calendar.DAY_OF_YEAR);
        }
    },
    LAST_24_HOURS() {
        @Override
        protected RangeData<Date> performCalculation(final TimeZone timeZone, final Date start, final Date end) {
            return createIntervalDateRange(timeZone, Calendar.HOUR_OF_DAY, -24, true);
        }
    },
    THIS_WEEK() {
        @Override
        protected RangeData<Date> performCalculation(final TimeZone timeZone, final Date start, final Date end) {
            final Calendar cal = Calendar.getInstance();
            cal.setTimeZone(timeZone);
            
            return createSingleWeekDateRange(cal);
        }
    },
    LAST_WEEK() {
        @Override
        protected RangeData<Date> performCalculation(final TimeZone timeZone, final Date start, final Date end) {
            final Calendar cal = Calendar.getInstance();
            cal.setTimeZone(timeZone);
            cal.add(Calendar.WEEK_OF_YEAR, -1);
            
            return createSingleWeekDateRange(cal);
        }
    },
    LAST_7_DAYS() {
        @Override
        protected RangeData<Date> performCalculation(final TimeZone timeZone, final Date start, final Date end) {
            return createIntervalDateRange(timeZone, Calendar.DAY_OF_YEAR, -7, false);
        }
    },
    THIS_MONTH() {
        @Override
        protected RangeData<Date> performCalculation(final TimeZone timeZone, final Date start, final Date end) {
            final Calendar cal = Calendar.getInstance();
            cal.setTimeZone(timeZone);
            
            return createSingleDateRange(cal, Calendar.MONTH);
        }
    },
    LAST_MONTH() {
        @Override
        protected RangeData<Date> performCalculation(final TimeZone timeZone, final Date start, final Date end) {
            final Calendar cal = Calendar.getInstance();
            cal.setTimeZone(timeZone);
            cal.add(Calendar.MONTH, -1);
            
            return createSingleDateRange(cal, Calendar.MONTH);
        }
    },
    LAST_30_DAYS() {
        @Override
        protected RangeData<Date> performCalculation(final TimeZone timeZone, final Date start, final Date end) {
            return createIntervalDateRange(timeZone, Calendar.DAY_OF_YEAR, -30, false);
        }
    },
    LAST_12_MONTHS() {
        @Override
        protected RangeData<Date> performCalculation(final TimeZone timeZone, final Date start, final Date end) {
            return createIntervalDateRange(timeZone, Calendar.MONTH, -12, false);
        }
    },
    THIS_YEAR() {
        @Override
        protected RangeData<Date> performCalculation(final TimeZone timeZone, final Date start, final Date end) {
            final Calendar cal = Calendar.getInstance();
            cal.setTimeZone(timeZone);
            
            return createSingleDateRange(cal, Calendar.YEAR);
        }
    },
    LAST_YEAR() {
        @Override
        protected RangeData<Date> performCalculation(final TimeZone timeZone, final Date start, final Date end) {
            final Calendar cal = Calendar.getInstance();
            cal.setTimeZone(timeZone);
            cal.add(Calendar.YEAR, -1);
            
            return createSingleDateRange(cal, Calendar.YEAR);
        }
    };
    
    private boolean requireStart;
    private boolean requireEnd;
    
    DateRangeOption() {
        this(false, false);
    }
    
    DateRangeOption(final boolean requireStart, final boolean requireEnd) {
        this.requireStart = requireStart;
        this.requireEnd = requireEnd;
    }
    
    public boolean isRequireStart() {
        return requireStart;
    }
    
    public boolean isRequireEnd() {
        return requireEnd;
    }
    
    protected abstract RangeData<Date> performCalculation(TimeZone timeZone, Date start, Date end);
    
    public RangeData<Date> calculateRange(TimeZone timeZone, Date start, Date end) {
        if (this.requireStart && (start == null)) {
            throw new IllegalArgumentException("Could not calculate " + this + " range with out \"start\" of the range !");
        }
        if (this.requireEnd && (end == null)) {
            throw new IllegalArgumentException("Could not calculate " + this + " range with out \"end\" of the range !");
        }
        
        return performCalculation(timeZone, null, null);
    }
    
    public RangeData<Date> calculateRange() {
        return calculateRange(TimeZone.getDefault());
    }
    
    public RangeData<Date> calculateRange(TimeZone timeZone) {
        if (this.requireStart) {
            throw new IllegalArgumentException("Could not calculate " + this + " range with out \"start\" of the range !");
        }
        if (this.requireEnd) {
            throw new IllegalArgumentException("Could not calculate " + this + " range with out \"end\" of the range !");
        }
        
        return performCalculation(timeZone, null, null);
    }
    
    protected static RangeData<Date> createDateRange(TimeZone timeZone, Date start, Date end) {
        return new RangeData<Date>(start, end);
    }
    
    protected static RangeData<Date> createSingleWeekDateRange(Calendar week) {
        RangeData<Date> result = new RangeData<Date>();
        
        int year = week.get(Calendar.YEAR);
        int month = week.get(Calendar.MONTH);
        int date = week.get(Calendar.DAY_OF_MONTH);
        
        week.clear();
        week.set(year, month, date);
        week.getTime(); // Force recalculation for the new Date.
        
        week.set(Calendar.DAY_OF_WEEK, week.getFirstDayOfWeek());
        result.setStart(week.getTime());
        
        week.add(Calendar.WEEK_OF_YEAR, 1);
        result.setEnd(week.getTime());
        
        return result;
    }
    
    protected static RangeData<Date> createSingleDateRange(Calendar target, int field) {
        RangeData<Date> result = new RangeData<Date>();
        
        clearLowerPriorityFields(target, field);
        result.setStart(target.getTime());
        
        target.add(field, 1);
        result.setEnd(target.getTime());
        
        return result;
    }
    
    protected static void clearLowerPriorityFields(Calendar cal, int field) {
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int date = cal.get(Calendar.DAY_OF_MONTH);
        int hourOfDay = cal.get(Calendar.HOUR_OF_DAY);
        int minute = cal.get(Calendar.MINUTE);
        int second = cal.get(Calendar.SECOND);
        switch (field) {
            case Calendar.YEAR: {
                month = 0;
            }
                ;
            case Calendar.MONTH: {
                date = 1;
            }
                ;
            case Calendar.DAY_OF_MONTH:
                ;
            case Calendar.DAY_OF_YEAR: {
                hourOfDay = 0;
            }
            case Calendar.HOUR:
                ;
            case Calendar.HOUR_OF_DAY: {
                minute = 0;
            }
            case Calendar.MINUTE: {
                second = 0;
                break;
            }
            default: {
                throw new IllegalArgumentException("Unsupported Calendar's field: " + field);
            }
        }
        
        cal.clear();
        cal.set(year, month, date, hourOfDay, minute, second);
    }
    
    protected static RangeData<Date> createIntervalDateRange(TimeZone timeZone, int field, int interval, boolean includeNow) {
        RangeData<Date> result = new RangeData<Date>();
        
        Calendar curr = Calendar.getInstance();
        curr.setTimeZone(timeZone);
        
        Date first = null;
        if (includeNow) {
            first = curr.getTime();
            clearLowerPriorityFields(curr, field);
        } else {
            clearLowerPriorityFields(curr, field);
            first = curr.getTime();
        }
        
        if (interval < 0) {
            result.setEnd(first);
            
            curr.add(field, interval);
            result.setStart(curr.getTime());
        } else {
            result.setStart(first);
            
            curr.add(field, interval);
            result.setEnd(curr.getTime());
        }
        
        return result;
    }
    
    public static void main(String[] args) {
        Calendar cal = Calendar.getInstance();
        System.out.println(cal.getTime());
        
        cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
        System.out.println(cal.getTime());
    }
}
