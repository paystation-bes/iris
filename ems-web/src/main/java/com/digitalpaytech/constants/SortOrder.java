package com.digitalpaytech.constants;

public enum SortOrder {
	ASC, DESC;
}
