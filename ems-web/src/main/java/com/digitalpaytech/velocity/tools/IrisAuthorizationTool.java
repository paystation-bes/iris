package com.digitalpaytech.velocity.tools;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.SubscriptionType;
import com.digitalpaytech.dto.customeradmin.CustomerDetails;
import com.digitalpaytech.mvc.helper.PermissionChecker;
import com.digitalpaytech.mvc.helper.SubscriptionChecker;
import com.digitalpaytech.mvc.helper.UserLevelChecker;
import com.digitalpaytech.service.PermissionService;
import com.digitalpaytech.service.SubscriptionTypeService;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.scripting.CalculableNode;
import com.digitalpaytech.util.scripting.InvalidExpressionException;
import com.digitalpaytech.util.scripting.impl.BooleanExpressionBuilder;
import com.digitalpaytech.util.scripting.impl.BooleanExpressionTokenizer;
import com.digitalpaytech.util.scripting.operator.LogicalAND;
import com.digitalpaytech.util.scripting.operator.ReferenceOperator;

@Component("IrisAuthorizationTool")
public class IrisAuthorizationTool {
    private static Logger log = Logger.getLogger(IrisAuthorizationTool.class);
    
    private static final String PREFIX_PERMISSION = "PERMISSION_";
    private static final String PREFIX_SUBSCRIPTION = "SUBSCRIPTION_TYPE_";
    
    private static final String KEY_PREFIX_CUSTOMER = "customer.";
    private static final String KEY_PREFIX_SYSTEM = "system.";
    
    @Autowired
    private PermissionService permissionService;
    
    @Autowired
    private SubscriptionTypeService subscriptionService;
    
    private FieldAccessor fieldAccessor;
    private BooleanExpressionBuilder nodeBuilder;
    
    private Map<String, CalculableNode<Boolean>> authsMap;
    
    public IrisAuthorizationTool() {
        
    }
    
    @PostConstruct
    protected void init() {
        Properties conf = new Properties();
        try {
            InputStream is = this.getClass().getResourceAsStream(WebCoreConstants.PATH_UI_ACCESS_CONF);
            try {
                conf.load(is);
                loadAuthConfig(conf);
            } finally {
                is.close();
            }
        } catch (IOException ioe) {
            log.error("Could not load \"" + WebCoreConstants.PATH_UI_ACCESS_CONF + "\"", ioe);
        }
        
        log.info("Initialized !");
    }
    
    private void loadAuthConfig(Properties conf) {
        log.info("Loading " + conf.size() + " authorization configurations...");
        
        final Map<Integer, Set<Integer>> parentsChildsPermissionMap = new HashMap<Integer, Set<Integer>>();
        final HashSet<Integer> unusedPermissionIds = new HashSet<Integer>();
        final HashSet<Integer> unusedSubscriptionIds = new HashSet<Integer>();
        final HashSet<String> uninitReferences = new HashSet<String>();
        
        List<Permission> permissions = this.permissionService.findAllPermissions();
        for (Permission perm : permissions) {
            if (!perm.isIsParent()) {
                unusedPermissionIds.add(perm.getId());
            }
            
            if (perm.getPermission() != null) {
                Set<Integer> childs = parentsChildsPermissionMap.get(perm.getPermission().getId());
                if (childs == null) {
                    childs = new HashSet<Integer>();
                    parentsChildsPermissionMap.put(perm.getPermission().getId(), childs);
                }
                
                childs.add(perm.getId());
            }
        }
        
        log.debug("Loaded " + permissions.size() + " permissions.");
        
        List<SubscriptionType> stsList = this.subscriptionService.loadAll();
        for (SubscriptionType st : stsList) {
            if (!st.isIsParent()) {
                unusedSubscriptionIds.add(st.getId());
            }
        }
        
        log.debug("Loaded " + stsList.size() + " subscriptions.");
        
        this.authsMap = new HashMap<String, CalculableNode<Boolean>>(conf.size() * 2);
        
        this.fieldAccessor = new FieldAccessor();
        
        this.nodeBuilder = new BooleanExpressionBuilder() {
            @Override
            public CalculableNode<Boolean> createOperand(CharSequence input) throws InvalidExpressionException {
                CalculableNode<Boolean> result = null;
                
                String fieldName = input.toString();
                if (fieldName.startsWith(PREFIX_PERMISSION)) {
                    Integer fieldId = fieldAccessor.getPermission(fieldName);
                    if ((fieldId == null) || (fieldId == -1)) {
                        throw new InvalidExpressionException("Un-recognized permission name: " + fieldName);
                    }
                    
                    result = new PermissionChecker(fieldId);
                    unusedPermissionIds.remove(fieldId);
                    
                    Set<Integer> childs = parentsChildsPermissionMap.remove(fieldId);
                    if (childs != null) {
                        unusedPermissionIds.removeAll(childs);
                    }
                } else if (fieldName.startsWith(PREFIX_SUBSCRIPTION)) {
                    Integer fieldId = fieldAccessor.getSubscription(fieldName);
                    if ((fieldId == null) || (fieldId == -1)) {
                        throw new InvalidExpressionException("Un-recognized subscription name: " + fieldName);
                    }
                    
                    result = new SubscriptionChecker(fieldId);
                    unusedSubscriptionIds.remove(fieldId);
                } else {
                    Matcher m = PTTRN_BOOLEAN.matcher(input);
                    if (m.find()) {
                        result = super.createOperand(input);
                    } else {
                        result = authsMap.get(fieldName);
                        if (result == null) {
                            result = new CachedOperator<Boolean>();
                            authsMap.put(fieldName, result);
                            uninitReferences.add(fieldName);
                        }
                    }
                }
                
                return result;
            }
        };
        
        UserLevelChecker systemChecker = new UserLevelChecker(true);
        UserLevelChecker customerChecker = new UserLevelChecker(false);
        
        for (Object key : conf.keySet()) {
            if (key instanceof String) {
                String authName = (String) key;
                String authExpr = conf.getProperty(authName);
                if ("ignored".equals(key)) {
                    for (String attrName : authExpr.split(",")) {
                        attrName = attrName.trim();
                        if (attrName.startsWith(PREFIX_PERMISSION)) {
                            unusedPermissionIds.remove(fieldAccessor.getPermission(attrName));
                        } else if (attrName.startsWith(PREFIX_SUBSCRIPTION)) {
                            unusedSubscriptionIds.remove(fieldAccessor.getSubscription(attrName));
                        } else {
                            log.error("Unknown Attribute: " + attrName);
                        }
                    }
                } else {
                    CalculableNode<Boolean> authLogic = BooleanExpressionBuilder.OPERAND_FALSE;
                    
                    authExpr = authExpr.trim();
                    if (authExpr.length() > 0) {
                        try {
                            authLogic = CalculableNode.createOperation(new BooleanExpressionTokenizer(authExpr), this.nodeBuilder);
                            log.debug("Loaded UI authorization \"" + authName + "\": " + authLogic);
                        } catch (Exception e) {
                            log.error("Could not load UI authorization \"" + authName + "\" !", e);
                        }
                    }
                    
                    if (authName.startsWith(KEY_PREFIX_CUSTOMER)) {
                        authLogic = new LogicalAND(customerChecker, authLogic);
                    } else if (authName.startsWith(KEY_PREFIX_SYSTEM)) {
                        authLogic = new LogicalAND(systemChecker, authLogic);
                    }
                    
                    CalculableNode<Boolean> existingLogic = this.authsMap.get(authName);
                    if ((existingLogic != null) && (existingLogic instanceof ReferenceOperator)) {
                        ReferenceOperator<Boolean> ref = (ReferenceOperator<Boolean>) existingLogic;
                        ref.addOperand(authLogic);
                        
                        uninitReferences.remove(authName);
                    } else {
                        this.authsMap.put(authName, new CachedOperator<Boolean>(authLogic));
                    }
                }
            }
        }
        
        if (unusedPermissionIds.size() > 0) {
            log.error("The following permissions are not used on UI: " + StringUtils.join(unusedPermissionIds, ','));
        }
        if (unusedSubscriptionIds.size() > 0) {
            log.error("The following subscriptions are not used on UI: " + StringUtils.join(unusedSubscriptionIds, ','));
        }
        if (uninitReferences.size() > 0) {
            log.error("The following authorization reference are not defined in the properties file: " + StringUtils.join(uninitReferences, ','));
        }
    }
    
    public boolean allow(String sectionName) {
        boolean result = false;
        CalculableNode<Boolean> logic = this.authsMap.get(sectionName);
        if (logic == null) {
            // @ TODO LOG
        } else {
            try {
                result = logic.execute(null);
            } catch (Exception e) {
                log.error("Exception thrown when evaluating expression \"" + logic + "\" !", e);
            }
        }
        
        return result;
    }
    
    public boolean allow(String sectionName, CustomerDetails custDtl) {
        Map<String, Object> ctx = new HashMap<String, Object>();
        if (custDtl != null) {
            ctx.put(SubscriptionChecker.CTXKEY_CUSTOMER_DETAILS, custDtl);
        }
        
        boolean result = false;
        CalculableNode<Boolean> logic = this.authsMap.get(sectionName);
        if (logic == null) {
            // @ TODO LOG
        } else {
            try {
                result = logic.execute(ctx);
            } catch (Exception e) {
                log.error("Exception thrown when evaluating expression \"" + logic + "\" !", e);
            }
        }
        
        return result;
    }
    
    protected CalculableNode<Boolean> compile(String expression) {
        BooleanExpressionTokenizer tokenizer = new BooleanExpressionTokenizer(expression);
        return CalculableNode.createOperation(tokenizer, this.nodeBuilder);
    }
    
    private static class FieldAccessor {
        private Map<String, Integer> permissionIdsMap;
        private Map<String, Integer> subscriptionIdsMap;
        
        public FieldAccessor() {
            this.permissionIdsMap = new HashMap<String, Integer>();
            this.subscriptionIdsMap = new HashMap<String, Integer>();
        }
        
        public Integer getSubscription(String name) {
            Integer id = this.subscriptionIdsMap.get(name);
            if (id == null) {
                try {
                    id = WebCoreConstants.class.getDeclaredField(name).getInt(WebCoreConstants.class);
                } catch (NoSuchFieldException nsfe) {
                    log.error("Failed to locate subscription \"" + name + "\" from WebCoreConstants !");
                } catch (IllegalAccessException iae) {
                    log.error("Failed to read value of subscription \"" + name + "\" from WebCoreConstants !");
                }
                
                if (id == null) {
                    id = -1;
                }
                
                this.subscriptionIdsMap.put(name, id);
            }
            
            return id;
        }
        
        public Integer getPermission(String name) {
            Integer id = this.permissionIdsMap.get(name);
            if (id == null) {
                try {
                    id = WebSecurityConstants.class.getDeclaredField(name).getInt(WebSecurityConstants.class);
                } catch (NoSuchFieldException nsfe) {
                    log.error("Failed to locate permission \"" + name + "\" from WebSecurityConstants !");
                } catch (IllegalAccessException iae) {
                    log.error("Failed to read value of permission \"" + name + "\" from WebSecurityConstants !");
                }
                
                if (id == null) {
                    id = -1;
                }
                
                this.permissionIdsMap.put(name, id);
            }
            
            return id;
        }
    }
}
