package com.digitalpaytech.velocity.tools;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.service.EmsPropertiesService;

@Component("EmsPropertiesTool")
public class EmsPropertiesTool {
    private static final Logger LOG = Logger.getLogger(EmsPropertiesTool.class);
    
    @Autowired
    private EmsPropertiesService propService;
    
    private String mappingURL;
    
    private String releaseVersion;
    
    private String staticDir;
    
    private String buildDate;
    
    private String buildDateHash;
    
    private String deploymentMode;
    
    @PostConstruct
    private void init() {
        LOG.info("Initializing EmsPropertiesTool...");
        
        this.mappingURL = this.propService.getPropertyValue(EmsPropertiesService.MAPPING_URL_PROPERTY_NAME);
        
        this.releaseVersion = this.propService.getIrisVersion();
        this.buildDate = this.propService.getIrisBuildDate();
        
        this.staticDir = this.propService.getStaticContentPath();
        this.buildDateHash = this.propService.getStaticContentPatam();
        
        this.deploymentMode = this.propService.getIrisDeploymentMode();
        
        this.propService = null;
    }
    
    public final String getMappingURL() {
        return this.mappingURL;
    }
    
    public final String getReleaseVersion() {
        return this.releaseVersion;
    }
    
    public final String getBuildDate() {
        return this.buildDate;
    }
    
    public final String getBuildDateHash() {
        return this.buildDateHash;
    }
    
    public final String getStaticDir() {
        return this.staticDir;
    }
    
    public final String getDeploymentMode() {
        return this.deploymentMode;
    }
}
