package com.digitalpaytech.velocity.tools;

import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.context.Context;
import org.apache.velocity.tools.ToolContext;
import org.apache.velocity.tools.ToolManager;
import org.apache.velocity.tools.generic.LinkTool;
import org.apache.velocity.tools.view.CookieTool;
import org.apache.velocity.tools.view.ParameterTool;
import org.springframework.web.servlet.view.velocity.VelocityView;

public class VelocityToolsView extends VelocityView {

    private static ToolContext toolContext = initVelocityToolContext();

    @Override
    protected Context createVelocityContext(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
        VelocityContext context = new VelocityContext(toolContext);
        if(model != null) {
            for(Map.Entry<String, Object> entry : (Set<Map.Entry<String, Object>>)model.entrySet()) {
                context.put(entry.getKey(), entry.getValue());
            }
        }
        context.put("params", new ParameterTool(request));
        context.put("link", new LinkTool());
        CookieTool cookieTool = new CookieTool();
		cookieTool.setRequest(request);
		cookieTool.setResponse(response);
        context.put("cookies", cookieTool);
        return context;
    }

    private static ToolContext initVelocityToolContext() {
        if(toolContext == null) {
            ToolManager velocityToolManager = new ToolManager();
            toolContext = velocityToolManager.createContext();
        }
        return toolContext;
    }
}
