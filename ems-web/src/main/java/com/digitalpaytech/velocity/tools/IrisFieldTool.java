package com.digitalpaytech.velocity.tools;

import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.apache.velocity.tools.generic.FieldTool;

public class IrisFieldTool extends FieldTool {
	private static final Logger logger = Logger.getLogger(IrisFieldTool.class);
	
	private ConcurrentHashMap<Class<?>, FieldToolSub> cache = new ConcurrentHashMap<Class<?>, FieldToolSub>();
	
	@Override
	public FieldToolSub in(@SuppressWarnings("rawtypes") Class clazz) {
		FieldToolSub result = this.cache.get(clazz);
		if(result == null) {
			logger.info("Prepare field for class: " + clazz.getName());
			result = super.in(clazz);
			
			FieldToolSub buffer = this.cache.putIfAbsent(clazz, result);
			if(buffer != null) {
				result = buffer;
			}
		}
		
		return result;
	}

}
