package com.digitalpaytech.velocity.tools;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import com.digitalpaytech.util.scripting.CalculableNode;
import com.digitalpaytech.util.scripting.operator.ReferenceOperator;

public class CachedOperator<R> extends ReferenceOperator<R> {
	private static final long serialVersionUID = 2368762395574646148L;

	public CachedOperator() {
		
	}
	
	public CachedOperator(CalculableNode<R> operand) {
		super(operand);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public R execute(Map<String, Object> context) {
		R result = null;
		Map<Object, R> resultsCache = null;
		
		RequestAttributes attr = RequestContextHolder.getRequestAttributes();
		if(attr != null) {
			String cacheKey = this.getClass().getName();
			
			resultsCache = (Map<Object, R>) attr.getAttribute(cacheKey, RequestAttributes.SCOPE_REQUEST);
			if(resultsCache == null) {
				resultsCache = new HashMap<Object, R>();
				attr.setAttribute(cacheKey, resultsCache, RequestAttributes.SCOPE_REQUEST);
			}
		}
		
		if(resultsCache != null) {
			result = resultsCache.get(this);
		}
		
		if(result == null) {
			result = this.getOperand().execute(context);
			if(resultsCache != null) {
				resultsCache.put(this, result);
			}
		}
		
		return result;
	}
}
