package com.digitalpaytech.validation;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.fileupload.FileUploadBase;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.CommonValidator;
import com.digitalpaytech.util.DashboardUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObjectId;

@Component("baseValidator")
public abstract class BaseValidator<W extends Serializable> extends CommonValidator<WebSecurityForm<W>> implements Validator, MessageSourceAware {
        
    private static final String MESSAGE_KEY_DATE = "label.date";
    private static final String MESSAGE_KEY_TIME = "label.time";
    private static final Logger LOG = Logger.getLogger(BaseValidator.class);
    
    private MessageSourceAccessor messageAccessor;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    public abstract void validate(WebSecurityForm<W> target, Errors errors);
    
    @Override
    public boolean supports(@SuppressWarnings("rawtypes") Class clazz) {
        return WebSecurityForm.class.isAssignableFrom(clazz);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public final void validate(Object target, Errors errors) {
        validate((WebSecurityForm<W>) target, errors);
    }
    
    @Override
    protected void addError(WebSecurityForm<W> messageContainer, String fieldName, String message) {
        messageContainer.addErrorStatus(new FormErrorStatus(fieldName, message));
    }
    
    @Override
    public String getMessage(String code, Object... args) {
        return messageAccessor.getMessage(code, args);
    }
    
    protected void addErrorStatus(WebSecurityForm<W> form, String fieldName, String messageKey, Object... messageArgs) {
        form.addErrorStatus(new FormErrorStatus(fieldName, this.getMessage(messageKey, messageArgs)));
    }
    
    public MessageSourceAccessor getMessageAccessor() {
        return messageAccessor;
    }
    
    public void setMessageAccessor(MessageSourceAccessor messageAccessor) {
        this.messageAccessor = messageAccessor;
    }
    
    public void setMessageSource(MessageSource messageSource) {
        //
        messageAccessor = new MessageSourceAccessor(messageSource);
    }
    
    public String getMessage(String code) {
        return messageAccessor.getMessage(code);
    }
    
    public String getMessage(String code, Locale locale) {
        return messageAccessor.getMessage(code, locale);
    }
    
    public String getMessage(String code, Object[] args, Locale locale) {
        return messageAccessor.getMessage(code, args, locale);
    }
    
    protected WebSecurityForm<W> prepareSecurityForm(WebSecurityForm<W> webSecurityForm, Errors errors, String fieldName, boolean validatePostToken) {
        webSecurityForm.removeAllErrorStatus();
        
        // Check Post Token
        if (validatePostToken && (!webSecurityForm.isCorrectPostToken())) {
            webSecurityForm.addErrorStatus(new FormErrorStatus(fieldName, this.getMessage("error.common.invalid.posttoken")));
            return null;
        }
        return webSecurityForm;
    }
    
    public boolean validateMigrationStatus(WebSecurityForm<W> webSecurityForm) {
        boolean returnValue = WebSecurityUtil.isCustomerMigrated();
        if (!returnValue) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("postToken", this.getMessage("error.customer.notMigrated")));
            webSecurityForm.resetToken();
        }
        
        return returnValue;
    }
    
    public boolean validateSystemAdminMigrationStatus(WebSecurityForm<W> webSecurityForm, Customer customer) {
        boolean returnValue = customer.isIsMigrated();
        if (!returnValue) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("postToken", this.getMessage("error.customer.notMigrated.systemAdmin")));
            webSecurityForm.resetToken();
        }
        
        return returnValue;
    }
    
    protected WebSecurityForm<W> prepareSecurityForm(WebSecurityForm<W> command, Errors errors, String fieldName) {
        return prepareSecurityForm(command, errors, fieldName, true);
    }
    
    protected boolean isIntegerOrFloat(String value) {
        if (isEmpty(value)) {
            return (false);
        }
        for (int i = 0; i < value.length(); i++) {
            char character = value.charAt(i);
            if (!Character.isDigit(character) && character != '.') {
                return (false);
            }
        }
        return true;
    }
    
    protected boolean isAcceptedWildCardChar(char character) {
        // Check if allowed wildcard
        for (int i = 0; i < WebSecurityConstants.ALLOWED_WILDCARD_CHARACTERS.length; i++) {
            if (character == WebSecurityConstants.ALLOWED_WILDCARD_CHARACTERS[i]) {
                return (true);
            }
        }
        return (false);
    }
    
    protected boolean isEmpty(String value) {
        return (value == null || value.trim().length() == 0);
    }
    
    // for validating dates coming from database
    protected boolean isDateFormat(String value) {
        if ((value.length() != 10) && (value.length() != 19))
            return (false);
        
        for (int i = 0; i < value.length(); i++) {
            if (i == 4 || i == 7 || i == 10) {
                if (value.charAt(i) != '-')
                    return (false);
            } else if (i == 13 || i == 16) {
                if (value.charAt(i) != ':')
                    return (false);
            } else {
                if (!Character.isDigit(value.charAt(i)))
                    return (false);
            }
        }
        return (true);
    }
    
    protected boolean checkDateRange(Date startDate, Date endDate) {
        if (endDate == null || startDate == null)
            return (false);
        
        return (endDate.after(startDate));
    }
    
    protected boolean checkEndDate(Date endDate, Date currentDate) {
        if (endDate == null || currentDate == null)
            return (false);
        
        return (endDate.after(currentDate) || DateUtils.isSameDay(endDate, currentDate));
    }
    
    public boolean checkIfAllDigitsForFloat(String input) {
        String[] nums = input.split("\\.");
        // Max array size is 2.
        if (nums.length > 2) {
            return false;
        }
        for (String num : nums) {
            if (!WebCoreUtil.checkIfAllDigits(num)) {
                return false;
            }
        }
        return true;
    }
    
    protected boolean validateDropDownList(Object formObj, String fieldName, Object[] values, String labelCode, Errors errors) {
        String label = getMessage(labelCode);
        try {
            // get the property's String value
            Object value = PropertyUtils.getProperty(formObj, fieldName);
            ArrayList<Object> valueList = new ArrayList<Object>();
            for (int i = 0; i < values.length; i++)
                valueList.add(values[i]);
            if (!valueList.contains(value)) {
                errors.reject("error.common.invalid", new Object[] { label }, "error.common.invalid");
                return false;
            }
            
        } catch (Exception ex) {
            errors.reject("error.common.invalid", new Object[] { label }, "error.common.invalid");
            return false;
        }
        return true;
    }
    
    /**
     * @return boolean 'true' if value is 'true' or 'false'.
     */
    public boolean isBooleanString(String value) {
        if (StringUtils.isNotBlank(value) && (value.equalsIgnoreCase(Boolean.TRUE.toString()) || value.equalsIgnoreCase(Boolean.FALSE.toString()))) {
            return true;
        }
        return false;
    }
    
    public static boolean isSame(String string1, String string2) {
        if (StringUtils.isBlank(string1) || StringUtils.isBlank(string2)) {
            return false;
        }
        if (!string1.equals(string2)) {
            return false;
        }
        return true;
    }
    
    public static boolean isSameSessionToken(HttpServletRequest request, HttpSession httpSession) {
        if (request == null || httpSession == null) {
            return false;
        }
        
        //TODO Remove SessionToken
        //        String incomingSession = httpSession.getId();
        //        String existingToken = (String) httpSession.getAttribute(WebSecurityConstants.SESSION_TOKEN);
        //        
        //        if (StringUtils.isBlank(incomingSession) || StringUtils.isBlank(existingToken) || !incomingSession.equals(existingToken)) {
        //            return false;
        //        }
        
        return true;
    }
    
    /**
     * Validate values of action flags.
     * 
     * @param form
     *            form The WebSecurityForm Object containing the action flag
     * @param flagVals
     *            the action flags values, one of which must be equal to the action flag in form
     * 
     * @return true if flag is one of values supplied in FlagVals otherwise
     */
    protected boolean validateActionFlag(WebSecurityForm<W> form, Integer... flagVals) {
        HashSet<Integer> flagSet = new HashSet<Integer>();
        if (flagVals != null) {
            for (int i = -1; ++i < flagVals.length;) {
                flagSet.add(flagVals[i]);
            }
        }
        if (!flagSet.contains(form.getActionFlag())) {
            String label = this.getMessage("label.settings.actionFlg");
            form.addErrorStatus(new FormErrorStatus("actionFlag", this.getMessage("error.common.invalid", new Object[] { label })));
            return false;
        }
        return true;
    }
    
    /**
     * Validate whether the random key (database primary key) is in the correct format. This method is working the same way as
     * {@link #validateRandomIds(WebSecurityForm, String, String, Collection, RandomKeyMapping, String...)} This method doesn't connect to database to verify
     * that the record is actually existed. You should throw out an Exception in Service Layer instead of validate them here.
     * Because the Controller layer should not be included in the transaction scope and the only place that you can 100% guarantee that the data existing is in
     * the transaction scope.
     * 
     * @param form
     *            The WebSecurityForm Object containing the data field.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabel
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @param keyMapping
     *            An Instance of {@link RandomKeyMapping} which will be use to convert the random key back to the actual database key
     * @param String
     *            to be exclude from validation (WebObjectId for any random Ids matched the skipIds will also be excluded from result).
     * @return The actual keyObject with Id and classType information. Return null if validataion fail.
     */
    protected WebObjectId validateRandomIdForWebObject(WebSecurityForm<W> form, String fieldName, String fieldLabel, String value,
        RandomKeyMapping keyMapping, String... skipIds) {
        WebObjectId webObject = null;
        
        String valBuff = value;
        if (valBuff != null) {
            valBuff = valBuff.trim();
            if (valBuff.length() <= 0) {
                valBuff = null;
            }
        }
        
        if (valBuff != null) {
            boolean shouldCheck = true;
            int i = skipIds.length;
            while (shouldCheck && (--i >= 0)) {
                shouldCheck = !value.equals(skipIds[i]);
            }
            
            if (shouldCheck) {
                webObject = keyMapping.getKeyObject(value);
                if (webObject == null) {
                    addErrorStatus(form, fieldName, "error.common.invalid", new Object[] { this.getMessage(fieldLabel) });
                }
            }
            
            /*
             * if ((valBuff.length() > WebCoreConstants.RANDOMKEY_MAX_HEX_LENGTH) || (valBuff.length() < WebCoreConstants.RANDOMKEY_MIN_HEX_LENGTH)
             * || (!valBuff.matches("[0-9A-Z]+"))) {
             * addErrorStatus(form, fieldName, "error.common.invalid", new Object[] { this.getMessage(fieldLabel) });
             * } else {
             * webObject = keyMapping.getKeyObject(valBuff);
             * if (webObject == null) {
             * addErrorStatus(form, fieldName, "error.common.invalid", new Object[] { this.getMessage(fieldLabel) });
             * } else {
             * if (webObject.getId() instanceof String) {
             * if (!valBuff.matches(WebCoreConstants.REGEX_INTEGER)) {
             * addErrorStatus(form, fieldName, "error.common.invalid", new Object[] { this.getMessage(fieldLabel) });
             * webObject = null;
             * } else {
             * webObject.setId(new Integer(valBuff));
             * }
             * } else if (!(webObject.getId() instanceof Integer)) {
             * addErrorStatus(form, fieldName, "error.common.invalid", new Object[] { this.getMessage(fieldLabel) });
             * webObject = null;
             * }
             * }
             * }
             */
        }
        
        return webObject;
    }
    
    /**
     * Validate whether the random keys (database primary keys) is in the correct format. This method is working the same way as
     * {@link #validateRandomIds(WebSecurityForm, String, String, Collection, RandomKeyMapping, String...)} This method doesn't connect to database to verify
     * that the record is actually existed. You should throw out an Exception in Service Layer instead of validate them here.
     * Because the Controller layer should not be included in the transaction scope and the only place that you can 100% guarantee that the data existing is in
     * the transaction scope.
     * 
     * @param form
     *            The WebSecurityForm Object containing the data field.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabel
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param valuesList
     *            List of the field values.
     * @param keyMapping
     *            An Instance of {@link RandomKeyMapping} which will be use to convert the random key back to the actual database key
     * @param String
     *            to be exclude from validation (WebObjectId for any random Ids matched the skipIds will also be excluded from result).
     * @return List of the actual keyObject with Id and classType information. Return null if validataion fail.
     */
    protected List<WebObjectId> validateRandomIdsForWebObjects(WebSecurityForm<W> form, String fieldName, String fieldLabel,
        Collection<String> valuesList, RandomKeyMapping keyMapping, String... skipIds) {
        List<WebObjectId> result = null;
        if (valuesList != null) {
            result = new ArrayList<WebObjectId>(valuesList.size());
            
            HashSet<String> ignoredSet;
            if (skipIds == null) {
                ignoredSet = new HashSet<String>(2, 0.9F);
            } else {
                ignoredSet = new HashSet<String>(skipIds.length * 2, 0.75F);
                int i = skipIds.length;
                while (--i >= 0) {
                    ignoredSet.add(skipIds[i]);
                }
            }
            
            boolean valid = true;
            Iterator<String> valueItr = valuesList.iterator();
            while (valid && (valueItr.hasNext())) {
                String valueStr = valueItr.next();
                if (!ignoredSet.contains(valueStr)) {
                    WebObjectId buffer = keyMapping.getKeyObject(valueStr.trim());
                    if (buffer != null) {
                        result.add(buffer);
                    } else {
                        valid = false;
                        addErrorStatus(form, fieldName, "error.common.invalid", new Object[] { this.getMessage(fieldLabel) });
                    }
                }
            }
            
            if (!valid) {
                result = null;
            }
        }
        
        return result;
    }
    
    /**
     * Validate whether the random key (database primary key) is in the correct format. This method doesn't connect to database to verify
     * that the record is actually existed. You should throw out an Exception in Service Layer instead of validate them here.
     * Because the Controller layer should not be included in the transaction scope and the only place that you can 100% guarantee that the data existing is in
     * the transaction scope.
     * 
     * @param form
     *            The WebSecurityForm Object containing the data field.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabel
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param className
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @param keyMapping
     *            An Instance of {@link RandomKeyMapping} which will be use to convert the random key back to the actual database key
     * @param objectType
     *            Class of the Object Type that the randomId was generated from
     * @param idType
     *            Class of the attribute that the randomId was generated from
     * @return The actual databaske key. Return null if validataion fail.
     */
    @SuppressWarnings("unchecked")
    protected <ID> ID validateRandomId(WebSecurityForm<W> form, String fieldName, String fieldLabel, String value, RandomKeyMapping keyMapping,
        Class<?> objectType, Class<ID> idType, String... skipIds) {
        ID result = null;
        if (value != null) {
            boolean shouldCheck = true;
            int i = skipIds.length;
            while (shouldCheck && (--i >= 0)) {
                shouldCheck = !value.equals(skipIds[i]);
            }
            
            if (shouldCheck) {
                WebObjectId buffer = keyMapping.getKeyObject(value);
                if ((buffer != null) && (objectType.equals(buffer.getObjectType())) && (idType.isAssignableFrom(buffer.getId().getClass()))) {
                    result = (ID) buffer.getId();
                } else {
                    addErrorStatus(form, fieldName, "error.common.invalid", new Object[] { this.getMessage(fieldLabel) });
                }
            }
        }
        
        return result;
    }
    
    /**
     * Validate whether the random key (database primary key) is in the correct format. This method doesn't connect to database to verify
     * that the record is actually existed. You should throw out an Exception in Service Layer instead of validate them here.
     * Because the Controller layer should not be included in the transaction scope and the only place that you can 100% guarantee that the data existing is in
     * the transaction scope.
     * 
     * @param form
     *            The WebSecurityForm Object containing the data field.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabel
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param valuesList
     *            The values of the field.
     * @param keyMapping
     *            An Instance of {@link RandomKeyMapping} which will be use to convert the random key back to the actual database key
     * @param objectType
     *            Class of the Object Type that the randomId was generated from
     * @param idType
     *            Class of the attribute that the randomId was generated from
     * @param skipIds
     *            random key which will not be included in the validation. For example, you might want to skip "0" if you defined "0" as all data in database.
     * @return The actual database key. Return null if validation fails.
     */
    @SuppressWarnings("unchecked")
    protected <ID> List<ID> validateRandomIds(WebSecurityForm<W> form, String fieldName, String fieldLabel, Collection<String> valuesList,
        RandomKeyMapping keyMapping, Class<?> objectType, Class<ID> idType, String... skipIds) {
        List<ID> result = null;
        if (valuesList != null) {
            result = new ArrayList<ID>(valuesList.size());
            
            HashSet<String> ignoredSet;
            if (skipIds == null) {
                ignoredSet = new HashSet<String>(2, 0.9F);
            } else {
                ignoredSet = new HashSet<String>(skipIds.length * 2, 0.75F);
                int i = skipIds.length;
                while (--i >= 0) {
                    ignoredSet.add(skipIds[i]);
                }
            }
            
            boolean valid = true;
            Iterator<String> valueItr = valuesList.iterator();
            while (valid && (valueItr.hasNext())) {
                String valueStr = valueItr.next();
                if (!ignoredSet.contains(valueStr)) {
                    WebObjectId buffer = keyMapping.getKeyObject(valueStr.trim());
                    if ((buffer != null) && (objectType.equals(buffer.getObjectType())) && (idType.isAssignableFrom(buffer.getId().getClass()))) {
                        result.add((ID) buffer.getId());
                    } else {
                        valid = false;
                        addErrorStatus(form, fieldName, "error.common.invalid", new Object[] { this.getMessage(fieldLabel) });
                    }
                }
            }
            
            if (!valid) {
                result = null;
            }
        }
        
        return result;
    }
    
    /**
     * This method is deprecated. Please use
     * {@link BaseValidator#validateRandomId(WebSecurityForm, String, String, String, RandomKeyMapping, Class, Class, String...)} instead.
     */
    @Deprecated
    protected Integer validateRandomId(WebSecurityForm<W> form, String fieldName, String fieldLabel, String className, String value,
        RandomKeyMapping keyMapping, String... skipIds) {
        Integer result = null;
        
        String valBuff = value;
        if (valBuff != null) {
            valBuff = valBuff.trim();
            if (valBuff.length() <= 0) {
                valBuff = null;
            }
        }
        
        boolean skip = false;
        for (int i = -1; (!skip) && (++i < skipIds.length);) {
            skip = skipIds[i].equals(valBuff);
        }
        
        if ((!skip) && (valBuff != null)) {
            WebObjectId buffer = validateRandomIdForWebObject(form, fieldName, fieldLabel, value, keyMapping);
            if ((buffer != null) && (buffer.getObjectType().getName().equals(className)) && (buffer.getId() instanceof Integer)) {
                result = (Integer) buffer.getId();
            }
            /*
             * if ((valBuff.length() > WebCoreConstants.RANDOMKEY_MAX_HEX_LENGTH) || (valBuff.length() < WebCoreConstants.RANDOMKEY_MIN_HEX_LENGTH)
             * || (!valBuff.matches("[0-9A-Z]+"))) {
             * addErrorStatus(form, fieldName, "error.common.invalid", new Object[] { this.getMessage(fieldLabel) });
             * } else {
             * WebObjectId webObject = keyMapping.getKeyObject(valBuff);
             * if (webObject == null) {
             * addErrorStatus(form, fieldName, "error.common.invalid", new Object[] { this.getMessage(fieldLabel) });
             * } else {
             * if (!className.equals(webObject.getObjectType())) {
             * addErrorStatus(form, fieldName, "error.common.invalid", new Object[] { this.getMessage(fieldLabel) });
             * }
             * if (webObject.getId() instanceof Integer) {
             * result = (Integer) webObject.getId();
             * } else if (webObject.getId() instanceof String) {
             * if (!valBuff.matches(WebCoreConstants.REGEX_INTEGER)) {
             * addErrorStatus(form, fieldName, "error.common.invalid", new Object[] { this.getMessage(fieldLabel) });
             * } else {
             * result = new Integer((String)webObject.getId());
             * }
             * }
             * }
             * }
             */
        }
        
        return result;
    }
    
    /**
     * This method is deprecated. Please use
     * {@link BaseValidator#validateRandomId(WebSecurityForm, String, String, String, RandomKeyMapping, Class, Class, String...)} instead.
     */
    @Deprecated
    protected Integer validateRandomId(WebSecurityForm<W> form, String fieldName, String fieldLabel, String value, RandomKeyMapping keyMapping,
        String... skipIds) {
        Integer result = null;
        
        String valBuff = value;
        if (valBuff != null) {
            valBuff = valBuff.trim();
            if (valBuff.length() <= 0) {
                valBuff = null;
            }
        }
        
        if (valBuff != null) {
            boolean skip = false;
            for (int i = -1; (!skip) && (++i < skipIds.length);) {
                skip = skipIds[i].equals(valBuff);
            }
            
            if (!skip) {
                ArrayList<String> valList = new ArrayList<String>();
                valList.add(valBuff);
                
                List<Integer> resultList = validateRandomIds(form, fieldName, fieldLabel, valList, keyMapping);
                if ((resultList != null) && (resultList.size() > 0)) {
                    result = resultList.get(0);
                }
            }
        }
        
        return result;
    }
    
    /**
     * This method is deprecated. Please use
     * {@link BaseValidator#validateRandomIds(WebSecurityForm, String, String, Collection, RandomKeyMapping, Class, Class, String...)} instead.
     */
    @Deprecated
    protected List<Integer> validateRandomIds(WebSecurityForm<W> form, String fieldName, String fieldLabel, Collection<String> valuesList,
        RandomKeyMapping keyMapping, String... skipIds) {
        List<Integer> result = null;
        if (valuesList != null) {
            result = new ArrayList<Integer>(valuesList.size());
            
            boolean valid = true;
            HashSet<String> ignoredSet = new HashSet<String>();
            if (skipIds != null) {
                for (int i = -1; ++i < skipIds.length;) {
                    ignoredSet.add(skipIds[i]);
                }
            }
            
            Iterator<String> valueItr = valuesList.iterator();
            while (valid && (valueItr.hasNext())) {
                String valueStr = valueItr.next();
                if (!ignoredSet.contains(valueStr)) {
                    WebObjectId buffer = keyMapping.getKeyObject(valueStr.trim());
                    if ((buffer != null) && (buffer.getId() instanceof Integer)) {
                        result.add((Integer) buffer.getId());
                    } else {
                        addErrorStatus(form, fieldName, "error.common.invalid", new Object[] { this.getMessage(fieldLabel) });
                    }
                    /*
                     * if (valueStr.length() <= 0) {
                     * valueStr = null;
                     * }
                     * if ((valueStr == null) || (valueStr.length() > WebCoreConstants.RANDOMKEY_MAX_HEX_LENGTH)
                     * || (valueStr.length() < WebCoreConstants.RANDOMKEY_MIN_HEX_LENGTH) || (!valueStr.matches("[0-9A-Z]+"))) {
                     * valid = false;
                     * addErrorStatus(form, fieldName, "error.common.invalid", new Object[] { this.getMessage(fieldLabel) });
                     * } else {
                     * Object buffer = keyMapping.getKey(valueStr);
                     * if (buffer == null) {
                     * valid = false;
                     * addErrorStatus(form, fieldName, "error.common.invalid", new Object[] { this.getMessage(fieldLabel) });
                     * } else {
                     * if (buffer instanceof Integer) {
                     * result.add((Integer) buffer);
                     * } else if (buffer instanceof String) {
                     * if (!valueStr.matches(WebCoreConstants.REGEX_INTEGER)) {
                     * valid = false;
                     * addErrorStatus(form, fieldName, "error.common.invalid", new Object[] { this.getMessage(fieldLabel) });
                     * } else {
                     * result.add(new Integer(valueStr));
                     * }
                     * }
                     * }
                     * }
                     */
                }
            }
            
            if (!valid) {
                result = null;
            }
        }
        
        return result;
    }
    
    /**
     * Check if the list is null or empty
     * 
     * @param list
     *            List to be verified
     * @return True of False
     */
    
    protected boolean isListNullorEmpty(Collection<?> list) {
        if (list == null || list.isEmpty() || (list.size() == 1 && list.contains(WebCoreConstants.EMPTY_STRING))) {
            return true;
            
        }
        return false;
    }
    
    /**
     * Get Integer value
     * 
     * @param value
     *            Value to be processed
     * @return value unless null in which case we return zero
     */
    
    protected Integer processIntVal(Integer value) {
        if (value == null) {
            return 0;
            
        }
        return value;
    }
    
    /**
     * Check if the list has any values
     * 
     * @param list
     *            List to be verified
     * @return True of False
     */
    
    protected boolean listHasValues(List<?> list) {
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                Object item = list.get(i);
                if (item == null) {
                    return false;
                }
                if (item instanceof String) {
                    if (StringUtils.isEmpty((String) item)) {
                        return false;
                    }
                }
            }
            
        } else {
            return false;
        }
        
        return true;
    }
    
    /**
     * This method validates randomized ID from request parameter and converts it to a database id
     * when there is no edit form to store results or errors like during a GET request.
     * 
     * @param request
     *            HttpServletRequest object
     * @return actual customer ID if process succeeds, "false" string if process
     *         fails.
     */
    public static Object validateRandomIdNoForm(HttpServletRequest request, String paramName) {
        Logger log = Logger.getLogger(BaseValidator.class);
        /* validate the randomized alert ID from request parameter. */
        String randomizedId = request.getParameter(paramName);
        if (StringUtils.isBlank(randomizedId) || !DashboardUtil.validateRandomId(randomizedId)) {
            log.warn("### Invalid " + paramName + " in the request. ###");
            return WebCoreConstants.RECORD_NOT_FOUND;
        }
        
        /* validate the actual customer ID. */
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        Object actualId = keyMapping.getKey(randomizedId);
        if (actualId == null || !DashboardUtil.validateDBIntegerPrimaryKey(actualId.toString())) {
            log.warn("### Invalid " + paramName + " in the request. ###");
            return WebCoreConstants.RECORD_NOT_FOUND;
        }
        
        return actualId;
    }
    
    public boolean handleMultipartExceptions(WebSecurityForm<W> secureForm, HttpServletRequest request) {
        FormErrorStatus error = null;
        
        SessionTool sessionTool = SessionTool.getInstance(request);
        Exception exception = (Exception) request.getAttribute(WebCoreConstants.REQKEY_UPLOAD_EXCEPTION);
        if (exception != null) {
            error = new FormErrorStatus();
            if (exception instanceof FileUploadBase.SizeLimitExceededException) {
                FileUploadBase.SizeLimitExceededException slee = (FileUploadBase.SizeLimitExceededException) exception;
                error.setMessage(this
                        .getMessage("error.common.import.exceeding.size.limit",
                                    new Object[] { convertToMegaBytes(slee.getPermittedSize()), convertToMegaBytes(slee.getActualSize()) }));
            } else {
                error.setMessage(this.getMessage("error.common.import"));
            }
            
            secureForm.addErrorStatus(error);
        }
        
        return (error != null);
    }
    
    protected double convertToMegaBytes(long bytes) {
        return Math.round((bytes * 100d / 1048576d)) / 100d;
    }
    
    public final boolean validateSchedule(final WebSecurityForm<W> webSecurityForm, final String scheduleDate, final String scheduleTime) {
        if (validateScheduleFormat(webSecurityForm, scheduleDate, scheduleTime)
            && validateScheduleInFuture(WebSecurityUtil.getCustomerTimeZone(), webSecurityForm, scheduleDate, scheduleTime)) {
            return true;
        }
        return false;
    }
    
    public final boolean validateScheduleFormat(final WebSecurityForm<W> webSecurityForm, final String scheduleDate, final String scheduleTime) {
        if (validateRequired(webSecurityForm, FormFieldConstants.SCHEDULE_DATE, MESSAGE_KEY_DATE, scheduleDate) == null
            || validateRequired(webSecurityForm, FormFieldConstants.SCHEDULE_TIME, MESSAGE_KEY_TIME, scheduleTime) == null) {
            return false;
        }
        if (super.validateDateTime(webSecurityForm, FormFieldConstants.SCHEDULE_DATE, MESSAGE_KEY_DATE, scheduleDate,
                                   FormFieldConstants.SCHEDULE_TIME, MESSAGE_KEY_TIME, scheduleTime,
                                   TimeZone.getTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT)) == null) {
            return false;
        }
        return true;
    }
    
    public final boolean validateScheduleInFuture(final String customerTimeZone, final WebSecurityForm<W> webSecurityForm, final String scheduleDate,
        final String scheduleTime) {
        
        try {
            final Date schDate = getScheduleDate(customerTimeZone, scheduleDate, scheduleTime);
            final GregorianCalendar scheduledCal = new GregorianCalendar();
            scheduledCal.setTimeInMillis(schDate.getTime());
            
            final Date now = DateUtil.getDatetimeInGMT(new Date(), TimeZone.getDefault().getDisplayName());
            final GregorianCalendar minsAgo = new GregorianCalendar();
            minsAgo.setTimeInMillis(now.getTime());
            minsAgo.add(Calendar.MINUTE,
                        this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.CONFIGURATION_GROUP_SCHEDULE_MAX_MINUTES_BEFORE,
                                                                        EmsPropertiesService.DEFAULT_CONFIGURATION_GROUP_SCHEDULE_MAX_MINUTES_BEFORE)
                                         * -1);
            
            if (scheduledCal.before(minsAgo)) {
                return false;
            }
        } catch (ParseException pe) {
            LOG.error(getInvalidFormatMessage(scheduleDate + WebCoreConstants.EMPTY_SPACE + scheduleTime), pe);
            return false;
        }
        return true;
    }
    
    public final Date getScheduleDate(final String customerTimeZone, final String scheduleDate, final String scheduleTime) throws ParseException {
        
        final SimpleDateFormat formatter =
                new SimpleDateFormat(WebCoreConstants.DATE_UI_FORMAT + WebCoreConstants.EMPTY_SPACE + WebCoreConstants.TIME_UI_FORMAT);
        formatter.setTimeZone(TimeZone.getTimeZone(customerTimeZone));
        final Date date = formatter.parse(scheduleDate + WebCoreConstants.EMPTY_SPACE + scheduleTime);
        return date;
    }
    
    private static String getInvalidFormatMessage(final String scheduleDateTime) {
        return "Schedule is not in the correct format: " + scheduleDateTime;
    }

    public void setEmsPropertiesService(EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
}