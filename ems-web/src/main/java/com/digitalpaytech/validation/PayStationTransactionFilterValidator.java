package com.digitalpaytech.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.mvc.support.PayStationTransactionFilterForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;

@Component("payStationTransactionFilterValidator")
public class PayStationTransactionFilterValidator extends BaseValidator<PayStationTransactionFilterForm> {
    @Override
    public void validate(WebSecurityForm<PayStationTransactionFilterForm> webSecurityForm, Errors errors) {
        webSecurityForm.removeAllErrorStatus();
        
        RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        checkId(webSecurityForm, keyMapping);
    }
    
    private void checkId(WebSecurityForm<PayStationTransactionFilterForm> webSecurityForm, RandomKeyMapping keyMapping) {
        
        PayStationTransactionFilterForm form = webSecurityForm.getWrappedObject();
        
        super.validateRequired(webSecurityForm, "randomId", "label.payStation", form.getRandomId());
        form.setPointOfSaleId(super.validateRandomId(webSecurityForm, "randomId", "label.payStation", form.getRandomId(), keyMapping, PointOfSale.class,
                                                     Integer.class));
    }
}
