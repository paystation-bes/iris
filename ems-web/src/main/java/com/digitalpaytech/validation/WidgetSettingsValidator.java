package com.digitalpaytech.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetFilterType;
import com.digitalpaytech.domain.WidgetMetricType;
import com.digitalpaytech.domain.WidgetRangeType;
import com.digitalpaytech.domain.WidgetTierType;
import com.digitalpaytech.dto.AlertWidgetType;
import com.digitalpaytech.dto.CardProcMethodType;
import com.digitalpaytech.dto.CitatType;
import com.digitalpaytech.dto.CollType;
import com.digitalpaytech.dto.Display;
import com.digitalpaytech.dto.DisplayType;
import com.digitalpaytech.dto.Filter;
import com.digitalpaytech.dto.LocationType;
import com.digitalpaytech.dto.MerchType;
import com.digitalpaytech.dto.Metric;
import com.digitalpaytech.dto.OrganizationType;
import com.digitalpaytech.dto.Range;
import com.digitalpaytech.dto.RangeType;
import com.digitalpaytech.dto.RateType;
import com.digitalpaytech.dto.RevType;
import com.digitalpaytech.dto.RouteType;
import com.digitalpaytech.dto.SettingsType;
import com.digitalpaytech.dto.Tier1;
import com.digitalpaytech.dto.Tier2;
import com.digitalpaytech.dto.Tier3;
import com.digitalpaytech.dto.TierType;
import com.digitalpaytech.dto.TxnType;
import com.digitalpaytech.dto.WidgetMetrics;
import com.digitalpaytech.dto.WidgetSettingsOptions;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetSettingsForm;
import com.digitalpaytech.util.DashboardUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WidgetConstants;

/**
 * WidgetSettingsValidator loads duration data once when server is starting and injects WidgetMetrics for widget setting validation.
 * 
 * @author Allen Liang
 */

@Component("widgetSettingsValidator")
public class WidgetSettingsValidator extends BaseValidator<WidgetSettingsForm> {
    
    private static Logger log = Logger.getLogger(WidgetSettingsValidator.class);
    private WidgetMetrics widgetMetrics;
    private Map<Integer, String> metricMap;
    private Map<Integer, String> durationMap;
    private Map<Integer, String> filterMap;
    private Map<Integer, String> tierMap;
    private Map<Integer, String> chartMap;
    
    @Override
    public void validate(final WebSecurityForm<WidgetSettingsForm> target, final Errors errors) {
    }
    
    // This is for the sake of unit-test
    @SuppressWarnings("checkstyle:designforextension")
    public void validate(final WebSecurityForm<WidgetSettingsForm> command, final Errors errors, final Widget widget, final boolean isApply) {
        final WebSecurityForm<WidgetSettingsForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
        
        final WidgetSettingsForm widgetSettingsForm = webSecurityForm.getWrappedObject();
        validateFields(webSecurityForm, errors, isApply);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().isEmpty()) {
            validateSettings(widgetSettingsForm, webSecurityForm, errors, widget, isApply);
        }
    }
    
    private void validateSettings(final WidgetSettingsForm widgetSettingsForm, final WebSecurityForm<WidgetSettingsForm> webSecurityForm
            , final Errors errors, final Widget widget, final boolean isApply) {
        if (log.isDebugEnabled()) {
            log.debug(widgetSettingsForm.toString());
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        
        final String rangeName = this.durationMap.get(getSettingTypeIdFromRandomId(widgetSettingsForm.getRange(), keyMapping));
        final String metricName = widgetSettingsForm.getMetricName();
        
        final Metric metric = this.widgetMetrics.getMetric(metricName);
        if (metricName == null) {
            if (log.isDebugEnabled()) {
                log.debug("widgetSettingsForm.getMetricName() is null ");
            }
            webSecurityForm.addErrorStatus(new FormErrorStatus("metricName", this
                    .getMessage("error.dashboard.widgetSettings.missing.tierValue",
                                new Object[] { this.getMessage("label.dashboard.widgetSettings.metricName") })));
            return;
        }
        
        //validates that the metric has not changed
        if (!widget.getWidgetMetricType().getName().equals(metric.getName())) {
            if (log.isDebugEnabled()) {
                log.debug("widgetSettingsForm.getMetricName(): " + metricName + ", does not match widget metric " + widget.getWidgetMetricType().getName());
            }
            webSecurityForm.addErrorStatus(new FormErrorStatus("metricName", this.getMessage("error.dashboard.widgetSettings.invalid.config",
                                                                                             new Object[] { metricName })));
            return;
        }
        if ("true".equalsIgnoreCase(widgetSettingsForm.getTargetValue())) {
            if (!metricName.equals(this.metricMap.get(WidgetConstants.METRIC_TYPE_REVENUE))) {
                if (log.isDebugEnabled()) {
                    log.debug("widgetSettingsForm.getTargetValue(): True cannot be selected for Widget metric type " + widget.getWidgetMetricType().getName());
                }
                webSecurityForm.addErrorStatus(new FormErrorStatus("targetValue", this.getMessage("error.dashboard.widgetSettings.invalid.config",
                                                                                                  new Object[] { widget.getWidgetMetricType().getName() })));
                return;
            }
        } else if (!"false".equalsIgnoreCase(widgetSettingsForm.getTargetValue())) {
            super.validateFloat(webSecurityForm, "targetValue", "label.dashboard.widgetSettings.target", widgetSettingsForm.getTargetValue(), 2);
            if (widgetSettingsForm.getTargetValue() != null &&  !WebCoreConstants.EMPTY_STRING.equals(widgetSettingsForm.getTargetValue())) {
                super.validateDoubleLimits(webSecurityForm, "targetValue", "label.dashboard.widgetSettings.target",
                                           Double.parseDouble(widgetSettingsForm.getTargetValue()), 0, WebCoreConstants.MAX_WIDGET_TARGETVALUE);
            }
        }
        
        /*
         * check chart type before returning.
         * Bypass chart type validation if this validation is not for applying changes, chartType validation
         * may fail on JSON update validation as a tier may change resulting in an invalid chart being selected
         */
        if (!isApply) {
            return;
        } else {
            if (widgetSettingsForm.getChartType() == null || WebCoreConstants.EMPTY_STRING.equals(widgetSettingsForm.getChartType())) {
                if (log.isDebugEnabled()) {
                    log.debug("widgetSettingsForm.getTargetValue(): True cannot be selected for Widget metric type " + widget.getWidgetMetricType().getName());
                }
                
                String errorMsg = this.getMessage("error.dashboard.widgetSettings.invalid.chartType",
            			new Object[] { widgetSettingsForm.getChartType() });
			    if (StringUtils.isEmpty(widgetSettingsForm.getChartType())) {
			    	errorMsg = this.getMessage("error.dashboard.widgetSettings.no.chartType");
			    }			                    
                webSecurityForm.addErrorStatus(new FormErrorStatus("chartType", errorMsg));
                
                return;
            }
        }
        final Range range = metric.getRange(rangeName);
        //TODO: remove inline conditional statement after todd returns filter type for "All" type widgets
        final int filterId = "".equals(widgetSettingsForm.getFilter()) ? 0 : getSettingTypeIdFromRandomId(widgetSettingsForm.getFilter(), keyMapping);
        final String filterName = this.filterMap.get(filterId);
        
        // Validates invalid range name, filter option based on selected metric & range.
        if (range == null || filterName == null || range.getFilters(filterName).isEmpty()) {
            if (log.isDebugEnabled()) {
                String output;
                if (range == null) {
                    output = "Range (duration)";
                } else {
                    output = range.toString();
                }
                log.debug("widgetSettingsForm.getFilter(): " + filterName + " or " + output + "is invalid.");
            }
            
            webSecurityForm.addErrorStatus(new FormErrorStatus("filter", this.getMessage("error.dashboard.widgetSettings.invalid.config",
                                                                                         new Object[] { StringUtils.isEmpty(filterName) ? this
                                                                                                 .getMessage("label.dashboard.widgetSettings.filter")
                                                                                                 : filterName, })));
            
            return;
        }
        
        final String tier1Name = this.tierMap.get(getSettingTypeIdFromRandomId(widgetSettingsForm.getTier1(), keyMapping));
        
        if (filterId == WidgetConstants.FILTER_TYPE_TOP || filterId == WidgetConstants.FILTER_TYPE_BOTTOM) {
            boolean invalidLength = true;
            final Integer length = widgetSettingsForm.getLength();
            if (widgetSettingsForm.getLength() != null) {
                for (Integer filterLength : WidgetConstants.FILTER_OPTIONS) {
                    if (widgetSettingsForm.getLength().equals(filterLength)) {
                        invalidLength = false;
                        break;
                    }
                }
            }
            if (invalidLength) {
                if (log.isDebugEnabled()) {
                    log.debug("widgetSettingsForm.getLength(): " + length + " is invalid, does not match " + Arrays.toString(WidgetConstants.FILTER_OPTIONS)
                              + ".");
                }
                webSecurityForm.addErrorStatus(new FormErrorStatus("length", this.getMessage("error.dashboard.widgetSettings.invalid.length",
                                                                                             new Object[] { filterName })));
            }
            if ((widgetSettingsForm.getTier1Attr() != null && widgetSettingsForm.getTier1Attr().length() > 0)
                || (widgetSettingsForm.getTier3Attr() != null && widgetSettingsForm.getTier3Attr().length() > 0)) {
                if (log.isDebugEnabled()) {
                    log.debug("widgetSettingsForm.TierAttr: is invalid, cannot make a sub selection with filter type: " + filterName + ".");
                }
                webSecurityForm.addErrorStatus(new FormErrorStatus("tier1Atrr", this.getMessage("error.dashboard.widgetSettings.invalid.tierAttr",
                                                                                                new Object[] { filterName })));
            }
        } else {
            if ((WebCoreConstants.N_A != widget.getWidgetTierTypeByWidgetTier1Type().getId())
                && (WidgetConstants.TIER_TYPE_HOUR != widget.getWidgetTierTypeByWidgetTier1Type().getId())
                && (WidgetConstants.TIER_TYPE_DAY != widget.getWidgetTierTypeByWidgetTier1Type().getId())
                && (WidgetConstants.TIER_TYPE_MONTH != widget.getWidgetTierTypeByWidgetTier1Type().getId())) {
                if (StringUtils.isBlank(widgetSettingsForm.getTier1Attr())) {
                    webSecurityForm.addErrorStatus(new FormErrorStatus("tier1", this.getMessage("error.dashboard.widgetSettings.missing.tierValue",
                                                                                                new Object[] { tier1Name })));
                }
            }
        }
        
        // Validates invalid tier 1 option based on selected metric & range.
        
        if (tier1Name == null || range.getTier1s(tier1Name).isEmpty()) {
            if (log.isDebugEnabled()) {
                log.debug(getInvalidMessage("1", tier1Name, showTier1s(range.getTier1s(tier1Name))));
            }
            webSecurityForm.addErrorStatus(new FormErrorStatus("tier1", this.getMessage("error.dashboard.widgetSettings.missing.tierValue",
                                                                                        new Object[] { tier1Name })));
            return;
        }
        
        //validates that the tier1 has not changed for all/subset widgets (can change for top/bottom widgets)
        if (!widget.getWidgetTierTypeByWidgetTier1Type().getName().equals(tier1Name)
            && (filterId == WidgetConstants.FILTER_TYPE_ALL || filterId == WidgetConstants.FILTER_TYPE_SUBSET)
            && getSettingTypeIdFromRandomId(widgetSettingsForm.getChartType(), keyMapping) != WidgetConstants.CHART_TYPE_MAP) {
            if (log.isDebugEnabled()) {
                log.debug("widgetSettingsForm.getTier1(): " + tier1Name + ", does not match widget tier1 "
                          + widget.getWidgetTierTypeByWidgetTier1Type().getName() + ".");
            }
            webSecurityForm.addErrorStatus(new FormErrorStatus("tier1", this.getMessage("error.dashboard.widgetSettings.invalid.config",
                                                                                        new Object[] { tier1Name })));
            return;
        }
        
        final List<Filter> filters = range.getFilters(filterName);
        final List<Tier1> tier1s = new ArrayList<Tier1>();
        for (Filter filter : filters) {
            tier1s.addAll(filter.getTier1s(tier1Name));
        }
        
        final String tier2Name = this.tierMap.get(getSettingTypeIdFromRandomId(widgetSettingsForm.getTier2(), keyMapping));
        final String tier3Name = this.tierMap.get(getSettingTypeIdFromRandomId(widgetSettingsForm.getTier3(), keyMapping));
        
        // Checks if rule allows empty tier 2.
        final boolean allowNoneTier2Flag = allowNoneTier2(tier1s);
        if ((tier2Name == null || tier2Name.equalsIgnoreCase(WebCoreConstants.NOT_APPLICABLE)) && allowNoneTier2Flag) {
            
            /*
             * check chart type before returning.
             * Bypass chart type validation if this validation is not for applying changes, chartType validation
             * may fail on JSON update validation as a tier may change resulting in an invalid chart being selected
             */
            if ((!isApply) || validateChartType(widgetSettingsForm, keyMapping)) {
                return;
            } else {
                final String chartName = this.chartMap.get(getSettingTypeIdFromRandomId(widgetSettingsForm.getChartType(), keyMapping));
                if (log.isDebugEnabled()) {
                    log.debug("widgetSettingsForm.getChartType(): " + chartName + " is invalid.");
                }
                String errorMsg = this.getMessage("error.dashboard.widgetSettings.invalid.chartType",
            			new Object[] { widgetSettingsForm.getChartType() });
			    if (StringUtils.isEmpty(widgetSettingsForm.getChartType())) {
			    	errorMsg = this.getMessage("error.dashboard.widgetSettings.no.chartType");
			    }			                    
                webSecurityForm.addErrorStatus(new FormErrorStatus("chartType", errorMsg));                
                return;
            }
        }
        
        Tier1 tier = null;
        boolean validTier2Flag = false;
        boolean validTier3Flag = false;
        
        if (isApply && !validateSubSelection(widgetSettingsForm, keyMapping)) {
            if (log.isDebugEnabled()) {
                log.debug("widgetSettingsForm: An invalid tier sub selection was made.");
            }
            webSecurityForm.addErrorStatus(new FormErrorStatus("tier1", this.getMessage("error.dashboard.widgetSettings.invalid.tierAttr",
                                                                                        new Object[] { widgetSettingsForm.getFilter() })));
            return;
        }
        
        final Iterator<Tier1> tier1Iter = tier1s.iterator();
        while (tier1Iter.hasNext()) {
            tier = tier1Iter.next();
            // Tier 1 is "none", exits out of the method.
            if (tier.getType() != null && tier.getType().trim().isEmpty()) {
                return;
            }
            
            // If there are tier 2 match with input tier 2 criteria, set 'validTier2' to true.
            if (tier.getType() != null && !tier.getTier2s(tier2Name).isEmpty()) {
                validTier2Flag = true;
                if ((!WebCoreConstants.N_A_STRING.equals(tier2Name)) && StringUtils.isEmpty(widgetSettingsForm.getTier2Attr())) {
                    webSecurityForm.addErrorStatus(new FormErrorStatus("tier2", this.getMessage("error.dashboard.widgetSettings.missing.tierValue",
                                                                                                new Object[] { tier2Name })));
                }
                
                // Validates tier 3 option based on selected metric, range, tier 1 & 2.
                if (validateTier3(widgetSettingsForm, tier.getTier2s(tier2Name), errors, keyMapping)) {
                    // All good, there is a valid tier 3 option in widget metric.
                    validTier3Flag = true;
                    if ((!WebCoreConstants.N_A_STRING.equals(tier3Name)) && StringUtils.isEmpty(widgetSettingsForm.getTier3Attr())) {
                        validTier3Flag = false;
                        webSecurityForm.addErrorStatus(new FormErrorStatus("tier3", this.getMessage("error.dashboard.widgetSettings.missing.tierValue",
                                                                                                    new Object[] { tier3Name })));
                    }
                    
                    //check chart type before returning
                    //Bypass chart type validation if this validation is not for applying changes,
                    //	chartType validation may fail on JSON update validation as a tier may change resulting in an invalid chart being selected 
                    if ((!isApply) || validateChartType(widgetSettingsForm, keyMapping)) {
                        break;
                    } else {
                        final String chartName = this.chartMap.get(getSettingTypeIdFromRandomId(widgetSettingsForm.getChartType(), keyMapping));
                        if (log.isDebugEnabled()) {
                            log.debug("widgetSettingsForm.getChartType(): " + chartName + " is invalid.");
                        }                        
                        String errorMsg = this.getMessage("error.dashboard.widgetSettings.invalid.chartType",
                                			new Object[] { chartName });
                        if (StringUtils.isEmpty(chartName)) {
                        	errorMsg = this.getMessage("error.dashboard.widgetSettings.no.chartType");
                        }
                        webSecurityForm.addErrorStatus(new FormErrorStatus("chartType", errorMsg));
                        return;
                    }
                }
            }
        }
        
        // There is no associated tier 2.
        if (!validTier2Flag) {
            if (log.isDebugEnabled()) {
                log.debug(getInvalidMessage("2", tier2Name, showTier1s(tier1s)));
            }
            webSecurityForm.addErrorStatus(new FormErrorStatus("tier2", this.getMessage("error.dashboard.widgetSettings.invalid.config",
                                                                                        new Object[] { StringUtils.isEmpty(tier2Name) ? this
                                                                                                .getMessage("label.dashboard.widgetSettings.TierName2")
                                                                                                : tier2Name, })));
        }
        // There is no associated tier 3, only adds error message key if tier 2 is fine.
        if (!validTier3Flag) {
            if (log.isDebugEnabled()) {
                log.debug(getInvalidMessage("3", tier3Name, null));
            }
            if (validTier2Flag) {
                webSecurityForm.addErrorStatus(new FormErrorStatus("tier3", this.getMessage("error.dashboard.widgetSettings.invalid.config",
                                                                                            new Object[] { StringUtils.isEmpty(tier3Name) ? this
                                                                                                    .getMessage("label.dashboard.widgetSettings.TierName3")
                                                                                                    : tier3Name, })));
            }
        }
        
    }
    
    private boolean validateSubSelection(final WidgetSettingsForm widgetSettingsForm, final RandomKeyMapping keyMapping) {
        
        final String tier1Name = this.tierMap.get(getSettingTypeIdFromRandomId(widgetSettingsForm.getTier1(), keyMapping));
        final String tier2Name = this.tierMap.get(getSettingTypeIdFromRandomId(widgetSettingsForm.getTier2(), keyMapping));
        final String tier3Name = this.tierMap.get(getSettingTypeIdFromRandomId(widgetSettingsForm.getTier3(), keyMapping));
        
        //TODO: remove inline conditional statement after todd returns filter type for "All" type widgets
        final int filterId = "".equals(widgetSettingsForm.getFilter()) ? 0 : getSettingTypeIdFromRandomId(widgetSettingsForm.getFilter(), keyMapping);
        
        if (filterId != WidgetConstants.FILTER_TYPE_TOP && filterId != WidgetConstants.FILTER_TYPE_BOTTOM && tier1Name != null
            && !tier1Name.equals(this.tierMap.get(WidgetConstants.TIER_TYPE_NA)) && !tier1Name.equals(this.tierMap.get(WidgetConstants.TIER_TYPE_DAY))
            && !tier1Name.equals(this.tierMap.get(WidgetConstants.TIER_TYPE_HOUR)) && !tier1Name.equals(this.tierMap.get(WidgetConstants.TIER_TYPE_MONTH))) {
            if (widgetSettingsForm.getTier1Attr().length() < WebCoreConstants.RANDOMKEY_MIN_HEX_LENGTH && widgetSettingsForm.getTier1Attr().length() > 0) {
                return false;
            }
        }
        //        int chartTypeId = getSettingTypeIdFromRandomId(widgetSettingsForm.getChartType(), keyMapping);
        //        if (widgetSettingsForm.getTier1Attr().length() == WebCoreConstants.RANDOMKEY_MIN_HEX_LENGTH
        //            && (chartTypeId == WidgetConstants.CHART_TYPE_AREA || chartTypeId == WidgetConstants.CHART_TYPE_LINE || chartTypeId == WidgetConstants.CHART_TYPE_PIE)) {
        //            return false;
        //        }
        if (tier2Name != null && !tier2Name.equals(this.tierMap.get(WidgetConstants.TIER_TYPE_NA))) {
            if (widgetSettingsForm.getTier2Attr().length() < WebCoreConstants.RANDOMKEY_MIN_HEX_LENGTH && widgetSettingsForm.getTier2Attr().length() > 0) {
                return false;
            }
        }
        if (tier3Name != null && !tier3Name.equals(this.tierMap.get(WidgetConstants.TIER_TYPE_NA))) {
            if (widgetSettingsForm.getTier3Attr().length() < WebCoreConstants.RANDOMKEY_MIN_HEX_LENGTH && widgetSettingsForm.getTier3Attr().length() > 0) {
                return false;
            }
        }
        return true;
    }
    
    private boolean validateChartType(final WidgetSettingsForm widgetSettingsForm, final RandomKeyMapping keyMapping) {
        
        final String metricName = widgetSettingsForm.getMetricName();
        final String rangeName = this.durationMap.get(getSettingTypeIdFromRandomId(widgetSettingsForm.getRange(), keyMapping));
        //TODO: remove inline conditional statement after todd returns filter type for "All" type widgets
        final String filterName = widgetSettingsForm.getFilter().equals("") ?
                "All" : filterMap.get(getSettingTypeIdFromRandomId(widgetSettingsForm.getFilter(), keyMapping));
        final String tier1Name = this.tierMap.get(getSettingTypeIdFromRandomId(widgetSettingsForm.getTier1(), keyMapping));
        String tier2Name = this.tierMap.get(getSettingTypeIdFromRandomId(widgetSettingsForm.getTier2(), keyMapping));
        if (tier2Name == null) {
            tier2Name = WebCoreConstants.NOT_APPLICABLE;
        }
        
        String tier3Name = this.tierMap.get(getSettingTypeIdFromRandomId(widgetSettingsForm.getTier3(), keyMapping));
        if (tier3Name == null) {
            tier3Name = WebCoreConstants.NOT_APPLICABLE;
        }
        
        final String chartName = this.chartMap.get(getSettingTypeIdFromRandomId(widgetSettingsForm.getChartType(), keyMapping));
        
        final List<Filter> filters = this.widgetMetrics.getMetric(metricName).getRange(rangeName).getFilters(filterName);
        
        for (Filter filter : filters) {
            List<Tier1> tier1s = new ArrayList<Tier1>();
            if (tier1Name == null) {
                tier1s = filter.getTier1s(WebCoreConstants.NOT_APPLICABLE);
            } else {
                tier1s = filter.getTier1s(tier1Name);
            }
            for (Tier1 tier1 : tier1s) {
                final List<Tier2> tier2List = tier1.getTier2s(tier2Name);
                if (tier2List == null || tier2List.isEmpty()) {
                    continue;
                }
                for (Tier2 tier2 : tier2List) {
                    final Tier3 tier3 = tier2.getTier3(tier3Name);
                    if (tier3 == null) {
                        continue;
                    }
                    if (validateChartTypeTier3(widgetSettingsForm, tier2, tier3Name, chartName)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    protected final boolean validateChartTypeTier2(final WidgetSettingsForm widgetSettingsForm, final Tier1 tier1, final String tier2Name
            , final String tier3Name, final String chartName) {
        List<Tier2> tier2s = new ArrayList<Tier2>();
        if (tier2Name == null) {
            tier2s = tier1.getTier2s(WebCoreConstants.NOT_APPLICABLE);
        } else {
            tier2s = tier1.getTier2s(tier2Name);
        }
        for (Tier2 tier2 : tier2s) {
            if (validateChartTypeTier3(widgetSettingsForm, tier2, tier3Name, chartName)) {
                return true;
            }
        }
        return false;
    }
    
    private boolean validateChartTypeTier3(final WidgetSettingsForm widgetSettingsForm, final Tier2 tier2, final String tier3Name
            , final String chartName) {
        List<Display> displays = new ArrayList<Display>();
        if (tier3Name == null) {
            displays = tier2.getTier3(WebCoreConstants.NOT_APPLICABLE).getDisplays();
        } else {
            if (tier2.getTier3(tier3Name) != null) {
                displays = tier2.getTier3(tier3Name).getDisplays();
            }
        }
        for (Display display : displays) {
            if (display.isCorrectType(chartName)) {
                return true;
            }
        }
        return false;
    }
    
    private boolean validateTier3(final WidgetSettingsForm widgetSettingsForm, final List<Tier2> tier2s, final Errors errors
            , final RandomKeyMapping keyMapping) {
        // Exits out the method if there is no Tier 3.
        final String tier3Name = this.tierMap.get(getSettingTypeIdFromRandomId(widgetSettingsForm.getTier3(), keyMapping));
        if (tier3Name == null || tier3Name.equals(WebCoreConstants.NOT_APPLICABLE)) {
            return true;
        }
        
        boolean tier3Allowed = false;
        Tier2 tier2;
        final Iterator<Tier2> tier2Iter = tier2s.iterator();
        while (tier2Iter.hasNext()) {
            tier2 = tier2Iter.next();
            if (tier2.getTier3(tier3Name) != null) {
                tier3Allowed = true;
                break;
            }
        }
        
        final String[] randomTier2AttrIds = widgetSettingsForm.getTier2Attr().split(",");
        if (randomTier2AttrIds.length > 1) {
            tier3Allowed = false;
        }
        
        return tier3Allowed;
    }
    
    private String getInvalidMessage(final String tierNumber, final String tierValue, final String options) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append("widgetSettingsForm.getTier").append(tierNumber).append("(): ").append(tierValue).append(", or no valid tier").append(tierNumber)
                .append(" option.");
        if (options != null) {
            bdr.append(" Accepted types are: ").append(options);
        }
        return bdr.toString();
    }
    
    private String showTier1s(final List<Tier1> tier1s) {
        final StringBuilder bdr = new StringBuilder();
        final Iterator<Tier1> iter = tier1s.iterator();
        while (iter.hasNext()) {
            bdr.append(iter.next().toString()).append(" ");
        }
        return bdr.toString();
    }
    
    /**
     * Checks Tier1 has list of Tier2 objects.
     * 
     * @param tier1s
     *            List<Tier1> Tier1 objects.
     * @return boolean Returns 'true' if there is NO tier2 list or tier2 list is empty or tier2 list only contains N/A.
     */
    private boolean allowNoneTier2(final List<Tier1> tier1s) {
        Tier1 tier1;
        final Iterator<Tier1> iter = tier1s.iterator();
        boolean onlyNA = true;
        while (iter.hasNext()) {
            tier1 = iter.next();
            if (tier1.getTier2s() == null || tier1.getTier2s().isEmpty()) {
                return true;
            }
            final List<Tier2> tier2s = tier1.getTier2s();
            for (Tier2 tier2 : tier2s) {
                if (!tier2.getType().equals(WebCoreConstants.NOT_APPLICABLE)) {
                    onlyNA = false;
                }
            }
        }
        if (onlyNA) {
            return true;
        }
        return false;
    }
    
    private void validateFields(final WebSecurityForm<WidgetSettingsForm> webSecurityForm, final Errors errors, final boolean isApply) {
        final WidgetSettingsForm form = webSecurityForm.getWrappedObject();
        final RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        if (isApply) {
            String widgetName = (String) super.validateRequired(webSecurityForm, "widgetName", "label.dashboard.widgetSettings.widgetName",
                                                                form.getWidgetName());
            widgetName = (String) super.validateStringLength(webSecurityForm, "widgetName", "label.dashboard.widgetSettings.widgetName", widgetName,
                                                             WebCoreConstants.VALIDATION_MIN_LENGTH_1, WebCoreConstants.VALIDATION_MAX_LENGTH_32);
            widgetName = super.validateStandardText(webSecurityForm, "widgetName", "label.dashboard.widgetSettings.widgetName", widgetName);
        }
        if (this.metricMap != null && !this.metricMap.containsValue(form.getMetricName())) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("metricName", this
                    .getMessage("error.dashboard.widgetSettings.invalid.metric.name",
                                new Object[] { this.getMessage("label.dashboard.widgetSettings.widgetType") })));
        }
        
        if (form.getTier1() == null || form.getTier2() == null || form.getTier3() == null) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("tier1", this.getMessage("error.dashboard.widgetSettings.invalid.tierType",
                                                                                        new Object[] { form.getWidgetName() })));
        }
        final String range = (String) super.validateRequired(webSecurityForm, "range", "label.dashboard.widgetSettings.range", form.getRange());
        if (range != null) {
            if (this.durationMap.get(keyMapping.getKey(form.getRange())).isEmpty()) {
                webSecurityForm.addErrorStatus(new FormErrorStatus("range", this.getMessage("error.dashboard.widgetSettings.invalid.range",
                                                                                            new Object[] { String.valueOf(form.getRange()) })));
            }
        }
        final List<String> randomTierAttrIds = new ArrayList<String>();
        
        if (form.getTier1Attr().length() > 0 && !"false".equals(form.getTier1Attr())) {
            randomTierAttrIds.addAll(Arrays.asList(form.getTier1Attr().split(",")));
        }
        if (form.getTier2Attr().length() > 0 && !"false".equals(form.getTier2Attr())) {
            randomTierAttrIds.addAll(Arrays.asList(form.getTier2Attr().split(",")));
        }
        if (form.getTier3Attr().length() > 0 && !"false".equals(form.getTier3Attr())) {
            randomTierAttrIds.addAll(Arrays.asList(form.getTier3Attr().split(",")));
        }
        
        for (String randomId : randomTierAttrIds) {
            if (!DashboardUtil.validateRandomId(randomId)) {
                webSecurityForm.addErrorStatus(new FormErrorStatus("range", this.getMessage("error.dashboard.widgetSettings.invalid.subSelection",
                                                                                            new Object[] { String.valueOf(form.getRange()) })));
            }
        }
        
    }
    
    // ------------------------------------------------------------------------------
    // Updates Widget Settings 
    // ------------------------------------------------------------------------------
    
    /**
     * Updates WidgetSettingsOptions based on the current selections found in WidgetSettingsForm.
     * This method is strictly called after WidgetSettingsValidation.validate() so we can assume that the WidgetSettingsForm is well formed.
     * 
     * @param command
     * @param settingsOptions
     * @param errors
     * @return
     */
    public WidgetSettingsOptions updateWidgetSettingsOptions(final WebSecurityForm<WidgetSettingsForm> command
            , final WidgetSettingsOptions settingsOptionsIn, final Errors errors, final RandomKeyMapping keyMapping) {
        final WebSecurityForm<WidgetSettingsForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return settingsOptionsIn;
        }
        
        WidgetSettingsOptions settingsOptions = settingsOptionsIn;
        final WidgetSettingsForm widgetSettingsForm = webSecurityForm.getWrappedObject();
        
        /*
         * Based on incoming widgetSettingsForm, and WidgetSettingsOptions in the session, updates WidgetSettingsOptions
         * by setting 'widgetStatus' to WebCoreConstants.AVAILABLE, WebCoreConstants.NOT_AVAILABLE or WebCoreConstants.IN_ACTIVE.
         */
        
        // retrieve data from the WidgetSettingsForm	
        final String newTargetValue = widgetSettingsForm.getTargetValue();
        final String newChartTypeRandomId = widgetSettingsForm.getChartType();
        final String newRangeTypeRandomId = widgetSettingsForm.getRange();
        final String newWidgetName = widgetSettingsForm.getWidgetName();
        final String newMetricName = widgetSettingsForm.getMetricName();
        final String newRangeName = this.durationMap.get(getSettingTypeIdFromRandomId(widgetSettingsForm.getRange(), keyMapping));
        
        //TODO: remove inline conditional statement after todd returns filter type for "All" type widgets
        final String newFilterName = widgetSettingsForm.getFilter() == "" ?
                "All" : this.filterMap.get(getSettingTypeIdFromRandomId(widgetSettingsForm.getFilter(), keyMapping));
        //String newFilterName = filterMap.get(getSettingTypeIdFromRandomId(widgetSettingsForm.getFilter(), keyMapping));	
        
        // Retrieves tier information from the form, the RandomId, actual Id, and Name
        final String newRandomTier1Id = widgetSettingsForm.getTier1();
        final String newRandomTier2Id = widgetSettingsForm.getTier2();
        String newRandomTier3Id = widgetSettingsForm.getTier3();
        final Integer newTier1Id = getSettingTypeIdFromRandomId(newRandomTier1Id, keyMapping);
        final Integer newTier2Id = getSettingTypeIdFromRandomId(newRandomTier2Id, keyMapping);
        Integer newTier3Id = getSettingTypeIdFromRandomId(newRandomTier3Id, keyMapping);
        
        // Fix 'Refine by Revenue Type/Transaction Type as 2nd and 3rd refinement can still be done if 3rd refinement is set first'.
        final List<TierType> tier3Types = settingsOptions.getTier3Types();
        if (newTier2Id != null && newTier3Id != null && isRevenueOrTransactionType(newTier2Id) && isRevenueOrTransactionType(newTier3Id)) {
            
            // Set TierType 3 to 'disabled' and tier 3 random id and id to 'N/A''s ids.
            for (TierType t3 : tier3Types) {
                t3.setStatus(WebCoreConstants.DISABLED);
                if (t3.getName().equals(WebCoreConstants.N_A_STRING)) {
                    newRandomTier3Id = t3.getRandomId();
                    newTier3Id = getSettingTypeIdFromRandomId(newRandomTier3Id, keyMapping);
                }
            }
        }
        
        final String newTier1Name = this.tierMap.get(newTier1Id);
        final String newTier2Name = this.tierMap.get(newTier2Id);
        final String newTier3Name = this.tierMap.get(newTier3Id);
        
        // retrieve data from the settingsOptions to update and return back to the UI
        final List<DisplayType> chartTypes = settingsOptions.getDisplayTypes();
        final List<RangeType> rangeTypes = settingsOptions.getRangeTypes();
     // will only change if filter type is not "All"
        final List<TierType> tier1Types = settingsOptions.getTier1Types();
        //List<MetricType> 	metricTypes = settingsOptions.getMetricTypes();	// will not change in the current implementation
        //List<FilterType> 	filterTypes = settingsOptions.getFilterTypes();	// will not change in the current implementation	
        
        //Sets up subsets within the tier selection
        settingsOptions = resetTierSubsetSelection(settingsOptions);
        if (widgetSettingsForm.getTier1Attr().length() > 0) {
            final String[] randomTier1AttrIds = widgetSettingsForm.getTier1Attr().split(",");
            settingsOptions = setTierSubsetSelection(settingsOptions, newTier1Id, randomTier1AttrIds);
        }
        if (widgetSettingsForm.getTier2Attr().length() > 0) {
            final String[] randomTier2AttrIds = widgetSettingsForm.getTier2Attr().split(",");
            settingsOptions = setTierSubsetSelection(settingsOptions, newTier2Id, randomTier2AttrIds);
        }
        if (widgetSettingsForm.getTier3Attr().length() > 0) {
            final String[] randomTier3AttrIds = widgetSettingsForm.getTier3Attr().split(",");
            settingsOptions = setTierSubsetSelection(settingsOptions, newTier3Id, randomTier3AttrIds);
        }
        
        //This widget is used to mock the an existing widget from WidgetSettingsForm for the purpose of setting inactive/disabled statuses
        final Widget tempWidget = tempWidget();
        tempWidget.getWidgetMetricType().setName(newMetricName);
        tempWidget.getWidgetFilterType().setName(newFilterName);
        tempWidget.getWidgetRangeType().setName(newRangeName);
        tempWidget.getWidgetTierTypeByWidgetTier1Type().setName(newTier1Name);
        tempWidget.getWidgetTierTypeByWidgetTier2Type().setName(newTier2Name);
        tempWidget.getWidgetTierTypeByWidgetTier3Type().setName(newTier3Name);
        
        if (newMetricName.equals(this.metricMap.get(WidgetConstants.METRIC_TYPE_MAP))
                || newFilterName.equals(this.filterMap.get(WidgetConstants.FILTER_TYPE_TOP))
                || newFilterName.equals(this.filterMap.get(WidgetConstants.FILTER_TYPE_BOTTOM))) {
            //reset selectedtier1 type before updating inactive/active based on filter values and selecting new tier1 type
            for (TierType tierType : tier1Types) {
                if (tierType.getStatus().equals(WebCoreConstants.SELECTED)) {
                    tierType.setStatus(WebCoreConstants.ACTIVE);
                }
            }
            settingsOptions.setTier1Type(newRandomTier1Id);
        }
        
        //reset chart type before updating inactive/active based on new tier2/3 values and selecting new chart type
        for (DisplayType chartType : chartTypes) {
            chartType.setStatus(WebCoreConstants.ACTIVE);
        }
        
        //reset range type
        for (RangeType rangeType : rangeTypes) {
            if (rangeType.getStatus().equals(WebCoreConstants.SELECTED)) {
                rangeType.setStatus(WebCoreConstants.ACTIVE);
                break;
            }
        }
        
        // Update available Options
        prepareOptions(tempWidget, settingsOptions);
        
        // for top/bottom N
        final int newFilterLength = (widgetSettingsForm.getLength() == null) ? 0 : widgetSettingsForm.getLength();
        if (newFilterLength > 0) {
            settingsOptions.setLength(Integer.toString(newFilterLength));
        }
        settingsOptions.setWidgetName(newWidgetName);
        settingsOptions.setTargetValue(newTargetValue);
        settingsOptions.setTier2Type(newRandomTier2Id);
        settingsOptions.setTier3Type(newRandomTier3Id);
        settingsOptions.setRangeType(newRangeTypeRandomId);
        
        if (validateChartType(widgetSettingsForm, keyMapping)) {
            settingsOptions.setChartType(newChartTypeRandomId);
        }
        
        //settingsOptions.setFilterType(filterTypeRandomId); 	Cannot change in current implementation
        //settingsOptions.setMetricType(metricTypeRandomId)		Cannot change in current implementation
        
        return settingsOptions;
    }
    
    private Widget tempWidget() {
        final Widget tempWidget = new Widget();
        
        tempWidget.setWidgetMetricType(new WidgetMetricType());
        tempWidget.setWidgetFilterType(new WidgetFilterType());
        tempWidget.setWidgetRangeType(new WidgetRangeType());
        tempWidget.setWidgetTierTypeByWidgetTier1Type(new WidgetTierType());
        tempWidget.setWidgetTierTypeByWidgetTier2Type(new WidgetTierType());
        tempWidget.setWidgetTierTypeByWidgetTier3Type(new WidgetTierType());
        
        return tempWidget;
    }
    
    private WidgetSettingsOptions resetTierSubsetSelection(final WidgetSettingsOptions settingsOptions) {
        
        final List<RateType> rateTypes = settingsOptions.getRateTypes();
        for (RateType rate : rateTypes) {
            rate.setStatus(WebCoreConstants.ACTIVE);
        }
        final List<RevType> revenueTypes = settingsOptions.getRevenueTypes();
        for (RevType rev : revenueTypes) {
            rev.setStatus(WebCoreConstants.ACTIVE);
        }
        final List<RouteType> routeTypes = settingsOptions.getRouteTypes();
        for (RouteType route : routeTypes) {
            route.setStatus(WebCoreConstants.ACTIVE);
        }
        final List<TxnType> transactionTypes = settingsOptions.getTransactionTypes();
        for (TxnType txn : transactionTypes) {
            txn.setStatus(WebCoreConstants.ACTIVE);
        }
        final List<LocationType> locationTypes = settingsOptions.getLocationTypes();
        for (LocationType location : locationTypes) {
            location.setStatus(WebCoreConstants.ACTIVE);
        }
        final List<LocationType> parentLocationTypes = settingsOptions.getParentLocationTypes();
        for (LocationType location : parentLocationTypes) {
            location.setStatus(WebCoreConstants.ACTIVE);
        }
        final List<CollType> collTypes = settingsOptions.getCollTypes();
        for (CollType collType : collTypes) {
            collType.setStatus(WebCoreConstants.ACTIVE);
        }
        final List<MerchType> merchTypes = settingsOptions.getMerchTypes();
        for (MerchType merchType : merchTypes) {
            merchType.setStatus(WebCoreConstants.ACTIVE);
        }
        final List<OrganizationType> orgTypes = settingsOptions.getOrganizationTypes();
        for (OrganizationType orgType : orgTypes) {
            orgType.setStatus(WebCoreConstants.ACTIVE);
        }
        final List<AlertWidgetType> alertTypes = settingsOptions.getAlertTypes();
        for (AlertWidgetType alert : alertTypes) {
            alert.setStatus(WebCoreConstants.ACTIVE);
        }
        
        return settingsOptions;
    }
    
    private WidgetSettingsOptions setTierSubsetSelection(final WidgetSettingsOptions settingsOptions, final int tierId, final String[] randomTierAttrIds) {
        
        if (tierId == WidgetConstants.TIER_TYPE_RATE) {
            final List<RateType> rateTypes = settingsOptions.getRateTypes();
            rateLoop: for (RateType rate : rateTypes) {
                for (String selectedRate : randomTierAttrIds) {
                    if (rate.getRandomId().equals(selectedRate)) {
                        rate.setStatus(WebCoreConstants.SELECTED);
                        continue rateLoop;
                    }
                }
            }
        } else if (tierId == WidgetConstants.TIER_TYPE_REVENUE_TYPE) {
            final List<RevType> revenueTypes = settingsOptions.getRevenueTypes();
            revLoop: for (RevType rev : revenueTypes) {
                for (String selectedRev : randomTierAttrIds) {
                    if (rev.getRandomId().equals(selectedRev)) {
                        rev.setStatus(WebCoreConstants.SELECTED);
                        continue revLoop;
                    }
                }
            }
        } else if (tierId == WidgetConstants.TIER_TYPE_ROUTE) {
            final List<RouteType> routeTypes = settingsOptions.getRouteTypes();
            routeLoop: for (RouteType route : routeTypes) {
                for (String selectedRoute : randomTierAttrIds) {
                    if (route.getRandomId().equals(selectedRoute)) {
                        route.setStatus(WebCoreConstants.SELECTED);
                        continue routeLoop;
                    }
                }
            }
        } else if (tierId == WidgetConstants.TIER_TYPE_TRANSACTION_TYPE) {
            final List<TxnType> transactionTypes = settingsOptions.getTransactionTypes();
            txnLoop: for (TxnType txn : transactionTypes) {
                for (String selectedTrans : randomTierAttrIds) {
                    if (txn.getRandomId().equals(selectedTrans)) {
                        txn.setStatus(WebCoreConstants.SELECTED);
                        continue txnLoop;
                    }
                }
            }
        } else if (tierId == WidgetConstants.TIER_TYPE_LOCATION) {
            final List<LocationType> locationTypes = settingsOptions.getLocationTypes();
            locationLoop: for (LocationType location : locationTypes) {
                for (String selectedLoc : randomTierAttrIds) {
                    if (location.getRandomId().equals(selectedLoc)) {
                        location.setStatus(WebCoreConstants.SELECTED);
                        continue locationLoop;
                    }
                }
            }
        } else if (tierId == WidgetConstants.TIER_TYPE_PARENT_LOCATION) {
            final List<LocationType> parentLocationTypes = settingsOptions.getParentLocationTypes();
            locationLoop: for (LocationType parentLocation : parentLocationTypes) {
                for (String selectedPLoc : randomTierAttrIds) {
                    if (parentLocation.getRandomId().equals(selectedPLoc)) {
                        parentLocation.setStatus(WebCoreConstants.SELECTED);
                        continue locationLoop;
                    }
                }
            }
        } else if (tierId == WidgetConstants.TIER_TYPE_ORG) {
            final List<OrganizationType> orgTypes = settingsOptions.getOrganizationTypes();
            organizationLoop: for (OrganizationType orgType : orgTypes) {
                for (String selectedOrg : randomTierAttrIds) {
                    if (orgType.getRandomId().equals(selectedOrg)) {
                        orgType.setStatus(WebCoreConstants.SELECTED);
                        continue organizationLoop;
                    }
                }
            }
        } else if (tierId == WidgetConstants.TIER_TYPE_COLLECTION_TYPE) {
            final List<CollType> collTypes = settingsOptions.getCollTypes();
            collectionLoop: for (CollType collType : collTypes) {
                for (String selectedColl : randomTierAttrIds) {
                    if (collType.getRandomId().equals(selectedColl)) {
                        collType.setStatus(WebCoreConstants.SELECTED);
                        continue collectionLoop;
                    }
                }
            }
        } else if (tierId == WidgetConstants.TIER_TYPE_MERCHANT_ACCOUNT) {
            final List<MerchType> merchTypes = settingsOptions.getMerchTypes();
            merchantLoop: for (MerchType merchType : merchTypes) {
                for (String selectedMerch : randomTierAttrIds) {
                    if (merchType.getRandomId().equals(selectedMerch)) {
                        merchType.setStatus(WebCoreConstants.SELECTED);
                        continue merchantLoop;
                    }
                }
            }
        } else if (tierId == WidgetConstants.TIER_TYPE_ALERT_TYPE) {
            final List<AlertWidgetType> alertTypes = settingsOptions.getAlertTypes();
            alertLoop: for (AlertWidgetType alert : alertTypes) {
                for (String selectedAlert : randomTierAttrIds) {
                    if (alert.getRandomId().equals(selectedAlert)) {
                        alert.setStatus(WebCoreConstants.SELECTED);
                        continue alertLoop;
                    }
                }
            }
        } else if (tierId == WidgetConstants.TIER_TYPE_CARD_PROCESS_METHOD_TYPE) {
            final List<CardProcMethodType> cardProcMethodTypes = settingsOptions.getCardProcessMethodTypes();
            cardProcLoop: for (CardProcMethodType cardProcMethodType : cardProcMethodTypes) {
                for (String selectedCardProc: randomTierAttrIds) {
                    if (cardProcMethodType.getRandomId().equals(selectedCardProc)) {
                        cardProcMethodType.setStatus(WebCoreConstants.SELECTED);
                        continue cardProcLoop;
                    }
                }
            }
        } else if (tierId == WidgetConstants.TIER_TYPE_CITATION_TYPE) {
            final List<CitatType> citatTypes = settingsOptions.getCitatTypes();
            citatLoop: for (CitatType citatType : citatTypes) {
                for (String selectedCitat: randomTierAttrIds) {
                    if (citatType.getRandomId().equals(selectedCitat)) {
                        citatType.setStatus(WebCoreConstants.SELECTED);
                        continue citatLoop;
                    }
                }
            }
        }
        
        return settingsOptions;
    }
    
    private Integer getSettingTypeIdFromRandomId(final String randomId, final RandomKeyMapping keyMapping) {
        if (keyMapping.getKey(randomId) == null) {
            return null;
        }
        return (Integer) keyMapping.getKey(randomId);
    }
    
    public final List<TierType> updateStatusToTierTypes(final String widgetStatus, final List<TierType> tierTypes) {
        final Iterator<TierType> iter = tierTypes.iterator();
        while (iter.hasNext()) {
            iter.next().setStatus(widgetStatus);
        }
        return tierTypes;
    }
    
    public final void setWidgetMetrics(final WidgetMetrics widgetMetrics) {
        this.widgetMetrics = widgetMetrics;
    }
    
    public final void setDurationMap(final Map<Integer, String> durationMap) {
        this.durationMap = durationMap;
    }
    
    public final Map<Integer, String> getDurationMap() {
        return this.durationMap;
    }
    
    public final Map<Integer, String> getMetricMap() {
        return this.metricMap;
    }
    
    public final void setMetricMap(final Map<Integer, String> metricMap) {
        this.metricMap = metricMap;
    }
    
    public final Map<Integer, String> getFilterMap() {
        return this.filterMap;
    }
    
    public final void setFilterMap(final Map<Integer, String> filterMap) {
        this.filterMap = filterMap;
    }
    
    public final Map<Integer, String> getTierMap() {
        return this.tierMap;
    }
    
    public final void setTierMap(final Map<Integer, String> tierMap) {
        this.tierMap = tierMap;
    }
    
    public final Map<Integer, String> getChartMap() {
        return this.chartMap;
    }
    
    public final void setChartMap(final Map<Integer, String> chartMap) {
        this.chartMap = chartMap;
    }
    
    /**
     * Uses the values found in the widget, widgetMetrics and the isSelected boolean to determine if
     * tier1Type should be Inactive, disabled, selected or default status (active).
     * 
     * @param tier1Type
     * @param widget
     * @param isSelected
     * @param parentOrganizationFlag
     * @return tier1Type with set statuses (DISABLED, IN_ACTIVE, SELECTED, ACTIVE)
     */
    public final TierType setStatusTier1Type(final TierType tier1Type, final Widget widget, final boolean isSelected, final boolean parentOrganizationFlag) {
        
        final Metric metric = this.widgetMetrics.getMetric(widget.getWidgetMetricType().getName());
        
        boolean isDisabled = true;
        //boolean isInActive = true; May be introduced later, tier1 will be inactive based on metric selection
        
        final Range range = metric.getRange(widget.getWidgetRangeType().getName());
        
        //allow active tier 1 selections for top/bottom and map widgets only
        if ((widget.getWidgetFilterType().getId() == WidgetConstants.FILTER_TYPE_ALL
                || widget.getWidgetFilterType().getId() == WidgetConstants.FILTER_TYPE_SUBSET)
                && widget.getWidgetChartType().getId() != WidgetConstants.CHART_TYPE_MAP) {
            if (tier1Type.getName().equals(widget.getWidgetTierTypeByWidgetTier1Type().getName())) {
                isDisabled = false;
            }
        } else {
            //Creates a list of valid tier1's based on the current filter selection
            List<Filter> filters = range.getFilters(widget.getWidgetFilterType().getName());
            if (widget.getWidgetFilterType().getId() == WidgetConstants.FILTER_TYPE_SUBSET) {
                filters = range.getFilters(this.filterMap.get(WidgetConstants.FILTER_TYPE_ALL));
            }
            final List<Tier1> tier1List = new ArrayList<Tier1>();
            for (Filter filter : filters) {
                tier1List.addAll(filter.getTier1s());
            }
            
            //if the tier1Type we are verifying is found in the list, we don't need to set it to disabled.
            for (Tier1 tier1 : tier1List) {
                if (tier1.getType().equals(tier1Type.getName())) {
                    isDisabled = false;
                    break;
                }
            }
        }
        
        // The default value for the Parent Map Widgets should be Organization with all organizations selected not N/A.
        if ("Map".equalsIgnoreCase(metric.getName()) && parentOrganizationFlag && "Organization".equalsIgnoreCase(tier1Type.getName())) {
            tier1Type.setStatus(WebCoreConstants.SELECTED);
            return tier1Type;
        }
        
        if (isDisabled) {
            tier1Type.setStatus(WebCoreConstants.DISABLED);
            return tier1Type;
        }
        if (isSelected) {
            tier1Type.setStatus(WebCoreConstants.SELECTED);
        }
        
        return tier1Type;
    }
    
    /**
     * Uses the values found in the widget, widgetMetrics and the isSelected boolean to determine if
     * tier2Type should be Inactive, disabled, selected or default status (active).
     * 
     * @param tier2Type
     * @param widget
     * @param isSelected
     * @return tier2Type with set statuses (DISABLED, IN_ACTIVE, SELECTED, ACTIVE)
     */
    public final TierType setStatusTier2Type(final TierType tier2Type, final WidgetTierType tier3, final Widget widget, final boolean isSelected) {
        
        final Metric metric = this.widgetMetrics.getMetric(widget.getWidgetMetricType().getName());
        tier2Type.setStatus(WebCoreConstants.ACTIVE);
        
        boolean isDisabled = true;
        boolean isInActive = true;
        
        final Range range = metric.getRange(widget.getWidgetRangeType().getName());
        
        String filterName = null;
        if (widget.getWidgetFilterType().getId() == WidgetConstants.FILTER_TYPE_SUBSET) {
            filterName = this.filterMap.get(WidgetConstants.FILTER_TYPE_ALL);
        } else {
            filterName = widget.getWidgetFilterType().getName();
        }
        final List<Filter> filters = range.getFilters(filterName);
        filterLoop: for (Filter filter : filters) {
            
            //creates list of valid tier1 based on the current filter
            final List<Tier1> tier1s = filter.getTier1s(widget.getWidgetTierTypeByWidgetTier1Type().getName());
            
            //loops through the tier1 list, if the tier2Type we are verifying is found in the list, we don't need to set it to disabled.
            for (Tier1 tier1 : tier1s) {
                if (tier1.getTier2s(tier2Type.getName()).size() > 0) {
                    isDisabled = false;
                    
                    //if tier2 contains the selected tier3, it will not be inactive
                    if (tier3 == null) {
                        isInActive = false;
                    } else {
                        for (Tier2 tier2 : tier1.getTier2s(tier2Type.getName())) {
                            if (tier2.getTier3(tier3.getName()) != null) {
                                isInActive = false;
                                break filterLoop;
                            }
                        }
                    }
                }
            }
        }
        
        if (isDisabled) {
            tier2Type.setStatus(WebCoreConstants.DISABLED);
            return tier2Type;
        }
        if (isInActive) {
            tier2Type.setStatus(WebCoreConstants.IN_ACTIVE);
            return tier2Type;
        }
        if (isSelected) {
            tier2Type.setStatus(WebCoreConstants.SELECTED);
        }
        
        return tier2Type;
    }
    
    /**
     * Uses the values found in the widget, widgetMetrics and the isSelected boolean to determine if
     * tier3Type should be Inactive, disabled, selected or default status (active).
     * 
     * @param tier3Type
     * @param widget
     * @param isSelected
     * @return tier3Type with set statuses (DISABLED, IN_ACTIVE, SELECTED, ACTIVE)
     */
    public final TierType setStatusTier3Type(final TierType tier3Type, final Widget widget, final boolean isSelected) {
        final Metric metric = this.widgetMetrics.getMetric(widget.getWidgetMetricType().getName());
        tier3Type.setStatus(WebCoreConstants.ACTIVE);
        
        boolean isDisabled = true;
        final boolean isInActive = true;
        
        // Tier 2 and Tier 3 cannot be the same if they are not N/A 
        if (!WebCoreConstants.NOT_APPLICABLE.equals(widget.getWidgetTierTypeByWidgetTier2Type().getName())
            && widget.getWidgetTierTypeByWidgetTier2Type().getName().equals(tier3Type.getName())) {
            tier3Type.setStatus(WebCoreConstants.DISABLED);
            return tier3Type;
        }
        
        final Range range = metric.getRange(widget.getWidgetRangeType().getName());
        
        String filterName = null;
        if (widget.getWidgetFilterType().getId() == WidgetConstants.FILTER_TYPE_SUBSET) {
            filterName = this.filterMap.get(WidgetConstants.FILTER_TYPE_ALL);
        } else {
            filterName = widget.getWidgetFilterType().getName();
        }
        final List<Filter> filters = range.getFilters(filterName);
        filterLoop: for (Filter filter : filters) {
            final boolean matchedFilter = matchCommaSeparatedString(filter.getType(), filterName);
            
            //loops through the list of valid tier1 based on the current filter
            for (Tier1 tier1 : filter.getTier1s(widget.getWidgetTierTypeByWidgetTier1Type().getName())) {
                if (tier1.getTier2s() != null) {
                    /*
                     * //loops through the list of valid tier2 based on the current tier1
                     * tier2Loop: for (Tier2 tier2 : tier1.getTier2s()) {
                     * 
                     * //if the tier3Type we are verifying is found in the list, we don't need to set it to disabled.
                     * if (matchedFilter && tier2.getTier3(tier3Type.getName()) != null) {
                     * isInActive = false;
                     * break tier2Loop;
                     * }
                     * }
                     */
                    //loops through the list of valid tier2 based on the current tier1 and current tier2 selection
                    for (Tier2 tier2 : tier1.getTier2s(widget.getWidgetTierTypeByWidgetTier2Type().getName())) {
                        
                        //if the tier3Type we are verifying is found in the list, we don't need to set it to inactive.		
                        if (matchedFilter && tier2.getTier3(tier3Type.getName()) != null) {
                            isDisabled = false;
                            break filterLoop;
                        }
                    }
                }
            }
        }
        if (isDisabled) {
            tier3Type.setStatus(WebCoreConstants.DISABLED);
            return tier3Type;
        }
        if (isInActive) {
            tier3Type.setStatus(WebCoreConstants.IN_ACTIVE);
            return tier3Type;
        }
        if (isSelected) {
            tier3Type.setStatus(WebCoreConstants.SELECTED);
            return tier3Type;
        }
        
        tier3Type.setStatus(WebCoreConstants.ACTIVE);
        return tier3Type;
    }
    
    /**
     * Uses the values found in the widget and widgetMetrics to determine if the range type should be Inactive, disabled, or default status (active).
     * 
     * @param rangeTypes
     * @param widget
     * @return rangeTypes with set statuses (DISABLED, IN_ACTIVE, SELECTED, ACTIVE)
     */
    public final List<RangeType> setStatusRangeType(List<RangeType> rangeTypes, Widget widget) {
        final Metric metric = this.widgetMetrics.getMetric(widget.getWidgetMetricType().getName());
        final List<Range> ranges = metric.getRanges();
        
        final String filterName = ((widget.getWidgetFilterType() == null) || (widget.getWidgetFilterType().getName() == null)) ? 
                WebCoreConstants.NOT_APPLICABLE : widget.getWidgetFilterType().getName().trim().toLowerCase();
        final boolean filterSubset = (widget.getWidgetFilterType() == null) ? 
                false : (widget.getWidgetFilterType().getId() == WidgetConstants.FILTER_TYPE_SUBSET);
        
        for (RangeType rangeType : rangeTypes) {
            
            boolean isDisabled = true;
            //boolean isInActive = true; May be introduced later, Range will be inactive based on tier1/metric selection
            
            //loop through valid ranges based on currently selected metric
            for (Range range : ranges) {
                if (range.getType().contains(rangeType.getName())) {
                    //if the range we are verifying is found in the list and contains the currently selected tier1, we don't need to set it to disabled.
                    for (Filter f : range.getFilters()) {
                        if ((matchCommaSeparatedString(f.getType(), filterName) || filterSubset)
                            && (range.getTier1s(widget.getWidgetTierTypeByWidgetTier1Type().getName()).size() > 0)) {
                            isDisabled = false;
                            break;
                        }
                    }
                }
            }
            if (isDisabled) {
                rangeType.setStatus(WebCoreConstants.DISABLED);
                continue;
            }
        }
        return rangeTypes;
    }
    
    private boolean matchCommaSeparatedString(final String css, final String target) {
        boolean matched = false;
        
        final StringTokenizer rangeTokenizer = new StringTokenizer(css, ",");
        while ((!matched) && rangeTokenizer.hasMoreTokens()) {
            matched = target.equalsIgnoreCase(rangeTokenizer.nextToken().trim());
        }
        
        return matched;
    }
    
    /**
     * Uses the values found in the widget and widgetMetrics to determine if the chart type should be Inactive, disabled, or default status (active).
     * 
     * @param rangeTypes
     * @param widget
     * @return rangeTypes with set statuses (DISABLED, IN_ACTIVE, SELECTED, ACTIVE)
     */
    public List<DisplayType> setStatusChartType(final List<DisplayType> chartTypes, final Widget widget, final WidgetSettingsOptions settingOptions) {
        final String rangeName = widget.getWidgetRangeType().getName().trim().toLowerCase();
        
        String filterName = ((widget.getWidgetFilterType() == null) || (widget.getWidgetFilterType().getName() == null)) ? WebCoreConstants.NOT_APPLICABLE
                : widget.getWidgetFilterType().getName();
        filterName = filterName.trim().toLowerCase();
        
        final boolean filterSubset = (widget.getWidgetFilterType() == null) ?
                false : (widget.getWidgetFilterType().getId() == WidgetConstants.FILTER_TYPE_SUBSET);
        
        final String tier1Name = widget.getWidgetTierTypeByWidgetTier1Type().getName().trim().toLowerCase();
        String tier2Name = ((widget.getWidgetTierTypeByWidgetTier2Type() == null) || (widget.getWidgetTierTypeByWidgetTier2Type().getName() == null)) ?
                WebCoreConstants.NOT_APPLICABLE : widget.getWidgetTierTypeByWidgetTier2Type().getName();
        if (tier2Name.length() <= 0) {
            tier2Name = WebCoreConstants.NOT_APPLICABLE;
        }
        
        tier2Name = tier2Name.trim().toLowerCase();
        
        String tier3Name = ((widget.getWidgetTierTypeByWidgetTier3Type() == null) || (widget.getWidgetTierTypeByWidgetTier3Type().getName() == null)) ?
                WebCoreConstants.NOT_APPLICABLE : widget.getWidgetTierTypeByWidgetTier3Type().getName();
        if (tier3Name.length() <= 0) {
            tier3Name = WebCoreConstants.NOT_APPLICABLE;
        }
        
        tier3Name = tier3Name.trim().toLowerCase();
        
        final Set<String> availableCharts = new HashSet<String>(chartTypes.size() * 2);
        //        HashSet<String> activeCharts = new HashSet<String>(chartTypes.size() * 2);
        
        final Metric metric = this.widgetMetrics.getMetric(widget.getWidgetMetricType().getName());
        final List<Range> ranges = metric.getRanges();
        for (Range range : ranges) {
            final boolean matchedRange = matchCommaSeparatedString(range.getType(), rangeName);
            for (Filter f : range.getFilters()) {
                final boolean matchedFilter = matchCommaSeparatedString(f.getType(), filterName) || filterSubset;
                for (Tier1 t1 : f.getTier1s()) {
                    final boolean matchedTier1 = tier1Name.equals(t1.getType().trim().toLowerCase());
                    for (Tier2 t2 : t1.getTier2s()) {
                        final boolean matchedTier2 = tier2Name.equals(t2.getType().trim().toLowerCase());
                        for (Tier3 t3 : t2.getTier3s()) {
                            final boolean matchedTier3 = tier3Name.equals(t3.getType().trim().toLowerCase());
                            for (Display d : t3.getDisplays()) {
                                final StringTokenizer chartTypeTokenizer = new StringTokenizer(d.getType(), ",");
                                while (chartTypeTokenizer.hasMoreTokens()) {
                                    final String chartName = chartTypeTokenizer.nextToken().trim().toLowerCase();
                                    
                                    if (matchedRange && matchedFilter && matchedTier1 && matchedTier2 && matchedTier3) {
                                        availableCharts.add(chartName);
                                        //                                        activeCharts.add(chartName);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        boolean allowAllChartTypes = false;
        if (tier1Name != null) {
            allowAllChartTypes = tier1Name.equals(this.tierMap.get(WidgetConstants.TIER_TYPE_HOUR))
                    || tier1Name.equals(this.tierMap.get(WidgetConstants.TIER_TYPE_DAY))
                    || tier1Name.equals(this.tierMap.get(WidgetConstants.TIER_TYPE_MONTH));
            if (!allowAllChartTypes) {
                allowAllChartTypes = true;
                
                if ((widget.getWidgetTierTypeByWidgetTier1Type() != null) && (widget.getWidgetTierTypeByWidgetTier1Type().getId() != null)) {
                    final int tier1Id = widget.getWidgetTierTypeByWidgetTier1Type().getId();
                    final List<? extends SettingsType> tier2Settings = settingOptions.getSettingsForTierType(tier1Id);
                    if (tier2Settings != null) {
                        int stCnt = 0;
                        for (SettingsType st : tier2Settings) {
                            if (WebCoreConstants.SELECTED.equals(st.getStatus())) {
                                ++stCnt;
                            }
                        }
                        
                        allowAllChartTypes = stCnt != 1;
                    }
                }
            }
        }
        
        for (DisplayType d : chartTypes) {
            final String chartName = d.getName().trim().toLowerCase();
            if ((!allowAllChartTypes)
                && ((WidgetConstants.CHART_TYPE_AREA == d.getActuaId()) || (WidgetConstants.CHART_TYPE_PIE == d.getActuaId())
                        || (WidgetConstants.CHART_TYPE_LINE == d.getActuaId()))) {
                d.setStatus(WebCoreConstants.DISABLED);
            } else if (!availableCharts.contains(chartName)) {
                d.setStatus(WebCoreConstants.IN_ACTIVE);
                //            } else if (!activeCharts.contains(chartName)) {
                //                d.setStatus(WebCoreConstants.IN_ACTIVE);
            } else {
                d.setStatus(WebCoreConstants.ACTIVE);
            }
        }
        
        return chartTypes;
    }
    
    /* This is incomplete. In future, this method should be use to translate XML into Object (for JSON) with accurate configurations */
    public final void prepareOptions(final Widget widget, final WidgetSettingsOptions settingsOptions) {
        final String selectedRangeName = (widget.getWidgetRangeType() == null) ? null : StringUtils.trimToNull(widget.getWidgetRangeType().getName());
        String selectedFilterName = null;
        if (widget.getWidgetFilterType() != null) {
            // Treat "Subset" filter like "All"
            if (WidgetConstants.FILTER_TYPE_SUBSET == widget.getWidgetFilterType().getId()) {
                selectedFilterName = this.filterMap.get(WidgetConstants.FILTER_TYPE_ALL);
            } else {
                selectedFilterName = StringUtils.trimToNull(widget.getWidgetFilterType().getName());
            }
        }
        
        final String selectedTier1Name = (widget.getWidgetTierTypeByWidgetTier1Type() == null) ? null : StringUtils.trimToNull(widget
                .getWidgetTierTypeByWidgetTier1Type().getName());
        final String selectedTier2Name = (widget.getWidgetTierTypeByWidgetTier2Type() == null) ? null : StringUtils.trimToNull(widget
                .getWidgetTierTypeByWidgetTier2Type().getName());
        final String selectedTier3Name = (widget.getWidgetTierTypeByWidgetTier3Type() == null) ? null : StringUtils.trimToNull(widget
                .getWidgetTierTypeByWidgetTier3Type().getName());
        final String selectedChartName = (widget.getWidgetChartType() == null) ? null : StringUtils.trimToNull(widget.getWidgetChartType().getName());
        
        final List<TierType> tier1Types = settingsOptions.getTier1Types();
        final Set<String> availableTier1s = new HashSet<String>(tier1Types.size() * 2);
        final Set<String> selectableTier1s = new HashSet<String>(tier1Types.size());
        
        final List<TierType> tier2Types = settingsOptions.getTier2Types();
        final Set<String> availableTier2s = new HashSet<String>(tier2Types.size() * 2);
        final Set<String> selectableTier2s = new HashSet<String>(tier2Types.size());
        
        final List<TierType> tier3Types = settingsOptions.getTier3Types();
        final Set<String> availableTier3s = new HashSet<String>(tier3Types.size() * 2);
        final Set<String> selectableTier3s = new HashSet<String>(tier3Types.size());
        
        final List<RangeType> rangeTypes = settingsOptions.getRangeTypes();
        final Set<String> availableRanges = new HashSet<String>(rangeTypes.size() * 2);
        final Set<String> selectableRanges = new HashSet<String>(rangeTypes.size());
        
        final List<DisplayType> chartTypes = settingsOptions.getDisplayTypes();
        //        HashSet<String> availableCharts = new HashSet<String>(chartTypes.size() * 2);
        final Set<String> selectableCharts = new HashSet<String>(chartTypes.size());
        
        final Metric metric = this.widgetMetrics.getMetric(widget.getWidgetMetricType().getName());
        for (Range range : metric.getRanges()) {
            boolean hasSelectedFilter = false;
            boolean hasSelectedTier1 = false;
            
            final boolean rangeSelected = matchCommaSeparatedString(range.getType(), selectedRangeName);
            for (Filter f : range.getFilters()) {
                final boolean filterSelected = (selectedFilterName == null) || (matchCommaSeparatedString(f.getType(), selectedFilterName));
                hasSelectedFilter |= filterSelected;
                if (((selectedFilterName != null) || (selectedRangeName == null)) && filterSelected) {
                    for (Tier1 t1 : f.getTier1s()) {
                        selectableTier1s.add(t1.getType());
                        
                        final boolean tier1Selected = (selectedTier1Name == null) || (t1.getType().equals(selectedTier1Name));
                        hasSelectedTier1 |= tier1Selected;
                        if (((selectedRangeName == null) || rangeSelected) && ((selectedTier1Name != null)
                                || (selectedFilterName == null)) && tier1Selected) {
                            for (Tier2 t2 : t1.getTier2s()) {
                                selectableTier2s.add(t2.getType());
                                
                                if (((selectedTier2Name != null) || (selectedTier1Name == null))
                                    && ((selectedTier2Name == null) || (t2.getType().equals(selectedTier2Name)))) {
                                    for (Tier3 t3 : t2.getTier3s()) {
                                        selectableTier3s.add(t3.getType());
                                        
                                        if (((selectedTier3Name != null) || (selectedTier2Name == null))
                                            && ((selectedTier3Name == null) || (t3.getType().equals(selectedTier3Name)))) {
                                            for (Display d : t3.getDisplays()) {
                                                final StringTokenizer chartTypeTokenizer = new StringTokenizer(d.getType(), ",");
                                                while (chartTypeTokenizer.hasMoreTokens()) {
                                                    final String chartName = chartTypeTokenizer.nextToken().trim();
                                                    selectableCharts.add(chartName);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            final StringTokenizer rangeTokenizer = new StringTokenizer(range.getType(), ",");
            while (rangeTokenizer.hasMoreTokens()) {
                final String rangeName = rangeTokenizer.nextToken().trim();
                if (hasSelectedFilter && hasSelectedTier1) {
                    selectableRanges.add(rangeName);
                } else {
                    availableRanges.add(rangeName);
                }
            }
        }
        
        boolean allowAllChartTypes = false;
        if (selectedTier1Name != null) {
            allowAllChartTypes = selectedTier1Name.equals(this.tierMap.get(WidgetConstants.TIER_TYPE_HOUR))
                                 || selectedTier1Name.equals(this.tierMap.get(WidgetConstants.TIER_TYPE_DAY))
                                 || selectedTier1Name.equals(this.tierMap.get(WidgetConstants.TIER_TYPE_MONTH));
            if (!allowAllChartTypes) {
                allowAllChartTypes = true;
                
                if ((widget.getWidgetTierTypeByWidgetTier1Type() != null) && (widget.getWidgetTierTypeByWidgetTier1Type().getId() != null)) {
                    final int tier1Id = widget.getWidgetTierTypeByWidgetTier1Type().getId();
                    final List<? extends SettingsType> tier2Settings = settingsOptions.getSettingsForTierType(tier1Id);
                    if (tier2Settings != null) {
                        int stCnt = 0;
                        for (SettingsType st : tier2Settings) {
                            if (WebCoreConstants.SELECTED.equals(st.getStatus())) {
                                ++stCnt;
                            }
                        }
                        
                        allowAllChartTypes = stCnt != 1;
                    }
                }
            }
        }
        
        for (RangeType r : rangeTypes) {
            if ((selectedRangeName != null) && selectedRangeName.equals(r.getName())) {
                r.setStatus(WebCoreConstants.SELECTED);
            } else if (selectableRanges.contains(r.getName())) {
                r.setStatus(WebCoreConstants.ACTIVE);
            } else if (availableRanges.contains(r.getName())) {
                r.setStatus(WebCoreConstants.DISABLED);
            } else {
                r.setStatus(WebCoreConstants.IN_ACTIVE);
            }
        }
        
        for (TierType t1 : tier1Types) {
            if ((selectedTier1Name != null) && selectedTier1Name.equals(t1.getName())) {
                t1.setStatus(WebCoreConstants.SELECTED);
            } else if (selectableTier1s.contains(t1.getName())) {
                t1.setStatus(WebCoreConstants.ACTIVE);
            } else if (availableTier1s.contains(t1.getName())) {
                t1.setStatus(WebCoreConstants.IN_ACTIVE);
            } else {
                t1.setStatus(WebCoreConstants.DISABLED);
            }
        }
        
        for (TierType t2 : tier2Types) {
            if ((selectedTier2Name != null) && selectedTier2Name.equals(t2.getName())) {
                t2.setStatus(WebCoreConstants.SELECTED);
            } else if (selectableTier2s.contains(t2.getName())) {
                t2.setStatus(WebCoreConstants.ACTIVE);
            } else if (availableTier2s.contains(t2.getName())) {
                t2.setStatus(WebCoreConstants.IN_ACTIVE);
            } else {
                t2.setStatus(WebCoreConstants.DISABLED);
            }
        }
        
        for (TierType t3 : tier3Types) {
            if ((selectedTier3Name != null) && selectedTier3Name.equals(t3.getName())) {
                t3.setStatus(WebCoreConstants.SELECTED);
            } else if (selectableTier3s.contains(t3.getName())) {
                t3.setStatus(WebCoreConstants.ACTIVE);
            } else if (availableTier3s.contains(t3.getName())) {
                t3.setStatus(WebCoreConstants.IN_ACTIVE);
            } else {
                t3.setStatus(WebCoreConstants.DISABLED);
            }
        }
        
        for (DisplayType d : chartTypes) {
            final String chartName = d.getName();
            if ((selectedChartName != null) && selectedChartName.equals(chartName)) {
                d.setStatus(WebCoreConstants.SELECTED);
            } else if ((!allowAllChartTypes)
                       && ((WidgetConstants.CHART_TYPE_AREA == d.getActuaId())
                               || (WidgetConstants.CHART_TYPE_PIE == d.getActuaId()) || (WidgetConstants.CHART_TYPE_LINE == d
                               .getActuaId()))) {
                d.setStatus(WebCoreConstants.DISABLED);
            } else if (!selectableCharts.contains(chartName)) {
                d.setStatus(WebCoreConstants.IN_ACTIVE);
                //            } else if (!activeCharts.contains(chartName)) {
                //                d.setStatus(WebCoreConstants.IN_ACTIVE);
            } else {
                d.setStatus(WebCoreConstants.ACTIVE);
            }
        }
    }
    
    private boolean isRevenueOrTransactionType(final Integer selectedTierId) {
        if (selectedTierId == WidgetConstants.TIER_TYPE_REVENUE_TYPE || selectedTierId == WidgetConstants.TIER_TYPE_TRANSACTION_TYPE) {
            return true;
        }
        return false;
    }
}
