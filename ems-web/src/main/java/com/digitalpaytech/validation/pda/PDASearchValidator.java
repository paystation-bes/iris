package com.digitalpaytech.validation.pda;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import java.net.URLEncoder;
import java.io.UnsupportedEncodingException;

import com.digitalpaytech.domain.Location;
import com.digitalpaytech.mvc.pda.support.PDASearchForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.validation.BaseValidator;

@Component("pdaSearchValidator")
public class PDASearchValidator extends BaseValidator<PDASearchForm> {
    
    private static Logger log = Logger.getLogger(PDASearchValidator.class);
    
    @Override
    public void validate(WebSecurityForm<PDASearchForm> target, Errors errors) {                
    }
    
    public void validate(WebSecurityForm<PDASearchForm> command, RandomKeyMapping keyMapping, boolean validatePostToken, boolean idsOnly) {
        WebSecurityForm<PDASearchForm> webSecurityForm = super.prepareSecurityForm(command, null, "postToken", validatePostToken);
        if(webSecurityForm == null){
            return;
        }                
        checkIds(webSecurityForm, keyMapping);
        if(!idsOnly){
            checkPSSettingName(webSecurityForm);
            checkSpaceNumbers(webSecurityForm);
            checkLicensePlate(webSecurityForm);
            checkSearchByAndType(webSecurityForm);
        }
    }
    
    private void checkIds(WebSecurityForm<PDASearchForm> webSecurityForm, RandomKeyMapping keyMapping) {
        PDASearchForm form = webSecurityForm.getWrappedObject();
        if(!StringUtils.isEmpty(form.getLocationRandomId())){
            Integer locationId = super.validateRandomId(webSecurityForm, "locationId", "label.location", form.getLocationRandomId(), keyMapping, Location.class, Integer.class);
            form.setLocationId(locationId);
        }                        
    }
    
    private void checkSpaceNumbers(WebSecurityForm<PDASearchForm> webSecurityForm){
        PDASearchForm form = webSecurityForm.getWrappedObject();
        String startSpace = form.getStartStall();
        String endSpace = form.getEndStall();
        if(searchTypeIsStall(form)){
            boolean rangeMode = (form.getSearchType() == WebCoreConstants.SEARCH_BY_VALID_STALL_RANGE || form.getSearchType() == WebCoreConstants.SEARCH_BY_EXPIRED_STALL_RANGE);
            String stallLabel;
            if(rangeMode) {
                stallLabel = "label.pda.search.form.stallNumber.start";
            }
            else {
                stallLabel = "label.pda.search.form.stallNumber";
            }
            
            
            Object requiredStart = super.validateRequired(webSecurityForm, "startStall", stallLabel, startSpace);
            if (requiredStart != null){
                int num = super.validatePositiveInteger(webSecurityForm, "startStall", stallLabel, startSpace);
                if (num < 0) {
                    try {
                        webSecurityForm.getWrappedObject().setStartStall(URLEncoder.encode(webSecurityForm.getWrappedObject().getStartStall(), WebSecurityConstants.URL_ENCODING_LATIN1));
                    } catch (UnsupportedEncodingException uee) {
                        log.error("Encoding error, remove the value from wrappedObject.", uee);
                        webSecurityForm.getWrappedObject().setStartStall("");
                    }
                }
                form.setStartStallAsNumber(num);                
            }
            
            if(rangeMode) {
                Object requiredEnd = super.validateRequired(webSecurityForm, "endStall", "label.pda.search.form.stallNumber.end", endSpace);
                if (requiredEnd != null){
                    int num = super.validatePositiveInteger(webSecurityForm, "endStall", "label.pda.search.form.stallNumber.end", endSpace);
                    if (num < 0) {
                        try {
                            webSecurityForm.getWrappedObject().setEndStall(URLEncoder.encode(webSecurityForm.getWrappedObject().getEndStall(), WebSecurityConstants.URL_ENCODING_LATIN1));
                        } catch (UnsupportedEncodingException uee) {
                            log.error("Encoding error, remove the value from wrappedObject.", uee);
                            webSecurityForm.getWrappedObject().setEndStall("");
                        }
                    }                    
                    form.setEndStallAsNumber(num);
                    super.validateLesserOrEqualsValue(webSecurityForm, "startStall", stallLabel, startSpace, "endStall", "label.pda.search.form.stallNumber.end", endSpace);
                }
            }
        }
    }
    
    private void checkLicensePlate(WebSecurityForm<PDASearchForm> webSecurityForm){
        PDASearchForm form = webSecurityForm.getWrappedObject();        
        if(form.getSearchType() == WebCoreConstants.SEARCH_BY_PLATE){
            String requiredLicNo = (String) super.validateRequired(webSecurityForm, "licensePlateNo", "label.pda.search.form.plateNumber", form.getLicensePlateNo());
            if (requiredLicNo != null){
                super.validateStringLength(webSecurityForm, "licensePlateNo", "label.pda.search.form.plateNumber", requiredLicNo, WebCoreConstants.VALIDATION_MIN_LENGTH_1,
                                                                 WebCoreConstants.VALIDATION_MAX_LENGTH_15);                
                String alphaNumResult = super.validateAlphaNumericWithSpace(webSecurityForm, "licensePlateNo", "label.pda.search.form.plateNumber", requiredLicNo);
                if (StringUtils.isBlank(alphaNumResult)) {
                    try {
                        webSecurityForm.getWrappedObject().setLicensePlateNo(URLEncoder.encode(webSecurityForm.getWrappedObject().getLicensePlateNo(), WebSecurityConstants.URL_ENCODING_LATIN1));
                    } catch (UnsupportedEncodingException uee) {
                        log.error("Encoding error, remove the value from wrappedObject.", uee);
                        webSecurityForm.getWrappedObject().setLicensePlateNo("");
                    }
                }
                try {
                    form.setLicensePlateNo(URLEncoder.encode(requiredLicNo, WebSecurityConstants.URL_ENCODING_LATIN1));
                } catch (UnsupportedEncodingException uee) {
                    log.error("Encoding error, remove the value from wrappedObject.", uee);
                    form.setLicensePlateNo("");
                }
            }
        }
        else if (form.getSearchType() == WebCoreConstants.SEARCH_BY_EXPIRED_PLATE){
            String requiredMinBack = (String) super.validateRequired(webSecurityForm, "expiredMinutesBack", "label.pda.search.form.expireWithIn", form.getExpiredMinutesBack());
            if (requiredMinBack != null){
                int num = super.validateIntegerLimits(webSecurityForm, "expiredMinutesBack", "label.pda.search.form.expireWithIn", requiredMinBack, 
                                                      WebCoreConstants.EXPIRED_PLATE_MINUTES_MIN, WebCoreConstants.EXPIRED_PLATE_MINUTES_MAX);
                if (num < 0) {
                    try {
                        webSecurityForm.getWrappedObject().setExpiredMinutesBack(URLEncoder.encode(webSecurityForm.getWrappedObject().getExpiredMinutesBack(), WebSecurityConstants.URL_ENCODING_LATIN1));
                    } catch (UnsupportedEncodingException uee) {
                        log.error("Encoding error, remove the value from wrappedObject.", uee);
                        webSecurityForm.getWrappedObject().setExpiredMinutesBack("");
                    }
                }                    
            }            
        }
    }    
    
    private void checkPSSettingName(WebSecurityForm<PDASearchForm> webSecurityForm) {
        PDASearchForm form = webSecurityForm.getWrappedObject();
        
        //if search type is stall and search by is setting then do a required check
        if(searchTypeIsStall(form) && WebCoreConstants.PAYSTATION.equals(form.getSearchBy())){         
            String requiredPSSettingName = (String) super.validateRequired(webSecurityForm, "paystationSettingName", "label.settings.locations.payStationSettings.payStationSettings", 
                                                                   form.getPaystationSettingName());
            if (requiredPSSettingName != null){
                super.validateStringLength(webSecurityForm, "paystationSettingName", "label.settings.locations.payStationSettings.payStationSettings", 
                                           requiredPSSettingName, WebCoreConstants.VALIDATION_MIN_LENGTH_1, WebCoreConstants.VALIDATION_MAX_LENGTH_20);
                super.validateExtraStandardText(webSecurityForm, "paystationSettingName", "label.settings.locations.payStationSettings.payStationSettings", 
                                                                  form.getPaystationSettingName());                
                form.setPaystationSettingName(requiredPSSettingName);
                
            }
        }
    }
    
    private void checkSearchByAndType(WebSecurityForm<PDASearchForm> webSecurityForm) {
        PDASearchForm form = webSecurityForm.getWrappedObject();
        boolean searchTypeIsStall = searchTypeIsStall(form);
        boolean searchTypeIsPlate = searchTypeIsPlate(form);

        if(!(searchTypeIsStall || searchTypeIsPlate)){
            webSecurityForm.addErrorStatus(new FormErrorStatus("searchType", this.getMessage("error.common.invalid", this.getMessage("label.pda.menu.main.header"))));
        }
        
        if(searchTypeIsStall){
           if(!(WebCoreConstants.LOCATION.equals(form.getSearchBy()) || WebCoreConstants.CUSTOMER.equals(form.getSearchBy()) ||
                   WebCoreConstants.PAYSTATION.equals(form.getSearchBy()))){
               webSecurityForm.addErrorStatus(new FormErrorStatus("searchBy", this.getMessage("error.common.invalid", this.getMessage("error.pda.menu.plate.search.by"))));
           }
           else if (searchTypeIsPlate){
               if(!(WebCoreConstants.LOCATION.equals(form.getSearchBy()) || WebCoreConstants.CUSTOMER.equals(form.getSearchBy()))){
                   webSecurityForm.addErrorStatus(new FormErrorStatus("searchBy", this.getMessage("error.common.invalid", this.getMessage("error.pda.menu.plate.search.by"))));
               }               
           }            
        }                
    }
    
    private boolean searchTypeIsStall(PDASearchForm form){
        boolean retVal = false;
        if(form.getSearchType() == WebCoreConstants.SEARCH_BY_VALID_STALL || form.getSearchType() == WebCoreConstants.SEARCH_BY_VALID_STALL_RANGE ||
                form.getSearchType() == WebCoreConstants.SEARCH_BY_EXPIRED_STALL || form.getSearchType() == WebCoreConstants.SEARCH_BY_EXPIRED_STALL_RANGE){
            retVal = true;
        }
        return retVal;
    }
    
    private boolean searchTypeIsPlate(PDASearchForm form){
        boolean retVal = false;
        if(form.getSearchType() == WebCoreConstants.SEARCH_BY_VALID_PLATE || form.getSearchType() == WebCoreConstants.SEARCH_BY_EXPIRED_PLATE ||
                form.getSearchType() == WebCoreConstants.SEARCH_BY_PLATE){
            retVal = true;
        }
        return retVal;
    }    
}
