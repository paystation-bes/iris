package com.digitalpaytech.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.mvc.support.PayStationCollectionFilterForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;

@Component("payStationCollectionFilterValidator")
public class PayStationCollectionFilterValidator extends BaseValidator<PayStationCollectionFilterForm> {
    @Override
    public void validate(WebSecurityForm<PayStationCollectionFilterForm> webSecurityForm, Errors errors) {
        webSecurityForm.removeAllErrorStatus();
        
        RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        checkId(webSecurityForm, keyMapping);
    }
    
    private void checkId(WebSecurityForm<PayStationCollectionFilterForm> webSecurityForm, RandomKeyMapping keyMapping) {
        
        PayStationCollectionFilterForm form = webSecurityForm.getWrappedObject();
        
        
        super.validateRequired(webSecurityForm, "randomId", "label.payStation",  form.getRandomId());
        form.setPointOfSaleId(super.validateRandomId(webSecurityForm, "randomId", "label.payStation", form.getRandomId(), keyMapping,
                                                PointOfSale.class, Integer.class));
    }
}
