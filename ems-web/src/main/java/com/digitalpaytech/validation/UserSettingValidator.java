package com.digitalpaytech.validation;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.List;

import org.apache.cxf.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.UserAccountEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.RoleService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Component("userSettingValidator")
public class UserSettingValidator extends BaseValidator<UserAccountEditForm> {
    
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String USER_NAME = "userName";
    private static final String USER_PWD = "userPassword";
    private static final String PWD_CONFIRM = "userConfirm";
    private static final String USER_ALL_ROLES = "userAllRoles";
    private static final String USER_ALL_ACCOUNTID = "userAccountID";
    private static final String CUSTOMER_ID = "customerId";
    
    @Autowired
    protected UserAccountService userAccountService;
    
    @Autowired
    protected RoleService roleService;
    
    // Note: To be used only for JUnit testing
    public final void setUserAccountService(final UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }
    
    // Note: To be used only for JUnit testing
    public final void setRoleService(final RoleService roleService) {
        this.roleService = roleService;
    }
    
    @Override
    public void validate(final WebSecurityForm<UserAccountEditForm> command, final Errors errors) {
        final WebSecurityForm<UserAccountEditForm> webSecurityForm = prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        
        if (webSecurityForm.getActionFlag() != WebSecurityConstants.ACTION_CREATE
            && webSecurityForm.getActionFlag() != WebSecurityConstants.ACTION_UPDATE) {
            final String label = this.getMessage("label.settings.actionFlg");
            webSecurityForm.addErrorStatus(new FormErrorStatus("actionFlag", this.getMessage(ErrorConstants.INVALID, new Object[] { label, })));
            return;
        }
        
        checkId(webSecurityForm, keyMapping);
        validateDisplayableName(webSecurityForm);
        validateUserName(webSecurityForm);
        validateEmailAddress(webSecurityForm);
        validateUserPassword(webSecurityForm);
        final FoundRoles foundRoles = validateRoles(webSecurityForm, keyMapping);
        final UserAccount userAccount = WebSecurityUtil.getUserAccount();
        if (foundRoles.isChildRoleFound() && userAccount.getCustomer().getCustomerType().getId() == WebCoreConstants.CUSTOMER_TYPE_PARENT) {
            validateChildCustomers(webSecurityForm, keyMapping);
            validateDefaultAliasCustomerId(webSecurityForm, foundRoles, keyMapping);
        }
    }
    
    private void checkId(final WebSecurityForm<UserAccountEditForm> webSecurityForm, final RandomKeyMapping keyMapping) {
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
            final UserAccountEditForm form = webSecurityForm.getWrappedObject();
            validateRequired(webSecurityForm, UserSettingValidator.USER_ALL_ACCOUNTID, "label.settings.users.id", form.getRandomizedUserAccountId());
            final Integer id = super.validateRandomId(webSecurityForm, UserSettingValidator.USER_ALL_ACCOUNTID, "label.settings.users.id",
                                                      form.getRandomizedUserAccountId(), keyMapping);
            form.setUserAccountId(id);
        }
    }
    
    private void validateDefaultAliasCustomerId(final WebSecurityForm<UserAccountEditForm> webSecurityForm, final FoundRoles foundRoles,
        final RandomKeyMapping keyMapping) {
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE
            || webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_CREATE) {
            final UserAccountEditForm form = webSecurityForm.getWrappedObject();
            final List<String> randomizedCustomerIds = transformCommaSeparatedList(form.getRandomizedChildCustomerIds());
            if (randomizedCustomerIds != null && !randomizedCustomerIds.isEmpty()) {
                String randomizedDefaultCustomerId = form.getDefaultAliasCustomerRandomId();
                if ((randomizedDefaultCustomerId == null || StandardConstants.STRING_MINUS_ONE.equals(randomizedDefaultCustomerId))
                    && !foundRoles.isParentRoleFound()) {
                    randomizedDefaultCustomerId = randomizedCustomerIds.get(0);
                }
                if (!StringUtils.isEmpty(randomizedDefaultCustomerId) && !StandardConstants.STRING_MINUS_ONE.equals(randomizedDefaultCustomerId)) {
                    
                    final Integer defaultCustomerId = super
                            .validateRandomId(webSecurityForm, UserSettingValidator.CUSTOMER_ID, "label.customerAdmin.customer.id",
                                              randomizedDefaultCustomerId, keyMapping, Customer.class, Integer.class, WebCoreConstants.EMPTY_STRING);
                    form.setDefaultAliasCustomerId(defaultCustomerId);
                }
            }
        }
    }
    
    /**
     * This method validates user input displayable name.
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     */
    private void validateDisplayableName(final WebSecurityForm<UserAccountEditForm> webSecurityForm) {
        
        final UserAccountEditForm userAccountEditForm = webSecurityForm.getWrappedObject();
        userAccountEditForm.setFirstName(userAccountEditForm.getFirstName() != null ? userAccountEditForm.getFirstName().trim() : null);
        userAccountEditForm.setLastName(userAccountEditForm.getLastName() != null ? userAccountEditForm.getLastName().trim() : null);
        
        validateRequired(webSecurityForm, UserSettingValidator.FIRST_NAME, "label.settings.users.form.firstname", userAccountEditForm.getFirstName());
        validateStringLength(webSecurityForm, UserSettingValidator.FIRST_NAME, "label.settings.users.form.firstname",
                             userAccountEditForm.getFirstName(), WebCoreConstants.VALIDATION_MIN_LENGTH_1, WebCoreConstants.VALIDATION_MAX_LENGTH_25);
        validateStandardText(webSecurityForm, UserSettingValidator.FIRST_NAME, "label.settings.users.form.firstname",
                             userAccountEditForm.getFirstName());
        
        validateRequired(webSecurityForm, UserSettingValidator.LAST_NAME, "label.settings.users.form.lastname", userAccountEditForm.getLastName());
        validateStringLength(webSecurityForm, UserSettingValidator.LAST_NAME, "label.settings.users.form.lastname",
                             userAccountEditForm.getLastName(), WebCoreConstants.VALIDATION_MIN_LENGTH_1, WebCoreConstants.VALIDATION_MAX_LENGTH_25);
        validateStandardText(webSecurityForm, UserSettingValidator.LAST_NAME, "label.settings.users.form.lastname", userAccountEditForm.getLastName());
    }
    
    /**
     * This method validates user name that user has input.
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     */
    private void validateUserName(final WebSecurityForm<UserAccountEditForm> webSecurityForm) {
        
        final UserAccountEditForm userAccountEditForm = webSecurityForm.getWrappedObject();
        userAccountEditForm.setUserName(userAccountEditForm.getUserName().trim());
        final String userName = userAccountEditForm.getUserName();
        
        validateRequired(webSecurityForm, UserSettingValidator.USER_NAME, "label.settings.users.form.username", userName);
        validateStringLength(webSecurityForm, UserSettingValidator.USER_NAME, "label.settings.users.form.username", userName,
                             WebSecurityConstants.USERNAME_MIN_LENGTH, WebSecurityConstants.USERNAME_MAX_LENGTH);
        validatePrintableText(webSecurityForm, UserSettingValidator.USER_NAME, "label.settings.users.form.username", userName);
        
        try {
            final String encodedUserName = URLEncoder.encode(userAccountEditForm.getUserName(), WebSecurityConstants.URL_ENCODING_LATIN1);
            if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_CREATE) {
                final UserAccount existingUser = this.userAccountService.findUndeletedUserAccount(encodedUserName);
                if (existingUser != null) {
                    final FormErrorStatus status = new FormErrorStatus(UserSettingValidator.USER_NAME,
                            this.getMessage("error.common.duplicated",
                                            new Object[] { this.getMessage("label.user"), this.getMessage("label.settings.users.form.username") }));
                    webSecurityForm.getValidationErrorInfo().addErrorStatus(status);
                }
            } else {
                final UserAccount existingUser = this.userAccountService.findUserAccount(userAccountEditForm.getUserAccountId());
                if (!existingUser.getUserName().equals(encodedUserName)) {
                    final UserAccount existingUserAccount = this.userAccountService.findUndeletedUserAccount(encodedUserName);
                    if (existingUserAccount != null) {
                        final FormErrorStatus status = new FormErrorStatus(
                                UserSettingValidator.USER_NAME,
                                this.getMessage("error.common.duplicated",
                                                new Object[] { this.getMessage("label.user"), this.getMessage("label.settings.users.form.username") }));
                        webSecurityForm.getValidationErrorInfo().addErrorStatus(status);
                    }
                }
            }
        } catch (UnsupportedEncodingException e) {
            final FormErrorStatus status = new FormErrorStatus(UserSettingValidator.USER_NAME,
                    this.getMessage(ErrorConstants.INVALID, new Object[] { this.getMessage("label.settings.users.form.username") }));
            webSecurityForm.getValidationErrorInfo().addErrorStatus(status);
        }
    }
    
    private void validateEmailAddress(final WebSecurityForm<UserAccountEditForm> webSecurityForm) {
        final UserAccountEditForm userAccountEditForm = webSecurityForm.getWrappedObject();
        final String email = userAccountEditForm.getEmailAddress();
        validateEmailAddress(webSecurityForm, "email", "label.settings.users.email", email);
    }
    
    /**
     * This method validates user input password.
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     */
    private void validateUserPassword(final WebSecurityForm<UserAccountEditForm> webSecurityForm) {
        
        final UserAccountEditForm userAccountEditForm = webSecurityForm.getWrappedObject();
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_CREATE) {
            validateRequired(webSecurityForm, UserSettingValidator.USER_PWD, "label.changePassword.newPassword",
                             userAccountEditForm.getUserPassword());
            validateRequired(webSecurityForm, UserSettingValidator.PWD_CONFIRM, "label.changePassword.confirmPassword",
                             userAccountEditForm.getPasswordConfirm());
            
        }
        
        validateStringLength(webSecurityForm, UserSettingValidator.USER_PWD, "label.changePassword.newPassword",
                             userAccountEditForm.getUserPassword(), WebSecurityConstants.USER_PASSWORD_MIN_LENGTH,
                             WebSecurityConstants.USER_PASSWORD_MAX_LENGTH);
        validateStringLength(webSecurityForm, UserSettingValidator.PWD_CONFIRM, "label.changePassword.confirmPassword",
                             userAccountEditForm.getPasswordConfirm(), WebSecurityConstants.USER_PASSWORD_MIN_LENGTH,
                             WebSecurityConstants.USER_PASSWORD_MAX_LENGTH);
        
        if ((userAccountEditForm.getUserPassword() != null) && (userAccountEditForm.getPasswordConfirm() != null)
            && (!userAccountEditForm.getUserPassword().equals(userAccountEditForm.getPasswordConfirm()))) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("userPassword,passwordConfirm", this.getMessage("error.user.password.not.matched")));
        }
        
        validateRegEx(webSecurityForm, UserSettingValidator.USER_PWD, "label.changePassword.newPassword", userAccountEditForm.getUserPassword(),
                      WebSecurityConstants.COMPLEX_PASSWORD_EXP, "error.user.password.not.complex", (String) null);
    }
    
    /**
     * This method validates user input list of role.
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     * @param keyMapping
     *            RandomKeyMapping object
     */
    private FoundRoles validateRoles(final WebSecurityForm<UserAccountEditForm> webSecurityForm, final RandomKeyMapping keyMapping) {
        
        final FoundRoles foundRoles = new FoundRoles();
        
        final UserAccountEditForm userAccountEditForm = webSecurityForm.getWrappedObject();
        
        final List<String> randomizedroleIds = transformCommaSeparatedList(userAccountEditForm.getRandomizedRoleIds());
        if (validateRequired(webSecurityForm, "RoleList", "label.role", randomizedroleIds) != null) {
            userAccountEditForm.setRoleIds(validateRandomIds(webSecurityForm, "RoleList", "label.role", randomizedroleIds, keyMapping));
        }
        
        final Collection<Integer> roleIds = userAccountEditForm.getRoleIds();
        if (roleIds != null && !roleIds.isEmpty()) {
            for (Integer id : roleIds) {
                final Role role = this.roleService.findRoleByRoleIdAndEvict(id);
                if (role.getCustomerType().getId() == WebCoreConstants.CUSTOMER_TYPE_PARENT) {
                    foundRoles.setParentRoleFound(true);
                }
                if (role.getCustomerType().getId() == WebCoreConstants.CUSTOMER_TYPE_CHILD) {
                    foundRoles.setChildRoleFound(true);
                }
            }
        }
        
        if (userAccountEditForm.getChildCustomerIds() != null && !userAccountEditForm.getChildCustomerIds().isEmpty()
            && !foundRoles.isChildRoleFound()) {
            webSecurityForm.addErrorStatus(new FormErrorStatus(UserSettingValidator.USER_ALL_ROLES, this.getMessage("error.user.role.need.child")));
        }
        return foundRoles;
    }
    
    /**
     * This method validates user input list of customer.
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     * @param keyMapping
     *            RandomKeyMapping object
     */
    private void validateChildCustomers(final WebSecurityForm<UserAccountEditForm> webSecurityForm, final RandomKeyMapping keyMapping) {
        
        final UserAccountEditForm userAccountEditForm = webSecurityForm.getWrappedObject();
        
        final List<String> randomizedCustomerIds = transformCommaSeparatedList(userAccountEditForm.getRandomizedChildCustomerIds());
        if (randomizedCustomerIds == null || randomizedCustomerIds.isEmpty()) {
            webSecurityForm.addErrorStatus(new FormErrorStatus(UserSettingValidator.USER_ALL_ROLES, this
                    .getMessage("error.user.child.role.needs.child")));
        } else {
            userAccountEditForm.setChildCustomerIds(validateRandomIds(webSecurityForm, "ChildCustomerList", "label.customer", randomizedCustomerIds,
                                                                      keyMapping, Customer.class, Integer.class, "0"));
        }
    }
    
    public class FoundRoles {
        private boolean childRoleFound;
        private boolean parentRoleFound;
        
        public FoundRoles() {
            this.childRoleFound = false;
            this.parentRoleFound = false;
        }
        
        public final boolean isChildRoleFound() {
            return this.childRoleFound;
        }
        
        public final void setChildRoleFound(final boolean childRoleFound) {
            this.childRoleFound = childRoleFound;
        }
        
        public final boolean isParentRoleFound() {
            return this.parentRoleFound;
        }
        
        public final void setParentRoleFound(final boolean parentRoleFound) {
            this.parentRoleFound = parentRoleFound;
        }
        
    }
    
}
