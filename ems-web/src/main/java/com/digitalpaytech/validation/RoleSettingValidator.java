package com.digitalpaytech.validation;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.RoleEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

@Component("roleSettingValidator")
public class RoleSettingValidator extends BaseValidator<RoleEditForm> {
    
    private static final String ROLE_ID = "roleID";
    
    @SuppressWarnings({ "checkstyle:designforextension", "checkstyle:multiplestringliterals" })
    @Override
    public void validate(final WebSecurityForm<RoleEditForm> command, final Errors errors) {
        final WebSecurityForm<RoleEditForm> webSecurityForm = prepareSecurityForm(command, errors, WebCoreConstants.POSTTOKEN_STRING);
        if (webSecurityForm == null) {
            return;
        }
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        
        if (webSecurityForm.getActionFlag() != WebSecurityConstants.ACTION_CREATE && webSecurityForm.getActionFlag() != WebSecurityConstants.ACTION_UPDATE) {
            final String label = this.getMessage("label.settings.actionFlg");
            webSecurityForm.addErrorStatus(new FormErrorStatus("actionFlag", this.getMessage("error.common.invalid", new Object[] { label })));
            return;
        }
        
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
            final RoleEditForm roleEditForm = webSecurityForm.getWrappedObject();
            final String randomizedRoleId = roleEditForm.getRandomizedRoleId();
            validateRequired(webSecurityForm, ROLE_ID, "label.settings.roles.id", randomizedRoleId);
            roleEditForm.setRoleId(validateRandomId(webSecurityForm, ROLE_ID, "label.settings.roles.id", randomizedRoleId, keyMapping));
        }
        
        validateRoleType(webSecurityForm);
        validateRoleName(webSecurityForm);
        validatePermissionList(webSecurityForm, keyMapping);
    }
    
    private void validateRoleType(final WebSecurityForm<RoleEditForm> webSecurityForm) {
        final RoleEditForm roleEditForm = webSecurityForm.getWrappedObject();
        
        final Integer roleType = roleEditForm.getTypeId();
        if (roleType != null && roleType.intValue() != WebCoreConstants.CUSTOMER_TYPE_DPT && roleType.intValue() != WebCoreConstants.CUSTOMER_TYPE_PARENT
            && roleType.intValue() != WebCoreConstants.CUSTOMER_TYPE_CHILD) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("typeId", this.getMessage("error.common.invalid",
                                                                                         new Object[] { this.getMessage("label.roleType") })));
            return;
        }
    }
    
    /**
     * It validates user input role name value.
     * 
     * @param webSecurityForm
     *            user input form
     */
    @SuppressWarnings({ "checkstyle:multiplestringliterals" })
    private void validateRoleName(final WebSecurityForm<RoleEditForm> webSecurityForm) {
        final RoleEditForm roleEditForm = webSecurityForm.getWrappedObject();
        
        final String roleName = roleEditForm.getRoleName();
        validateRequired(webSecurityForm, "roleName", "label.settings.roles.RoleName", roleName);
        validateStringLength(webSecurityForm, "roleName", "label.settings.roles.RoleName", roleName, WebCoreConstants.VALIDATION_MIN_LENGTH_1,
                             WebCoreConstants.VALIDATION_MAX_LENGTH_30);
        validateStandardText(webSecurityForm, "roleName", "label.settings.roles.RoleName", roleName);
    }
    
    @SuppressWarnings({ "checkstyle:multiplestringliterals" })
    private void validatePermissionList(final WebSecurityForm<RoleEditForm> webSecurityForm, final RandomKeyMapping keyMapping) {
        
        final RoleEditForm roleEditForm = webSecurityForm.getWrappedObject();
        final List<String> randomizedPermissionIds = transformCommaSeparatedList(roleEditForm.getRandomizedPermissionIds());
        if (validateRequired(webSecurityForm, "permissionList", "label.settings.roles.permission.selection", randomizedPermissionIds) != null) {
            roleEditForm.setPermissionIds(validateRandomIds(webSecurityForm, "permissionList", "label.settings.roles.permissions", randomizedPermissionIds,
                                                            keyMapping));
        }
    }
}
