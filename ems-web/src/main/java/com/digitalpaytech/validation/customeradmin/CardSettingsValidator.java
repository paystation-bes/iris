package com.digitalpaytech.validation.customeradmin;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.AuthorizationType;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.mvc.customeradmin.support.CardSettingsEditForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.validation.BaseValidator;

@Component("cardSettingsValidator")
public class CardSettingsValidator extends BaseValidator<CardSettingsEditForm> {
    @Autowired
    private CustomerCardTypeService customerCardTypeService;    
    
    public void setCustomerCardTypeService(CustomerCardTypeService customerCardTypeService) {
        this.customerCardTypeService = customerCardTypeService;
    }
    
    @Override
    public void validate(WebSecurityForm<CardSettingsEditForm> command, Errors errors) {    	
    }
        
    public void validate(WebSecurityForm<CardSettingsEditForm> command, Errors errors, RandomKeyMapping keyMapping) {
    	WebSecurityForm<CardSettingsEditForm> webSecurityForm = super.prepareSecurityForm(command, errors, "creditCardOfflineRetry");
        if (webSecurityForm == null) {
            return;
        }                
        
        CardSettingsEditForm form = webSecurityForm.getWrappedObject();
        if (form.isAllEmpty()) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("creditCardOfflineRetry", this.getMessage("error.common.required", 
            		(Object []) new String[] { this.getMessage("label.settings.cardSettings.RetryTimesBeforeBadCardList") })));
            return;
        }
        
        if (super.validateRequired(webSecurityForm, "creditCardOfflineRetry", "label.settings.cardSettings.RetryTimesBeforeBadCardList", form.getFormType()) == null){
        	return;
        }
                                
        if(webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE && form.getCustomerCardTypeRandomId() != null){
            form.setCustomerCardTypeId(super.validateRandomId(webSecurityForm, "customerCardTypeId", "label.settings.cardSettings.id",
            		form.getCustomerCardTypeRandomId(), keyMapping, CustomerCardType.class, Integer.class));
        }        
        
        if (form.getFormType().equalsIgnoreCase("types")) {
            validateEditFormCardTypes(webSecurityForm, errors);
        } else {
            validateEditFormForCardSettings(webSecurityForm, errors);
        }
        
        String description = super.validateExternalDescription(webSecurityForm, "description", "label.settings.cardSettings.description", form.getDescription(),
                												WebCoreConstants.VALIDATION_MIN_LENGTH_0);
        if (description != null){
        	form.setDescription(description);
        }
    }
     
    /**
     * Validates card Name, Track 2 Pattern, Authorization Method.
     * 
     * @param webSecurityForm
     *            Contains user input form
     * @param errors
     *            Object for accumulating error status  
     * @param cctId                  
     */    
    private void validateEditFormCardTypes(WebSecurityForm<CardSettingsEditForm> webSecurityForm, Errors errors) {
        
        CardSettingsEditForm form = webSecurityForm.getWrappedObject();
        
        String cardTypeName = (String) super.validateRequired(webSecurityForm, "cardTypeName", "label.name", form.getCardTypeName());        
        if (cardTypeName != null){
        	form.setCardTypeName(cardTypeName);
        	if (super.validateStandardText(webSecurityForm, "cardTypeName", "label.name", cardTypeName) == null) {
   	            webSecurityForm.addErrorStatus(new FormErrorStatus("cardTypeName", this.getMessage("error.card.settings.cardTypeName.missing")));
        	}
        }
        
        String track2Pattern = (String) super.validateRequired(webSecurityForm, "track2Pattern", "label.settings.cardSettings.Track2Pattern", form.getTrack2Pattern());
        if (track2Pattern != null) {
        	if (super.validateStringLength(webSecurityForm, "track2Pattern", "label.settings.cardSettings.Track2Pattern", track2Pattern, WebCoreConstants.VALIDATION_MIN_LENGTH_1,
                    WebCoreConstants.VALIDATION_MAX_LENGTH_25) == null) {
        		return;
        	}

        	form.setTrack2Pattern(track2Pattern);                    
	        if (!isValidPattern(track2Pattern) 
	        		|| isExistTrack2PatternInCreateMode(webSecurityForm, errors, form.getCustomerId(), track2Pattern, form.getCustomerCardTypeId())
	        		|| containsInvalidCharacters(webSecurityForm, errors, track2Pattern)) {
	            webSecurityForm.addErrorStatus(new FormErrorStatus("track2Pattern", this.getMessage("error.card.settings.invalidTrack2Pattern")));
	        }
        }   
        
        String authMethod = (String) super.validateRequired(webSecurityForm, "authorizationMethod", "label.settings.cardSettings.AuthorizationMethod", form.getAuthorizationMethod());        
        if (authMethod != null){
	        if (authMethod.equals(WebCoreConstants.NOT_APPLICABLE) || authMethod.equals(WebCoreConstants.ERROR_FEATURENOTENABLED)) {
	            webSecurityForm.addErrorStatus(new FormErrorStatus("authorizationMethod", this.getMessage("error.common.invalid", 
	            		(Object []) new String[] { this.getMessage("label.settings.cardSettings.AuthorizationMethod") })));
	        }
        }
    }
    
    private boolean containsInvalidCharacters(WebSecurityForm<CardSettingsEditForm> webSecurityForm, Errors errors, String track2Pattern) {
        for (int i = 0; i < track2Pattern.length(); i++) {
            if (WebCoreConstants.VALID_TRACK_2_PATTNER_CHARACTERS.indexOf(Character.toString(track2Pattern.charAt(i))) == -1) {
                return true;
            }
        }
        return false;
    }

    private boolean isValidPattern(String track2Pattern) {
        if (!CardProcessingUtil.isValidPattern(track2Pattern)) {
            return false;
        }
    	return true;
    }
    
    private boolean isExistTrack2PatternInCreateMode(WebSecurityForm<CardSettingsEditForm> webSecurityForm, Errors errors, Integer customerId, String track2Pattern, Integer cctId) {
        if (isExistTrack2Pattern(webSecurityForm, errors, customerId, track2Pattern, cctId) 
                && webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_CREATE) {
            return true;
        }
        return false;
    }
    
    
    /**
     * Checks CustomerCardType database table for existing pattern. 
     */
    private boolean isExistTrack2Pattern(WebSecurityForm<CardSettingsEditForm> webSecurityForm, Errors errors, Integer customerId, String track2Pattern, Integer cctId) {
        CustomerCardType ccType = customerCardTypeService.getCardTypeByTrack2Data(customerId, track2Pattern);
        if (ccType != null) {
            // Check if it's in Edit mode (cctId is not null) and not same ids.
            if (!ccType.getCardType().getName().equalsIgnoreCase(WebCoreConstants.NOT_APPLICABLE)
                && (cctId != null && ccType.getId().intValue() != cctId.intValue())) {
                return true;            
            }
        }  
        if (customerCardTypeService.isExistTrack2Pattern(customerId, track2Pattern)) {
            return true;
        }
        return false;
    }
        
    public boolean isExistCardTypeName(Integer customerId, String cardTypeName, Integer cctId) {
        List<CustomerCardType> existingCardTypeList = customerCardTypeService.getCustomerCardTypesByCustomerAndName(customerId, cardTypeName);
        if (existingCardTypeList != null) {
            for (CustomerCardType cct : existingCardTypeList) {
                if (cctId == null || cct.getId().intValue() != cctId.intValue()) {
                    return true;
                }
            }
        }
        return false;
    }
    
    private void validateEditFormForCardSettings(WebSecurityForm<CardSettingsEditForm> webSecurityForm, Errors errors) {
        
        CardSettingsEditForm form = webSecurityForm.getWrappedObject();
        // Validates credit card retry times.
        
        int maxOfflineRetry = super.validateIntegerLimits(webSecurityForm, "maxOfflineRetry", "label.settings.cardSettings.MaximumRetryTimes",
                                                          form.getMaxOfflineRetry(), WebCoreConstants.RETRY_MIN_LIMIT, WebCoreConstants.RETRY_MAX_LIMIT);
        super.validateIntegerLimits(webSecurityForm, "creditCardOfflineRetry", "label.settings.cardSettings.RetryTimesBeforeBadCardList", form
                .getCreditCardOfflineRetry(), WebCoreConstants.BAD_CARD_MIN_LIMIT, maxOfflineRetry < WebCoreConstants.BAD_CARD_MAX_LIMIT ? maxOfflineRetry : WebCoreConstants.BAD_CARD_MAX_LIMIT);
        
    }
    
    public void validateAuthorizationType(WebSecurityForm<CardSettingsEditForm> webSecurityForm, Errors errors, List<AuthorizationType> authorizationTypes, RandomKeyMapping keyMapping) {
    	CardSettingsEditForm form = webSecurityForm.getWrappedObject();
        String authTypeRandomId = form.getAuthorizationMethod();
        AuthorizationType type;
        Iterator<AuthorizationType> iter = authorizationTypes.iterator();
        while (iter.hasNext()) {
            type = iter.next();
            if (authTypeRandomId.equalsIgnoreCase(type.getRandomId())) {
            	form.setAuthorizeTypeId(super.validateRandomId(webSecurityForm, "authorizationMethod", "label.settings.cardSettings.AuthorizationMethod",
            			authTypeRandomId, keyMapping, AuthorizationType.class, Integer.class));
            	return;
            }
        }
        // Invalid AuthorizationType randomId.
        webSecurityForm.addErrorStatus(new FormErrorStatus("authorizationMethod", this.getMessage("error.common.invalid", (Object[]) new String[] { this
                .getMessage("label.settings.cardSettings.AuthorizationMethod") })));
    }           
}
