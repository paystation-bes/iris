package com.digitalpaytech.validation.customeradmin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.mvc.customeradmin.support.BannedCardSearchForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.LabelConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.validation.BaseValidator;
import com.digitalpaytech.validation.CardNumberValidator;

/**
 * This class validates user input banned card search form.
 * 
 * @author Brian Kim
 *
 */
@Component("cardManagementSearchValidator")
public class CardManagementSearchValidator extends BaseValidator<BannedCardSearchForm> {
    private static final String FILTER_CARD_NUMBER = "FilterCardNumber";
    
    @Autowired
    private CardNumberValidator<BannedCardSearchForm> cardNumberValidator;
    
    /**
     * This method validates card number and card type user input.
     * 
     */
    @Override
    public void validate(final WebSecurityForm<BannedCardSearchForm> command, final Errors errors) {
        final WebSecurityForm<BannedCardSearchForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken", false);
        final BannedCardSearchForm form = (BannedCardSearchForm) webSecurityForm.getWrappedObject();
        final RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        
        form.setFilterCardTypeId((String) validateRequired(webSecurityForm, "cardType", LabelConstants.CARD_SETTINGS_CARD_TYPE,
                                                           form.getFilterCardTypeId()));
        final Integer customerCardTypeId = super.validateRandomId(webSecurityForm, "filterCardType", LabelConstants.CARD_SETTINGS_CARD_TYPE,
                                                            form.getFilterCardTypeId(), keyMapping, CustomerCardType.class, Integer.class, "", "0");
        form.setCustomerCardTypeId(customerCardTypeId);
    }
    
    public final void validateCardNumber(final WebSecurityForm<BannedCardSearchForm> webSecurityForm) {
        
        final BannedCardSearchForm form = (BannedCardSearchForm) webSecurityForm.getWrappedObject();
        final CustomerCardType customerCardType = form.getCustomerCardType();
        Integer cardTypeId = null;
        if (customerCardType != null && customerCardType.getCardType() != null) {
            cardTypeId = customerCardType.getCardType().getId();
        }
        // validate for all types of cards
        if (cardTypeId == null) {
            final String cardNumber =
                    super.validateNumber(webSecurityForm, FILTER_CARD_NUMBER, LabelConstants.CARD_SETTINGS_CARD_NUMBER, form.getFilterCardNumber());
            if (cardNumber != null) {
                this.cardNumberValidator.validateAnyCardNumber(webSecurityForm, FILTER_CARD_NUMBER, LabelConstants.CARD_SETTINGS_CARD_NUMBER,
                                                               form.getFilterCardNumber());
            }
        } else {
            // validate for credit card
            if (cardTypeId == CardProcessingConstants.CARD_TYPE_CREDIT_CARD) {
                final String cardNumber =
                        super.validateNumber(webSecurityForm, FILTER_CARD_NUMBER, LabelConstants.CARD_SETTINGS_CARD_NUMBER, form.getFilterCardNumber());
                if (cardNumber != null) {
                    this.cardNumberValidator.validateCreditCardNumber(webSecurityForm, FILTER_CARD_NUMBER, LabelConstants.CARD_SETTINGS_CARD_NUMBER,
                                                                      form.getFilterCardNumber());
                }
            } else {
                final String cardNumber =
                        super.validateNumber(webSecurityForm, FILTER_CARD_NUMBER, LabelConstants.CARD_SETTINGS_CARD_NUMBER, form.getFilterCardNumber());
                if (cardNumber != null) {
                    // validate for smart card
                    if (cardTypeId == CardProcessingConstants.CARD_TYPE_SMART_CARD) {
                        this.cardNumberValidator.validateSmartCardNumber(webSecurityForm, FILTER_CARD_NUMBER, LabelConstants.CARD_SETTINGS_CARD_NUMBER,
                                                                         form.getFilterCardNumber());
                    } else {
                        // validate for value card
                        this.cardNumberValidator.validateValueCardNumber(webSecurityForm, FILTER_CARD_NUMBER, LabelConstants.CARD_SETTINGS_CARD_NUMBER,
                                                                         form.getFilterCardNumber());
                    }
                }
            }
        }
    }
    
    public final void setCardNumberValidator(final CardNumberValidator<BannedCardSearchForm> cardNumberValidator) {
        this.cardNumberValidator = cardNumberValidator;
    }
}
