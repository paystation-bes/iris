package com.digitalpaytech.validation.customeradmin;

import java.util.Calendar;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.EventDeviceType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.mvc.customeradmin.support.AlertCentreFilterForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.support.WebObjectId;
import com.digitalpaytech.validation.BaseValidator;

@Component("alertCentreFilterValidator")
public class AlertCentreFilterValidator extends BaseValidator<AlertCentreFilterForm> {
    @Override
    public void validate(final WebSecurityForm<AlertCentreFilterForm> command, final Errors errors) {
        
        final WebSecurityForm<AlertCentreFilterForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
        final RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        checkId(webSecurityForm, keyMapping);
        checkIsAlert(webSecurityForm);
        checkFilterIds(webSecurityForm, keyMapping);
    }
    
    private void checkId(final WebSecurityForm<AlertCentreFilterForm> webSecurityForm, final RandomKeyMapping keyMapping) {
        final AlertCentreFilterForm form = webSecurityForm.getWrappedObject();
        
        if (WebCoreConstants.ALL_STRING.equalsIgnoreCase(form.getRandomId())) {
            form.setId(null);
        } else {
            final WebObjectId webObject = super.validateRandomIdForWebObject(webSecurityForm, "randomId", "label.allCaps.filter"
                                                                             , form.getRandomId(), keyMapping);
            if (webObject != null) {
                if (webObject.getId() != null) {
                    final Integer id = (Integer) webObject.getId();
                    form.setId(id.intValue());
                } else {
                    form.setId(null);
                }
                if (webObject.getObjectType() != null) {
                    if (Location.class.equals(webObject.getObjectType())) {
                        form.setLocationOrRoute(false);
                    } else if (Route.class.equals(webObject.getObjectType())) {
                        form.setLocationOrRoute(true);
                    }
                } else {
                    form.setId(null);
                }
            }
        }
    }

    private void checkIsAlert(final WebSecurityForm<AlertCentreFilterForm> webSecurityForm) {
        final AlertCentreFilterForm form = webSecurityForm.getWrappedObject();
        if (!form.isAlert()) {
            if (form.getPageNumber() == null || 0 == form.getPageNumber().intValue()) {
                form.setPageNumber(1);
            }
            if (form.getDataKey() == null || 0 == form.getDataKey().longValue()) {
                form.setDataKey(Calendar.getInstance().getTimeInMillis());
            }
        }
        
    }

    private void checkFilterIds(final WebSecurityForm<AlertCentreFilterForm> webSecurityForm, final RandomKeyMapping keyMapping) {
        
        final AlertCentreFilterForm form = webSecurityForm.getWrappedObject();
        
        form.setSeverityId(super.validateRandomId(webSecurityForm, "severityRandomId", "label.settings.alerts.severity"
                                                  , form.getSeverityRandomId(), keyMapping, "-1"));
        form.setEventDeviceId(super.validateRandomId(webSecurityForm, "eventDeviceRandomId", "label.settings.alerts.module"
                                                     , form.getEventDeviceRandomId(), keyMapping, "-1"));
    }

}
