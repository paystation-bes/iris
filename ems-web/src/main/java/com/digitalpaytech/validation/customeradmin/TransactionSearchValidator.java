package com.digitalpaytech.validation.customeradmin;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Interval;
import org.joda.time.Period;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.constants.DateRangeOption;
import com.digitalpaytech.data.RangeData;
import com.digitalpaytech.mvc.customeradmin.support.TransactionReceiptSearchForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.BaseValidator;

@Component("transactionSearchValidator")
public class TransactionSearchValidator extends BaseValidator<TransactionReceiptSearchForm> {
    
    private static final int MAX_DATE_SEARCH = 30;
    private static final int MAX_YEARS_SEARCH = 2;
    private static final String TWO_YEARS ="2 years";
    
    @Override
    public final void validate(final WebSecurityForm<TransactionReceiptSearchForm> webSecurityForm, final Errors errors) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        String timeZoneStr = WebSecurityUtil.getCustomerTimeZone();
        if (timeZoneStr == null) {
            timeZoneStr = "GMT";
        }
        
        final TimeZone timeZone = TimeZone.getTimeZone(timeZoneStr);
        
        final TransactionReceiptSearchForm form = webSecurityForm.getWrappedObject();
        
        // Coupon Validation
        super.validateStringLength(webSecurityForm, "couponNumber", "label.coupon.id", form.getCouponNumber(), 0,
            WebCoreConstants.VALIDATION_MAX_LENGTH_20);
        super.validateAlphaNumericSearch(webSecurityForm, "couponNumber", "label.coupon.id", form.getCouponNumber());
        
        if (webSecurityForm != null) {
            form.setCouponNumber(form.getCouponNumber().trim());
            if (form.getCouponNumber().length() <= 0) {
                form.setCouponNumber((String) null);
            }
        }
        
        // Mobile Number Validation
        super.validateNumber(webSecurityForm, "smsNumber", "label.accounts.transactionSearch.mobileNumber", form.getMobileNumber());
        super.validateStringLength(webSecurityForm, "smsNumber", "label.accounts.transactionSearch.mobileNumber", form.getMobileNumber(), 0,
            WebCoreConstants.VALIDATION_MAX_LENGTH_15);
        
        // Space Number Validation
        super.validateNumber(webSecurityForm, "spaceNumber", "label.reporting.form.filter.parker.spaceNumber", form.getSpaceNumber());
        super.validateStringLength(webSecurityForm, "spaceNumber", "label.reporting.form.filter.parker.spaceNumber", form.getSpaceNumber(), 0,
            WebCoreConstants.VALIDATION_MIN_LENGTH_8);
        
        // Email Address Validation
        super.validateStringLength(webSecurityForm, "emailAddress", "label.accounts.transactionSearch.emailAddress", form.getEmailAddress(), 0,
            WebCoreConstants.VALIDATION_MAX_LENGTH_255);
        super.validateEmailAddress(webSecurityForm, "emailAddress", "label.accounts.transactionSearch.emailAddress", form.getEmailAddress());
        
        // Licence Plate Validation
        super.validateAlphaNumeric(webSecurityForm, "licencePlate", "label.license.plate", form.getPlateNumber());
        super.validateStringLength(webSecurityForm, "licencePlate", "label.license.plate", form.getPlateNumber(), 0,
            WebCoreConstants.VALIDATION_MAX_LENGTH_15);
        
        // Card Number Validation
        super.validateNumber(webSecurityForm, "cardNumber", "label.reporting.form.filter.card.number", form.getCardNumber());
        super.validateStringLength(webSecurityForm, "cardNumber", "label.reporting.form.filter.card.number", form.getCardNumber(), 0,
            WebCoreConstants.VALIDATION_MIN_LENGTH_4);
        
        //        if (webSecurityForm.getActionFlag() != WebSecurityConstants.ACTION_SEARCH) {
        //            webSecurityForm.addErrorStatus(new FormErrorStatus("actionFlag", this.getMessage("error.common.invalid", this.getMessage("label.information"))));
        //            return;
        //        }
        
        validateOtherSearchParams(webSecurityForm, keyMapping, timeZone);
    }
    
    public final void validateOtherSearchParams(final WebSecurityForm<TransactionReceiptSearchForm> webSecurityForm,
        final RandomKeyMapping keyMapping, final TimeZone timeZone) {
        
        final TransactionReceiptSearchForm form = webSecurityForm.getWrappedObject();
        
        validateRequired(webSecurityForm, "filterDateType", "label.cardManagement.cardRefund.txDate", form.getDateType());
        
        Date startDateTime = null;
        Date endDateTime = null;
        if ((form.getDateType().isRequireStart()) && (form.getDateType().isRequireEnd())) {
            validateRequired(webSecurityForm, "filterStartDate", "label.cardManagement.cardRefund.txDate.start", form.getStartDate());
            validateRequired(webSecurityForm, "filterEndDate", "label.cardManagement.cardRefund.txDate.end", form.getEndDate());
            startDateTime = validateDateTime(webSecurityForm, "filterStartDate", "label.cardManagement.cardRefund.txDate", webSecurityForm
                    .getWrappedObject().getStartDate(), "filterStartTime", "label.cardManagement.cardRefund.txDate.start",
                niceStartTime(form.getStartTime()), timeZone);
            endDateTime = validateDateTime(webSecurityForm, "filterEndDate", "label.cardManagement.cardRefund.txDate", webSecurityForm
                    .getWrappedObject().getEndDate(), "filterEndTime", "label.cardManagement.cardRefund.txDate.end", niceEndTime(webSecurityForm
                    .getWrappedObject().getEndTime()), timeZone);
        } else {
            final RangeData<Date> dateRange = form.getDateType().calculateRange(timeZone);
            startDateTime = dateRange.getStart();
            endDateTime = dateRange.getEnd();
        }

       
        final int dateDurationByDays = Days.daysBetween(new DateTime(startDateTime), new DateTime(endDateTime)).getDays();
        
        final Calendar calendar = Calendar.getInstance(timeZone);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
      
        //calculate two years ago from current date
        calendar.add(Calendar.YEAR, -MAX_YEARS_SEARCH);
        final Date twoYearsBack = calendar.getTime();
        
        final DateRangeOption rangeOption = form.getDateType();

        if (DateRangeOption.DATE_TIME_RANGE.equals(rangeOption)) {
            if (dateDurationByDays > MAX_DATE_SEARCH) {
                addError(webSecurityForm,
                         "startDateTime",
                         this.getMessage("error.common.invalid.span.not.within", MAX_DATE_SEARCH + " Days of each other",
                                         this.getMessage("label.cardManagement.cardRefund.txDate.start.to")));
                
                //check if startDateTime is not within two years ago
            } else if (startDateTime != null && !startDateTime.after(twoYearsBack)) {
                addError(webSecurityForm,
                         "startDateTime",
                         this.getMessage("error.common.invalid.date.not.within", "Last " + TWO_YEARS,
                                         this.getMessage("label.cardManagement.cardRefund.txDate.start")));
            }
        }
        
        form.setStartDateTime(startDateTime);
        form.setEndDateTime(endDateTime);
    }
    
    private String niceStartTime(final String startTime) {
        String buffer = startTime;
        if (buffer != null) {
            buffer = buffer.trim();
            if (buffer.length() <= 0) {
                buffer = null;
            }
        }
        
        String result = "00:00";
        if (buffer != null) {
            result = buffer;
        }
        
        return result;
    }
    
    private String niceEndTime(final String endTime) {
        String buffer = endTime;
        if (buffer != null) {
            buffer = buffer.trim();
            if (buffer.length() <= 0) {
                buffer = null;
            }
        }
        
        String result = "23:59";
        if (buffer != null) {
            result = buffer;
        }
        
        return result;
    }
}
