package com.digitalpaytech.validation.customeradmin;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.mvc.customeradmin.support.GlobalPreferencesEditForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.validation.BaseValidator;

@Component("querySpaceByValidator")
public class QuerySpaceByValidator extends BaseValidator<GlobalPreferencesEditForm> {
    
    @SuppressWarnings({ "checkstyle:designforextension" })
    @Override
    public void validate(final WebSecurityForm<GlobalPreferencesEditForm> command, final Errors errors) {
        final WebSecurityForm<GlobalPreferencesEditForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
        
        final GlobalPreferencesEditForm form = webSecurityForm.getWrappedObject();
        final Integer queryBy = form.getQuerySpacesBy();
        if (queryBy == null || queryBy.intValue() != PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME
                               && queryBy.intValue() != PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT
                               && queryBy.intValue() != PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_LATEST_EXPIRY_TIME
                               && queryBy.intValue() != PaystationConstants.PAYSTATION_STALL_QUERY_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("selectQuerySpacesBy",
                    this.getMessage("error.common.invalid", this.getMessage("label.settings.globalPref.selected.querySpacesBy"))));
        }
    }
    
}
