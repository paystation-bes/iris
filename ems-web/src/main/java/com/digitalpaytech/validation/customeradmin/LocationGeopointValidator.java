package com.digitalpaytech.validation.customeradmin;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.mvc.customeradmin.support.LocationLineGeopointEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.validation.BaseValidator;

@Component("locationGeopointValidator")
public class LocationGeopointValidator extends BaseValidator<LocationLineGeopointEditForm> {
    
    
    @Override
    public void validate(final WebSecurityForm<LocationLineGeopointEditForm> command, final Errors errors) {
        
    }
    
    public void validate(final WebSecurityForm<LocationLineGeopointEditForm> command, final Errors errors, final boolean isMigrated, final int customerId) {
    }    
}
