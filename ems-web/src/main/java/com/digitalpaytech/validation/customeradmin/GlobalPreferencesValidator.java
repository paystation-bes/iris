package com.digitalpaytech.validation.customeradmin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.TimezoneVId;
import com.digitalpaytech.mvc.customeradmin.support.GlobalPreferencesEditForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.validation.BaseValidator;

@Component("globalPreferencesValidator")
public class GlobalPreferencesValidator extends BaseValidator<GlobalPreferencesEditForm> {
    
    private static final String LABEL_SETTINGS_GLOBAL_PREF_SELECTED_TIME_ZONE = "label.settings.globalPref.selected.timeZone";
    private static final String SELECT_TIME_ZONE = "selectTimeZone";
    @Autowired
    private CustomerAdminService customerAdminService;
    
    @Override
    @SuppressWarnings({ "checkstyle:designforextension" })
    public void validate(final WebSecurityForm<GlobalPreferencesEditForm> command, final Errors errors) {
        final WebSecurityForm<GlobalPreferencesEditForm> webSecurityForm = super.prepareSecurityForm(command, errors,  WebCoreConstants.POSTTOKEN_STRING);
        if (webSecurityForm == null) {
            return;
        }
        
        final GlobalPreferencesEditForm form = webSecurityForm.getWrappedObject();
        
        String timezone = (String) super.validateRequired(webSecurityForm, SELECT_TIME_ZONE, LABEL_SETTINGS_GLOBAL_PREF_SELECTED_TIME_ZONE,
                                                          form.getCurrentTimeZone());
        
        if (timezone != null) {
            
            timezone = super.validateStringLength(webSecurityForm, SELECT_TIME_ZONE, LABEL_SETTINGS_GLOBAL_PREF_SELECTED_TIME_ZONE, timezone,
                                                  WebCoreConstants.VALIDATION_MIN_LENGTH_1, WebCoreConstants.VALIDATION_MAX_LENGTH_30);
            
            timezone = super.validateRegEx(webSecurityForm, SELECT_TIME_ZONE, LABEL_SETTINGS_GLOBAL_PREF_SELECTED_TIME_ZONE, timezone,
                                           WebCoreConstants.REGEX_TIME_ZONE, "error.common.invalid.special.character.name", "error.regex.timezone");
            
            /* validate if the input value is existing in the time zone list. */
            if (!isExistingTimeZone(timezone)) {
                webSecurityForm.addErrorStatus(new FormErrorStatus(SELECT_TIME_ZONE,
                        this.getMessage("error.common.required", this.getMessage(LABEL_SETTINGS_GLOBAL_PREF_SELECTED_TIME_ZONE))));
            }
        }
    }
    
    
    /**
     * This method validates time zone input value.
     * 
     * @param timeZone
     *            time zone string value
     * @return true if the value exists in the time zone list, false if it does not exist in the list
     */
    private boolean isExistingTimeZone(final String timeZone) {
        boolean result = false;
        final List<TimezoneVId> timezones = customerAdminService.getTimeZones();
        for (TimezoneVId tz : timezones) {
            if (tz.getName().equals(timeZone)) {
                result = true;
                break;
            }
        }
        return result;
    }
}
