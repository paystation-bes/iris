package com.digitalpaytech.validation.customeradmin;

import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.mvc.customeradmin.support.CardRefundForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.BaseValidator;
import com.digitalpaytech.validation.CardNumberValidator;

/**
 * This class validates Card Refund user input form to refund the permit transaction.
 * 
 * @author Brian Kim
 *            
 */
@Component("cardRefundValidator")
public class CardRefundValidator extends BaseValidator<CardRefundForm> {
    private static final String PTID = "ptId";
    private static final String LABEL_CARDMGMT_REFUND = "label.cardManagement.cardRefund.tx";
    private static final String LABEL_SETTINGS_CARDEXP = "label.settings.cardSettings.CardExpiry";
    private static final String LABEL_CARD_NUMBER = "label.settings.cardSettings.CardNumber";
    
    @Autowired
    private CardNumberValidator<CardRefundForm> cardValidator;
    
    /**
     * 
     */
    @Override
    public final void validate(final WebSecurityForm<CardRefundForm> command, final Errors errors) {
        final WebSecurityForm<CardRefundForm> webSecurityForm = prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
                       
        if (webSecurityForm.getActionFlag() != WebSecurityConstants.ACTION_REFUND) {
            webSecurityForm
                    .addErrorStatus(new FormErrorStatus("actionFlag", this.getMessage("error.common.invalid", this.getMessage("label.information"))));
            return;
        }
        
        final CardRefundForm form = webSecurityForm.getWrappedObject();
        
        if (!form.getIsCPS()) {
            // Validate Card Number & Expiry Date
            validateRequired(webSecurityForm, "cardNumber", LABEL_CARD_NUMBER, form.getCardNumber());
            validateRequired(webSecurityForm, "refundCardExpiry", LABEL_SETTINGS_CARDEXP, form.getRefundCardExpiry());
        }
        String timeZoneStr = WebSecurityUtil.getCustomerTimeZone();
        if (timeZoneStr == null) {
            timeZoneStr = "GMT";
        }
        final TimeZone timeZone = TimeZone.getTimeZone(timeZoneStr);
        
        if (!form.getIsCPS() || !StringUtils.isBlank(form.getRefundCardExpiry())) {
            this.cardValidator.validateExpiryDate(webSecurityForm, "filterExpiry", LABEL_SETTINGS_CARDEXP, form.getRefundCardExpiry(), true, timeZone);
        }
        
    }
        
    public final void validateProcessorTransaction(final WebSecurityForm<CardRefundForm> command, final Errors errors) {
        final WebSecurityForm<CardRefundForm> webSecurityForm = prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
                               
        final CardRefundForm form = webSecurityForm.getWrappedObject();
        validateRequired(webSecurityForm, PTID, LABEL_CARDMGMT_REFUND, form.getRandomProcessorTransactionId());
        final RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        form.setProcessorTransactionId(validateRandomId(webSecurityForm, PTID, LABEL_CARDMGMT_REFUND, form.getRandomProcessorTransactionId(),
                                                        keyMapping, ProcessorTransaction.class, Long.class));
        
    }
    
    public final void setCardValidator(final CardNumberValidator<CardRefundForm> cardValidator) {
        this.cardValidator = cardValidator;
    }
}
