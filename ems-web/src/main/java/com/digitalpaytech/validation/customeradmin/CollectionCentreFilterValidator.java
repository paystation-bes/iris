package com.digitalpaytech.validation.customeradmin;

import java.util.Calendar;

import org.apache.cxf.common.util.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.mvc.customeradmin.support.CollectionCentreFilterForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.support.WebObjectId;
import com.digitalpaytech.validation.BaseValidator;

@Component("collectionCentreFilterValidator")
public class CollectionCentreFilterValidator extends BaseValidator<CollectionCentreFilterForm> {
    @Override
    public void validate(final WebSecurityForm<CollectionCentreFilterForm> command, final Errors errors) {
                
        final WebSecurityForm<CollectionCentreFilterForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");        
                
        if (webSecurityForm == null) {
            return;
        }
        final RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        checkId(webSecurityForm, keyMapping);
        checkIsAlert(webSecurityForm);
    }
    
    private void checkId(final WebSecurityForm<CollectionCentreFilterForm> webSecurityForm, final RandomKeyMapping keyMapping) {
        
        final CollectionCentreFilterForm form = webSecurityForm.getWrappedObject();
        
        if (WebCoreConstants.ALL_STRING.equalsIgnoreCase(form.getRandomId())) {
            form.setId(null);
        } else {
            final WebObjectId webObject = super.validateRandomIdForWebObject(webSecurityForm, "randomId", "label.allCaps.filter", 
                                                                             form.getRandomId(), keyMapping);
            if (webObject != null) {
                if (webObject.getId() != null) {
                    final Integer id = (Integer) webObject.getId();
                    form.setId(id.intValue());
                } else {
                    form.setId(null);
                }
                if (webObject.getObjectType() != null) {
                    if (Location.class.equals(webObject.getObjectType())) {
                        form.setLocationOrRoute(false);
                    } else if (Route.class.equals(webObject.getObjectType())) {
                        form.setLocationOrRoute(true);
                    }                     
                } else {
                    form.setId(null);
                }
            }            
        }
        
        if (!StringUtils.isEmpty(form.getSeverityRandomId())) {
            form.setSeverity(true);
            form.setSeverityId((Integer) keyMapping.getKey(form.getSeverityRandomId()));
        }        
    }
    
    private void checkIsAlert(final WebSecurityForm<CollectionCentreFilterForm> webSecurityForm) {
        final CollectionCentreFilterForm form = webSecurityForm.getWrappedObject();
        if (!form.isAlert()) {
            if (form.getPageNumber() == null || 0 == form.getPageNumber().intValue()) {
                form.setPageNumber(1);
            }
            if (form.getTimeStamp() == null || 0 == form.getTimeStamp().longValue()) {
                form.setTimeStamp(Calendar.getInstance().getTimeInMillis());
            }
        }
        
    }

}
