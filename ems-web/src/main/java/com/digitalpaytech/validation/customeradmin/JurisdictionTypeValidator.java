package com.digitalpaytech.validation.customeradmin;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import java.util.Set;
import java.util.HashSet;
import java.util.Arrays;

import com.digitalpaytech.mvc.customeradmin.support.JurisdictionTypeEditForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.validation.BaseValidator;

@Component("jurisdictionTypePreferredValidator")
public class JurisdictionTypeValidator extends BaseValidator<JurisdictionTypeEditForm> {
    private static final String JURISDICTION_TYPE_PREFERRED = "jurisdictionTypePreferred";
    private static final String ERROR_COMMON_INVALID = "error.common.invalid";
    private static final Set<Integer> PREFERRED_OPTIONS =
            new HashSet<>(Arrays.asList(PaystationConstants.PAYSTATION_JURISDICTION_PREFERRED_DISABLED,
                                        PaystationConstants.PAYSTATION_JURISDICTION_PREFERRED_BY_CUSTOMER,
                                        PaystationConstants.PAYSTATION_JURISDICTION_PREFERRED_BY_LOCATION));
    private static final Set<Integer> LIMITED_OPTIONS = new HashSet<>(
            Arrays.asList(PaystationConstants.PAYSTATION_JURISDICTION_LIMITED_DISABLED, PaystationConstants.PAYSTATION_JURISDICTION_LIMITED_BY_CUSTOMER,
                          PaystationConstants.PAYSTATION_JURISDICTION_LIMITED_BY_LOCATION));
    
    @SuppressWarnings({ "checkstyle:designforextension" })
    @Override
    public void validate(final WebSecurityForm<JurisdictionTypeEditForm> command, final Errors errors) {
        final WebSecurityForm<JurisdictionTypeEditForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
        
        final JurisdictionTypeEditForm form = webSecurityForm.getWrappedObject();
        final Integer jurisdictionTypePreferred = form.getJurisdictionTypePreferred();
        final Integer jurisdictionTypeLimited = form.getJurisdictionTypeLimited();
        validateJurisdictionType(webSecurityForm, jurisdictionTypePreferred, jurisdictionTypeLimited);
    }
    
    public void validateJurisdictionType(final WebSecurityForm<JurisdictionTypeEditForm> webSecurityForm, final Integer jurisdictionTypePreferred,
        final Integer jurisdictionTypeLimited) {
        
        if (jurisdictionTypePreferred == null || !validateJurisdictionType(jurisdictionTypePreferred.intValue(), PREFERRED_OPTIONS)) {
            webSecurityForm.addErrorStatus(new FormErrorStatus(JURISDICTION_TYPE_PREFERRED,
                    this.getMessage(ERROR_COMMON_INVALID, this.getMessage("label.settings.globalPref.selected.jurisdiction.type.preferred"))));
        }
        
        if (jurisdictionTypeLimited == null || !validateJurisdictionType(jurisdictionTypeLimited.intValue(), LIMITED_OPTIONS)) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("jurisdictionTypeLimited",
                    this.getMessage(ERROR_COMMON_INVALID, this.getMessage("label.settings.globalPref.selected.jurisdiction.type.limited"))));
        }
    }
    
    public boolean validateJurisdictionType(final Integer jurisdictionType, final Set<Integer> options) {
        return options.contains(jurisdictionType);
    }
    
    public void validatePreferredParkerImportForm(final WebSecurityForm<JurisdictionTypeEditForm> command, final int origPreferredValue,
        final Errors errors, final boolean isPrepared) {
        
        final WebSecurityForm<JurisdictionTypeEditForm> webSecurityForm;
        if (!isPrepared) {
            webSecurityForm = super.prepareSecurityForm(command, errors, WebCoreConstants.POSTTOKEN_STRING);
        } else {
            webSecurityForm = command;
        }
        
        if (webSecurityForm == null || webSecurityForm.getWrappedObject() == null
            || webSecurityForm.getWrappedObject().getJurisdictionTypePreferred() == null) {
            return;
        }
        
        final boolean differentFlag = webSecurityForm.getWrappedObject().getJurisdictionTypePreferred()
                .intValue() != PaystationConstants.PAYSTATION_JURISDICTION_PREFERRED_DISABLED
                                      && webSecurityForm.getWrappedObject().getJurisdictionTypePreferred().intValue() != origPreferredValue;
        
        if (differentFlag && isJurisdictionTypeFileEmpty(webSecurityForm)) {
            webSecurityForm.addErrorStatus(new FormErrorStatus(JURISDICTION_TYPE_PREFERRED,
                    this.getMessage("error.settings.preferredparking.importForm.not.exist")));
        } else if (differentFlag) {
            super.validateRequired(webSecurityForm, FormFieldConstants.FILE, "label.settings.preferredparking.importForm.sourceFile",
                                   webSecurityForm.getWrappedObject().getFile().getOriginalFilename());
        }
    }
    
    public boolean isJurisdictionTypeFileEmpty(final WebSecurityForm<JurisdictionTypeEditForm> webSecurityForm) {
        final JurisdictionTypeEditForm jurisdictionTypeEditForm = webSecurityForm.getWrappedObject();
        if (jurisdictionTypeEditForm.getFile() == null || jurisdictionTypeEditForm.getFile().isEmpty()) {
            return true;
        }
        return false;
    }
    
}
