package com.digitalpaytech.validation.customeradmin;

import java.util.Date;
import java.util.TimeZone;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.mvc.customeradmin.support.CustomerCardForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.BaseValidator;

@Component("customerCardValidator")
public class CustomerCardValidator extends BaseValidator<CustomerCardForm> {
	@Override
	public void validate(WebSecurityForm<CustomerCardForm> target, Errors errors) {
		WebSecurityForm<CustomerCardForm> form = prepareSecurityForm(target, errors, "postToken");
		if(form != null) {
			RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
			TimeZone timeZone = TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone());
			CustomerCardForm cardForm = form.getWrappedObject();
			
			if(form.getActionFlag() == WebSecurityConstants.ACTION_CREATE) {
				// Validate Card Number
				validateRequired(form, "cardNumber", "label.customerCard.cardNumber", cardForm.getCardNumber());
				validateNumberStringLength(form, "cardNumber", "label.customerCard.cardNumber", cardForm.getCardNumber(), CardProcessingConstants.VALUE_CARD_MIN_LENGTH, CardProcessingConstants.BAD_VALUE_CARD_MAX_LENGTH);
				validateCustomerCardNumber(form, "cardNumber", "label.customerCard.cardNumber", cardForm.getCardNumber());
				
				// Validate Card Type
				if(validateRequired(form, "cardType", "label.customerCard.message.cardType", cardForm.getCardTypeRandomId()) != null) {
					Integer actualId = validateRandomId(form, "cardType", "label.customerCard.cardType", cardForm.getCardTypeRandomId(), keyMapping, CustomerCardType.class, Integer.class, "0");
					if(actualId == null) {
						cardForm.setCardTypeRandomId((String) null);
					}
				}
			}
			
			// Validate Card Begin Date
			Date startDate = validateDate(form, "validFrom", "label.customerCard.validFrom", cardForm.getStartValidDate(), timeZone);
			if(startDate == null) {
				cardForm.setStartValidDate((String) null);
			}
			
			// Validate Card Expire Date
			Date expireDate = validateDate(form, "validTo", "label.customerCard.validTo", cardForm.getExpireDate(), timeZone);
			if(expireDate == null) {
				cardForm.setExpireDate((String) null);
			}
			
			// Validate Card Expire Date not before Card Start Date
			
			Date currentDate = DateUtil.getCurrentGmtDate();
	        String expiry_date_label = getMessage("label.coupon.expiryDate");
	        String current_date_label = getMessage("label.coupon.currentDate");
	        String start_date_label = getMessage("label.coupon.startDate");
			
			if (expireDate != null && startDate != null && !checkEndDate(expireDate, startDate)) {
	            super.addErrorStatus(form, "startDate", "error.common.endAfterCurrent", new Object[] { expiry_date_label, start_date_label});            
	        }
			
			// Validate Card Expire Date not before current date
	        
	        // Check end date is not before current date
	        if (expireDate != null && currentDate != null && !checkEndDate(expireDate, currentDate)) {
	            super.addErrorStatus(form, "startDate", "error.common.endAfterCurrent", new Object[] { expiry_date_label, current_date_label});            
	        }
			
			// Validate Grace Period
			cardForm.setGracePeriod(validateNumber(form, "gracePeriod", "label.customerCard.gracePeriod.cleaned", cardForm.getGracePeriod()));
			if(cardForm.getGracePeriod() != null) {
				validateIntegerLimits(form, "gracePeriod", "label.customerCard.gracePeriod.cleaned", cardForm.getGracePeriod(), 0, WebCoreConstants.CUSTOM_CARD_GRACE_PERIOD_MAX);
			}
			
			// Validate Max number of usage
			cardForm.setMaxNumOfUses(validateNumber(form, "maxNumOfUses", "label.customerCard.maxNumOfUses", cardForm.getMaxNumOfUses()));
			if(cardForm.getMaxNumOfUses() != null) {
			    
				validateIntegerLimits(form, "maxNumOfUses", "label.customerCard.maxNumOfUses", cardForm.getMaxNumOfUses(), 1, WebCoreConstants.CUSTOM_CARD_MAX_NUM_OF_USES);
			}
			
			// Validate Location
			validateRandomIdForWebObject(form, "location", "label.customerCard.location", cardForm.getLocationRandomId(), keyMapping, "0");
			
			// Validate Comment			
			cardForm.setComment(super.validateExternalDescription(form, "comment", "label.customerCard.comment", cardForm.getComment(), 0));			
		}
	}

}
