package com.digitalpaytech.validation.customeradmin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.mvc.customeradmin.support.CustomerAdminPayStationEditForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.BaseValidator;
import com.digitalpaytech.service.PointOfSaleService;

@Component("customerAdminPayStationValidator")
public class CustomerAdminPayStationValidator extends BaseValidator<CustomerAdminPayStationEditForm> {
    private static final String PAY_STATION_RANDOM_ID = "payStationRandomId";
    private static final String LABEL_PAY_STATION = "label.payStation";
    private static final String NAME = "name";
    private static final String LABEL_NAME = "label.name";
    private static final String LOCATION_RANDOM_ID = "locationRandomId";
    private static final String LABEL_LOCATION = "label.location";
    private static final String ERROR_INVALID_CONFIG = "error.invalid.config";
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Override
    public void validate(final WebSecurityForm<CustomerAdminPayStationEditForm> target, final Errors errors) {
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }

    public final void validate(final WebSecurityForm<CustomerAdminPayStationEditForm> command, 
                               final Errors errors, 
                               final RandomKeyMapping keyMapping) {
        
        final WebSecurityForm<CustomerAdminPayStationEditForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        
        checkId(webSecurityForm, keyMapping);
        checkName(webSecurityForm);
        checkLocation(webSecurityForm, keyMapping);
        checkStatus(webSecurityForm);
        
    }
    
    private void checkId(final WebSecurityForm<CustomerAdminPayStationEditForm> webSecurityForm, final RandomKeyMapping keyMapping) {
        
        final CustomerAdminPayStationEditForm form = webSecurityForm.getWrappedObject();
        
        final Object requiredCheck = super.validateRequired(webSecurityForm, PAY_STATION_RANDOM_ID, LABEL_PAY_STATION, form.getRandomId());
        if (requiredCheck != null) {
            final Integer id = super.validateRandomId(webSecurityForm, PAY_STATION_RANDOM_ID, LABEL_PAY_STATION,
                                                           form.getRandomId(), keyMapping, PointOfSale.class, Integer.class);
            if (id != null) {
                form.setPointOfSaleId(id.intValue());
            }
        }
    }
    
    private void checkName(final WebSecurityForm<CustomerAdminPayStationEditForm> webSecurityForm) {
        
        final CustomerAdminPayStationEditForm form = webSecurityForm.getWrappedObject();
        final String name = form.getName();
        
        final Object requiredCheck = super.validateRequired(webSecurityForm, NAME, LABEL_NAME, name);

        if (requiredCheck != null) {
            super.validateStringLength(webSecurityForm, NAME, LABEL_NAME, name, WebCoreConstants.VALIDATION_MIN_LENGTH_1, 
                                                                                    WebCoreConstants.VALIDATION_MAX_LENGTH_25);
            super.validatePayStationText(webSecurityForm, NAME, LABEL_NAME, name);
            
            final PointOfSale pos = this.pointOfSaleService.findPointOfSaleById(form.getPointOfSaleId());
            
            final Collection<String> names = new ArrayList<>();
            names.add(name.toLowerCase());
            // null for third parameter, locPosNameSeparator, for "PointOfSale.findPointOfSaleByNames".
            final List<PointOfSale> list = 
                    this.pointOfSaleService.getDetachedPointOfSaleByLowerCaseNames(WebSecurityUtil.getUserAccount().getCustomer().getId(), names, null);
            if (list != null && !list.isEmpty() && !list.get(0).getName().equalsIgnoreCase(pos.getName())) {
                super.addError(webSecurityForm, NAME, super.getMessage("error.common.invalid.paystation.same.name", name));
            }            
        }

        return;
        
    }
    
    private void checkLocation(final WebSecurityForm<CustomerAdminPayStationEditForm> webSecurityForm, final RandomKeyMapping keyMapping) {
        
        final CustomerAdminPayStationEditForm form = webSecurityForm.getWrappedObject();
        
        final Object requiredCheck = super.validateRequired(webSecurityForm, LOCATION_RANDOM_ID, LABEL_LOCATION, form.getLocationRandomId());
        if (requiredCheck != null) {
            final Integer id = super.validateRandomId(webSecurityForm, LOCATION_RANDOM_ID, LABEL_LOCATION, form.getLocationRandomId(),
                                                keyMapping, Location.class, Integer.class);
            if (id != null) {
                form.setLocationId(id.intValue());
            }
        }
    }
    
    private void checkStatus(final WebSecurityForm<CustomerAdminPayStationEditForm> webSecurityForm) {
        
        final CustomerAdminPayStationEditForm form = webSecurityForm.getWrappedObject();
        final Boolean active = form.getActive();
        final Boolean hidden = form.getHidden();
        
        if (active == null) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("active", this.getMessage(ERROR_INVALID_CONFIG)));
        }
        if (hidden == null) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("hidden", this.getMessage(ERROR_INVALID_CONFIG)));
        }
    }
}
