package com.digitalpaytech.validation.customeradmin;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.mvc.customeradmin.support.CustomerCardUploadForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.validation.BaseValidator;

@Component("customerCardUploadValidator")
public class CustomerCardUploadValidator extends BaseValidator<CustomerCardUploadForm> {
    @Override
    public final void validate(final WebSecurityForm<CustomerCardUploadForm> target, final Errors errors) {
        final WebSecurityForm<CustomerCardUploadForm> form = prepareSecurityForm(target, errors, WebCoreConstants.POSTTOKEN_STRING);
        if (form != null) {
            final CustomerCardUploadForm uploadForm = form.getWrappedObject();
            validateRequired(form, FormFieldConstants.FILE, "label.customerCard.importForm.sourceFile", uploadForm.getFile().getOriginalFilename());
            validateRequired(form, FormFieldConstants.IMPORT_MODE, FormFieldConstants.LABEL_IMPORT_MODE, uploadForm.getImportMode());
        }
    }
    
}
