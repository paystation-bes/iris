package com.digitalpaytech.validation.customeradmin;

import java.util.Calendar;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.mvc.customeradmin.support.BannedCardEditForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.DashboardUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.validation.BaseValidator;
import com.digitalpaytech.validation.CardNumberValidator;

/**
 * This class validates user input banned card (add, edit) form.
 * 
 * @author Brian Kim
 * 
 */
@Component("cardManagementEditValidator")
public class CardManagementEditValidator extends BaseValidator<BannedCardEditForm> {
	@Autowired
	private CardNumberValidator<BannedCardEditForm> cardNumberValidator;
	
    @Autowired
    private CustomerCardTypeService customerCardTypeService;
    
    public void setCustomerCardTypeService(CustomerCardTypeService customerCardTypeService) {
        this.customerCardTypeService = customerCardTypeService;
    }
    
    @Override
    public void validate(WebSecurityForm<BannedCardEditForm> command, Errors errors) {
        WebSecurityForm<BannedCardEditForm> webSecurityForm = prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
        
        RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        if(!super.validateActionFlag(webSecurityForm, WebSecurityConstants.ACTION_UPDATE, WebSecurityConstants.ACTION_CREATE)){
            return;
        }
         
        BannedCardEditForm form = webSecurityForm.getWrappedObject();
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
            String randomCustomerBadCardId = form.getRandomCustomerBadCardId();
            if (StringUtils.isBlank(randomCustomerBadCardId) || !DashboardUtil.validateRandomId(randomCustomerBadCardId)) {
                webSecurityForm.addErrorStatus(new FormErrorStatus("randomCustomerBadCardId", this.getMessage("error.common.invalid", this.getMessage("label.settings.cardSettings.CardType"))));
                return;
            }
            
            Object customerBadCardId = keyMapping.getKey(randomCustomerBadCardId);
            if (customerBadCardId == null || !DashboardUtil.validateDBIntegerPrimaryKey(customerBadCardId.toString())) {
                form.setCustomerBadCardId(null);
                webSecurityForm.addErrorStatus(new FormErrorStatus("randomCustomerBadCardId", this.getMessage("error.common.invalid", this.getMessage("label.settings.cardSettings.CardType"))));
            } else {
                form.setCustomerBadCardId(new Integer(customerBadCardId.toString()));
            }
        }
        
        validateCardNumberAndCardType(webSecurityForm, keyMapping);
        
        validateComment(webSecurityForm);
    }
    
    /**
     * This method validates card number and card type user input.
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     */
    private void validateCardNumberAndCardType(WebSecurityForm<BannedCardEditForm> webSecurityForm, RandomKeyMapping keyMapping) {
        TimeZone timeZone = TimeZone.getTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT);
        BannedCardEditForm form = webSecurityForm.getWrappedObject();
        
        boolean isCreditCard = false;
        boolean validateExpiry = false;
        int minLength = 1;
        int maxLength = -1;
        
        validateRequired(webSecurityForm, "cardType", "label.settings.cardSettings.CardType", form.getCardType());
        form.setCardTypeId(validateRandomId(webSecurityForm, "cardType", "label.settings.cardSettings.CardType", form.getCardType(), keyMapping, CustomerCardType.class, Integer.class));
        
        CardType cardType = null;
        if (form.getCardTypeId() != null) {
        	CustomerCardType cct = this.customerCardTypeService.getCardType(form.getCardTypeId());
        	if(cct != null) {
        		cardType = cct.getCardType();
        	}
        }
        
        if(cardType != null) {
            switch (cardType.getId()) {
                case WebCoreConstants.CREDIT_CARD: {
                	minLength = CardProcessingConstants.CREDIT_CARD_MIN_LENGTH;
                    maxLength = CardProcessingConstants.CREDIT_CARD_MAX_LENGTH;
                    isCreditCard = true;
                    validateExpiry = true;
                    break;
                }
                case WebCoreConstants.SMART_CARD: {
                	minLength = CardProcessingConstants.SMART_CARD_MIN_LENGTH;
                    maxLength = CardProcessingConstants.BAD_SMART_CARD_MAX_LENGTH;
                    break;
                }
                case WebCoreConstants.VALUE_CARD: {
                	minLength = CardProcessingConstants.VALUE_CARD_MIN_LENGTH;
                    maxLength = CardProcessingConstants.BAD_VALUE_CARD_MAX_LENGTH;
                    break;
                }
                default: {
                    webSecurityForm.addErrorStatus(new FormErrorStatus("cardType", this.getMessage("error.common.invalid", this.getMessage("label.settings.cardSettings.CardType") )));
                }
            }
        }
        
        form.setCardNumber((String) validateRequired(webSecurityForm, "cardNumber", "label.settings.cardSettings.CardNumber", form.getCardNumber()));
        validateNumber(webSecurityForm, "cardNumber", "label.settings.cardSettings.CardNumber", form.getCardNumber());
        
        if ((maxLength >= 0)
            && (validateNumberStringLength(webSecurityForm, "cardNumber", "label.settings.cardSettings.CardNumber", form.getCardNumber(), minLength, maxLength) != null)) {
            if (isCreditCard) {
                if (!customerCardTypeService.isCreditCardData(form.getCardNumber())) {
                    webSecurityForm.addErrorStatus(new FormErrorStatus("cardNumber", this.getMessage("error.common.invalid", this.getMessage("label.settings.cardSettings.CardNumber"))));
                }
            }
        }
        
        if (validateExpiry) {
            validateRequired(webSecurityForm, "cardExpiry", "label.settings.cardSettings.CardExpiry", form.getCardExpiry());
            if (validateStringLength(webSecurityForm, "cardExpiry", "label.settings.cardSettings.CardExpiry", form.getCardExpiry(), 4) != null) {
                if (validateNumber(webSecurityForm, "cardExpiry", "label.settings.cardSettings.CardExpiry", form.getCardExpiry()) != null) {
                    int expiryMonth = Integer.parseInt(form.getCardExpiry().substring(0, 2));
                    if ((expiryMonth <= 0) || (expiryMonth > 12)) {
                        webSecurityForm.addErrorStatus(new FormErrorStatus("cardExpiry", this.getMessage("error.common.invalid", this.getMessage("label.settings.cardSettings.CardExpiry"))));
                    } else {
                        int expiryYear = Integer.parseInt(form.getCardExpiry().substring(2));
                        
                        Calendar curr = Calendar.getInstance(timeZone);
                        int currYear = curr.get(Calendar.YEAR) % 100;
                        int currMonth = curr.get(Calendar.MONTH) + 1;
                        if ((expiryYear < currYear) || ((expiryYear == currYear) && (expiryMonth < currMonth))) {
                            webSecurityForm.addErrorStatus(new FormErrorStatus("cardExpiry", this.getMessage("error.cardExpiry.expired")));
                        }
                    }
                }
            }
        }
    }
    
    private void validateComment(WebSecurityForm<BannedCardEditForm> webSecurityForm) {
        BannedCardEditForm form = webSecurityForm.getWrappedObject();
        String comment = form.getComment();
        if (!StringUtils.isBlank(comment)) {            
            form.setComment(validateExternalDescription(webSecurityForm, "comment", "label.cardManagement.bannedCard.comment", 
                            form.getComment(), WebCoreConstants.VALIDATION_MIN_LENGTH_1));
        }
    }
}
