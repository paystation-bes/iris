package com.digitalpaytech.validation.customeradmin;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.RouteType;
import com.digitalpaytech.mvc.customeradmin.support.RouteEditForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.MessageInfo;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.CustomerAlertTypeService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.util.DashboardUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.BaseValidator;

@Component("routeSettingValidator")
public class RouteSettingValidator extends BaseValidator<RouteEditForm> {
    
    private static Logger log = Logger.getLogger(RouteSettingValidator.class);
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private RouteService routeService;
    
    @Autowired
    private CustomerAlertTypeService customerAlertTypeService;
    
    // This should only be called by JUnit classes
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }
    
    // This should only be called by JUnit classes
    public void setRouteService(RouteService routeService) {
        this.routeService = routeService;
    }
    
    @Override
    public void validate(WebSecurityForm<RouteEditForm> command, Errors errors) {
        WebSecurityForm<RouteEditForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
        RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        
        if (webSecurityForm.getActionFlag() != WebSecurityConstants.ACTION_CREATE
            && webSecurityForm.getActionFlag() != WebSecurityConstants.ACTION_UPDATE) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("actionFlag", this.getMessage("error.common.invalid",
                this.getMessage("label.information"))));
            return;
        }
        
        checkId(webSecurityForm, keyMapping);
        validateGroupName(webSecurityForm);
        validateRouteType(webSecurityForm, keyMapping);
        validatePaystationIds(webSecurityForm, keyMapping);
    }
    
    // TODO Probably not necessary
    //    private void checkNotMigrated(WebSecurityForm webSecurityForm, RandomKeyMapping keyMapping) {
    //        if (webSecurityForm.getActionFlag() != WebSecurityConstants.ACTION_UPDATE) {
    //            webSecurityForm.addErrorStatus(new FormErrorStatus("actionFlag", this.getMessage("error.customer.notMigrated")));
    //            return;
    //        }
    //        
    //        checkId(webSecurityForm, keyMapping);
    //        RouteEditForm form = (RouteEditForm) webSecurityForm.getWrappedObject();
    //        Route existingRoute = this.routeService.findRouteById(form.getRouteId());
    //        if (!form.getRouteName().equals(existingRoute.getName())) {
    //            webSecurityForm.addErrorStatus(new FormErrorStatus("routeName", this.getMessage("error.field.notMigrated", this.getMessage("label.name"))));
    //        }
    //        validateRouteType(webSecurityForm, keyMapping);
    //        
    //        Set<RoutePOS> posList = existingRoute.getRoutePOSes();
    //        
    //        if (posList == null || posList.size() == 0) {
    //            if (!(form.getRandomizedPointOfSaleIds() == null || form.getRandomizedPointOfSaleIds().isEmpty() || WebCoreConstants.EMPTY_STRING.equals(form
    //                    .getRandomizedPointOfSaleIds().get(0)))) {
    //                webSecurityForm.addErrorStatus(new FormErrorStatus("PSList", this.getMessage("error.field.notMigrated", this.getMessage("label.payStations"))));
    //            }
    //        } else {
    //            List<String> posIds = Arrays.asList(form.getRandomizedPointOfSaleIds().get(0).split(","));
    //            if (posList.size() != posIds.size()) {
    //                webSecurityForm.addErrorStatus(new FormErrorStatus("PSList", this.getMessage("error.field.notMigrated", this.getMessage("label.payStations"))));
    //            } else {
    //                form.setPointOfSaleIds(super.validateRandomIds(webSecurityForm, "PSList", "label.information", posIds, keyMapping));
    //                for (RoutePOS routePos : posList) {
    //                    boolean isFound = false;
    //                    for (Integer posId : form.getPointOfSaleIds()) {
    //                        if (routePos.getPointOfSale().getId().intValue() == posId.intValue()) {
    //                            isFound = true;
    //                            break;
    //                        }
    //                    }
    //                    if (!isFound) {
    //                        webSecurityForm.addErrorStatus(new FormErrorStatus("PSList", this.getMessage("error.field.notMigrated",
    //                                                                                                     this.getMessage("label.payStations"))));
    //                        break;
    //                    }
    //                }
    //            }
    //        }
    //        
    //    }
    
    private void checkId(WebSecurityForm<RouteEditForm> webSecurityForm, RandomKeyMapping keyMapping) {
        
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
            
            RouteEditForm form = webSecurityForm.getWrappedObject();
            
            Object requiredCheck = super.validateRequired(webSecurityForm, "routeID", "label.route", form.getRandomizedRouteId());
            if (requiredCheck != null) {
                Integer id = super.validateRandomId(webSecurityForm, "routeID", "label.route", form.getRandomizedRouteId(), keyMapping, Route.class,
                    Integer.class);
                form.setRouteId(id.intValue());
            }
        }
        
    }
    
    /**
     * It validates group name user input field.
     * 
     * @param webSecurityForm
     *            user input form
     */
    private void validateGroupName(WebSecurityForm<RouteEditForm> webSecurityForm) {
        
        RouteEditForm routeEditForm = webSecurityForm.getWrappedObject();
        String routeName = routeEditForm.getRouteName();
        Object requiredCheck = super.validateRequired(webSecurityForm, "routeName", "label.name", routeName);
        if (requiredCheck != null) {
            super.validateStringLength(webSecurityForm, "routeName", "label.name", routeName, WebCoreConstants.VALIDATION_MIN_LENGTH_1,
                WebCoreConstants.VALIDATION_MAX_LENGTH_25);
            super.validateStandardText(webSecurityForm, "routeName", "label.name", routeName);
            
            if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 0) {
                
                String oldRouteName = "";
                
                if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
                    Route existingRoute = this.routeService.findRouteById(routeEditForm.getRouteId());
                    oldRouteName = existingRoute.getName();
                }
                if (!oldRouteName.equals(routeEditForm.getRouteName()) && sameNameExists(routeName.trim())) {
                    webSecurityForm.addErrorStatus(new FormErrorStatus("routeName", this.getMessage("error.common.duplicated",
                        new Object[] { this.getMessage("label.route"), this.getMessage("label.name") })));
                }
                
            }
        } else {
            return;
        }
        if (routeName != null) {
            routeEditForm.setRouteName(routeName.trim());
        }
    }
    
    /**
     * It validates user input route type field.
     * 
     * @param webSecurityForm
     *            user input form
     * @param keyMapping
     *            RandomKeyMapping obj to validate randomized string value.
     */
    private void validateRouteType(WebSecurityForm<RouteEditForm> webSecurityForm, RandomKeyMapping keyMapping) {
        
        RouteEditForm routeEditForm = webSecurityForm.getWrappedObject();
        Object requiredCheck = super.validateRequired(webSecurityForm, "routeType", "label.type", routeEditForm.getRandomizedRouteTypeId());
        if (requiredCheck != null) {
            Integer id = super.validateRandomId(webSecurityForm, "routeType", "label.type", routeEditForm.getRandomizedRouteTypeId(), keyMapping,
                RouteType.class, Integer.class);
            routeEditForm.setRouteTypeId(id);
            
            // Note: This code will be used if it's decided customers want the restriction that you can't
            //		 change the route type of a route that is attached to a customer defined alert.
            //            if(webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
            //	            Route route = this.routeService.findRouteByIdAndEvict(routeEditForm.getRouteId());
            //	            if(route.getRouteType().getId() != routeEditForm.getRouteTypeId().intValue() &&
            //	            		this.attachedToAlerts(routeEditForm.getRouteTypeId())) {
            //	            	super.addErrorStatus(webSecurityForm, "routeType", "alert.settings.routes.ChangeRouteTypeWithDefinedAlert", new Object[] { });
            //	            }
            //            }
        }
    }
    
    /**
     * It validates list of paystations that user has selected.
     * 
     * @param webSecurityForm
     *            user input form
     * @param keyMapping
     *            RandomKeyMapping obj to validate randomized string value.
     */
    private void validatePaystationIds(WebSecurityForm<RouteEditForm> webSecurityForm, RandomKeyMapping keyMapping) {
        RouteEditForm routeEditForm = webSecurityForm.getWrappedObject();
        List<String> posIds = routeEditForm.getRandomizedPointOfSaleIds();
        Collection<String> ids = null;
        
        if (!super.isListNullorEmpty(posIds)) {
            ids = Arrays.asList(routeEditForm.getRandomizedPointOfSaleIds().get(0).split(","));
            routeEditForm.setPointOfSaleIds(super.validateRandomIds(webSecurityForm, "PSList", "link.nav.settings.payStation.selection", ids,
                keyMapping));
        }
    }
    
    /**
     * This method validates user input randomized route ID. Doesn't call super.validateRandomId because it
     * is called by methods that don't have the web form object
     * 
     * @param request
     *            HttpServletRequest object
     * @return actual route ID if process succeeds, "false" string if process
     *         fails.
     */
    public String validateRouteId(HttpServletRequest request) {
        /* validate the randomized routeID from request parameter. */
        String randomizedRouteId = request.getParameter("routeID");
        if (StringUtils.isBlank(randomizedRouteId) || !DashboardUtil.validateRandomId(randomizedRouteId)) {
            log.warn("### Invalid RouteId in the request. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        /* validate the actual routeID. */
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        Object actualRouteId = keyMapping.getKey(randomizedRouteId);
        if (actualRouteId == null || !DashboardUtil.validateDBIntegerPrimaryKey(actualRouteId.toString())) {
            log.warn("### Invalid RouteId in the request. ###");
            return WebCoreConstants.RESPONSE_FALSE;
        }
        
        return actualRouteId.toString();
    }
    
    /**
     * Verify whether this route can be deleted from database
     * 
     * @param request
     * @return {@link MessageInfo} in JSON format.
     */
    public String verifyDeleteRoute(HttpServletRequest request) {
        MessageInfo msg = new MessageInfo();
        String actualRouteId = this.validateRouteId(request);
        if (actualRouteId.equals(WebCoreConstants.RESPONSE_FALSE)) {
            return WebCoreConstants.RESPONSE_FALSE;
        }
        Integer routeId = new Integer(actualRouteId);
        if (this.attachedToAlerts(routeId)) {
            msg.setRequireConfirmation(true);
            msg.addMessage(this.getMessage("alert.settings.routes.DeleteRouteWithDefinedAlert"));
        }
        
        return WidgetMetricsHelper.convertToJson(msg, "messages", true);
    }
    
    /**
     * Verify whether this route is used in Customer Alerts
     * 
     * @param request
     * @return true if number of customer alerts using this rout is > 0, false otherwise
     */
    public boolean attachedToAlerts(Integer routeId) {
        /* validate the randomized routeID from request parameter. */
        int definedAlertsCnt = this.customerAlertTypeService.countByRouteId(routeId);
        if (definedAlertsCnt > 0) {
            return true;
        }
        return false;
    }
    
    public boolean sameNameExists(String newRouteName) {
        Integer custId = WebSecurityUtil.getUserAccount().getCustomer().getId();
        Collection<Route> routes = routeService.findRoutesByCustomerIdAndNameUpperCase(custId, newRouteName.toUpperCase());
        if (routes != null && !routes.isEmpty()) {
            return true;
        }
        return false;
    }
}
