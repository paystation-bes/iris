package com.digitalpaytech.validation.customeradmin;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.DayOfWeek;
import com.digitalpaytech.domain.ExtensibleRate;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.ParkingPermission;
import com.digitalpaytech.mvc.customeradmin.support.EbpRatePermissionEditForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.util.ComparableRange;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.BaseValidator;

/**
 * ExtendByPhoneValidator validates Add/Edit ExtensibleRate and ParkingPermission.
 * 
 * @author Allen Liang
 */

@Component("extendByPhoneValidator")
public class ExtendByPhoneValidator extends BaseValidator<EbpRatePermissionEditForm> {
    @Autowired
    private LocationService locationService;
    private static final String UNLIMITED = "unlimited";
    private static final String TIME_LIMITED = "timelimited";
    
    @Override
    public void validate(WebSecurityForm<EbpRatePermissionEditForm> command, Errors errors) {
        WebSecurityForm<EbpRatePermissionEditForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
        EbpRatePermissionEditForm form = webSecurityForm.getWrappedObject();
        
        checkId(webSecurityForm);
        
        int locId = Integer.parseInt(form.getLocationId());
        validateCommonFields(webSecurityForm, errors);
        if (webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0) {
            return;
        }
        
        checkName(webSecurityForm);
        
        if (Boolean.valueOf(form.getExtensibleRateFlag())) {
            validateExtensibleRate(webSecurityForm, errors);
            
        } else if (Boolean.valueOf(form.getParkingPermissionFlag())) {
            validateParkingPermission(webSecurityForm, form, locId, errors);
            
        } else {
            webSecurityForm
                    .addErrorStatus(new FormErrorStatus("options", this.getMessage("error.common.required", this.getMessage("label.options"))));
        }
    }
    
    private void checkId(WebSecurityForm<EbpRatePermissionEditForm> webSecurityForm) {
        
        EbpRatePermissionEditForm form = webSecurityForm.getWrappedObject();
        RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        if (Boolean.valueOf(form.getExtensibleRateFlag()) && !StringUtils.isBlank(form.getExtensibleRateId())) {
            Integer id = super.validateRandomId(webSecurityForm, "extensibleRateId", "label.settings.locations.extendByPhone.rates",
                                                form.getExtensibleRateId(), keyMapping, ExtensibleRate.class, Integer.class);
            form.setExtensibleRateRealId(0);
            if (id != null) {
                form.setExtensibleRateRealId(id);
            }
        } else if (Boolean.valueOf(form.getParkingPermissionFlag()) && !StringUtils.isBlank(form.getParkingPermissionId())) {
            Integer id = super.validateRandomId(webSecurityForm, "parkingPermissionId", "label.settings.locations.extendByPhone.parkingPolicy",
                                                form.getParkingPermissionId(), keyMapping, ParkingPermission.class, Integer.class);
            form.setParkingPermissionRealId(0);
            if (id != null) {
                form.setParkingPermissionRealId(id);
            }
        }
    }
    
    private void checkName(WebSecurityForm<EbpRatePermissionEditForm> webSecurityForm) {
        EbpRatePermissionEditForm form = webSecurityForm.getWrappedObject();
        String name = form.getName();
        name = (String) super.validateRequired(webSecurityForm, "name", "label.name", name);
        if (name != null) {
            
            super.validateStringLength(webSecurityForm, "name", "label.name", name, WebCoreConstants.VALIDATION_MIN_LENGTH_1,
                                       WebCoreConstants.VALIDATION_MAX_LENGTH_16);
            
            super.validateExtraStandardText(webSecurityForm, "name", "label.name", name);
        }
    }
    
    private void validateCommonFields(WebSecurityForm<EbpRatePermissionEditForm> webSecurityForm, Errors errors) {
        EbpRatePermissionEditForm form = webSecurityForm.getWrappedObject();
        if (!hasSelectedAtLeastOneDay(form)) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("daysOfWeek", this.getMessage("error.common.required", this
                    .getMessage("label.settings.locations.extendByPhone.occurance"))));
        }
        super.validateRequired(webSecurityForm, "startTime", "error.common.required", form.getStartTime());
        super.validateRequired(webSecurityForm, "endTime", "error.common.required", form.getEndTime());
        
        String startTime = form.getStartTime();
        String endTime = form.getEndTime();
        
        if (endTime != null && startTime != null && endTime.compareTo(startTime) < 1) {
            super.addErrorStatus(webSecurityForm, "startDate", "error.common.endBeforeStart", new Object[] { getMessage("label.end.time"),
                getMessage("label.start.time") });
        }
        
        String name = form.getName();
        
    }
    
    private void validateExtensibleRate(WebSecurityForm<EbpRatePermissionEditForm> webSecurityForm, Errors errors) {
        EbpRatePermissionEditForm form = webSecurityForm.getWrappedObject();
        super.validateRequired(webSecurityForm, "rateValue", "label.settings.locations.extendByPhone.ratePerHour", form.getRateValue());
        super.validateDollarAmt(webSecurityForm, "rateValue", "label.settings.locations.extendByPhone.ratePerHour", form.getRateValue());
        super.validateDoubleLimits(webSecurityForm, "rateValue", "label.settings.locations.extendByPhone.ratePerHour", form.getRateValue(),
                                   WebCoreConstants.EBP_RATE_LIMIT_MIN, WebCoreConstants.EBP_RATE_LIMIT_MAX);
        super.validateRequired(webSecurityForm, "serviceFee", "label.settings.locations.extendByPhone.serviceFee", form.getRateServiceFee());
        super.validateDollarAmt(webSecurityForm, "serviceFee", "label.settings.locations.extendByPhone.serviceFee", form.getRateServiceFee());
        super.validateDoubleLimits(webSecurityForm, "serviceFee", "label.settings.locations.extendByPhone.serviceFee", form.getRateServiceFee(),
                                   WebCoreConstants.EBP_SERVICE_FEE_LIMIT_MIN, WebCoreConstants.EBP_SERVICE_FEE_LIMIT_MAX);
        super.validateRequired(webSecurityForm, "minExtensionMinutes", "label.settings.locations.extendByPhone.minimumExtension",
                               form.getMinExtensionMinutes());
        super.validateFloat(webSecurityForm, "minExtensionMinutes", "label.settings.locations.extendByPhone.minimumExtension",
                            form.getMinExtensionMinutes());
        super.validateIntegerLimits(webSecurityForm, "minExtensionMinutes", "label.settings.locations.extendByPhone.minimumExtension",
                                    form.getMinExtensionMinutes(), WebCoreConstants.EBP_EXTENTION_MINS_LIMIT_MIN,
                                    WebCoreConstants.EBP_EXTENTION_MINS_LIMIT_MAX);
        
        validateExistingName(webSecurityForm);
        
        super.validateTime(webSecurityForm, "startTime", "alert.settings.locations.ExtendByPhone.setTime", form.getStartTime(),
                           TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone()));
        super.validateTime(webSecurityForm, "endTime", "alert.settings.locations.ExtendByPhone.setTime", form.getEndTime(),
                           TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone()));
        validateOverlappedExtensibleRate(webSecurityForm);
        
        if (StringUtils.isNotBlank(form.getMinExtensionMinutes())) {
            String[] startHrMin = form.getStartTime().split(WebCoreConstants.COLON);
            String[] endHrMin = form.getEndTime().split(WebCoreConstants.COLON);
            startHrMin = DateUtil.addMinIfMissing(startHrMin);
            endHrMin = DateUtil.addMinIfMissing(endHrMin);
            validateMinExtension(webSecurityForm, startHrMin, endHrMin, Integer.parseInt(form.getMinExtensionMinutes()));
        }
    }
    
    private void validateMinExtension(WebSecurityForm<EbpRatePermissionEditForm> webSecurityForm, String[] startHrMin, String[] endHrMin,
        int minExtensionMinutes) {
        if (minExtensionMinutes > super.getTimeDiffInMinute(startHrMin, endHrMin)) {
            EbpRatePermissionEditForm form = webSecurityForm.getWrappedObject();
            webSecurityForm.addErrorStatus(new FormErrorStatus("minExtensionMinutes", this
                    .getMessage("alert.settings.locations.ExtendByPhone.Rate.MinExtension.invalid", form.getMinExtensionMinutes())));
        }
    }
    
    private void validateOverlappedExtensibleRate(WebSecurityForm<EbpRatePermissionEditForm> webSecurityForm) {
        EbpRatePermissionEditForm form = webSecurityForm.getWrappedObject();
        int locId = Integer.parseInt(form.getLocationId());
        Location loc = new Location();
        loc.setId(locId);
        String[] startHrMin = form.getStartTime().split(WebCoreConstants.COLON);
        String[] endHrMin = form.getEndTime().split(WebCoreConstants.COLON);
        startHrMin = DateUtil.addMinIfMissing(startHrMin);
        endHrMin = DateUtil.addMinIfMissing(endHrMin);
        List<Integer> daysOfWeek = createIntegerDaysOfWeek(form.getDaysOfWeeks());
        ExtensibleRate rate = new ExtensibleRate();
        rate.setLocation(loc);
        rate.setBeginHourLocal(Integer.parseInt(startHrMin[0]));
        rate.setBeginMinuteLocal(Integer.parseInt(startHrMin[1]));
        rate.setEndHourLocal(Integer.parseInt(endHrMin[0]));
        rate.setEndMinuteLocal(Integer.parseInt(endHrMin[1]));
        rate.setId(form.getExtensibleRateRealId());
        
        List<ExtensibleRate> list = locationService.findOverlappedExtensibleRate(rate, daysOfWeek);
        if (list != null && !list.isEmpty() && !isValidTimeFrame(list, rate)) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("startTime", this.getMessage("error.extendByPhone.rate.overlapped", form.getName(),
                                                                                            createDayNames(form))));
        }
    }
    
    private ComparableRange<Integer> createRange(ExtensibleRate rate) {
        ComparableRange<Integer> result = null;
        if ((rate.getBeginHourLocal() == 0) && (rate.getBeginMinuteLocal() == 0) && (rate.getEndHourLocal() == 0) && (rate.getEndMinuteLocal() == 0)) {
            result = new ComparableRange<Integer>(0, true, 840, true);
        } else {
            result = new ComparableRange<Integer>((rate.getBeginHourLocal() * 60) + rate.getBeginMinuteLocal(), true, (rate.getEndHourLocal() * 60)
                                                                                                                      + rate.getEndMinuteLocal(),
                    true);
        }
        
        return result;
    }
    
    protected boolean isValidTimeFrame(List<ExtensibleRate> extensibleRates, ExtensibleRate newRate) {
        boolean valid = true;
        
        ComparableRange<Integer> newRange = createRange(newRate);
        Iterator<ExtensibleRate> iter = extensibleRates.iterator();
        while (valid && iter.hasNext()) {
            ExtensibleRate rate = iter.next();
            if ((newRate.getId() == null) || (!newRate.getId().equals(rate.getId()))) {
                ComparableRange<Integer> oldRange = createRange(rate);
                valid = !newRange.isOverlapWith(oldRange);
            }
        }
        
        return valid;
        
        //        GregorianCalendar newBeginCal = new GregorianCalendar();
        //        setCalendar(newBeginCal, newRate.getBeginHourLocal(), newRate.getBeginMinuteLocal(), 0);
        //        
        //        GregorianCalendar newEndCal = new GregorianCalendar();
        //        setCalendar(newEndCal, newRate.getEndHourLocal(), newRate.getEndMinuteLocal(), 0);
        //        
        //        GregorianCalendar beginCal = new GregorianCalendar();
        //        GregorianCalendar endCal = new GregorianCalendar();
        //        ExtensibleRate rate;
        //        Iterator<ExtensibleRate> iter = extensibleRates.iterator();
        //        while (iter.hasNext()) {
        //            rate = iter.next();
        //            boolean isSameRate = false;
        //            if (rate.getId() != null && rate.getId().equals(newRate.getId())) {
        //                isSameRate = true;
        //            }
        //            beginCal = setCalendar(beginCal, rate.getBeginHourLocal(), rate.getBeginMinuteLocal(), 0);
        //            endCal = setCalendar(endCal, rate.getEndHourLocal(), rate.getEndMinuteLocal(), 0);
        //            
        //            if ((rate.getBeginHourLocal() == 0 && rate.getBeginMinuteLocal() == 0 && rate.getEndHourLocal() == 0 && rate.getEndMinuteLocal() == 0)
        //                && !isSameRate) {
        //                // Whole day rate (from 00-24)
        //                return false;
        //            } else if ((isSameHoursMinutes(newBeginCal, beginCal) && isSameHoursMinutes(newEndCal, endCal)) && !isSameRate) {
        //                return false;
        //            } else if (isInBetween(newBeginCal, newEndCal, beginCal, endCal) && !isSameRate) {
        //                return false;
        //            } else if (isBeforeBeginAndAfterEnd(newBeginCal, newEndCal, beginCal, endCal) && !isSameRate) {
        //                return false;
        //            }
        //        }
        //        return true;
    }
    
    private ComparableRange<Integer> createRange(ParkingPermission permission) {
        ComparableRange<Integer> result = null;
        if ((permission.getBeginHourLocal() == 0) && (permission.getBeginMinuteLocal() == 0) && (permission.getEndHourLocal() == 0)
            && (permission.getEndMinuteLocal() == 0)) {
            result = new ComparableRange<Integer>(0, true, 840, true);
        } else {
            result = new ComparableRange<Integer>((permission.getBeginHourLocal() * 60) + permission.getBeginMinuteLocal(), true,
                    (permission.getEndHourLocal() * 60) + permission.getEndMinuteLocal(), true);
        }
        
        return result;
    }
    
    protected boolean isValidTimeFrame(List<ParkingPermission> parkingPermissions, ParkingPermission newPermission) {
        boolean valid = true;
        
        ComparableRange<Integer> newRange = createRange(newPermission);
        Iterator<ParkingPermission> iter = parkingPermissions.iterator();
        while (valid && iter.hasNext()) {
            ParkingPermission permission = iter.next();
            if ((newPermission.getId() == null) || (!newPermission.getId().equals(permission.getId()))) {
                ComparableRange<Integer> oldRange = createRange(permission);
                valid = !newRange.isOverlapWith(oldRange);
            }
        }
        
        return valid;
        
        //        GregorianCalendar newBeginCal = new GregorianCalendar();
        //        setCalendar(newBeginCal, newPermission.getBeginHourLocal(), newPermission.getBeginMinuteLocal(), 0);
        //        
        //        GregorianCalendar newEndCal = new GregorianCalendar();
        //        setCalendar(newEndCal, newPermission.getEndHourLocal(), newPermission.getEndMinuteLocal(), 0);
        //        
        //        GregorianCalendar beginCal = new GregorianCalendar();
        //        GregorianCalendar endCal = new GregorianCalendar();
        //        ParkingPermission perm;
        //        Iterator<ParkingPermission> iter = parkingPermissions.iterator();
        //        while (iter.hasNext()) {
        //            perm = iter.next();
        //            beginCal = setCalendar(beginCal, perm.getBeginHourLocal(), perm.getBeginMinuteLocal(), 0);
        //            endCal = setCalendar(endCal, perm.getEndHourLocal(), perm.getEndMinuteLocal(), 0);
        //            boolean isSamePerm = false;
        //            if (perm.getId() != null && perm.getId().equals(newPermission.getId())) {
        //                isSamePerm = true;
        //            }
        //            
        //            if ((perm.getBeginHourLocal() == 0 && perm.getBeginMinuteLocal() == 0 && perm.getEndHourLocal() == 0 && perm.getEndMinuteLocal() == 0)
        //                && !isSamePerm) {
        //                // Whole day rate (from 00-24)
        //                return false;
        //            } else if ((isSameHoursMinutes(newBeginCal, beginCal) && isSameHoursMinutes(newEndCal, endCal)) && !isSamePerm) {
        //                return false;
        //            } else if (isInBetween(newBeginCal, newEndCal, beginCal, endCal) && !isSamePerm) {
        //                return false;
        //            } else if (isBeforeBeginAndAfterEnd(newBeginCal, newEndCal, beginCal, endCal) && !isSamePerm) {
        //                return false;
        //            }
        //        }
        //        return true;
    }
    
    private StringBuilder appendCommaCharIfNecessary(StringBuilder bdr, String str) {
        if (!bdr.toString().endsWith(str)) {
            bdr.append(StandardConstants.STRING_COMMA).append(str);
        }
        return bdr;
    }
    
    private String createDayNames(EbpRatePermissionEditForm form) {
        String SPACE = " ";
        StringBuilder bdr = new StringBuilder();
        if (Boolean.valueOf(form.getSunday())) {
            bdr.append(this.getMessage("label.sunday"));
        }
        if (Boolean.valueOf(form.getMonday())) {
            bdr = appendCommaCharIfNecessary(bdr, SPACE);
            bdr.append(this.getMessage("label.monday"));
        }
        if (Boolean.valueOf(form.getTuesday())) {
            bdr = appendCommaCharIfNecessary(bdr, SPACE);
            bdr.append(this.getMessage("label.tuesday"));
        }
        if (Boolean.valueOf(form.getWednesday())) {
            bdr = appendCommaCharIfNecessary(bdr, SPACE);
            bdr.append(this.getMessage("label.wednesday"));
        }
        if (Boolean.valueOf(form.getThursday())) {
            bdr = appendCommaCharIfNecessary(bdr, SPACE);
            bdr.append(this.getMessage("label.thursday"));
        }
        if (Boolean.valueOf(form.getFriday())) {
            bdr = appendCommaCharIfNecessary(bdr, SPACE);
            bdr.append(this.getMessage("label.friday"));
        }
        if (Boolean.valueOf(form.getSaturday())) {
            bdr = appendCommaCharIfNecessary(bdr, SPACE);
            bdr.append(this.getMessage("label.saturday"));
        }
        return bdr.toString();
    }
    
    private void validateParkingPermission(WebSecurityForm<EbpRatePermissionEditForm> webSecurityForm, EbpRatePermissionEditForm form, int locId,
        Errors errors) {
        if (StringUtils.isBlank(form.getName())) {//remove
            webSecurityForm.addErrorStatus(new FormErrorStatus("name", this.getMessage("error.common.required", this.getMessage("label.name"))));
        }
        if (StringUtils.isNotBlank(form.getName()) && form.getName().length() > WebCoreConstants.VALIDATION_MAX_LENGTH_20) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("name",
                    this.getMessage("error.common.invalid.lengths", this.getMessage("label.name"), "1",
                                    String.valueOf(WebCoreConstants.VALIDATION_MAX_LENGTH_20))));
        }
        if (!form.getPermissionStatus().equalsIgnoreCase(UNLIMITED) && !form.getPermissionStatus().equalsIgnoreCase(TIME_LIMITED)) {
            webSecurityForm.addErrorStatus(new FormErrorStatus(TIME_LIMITED, this.getMessage("error.common.required", this
                    .getMessage("label.settings.locations.extendByPhone.timeLimitedParking"))));
            
        } else if (form.getPermissionStatus().equalsIgnoreCase(TIME_LIMITED) && StringUtils.isBlank(form.getMaxDurationMinutes())) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("maxDurationMinutes", this.getMessage("error.common.required", this
                    .getMessage("label.settings.locations.extendByPhone.maxDuration"))));
        }
        
        String[] startHrMin = form.getStartTime().split(WebCoreConstants.COLON);
        String[] endHrMin = form.getEndTime().split(WebCoreConstants.COLON);
        startHrMin = DateUtil.addMinIfMissing(startHrMin);
        endHrMin = DateUtil.addMinIfMissing(endHrMin);
        
        validateExistingName(webSecurityForm);
        
        super.validateTime(webSecurityForm, "startTime", "alert.settings.locations.ExtendByPhone.setTime", form.getStartTime(),
                           TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone()));
        super.validateTime(webSecurityForm, "endTime", "alert.settings.locations.ExtendByPhone.setTime", form.getEndTime(),
                           TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone()));
        validateOverlappedParkingPermission(webSecurityForm, form, locId, startHrMin, endHrMin);
        
        if (form.getPermissionStatus().equalsIgnoreCase(TIME_LIMITED) && StringUtils.isNotBlank(form.getMaxDurationMinutes())
            && StringUtils.isNumeric(form.getMaxDurationMinutes())) {
            validateMaxDuration(webSecurityForm, startHrMin, endHrMin, Integer.parseInt(form.getMaxDurationMinutes()));
        }
    }
    
    private void validateMaxDuration(WebSecurityForm<EbpRatePermissionEditForm> webSecurityForm, String[] startHrMin, String[] endHrMin,
        int maxDurationMinutes) {
        if (maxDurationMinutes > super.getTimeDiffInMinute(startHrMin, endHrMin)) {
            EbpRatePermissionEditForm form = webSecurityForm.getWrappedObject();
            webSecurityForm.addErrorStatus(new FormErrorStatus("maxDurationMinutes", this
                    .getMessage("alert.settings.locations.ExtendByPhone.maxDurationLength", form.getMaxDurationMinutes())));
        }
    }
    
    private void validateOverlappedParkingPermission(WebSecurityForm<EbpRatePermissionEditForm> webSecurityForm, EbpRatePermissionEditForm form,
        int locId, String[] startHrMin, String[] endHrMin) {
        Location loc = new Location();
        loc.setId(locId);
        List<Integer> daysOfWeek = createIntegerDaysOfWeek(form.getDaysOfWeeks());
        ParkingPermission perm = new ParkingPermission();
        perm.setLocation(loc);
        perm.setBeginHourLocal(Integer.parseInt(startHrMin[0]));
        perm.setBeginMinuteLocal(Integer.parseInt(startHrMin[1]));
        perm.setEndHourLocal(Integer.parseInt(endHrMin[0]));
        perm.setEndMinuteLocal(Integer.parseInt(endHrMin[1]));
        perm.setId(form.getParkingPermissionRealId());
        
        List<ParkingPermission> list = locationService.findOverlappedParkingPermission(perm, daysOfWeek);
        if (!super.isListNullorEmpty(list) && !isValidTimeFrame(list, perm)) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("startTime", this.getMessage("error.extendByPhone.policy.overlapped", form.getName(),
                                                                                            createDayNames(form))));
        }
    }
    
    private void validateExistingName(WebSecurityForm<EbpRatePermissionEditForm> webSecurityForm) {
        
        EbpRatePermissionEditForm form = webSecurityForm.getWrappedObject();
        
        int locId = Integer.parseInt(form.getLocationId());
        
        if (Boolean.valueOf(form.getExtensibleRateFlag())) {
            List<List<ExtensibleRate>> rateDayOfWeeks = locationService.getValidRatesByLocationId(locId);
            Iterator<ExtensibleRate> erIter;
            Iterator<List<ExtensibleRate>> iter = rateDayOfWeeks.iterator();
            while (iter.hasNext()) {
                erIter = iter.next().iterator();
                while (erIter.hasNext()) {
                    ExtensibleRate er = erIter.next();
                    if (er.getName().equalsIgnoreCase(form.getName()) && er.getId() != form.getExtensibleRateRealId()) {
                        webSecurityForm.addErrorStatus(new FormErrorStatus("name", this.getMessage("error.extendByPhone.rate.duplicateName",
                                                                                                   form.getName())));
                        return;
                    }
                }
            }
        } else if (Boolean.valueOf(form.getParkingPermissionFlag())) {
            List<List<ParkingPermission>> permissionDayOfWeeks = locationService.getValidParkingPermissionsByLocationId(locId);
            Iterator<ParkingPermission> ppIter;
            Iterator<List<ParkingPermission>> iter = permissionDayOfWeeks.iterator();
            while (iter.hasNext()) {
                ppIter = iter.next().iterator();
                while (ppIter.hasNext()) {
                    ParkingPermission pp = ppIter.next();
                    if (pp.getName().equalsIgnoreCase(form.getName()) && pp.getId() != form.getParkingPermissionRealId()) {
                        webSecurityForm.addErrorStatus(new FormErrorStatus("name", this.getMessage("error.extendByPhone.policy.duplicateName",
                                                                                                   form.getName())));
                        return;
                    }
                }
            }
            
        }
    }
    
    private List<Integer> createIntegerDaysOfWeek(List<DayOfWeek> dayOfWeeks) {
        List<Integer> dows = new ArrayList<Integer>(dayOfWeeks.size());
        Iterator<DayOfWeek> iter = dayOfWeeks.iterator();
        while (iter.hasNext()) {
            dows.add(iter.next().getDayOfWeek());
        }
        return dows;
    }
    
    private boolean hasSelectedAtLeastOneDay(EbpRatePermissionEditForm form) {
        if (Boolean.valueOf(form.getSunday()) || Boolean.valueOf(form.getMonday()) || Boolean.valueOf(form.getTuesday())
            || Boolean.valueOf(form.getWednesday()) || Boolean.valueOf(form.getThursday()) || Boolean.valueOf(form.getFriday())
            || Boolean.valueOf(form.getSaturday())) {
            return true;
        }
        return false;
    }
    
    public void validateWarningPeriod(WebSecurityForm<EbpRatePermissionEditForm> command, Errors errors) {
        WebSecurityForm<EbpRatePermissionEditForm> webSecurityForm = super.prepareSecurityForm(command, errors, "warningPeriod");
        if (webSecurityForm == null) {
            return;
        }
        
        EbpRatePermissionEditForm form = webSecurityForm.getWrappedObject();
        
        super.validateIntegerLimits(webSecurityForm, "warningPeriod", "label.settings.locations.extendByPhone.warningPeriod",
                                    form.getWarningPeriod(), WebCoreConstants.SMS_WARNING_PERIOD_MIN, WebCoreConstants.SMS_WARNING_PERIOD_MAX);
        
    }
    
    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }
}
