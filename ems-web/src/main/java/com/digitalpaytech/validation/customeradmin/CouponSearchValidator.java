package com.digitalpaytech.validation.customeradmin;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.mvc.customeradmin.support.CouponSearchForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.validation.BaseValidator;

@Component("CouponSearchValidator")
public class CouponSearchValidator extends BaseValidator<CouponSearchForm> {
    @Override
    public void validate(WebSecurityForm<CouponSearchForm> webSecurityForm, Errors errors) {
        webSecurityForm.removeAllErrorStatus();
        
        CouponSearchForm form = (CouponSearchForm) webSecurityForm.getWrappedObject();
        super.validateStringLength(webSecurityForm, "filterValue", "label.name", form.getFilterName(), 0, WebCoreConstants.VALIDATION_MAX_LENGTH_25);
        if(form.isQuickAdd()) {
        	super.validateCouponWithDiscountSearchText(webSecurityForm, "filterValue", "label.name", form.getFilterName());
        }
        else {
        	super.validateCouponSearchText(webSecurityForm, "filterValue", "label.name", form.getFilterName());
        }
        
        super.validateDiscountText(webSecurityForm, "filterDiscount", "label.coupon.filterDiscount", form.getDiscount());
        
        if (form.getFilterName() != null) {
            form.setFilterName(form.getFilterName().trim());
            if (form.getFilterName().length() <= 0) {
                form.setFilterName((String) null);
            }
        }
    }
    
}
