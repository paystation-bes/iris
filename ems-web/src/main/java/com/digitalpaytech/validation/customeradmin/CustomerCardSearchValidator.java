package com.digitalpaytech.validation.customeradmin;


import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.mvc.customeradmin.support.CustomerCardSearchForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.validation.BaseValidator;

@Component("customerCardSearchValidator")
public class CustomerCardSearchValidator extends BaseValidator<CustomerCardSearchForm> {
	@Override
	public void validate(WebSecurityForm<CustomerCardSearchForm> target, Errors errors) {
		WebSecurityForm<CustomerCardSearchForm> form = prepareSecurityForm(target, errors, "postToken", false);
		if(form != null) {
			CustomerCardSearchForm cardSearchForm = (CustomerCardSearchForm) form.getWrappedObject();
			this.validateStandardText(form, "filterValue", "label.customerCard.filterValue", cardSearchForm.getFilterValue());
		}
	}

}
