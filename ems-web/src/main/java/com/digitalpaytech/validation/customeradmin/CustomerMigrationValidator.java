package com.digitalpaytech.validation.customeradmin;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.mvc.customeradmin.support.CustomerMigrationForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.LabelConstants;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.validation.BaseValidator;

@Component("customerMigrationValidator")
public class CustomerMigrationValidator extends BaseValidator<CustomerMigrationForm> {
    
    public final void validate(final WebSecurityForm<CustomerMigrationForm> command, final Errors errors) {
        final WebSecurityForm<CustomerMigrationForm> webSecurityForm = prepareSecurityForm(command, errors, WebCoreConstants.POSTTOKEN_STRING);
        if (webSecurityForm == null) {
            return;
        }
        
        validateEmailAddresses(webSecurityForm);
        validateScheduledTime(webSecurityForm);
    }
    
    private void validateEmailAddresses(final WebSecurityForm<CustomerMigrationForm> webSecurityForm) {
        final CustomerMigrationForm form = webSecurityForm.getWrappedObject();
        final List<String> emailList = new ArrayList<String>();
        validateRequired(webSecurityForm, FormFieldConstants.MIGRATION_CONTACTS, LabelConstants.LABEL_EMAIL, form.getMigrationContacts());
        final String[] splitEmails = form.getMigrationContacts().get(0).split(StandardConstants.STRING_COMMA);
        for (String email : splitEmails) {
            validateRequired(webSecurityForm, FormFieldConstants.MIGRATION_CONTACTS, LabelConstants.LABEL_EMAIL, email);
            validateEmailAddress(webSecurityForm, FormFieldConstants.MIGRATION_CONTACTS, LabelConstants.LABEL_EMAIL, email);
            emailList.add(email.toLowerCase(WebCoreConstants.DEFAULT_LOCALE));
        }
        form.setMigrationContacts(emailList);
    }
    
    private void validateScheduledTime(final WebSecurityForm<CustomerMigrationForm> webSecurityForm) {
        
        final CustomerMigrationForm form = webSecurityForm.getWrappedObject();
        final TimeZone timeZone = TimeZone.getTimeZone(WebCoreConstants.SYSTEM_ADMIN_UI_TIMEZONE);
        
        final Calendar defaultDate = Calendar.getInstance();
        defaultDate.setTimeZone(timeZone);
        defaultDate.set(Calendar.HOUR_OF_DAY, 0);
        defaultDate.set(Calendar.MINUTE, 0);
        defaultDate.set(Calendar.SECOND, 0);
        defaultDate.set(Calendar.MILLISECOND, 0);
        defaultDate.add(Calendar.DAY_OF_YEAR, StandardConstants.CONSTANT_4);
        if (defaultDate.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
            defaultDate.add(Calendar.DAY_OF_YEAR, StandardConstants.CONSTANT_2);
        } else if (defaultDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            defaultDate.add(Calendar.DAY_OF_YEAR, StandardConstants.CONSTANT_1);
        }
        
        Date scheduleRealDate = null;
        if (form.getScheduleDate() == null) {
            scheduleRealDate = defaultDate.getTime();
        } else {
            scheduleRealDate = validateDate(webSecurityForm, FormFieldConstants.SCHEDULE_DATE, LabelConstants.LABEL_SCHEDULE_DATE,
                                            form.getScheduleDate(), timeZone);
        }
        final Calendar startDate = (Calendar) defaultDate.clone();
        final Calendar endDate = (Calendar) defaultDate.clone();
        endDate.add(Calendar.DAY_OF_YEAR, StandardConstants.DAYS_IN_A_WEEK * StandardConstants.CONSTANT_4);
        endDate.add(Calendar.HOUR_OF_DAY, StandardConstants.LAST_HOUR_OF_THE_DAY);
        validateDateLimits(webSecurityForm, FormFieldConstants.SCHEDULE_DATE, LabelConstants.LABEL_SCHEDULE_DATE, scheduleRealDate,
                           startDate.getTime(), endDate.getTime(), DateUtil.createUIDateOnlyString(startDate.getTime(), timeZone),
                           DateUtil.createUIDateOnlyString(endDate.getTime(), timeZone));
        form.setScheduleRealDate(scheduleRealDate);
    }
}
