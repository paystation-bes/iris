package com.digitalpaytech.validation.customeradmin;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.client.dto.fms.BulkSchedule;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.validation.BaseValidator;

@Component("bulkScheduleValidator")
public class BulkScheduleValidator extends BaseValidator<BulkSchedule> {
    
    @Override
    public final void validate(final WebSecurityForm<BulkSchedule> target, final Errors errors) {
        final WebSecurityForm<BulkSchedule> form = prepareSecurityForm(target, errors, WebCoreConstants.POSTTOKEN_STRING, false);
        if (form == null) {
            return;
        }
    }
    
}