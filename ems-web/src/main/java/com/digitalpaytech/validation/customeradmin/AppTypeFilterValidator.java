package com.digitalpaytech.validation.customeradmin;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.mvc.customeradmin.support.AppTypeFilterForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.validation.BaseValidator;

@Component("appTypeFilterValidator")
public class AppTypeFilterValidator extends BaseValidator<AppTypeFilterForm>{

    @Override
    public void validate(WebSecurityForm<AppTypeFilterForm> command, Errors errors) {
        super.prepareSecurityForm(command, errors, "", false);
    }
}
