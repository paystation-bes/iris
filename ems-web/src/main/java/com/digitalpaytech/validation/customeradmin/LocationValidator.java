package com.digitalpaytech.validation.customeradmin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.CaseLocationLot;
import com.digitalpaytech.domain.FlexLocationFacility;
import com.digitalpaytech.domain.FlexLocationProperty;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.mvc.customeradmin.support.LocationEditForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.BaseValidator;

@Component("locationValidator")
public class LocationValidator extends BaseValidator<LocationEditForm> {
    
    @Autowired
    private LocationService locationService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private CustomerService customerService;
    
    @Override
    public void validate(final WebSecurityForm<LocationEditForm> command, final Errors errors) {
        
    }
    
    public void validate(final WebSecurityForm<LocationEditForm> command, final Errors errors, final boolean isMigrated, final int customerId) {
        
        final WebSecurityForm<LocationEditForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
        final RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        
        final WebUser user = (WebUser) WebSecurityUtil.getAuthentication().getPrincipal();
        
        if (!isMigrated) {
            checkNotMigrated(webSecurityForm, keyMapping, user.isSystemAdmin());
        } else {
            
            final LocationEditForm form = webSecurityForm.getWrappedObject();
            if (form.getSelectedPayStationRandomIds() != null && !form.getSelectedPayStationRandomIds().isEmpty()) {
                form.setSelectedPayStationRandomIds(Arrays.asList(form.getSelectedPayStationRandomIds().get(0).split(StandardConstants.STRING_COMMA)));
            }
            if (form.getSelectedFacilityRandomIds() != null && !form.getSelectedFacilityRandomIds().isEmpty()
                && form.getSelectedFacilityRandomIds().get(0).contains(StandardConstants.STRING_COMMA)) {
                form.setSelectedFacilityRandomIds(Arrays.asList(form.getSelectedFacilityRandomIds().get(0).split(StandardConstants.STRING_COMMA)));
            }
            if (form.getSelectedPropertyRandomIds() != null && !form.getSelectedPropertyRandomIds().isEmpty()
                && form.getSelectedPropertyRandomIds().get(0).contains(StandardConstants.STRING_COMMA)) {
                form.setSelectedPropertyRandomIds(Arrays.asList(form.getSelectedPropertyRandomIds().get(0).split(StandardConstants.STRING_COMMA)));
            }
            if (form.getSelectedLotRandomIds() != null && !form.getSelectedLotRandomIds().isEmpty()
                && form.getSelectedLotRandomIds().get(0).contains(StandardConstants.STRING_COMMA)) {
                form.setSelectedLotRandomIds(Arrays.asList(form.getSelectedLotRandomIds().get(0).split(StandardConstants.STRING_COMMA)));
            }
            
            if (!form.isIsUnassigned()) {
                
                if (form.getSelectedChildLocationRandomIds() != null && !form.getSelectedChildLocationRandomIds().isEmpty()) {
                    form.setSelectedChildLocationRandomIds(Arrays.asList(form.getSelectedChildLocationRandomIds().get(0)
                            .split(StandardConstants.STRING_COMMA)));
                }
                
                final String parentLocationRandomId = form.getParentLocationId();
                if (!form.isParent() && !StringUtils.isEmpty(parentLocationRandomId)
                    && !parentLocationRandomId.equals(StandardConstants.STRING_MINUS_ONE) && !"---".equals(parentLocationRandomId)
                    && !"null".equals(parentLocationRandomId)) {
                    form.setParentLocationRealId(super.validateRandomId(webSecurityForm, "parentLocationId",
                                                                        "label.settings.locations.parentLocation", parentLocationRandomId,
                                                                        keyMapping, Location.class, Integer.class));
                } else {
                    form.setParentLocationRealId(0);
                }
                
                checkId(webSecurityForm, keyMapping);
                checkName(webSecurityForm, customerId);
                checkDescription(webSecurityForm);
                checkLocation(webSecurityForm, keyMapping);
                
                if (!form.isParent()) {
                    super.validateRequired(webSecurityForm, "permitIssueType", "label.settings.rates.operatingMode", form.getPermitIssueTypeId(), -1);
                }
                
                if ((form.getPermitIssueTypeId() == null) || (form.getPermitIssueTypeId() < 0)) {
                    form.setPermitIssueTypeId(0);
                }
            } else {
                /*
                 * If operation is happening in Unassigned location, need to verify and set Location id to
                 * LocationEditForm. Then in LocationDetailsController, saveLocation method, the logic will be
                 * able to update PointOfSale and Location database tables.
                 */
                checkId(webSecurityForm, keyMapping);
                checkLocation(webSecurityForm, keyMapping);
            }
        }
    }
    
    private void checkNotMigrated(final WebSecurityForm<LocationEditForm> webSecurityForm, final RandomKeyMapping keyMapping,
        final boolean isSystemAdmin) {
        final String systemAdminLabelAdd = isSystemAdmin ? ".systemAdmin" : "";
        if (webSecurityForm.getActionFlag() != WebSecurityConstants.ACTION_UPDATE) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("actionFlag", this.getMessage("error.customer.notMigrated" + systemAdminLabelAdd)));
            return;
        }
        
        checkId(webSecurityForm, keyMapping);
        
        final LocationEditForm form = webSecurityForm.getWrappedObject();
        final Location existingLocation = this.locationService.findLocationById(form.getLocationId());
        
        form.setIsParent(existingLocation.getIsParent());
        form.setName(existingLocation.getName());
        form.setDescription(existingLocation.getDescription());
        form.setParentLocationRealId(existingLocation.getLocation() != null ? existingLocation.getLocation().getId() : 0);
        
        if (StringUtils.isBlank(form.getCapacity())) {
            //we are allowing an empty capacity, will default to 0
            form.setCapacity(WebCoreConstants.ZERO);
            //webSecurityForm.addErrorStatus(new FormErrorStatus("locationCapacity","error.common.required"));      
        }
        if (StringUtils.isBlank(form.getTargetMonthlyRevenue())) {
            //we are allowing an empty target revenue, will default to 0
            form.setTargetMonthlyRevenue(WebCoreConstants.ZERO);
            //webSecurityForm.addErrorStatus(new FormErrorStatus("locationTargetMonthlyRevenue","error.common.required"));
        }
        
        checkOpenClose(form, webSecurityForm);
    }
    
    private void checkId(final WebSecurityForm<LocationEditForm> webSecurityForm, final RandomKeyMapping keyMapping) {
        
        final LocationEditForm form = webSecurityForm.getWrappedObject();
        
        final Integer id = super.validateRandomId(webSecurityForm, "randomId", "label.location", form.getRandomId(), keyMapping, Location.class,
                                                  Integer.class, WebCoreConstants.EMPTY_STRING);
        if (id != null) {
            form.setLocationId(id.intValue());
        } else {
            form.setLocationId(0);
        }
    }
    
    private void checkLocation(final WebSecurityForm<LocationEditForm> webSecurityForm, final RandomKeyMapping keyMapping) {
        final LocationEditForm form = webSecurityForm.getWrappedObject();
        if (form.isParent()) {
            verifyLocationAsParent(webSecurityForm, keyMapping);
        } else {
            verifyLocationAsChild(webSecurityForm, keyMapping);
            checkOpenClose(form, webSecurityForm);
        }
    }
    
    private void checkOpenClose(final LocationEditForm form, final WebSecurityForm<LocationEditForm> webSecurityForm) {
        
        final List<Integer> locationOpen = form.getLocationOpen();
        final List<Integer> locationClose = form.getLocationClose();
        final List<Boolean> openEntireDay = form.getOpenEntireDay();
        final List<Boolean> closedEntireDay = form.getClosedEntireDay();
        boolean locationOpenListOK = true;
        boolean locationCloseListOK = true;
        boolean openEntireDayListOK = true;
        boolean closedEntireDayListOK = true;
        final StringBuilder bdr = new StringBuilder();
        
        // don't need to check for values (sometimes we don't need them like when the meter is open or closed 24 hours)
        if (super.isListNullorEmpty(locationOpen) || locationOpen.size() != 7) {
            locationOpenListOK = false;
            bdr.append(this.getMessage("label.settings.locations.operatingHours")).append(" ")
                    .append(this.getMessage("label.settings.locations.operatingHours.open"));
            webSecurityForm.addErrorStatus(new FormErrorStatus("locationOpen", this.getMessage("error.common.required", bdr.toString())));
        }
        if (super.isListNullorEmpty(locationClose) || locationClose.size() != 7) {
            locationCloseListOK = false;
            bdr.replace(0, bdr.length(), "");
            bdr.append(this.getMessage("label.settings.locations.operatingHours")).append(" ")
                    .append(this.getMessage("label.settings.locations.operatingHours.close"));
            webSecurityForm.addErrorStatus(new FormErrorStatus("locationClose", this.getMessage("error.common.required", bdr.toString())));
        }
        if (super.isListNullorEmpty(openEntireDay) || openEntireDay.size() != 7) {
            openEntireDayListOK = false;
            webSecurityForm.addErrorStatus(new FormErrorStatus("openEntireDay", this.getMessage("error.common.required", this
                    .getMessage("label.settings.locations.operatingHours"))));
        }
        if (super.isListNullorEmpty(closedEntireDay) || closedEntireDay.size() != 7) {
            closedEntireDayListOK = false;
            webSecurityForm.addErrorStatus(new FormErrorStatus("closedEntireDay", this.getMessage("error.common.required", this
                    .getMessage("label.settings.locations.operatingHours"))));
        }
        
        if (locationOpenListOK && locationCloseListOK && openEntireDayListOK && closedEntireDayListOK) {
            int dayCount = 0;
            while (dayCount < 7) {
                if (!openEntireDay.get(dayCount) && !closedEntireDay.get(dayCount)) {
                    if ((locationOpen.get(dayCount) == null || locationOpen.get(dayCount) < 0)
                        && (locationClose.get(dayCount) == null || locationClose.get(dayCount) < 0)) {
                        // if nothing is set, the default is Open 24 hrs
                        locationOpen.set(dayCount, -1);
                        locationClose.set(dayCount, -1);
                        openEntireDay.set(dayCount, true);
                    } else {
                        if (locationOpen.get(dayCount) == null) {
                            if (locationClose.get(dayCount) != null) {
                                webSecurityForm.addErrorStatus(new FormErrorStatus("locationClose", this
                                        .getMessage("error.settings.locations.invalid.openTime")));
                            } else {
                                // if nothing is set, the default is Open 24 hrs
                                openEntireDay.set(dayCount, true);
                            }
                        }
                        if (locationClose.get(dayCount) == null) {
                            if (locationOpen.get(dayCount) != null) {
                                webSecurityForm.addErrorStatus(new FormErrorStatus("locationOpen", this
                                        .getMessage("error.settings.locations.invalid.closeTime")));
                            } else {
                                // if nothing is set, the default is Open 24 hrs
                                openEntireDay.set(dayCount, true);
                            }
                        }
                        if (locationOpen.get(dayCount) != null && locationClose.get(dayCount) != null) {
                            if (locationOpen.get(dayCount) < 0 || locationOpen.get(dayCount) > 95) {
                                webSecurityForm.addErrorStatus(new FormErrorStatus("locationOpen", this
                                        .getMessage("error.settings.locations.invalid.openTime")));
                            }
                            if (locationClose.get(dayCount) < 0 || locationClose.get(dayCount) > 191) {
                                webSecurityForm.addErrorStatus(new FormErrorStatus("locationClose", this
                                        .getMessage("error.settings.locations.invalid.closeTime")));
                            }
                            if (locationOpen.get(dayCount) > locationClose.get(dayCount)) {
                                webSecurityForm.addErrorStatus(new FormErrorStatus("locationOpen", this
                                        .getMessage("error.settings.locations.invalid.openTime")));
                                webSecurityForm.addErrorStatus(new FormErrorStatus("locationClose", this
                                        .getMessage("error.settings.locations.invalid.closeTime")));
                            }
                            if (locationClose.get(dayCount) > 95) {
                                if (locationOpen.get((dayCount + 1) % 7) != null
                                    && locationClose.get(dayCount) % 96 > locationOpen.get((dayCount + 1) % 7)) {
                                    webSecurityForm.addErrorStatus(new FormErrorStatus("locationOpen", this
                                            .getMessage("error.settings.locations.invalid.openTime")));
                                    webSecurityForm.addErrorStatus(new FormErrorStatus("locationClose", this
                                            .getMessage("error.settings.locations.invalid.closeTime")));
                                }
                            }
                        }
                    }
                } else if (openEntireDay.get(dayCount)) {
                    if (closedEntireDay.get(dayCount)) {
                        webSecurityForm.addErrorStatus(new FormErrorStatus("openEntireDay", this
                                .getMessage("error.settings.locations.invalid.openClose")));
                    }
                } else if (closedEntireDay.get(dayCount)) {
                    if (openEntireDay.get(dayCount)) {
                        webSecurityForm.addErrorStatus(new FormErrorStatus("openEntireDay", this
                                .getMessage("error.settings.locations.invalid.openClose")));
                    }
                }
                
                dayCount++;
            }
        }
        
    }
    
    private void verifyLocationAsChild(final WebSecurityForm<LocationEditForm> webSecurityForm, final RandomKeyMapping keyMapping) {
        
        final LocationEditForm form = webSecurityForm.getWrappedObject();
        if (super.listHasValues(form.getSelectedPayStationRandomIds())) {
            form.setPointOfSaleIdList(super.validateRandomIds(webSecurityForm, "payStations", "label.payStations",
                                                              form.getSelectedPayStationRandomIds(), keyMapping, PointOfSale.class, Integer.class));
        }
        
        if (super.listHasValues(form.getSelectedFacilityRandomIds())) {
            form.setSelectedFacilityIdList(super.validateRandomIds(webSecurityForm, "facilities", "label.facilities",
                                                                   form.getSelectedFacilityRandomIds(), keyMapping, FlexLocationFacility.class,
                                                                   Integer.class));
        }
        
        if (super.listHasValues(form.getSelectedPropertyRandomIds())) {
            form.setSelectedPropertyIdList(super.validateRandomIds(webSecurityForm, "properties", "label.properties",
                                                                   form.getSelectedPropertyRandomIds(), keyMapping, FlexLocationProperty.class,
                                                                   Integer.class));
        }
        
        if (super.listHasValues(form.getSelectedLotRandomIds())) {
            form.setSelectedLotIdList(super.validateRandomIds(webSecurityForm, "lots", "label.lots", form.getSelectedLotRandomIds(), keyMapping,
                                                              CaseLocationLot.class, Integer.class));
        }
        
        final List<String> childLocationRandomIdList = form.getSelectedChildLocationRandomIds();
        
        if (!(super.isListNullorEmpty(childLocationRandomIdList)) && !WebCoreConstants.EMPTY_STRING.equals(childLocationRandomIdList.get(0))) {
            webSecurityForm.addErrorStatus(new FormErrorStatus(FormFieldConstants.PARAMETER_IS_PARENT, this
                    .getMessage("error.settings.locations.child.invalid.isParent")));
        }
        
        form.setSelectedChildLocationRandomIds(new ArrayList<String>());
        
        if (StringUtils.isBlank(form.getCapacity())) {
            //we are allowing an empty capacity, will default to 0
            form.setCapacity(WebCoreConstants.ZERO);
            //webSecurityForm.addErrorStatus(new FormErrorStatus("locationCapacity","error.common.required"));      
        } else {
            super.validateIntegerLimits(webSecurityForm, "capacity", "label.capacity", form.getCapacity(), 0, WebCoreConstants.MAX_LOCATION_CAPACITY);
        }
        
        if (StringUtils.isBlank(form.getTargetMonthlyRevenue())) {
            //we are allowing an empty target revenue, will default to 0
            form.setTargetMonthlyRevenue(WebCoreConstants.ZERO);
            //webSecurityForm.addErrorStatus(new FormErrorStatus("locationTargetMonthlyRevenue","error.common.required"));
        } else {
            super.validateIntegerLimits(webSecurityForm, "targetMonthlyRevenue", "label.settings.locations.trgtMonthlyRevenue",
                                        form.getTargetMonthlyRevenue(), 0, WebCoreConstants.MAX_LOCATION_REVENUE);
        }
        
    }
    
    private void verifyLocationAsParent(final WebSecurityForm<LocationEditForm> webSecurityForm, final RandomKeyMapping keyMapping) {
        
        final LocationEditForm form = webSecurityForm.getWrappedObject();
        
        final List<String> childLocationRandomIdList = form.getSelectedChildLocationRandomIds();
        
        if (super.listHasValues(childLocationRandomIdList)) {
            form.setChildLocationIdList(super.validateRandomIds(webSecurityForm, "childLocations", "label.locations",
                                                                form.getSelectedChildLocationRandomIds(), keyMapping, Location.class, Integer.class));
        }
        
        final List<String> paystationRandomIsList = form.getSelectedPayStationRandomIds();
        
        if (!(super.isListNullorEmpty(paystationRandomIsList)) && !WebCoreConstants.EMPTY_STRING.equals(paystationRandomIsList.get(0))) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("isParent", this
                    .getMessage("error.settings.locations.parent.invalid.containsPayStations")));
        }
        
        final List<String> facilityRandomIdList = form.getSelectedFacilityRandomIds();
        
        if (!(super.isListNullorEmpty(facilityRandomIdList)) && !WebCoreConstants.EMPTY_STRING.equals(facilityRandomIdList.get(0))) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("isParent", this
                    .getMessage("error.settings.locations.parent.invalid.containsPayStations")));
        }
        
        if (form.getLocationId() != 0) {
            final Location parentLocation = this.locationService.findLocationById(form.getLocationId());
            if (parentLocation != null) {
                //location will be null for new parent locations
                if (this.locationService.locationContainsPermitsById(form.getLocationId())) {
                    webSecurityForm.addErrorStatus(new FormErrorStatus("isParent", this
                            .getMessage("error.settings.locations.parent.invalid.containsPermits")));
                }
            }
        }
        
    }
    
    private void checkName(final WebSecurityForm<LocationEditForm> webSecurityForm, final int customerId) {
        final LocationEditForm form = webSecurityForm.getWrappedObject();
        final String name = form.getName();
        final Object requiredCheck = super.validateRequired(webSecurityForm, "name", "label.name", name);
        if (requiredCheck != null) {
            super.validateStringLength(webSecurityForm, "name", "label.name", name, WebCoreConstants.VALIDATION_MIN_LENGTH_4,
                                       WebCoreConstants.VALIDATION_MAX_LENGTH_25);
            super.validateLocationText(webSecurityForm, "name", "label.name", name);
        } else {
            return;
        }
        if (name.toLowerCase().equals(WebCoreConstants.DEFAULT_LOCATION.toLowerCase())) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("name", this.getMessage("error.common.invalid", this.getMessage("label.name"))));
        }
        
        final Location location = this.locationService.findLocationByCustomerIdAndName(customerId, name);
        if (location != null) {
            if (location.getId().intValue() != form.getLocationId()) {
                webSecurityForm.addErrorStatus(new FormErrorStatus("name", this.getMessage("error.settings.locations.existing.name")));
            }
        }
        
        return;
    }
    
    private void checkDescription(final WebSecurityForm<LocationEditForm> webSecurityForm) {
        final LocationEditForm form = webSecurityForm.getWrappedObject();
        final String description = form.getDescription();
        if (description != null && !StringUtils.isBlank(description)) {
            form.setDescription(super.validateExternalDescription(webSecurityForm, "locationDescription", "label.description", description,
                                                                  WebCoreConstants.VALIDATION_MIN_LENGTH_1));
        }
        return;
    }
    
    /*
     * TODO: remove?
     * private boolean capacityMatchesChildren(Integer capacity, List<Location> childLocations) {
     * boolean result = false;
     * int childCapacity = 0;
     * for(Location child: childLocations){
     * childCapacity += child.getNumberOfSpaces();
     * }
     * if(capacity == childCapacity){
     * result = true;
     * }
     * return result;
     * }
     * private boolean targetMonthlyRevenueMatchesChildren(Integer targetRevenue, List<Location> childLocations) {
     * boolean result = false;
     * int childTarget = 0;
     * for(Location child: childLocations){
     * childTarget += child.getTargetMonthlyRevenue();
     * }
     * if(targetRevenue == childTarget){
     * result = true;
     * }
     * return result;
     * }
     */
    
}
