package com.digitalpaytech.validation.customeradmin;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.mvc.customeradmin.support.io.CustomerBadCardCsvReader;
import com.digitalpaytech.mvc.customeradmin.support.io.dto.CustomerBadCardCsvDTO;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.csv.CsvCommonValidator;
import com.digitalpaytech.util.csv.CsvProcessingError;

@Component("cardManagementCsvValidator")
public class CardManagementCsvValidator extends CsvCommonValidator<CustomerBadCardCsvDTO> {
	@Override
	public void validateRow(List<CsvProcessingError> errors,
			List<CsvProcessingError> warnings, CustomerBadCardCsvDTO dto,
			Map<String, Object> context) {
		@SuppressWarnings("unchecked")
		Map<String, CustomerCardType> cardTypesMap = (Map<String, CustomerCardType>) context.get(CustomerBadCardCsvReader.CTXKEY_CARD_TYPES);
		
		boolean furtherValidation = false;
        CustomerCardType ccType = null;
        if (validateRequired(errors, "cardType", "label.settings.cardSettings.CardType", dto.getCardType()) != null) {
        	ccType = cardTypesMap.get(dto.getCardType().toLowerCase());
        	if(ccType != null) {
        		furtherValidation = true;
        	}
        	else {
        		errors.add(new CsvProcessingError(this.getMessage("error.common.invalid",
        				this.getMessage("label.settings.cardSettings.CardType"))));
        	}
        }
        
        if (validateRequired(errors, "cardNumber", "label.settings.cardSettings.CardNumber", dto.getCardNumber()) == null) {
            furtherValidation = false;
        }
        
        if (furtherValidation) {
            Integer cardTypeId = null;
            if(ccType.getCardType() != null) {
            	cardTypeId = ccType.getCardType().getId(); //If null, it is value card
            }
            
            if ((cardTypeId == null) || (cardTypeId == CardProcessingConstants.CARD_TYPE_VALUE_CARD)) {
                validateNumberStringLength(errors, "cardNumber", "label.settings.cardSettings.CardNumber", dto.getCardNumber(), CardProcessingConstants.VALUE_CARD_MIN_LENGTH, CardProcessingConstants.BAD_VALUE_CARD_MAX_LENGTH);
                validateNumber(errors, "cardNumber", "label.settings.cardSettings.CardNumber", dto.getCardNumber());
            }
            else if(cardTypeId == CardProcessingConstants.CARD_TYPE_SMART_CARD) {
                validateNumberStringLength(errors, "cardNumber", "label.settings.cardSettings.CardNumber", dto.getCardNumber(), CardProcessingConstants.SMART_CARD_MIN_LENGTH, CardProcessingConstants.BAD_SMART_CARD_MAX_LENGTH);
                validateNumber(errors, "cardNumber", "label.settings.cardSettings.CardNumber", dto.getCardNumber());
            }
            else {
            	if (dto.getCardNumber().toLowerCase().contains("x")) {
                    validateNumber(errors, "cardNumber", "label.settings.cardSettings.CardNumber", dto.getCardNumber().substring(dto.getCardNumber().length() - 4));
                } else {
                    validateNumberStringLength(errors, "cardNumber", "label.settings.cardSettings.CardNumber", dto.getCardNumber(), CardProcessingConstants.CREDIT_CARD_MIN_LENGTH,
                                             CardProcessingConstants.CREDIT_CARD_MAX_LENGTH);
                    validateNumber(errors, "cardNumber", "label.settings.cardSettings.CardNumber", dto.getCardNumber());
                }
                
                if (validateRequired(errors, "cardExpiry", "label.settings.cardSettings.CardExpiry", dto.getExpiryDate()) != null) {
                    validateStringLength(errors, "cardExpiry", "label.settings.cardSettings.CardExpiry", dto.getExpiryDate(), 4);
                    validateNumber(errors, "cardExpiry", "label.settings.cardSettings.CardExpiry", dto.getExpiryDate());
                }
            }
        }
        
        validateExternalDescription(errors, "comment", "label.cardManagement.bannedCard.comment", dto.getComment(), WebCoreConstants.VALIDATION_MIN_LENGTH_1);
	}
}
