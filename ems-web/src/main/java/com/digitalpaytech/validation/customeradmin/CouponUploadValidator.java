package com.digitalpaytech.validation.customeradmin;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.mvc.customeradmin.support.CouponUploadForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.validation.BaseValidator;

@Component("couponUploadValidator")
public class CouponUploadValidator extends BaseValidator<CouponUploadForm> {
    @Override
    public final void validate(final WebSecurityForm<CouponUploadForm> target, final Errors errors) {
        final WebSecurityForm<CouponUploadForm> form = prepareSecurityForm(target, errors, WebCoreConstants.POSTTOKEN_STRING);
        if (form != null) {
            final CouponUploadForm uploadForm = form.getWrappedObject();
            validateRequired(form, FormFieldConstants.FILE, "label.coupon.importForm.sourceFile", uploadForm.getFile().getOriginalFilename());
            validateRequired(form, FormFieldConstants.IMPORT_MODE, FormFieldConstants.LABEL_IMPORT_MODE, uploadForm.getCreateUpdateOrDelete());
        }
    }
}
