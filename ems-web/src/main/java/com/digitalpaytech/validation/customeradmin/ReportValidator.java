package com.digitalpaytech.validation.customeradmin;

import static com.digitalpaytech.util.ReportingConstants.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.mvc.customeradmin.support.ReportEditForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.PaystationSettingService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.ReportingUtil;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.BaseValidator;

@Component("reportValidator")
public class ReportValidator extends BaseValidator<ReportEditForm> {
    private static final int REPORT_TITLE_LENGTH = 80;
    private static final int LICENSE_PLATE_LENGTH = 10;
    private static final int COUPON_NUMBER_LENGTH = 10;
    
    private static final int MAX_TICKET_NUMBER = 99999999;
    
    private static final String ID_ALL = "0";
    
    private static final String DATE_TIME_SEPARATOR = " : ";
    private static final String FMT_UI_DATE = WebCoreConstants.DATE_UI_FORMAT;
    private static final String FMT_UI_TIME = "HH:mm";
    private static final String FMT_UI_DATE_TIME = FMT_UI_DATE + DATE_TIME_SEPARATOR + FMT_UI_TIME;
    
    private static final String ATTR_PRIMARY_DATE_BEGIN = "primaryDateBegin";
    private static final String ATTR_PRIMARY_TIME_BEGIN = "primaryTimeBegin";
    private static final String ATTR_PRIMARY_MONTH_BEGIN = "primaryMonthBegin";
    private static final String ATTR_PRIMARY_YEAR_BEGIN = "primaryYearBegin";
    
    private static final String ATTR_PRIMARY_DATE_END = "primaryDateEnd";
    private static final String ATTR_PRIMARY_TIME_END = "primaryTimeEnd";
    private static final String ATTR_PRIMARY_MONTH_END = "primaryMonthEnd";
    private static final String ATTR_PRIMARY_YEAR_END = "primaryYearEnd";
    
    private static final String ATTR_SECONDARY_DATE_BEGIN = "secondaryDateBegin";
    private static final String ATTR_SECONDARY_TIME_BEGIN = "secondaryTimeBegin";
    private static final String ATTR_SECONDARY_MONTH_BEGIN = "secondaryMonthBegin";
    private static final String ATTR_SECONDARY_YEAR_BEGIN = "secondaryYearBegin";
    
    private static final String ATTR_SECONDARY_DATE_END = "secondaryDateEnd";
    private static final String ATTR_SECONDARY_TIME_END = "secondaryTimeEnd";
    private static final String ATTR_SECONDARY_MONTH_END = "secondaryMonthEnd";
    private static final String ATTR_SECONDARY_YEAR_END = "secondaryYearEnd";
    
    private static final String ATTR_TITLE = "reportTitle";
    private static final String ATTR_START_DATE = "startDate";
    private static final String ATTR_EMAIL = "reportEmail";
    private static final String ATTR_COUPON_NUMBER = "couponNumber";
    private static final String ATTR_ORG_TREE = "reportOrgTree";
    private static final String ATTR_REPEAT_TYPE = "repeatType";
    private static final String ATTR_REPEAT_FREQUENCY = "repeatFrequency";
    private static final String ATTR_REPEAT_ON = "repeatOn";
    private static final String ATTR_LICENSE_PLATE = "licensePlate";
    private static final String ATTR_LOCATION = "location";
    private static final String ATTR_LOCATION_PAYSTATION = "locationPOSListFilters";
    private static final String ATTR_ROUTE = "routeListFilters";
    private static final String ATTR_PAYSTATION = "payStationListFilters";
    private static final String ATTR_CARD_NUMBER = "cardNumberSpecific";
    private static final String ATTR_TICKET_BEGIN = "ticketNumberBegin";
    private static final String ATTR_TICKET_END = "ticketNumberEnd";
    private static final String ATTR_SPACE_BEGIN = "spaceNumberBegin";
    private static final String ATTR_SPACE_END = "spaceNumberEnd";
    
    private static final String LABEL_FORM_TXTIME = "label.reporting.form.filter.transactionTime";
    private static final String LABEL_FORM_TICKETNUMBER = "label.reporting.form.filter.parker.ticketNumber";
    private static final String LABEL_FORM_CARD_REFUND_TIME = "label.reporting.form.filter.cardRefundTime";
    private static final String LABEL_FORM_CARD_PROCESSING_TIME = "label.reporting.form.filter.cardProcessingTime";
    private static final String LABEL_FORM_CARD_RETRY_TIME = "label.reporting.form.filter.cardRetryTime";
    private static final String LABEL_FORM_AUDIT_BEGINT_TIME = "label.reporting.form.filter.auditBeginTime";
    private static final String LABEL_FORM_AUDIT_END_TIME = "label.reporting.form.filter.auditEndTime";
    private static final String LABEL_FORM_REPORT_NUMBER = "label.reporting.form.filter.parker.reportNumber";
    private static final String LABEL_FORM_REPLENISH_TIME = "label.reporting.form.filter.replenishTime";
    private static final String LABEL_FORM_TAX_TIME = "label.reporting.form.filter.taxTime";
    
    private static final String LABEL_FORM_TITLE = "label.reporting.form.reportTitle";
    private static final String LABEL_FORM_SPACE_NUMBER = "label.reporting.form.filter.parker.spaceNumber";
    private static final String LABEL_FORM_SCHEDULE_REPEAT_TYPE = "label.reporting.form.schedule.repeatType";
    private static final String LABEL_FORM_SCHEDULE_START_TIME = "label.reporting.form.schedule.startTime";
    private static final String LABEL_FORM_SCHEDULE_REPEAT_FREQUENCY = "label.reporting.form.schedule.repeatFrequency";
    private static final String LABEL_FORM_SCHEDULE_EMAIL = "label.reporting.form.schedule.email";
    private static final String LABEL_FORM_COUPON_NUMBER = "label.reporting.form.filter.parker.couponNumber";
    private static final String LABEL_FORM_CARD_NUMBER = "label.reporting.form.filter.card.number";
    private static final String LABEL_FORM_LICENSE_PLATE = "label.reporting.form.filter.parker.licensePlate";
    
    private static final String LABEL_FORM_LOCATION_TYPE = "label.reporting.form.filter.location.locationType";
    private static final String LABEL_FORM_LOCATION = "label.reporting.form.filter.location";
    
    private static final String LABEL_LOCATION_AND_PAYSTATION_FULL = "label.reporting.form.filter.location.locationAndPayStation.full";
    private static final String LABEL_ROUTE_FULL = "label.reporting.form.filter.location.route.full";
    private static final String LABEL_PAYSTATION_FULL = "label.reporting.form.filter.location.paystation.full";
    
    private static final String PAR_LABEL_DATE_BEGIN = ".date.begin";
    private static final String PAR_LABEL_TIME_BEGIN = ".time.begin";
    private static final String PAR_LABEL_YEAR_BEGIN = ".year.begin";
    private static final String PAR_LABEL_MONTH_BEGIN = ".month.begin";
    
    private static final String PAR_LABEL_DATE_END = ".date.end";
    private static final String PAR_LABEL_TIME_END = ".time.end";
    private static final String PAR_LABEL_YEAR_END = ".year.end";
    private static final String PAR_LABEL_MONTH_END = ".month.end";
    
    private static final ReportSpecificLabel[] LABEL = new ReportSpecificLabel[] { null,
        // txAll
        new ReportSpecificLabel(LABEL_FORM_TXTIME, WebCoreConstants.EMPTY_STRING, LABEL_FORM_TICKETNUMBER, LABEL_FORM_TICKETNUMBER),
        // txCash
        new ReportSpecificLabel(LABEL_FORM_TXTIME, WebCoreConstants.EMPTY_STRING, LABEL_FORM_TICKETNUMBER, LABEL_FORM_TICKETNUMBER),
        // txCC
        new ReportSpecificLabel(LABEL_FORM_TXTIME, WebCoreConstants.EMPTY_STRING, LABEL_FORM_TICKETNUMBER, LABEL_FORM_TICKETNUMBER),
        // txCCRefund
        new ReportSpecificLabel(LABEL_FORM_CARD_REFUND_TIME, WebCoreConstants.EMPTY_STRING, LABEL_FORM_TICKETNUMBER, LABEL_FORM_TICKETNUMBER),
        // txCardProcessing
        new ReportSpecificLabel(LABEL_FORM_CARD_PROCESSING_TIME, LABEL_FORM_TXTIME, LABEL_FORM_TICKETNUMBER, LABEL_FORM_TICKETNUMBER),
        // txCardRetry
        new ReportSpecificLabel(LABEL_FORM_CARD_RETRY_TIME, LABEL_FORM_TXTIME, LABEL_FORM_TICKETNUMBER, LABEL_FORM_TICKETNUMBER),
        // txPatrollerCard
        new ReportSpecificLabel(LABEL_FORM_TXTIME, WebCoreConstants.EMPTY_STRING, LABEL_FORM_TICKETNUMBER, LABEL_FORM_TICKETNUMBER),
        // txRate
        new ReportSpecificLabel(LABEL_FORM_TXTIME, WebCoreConstants.EMPTY_STRING, WebCoreConstants.EMPTY_STRING, WebCoreConstants.EMPTY_STRING),
        // txRateSummary
        new ReportSpecificLabel(LABEL_FORM_TXTIME, WebCoreConstants.EMPTY_STRING, WebCoreConstants.EMPTY_STRING, WebCoreConstants.EMPTY_STRING),
        // txSmartCard
        new ReportSpecificLabel(LABEL_FORM_TXTIME, WebCoreConstants.EMPTY_STRING, LABEL_FORM_TICKETNUMBER, LABEL_FORM_TICKETNUMBER),
        // txValueCard
        new ReportSpecificLabel(LABEL_FORM_TXTIME, WebCoreConstants.EMPTY_STRING, LABEL_FORM_TICKETNUMBER, LABEL_FORM_TICKETNUMBER),
        // txStallReports
        new ReportSpecificLabel(WebCoreConstants.EMPTY_STRING, WebCoreConstants.EMPTY_STRING, WebCoreConstants.EMPTY_STRING,
                WebCoreConstants.EMPTY_STRING),
        // couponUsageSummary
        new ReportSpecificLabel(LABEL_FORM_TXTIME, WebCoreConstants.EMPTY_STRING, WebCoreConstants.EMPTY_STRING, WebCoreConstants.EMPTY_STRING),
        // collectionReports
        new ReportSpecificLabel(LABEL_FORM_AUDIT_BEGINT_TIME, LABEL_FORM_AUDIT_END_TIME, LABEL_FORM_REPORT_NUMBER, LABEL_FORM_REPORT_NUMBER),
        // replenishReports
        new ReportSpecificLabel(LABEL_FORM_REPLENISH_TIME, WebCoreConstants.EMPTY_STRING, LABEL_FORM_REPORT_NUMBER, LABEL_FORM_REPORT_NUMBER),
        // payStationSummary
        new ReportSpecificLabel(WebCoreConstants.EMPTY_STRING, WebCoreConstants.EMPTY_STRING, WebCoreConstants.EMPTY_STRING,
                WebCoreConstants.EMPTY_STRING),
        // taxesReport
        new ReportSpecificLabel(LABEL_FORM_TAX_TIME, WebCoreConstants.EMPTY_STRING, WebCoreConstants.EMPTY_STRING, WebCoreConstants.EMPTY_STRING),
        // txDelay
        new ReportSpecificLabel(LABEL_FORM_TXTIME, WebCoreConstants.EMPTY_STRING, LABEL_FORM_TICKETNUMBER, LABEL_FORM_TICKETNUMBER),
        // inventory
        new ReportSpecificLabel(WebCoreConstants.EMPTY_STRING, WebCoreConstants.EMPTY_STRING, WebCoreConstants.EMPTY_STRING,
                WebCoreConstants.EMPTY_STRING),
        // paystationInventory(sysadmin version)
        new ReportSpecificLabel(WebCoreConstants.EMPTY_STRING, WebCoreConstants.EMPTY_STRING, WebCoreConstants.EMPTY_STRING,
                WebCoreConstants.EMPTY_STRING),
        // maintenanceRequired
        new ReportSpecificLabel(WebCoreConstants.EMPTY_STRING, WebCoreConstants.EMPTY_STRING, WebCoreConstants.EMPTY_STRING,
                WebCoreConstants.EMPTY_STRING),
        // collectionRequired
        new ReportSpecificLabel(WebCoreConstants.EMPTY_STRING, WebCoreConstants.EMPTY_STRING, WebCoreConstants.EMPTY_STRING,
                WebCoreConstants.EMPTY_STRING), };
    
    @Autowired
    private LocationService locationService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private RouteService routeService;
    
    @Autowired
    private PaystationSettingService paystationSettingService;
    
    @Autowired
    private ReportingUtil reportingUtil;
    
    public final void setLocationService(final LocationService locationService) {
        this.locationService = locationService;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final void setRouteService(final RouteService routeService) {
        this.routeService = routeService;
    }
    
    public final void setPaystationSettingService(final PaystationSettingService paystationSettingService) {
        this.paystationSettingService = paystationSettingService;
    }
    
    public final void setReportingUtil(final ReportingUtil reportingUtil) {
        this.reportingUtil = reportingUtil;
    }
    
    private List<String> parseCommaSeparatedString(final String str) {
        List<String> result = null;
        if ((str != null) && (str.length() > 0)) {
            final String[] buffer = str.trim().split(",");
            if (buffer.length > 0) {
                buffer[0] = buffer[0].trim();
                if (buffer[0].length() > 0) {
                    result = new ArrayList<String>(buffer.length);
                    for (int i = -1; ++i < buffer.length;) {
                        result.add(buffer[i].trim());
                    }
                }
            }
        }
        
        return result;
    }
    
    // Suppressed so that unit test can override this method.
    @SuppressWarnings({ "checkstyle:designforextension" })
    @Override
    public void validate(final WebSecurityForm<ReportEditForm> command, final Errors errors) {
        
        final WebSecurityForm<ReportEditForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
        final RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        final TimeZone timeZone = TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone());
        
        final ReportEditForm form = webSecurityForm.getWrappedObject();
        //        String reportTitle = form.getReportTitle();
        //        String licensePlate = form.getLicencePlate();
        //        String couponNumber = form.getCouponNumberSpecific();                
        
        if (form.getCustomerRandomId() != null) {
            form.setCustomerId(super.validateRandomId(webSecurityForm, "customerRandomId", "label.customer", form.getCustomerRandomId(), keyMapping,
                                                      Customer.class, Integer.class));
        }
        
        if ((form.getSelectedOrgRandomIds() != null) && (!form.getSelectedOrgRandomIds().isEmpty())) {
            form.setSelectedOrgRandomIds(parseCommaSeparatedString(form.getSelectedOrgRandomIds().get(0)));
        }
        
        if ((form.getSelectedLocationRandomIds() != null) && (!form.getSelectedLocationRandomIds().isEmpty())) {
            form.setSelectedLocationRandomIds(parseCommaSeparatedString(form.getSelectedLocationRandomIds().get(0)));
        }
        
        if (form.getRepeatOn() != null && !form.getRepeatOn().isEmpty()) {
            form.setRepeatOn(parseCommaSeparatedString(form.getRepeatOn().get(0)));
        }
        
        if (form.getEmailList() != null && !form.getEmailList().isEmpty()) {
            form.setEmailList(parseCommaSeparatedString(form.getEmailList().get(0)));
        }
        
        this.checkFilters(keyMapping, webSecurityForm, errors);
        
        //        checkLists(keyMapping, form, errors, webSecurityForm);
        
        if (form.getIsScheduled()) {
            //            Integer reportRepeatType = form.getRepeatType();
            //          checkTitle(reportTitle, reportRepeatType, errors, webSecurityForm);
            validateRequired(webSecurityForm, ATTR_TITLE, LABEL_FORM_TITLE, form.getReportTitle());
            validateStringLength(webSecurityForm, ATTR_TITLE, LABEL_FORM_TITLE, form.getReportTitle(), 1, REPORT_TITLE_LENGTH);
            validateStandardText(webSecurityForm, ATTR_TITLE, LABEL_FORM_TITLE, form.getReportTitle());
            if ((form.getStartDate() != null) && (form.getStartDate().trim().length() > 0)) {
                validateRequired(webSecurityForm, ATTR_START_DATE, LABEL_FORM_SCHEDULE_START_TIME, form.getStartDate());
                validateDate(webSecurityForm, ATTR_START_DATE, LABEL_FORM_SCHEDULE_START_TIME, form.getStartDate(), timeZone);
                final DateFormat dateTimeFmt = new SimpleDateFormat(FMT_UI_DATE_TIME);
                dateTimeFmt.setTimeZone(timeZone);
                Date startDate = DateUtil.getCurrentGmtDate();
                try {
                    startDate = dateTimeFmt.parse(form.getStartDate() + DATE_TIME_SEPARATOR + form.getStartTime());
                } catch (ParseException pe) {
                    throw new IllegalStateException("Un-expected incorrect date/time string!");
                }
                if (startDate.before(DateUtil.getCurrentGmtDate())) {
                    addErrorStatus(webSecurityForm, "startTime", "error.report.schedule.time.before");
                }
            }
            
            final Integer repeatType = (Integer) validateRequired(webSecurityForm, ATTR_REPEAT_TYPE, LABEL_FORM_SCHEDULE_REPEAT_TYPE,
                                                                  form.getRepeatType());
            if (repeatType != null) {
                final int type = repeatType.intValue();
                int maxLimit = StandardConstants.MAX_DAYS_IN_MONTH;
                if (type == REPORT_REPEAT_TYPE_WEEKLY) {
                    maxLimit = StandardConstants.WEEKS_IN_MONTH_5;
                } else if (type == REPORT_REPEAT_TYPE_MONTHLY) {
                    maxLimit = StandardConstants.MONTHS_IN_A_YEAR;
                } else if (type == REPORT_REPEAT_TYPE_DAILY) {
                    maxLimit = StandardConstants.MAX_DAYS_IN_MONTH;
                } else {
                    webSecurityForm.addErrorStatus(new FormErrorStatus(ATTR_REPEAT_TYPE,
                            this.getMessage(ErrorConstants.INVALID, this.getMessage(LABEL_FORM_SCHEDULE_REPEAT_TYPE))));
                }
                
                validateRequired(webSecurityForm, ATTR_REPEAT_FREQUENCY, LABEL_FORM_SCHEDULE_REPEAT_FREQUENCY, form.getRepeatEvery());
                if (form.getRepeatEvery() != null) {
                    validateIntegerLimits(webSecurityForm, ATTR_REPEAT_FREQUENCY, LABEL_FORM_SCHEDULE_REPEAT_FREQUENCY, form.getRepeatEvery(), 1,
                                          maxLimit);
                }
            }
            
            if ((form.getRepeatType() != null)
                && ((form.getRepeatType() == REPORT_REPEAT_TYPE_MONTHLY) || (form.getRepeatType() == REPORT_REPEAT_TYPE_WEEKLY))) {
                validateRequired(webSecurityForm, ATTR_REPEAT_ON, "label.reporting.form.schedule.repeatOn", form.getRepeatOn());
            }
        }
        
        validateEmailAddresses(webSecurityForm, ATTR_EMAIL, LABEL_FORM_SCHEDULE_EMAIL, form.getEmailList());
        if (((WebUser) WebSecurityUtil.getAuthentication().getPrincipal()).isSystemAdmin()) {
            if (form.getEmailList() != null && !form.getEmailList().isEmpty()) {
                validateRegEx(webSecurityForm, ATTR_EMAIL, LABEL_FORM_SCHEDULE_EMAIL, getEmailsLowerCase(form.getEmailList()),
                              WebCoreConstants.REGEX_EMAIL_DPT_OR_T2, "error.report.email.not.dpt.or.t2", null);
            }
        }
        
        if (REPORT_TYPE_STALL_REPORTS == form.getReportType() && (!form.getIsPDF())) {
            addErrorStatus(webSecurityForm, "outputFormat", ErrorConstants.INVALID,
                           new Object[] { this.getMessage("label.reporting.form.outputFormat") });
        }
    }
    
    final List<String> getEmailsLowerCase(final List<String> enteredEmails) {
        final List<String> lowerCaseEmailList = new ArrayList<String>();
        for (String email : enteredEmails) {
            lowerCaseEmailList.add(email.toLowerCase());
        }
        enteredEmails.removeAll(enteredEmails);
        enteredEmails.addAll(lowerCaseEmailList);
        return enteredEmails;
    }
    
    /*
     * private void checkLists(RandomKeyMapping keyMapping, ReportEditForm form, Errors errors, WebSecurityForm webSecurityForm) {
     * if (form.getSelectedPOSRandomIds() != null && !form.getSelectedPOSRandomIds().isEmpty()) {
     * form.setSelectedPOSRandomIds(Arrays.asList(form.getSelectedPOSRandomIds().get(0).split(",")));
     * for (String formPOSRandomId : form.getSelectedPOSRandomIds()) {
     * if (!formPOSRandomId.isEmpty() && !ID_ALL.equals(formPOSRandomId) && verifyAndReturnLocationByRandomId(keyMapping, formPOSRandomId) == null) {
     * webSecurityForm.addErrorStatus(new FormErrorStatus("posListFilters", this.getMessage("error.reporting.invalid.pos")));
     * }
     * }
     * } else if (form.getSelectedLocationPOSRandomIds() != null && !form.getSelectedLocationPOSRandomIds().isEmpty()) {
     * form.setSelectedLocationPOSRandomIds(Arrays.asList(form.getSelectedLocationPOSRandomIds().get(0).split(",")));
     * for (String formLocationPOSRandomId : form.getSelectedLocationPOSRandomIds()) {
     * if (!formLocationPOSRandomId.isEmpty() && !ID_ALL.equals(formLocationPOSRandomId)) {
     * if (verifyAndReturnLocationByRandomId(keyMapping, formLocationPOSRandomId) == null
     * && verifyAndReturnPOSByRandomId(keyMapping, formLocationPOSRandomId) == null) {
     * webSecurityForm.addErrorStatus(new FormErrorStatus("locationListFilters", this.getMessage("error.reporting.invalid.locationPOS")));
     * }
     * }
     * }
     * } else if (form.getSelectedRouteRandomIds() != null && !form.getSelectedRouteRandomIds().isEmpty()) {
     * form.setSelectedRouteRandomIds(Arrays.asList(form.getSelectedRouteRandomIds().get(0).split(",")));
     * for (String formRouteRandomId : form.getSelectedRouteRandomIds()) {
     * if (!formRouteRandomId.isEmpty() && !ID_ALL.equals(formRouteRandomId) && verifyAndReturnRouteByRandomId(keyMapping, formRouteRandomId) == null) {
     * webSecurityForm.addErrorStatus(new FormErrorStatus(ATTR_ROUTE, this.getMessage("error.reporting.invalid.route")));
     * }
     * }
     * }
     * }
     */
    
    private void checkFilters(final RandomKeyMapping keyMapping, final WebSecurityForm<ReportEditForm> secForm, final Errors errors) {
        final TimeZone timeZone = TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone());
        
        final ReportEditForm form = secForm.getWrappedObject();
        
        if (!StringUtils.isBlank(form.getAccountFilterValue()) && StringUtils.isBlank(form.getConsumerRandomId())) {
            secForm.addErrorStatus(new FormErrorStatus("accountListFilter", this.getMessage("alert.report.require.accountFilterType")));
        }
        
        final Integer reportType = form.getReportType();
        final ReportSpecificLabel label = LABEL[reportType];
        
        // Validate Primary Date
        if (form.getPrimaryDateType() != null) {
            final int primaryDateType = form.getPrimaryDateType();
            if (!this.reportingUtil.filterValueValid(reportType, REPORT_FILTER_TYPE_PRIMARY_DATE, primaryDateType)) {
                secForm.addErrorStatus(new FormErrorStatus("primaryDateType", this.getMessage(ErrorConstants.INVALID,
                                                                                              this.getMessage(label.getPrimaryDateLabel()))));
            } else if (REPORT_FILTER_DATE_DATE_TIME == primaryDateType) {
                validateRequired(secForm, ATTR_PRIMARY_DATE_BEGIN, label.getPrimaryDateLabel() + PAR_LABEL_DATE_BEGIN, form.getPrimaryDateBegin());
                validateRequired(secForm, ATTR_PRIMARY_TIME_BEGIN, label.getPrimaryDateLabel() + PAR_LABEL_TIME_BEGIN, form.getPrimaryTimeBegin());
                final Date primaryDateBegin = validateReportsDateTime(secForm, ATTR_PRIMARY_DATE_BEGIN, label.getPrimaryDateLabel() + PAR_LABEL_DATE_BEGIN,
                                                               form.getPrimaryDateBegin(), ATTR_PRIMARY_TIME_BEGIN, label.getPrimaryDateLabel()
                                                                                                                    + PAR_LABEL_TIME_BEGIN,
                                                               form.getPrimaryTimeBegin(), timeZone);
                
                validateRequired(secForm, ATTR_PRIMARY_DATE_END, label.getPrimaryDateLabel() + PAR_LABEL_DATE_END, form.getPrimaryDateEnd());
                validateRequired(secForm, ATTR_PRIMARY_TIME_END, label.getPrimaryDateLabel() + PAR_LABEL_TIME_END, form.getPrimaryTimeEnd());
                final Date primaryDateEnd = validateReportsDateTime(secForm, ATTR_PRIMARY_DATE_END, label.getPrimaryDateLabel() + PAR_LABEL_DATE_END,
                                                             form.getPrimaryDateEnd(), ATTR_PRIMARY_TIME_END, label.getPrimaryDateLabel()
                                                                                                              + PAR_LABEL_TIME_END,
                                                             form.getPrimaryTimeEnd(), timeZone);
                if (primaryDateBegin != null && primaryDateEnd != null) {
                    final long timeBegin = primaryDateBegin.getTime() % WebCoreConstants.ONE_DAY_IN_MILLISECONDS;
                    final long timeEnd = primaryDateEnd.getTime() % WebCoreConstants.ONE_DAY_IN_MILLISECONDS;
                    if (validateLesserOrEqualsValue(secForm, ATTR_PRIMARY_DATE_BEGIN, label.getPrimaryDateLabel() + PAR_LABEL_DATE_BEGIN, new Date(
                                                            primaryDateBegin.getTime() - timeBegin), ATTR_PRIMARY_DATE_END,
                                                    label.getPrimaryDateLabel() + PAR_LABEL_DATE_END,
                                                    new Date(primaryDateEnd.getTime() - timeEnd)) == 0) {
                        validateLesserValue(secForm, ATTR_PRIMARY_TIME_BEGIN, label.getPrimaryDateLabel() + PAR_LABEL_TIME_BEGIN,
                                                    primaryDateBegin, ATTR_PRIMARY_TIME_END, label.getPrimaryDateLabel() + PAR_LABEL_TIME_END,
                                                    primaryDateEnd);
                    }
                }
            } else if (REPORT_FILTER_DATE_MONTH == primaryDateType) {
                validateRequired(secForm, ATTR_PRIMARY_MONTH_BEGIN, label.getPrimaryDateLabel() + PAR_LABEL_MONTH_BEGIN, form.getPrimaryMonthBegin());
                validateRequired(secForm, ATTR_PRIMARY_YEAR_BEGIN, label.getPrimaryDateLabel() + PAR_LABEL_YEAR_BEGIN, form.getPrimaryYearBegin());
                
                validateRequired(secForm, ATTR_PRIMARY_MONTH_END, label.getPrimaryDateLabel() + PAR_LABEL_MONTH_END, form.getPrimaryMonthEnd());
                validateRequired(secForm, ATTR_PRIMARY_YEAR_END, label.getPrimaryDateLabel() + PAR_LABEL_YEAR_END, form.getPrimaryYearEnd());
                
                if (validateLesserOrEqualsValue(secForm, ATTR_PRIMARY_YEAR_BEGIN, label.getPrimaryDateLabel() + PAR_LABEL_YEAR_BEGIN,
                                                form.getPrimaryYearBegin(), ATTR_PRIMARY_YEAR_END, label.getPrimaryDateLabel() + PAR_LABEL_YEAR_END,
                                                form.getPrimaryYearEnd()) == 0) {
                    validateLesserOrEqualsValue(secForm, ATTR_PRIMARY_MONTH_BEGIN, label.getPrimaryDateLabel() + PAR_LABEL_MONTH_BEGIN,
                                                form.getPrimaryMonthBegin(), ATTR_PRIMARY_MONTH_END, label.getPrimaryDateLabel()
                                                                                                     + PAR_LABEL_MONTH_END, form.getPrimaryMonthEnd());
                }
            }
        }
        
        // Validate Secondary Date
        if (form.getSecondaryDateType() != null) {
            final int secondaryDateType = form.getSecondaryDateType();
            if (!this.reportingUtil.filterValueValid(reportType, REPORT_FILTER_TYPE_SECONDARY_DATE, secondaryDateType)) {
                secForm.addErrorStatus(new FormErrorStatus("secondaryDateType", this.getMessage(ErrorConstants.INVALID,
                                                                                                this.getMessage(label.getSecondaryDateLabel()))));
            } else if (REPORT_FILTER_DATE_DATE_TIME == secondaryDateType) {
                validateRequired(secForm, ATTR_SECONDARY_DATE_BEGIN, label.getSecondaryDateLabel() + PAR_LABEL_DATE_BEGIN,
                                 form.getSecondaryDateBegin());
                validateRequired(secForm, ATTR_SECONDARY_TIME_BEGIN, label.getSecondaryDateLabel() + PAR_LABEL_TIME_BEGIN,
                                 form.getSecondaryTimeBegin());
                final Date secondaryDateBegin = validateReportsDateTime(secForm, ATTR_SECONDARY_DATE_BEGIN, label.getSecondaryDateLabel()
                                                                                                     + PAR_LABEL_DATE_BEGIN,
                                                                 form.getSecondaryDateBegin(), ATTR_SECONDARY_TIME_BEGIN,
                                                                 label.getSecondaryDateLabel() + PAR_LABEL_TIME_BEGIN, form.getSecondaryTimeBegin(),
                                                                 timeZone);
                
                validateRequired(secForm, ATTR_SECONDARY_DATE_END, label.getSecondaryDateLabel() + PAR_LABEL_DATE_END, form.getSecondaryDateEnd());
                validateRequired(secForm, ATTR_SECONDARY_TIME_END, label.getSecondaryDateLabel() + PAR_LABEL_TIME_END, form.getSecondaryTimeEnd());
                final Date secondaryDateEnd = validateReportsDateTime(secForm, ATTR_SECONDARY_DATE_END, label.getSecondaryDateLabel() + PAR_LABEL_DATE_END,
                                                               form.getSecondaryDateEnd(), ATTR_SECONDARY_TIME_END, label.getSecondaryDateLabel()
                                                                                                                    + PAR_LABEL_TIME_END,
                                                               form.getSecondaryTimeEnd(), timeZone);
                
                if (secondaryDateBegin != null && secondaryDateEnd != null) {
                    final long timeBegin = secondaryDateBegin.getTime() % WebCoreConstants.ONE_DAY_IN_MILLISECONDS;
                    final long timeEnd = secondaryDateEnd.getTime() % WebCoreConstants.ONE_DAY_IN_MILLISECONDS;
                    if (validateLesserOrEqualsValue(secForm, ATTR_SECONDARY_DATE_BEGIN, label.getSecondaryDateLabel() + PAR_LABEL_DATE_BEGIN,
                                                    new Date(secondaryDateBegin.getTime() - timeBegin), ATTR_SECONDARY_DATE_END,
                                                    label.getSecondaryDateLabel() + PAR_LABEL_DATE_END,
                                                    new Date(secondaryDateEnd.getTime() - timeEnd)) == 0) {
                        validateLesserValue(secForm, ATTR_SECONDARY_TIME_BEGIN, label.getSecondaryDateLabel() + PAR_LABEL_TIME_BEGIN,
                                                    secondaryDateBegin, ATTR_SECONDARY_TIME_END, label.getSecondaryDateLabel() + PAR_LABEL_TIME_END,
                                                    secondaryDateEnd);
                    }
                }
            } else if (REPORT_FILTER_DATE_MONTH == secondaryDateType) {
                validateRequired(secForm, ATTR_SECONDARY_MONTH_BEGIN, label.getSecondaryDateLabel() + PAR_LABEL_MONTH_BEGIN,
                                 form.getSecondaryMonthBegin());
                validateRequired(secForm, ATTR_SECONDARY_YEAR_BEGIN, label.getSecondaryDateLabel() + PAR_LABEL_YEAR_BEGIN,
                                 form.getSecondaryYearBegin());
                
                validateRequired(secForm, ATTR_SECONDARY_MONTH_END, label.getSecondaryDateLabel() + PAR_LABEL_MONTH_END, form.getSecondaryMonthEnd());
                validateRequired(secForm, ATTR_SECONDARY_YEAR_END, label.getSecondaryDateLabel() + PAR_LABEL_YEAR_END, form.getSecondaryYearEnd());
                
                if (validateLesserOrEqualsValue(secForm, ATTR_SECONDARY_YEAR_BEGIN, label.getSecondaryDateLabel() + PAR_LABEL_YEAR_BEGIN,
                                                form.getSecondaryYearBegin(), ATTR_SECONDARY_YEAR_END, label.getSecondaryDateLabel()
                                                                                                       + PAR_LABEL_YEAR_END,
                                                form.getSecondaryYearEnd()) == 0) {
                    validateLesserOrEqualsValue(secForm, ATTR_SECONDARY_MONTH_BEGIN, label.getSecondaryDateLabel() + PAR_LABEL_MONTH_BEGIN,
                                                form.getSecondaryMonthBegin(), ATTR_SECONDARY_MONTH_END, label.getSecondaryDateLabel()
                                                                                                         + PAR_LABEL_MONTH_END,
                                                form.getSecondaryMonthEnd());
                }
            }
        }
        
        // Validate Organization
        final boolean needOrgTree = WebCoreConstants.CUSTOMER_TYPE_PARENT == WebSecurityUtil.getUserAccount().getCustomer().getCustomerType().getId();
        if (needOrgTree) {
            validateRequired(secForm, ATTR_ORG_TREE, LABEL_LOCATION_AND_PAYSTATION_FULL, form.getSelectedOrgRandomIds());
            validateRandomIds(secForm, ATTR_ORG_TREE, LABEL_LOCATION_AND_PAYSTATION_FULL, form.getSelectedOrgRandomIds(), keyMapping, Customer.class,
                              Integer.class, ID_ALL);
        }
        
        if (reportType == ReportingConstants.REPORT_TYPE_STALL_REPORTS) {
            validateRequired(secForm, ATTR_LOCATION, LABEL_FORM_LOCATION, form.getSelectedLocationRandomIds());
            validateRandomIds(secForm, ATTR_LOCATION, LABEL_FORM_LOCATION, form.getSelectedLocationRandomIds(), keyMapping, Location.class,
                              Integer.class);
        } else if (form.getLocationType() != null) {
            final int locationType = form.getLocationType();
            if (!this.reportingUtil.filterValueValid(reportType, REPORT_FILTER_TYPE_LOCATION, form.getLocationType())) {
                secForm.addErrorStatus(new FormErrorStatus("locationType", this.getMessage(ErrorConstants.INVALID,
                                                                                           this.getMessage(LABEL_FORM_LOCATION_TYPE))));
            } else if (REPORT_FILTER_LOCATION_LOCATION_TREE == locationType) {
                validateRequired(secForm, ATTR_LOCATION_PAYSTATION, LABEL_LOCATION_AND_PAYSTATION_FULL, form.getSelectedLocationRandomIds());
                validateRandomIds(secForm, ATTR_LOCATION_PAYSTATION, LABEL_LOCATION_AND_PAYSTATION_FULL, form.getSelectedLocationRandomIds(),
                                  keyMapping, Location.class, Integer.class, ID_ALL);
            } else if (REPORT_FILTER_LOCATION_ROUTE_TREE == locationType) {
                validateRequired(secForm, ATTR_ROUTE, LABEL_ROUTE_FULL, form.getSelectedLocationRandomIds());
                validateRandomIds(secForm, ATTR_ROUTE, LABEL_ROUTE_FULL, form.getSelectedLocationRandomIds(), keyMapping, Route.class, Integer.class,
                                  ID_ALL);
            } else if (REPORT_FILTER_LOCATION_PAY_STATION_TREE == locationType) {
                validateRequired(secForm, ATTR_PAYSTATION, LABEL_PAYSTATION_FULL, form.getSelectedLocationRandomIds());
                validateRandomIds(secForm, ATTR_ROUTE, LABEL_PAYSTATION_FULL, form.getSelectedLocationRandomIds(), keyMapping, PointOfSale.class,
                                  Integer.class, ID_ALL);
            }
        }
        if (form.getSpaceNumberType() != null) {
            final int spaceNumberType = form.getSpaceNumberType();
            if (!this.reportingUtil.filterValueValid(reportType, REPORT_FILTER_TYPE_SPACE_NUMBER, spaceNumberType)) {
                secForm.addErrorStatus(new FormErrorStatus("spaceNumberType", this.getMessage(ErrorConstants.INVALID,
                                                                                              this.getMessage(LABEL_FORM_SPACE_NUMBER))));
            }
            if (REPORT_FILTER_SPACE_NUMBER_EQUAL == spaceNumberType || REPORT_FILTER_SPACE_NUMBER_GREATER == spaceNumberType
                || REPORT_FILTER_SPACE_NUMBER_LESS == spaceNumberType || REPORT_FILTER_SPACE_NUMBER_BETWEEN == spaceNumberType) {
                validateRequired(secForm, ATTR_SPACE_BEGIN, LABEL_FORM_SPACE_NUMBER, form.getSpaceNumberBegin());
                validateIntegerLimits(secForm, ATTR_SPACE_BEGIN, LABEL_FORM_SPACE_NUMBER, form.getSpaceNumberBegin(), 1,
                                      WebCoreConstants.STALL_NUM_MAX);
            }
            if (REPORT_FILTER_SPACE_NUMBER_BETWEEN == spaceNumberType) {
                validateRequired(secForm, ATTR_SPACE_END, LABEL_FORM_SPACE_NUMBER, form.getSpaceNumberEnd());
                validateIntegerLimits(secForm, ATTR_SPACE_END, LABEL_FORM_SPACE_NUMBER, form.getSpaceNumberEnd(), 1, WebCoreConstants.STALL_NUM_MAX);
                validateLesserOrEqualsValue(secForm, ATTR_SPACE_BEGIN, "label.reporting.form.filter.parker.spaceNumber.begin",
                                            form.getSpaceNumberBegin(), ATTR_SPACE_END, "label.reporting.form.filter.parker.spaceNumber.end",
                                            form.getSpaceNumberEnd());
            }
        }
        if (form.getCardType() != null) {
            this.validateRandomIdForWebObject(secForm, "cardType", "label.reporting.form.filter.card.type", form.getCardType(), keyMapping, ID_ALL);
        }
        if (StringUtils.isNotBlank(form.getMerchantAccount())) {
            this.validateRandomId(secForm, "merchantAccount", "label.reporting.form.filter.card.merchantAccount", form.getMerchantAccount(),
                                  keyMapping, MerchantAccount.class, Integer.class, ID_ALL);
        }
        if (form.getCardNumberType() != null) {
            final int cardNumberType = form.getCardNumberType();
            if (!this.reportingUtil.filterValueValid(reportType, REPORT_FILTER_TYPE_CARD_NUMBER, cardNumberType)) {
                secForm.addErrorStatus(new FormErrorStatus("cardNumberType", this.getMessage(ErrorConstants.INVALID,
                                                                                             this.getMessage(LABEL_FORM_CARD_NUMBER))));
            } else if (REPORT_FILTER_CARD_NUMBER_SPECIFIC == cardNumberType) {
                validateRequired(secForm, ATTR_CARD_NUMBER, LABEL_FORM_CARD_NUMBER, form.getCardNumberSpecific());
                if (REPORT_TYPE_TRANSACTION_SMART_CARD == reportType) {
                    validateNumberStringLength(secForm, ATTR_CARD_NUMBER, LABEL_FORM_CARD_NUMBER, form.getCardNumberSpecific(),
                                               CardProcessingConstants.SMART_CARD_MIN_LENGTH, CardProcessingConstants.BAD_SMART_CARD_MAX_LENGTH);
                    validateAlphaNumeric(secForm, ATTR_CARD_NUMBER, LABEL_FORM_CARD_NUMBER, form.getCardNumberSpecific());
                } else if (REPORT_TYPE_TRANSACTION_VALUE_CARD == reportType || REPORT_TYPE_TRANSACTION_PATROLLER_CARD == reportType) {
                    validateNumberStringLength(secForm, ATTR_CARD_NUMBER, LABEL_FORM_CARD_NUMBER, form.getCardNumberSpecific(),
                                               CardProcessingConstants.VALUE_CARD_MIN_LENGTH, CardProcessingConstants.BAD_VALUE_CARD_MAX_LENGTH);
                    validateNumber(secForm, ATTR_CARD_NUMBER, LABEL_FORM_CARD_NUMBER, form.getCardNumberSpecific());
                } else {
                    validateStringLength(secForm, ATTR_CARD_NUMBER, LABEL_FORM_CARD_NUMBER, form.getCardNumberSpecific(),
                                         WebCoreConstants.MAX_NUMBERS_LAST_DIGITS);
                    validateNumber(secForm, ATTR_CARD_NUMBER, LABEL_FORM_CARD_NUMBER, form.getCardNumberSpecific());
                }
            }
        }
        if (form.getApprovalStatus() != null
            && !this.reportingUtil.filterValueValid(reportType, REPORT_FILTER_TYPE_APPROVAL_STATUS, form.getApprovalStatus())) {
            secForm.addErrorStatus(new FormErrorStatus("approvalStatus", this.getMessage(ErrorConstants.INVALID, this
                    .getMessage("label.reporting.form.filter.card.approvalStatus"))));
        }
        if (form.getTicketNumberType() != null) {
            final int ticketNumberType = form.getTicketNumberType();
            if (!this.reportingUtil.filterValueValid(reportType, REPORT_FILTER_TYPE_TICKET_NUMBER, ticketNumberType)) {
                secForm.addErrorStatus(new FormErrorStatus("ticketNumberType", this.getMessage(ErrorConstants.INVALID,
                                                                                               this.getMessage(label.getTicketNumberTypeLabel()))));
            }
            if (REPORT_FILTER_TICKET_NUMBER_SPECIFIC == ticketNumberType || REPORT_FILTER_TICKET_NUMBER_BETWEEN == ticketNumberType
                || REPORT_FILTER_REPORT_NUMBER_SPECIFIC == ticketNumberType || REPORT_FILTER_REPORT_NUMBER_LESS == ticketNumberType
                || REPORT_FILTER_REPORT_NUMBER_GREATER == ticketNumberType || REPORT_FILTER_REPORT_NUMBER_BETWEEN == ticketNumberType) {
                validateRequired(secForm, ATTR_TICKET_BEGIN, label.getTicketNumberLabel(), form.getTicketNumberBegin());
                validateIntegerLimits(secForm, ATTR_TICKET_BEGIN, label.getTicketNumberLabel(), form.getTicketNumberBegin(), 0, MAX_TICKET_NUMBER);
            }
            if (REPORT_FILTER_TICKET_NUMBER_BETWEEN == ticketNumberType || REPORT_FILTER_REPORT_NUMBER_BETWEEN == ticketNumberType) {
                validateRequired(secForm, ATTR_TICKET_END, label.getTicketNumberLabel(), form.getTicketNumberEnd());
                validateIntegerLimits(secForm, ATTR_TICKET_END, label.getTicketNumberLabel(), form.getTicketNumberEnd(), 0, MAX_TICKET_NUMBER);
                validateLesserOrEqualsValue(secForm, ATTR_TICKET_BEGIN, label.getTicketNumberLabel() + ".begin", form.getTicketNumberBegin(),
                                            ATTR_TICKET_END, label.getTicketNumberLabel() + ".end", form.getTicketNumberEnd());
            }
        }
        if (form.getCouponNumberType() != null) {
            final int couponNumberType = form.getCouponNumberType();
            if (!this.reportingUtil.filterValueValid(reportType, REPORT_FILTER_TYPE_COUPON_NUMBER, couponNumberType)) {
                secForm.addErrorStatus(new FormErrorStatus("couponNumberType", this.getMessage(ErrorConstants.INVALID,
                                                                                               this.getMessage(LABEL_FORM_COUPON_NUMBER))));
            } else if (REPORT_FILTER_COUPON_NUMBER_SPECIFIC == couponNumberType) {
                validateRequired(secForm, ATTR_COUPON_NUMBER, LABEL_FORM_COUPON_NUMBER, form.getCouponNumberSpecific());
                validateStringLength(secForm, ATTR_COUPON_NUMBER, LABEL_FORM_COUPON_NUMBER, form.getCouponNumberSpecific(), 1, COUPON_NUMBER_LENGTH);
                validateAlphaNumericSearch(secForm, ATTR_COUPON_NUMBER, LABEL_FORM_COUPON_NUMBER, form.getCouponNumberSpecific());
            }
        }
        if (form.getCouponType() != null && !this.reportingUtil.filterValueValid(reportType, REPORT_FILTER_TYPE_COUPON_TYPE, form.getCouponType())) {
            secForm.addErrorStatus(new FormErrorStatus("couponType",
                    this.getMessage(ErrorConstants.INVALID, this.getMessage("label.reporting.form.filter.parker.couponType"))));
        }
        if ((form.getLicencePlate() != null) && (form.getLicencePlate().trim().length() > 0)) {
            validateStringLength(secForm, ATTR_LICENSE_PLATE, LABEL_FORM_LICENSE_PLATE, form.getLicencePlate(), 1, LICENSE_PLATE_LENGTH);
            validateAlphaNumericWithSpace(secForm, ATTR_LICENSE_PLATE, LABEL_FORM_LICENSE_PLATE, form.getLicencePlate());
        }
        if (reportType == ReportingConstants.REPORT_TYPE_COLLECTION_SUMMARY) {
            if (!(form.isForBillAmount() || form.isForBillCount() || form.isForCardAmount() || form.isForCardCount() || form.isForCoinAmount()
                  || form.isForCoinCount() || form.isForRunningTotal() || form.isForOverdueCollection())) {
                secForm.addErrorStatus(new FormErrorStatus("collectionType", this.getMessage("error.common.required", this
                        .getMessage("label.reporting.form.filter.collectionType"))));
            }
        }
        if (form.getOtherParameters() != null
            && !this.reportingUtil.filterValueValid(reportType, REPORT_FILTER_TYPE_OTHER_PARAMETERS, form.getOtherParameters())) {
            secForm.addErrorStatus(new FormErrorStatus("otherParameters", this.getMessage(ErrorConstants.INVALID,
                                                                                          this.getMessage("label.reporting.form.filter.other"))));
        }
        if (form.getGroupBy() != null && !this.reportingUtil.filterValueValid(reportType, REPORT_FILTER_TYPE_GROUP_BY, form.getGroupBy())) {
            secForm.addErrorStatus(new FormErrorStatus("groupBy", this.getMessage(ErrorConstants.INVALID,
                                                                                  this.getMessage("label.reporting.form.filter.other.groupBy"))));
        }
        if (form.getSortBy() != null && !this.reportingUtil.filterValueValid(reportType, REPORT_FILTER_TYPE_SORT_BY, form.getSortBy())) {
            secForm.addErrorStatus(new FormErrorStatus("sortBy", this.getMessage(ErrorConstants.INVALID,
                                                                                 this.getMessage("label.reporting.form.filter.other.orderBy"))));
        }
        return;
    }
    
    /*
     * private void checkTitle(String title, int reportRepeatType, Errors errors, WebSecurityForm webSecurityForm) {
     * if (StringUtils.isBlank(title) && reportRepeatType > 1) {
     * webSecurityForm.addErrorStatus(new FormErrorStatus(ATTR_TITLE, this.getMessage(ErrorConstants.INVALID, new String[] {
     * this.getMessage("label.serviceAgreement.title") } )));
     * return;
     * } else if (title.length() > REPORT_TITLE_LENGTH) {
     * webSecurityForm.addErrorStatus(new FormErrorStatus(ATTR_TITLE, this.getMessage("error.common.invalid.lengths",
     * new String[] { this.getMessage("label.serviceAgreement.title"),
     * "1", String.valueOf(REPORT_TITLE_LENGTH) } )));
     * }
     * if (!title.matches(REPORT_TITLE_REG_EXP)) {
     * webSecurityForm.addErrorStatus(new FormErrorStatus(ATTR_TITLE, this.getMessage("error.common.invalid.special.character.name",
     * new String[] { this.getMessage("label.serviceAgreement.title"), "a-z A-Z 0-9" } )));
     * }
     * return;
     * }
     * private PointOfSale verifyAndReturnPOSByRandomId(RandomKeyMapping keyMapping, String strRandomPOSId) {
     * if (!DashboardUtil.validateRandomId(strRandomPOSId)) {
     * return null;
     * }
     * Object actualPOSId = keyMapping.getKey(strRandomPOSId);
     * if (!DashboardUtil.validateDBIntegerPrimaryKey(actualPOSId.toString())) {
     * return null;
     * }
     * int pointOfSaleId = Integer.parseInt(actualPOSId.toString());
     * PointOfSale pointOfSale = pointOfSaleService.findPointOfSaleById(pointOfSaleId);
     * return pointOfSale;
     * }
     * private Location verifyAndReturnLocationByRandomId(RandomKeyMapping keyMapping, String strRandomLocationId) {
     * if (!DashboardUtil.validateRandomId(strRandomLocationId)) {
     * return null;
     * }
     * Object actualLocationId = keyMapping.getKey(strRandomLocationId);
     * if (!DashboardUtil.validateDBIntegerPrimaryKey(actualLocationId.toString())) {
     * return null;
     * }
     * int locationId = Integer.parseInt(actualLocationId.toString());
     * Location location = locationService.findLocationById(locationId);
     * return location;
     * }
     * private Route verifyAndReturnRouteByRandomId(RandomKeyMapping keyMapping, String strRandomRouteId) {
     * if (!DashboardUtil.validateRandomId(strRandomRouteId)) {
     * return null;
     * }
     * Object actualRouteId = keyMapping.getKey(strRandomRouteId);
     * if (!DashboardUtil.validateDBIntegerPrimaryKey(actualRouteId.toString())) {
     * return null;
     * }
     * int routeId = Integer.parseInt(actualRouteId.toString());
     * Route route = routeService.findRouteById(routeId);
     * return route;
     * }
     * private PaystationSetting verifyAndReturnPaystationSettingByRandomId(RandomKeyMapping keyMapping, String strRandomPaystationSettingId) {
     * if (!DashboardUtil.validateRandomId(strRandomPaystationSettingId)) {
     * return null;
     * }
     * Object actualPaystationSettingId = keyMapping.getKey(strRandomPaystationSettingId);
     * if (!DashboardUtil.validateDBIntegerPrimaryKey(actualPaystationSettingId.toString())) {
     * return null;
     * }
     * int paystationSettingId = Integer.parseInt(actualPaystationSettingId.toString());
     * PaystationSetting paystationSetting = paystationSettingService.findPaystationSetting(paystationSettingId);
     * return paystationSetting;
     * }
     * private void validateEmail(ReportEditForm form, WebSecurityForm webSecurityForm) {
     * List<String> emailList = form.getEmailList();
     * List<String> emails = null;
     * if (emailList != null && !emailList.isEmpty()) {
     * emails = Arrays.asList(emailList.get(0).split(","));
     * }
     * if (emails == null || emails.isEmpty()) {
     * webSecurityForm.addErrorStatus(new FormErrorStatus("alertContacts", this.getMessage("error.common.required", new String[] {
     * this.getMessage("label.login.username") } )));
     * return;
     * }
     * boolean hasError = false;
     * for (String email : emails) {
     * if (StringUtils.isBlank(email)) {
     * hasError = true;
     * webSecurityForm.addErrorStatus(new FormErrorStatus("alertContacts", this.getMessage("error.common.required", new String[] {
     * this.getMessage("label.login.username") } )));
     * break;
     * }
     * if (!email.matches(WebCoreConstants.REGEX_EMAIL)) {
     * hasError = true;
     * webSecurityForm.addErrorStatus(new FormErrorStatus("alertContacts", this.getMessage(ErrorConstants.INVALID, new String[] {
     * this.getMessage("label.login.username") } )));
     * break;
     * }
     * }
     * if (!hasError) {
     * form.setEmailList(emails);
     * } else {
     * form.setEmailList(null);
     * }
     * }
     */
    
    protected static final class ReportSpecificLabel {
        private String primaryDateLabel;
        private String secondaryDateLabel;
        
        private String ticketNumberTypeLabel;
        private String ticketNumberLabel;
        
        public ReportSpecificLabel(final String primaryDateLabel, final String secondaryDateLabel, final String ticketNumberTypeLabel,
                final String ticketNumberLabel) {
            this.primaryDateLabel = primaryDateLabel;
            this.secondaryDateLabel = secondaryDateLabel;
            
            this.ticketNumberTypeLabel = ticketNumberTypeLabel;
            this.ticketNumberLabel = ticketNumberLabel;
        }
        
        public String getPrimaryDateLabel() {
            return this.primaryDateLabel;
        }
        
        public void setPrimaryDateLabel(final String primaryDateLabel) {
            this.primaryDateLabel = primaryDateLabel;
        }
        
        public String getSecondaryDateLabel() {
            return this.secondaryDateLabel;
        }
        
        public void setSecondaryDateLabel(final String secondaryDateLabel) {
            this.secondaryDateLabel = secondaryDateLabel;
        }
        
        public String getTicketNumberTypeLabel() {
            return this.ticketNumberTypeLabel;
        }
        
        public void setTicketNumberTypeLabel(final String ticketNumberTypeLabel) {
            this.ticketNumberTypeLabel = ticketNumberTypeLabel;
        }
        
        public String getTicketNumberLabel() {
            return this.ticketNumberLabel;
        }
        
        public void setTicketNumberLabel(final String ticketNumberLabel) {
            this.ticketNumberLabel = ticketNumberLabel;
        }
    }
}
