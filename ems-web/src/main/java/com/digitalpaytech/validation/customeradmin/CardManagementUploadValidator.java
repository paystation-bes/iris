package com.digitalpaytech.validation.customeradmin;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.mvc.customeradmin.support.BannedCardUploadForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.validation.BaseValidator;

@Component("cardManagementUploadValidator")
public class CardManagementUploadValidator extends BaseValidator<BannedCardUploadForm> {
    @Override
    public final void validate(final WebSecurityForm<BannedCardUploadForm> target, final Errors errors) {
        final WebSecurityForm<BannedCardUploadForm> form = prepareSecurityForm(target, errors, WebCoreConstants.POSTTOKEN_STRING);
        if (form != null) {
            final BannedCardUploadForm uploadForm = form.getWrappedObject();
            validateRequired(form, FormFieldConstants.FILE, "label.cardManagement.bannedCard.importForm.sourceFile",
                             uploadForm.getFile().getOriginalFilename());
            validateRequired(form, FormFieldConstants.IMPORT_MODE, FormFieldConstants.LABEL_IMPORT_MODE, uploadForm.getRecordStatus());
        }
    }
}
