package com.digitalpaytech.validation.customeradmin;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.digitalpaytech.mvc.customeradmin.support.TransactionReceiptSendForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.validation.BaseValidator;

@Component("transactionReceiptSendValidator")
public class TransactionReceiptSendValidator extends BaseValidator<TransactionReceiptSendForm> {
    
    @Override
    public void validate(WebSecurityForm<TransactionReceiptSendForm> command, Errors errors) {
        
        WebSecurityForm<TransactionReceiptSendForm> webSecurityForm = prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
        
        TransactionReceiptSendForm form = webSecurityForm.getWrappedObject();
        
        super.validateRequired(command, "emailAddress", "label.accounts.transactionSearch.emailAddress", form.getEmailAddress());
        super.validateStringLength(webSecurityForm, "emailAddress", "label.accounts.transactionSearch.emailAddress", form.getEmailAddress(), 0,
                                   WebCoreConstants.VALIDATION_MAX_LENGTH_255);
        
        if (!StringUtils.isEmpty(form.getEmailAddress())) {
            String emailAddress = this.validateEmailAddress(webSecurityForm, "emailAddress", "label.accounts.transactionSearch.emailAddress",
                                                            form.getEmailAddress());
            if (emailAddress != null) {
                form.setEmailAddress(emailAddress);
            }
        }
        
    }
}
