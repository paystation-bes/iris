package com.digitalpaytech.validation.customeradmin;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.Consumer;
import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.domain.CustomerCard;
import com.digitalpaytech.domain.EmailAddress;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.customeradmin.support.CustomerAdminConsumerAccountEditForm;

import com.digitalpaytech.mvc.support.WebSecurityForm;

import com.digitalpaytech.service.ConsumerService;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.BaseValidator;

@Component("customerAdminConsumerValidator")
public class CustomerAdminConsumerAccountsValidator extends BaseValidator<CustomerAdminConsumerAccountEditForm> {
	@Autowired
    protected ConsumerService consumerService;
    
    @Override
    public void validate(WebSecurityForm<CustomerAdminConsumerAccountEditForm> command, Errors errors){        
    }
       
    public void validate(WebSecurityForm<CustomerAdminConsumerAccountEditForm> command, Errors errors, RandomKeyMapping keyMapping) {
        
        WebSecurityForm<CustomerAdminConsumerAccountEditForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if(webSecurityForm == null){
            return;
        }
                
        checkIds(webSecurityForm, keyMapping);
        checkNames(webSecurityForm);
        
        checkEmail(webSecurityForm, keyMapping);
        checkDescription(webSecurityForm);
    }
    
    /**
     * Converts random ids from form into a database ids that are saved for later use
     * 
     * @param webSecurityForm WebSecurityForm object
     */ 
    private void checkIds(WebSecurityForm<CustomerAdminConsumerAccountEditForm> webSecurityForm, RandomKeyMapping keyMapping) {
        CustomerAdminConsumerAccountEditForm form = webSecurityForm.getWrappedObject();
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
            String randomId = (String) super.validateRequired(webSecurityForm, "consumerId", "label.customerAdmin.consumer.id", form.getRandomId());                         
            Integer consumerId = super.validateRandomId(webSecurityForm, "consumerId", "label.customerAdmin.consumer.id", randomId, keyMapping, Consumer.class, Integer.class);
            if(consumerId == null){
                return;
            }
            else {
            	form.setConsumerId(consumerId);
            }
        }

        form.setCouponIds(null);
        if(form.getCouponRandomIds() != null && super.listHasValues(form.getCouponRandomIds())){
            String couponRandomIds [] = form.getCouponRandomIds().get(0).split(",");
            Collection<String> couponRandomIdsList = Arrays.asList(couponRandomIds);
            form.setCouponIds(validateRandomIds(webSecurityForm, "couponId", "label.customerAdmin.consumer.coupon.id", couponRandomIdsList, keyMapping, Coupon.class, Integer.class));
        }                
        form.setCustomCardIds(null);
        if(form.getCustomCardRandomIds() != null && super.listHasValues(form.getCustomCardRandomIds())){
            String customCardRandomIds [] = form.getCustomCardRandomIds().get(0).split(",");
            Collection<String> customCardIdList = Arrays.asList(customCardRandomIds);
            form.setCustomCardIds(validateRandomIds(webSecurityForm, "customCardId", "label.customerAdmin.consumer.customcard.id", customCardIdList, keyMapping, CustomerCard.class, Integer.class));
        }
    }
           
    private void checkNames(WebSecurityForm<CustomerAdminConsumerAccountEditForm> webSecurityForm) {
        
        CustomerAdminConsumerAccountEditForm form = webSecurityForm.getWrappedObject();
        String firstName = form.getFirstName();        
        firstName = (String )super.validateRequired(webSecurityForm, "firstName", "label.customerAdmin.consumer.firstName", firstName);
        if (firstName != null) {
            super.validateStringLength(webSecurityForm, "firstName", "label.customerAdmin.consumer.firstName", firstName, WebCoreConstants.VALIDATION_MIN_LENGTH_1,
                                       WebCoreConstants.VALIDATION_MAX_LENGTH_25);
            form.setFirstName(super.validateStandardText(webSecurityForm, "firstName", "label.customerAdmin.consumer.firstName", firstName));
        }
        
        String lastName = form.getLastName();        
        lastName = (String )super.validateRequired(webSecurityForm, "lastName", "label.customerAdmin.consumer.lastName", lastName);
        if (lastName != null) {
            super.validateStringLength(webSecurityForm, "lastName", "label.customerAdmin.consumer.lastName", lastName, WebCoreConstants.VALIDATION_MIN_LENGTH_1,
                                       WebCoreConstants.VALIDATION_MAX_LENGTH_25);
            form.setLastName(super.validateStandardText(webSecurityForm, "lastName", "label.customerAdmin.consumer.lastName", lastName));
        }        
    }
        
    private void checkEmail(WebSecurityForm<CustomerAdminConsumerAccountEditForm> webSecurityForm, RandomKeyMapping keyMapping) {                       
        CustomerAdminConsumerAccountEditForm form = webSecurityForm.getWrappedObject();
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE && !StringUtils.isEmpty(form.getEmailAddressRandomId())) {                                
            Integer emailId = validateRandomId(webSecurityForm, "emailAddressId", "label.customerAdmin.consumer.email.id", form.getEmailAddressRandomId(), keyMapping, EmailAddress.class, Integer.class);
            if(emailId != null) {
            	form.setEmailAddressId(emailId);
            }
        }     
        
        if (!StringUtils.isEmpty(form.getEmailAddress())){
            String emailAddress = this.validateEmailAddress(webSecurityForm, "emailAddress", "label.customerAdmin.consumer.email", form.getEmailAddress());
            if(emailAddress != null){
                form.setEmailAddress(emailAddress);
            }
        }
        
        List<EmailAddress> emails = consumerService.findEmailAddressByEmail(form.getEmailAddress());
        if(emails != null && !super.isListNullorEmpty(emails)){
            UserAccount userAccount = WebSecurityUtil.getUserAccount();
            List<Consumer> consumerByEmailList = consumerService.findOtherConsumerByCustomerIdAndEmailId(form.getConsumerId(), userAccount.getCustomer().getId(), emails.get(0).getId());
            if(consumerByEmailList != null && !super.isListNullorEmpty(consumerByEmailList)){
                super.addErrorStatus(webSecurityForm, "emailAddress",  "error.common.duplicate.email", new Object[] { form.getEmailAddress() });                          
            } else {
                form.setEmailAddressObj(emails.get(0));
            }          
        }
        
    } 
        
    private void checkDescription(WebSecurityForm<CustomerAdminConsumerAccountEditForm> webSecurityForm) {        
        CustomerAdminConsumerAccountEditForm form = (CustomerAdminConsumerAccountEditForm) webSecurityForm.getWrappedObject();                                        
        form.setDescription(super.validateExternalDescription(webSecurityForm, "description", "label.customerAdmin.consumer.description", form.getDescription(),
                                                              WebCoreConstants.VALIDATION_MIN_LENGTH_1));
    }
}
