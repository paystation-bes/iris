package com.digitalpaytech.validation.customeradmin;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.validation.BaseValidator;
import com.digitalpaytech.client.dto.fms.DeviceGroup;
import com.digitalpaytech.mvc.support.WebSecurityForm;

@Component("deviceGroupValidator")
public class DeviceGroupValidator extends BaseValidator<DeviceGroup> {
       
    @Override
    public final void validate(final WebSecurityForm<DeviceGroup> target, final Errors errors) {
        final WebSecurityForm<DeviceGroup> form = prepareSecurityForm(target, errors, WebCoreConstants.POSTTOKEN_STRING, false);
        if (form == null) {
            return;
        }
    }
        
}