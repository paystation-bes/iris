package com.digitalpaytech.validation.customeradmin;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.digitalpaytech.mvc.customeradmin.support.io.dto.CouponCsvDTO;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.csv.CsvCommonValidator;
import com.digitalpaytech.util.csv.CsvProcessingError;

@Component("couponCsvValidator")
public class CouponCsvValidator extends CsvCommonValidator<CouponCsvDTO> {

	@Override
	public void validateRow(List<CsvProcessingError> errors,List<CsvProcessingError> warnings, CouponCsvDTO dto, Map<String, Object> context) {
		// Validate Coupon Number
		validateRequired(errors, "couponCode", "label.coupon.number", dto.getCouponCode());
		validateStringLength(errors, "couponCode", "label.coupon.number", dto.getCouponCode(), WebCoreConstants.COUPON_LENGTH_MIN, WebCoreConstants.COUPON_LENGTH_MAX);
		validateAlphaNumeric(errors, "couponCode", "label.coupon.number", dto.getCouponCode());
		
		// Validate Description
		validateExternalDescription(errors, "description", "label.coupon.description", dto.getDescription(), WebCoreConstants.VALIDATION_MIN_LENGTH_1);
		
		// Validate Percent Discount
		if(validateNumber(errors, "percentDiscount", "label.coupon.discount.unit.percentage", dto.getPercentDiscount()) != null) {
			validateIntegerLimits(errors, "percentDiscount", "label.coupon.discount.unit.percentage", dto.getPercentDiscount(), WebCoreConstants.COUPON_DISCOUNT_MIN, WebCoreConstants.COUPON_DISCOUNT_MAX);
		}
		
		// Validate Dollar Discount
		if(validateDollarAmt(errors, "dollarDiscount", "label.coupon.discount.unit.dollar", dto.getDollarDiscount()) != null) {
			validateDoubleLimits(errors, "dollarDiscount", "label.coupon.discount.unit.dollar", dto.getDollarDiscount(), WebCoreConstants.COUPON_DISCOUNT_MIN, WebCoreConstants.COUPON_DOLLARS_DISCOUNT_MAX);
		}
		
		// Validate Num Uses
		if(!WebCoreConstants.UNLIMITED.equalsIgnoreCase(dto.getNumUses())) {
			if(validateNumber(errors, "maxNumOfUses", "label.coupon.maxNumOfUses", dto.getNumUses()) != null) {
				validateIntegerLimits(errors, "maxNumOfUses", "label.coupon.maxNumOfUses", dto.getNumUses(), WebCoreConstants.COUPON_USES_MIN, WebCoreConstants.COUPON_USES_MAX);
			}
		}
		
		/*
		 * Validate PND, PBP, PBS. If it's not empty/blank, the value needs to be "on", "true" or "yes".
		 */
		Boolean isPnd = validateBooleanString(errors, "pnd", "label.coupon.available.pnd", dto.getPnd());
		Boolean isPbs = validateBooleanString(errors, "pbs", "label.coupon.available.pbs", dto.getPbs());
		if((isPnd != null) && (isPbs != null) && !isPnd && !isPbs) {
			addError(errors, "payOptions", getMessage("error.common.required.atLeast1Of2",  new Object[] { getMessage("label.coupon.available.pnd"), getMessage("label.coupon.available.pbs") }));
		}
		
		if((isPbs != null) && (isPbs)) {
			// Validate Stall Range
			validateIntegerRangeString(errors, "spaceRange", "label.coupon.spaceRange", dto.getSpaceRange(), WebCoreConstants.STALL_NUM_MIN, WebCoreConstants.STALL_NUM_MAX);
		}
	}
	
}
