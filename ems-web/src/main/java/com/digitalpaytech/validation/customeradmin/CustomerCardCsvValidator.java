package com.digitalpaytech.validation.customeradmin;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.digitalpaytech.mvc.customeradmin.support.io.dto.CustomerCardCsvDTO;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.csv.CsvCommonValidator;
import com.digitalpaytech.util.csv.CsvProcessingError;

@Component("customerCardCsvValidator")
public class CustomerCardCsvValidator extends CsvCommonValidator<CustomerCardCsvDTO> {
    @Override
    public void validateRow(List<CsvProcessingError> errors, List<CsvProcessingError> warnings, CustomerCardCsvDTO dto, Map<String, Object> context) {
        // Validate Card Number
        if (validateRequired(errors, "cardNumber", "label.settings.cardSettings.CardNumber", dto.getCardNumber()) != null) {
            validateNumberStringLength(errors, "cardNumber", "label.settings.cardSettings.CardNumber", dto.getCardNumber(),
                                       CardProcessingConstants.VALUE_CARD_MIN_LENGTH, CardProcessingConstants.BAD_VALUE_CARD_MAX_LENGTH);
            validateCustomerCardNumber(errors, "cardNumber", "label.settings.cardSettings.CardNumber", dto.getCardNumber());
        }
        
        // Validate Card Type
        validateRequired(errors, "cardType", "label.customerCard.message.cardType", dto.getCardType());
        
        // Validate Grace Period
        if (validateNumber(errors, "gracePeriod", "label.customerCard.gracePeriod.cleaned", dto.getGracePeriod()) != null) {
            validateIntegerLimits(errors, "gracePeriod", "label.customerCard.gracePeriod.cleaned", dto.getGracePeriod(), 0,
                                  WebCoreConstants.CUSTOM_CARD_GRACE_PERIOD_MAX);
        }
        
        // Validate Max number of usage
        if (validateNumber(errors, "maxNumOfUses", "label.customerCard.maxNumOfUses", dto.getMaxNumOfUses()) != null) {
            validateIntegerLimits(errors, "maxNumOfUses", "label.customerCard.maxNumOfUses", dto.getMaxNumOfUses(), 1,
                                  WebCoreConstants.CUSTOM_CARD_MAX_NUM_OF_USES);
        }
        
        // Validate Comment
        validateExternalDescription(errors, "description", "label.customerCard.comment", dto.getComment(), 0);
    }
    
}
