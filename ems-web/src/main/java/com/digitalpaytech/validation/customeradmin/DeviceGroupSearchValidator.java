package com.digitalpaytech.validation.customeradmin;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.client.dto.DeviceGroupFilter;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.validation.BaseValidator;

@Component("deviceGroupSearchValidator")
public class DeviceGroupSearchValidator extends BaseValidator<DeviceGroupFilter> {
    @Override
    public void validate(final WebSecurityForm<DeviceGroupFilter> target, final Errors errors) {
        // @ TODO Implement this when the filter feature has been implemented.
    }
}
