package com.digitalpaytech.validation.customeradmin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.ActivityLog;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.customeradmin.support.ActivityLogFilterForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.validation.BaseValidator;

@Component("activityLogFormValidator")
public class ActivityLogFormValidator extends BaseValidator<ActivityLogFilterForm> {
    private static Logger log = Logger.getLogger(ActivityLogFormValidator.class);
    
    @Override
    public void validate(WebSecurityForm<ActivityLogFilterForm> command, Errors errors) {        
    }
    
    public void validate(WebSecurityForm<ActivityLogFilterForm> command, Errors errors, RandomKeyMapping keyMapping) {
        
        WebSecurityForm<ActivityLogFilterForm> secForm = super.prepareSecurityForm(command, errors, "postToken");
        if(secForm == null){
            return;
        }
        
        ActivityLogFilterForm activityLogFilter = (ActivityLogFilterForm) secForm.getWrappedObject();
        this.validateSettings(secForm, errors, keyMapping);
        if (!StringUtils.isBlank(activityLogFilter.getPage())) {
        	super.validateNumber(secForm, "page", "label.page", activityLogFilter.getPage());
            super.validateLesserValue(secForm, "0", "label.zero", 0, "page", "label.page", Integer.parseInt(activityLogFilter.getPage()));
        } else {
            activityLogFilter.setPage("1");
        }
        
        if (!StringUtils.isBlank(activityLogFilter.getNumOfRecord())) {
        	super.validateNumber(secForm, "numOfRecord", "label.numOfRecord", activityLogFilter.getNumOfRecord());
            super.validateLesserValue(secForm, "0", "label.zero", 0, "numOfRecord", "label.numOfRecord", Integer.parseInt(activityLogFilter.getNumOfRecord()));
        } else {
            activityLogFilter.setNumOfRecord("30");
        }
    }    
    
    private void validateSettings(WebSecurityForm<ActivityLogFilterForm> webSecurityForm, Errors errors, RandomKeyMapping keyMapping) {                
        ActivityLogFilterForm activityLogFilter = webSecurityForm.getWrappedObject();
        
        if (activityLogFilter.getSelectedUserAccounts() != null && !StringUtils.isEmpty(activityLogFilter.getSelectedUserAccounts()) && !"All".equals(activityLogFilter.getSelectedUserAccounts())) {            
            Object userId = super.validateRandomId(webSecurityForm, "userAccount", "label.recentActivity.user", 
                                        activityLogFilter.getSelectedUserAccounts(), keyMapping, UserAccount.class, Integer.class);
            activityLogFilter.setSelectedUserAccountsRealValue((Integer) userId);
        }
        
        if (activityLogFilter.getSelectedLogTypes() != null && !StringUtils.isEmpty(activityLogFilter.getSelectedLogTypes())) {
            Object logId = super.validateRandomId(webSecurityForm, "logType", "label.recentActivity.activityLog", 
                                                  activityLogFilter.getSelectedLogTypes(), keyMapping, ActivityLog.class, Integer.class);
            activityLogFilter.setSelectedLogTypesRealValue((Integer) logId);
        }
        
        if (activityLogFilter.getSelectedDateRange() != null && !activityLogFilter.getSelectedDateRange().isEmpty()) {
            String date = activityLogFilter.getSelectedDateRange();
            
            if (!(date.equalsIgnoreCase(getMessage("label.all"))                                        // All 
                    || date.equalsIgnoreCase(getMessage("label.date.range.option.LAST_24_HOURS"))       // Last 24 Hours 
                    || date.equalsIgnoreCase(getMessage("label.date.range.option.THIS_WEEK"))           // Last Week 
                    || date.equalsIgnoreCase(getMessage("label.date.range.option.LAST_WEEK"))           // Last Week 
                    || date.equalsIgnoreCase(getMessage("label.date.range.option.LAST_2_WEEKS"))        // Last 2 Weeks 
                    || date.equalsIgnoreCase(getMessage("label.date.range.option.LAST_MONTH"))          // Last Month
                    || date.equalsIgnoreCase(getMessage("label.date.range.option.LAST_3_MONTHS"))       // Last 3 Months
                    || date.equalsIgnoreCase(getMessage("label.date.range.option.LAST_6_MONTHS"))       // Last 6 Months 
                    || date.equalsIgnoreCase(getMessage("label.date.range.option.LAST_YEAR"))           // Last Year
                    || date.equalsIgnoreCase(getMessage("label.date.range.option.CUSTOM_DATE_RANGE")))) {     // Custom Date Range
                if (log.isDebugEnabled()) {
                    log.debug("activityLogFilterForm.getSelectedDateRanges(): invalid date range " + date);
                }
                webSecurityForm.addErrorStatus(new FormErrorStatus("dateRange", this.getMessage("error.invalid.config")));
            }
            if (date.equals("Custom Date Range")) {
                String startDate = activityLogFilter.getCustomDateStart();
                String endDate = activityLogFilter.getCustomDateEnd();
                
                SimpleDateFormat sdf = new SimpleDateFormat(WebCoreConstants.DATE_UI_FORMAT);
                try {
                    @SuppressWarnings("unused")
                    Date testDate = sdf.parse(startDate);
                    @SuppressWarnings("unused")
                    Date testDate2 = sdf.parse(endDate);
                } catch (ParseException e) {
                    if (log.isDebugEnabled()) {
                        log.debug("activityLogFilterForm.getCustomDateStart(): invalid start date " + startDate);
                        log.debug("activityLogFilterForm.getCustomDateEnd(): invalid end date " + endDate);
                    }
                    webSecurityForm.addErrorStatus(new FormErrorStatus("dateRange", this.getMessage("error.invalid.config")));
                }
                ;
            }
        }
    }
}
