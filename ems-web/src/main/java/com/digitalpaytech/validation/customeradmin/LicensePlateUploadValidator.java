package com.digitalpaytech.validation.customeradmin;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.mvc.customeradmin.support.LicensePlateUploadForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.validation.BaseValidator;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Component("licensePlateUploadValidator")
public class LicensePlateUploadValidator extends BaseValidator<LicensePlateUploadForm> {
    @Override
    public final void validate(final WebSecurityForm<LicensePlateUploadForm> target, final Errors errors) {
        final WebSecurityForm<LicensePlateUploadForm> form = super.prepareSecurityForm(target, errors, WebCoreConstants.POSTTOKEN_STRING);
        if (form != null) {
            final LicensePlateUploadForm uploadForm = form.getWrappedObject();
            
            validateRequired(form, FormFieldConstants.FILE, "label.settings.preferredparking.importForm.sourceFile",
                             uploadForm.getFile().getOriginalFilename());
        }
    }
}
