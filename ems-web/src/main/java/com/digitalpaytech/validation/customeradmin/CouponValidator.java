package com.digitalpaytech.validation.customeradmin;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.web.multipart.MultipartFile;

import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.BaseValidator;
import com.digitalpaytech.mvc.customeradmin.support.CouponForm;

@Component("couponValidator")
public class CouponValidator extends BaseValidator<CouponForm> {
    @Override
    public void validate(WebSecurityForm<CouponForm> command, Errors errors) {
        
    }
        
    public void validate(WebSecurityForm<CouponForm> command, Errors errors, RandomKeyMapping keyMapping) {        
        WebSecurityForm<CouponForm> secureForm = super.prepareSecurityForm(command, errors, "postToken");
        if (secureForm == null) {
            return;
        }
        
        if(!super.validateActionFlag(secureForm, WebSecurityConstants.ACTION_UPDATE, WebSecurityConstants.ACTION_CREATE)){            
            return;
        }        

        checkIds(secureForm, keyMapping);
        CouponForm couponForm = secureForm.getWrappedObject();
        checkCouponCode(couponForm, secureForm, errors);        
        checkPercentOrDollarDiscount(couponForm, secureForm, errors);
        checkDateRange(couponForm, secureForm, errors);
        checkUsesRemaining(couponForm, secureForm, errors);
        checkAvailableFor(couponForm, secureForm, errors);
        couponForm.setDescription(super.validateExternalDescription(secureForm, "description", "label.coupon.description", couponForm.getDescription(), WebCoreConstants.VALIDATION_MIN_LENGTH_1));
    }
    
    /**
     * Converts random id from form into a database id that is saved for later use
     * 
     * @param webSecurityForm WebSecurityForm object
     */ 
    private void checkIds(WebSecurityForm<CouponForm> webSecurityForm, RandomKeyMapping keyMapping) {        
        CouponForm form = webSecurityForm.getWrappedObject();        
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
            String randomId = (String) super.validateRequired(webSecurityForm, "id", "label.coupon.identifier",  form.getRandomId());
            Integer id = super.validateRandomId(webSecurityForm, "couponId", "label.coupon.id", randomId, keyMapping, Coupon.class, Integer.class);
            form.setId(id);                                    
        }
                
        String customerRandomId = (String) super.validateRequired(webSecurityForm, "customerId", "label.customerAdmin.customer.id",  form.getCustomerRandomId());
        Integer customerId = super.validateRandomId(webSecurityForm, "customerId", "label.customerAdmin.customer.id", customerRandomId, keyMapping, Customer.class, Integer.class);
        form.setCustomerId(customerId);
        
        if(!WebCoreUtil.isEmpty(form.getLocationRandomId())){
            Integer locationId = super.validateRandomId(webSecurityForm, "locationId", "label.location", form.getLocationRandomId(), keyMapping, Location.class, Integer.class, "0");
            form.setLocationId(locationId);            
        }        
    }       
    
    public boolean validateCouponMultipartFile(MultipartFile file) {
        if (StringUtils.isBlank(file.getOriginalFilename())) {
            return false;
        }
        return true;
    }
    
    public boolean validateCreateUpdateOrDeleteOption(String option) {
        return (StringUtils.isBlank(option) || "replace".equalsIgnoreCase(option) || "merge".equalsIgnoreCase(option));
    }
    
    private void checkCouponCode(CouponForm couponForm, WebSecurityForm<CouponForm> webSecurityForm, Errors errors) {
    	if(webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_CREATE) {
    		couponForm.setCouponCode((String) super.validateRequired(webSecurityForm, "couponCode", "label.coupon.number",  couponForm.getCouponCode()));
            super.validateStringLength(webSecurityForm, "couponCode", "label.coupon.number", couponForm.getCouponCode(), WebCoreConstants.COUPON_LENGTH_MIN, WebCoreConstants.COUPON_LENGTH_MAX);
            super.validateAlphaNumeric(webSecurityForm, "couponCode", "label.coupon.number", couponForm.getCouponCode());
    	}
    }
        
    private void checkPercentOrDollarDiscount(CouponForm couponForm, WebSecurityForm<CouponForm> webSecurityForm, Errors errors) {
        String label;
        if (StringUtils.isNotBlank(couponForm.getDiscountType()) && couponForm.getDiscountType().equalsIgnoreCase(WebCoreConstants.DISCOUNT_TYPE_DOLLARS)) {
            label = "label.coupon.discount.dollar";
        } else {
            label = "label.coupon.discount.percentage";
        }
                
        String discount_string = couponForm.getDiscountValue();
        if (StringUtils.isNotBlank(discount_string)) {
            discount_string = discount_string.trim();
        }
        
        if (couponForm.getDiscountType().equalsIgnoreCase(WebCoreConstants.DISCOUNT_TYPE_PERCENT)) {                        
            super.validateIntegerLimits(webSecurityForm, "discountValue", label, discount_string, 
                                        WebCoreConstants.COUPON_DISCOUNT_MIN, WebCoreConstants.COUPON_DISCOUNT_MAX);
        } else if (couponForm.getDiscountType().equalsIgnoreCase(WebCoreConstants.DISCOUNT_TYPE_DOLLARS)) {
            String discountAmt = super.validateDollarAmt(webSecurityForm, "discountValue", label, discount_string);
            if (discountAmt != null){
                super.validateDoubleLimits(webSecurityForm, "discountValue", label, discountAmt, 
                                           WebCoreConstants.COUPON_DISCOUNT_MIN, WebCoreConstants.COUPON_DOLLARS_DISCOUNT_MAX);
            }
        }
    }

    
    private void checkDateRange(CouponForm couponForm, WebSecurityForm<CouponForm> webSecurityForm, Errors errors) {
        TimeZone timeZone = TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone());
        Date currentDate = DateUtil.removeTime(System.currentTimeMillis());
        Date startDate = null;
  
        if (!WebCoreUtil.isEmpty(couponForm.getStartDate())) {
            startDate = super.validateDate(webSecurityForm, "startDate", "label.coupon.startDate", couponForm.getStartDate(), timeZone);            
            if (startDate != null) {               
            	startDate = validateDateLimits(webSecurityForm, "startDate", "label.coupon.startDate", startDate
            			, WebCoreConstants.COUPON_DATE_MIN, WebCoreConstants.COUPON_DATE_MAX
            			, WebCoreConstants.COUPON_DATE_MIN_STRING, WebCoreConstants.COUPON_DATE_MAX_STRING);
            }
        }
        
        Date endDate = null;
        
        if (!WebCoreUtil.isEmpty(couponForm.getEndDate())) {
            endDate = super.validateDate(webSecurityForm, "endDate", "label.coupon.endDate", couponForm.getEndDate(), timeZone);
            if (endDate != null) {               
            	endDate = validateDateLimits(webSecurityForm, "endDate", "label.coupon.endDate", endDate
            			, currentDate, (Date) null
            			, getMessage("label.coupon.currentDate"), (String) null);
            }
            if(endDate != null) {
            	endDate = validateDateLimits(webSecurityForm, "endDate", "label.coupon.endDate", endDate
            			, (Date) null, WebCoreConstants.COUPON_DATE_MAX
            			, (String) null, WebCoreConstants.COUPON_DATE_MAX_STRING);
            }
        }
        
        // Check range
        if ((startDate != null) && (endDate != null)) {
        	validateDateLimits(webSecurityForm, "startDate", "label.coupon.startDate", startDate, (Date) null, endDate, (String) null, getMessage("label.coupon.endDate"));            
        }
    }
    
    private void checkUsesRemaining(CouponForm couponForm, WebSecurityForm<CouponForm> webSecurityForm, Errors errors) {
        
        String num_uses_label = getMessage("label.coupon.maxNumOfUses");
        
        if(validateRequired(webSecurityForm, "numUsesType", "label.coupon.maxNumOfUses", couponForm.getNumUsesType()) != null) {
        	switch(Character.toUpperCase(couponForm.getNumUsesType())) {
            case 'S': {
            	validateRequired(webSecurityForm, "useNumUses", "label.coupon.maxNumOfUses", couponForm.getNumUses());
            	if(validateNumber(webSecurityForm, "useNumUses", "webSecurityForm", couponForm.getNumUses()) != null) {
            		int num_uses = Integer.parseInt(couponForm.getNumUses());                
                    // Check value
                    if (num_uses < WebCoreConstants.COUPON_USES_MIN || num_uses > WebCoreConstants.COUPON_USES_MAX) {
                        super.addErrorStatus(webSecurityForm, "useNumUses", "error.common.invalid.range", new Object[] { num_uses_label, WebCoreConstants.COUPON_USES_MIN,
                                WebCoreConstants.COUPON_USES_MAX });                    
                        couponForm.setNumUses("");
                    }
            	}
            	
            	break;
            }
            case 'U': {
            	break;
            }
            default: {
            	super.addErrorStatus(webSecurityForm, "numUsesType", "error.common.invalid", num_uses_label);
            }
            }
        }
    }
    
    private void checkAvailableFor(CouponForm couponForm, WebSecurityForm<CouponForm> webSecurityForm, Errors errors) {
        boolean pbs_enabled = couponForm.isPbsEnabled();
        boolean pnd_enabled = couponForm.isPndEnabled();
        if (!pbs_enabled && !pnd_enabled) {
            super.addErrorStatus(webSecurityForm, "payOptions", "error.common.required.atLeast1Of2",  new Object[] { getMessage("label.coupon.available.pnd"), getMessage("label.coupon.available.pbs") });
        }        
        if (pbs_enabled) {
//        	if(super.validateRequired(webSecurityForm, "stallRange", "label.coupon.spaceRange", couponForm.getStallRange()) != null) {
	            String stallRange = couponForm.getStallRange();
	            if (!StringUtils.isBlank(stallRange)){
	                stallRange = super.validateStringLength(webSecurityForm, "stallRange", "label.coupon.spaceRange", stallRange, WebCoreConstants.VALIDATION_MIN_LENGTH_1,
	                                                        WebCoreConstants.COUPON_STALL_RANGES_LENGTH_MAX);            
	                if (stallRange != null){
	                	validateIntegerRangeString(webSecurityForm, "stallRange", "label.coupon.spaceRange", couponForm.getStallRange(), WebCoreConstants.STALL_NUM_MIN, WebCoreConstants.STALL_NUM_MAX);
	                }
	            }
//        	}
        }
    }
    
    /**
     * Validate coupon code following these rules:
     *  - null or empty
     *  - length
     *  - format (alphanumeric)
     * @param couponCode Coupon code
     * @return boolean return 'false' if it's not valid.
     */
    public boolean validateCouponCode(String couponCode) {
        if (StringUtils.isBlank(couponCode)) {
            return false;
        } 
        if (couponCode.length() > WebCoreConstants.COUPON_LENGTH_MAX) {
            return false;
        } 
        if (!couponCode.matches(WebCoreConstants.REGULAR_EXPRESSION_FORMAT)) {
            return false;
        }
        return true;
    }    
}
