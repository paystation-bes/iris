package com.digitalpaytech.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.RateProfile;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.RateProfileEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.RateProfileService;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

@Component("rateProfileValidator")
public class RateProfileValidator extends BaseValidator<RateProfileEditForm> {
    private static final int MIN_PERMIT_ISSUE_TYPE = 1;
    private static final int MAX_PERMIT_ISSUE_TYPE = 3;
    
    @Autowired
    private RateProfileService rateProfileService;
    
    @Override
    public void validate(final WebSecurityForm<RateProfileEditForm> command, final Errors errors) {
        
    }
    
    public final void validate(final WebSecurityForm<RateProfileEditForm> command, final Errors errors, final Integer customerId) {
        //JOptionPane.showMessageDialog(null, "Inside as Validator");
        final WebSecurityForm<RateProfileEditForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
        
        if (!super.validateActionFlag(webSecurityForm, WebSecurityConstants.ACTION_UPDATE, WebSecurityConstants.ACTION_CREATE)) {
            return;
        }
        
        checkId(webSecurityForm);
        
        validateRateProfileName(webSecurityForm, customerId);
        validatePermitIssueType(webSecurityForm);
    }
    
    /**
     * Converts random id from form into a database id that is saved for later use
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     */
    private void checkId(final WebSecurityForm<RateProfileEditForm> webSecurityForm) {
        final RateProfileEditForm form = webSecurityForm.getWrappedObject();
        final RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
            validateRequired(webSecurityForm, "randomId", "label.settings.rates.rate", form.getRandomId());
            final Integer id = super.validateRandomId(webSecurityForm, "randomId", "label.settings.rates.rateProfile", form.getRandomId(),
                keyMapping, RateProfile.class, Integer.class);
            form.setRateProfileId(id);
        }
    }
    
    /**
     * It validates user input alert name
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     */
    private void validateRateProfileName(final WebSecurityForm<RateProfileEditForm> webSecurityForm, final Integer customerId) {
        final RateProfileEditForm form = webSecurityForm.getWrappedObject();
        final String rateName = form.getName();
        
        final Object requiredCheck = super.validateRequired(webSecurityForm, "rateProfileName", "label.settings.rates.profile.profileName", rateName);
        if (requiredCheck != null) {
            super.validateStringLength(webSecurityForm, "rateProfileName", "label.settings.rates.profile.profileName", rateName,
                WebCoreConstants.VALIDATION_MIN_LENGTH_1, WebCoreConstants.VALIDATION_MAX_LENGTH_20);
            super.validatePrintableText(webSecurityForm, "rateProfileName", "label.settings.rates.profile.profileName", rateName);
            if (rateName != null) {
                form.setName(rateName.trim());
            }
            
            final RateProfile existingRateProfile = this.rateProfileService.findRateProfileByCustomerIdAndPermitIssueTypeIdAndName(customerId,
                form.getPermitIssueTypeId(), form.getName().trim());
            
            if (existingRateProfile != null) {
                if (!existingRateProfile.getId().equals(form.getRateProfileId())) {
                    final FormErrorStatus status = new FormErrorStatus("rateProfileName", this.getMessage("error.common.duplicated", new Object[] {
                        this.getMessage("label.settings.rates.rateProfile"), this.getMessage("label.settings.rates.profile.profileName") }));
                    webSecurityForm.getValidationErrorInfo().addErrorStatus(status);
                }
            }
            
        }
        return;
    }
    
    /**
     * It validates user input permit issue type.
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     * @param keyMapping
     *            RandomKeyMapping object
     */
    private void validatePermitIssueType(final WebSecurityForm<RateProfileEditForm> webSecurityForm) {
        final RateProfileEditForm form = webSecurityForm.getWrappedObject();
        Integer typeId = form.getPermitIssueTypeId();
        typeId = super.validateRequired(webSecurityForm, "permitIssueTypeId", "label.settings.rates.operatingMode", typeId, "0", "-1",
            Integer.valueOf(0), Integer.valueOf(-1));
        if (typeId == null) {
            return;
        } else {
            typeId = super.validateIntegerLimits(webSecurityForm, "permitIssueTypeId", "label.settings.rates.operatingMode", typeId,
                MIN_PERMIT_ISSUE_TYPE, MAX_PERMIT_ISSUE_TYPE);
        }
    }
    
}
