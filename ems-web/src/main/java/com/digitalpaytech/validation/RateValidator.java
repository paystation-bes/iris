package com.digitalpaytech.validation;

import java.util.Date;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.Rate;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.RateEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.RateService;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

@Component("rateValidator")
@SuppressWarnings({ "checkstyle:multiplestringliterals" })
public class RateValidator extends BaseValidator<RateEditForm> {
    private static final int MIN_RATE_TYPE = 0;
    private static final int MAX_RATE_TYPE = 5;
    private static final int MIN_EBP_MIN_EXT = 1;
    private static final int MAX_EBP_MIN_EXT = 99;
    private static final int MIN_CC_INC = 1;
    private static final int MAX_CC_INC = 60;
    
    @Autowired
    private RateService rateService;
    
    @Override
    public void validate(final WebSecurityForm<RateEditForm> command, final Errors errors) {
        
    }
    
    public final void validate(final WebSecurityForm<RateEditForm> command, final Errors errors, final Integer customerId,
        final RandomKeyMapping keyMapping) {
        //JOptionPane.showMessageDialog(null, "Inside as Validator");
        final WebSecurityForm<RateEditForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
        
        if (!super.validateActionFlag(webSecurityForm, WebSecurityConstants.ACTION_UPDATE, WebSecurityConstants.ACTION_CREATE)) {
            return;
        }
        
        checkId(webSecurityForm);
        
        if (validateRateType(webSecurityForm, keyMapping)) {
            validateRateName(webSecurityForm, customerId, true);
        } else {
            validateRateName(webSecurityForm, customerId, false);
        }
    }
    
    /**
     * Converts random id from form into a database id that is saved for later use
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     */
    private void checkId(final WebSecurityForm<RateEditForm> webSecurityForm) {
        final RateEditForm form = webSecurityForm.getWrappedObject();
        final RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
            validateRequired(webSecurityForm, "randomId", "label.settings.rates.rate", form.getRandomId());
            final Integer id = super.validateRandomId(webSecurityForm, "randomId", "label.settings.rates.rate", form.getRandomId(), keyMapping,
                Rate.class, Integer.class);
            form.setRateId(id);
        }
    }
    
    /**
     * It validates user input alert name
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     */
    private void validateRateName(final WebSecurityForm<RateEditForm> webSecurityForm, final Integer customerId, final boolean isDBCheck) {
        final RateEditForm form = webSecurityForm.getWrappedObject();
        final String rateName = form.getName().trim();
        
        final Object requiredCheck = super.validateRequired(webSecurityForm, "rateName", "label.settings.rates.rateName", rateName);
        if (requiredCheck != null) {
            super.validateStringLength(webSecurityForm, "rateName", "label.settings.rates.rateName", rateName,
                WebCoreConstants.VALIDATION_MIN_LENGTH_1, WebCoreConstants.VALIDATION_MAX_LENGTH_20);
            super.validatePrintableText(webSecurityForm, "rateName", "label.settings.rates.rateName", rateName);
            if (rateName != null) {
                form.setName(rateName.trim());
            }
            
            if (isDBCheck) {
                final Rate existingRate = this.rateService.findRateByCustomerIdAndName(customerId, form.getRateType().byteValue(), form.getName()
                        .trim());
                
                if (existingRate != null) {
                    if (!existingRate.getId().equals(form.getRateId())) {
                        final FormErrorStatus status = new FormErrorStatus("rateName", this.getMessage("error.common.duplicated",
                            new Object[] { this.getMessage("label.rate"), this.getMessage("label.settings.rates.rateName"), }));
                        webSecurityForm.getValidationErrorInfo().addErrorStatus(status);
                    }
                }
            }
            
        }
        return;
    }
    
    /**
     * It validates user input alert type.
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     * @param keyMapping
     *            RandomKeyMapping object
     */
    @SuppressWarnings({ "checkstyle:cyclomaticcomplexity" })
    private boolean validateRateType(final WebSecurityForm<RateEditForm> webSecurityForm, final RandomKeyMapping keyMapping) {
        final RateEditForm form = webSecurityForm.getWrappedObject();
        Integer typeId = form.getRateType();
        typeId = super.validateRequired(webSecurityForm, "rateType", "label.settings.rates.rateType", typeId, 0, -1);
        if (typeId == null) {
            return false;
        } else {
            typeId = super.validateIntegerLimits(webSecurityForm, "rateType", "label.settings.rates.rateType", typeId, MIN_RATE_TYPE, MAX_RATE_TYPE);
        }
        if (form.getRateType() != WebCoreConstants.RATE_TYPE_PARKING_RESTRICTION) {
            validatePaymentType(webSecurityForm);
        }
        
        if (form.getRateType() != WebCoreConstants.RATE_TYPE_PARKING_RESTRICTION && form.getRateType() != WebCoreConstants.RATE_TYPE_MONTHLY) {
            validateAddTime(webSecurityForm);
        } else {
            form.setEnableAddTime(false);
        }
        
        switch (typeId.byteValue()) {
            case WebCoreConstants.RATE_TYPE_FLAT:
                validateFlat(webSecurityForm);
                break;
            case WebCoreConstants.RATE_TYPE_DAILY:
                validateDaily(webSecurityForm);
                break;
            case WebCoreConstants.RATE_TYPE_HOURLY:
                validateHourly(webSecurityForm);
                break;
            case WebCoreConstants.RATE_TYPE_INCREMENTAL:
                validateIncremental(webSecurityForm);
                break;
            case WebCoreConstants.RATE_TYPE_MONTHLY:
                validateMonthly(webSecurityForm);
                break;
            case WebCoreConstants.RATE_TYPE_PARKING_RESTRICTION:
                validateRestriction(webSecurityForm);
                break;
            default:
                break;
        }
        return true;
    }
    
    private void validatePaymentType(final WebSecurityForm<RateEditForm> webSecurityForm) {
        final RateEditForm form = webSecurityForm.getWrappedObject();
        if (!form.getPaymentBill() && !form.getPaymentCoin() && !form.getPaymentCC() && !form.getPaymentSC() && !form.getPaymentRF()
            && !form.getPaymentPC() && !form.getPaymentCoupon()) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("paymentCoin", this.getMessage("error.settings.rates.paymentTypeMissing")));
        }
        if (form.getPaymentCoupon()) {
            validateRequired(webSecurityForm, "promptForCoupon", "label.settings.rates.promptForCoupons", form.getPromptForCoupon());
        }
    }
    
    private void validateAddTime(final WebSecurityForm<RateEditForm> webSecurityForm) {
        final RateEditForm form = webSecurityForm.getWrappedObject();
        if (form.getEnableAddTime()) {
            if (!form.getUseAddTimeNumber() && !form.getUseSpaceOrPlate() && !form.getUseEbP()) {
                webSecurityForm.addErrorStatus(new FormErrorStatus("useAddTimeNumber", this.getMessage("error.settings.rates.addTimeOrSpacePlate",
                    this.getMessage("label.settings.rates.useAddTime"), this.getMessage("label.settings.rates.useSpacePlate"),
                    this.getMessage("label.settings.rates.useEbP"))));
            }
            if (form.getUseEbP()) {
                super.validateRequired(webSecurityForm, "ebPServiceFeeAmount", "label.settings.rates.serviceFee", form.getEbPServiceFeeAmount());
                if (form.getEbPServiceFeeAmount() != null) {
                    super.validateDollarAmt(webSecurityForm, "ebPServiceFeeAmount", "label.settings.rates.serviceFee", form.getEbPServiceFeeAmount());
                }
                super.validateRequired(webSecurityForm, "ebPMiniumExt", "label.settings.rates.minimumExtension", form.getEbPMiniumExt());
                if (form.getEbPMiniumExt() != null) {
                    super.validateIntegerLimits(webSecurityForm, "ebPMiniumExt", "label.settings.rates.minimumExtension", form.getEbPMiniumExt()
                            .intValue(), MIN_EBP_MIN_EXT, MAX_EBP_MIN_EXT);
                }
            }
        }
    }
    
    //TODO
    private void validateFlat(final WebSecurityForm<RateEditForm> webSecurityForm) {
        final RateEditForm form = webSecurityForm.getWrappedObject();
    }
    
    //TODO
    private void validateDaily(final WebSecurityForm<RateEditForm> webSecurityForm) {
        final RateEditForm form = webSecurityForm.getWrappedObject();
    }
    
    //TODO
    private void validateHourly(final WebSecurityForm<RateEditForm> webSecurityForm) {
        final RateEditForm form = webSecurityForm.getWrappedObject();
    }
    
    @SuppressWarnings({ "checkstyle:npathcomplexity", "checkstyle:cyclomaticcomplexity" })
    private void validateIncremental(final WebSecurityForm<RateEditForm> webSecurityForm) {
        final RateEditForm form = webSecurityForm.getWrappedObject();
        super.validateRequired(webSecurityForm, "rateAmount", "label.settings.rates.rateAmount", form.getRateAmount());
        if (form.getRateAmount() != null) {
            super.validateDollarAmt(webSecurityForm, "rateAmount", "label.settings.rates.rateAmount", form.getRateAmount());
        }
        super.validateRequired(webSecurityForm, "minPaymentAmount", "label.settings.rates.minimumPayment", form.getMinPaymentAmount());
        Double minValue = null;
        Double maxValue = null;
        if (form.getMinPaymentAmount() != null) {
            super.validateDollarAmt(webSecurityForm, "minPaymentAmount", "label.settings.rates.minimumPayment", form.getMinPaymentAmount());
            try {
                minValue = Double.parseDouble(form.getMinPaymentAmount());
            } catch (NumberFormatException nfe) {
                // ignore if fails
                minValue = null;
            }
            
        }
        if (form.getMaxPaymentAmount() != null) {
            super.validateDollarAmt(webSecurityForm, "maxPaymentAmount", "label.settings.rates.maximumPayment", form.getMaxPaymentAmount());
            try {
                maxValue = Double.parseDouble(form.getMaxPaymentAmount());
            } catch (NumberFormatException nfe) {
                // ignore if fails
                maxValue = null;
            }
            if (minValue != null && maxValue != null && minValue > maxValue) {
                webSecurityForm.addErrorStatus(new FormErrorStatus("maxPaymentAmount", this.getMessage("label.settings.rates.maxMinErrorMsg")));
                
            }
        }
        
        if (form.getUseFlatRate()) {
            super.validateRequired(webSecurityForm, "flatRateAmount", "label.settings.rates.flatRateAmount", form.getFlatRateAmount());
            if (form.getFlatRateAmount() != null) {
                super.validateDollarAmt(webSecurityForm, "flatRateAmount", "label.settings.rates.flatRateAmount", form.getFlatRateAmount());
            }
            super.validateRequired(webSecurityForm, "flatRateStartTime", "label.settings.rates.startTime", form.getFlatRateStartTime());
            final Date startTime = super.validateTime(webSecurityForm, "flatRateStartTime", "label.settings.rates.startTime",
                form.getFlatRateStartTime(), TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone()));
            super.validateRequired(webSecurityForm, "flatRateEndTime", "label.settings.rates.endTime", form.getFlatRateEndTime());
            final Date endTime = super.validateTime(webSecurityForm, "flatRateEndTime", "label.settings.rates.endTime", form.getFlatRateEndTime(),
                TimeZone.getTimeZone(WebSecurityUtil.getCustomerTimeZone()));
            
            if ((startTime != null) && (endTime != null) && !(startTime.before(endTime))) {
                final FormErrorStatus status = new FormErrorStatus("flatRateStartTime", this.getMessage("error.common.invalid.date.not.lesser",
                    new Object[] { this.getMessage("label.settings.rates.endTime"), this.getMessage("label.settings.rates.startTime"), }));
                webSecurityForm.getValidationErrorInfo().addErrorStatus(status);
            }
        }
        if (form.getPaymentCC()) {
            if (form.getUseInc01()) {
                super.validateRequired(webSecurityForm, "minsInc01", "label.settings.rates.mins.01", form.getMinsInc01());
                if (form.getMinsInc01() != null) {
                    super.validateNumber(webSecurityForm, "minsInc01", "label.settings.rates.mins.01", form.getMinsInc01());
                    super.validateIntegerLimits(webSecurityForm, "minsInc01", "label.settings.rates.mins.01", form.getMinsInc01(), MIN_CC_INC,
                        MAX_CC_INC);
                }
            }
            if (form.getUseInc02()) {
                super.validateRequired(webSecurityForm, "minsInc02", "label.settings.rates.mins.02", form.getMinsInc02());
                if (form.getMinsInc02() != null) {
                    super.validateNumber(webSecurityForm, "minsInc02", "label.settings.rates.mins.02", form.getMinsInc02());
                    super.validateIntegerLimits(webSecurityForm, "minsInc02", "label.settings.rates.mins.02", form.getMinsInc02(), MIN_CC_INC,
                        MAX_CC_INC);
                }
            }
            if (form.getUseInc03()) {
                super.validateRequired(webSecurityForm, "minsInc03", "label.settings.rates.mins.03", form.getMinsInc03());
                if (form.getMinsInc03() != null) {
                    super.validateNumber(webSecurityForm, "minsInc03", "label.settings.rates.mins.03", form.getMinsInc03());
                    super.validateIntegerLimits(webSecurityForm, "minsInc03", "label.settings.rates.mins.03", form.getMinsInc03(), MIN_CC_INC,
                        MAX_CC_INC);
                }
            }
            if (form.getUseInc04()) {
                super.validateRequired(webSecurityForm, "minsInc04", "label.settings.rates.mins.04", form.getMinsInc04());
                if (form.getMinsInc04() != null) {
                    super.validateNumber(webSecurityForm, "minsInc04", "label.settings.rates.mins.04", form.getMinsInc04());
                    super.validateIntegerLimits(webSecurityForm, "minsInc04", "label.settings.rates.mins.04", form.getMinsInc04(), MIN_CC_INC,
                        MAX_CC_INC);
                }
            }
            if (form.getUseInc05()) {
                super.validateRequired(webSecurityForm, "minsInc05", "label.settings.rates.mins.05", form.getMinsInc05());
                if (form.getMinsInc05() != null) {
                    super.validateNumber(webSecurityForm, "minsInc05", "label.settings.rates.mins.05", form.getMinsInc05());
                    super.validateIntegerLimits(webSecurityForm, "minsInc05", "label.settings.rates.mins.05", form.getMinsInc05(), MIN_CC_INC,
                        MAX_CC_INC);
                }
            }
            if (form.getUseInc06()) {
                super.validateRequired(webSecurityForm, "minsInc06", "label.settings.rates.mins.06", form.getMinsInc06());
                if (form.getMinsInc06() != null) {
                    super.validateNumber(webSecurityForm, "minsInc06", "label.settings.rates.mins.06", form.getMinsInc06());
                    super.validateIntegerLimits(webSecurityForm, "minsInc06", "label.settings.rates.mins.06", form.getMinsInc06(), MIN_CC_INC,
                        MAX_CC_INC);
                }
            }
            if (form.getUseInc07()) {
                super.validateRequired(webSecurityForm, "minsInc07", "label.settings.rates.mins.07", form.getMinsInc07());
                if (form.getMinsInc07() != null) {
                    super.validateNumber(webSecurityForm, "minsInc07", "label.settings.rates.mins.07", form.getMinsInc07());
                    super.validateIntegerLimits(webSecurityForm, "minsInc07", "label.settings.rates.mins.07", form.getMinsInc07(), MIN_CC_INC,
                        MAX_CC_INC);
                }
            }
            if (form.getUseInc08()) {
                super.validateRequired(webSecurityForm, "minsInc08", "label.settings.rates.mins.08", form.getMinsInc08());
                if (form.getMinsInc08() != null) {
                    super.validateNumber(webSecurityForm, "minsInc08", "label.settings.rates.mins.08", form.getMinsInc08());
                    super.validateIntegerLimits(webSecurityForm, "minsInc08", "label.settings.rates.mins.08", form.getMinsInc08(), MIN_CC_INC,
                        MAX_CC_INC);
                }
            }
            if (form.getUseInc09()) {
                super.validateRequired(webSecurityForm, "minsInc09", "label.settings.rates.mins.09", form.getMinsInc09());
                if (form.getMinsInc09() != null) {
                    super.validateNumber(webSecurityForm, "minsInc09", "label.settings.rates.mins.09", form.getMinsInc09());
                    super.validateIntegerLimits(webSecurityForm, "minsInc09", "label.settings.rates.mins.09", form.getMinsInc09(), MIN_CC_INC,
                        MAX_CC_INC);
                }
            }
            if (form.getUseInc10()) {
                super.validateRequired(webSecurityForm, "minsInc10", "label.settings.rates.mins.10", form.getMinsInc10());
                if (form.getMinsInc10() != null) {
                    super.validateNumber(webSecurityForm, "minsInc10", "label.settings.rates.mins.10", form.getMinsInc10());
                    super.validateIntegerLimits(webSecurityForm, "minsInc10", "label.settings.rates.mins.10", form.getMinsInc10(), MIN_CC_INC,
                        MAX_CC_INC);
                }
            }
            
        }
        
    }
    
    //TODO
    private void validateMonthly(final WebSecurityForm<RateEditForm> webSecurityForm) {
        final RateEditForm form = webSecurityForm.getWrappedObject();
    }
    
    //TODO
    private void validateRestriction(final WebSecurityForm<RateEditForm> webSecurityForm) {
        final RateEditForm form = webSecurityForm.getWrappedObject();
    }
    
}
