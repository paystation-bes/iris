package com.digitalpaytech.validation;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.FlexWSUserAccount;
import com.digitalpaytech.mvc.support.FlexHelper;
import com.digitalpaytech.mvc.support.FlexWSUserAccountForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;

@Component("flexCredentialValidator")
public class FlexWSUserAccountValidator extends BaseValidator<FlexWSUserAccountForm> {
    @Autowired
    private FlexHelper flexHelper;
    
    @Override
    public void validate(WebSecurityForm<FlexWSUserAccountForm> target, Errors errors) {
        final RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        
        WebSecurityForm<FlexWSUserAccountForm> secureForm = prepareSecurityForm(target, errors, "postToken");
        if(secureForm != null) {
            FlexWSUserAccountForm form = secureForm.getWrappedObject();
            
            if (!StringUtils.isBlank(form.getRandomId())) {
                validateRandomId(secureForm, "randomId", "label.flexCredentials.id", form.getRandomId(), keyMapping, FlexWSUserAccount.class, Integer.class);
            }
            
            if (validateRequired(secureForm, "url", "label.systemAdmin.workspace.licenses.flexCredentials.url", form.getUrl()) != null) {
                validateURL(secureForm, "url", "label.systemAdmin.workspace.licenses.flexCredentials.url", form.getUrl());
            }
            
            if (validateRequired(secureForm, "username", "label.systemAdmin.workspace.licenses.flexCredentials.username", form.getUsername()) != null) {
                validateStringLength(secureForm, "username", "label.systemAdmin.workspace.licenses.flexCredentials.username", form.getUsername(), 1, 30);
            }
            
            if (flexHelper.hasPasswordChanged(form)) {
                if (validateRequired(secureForm, "password", "label.systemAdmin.workspace.licenses.flexCredentials.password", form.getPassword()) != null) {
                    validateStringLength(secureForm, "password", "label.systemAdmin.workspace.licenses.flexCredentials.password", form.getPassword(), 1, 30);
                }
            }
        }
    }
}
