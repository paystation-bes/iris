package com.digitalpaytech.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.mvc.support.AlertEditForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

@Component("alertSettingValidator")
public class AlertSettingValidator extends BaseValidator<AlertEditForm> {
    private static final String ALERT_TYPE_ID = "alertTypeId";
    private static final String LABEL_SETTINGS_ALERTS_ALERT_TYPE = "label.settings.alerts.alertType";
    
    @Override
    public void validate(final WebSecurityForm<AlertEditForm> command, final Errors errors) {
        
    }
    
    public void validate(final WebSecurityForm<AlertEditForm> command, final Errors errors, final RandomKeyMapping keyMapping) {
        
        final WebSecurityForm<AlertEditForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
        
        if (!super.validateActionFlag(webSecurityForm, WebSecurityConstants.ACTION_UPDATE, WebSecurityConstants.ACTION_CREATE)) {
            return;
        }
        
        checkId(webSecurityForm);
        
        this.validateAlertName(webSecurityForm);
        this.validateAlertType(webSecurityForm, keyMapping);
        this.validateThreshold(webSecurityForm, keyMapping);
        this.validateRoute(webSecurityForm, keyMapping);
        this.validateEmail(webSecurityForm);
        this.validateDelayed(webSecurityForm);
    }
    
    private void validateDelayed(final WebSecurityForm<AlertEditForm> webSecurityForm) {
        final AlertEditForm alertEditForm = webSecurityForm.getWrappedObject();
        if (alertEditForm.getIsDelayed()) {
            if (alertEditForm.getAlertTypeId() != null && alertEditForm.getAlertTypeId().equals(WebCoreConstants.ALERTTYPE_ID_PAYSTATIONALERT)) {
                if (alertEditForm.getDelayedByMinutes() < 1) {
                    webSecurityForm.addErrorStatus(new FormErrorStatus("label.settings.alerts.form.delayedByMinutes",
                            getMessage(ErrorConstants.ALERT_DELAY_BY_MINUTES_ZERO)));
                }
                
            } else {
                webSecurityForm
                        .addErrorStatus(new FormErrorStatus(LABEL_SETTINGS_ALERTS_ALERT_TYPE, getMessage(ErrorConstants.ALERT_CANNOT_BE_DELAYED)));
            }
        }
        
    }
    
    /**
     * Converts random id from form into a database id that is saved for later use
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     */
    private void checkId(final WebSecurityForm<AlertEditForm> webSecurityForm) {
        final AlertEditForm form = webSecurityForm.getWrappedObject();
        final RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
            validateRequired(webSecurityForm, "alertID", LABEL_SETTINGS_ALERTS_ALERT_TYPE, form.getRandomizedAlertId());
            final Integer id = super.validateRandomId(webSecurityForm, "alertID", LABEL_SETTINGS_ALERTS_ALERT_TYPE, form.getRandomizedAlertId(),
                                                      keyMapping, CustomerAlertType.class, Integer.class);
            form.setAlertId(id);
        }
    }
    
    /**
     * It validates user input alert name
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     */
    private void validateAlertName(final WebSecurityForm<AlertEditForm> webSecurityForm) {
        final AlertEditForm form = webSecurityForm.getWrappedObject();
        final String alertName = form.getAlertName();
        
        final Object requiredCheck = super.validateRequired(webSecurityForm, "alertName", "label.name", alertName);
        if (requiredCheck != null) {
            super.validateStringLength(webSecurityForm, "alertName", "label.name", alertName, WebCoreConstants.VALIDATION_MIN_LENGTH_1,
                                       WebCoreConstants.VALIDATION_MAX_LENGTH_25);
            super.validateStandardText(webSecurityForm, "alertName", "label.name", alertName);
            form.setAlertName(alertName.trim());
        }
        return;
    }
    
    /**
     * It validates user input alert type.
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     * @param keyMapping
     *            RandomKeyMapping object
     */
    private void validateAlertType(final WebSecurityForm<AlertEditForm> webSecurityForm, final RandomKeyMapping keyMapping) {
        final AlertEditForm alertEditForm = webSecurityForm.getWrappedObject();
        Short typeId = alertEditForm.getAlertTypeId();
        typeId = (Short) super.validateRequired(webSecurityForm, ALERT_TYPE_ID, LABEL_SETTINGS_ALERTS_ALERT_TYPE, typeId, "0", "-1");
        if (typeId != null) {
            typeId = (short) super.validateIntegerLimits(webSecurityForm, ALERT_TYPE_ID, LABEL_SETTINGS_ALERTS_ALERT_TYPE, typeId,
                                                         WebSecurityConstants.MIN_TYPEID, WebSecurityConstants.MAX_TYPEID);
        }
        
        alertEditForm.setAlertTypeId(null);
        if (typeId != null) {
            alertEditForm.setAlertTypeId(typeId);
        }
        
    }
    
    /**
     * It validates user input threshold.
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     * @param keyMapping
     *            RandomKeyMapping object
     */
    private void validateThreshold(final WebSecurityForm<AlertEditForm> webSecurityForm, final RandomKeyMapping keyMapping) {
        final AlertEditForm alertEditForm = webSecurityForm.getWrappedObject();
        final String thresholdStr = alertEditForm.getAlertThresholdExceed();
        final int alertType = (alertEditForm.getAlertTypeId() == null) ? -1 : alertEditForm.getAlertTypeId().intValue();
        if (alertType == 1) {
            // This "validateIntegerLimits" already includes "validateRequired" and "validateNumber".
            super.validateIntegerLimits(webSecurityForm, "alertThresholdExceed", "label.settings.alerts.threshold.exceeds", thresholdStr,
                                        WebSecurityConstants.MIN_THRESHOLD_HOUR, WebSecurityConstants.MAX_THRESHOLD_HOUR);
        } else if (alertType == 2) {
            if (super.validateRequired(webSecurityForm, "alertThresholdExceed", "label.settings.alerts.threshold.exceeds", thresholdStr,
                                       new Object[] { 0 }) != null) {
                if (super.validateDollarAmt(webSecurityForm, "alertThresholdExceed", "label.settings.alerts.threshold.exceeds",
                                            thresholdStr) != null) {
                    super.validateDoubleLimits(webSecurityForm, "alertThresholdExceed", "label.settings.alerts.threshold.exceeds", thresholdStr,
                                               WebSecurityConstants.MIN_THRESHOLD_DOLLAR, WebSecurityConstants.MAX_THRESHOLD_DOLLAR);
                }
            }
        } else if (alertType > 2 && alertType < 6) {
            if (super.validateRequired(webSecurityForm, "alertThresholdType", "label.settings.alerts.threshold.type",
                                       alertEditForm.getAlertThresholdExceedDisplay(), new Object[] { 0 }) != null) {
                final short thresholdType = alertEditForm.getAlertThresholdExceedDisplay();
                if (thresholdType == 1) {
                    // This "validateDoubleLimits" already includes "validateRequired" and "validateNumber".
                    super.validateDoubleLimits(webSecurityForm, "alertThresholdExceed", "label.settings.alerts.threshold.exceeds", thresholdStr,
                                               WebSecurityConstants.MIN_THRESHOLD_COUNT, WebSecurityConstants.MAX_THRESHOLD_COUNT);
                } else {
                    if (super.validateRequired(webSecurityForm, "alertThresholdExceed", "label.settings.alerts.threshold.exceeds", thresholdStr,
                                               new Object[] { 0 }) != null) {
                        if (super.validateDollarAmt(webSecurityForm, "alertThresholdExceed", "label.settings.alerts.threshold.exceeds",
                                                    thresholdStr) != null) {
                            super.validateDoubleLimits(webSecurityForm, "alertThresholdExceed", "label.settings.alerts.threshold.exceeds",
                                                       thresholdStr, WebSecurityConstants.MIN_THRESHOLD_DOLLAR,
                                                       WebSecurityConstants.MAX_THRESHOLD_DOLLAR);
                        }
                    }
                }
            }
        } else if (alertType == 7) {
            // This "validateIntegerLimits" already includes "validateRequired" and "validateNumber".
            super.validateIntegerLimits(webSecurityForm, "alertThresholdExceed", "label.settings.alerts.threshold.exceeds", thresholdStr,
                                        WebSecurityConstants.MIN_THRESHOLD_DAY, WebSecurityConstants.MAX_THRESHOLD_DAY);
        } else {
            alertEditForm.setAlertThresholdExceed("0");
        }
    }
    
    /**
     * It validates user input route.
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     * @param keyMapping
     *            RandomKeyMapping object
     */
    private void validateRoute(final WebSecurityForm<AlertEditForm> webSecurityForm, final RandomKeyMapping keyMapping) {
        final AlertEditForm alertEditForm = webSecurityForm.getWrappedObject();
        Integer routeId = null; // default to all routes
        if (!("0".equals(alertEditForm.getRandomizedRouteId()))) {
            final Object[] invalidTypeIds = { "-1" };
            final String randomizedRouteId = (String) super.validateRequired(webSecurityForm, "alertRoute", "label.route",
                                                                             alertEditForm.getRandomizedRouteId(), (Object) invalidTypeIds);
            routeId = super.validateRandomId(webSecurityForm, "alertRoute", "label.route", randomizedRouteId, keyMapping, Route.class, Integer.class);
        }
        if (routeId != null) {
            alertEditForm.setRouteId(routeId);
        }
    }
    
    /**
     * It validates user input email address.
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     */
    private void validateEmail(final WebSecurityForm<AlertEditForm> webSecurityForm) {
        final AlertEditForm alertEditForm = webSecurityForm.getWrappedObject();
        final List<String> emailList = alertEditForm.getAlertContacts();
        List<String> emails = null;
        if (!super.listHasValues(emailList)) {
            alertEditForm.setAlertContacts(new ArrayList<String>());
        } else {
            emails = Arrays.asList(emailList.get(0).split(","));
            if (super.validateRequired(webSecurityForm, "alertContacts", "label.settings.alerts.form.notificationEmail", emails) == null) {
                emails = null;
            }
            emails = super.validateEmailAddresses(webSecurityForm, "alertContacts", "label.settings.alerts.form.notificationEmail", emails);
            alertEditForm.setAlertContacts(emails);
        }
    }
}
