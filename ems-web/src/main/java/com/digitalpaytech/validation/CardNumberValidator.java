package com.digitalpaytech.validation;

import java.io.Serializable;
import java.util.Calendar;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.util.CardProcessingConstants;

/**
 * Validator for card validation.
 * 
 * @author wittawatv
 *
 */
@Component("cardNumberValidator")
public class CardNumberValidator<W extends Serializable> extends BaseValidator<W> {
    @Autowired
    private CustomerCardTypeService customerCardTypeService;
    
    public void setCustomerCardTypeService(CustomerCardTypeService customerCardTypeService) {
        this.customerCardTypeService = customerCardTypeService;
    }
    
    /**
     * This method will always throw UnsupportedOperationException because it should not be invoked !
     * 
     */
    @Override
    public void validate(WebSecurityForm<W> target, Errors errors) {
        throw new UnsupportedOperationException();
    }
    
    /**
     * Validate whether the field is a credit card number or not. (By validating that the string contains only number characters and it has correct length)
     * This method also use track 2 data to resolve type of the credit card and check whether EMS supports this card type or not.
     * Please see {@link CustomerCardTypeService#isCreditCardData(String)} and its actual implementation for more information.
     * 
     * @param form
     *            The WebSecurityForm Object containing the data field.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabelKey
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @return Trimmed card number if the card number is valid. Else return null.
     */
    public String validateCreditCardNumber(WebSecurityForm<W> form, String fieldName, String fieldLabelKey, String value) {
        String result = null;
        if(validateNumber(form, fieldName, fieldLabelKey, value) != null) {
            result = validateCreditCardLength(form, fieldName, fieldLabelKey, value.trim());
            if ((result != null) && result.length() > CardProcessingConstants.CREDIT_CARD_LAST_4_DIGITS_LENGTH && (!customerCardTypeService.isCreditCardData(result))) {
                result = null;
                form.addErrorStatus(new FormErrorStatus(fieldName, this.getMessage("error.common.invalid", this.getMessage(fieldLabelKey))));
            }
        }
        
        return result;
    }
    
    /**
     * Validate whether the field is a smart card number or not. (By validating that the string contains only number characters and it has correct length)
     * 
     * @param form
     *            The WebSecurityForm Object containing the data field.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabelKey
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @return Trimmed card number if the card number is valid. Else return null.
     */
    public String validateSmartCardNumber(WebSecurityForm<W> form, String fieldName, String fieldLabelKey, String value) {
        String result = null;
        if(validateNumber(form, fieldName, fieldLabelKey, value) != null) {
            result = validateNumberStringLength(form, fieldName, fieldLabelKey, value, CardProcessingConstants.SMART_CARD_MIN_LENGTH, CardProcessingConstants.BAD_SMART_CARD_MAX_LENGTH);
           
        }
        
        return result;
    }
    
    /**
     * Validate whether the field is a value card number or not. (By validating that the string contains only number characters and it has correct length)
     * 
     * @param form
     *            The WebSecurityForm Object containing the data field.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabelKey
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @return Trimmed card number if the card number is valid. Else return null.
     */
    public String validateValueCardNumber(WebSecurityForm<W> form, String fieldName, String fieldLabelKey, String value) {
        String result = null;
        if(validateNumber(form, fieldName, fieldLabelKey, value) != null) {
            result = validateNumberStringLength(form, fieldName, fieldLabelKey, value, CardProcessingConstants.VALUE_CARD_MIN_LENGTH, CardProcessingConstants.BAD_VALUE_CARD_MAX_LENGTH);
        }
        
        return result;
    }
    
    /**
     * Validate whether the field is a card number of any kind or not.  This is used when the customer searches using the card parameter "all".  This validation step will pass
     * if a customer enters at least as many characters for the minimum length of any card type, but less than the maximum of any card type.
     * @param form
     *            The WebSecurityForm Object containing the data field.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabelKey
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     */
    public String validateAnyCardNumber(WebSecurityForm<W> form, String fieldName, String fieldLabelKey, String value){
        String result = null;
        if(validateNumber(form, fieldName, fieldLabelKey, value) != null){
            result = validateNumberStringLength(form, fieldName, fieldLabelKey, value, CardProcessingConstants.VALUE_CARD_MIN_LENGTH, CardProcessingConstants.BAD_VALUE_CARD_MAX_LENGTH);
        }
        return result;
    }
    /**
     * 
     * 
     * @param form
     *            The WebSecurityForm Object containing the data field.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabelKey
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @return
     */
    public Integer validateExpiryDate(WebSecurityForm<W> form, String fieldName, String fieldLabelKey, String value) {
        return validateExpiryDate(form, fieldName, fieldLabelKey, value, true, (TimeZone) null);
    }
    
    /**
     * 
     * 
     * @param form
     *            The WebSecurityForm Object containing the data field.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabelKey
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @param allowExpired
     * @param timeZone
     * @return
     */
    public Integer validateExpiryDate(WebSecurityForm<W> form, String fieldName, String fieldLabelKey, String value, boolean allowExpired, TimeZone timeZone) {
        Integer result = null;
        if (validateStringLength(form, fieldName, fieldLabelKey, value, 4) != null) {
            if (validateNumber(form, fieldName, fieldLabelKey, value) != null) {
                int expiryMonth = Integer.parseInt(value.substring(0, 2));
                if ((expiryMonth <= 0) || (expiryMonth > 12)) {
                    form.addErrorStatus(new FormErrorStatus(fieldName, this.getMessage("error.common.invalid", this.getMessage(fieldLabelKey))));
                } else {
                    int expiryYear = Integer.parseInt(value.substring(2));
                    result = (expiryYear * 100) + expiryMonth;
                    if(!allowExpired) {
                        Calendar cal = Calendar.getInstance(timeZone);
                        Integer curr = ((cal.get(Calendar.YEAR) % 100) * 100) + (cal.get(Calendar.MONDAY) + 1);
                        if (result < curr) {
                            result = null;
                            form.addErrorStatus(new FormErrorStatus(fieldName, this.getMessage("error.cardExpiry.expired")));
                        }
                    }
                }
            }
        }
        
        return result;
    }
    
    
    /**
     * For searching credit card, must enter the last 4 digits or the whole numbers.
     * e.g. 5111111111115555 or 5555 
     * @param messageContainer
     * @param fieldName
     * @param fieldLabel
     * @param value Entered credit card number.
     * @return Trimmed card number if the card number is valid. Else return null.
     */
    protected String validateCreditCardLength(WebSecurityForm<W> form, String fieldName, String fieldLabelKey, String value) {
        int length = value.length();
        if (length > CardProcessingConstants.CREDIT_CARD_MAX_LENGTH || length < CardProcessingConstants.CREDIT_CARD_LAST_4_DIGITS_LENGTH 
                || (length >CardProcessingConstants.CREDIT_CARD_LAST_4_DIGITS_LENGTH && length < CardProcessingConstants.CREDIT_CARD_MIN_LENGTH)) {
            String msg = this.getMessage("error.common.invalid.creditCard.lengths.digits", 
                                         this.getMessage(fieldLabelKey), 
                                         CardProcessingConstants.CREDIT_CARD_LAST_4_DIGITS_LENGTH, 
                                         CardProcessingConstants.CREDIT_CARD_MIN_LENGTH, 
                                         CardProcessingConstants.CREDIT_CARD_MAX_LENGTH);
            form.addErrorStatus(new FormErrorStatus(fieldName, msg));
            return null;
        } 
        return value;
    }
}
