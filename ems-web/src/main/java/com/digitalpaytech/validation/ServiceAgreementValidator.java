package com.digitalpaytech.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.mvc.support.ServiceAgreementForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.WebCoreConstants;

@Component("serviceAgreementValidator")
public class ServiceAgreementValidator extends BaseValidator<ServiceAgreementForm> {
	@Override
	public void validate(WebSecurityForm<ServiceAgreementForm> command, Errors errors) {		
		WebSecurityForm<ServiceAgreementForm> pkForm = super.prepareSecurityForm(command, errors, "postToken");
        if (pkForm == null) {
            return;
        }
		checkName(pkForm);
		checkTitle(pkForm);
		checkOrg(pkForm);
	}

	private void checkName(WebSecurityForm<ServiceAgreementForm> webSecurityForm) {
		ServiceAgreementForm form = (ServiceAgreementForm) webSecurityForm.getWrappedObject();		
		String name = (String) super.validateRequired(webSecurityForm, "name", "label.serviceAgreement.name", form.getName());								
	    name = super.validateStringLength(webSecurityForm, "name", "label.serviceAgreement.name", name, WebCoreConstants.VALIDATION_MIN_LENGTH_1,
	    		WebCoreConstants.VALIDATION_MAX_LENGTH_30);
	    name = super.validateStandardText(webSecurityForm, "name", "label.serviceAgreement.name", name);					
		if(name != null) {
		    form.setName(name.trim());
		}
	}

	private void checkTitle(WebSecurityForm<ServiceAgreementForm> webSecurityForm) {
		ServiceAgreementForm form = webSecurityForm.getWrappedObject();
		String title = (String) super.validateRequired(webSecurityForm, "title", "label.serviceAgreement.title", form.getTitle());
        title = super.validateStringLength(webSecurityForm, "title", "label.serviceAgreement.title", title, WebCoreConstants.VALIDATION_MIN_LENGTH_1,
        		WebCoreConstants.VALIDATION_MAX_LENGTH_30);
        title = super.validateStandardText(webSecurityForm, "title", "label.serviceAgreement.title", title);
        if(title != null) {
            form.setTitle(title);
        }		
	}

	private void checkOrg(WebSecurityForm<ServiceAgreementForm> webSecurityForm) {
        ServiceAgreementForm form = webSecurityForm.getWrappedObject();
        String org = (String) super.validateRequired(webSecurityForm, "organization", "label.serviceAgreement.org", form.getOrganization());
        org = super.validateStringLength(webSecurityForm, "organization", "label.serviceAgreement.org", org, WebCoreConstants.VALIDATION_MIN_LENGTH_1,
        		WebCoreConstants.VALIDATION_MAX_LENGTH_255);
        org = super.validatePrintableText(webSecurityForm, "organization", "label.serviceAgreement.org", org);
        if(org != null) {
            form.setOrganization(org);
        }                
	}
}
