package com.digitalpaytech.validation;

import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.authentication.dao .SaltSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.Authentication;
import org.springframework.validation.Errors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.support.ChangePasswordForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;


/**
 * PasswordProfileValidator contains validation methods for passwords.
 * 
 * @author Allen Liang
 */

@Component("passwordProfileValidator")
public class PasswordProfileValidator extends BaseValidator<ChangePasswordForm> {
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private SaltSource saltSource;
	
	@Override
	public void validate(WebSecurityForm<ChangePasswordForm> command, Errors errors) {
        WebSecurityForm<ChangePasswordForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if(webSecurityForm == null){
            return;
        }	    
		
        if(checkPasswordsSame(webSecurityForm, webSecurityForm.getWrappedObject().getNewPassword(), 
                              webSecurityForm.getWrappedObject().getNewPasswordAgain())){
            checkPassword(webSecurityForm, errors);
        }
	}
	
	private boolean checkPasswordsSame(WebSecurityForm<ChangePasswordForm> webSecurityForm, String newPassword, String confirmPassword){	    
	    if (!isSame(newPassword, confirmPassword)) {
	        webSecurityForm.addErrorStatus(new FormErrorStatus("newPassword,newPasswordAgain", this.getMessage("error.user.password.not.matched")));
	        return false;
	    }
	    return true;
	}

	/**
	 * Check if new password is the same as the temporary password. Throws an error with message key "error.user.old.new.password.same"
	 * if it's identical.
	 * @param webSecurityForm
	 * @param newRawPassword New encoded password.
	 * @param userAccount Existing userAccount object.
	 */
	public void validateNewAndOldPasswords(WebSecurityForm<ChangePasswordForm> webSecurityForm, String newRawPassword, UserAccount userAccount) {
	    String newEncodedPassword = passwordEncoder.encodePassword(newRawPassword, userAccount.getPasswordSalt());
	    if (newEncodedPassword.equals(userAccount.getPassword())) {
	        webSecurityForm.addErrorStatus(new FormErrorStatus("oldPassword,newPassword", this.getMessage("error.user.old.new.password.same")));
	    }
	}
	
	//----------- Copy from ems_v2 UserAccountValidator.java ------------
	private void checkPassword(WebSecurityForm<ChangePasswordForm> webSecurityForm, Errors errors) {
	    ChangePasswordForm passwdProfile = webSecurityForm.getWrappedObject();
	    String newPassword = (String) super.validateRequired(webSecurityForm, "newPassword", "label.changePassword.newPassword", passwdProfile.getNewPassword());	    
	    newPassword = super.validateStringLength(webSecurityForm, "newPassword", "label.changePassword.newPassword", newPassword, WebSecurityConstants.USER_PASSWORD_MIN_LENGTH,
	                                          WebSecurityConstants.USER_PASSWORD_MAX_LENGTH);	    
	    newPassword = super.validateRegEx(webSecurityForm, "newPassword", "label.changePassword.newPassword", newPassword, WebSecurityConstants.COMPLEX_PASSWORD_EXP,
                      "error.user.password.not.complex", (String) null);
	    
	    if(newPassword != null){
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            WebUser webUser = (WebUser) authentication.getPrincipal();
//            String encPassword = passwordEncoder.encodePassword(passwdProfile.getNewPassword(), this.saltSource.getSalt(webUser));
            
            // Set the Encode Password.
            webUser.setIsComplexPassword(true);
//            passwdProfile.setNewPassword(encPassword);	        
	    }
	}

	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

	public void setSaltSource(SaltSource saltSource) {
		this.saltSource = saltSource;
	}
}
