package com.digitalpaytech.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.EventDeviceType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.mvc.support.PayStationRecentActivityFilterForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;

@Component("payStationRecentActivityFilterValidator")
public class PayStationRecentActivityFilterValidator extends BaseValidator<PayStationRecentActivityFilterForm> {
    @Override
    public void validate(WebSecurityForm<PayStationRecentActivityFilterForm> webSecurityForm, Errors errors) {
        webSecurityForm.removeAllErrorStatus();
        
        RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        checkId(webSecurityForm, keyMapping);
    }
    
    private void checkId(WebSecurityForm<PayStationRecentActivityFilterForm> webSecurityForm, RandomKeyMapping keyMapping) {
        
        PayStationRecentActivityFilterForm form = webSecurityForm.getWrappedObject();
        
        
        super.validateRequired(webSecurityForm, "randomId", "label.payStation",  form.getRandomId());
        form.setPointOfSaleId(super.validateRandomId(webSecurityForm, "randomId", "label.payStation", form.getRandomId(), keyMapping,
                                                PointOfSale.class, Integer.class));
        
        if (WebCoreConstants.ALL_STRING.equalsIgnoreCase(form.getModuleRandomId()) || WebCoreConstants.EMPTY_STRING.equals(form.getModuleRandomId())) {
            form.setModuleId(null);
        } else {
            form.setModuleId(super.validateRandomId(webSecurityForm, "moduleRandomId", "label.settings.alerts.module", form.getModuleRandomId(), keyMapping,
                                                    EventDeviceType.class, Integer.class));
        }
        if (WebCoreConstants.ALL_STRING.equalsIgnoreCase(form.getSeverityRandomId()) || WebCoreConstants.EMPTY_STRING.equals(form.getSeverityRandomId())) {
            form.setSeverityId(null);
        } else {
            form.setSeverityId(super.validateRandomId(webSecurityForm, "severityRandomId", "label.settings.alerts.severity", form.getSeverityRandomId(),
                                                      keyMapping, EventSeverityType.class, Integer.class));
        }
    }
}
