package com.digitalpaytech.validation;

import java.util.Date;
import java.util.TimeZone;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.Rate;
import com.digitalpaytech.domain.RateRateProfile;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.RateRateProfileEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

@Component("rateRateProfileValidator")
public class RateRateProfileValidator extends BaseValidator<RateRateProfileEditForm> {
    
    @Override
    public final void validate(final WebSecurityForm<RateRateProfileEditForm> command, final Errors errors) {
        //JOptionPane.showMessageDialog(null, "Inside as Validator");
        final WebSecurityForm<RateRateProfileEditForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
        
        if (!super.validateActionFlag(webSecurityForm, WebSecurityConstants.ACTION_UPDATE, WebSecurityConstants.ACTION_CREATE)) {
            return;
        }
        
        checkId(webSecurityForm);
        checkRateId(webSecurityForm);
        checkAvailability(webSecurityForm);
        checkMaxExpiryTime(webSecurityForm);
        
    }
    
    public final void validateReschedule(final WebSecurityForm<RateRateProfileEditForm> command, final Errors errors) {
        //JOptionPane.showMessageDialog(null, "Inside as Validator");
        final WebSecurityForm<RateRateProfileEditForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
        
        checkId(webSecurityForm);
        checkAvailability(webSecurityForm);
        
    }
    
    /**
     * Converts random id from form into a database id that is saved for later use
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     */
    private void checkId(final WebSecurityForm<RateRateProfileEditForm> webSecurityForm) {
        final RateRateProfileEditForm form = webSecurityForm.getWrappedObject();
        final RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        validateRequired(webSecurityForm, "randomId", "label.settings.rates.rate", form.getRateRateProfileRandomId());
        final Integer id = super.validateRandomId(webSecurityForm, "randomId", "label.settings.rates.rate", form.getRateRateProfileRandomId(), keyMapping,
                                                  RateRateProfile.class, Integer.class);
    }
    
    private void checkRateId(final WebSecurityForm<RateRateProfileEditForm> webSecurityForm) {
        final RateRateProfileEditForm form = webSecurityForm.getWrappedObject();
        final RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        final Integer id = super.validateRandomId(webSecurityForm, "rateRandomId", "label.settings.rates.rate", form.getRateRandomId(), keyMapping, Rate.class,
                                                  Integer.class);
    }
    
    private void checkAvailability(final WebSecurityForm<RateRateProfileEditForm> webSecurityForm) {
        final RateRateProfileEditForm form = webSecurityForm.getWrappedObject();
        final boolean isScheduled = form.getUseScheduled();
        if (isScheduled) {
            super.validateRequired(webSecurityForm, "availabilityStartDate", "label.start.date", form.getAvailabilityStartDate());
            super.validateRequired(webSecurityForm, "availabilityEndDate", "label.end.date", form.getAvailabilityStartDate());
            super.validateRequired(webSecurityForm, "availabilityStartTime", "label.start.time", form.getAvailabilityStartDate());
            super.validateRequired(webSecurityForm, "availabilityEndTime", "label.end.time", form.getAvailabilityStartDate());
            form.setStartDate(super.validateDate(webSecurityForm, "availabilityStartDate", "label.start.date", form.getAvailabilityStartDate(),
                                                 TimeZone.getTimeZone(WebCoreConstants.GMT)));
            form.setEndDate(super.validateDate(webSecurityForm, "availabilityEndDate", "label.end.date", form.getAvailabilityEndDate(),
                                               TimeZone.getTimeZone(WebCoreConstants.GMT)));
            form.setStartTime(super.validateTime(webSecurityForm, "availabilityStartTime", "label.start.time", form.getAvailabilityStartTime(),
                                                 TimeZone.getTimeZone(WebCoreConstants.GMT)));
            form.setEndTime(super.validateTime(webSecurityForm, "availabilityEndTime", "label.end.time", form.getAvailabilityEndTime(),
                                               TimeZone.getTimeZone(WebCoreConstants.GMT)));
            if (form.getStartDate().after(form.getEndDate())) {
                final FormErrorStatus status = new FormErrorStatus("availabilityStartDate", this.getMessage("error.common.date.more.than.max", new Object[] {
                        this.getMessage("label.start.date"), this.getMessage("label.end.date"), }));
                webSecurityForm.getValidationErrorInfo().addErrorStatus(status);
                
            }
            if ((form.getStartDate().equals(form.getEndDate())) && (form.getStartTime().after(form.getEndTime()))) {
                final FormErrorStatus status = new FormErrorStatus("availabilityStartTime", this.getMessage("error.common.date.more.than.max", new Object[] {
                        this.getMessage("label.start.time"), this.getMessage("label.end.time"), }));
                webSecurityForm.getValidationErrorInfo().addErrorStatus(status);
                
            }
        } else {
            if (!(form.getUseMonday() || form.getUseTuesday() || form.getUseWednesday() || form.getUseThursday() || form.getUseFriday()
                  || form.getUseSaturday() || form.getUseSunday())) {
                final FormErrorStatus status = new FormErrorStatus("useMonday", this.getMessage("error.settings.rates.noDaysSelected"));
                webSecurityForm.getValidationErrorInfo().addErrorStatus(status);
            }
            
            final Date startTime = super.validateTime(webSecurityForm, "availabilityStartTime"
                                                      , "label.start.time", form.getAvailabilityStartTime(), TimeZone.getTimeZone(WebCoreConstants.GMT));
            final Date endTime = super.validateTime(webSecurityForm, "availabilityEndTime"
                                                    , "label.end.time", form.getAvailabilityEndTime(), TimeZone.getTimeZone(WebCoreConstants.GMT));
            if ((startTime != null) && (endTime != null) && (((endTime.getTime() % 86400000L) > 0) && endTime.before(startTime))) {
                final FormErrorStatus status = new FormErrorStatus("availabilityStartTime", this.getMessage("error.common.date.more.than.max", new Object[] {
                        this.getMessage("label.start.time"), this.getMessage("label.end.time"), }));
                webSecurityForm.getValidationErrorInfo().addErrorStatus(status);
            }
        }
    }
    
    private void checkMaxExpiryTime(final WebSecurityForm<RateRateProfileEditForm> webSecurityForm) {
        final RateRateProfileEditForm form = webSecurityForm.getWrappedObject();
        final boolean isMaxExpiry = form.getUseMaxExpiry();
        if (isMaxExpiry) {
            super.validateRequired(webSecurityForm, "expiryDayType", "label.settings.rates.expiryDay", form.getExpiryDayType());
            super.validateRequired(webSecurityForm, "expiryTime", "label.settings.rates.expiryTime", form.getExpiryTime());
            super.validateTime(webSecurityForm, "expiryTime", "label.settings.rates.expiryTime", form.getExpiryTime(),
                               TimeZone.getTimeZone(WebCoreConstants.GMT));
        }
    }
    
}
