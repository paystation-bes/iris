package com.digitalpaytech.validation.systemadmin;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.cxf.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.domain.PosStatus;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.support.SysAdminPayStationEditForm;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObjectId;
import com.digitalpaytech.validation.BaseValidator;

@Component("sysAdminPayStationValidator")
public class SysAdminPayStationValidator extends BaseValidator<SysAdminPayStationEditForm> {
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private CustomerService customerService;
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    @Override
    public void validate(final WebSecurityForm<SysAdminPayStationEditForm> target, final Errors errors) {
    }
    
    public final void validate(final WebSecurityForm<SysAdminPayStationEditForm> command, final Errors errors, final RandomKeyMapping keyMapping) {
        
        final WebSecurityForm<SysAdminPayStationEditForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
        
        final SysAdminPayStationEditForm form = webSecurityForm.getWrappedObject();
        
        fixDropDowns(webSecurityForm);
        
        checkIds(webSecurityForm, keyMapping);
        
        validatePrintableText(webSecurityForm, "comment", "label.comment", form.getComment());
        
        if ((form.getMoveCustomerBoolean() != null) && form.getMoveCustomerBoolean().booleanValue()) {
            if (!WebSecurityUtil.hasSystemAdminUserValidPermission(WebSecurityConstants.PERMISSION_DPT_MOVE_PAYSTATION)) {
                webSecurityForm.addErrorStatus(new FormErrorStatus("customerRandomId", this.getMessage("error.paystation.move.no.permission")));
                return;
            }
            
            if (validateRequired(webSecurityForm, "customerRandomId", "label.customer", form.getCustomerRandomId()) == null) {
                return;
            }
            
            if (!form.getCustomerRandomId().equals(form.getCurrentCustomerRandomId())) {
                final Customer customer = this.customerService.findCustomer(form.getCustomerRealId());
                if (customer.isIsParent()) {
                    webSecurityForm.addErrorStatus(new FormErrorStatus("customerRandomId",
                            this.getMessage("error.paystation.move.to.parent.customer.invalid")));
                }
            } else {
                webSecurityForm
                        .addErrorStatus(new FormErrorStatus("customerRandomId", this.getMessage("error.paystation.move.to.same.customer.invalid")));
                        
            }
        } else if (form.getPayStationRealIds() == null || form.getPayStationRealIds().isEmpty()) {
            
            checkPayStationName(webSecurityForm, keyMapping);
            checkSerialNumber(webSecurityForm, keyMapping);
            checkBooleans(webSecurityForm);
        }
    }
    
    private void fixDropDowns(final WebSecurityForm<SysAdminPayStationEditForm> webSecurityForm) {
        final SysAdminPayStationEditForm form = webSecurityForm.getWrappedObject();
        form.setDeleteCreditCardMerchAcct(false);
        form.setDeleteCustomCardMerchAcct(false);
        if ((form.getCreditCardMerchantRandomId() == null || WebCoreConstants.EMPTY_STRING.equals(form.getCreditCardMerchantRandomId()))
            && (WebCoreConstants.EMPTY_STRING.equals(form.getCreditCardMerchantName()) || form.getCreditCardMerchantName() == null)) {
            form.setDeleteCreditCardMerchAcct(true);
        }
        
        if ((form.getCustomCardMerchantRandomId() == null || WebCoreConstants.EMPTY_STRING.equals(form.getCustomCardMerchantRandomId()))
            && (WebCoreConstants.EMPTY_STRING.equals(form.getCustomCardMerchantName()) || form.getCustomCardMerchantName() == null)) {
            form.setDeleteCustomCardMerchAcct(true);
        }
        
        if ("-1".equals(form.getCreditCardMerchantRandomId())) {
            form.setCreditCardMerchantRandomId(WebCoreConstants.EMPTY_STRING);
            form.setDeleteCreditCardMerchAcct(true);
        }
        if ("-1".equals(form.getCustomCardMerchantRandomId())) {
            form.setCustomCardMerchantRandomId(WebCoreConstants.EMPTY_STRING);
            form.setDeleteCustomCardMerchAcct(true);
        }
        if ("-1".equals(form.getLocationRandomId())) {
            form.setLocationRandomId(WebCoreConstants.EMPTY_STRING);
            //webSecurityForm.addErrorStatus(new FormErrorStatus("locationRandomId", this.getMessage("error.common.required",
            //                                                                                       new Object[] { this.getMessage("label.location") })));
            //super.addError(webSecurityForm, "locationId", this.getMessage("error.common.required", this.getMessage("label.location")));
        }
    }
    
    private void checkBooleans(final WebSecurityForm<SysAdminPayStationEditForm> webSecurityForm) {
        
        final SysAdminPayStationEditForm form = webSecurityForm.getWrappedObject();
        
        // Editing active, billable, or bundled flags is only available when editing a single pay station
        if (form.getPayStations().size() < 1) {
            if (form.getActive() == null) {
                webSecurityForm
                        .addErrorStatus(new FormErrorStatus("isActive", this.getMessage("error.common.required", new Object[] { "isActive" })));
            }
            if (form.getBillable() == null) {
                form.setBillable(false);
            }
            if (form.getBundledData() == null) {
                form.setBundledData(false);
            }
        }
    }
    
    private void checkSerialNumber(final WebSecurityForm<SysAdminPayStationEditForm> webSecurityForm, final RandomKeyMapping keyMapping) {
        
        final SysAdminPayStationEditForm form = webSecurityForm.getWrappedObject();
        
        // Editing serial number is only allowed on single pay station edits
        if (form.getRandomId() != null && !WebCoreConstants.EMPTY_STRING.equals(form.getRandomId())) {
            final Integer actualId = super.validateRandomId(webSecurityForm, "randomId", "label.payStation", form.getRandomId(), keyMapping,
                                                            PointOfSale.class, Integer.class);
            final PointOfSale pos = this.pointOfSaleService.findPointOfSaleById(actualId);
            // the serial number cannot be changed on an active pay station
            if (!form.getSerialNumber().equals(pos.getSerialNumber())) {
                final PosStatus pointOfSaleStatus = this.pointOfSaleService.findPointOfSaleStatusByPOSId(actualId, true);
                final PosHeartbeat heartbeat = pointOfSaleService.findPosHeartbeatByPointOfSaleId(actualId);
                if (heartbeat != null && heartbeat.getLastHeartbeatGmt() != null
                    && !heartbeat.getLastHeartbeatGmt().equals(pointOfSaleStatus.getIsProvisionedGmt())) {
                    webSecurityForm.addErrorStatus(new FormErrorStatus("serialNumber",
                            this.getMessage("error.common.invalid.active.paystation", new Object[] { this.getMessage("label.serial.number") })));
                    return;
                } else {
                    final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleBySerialNumber(form.getSerialNumber());
                    if (pointOfSale != null) {
                        webSecurityForm.addErrorStatus(new FormErrorStatus("serialNumber",
                                this.getMessage("error.common.invalid.paystation.exists", new Object[] { form.getSerialNumber() })));
                        return;
                    }
                }
                
                super.validateStringLength(webSecurityForm, "serialNumbers", "label.serial.number", form.getSerialNumber(),
                                           WebSecurityConstants.MIN_SERIALNUMBER_LENGTH, WebSecurityConstants.MAX_SERIALNUMBER_LENGTH);
                super.validateAlphaNumeric(webSecurityForm, "serialNumbers", "label.serial.number", form.getSerialNumber());
            }
        }
    }
    
    private void checkPayStationName(final WebSecurityForm<SysAdminPayStationEditForm> webSecurityForm, final RandomKeyMapping keyMapping) {
        
        final SysAdminPayStationEditForm form = webSecurityForm.getWrappedObject();
        
        final Object requiredCheck = super.validateRequired(webSecurityForm, "name", "label.name", form.getName());
        final String  formPosName = form.getName();
        if (requiredCheck != null) {
            super.validateStringLength(webSecurityForm, "name", "label.name", formPosName, WebCoreConstants.VALIDATION_MIN_LENGTH_1,
                                       WebCoreConstants.VALIDATION_MAX_LENGTH_25);
            super.validatePayStationText(webSecurityForm, "name", "label.name", formPosName);
        }
        final Integer pointOfSaleId = form.getPointOfSaleRealId();
        final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(pointOfSaleId);
        boolean isNamechange = !formPosName.equals(pointOfSale.getName());
        if (isNamechange && this.pointOfSaleService.findAllPointOfSalesByPosNameForCustomer(formPosName,form.getCustomerRealId()).size()>0) {
        	  super.addErrorStatus(webSecurityForm, "name", "error.common.invalid.paystation.same.name",formPosName);
        }
    }
    
    private void checkIds(final WebSecurityForm<SysAdminPayStationEditForm> webSecurityForm, final RandomKeyMapping keyMapping) {
        
        final SysAdminPayStationEditForm form = webSecurityForm.getWrappedObject();
        
        if (form.getRandomId() == null || WebCoreConstants.EMPTY_STRING.equals(form.getRandomId())) {
            final List<Integer> customerIds = new ArrayList<Integer>();
            final List<Integer> locationIds = new ArrayList<Integer>();
            final List<Integer> routeIds = new ArrayList<Integer>();
            final Set<Integer> pointOfSaleIds = new HashSet<Integer>();
            for (String randomId : form.getPayStationRandomIds()) {
                final WebObjectId buffer =
                        super.validateRandomIdForWebObject(webSecurityForm, "payStationRandomId",
                                                           "label.settings.locations.paystationList.paystationHdr", randomId, keyMapping);
                if (PointOfSale.class.isAssignableFrom(buffer.getObjectType())) {
                    pointOfSaleIds.add((Integer) buffer.getId());
                } else if (Location.class.isAssignableFrom(buffer.getObjectType())) {
                    locationIds.add((Integer) buffer.getId());
                } else if (Route.class.isAssignableFrom(buffer.getObjectType())) {
                    routeIds.add((Integer) buffer.getId());
                } else if (Customer.class.isAssignableFrom(buffer.getObjectType())) {
                    customerIds.add((Integer) buffer.getId());
                }
            }
            
            if ((!customerIds.isEmpty()) || (!locationIds.isEmpty()) || (!routeIds.isEmpty())) {
                pointOfSaleIds.addAll(this.pointOfSaleService.findAllIds(customerIds, locationIds, routeIds));
            }
            
            form.setPayStationRealIds(pointOfSaleIds);
            
            final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(form.getPayStationRealIds().iterator().next());
            form.setCustomerRealId(pointOfSale.getCustomer().getId());
        } else {
            form.setPointOfSaleRealId(super.validateRandomId(webSecurityForm, "payStationRandomId",
                                                             "label.settings.locations.paystationList.paystationHdr", form.getRandomId(), keyMapping,
                                                             PointOfSale.class, Integer.class));
                                                             
            final PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(form.getPointOfSaleRealId());
            form.setCustomerRealId(pointOfSale.getCustomer().getId());
        }
        
        if (form.getMoveCustomerBoolean() != null && form.getMoveCustomerBoolean()) {
            
            final Integer realCustomerId =
                    super.validateRandomId(webSecurityForm, "customerRandomId", "label.systemAdmin.workspace.payStation.moveCustomer",
                                           form.getCustomerRandomId(), keyMapping, Customer.class, Integer.class, WebCoreConstants.EMPTY_STRING);
            if (realCustomerId == null) {
                super.addErrorStatus(webSecurityForm, "customerRandomId", "error.paystation.move.to.customer.invalid",
                                     new Object[] { "label.systemAdmin.workspace.payStation.moveCustomer" });
                return;
            } else {
                form.setMoveCustomerRealId(realCustomerId);
                
                if (form.getCustomerRealId().equals(form.getMoveCustomerRealId())) {
                    super.addErrorStatus(webSecurityForm, "customerRandomId", "error.paystation.move.to.same.customer.invalid",
                                         new Object[] { "label.systemAdmin.workspace.payStation.moveCustomer" });
                    return;
                }
            }
        } else {
            final boolean isSinglePOS = form.getRandomId() != null && !StandardConstants.STRING_EMPTY_STRING.equals(form.getRandomId())
                                        && !StringUtils.isEmpty(form.getRandomId());
            if (isSinglePOS || form.getUpdateCreditMerchant() != null && form.getUpdateCreditMerchant()) {
                form.setCreditCardMerchantRealId(super.validateRandomId(webSecurityForm, "creditCardMerchantAccountRandomId",
                                                                        "label.settings.locations.paystationList.information.creditCardMerchantAccount",
                                                                        form.getCreditCardMerchantRandomId(), keyMapping, MerchantAccount.class,
                                                                        Integer.class, WebCoreConstants.EMPTY_STRING));
            }
            if (isSinglePOS || form.getUpdateCustomMerchant() != null && form.getUpdateCustomMerchant()) {
                form.setCustomCardMerchantRealId(super.validateRandomId(webSecurityForm, "customCardMerchantAccountRandomId",
                                                                        "label.settings.locations.paystationList.information.creditCardMerchantAccount",
                                                                        form.getCustomCardMerchantRandomId(), keyMapping, MerchantAccount.class,
                                                                        Integer.class, WebCoreConstants.EMPTY_STRING));
            }
            if (isSinglePOS || form.getUpdateLocations() != null && form.getUpdateLocations()) {
                Object requiredCheck = super.validateRequired(webSecurityForm, "locationRandomId", "label.location", form.getLocationRandomId());
                if (requiredCheck != null) {
                    form.setLocationRealId(super.validateRandomId(webSecurityForm, "locationRandomId", "label.location", form.getLocationRandomId(),
                                                                  keyMapping, Location.class, Integer.class));
                }
            }
            
        }
    }
}
