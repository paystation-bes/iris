package com.digitalpaytech.validation.systemadmin;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.support.CustomerSearchForm;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.validation.BaseValidator;

@Component("customerSearchValidator")
public class CustomerSearchValidator extends BaseValidator<CustomerSearchForm> {
    @Override
    public void validate(WebSecurityForm<CustomerSearchForm> command, Errors errors) {
    	WebSecurityForm<CustomerSearchForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        if(webSecurityForm == null){
            return;
        }
        
        if(!super.validateActionFlag(webSecurityForm, WebSecurityConstants.ACTION_SEARCH)){            
            return;
        }
        
        CustomerSearchForm form = webSecurityForm.getWrappedObject();
        String randomId = (String) super.validateRequired(webSecurityForm, "search", "label.systemAdmin.customerPayStation", form.getRandomId());
        Integer actualId = super.validateRandomId(webSecurityForm, "search", "label.systemAdmin.customerPayStation", randomId, keyMapping);      
    }
    
}
