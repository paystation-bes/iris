package com.digitalpaytech.validation.systemadmin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.TimezoneVId;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.support.CustomerEditForm;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.validation.BaseValidator;

@Component("customerValidator")
public class CustomerValidator extends BaseValidator<CustomerEditForm> {
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    //TODO can be reactivated after all customers are migrated
    @Override
    public void validate(WebSecurityForm<CustomerEditForm> command, Errors errors) {
        //        WebSecurityForm<CustomerEditForm> webSecurityForm = prepareSecurityForm(command, errors, "postToken");
        //        if (webSecurityForm == null) {
        //            return;
        //        }
        //        
        //        RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        //        if (!super.validateActionFlag(webSecurityForm, WebSecurityConstants.ACTION_UPDATE, WebSecurityConstants.ACTION_CREATE)) {
        //            return;
        //        }
        //        this.checkId(webSecurityForm, keyMapping);
        //        // check that no change is being made to child/parent status
        //        this.validateAccountIsParentStatus(webSecurityForm);
        //        this.validateLegalName(webSecurityForm);
        //        this.validateParentCompany(webSecurityForm, keyMapping);
        //        this.validateAdminUserName(webSecurityForm);
        //        this.validateAccountStatus(webSecurityForm);
        //        CustomerEditForm customerEditForm = webSecurityForm.getWrappedObject();
        //        if (customerEditForm.getHidePassword() == null || !customerEditForm.getHidePassword()) {
        //            this.validatePassword(webSecurityForm);
        //        }
    }
    
    public boolean validateWithMigrated(WebSecurityForm<CustomerEditForm> command, Errors errors) {
        boolean isMigrated = true;
        
        WebSecurityForm<CustomerEditForm> webSecurityForm = prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return false;
        }
        
        RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        if (!super.validateActionFlag(webSecurityForm, WebSecurityConstants.ACTION_UPDATE, WebSecurityConstants.ACTION_CREATE)) {
            return false;
        }
        this.checkId(webSecurityForm, keyMapping);
        
        CustomerEditForm form = (CustomerEditForm) webSecurityForm.getWrappedObject();
        Integer customerId = form.getCustomerId();
        Customer customer = customerId != null ? customerService.findCustomer(customerId) : null;
        
        if (customer != null) {
            isMigrated = customer.isIsMigrated();
        }
        
        this.validateAdminUserName(webSecurityForm);
        CustomerEditForm customerEditForm = webSecurityForm.getWrappedObject();
        if (customerEditForm.getHidePassword() == null || !customerEditForm.getHidePassword()) {
            this.validatePassword(webSecurityForm);
        }
        
        if (isMigrated) {            
            // check that no change is being made to child/parent status
            this.validateAccountIsParentStatus(webSecurityForm, customer);
            this.validateLegalName(webSecurityForm);
            this.validateParentCompany(webSecurityForm, keyMapping);
            this.validateAccountStatus(webSecurityForm);
            this.validateTimeZone(webSecurityForm);
            this.validateUnifiId(webSecurityForm, customer);
        }
        
        return isMigrated;
    }

    /*
     * Validations:
     * 1. If UnifiId is set already, it should be ReadOnly. If it is not set, then it can be updated.
     */
    private void validateUnifiId(final WebSecurityForm<CustomerEditForm> webSecurityForm, final Customer customer) {
        if (customer == null) {
            return;
        }
        final Integer currentUnifiId = customer.getUnifiId();
        final Integer newUnifiId = webSecurityForm.getWrappedObject().getUnifiId();
        if (currentUnifiId != null && !currentUnifiId.equals(newUnifiId)) {
            final String label = this.getMessage("label.customer.unifiId");
            webSecurityForm.addErrorStatus(new FormErrorStatus("unifiId", this.getMessage("error.common.invalid.update", new Object[] { label })));
        }
    }
    
    public final void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final void setCustomerAdminService(CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    private void validateAccountIsParentStatus(WebSecurityForm webSecurityForm, Customer customer) {
        CustomerEditForm form = (CustomerEditForm) webSecurityForm.getWrappedObject();
        if (customer == null) {
            return;
        }
        if (form.getIsParentCompany() != customer.isIsParent()) {
            String message;
            if (form.getIsParentCompany()) {
                message = "Cannot change customer from child to parent";
            } else {
                message = "Cannot change customer from parent to child";
            }
            webSecurityForm.addErrorStatus(new FormErrorStatus("", message));
        }
    }
    
    /**
     * This method validates the form's random id and returns a real id.
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     * 
     * @param keyMapping
     *            RandomKeyMapping object
     */
    private void checkId(WebSecurityForm<CustomerEditForm> webSecurityForm, RandomKeyMapping keyMapping) {
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
            CustomerEditForm form = webSecurityForm.getWrappedObject();
            validateRequired(webSecurityForm, FormFieldConstants.PARAMETER_CUSTOMER_ID, "label.systemAdmin.customer.id", form.getRandomCustomerId());
            Integer id = super.validateRandomId(webSecurityForm, FormFieldConstants.PARAMETER_CUSTOMER_ID, "label.systemAdmin.customer.id",
                                                form.getRandomCustomerId(), keyMapping);
            form.setCustomerId(id);
        }
    }
    
    /**
     * This method validates user input customer legal name.
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     */
    private void validateLegalName(WebSecurityForm<CustomerEditForm> webSecurityForm) {
        CustomerEditForm form = webSecurityForm.getWrappedObject();
        String name = (String) super.validateRequired(webSecurityForm, "legalName", "label.systemAdmin.customer.legal.name", form.getLegalName());
        if (name != null) {
            super.validateStringLength(webSecurityForm, "legalName", "label.systemAdmin.customer.legal.name", name,
                                       WebCoreConstants.VALIDATION_MIN_LENGTH_4, WebCoreConstants.VALIDATION_MAX_LENGTH_25);
            super.validatePrintableText(webSecurityForm, "legalName", "label.systemAdmin.customer.legal.name", name);
        }
    }
    
    /**
     * This method validates user input parent company field.
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     * @param keyMapping
     *            RandomKeyMapping object
     */
    private void validateParentCompany(WebSecurityForm<CustomerEditForm> webSecurityForm, RandomKeyMapping keyMapping) {
        CustomerEditForm form = webSecurityForm.getWrappedObject();
        form.setParentCustomerId(super.validateRandomId(webSecurityForm, "parentCompany", "label.systemAdmin.parent.company",
                                                        form.getRandomParentCustomerId(), keyMapping));
    }
    
    /**
     * This method validates user input admin user name.
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     */
    private void validateAdminUserName(WebSecurityForm<CustomerEditForm> webSecurityForm) {
        CustomerEditForm form = webSecurityForm.getWrappedObject();
        form.setUserName(form.getUserName().trim());
        String name = (String) super.validateRequired(webSecurityForm, "userName", "label.systemAdmin.user.name", form.getUserName());
        if (name != null) {
            super.validateStringLength(webSecurityForm, "userName", "label.systemAdmin.user.name", name, WebSecurityConstants.USERNAME_MIN_LENGTH,
                                       WebSecurityConstants.USERNAME_MAX_LENGTH);
            super.validatePrintableText(webSecurityForm, "userName", "label.systemAdmin.user.name", name);
        }
    }
    
    /**
     * This method validates user input account status (enabled, disabled, trial).
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     */
    private void validateAccountStatus(WebSecurityForm<CustomerEditForm> webSecurityForm) {
        CustomerEditForm form = webSecurityForm.getWrappedObject();
        String status = form.getAccountStatus();
        if (status == null || !(status.equals("enabled") || status.equals("disabled") || status.equals("trial"))) {
            String label = this.getMessage("label.systemAdmin.account.status");
            webSecurityForm.addErrorStatus(new FormErrorStatus("accountEnabled", this.getMessage("error.common.invalid", new Object[] { label })));
            return;
        }
        
        if (status.equals("trial")) {
            String trialDateString = form.getTrialDateString();
            if (trialDateString == null) {
                String label = this.getMessage("label.systemAdmin.trialEndDate");
                webSecurityForm.addErrorStatus(new FormErrorStatus("trialEndDat", this.getMessage("error.common.required", new Object[] { label })));
            } else {
                String formatString = "MM/dd/yyyy";
                SimpleDateFormat format = new SimpleDateFormat(formatString);
                Date trialDate = null;
                try {
                    trialDate = format.parse(trialDateString);
                    Date currentDate = DateUtil.createStartOfTodayDate();
                    if (trialDate.before(currentDate)) {
                        String label = this.getMessage("label.systemAdmin.trialEndDate");
                        webSecurityForm.addErrorStatus(new FormErrorStatus("trialEndDat", this
                                .getMessage("error.common.invalid.date.not.greater.nor.equals", new Object[] { "current date", label })));
                    }
                } catch (ParseException e) {
                    String label = this.getMessage("label.systemAdmin.trialEndDate");
                    webSecurityForm.addErrorStatus(new FormErrorStatus("trialEndDat", this.getMessage("error.common.invalid.date", new Object[] {
                        label, formatString })));
                }
            }
        }
    }
    
    /**
     * This method validates user input password.
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     */
    private void validatePassword(WebSecurityForm<CustomerEditForm> webSecurityForm) {
        
        CustomerEditForm form = webSecurityForm.getWrappedObject();
        String userPassword = (String) super.validateRequired(webSecurityForm, "adminPass", "label.systemAdmin.admin.password", form.getPassword());
        String passwordConfirm = (String) super.validateRequired(webSecurityForm, "passConfirm", "label.systemAdmin.password.confirm",
                                                                 form.getPasswordConfirmed());
        
        if (userPassword == null || passwordConfirm == null) {
            return;
        }
        
        if (!userPassword.equals(passwordConfirm)) {
            String label = this.getMessage("label.systemAdmin.password.confirm");
            webSecurityForm.addErrorStatus(new FormErrorStatus("adminPass", this.getMessage("error.common.invalid", new Object[] { label })));
        }
        
        super.validateStringLength(webSecurityForm, "adminPass", "label.systemAdmin.admin.password", userPassword,
                                   WebSecurityConstants.USER_PASSWORD_MIN_LENGTH, WebSecurityConstants.USER_PASSWORD_MAX_LENGTH);
        super.validateStringLength(webSecurityForm, "passConfirm", "label.systemAdmin.password.confirm", passwordConfirm,
                                   WebSecurityConstants.USER_PASSWORD_MIN_LENGTH, WebSecurityConstants.USER_PASSWORD_MAX_LENGTH);
        super.validateRegEx(webSecurityForm, "adminPass", "label.systemAdmin.admin.password", userPassword,
                            WebSecurityConstants.COMPLEX_PASSWORD_EXP, "error.user.password.not.complex", null);
    }
    
    /**
     * This method validates customer time zone.
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     */
    private void validateTimeZone(WebSecurityForm<CustomerEditForm> webSecurityForm) {
        CustomerEditForm form = webSecurityForm.getWrappedObject();
        String timeZone = form.getCurrentTimeZone();
        if (timeZone != null && !timeZone.equals(WebCoreConstants.BLANK) && !timeZone.equals(WebCoreConstants.CUSTOMER_TIME_ZONE_NO_SELECTION)) {
            
            timeZone = super.validateStringLength(webSecurityForm, "selectTimeZone", "label.settings.globalPref.selected.timeZone", timeZone,
                                                  WebCoreConstants.VALIDATION_MIN_LENGTH_1, WebCoreConstants.VALIDATION_MAX_LENGTH_30);
            
            timeZone = super.validateRegEx(webSecurityForm, "selectTimeZone", "label.settings.globalPref.selected.timeZone", timeZone,
                                           WebCoreConstants.REGEX_TIME_ZONE, "error.common.invalid.special.character.name", "error.regex.timezone");
            
            /* validate if the input value is existing in the time zone list. */
            if (!isExistingTimeZone(timeZone)) {
                webSecurityForm.addErrorStatus(new FormErrorStatus("selectTimeZone", this.getMessage("error.common.required", this
                        .getMessage("label.settings.globalPref.selected.timeZone"))));
            }
        }
    }
    
    private boolean isExistingTimeZone(String timeZone) {
        boolean result = false;
        List<TimezoneVId> timezones = customerAdminService.getTimeZones();
        for (TimezoneVId tz : timezones) {
            if (tz.getName().equals(timeZone)) {
                result = true;
                break;
            }
        }
        return result;
    }
}
