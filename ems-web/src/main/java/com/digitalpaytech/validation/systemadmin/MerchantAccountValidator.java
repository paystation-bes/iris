package com.digitalpaytech.validation.systemadmin;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.support.MerchantAccountEditForm;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.validation.BaseValidator;

@Component("merchantAccountValidator")
public class MerchantAccountValidator extends BaseValidator<MerchantAccountEditForm> {
    
    private static final String DOT = ".";
    
    /* 
     * 4  - Link2Gov, 7  - Paymentech,      11 - AuthorizeNet,    13 - BlackBoard, 14 - TotalCard, 15 - NuVision, 
     * 16 - Elavon,   17 - CBORD - CS Gold, 18 - CBORD - Odyssey, 19 - Elavon via Conex  21 - TDMerchant
     */
    private static final int[] PROCESSOR_IDS_FOR_FIELD3_TO_5 = { 4, 7, 11, 13, 14, 15, 16, 17, 18, 19, 20, 21 };    
    /* 
     * 7  - Paymentech,      13 - BlackBoard,  14 - TotalCard, 15 - NuVision, 17 - CBORD - CS Gold, 
     * 18 - CBORD - Odyssey
     */
    private static final int[] PROCESSOR_IDS_FOR_FIELD4 = { 7,  13, 14, 15, 17, 18, 20 };            
    private static final int[] PROCESSOR_IDS_FOR_FIELD5 = { 7, 18, 20 };
    
    
    private static Logger log = Logger.getLogger(MerchantAccountValidator.class);
    
    // Processor IDs are hardcoded if they are changed in the DB they need to change here too
    //    1  - Concord
    //    2  - First Data Nashville
    //    3  - Heartland
    //    4  - Link2Gov
    //    5  - Parcxmart
    //    6  - Alliance
    //    7  - Paymenttech
    //    8  - Paradata
    //    9  - Moneris
    //    10 - First Horizon
    //    11 - AuthorizeNet
    //    12 - Datawire
    //    13 - BlackBoard
    //    14 - TotalCard
    //    15 - NuVision
    //    16 - Elavon (Converge)
    //    17 - CBORD - CS Gold
    //    18 - CBORD - Odyssey
    //    19 - Elavon viaConex
    //    20 - CreditCall
    //    21 - TD Merchant Services
    //    22 - Heartland SP+
    
    @Autowired
    private MerchantAccountService merchantAccountService;
    
    @Override
    public void validate(final WebSecurityForm<MerchantAccountEditForm> command, final Errors errors) {
    }
    
    public final void validate(final WebSecurityForm<MerchantAccountEditForm> command, final Errors errors, final Processor processor) {
        final WebSecurityForm<MerchantAccountEditForm> webSecurityForm = prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        } 
        
        if (!super.validateActionFlag(webSecurityForm, WebSecurityConstants.ACTION_UPDATE, WebSecurityConstants.ACTION_CREATE)) {            
            return;
        }
        final RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        this.checkId(webSecurityForm, keyMapping);
        if (this.checkCustomerId(webSecurityForm, keyMapping)) {        
            validateName(webSecurityForm, errors);
            validateFields(webSecurityForm, errors, processor);
        }
        super.validateIntegerLimits(webSecurityForm, "quarterOfTheDay", "label.merchantAccount.endOfDay.time", webSecurityForm.getWrappedObject()
                .getQuarterOfTheDay(), 0, 95);
        if (webSecurityForm.getWrappedObject().getTimeZone() == null 
                || WebCoreConstants.EMPTY_STRING.equals(webSecurityForm.getWrappedObject().getTimeZone())) {
            webSecurityForm.getWrappedObject().setTimeZone(WebCoreConstants.GMT);
        }
    }
    
    /**
     * This method validates the form's random id and returns a real id.
     * 
     * @param webSecurityForm WebSecurityForm object
     * 
     * @param keyMapping RandomKeyMapping object
     */ 
    private void checkId(WebSecurityForm<MerchantAccountEditForm> webSecurityForm, RandomKeyMapping keyMapping) {
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
            MerchantAccountEditForm form = (MerchantAccountEditForm) webSecurityForm.getWrappedObject();
            validateRequired(webSecurityForm, "merchantAccountId", "label.systemAdmin.workspace.customer.merchantAccount", form.getRandomId());
            Integer id = super.validateRandomId(webSecurityForm, "merchantAccountId", "label.systemAdmin.workspace.customer.merchantAccount", form.getRandomId(), keyMapping);
            form.setMerchantAccountId(id);
        }
    }     
    
    
    /**
     * This method validates the form's random customer id and returns a real customer id.
     * 
     * @param webSecurityForm WebSecurityForm object
     * 
     * @param keyMapping RandomKeyMapping object
     */ 
    private boolean checkCustomerId(WebSecurityForm<MerchantAccountEditForm> webSecurityForm, RandomKeyMapping keyMapping) {         
        MerchantAccountEditForm form = (MerchantAccountEditForm) webSecurityForm.getWrappedObject();
        validateRequired(webSecurityForm, FormFieldConstants.PARAMETER_CUSTOMER_ID, "label.systemAdmin.customer.id", form.getCustomerId());
        Integer id = super.validateRandomId(webSecurityForm, FormFieldConstants.PARAMETER_CUSTOMER_ID, "label.systemAdmin.customer.id", form.getCustomerId(), keyMapping);
        form.setCustomerRealId(id);
        if(id == null){
            return false;
        }
        return true;
    }       
    
    public void validateName(WebSecurityForm<MerchantAccountEditForm> webSecurityForm, Errors errors) {
        MerchantAccountEditForm form = (MerchantAccountEditForm) webSecurityForm.getWrappedObject();
        String name = (String) super.validateRequired(webSecurityForm, "accountName", "label.merchantAccount.name", form.getName());
        if (name != null) {
            super.validateStringLength(webSecurityForm, "accountName", "label.merchantAccount.name", name, WebSecurityConstants.MERCHANT_ACCOUNT_NAME_MIN_LENGTH, WebSecurityConstants.MERCHANT_ACCOUNT_NAME_MAX_LENGTH);
            super.validateStandardText(webSecurityForm, "accountName", "label.merchantAccount.name", name);
        }
    }
    
    
    private static String combineKeyAndProcessorId(String messageKey, int processorId) {
        StringBuilder bdr = new StringBuilder();
        bdr.append(messageKey).append(DOT).append(processorId);
        return bdr.toString();
    }
    
    private static String removeDash(String value) {
        String newValue;
        if (StringUtils.isNotBlank(value)) {
            try {
                newValue = value.replaceAll("-", "");
            } catch (Exception e) {
                log.error("Cannot do 'replaceAll' on value: " + value, e);
                return value;
            }
        } else {
            newValue = "";
        }
        return newValue;
    }
    
    /** 
     * Check if selected processor need validation. 
     * @param int[] int array contains required processor ids
     * @param int selected processor id
     * @return boolean true if needs to be validated. 
     */
    private boolean checkRequiredFields(final int[] requiredProcessorIds, final int processorId) {
        for (int i = 0; i < requiredProcessorIds.length; i++) {
            if (requiredProcessorIds[i] == processorId) {
                return true;
            }
        }
        return false;
    }
    
    public void validateFields(WebSecurityForm<MerchantAccountEditForm> webSecurityForm, Errors errors, Processor processor) {
        MerchantAccountEditForm form = (MerchantAccountEditForm) webSecurityForm.getWrappedObject();
        
        int processorId = processor.getId();
        
        // 4 - Link2Gov
        if (processorId == 4) {
            // e.g. ABCDE-ABCDE-ABCDE-A  ->  ABCDEABCDEABCDEA (16 alphanumeric)
            String field1 = removeDash(form.getField1());
            if(super.validateRequired(webSecurityForm, "field1", MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field1", processorId), field1) != null) {
                super.validateStringLength(webSecurityForm, 
                                           "field1", 
                                           MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field1", processorId), 
                                           field1, 
                                           WebSecurityConstants.MERCHANT_ACCOUNT_LINK2GOV_MERCHANT_CODE_LENGTH);
            }
            // e.g. ABCDE-ABCDE-ABCDE-11  ->  ABCDEABCDEABCDE11 (17 alphanumeric)
            String field2 = removeDash(form.getField2());
            if(super.validateRequired(webSecurityForm, "field2", MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field2", processorId), field2) != null) {
                super.validateStringLength(webSecurityForm, 
                                           "field2", 
                                           MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field2", processorId), 
                                           field2, 
                                           WebSecurityConstants.MERCHANT_ACCOUNT_LINK2GOV_SETTLE_MERCHANT_CODE_LENGTH);
            }        
            
        
        } else if (processorId == 19) {
            if (super.validateRequired(webSecurityForm, "field1", MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field2", processorId), form.getField1()) != null) {
                // Length is 22.
                super.validateStringLength(webSecurityForm, 
                                           "field1", 
                                           // message.properties, label.merchantAccount.field2.19 = Terminal ID
                                           MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field2", processorId), 
                                           form.getField1(), 
                                           WebSecurityConstants.MERCHANT_ACCOUNT_ELAVON_VIA_CONEX_TERMINAL_ID_LENGTH);
            }
            super.validateRequired(webSecurityForm, "field2", MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field1", processorId), form.getField2());            
            super.validateNumber(webSecurityForm, "field1",
                                 MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field2", processorId), form.getField1());
            super.validateNumber(webSecurityForm, "field2",
                                 MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field1", processorId), form.getField2());                
        
        } else if (processorId == 20) {
            final Object[] disallowedValues = { String.valueOf(WebCoreConstants.INT_RECORD_NOT_FOUND) };
            super.validateRequired(webSecurityForm, 
                                   "field1", 
                                   MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field1", processorId), 
                                   form.getField1(), 
                                   disallowedValues);
            super.validateStringLength(webSecurityForm, 
                                       "field2", 
                                       MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field2", processorId), 
                                       form.getField2(), 
                                       WebSecurityConstants.MERCHANT_ACCOUNT_FIELDS_MIN_LENGTH, 
                                       WebSecurityConstants.MERCHANT_ACCOUNT_FIELDS_MAX_LENGTH);
             
        } else {
            if(super.validateRequired(webSecurityForm, "field1", MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field1", processorId), form.getField1()) != null) {
                super.validateStringLength(webSecurityForm, 
                                           "field1", 
                                           MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field1", processorId), 
                                           form.getField1(), 
                                           WebSecurityConstants.MERCHANT_ACCOUNT_FIELDS_MIN_LENGTH, 
                                           WebSecurityConstants.MERCHANT_ACCOUNT_FIELDS_MAX_LENGTH);
            }
            if(super.validateRequired(webSecurityForm, "field2", MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field2", processorId), form.getField2()) != null) {
                super.validateStringLength(webSecurityForm, 
                                           "field2", 
                                           MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field2", processorId), 
                                           form.getField2(), 
                                           WebSecurityConstants.MERCHANT_ACCOUNT_FIELDS_MIN_LENGTH, 
                                           WebSecurityConstants.MERCHANT_ACCOUNT_FIELDS_MAX_LENGTH);
            }        
        }
        
        if (processorId == 21) {
            super.validateNumber(webSecurityForm, "field1",
                                 MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field1", processorId), form.getField1());
            super.validateAlphaNumeric(webSecurityForm, "field2",
                                       MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field2", processorId),
                                       form.getField2());
        }
        
        if (checkRequiredFields(PROCESSOR_IDS_FOR_FIELD3_TO_5, processorId)) {
            
            if (super.validateRequired(webSecurityForm, "field3", MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field3", processorId), form.getField3()) != null) {
                int minLength = 0;
            	int maxLength = 0;
                
                if (processorId == 4) {
            		minLength = WebSecurityConstants.MERCHANT_ACCOUNT_LINK2GOV_MIN_PASSWORD_LENGTH;
            		maxLength = WebSecurityConstants.MERCHANT_ACCOUNT_LINK2GOV_MAX_PASSWORD_LENGTH;
                } else if (processorId == 19) {
                    minLength = WebSecurityConstants.MERCHANT_ACCOUNT_ELAVON_VIA_CONEX_MIN_CLOSE_BATCH_LENGTH;
                    maxLength = WebSecurityConstants.MERCHANT_ACCOUNT_ELAVON_VIA_CONEX_MAX_CLOSE_BATCH_LENGTH;  
                } else if (processorId == 20) {
                    super.validateStringLength(webSecurityForm, 
                                               "field3", 
                                               MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field3", processorId), 
                                               form.getField3(), 
                                               WebSecurityConstants.MERCHANT_ACCOUNT_CREDITCALL_TERMINALID_CODE_LENGTH);  
                    super.validateNumber(webSecurityForm, "field3",
                                         MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field3", processorId), form.getField3());
                } else {
                    minLength = WebSecurityConstants.MERCHANT_ACCOUNT_FIELDS_MIN_LENGTH;
                    maxLength = WebSecurityConstants.MERCHANT_ACCOUNT_FIELDS_MAX_LENGTH;
            	}
                super.validateStringLength(webSecurityForm, 
                                           "field3", 
                                           MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field3", processorId), 
                                           form.getField3(), 
                                           minLength, 
                                           maxLength);
            }
            
            if (processorId == 4) {
            	validateLink2GovPasswordField(webSecurityForm, errors, processorId);
            }
            
            if (processorId == 19) {
                validateElavonViaConexCloseBatchSize(webSecurityForm, errors, processorId);
            }
            
            if (processorId == 21) {
                Object[] disallowedValues = { String.valueOf(WebCoreConstants.INT_RECORD_NOT_FOUND) };
                super.validateRequired(webSecurityForm, 
                                       "field3", 
                                       MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field3", processorId), 
                                       form.getField3(), 
                                       disallowedValues);
            }
            
            if (checkRequiredFields(PROCESSOR_IDS_FOR_FIELD4, processorId)) {
                if (super.validateRequired(webSecurityForm, "field4",
                                           MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field4", processorId),
                                           form.getField4()) != null) {
                    if (processorId == 20) {
                        super.validateStringLength(webSecurityForm, "field4",
                                                   MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field4", processorId),
                                                   form.getField4(), WebSecurityConstants.MERCHANT_ACCOUNT_FIELDS_MIN_LENGTH,
                                                   WebSecurityConstants.MERCHANT_ACCOUNT_CREDITCALL_TRANSACTION_KEY_MAX_LENGTH);
                        super.validateAlphaNumeric(webSecurityForm, "field4",
                                                   MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field4", processorId),
                                                   form.getField4());
                    } else {
                        super.validateStringLength(webSecurityForm, "field4",
                                                   MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field4", processorId),
                                                   form.getField4(), WebSecurityConstants.MERCHANT_ACCOUNT_FIELDS_MIN_LENGTH,
                                                   WebSecurityConstants.MERCHANT_ACCOUNT_FIELDS_MAX_LENGTH);
                    }
                }
            } else if (processorId == 16) {
                Object[] disallowedValues = { String.valueOf(WebCoreConstants.INT_RECORD_NOT_FOUND) };
                super.validateRequired(webSecurityForm, "field4",
                                       MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field4", processorId),
                                       form.getField4(), disallowedValues);
            }
            
            if (checkRequiredFields(PROCESSOR_IDS_FOR_FIELD5, processorId)) {
                if (super.validateRequired(webSecurityForm, "field5",
                                           MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field5", processorId),
                                           form.getField5()) != null) {
                    if (processorId == 20) {
                        final Object[] disallowedValues = { String.valueOf(WebCoreConstants.INT_RECORD_NOT_FOUND) };
                        super.validateRequired(webSecurityForm, "field5",
                                               MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field5", processorId),
                                               form.getField5(), disallowedValues);
                    } else {
                        super.validateStringLength(webSecurityForm, "field5",
                                                   MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field5", processorId),
                                                   form.getField5(), WebSecurityConstants.MERCHANT_ACCOUNT_FIELDS_MIN_LENGTH,
                                                   WebSecurityConstants.MERCHANT_ACCOUNT_FIELDS_MAX_LENGTH);
                    }
                }
            }
        } else if (processorId == 2) {
            // First Data Nashville
            if (super.validateRequired(webSecurityForm, "field5", MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field5", processorId), form.getField5()) != null) {
                super.validateStringLength(webSecurityForm, 
                                           "field5", 
                                           MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field5", processorId), 
                                           form.getField5(), 
                                           WebSecurityConstants.MERCHANT_ACCOUNT_FIELDS_MIN_LENGTH, 
                                           WebSecurityConstants.MERCHANT_ACCOUNT_FIELDS_MAX_LENGTH);
            }                 
            if (super.validateRequired(webSecurityForm, "field6", MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field6", processorId), form.getField6()) != null) {
                super.validateStringLength(webSecurityForm, 
                                           "field6", 
                                           MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field6", processorId), 
                                           form.getField6(), 
                                           WebSecurityConstants.MERCHANT_ACCOUNT_FIELDS_MIN_LENGTH, 
                                           WebSecurityConstants.MERCHANT_ACCOUNT_FIELDS_MAX_LENGTH);
            }                 
        }
        
        // currently there are no processors using 6 fields
        //        if (false) {
//                    if(super.validateRequired(webSecurityForm, "field6", "label.merchantAccount.field6", form.getField6()) != null){
//                        super.validateStringLength(webSecurityForm, "field6", "label.merchantAccount.field6", form.getField6(), WebSecurityConstants.MERCHANT_ACCOUNT_FIELDS_MIN_LENGTH, WebSecurityConstants.MERCHANT_ACCOUNT_FIELDS_MAX_LENGTH);
//                    }    
        //        }
    }
    
    
    public void validateAccountWithProcessorAndField1And2(MerchantAccount merchantAccount, Errors errors) {
        Object[] objs = WebCoreUtil.createObjectArray("processorId", "field1", "field2");
        String[] params = (String[]) Arrays.asList(objs).toArray(new String[objs.length]);
        
        Object[] values = WebCoreUtil.createObjectArray(merchantAccount.getProcessor().getId(), merchantAccount.getField1(), merchantAccount.getField2());
        
        List<MerchantAccount> merchatAccts = merchantAccountService.findByProcessorAndFields(params, values);
        if (merchatAccts.size() > 0) {
            StringBuilder bdr = new StringBuilder();
            Iterator<MerchantAccount> iter = merchatAccts.iterator();
            while (iter.hasNext()) {
                bdr.append(iter.next().getName()).append(", ");
            }
            String err = "error.merchantAccount.field.inUse";
            errors.reject(err, new Object[] { bdr.toString() }, err);
        }
    }
        
    //as per ems6 and required by ems 3275
    private void validateLink2GovPasswordField(WebSecurityForm<MerchantAccountEditForm> webSecurityForm, Errors errors, int processorId) {
    	MerchantAccountEditForm form = webSecurityForm.getWrappedObject();
    	String password = form.getField3();
    	if (password == null || !password.matches(WebCoreConstants.REGEX_ALPHANUMERIC_LINK2GOV)){
    		String err = getMessage("error.common.invalid.special.character.name", new Object[]{"password", getMessage("error.regex.text.link2gov")});
    		
    		this.addError(webSecurityForm, MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field3", processorId) , err);
    	}
    }
    
    private final void validateElavonViaConexCloseBatchSize(WebSecurityForm<MerchantAccountEditForm> webSecurityForm, Errors errors, int processorId) {
        final MerchantAccountEditForm form = webSecurityForm.getWrappedObject();
        this.validateNumber(webSecurityForm, "field3", MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field3", processorId), form.getField3());
        this.validateIntegerRangeString(webSecurityForm, "field3", MerchantAccountValidator.combineKeyAndProcessorId("label.merchantAccount.field3", processorId), 
                                        form.getField3(), 1, WebSecurityConstants.MERCHANT_ACCOUNT_ELAVON_VIA_CONEX_MAX_CLOSE_BATCH_SIZE);
    }
    
    public void setMerchantAccountService(MerchantAccountService merchantAccountService) {
        this.merchantAccountService = merchantAccountService;
    }
}
