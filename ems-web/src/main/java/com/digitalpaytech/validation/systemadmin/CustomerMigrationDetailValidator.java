package com.digitalpaytech.validation.systemadmin;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.support.CustomerMigrationDetailForm;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.LabelConstants;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.validation.BaseValidator;

@Component("customerMigrationDetailValidator")
public class CustomerMigrationDetailValidator extends BaseValidator<CustomerMigrationDetailForm> {
    
    public final void validate(final WebSecurityForm<CustomerMigrationDetailForm> command, final Errors errors) {
        final WebSecurityForm<CustomerMigrationDetailForm> webSecurityForm = prepareSecurityForm(command, errors, WebCoreConstants.POSTTOKEN_STRING);
        if (webSecurityForm == null) {
            return;
        }
        
        validateEmailAddresses(webSecurityForm);
        validateScheduledTime(webSecurityForm);
    }
    
    private void validateEmailAddresses(final WebSecurityForm<CustomerMigrationDetailForm> webSecurityForm) {
        final CustomerMigrationDetailForm form = webSecurityForm.getWrappedObject();
        final List<String> emailList = new ArrayList<String>();
        if (form.getMigrationContacts() == null || form.getMigrationContacts().isEmpty()) {
            addError(webSecurityForm, FormFieldConstants.MIGRATION_CONTACTS,
                     this.getMessage(ErrorConstants.REQUIRED, this.getMessage(LabelConstants.LABEL_EMAIL)));
        } else {
            validateRequired(webSecurityForm, FormFieldConstants.MIGRATION_CONTACTS, LabelConstants.LABEL_EMAIL, form.getMigrationContacts());
            final String[] splitEmails = form.getMigrationContacts().get(0).split(StandardConstants.STRING_COMMA);
            for (String email : splitEmails) {
                validateRequired(webSecurityForm, FormFieldConstants.MIGRATION_CONTACTS, LabelConstants.LABEL_EMAIL, email);
                validateEmailAddress(webSecurityForm, FormFieldConstants.MIGRATION_CONTACTS, LabelConstants.LABEL_EMAIL, email);
                emailList.add(email.toLowerCase());
            }
            form.setMigrationContacts(emailList);
        }
    }
    
    private void validateScheduledTime(final WebSecurityForm<CustomerMigrationDetailForm> webSecurityForm) {
        
        final CustomerMigrationDetailForm form = webSecurityForm.getWrappedObject();
        final TimeZone timeZone = TimeZone.getTimeZone(WebCoreConstants.SYSTEM_ADMIN_UI_TIMEZONE);
        
        final Calendar defaultDate = Calendar.getInstance();
        defaultDate.setTimeZone(timeZone);
        defaultDate.set(Calendar.HOUR_OF_DAY, 0);
        defaultDate.set(Calendar.MINUTE, 0);
        defaultDate.set(Calendar.SECOND, 0);
        defaultDate.set(Calendar.MILLISECOND, 0);
        defaultDate.add(Calendar.DAY_OF_YEAR, StandardConstants.DAYS_IN_A_WEEK);
        if (defaultDate.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
            defaultDate.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        } else if (defaultDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            defaultDate.add(Calendar.DAY_OF_YEAR, StandardConstants.DAYS_IN_A_WEEK);
        }
        
        Date scheduleRealDate = null;
        if (form.getScheduleDate() != null) {
            if (form.getScheduleTime() != null && !"-1".equals(form.getScheduleTime())
                && !StandardConstants.STRING_EMPTY_STRING.equals(form.getScheduleTime())) {
                scheduleRealDate = validateDateTime(webSecurityForm, FormFieldConstants.SCHEDULE_DATE, LabelConstants.LABEL_SCHEDULE_DATE,
                                                    form.getScheduleDate(), FormFieldConstants.SCHEDULE_TIME, LabelConstants.LABEL_SCHEDULE_DATE,
                                                    form.getScheduleTime(), timeZone);
            } else {
                scheduleRealDate = validateDate(webSecurityForm, FormFieldConstants.SCHEDULE_DATE, LabelConstants.LABEL_SCHEDULE_DATE,
                                                form.getScheduleDate(), timeZone);
                
            }
        } else {
            scheduleRealDate = defaultDate.getTime();
        }
        form.setScheduleRealDate(scheduleRealDate);
    }
}
