package com.digitalpaytech.validation.systemadmin;

import java.util.TimeZone;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.Notification;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.support.NotificationManagementForm;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.validation.BaseValidator;

@Component("notificationManagementValidator")
public class NotificationManagementValidator extends BaseValidator<NotificationManagementForm> {
    @Override
    public void validate(WebSecurityForm<NotificationManagementForm> command, Errors errors) {
        WebSecurityForm<NotificationManagementForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if(webSecurityForm == null){
            return;
        }
        
        if(!super.validateActionFlag(webSecurityForm, WebSecurityConstants.ACTION_UPDATE, WebSecurityConstants.ACTION_CREATE)){            
            return;
        }        
        
        this.checkId(webSecurityForm);        
        this.validateStartAndEndTime(webSecurityForm);
        this.validateTitle(webSecurityForm);
        this.validateMessage(webSecurityForm);
        this.validateLink(webSecurityForm);
    }
    
    /**
     * Converts random id from form into a database id that is saved for later use
     * 
     * @param webSecurityForm WebSecurityForm object
     */ 
    private void checkId(WebSecurityForm<NotificationManagementForm> webSecurityForm) {        
        NotificationManagementForm form = webSecurityForm.getWrappedObject();
        RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        if (webSecurityForm.getActionFlag() == WebSecurityConstants.ACTION_UPDATE) {
            String randomNotificationId = (String) validateRequired(webSecurityForm, "notificationID", "label.systemAdmin.notification.id",  form.getRandomNotificationId());
            Integer notificationId = super.validateRandomId(webSecurityForm, "notificationID", "label.systemAdmin.notification.id", 
                                                randomNotificationId, keyMapping, Notification.class, Integer.class);
            form.setNotificationId(notificationId);
        }
    }    
    
    private void validateStartAndEndTime(WebSecurityForm<NotificationManagementForm> webSecurityForm) {
        NotificationManagementForm form = webSecurityForm.getWrappedObject();
        boolean isInvalid = false;
        
        if (super.validateRequired(webSecurityForm, "startDate", "label.start.date", form.getStartDate()) == null) {
            isInvalid = true;
        }
        if (super.validateRequired(webSecurityForm, "startTime", "label.start.time", form.getStartTime()) == null) {
            isInvalid = true;
        }
        if (super.validateRequired(webSecurityForm, "endDate", "label.end.date", form.getEndDate()) == null) {
            isInvalid = true;
        }
        if (super.validateRequired(webSecurityForm, "endTime", "label.end.time", form.getEndTime()) == null) {
            isInvalid = true;
        }
        
        if (isInvalid) {
            return;
        }
        
        if (super.validateDateTime(webSecurityForm, "startDate", "label.start.date", form.getStartDate(), "startTime", "label.start.time", form.getStartTime(),
                             TimeZone.getTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT)) == null) {
            isInvalid = true;
        }
        
        if (super.validateDateTime(webSecurityForm, "endDate", "label.end.date", form.getEndDate(), "endTime", "label.end.time", form.getEndTime(),
                             TimeZone.getTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT)) == null) {
            isInvalid = true;
        }
        
        if (isInvalid) {
            return;
        }
        
        super.validateLesserOrEqualsValue(webSecurityForm, "startDate,startTime", "label.start.date", form.getStartDateTime(), "endDate,endTime", "label.end.date",
                                    form.getEndDateTime());
    }
    
    private void validateTitle(WebSecurityForm<NotificationManagementForm> webSecurityForm) {
        
        NotificationManagementForm form = webSecurityForm.getWrappedObject();
        
        super.validateRequired(webSecurityForm, "title", "label.title", form.getTitle());
        super.validateStringLength(webSecurityForm, "title", "label.title", form.getTitle(), WebCoreConstants.VALIDATION_MIN_LENGTH_1,
                             WebCoreConstants.VALIDATION_MAX_LENGTH_50);
        super.validateStandardText(webSecurityForm, "title", "label.title", form.getTitle());
    }
    
    private void validateMessage(WebSecurityForm<NotificationManagementForm> webSecurityForm) {
        
        NotificationManagementForm form = webSecurityForm.getWrappedObject();
        String message = super.validateRequired(webSecurityForm, "message", "label.messsage", form.getMessage());
        if(message != null){
            super.validateStringLength(webSecurityForm, "message", "label.messsage", message, WebCoreConstants.VALIDATION_MIN_LENGTH_1,
                                 WebCoreConstants.VALIDATION_MAX_LENGTH_512);
            super.validatePrintableText(webSecurityForm, "message", "label.messsage", message);
        }
    }
    
    private void validateLink(WebSecurityForm<NotificationManagementForm> webSecurityForm) {
        
        NotificationManagementForm form = webSecurityForm.getWrappedObject();
        String link = super.validateStringLength(webSecurityForm, "link", "label.link", form.getLink(), WebCoreConstants.VALIDATION_MIN_LENGTH_1,
                             WebCoreConstants.VALIDATION_MAX_LENGTH_512);
        link = super.validateURL(webSecurityForm, "link", "label.link", form.getLink());
        if(link != null) {
        	if((!link.startsWith("http://")) && (!link.startsWith("https://"))) {
        		form.setLink("http://" + link);
        	}
        }
        
    }
}
