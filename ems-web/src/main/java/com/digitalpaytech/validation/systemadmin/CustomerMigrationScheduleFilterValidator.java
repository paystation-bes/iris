package com.digitalpaytech.validation.systemadmin;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.support.CustomerMigrationScheduleFilterForm;
import com.digitalpaytech.validation.BaseValidator;

@Component("customerMigrationScheduleFilterValidator")
public class CustomerMigrationScheduleFilterValidator extends BaseValidator<CustomerMigrationScheduleFilterForm> {
    @Override
    public final void validate(final WebSecurityForm<CustomerMigrationScheduleFilterForm> command, final Errors errors) {
        final WebSecurityForm<CustomerMigrationScheduleFilterForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
    }
}
