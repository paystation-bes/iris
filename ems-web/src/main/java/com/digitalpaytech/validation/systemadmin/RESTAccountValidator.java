package com.digitalpaytech.validation.systemadmin;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.RestAccount;
import com.digitalpaytech.domain.RestAccountType;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.support.RESTAccountEditForm;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RestAccountTypeService;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.validation.BaseValidator;

@Component("restAccountValidator")
public class RESTAccountValidator extends BaseValidator<RESTAccountEditForm> {
    
    @Autowired
    private RestAccountTypeService restAccountTypeService;

    @Autowired
    private PointOfSaleService pointOfSaleService;

    public void setRestAccountTypeService(RestAccountTypeService restAccountTypeService) {
        this.restAccountTypeService = restAccountTypeService;
    }

    public void setPointOfSaleService(PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    @Override
    public void validate(WebSecurityForm<RESTAccountEditForm> command, Errors errors) {
    }

    public void validate(WebSecurityForm<RESTAccountEditForm> command, Errors errors, RandomKeyMapping keyMapping) {
        WebSecurityForm<RESTAccountEditForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if(webSecurityForm == null){
            return;
        }
  
        if(!super.validateActionFlag(webSecurityForm, WebSecurityConstants.ACTION_UPDATE, WebSecurityConstants.ACTION_CREATE)){            
            return;
        }                
        
        if(checkIds(webSecurityForm, keyMapping) != null){
            checkName(webSecurityForm);
            validateType(webSecurityForm);
            validateVirtualPOS(webSecurityForm, keyMapping);
            validateComments(webSecurityForm);
        }
    }
    
    /**
     * Converts random ids from form into a database ids that are saved for later use
     * 
     * @param webSecurityForm WebSecurityForm object
     */ 
    private Integer checkIds(WebSecurityForm<RESTAccountEditForm> webSecurityForm, RandomKeyMapping keyMapping) {
        RESTAccountEditForm form = webSecurityForm.getWrappedObject();               
        String customerRandomId = (String) super.validateRequired(webSecurityForm, "customerId", "label.systemAdmin.customer.id", form.getCustomerId());        
        if(customerRandomId == null){            
            return null;
        }        
        String randomId = form.getRandomId();        
        
        Integer restAcctId = super.validateRandomId(webSecurityForm, "restAccountId", "", randomId, keyMapping);        
        form.setRestAccountId(restAcctId);                
        Integer customerRealId = super.validateRandomId(webSecurityForm, "customerId", "label.systemAdmin.customer.id", customerRandomId, keyMapping, Customer.class, Integer.class);        
        form.setCustomerRealId(customerRealId);
        return customerRealId;
    }    
    
    private void checkName(WebSecurityForm<RESTAccountEditForm> webSecurityForm) {
        RESTAccountEditForm form = webSecurityForm.getWrappedObject();        
        Object requiredCheck = super.validateRequired(webSecurityForm, "accountName", "label.settings.globalPref.accountName", form.getAccountName());
        if (requiredCheck != null) {
            super.validateStringLength(webSecurityForm, "accountName", "label.settings.globalPref.accountName", form.getAccountName(), WebCoreConstants.VALIDATION_MIN_LENGTH_8,
                                       WebCoreConstants.VALIDATION_MAX_LENGTH_255);
            super.validatePrintableText(webSecurityForm, "accountName", "label.settings.globalPref.accountName", form.getAccountName());
        } 
        return;
    }    
    
    public void validateType(WebSecurityForm<RESTAccountEditForm> webSecurityForm) {
        RESTAccountEditForm form = webSecurityForm.getWrappedObject();               
        String typeName = this.restAccountTypeService.getText(form.getType());        
        super.validateRequired(webSecurityForm, "type", "label.type",  typeName);
    }
    
    public void validateVirtualPOS(WebSecurityForm<RESTAccountEditForm> webSecurityForm, RandomKeyMapping keyMapping) {
        RESTAccountEditForm form = webSecurityForm.getWrappedObject();
        String virtualPosRandomId = (String) super.validateRequired(webSecurityForm, "virtualPOSId", "label.settings.globalPref.virtualPS",  form.getVirtualPOSId(), "-1");                
        if (virtualPosRandomId == null) {            
            return;
        }
        
        Integer virtualPosRealId = super.validateRandomId(webSecurityForm, "virtualPOSId", "label.settings.globalPref.virtualPS", virtualPosRandomId, keyMapping);
        form.setVirtualPosRealId(virtualPosRealId);
        
        PointOfSale pointOfSale = this.pointOfSaleService.findPointOfSaleById(virtualPosRealId);
        
        if (WebCoreConstants.PAY_STATION_TYPE_TEST_VIRTUAL != pointOfSale.getPaystation().getPaystationType().getId()) {
            String label = this.getMessage("label.settings.globalPref.virtualPS");
            webSecurityForm.addErrorStatus(new FormErrorStatus("virtualPOSId", this.getMessage("error.common.invalid", new Object[] { label })));
            return;
        }
    }

    public void validateComments(WebSecurityForm<RESTAccountEditForm> webSecurityForm) {
        RESTAccountEditForm form = webSecurityForm.getWrappedObject();
        super.validateStringLength(webSecurityForm, "comments", "label.comment", form.getComments(), WebCoreConstants.VALIDATION_MIN_LENGTH_1,
                                   WebCoreConstants.VALIDATION_MAX_LENGTH_100);
        super.validateExtraStandardText(webSecurityForm, "comments", "label.comment", form.getComments());
    }

    
    /**
     * 1. Check if input 'restAccountTypeId' is valid.
     * 2. Check if selected rest account type is being used.
     * @param restAccounts List of RestAccount objects with same PointOfSaleId.
     * @apram restAccountTypeId Selected RestAccountType id.
     */
    public boolean isRestAccountValidAndAvailable(List<RestAccount> restAccounts, int restAccountTypeId) {
        Map<Integer, RestAccountType> map = restAccountTypeService.getRestAccountTypesMap();
        if (map.get(restAccountTypeId) == null) {
            return false;
        }
        
        for (RestAccount ra : restAccounts) {
            if (ra.getRestAccountType().getId() == restAccountTypeId) {
                return false;
            }
        }
        return true;
    }
}
