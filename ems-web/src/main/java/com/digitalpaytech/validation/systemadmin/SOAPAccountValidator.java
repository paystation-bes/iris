package com.digitalpaytech.validation.systemadmin;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.support.SOAPAccountEditForm;
import com.digitalpaytech.service.WebServiceEndPointTypeService;
import com.digitalpaytech.util.ErrorConstants;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.LabelConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.validation.BaseValidator;

@Component("soapAccountValidator")
public class SOAPAccountValidator extends BaseValidator<SOAPAccountEditForm> {
    
    @Autowired
    private WebServiceEndPointTypeService webServiceEndPointTypeService;
    
    public final void setWebServiceEndPointTypeService(final WebServiceEndPointTypeService webServiceEndPointTypeService) {
        this.webServiceEndPointTypeService = webServiceEndPointTypeService;
    }
    
    @Override
    public void validate(final WebSecurityForm<SOAPAccountEditForm> command, final Errors errors) {
        final WebSecurityForm<SOAPAccountEditForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
        
        if (!super.validateActionFlag(webSecurityForm, WebSecurityConstants.ACTION_UPDATE, WebSecurityConstants.ACTION_CREATE)) {
            return;
        }
        
        if (checkId(webSecurityForm) != null) {
            validateType(webSecurityForm, errors);
        }
        
        validateToken(webSecurityForm, errors);
        validateComments(webSecurityForm);
        
    }
    
    /**
     * Converts random id from form into a database id that is saved for later use
     * 
     * @param webSecurityForm
     *            WebSecurityForm object
     */
    private Integer checkId(final WebSecurityForm<SOAPAccountEditForm> webSecurityForm) {
        final SOAPAccountEditForm form = webSecurityForm.getWrappedObject();
        final RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        final Integer id = super.validateRandomId(webSecurityForm, FormFieldConstants.PARAMETER_CUSTOMER_ID, "label.systemAdmin.customer.id",
                                                  form.getCustomerId(), keyMapping);
        form.setCustomerRealId(id);
        return id;
    }
    
    public final void validateType(final WebSecurityForm<SOAPAccountEditForm> webSecurityForm, final Errors errors) {
        
        final SOAPAccountEditForm form = webSecurityForm.getWrappedObject();
        String typeName = null;
        final Integer typeValue = form.getType();
        if (typeValue != null) {
            typeName = this.webServiceEndPointTypeService.getText(typeValue.byteValue());
        }
        if (typeName == null) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("type", this.getMessage(ErrorConstants.REQUIRED,
                                                                                       new Object[] { this.getMessage("label.type") })));
            
        }
    }
    
    private void validateToken(final WebSecurityForm<SOAPAccountEditForm> webSecurityForm, final Errors errors) {
        final SOAPAccountEditForm form = webSecurityForm.getWrappedObject();
        if (StringUtils.isBlank(form.getToken())) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("token", this.getMessage(ErrorConstants.REQUIRED, new Object[] { this
                    .getMessage("label.settings.globalPref.token"), })));
        }
    }
    
    private void validateComments(final WebSecurityForm<SOAPAccountEditForm> webSecurityForm) {
        final SOAPAccountEditForm form = webSecurityForm.getWrappedObject();
        super.validateStringLength(webSecurityForm, FormFieldConstants.COMMENTS, LabelConstants.LABEL_COMMENT, form.getComments(),
                                   WebCoreConstants.VALIDATION_MIN_LENGTH_1, WebCoreConstants.VALIDATION_MAX_LENGTH_100);
        super.validateExtraStandardText(webSecurityForm, FormFieldConstants.COMMENTS, LabelConstants.LABEL_COMMENT, form.getComments());
    }
    
}
