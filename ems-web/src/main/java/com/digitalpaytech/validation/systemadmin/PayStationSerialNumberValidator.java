package com.digitalpaytech.validation.systemadmin;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.support.SysAdminPayStationEditForm;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.validation.BaseValidator;

@Component("payStationSerialNumberValidator")
public class PayStationSerialNumberValidator extends BaseValidator<SysAdminPayStationEditForm> {
    @Override
    public void validate(WebSecurityForm<SysAdminPayStationEditForm> webSecurityForm, Errors errors) {
        webSecurityForm.removeAllErrorStatus();
        
        // Check Post Token
        if (!webSecurityForm.isCorrectPostToken()) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("postToken", this.getMessage("error.common.invalid.posttoken")));
            return;
        }
        
        checkIds(webSecurityForm);
    }
    
    private void checkIds(WebSecurityForm<SysAdminPayStationEditForm> webSecurityForm) {
        
        SysAdminPayStationEditForm form = webSecurityForm.getWrappedObject();
        if (form.getSerialNumbers().size() != 1) {
            webSecurityForm.addErrorStatus(new FormErrorStatus("actionFlag", this.getMessage("error.common.invalid",
                                                                                             new Object[] { this.getMessage("label.serial.number") })));
        }
        
        String[] serialArray = form.getSerialNumbers().get(0).split("\n");
        for (String serialNumber : serialArray) {
            validateSerialNumberLength(webSecurityForm, serialNumber, WebSecurityConstants.MIN_SERIALNUMBER_LENGTH,
                                       WebSecurityConstants.MAX_SERIALNUMBER_LENGTH);
            super.validateAlphaNumeric(webSecurityForm, "serialNumbers", "label.serial.number", serialNumber);
        }
    }
    
    protected String validateSerialNumberLength(WebSecurityForm<SysAdminPayStationEditForm> webSecurityForm, String serialNumber, int minLength, int maxLength) {
        int actualMinLength, actualMaxLength;
        if ((maxLength > 0) && (minLength > maxLength)) {
            actualMinLength = maxLength;
            actualMaxLength = minLength;
        } else {
            actualMinLength = minLength;
            actualMaxLength = maxLength;
        }
        
        String result = null;
        if (serialNumber != null) {
            result = serialNumber.trim();
            if (result.length() <= 0) {
                result = null;
            }
        }
        
        if (result != null) {
            if (actualMinLength > 0) {
                if (actualMaxLength > 0) {
                    if ((result.length() < actualMinLength) || (result.length() > actualMaxLength)) {
                        addError(webSecurityForm, "serialNumbers",
                                 this.getMessage("error.common.invalid.lengths", serialNumber, actualMinLength, actualMaxLength));
                        result = null;
                    }
                } else {
                    if (result.length() != actualMinLength) {
                        addError(webSecurityForm, "serialNumbers", this.getMessage("error.common.invalid.length", serialNumber, actualMinLength));
                        result = null;
                    }
                }
            }
        }
        
        return result;
    }
    
}
