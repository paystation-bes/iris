package com.digitalpaytech.validation.systemadmin;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;


import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.support.MobileLicenseEditForm;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.support.WebObjectId;
import com.digitalpaytech.validation.BaseValidator;

@Component("mobileLicenseEditValidator")
public class MobileLicenseEditValidator extends BaseValidator<MobileLicenseEditForm>{

    @Override
    public void validate(WebSecurityForm<MobileLicenseEditForm> command, Errors errors) {
        WebSecurityForm<MobileLicenseEditForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
        RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        validateIds(webSecurityForm, keyMapping);
        validateLicenseCount(webSecurityForm);
    }
           
    private void validateIds(WebSecurityForm<MobileLicenseEditForm> webSecurityForm, RandomKeyMapping keyMapping){
        MobileLicenseEditForm form = webSecurityForm.getWrappedObject();
        
        WebObjectId customerWebObject =  super.validateRandomIdForWebObject(webSecurityForm, "customer", "label.allCaps.filter", form.getCustomerId(), keyMapping);
        if (customerWebObject != null) {
            if (customerWebObject.getId() != null) {
                Integer id = (Integer) customerWebObject.getId();
                form.setCustomerRealId(id.intValue());
            } else {
                form.setCustomerRealId(null);
            }
        }
        WebObjectId appWebObject = super.validateRandomIdForWebObject(webSecurityForm, "license", "label.allCaps.filter", form.getAppId(), keyMapping);
        if (appWebObject != null) {
            if (appWebObject.getId() != null) {
                Integer id = (Integer) appWebObject.getId();
                form.setAppRealId(id.intValue());
            } else {
                form.setAppRealId(null);
            }
        }
    }
    
    private void validateLicenseCount(WebSecurityForm<MobileLicenseEditForm> webSecurityForm){
    	MobileLicenseEditForm form = webSecurityForm.getWrappedObject();
    	super.validateIntegerLimits(webSecurityForm, "licenseCount", "label.systemAdmin.Licenses", 
    			form.getLicenseCount(), 0, Integer.MAX_VALUE);
 
    }    
}
