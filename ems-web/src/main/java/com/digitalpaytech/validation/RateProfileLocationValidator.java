package com.digitalpaytech.validation;

import java.util.Date;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.RateProfile;
import com.digitalpaytech.domain.RateProfileLocation;
import com.digitalpaytech.mvc.support.RateProfileLocationForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;

@Component("rateProfileLocationValidator")
public class RateProfileLocationValidator extends BaseValidator<RateProfileLocationForm> {
    @Override
    public final void validate(final WebSecurityForm<RateProfileLocationForm> command, final Errors errors) {
        final WebSecurityForm<RateProfileLocationForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
        
        checkIds(webSecurityForm);
        
        RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        super.validateRandomId(webSecurityForm, "location", "label.location", webSecurityForm.getWrappedObject().getLocationRandomId(), keyMapping,
                               Location.class, Integer.class, "0");
        
        checkDates(webSecurityForm);
        checkSpaceRange(webSecurityForm);
    }
    
    public final void validateInLocationPage(final WebSecurityForm<RateProfileLocationForm> command, final Errors errors) {
        final WebSecurityForm<RateProfileLocationForm> webSecurityForm = super.prepareSecurityForm(command, errors, "postToken");
        if (webSecurityForm == null) {
            return;
        }
        
        checkIds(webSecurityForm);
        
        RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        super.validateRandomId(webSecurityForm, "rateProfile", "label.settings.rates.rateProfile", webSecurityForm.getWrappedObject().getRateProfileRandomId(),
                               keyMapping, RateProfile.class, Integer.class);
        
        checkDates(webSecurityForm);
        checkSpaceRange(webSecurityForm);
    }
    
    protected void checkIds(final WebSecurityForm<RateProfileLocationForm> webSecurityForm) {
        RandomKeyMapping keyMapping = SessionTool.getInstance().getKeyMapping();
        
        if (!StringUtils.isBlank(webSecurityForm.getWrappedObject().getRandomId())) {
            Integer id = keyMapping.getKey(webSecurityForm.getWrappedObject().getRandomId(), RateProfileLocation.class, Integer.class);
            if (id == null) {
                throw new IllegalStateException("Invalid RateProfileLocation's random Id!");
            }
        }
    }
    
    protected void checkDates(final WebSecurityForm<RateProfileLocationForm> webSecurityForm) {
        final TimeZone timeZone = TimeZone.getTimeZone("GMT");
        final Date currentDate = DateUtil.removeTime(System.currentTimeMillis());
        Date startDate = null;
        Date endDate = null;
        
        final String startDateStr = webSecurityForm.getWrappedObject().getStartDate();
        if (super.validateRequired(webSecurityForm, "startDate", "label.start.date", startDateStr) != null) {
            startDate = super.validateDate(webSecurityForm, "startDate", "label.start.date", startDateStr, timeZone);
        }
        
        final String endDateStr = webSecurityForm.getWrappedObject().getEndDate();
        if (!StringUtils.isBlank(endDateStr)) {
            endDate = super.validateDate(webSecurityForm, "endDate", "label.end.date", endDateStr, timeZone);
        }
        
        if (endDate != null) {
            endDate = validateDateLimits(webSecurityForm, "endDate", "label.end.date", endDate, currentDate, (Date) null,
                                         getMessage("label.coupon.currentDate"), (String) null);
        }
        
        // Check range
        if ((startDate != null) && (endDate != null)) {
            if (validateDateLimits(webSecurityForm, "startDate", "label.start.date", startDate, (Date) null, endDate, (String) null,
                                   getMessage("label.end.date")) != null) {
                webSecurityForm.getWrappedObject().setStartDateObj(startDate);
                webSecurityForm.getWrappedObject().setEndDateObj(endDate);
            }
        } else if (startDate != null) {
            webSecurityForm.getWrappedObject().setStartDateObj(startDate);
        }
    }
    
    protected void checkSpaceRange(final WebSecurityForm<RateProfileLocationForm> webSecurityForm) {
        String spaceRange = webSecurityForm.getWrappedObject().getSpaceRange();
        if (!StringUtils.isBlank(spaceRange)) {
            if (validateStringLength(webSecurityForm, "spaceRange", "label.coupon.spaceRange", spaceRange, WebCoreConstants.VALIDATION_MIN_LENGTH_1,
                                     WebCoreConstants.VALIDATION_MAX_LENGTH_255) != null) {
                validateIntegerRangeString(webSecurityForm, "spaceRange", "label.coupon.spaceRange", spaceRange, WebCoreConstants.STALL_NUM_MIN,
                                           WebCoreConstants.STALL_NUM_MAX);
            }
        }
    }
}
