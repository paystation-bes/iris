package com.digitalpaytech.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.validation.DirectFieldBindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetChartType;
import com.digitalpaytech.domain.WidgetFilterType;
import com.digitalpaytech.domain.WidgetMetricType;
import com.digitalpaytech.domain.WidgetRangeType;
import com.digitalpaytech.domain.WidgetTierType;
import com.digitalpaytech.dto.WidgetMetrics;
import com.digitalpaytech.mvc.WidgetController;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.mvc.support.WidgetSettingsForm;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WidgetConstants;

@org.junit.Ignore
public class WidgetSettingsValidatorTest {
    
    private static Map<Integer, String> durationMap;
    private static Map<Integer, String> tierMap;
    private static Map<Integer, String> metricMap;
    private static Map<Integer, String> filterMap;
    private static Map<Integer, String> chartMap;
    
    private static Widget widget;
    
    private static MockHttpServletRequest request;
    private static MockHttpSession session;
    
    private static RandomKeyMapping keyMapping;
    
    @BeforeClass
    public static void setUp() {
        
        setUpDurationMap();
        setUpTierMap();
        setUpMetricMap();
        setUpFilterMap();
        setUpChartsMap();
        setUpWidget();
        
        request = new MockHttpServletRequest();
        session = new MockHttpSession();
        request.setSession(session);
        
        keyMapping = SessionTool.getInstance(request).getKeyMapping();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }
    
    @Test
    public void validateFields() {
        WebSecurityForm<WidgetSettingsForm> secForm = createWebSecurityForm(true);
        WidgetSettingsForm theForm = new WidgetSettingsForm();
        
        secForm.setWrappedObject(theForm);
        Errors errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        WidgetSettingsValidator validator = new WidgetSettingsValidator() {
            @Override
            public String getMessage(String code) {
                return "TEST";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "TEST 2";
            }
            
            //            @Override
            //            public String getMessage(String code, Object[] args) {
            //                return "TEST 2";
            //            }			
        };
        
        WidgetRangeType range = new WidgetRangeType();
        WidgetFilterType filter = new WidgetFilterType();
        WidgetTierType tier = new WidgetTierType();
        WidgetChartType chart = new WidgetChartType();
        
        validator.setChartMap(chartMap);
        validator.setDurationMap(durationMap);
        validator.setMetricMap(metricMap);
        validator.setFilterMap(filterMap);
        validator.setTierMap(tierMap);
        validator.setWidgetMetrics(loadWidgetMetrics());
        validator.validate(secForm, errors, widget, false);
        
        //JOptionPane.showMessageDialog(null, "secForm.getValidationErrorInfo().getErrorStatus().size() = " + secForm.getValidationErrorInfo().getErrorStatus().size());
        // Empty widget name, duration.
        assertEquals(2, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        // Empty metric name, duration.
        theForm.setWidgetName("testWidget");
        secForm.setWrappedObject(theForm);
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        validator.validate(secForm, errors, widget, false);
        assertEquals(2, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        widget.getWidgetMetricType().setName("Revenue");
        widget.getWidgetTierTypeByWidgetTier1Type().setName("Day");
        chart.setId(WidgetConstants.CHART_TYPE_LIST);
        theForm.setChartType(keyMapping.getRandomString(chart, "id")); // List
        
        // Empty trendValue.		
        theForm.setMetricName("Revenue");
        range.setId(WidgetConstants.RANGE_TYPE_LAST_YEAR);
        theForm.setRange(keyMapping.getRandomString(range, "id")); // This Year		
        theForm.setTier1("Day");
        theForm.setTier2("Route");
        secForm.setWrappedObject(theForm);
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        validator.validate(secForm, errors, widget, false);
        assertEquals(1, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        // Pass, Total Revenue, Today, All, Hour (tier 1), Route (tier 2)
        theForm.setTargetValue("400");
        range.setId(WidgetConstants.RANGE_TYPE_LAST_30DAYS);
        theForm.setRange(keyMapping.getRandomString(range, "id")); // Last 30 Days
        filter.setId(WidgetConstants.FILTER_TYPE_ALL);
        theForm.setFilter(keyMapping.getRandomString(filter, "id"));// All 		
        tier.setId(WidgetConstants.TIER_TYPE_DAY);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // Day  	
        tier.setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
        theForm.setTier2(keyMapping.getRandomString(tier, "id")); // Route 
        secForm.setWrappedObject(theForm);
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        validator.validate(secForm, errors, widget, false);
        assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
    }
    
    @Test
    public void validateRevenueAndPurchasesSettings() {
        WidgetSettingsValidator validator = new WidgetSettingsValidator() {
            @Override
            public String getMessage(String code) {
                return "TEST";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "TEST 2";
            }
        };
        WidgetRangeType range = new WidgetRangeType();
        WidgetFilterType filter = new WidgetFilterType();
        WidgetTierType tier = new WidgetTierType();
        WidgetChartType chart = new WidgetChartType();
        
        validator.setChartMap(chartMap);
        validator.setDurationMap(durationMap);
        validator.setMetricMap(metricMap);
        validator.setFilterMap(filterMap);
        validator.setTierMap(tierMap);
        validator.setWidgetMetrics(loadWidgetMetrics());
        
        widget.getWidgetMetricType().setName("Revenue");
        widget.getWidgetTierTypeByWidgetTier1Type().setName(WebCoreConstants.NOT_APPLICABLE);
        // Total Revenue, Today, 
        WidgetSettingsForm theForm = new WidgetSettingsForm();
        theForm.setWidgetName("testWidget2");
        theForm.setMetricName("Revenue");
        chart.setId(WidgetConstants.CHART_TYPE_SINGLE_VALUE);
        theForm.setChartType(keyMapping.getRandomString(chart, "id")); // Single Value	
        filter.setId(WidgetConstants.FILTER_TYPE_ALL);
        theForm.setFilter(keyMapping.getRandomString(filter, "id")); // All 		
        range.setId(WidgetConstants.RANGE_TYPE_TODAY);
        theForm.setRange(keyMapping.getRandomString(range, "id")); // Today
        tier.setId(WidgetConstants.TIER_TYPE_NA);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // N/A
        theForm.setTargetValue("500");
        
        WebSecurityForm<WidgetSettingsForm> secForm = createWebSecurityForm(true);
        Errors errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
        //--------------------------------------------------------------------------------
        // Total Revenue, Today, Hour (tier 1), Route (tier 2)	
        widget.getWidgetTierTypeByWidgetTier1Type().setName("Hour");
        
        chart.setId(WidgetConstants.CHART_TYPE_LIST);
        theForm.setChartType(keyMapping.getRandomString(chart, "id")); // List		
        tier.setId(WidgetConstants.TIER_TYPE_HOUR);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // Hour  	
        tier.setId(WidgetConstants.TIER_TYPE_ROUTE);
        theForm.setTier2(keyMapping.getRandomString(tier, "id")); // Route 
        
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        //--------------------------------------------------------------------------------
        // Total Purchases, Last 7 days, Organization (tier 1), RevenueType (tier 2)
        widget.getWidgetMetricType().setName("Purchases");
        widget.getWidgetTierTypeByWidgetTier1Type().setName("Organization");
        theForm.setMetricName("Purchases");
        
        tier.setId(WidgetConstants.TIER_TYPE_ORG);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // Organization  	
        tier.setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
        theForm.setTier2(keyMapping.getRandomString(tier, "id")); // Revenue Type 
        range.setId(WidgetConstants.RANGE_TYPE_LAST_7DAYS);
        theForm.setRange(keyMapping.getRandomString(range, "id")); // Last 7 days
        
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        // Total Purchases, Last 7 days, Organization (tier 1), RevenueType (tier 2), Location (tier 3) 
        tier.setId(WidgetConstants.TIER_TYPE_LOCATION);
        theForm.setTier3(keyMapping.getRandomString(tier, "id")); // Location 
        validator.validate(secForm, errors, widget, false);
        assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        // Total Purchases, Last 7 days, Organization (tier 1), RevenueType (tier 2), 'none' (tier 3) 
        tier.setId(WidgetConstants.TIER_TYPE_NA);
        theForm.setTier3(keyMapping.getRandomString(tier, "id")); // N/A 
        validator.validate(secForm, errors, widget, false);
        assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        // Total Purchases, Last 7 days, Organization (tier 1), RevenueType (tier 2), Rate (invalid tier 3) 
        tier.setId(WidgetConstants.TIER_TYPE_RATE);
        theForm.setTier3(keyMapping.getRandomString(tier, "id")); // Rate
        validator.validate(secForm, errors, widget, false);
        assertEquals(1, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        // Total Purchases, Last 7 days, Organization (tier 1), RevenueType (tier 2), Day (invalid tier 3) 
        tier.setId(WidgetConstants.TIER_TYPE_DAY);
        theForm.setTier3(keyMapping.getRandomString(tier, "id")); // Day
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        validator.validate(secForm, errors, widget, false);
        assertEquals(1, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        //--------------------------------------------------------------------------------
        // Total Purchases, Last year, route (tier 1)
        widget.getWidgetMetricType().setName("Purchases");
        widget.getWidgetTierTypeByWidgetTier1Type().setName("Route");
        theForm.setMetricName("Purchases");
        
        tier.setId(WidgetConstants.TIER_TYPE_ROUTE);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // Route  	
        tier.setId(WidgetConstants.TIER_TYPE_NA);
        theForm.setTier2(keyMapping.getRandomString(tier, "id")); // N/A 
        tier.setId(WidgetConstants.TIER_TYPE_NA);
        theForm.setTier3(keyMapping.getRandomString(tier, "id")); // N/A
        range.setId(WidgetConstants.RANGE_TYPE_LAST_7DAYS);
        theForm.setRange(keyMapping.getRandomString(range, "id")); // Last 7 days
        
        secForm.setWrappedObject(theForm);
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        validator.validate(secForm, errors, widget, false);
        assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        // Total Purchases, Last year, route (tier 1), RevenueType (tier 2)
        tier.setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
        theForm.setTier2(keyMapping.getRandomString(tier, "id")); // Revenue Type
        tier.setId(WidgetConstants.TIER_TYPE_NA);
        theForm.setTier3(keyMapping.getRandomString(tier, "id")); // N/A
        secForm.setWrappedObject(theForm);
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        validator.validate(secForm, errors, widget, false);
        assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        // Total Purchases, Last year, route (tier 1), RevenueType (tier 2),  route (invalid tier 3)
        tier.setId(WidgetConstants.TIER_TYPE_ROUTE);
        theForm.setTier3(keyMapping.getRandomString(tier, "id")); // route
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        assertEquals(1, secForm.getValidationErrorInfo().getErrorStatus().size());
        
    }
    
    @Test
    public void validateSpaceTurnoverSettings() {
        WidgetSettingsValidator validator = new WidgetSettingsValidator() {
            @Override
            public String getMessage(String code) {
                return "TEST";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "TEST 2";
            }
        };
        WidgetRangeType range = new WidgetRangeType();
        WidgetFilterType filter = new WidgetFilterType();
        WidgetTierType tier = new WidgetTierType();
        WidgetChartType chart = new WidgetChartType();
        
        validator.setChartMap(chartMap);
        validator.setDurationMap(durationMap);
        validator.setMetricMap(metricMap);
        validator.setFilterMap(filterMap);
        validator.setTierMap(tierMap);
        validator.setWidgetMetrics(loadWidgetMetrics());
        
        widget.getWidgetMetricType().setName("Turnover");
        widget.getWidgetTierTypeByWidgetTier1Type().setName(WebCoreConstants.NOT_APPLICABLE);
        
        // Average Space Turnover, Last 30 Days, 
        WidgetSettingsForm theForm = new WidgetSettingsForm();
        theForm.setWidgetName("testWidget2");
        theForm.setTargetValue("5000");
        theForm.setMetricName("Turnover");
        
        chart.setId(WidgetConstants.CHART_TYPE_LIST);
        theForm.setChartType(keyMapping.getRandomString(chart, "id")); // List
        filter.setId(WidgetConstants.FILTER_TYPE_ALL);
        theForm.setFilter(keyMapping.getRandomString(filter, "id")); // All 		
        range.setId(WidgetConstants.RANGE_TYPE_LAST_30DAYS);
        theForm.setRange(keyMapping.getRandomString(range, "id")); // Last 30 Days
        tier.setId(WidgetConstants.TIER_TYPE_NA);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // N/A
        
        WebSecurityForm<WidgetSettingsForm> secForm = createWebSecurityForm(true);
        Errors errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        assertTrue(secForm.getValidationErrorInfo().getErrorStatus().size() > 0);
        
        // Average Space Turnover, Last 30 Days, Day (tier 1)
        widget.getWidgetTierTypeByWidgetTier1Type().setName("Day");
        
        tier.setId(WidgetConstants.TIER_TYPE_DAY);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // Day  	
        tier.setId(WidgetConstants.TIER_TYPE_NA);
        theForm.setTier2(keyMapping.getRandomString(tier, "id")); // N/A 
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        validator.validate(secForm, errors, widget, false);
        assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        // Average Space Turnover, Last 30 Days, Day (tier 1), Location (tier 2)
        tier.setId(WidgetConstants.TIER_TYPE_LOCATION);
        theForm.setTier2(keyMapping.getRandomString(tier, "id")); // Location 
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        validator.validate(secForm, errors, widget, false);
        assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        // Average Space Turnover, Last 30 Days, Location (tier 1)
        widget.getWidgetTierTypeByWidgetTier1Type().setName("Location");
        
        tier.setId(WidgetConstants.TIER_TYPE_LOCATION);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // Location  	
        tier.setId(WidgetConstants.TIER_TYPE_NA);
        theForm.setTier2(keyMapping.getRandomString(tier, "id")); // N/A 		
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        validator.validate(secForm, errors, widget, false);
        assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        // Average Space Turnover, Last 30 Days, RevenueType (invalid tier 1)
        widget.getWidgetTierTypeByWidgetTier1Type().setName("Revenue Type");
        
        tier.setId(WidgetConstants.TIER_TYPE_REVENUE_TYPE);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // Revenue Type  	
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        validator.validate(secForm, errors, widget, false);
        assertTrue(secForm.getValidationErrorInfo().getErrorStatus().size() > 0);
        
        // Average Space Turnover, Last 30 Days, Rate (invalid tier 1)
        widget.getWidgetTierTypeByWidgetTier1Type().setName("Rate");
        
        tier.setId(WidgetConstants.TIER_TYPE_RATE);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // Rate  
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        validator.validate(secForm, errors, widget, false);
        assertTrue(secForm.getValidationErrorInfo().getErrorStatus().size() > 0);
    }
    
    @Test
    public void validateUtilizationSettings() {
        WidgetSettingsValidator validator = new WidgetSettingsValidator() {
            @Override
            public String getMessage(String code) {
                return "TEST";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "TEST 2";
            }
        };
        WidgetRangeType range = new WidgetRangeType();
        WidgetFilterType filter = new WidgetFilterType();
        WidgetTierType tier = new WidgetTierType();
        WidgetChartType chart = new WidgetChartType();
        
        validator.setChartMap(chartMap);
        validator.setDurationMap(durationMap);
        validator.setMetricMap(metricMap);
        validator.setFilterMap(filterMap);
        validator.setTierMap(tierMap);
        validator.setWidgetMetrics(loadWidgetMetrics());
        
        widget.getWidgetMetricType().setName("Utilization");
        widget.getWidgetTierTypeByWidgetTier1Type().setName(WebCoreConstants.NOT_APPLICABLE);
        
        // Average Space Turnover, Last 24 Hours, 
        WidgetSettingsForm theForm = new WidgetSettingsForm();
        theForm.setWidgetName("testWidget7");
        theForm.setTargetValue("6000");
        theForm.setMetricName("Utilization");
        
        chart.setId(WidgetConstants.CHART_TYPE_LIST);
        theForm.setChartType(keyMapping.getRandomString(chart, "id")); // List
        filter.setId(WidgetConstants.FILTER_TYPE_ALL);
        theForm.setFilter(keyMapping.getRandomString(filter, "id")); // All 		
        range.setId(WidgetConstants.RANGE_TYPE_LAST_24HOURS);
        theForm.setRange(keyMapping.getRandomString(range, "id")); // Last 24 Hours
        tier.setId(WidgetConstants.TIER_TYPE_NA);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // N/A
        
        WebSecurityForm<WidgetSettingsForm> secForm = createWebSecurityForm(true);
        Errors errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        assertTrue(secForm.getValidationErrorInfo().getErrorStatus().size() > 0);
        
        // Utilization, Last 24 Hours, Hour (tier 1), no tier 2, invalid
        widget.getWidgetTierTypeByWidgetTier1Type().setName("Hour");
        
        tier.setId(WidgetConstants.TIER_TYPE_HOUR);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // Hour
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        assertTrue(secForm.getValidationErrorInfo().getErrorStatus().size() > 0);
        
        // Utilization, Last 24 Hours, empty tier 1, invalid		
        widget.getWidgetTierTypeByWidgetTier1Type().setName(WebCoreConstants.NOT_APPLICABLE);
        
        tier.setId(WidgetConstants.TIER_TYPE_NA);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // N/A
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        assertTrue(secForm.getValidationErrorInfo().getErrorStatus().size() > 0);
        
        // Utilization, Last 24 Hours, Hour (tier 1), Location (tier 2)
        widget.getWidgetTierTypeByWidgetTier1Type().setName("Hour");
        
        tier.setId(WidgetConstants.TIER_TYPE_HOUR);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // Hour
        tier.setId(WidgetConstants.TIER_TYPE_LOCATION);
        theForm.setTier2(keyMapping.getRandomString(tier, "id")); // Location
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
    }
    
    @Test
    public void validateOccupancySettings() {
        WidgetSettingsValidator validator = new WidgetSettingsValidator() {
            @Override
            public String getMessage(String code) {
                return "TEST";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "TEST 2";
            }
        };
        WidgetRangeType range = new WidgetRangeType();
        WidgetFilterType filter = new WidgetFilterType();
        WidgetTierType tier = new WidgetTierType();
        WidgetChartType chart = new WidgetChartType();
        
        validator.setChartMap(chartMap);
        validator.setDurationMap(durationMap);
        validator.setMetricMap(metricMap);
        validator.setFilterMap(filterMap);
        validator.setTierMap(tierMap);
        validator.setWidgetMetrics(loadWidgetMetrics());
        
        widget.getWidgetMetricType().setName("Paid Occupancy");
        widget.getWidgetTierTypeByWidgetTier1Type().setName(WebCoreConstants.NOT_APPLICABLE);
        
        // Occupancy, This Month, no tier, invalid 
        WidgetSettingsForm theForm = new WidgetSettingsForm();
        theForm.setWidgetName("testWidget9");
        theForm.setTargetValue("6000");
        theForm.setMetricName("Paid Occupancy");
        
        chart.setId(WidgetConstants.CHART_TYPE_LIST);
        theForm.setChartType(keyMapping.getRandomString(chart, "id")); // List
        filter.setId(WidgetConstants.FILTER_TYPE_ALL);
        theForm.setFilter(keyMapping.getRandomString(filter, "id")); // All 		
        range.setId(WidgetConstants.RANGE_TYPE_LAST_MONTH);
        theForm.setRange(keyMapping.getRandomString(range, "id")); // This Month
        tier.setId(WidgetConstants.TIER_TYPE_NA);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // N/A
        
        WebSecurityForm<WidgetSettingsForm> secForm = createWebSecurityForm(true);
        Errors errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        assertTrue(secForm.getValidationErrorInfo().getErrorStatus().size() > 0);
        
        // Occupancy, This Month, Day (tier 1), no tier 2, invalid
        widget.getWidgetTierTypeByWidgetTier1Type().setName("Day");
        
        tier.setId(WidgetConstants.TIER_TYPE_DAY);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // Day
        tier.setId(WidgetConstants.CHART_TYPE_NA);
        theForm.setTier2(keyMapping.getRandomString(tier, "id")); // N/A
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        assertTrue(secForm.getValidationErrorInfo().getErrorStatus().size() > 0);
        
        // Occupancy, This Month, Day (tier 1), Location (tier 2)
        tier.setId(WidgetConstants.TIER_TYPE_LOCATION);
        theForm.setTier2(keyMapping.getRandomString(tier, "id")); // Location
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
    }
    
    @Test
    public void validateActiveAlertsSettings() {
        WidgetSettingsValidator validator = new WidgetSettingsValidator() {
            @Override
            public String getMessage(String code) {
                return "TEST";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "TEST 2";
            }
        };
        WidgetRangeType range = new WidgetRangeType();
        WidgetFilterType filter = new WidgetFilterType();
        WidgetTierType tier = new WidgetTierType();
        WidgetChartType chart = new WidgetChartType();
        
        validator.setChartMap(chartMap);
        validator.setDurationMap(durationMap);
        validator.setMetricMap(metricMap);
        validator.setFilterMap(filterMap);
        validator.setTierMap(tierMap);
        validator.setWidgetMetrics(loadWidgetMetrics());
        
        widget.getWidgetMetricType().setName("Active Alerts");
        widget.getWidgetTierTypeByWidgetTier1Type().setName("N/A");
        
        // Pay Stations with Active Alerts, This Month, no tier, invalid 
        WidgetSettingsForm theForm = new WidgetSettingsForm();
        theForm.setWidgetName("testWidget9");
        theForm.setTargetValue("7000");
        theForm.setMetricName("Active Alerts");
        
        chart.setId(WidgetConstants.CHART_TYPE_LIST);
        theForm.setChartType(keyMapping.getRandomString(chart, "id")); // List
        filter.setId(WidgetConstants.FILTER_TYPE_ALL);
        theForm.setFilter(keyMapping.getRandomString(filter, "id")); // All 		
        range.setId(WidgetConstants.RANGE_TYPE_LAST_MONTH);
        theForm.setRange(keyMapping.getRandomString(range, "id")); // This Month
        tier.setId(WidgetConstants.TIER_TYPE_NA);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // N/A
        
        WebSecurityForm<WidgetSettingsForm> secForm = createWebSecurityForm(true);
        Errors errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        // Invalid range (duration)
        assertTrue(secForm.getValidationErrorInfo().getErrorStatus().size() > 0);
        
        // Pay Stations with Active Alerts, Now
        chart.setId(WidgetConstants.CHART_TYPE_SINGLE_VALUE);
        theForm.setChartType(keyMapping.getRandomString(chart, "id")); // Single Value
        range.setId(WidgetConstants.RANGE_TYPE_NOW);
        theForm.setRange(keyMapping.getRandomString(range, "id")); // Now
        tier.setId(WidgetConstants.TIER_TYPE_NA);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // N/A
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        // Pay Stations with Active Alerts, Now, Route (tier 1)
        widget.getWidgetTierTypeByWidgetTier1Type().setName("Route");
        
        chart.setId(WidgetConstants.CHART_TYPE_LIST);
        theForm.setChartType(keyMapping.getRandomString(chart, "id")); // List
        tier.setId(WidgetConstants.TIER_TYPE_ROUTE);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); //Route
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        // Pay Stations with Active Alerts, Now, Route (tier 1), Location (tier 2), invalid
        tier.setId(WidgetConstants.TIER_TYPE_LOCATION);
        theForm.setTier2(keyMapping.getRandomString(tier, "id")); // Location
        theForm.setTier1("Route");
        theForm.setTier2("Location");
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        assertTrue(secForm.getValidationErrorInfo().getErrorStatus().size() > 0);
    }
    
    @Test
    public void validateCollectionsSettings() {
        WidgetSettingsValidator validator = new WidgetSettingsValidator() {
            @Override
            public String getMessage(String code) {
                return "TEST";
            }
        };
        WidgetRangeType range = new WidgetRangeType();
        WidgetFilterType filter = new WidgetFilterType();
        WidgetTierType tier = new WidgetTierType();
        WidgetChartType chart = new WidgetChartType();
        
        validator.setChartMap(chartMap);
        validator.setDurationMap(durationMap);
        validator.setMetricMap(metricMap);
        validator.setFilterMap(filterMap);
        validator.setTierMap(tierMap);
        validator.setWidgetMetrics(loadWidgetMetrics());
        
        widget.getWidgetMetricType().setName("Collections");
        widget.getWidgetTierTypeByWidgetTier1Type().setName("N/A");
        
        // Total Collections, Last Year
        WidgetSettingsForm theForm = new WidgetSettingsForm();
        theForm.setWidgetName("testWidget9");
        theForm.setTargetValue("3000");
        theForm.setMetricName("Collections");
        
        chart.setId(WidgetConstants.CHART_TYPE_SINGLE_VALUE);
        theForm.setChartType(keyMapping.getRandomString(chart, "id")); // Single Value	
        filter.setId(WidgetConstants.FILTER_TYPE_ALL);
        theForm.setFilter(keyMapping.getRandomString(filter, "id")); // All 		
        range.setId(WidgetConstants.RANGE_TYPE_LAST_YEAR);
        theForm.setRange(keyMapping.getRandomString(range, "id")); // Last Year
        tier.setId(WidgetConstants.TIER_TYPE_NA);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // N/A
        
        WebSecurityForm<WidgetSettingsForm> secForm = createWebSecurityForm(true);
        Errors errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        // Total Collections, Last Year, CollectionType (tier 1)
        widget.getWidgetTierTypeByWidgetTier1Type().setName("Collection Type");
        
        chart.setId(WidgetConstants.CHART_TYPE_LIST);
        theForm.setChartType(keyMapping.getRandomString(chart, "id")); // List
        tier.setId(WidgetConstants.TIER_TYPE_COLLECTION_TYPE);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // Collection Type
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
    }
    
    @Test
    public void validateDollarsSettledSettings() {
        WidgetSettingsValidator validator = new WidgetSettingsValidator() {
            @Override
            public String getMessage(String code) {
                return "TEST";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "TEST 2";
            }
        };
        WidgetRangeType range = new WidgetRangeType();
        WidgetFilterType filter = new WidgetFilterType();
        WidgetTierType tier = new WidgetTierType();
        WidgetChartType chart = new WidgetChartType();
        
        validator.setChartMap(chartMap);
        validator.setDurationMap(durationMap);
        validator.setMetricMap(metricMap);
        validator.setFilterMap(filterMap);
        validator.setTierMap(tierMap);
        validator.setWidgetMetrics(loadWidgetMetrics());
        
        widget.getWidgetMetricType().setName("Settled Card");
        widget.getWidgetTierTypeByWidgetTier1Type().setName("N/A");
        
        // Total $ Settled, Today, invalid
        WidgetSettingsForm theForm = new WidgetSettingsForm();
        theForm.setWidgetName("testWidget10");
        theForm.setTargetValue("5000");
        theForm.setMetricName("Settled Card");
        
        chart.setId(WidgetConstants.CHART_TYPE_LIST);
        theForm.setChartType(keyMapping.getRandomString(chart, "id")); // List
        filter.setId(WidgetConstants.FILTER_TYPE_ALL);
        theForm.setFilter(keyMapping.getRandomString(filter, "id")); // All 		
        range.setId(WidgetConstants.RANGE_TYPE_TODAY);
        theForm.setRange(keyMapping.getRandomString(range, "id")); // Today
        tier.setId(WidgetConstants.TIER_TYPE_NA);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // N/A
        
        WebSecurityForm<WidgetSettingsForm> secForm = createWebSecurityForm(true);
        Errors errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        // Invalid range (duration)
        assertTrue(secForm.getValidationErrorInfo().getErrorStatus().size() > 0);
        
        // Total $ Settled, yesterday
        chart.setId(WidgetConstants.CHART_TYPE_SINGLE_VALUE);
        theForm.setChartType(keyMapping.getRandomString(chart, "id")); // Single Value
        range.setId(WidgetConstants.RANGE_TYPE_YESTERDAY);
        theForm.setRange(keyMapping.getRandomString(range, "id")); // Yesterday
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        // Total $ Settled, yesterday, Merchant Account (tier 1)		
        widget.getWidgetTierTypeByWidgetTier1Type().setName("Merchant Account");
        
        chart.setId(WidgetConstants.CHART_TYPE_LIST);
        theForm.setChartType(keyMapping.getRandomString(chart, "id")); // List
        tier.setId(WidgetConstants.TIER_TYPE_MERCHANT_ACCOUNT);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // Merchant Account
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        // Total $ Settled, yesterday, Hour (tier 1), invalid		
        widget.getWidgetTierTypeByWidgetTier1Type().setName("Hour");
        
        tier.setId(WidgetConstants.TIER_TYPE_HOUR);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // Hour
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        assertTrue(secForm.getValidationErrorInfo().getErrorStatus().size() > 0);
        
        // Total $ Settled, yesterday, Hour (tier 1), invalid		
        widget.getWidgetTierTypeByWidgetTier1Type().setName("Day");
        
        tier.setId(WidgetConstants.TIER_TYPE_DAY);
        theForm.setTier1(keyMapping.getRandomString(tier, "id")); // Day
        tier.setId(WidgetConstants.TIER_TYPE_LOCATION);
        theForm.setTier2(keyMapping.getRandomString(tier, "id")); // Location
        errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        secForm.setWrappedObject(theForm);
        validator.validate(secForm, errors, widget, false);
        assertTrue(secForm.getValidationErrorInfo().getErrorStatus().size() > 0);
    }
    
    private WidgetMetrics loadWidgetMetrics() {
        return WidgetMetricsHelper.loadWidgetMetricsXml("/widgetmetrics.xml", WidgetSettingsValidatorTest.class.getResourceAsStream("/widgetmetrics.xml"));
    }
    
    private WebSecurityForm<WidgetSettingsForm> createWebSecurityForm(boolean validTokenFlag) {
    	WebSecurityForm<WidgetSettingsForm> secForm = new WebSecurityForm<WidgetSettingsForm>();
        if (validTokenFlag) {
            secForm.setPostToken("TEST123");
            secForm.setInitToken("TEST123");
        } else {
            secForm.setPostToken("TEST123");
            secForm.setInitToken("TEST1234");
        }
        return secForm;
    }
    
    @Test
    public void supports() {
        WidgetSettingsValidator validator = new WidgetSettingsValidator();
        assertTrue(validator.supports(WidgetSettingsValidator.class));
        assertFalse(validator.supports(WidgetSettingsValidatorTest.class));
        assertFalse(validator.supports(WidgetController.class));
    }
    
    @Test
    public void validateToken() {
    	WebSecurityForm<WidgetSettingsForm> secForm = createWebSecurityForm(false);
        secForm.setPostToken("TEST123");
        secForm.setInitToken("TEST123");
        WidgetSettingsForm theForm = new WidgetSettingsForm();
        secForm.setWrappedObject(theForm);
        
        Errors errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        
        WidgetSettingsValidator validator = new WidgetSettingsValidator() {
            @Override
            public String getMessage(String code) {
                return "TEST";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "TEST 2";
            }
            
        };
        validator.setDurationMap(durationMap);
        validator.setWidgetMetrics(loadWidgetMetrics());
        
        // Invalid token
        validator.validate(secForm, errors, widget, false);
        assertEquals(1, secForm.getValidationErrorInfo().getErrorStatus().size());
    }
    
    @Test
    public void isIntegerOrFloat() {
        WidgetSettingsValidator validator = new WidgetSettingsValidator();
        assertTrue(validator.isIntegerOrFloat("5.55"));
        assertTrue(validator.isIntegerOrFloat("0.50"));
        assertTrue(validator.isIntegerOrFloat("0.0"));
        assertFalse(validator.isIntegerOrFloat("x.55"));
        assertFalse(validator.isIntegerOrFloat("5.x"));
        assertFalse(validator.isIntegerOrFloat("5.5x"));
    }
    
    @Test
    public void isBooleanString() {
        WidgetSettingsValidator val = new WidgetSettingsValidator();
        assertTrue(val.isBooleanString("true"));
        assertTrue(val.isBooleanString("TRUE"));
        assertTrue(val.isBooleanString("FALSE"));
        assertTrue(val.isBooleanString("false"));
        assertFalse(val.isBooleanString(null));
        assertFalse(val.isBooleanString(""));
        assertFalse(val.isBooleanString("truee"));
        assertFalse(val.isBooleanString("fase"));
    }
    
    private static void setUpChartsMap() {
        chartMap = new HashMap<Integer, String>(8);
        chartMap.put(WidgetConstants.CHART_TYPE_LIST, "List");
        chartMap.put(WidgetConstants.CHART_TYPE_BAR, "Bar");
        chartMap.put(WidgetConstants.CHART_TYPE_COLUMN, "Column");
        chartMap.put(WidgetConstants.CHART_TYPE_LINE, "Line");
        chartMap.put(WidgetConstants.CHART_TYPE_AREA, "Area");
        chartMap.put(WidgetConstants.CHART_TYPE_SINGLE_VALUE, "Single Value");
        chartMap.put(WidgetConstants.CHART_TYPE_PIE, "Pie");
        chartMap.put(WidgetConstants.CHART_TYPE_MAP, "Map");
    }
    
    private static void setUpFilterMap() {
        filterMap = new HashMap<Integer, String>(4);
        filterMap.put(WidgetConstants.FILTER_TYPE_ALL, "All");
        filterMap.put(WidgetConstants.FILTER_TYPE_TOP, "Top");
        filterMap.put(WidgetConstants.FILTER_TYPE_BOTTOM, "Bottom");
        filterMap.put(WidgetConstants.FILTER_TYPE_SUBSET, "Subset");
    }
    
    private static void setUpMetricMap() {
        metricMap = new HashMap<Integer, String>(11);
        metricMap.put(WidgetConstants.METRIC_TYPE_REVENUE, "Revenue");
        metricMap.put(WidgetConstants.METRIC_TYPE_COLLECTIONS, "Collections");
        metricMap.put(WidgetConstants.METRIC_TYPE_SETTLED_CARD, "Settled Card");
        metricMap.put(WidgetConstants.METRIC_TYPE_PURCHASES, "Purchases");
        metricMap.put(WidgetConstants.METRIC_TYPE_PAID_OCCUPANCY, "Paid Occupancy");
        metricMap.put(WidgetConstants.METRIC_TYPE_TURNOVER, "Turnover");
        metricMap.put(WidgetConstants.METRIC_TYPE_UTILIZATION, "Utilization");
        metricMap.put(WidgetConstants.METRIC_TYPE_DURATION, "Duration");
        metricMap.put(WidgetConstants.METRIC_TYPE_ACTIVE_ALERTS, "Active Alerts");
        metricMap.put(WidgetConstants.METRIC_TYPE_ALERT_STATUS, "Alert Status");
        metricMap.put(WidgetConstants.METRIC_TYPE_MAP, "Map");
        metricMap.put(WidgetConstants.METRIC_TYPE_AVERAGE_PRICE, "Average Price per Permit");
        metricMap.put(WidgetConstants.METRIC_TYPE_AVERAGE_REVENUE, "Average Revenue per Space");
        metricMap.put(WidgetConstants.METRIC_TYPE_AVERAGE_DURATION, "Average Duration per Permit");

    }
    
    private static void setUpTierMap() {
        tierMap = new HashMap<Integer, String>(16);
        tierMap.put(WidgetConstants.TIER_TYPE_NA, "N/A");
        tierMap.put(WidgetConstants.TIER_TYPE_HOUR, "Hour");
        tierMap.put(WidgetConstants.TIER_TYPE_DAY, "Day");
        tierMap.put(WidgetConstants.TIER_TYPE_MONTH, "Month");
        tierMap.put(WidgetConstants.TIER_TYPE_ALL_ORG, "All Organizations");
        tierMap.put(WidgetConstants.TIER_TYPE_ORG, "Organization");
        tierMap.put(WidgetConstants.TIER_TYPE_PARENT_LOCATION, "Parent Location");
        tierMap.put(WidgetConstants.TIER_TYPE_LOCATION, "Location");
        tierMap.put(WidgetConstants.TIER_TYPE_ROUTE, "Route");
        tierMap.put(WidgetConstants.TIER_TYPE_PAYSTATION, "Pay Station");
        tierMap.put(WidgetConstants.TIER_TYPE_RATE, "Rate");
        tierMap.put(WidgetConstants.TIER_TYPE_MERCHANT_ACCOUNT, "Merchant Account");
        tierMap.put(WidgetConstants.TIER_TYPE_REVENUE_TYPE, "Revenue Type");
        tierMap.put(WidgetConstants.TIER_TYPE_TRANSACTION_TYPE, "Transaction Type");
        tierMap.put(WidgetConstants.TIER_TYPE_COLLECTION_TYPE, "Collection Type");
        tierMap.put(WidgetConstants.TIER_TYPE_ALERT_TYPE, "Alert Type");
        tierMap.put(WidgetConstants.TIER_TYPE_CARD_PROCESS_METHOD_TYPE, "Card Processing Method Type");
        tierMap.put(WidgetConstants.TIER_TYPE_CITATION_TYPE, "Citation Type");
    }
    
    private static void setUpDurationMap() {
        durationMap = new HashMap<Integer, String>(14);
        durationMap.put(WidgetConstants.RANGE_TYPE_NOW, "now");
        durationMap.put(WidgetConstants.RANGE_TYPE_TODAY, "Today");
        durationMap.put(WidgetConstants.RANGE_TYPE_YESTERDAY, "Yesterday");
        durationMap.put(WidgetConstants.RANGE_TYPE_LAST_24HOURS, "Last 24 Hours");
//        durationMap.put(WidgetConstants.RANGE_TYPE_THIS_WEEK, "This Week");
//        durationMap.put(WidgetConstants.RANGE_TYPE_THIS_MONTH, "This Month");
//        durationMap.put(WidgetConstants.RANGE_TYPE_THIS_YEAR, "This Year");
        durationMap.put(WidgetConstants.RANGE_TYPE_LAST_WEEK, "Last Week");
        durationMap.put(WidgetConstants.RANGE_TYPE_LAST_7DAYS, "Last 7 days");
        durationMap.put(WidgetConstants.RANGE_TYPE_LAST_MONTH, "Last_Month");
        durationMap.put(WidgetConstants.RANGE_TYPE_LAST_30DAYS, "Last 30 Days");
        durationMap.put(WidgetConstants.RANGE_TYPE_LAST_12MONTHS, "Last 12 Months");
        durationMap.put(WidgetConstants.RANGE_TYPE_LAST_YEAR, "Last Year");
        durationMap.put(WidgetConstants.RANGE_TYPE_YEAR_TO_DATE, "Year to Date");
    }
    
    private static void setUpWidget() {
        widget = new Widget();
        
        WidgetTierType type1 = new WidgetTierType();
        WidgetTierType type2 = new WidgetTierType();
        WidgetTierType type3 = new WidgetTierType();
        WidgetRangeType rangeType = new WidgetRangeType();
        WidgetMetricType widgetMetricType = new WidgetMetricType();
        WidgetFilterType widgetFilterType = new WidgetFilterType();
        WidgetChartType widgetChartType = new WidgetChartType();
        widgetMetricType.setName("Revenue");
        type1.setId(0);
        type2.setId(0);
        type3.setId(0);
        
        widget.setWidgetTierTypeByWidgetTier1Type(type1);
        widget.setWidgetTierTypeByWidgetTier2Type(type2);
        widget.setWidgetTierTypeByWidgetTier3Type(type3);
        widget.setWidgetRangeType(rangeType);
        widget.setWidgetMetricType(widgetMetricType);
        widget.setWidgetFilterType(widgetFilterType);
        widget.setWidgetChartType(widgetChartType);
        widget.setName("test widget");
        widget.setId(12345);
    }
}
