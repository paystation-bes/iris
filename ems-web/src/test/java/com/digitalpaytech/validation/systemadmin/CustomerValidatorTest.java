package com.digitalpaytech.validation.systemadmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Locale;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.support.CustomerEditForm;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.testing.services.impl.CustomerServiceTestImpl;

public class CustomerValidatorTest {
    
    private WebSecurityForm<CustomerEditForm> webSecurityForm;
    private CustomerEditForm customerEditForm;
    private CustomerService customerService;
    
    @Before
    public void setUp() throws Exception {
        customerEditForm = new CustomerEditForm();
        customerEditForm.setAccountStatus("enabled");
        customerEditForm.setPassword("Password$1");
        customerEditForm.setPasswordConfirmed("Password$1");
        customerEditForm.setLegalName("Testtesttest");
        customerEditForm.setUserName("Testtest2");
        webSecurityForm = new WebSecurityForm<CustomerEditForm>(null, customerEditForm);
        webSecurityForm.setActionFlag(0);
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpSession session = new MockHttpSession();
        request.setSession(session);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        customerService = new CustomerServiceTestImpl() {
            @Override
            public Customer findCustomer(Integer customerId) {
                return new Customer() {
                    @Override
                    public boolean isIsParent() {
                        return false;
                    }
                };
            }
        };
    }
    
    @After
    public void tearDown() throws Exception {
        webSecurityForm = null;
        customerEditForm = null;
    }
    
    @Test
    public void test_invalid_token() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("2222222222222");
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "customerEditForm");
        
        CustomerValidator validator = new CustomerValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Locale locale) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object[] args, Locale locale) {
                return "error.common.invalid.posttoken";
            }
        };
        validator.validateWithMigrated(webSecurityForm, result);
        //JOptionPane.showMessageDialog(null, "0. test_invalid_token: " + webSecurityForm.getValidationErrorInfo().getErrorStatus().size());
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getErrorFieldName(), "postToken");
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getMessage(), "error.common.invalid.posttoken");
    }
    
    @Test
    public void test_invalid_actionFlag() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(2);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "customerEditForm");
        
        CustomerValidator validator = new CustomerValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Locale locale) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object[] args, Locale locale) {
                return "error.common.invalid";
            }
        };
        validator.validateWithMigrated(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getErrorFieldName(), "actionFlag");
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getMessage(), "error.common.invalid");
    }
    
    @Test
    public void test_invalid_input() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        customerEditForm.setAccountStatus("xxenabled");
        customerEditForm.setPassword("Password");
        customerEditForm.setPasswordConfirmed("Password");
        customerEditForm.setLegalName("Te");
        customerEditForm.setUserName("Tes");
        
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "customerEditForm");
        
        CustomerValidator validator = new CustomerValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Locale locale) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object[] args, Locale locale) {
                return "error.common.invalid";
            }
        };
        validator.setCustomerService(customerService);
        validator.validateWithMigrated(webSecurityForm, result);
        //JOptionPane.showMessageDialog(null, "2. test_invalid_input: " + webSecurityForm.getValidationErrorInfo().getErrorStatus().size());
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 4);
    }
    
    @Test
    public void test_invalid_length() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "customerEditForm");
        customerEditForm.setLegalName("tes");
        customerEditForm.setUserName("test");
        
        CustomerValidator validator = new CustomerValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Locale locale) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object[] args, Locale locale) {
                return "error.common.invalid";
            }
        };
        validator.setCustomerService(customerService);
        validator.validateWithMigrated(webSecurityForm, result);
        //JOptionPane.showMessageDialog(null, "4. test_invalid_length: " + webSecurityForm.getValidationErrorInfo().getErrorStatus().size());
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 2);
    }
    
    @Test
    public void test_special_chars() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "customerEditForm");
        
        customerEditForm.setLegalName("test");
        customerEditForm.setUserName("testtest©");
        customerEditForm.setPassword("tst©");
        customerEditForm.setPasswordConfirmed("ts©");
        
        CustomerValidator validator = new CustomerValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Locale locale) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object[] args, Locale locale) {
                return "error.common.invalid";
            }
        };
        validator.setCustomerService(customerService);
        validator.validateWithMigrated(webSecurityForm, result);
        
        //      String strErrors = "Errors = " + webSecurityForm.getValidationErrorInfo().getErrorStatus().size() + "\n";
        //      for (int i = 0; i < webSecurityForm.getValidationErrorInfo().getErrorStatus().size(); i++) {
        //          strErrors += ((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(i))).getErrorFieldName() + " --- ";
        //          strErrors += ((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(i))).getMessage() + "; \n";
        //      }
        //            
        //      JOptionPane.showMessageDialog(null, strErrors);     		
        
        //JOptionPane.showMessageDialog(null, "3. test_special_chars: " + webSecurityForm.getValidationErrorInfo().getErrorStatus().size());
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 5);
    }
    
}
