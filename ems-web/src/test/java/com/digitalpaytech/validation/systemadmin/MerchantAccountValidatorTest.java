package com.digitalpaytech.validation.systemadmin;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.support.MerchantAccountEditForm;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

public class MerchantAccountValidatorTest {
    @Before
    public void setUp() throws Exception {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpSession session = new MockHttpSession();
        request.setSession(session);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }
    
	@Test
	public void validateActionFlagCustomerRandomId() {
		MerchantAccountEditForm form = new MerchantAccountEditForm();
		WebSecurityForm<MerchantAccountEditForm> webSecurityForm = createValidWebSecurityForm(form);
		webSecurityForm.setActionFlag(2);
		// Invalid, no actionFlag
		Processor processor = new Processor();
		processor.setId(7);
		
		BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "merchantAccountEditForm");
		MerchantAccountValidator validator = new MerchantAccountValidator() {
		    @Override
            public String getMessage(String code) {
                return "error.common.invalid";
            }
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid";
            }
            @Override
            public String getMessage(String code, Locale locale) {
                return "error.common.invalid";
            }
            @Override
            public String getMessage(String code, Object[] args, Locale locale) {
                return "error.common.invalid";
            }
		};
		//JOptionPane.showMessageDialog(null, "Tester Flag val = " + webSecurityForm.getActionFlag());
		validator.validate(webSecurityForm, result, processor);
		
		assertEquals(1, webSecurityForm.getValidationErrorInfo().getErrorStatus().size());			
		
		// Invalid, no customer random id.
		webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
		result = new BeanPropertyBindingResult(webSecurityForm, "merchantAccountEditForm");
		((MerchantAccountEditForm) webSecurityForm.getWrappedObject()).setQuarterOfTheDay(0);
		validator.validate(webSecurityForm, result, processor);
		//JOptionPane.showMessageDialog(null, "0. random id: " + form.getCustomerId());
		assertEquals(1, webSecurityForm.getValidationErrorInfo().getErrorStatus().size());			
		
		((MerchantAccountEditForm) webSecurityForm.getWrappedObject()).setCustomerId("");
		result = new BeanPropertyBindingResult(webSecurityForm, "merchantAccountEditForm");
		validator.validate(webSecurityForm, result, processor);
		assertEquals(1, webSecurityForm.getValidationErrorInfo().getErrorStatus().size());	
	}
	
	
	@Test
	public void validateName() {
		MerchantAccountEditForm form = new MerchantAccountEditForm();
		form.setCustomerId("randomId");
		form.setName("dddssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
		WebSecurityForm<MerchantAccountEditForm> secForm = createValidWebSecurityForm(form);
		secForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
		
		// Invalid, no accountName
		BindingResult result = new BeanPropertyBindingResult(secForm, "merchantAccountEditForm");
		MerchantAccountValidator validator = new MerchantAccountValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid";
            }
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid";
            }
            @Override
            public String getMessage(String code, Locale locale) {
                return "error.common.invalid";
            }
            @Override
            public String getMessage(String code, Object[] args, Locale locale) {
                return "error.common.invalid";
            }
        };
		validator.validateName(secForm, result);
		      
		assertEquals(1, secForm.getValidationErrorInfo().getErrorStatus().size());			
		
		form.setName("");
		result = new BeanPropertyBindingResult(secForm, "merchantAccountEditForm");
		validator.validateName(secForm, result);
		assertEquals(2, secForm.getValidationErrorInfo().getErrorStatus().size());			

		// Invalid, actionName is < 5 or > 30
		form.setName("test");
		result = new BeanPropertyBindingResult(secForm, "merchantAccountEditForm");
		validator.validateName(secForm, result);
		assertEquals(3, secForm.getValidationErrorInfo().getErrorStatus().size());			

		form.setName("test5test5test5test5test5test5t");
		result = new BeanPropertyBindingResult(secForm, "merchantAccountEditForm");
		validator.validateName(secForm, result);
		assertEquals(4, secForm.getValidationErrorInfo().getErrorStatus().size());			
		
		// Valid, actionName has length of 30
		form.setName("test5test5test5test5test5test5");
		result = new BeanPropertyBindingResult(secForm, "merchantAccountEditForm");
		validator.validateName(secForm, result);
		assertEquals(4, secForm.getValidationErrorInfo().getErrorStatus().size());			
	}

	@Test
	public void validateFields() {
		MerchantAccountEditForm form = new MerchantAccountEditForm();
		form.setCustomerId("randomId");
		
//		form.setField1("testval");
//		form.setField2("testval");
//		form.setField3("testval");
//		form.setField4("testval");
//		form.setField5("testval");
		
		WebSecurityForm<MerchantAccountEditForm> secForm = createValidWebSecurityForm(form);
		secForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
		
		// Invalid, no totalNumOfFields
		BindingResult result = new BeanPropertyBindingResult(secForm, "merchantAccountEditForm");
		MerchantAccountValidator validator = new MerchantAccountValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid";
            }
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid";
            }
            @Override
            public String getMessage(String code, Locale locale) {
                return "error.common.invalid";
            }
            @Override
            public String getMessage(String code, Object[] args, Locale locale) {
                return "error.common.invalid";
            }
        };
        Processor processor = new Processor();
        processor.setId(7);
		validator.validateFields(secForm, result, processor);

//  String strErrors = "Number of Errors = " + secForm.getValidationErrorInfo().getErrorStatus().size() + "\n";
//  for (int i = 0; i < secForm.getValidationErrorInfo().getErrorStatus().size(); i++) {
//      strErrors += ((FormErrorStatus) (secForm.getValidationErrorInfo().getErrorStatus().get(i))).getErrorFieldName() + " --- ";
//      strErrors += ((FormErrorStatus) (secForm.getValidationErrorInfo().getErrorStatus().get(i))).getMessage() + "; \n";
//  }            
//  JOptionPane.showMessageDialog(null, strErrors);
	      
		assertEquals(5, secForm.getValidationErrorInfo().getErrorStatus().size());
		
		// No fields - Should have 7 fields
		result = new BeanPropertyBindingResult(secForm, "merchantAccountEditForm");
		form.setField1("1");
		validator.validateFields(secForm, result, processor);
		assertEquals(9, secForm.getValidationErrorInfo().getErrorStatus().size());

		// Invalid, field 2 is too long
		form.setField1(WebCoreConstants.NOT_APPLICABLE);
		form.setField2("12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901");
		form.setField3(WebCoreConstants.NOT_APPLICABLE);
		form.setField4(WebCoreConstants.NOT_APPLICABLE);
		form.setField5(WebCoreConstants.NOT_APPLICABLE);
		form.setField6(WebCoreConstants.NOT_APPLICABLE);
		result = new BeanPropertyBindingResult(secForm, "merchantAccountEditForm");
		validator.validateFields(secForm, result, processor);
		assertEquals(10, secForm.getValidationErrorInfo().getErrorStatus().size());
		
	}	

	
	@Test
	public void validateAccountWithProcessorAndField1And2() {
		MerchantAccountService serv = new MerchantAccountServiceImpl() {
			@Override
			public List<MerchantAccount> findByProcessorAndFields(String[] paramNames, Object[] values) {
				List<MerchantAccount> list = new ArrayList<MerchantAccount>();
				if (values[0] instanceof Integer && (((Integer) values[0]).intValue() == 2 || ((Integer) values[0]).intValue() == 6)) {
					MerchantAccount ma = new MerchantAccount();
					list.add(ma);
				}
				return list;
			}
		};
		Processor p = new Processor();
		p.setId(2);
		p.setName("First Data Nashville");
		MerchantAccount ma = new MerchantAccount();
		ma.setProcessor(p);
		ma.setField1("11169736");
		ma.setField2("FN08001308");
		ma.setField3("33203");

		MerchantAccountEditForm form = new MerchantAccountEditForm();
		form.setCustomerId("randomId");
		WebSecurityForm<MerchantAccountEditForm> secForm = createValidWebSecurityForm(form);
		secForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
		BindingResult result = new BeanPropertyBindingResult(secForm, "merchantAccountEditForm");

		// Invalid - account already in used
		MerchantAccountValidator va = new MerchantAccountValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid";
            }
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid";
            }
            @Override
            public String getMessage(String code, Locale locale) {
                return "error.common.invalid";
            }
            @Override
            public String getMessage(String code, Object[] args, Locale locale) {
                return "error.common.invalid";
            }
        };
		va.setMerchantAccountService(serv);
		va.validateAccountWithProcessorAndField1And2(ma, result);
		assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
		
		//---------------------------------------------------------------------------------------------
		
		p.setId(6);
		p.setName("Alliance");
		ma = new MerchantAccount();
		ma.setProcessor(p);
		ma.setField1("700000002018");
		ma.setField2("001");
		ma.setField3("00010372867883130794");
		result = new BeanPropertyBindingResult(secForm, "merchantAccountEditForm");

		// Invalid - account already in used
		va.validateAccountWithProcessorAndField1And2(ma, result);
		assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());

		//---------------------------------------------------------------------------------------------

		p.setId(1);
		p.setName("Paradata");
		ma = new MerchantAccount();
		ma.setProcessor(p);
		ma.setField1("111111002018");
		ma.setField2("623");
		ma.setField3("4545333535343794");
		result = new BeanPropertyBindingResult(secForm, "merchantAccountEditForm");

		va.validateAccountWithProcessorAndField1And2(ma, result);
		assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());

	}
	
	
	private WebSecurityForm<MerchantAccountEditForm> createValidWebSecurityForm(MerchantAccountEditForm form) {
		WebSecurityForm<MerchantAccountEditForm> secForm = new WebSecurityForm<MerchantAccountEditForm>();
		secForm.setInitToken("token");
		secForm.setPostToken("token");
		secForm.setWrappedObject(form);
		return secForm;
	}

}
