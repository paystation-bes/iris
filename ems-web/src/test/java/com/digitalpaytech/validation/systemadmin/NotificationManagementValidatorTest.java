package com.digitalpaytech.validation.systemadmin;

import static org.junit.Assert.assertTrue;

import java.util.Locale;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.support.NotificationManagementForm;

public class NotificationManagementValidatorTest {
    
    private WebSecurityForm<NotificationManagementForm> webSecurityForm;
    private NotificationManagementForm notificationManagementForm;
    
    @Before
    public void setUp() throws Exception {
        notificationManagementForm = new NotificationManagementForm();
        webSecurityForm = new WebSecurityForm<NotificationManagementForm>(null, notificationManagementForm);
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpSession session = new MockHttpSession();
        request.setSession(session);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }
    
    @After
    public void tearDown() throws Exception {
        webSecurityForm = null;
        notificationManagementForm = null;
    }
    
    @Test
    public void test_invalid_token() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("2222222222222");
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "notificationManagementForm");
        
        NotificationManagementValidator validator = new NotificationManagementValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Locale locale) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object[] args, Locale locale) {
                return "error.common.invalid";
            }
        };
        validator.validate(webSecurityForm, result);
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
    }
    
    @Test
    public void test_invalid_actionFlag() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(2);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "notificationManagementForm");
        
        NotificationManagementValidator validator = new NotificationManagementValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Locale locale) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object[] args, Locale locale) {
                return "error.common.invalid";
            }
        };
        validator.validate(webSecurityForm, result);
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
    }
    
    @Test
    public void test_input_required() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "notificationManagementForm");
        
        NotificationManagementValidator validator = new NotificationManagementValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Locale locale) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object[] args, Locale locale) {
                return "error.common.invalid";
            }
        };
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 6);
    }
    
    @Test
    public void test_invalid_input() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        NotificationManagementForm form = (NotificationManagementForm) webSecurityForm.getWrappedObject();
        form.setStartDate("14/30/2000");
        form.setStartTime("25:00");
        form.setEndDate("15/07/2000");
        form.setEndTime("25:30");
        
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "routeEditForm");
        
        NotificationManagementValidator validator = new NotificationManagementValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Locale locale) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object[] args, Locale locale) {
                return "error.common.invalid";
            }
        };
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 6);
    }
    
    @Test
    public void test_invalid_input_startTime_greater() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        NotificationManagementForm form = (NotificationManagementForm) webSecurityForm.getWrappedObject();
        form.setStartDate("12/08/2000");
        form.setStartTime("05:00");
        form.setEndDate("12/07/2000");
        form.setEndTime("23:30");
        
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "routeEditForm");
        
        NotificationManagementValidator validator = new NotificationManagementValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Locale locale) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object[] args, Locale locale) {
                return "error.common.invalid";
            }
        };
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 2);
    }
    
    @Test
    public void test_invalid_length() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        NotificationManagementForm form = (NotificationManagementForm) webSecurityForm.getWrappedObject();
        form.setStartDate("11/08/2000");
        form.setStartTime("05:00");
        form.setEndDate("12/07/2000");
        form.setEndTime("23:30");
        form.setTitle("12345678901234567890123456789012345678901234567890Q");
        form.setMessage("dksfj;dlskfjdsklfdskfjdsklfjdslkfjdslkfjsdklfjsdklfjdsklfdkgjfkbhjgkbngmfnklgmfsnkglnmhknmhklnmhklfnmkflnmkl;fhmn;fh"
                        + "lknmklfhmnklmkl;dgm;ertjierhterutherutheruhterjthkjerthrkejtherdsfsdfdsfsdfdsfsdfsdfdksfj;dlskfjdsklfdskfjdsklfjdslkfjdslkfjsdklf"
                        + "jsdklfjdsklfdkgjfkbhjgkbngmfnklgmfsnkglnmhknmhklnmhklfnmkflnmkl;fhmn;fhlknmklfhmnklmkl;dgm;ertjierhterutherutheruhterjthkjerthrkej"
                        + "therdsfsdfdsfsdfdsfsdfsdfdksfj;dlskfjdsklfdskfjdsklfjdslkfjdslkfjsdklfjsdklfjdsklfdkgjfkbhjgkbngmfnklgmfsnkglnmhknmhklnmhklfnmkflnmk"
                        + "l;fhmn;fhlknmklfhmnklmkl;dgm;ertjierhterutherutheruhterjthkjerthrkejtherdsfsdfdsfsdfdsfsdfsdf");
        form.setLink("http://dksfj;dlskfjdsklfdskfjdsklfjdslkfjdslkfjsdklfjsdklfjdsklfdkgjfkbhjgkbngmfnklgmfsnkglnmhknmhklnmhklfnmkflnmkl;fhmn;fh"
                     + "lknmklfhmnklmkl;dgm;ertjierhterutherutheruhterjthkjerthrkejtherdsfsdfdsfsdfdsfsdfsdfdksfj;dlskfjdsklfdskfjdsklfjdslkfjdslkfjsdklf"
                     + "jsdklfjdsklfdkgjfkbhjgkbngmfnklgmfsnkglnmhknmhklnmhklfnmkflnmkl;fhmn;fhlknmklfhmnklmkl;dgm;ertjierhterutherutheruhterjthkjerthrkej"
                     + "therdsfsdfdsfsdfdsfsdfsdfdksfj;dlskfjdsklfdskfjdsklfjdslkfjdslkfjsdklfjsdklfjdsklfdkgjfkbhjgkbngmfnklgmfsnkglnmhknmhklnmhklfnmkflnmk"
                     + "l;fhmn;fhlknmklfhmnklmkl;dgm;ertjierhterutherutheruhterjthkjerthrkejtherdsfsdfdsfsdfdsfsdfsdf");
        
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "routeEditForm");
        
        NotificationManagementValidator validator = new NotificationManagementValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Locale locale) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object[] args, Locale locale) {
                return "error.common.invalid";
            }
        };
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 3);
    }
    
    @Test
    public void test_special_characters() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        NotificationManagementForm form = (NotificationManagementForm) webSecurityForm.getWrappedObject();
        form.setStartDate("11/08/2000");
        form.setStartTime("05:00");
        form.setEndDate("12/07/2000");
        form.setEndTime("23:30");
        form.setTitle("TestTime@#$%");
        form.setMessage("TestMessage<>$");
        form.setLink("qa.digi");
        
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "routeEditForm");
        
        NotificationManagementValidator validator = new NotificationManagementValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Locale locale) {
                return "error.common.invalid";
            }
            
            @Override
            public String getMessage(String code, Object[] args, Locale locale) {
                return "error.common.invalid";
            }
        };
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 2);
    }
}
