package com.digitalpaytech.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class BaseValidatorTest {

	@Test
	public void isSame() {
		String s1 = "aBc1$";
		String s2 = "aBc1$";
		assertTrue(BaseValidator.isSame(s1, s2));
		assertFalse(BaseValidator.isSame(s1, s2.toLowerCase()));

		s1 = null;
		s2 = "";
		assertFalse(BaseValidator.isSame(s1, s2));
		
		s2 = null;
		assertFalse(BaseValidator.isSame(s1, s2));
		
		s1 = "";
		s2 = "";
		assertFalse(BaseValidator.isSame(s1, s2));
	}
	
	
	@Test
	public void isSameSessionToken() {
		// null check
		assertFalse(BaseValidator.isSameSessionToken(null, null));
		
//		MockHttpServletRequest mockReq = new MockHttpServletRequest();
//		// null check + not equal
//		assertFalse(BaseValidator.isSameSessionToken(mockReq, null));
//		
//		String sessTok = null;
//	    //TODO Remove SessionToken
////		mockReq.addParameter(WebSecurityConstants.SESSION_TOKEN, sessTok);
//		// MockHttpServletRequest sessionToken null check
//		assertFalse(BaseValidator.isSameSessionToken(mockReq, null));
//		
//		MockHttpSession mockSes = new MockHttpSession();
//		// No sessionToken in MockHttpServletRequest & MockHttpSession check
//		assertFalse(BaseValidator.isSameSessionToken(mockReq, mockSes));
//
//	    //TODO Remove SessionToken
////		mockReq.setParameter(WebSecurityConstants.SESSION_TOKEN, "ABC123");
//		// Compare sessionToken in MockHttpSession & MockHttpSession
////		assertFalse(BaseValidator.isSameSessionToken(mockReq, mockSes));
////		
////	    //TODO Remove SessionToken
////		mockSes.setAttribute(WebSecurityConstants.SESSION_TOKEN, sessTok);
//		// MockHttpSession sessionToken null check
//		assertFalse(BaseValidator.isSameSessionToken(null, mockSes));
//		
//		// No sessionToken in MockHttpSession check
//		assertFalse(BaseValidator.isSameSessionToken(mockReq, mockSes));
//		
//		// Compare sessionToken in MockHttpSession & MockHttpSession
//	    //TODO Remove SessionToken
////		mockSes.setAttribute(WebSecurityConstants.SESSION_TOKEN, "123ABC");
//		assertFalse(BaseValidator.isSameSessionToken(mockReq, mockSes));
//		
//	    //TODO Remove SessionToken
////		mockSes.setAttribute(WebSecurityConstants.SESSION_TOKEN, "abc123");
//		assertFalse(BaseValidator.isSameSessionToken(mockReq, mockSes));
//		
//		
//	    //TODO Remove SessionToken
////		mockSes.setAttribute(WebSecurityConstants.SESSION_TOKEN, "ABC123");
//		assertTrue(BaseValidator.isSameSessionToken(mockReq, mockSes));
	}
	
}
