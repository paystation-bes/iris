package com.digitalpaytech.validation;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.mvc.support.RoleEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.support.WebObject;

public class RoleSettingValidatorTest {
    
    private WebSecurityForm<RoleEditForm> webSecurityForm;
    private RoleEditForm roleEditForm;
    private RandomKeyMapping keyMapping;
    
    @Before
    public final void setUp() throws Exception {
        
        this.roleEditForm = new RoleEditForm();
        this.webSecurityForm = new WebSecurityForm<RoleEditForm>(null, this.roleEditForm);
        final MockHttpServletRequest request = new MockHttpServletRequest();
        final MockHttpSession session = new MockHttpSession();
        request.setSession(session);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        this.keyMapping = SessionTool.getInstance(request).getKeyMapping();
    }
    
    @Test
    public final void test_invalid_token() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("2222222222222");
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "roleEditForm");
        
        RoleSettingValidator validator = new RoleSettingValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid.posttoken";
            }
        };
        validator.validate(webSecurityForm, result);
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
    }
    
    @Test
    public void test_invalid_actionFlag() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(2);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "roleEditForm");
        
        RoleSettingValidator validator = new RoleSettingValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid.posttoken";
            }
        };
        validator.validate(webSecurityForm, result);
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
    }
    
    @Test
    public void test_invalid_input() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "roleEditForm");
        RoleSettingValidator validator = new RoleSettingValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid.posttoken";
            }
        };
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 2);
    }
    
    @Test
    public void test_invalid_length() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        roleEditForm.setRoleName("TestTestTestTestTestTestTestTest");
        roleEditForm.setTypeId(WebCoreConstants.CUSTOMER_TYPE_CHILD);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "roleEditForm");
        RoleSettingValidator validator = new RoleSettingValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid.posttoken";
            }
        };
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 2);
    }
    
    @Test
    public void test_invalid_character() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        roleEditForm.setRoleName("TestTestTestTestTestTestTe$t");
        roleEditForm.setTypeId(WebCoreConstants.CUSTOMER_TYPE_CHILD);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "roleEditForm");
        RoleSettingValidator validator = new RoleSettingValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid.posttoken";
            }
        };
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 2);
    }
    
    @Test
    public void test_invalid_enabled() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        roleEditForm.setRoleName("TestTestTestTestTestTestTest");
        roleEditForm.setStatusEnabled(true);
        roleEditForm.setTypeId(WebCoreConstants.CUSTOMER_TYPE_CHILD);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "roleEditForm");
        RoleSettingValidator validator = new RoleSettingValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid.posttoken";
            }
        };
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
    }
    
    @Test
    public void test_permissionIdList_null() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "roleEditForm");
        
        roleEditForm.setRoleName("TestTestTestTestTestTestTest");
        roleEditForm.setStatusEnabled(true);
        roleEditForm.setTypeId(WebCoreConstants.CUSTOMER_TYPE_CHILD);
        
        RoleSettingValidator validator = new RoleSettingValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid.posttoken";
            }
        };
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
    }
    
    @Test
    public void test_randomized_permissionId_null() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "roleEditForm");
        
        List<String> randomizedPermissionIds = new ArrayList<String>();
        randomizedPermissionIds.add("");
        
        roleEditForm.setRoleName("TestTestTestTestTestTestTest");
        roleEditForm.setStatusEnabled(true);
        roleEditForm.setRandomizedPermissionIds(randomizedPermissionIds);
        roleEditForm.setTypeId(WebCoreConstants.CUSTOMER_TYPE_CHILD);
        RoleSettingValidator validator = new RoleSettingValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid.posttoken";
            }
        };
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
    }
    
    @Test
    public void test_invalid_randomized_permissionId() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "roleEditForm");
        
        List<String> randomizedPermissionIds = new ArrayList<String>();
        randomizedPermissionIds.add("111122222333$$$$");
        
        roleEditForm.setRoleName("TestTestTestTestTestTestTest");
        roleEditForm.setStatusEnabled(true);
        roleEditForm.setRandomizedPermissionIds(randomizedPermissionIds);
        roleEditForm.setTypeId(WebCoreConstants.CUSTOMER_TYPE_CHILD);
        RoleSettingValidator validator = new RoleSettingValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid.posttoken";
            }
        };
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
    }
    
    @Test
    public void test_validate_ok() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "roleEditForm");
        RoleSettingValidator validator = new RoleSettingValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid.posttoken";
            }
        };
        
        Collection<Permission> permissionList = new ArrayList<Permission>();
        Permission p1 = new Permission();
        p1.setId(1);
        p1.setName("Alerts Management");
        Permission p2 = new Permission();
        p2.setId(2);
        p2.setName("View Collection Status");
        Permission p3 = new Permission();
        p3.setId(3);
        p2.setName("View Maintenance Centre");
        
        permissionList.add(p1);
        permissionList.add(p2);
        permissionList.add(p3);
        
        Collection<WebObject> randomized = keyMapping.hideProperties(permissionList, WebCoreConstants.ID_LOOK_UP_NAME);
        
        List<String> randomizedPermissionIds = new ArrayList<String>();
        for (WebObject obj : randomized) {
            randomizedPermissionIds.add(obj.getPrimaryKey().toString());
        }
        
        roleEditForm.setRoleName("TestTestTestTestTestTestTest");
        roleEditForm.setStatusEnabled(true);
        roleEditForm.setRandomizedPermissionIds(randomizedPermissionIds);
        roleEditForm.setTypeId(WebCoreConstants.CUSTOMER_TYPE_CHILD);
        
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 0);
    }
}
