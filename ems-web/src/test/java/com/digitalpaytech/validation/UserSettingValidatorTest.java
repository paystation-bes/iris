package com.digitalpaytech.validation;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerType;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserStatusType;
import com.digitalpaytech.mvc.support.UserAccountEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.RoleServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObject;

public class UserSettingValidatorTest {

	private WebSecurityForm<UserAccountEditForm> webSecurityForm;
	private UserAccountEditForm userAccountEditForm;
	private RandomKeyMapping keyMapping;
	private WebSecurityUtil securityUtil;	
	private UserAccount userAccount;
	
	@Before
	public void setUp() throws Exception {

		userAccount = new UserAccount();
		Customer customer = new Customer();
		customer.setId(3);
		CustomerType ct = new CustomerType();
		ct.setId(2);
		customer.setCustomerType(ct);
		userAccount.setId(1);
		userAccount.setCustomer(customer);					
		securityUtil = new WebSecurityUtil();
		createAuthentication(userAccount);	
		
		securityUtil.setEntityService(new EntityServiceImpl() {
			public Object merge(Object entity) {
				return userAccount;
			}
		});		
		securityUtil.setUserAccountService(new UserAccountServiceImpl() {
			public UserAccount findUserAccount(int userAccountId) {
				return userAccount;
			}
		});		
		userAccountEditForm = new UserAccountEditForm();
		userAccountEditForm.setUserName("testguy@hey.com");
		webSecurityForm = new WebSecurityForm<UserAccountEditForm>(null, userAccountEditForm);
		MockHttpServletRequest request = new MockHttpServletRequest();
		MockHttpSession session = new MockHttpSession();
		request.setSession(session);
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		keyMapping = SessionTool.getInstance(request).getKeyMapping();
	}
	
	private UserAccount createAuthentication(UserAccount userAccount) {
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("Admin"));
		WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
		user.setCustomerTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT);
		user.setIsComplexPassword(true);
		Authentication authentication = new UsernamePasswordAuthenticationToken(
				user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_SYSTEM_ACTIVITIES);
        user.setUserPermissions(permissionList);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		return userAccount;
	}	

	@Test
	public void test_invalid_token() {
		webSecurityForm.setInitToken("1111111111111");
		webSecurityForm.setPostToken("2222222222222");
		BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "userAccountEditForm");
		
		UserSettingValidator validator = new UserSettingValidator() {
		    @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid.posttoken";
            }
		};
		validator.setUserAccountService(getUserAccountService());
		validator.setRoleService(getRoleService());
		validator.validate(webSecurityForm, result);
		assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
	}
	
	@Test
	public void test_invalid_actionFlag() {
		webSecurityForm.setInitToken("1111111111111");
		webSecurityForm.setPostToken("1111111111111");
		webSecurityForm.setActionFlag(2);
		BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "userAccountEditForm");
		
		UserSettingValidator validator = new UserSettingValidator() {
		    @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid.posttoken";
            }
		};
		validator.setUserAccountService(getUserAccountService());
		validator.setRoleService(getRoleService());
		validator.validate(webSecurityForm, result);
		assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
	}
	
	@Test
	public void test_invalid_input() {
		webSecurityForm.setInitToken("1111111111111");
		webSecurityForm.setPostToken("1111111111111");
		webSecurityForm.setActionFlag(0);
		BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "userAccountEditForm");
		
		UserSettingValidator validator = new UserSettingValidator() {
		    @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid.posttoken";
            }
		};
		validator.setUserAccountService(getUserAccountService());
		validator.setRoleService(getRoleService());
		validator.validate(webSecurityForm, result);
		assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 6);
	}
	
	@Test
	public void test_invalid_length() {
		webSecurityForm.setInitToken("1111111111111");
		webSecurityForm.setPostToken("1111111111111");
		webSecurityForm.setActionFlag(0);
		BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "userAccountEditForm");
		
		userAccountEditForm.setFirstName("testtesttesttesttesttestte");
		userAccountEditForm.setLastName("testtesttesttesttesttestte");
		userAccountEditForm.setUserName("test");
		userAccountEditForm.setUserPassword("pass");
		userAccountEditForm.setPasswordConfirm("passw");
		
		UserSettingValidator validator = new UserSettingValidator() {
		    @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid.posttoken";
            }
		};
		validator.setUserAccountService(getUserAccountService());
		validator.setRoleService(getRoleService());
		validator.validate(webSecurityForm, result);
		assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 9);
	}
	
	@Test
	public void test_special_char() {
		webSecurityForm.setInitToken("1111111111111");
		webSecurityForm.setPostToken("1111111111111");
		webSecurityForm.setActionFlag(0);
		BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "userAccountEditForm");
		
		userAccountEditForm.setFirstName("Test©");
		userAccountEditForm.setLastName("User©");
		userAccountEditForm.setUserName("testuser©");
		userAccountEditForm.setStatus(true);
		userAccountEditForm.setUserPassword("Password$1");
		userAccountEditForm.setPasswordConfirm("Password$1");
		
		UserSettingValidator validator = new UserSettingValidator() {
		    @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid.posttoken";
            }
		};
		validator.setUserAccountService(getUserAccountService());
		validator.setRoleService(getRoleService());
		validator.validate(webSecurityForm, result);
		assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 5);
	}
	
	@Test
	public void test_validate_ok() {
		webSecurityForm.setInitToken("1111111111111");
		webSecurityForm.setPostToken("1111111111111");
		webSecurityForm.setActionFlag(0);
		BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "userAccountEditForm");
		Collection<Role> roleList = new ArrayList<Role>();
		
		Role r1 = new Role();
		r1.setId(1);
		r1.setName("Child Admin");
		Role r2 = new Role();
		r2.setId(2);
		r2.setName("Customized Role");
		
		roleList.add(r1);
		roleList.add(r2);
		
		Collection<WebObject> randomized = keyMapping.hideProperties(roleList,  WebCoreConstants.ID_LOOK_UP_NAME);
		
		List<String> randomizedroleIds = new ArrayList<String>();
		for (WebObject obj : randomized) {
			randomizedroleIds.add(obj.getPrimaryKey().toString());
		}
		
		userAccountEditForm.setFirstName("Test");
		userAccountEditForm.setLastName("User");
		userAccountEditForm.setUserName("testuser");
		userAccountEditForm.setStatus(true);
		userAccountEditForm.setUserPassword("Password$1");
		userAccountEditForm.setPasswordConfirm("Password$1");
		userAccountEditForm.setRandomizedRoleIds(randomizedroleIds);				
		
		UserSettingValidator validator = new UserSettingValidator(){
		    @Override
	        public String getMessage(String code) {
	            return "error.common.invalid.posttoken";
	        }
	        @Override
	        public String getMessage(String code, Object... args) {
	            return "error.common.invalid.posttoken";
	        }
		};
		validator.setUserAccountService(getUserAccountService());
		validator.setRoleService(getRoleService());
		validator.validate(webSecurityForm, result);
		assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
	}
		
	private UserAccountServiceImpl getUserAccountService() {
		
		return new UserAccountServiceImpl() {
		    @Override
			public Collection<UserAccount> findUserAccountsByCustomerId(int customerId) {
				UserAccount ua1 = new UserAccount();
				ua1.setId(3);
				ua1.setFirstName("TestFirstName1");
				ua1.setLastName("TestLastName1");
				ua1.setUserName("TestUserName1");
				UserStatusType ust1 = new UserStatusType();
				ust1.setId(2);
				ua1.setUserStatusType(ust1);
				
				UserAccount ua2 = new UserAccount();
				ua2.setId(7);
				ua2.setFirstName("TestFirstName2");
				ua2.setLastName("TestLastName2");
				ua2.setUserName("TestUserName1");
				UserStatusType ust2 = new UserStatusType();
				ust2.setId(2);
				ua2.setUserStatusType(ust2);
				
				Collection<UserAccount> lists = new ArrayList<UserAccount>();
				lists.add(ua1);
				lists.add(ua2);
				
				return lists;
			}
		    @Override
			public Collection<UserAccount> findUserAccountsByCustomerIdAndUserAccountTypeId(Integer customerId) {
                UserAccount ua1 = new UserAccount();
                ua1.setId(3);
                ua1.setFirstName("TestFirstName1");
                ua1.setLastName("TestLastName1");
                ua1.setUserName("TestUserName1");
                UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                ua1.setUserStatusType(ust1);
                
                UserAccount ua2 = new UserAccount();
                ua2.setId(7);
                ua2.setFirstName("TestFirstName2");
                ua2.setLastName("TestLastName2");
                ua2.setUserName("TestUserName1");
                UserStatusType ust2 = new UserStatusType();
                ust2.setId(2);
                ua2.setUserStatusType(ust2);
                
                Collection<UserAccount> lists = new ArrayList<UserAccount>();
                lists.add(ua1);
                lists.add(ua2);
                
                return lists;
            }
		    
			public UserAccount findUserAccount(String userName) {			
	            UserAccount ua1 = new UserAccount();
	            ua1.setId(3);
	            ua1.setFirstName("TestFirstName1");
	            ua1.setLastName("TestLastName1");
	            ua1.setUserName("TestUserName1");
	            UserStatusType ust1 = new UserStatusType();
	            ust1.setId(2);
	            ua1.setUserStatusType(ust1);
	            
	            return ua1;			
			}
			
			public UserAccount findUndeletedUserAccount(String userName) {			
	            UserAccount ua1 = new UserAccount();
	            ua1.setId(3);
	            ua1.setFirstName("TestFirstName1");
	            ua1.setLastName("TestLastName1");
	            ua1.setUserName("TestUserName1");
	            UserStatusType ust1 = new UserStatusType();
	            ust1.setId(2);
	            ua1.setUserStatusType(ust1);
	            
	            return ua1;			
			}			
		    
		};				
	}	
	
	private RoleServiceImpl getRoleService() {		
		return new RoleServiceImpl() {
		    @Override
		    public Role findRoleByRoleIdAndEvict(Integer roleId) {
				Role r1 = new Role();
				r1.setId(1);
				r1.setName("ParentRole1");
				CustomerType ct = new CustomerType();
				ct.setId(2);
				r1.setCustomerType(ct);
				return r1;
			}
		};				
	}
	
}
