package com.digitalpaytech.validation.customeradmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PaystationType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.RoutePOS;
import com.digitalpaytech.domain.RouteType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.customeradmin.support.RouteEditForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.testing.services.impl.CustomerServiceTestImpl;
import com.digitalpaytech.testing.services.impl.RouteServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.support.WebObject;

public class RouteSettingValidatorTest {
    
    private WebSecurityForm<RouteEditForm> webSecurityForm;
    private RouteEditForm routeEditForm;
    private RandomKeyMapping keyMapping;
    private RouteSettingValidator validator;
    private UserAccount userAccount;
    
    @Before
    public void setUp() throws Exception {
        routeEditForm = new RouteEditForm();
        webSecurityForm = new WebSecurityForm<RouteEditForm>(null, routeEditForm);
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpSession session = new MockHttpSession();
        request.setSession(session);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        keyMapping = SessionTool.getInstance(request).getKeyMapping();
        validator = new RouteSettingValidator() {
            @Override
            public String getMessage(String code) {
                return code;
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return code;
            }
            
            @Override
            public boolean sameNameExists(String newRouteName) {
                return false;
            }
        };
        validator.setCustomerService(new CustomerServiceTestImpl() {
            public Customer findCustomer(Integer id) {
                Customer customer = new Customer();
                customer.setIsMigrated(true);
                return customer;
            }
        });
        
        setRouteService();
        userAccount = new UserAccount();
        userAccount.setId(2);
        Customer customer = new Customer();
        customer.setId(2);
        userAccount.setCustomer(customer);
        createAuthentication(userAccount);
        
    }
    
    @After
    public void cleanUp() throws Exception {
        webSecurityForm = null;
        routeEditForm = null;
        keyMapping = null;
    }
    
    @Test
    public void test_invalid_token() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("2222222222222");
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "routeEditForm");
        
        try {
            //JOptionPane.showMessageDialog(null, "equals = "  );
            validator.validate(webSecurityForm, result);
        } catch (Exception e) {
        }
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getErrorFieldName(), "postToken");
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getMessage(),
                     "error.common.invalid.posttoken");
    }
    
    @Test
    public void test_invalid_actionFlag() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(2);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "routeEditForm");
        
        validator.validate(webSecurityForm, result);
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getErrorFieldName(), "actionFlag");
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getMessage(), "error.common.invalid");
    }
    
    @Test
    public void test_invalid_input() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "routeEditForm");
        
        validator.validate(webSecurityForm, result);
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 2);
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getErrorFieldName(), "routeName");
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getMessage(), "error.common.required");
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(1))).getErrorFieldName(), "routeType");
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(1))).getMessage(), "error.common.required");
    }
    
    @Test
    public void test_invalid_length() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "routeEditForm");
        routeEditForm.setRouteName("TestRoute11111111111111111111111111");
        
        validator.validate(webSecurityForm, result);
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 2);
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getErrorFieldName(), "routeName");
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getMessage(),
                     "error.common.invalid.lengths");
        //		assertEquals(((FormErrorStatus)(webSecurityForm.getValidationErrorInfo().getErrorStatus().get(1))).getErrorFieldName(), "routeType");
        //		assertEquals(((FormErrorStatus)(webSecurityForm.getValidationErrorInfo().getErrorStatus().get(1))).getMessage(), "error.common.invalid");
    }
    
    @Test
    public void test_special_chars() {
        routeEditForm.setRouteName("TestRoute$%^");
        List<String> pointOfSaleIds = new ArrayList<String>();
        pointOfSaleIds.add("A1");
        routeEditForm.setRandomizedPointOfSaleIds(pointOfSaleIds);
        webSecurityForm = new WebSecurityForm<RouteEditForm>(null, routeEditForm);
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "routeEditForm");
        
        validator.validate(webSecurityForm, result);
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 3);
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getErrorFieldName(), "routeName");
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getMessage(),
                     "error.common.invalid.special.character.name");
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(1))).getErrorFieldName(), "routeType");
        // don't think we need to set errors for this in this case
        //assertEquals(((FormErrorStatus)(webSecurityForm.getValidationErrorInfo().getErrorStatus().get(1))).getMessage(), "error.common.invalid");
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(2))).getErrorFieldName(), "PSList");
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(2))).getMessage(), "error.common.invalid");
    }
    
    @Test
    public void test_validate_ok() {
        
        routeEditForm.setRouteName("TestRoute");
        
        List<String> pointOfSaleIds = new ArrayList<String>();
        pointOfSaleIds.add("1111111111111");
        routeEditForm.setRandomizedPointOfSaleIds(pointOfSaleIds);
        
        List<PointOfSale> pos = new ArrayList<PointOfSale>();
        PointOfSale ps1 = new PointOfSale();
        ps1.setId(1);
        ps1.setName("POSTestName1");
        PointOfSale ps2 = new PointOfSale();
        ps2.setId(2);
        ps2.setName("POSTestName2");
        pos.add(ps1);
        pos.add(ps2);
        
        Collection<RouteType> routeTypes = getRouteType();
        List<WebObject> randomized = (List<WebObject>) keyMapping.hideProperties(routeTypes, "id");
        List<WebObject> randomizedPos = (List<WebObject>) keyMapping.hideProperties(pos, "id");
        List<String> randomizedPointOfSaleIds = new ArrayList<String>();
        for (WebObject wo : randomizedPos) {
            randomizedPointOfSaleIds.add(wo.getPrimaryKey().toString());
        }
        String randomRouteTypeId = randomized.get(1).getPrimaryKey().toString();
        routeEditForm.setRandomizedRouteTypeId(randomRouteTypeId);
        //JOptionPane.showMessageDialog(null, "1. random route type id = " + randomRouteTypeId);
        routeEditForm.setRandomizedPointOfSaleIds(randomizedPointOfSaleIds);
        webSecurityForm = new WebSecurityForm<RouteEditForm>(null, routeEditForm);
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "routeEditForm");
        
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 0);
    }
    
    private Collection<RouteType> getRouteType() {
        List<RouteType> types = new ArrayList<RouteType>();
        types.add(new RouteType(new Integer(1).byteValue(), "Collections", 5433, new Date(), 1));
        types.add(new RouteType(new Integer(2).byteValue(), "Maintenance", 6545, new Date(), 1));
        types.add(new RouteType(new Integer(3).byteValue(), "Other", 2323, new Date(), 1));
        return types;
    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        user.setIsMigrated(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_ROUTES);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_ROUTES);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private void setRouteService() {
        validator.setRouteService(new RouteServiceTestImpl() {
            public Route findRouteById(Integer id) {
                Route r2 = new Route();
                RouteType rt2 = new RouteType();
                Set<RoutePOS> routePOSes2 = new HashSet<RoutePOS>();
                RoutePOS rp3 = new RoutePOS();
                RoutePOS rp4 = new RoutePOS();
                PointOfSale pos3 = new PointOfSale();
                PointOfSale pos4 = new PointOfSale();
                Paystation p3 = new Paystation();
                Paystation p4 = new Paystation();
                PaystationType pt3 = new PaystationType();
                PaystationType pt4 = new PaystationType();
                
                p3.setId(3);
                p4.setId(4);
                pt3.setId(3);
                pt4.setId(4);
                p3.setPaystationType(pt3);
                p4.setPaystationType(pt4);
                pos3.setPaystation(p3);
                pos4.setPaystation(p4);
                
                pos3.setId(3);
                pos3.setName("TestPointOfSale3");
                pos3.setLatitude(new BigDecimal(45));
                pos3.setLongitude(new BigDecimal(279));
                pos4.setId(4);
                pos4.setName("TestPointOfSale4");
                pos4.setLatitude(new BigDecimal(74));
                pos4.setLongitude(new BigDecimal(612));
                rp3.setId(3L);
                rp4.setId(4L);
                rp3.setPointOfSale(pos3);
                rp4.setPointOfSale(pos4);
                routePOSes2.add(rp3);
                routePOSes2.add(rp4);
                r2.setId(2);
                r2.setName("TestRoute2");
                rt2.setId(new Byte("2"));
                rt2.setName("Collections");
                r2.setRouteType(rt2);
                r2.setRoutePOSes(routePOSes2);
                
                return r2;
            }
            
            public Collection<Route> findRoutesByCustomerId(int customerId) {
                List<Route> result = new ArrayList<Route>();
                Route r = new Route();
                r.setId(1);
                RouteType type = new RouteType();
                type.setId(2);
                type.setName("Maintenance");
                r.setRouteType(type);
                result.add(r);
                return result;
            }
            
            public List<RoutePOS> findRoutePOSesByPointOfSaleId(Integer pointOfSaleId) {
                
                List<RoutePOS> resultList = new ArrayList<RoutePOS>();
                
                RoutePOS rp1 = new RoutePOS();
                RoutePOS rp2 = new RoutePOS();
                PointOfSale pos1 = new PointOfSale();
                Route route1 = new Route();
                Route route2 = new Route();
                route1.setId(1);
                route2.setId(2);
                
                rp1.setPointOfSale(pos1);
                rp2.setPointOfSale(pos1);
                rp1.setRoute(route1);
                rp2.setRoute(route2);
                
                resultList.add(rp1);
                resultList.add(rp2);
                
                return resultList;
            }
            
        });
    }
    
}
