package com.digitalpaytech.validation.customeradmin;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.digitalpaytech.client.dto.fms.DeviceGroup;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.impl.EmsPropertiesServiceImpl;

import java.text.*;
import java.util.*;

public class DeviceGroupValidatorTest {
    
    private static final Logger LOG = Logger.getLogger(DeviceGroupValidatorTest.class);
    
    @Test
    public void validateScheduleFormat() {
        
        final DeviceGroup devGr = new DeviceGroup();
        // Bad case 1
        devGr.setScheduleDate("2017-03-25");
        devGr.setScheduleTime(null);
        
        final DeviceGroupValidator validator = new DeviceGroupValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid.posttoken";
            }
        };
        
        final WebSecurityForm<DeviceGroup> webSecurityForm = new WebSecurityForm();
        webSecurityForm.setWrappedObject(devGr);
        assertFalse(validator.validateScheduleFormat(webSecurityForm, webSecurityForm.getWrappedObject().getScheduleDate(),
                                                     webSecurityForm.getWrappedObject().getScheduleTime()));
        
        // Bad case 2
        devGr.setScheduleDate("11/04/2017");
        devGr.setScheduleTime("231");
        webSecurityForm.setWrappedObject(devGr);
        assertFalse(validator.validateScheduleFormat(webSecurityForm, webSecurityForm.getWrappedObject().getScheduleDate(),
                                                     webSecurityForm.getWrappedObject().getScheduleTime()));
        
        // Good case
        devGr.setScheduleDate("03/11/2017");
        devGr.setScheduleTime("15:00");
        webSecurityForm.setWrappedObject(devGr);
        assertTrue(validator.validateScheduleFormat(webSecurityForm, webSecurityForm.getWrappedObject().getScheduleDate(),
                                                    webSecurityForm.getWrappedObject().getScheduleTime()));
    }
    
    @Test
    public void validateScheduleInFuture() {
        final DeviceGroup devGr = new DeviceGroup();
        
        final GregorianCalendar cal = new GregorianCalendar();
        cal.add(Calendar.DATE, 1);
        
        // "MM/dd/yyyy";
        final SimpleDateFormat dateFormat = new SimpleDateFormat(WebCoreConstants.DATE_UI_FORMAT);
        // H:mm
        final SimpleDateFormat timeFormat = new SimpleDateFormat(WebCoreConstants.TIME_UI_FORMAT);
        
        devGr.setScheduleDate(dateFormat.format(cal.getTime()));
        devGr.setScheduleTime(timeFormat.format(cal.getTime()));
        
        final WebSecurityForm<DeviceGroup> webSecurityForm = new WebSecurityForm();
        webSecurityForm.setWrappedObject(devGr);
        final DeviceGroupValidator validator = new DeviceGroupValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid.posttoken";
            }
        };
        validator.setEmsPropertiesService(createEmsPropertiesService());
        assertTrue(validator.validateScheduleInFuture(TimeZone.getDefault().getDisplayName(), webSecurityForm,
                                                      webSecurityForm.getWrappedObject().getScheduleDate(),
                                                      webSecurityForm.getWrappedObject().getScheduleTime()));
        
        cal.add(Calendar.DATE, -2);
        devGr.setScheduleDate(dateFormat.format(cal.getTime()));
        devGr.setScheduleTime(timeFormat.format(cal.getTime()));
        assertFalse(validator.validateScheduleInFuture(TimeZone.getDefault().getDisplayName(), webSecurityForm,
                                                       webSecurityForm.getWrappedObject().getScheduleDate(),
                                                       webSecurityForm.getWrappedObject().getScheduleTime()));
    }
    
    @Test
    public void getScheduleDate() {
        final DeviceGroupValidator validator = new DeviceGroupValidator();
        try {
            validator.getScheduleDate(TimeZone.getDefault().getDisplayName(), "2017-03-25", "16:12");
            // Should not be here.
            fail("ParseException should be thrown!");
            
        } catch (Exception e) {
            LOG.error("this message is expected: " + e.getMessage());
        }
        try {
            final Date schDate = validator.getScheduleDate(TimeZone.getDefault().getDisplayName(), "03/11/2017", "15:00");
            LOG.debug("all good, " + schDate);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
    
    private EmsPropertiesService createEmsPropertiesService() {
        return new EmsPropertiesServiceImpl() {
            @Override
            public int getPropertyValueAsInt(final String propertyName, final int defaultValue, final boolean forceGet) {
                return 5;
            }
        };
    }
}
