package com.digitalpaytech.validation.customeradmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.ExtensibleRate;
import com.digitalpaytech.domain.ParkingPermission;
import com.digitalpaytech.mvc.customeradmin.support.EbpRatePermissionEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.impl.LocationServiceImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;

public class ExtendByPhoneValidatorTest {
    
    private static RandomKeyMapping keyMapping;

    @Test
    public void validate() {
        
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpSession session = new MockHttpSession();
        request.setSession(session);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        createAuthentication();
        keyMapping = SessionTool.getInstance(request).getKeyMapping();

        // validateCommonField
        EbpRatePermissionEditForm form = new EbpRatePermissionEditForm();
        form.setLocationId("0");
        form.setDaysOfWeeks(new ArrayList());
        // no day selected
        WebSecurityForm<EbpRatePermissionEditForm> secForm = createValidWebSecurityForm(form);
//        Authentication authentication = WebSecurityUtil.getAuthentication();
//        ((WebUser) authentication.getPrincipal()).setCustomerTimeZone("PST");
        
        LocationService serv = new LocationServiceImpl() {
            @Override
            public List<List<ExtensibleRate>> getValidRatesByLocationId(Integer locationId) {
                return new ArrayList();
            }
            
            @Override
            public List<List<ParkingPermission>> getValidParkingPermissionsByLocationId(Integer locationId) {
                return new ArrayList();
            }
            
            @Override
            public List<ExtensibleRate> findOverlappedExtensibleRate(ExtensibleRate extensibleRate, List<Integer> daysOfWeek) {
                return new ArrayList();
            }
            
            @Override
            public List<ParkingPermission> findOverlappedParkingPermission(ParkingPermission pp, List<Integer> daysOfWeek) {
                return new ArrayList();
            }
        };
        
        // No day of week, start/end time & option.
        BindingResult result = new BeanPropertyBindingResult(secForm, "ebpRatePermissionEditForm");
        ExtendByPhoneValidator validator = new ExtendByPhoneValidator() {
            @Override
            public String getMessage(String code) {
                return "";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
            
            @Override
            protected Date validateTime(WebSecurityForm<EbpRatePermissionEditForm> form, String fieldName, String fieldLabelKey, String value, TimeZone timeZone){
                return super.validateTime(form, fieldName, fieldLabelKey, value, TimeZone.getTimeZone("PST"));
            }
        };
        validator.setLocationService(serv);
        
        validator.validate(secForm, result);
        assertEquals(3, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        // no end date/time, option.
        form.setSunday("true");
        form.setThursday("true");
        form.setEndTime("16:00");
        secForm = createValidWebSecurityForm(form);
        result = new BeanPropertyBindingResult(secForm, "ebpRatePermissionEditForm");
        validator.validate(secForm, result);
        assertEquals(1, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        // no ExtnesibleRate or ParkingPermission
        form.setStartTime("05:30");
        form.setName("test1");
        secForm = createValidWebSecurityForm(form);
        result = new BeanPropertyBindingResult(secForm, "ebpRatePermissionEditForm");
        validator.validate(secForm, result);
        EbpRatePermissionEditForm testform = secForm.getWrappedObject();
        //JOptionPane.showMessageDialog(null,testform.getName());
        //JOptionPane.showMessageDialog(null, ((FormErrorStatus)(secForm.getValidationErrorInfo().getErrorStatus().get(1))).getErrorFieldName());
        
        assertEquals(1, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        // ExtensibleRate, no name, RateValue, RateServiceFee, MinExtensionMinutes
        form.setName("");
        form.setExtensibleRateFlag("true");
        secForm = createValidWebSecurityForm(form);
        result = new BeanPropertyBindingResult(secForm, "ebpRatePermissionEditForm");
        //JOptionPane.showMessageDialog(null, "TimeZone String = " + WebSecurityUtil.getCustomerTimeZone());
        validator.validate(secForm, result);
        //JOptionPane.showMessageDialog(null, secForm.getValidationErrorInfo().getErrorStatus().size());
        //JOptionPane.showMessageDialog(null,testform.getName());
        assertEquals(7, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        // ParkingPermission, no Name, maxDurationMinutes
        form.setExtensibleRateFlag(null);
        form.setParkingPermissionFlag("true");
        form.setPermissionStatus("unlimited");
        
        secForm = createValidWebSecurityForm(form);
        result = new BeanPropertyBindingResult(secForm, "ebpRatePermissionEditForm");
        validator.validate(secForm, result);
        //JOptionPane.showMessageDialog(null, secForm.getValidationErrorInfo().getErrorStatus().size());
        assertEquals(2, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        form.setName("Parking");
        form.setPermissionStatus("timelimited");
        secForm = createValidWebSecurityForm(form);
        result = new BeanPropertyBindingResult(secForm, "ebpRatePermissionEditForm");
        validator.validate(secForm, result);
        assertEquals(1, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        form.setMaxDurationMinutes("60");
        secForm = createValidWebSecurityForm(form);
        result = new BeanPropertyBindingResult(secForm, "ebpRatePermissionEditForm");
        validator.validate(secForm, result);
        assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
    }
    
    private WebSecurityForm<EbpRatePermissionEditForm> createValidWebSecurityForm(EbpRatePermissionEditForm form) {
    	WebSecurityForm<EbpRatePermissionEditForm> secForm = new WebSecurityForm<EbpRatePermissionEditForm>();
        secForm.setInitToken("token");
        secForm.setPostToken("token");
        secForm.setWrappedObject(form);
        return secForm;
    }
    
    @Test
    public void validateWarningPeriod() {
        EbpRatePermissionEditForm form = new EbpRatePermissionEditForm();
        WebSecurityForm<EbpRatePermissionEditForm> secForm = createValidWebSecurityForm(form);
        BindingResult result = new BeanPropertyBindingResult(secForm, "ebpRatePermissionEditForm");
        ExtendByPhoneValidator validator = new ExtendByPhoneValidator() {
            @Override
            public String getMessage(String code) {
                return "";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        validator.validateWarningPeriod(secForm, result);
        assertEquals(1, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        form.setWarningPeriod("");
        secForm = createValidWebSecurityForm(form);
        result = new BeanPropertyBindingResult(secForm, "ebpRatePermissionEditForm");
        validator.validateWarningPeriod(secForm, result);
        assertEquals(1, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        form.setWarningPeriod("p");
        secForm = createValidWebSecurityForm(form);
        result = new BeanPropertyBindingResult(secForm, "ebpRatePermissionEditForm");
        validator.validateWarningPeriod(secForm, result);
        assertEquals(1, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        form.setWarningPeriod("-1");
        secForm = createValidWebSecurityForm(form);
        result = new BeanPropertyBindingResult(secForm, "ebpRatePermissionEditForm");
        validator.validateWarningPeriod(secForm, result);
        assertEquals(1, secForm.getValidationErrorInfo().getErrorStatus().size());
        
        form.setWarningPeriod("40");
        secForm = createValidWebSecurityForm(form);
        result = new BeanPropertyBindingResult(secForm, "ebpRatePermissionEditForm");
        validator.validateWarningPeriod(secForm, result);
        assertEquals(0, secForm.getValidationErrorInfo().getErrorStatus().size());
    }
    
    @Test
    public void checkIfAllDigitsForFloat() {
        ExtendByPhoneValidator validator = new ExtendByPhoneValidator();
        assertTrue(validator.checkIfAllDigitsForFloat("0.50"));
        assertTrue(validator.checkIfAllDigitsForFloat("2"));
        assertTrue(validator.checkIfAllDigitsForFloat("0"));
        assertFalse(validator.checkIfAllDigitsForFloat("2.5.5"));
    }
    
    
    @Test
    public void isValidTimeFrame() {
        // protected boolean isValidTimeFrame(List<ExtensibleRate> extensibleRates, ExtensibleRate newRate) {
        List<ExtensibleRate> list = new ArrayList<ExtensibleRate>();
        list.add(createExtensibleRate(8, 0, 10, 0));    // 8am - 10am
        list.add(createExtensibleRate(13, 0, 16, 0));   // 1pm - 4pm
        list.add(createExtensibleRate(16, 0, 18, 0));   // 4pm - 6pm
        
        ExtensibleRate r = createExtensibleRate(9, 0, 10, 0);   // 9am - 10am, invalid
        
        ExtendByPhoneValidator validator = new ExtendByPhoneValidator();
        assertFalse(validator.isValidTimeFrame(list, r));

        r = createExtensibleRate(8, 0, 10, 0);     // 8am - 10pm, invalid
        assertFalse(validator.isValidTimeFrame(list, r));

        r = createExtensibleRate(7, 45, 10, 30);     // 7:45am - 10:30pm, invalid
        assertFalse(validator.isValidTimeFrame(list, r));
        
        r = createExtensibleRate(11, 0, 13, 30);     // 11am - 1:30pm, invalid
        assertFalse(validator.isValidTimeFrame(list, r));
        
        r = createExtensibleRate(10, 0, 13, 0);     // 10am - 1pm, ok
        assertTrue(validator.isValidTimeFrame(list, r));
        
        r = createExtensibleRate(7, 0, 8, 0);       // 7am - 8am, ok
        assertTrue(validator.isValidTimeFrame(list, r));
    }
    
    
    private ExtensibleRate createExtensibleRate(int beginHr, int beginMin, int endHr, int endMin) {
        ExtensibleRate r = new ExtensibleRate();
        r.setBeginHourLocal(beginHr);
        r.setBeginMinuteLocal(beginMin);
        r.setEndHourLocal(endHr);
        r.setEndMinuteLocal(endMin);
        return r;
        
    }
    
    private void createAuthentication() {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("CustomerAdmin"));
        WebUser user = new WebUser(3, 3, "testUser", "password", "salt", true, authorities);
        user.setCustomerTimeZone("US/Pacific");
        user.setIsComplexPassword(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

}
