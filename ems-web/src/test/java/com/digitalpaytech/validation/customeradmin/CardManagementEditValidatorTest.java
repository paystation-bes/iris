package com.digitalpaytech.validation.customeradmin;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.digitalpaytech.mvc.customeradmin.support.BannedCardEditForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.impl.CustomerCardTypeServiceImpl;

/**
 * 
 * 
 * @author Brian Kim
 * 
 */
public class CardManagementEditValidatorTest {
    
    private WebSecurityForm<BannedCardEditForm> webSecurityForm;
    private BannedCardEditForm bannedCardEditForm;
    
    @Before
    public void setUp() throws Exception {
        bannedCardEditForm = new BannedCardEditForm();
        webSecurityForm = new WebSecurityForm<BannedCardEditForm>(null, bannedCardEditForm);
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpSession session = new MockHttpSession();
        request.setSession(session);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }
    
    @After
    public void tearDown() throws Exception {
        webSecurityForm = null;
        bannedCardEditForm = null;
    }
    
    @Test
    public void test_invalid_token() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("2222222222222");
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardEditForm");
        
        CardManagementEditValidator validator = new CardManagementEditValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getErrorFieldName(), "postToken");
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getMessage(), "error.common.invalid.posttoken");
    }
    
    @Test
    public void test_invalid_actionFlag() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(2);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardEditForm");
        
        CardManagementEditValidator validator = new CardManagementEditValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid";
            }
        };
        
        validator.validate(webSecurityForm, result);
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getErrorFieldName(), "actionFlag");
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getMessage(), "error.common.invalid");
    }
    
    @Test
    public void test_cardNumber_required() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardEditForm");
        
        CardManagementEditValidator validator = new CardManagementEditValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 2);
    }
    
    @Test
    public void test_cardtype_required() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardEditForm");
        
        bannedCardEditForm.setCardNumber("3439824733843");
        CardManagementEditValidator validator = new CardManagementEditValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
    }
    
    @Test
    public void test_invalid_char() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardEditForm");
        
        bannedCardEditForm.setCardNumber("3439d824733843");
        bannedCardEditForm.setCardTypeId(1);
        CardManagementEditValidator validator = new CardManagementEditValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 2);
    }
    
    @Test
    public void test_invalid_cardType() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardEditForm");
        
        bannedCardEditForm.setCardNumber("3439d824733843");
        bannedCardEditForm.setCardType("dskfsdf");
        CardManagementEditValidator validator = new CardManagementEditValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 2);
    }
    
    @Test
    public void test_invalid_creditcard() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardEditForm");
        bannedCardEditForm.setCardNumber("3439d824733843");
        bannedCardEditForm.setCardType("Credit");
        bannedCardEditForm.setCardExpiry("0513");
        
        CardManagementEditValidator validator = new CardManagementEditValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        validator.setCustomerCardTypeService(createFalseCustomerCardTypeService());
        
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 2);
    }
    
    @Test
    public void test_invalid_smartcard() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardEditForm");
        bannedCardEditForm.setCardNumber("3439d824733843");
        bannedCardEditForm.setCardType("Smart");
        
        CardManagementEditValidator validator = new CardManagementEditValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        validator.setCustomerCardTypeService(createTrueCustomerCardTypeService());
        
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 2);
    }
    
    @Test
    public void test_invalid_valuecard() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardEditForm");
        bannedCardEditForm.setCardNumber("3439d824733843");
        bannedCardEditForm.setCardType("Smart");
        
        CardManagementEditValidator validator = new CardManagementEditValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        validator.setCustomerCardTypeService(createTrueCustomerCardTypeService());
        
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 2);
    }
    
    @Test
    public void test_invalid_cardExpiry() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardEditForm");
        bannedCardEditForm.setCardNumber("343982473384");
        bannedCardEditForm.setCardType("Credit");
        bannedCardEditForm.setCardExpiry("0512");
        
        CardManagementEditValidator validator = new CardManagementEditValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        validator.setCustomerCardTypeService(createTrueCustomerCardTypeService());
        
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
    }
    
    @Test
    public void test_valid_comment() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardEditForm");
        bannedCardEditForm.setCardNumber("343982473384");
        bannedCardEditForm.setCardType("Credit");
        bannedCardEditForm.setCardExpiry("0513");
        bannedCardEditForm.setComment("correct comment");
        
        CardManagementEditValidator validator = new CardManagementEditValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        validator.setCustomerCardTypeService(createTrueCustomerCardTypeService());
        
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
    }
    
    @Test
    public void test_invalid_comment() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardEditForm");
        bannedCardEditForm.setCardNumber("343982473384");
        bannedCardEditForm.setCardType("Credit");
        bannedCardEditForm.setCardExpiry("0513");
        bannedCardEditForm.setComment("correct! comment");
        
        CardManagementEditValidator validator = new CardManagementEditValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        validator.setCustomerCardTypeService(createTrueCustomerCardTypeService());
        
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
    }
    
    @Test
    public void test_invalid_length_comment() {
        webSecurityForm.clearValidationErrors();
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardEditForm");
        bannedCardEditForm.setCardNumber("343982473384");
        bannedCardEditForm.setCardType("Credit");
        bannedCardEditForm.setCardExpiry("0513");
        bannedCardEditForm.setComment("This is the incorrect comment. It's over 80 chars. " +
        		"That's a lot of chars. More than enough to cause a validation error, don't you think. " +
        		"Well I could keep going on and on but I'm pretty sure we're over 80 chars. At least I think we're " +
        		"over 80 chars. Yeah we must be over chars by now, no doubt about it.");
        
        CardManagementEditValidator validator = new CardManagementEditValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        validator.setCustomerCardTypeService(createTrueCustomerCardTypeService());
        
        validator.validate(webSecurityForm, result);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 2);
    }
    
    private CustomerCardTypeService createTrueCustomerCardTypeService() {
        return new CustomerCardTypeServiceImpl() {
            public boolean isCreditCardData(String track2OrAcctNum) {
                return true;
            }
        };
    }
    
    private CustomerCardTypeService createFalseCustomerCardTypeService() {
        return new CustomerCardTypeServiceImpl() {
            public boolean isCreditCardData(String track2OrAcctNum) {
                return false;
            }
        };
    }
}
