package com.digitalpaytech.validation.customeradmin;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.mvc.customeradmin.support.BannedCardSearchForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.impl.CustomerCardTypeServiceImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.support.WebObject;
import com.digitalpaytech.validation.CardNumberValidator;

/**
 * 
 * 
 * @author Brian Kim
 * 
 */
public class CardManagementSearchValidatorTest {
    
    private WebSecurityForm<BannedCardSearchForm> webSecurityForm;
    private BannedCardSearchForm bannedCardSearchForm;
    private RandomKeyMapping keyMapping;
    
    @Before
    public void setUp() throws Exception {
        bannedCardSearchForm = new BannedCardSearchForm();        
        webSecurityForm = new WebSecurityForm<BannedCardSearchForm>(null, bannedCardSearchForm);
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpSession session = new MockHttpSession();
        request.setSession(session);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        keyMapping = SessionTool.getInstance(request).getKeyMapping();
    }
    
    @After
    public void tearDown() throws Exception {
        webSecurityForm = null;
        bannedCardSearchForm = null;
        keyMapping = null;
    }
    
    // CardManagementSearchValidator no longer validates token
//    @Test
//    public void test_invalid_token() {
//        webSecurityForm.setInitToken("1111111111111");
//        webSecurityForm.setPostToken("2222222222222");
//        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardSearchForm");
//        
//        CardManagementSearchValidator validator = new CardManagementSearchValidator() {
//            @Override
//            public String getMessage(String code) {
//                return "error.common.invalid.posttoken";
//            }
//            
//            @Override
//            public String getMessage(String code, Object[] args) {
//                return "";
//            }
//        };
//        validator.validate(webSecurityForm, result);
//        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
//        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getErrorFieldName(), "actionFlag");
//        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getMessage(), "error.common.invalid.posttoken");
//    }
    
//    @Test
//    public void test_valid_input_no_selection() {
//        webSecurityForm.setInitToken("1111111111111");
//        webSecurityForm.setPostToken("1111111111111");
//        webSecurityForm.setActionFlag(2);
//        CardType type = new CardType();
//        type.setId(2);
//        type.setName("Smart Card");
//        WebObject obj = keyMapping.hideProperty(type, "id");        
//        bannedCardSearchForm.setFilterCardTypeId(obj.getPrimaryKey().toString());        
//        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardSearchForm");
//        
//        CardManagementSearchValidator validator = new CardManagementSearchValidator() {
//            @Override
//            public String getMessage(String code) {
//                return "";
//            }
//            
//            @Override
//            public String getMessage(String code, Object[] args) {
//                return "";
//            }
//        };
//        validator.validate(webSecurityForm, result);       
//        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 0);
//    }
    
    @Test
    public void test_valid_creditcard_input() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(2);
        bannedCardSearchForm.setFilterCardNumber("234567896543");
        CustomerCardType cct = new CustomerCardType();
        cct.setId(1);
        CardType type = new CardType();
        type.setId(3);
        type.setName("Value Card");
        cct.setCardType(type);
        WebObject obj = keyMapping.hideProperty(cct, "id");        
        bannedCardSearchForm.setFilterCardTypeId(obj.getPrimaryKey().toString());
        bannedCardSearchForm.setCustomerCardType(cct);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardSearchForm");
        
        CardManagementSearchValidator validator = new CardManagementSearchValidator() {
            @Override
            public String getMessage(String code) {
                return "";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        validator.setCardNumberValidator(createCardNumberValidator());
        validator.validate(webSecurityForm, result);
        validator.validateCardNumber(webSecurityForm);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 0);
    }
    
//    @Test
//    public void test_valid_smartcard_input() {
//        webSecurityForm.setInitToken("1111111111111");
//        webSecurityForm.setPostToken("1111111111111");
//        webSecurityForm.setActionFlag(2);
//        bannedCardSearchForm.setFilterCardNumber("234567896543");
//        CardType type = new CardType();
//        type.setId(2);
//        type.setName("Smart Card");
//        WebObject obj = keyMapping.hideProperty(type, "id");
//        
//        bannedCardSearchForm.setFilterCardTypeId(obj.getPrimaryKey().toString());
//        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardSearchForm");
//        
//        CardManagementSearchValidator validator = new CardManagementSearchValidator() {
//            @Override
//            public String getMessage(String code) {
//                return "";
//            }
//            
//            @Override
//            public String getMessage(String code, Object[] args) {
//                return "";
//            }
//        };
//        validator.setCardNumberValidator(createCardNumberValidator());
//        validator.validate(webSecurityForm, result);
//        
//        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 0);
//    }
    
//    @Test
//    public void test_valid_valuecard_input() {
//        webSecurityForm.setInitToken("1111111111111");
//        webSecurityForm.setPostToken("1111111111111");
//        webSecurityForm.setActionFlag(2);
//        bannedCardSearchForm.setFilterCardNumber("234567896543");
//        CardType type = new CardType();
//        type.setId(3);
//        type.setName("Value Card");
//        WebObject obj = keyMapping.hideProperty(type, "id");
//        
//        bannedCardSearchForm.setFilterCardTypeId(obj.getPrimaryKey().toString());
//        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardSearchForm");
//        
//        CardManagementSearchValidator validator = new CardManagementSearchValidator() {
//            @Override
//            public String getMessage(String code) {
//                return "";
//            }
//            
//            @Override
//            public String getMessage(String code, Object[] args) {
//                return "";
//            }
//        };
//        validator.setCardNumberValidator(createCardNumberValidator());
//        validator.validate(webSecurityForm, result);
//        
//        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 0);
//    }
    
    @Test
    public void test_invalid_creditcard_length() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(2);
        bannedCardSearchForm.setFilterCardNumber("234567896543123486");
        CustomerCardType cct = new CustomerCardType();
        cct.setId(1);
        CardType type = new CardType();
        type.setId(1);
        type.setName("Credit Card");
        cct.setCardType(type);
        WebObject obj = keyMapping.hideProperty(cct, "id");        
        bannedCardSearchForm.setFilterCardTypeId(obj.getPrimaryKey().toString());
        bannedCardSearchForm.setCustomerCardType(cct);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardSearchForm");
        
        CardManagementSearchValidator validator = new CardManagementSearchValidator() {
            @Override
            public String getMessage(String code) {
                return "";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        validator.setCardNumberValidator(createCardNumberValidator());
        validator.validate(webSecurityForm, result);
        validator.validateCardNumber(webSecurityForm);
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
    }
    
    @Test
    public void test_invalid_smartcard_length() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(2);
        bannedCardSearchForm.setFilterCardNumber("234567896543345865476");
        CustomerCardType cct = new CustomerCardType();
        cct.setId(1);
        CardType type = new CardType();
        type.setId(2);
        type.setName("Smart Card");
        cct.setCardType(type);
        WebObject obj = keyMapping.hideProperty(cct, "id");
        
        bannedCardSearchForm.setFilterCardTypeId(obj.getPrimaryKey().toString());
        bannedCardSearchForm.setCustomerCardType(cct);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardSearchForm");
        
        CardManagementSearchValidator validator = new CardManagementSearchValidator() {
            @Override
            public String getMessage(String code) {
                return "";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        validator.setCardNumberValidator(createCardNumberValidator());
        validator.validate(webSecurityForm, result);
        validator.validateCardNumber(webSecurityForm);
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
    }
    
    @Test
    public void test_invalid_valuecard_length() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(2);
        bannedCardSearchForm.setFilterCardNumber("234567896543548534958439578435348579345");
        CustomerCardType cct = new CustomerCardType();
        cct.setId(1);
        CardType type = new CardType();
        type.setId(3);
        type.setName("Value Card");
        cct.setCardType(type);
        WebObject obj = keyMapping.hideProperty(cct, "id");
        
        bannedCardSearchForm.setFilterCardTypeId(obj.getPrimaryKey().toString());
        bannedCardSearchForm.setCustomerCardType(cct);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardSearchForm");
        
        CardManagementSearchValidator validator = new CardManagementSearchValidator() {
            @Override
            public String getMessage(String code) {
                return "";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        validator.setCardNumberValidator(createCardNumberValidator());
        validator.validate(webSecurityForm, result);
        validator.validateCardNumber(webSecurityForm);
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0);
    }
    
    @Test
    public void test_invalid_creditcard_data() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(2);
        bannedCardSearchForm.setFilterCardNumber("234567896543435253425");
        CustomerCardType cct = new CustomerCardType();
        cct.setId(1);
        CardType type = new CardType();
        type.setId(1);
        type.setName("Credit Card");
        cct.setCardType(type);
        WebObject obj = keyMapping.hideProperty(cct, "id");
        
        bannedCardSearchForm.setFilterCardTypeId(obj.getPrimaryKey().toString());
        bannedCardSearchForm.setCustomerCardType(cct);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardSearchForm");
        
        CardManagementSearchValidator validator = new CardManagementSearchValidator() {
            @Override
            public String getMessage(String code) {
                return "";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        validator.setCardNumberValidator(createCardNumberValidator());
        validator.validate(webSecurityForm, result);
        validator.validateCardNumber(webSecurityForm);        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
    }
    
    @Test
    public void test_invalid_smartcard_data() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(2);
        bannedCardSearchForm.setFilterCardNumber("23459292938382929293838293829");
        CustomerCardType cct = new CustomerCardType();
        cct.setId(1);
        CardType type = new CardType();
        type.setId(2);
        type.setName("Smart Card");
        cct.setCardType(type);
        WebObject obj = keyMapping.hideProperty(cct, "id");
        
        bannedCardSearchForm.setFilterCardTypeId(obj.getPrimaryKey().toString());
        bannedCardSearchForm.setCustomerCardType(cct);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardSearchForm");
        
        CardManagementSearchValidator validator = new CardManagementSearchValidator() {
            @Override
            public String getMessage(String code) {
                return "";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        validator.setCardNumberValidator(createCardNumberValidator());
        validator.validate(webSecurityForm, result);
        validator.validateCardNumber(webSecurityForm);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0);
    }
    
    @Test
    public void test_invalid_valuecard_data() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(2);
        bannedCardSearchForm.setFilterCardNumber("");
        CustomerCardType cct = new CustomerCardType();
        cct.setId(1);
        CardType type = new CardType();
        type.setId(3);
        type.setName("Value Card");
        cct.setCardType(type);
        WebObject obj = keyMapping.hideProperty(cct, "id");
        
        bannedCardSearchForm.setFilterCardTypeId(obj.getPrimaryKey().toString());
        bannedCardSearchForm.setCustomerCardType(cct);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardSearchForm");
        
        CardManagementSearchValidator validator = new CardManagementSearchValidator() {
            @Override
            public String getMessage(String code) {
                return "";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        validator.setCardNumberValidator(createCardNumberValidator());
        validator.validate(webSecurityForm, result);
        validator.validateCardNumber(webSecurityForm);
        
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() > 0);
    }
    
    @Test
    public void test_invalid_card_type() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(2);
        bannedCardSearchForm.setFilterCardNumber("234567896543");
        CustomerCardType cct = new CustomerCardType();
        cct.setId(1);
        CardType type = new CardType();
        type.setId(10);
        type.setName("Test Card");
        cct.setCardType(type);
        WebObject obj = keyMapping.hideProperty(cct, "id");
        bannedCardSearchForm.setFilterCardTypeId(obj.getPrimaryKey().toString());
        bannedCardSearchForm.setCustomerCardType(cct);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "bannedCardSearchForm");
        CardManagementSearchValidator validator = new CardManagementSearchValidator() {
            @Override
            public String getMessage(String code) {
                return "";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        validator.setCardNumberValidator(createCardNumberValidator());
        //JOptionPane.showMessageDialog(null, "PREVALIDATE"); 
        validator.validate(webSecurityForm, result);
//      String strErrors = "Number of Errors = " + webSecurityForm.getValidationErrorInfo().getErrorStatus().size() + "\n";
//      for (int i = 0; i < webSecurityForm.getValidationErrorInfo().getErrorStatus().size(); i++) {
//          strErrors += ((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(i))).getErrorFieldName() + ";\n";          
//      }            
//      JOptionPane.showMessageDialog(null, strErrors);           
    //JOptionPane.showMessageDialog(null, "2. CardTypeId = " + bannedCardSearchForm.getCardTypeId());       
        //JOptionPane.showMessageDialog(null,testform.getName());        
        //JOptionPane.showMessageDialog(null, "Number of Errors: " + webSecurityForm.getValidationErrorInfo().getErrorStatus().size());
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 0);
    }

    
    private CustomerCardTypeService createTrueCustomerCardTypeService() {
        return new CustomerCardTypeServiceImpl() {
            public boolean isCreditCardData(String track2OrAcctNum) {
                return true;
            }
        };
    }
    
    private CardNumberValidator<BannedCardSearchForm> createCardNumberValidator() {
    	CardNumberValidator<BannedCardSearchForm> result = new CardNumberValidator<BannedCardSearchForm>() {
    		@Override
            public String getMessage(String code) {
                return "";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
    	};
    	
    	result.setCustomerCardTypeService(createTrueCustomerCardTypeService());
    	
    	return result;
    }
    
    private CustomerCardTypeService createFalseCustomerCardTypeService() {
        return new CustomerCardTypeServiceImpl() {
            public boolean isCreditCardData(String track2OrAcctNum) {
                return false;
            }
        };
    }
}
