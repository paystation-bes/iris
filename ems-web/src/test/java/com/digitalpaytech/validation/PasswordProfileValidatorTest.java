package com.digitalpaytech.validation;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.SystemWideSaltSource;
import org.springframework.security.authentication.encoding.PlaintextPasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.DirectFieldBindingResult;
import org.springframework.validation.Errors;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.mvc.support.ChangePasswordForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;

public class PasswordProfileValidatorTest {

	@Test
	public void validateBadPasswords() {
		WebSecurityForm<ChangePasswordForm> secForm = new WebSecurityForm<ChangePasswordForm>();
		secForm.setPostToken("TEST123");
		secForm.setInitToken("TEST1234");

		ChangePasswordForm passwdProf = new ChangePasswordForm("pass", "pass");
		secForm.setWrappedObject(passwdProf);		
		
		Errors errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
		PasswordProfileValidator validator = new PasswordProfileValidator() {
		    @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid.posttoken";
            }
		};
		
        validator.setPasswordEncoder(new PlaintextPasswordEncoder() {
            @Override
            public String encodePassword(String rawPass, Object salt) {
                return "password";
            }
        });
        validator.setSaltSource(new SystemWideSaltSource() {
            @Override
            public Object getSalt(UserDetails user) {
                return "";
            }
        });		
		
		// Invalid token
		validator.validate(secForm, errors);
		assertEquals(1, secForm.getValidationErrorInfo().getErrorStatus().size());
		secForm.removeAllErrorStatus();
		secForm.setInitToken("TEST123");
		
		// Passwords aren't the same
		passwdProf.setNewPassword("abc123");
		passwdProf.setNewPasswordAgain("abc1234");
		secForm.setWrappedObject(passwdProf);
		errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
		validator.validate(secForm, errors);
		assertEquals(1, secForm.getValidationErrorInfo().getErrorStatus().size());
		secForm.removeAllErrorStatus();
		
		// Invalid password length		
		passwdProf.setNewPasswordAgain("abc123");
		errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
		validator.validate(secForm, errors);
		assertEquals(1, secForm.getValidationErrorInfo().getErrorStatus().size());
		secForm.removeAllErrorStatus();
		
		// Simple password 
		passwdProf.setNewPassword("abcabcabc");
		passwdProf.setNewPasswordAgain("abcabcabc");
		secForm.setWrappedObject(passwdProf);
		errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
		validator.validate(secForm, errors);
		assertEquals(1, secForm.getValidationErrorInfo().getErrorStatus().size());
		secForm.removeAllErrorStatus();
		
		// Complex passwords aren't the same
		passwdProf.setNewPassword("abc123ABC$");
		passwdProf.setNewPasswordAgain("abc123ABC$$");
		secForm.setWrappedObject(passwdProf);
		errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
		validator.validate(secForm, errors);
		assertEquals(1, secForm.getValidationErrorInfo().getErrorStatus().size());
		secForm.removeAllErrorStatus();
	}
	
	
	@Test
	public void validateGoodPassword() {
		// Complex passwords checking
		ChangePasswordForm passwdProf = new ChangePasswordForm("abc123ABC$", "abc123ABC$");
		WebSecurityForm<ChangePasswordForm> secForm = new WebSecurityForm<ChangePasswordForm>();
		secForm.setPostToken("TEST123");
		secForm.setInitToken("TEST123");		
		secForm.setWrappedObject(passwdProf);
		Errors errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");

		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("Admin"));
		WebUser user = new WebUser(1, 1, "test", "password", "salt", true, authorities);
		Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		PasswordProfileValidator validator = new PasswordProfileValidator();
		validator.setPasswordEncoder(new PlaintextPasswordEncoder() {
			@Override
			public String encodePassword(String rawPass, Object salt) {
				return "password";
			}
		});
		validator.setSaltSource(new SystemWideSaltSource() {
			@Override
			public Object getSalt(UserDetails user) {
				return "";
			}
		});
		
		validator.validate(secForm, errors);
		assertEquals(0, errors.getErrorCount());		
	}
}
