package com.digitalpaytech.validation;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.digitalpaytech.mvc.support.AlertEditForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.validation.AlertSettingValidator;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/ems-web.xml" })
public class AlertSettingValidatorTest {
    
    private WebSecurityForm<AlertEditForm> webSecurityForm;
    private AlertEditForm alertEditForm;
    private static RandomKeyMapping keyMapping;
    
    @Autowired
    AlertSettingValidator asValidator;
    
    @Autowired
    MessageSourceAccessor messageAccessor;
    
    @Before
    public void setUp() throws Exception {
        alertEditForm = new AlertEditForm();
        alertEditForm.setRouteId(2);
        alertEditForm.setAlertName("AlertName");
        alertEditForm.setAlertTypeId((short)1);
        alertEditForm.setAlertThresholdExceed("1000");
        webSecurityForm = new WebSecurityForm<AlertEditForm>(null, alertEditForm);
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpSession session = new MockHttpSession();
        request.setSession(session);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        keyMapping = SessionTool.getInstance(request).getKeyMapping();
    }
    
    @After
    public void tearDown() throws Exception {
        webSecurityForm = null;
        alertEditForm = null;
    }
    
    @Test
    public void test_invalid_token() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("2222222222222");
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "alertEditForm");
        //asValidator.setMessageAccessor(messageAccessor);
//        AlertSettingValidator validator = new AlertSettingValidator() {
//            @Override
//            public String getMessage(String code) {
//                return "error.common.invalid.posttoken";
//            }
//            
//            @Override
//            public String getMessage(String code, Object[] args) {
//                return "error.common.invalid.posttoken";
//            }
//        };
        
        try {
            //validator.validate(webSecurityForm, result, keyMapping);
            asValidator.validate(webSecurityForm, result, keyMapping);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Exception Caught! " + e.getMessage()  );
        }        
//      String strErrors = "Number of errors = " + webSecurityForm.getValidationErrorInfo().getErrorStatus().size() + "\n";
//      for (int i = 0; i < webSecurityForm.getValidationErrorInfo().getErrorStatus().size(); i++) {
//          strErrors += ((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(i))).getErrorFieldName();
//         
//      }      
//      JOptionPane.showMessageDialog(null, strErrors);          
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
    }
    
    @Test
    public void test_invalid_actionFlag() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(2);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "alertEditForm");
        
//        AlertSettingValidator validator = new AlertSettingValidator() {
//            @Override
//            public String getMessage(String code) {
//                return "error.common.invalid.posttoken";
//            }
//            
//            @Override
//            public String getMessage(String code, Object[] args) {
//                return "error.common.invalid.posttoken";
//            }
//        };
    
        try{
            asValidator.validate(webSecurityForm, result, keyMapping);
        }catch (Exception e){}
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
    }
    
    @Test
    public void test_invalid_input() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "alertEditForm");
        
//        AlertSettingValidator alertSettingValidator = new AlertSettingValidator() {
//            @Override
//            public String getMessage(String code) {
//                return "error.common.invalid.posttoken";
//            }
//            
//            @Override
//            public String getMessage(String code, Object[] args) {
//                return "error.common.invalid.posttoken";
//            }
//        };
        try{
            asValidator.validate(webSecurityForm, result, keyMapping);            
        }catch (Exception e){}
        
        //JOptionPane.showMessageDialog(null, "0. asValidator = " + asValidator);
       // JOptionPane.showMessageDialog(null, "0. " + webSecurityForm.getValidationErrorInfo().getErrorStatus().size());
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 2);
    }
    
    @Test
    public void test_invalid_length_alertType1() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "alertEditForm");
        
        alertEditForm.setAlertName("11111111111111111111111111");
        alertEditForm.setAlertTypeId((short)1);
        alertEditForm.setAlertThresholdExceed("999");
//        AlertSettingValidator alertSettingValidator = new AlertSettingValidator() {
//            @Override
//            public String getMessage(String code) {
//                return "error.common.invalid.posttoken";
//            }
//            
//            @Override
//            public String getMessage(String code, Object[] args) {
//                return "error.common.invalid.posttoken";
//            }
//        };
        try{
            asValidator.validate(webSecurityForm, result, keyMapping);
        }catch (Exception e){}
                
       
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 2);
        
    }
    
    @Test
    public void test_invalid_length_alertType2() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "alertEditForm");
        
        alertEditForm.setAlertName("11111111111111111111111111");
        alertEditForm.setAlertTypeId((short)2);
        alertEditForm.setAlertThresholdExceed("1000");
//        AlertSettingValidator alertSettingValidator = new AlertSettingValidator() {
//            @Override
//            public String getMessage(String code) {
//                return "error.common.invalid.posttoken";
//            }
//            
//            @Override
//            public String getMessage(String code, Object[] args) {
//                return "error.common.invalid.posttoken";
//            }
//        };
        try{
           asValidator.validate(webSecurityForm, result, keyMapping);
        }catch (Exception e){}
        
//      String strErrors = "" + webSecurityForm.getValidationErrorInfo().getErrorStatus().size() + "\n";
//      for (int i = 0; i < webSecurityForm.getValidationErrorInfo().getErrorStatus().size(); i++) {
//          strErrors += ((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(i))).getErrorFieldName();
//         
//      }
//      
//      JOptionPane.showMessageDialog(null, strErrors);  
        
        //JOptionPane.showMessageDialog(null, "1. " + webSecurityForm.getValidationErrorInfo().getErrorStatus().size());
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 2);
        
    }
    
    @Test
    public void test_special_chars() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "alertEditForm");
        webSecurityForm.setErrorStatus(new ArrayList<FormErrorStatus>());
        alertEditForm.setAlertName("AlertN@me\"");
        alertEditForm.setAlertTypeId((short)2);
        alertEditForm.setAlertThresholdExceed("1000");
        alertEditForm.setRouteId(2);
        try{   
            
            asValidator.validate(webSecurityForm, result, keyMapping);
            
        }catch (Exception e){
            JOptionPane.showMessageDialog(null, "Exception Caught! " + e.getMessage()  );
        }
//        String strErrors = "" + webSecurityForm.getValidationErrorInfo().getErrorStatus().size() + "\n";
//        for (int i = 0; i < webSecurityForm.getValidationErrorInfo().getErrorStatus().size(); i++) {
//            strErrors += ((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(i))).getErrorFieldName() + " --- ";
//            strErrors += ((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(i))).getMessage() + "; \n";
//        }        
//        JOptionPane.showMessageDialog(null, strErrors);
//        JOptionPane.showMessageDialog(null, "2. " + webSecurityForm.getValidationErrorInfo().getErrorStatus().size());
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 2);
    }
    
    @Test
    public void test_invalid_threshold() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "alertEditForm");
        alertEditForm.setAlertName("AlertName");
        alertEditForm.setAlertTypeId((short)2);        
        alertEditForm.setAlertThresholdExceed("a");
//        AlertSettingValidator alertSettingValidator = new AlertSettingValidator() {
//            @Override
//            public String getMessage(String code) {
//                return "error.common.invalid.posttoken";
//            }
//            
//            @Override
//            public String getMessage(String code, Object[] args) {
//                return "error.common.invalid.posttoken";
//            }
//        };
        try{
            asValidator.validate(webSecurityForm, result, keyMapping);
        }catch (Exception e){}
        
      //JOptionPane.showMessageDialog(null, "5. " + webSecurityForm.getValidationErrorInfo().getErrorStatus().size());
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 2);
    }
    
    @Test
    public void test_invalid_email() {
        webSecurityForm.setInitToken("1111111111111");
        webSecurityForm.setPostToken("1111111111111");
        webSecurityForm.setActionFlag(0);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "alertEditForm");
        
        List<String> alertContacts = new ArrayList<String>();
        alertContacts.add("aaa@bbb.com,Brian.kim");
        alertEditForm.setAlertContacts(alertContacts);
//        AlertSettingValidator alertSettingValidator = new AlertSettingValidator() {
//            @Override
//            public String getMessage(String code) {
//                return "error.common.invalid.posttoken";
//            }
//            
//            @Override
//            public String getMessage(String code, Object[] args) {
//                return "error.common.invalid.posttoken";
//            }
//        };
        try{
            asValidator.validate(webSecurityForm, result, keyMapping);
        }catch (Exception e){}
        
        //JOptionPane.showMessageDialog(null, "3. " + webSecurityForm.getValidationErrorInfo().getErrorStatus().size());
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 3);
    }
}
