package com.digitalpaytech.bdd.mvc.customer;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.mvc.systemadmin.systemadminmaincontroller.TestCustomerAutoCompleteReturns;
import com.digitalpaytech.bdd.mvc.systemadmin.systemadminmaincontroller.TestCustomerDetailsLookup;
import com.digitalpaytech.bdd.mvc.systemadmin.systemadminmaincontroller.TestCustomerSave;
import com.digitalpaytech.bdd.mvc.systemadmin.systemadminmaincontroller.TestSearchForCustomer;
import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class AddCustomerSubscriptionsStories extends AbstractStories {
    
    public AddCustomerSubscriptionsStories() {
        super();
        this.testHandlers.registerTestHandler("profilecustomerprofile", TestSearchForCustomer.class);
        this.testHandlers.registerTestHandler("customersearchbarsearch", TestSearchForCustomer.class);
        this.testHandlers.registerTestHandler("customersearchbartype", TestCustomerAutoCompleteReturns.class);
        this.testHandlers.registerTestHandler("customersearchbarautocompletereturns", TestCustomerAutoCompleteReturns.class);
        this.testHandlers.registerTestHandler("verifiedcustomerdetailspage", TestCustomerDetailsLookup.class);
        this.testHandlers.registerTestHandler("customerreturned", TestCustomerDetailsLookup.class);
        this.testHandlers.registerTestHandler("customersave", TestCustomerSave.class);
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    // Here we specify the steps classes
    @Override
    public final InjectableStepsFactory stepsFactory() {
        // varargs, can have more that one steps classes
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new ControllerBaseSteps(this.testHandlers));
    }
}
