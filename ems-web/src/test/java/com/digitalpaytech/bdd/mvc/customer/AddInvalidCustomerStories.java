package com.digitalpaytech.bdd.mvc.customer;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.mvc.systemadmin.systemadminmaincontroller.TestCustomerSave;
import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class AddInvalidCustomerStories extends AbstractStories {
    
    public AddInvalidCustomerStories() {
        super();
        this.testHandlers.registerTestHandler("customersave", TestCustomerSave.class);
        this.testHandlers.registerTestHandler("customerreceive", TestCustomerSave.class);
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    // Here we specify the steps classes
    @Override
    public final InjectableStepsFactory stepsFactory() {
        // varargs, can have more that one steps classes
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new ControllerBaseSteps(this.testHandlers));
    }
}
