package com.digitalpaytech.bdd.mvc.customeradmin.transactionreceiptcontroller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.dao.impl.OpenSessionDaoTest;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.mvc.customeradmin.TransactionReceiptController;
import com.digitalpaytech.service.cps.impl.CPSDataServiceImpl;

import com.digitalpaytech.service.impl.CustomerAdminServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.PaymentCardServiceImpl;
import com.digitalpaytech.service.impl.PermitServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.ProcessorTransactionServiceImpl;
import com.digitalpaytech.service.impl.ProcessorTransactionTypeServiceImpl;
import com.digitalpaytech.service.impl.PurchaseEmailAddressServiceImpl;
import com.digitalpaytech.service.impl.PurchaseServiceImpl;
import com.digitalpaytech.service.impl.PurchaseTaxServiceImpl;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.impl.TLSUtilsImpl;

@SuppressWarnings({ "PMD.ExcessiveImports", "PMD.UnusedPrivateField" })
public class TestTransactionReceipts implements JBehaveTestHandler {
    
    @Mock
    private MessageHelper messages;
    
    @InjectMocks
    private OpenSessionDaoTest openSessionDao;
    
    @InjectMocks
    private ProcessorTransactionTypeServiceImpl processorTransactionTypeService;
    
    @InjectMocks
    private PaymentCardServiceImpl paymentCardService;
    
    @InjectMocks
    private ProcessorTransactionServiceImpl processorTransactionService;
    
    @InjectMocks
    private PermitServiceImpl permitService;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    private PurchaseTaxServiceImpl purchaseTaxService;
    
    @InjectMocks
    private PurchaseEmailAddressServiceImpl purchaseEmailAddressService;
    
    @InjectMocks
    private PurchaseServiceImpl purchaseService;
    
    @InjectMocks
    private CustomerAdminServiceImpl customerAdminService;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private EntityServiceImpl entityService;
    
    @InjectMocks
    private TransactionReceiptController trController;
    
    @InjectMocks
    private CPSDataServiceImpl cpsDataService;
    
    @InjectMocks
    private TLSUtilsImpl tlsUtils;
    
    public TestTransactionReceipts() {
        MockitoAnnotations.initMocks(this);
        
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    
    private String viewReceipt() {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final MockHttpServletRequest request = controllerFields.getRequest();
        final MockHttpServletResponse response = controllerFields.getResponse();
        final ModelMap model = controllerFields.getModel();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final List<Purchase> purchaseList = TestContext.getInstance().getDatabase().find(Purchase.class);
        if (purchaseList.size() != 1) {
            throw new IllegalStateException("This test handler only support single Purchase! Current size = " + purchaseList.size());
        }
        
        final Purchase purchase = purchaseList.get(0);
        
        request.setParameter("transactionId", keyMapping.getRandomString(Purchase.class, purchase.getId()));
        final String controllerResponse = this.trController.showReceiptDetails(request, response, model);
        final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
        wrapperObject.setControllerResponse(controllerResponse);
        return controllerResponse;
        
    }
    
    private void notEmvStuff() {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        assertThat(controllerFields.getControllerResponse(), containsString("cvm"));
        assertThat(controllerFields.getControllerResponse(), containsString("apl"));
        assertThat(controllerFields.getControllerResponse(), containsString("aid"));
    }
    
    private void showEmvStuff() {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        assertThat(controllerFields.getControllerResponse(), containsString("cvm"));
        assertThat(controllerFields.getControllerResponse(), containsString("apl"));
        assertThat(controllerFields.getControllerResponse(), containsString("aid"));
    }
    
    @Override
    public Object assertFailure(final Object... arg0) {
        notEmvStuff();
        return null;
    }
    
    @Override
    public Object assertSuccess(final Object... arg0) {
        showEmvStuff();
        return null;
    }
    
    @Override
    public Object performAction(final Object... arg0) {
        viewReceipt();
        return null;
    }
    
}
