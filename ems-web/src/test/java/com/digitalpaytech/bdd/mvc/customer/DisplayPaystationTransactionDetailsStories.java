package com.digitalpaytech.bdd.mvc.customer;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.mvc.customeradmin.customeradminpaystationdetailcontroller.TestTransactionReport;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.dto.customeradmin.PayStationListTransactionReportDetails;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class DisplayPaystationTransactionDetailsStories extends AbstractStories {
    
    public DisplayPaystationTransactionDetailsStories() {
        super();
        this.testHandlers.registerTestHandler("paystationtransactiondetailsview", TestTransactionReport.class);
        TestLookupTables.getExpressionParser().register(new Class[] {PayStationListTransactionReportDetails.class});
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }

    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new ControllerBaseSteps(this.testHandlers));
    }
    
}
