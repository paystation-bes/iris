package com.digitalpaytech.bdd.mvc.systemadmin.systemadminmaincontroller;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.digitalpaytech.bdd.services.CustomerServiceMockImpl;
import com.digitalpaytech.bdd.services.CustomerSubscriptionServiceMockImpl;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.EntityDB;
import com.digitalpaytech.bdd.util.TestConstants;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerPropertyType;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.SubscriptionType;
import com.digitalpaytech.dto.customeradmin.CustomerDetails;
import com.digitalpaytech.mvc.systemadmin.CustomerDetailsController;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerAgreementService;
import com.digitalpaytech.service.CustomerMigrationService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.CustomerSubscriptionService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.RestAccountService;
import com.digitalpaytech.service.WebServiceEndPointService;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;

import junit.framework.Assert;

@SuppressWarnings({ "PMD.BeanMembersShouldSerialize", "PMD.ExcessiveImports" })
public class TestCustomerDetailsLookup implements JBehaveTestHandler{
    
    private static final String CUSTOMER_ID = "customerID";
    
    @Mock
    private CustomerService customerService;
    
    @Mock
    private CustomerSubscriptionService customerSubscriptionService;
    
    @Mock
    private MerchantAccountService merchantAccountService;
    
    @Mock
    private WebServiceEndPointService webServiceEndPointService;
    
    @Mock
    private CustomerAgreementService customerAgreementService;
    
    @Mock
    private CustomerMigrationService customerMigrationService;
    
    @Mock
    private RestAccountService restAccountService;
    
    @Mock
    private CustomerAdminService customerAdminService;
    
    @InjectMocks
    private CustomerDetailsController customerDetailsController;
    
    public final void initializeMocks() {
        MockitoAnnotations.initMocks(this);
        when(this.customerService.findCustomer(anyInt())).thenAnswer(CustomerServiceMockImpl.findCustomer());
        
        when(this.customerSubscriptionService.findCustomerSubscriptionsByCustomerId(anyInt(), anyBoolean()))
                .thenAnswer(CustomerSubscriptionServiceMockImpl.findCustomerSubscriptionsByCustomerId());
                
        when(this.customerService.findAllParentCustomers()).thenReturn(null);
        when(this.merchantAccountService.findAllMerchantAccountsByCustomerId(anyInt())).thenReturn(null);
        when(this.webServiceEndPointService.findWebServiceEndPointsByCustomer(anyInt())).thenReturn(null);
        when(this.webServiceEndPointService.findPrivateWebServiceEndPointsByCustomer(anyInt())).thenReturn(null);
        when(this.customerAgreementService.findCustomerAgreementByCustomerId(anyInt())).thenReturn(null);
        when(this.customerMigrationService.findCustomerMigrationByCustomerId(anyInt())).thenReturn(null);
        when(this.restAccountService.findRestAccounts(anyInt())).thenReturn(null);
        when(this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(anyInt(), anyInt()))
                .thenAnswer(new Answer<CustomerProperty>() {
                    @Override
                    public CustomerProperty answer(final InvocationOnMock invocation) throws Throwable {
                        return new CustomerProperty(new CustomerPropertyType(), new Customer(), WebCoreConstants.SYSTEM_ADMIN_UI_TIMEZONE, new Date(),
                                0);
                    }
                    
                });
    }
    
    private void loadCustomerDetails() {
        initializeMocks();
        final ControllerFieldsWrapper wrapperObject =
                (ControllerFieldsWrapper) TestDBMap.findObjectByIdFromMemory(TestDBMap.CONTROLLER_FIELDS_WRAPPER_ID, ControllerFieldsWrapper.class);
        String dataFromController = wrapperObject.getData();
        if (dataFromController != null) {
            dataFromController = dataFromController.replaceAll("customer", WebCoreConstants.BLANK);
            dataFromController = dataFromController.replaceAll("Customer", WebCoreConstants.BLANK);
        }
        final Customer customer = (Customer) TestDBMap.findObjectByFieldFromMemory(dataFromController.trim(), Customer.ALIAS_NAME, Customer.class);
        if (customer == null) {
            Assert.fail("The customer" + dataFromController.trim() + "was not saved");
        }
        wrapperObject.getRequest().setParameter(CUSTOMER_ID, SessionTool.getInstance(wrapperObject.getRequest()).getKeyMapping().getRandomString(Customer.class, customer.getId()));
        final String controllerResponse =
                this.customerDetailsController.showCustomerDetails(wrapperObject.getRequest(), wrapperObject.getResponse(), wrapperObject.getModel());
        if (controllerResponse == null || "false".equalsIgnoreCase(controllerResponse)) {
            Assert.fail();
        }
    }
    
    private void assertCusterDetailProperties() {
        final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
        final Map<String, Object> expectedResult = wrapperObject.getExpectedResult();
        final CustomerDetails customerDetails = (CustomerDetails) wrapperObject.getModel().get("customerDetails");
        
        for (String expectedKey : expectedResult.keySet()) {
            switch (expectedKey) {
                case TestConstants.SUCCESS_STRING:
                    break;
                case TestConstants.ACTIVATED_SERVICES:
                    verifyCustomerDetailsServices(expectedResult, customerDetails);
                    break;
                case TestConstants.ACCOUNT_STATUS_STRING:
                    verifyAccountStatus(expectedResult, customerDetails);
                    break;
                case TestConstants.TRIAL_DATE_STRING:
                    verifyTrialDate(expectedResult, customerDetails);
                    break;
                default:
                    Assert.fail(expectedKey + "property not handled in the assertCusterDetailProperties");
                    break;
            }
        }
        
    }
    
    private void verifyTrialDate(final Map<String, Object> expectedResult, final CustomerDetails customerDetails) {
        final String expiryDateLocal = (String) expectedResult.get(TestConstants.TRIAL_DATE_STRING);
        Assert.assertTrue(expiryDateLocal.equalsIgnoreCase(customerDetails.getTrialExpiryLocal().toString()));
    }
    
    private void verifyAccountStatus(final Map<String, Object> expectedResult, final CustomerDetails customerDetails) {
        final String status = (String) expectedResult.get(TestConstants.ACCOUNT_STATUS_STRING);
        if (TestConstants.DISABLED_STRING.equalsIgnoreCase(status)) {
            Assert.assertTrue(customerDetails.getCustomer().getCustomerStatusType().getId() == TestConstants.DISABLED_STATUS);
        } else if (TestConstants.ENABLED_STRING.equalsIgnoreCase(status)) {
            Assert.assertTrue(customerDetails.getCustomer().getCustomerStatusType().getId() == TestConstants.ENABLED_STATUS);
        } else if (TestConstants.TRIAL_STRING.equalsIgnoreCase(status)) {
            Assert.assertTrue(customerDetails.getCustomer().getCustomerStatusType().getId() == TestConstants.TRIAL_STATUS);
        } else {
            Assert.fail("Cannot identify the account status in story file, found status = " + status);
        }
    }
    
    private void verifyCustomerDetailsServices(final Map<String, Object> expectedResult, final CustomerDetails customerDetails) {
        final List<CustomerSubscription> customerSubscriptionsFromModel = customerDetails.getCustomerSubscriptions();
        final String servicesActivated = (String) expectedResult.get(TestConstants.ACTIVATED_SERVICES);
        final List<String> customerSubscriptionsExpected = new ArrayList<String>(Arrays.asList(servicesActivated.split(WebCoreConstants.COMMA)));
        final List<String> customerSubscriptionsExpectedClone = new ArrayList<String>(customerSubscriptionsExpected);
        
        if (TestConstants.NO_STRING.equalsIgnoreCase(servicesActivated)) {
            customerSubscriptionsExpected.clear();
            customerSubscriptionsExpectedClone.clear();
        } else if (TestConstants.ALL_STRING.equalsIgnoreCase(servicesActivated)) {
            customerSubscriptionsExpected.clear();
            customerSubscriptionsExpectedClone.clear();
            for (SubscriptionType sub : TestContext.getInstance().getDatabase().find(SubscriptionType.class)) {
                customerSubscriptionsExpected.add(sub.getName());
                customerSubscriptionsExpectedClone.add(sub.getName());
            }
        }
        Assert.assertEquals(customerSubscriptionsFromModel.size(), customerSubscriptionsExpected.size());
        for (String customerSubscriptionString : customerSubscriptionsExpected) {
            final Integer subscriptionId = resolveSubsriptionTypeId(customerSubscriptionString.trim());
            if (subscriptionId == null) {
                continue;
            }
            for (CustomerSubscription customerSubscription : customerSubscriptionsFromModel) {
                if (customerSubscription.getSubscriptionType().getId() == subscriptionId) {
                    customerSubscriptionsExpectedClone.remove(customerSubscriptionString);
                }
            }
        }
        if (customerSubscriptionsExpectedClone.isEmpty()) {
            System.out.println("Following subscriptions couldnot be verified" + customerSubscriptionsExpectedClone);
        }
        Assert.assertTrue(customerSubscriptionsExpectedClone.isEmpty());
        
    }

    @Override
    public final Object performAction(final Object... objects) {
        loadCustomerDetails();
        return null;
    }

    @Override
    public final Object assertSuccess(final Object... objects) {
        assertCusterDetailProperties();
        return null;
    }

    @Override
    public final Object assertFailure(final Object... objects) {
        return null;
    }
    
    private Integer resolveSubsriptionTypeId(final String subscriptionTypeName) {
        final EntityDB db = TestContext.getInstance().getDatabase();
        return (Integer) db.session().createQuery("select id from SubscriptionType where lower(name) = ?").setString(0, subscriptionTypeName.toLowerCase()).uniqueResult();
    }
    
}
