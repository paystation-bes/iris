package com.digitalpaytech.bdd.mvc.devicegroup;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class GroupAutoCompleteStories extends AbstractStories {
    
    public GroupAutoCompleteStories() {
        super();
        this.testHandlers.registerTestHandler("groupisclickedfortestgroupvisit", TestGroupDetails.class);
        this.testHandlers.registerTestHandler("groupdetailsresponsecorrect", TestGroupDetails.class);
        this.testHandlers.registerTestHandler("autocompletetype", TestGroupAutoComplete.class);
        this.testHandlers.registerTestHandler("autocompleteinputreturns", TestGroupAutoComplete.class);
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new ControllerBaseSteps(this.testHandlers));
    }
    
}
