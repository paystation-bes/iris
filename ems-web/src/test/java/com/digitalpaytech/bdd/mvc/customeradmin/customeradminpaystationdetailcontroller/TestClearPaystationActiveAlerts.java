package com.digitalpaytech.bdd.mvc.customeradmin.customeradminpaystationdetailcontroller;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.mvc.PayStationDetailController;
import com.digitalpaytech.mvc.customeradmin.CustomerAdminPayStationDetailController;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.util.WebCoreConstants;

public class TestClearPaystationActiveAlerts implements JBehaveTestHandler {
    @Mock
    private PayStationDetailController payStationDetailController;
    @Mock
    private CommonControllerHelper commonControllerHelper;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private CustomerAdminPayStationDetailController customeradminpaystationController;
    
    public TestClearPaystationActiveAlerts() {
        MockitoAnnotations.initMocks(this);
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    
    private String clearPaystationActiveAlerts() {
        String controllerResponse;
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final MockHttpServletRequest request = controllerFields.getRequest();
        final MockHttpServletResponse response = controllerFields.getResponse();
        
        when(this.payStationDetailController.clearActiveAlert(request, response)).thenReturn(WebCoreConstants.RESPONSE_TRUE);
        when(this.commonControllerHelper.clearActiveAlert(request, response, false)).thenReturn(WebCoreConstants.RESPONSE_TRUE);
        controllerResponse = this.customeradminpaystationController.clearStationDetailsAlert(request, response);
        final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
        wrapperObject.setControllerResponse(controllerResponse);
        return controllerResponse;
        
    }
    
    private void assertClearPayStationAlerts() {
        final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
        assertTrue(Boolean.parseBoolean(wrapperObject.getControllerResponse()));
    }
    
    private void activeAlertIsNotClearable() {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        assertFalse(Boolean.parseBoolean(controllerFields.getControllerResponse()));
    }
    
    @Override
    public final Object performAction(final Object... objects) {
        clearPaystationActiveAlerts();
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... objects) {
        assertClearPayStationAlerts();
        return null;
    }
    
    @Override
    public final Object assertFailure(final Object... objects) {
        activeAlertIsNotClearable();
        return null;
    }
    
}
