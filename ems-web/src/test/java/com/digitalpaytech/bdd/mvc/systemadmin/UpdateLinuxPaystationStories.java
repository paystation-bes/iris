package com.digitalpaytech.bdd.mvc.systemadmin;

import java.util.Arrays;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.mvc.systemadmin.systemadminpaystationlistdetailcontroller.TestSavePOS;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.RibbonClientBaseSteps;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.client.AuthClient;
import com.digitalpaytech.client.TelemetryClient;
import com.digitalpaytech.mvc.systemadmin.support.SysAdminPayStationEditForm;
import com.digitalpaytech.util.kafka.KafkaBaseSteps;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class UpdateLinuxPaystationStories extends AbstractStories {
    
    public UpdateLinuxPaystationStories() {
        this.testHandlers.registerTestHandler("paystationsave", TestSavePOS.class);
        TestContext.getInstance().getObjectParser()
                .register(new Class[] { SysAdminPayStationEditForm.class, AuthClient.class, TelemetryClient.class });
        
        configuredEmbedder().useMetaFilters(Arrays.asList("-skip", "-ignored"));
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        // varargs, can have more that one steps classes
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new ControllerBaseSteps(this.testHandlers),
                new RibbonClientBaseSteps(this.testHandlers), new KafkaBaseSteps(this.testHandlers));
    }
    
}
