package com.digitalpaytech.bdd.mvc.customer;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.mvc.customeradmin.transactionreceiptcontroller.TestSearchPermits;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.mvc.customeradmin.support.TransactionReceiptSearchForm;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class PermitLookupStories extends AbstractStories {
    
    public PermitLookupStories() {
        super();
        this.testHandlers.registerTestHandler("searchbuttonclick", TestSearchPermits.class);
        this.testHandlers.registerTestHandler("resultsuccessful", TestSearchPermits.class);
        this.testHandlers.registerTestHandler("resultnotsuccessful", TestSearchPermits.class);
        TestLookupTables.getExpressionParser().register(new Class[] { TransactionReceiptSearchForm.class });
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        // varargs, can have more that one steps classes
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new ControllerBaseSteps(this.testHandlers));
    }
    
}
