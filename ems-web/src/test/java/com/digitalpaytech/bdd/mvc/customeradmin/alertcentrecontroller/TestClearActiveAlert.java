package com.digitalpaytech.bdd.mvc.customeradmin.alertcentrecontroller;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.customeradmin.AlertCentreController;
import com.digitalpaytech.mvc.support.impl.CommonControllerHelperImpl;
import com.digitalpaytech.scheduling.queue.alert.PosEventCurrentProcessor;
import com.digitalpaytech.service.impl.AlertsServiceImpl;
import com.digitalpaytech.service.impl.CustomerAdminServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.LocationServiceImpl;
import com.digitalpaytech.service.impl.MobileLicenseServiceImpl;
import com.digitalpaytech.service.impl.MobileSessionTokenServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.PosAlertStatusServiceImpl;
import com.digitalpaytech.service.impl.PosEventCurrentServiceImpl;
import com.digitalpaytech.service.impl.PosEventHistoryServiceImpl;
import com.digitalpaytech.service.impl.ReportDefinitionServiceImpl;
import com.digitalpaytech.service.impl.RouteServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.service.queue.QueueEventService;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.customeradmin.AlertCentreFilterValidator;

public class TestClearActiveAlert implements JBehaveTestHandler {
    
    @Mock
    private AlertCentreFilterValidator alertCentreFilterValidator;
    
    @InjectMocks
    private EntityDaoTest entityDaoTest;
    
    @InjectMocks
    private MobileSessionTokenServiceImpl mobileSessionTokenService;
    
    @InjectMocks
    private MobileLicenseServiceImpl mobileAppService;
    
    @InjectMocks
    private LocationServiceImpl locationService;
    
    @InjectMocks
    private RouteServiceImpl routeService;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    private AlertsServiceImpl alertsService;
    
    @InjectMocks
    private CustomerAdminServiceImpl customerAdminService;
    
    @InjectMocks
    private CommonControllerHelperImpl commonControllerHelper;
    
    @InjectMocks
    private ReportDefinitionServiceImpl reportDefinitionService;
    
    @InjectMocks
    private PosAlertStatusServiceImpl posAlertStatusService;
    
    @InjectMocks
    private PosEventCurrentServiceImpl posEventCurrentService;
    
    @InjectMocks
    private PosEventHistoryServiceImpl posEventHistoryService;
    
    @InjectMocks
    private MessageHelper messageHelper;
    
    @InjectMocks
    private UserAccountServiceImpl userAccountService;
    
    @InjectMocks
    private AlertCentreController alertsCentreController;
    
    @Mock
    private PosEventCurrentProcessor posEventCurrentProcessor;
    
    @Mock
    private QueueEventService queueEventService;
    
    public TestClearActiveAlert() {
        MockitoAnnotations.initMocks(this);
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    
    private void clearActiveAlert() {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final MockHttpServletRequest request = controllerFields.getRequest();
        final MockHttpServletResponse response = controllerFields.getResponse();
        final ModelMap model = controllerFields.getModel();
        final WebSecurityUtil util = new WebSecurityUtil();
        
        final PosEventCurrent activeAlert = TestContext.getInstance().getDatabase().find(PosEventCurrent.class).iterator().next();
        
        request.setParameter("activePosAlertId",
                             SessionTool.getInstance(request).getKeyMapping().getRandomString(PosEventCurrent.class, activeAlert.getId()));
        
        util.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                return (UserAccount) TestDBMap.findObjectByIdFromMemory(userAccountId, UserAccount.class);
            }
            
            public UserAccount findUndeletedUserAccount(final String encodedUserName) {
                return new UserAccount();
            }
        });
        
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(final Object entity) {
                return entity;
            }
        });
        controllerFields.setControllerResponse(this.alertsCentreController.clearActiveAlert(request, response, model));
    }
    
    @Override
    public final Object performAction(final Object... objects) {
        clearActiveAlert();
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... objects) {
        return null;
    }
    
    @Override
    public final Object assertFailure(final Object... objects) {
        return null;
    }
    
}
