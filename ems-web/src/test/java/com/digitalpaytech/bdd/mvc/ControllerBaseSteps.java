package com.digitalpaytech.bdd.mvc;

import static org.junit.Assert.fail;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandlers;
import com.digitalpaytech.bdd.util.TestConstants;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.bdd.util.expression.StoryObjectBuilder;
import com.digitalpaytech.bdd.util.expression.ValidationResult;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerPropertyType;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.CustomerType;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.RoleStatusType;
import com.digitalpaytech.domain.TimezoneVId;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.systemadmin.support.CustomerEditForm;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

import junit.framework.Assert;

@SuppressWarnings({ "PMD.GodClass", "PMD.ExcessiveImports", "PMD.UseConcurrentHashMap", "PMD.BeanMembersShouldSerialize",
    "PMD.TooFewBranchesForASwitchStatement", "PMD.TooManyMethods" })
public final class ControllerBaseSteps extends AbstractSteps {
    
    private static final String PASSWORD = "password";
    private static final String WHITE_SPACE_REGEX = "\\s";
    private static final String CARD_MANAGEMENT = "Card Management";
    private static final String MANAGE_BANNED_CARDS = "Manage Banned Cards";
    private static final String EDIT_EXISTING_CUSTOMER = "Edit Existing Customer";
    private static final String DPT_CUSTOMER_ADMINISTRATION = "DPT Customer Administration";
    private static final String CREATE_NEW_CUSTOMER = "Create New Customer";
    private static final String CREATE_NEW_MERCHANT_ACCOUNT = "Create New Merchant Account";
    private static final String VIEW_MAINTENANCE_CENTER = "View Maintenance Center";
    private static final String CLEAR_ACTIVE_ALERTS = "Clear Active Alerts";
    private static final String SALT = "salt";
    private BindingResult result;
    
    public ControllerBaseSteps(final JBehaveTestHandlers testHandlers) {
        super(testHandlers);
    }
    
    protected Class<?>[] getStoryObjectTypes() {
        
        return new Class[] { Customer.class, CustomerEditForm.class, UserAccount.class, TimezoneVId.class, Location.class, };
    }
    
    @BeforeScenario
    public void setUpForController() {
        final ControllerFieldsWrapper controllerFields = new ControllerFieldsWrapper();
        controllerFields.setRequest(new MockHttpServletRequest());
        controllerFields.setResponse(new MockHttpServletResponse());
        controllerFields.setModel(new ModelMap());
        controllerFields.setSession(new MockHttpSession());
        controllerFields.getRequest().setSession(controllerFields.getSession());
        // Force initialize the SessionTool
        SessionTool.getInstance(controllerFields.getRequest());
        controllerFields.setResult(this.result);
        TestDBMap.saveControllerFieldsWrapperToMem(controllerFields);
        TestLookupTables.getExpressionParser().register(getStoryObjectTypes());
    }
    
    /**
     * Retrieves ControllerFieldsWrapper from database, sets expected data and replaces it in the database
     *
     * 
     * @.example Then the *profile* is shown in the *customer profile* screen where the<br>
     *           *Customer name is Mackenzie Parking<br>
     *           Time zone is Canada/Pacific and<br>
     *           Status is enabled*
     * @param objectName
     *            the name of the object being acted on
     * @param target
     *            the name of the location a user would view
     * @param data
     *            the data string
     */
    @Then("the $objectName is shown in the $target screen where the $data")
    public void profileIsShownInTheCustomerProfileScreen(final String objectName, final String target, final String data) {
        final Map<String, Object> dataMap = parseAttributes(data);
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        controllerFields.setExpectedResult(dataMap);
        testHandlers.invokeAssertSuccessMethod(objectName, target);
    }
    
    /**
     * Creates a new user of the type and userName
     * 
     * @.example Given there is a *SystemAdmin* user *Bob*
     * @param type
     *            the type of user to create
     * @param userName
     *            new user's userName
     */
    @Given("there is a $type user $userName")
    @SuppressWarnings({ "checkstyle:cyclomaticcomplexity", "checkstyle:multiplestringliterals" })
    public void userWithPermission(final String type, final String userName) {
        int customerTypeId = WebCoreConstants.CUSTOMER_TYPE_CHILD;
        switch (type.toLowerCase(WebCoreConstants.DEFAULT_LOCALE)) {
            case "system":
            case "systemadmin":
            case "system admin":
                customerTypeId = WebCoreConstants.CUSTOMER_TYPE_DPT;
                break;
            case "child":
            case "child admin":
                customerTypeId = WebCoreConstants.CUSTOMER_TYPE_CHILD;
                break;
            case "parent":
            case "parent admin":
                customerTypeId = WebCoreConstants.CUSTOMER_TYPE_PARENT;
                break;
            default:
                //Check if the user has entered the customer name
                final Customer customer = (Customer) TestDBMap.findObjectByFieldFromMemory(type, Customer.ALIAS_NAME, Customer.class);
                if (customer != null && customer.isIsParent()) {
                    createUserAccount(customer, userName);
                    final ArrayList<Customer> childCustomers = new ArrayList<Customer>();
                    final Integer parentId = customer.getId();
                    for (Customer cust : (Collection<Customer>) TestDBMap.getTypeListFromMemory(Customer.class)) {
                        final Customer parentCustomer = cust.getParentCustomer();
                        final Integer pId = parentCustomer == null ? null : parentCustomer.getId();
                        if (pId != null && pId.equals(parentId)) {
                            childCustomers.add(cust);
                            createUserAccount(cust, userName + "child");
                            createAliasUserAccount(cust, userName);
                        }
                    }
                }
                return;
        }
        
        final CustomerType customerType = new CustomerType();
        customerType.setId(customerTypeId);
        final Customer customer = new Customer(customerTypeId);
        customer.setCustomerType(customerType);
        customer.setName(userName);
        customer.setIsMigrated(true);
        
        final UserAccount userAccount = new UserAccount();
        userAccount.setId(customerTypeId);
        userAccount.setUserName(userName);
        userAccount.setPassword(PASSWORD);
        userAccount.setPasswordSalt(SALT);
        userAccount.setCustomer(customer);
        
        TestDBMap.addToMemory(userAccount.getId(), userAccount);
        
        if (customerTypeId == WebCoreConstants.CUSTOMER_TYPE_CHILD || customerTypeId == WebCoreConstants.CUSTOMER_TYPE_PARENT) {
            TestDBMap.addToMemory(customer.getId(), customer);
        }
    }
    
    private void createUserAccount(final Customer customer, final String userName) {
        final UserAccount userAccount = new UserAccount();
        userAccount.setId(new Double(Math.random() * StandardConstants.CONSTANT_100).intValue());
        userAccount.setUserName(userName);
        userAccount.setPassword(PASSWORD);
        userAccount.setPasswordSalt(SALT);
        userAccount.setCustomer(customer);
        TestDBMap.addToMemory(userAccount.getId(), userAccount);
    }
    
    private void createAliasUserAccount(final Customer childCustomer, final String userName) {
        final UserAccount userAccount = new UserAccount();
        userAccount.setId(new Double(Math.random() * StandardConstants.CONSTANT_100).intValue());
        userAccount.setUserName(userName + childCustomer.getId());
        userAccount.setPassword("NOTUSED");
        userAccount.setIsAliasUser(true);
        userAccount.setPasswordSalt(SALT);
        userAccount.setCustomer(childCustomer);
        
        for (UserAccount account : (Collection<UserAccount>) TestDBMap.getTypeListFromMemory(UserAccount.class)) {
            final int customerAccountId = account.getCustomer().getId();
            if (customerAccountId == childCustomer.getParentCustomer().getId()) {
                userAccount.setUserAccount((UserAccount) TestDBMap.findObjectByIdFromMemory(account.getId(), UserAccount.class));
            }
        }
        
        TestDBMap.addToMemory(userAccount.getId(), userAccount);
    }
    
    /**
     * Gives user with userName a new permission
     * 
     * @.example Given *Bob* has permission to *add a customer*
     * @param userName
     *            the target user's name
     * @param permission
     *            permission to give the user
     */
    @Given("$userName has permission to $permission")
    public void createPermissions(final String userName, final String permission) {
        final UserAccount userAccount = (UserAccount) TestDBMap.findObjectByFieldFromMemory(userName, UserAccount.ALIAS_USER_NAME, UserAccount.class);
        
        if (userAccount != null) {
            final Role role;
            if (userAccount.getRoles() == null) {
                userAccount.setRoles(new HashSet<Role>());
                final Role newRole = new Role(
                        new RoleStatusType(WebCoreConstants.ROLE_STATUS_TYPE_ENABLED, TestConstants.ENABLED_STRING, new Date(),
                                WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID),
                        userAccount.getCustomer(), userAccount.getCustomer().getCustomerType(), userAccount, TestConstants.ROLE_STRING, false, false,
                        new Date());
                
                userAccount.getRoles().add(newRole);
                role = newRole;
                role.setRolePermissions(new HashSet<RolePermission>());
            } else {
                final Iterator<Role> iterator = userAccount.getRoles().iterator();
                if (iterator.hasNext()) {
                    role = iterator.next();
                } else {
                    final Role newRole = new Role(
                            new RoleStatusType(WebCoreConstants.ROLE_STATUS_TYPE_ENABLED, TestConstants.ENABLED_STRING, new Date(),
                                    WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID),
                            userAccount.getCustomer(), userAccount.getCustomer().getCustomerType(), userAccount, TestConstants.ROLE_STRING, false,
                            false, new Date());
                    
                    userAccount.getRoles().add(newRole);
                    role = newRole;
                    role.setRolePermissions(new HashSet<RolePermission>());
                }
            }
            addPermissionToRole(userAccount, role, permission.trim().toLowerCase(Locale.getDefault()));
            
        }
    }
    
    /**
     * Logs in the use user given in userName
     * 
     * @.example Given I logged in as *Bob*
     * @.example And I logged in as *Bob*
     * @param userName
     *            the user to log in as
     */
    @Given("I logged in as $userName")
    @SuppressWarnings({ "PMD.AvoidInstantiatingObjectsInLoops", "checkstyle:cyclomaticcomplexity" })
    public void createAuthentication(final String userName) {
        final UserAccount userAccount = (UserAccount) TestDBMap.findObjectByFieldFromMemory(userName, UserAccount.ALIAS_USER_NAME, UserAccount.class);
        if (userAccount != null) {
            final Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
            authorities.add(new SimpleGrantedAuthority("Admin"));
            final WebUser user = new WebUser(userAccount.getCustomer().getId(), userAccount.getId(), userAccount.getUserName(), "Password$1", SALT,
                    true, authorities);
            user.setIsComplexPassword(true);
            user.setIsSystemAdmin(userAccount.getCustomer().getCustomerType().getId() == WebCoreConstants.CUSTOMER_TYPE_DPT);
            user.setIsParent(userAccount.getCustomer().getCustomerType().getId() == WebCoreConstants.CUSTOMER_TYPE_PARENT);
            user.setIsMigrated(userAccount.getCustomer().isIsMigrated());
            final Authentication authentication = new UsernamePasswordAuthenticationToken(user, userAccount.getPassword(), authorities);
            final Collection<Integer> permissionList = new ArrayList<Integer>();
            
            final List<CustomerSubscription> subscriptions = TestContext.getInstance().getDatabase().find(CustomerSubscription.class);
            
            final Set<Integer> customerSubIds = new HashSet<>();
            for (CustomerSubscription sub : subscriptions) {
                if (sub.getCustomer().getId().equals(userAccount.getCustomer().getId())) {
                    customerSubIds.add(sub.getSubscriptionType().getId());
                }
            }
            
            user.setCustomerSubscriptions(customerSubIds);
            final Set<Role> roles = userAccount.getRoles();
            for (Role role : roles) {
                for (RolePermission rolePermission : role.getRolePermissions()) {
                    permissionList.add(rolePermission.getPermission().getId());
                }
            }
            final Customer customer = userAccount.getCustomer();
            final Collection<CustomerProperty> customerProperties =
                    (Collection<CustomerProperty>) TestContext.getInstance().getDatabase().find(CustomerProperty.class);
            if (customerProperties != null) {
                for (CustomerProperty customerProperty : customerProperties) {
                    if (customerProperty.getCustomerPropertyType() == null) {
                        customerProperty.setCustomerPropertyType(new CustomerPropertyType());
                    }
                    if (customerProperty.getCustomerPropertyType().getId() == StandardConstants.CONSTANT_1
                        && customer.getId().equals(customerProperty.getCustomer().getId())) {
                        user.setCustomerTimeZone(customerProperty.getPropertyValue());
                    }
                }
                if (customerProperties.isEmpty()) {
                    user.setCustomerTimeZone(WebCoreConstants.GMT);
                }
            }
            user.setUserPermissions(permissionList);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
    }
    
    protected void completeFormAndAddToSession(final Serializable newForm, final List<FormField> formFieldList, final String formName) {
        try {
            final Serializable form = newForm;
            for (FormField formField : formFieldList) {
                final Method setterMethod = form.getClass().getDeclaredMethod(formField.getSetterMethodName(), formField.getParameterType());
                setterMethod.invoke(form, formField.getValue());
                
            }
            this.result = new BeanPropertyBindingResult(form, formName);
            final ControllerFieldsWrapper controllerFields = (ControllerFieldsWrapper) TestDBMap
                    .findObjectByIdFromMemory(TestDBMap.CONTROLLER_FIELDS_WRAPPER_ID, ControllerFieldsWrapper.class);
            controllerFields.setForm(form);
            TestDBMap.addToMemory(TestDBMap.CONTROLLER_FIELDS_WRAPPER_ID, controllerFields);
        } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            fail(e.getMessage());
        }
    }
    
    /**
     * Invokes method to switch from one company to another company which are under the same parent company.
     * 
     * @.example I switch from company AirPort Parking to Downtown Parking
     * @param nameX
     *            the name of the company to switch from
     * @param nameY
     *            the name of the company to switch to
     */
    @When("I switch from company $nameX to $nameY")
    public void companySwitcher(final String nameX, final String nameY) {
        testHandlers.invokePerformMethod("company", "switch", nameX + ":" + nameY);
    }
    
    /**
     * Asserts searching in the given location returns the given string
     * 
     * @.example Then the *customer search bar auto complete* returns *Mackenzie Parking*
     * @param searchLocation
     *            location that the search happened in
     * @param expectedString
     *            string that is expected to be returned
     */
    @Then("the $searchLocation returns $expectedString")
    public void stringIsShownInTheAutoComplete(final String searchLocation, final String expectedString) {
        testHandlers.invokeAssertSuccessMethod(searchLocation, "returns", expectedString);
    }
    
    protected Map<String, Object> parseAttributes(final String data) {
        
        String tempData = data.replaceAll("and", "");
        if (!data.contains(TestConstants.FORM_FIELD_SEPERATOR_PLURAL)) {
            tempData = tempData.replaceAll(",", "");
        }
        tempData = tempData.replaceAll("\\.", "");
        final String[] lines = tempData.split(StandardConstants.REGEX_NEWLINE);
        final Map<String, Object> dataMap = new HashMap<String, Object>(lines.length);
        for (int i = 0; i < lines.length; i++) {
            if (lines[i].indexOf(TestConstants.FORM_FIELD_SEPERATOR) > 0) {
                final String[] labelValuePair = lines[i].split(TestConstants.FORM_FIELD_SEPERATOR);
                dataMap.put(labelValuePair[0].trim().toLowerCase(Locale.getDefault()), labelValuePair[1].trim());
            } else if (lines[i].indexOf(TestConstants.FORM_FIELD_SEPERATOR_PLURAL) > 0) {
                final String[] labelValuePair = lines[i].split(TestConstants.FORM_FIELD_SEPERATOR_PLURAL);
                dataMap.put(labelValuePair[0].trim().toLowerCase(Locale.getDefault()), labelValuePair[1].trim());
            } else if (lines[i].toLowerCase(WebCoreConstants.DEFAULT_LOCALE).indexOf(TestConstants.FORM_FIELD_BOOLEAN_TRUE) >= 0) {
                dataMap.put(lines[i].substring(TestConstants.FORM_FIELD_BOOLEAN_TRUE.length()).trim().toLowerCase(Locale.getDefault()), true);
            } else if (lines[i].toLowerCase(WebCoreConstants.DEFAULT_LOCALE).indexOf(TestConstants.FORM_FIELD_BOOLEAN_FALSE) >= 0) {
                dataMap.put(lines[i].substring(TestConstants.FORM_FIELD_BOOLEAN_FALSE.length()).trim().toLowerCase(Locale.getDefault()), false);
            }
        }
        return dataMap;
    }
    
    /**
     * Invokes method of concatenated action and object name parameters with the given data
     * 
     * @.example When the *subscription* is *saved* for created *customer*
     * 
     * @param action
     *            action that is taken
     * @param objectName
     *            object that is acted on
     * @param data
     *            object that was previously created
     */
    @When("the $objectName is $action for created $data")
    public void assertCustomerDetails(final String action, final String objectName, final String data) {
        final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
        wrapperObject.setData(data);
        testHandlers.invokePerformMethod(objectName, action);
    }
    
    /**
     * Invokes method of concatenated action and object name parameters with the given data
     * Asserts that an action was taken successfully or not
     * 
     * @.example Then the *customer* is *saved* successfully where the<br>
     *           *Customer name is Mackenzie Parking<br>
     *           Is not a Parent Company<br>
     *           Current Time Zone is Canada/Pacific<br>
     *           Account Status is enabled*<br>
     * @param action
     *            action that is taken, include the word not to assert that an action was not successful
     * @param objectName
     *            object that is acted on
     * @param data
     *            data to pass to the invoked method
     */
    @Then("the $objectName is $action successfully where the $data")
    public void assertActionSuccess(final String objectName, final String action, final String data) {
        final Boolean success = action.contains(TestConstants.NOT) ? false : true;
        final Map<String, Object> dataMap = parseAttributes(data);
        dataMap.put(TestConstants.SUCCESS_STRING, success);
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        controllerFields.setExpectedResult(dataMap);
        testHandlers.invokeAssertSuccessMethod(objectName, action);
    }
    
    /**
     * Asserts that an ui screen is displaying the correct data or not
     * 
     * @.example Then there exists a *pay station transaction report detail* screen where the<br>
     *           *date purchased is Feb 01, 2015 10:00 AM<br>
     *           cash paid is 10.0<br>
     *           charged amount is 3.0<br>
     *           excess payment is 7.0<br>
     *           refund slip is 0<br>
     *           payment type is cash<br>
     *           location is downtown<br>
     *           pay station name is 500000070001*
     * 
     * @param ui
     *            the object in model map
     *
     * @param data
     *            data to pass to the invoked method
     */
    
    @Then("there exists a $ui screen where the $data")
    public void assertDisplayUI(final String ui, final String data) {
        final StoryObjectBuilder objBuilder = TestContext.getInstance().getObjectBuilder();
        final String code = objBuilder.locate(ui).getCode();
        final Object objInModel = TestDBMap.getControllerFieldsWrapperFromMem().getModel().get(code);
        
        final ValidationResult res = TestContext.getInstance().getObjectParser().validate(data, objInModel);
        if (res.isAllPassed()) {
            Assert.assertTrue(ui + "'s data sucessfully passed", true);
        } else {
            fail(res.toString());
        }
    }
    
    /**
     * Invokes method of concatenated action and object name parameters with the given data
     * 
     * @.example Then I *receive* validation errors *password not complex, customer legal name is required, account status is invalid, customer user name is
     *           required* and the *customer* is *not saved*
     * @param action
     *            action that is taken, include the word not to assert that an action was not successful
     * @param objectName
     *            object that is acted on
     * @param status
     *            status if the action is successful or not
     */
    @SuppressWarnings("PMD.UseObjectForClearerAPI")
    @Then(value = "I $action validation errors $data and the $objectName is $status", priority = 1)
    public void assertValidationErrors(final String action, final String data, final String objectName, final String status) {
        final Boolean success = status.contains(TestConstants.NOT) ? false : true;
        final Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put(TestConstants.SUCCESS_STRING, success);
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        controllerFields.setData(data.trim());
        controllerFields.setExpectedResult(dataMap);
        testHandlers.invokeAssertFailureMethod(objectName, action);
    }
    
    /**
     * Asserts the data passed with the response from the controller
     * 
     * @.example Then the response received is *false:Empty response.*
     * 
     * @param data
     *            the entity compared to the response from the controller
     */
    @Then("the response received is $data")
    public void assertControllerResponse(final String data) {
        final ControllerFieldsWrapper controllerFieldsWrapper = TestDBMap.getControllerFieldsWrapperFromMem();
        final String expected = controllerFieldsWrapper.getControllerResponse();
        Assert.assertEquals(data.toLowerCase(Locale.getDefault()).replaceAll(WHITE_SPACE_REGEX, ""),
                            expected.toLowerCase(Locale.getDefault()).replaceAll(WHITE_SPACE_REGEX, ""));
    }
    
    /**
     * Invokes method of concatenated action and object name parameters with the given searchString
     * Intended for entering data into input fields
     * 
     * @.example When I *type* string *Mackenzie Parking* into *customer search bar* input
     * 
     * @param action
     *            action that is taken
     * @param searchString
     *            string to search for
     * @param objectName
     *            object that is acted on
     * 
     */
    @When("I $action string $searchString into $objectName input")
    public void enterSearchString(final String action, final String searchString, final String objectName) {
        testHandlers.invokePerformMethod(objectName, action, searchString);
    }
    
    @SuppressWarnings({ "checkstyle:methodlength", "checkstyle:multiplestringliterals", "checkstyle:cyclomaticcomplexity", "PMD.NcssMethodCount" })
    private void addPermissionToRole(final UserAccount userAccount, final Role role, final String permissionString) {
        final RolePermission rolePermission = new RolePermission();
        final Permission permission;
        final Permission parentPermission;
        final RolePermission parentRolePermission = new RolePermission();
        rolePermission.setUserAccount(userAccount);
        rolePermission.setRole(role);
        switch (permissionString) {
            case "add a customer":
                permission = new Permission(WebSecurityConstants.PERMISSION_CREATE_NEW_CUSTOMER, userAccount, CREATE_NEW_CUSTOMER, false, new Date());
                
                parentPermission = new Permission(WebSecurityConstants.PERMISSION_DPT_CUSTOMER_ADMINISTRATION, userAccount,
                        DPT_CUSTOMER_ADMINISTRATION, true, new Date());
                rolePermission.setPermission(permission);
                parentRolePermission.setPermission(parentPermission);
                break;
            case "edit a customer":
                permission = new Permission(WebSecurityConstants.PERMISSION_EDIT_EXISTING_CUSTOMER, userAccount, EDIT_EXISTING_CUSTOMER, false,
                        new Date());
                
                parentPermission = new Permission(WebSecurityConstants.PERMISSION_DPT_CUSTOMER_ADMINISTRATION, userAccount,
                        DPT_CUSTOMER_ADMINISTRATION, true, new Date());
                rolePermission.setPermission(permission);
                parentRolePermission.setPermission(parentPermission);
                break;
            case "view the dashboard":
                permission = new Permission(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD, userAccount, CREATE_NEW_CUSTOMER, false, new Date());
                
                parentPermission = new Permission(WebSecurityConstants.PERMISSION_DASHBOARD_MANAGEMENT, userAccount, DPT_CUSTOMER_ADMINISTRATION,
                        true, new Date());
                
                rolePermission.setPermission(permission);
                parentRolePermission.setPermission(parentPermission);
                break;
            case "manage the dashboard":
                permission =
                        new Permission(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD, userAccount, "View Widget Master List", false, new Date());
                
                parentPermission = new Permission(WebSecurityConstants.PERMISSION_DASHBOARD_MANAGEMENT, userAccount, DPT_CUSTOMER_ADMINISTRATION,
                        true, new Date());
                
                rolePermission.setPermission(permission);
                parentRolePermission.setPermission(parentPermission);
                break;
            case "view current alerts":
                permission = new Permission(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS, userAccount, "View Current Alerts", false,
                        new Date());
                
                parentPermission =
                        new Permission(WebSecurityConstants.PERMISSION_ALERTS_MANAGEMENT, userAccount, "Alerts Management", true, new Date());
                
                rolePermission.setPermission(permission);
                parentRolePermission.setPermission(parentPermission);
                break;
            case "manage user defined alerts":
                permission = new Permission(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT, userAccount, "Manage User Defined Alerts",
                        false, new Date());
                
                parentPermission =
                        new Permission(WebSecurityConstants.PERMISSION_ALERTS_MANAGEMENT, userAccount, "Alerts Management", true, new Date());
                
                rolePermission.setPermission(permission);
                parentRolePermission.setPermission(parentPermission);
                break;
            case "add a merchant account":
                permission = new Permission(WebSecurityConstants.PERMISSION_CREATE_NEW_CUSTOMER, userAccount, CREATE_NEW_MERCHANT_ACCOUNT, false,
                        new Date());
                
                parentPermission = new Permission(WebSecurityConstants.PERMISSION_DPT_CUSTOMER_ADMINISTRATION, userAccount,
                        DPT_CUSTOMER_ADMINISTRATION, true, new Date());
                rolePermission.setPermission(permission);
                parentRolePermission.setPermission(parentPermission);
                break;
            case "add banned cards":
                permission = new Permission(WebSecurityConstants.PERMISSION_MANAGE_BANNED_CARDS, userAccount, MANAGE_BANNED_CARDS, false, new Date());
                
                parentPermission = new Permission(WebSecurityConstants.PERMISSION_CC_SMARTCARD_PROCESSING_MANAGEMENT, userAccount, CARD_MANAGEMENT,
                        true, new Date());
                
                rolePermission.setPermission(permission);
                parentRolePermission.setPermission(parentPermission);
                break;
            case "view maintenance center":
                permission = new Permission(WebSecurityConstants.PERMISSION_VIEW_MAINTENANCE_CENTER, userAccount, VIEW_MAINTENANCE_CENTER, false,
                        new Date());
                parentPermission =
                        new Permission(WebSecurityConstants.PERMISSION_ALERTS_MANAGEMENT, userAccount, DPT_CUSTOMER_ADMINISTRATION, true, new Date());
                rolePermission.setPermission(permission);
                parentRolePermission.setPermission(parentPermission);
                break;
            case "clear active alerts":
                permission = new Permission(WebSecurityConstants.PERMISSION_CLEAR_ACTIVE_ALERTS, userAccount, CLEAR_ACTIVE_ALERTS, false, new Date());
                parentPermission =
                        new Permission(WebSecurityConstants.PERMISSION_ALERTS_MANAGEMENT, userAccount, DPT_CUSTOMER_ADMINISTRATION, true, new Date());
                rolePermission.setPermission(permission);
                parentRolePermission.setPermission(parentPermission);
                break;
            case "issue refunds":
                permission = new Permission(WebSecurityConstants.PERMISSION_ISSUE_REFUNDS, userAccount, "Issue Refunds", false, new Date());
                parentPermission = new Permission(WebSecurityConstants.PERMISSION_CC_SMARTCARD_PROCESSING_MANAGEMENT, userAccount, CARD_MANAGEMENT,
                        true, new Date());
                rolePermission.setPermission(permission);
                parentRolePermission.setPermission(parentPermission);
                break;
            case "view paystations":
                permission = new Permission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS, userAccount, "View Paystations", false, new Date());
                parentPermission = new Permission(WebSecurityConstants.PERMISSION_PAYSTATION_MANAGEMENT, userAccount, "Pay station Management", true,
                        new Date());
                rolePermission.setPermission(permission);
                parentRolePermission.setPermission(parentPermission);
                break;
            case "view paystation settings":
                permission = new Permission(WebSecurityConstants.PERMISSION_VIEW_PAYSTATION_SETTINGS, userAccount, "View Pay Station Settings", false,
                        new Date());
                parentPermission = new Permission(WebSecurityConstants.PERMISSION_PAYSTATION_MANAGEMENT, userAccount, "Pay station Management", true,
                        new Date());
                rolePermission.setPermission(permission);
                parentRolePermission.setPermission(parentPermission);
                break;
            case "manage paystation settings":
                permission = new Permission(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATION_SETTINGS, userAccount, "Manage Pay Station Settings",
                        false, new Date());
                parentPermission = new Permission(WebSecurityConstants.PERMISSION_PAYSTATION_MANAGEMENT, userAccount, "Pay station Management", true,
                        new Date());
                rolePermission.setPermission(permission);
                parentRolePermission.setPermission(parentPermission);
                break;
            case "schedule paystation settings update":
                permission = new Permission(WebSecurityConstants.PERMISSION_SCHEDULE_PAYSTATION_SETTINGS_UPDATE, userAccount,
                        "Schedule Pay Station Settings Update", false, new Date());
                parentPermission = new Permission(WebSecurityConstants.PERMISSION_PAYSTATION_MANAGEMENT, userAccount, "Pay station Management", true,
                        new Date());
                rolePermission.setPermission(permission);
                parentRolePermission.setPermission(parentPermission);
                break;
            case "manage paystations":
                permission =
                        new Permission(WebSecurityConstants.PERMISSION_DPT_MANAGE_PAYSTATION, userAccount, "Manage Paystations", false, new Date());
                parentPermission = new Permission(WebSecurityConstants.PERMISSION_DPT_PAYSTATION_ADMINISTRATION, userAccount,
                        "Pay station Management", true, new Date());
                rolePermission.setPermission(permission);
                parentRolePermission.setPermission(parentPermission);
                break;
            case "manage transaction reports":
                permission = new Permission(WebSecurityConstants.PERMISSION_MANAGE_TRANSACTION_REPORTS, userAccount, "Manage Transaction Reports",
                        false, new Date());
                parentPermission = new Permission(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT, userAccount, "Transaction Reports Management",
                        true, new Date());
                rolePermission.setPermission(permission);
                parentRolePermission.setPermission(parentPermission);
                break;
            case "view device groups":
                permission = new Permission(WebSecurityConstants.PERMISSION_DPT_VIEW_PAYSTATION_GROUPS, userAccount, "DPT View Device Groups", false,
                        new Date());
                parentPermission = new Permission(WebSecurityConstants.PERMISSION_DPT_PAYSTATION_ADMINISTRATION, userAccount,
                        "Pay station Management", true, new Date());
                rolePermission.setPermission(permission);
                parentRolePermission.setPermission(parentPermission);
                break;
                
            case "move paystations":
                permission = 
                        new Permission(WebSecurityConstants.PERMISSION_DPT_MOVE_PAYSTATION, userAccount, "Move Pay Station",
                                false, new Date());
                parentPermission = new Permission(WebSecurityConstants.PERMISSION_DPT_PAYSTATION_ADMINISTRATION, userAccount,
                        "DPT Pay Station Administration", true, new Date());
                rolePermission.setPermission(permission);
                parentRolePermission.setPermission(parentPermission);
                break;
            default:
                break;
        }
        role.getRolePermissions().add(rolePermission);
        role.getRolePermissions().add(parentRolePermission);
        
    }
    
    /**
     * Asserts that json response from the controller is correct
     * 
     * @.example Then the json response is *{ "serialNumber" : "1234" }*
     * 
     * @param data
     *            the expected json string which will be used to compare with controller's response
     */
    @Then("the json response is $expectedJson")
    public void assertControllerJsonResponse(final String expectedJson) {
        final ControllerFieldsWrapper controllerFieldsWrapper = TestDBMap.getControllerFieldsWrapperFromMem();
        TestContext.getInstance().getObjectParser().assertJsonMatched("Controller's json response doesn't matched",
                                                                      controllerFieldsWrapper.getControllerResponse(), expectedJson);
    }
    
    public class FormField {
        private String setterMethodName;
        private Class<?> parameterType;
        private Object value;
        
        public final String getSetterMethodName() {
            return this.setterMethodName;
        }
        
        public final void setSetterMethodName(final String setterMethodName) {
            this.setterMethodName = setterMethodName;
        }
        
        public final Class<?> getParameterType() {
            return this.parameterType;
        }
        
        public final void setParameterType(final Class<?> parameterType) {
            this.parameterType = parameterType;
        }
        
        public final Object getValue() {
            return this.value;
        }
        
        public final void setValue(final Object value) {
            this.value = value;
        }
    }
    
}
