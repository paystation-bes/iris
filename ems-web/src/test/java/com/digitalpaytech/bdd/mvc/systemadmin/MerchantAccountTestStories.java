package com.digitalpaytech.bdd.mvc.systemadmin;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.mvc.systemadmin.merchantaccountcontroller.TestMerchantAccountValidate;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantStatusType;
import com.digitalpaytech.domain.Processor;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class MerchantAccountTestStories extends AbstractStories {
    
    public MerchantAccountTestStories() {
        super();
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
        TestLookupTables.getExpressionParser()
                .register(new Class[] { Customer.class, MerchantAccount.class, Processor.class, MerchantStatusType.class });
        testHandlers.registerTestHandler("merchantaccounttest", TestMerchantAccountValidate.class);
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        // varargs, can have more that one steps classes
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new ControllerBaseSteps(this.testHandlers));
    }
}
