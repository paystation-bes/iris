package com.digitalpaytech.bdd.mvc.customeradmin.cardmanagementcontroller;

import junit.framework.Assert;

import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;

import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestConstants;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.mvc.customeradmin.CardManagementController;
import com.digitalpaytech.mvc.customeradmin.support.BannedCardEditForm;
import com.digitalpaytech.mvc.customeradmin.support.io.CustomerBadCardCsvReader;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.ServiceHelper;
import com.digitalpaytech.service.crypto.CryptoAlgorithmFactory;
import com.digitalpaytech.service.impl.CustomerBadCardServiceImpl;
import com.digitalpaytech.service.impl.CustomerCardTypeServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.service.kpi.KPIListenerService;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.impl.TLSUtilsImpl;
import com.digitalpaytech.validation.customeradmin.CardManagementEditValidator;
import com.digitalpaytech.validation.customeradmin.CardManagementSearchValidator;
import com.digitalpaytech.validation.customeradmin.CardManagementUploadValidator;

public class TestSaveBannedCard implements JBehaveTestHandler {
    
    @Mock
    private CryptoService cryptoService;
    
    @Mock
    private CardManagementSearchValidator cardManagementSearchValidator;
    
    @Mock
    private CardManagementEditValidator cardManagementEditValidator;
    
    @Mock
    private CardManagementUploadValidator uploadValidator;
    
    @Mock
    private CustomerBadCardCsvReader badCreditCardCsvReader;
    
    @Mock
    private EmsPropertiesService emsprops;
    
    @Mock
    private KPIListenerService kpiListenerService;
    
    @Mock
    private ServiceHelper serviceHelper;
    
    @Mock
    private MessageHelper messageHelper;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private CardManagementController cardManagementController;
    
    @InjectMocks
    private CustomerBadCardServiceImpl customerBadCardService;
    
    @InjectMocks
    private CustomerCardTypeServiceImpl customerCardTypeService;
    
    @InjectMocks
    private MerchantAccountServiceImpl merchantAccountService;
    
    @InjectMocks
    private TLSUtilsImpl tlsUtils;
    
    public TestSaveBannedCard() {
        MockitoAnnotations.initMocks(this);
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        
        Mockito.when(this.cardManagementEditValidator.validateMigrationStatus(Matchers.any(WebSecurityForm.class))).thenReturn(true);
    }
    
    private void saveBannedCard() throws CryptoException {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final BannedCardEditForm form = (BannedCardEditForm) controllerFields.getForm();
        final MockHttpServletRequest request = controllerFields.getRequest();
        final MockHttpServletResponse response = controllerFields.getResponse();
        final BindingResult result = controllerFields.getResult();
        
        final WebSecurityUtil util = new WebSecurityUtil();
        
        util.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                return (UserAccount) TestDBMap.findObjectByIdFromMemory(userAccountId, UserAccount.class);
            }
            
            public UserAccount findUndeletedUserAccount(final String encodedUserName) {
                return new UserAccount();
            }
        });
        
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(final Object entity) {
                return entity;
            }
        });
        
        final WebSecurityForm<BannedCardEditForm> webSecurityForm = new WebSecurityForm<BannedCardEditForm>(null, form);
        webSecurityForm.setInitToken(TestConstants.POST_TOKEN);
        webSecurityForm.setPostToken(TestConstants.POST_TOKEN);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        
        controllerFields.setControllerResponse(this.cardManagementController.saveBannedCard(webSecurityForm, result, request, response));
        
    }
    
    @Override
    public Object performAction(Object... objects) {
        try {
            saveBannedCard();
        } catch (CryptoException e) {
            Assert.fail(e.getMessage());
        }
        return null;
    }
    
    @Override
    public Object assertSuccess(Object... objects) {
        return null;
    }
    
    @Override
    public Object assertFailure(Object... objects) {
        return null;
    }
    
}
