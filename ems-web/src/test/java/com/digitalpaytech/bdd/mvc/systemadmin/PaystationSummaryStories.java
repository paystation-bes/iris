package com.digitalpaytech.bdd.mvc.systemadmin;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.AlertSteps;
import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.mvc.customeradmin.alertcentrecontroller.TestClearActiveAlert;
import com.digitalpaytech.bdd.mvc.customeradmin.alertcentrecontroller.TestPaystationSummary;
import com.digitalpaytech.bdd.service.paystation.XMLRPCEventSteps;
import com.digitalpaytech.bdd.service.paystation.impl.eventalertserviceimpl.TestProcessEvent;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class PaystationSummaryStories extends AbstractStories {
    
    public PaystationSummaryStories() {
        super();
        testHandlers.registerTestHandler("paystationsummarypagecheck", TestPaystationSummary.class);
        testHandlers.registerTestHandler("activealertclear", TestClearActiveAlert.class);
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new AlertSteps(this.testHandlers),
                new XMLRPCEventSteps(this.testHandlers), new ControllerBaseSteps(this.testHandlers));
    }
    
}
