package com.digitalpaytech.bdd.mvc.customeradmin.customeradminpaystationdetailcontroller;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.services.CustomerAdminServiceMockImpl;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.customeradmin.CustomerAdminPayStationDetailController;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.impl.CustomerAdminServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.PaymentCardServiceImpl;
import com.digitalpaytech.service.impl.PaymentSmartCardServiceImpl;
import com.digitalpaytech.service.impl.PermitServiceImpl;
import com.digitalpaytech.service.impl.ProcessorTransactionServiceImpl;
import com.digitalpaytech.service.impl.PurchaseServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.WebSecurityUtil;

public class TestTransactionReport implements JBehaveTestHandler {
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private CustomerAdminPayStationDetailController capsdController;

    @Mock
    private CustomerAdminService customerAdminServiceMock;
    
    @InjectMocks
    private PurchaseServiceImpl purchaseService;
    
    @InjectMocks
    private PermitServiceImpl permitService;
    
    @InjectMocks
    private CustomerAdminServiceImpl customerAdminService;
    
    @InjectMocks
    private ProcessorTransactionServiceImpl processorTransactionService;
    
    @InjectMocks
    private PaymentCardServiceImpl paymentCardService;
    
    @InjectMocks
    private PaymentSmartCardServiceImpl paymentSmartCardService;
    
    @Mock
    private CommonControllerHelper commonControllerHelper;
    
    public TestTransactionReport() {
        MockitoAnnotations.initMocks(this);

        Mockito.when(this.customerAdminServiceMock.getCustomerPropertyByCustomerIdAndCustomerPropertyType(Mockito.anyInt(), Mockito.anyInt()))
            .thenAnswer(CustomerAdminServiceMockImpl.createDummyCustomerProperty());
        
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    
    private String showDetailReport() {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final MockHttpServletRequest request = controllerFields.getRequest();
        final MockHttpServletResponse response = controllerFields.getResponse();
        final ModelMap model = controllerFields.getModel();
        
        final WebSecurityUtil util = new WebSecurityUtil();
        
        util.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                return TestContext.getInstance().getDatabase().findById(UserAccount.class, userAccountId);
            }
            
            public UserAccount findUndeletedUserAccount(final String encodedUserName) {
                return new UserAccount();
            }
        });
        
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(final Object entity) {
                return entity;
            }
        });
        
        final List<Purchase> purchaseList = TestContext.getInstance().getDatabase().find(Purchase.class);
        final Purchase purchase = purchaseList.get(0);
        request.setParameter("transactionId", "ABC123");
        Mockito.when(this.commonControllerHelper.verifyRandomIdAndReturnActualLong((HttpServletResponse) anyObject(), (RandomKeyMapping) anyObject(),
                                                                                   anyString(), anyString())).thenReturn(purchase.getId());
        final String controllerResponse = this.capsdController.payStationDetailsReportsTransactionDetails(request, response, model);
        final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
        wrapperObject.setControllerResponse(controllerResponse);
        return controllerResponse;
    }
    
    @Override
    public final Object performAction(final Object... objects) {
        showDetailReport();
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... objects) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Object assertFailure(final Object... objects) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
