package com.digitalpaytech.bdd.mvc.customeradmin.transactionreceiptcontroller;

import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.validation.BindingResult;

import com.digitalpaytech.bdd.util.Cache;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.cache.PermitCacheManager;
import com.digitalpaytech.mvc.customeradmin.TransactionReceiptController;
import com.digitalpaytech.mvc.customeradmin.support.TransactionReceiptSearchForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.customeradmin.TransactionSearchValidator;

import junit.framework.Assert;

public class TestSearchPermits implements JBehaveTestHandler {
    
    @InjectMocks
    private TransactionReceiptController transactionReceiptController;
    
    @InjectMocks
    private TransactionSearchValidator transactionSearchValidator;
    
    @InjectMocks
    private PermitCacheManager permitCacheManager;
    
    @Mock
    private MessageSourceAccessor messageAccessor;
    
    @Mock
    private MessageSource messageSource;
    
    public TestSearchPermits() {
        MockitoAnnotations.initMocks(this);
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    
    private String fixDate(final String date) {
        final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy").withZone(ZoneId.of("UTC"));
        switch (date) {
            case "today":
                return dtf.format(Instant.now());
            case "tomorrow":
                return dtf.format(Instant.now().plus(Duration.ofDays(1)));
            case "next year":
                return dtf.format(Instant.now().plus(Duration.ofDays(365)));
            default:
                return date;
        }
        
    }
    
    private void searchPermits() {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final MockHttpServletResponse response = controllerFields.getResponse();
        final TransactionReceiptSearchForm form = (TransactionReceiptSearchForm) controllerFields.getForm();
        form.setCouponNumber("");
        form.setStartDate(fixDate(form.getStartDate()));
        form.setEndDate(fixDate(form.getEndDate()));
        final BindingResult result = controllerFields.getResult();
        final WebSecurityForm<TransactionReceiptSearchForm> webSecurityForm = new WebSecurityForm<TransactionReceiptSearchForm>(null, form);
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                return TestContext.getInstance().getDatabase().findById(UserAccount.class, userAccountId);
            }
            
            public UserAccount findUndeletedUserAccount(final String encodedUserName) {
                return new UserAccount();
            }
        });
        
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(final Object entity) {
                return entity;
            }
        });
        
        when(this.messageAccessor.getMessage(anyString(), any(Object[].class))).thenAnswer(new Answer<String>() {
            @Override
            public String answer(final InvocationOnMock invocation) throws Throwable {
                return invocation.getArgumentAt(0, String.class);
            }
        });
        
        when(this.messageAccessor.getMessage(anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(final InvocationOnMock invocation) throws Throwable {
                return invocation.getArgumentAt(0, String.class);
            }
        });
        
        this.transactionSearchValidator.validate(webSecurityForm, result);
        TestContext.getInstance().getCache().save(WebSecurityForm.class, TestDBMap.WEB_SECURITY_FORM_ID, webSecurityForm);
    }
    
    private void findErrors() {
        final Cache cache = TestContext.getInstance().getCache();
        final WebSecurityForm<TransactionReceiptSearchForm> form = cache.get(WebSecurityForm.class, TestDBMap.WEB_SECURITY_FORM_ID);
        final List<FormErrorStatus> errorCount = form.getValidationErrorInfo().getErrorStatus();
        assertThat("errorCount", errorCount.size(), greaterThan(0));
    }
    
    private void noErrors() {
        final Cache cache = TestContext.getInstance().getCache();
        final WebSecurityForm<TransactionReceiptSearchForm> form = cache.get(WebSecurityForm.class, TestDBMap.WEB_SECURITY_FORM_ID);
        final List<FormErrorStatus> errorCount = form.getValidationErrorInfo().getErrorStatus();
        Assert.assertEquals(0, errorCount.size());
    }
    
    @Override
    public final Object assertFailure(final Object... arg0) {
        findErrors();
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... arg0) {
        noErrors();
        return null;
    }
    
    @Override
    public final Object performAction(final Object... arg0) {
        searchPermits();
        return null;
    }
    
}