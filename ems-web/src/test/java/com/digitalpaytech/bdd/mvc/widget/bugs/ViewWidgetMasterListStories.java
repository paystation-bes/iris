package com.digitalpaytech.bdd.mvc.widget.bugs;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.mvc.systemadmin.systemadminmaincontroller.TestCustomerSave;
import com.digitalpaytech.bdd.mvc.widgetcontroller.TestViewOccupanyMapByLocation;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class ViewWidgetMasterListStories extends AbstractStories {
    
    public ViewWidgetMasterListStories() {
        super();
        this.testHandlers.registerTestHandler("addwidgetbuttonclick", TestViewOccupanyMapByLocation.class);
        this.testHandlers.registerTestHandler("occupancymapbylocation(p)visible", TestViewOccupanyMapByLocation.class);
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }

    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new ControllerBaseSteps(this.testHandlers));
    }
    
}
