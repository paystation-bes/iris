package com.digitalpaytech.bdd.mvc.customer;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.mvc.customeradmin.transactionreceiptcontroller.TestTransactionReceipts;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.dto.customeradmin.TransactionReceiptDetails;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class ViewTransactionReceiptsStories extends AbstractStories {
    public ViewTransactionReceiptsStories() {
        super();
        this.testHandlers.registerTestHandler("transactionreceiptview", TestTransactionReceipts.class);
        this.testHandlers.registerTestHandler("receiptwithemvshown", TestTransactionReceipts.class);
        this.testHandlers.registerTestHandler("receiptwithemvnotshown", TestTransactionReceipts.class);
        TestContext.getInstance().getObjectParser().register(new Class[] { TransactionReceiptDetails.class});
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new ControllerBaseSteps(this.testHandlers));
    }
    
}
