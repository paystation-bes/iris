package com.digitalpaytech.bdd.mvc.widgetcontroller;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.ui.ModelMap;

import static org.hamcrest.CoreMatchers.containsString;

import com.digitalpaytech.bdd.services.MessageHelperMockImpl;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.SubscriptionType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.WidgetMetricType;
import com.digitalpaytech.dto.WidgetMasterList;
import com.digitalpaytech.mvc.WidgetController;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.service.impl.CustomerSubscriptionServiceImpl;
import com.digitalpaytech.service.impl.DashboardServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.SubscriptionTypeServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.WebSecurityUtil;

public class TestViewOccupanyMapByLocation implements JBehaveTestHandler {
    private static final String OCCUPANCY_MAP_LOCATION_P = "Occupancy Map by Location(P)";
    @Mock
    private CommonControllerHelper commonControllerHelper;

    @InjectMocks
    private CustomerSubscriptionServiceImpl customerSubscriptionService;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private DashboardServiceImpl dashboardService;
    
    @InjectMocks
    private WidgetMasterList widgetMasterList;
    
    @InjectMocks
    private WidgetController widgetController;
    
    @Mock
    private MessageHelper messageHelper;
    
    public TestViewOccupanyMapByLocation() {
        MockitoAnnotations.initMocks(this);
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        when(this.messageHelper.getMessage(Mockito.anyString())).then(MessageHelperMockImpl.getMessageEcho());
    }
    
    private String showMasterList() {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final MockHttpServletRequest request = controllerFields.getRequest();
        final MockHttpServletResponse response = controllerFields.getResponse();
        final ModelMap model = controllerFields.getModel();
        final WebSecurityUtil util = new WebSecurityUtil();
        
        util.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                return (UserAccount) TestDBMap.findObjectByIdFromMemory(userAccountId, UserAccount.class);
            }
            
            public UserAccount findUndeletedUserAccount(final String encodedUserName) {
                return new UserAccount();
            }
        });
        
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(final Object entity) {
                return entity;
            }
        });
        
        final String controllerResponse = this.widgetController.showWidgetMasterList(request, response, model);
        final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
        wrapperObject.setControllerResponse(controllerResponse);
        return controllerResponse;
    }
    
    private void assertMasterList() {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        assertThat(controllerFields.getControllerResponse(), containsString(OCCUPANCY_MAP_LOCATION_P));
    }
    
    @Override
    public final Object performAction(final Object... objects) {
        showMasterList();
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... objects) {
        assertMasterList();
        return null;
    }
    
    @Override
    public final Object assertFailure(final Object... objects) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
