package com.digitalpaytech.bdd.mvc.customeradmin.alertcentercontroller;

import static org.mockito.Mockito.when;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;

import com.digitalpaytech.bdd.services.MessageHelperMockImpl;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.customeradmin.AlertCentreController;
import com.digitalpaytech.mvc.customeradmin.support.AlertCentreFilterForm;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.PosEventCurrentServiceImpl;
import com.digitalpaytech.service.impl.RouteServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebSecurityUtil;

public class TestViewPaystationActiveAlerts implements JBehaveTestHandler {
    
    private static final String PAYSTATION_SERIAL_IS = "Serial number is ";
    
    @InjectMocks
    private RouteServiceImpl routeService;
    
    @InjectMocks
    private PosHeartbeat posHeartbeat;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @Mock
    private MessageHelper messages;
    
    @Mock
    private UserAccount userAccount;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private AlertCentreController acController;
    
    @InjectMocks
    private PosEventCurrentServiceImpl posEventCurrentService;
    
    public TestViewPaystationActiveAlerts() {
        MockitoAnnotations.initMocks(this);
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        when(this.messages.getMessage(Mockito.anyString())).then(MessageHelperMockImpl.getMessageEcho());
    }
    
    
    private String viewActiveAlerts(final String serialNumber) {
        String controllerResponse;
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final MockHttpServletRequest request = controllerFields.getRequest();
        final MockHttpServletResponse response = controllerFields.getResponse();
        final ModelMap model = controllerFields.getModel();
        final AlertCentreFilterForm form = (AlertCentreFilterForm) controllerFields.getForm();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        final PointOfSale pos = TestContext.getInstance().getDatabase()
                .findOne(PointOfSale.class, PointOfSale.ALIAS_NAME, serialNumber.split(PAYSTATION_SERIAL_IS)[1]);
        request.setParameter("pointOfSaleId", keyMapping.getRandomString(PointOfSale.class, pos.getId()));
        
        final WebSecurityUtil util = new WebSecurityUtil();       
        util.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                return (UserAccount) TestDBMap.findObjectByIdFromMemory(userAccountId, UserAccount.class);
            }
            
            public UserAccount findUndeletedUserAccount(final String encodedUserName) {
                return new UserAccount();
            }
        });
        
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(final Object entity) {
                return entity;
            }
        });
            
        controllerResponse = this.acController.getAlertCentrePayStationActiveAlerts(request, response, model);
        final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
        wrapperObject.setControllerResponse(controllerResponse);
        return controllerResponse;
    }
    
    private void assertPaystationActiveAlerts() {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        assertThat(controllerFields.getControllerResponse(), containsString("payStationInfo"));
        
    }
    
    @Override
    public final Object performAction(final Object... objects) {
        viewActiveAlerts((String) objects[0]);
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... objects) {
        assertPaystationActiveAlerts();
        return null;
    }
    
    @Override
    public final Object assertFailure(final Object... objects) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
