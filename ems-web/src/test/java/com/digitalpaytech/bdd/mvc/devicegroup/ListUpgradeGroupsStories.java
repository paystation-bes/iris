package com.digitalpaytech.bdd.mvc.devicegroup;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.RibbonClientBaseSteps;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.client.FacilitiesManagementClient;
import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class ListUpgradeGroupsStories extends AbstractStories {
    
    public ListUpgradeGroupsStories() {
        super();
        
        final TestContext ctx = TestContext.getInstance();
        
        ctx.getObjectParser().register(FacilitiesManagementClient.class);
        
        this.testHandlers.registerTestHandler("groupslistupgrade", TestListUpgradeGroups.class);
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new ControllerBaseSteps(this.testHandlers),
                new RibbonClientBaseSteps(this.testHandlers));
    }
}
