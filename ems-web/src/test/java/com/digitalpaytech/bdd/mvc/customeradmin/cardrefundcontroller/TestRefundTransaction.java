package com.digitalpaytech.bdd.mvc.customeradmin.cardrefundcontroller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.jfree.util.Log;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.validation.BindingResult;

import com.beanstream.Gateway;
import com.beanstream.exceptions.BeanstreamApiException;
import com.beanstream.responses.PaymentResponse;
import com.digitalpaytech.bdd.services.MessageHelperMockImpl;
import com.digitalpaytech.bdd.services.ProcessorTransactionServiceMockImpl;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestBeanstreamException;
import com.digitalpaytech.bdd.util.TestConstants;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.cardprocessing.CardProcessingManager;
import com.digitalpaytech.cardprocessing.cps.CPSProcessor;
import com.digitalpaytech.cardprocessing.cps.impl.CPSProcessorImpl;
import com.digitalpaytech.cardprocessing.impl.CardProcessingManagerImpl;
import com.digitalpaytech.cardprocessing.impl.CardProcessorFactoryImpl;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.dao.impl.OpenSessionDaoTest;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.domain.ProcessorTransactionType;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.ProcessorInfo;
import com.digitalpaytech.dto.customeradmin.TransactionReceiptDetails;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.mvc.customeradmin.CardRefundController;
import com.digitalpaytech.mvc.customeradmin.support.CardRefundForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.ribbon.mock.MockClientFactory;
import com.digitalpaytech.service.CcFailLogService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.HashAlgorithmTypeService;
import com.digitalpaytech.service.ProcessorTransactionTypeService;
import com.digitalpaytech.service.PurchaseEmailAddressService;
import com.digitalpaytech.service.cps.impl.CPSDataServiceImpl;
import com.digitalpaytech.service.cps.impl.CoreCPSServiceImpl;
import com.digitalpaytech.service.crypto.CryptoAlgorithmFactory;
import com.digitalpaytech.service.impl.CardTypeServiceImpl;
import com.digitalpaytech.service.impl.ClusterMemberServiceImpl;
import com.digitalpaytech.service.impl.CommonProcessingServiceImpl;
import com.digitalpaytech.service.impl.CustomerAdminServiceImpl;
import com.digitalpaytech.service.impl.CustomerCardTypeServiceImpl;
import com.digitalpaytech.service.impl.EmsPropertiesServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.MailerServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.ProcessorServiceImpl;
import com.digitalpaytech.service.impl.ProcessorTransactionServiceImpl;
import com.digitalpaytech.service.impl.SingleTransactionCardDataServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.service.impl.WebWidgetHelperServiceImpl;
import com.digitalpaytech.service.kpi.KPIService;
import com.digitalpaytech.service.kpi.impl.KPIListenerServiceImpl;
import com.digitalpaytech.service.processor.TDMerchantCommunicationService;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.impl.TLSUtilsImpl;
import com.digitalpaytech.validation.CardNumberValidator;
import com.digitalpaytech.validation.customeradmin.CardRefundValidator;

@SuppressWarnings({ "PMD.ExcessiveImports", "PMD.TooManyFields", "PMD.TooManyStaticImports", "PMD.UnusedPrivateField" })
public class TestRefundTransaction implements JBehaveTestHandler {
    private static final String SESSION_KEY_CARD_NUMBER = "refundCardNumber";
    private static final String REFUND_DECLINED = "errorFieldName-refundTransaction";
    private static final String APPROVED_AUTH_CODE = "TEST";
    private static final String APPROVED_STRING = "1";
    private static final String APPROVED_CARD_NUMBER = "4030000010001234";
    private static final String APPROVED_AMOUNT = "200";
    private static final String DECLINED_AMOUNT = "700";
    private static final String DECLINED = "Declined";
    private static final int TEST_HTTP_STATUS_FAIL = 400;
    private static final int CATEGORY_ONE = 1;
    private static final int TEST_FAIL_MESSAGE_CODE = 7;
    
    @Mock
    private BeanFactory beanFactory;
    
    @Mock
    private KPIService kpiService;
    
    @InjectMocks
    private ClusterMemberServiceImpl clusterMemberService;
    
    @InjectMocks
    private KPIListenerServiceImpl kpiListenerService;
    
    @InjectMocks
    private CustomerAdminServiceImpl customerAdminService;
    
    @InjectMocks
    private WebWidgetHelperServiceImpl webWidgetHelperService;
    
    @InjectMocks
    private CommonProcessingServiceImpl commonProcessingService;
    
    @Mock
    private TDMerchantCommunicationService tdMerchantCommunicationService;
    
    @InjectMocks
    private CustomerCardTypeServiceImpl customerCardTypeService;
    
    @InjectMocks
    private CardTypeServiceImpl cardTypeService;
    
    @InjectMocks
    private MerchantAccountServiceImpl merchantAccountService;
    
    @InjectMocks
    private EmsPropertiesServiceImpl emsPropertiesService;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    private MailerServiceImpl mailerService;
    
    @InjectMocks
    private OpenSessionDaoTest openSessionDao;
    
    @Mock
    private MessageHelper messages;
    
    @InjectMocks
    private ProcessorServiceImpl processorService;
    
    @Mock
    private Set<ProcessorInfo> creditCardProcessor;
    
    @Mock
    private Map<Integer, ProcessorInfo> cpsProcessorMap;
    
    @Mock
    private Set<Integer> processorIds;
    
    @Mock
    private Map<Integer, ProcessorInfo> processorsMap;
    
    @InjectMocks
    private Processor processor;
    
    @Mock
    private CcFailLogService ccFailLogService;
    
    @InjectMocks
    private ProcessorTransactionServiceImpl processorTransactionService;
    
    @InjectMocks
    private CardProcessorFactoryImpl cardProcessorFactory;
    
    @InjectMocks
    private CardProcessingManagerImpl cardProcessingManager;
    
    @Mock
    private PurchaseEmailAddressService purchaseEmailAddressService;
    
    @Mock
    private CryptoAlgorithmFactory cryptoAlgorithmFactory;
    
    @Mock
    private HashAlgorithmTypeService hashAlgorithmTypeService;
    
    @Mock
    private CryptoService cryptoService;
    
    @Mock
    private CardRefundValidator cardRefundValidator;
    
    @Mock
    private CardNumberValidator<CardRefundForm> cardValidator;
    
    @InjectMocks
    private CardRefundController crController;
    
    @InjectMocks
    private CPSProcessorImpl cpsProcessor;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private EntityServiceImpl entityService;
    
    @InjectMocks
    private SingleTransactionCardDataServiceImpl singleTransactionCardDataService;
    
    @InjectMocks
    private CPSDataServiceImpl cpsDataService;
    
    @Mock
    private Map<Integer, ProcessorTransactionType> procTxTypesMap;
    
    @Mock
    private ProcessorTransactionTypeService processorTransactionTypeService;
    
    @InjectMocks
    private CoreCPSServiceImpl coreCPSService;
    
    @InjectMocks
    private TLSUtilsImpl tlsUtils;
    
    private final MockClientFactory client = MockClientFactory.getInstance();
    
    public TestRefundTransaction() {
        MockitoAnnotations.initMocks(this);
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
            Mockito.when(this.cryptoAlgorithmFactory.getSha1Hash(anyString(), anyInt())).thenReturn("dummyhash");
        } catch (IllegalAccessException | CryptoException e) {
            e.printStackTrace();
        }
        
        Mockito.when(this.purchaseEmailAddressService.getTransactionReceiptDetails(Matchers.any(Purchase.class)))
                .thenReturn(new TransactionReceiptDetails());
        Mockito.when(this.cardRefundValidator.validateMigrationStatus(Matchers.any(WebSecurityForm.class))).thenReturn(true);
        Mockito.when(this.messages.getMessage(Mockito.anyString())).then(MessageHelperMockImpl.getMessageEcho());
        Mockito.when(this.beanFactory.getBean(anyString(), Mockito.eq(CardProcessingManager.class))).thenReturn(this.cardProcessingManager);
        Mockito.when(this.processorTransactionTypeService.findProcessorTransactionType(Mockito.anyInt()))
                .thenAnswer(ProcessorTransactionServiceMockImpl.getProcessorTransactionType());
    }
    
    private String doRefund() throws CryptoException {
        final String controllerResponse;
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final CardRefundForm form = (CardRefundForm) controllerFields.getForm();
        final MockHttpServletRequest request = controllerFields.getRequest();
        final MockHttpServletResponse response = controllerFields.getResponse();
        final BindingResult result = controllerFields.getResult();
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                return TestContext.getInstance().getDatabase().findById(UserAccount.class, userAccountId);
            }
            
            public UserAccount findUndeletedUserAccount(final String encodedUserName) {
                return new UserAccount();
            }
        });
        
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(final Object entity) {
                return entity;
            }
        });
        
        final WebSecurityForm<CardRefundForm> cardRefundForm = new WebSecurityForm<CardRefundForm>(null, form);
        cardRefundForm.setInitToken(TestConstants.POST_TOKEN);
        cardRefundForm.setPostToken(TestConstants.POST_TOKEN);
        cardRefundForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        
        request.getSession().setAttribute(SESSION_KEY_CARD_NUMBER, "123456");
        
        final PaymentResponse paymentResponse = new PaymentResponse();
        paymentResponse.approved = APPROVED_STRING;
        paymentResponse.authCode = APPROVED_AUTH_CODE;
        paymentResponse.messageId = APPROVED_STRING;
        
        try {
            if (form.getCardNumber().equals(APPROVED_CARD_NUMBER) && form.getRefundAmount().equals(APPROVED_AMOUNT)) {
                Mockito.when(this.tdMerchantCommunicationService.sendRefundRequest(Mockito.any(Gateway.class), anyString(), anyDouble()))
                        .thenReturn(paymentResponse);
            } else if (form.getCardNumber().equals(APPROVED_CARD_NUMBER) && form.getRefundAmount().equals(DECLINED_AMOUNT)) {
                Mockito.when(this.tdMerchantCommunicationService.sendRefundRequest(Mockito.any(Gateway.class), anyString(), anyDouble()))
                        .thenThrow(new TestBeanstreamException(TEST_FAIL_MESSAGE_CODE, CATEGORY_ONE, DECLINED, TEST_HTTP_STATUS_FAIL, true));
            }
        } catch (BeanstreamApiException e) {
            e.printStackTrace();
        }
        
        new HashMap<Integer, CPSProcessor>();
        this.coreCPSService.init();
        
        controllerResponse = this.crController.processRefund(cardRefundForm, result, request, response);
        TestContext.getInstance().getCache().save(WebSecurityForm.class, TestDBMap.WEB_SECURITY_FORM_ID, cardRefundForm);
        final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
        wrapperObject.setControllerResponse(controllerResponse);
        return controllerResponse;
    }
    
    private void failedRefund() {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        assertThat(controllerFields.getControllerResponse().replace("\":\"", "-"), containsString(REFUND_DECLINED));
    }
    
    @Override
    public final Object performAction(final Object... objects) {
        try {
            doRefund();
        } catch (CryptoException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... objects) {
        return null;
    }
    
    @Override
    public final Object assertFailure(final Object... objects) {
        failedRefund();
        return null;
    }
}
