package com.digitalpaytech.bdd.mvc.location;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.jbehave.core.annotations.Given;
import org.junit.Assert;

import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.JBehaveTestHandlers;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.LocationGeopoint;

public final class AddLocationSteps extends AbstractSteps {
    
    private static final String LOCATION_NAME_STRING = "Location Name";
    
    public AddLocationSteps(JBehaveTestHandlers testHandlers) {
        super(testHandlers);
    }
    
    /**
     * Defines a parking area in the given location
     * 
     * @.example Given the Location Airport Parking has a defined parking area
     * @param locationName
     *            the location to assign the parking area to
     */
    @Given("the Location $locationName has a defined parking area")
    public final void defineParkingArea(final String locationName) {
        final Location location = (Location) TestDBMap.findObjectByFieldFromMemory(locationName, Location.ALIAS_LOCATION_NAME, Location.class);
        final LocationGeopoint locationGeopoint =
                new LocationGeopoint(location, new BigDecimal("49.2827"), new BigDecimal("-123.1207"), new Date(), 1);
        final Set<LocationGeopoint> locationGeopoints = new HashSet<LocationGeopoint>();
        locationGeopoints.add(locationGeopoint);
        location.setLocationGeopoints(locationGeopoints);
    }
    
    
    /**
     * Asserts that there are no active permits at a given location
     * 
     * @.example Given there are no active permits at Location Airport Parking
     * @param locationName
     *            the location to check for active permits
     */
    @Given("there are no active permits at Location $locationName")
    public final void noActivePermits(final String locationName) {
        final Location location = (Location) TestDBMap.findObjectByFieldFromMemory(locationName, LOCATION_NAME_STRING, Location.class);
        Assert.assertTrue(location.getPermits().isEmpty());
    }
}
