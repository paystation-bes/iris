package com.digitalpaytech.bdd.mvc.cardmanagement.refunds;

import org.jbehave.core.steps.InjectableStepsFactory;

import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.mvc.customeradmin.cardrefundcontroller.TestRefundTransaction;
import com.digitalpaytech.bdd.mvc.customeradmin.transactionreceiptcontroller.TestTransactionReceipts;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.RibbonClientBaseSteps;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.client.CoreCPSClient;
import com.digitalpaytech.client.PaymentClient;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.mvc.customeradmin.support.CardRefundForm;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class RefundCreditCardStories extends AbstractStories {
    
    public RefundCreditCardStories() {
        super();
        this.testHandlers.registerTestHandler("refundnotprocessed", TestRefundTransaction.class);
        this.testHandlers.registerTestHandler("refundclick", TestRefundTransaction.class);
        this.testHandlers.registerTestHandler("transactionreceiptview", TestTransactionReceipts.class);
        this.testHandlers.registerTestHandler("receiptwithemvshown", TestTransactionReceipts.class);
        
        TestContext.getInstance().getObjectParser()
                .register(new Class[] { CardRefundForm.class, Processor.class, CoreCPSClient.class, PaymentClient.class });
        
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new ControllerBaseSteps(this.testHandlers),
                new RibbonClientBaseSteps(this.testHandlers));
    }
    
}
