package com.digitalpaytech.bdd.mvc.systemadmin.merchantaccountcontroller;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;

import com.beanstream.Gateway;
import com.beanstream.exceptions.BeanstreamApiException;
import com.beanstream.requests.CardPaymentRequest;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestBeanstreamException;
import com.digitalpaytech.bdd.util.TestConstants;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.cardprocessing.cps.impl.CPSProcessorImpl;
import com.digitalpaytech.cardprocessing.impl.CardProcessingManagerImpl;
import com.digitalpaytech.cardprocessing.impl.CardProcessorFactoryImpl;
import com.digitalpaytech.client.PaymentClient;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.MerchantAccountController;
import com.digitalpaytech.mvc.systemadmin.support.MerchantAccountEditForm;
import com.digitalpaytech.ribbon.mock.MockClientFactory;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.impl.CustomerAdminServiceImpl;
import com.digitalpaytech.service.impl.CustomerServiceImpl;
import com.digitalpaytech.service.impl.CustomerSubscriptionServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.ProcessorServiceImpl;
import com.digitalpaytech.service.impl.SubscriptionTypeServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.service.impl.WebWidgetHelperServiceImpl;
import com.digitalpaytech.service.paystation.impl.PosServiceStateServiceImpl;
import com.digitalpaytech.service.processor.TDMerchantCommunicationService;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.ReportingUtil;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.systemadmin.MerchantAccountValidator;

public final class TestMerchantAccountDelete implements JBehaveTestHandler {
    
    private static final int TEST_HTTP_STATUS_FAIL = 400;
    private static final int CATEGORY_ONE = 1;
    private static final int TEST_FAIL_MESSAGE_CODE = 7;
    
    @Mock
    private CommonProcessingService commonProcessingService;
    
    @Mock
    private EmsPropertiesService emsPropertiesService;
    
    @Mock
    private TDMerchantCommunicationService tdMerchantCommunicationService;
    
    @InjectMocks
    private CardProcessingManagerImpl cardProcessingManager;
    
    @Mock
    private MerchantAccount merchantAccount;
    
    @Mock
    private MessageHelper messages;
    
    @Mock
    private ReportingUtil reportingUtil;
    
    @InjectMocks
    private MerchantAccountValidator merchantAccountValidator;
    
    @InjectMocks
    private CustomerAdminServiceImpl customerAdminService;
    
    @InjectMocks
    private CustomerSubscriptionServiceImpl customerSubscriptionService;
    
    @InjectMocks
    private ProcessorServiceImpl processorService;
    
    @InjectMocks
    private CustomerServiceImpl customerService;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private EntityServiceImpl entityService;
    
    @InjectMocks
    private SubscriptionTypeServiceImpl subscriptionTypeService;
    
    @InjectMocks
    private MerchantAccountController maController;
    
    @InjectMocks
    private CardProcessorFactoryImpl cardProcessorFactory;
    
    @InjectMocks
    private MerchantAccountServiceImpl merchantAccountService;
    
    @InjectMocks
    private WebWidgetHelperServiceImpl webWidgetHelperService;
    
    @Mock
    private MessageSourceAccessor messageAccessor;
    
    @InjectMocks
    private CPSProcessorImpl cpsProcessor;
    
    @InjectMocks
    private PosServiceStateServiceImpl posServiceStateService;
      
    MockClientFactory client = new MockClientFactory();
    
    public TestMerchantAccountDelete() {
        MockitoAnnotations.initMocks(this);
        
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    
    private String deleteMerchantAccount(Object[] objects) {
        final String controllerResponse;
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final MockHttpServletRequest request = controllerFields.getRequest();
        final MockHttpServletResponse response = controllerFields.getResponse();
        final BindingResult result = controllerFields.getResult();
        final ModelMap model = controllerFields.getModel();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();

        
        //TODO get rid of hard coded name
        final Customer forCustomer = TestDBMap.findObjectByFieldFromMemory("Mackenzie Parking", Customer.ALIAS_NAME, Customer.class);
        final MerchantAccount merchantAccount = TestDBMap.findObjectByFieldFromMemory("EmvMerchant", MerchantAccount.ALIAS_NAME, MerchantAccount.class);
        final String randId = keyMapping.getRandomString(Customer.class, forCustomer.getId());
        request.setParameter("customerID", randId);
        final String merchantAccountRandId = keyMapping.getRandomString(MerchantAccount.class, merchantAccount.getId());
        request.setParameter("merchantAccountId", merchantAccountRandId);
        
        //final String merchantAccountrRandId = keyMapping.getRandomString(MerchantAccount.class, Integer.parseInt(form.getRandomId()));
        //form.setRandomId(merchantAccountrRandId);
//        form.setMerchantAccountId(Integer.parseInt(form.getRandomId()));
        
        final WebSecurityUtil util = new WebSecurityUtil();
        
        util.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                return TestContext.getInstance().getDatabase().findById(UserAccount.class, userAccountId);
            }
            
            public UserAccount findUndeletedUserAccount(final String encodedUserName) {
                return new UserAccount();
            }
        });
        
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(final Object entity) {
                return entity;
            }
        });
        
        when(this.messageAccessor.getMessage(anyString(), any(Object[].class))).thenAnswer(new Answer<String>() {
            @Override
            public String answer(final InvocationOnMock invocation) throws Throwable {
                return invocation.getArgumentAt(0, String.class);
            }
        });
        
        when(this.messageAccessor.getMessage(anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(final InvocationOnMock invocation) throws Throwable {
                return invocation.getArgumentAt(0, String.class);
            }
        });

        this.client.prepareForSuccessRequest(PaymentClient.class, "{}".getBytes());
        
        controllerResponse = this.maController.deleteMerchantAccount(request, response, model);
        final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
        wrapperObject.setControllerResponse(controllerResponse);
        return controllerResponse;
        
    }
    
    @Override
    public Object performAction(final Object... objects) {
        deleteMerchantAccount(objects);
        return null;
    }
    
    @Override
    public Object assertSuccess(final Object... objects) {
        return null;
    }
    
    @Override
    public Object assertFailure(final Object... objects) {
        return null;
    }
    
}
