package com.digitalpaytech.bdd.mvc.systemadmin.systemadminpaystationlistdetailcontroller;

import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;

import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestConstants;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.client.AuthClient;
import com.digitalpaytech.client.TelemetryClient;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.dao.impl.OpenSessionDaoTest;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.SystemAdminPayStationListController;
import com.digitalpaytech.mvc.systemadmin.support.SysAdminPayStationEditForm;
import com.digitalpaytech.ribbon.mock.MockClientFactory;
import com.digitalpaytech.service.impl.CustomerAdminServiceImpl;
import com.digitalpaytech.service.impl.CustomerServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.LocationServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.service.paystation.impl.PosServiceStateServiceImpl;
import com.digitalpaytech.service.systemadmin.impl.SystemAdminServiceImpl;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.kafka.MockMessageProducer;
import com.digitalpaytech.validation.systemadmin.SysAdminPayStationValidator;

public class TestUpdateLinuxPos implements JBehaveTestHandler {
    private MockClientFactory client = MockClientFactory.getInstance();
    
    private MockMessageProducer producer = MockMessageProducer.getInstance();
    
    @InjectMocks
    private OpenSessionDaoTest openSessionDao;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private SystemAdminPayStationListController sysAdminPSController;
    
    @Mock
    private SysAdminPayStationValidator sysAdminPayStationValidator;
    
    @InjectMocks
    private SystemAdminServiceImpl systemAdminService;
    
    @InjectMocks
    private MerchantAccountServiceImpl merchantAccountService;
    
    @InjectMocks
    private CustomerServiceImpl customerService;
    
    @InjectMocks
    private CustomerAdminServiceImpl customerAdminService;
    
    @InjectMocks
    private LocationServiceImpl locationService;
    
    @InjectMocks
    private PosServiceStateServiceImpl posServiceStateService;
    
    public TestUpdateLinuxPos() {
        MockitoAnnotations.initMocks(this);
        this.client.prepareForSuccessRequest(AuthClient.class, "{}".getBytes());
        this.client.prepareForSuccessRequest(TelemetryClient.class, "{}".getBytes());
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public Object assertFailure(Object... arg0) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Object assertSuccess(Object... arg0) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public Object performAction(Object... arg0) {
        final String controllerResponse;
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final SysAdminPayStationEditForm form = (SysAdminPayStationEditForm) controllerFields.getForm();
        final MockHttpServletRequest request = controllerFields.getRequest();
        final MockHttpServletResponse response = controllerFields.getResponse();
        final BindingResult result = controllerFields.getResult();
        final ModelMap model = controllerFields.getModel();
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                return TestContext.getInstance().getDatabase().findById(UserAccount.class, userAccountId);
            }
            
            public UserAccount findUndeletedUserAccount(final String encodedUserName) {
                return new UserAccount();
            }
        });
        
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(final Object entity) {
                return entity;
            }
        });
        
        form.setHidden(false);
        form.setActive(true);
        form.setBundledData(false);
        
        final WebSecurityForm<SysAdminPayStationEditForm> webSecurityForm = new WebSecurityForm<SysAdminPayStationEditForm>(null, form);
        webSecurityForm.setInitToken(TestConstants.POST_TOKEN);
        webSecurityForm.setPostToken(TestConstants.POST_TOKEN);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        
        Mockito.when(this.sysAdminPayStationValidator.validateSystemAdminMigrationStatus(Matchers.any(WebSecurityForm.class),
                                                                                         Matchers.any(Customer.class)))
                .thenReturn(true);
        controllerResponse = sysAdminPSController.savePayStation(webSecurityForm, result, request, response, model);
        TestContext.getInstance().getCache().save(WebSecurityForm.class, TestDBMap.WEB_SECURITY_FORM_ID, webSecurityForm);
        final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
        wrapperObject.setControllerResponse(controllerResponse);
        return controllerResponse;
    }
    
}
