package com.digitalpaytech.bdd.mvc.customer.bugs;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.mvc.customeradmin.customeradminuseraccountsettingcontroller.TestSwitchCompany;
import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class CustomerBugsStories extends AbstractStories {
    
    public CustomerBugsStories() {
        super();
        this.testHandlers.registerTestHandler("companyswitch", TestSwitchCompany.class);
        this.testHandlers.registerTestHandler("companyswitched", TestSwitchCompany.class);
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    // Here we specify the steps classes
    @Override
    public final InjectableStepsFactory stepsFactory() {
        // varargs, can have more that one steps classes
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new ControllerBaseSteps(this.testHandlers));
    }
}
