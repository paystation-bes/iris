package com.digitalpaytech.bdd.mvc.cardmanagement.bugs;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.mvc.customeradmin.cardmanagementcontroller.TestSaveBannedCard;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerBadCard;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.mvc.customeradmin.support.BannedCardEditForm;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class CardManagementBugsStories extends AbstractStories {
    
    public CardManagementBugsStories() {
        super();
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
        TestLookupTables.getExpressionParser().register(new Class[] { Customer.class, CustomerBadCard.class, BannedCardEditForm.class,
                                                            CardType.class, CustomerCardType.class });
        testHandlers.registerTestHandler("badcardsave", TestSaveBannedCard.class);
    }
    
    // Here we specify the steps classes
    @Override
    public final InjectableStepsFactory stepsFactory() {
        // varargs, can have more that one steps classes
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new ControllerBaseSteps(this.testHandlers));
    }
}
