package com.digitalpaytech.bdd.mvc.widget;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.digitalpaytech.bdd.services.EntityDaoServiceMockImpl;
import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandlers;
import com.digitalpaytech.bdd.util.EntityDB;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetChartType;
import com.digitalpaytech.domain.WidgetFilterType;
import com.digitalpaytech.domain.WidgetMetricType;
import com.digitalpaytech.domain.WidgetRangeType;
import com.digitalpaytech.domain.WidgetTierType;
import com.digitalpaytech.dto.WidgetMetricInfo;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.impl.WebWidgetHelperServiceImpl;
import com.digitalpaytech.service.impl.WebWidgetOccupancyMapServiceImpl;
import com.digitalpaytech.util.StandardConstants;

import junit.framework.Assert;

//Short is needed because code uses short, unused private field is mock, Switch Statements will grow as more widgets tested, ConcurrentHashMap not needed, 
@SuppressWarnings({ "PMD.ExcessiveImports", "PMD.UseConcurrentHashMap", "PMD.BeanMembersShouldSerialize", "PMD.TooFewBranchesForASwitchStatement",
    "PMD.AvoidUsingShortType", "PMD.UnusedPrivateField" })
public final class WidgetSteps extends AbstractSteps{
   
    private static final String WIDGET_RESULT = "widgetResult";
    private static final String OCCUPANCY_MAP_MARKERS = "Occupancy Map Markers";
    private static final String OCCUPANCY_MAP_MARKER = "Occupancy Map Marker";
    private static final String DEFAULT_USER_NAME = "admin@";
    private static final int OCCUPANCY_MAP_CHART_TYPE = 10;
    private static final int OCCUPANCY_MAP_TIER_TYPE = 105;
    private static final int OCCUPANCY_MAP_METRIC_TYPE = 21;
    
    @Mock
    private EntityDao entityDao;
    @Mock
    private CustomerAdminService customerAdminService;
    @InjectMocks
    private WebWidgetHelperServiceImpl webWidgetHelperService;
    @InjectMocks
    private WebWidgetOccupancyMapServiceImpl webWidgetOccupancyMapServiceImpl;
    
    public WidgetSteps(JBehaveTestHandlers testHandlers) {
        super(testHandlers);
    }
    
    /**
     * Creates a widget and assigns it to a Customer
     * 
     * @.example Given Mackenzie Parking has an Occupancy Map widget
     * @param customerName
     *            name of customer to assign widget to
     * @param widgetType
     *            widget to create
     */
    @Given("$customerName has an $widgetType widget")
    public final void dashboardHasWidget(final String customerName, final String widgetType) {
        final Customer customer = (Customer) TestDBMap.findObjectByFieldFromMemory(customerName, Customer.ALIAS_NAME, Customer.class);
        final UserAccount userAccount =
                (UserAccount) TestDBMap.findObjectByFieldFromMemory(DEFAULT_USER_NAME + customerName, UserAccount.ALIAS_USER_NAME, UserAccount.class);
        final Widget newWidget = new Widget();
        final Set<Widget> newWidgitSet = new HashSet<Widget>();
        newWidgitSet.add(newWidget);
        final WidgetMetricType widgetMetricType = new WidgetMetricType();
        final WidgetTierType widgetTierType = new WidgetTierType();
        final WidgetChartType widgetChartType = new WidgetChartType();
        
        switch (widgetType) {
            case "Occupancy Map":
                final WidgetRangeType widgetRangeType = new WidgetRangeType(1, "Now", (short) 2958, new Date(), 1, newWidgitSet);
                final WidgetFilterType widgetFilterType = new WidgetFilterType(0, "All", (short) 2850, new Date(), 1);
                widgetMetricType.setId(OCCUPANCY_MAP_METRIC_TYPE);
                widgetTierType.setId(OCCUPANCY_MAP_TIER_TYPE);
                widgetChartType.setId(OCCUPANCY_MAP_CHART_TYPE);
                newWidget.setId(new Double(Math.random() * StandardConstants.CONSTANT_100).intValue());
                newWidget.setWidgetTierTypeByWidgetTier1Type(widgetTierType);
                widgetTierType.setId(0);
                newWidget.setWidgetTierTypeByWidgetTier2Type(widgetTierType);
                widgetTierType.setId(0);
                newWidget.setWidgetTierTypeByWidgetTier3Type(widgetTierType);
                newWidget.setWidgetMetricType(widgetMetricType);
                newWidget.setCustomerId(customer.getId());
                newWidget.setUserAccountId(userAccount.getId());
                newWidget.setWidgetRangeType(widgetRangeType);
                newWidget.setWidgetFilterType(widgetFilterType);
                newWidget.setWidgetRunDate(new Date());
                newWidget.setWidgetChartType(widgetChartType);
                TestDBMap.addToMemory(newWidget.getId(), newWidget);
                break;
            default:
                break;
        }
    }
    
    /**
     * Asserts that the given data is displayed in a widget
     * 
     * @.example Then the Occupancy Map Markers should be displayed
     * @param data
     *            the expected result
     *            
     */
    @Then("the $data should be displayed in the widget")
    public final void theDataShouldBeDisplayed(final String data) {
        final ControllerFieldsWrapper controllerFieldsWrapper = TestDBMap.getControllerFieldsWrapperFromMem();
        switch (data) {
            case OCCUPANCY_MAP_MARKER:
            case OCCUPANCY_MAP_MARKERS:
                final Map<String, Object> resultMap = controllerFieldsWrapper.getExpectedResult();
                final WidgetMetricInfo widgetMetricInfo = (WidgetMetricInfo) resultMap.get(WIDGET_RESULT);
                Assert.assertEquals(BigDecimal.ZERO, widgetMetricInfo.getWidgetMetricValue());
                break;
            default:
                break;
        }
    }
    
    /**
     * Asserts that the widget data is in a given state
     * 
     * @.example Then the Occupancy Map Markers should be green
     * @param data
     *            the expected result
     * @param state
     *            the expected state of the given data
     *            
     */
    @Then("the $data should be $state")
    public final void theDataShouldBeState(final String data, final String state) {
        final ControllerFieldsWrapper controllerFieldsWrapper = TestDBMap.getControllerFieldsWrapperFromMem();
        switch (data) {
            case OCCUPANCY_MAP_MARKER:
            case OCCUPANCY_MAP_MARKERS:
                final String expected = "AS wData RIGHT JOIN";
                Assert.assertTrue(controllerFieldsWrapper.getDataList().get(0).contains(expected));
                Assert.assertFalse(controllerFieldsWrapper.getDataList().get(1).contains(expected));
                break;
            default:
                break;
        }
        
    }
    
    /**
     * Fetches the SQL query for getting occupancy data for the Occupancy Map Widget
     * 
     * @.example When the Occupancy Map widget is viewed on Mackenzie Parking dashboard
     * @param customerName
     *            the customer who owns the widget
     */
    //suppress because this should get moved to default data as we test more widgets
    @SuppressWarnings("checkstyle:multiplestringliterals")
    @When(value = "the Occupancy Map widget $name is viewed on $customerName dashboard", priority = 1)
    public final void viewWidgetData(final String name, final String customerName) {
        final ControllerFieldsWrapper controllerFieldsWrapper = TestDBMap.getControllerFieldsWrapperFromMem();
        final String jsonString = "[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"LineString\",\"coordinates\":"
                                  + "[[-123.17758983699605,49.19316941354254],[-123.17767164437099,49.19290034992428]]}},"
                                  + "{\"type\":\"Feature\",\"properties\":{},\"geometry\""
                                  + ":{\"type\":\"Point\",\"coordinates\":[-123.17732027499005,49.193101052240756]}},"
                                  + "{\"type\":\"Feature\",\"properties\":{},\"geometry\""
                                  + ":{\"type\":\"Point\",\"coordinates\":[-123.17736453143878,49.19296170006377]}}]";
        final Session session = Mockito.mock(Session.class);
        final SQLQuery sqlQuery = Mockito.mock(SQLQuery.class);
        final Customer customer = (Customer) TestDBMap.findObjectByFieldFromMemory(customerName, Customer.ALIAS_NAME, Customer.class);
        final WidgetMetricInfo widgetMetricInfo =
                new WidgetMetricInfo(customer.getId(), 553, 0, 0, "Airport", null, null, BigDecimal.ZERO, new Date(), jsonString);
        final List<WidgetMetricInfo> widgetMetricInfoList = new ArrayList<WidgetMetricInfo>(1);
        widgetMetricInfoList.add(widgetMetricInfo);
        MockitoAnnotations.initMocks(this);
        Mockito.when(sqlQuery.list()).thenReturn(widgetMetricInfoList);
        Mockito.when(this.entityDao.getCurrentSession()).thenReturn(session);
        Mockito.when(this.entityDao.createSQLQuery(Mockito.anyString())).thenAnswer(EntityDaoServiceMockImpl.switchSQLWithMock(sqlQuery));
        
        final EntityDB db = TestContext.getInstance().getDatabase();
        
        final UserAccount userAccount =
                (UserAccount) TestDBMap.findObjectByFieldFromMemory(DEFAULT_USER_NAME + customerName, UserAccount.ALIAS_USER_NAME, UserAccount.class);
        final Widget widget = (Widget) db.session().createQuery("from Widget w where w.customerId = :customerId and w.name = :name").setParameter("customerId", customer.getId()).setParameter("name", name).uniqueResult();
        this.webWidgetOccupancyMapServiceImpl.setWebWidgetHelperService(this.webWidgetHelperService);
        final Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put(WIDGET_RESULT, this.webWidgetOccupancyMapServiceImpl.getWidgetData(widget, userAccount));
        controllerFieldsWrapper.setExpectedResult(resultMap);
    }
}
