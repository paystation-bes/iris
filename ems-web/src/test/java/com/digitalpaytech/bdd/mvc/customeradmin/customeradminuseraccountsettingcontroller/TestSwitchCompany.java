package com.digitalpaytech.bdd.mvc.customeradmin.customeradminuseraccountsettingcontroller;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.bdd.services.UserAccountServiceMockImpl;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.domain.ActivityLogin;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.CustomerType;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.SubscriptionType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.customeradmin.CustomerAdminUserAccountSettingController;
import com.digitalpaytech.service.ActivityLogService;
import com.digitalpaytech.service.CustomerRoleService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.RoleService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.ActivityLoginServiceImpl;
import com.digitalpaytech.service.impl.CustomerSubscriptionServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.UserSettingValidator;

import junit.framework.Assert;

public class TestSwitchCompany implements JBehaveTestHandler {
    
    private static final String TIME_ZONE_IN_SESSION_STRING = "active time zone in the session";
    
    @Mock
    private ActivityLogService activityLogService;
    
    @Mock
    private UserAccountService userAccountService;
    
    @Mock
    private CustomerRoleService customerRoleService;
    
    @Mock
    private CustomerService customerService;
    
    @Mock
    private EntityService entityService;
    
    @Mock
    private RoleService roleService;
    
    @Mock
    private UserSettingValidator userSettingValidator;
    
    @Mock
    private PasswordEncoder passwordEncoder;
    
    @InjectMocks
    private CustomerAdminUserAccountSettingController customerAdminUserAccountsettings;
    
    public final void switchCompany(final String data) {
        MockitoAnnotations.initMocks(this);
        String fromCompany = null;
        String toCompany = null;
        if (data != null && data.contains(WebCoreConstants.COLON)) {
            fromCompany = data.split(WebCoreConstants.COLON)[0];
            toCompany = data.split(WebCoreConstants.COLON)[1];
        }
        final Customer fromCustomer = (Customer) TestDBMap.findObjectByFieldFromMemory(fromCompany, Customer.ALIAS_NAME, Customer.class);
        final Customer toCustomer = (Customer) TestDBMap.findObjectByFieldFromMemory(toCompany, Customer.ALIAS_NAME, Customer.class);
        UserAccount switchedUserAccount = null;
        
        if (fromCustomer.isIsParent() && !toCustomer.isIsParent()) {
            for (UserAccount userAccount : (Collection<UserAccount>) TestDBMap.getTypeListFromMemory(UserAccount.class)) {
                if (userAccount.isIsAliasUser()) {
                    switchedUserAccount = userAccount;
                }
            }
        } else {
            for (UserAccount userAccount : (Collection<UserAccount>) TestDBMap.getTypeListFromMemory(UserAccount.class)) {
                if (userAccount.getCustomer().getId() == toCustomer.getId()) {
                    switchedUserAccount = userAccount;
                }
            }
            
        }
        
        TestDBMap.getControllerFieldsWrapperFromMem().getRequest().setParameter("randomId", TestDBMap.getKeyMapping().getRandomString(UserAccount.class, switchedUserAccount.getId()));
                
        when(this.userAccountService.findUserAccount(anyInt())).thenAnswer(UserAccountServiceMockImpl.findUserAccount());
        
        final WebSecurityUtil util = new WebSecurityUtil();
        
        util.setActivityLoginService(new ActivityLoginServiceImpl() {
            public Serializable saveActivityLogin(final ActivityLogin activityLogin) {
                return null;
            }
        });
        
        util.setCustomerSubscriptionService(new CustomerSubscriptionServiceImpl() {
            public List<CustomerSubscription> findActiveCustomerSubscriptionByCustomerId(final int id, final boolean cacheable) {
                final ArrayList<CustomerSubscription> list = new ArrayList<CustomerSubscription>();
                final CustomerSubscription customerSubscription = new CustomerSubscription();
                customerSubscription.setSubscriptionType(new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_STANDARD_REPORT));
                list.add(customerSubscription);
                return list;
            }
        });
        util.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                return (UserAccount) TestDBMap.findObjectByIdFromMemory(userAccountId, UserAccount.class);
            }
            
            public UserAccount findUndeletedUserAccount(final String encodedUserName) {
                return new UserAccount();
            }
            
            public List<RolePermission> findUserRoleAndPermissionForCustomerType(final Integer userAccountId, final Integer customerTypeId) {
                final Role role = new Role();
                final CustomerType customerType = new CustomerType();
                customerType.setId(WebCoreConstants.CUSTOMER_TYPE_CHILD);
                role.setCustomerType(customerType);
                final Permission permission = new Permission();
                permission.setId(WebCoreConstants.SUBSCRIPTION_TYPE_STANDARD_REPORT);
                final RolePermission rolePermission = new RolePermission();
                rolePermission.setRole(role);
                rolePermission.setPermission(permission);
                final ArrayList<RolePermission> rolePermissions = new ArrayList<RolePermission>();
                rolePermissions.add(rolePermission);
                return rolePermissions;
            }
        });
        util.setEntityService(new EntityServiceImpl() {
            
            public Object merge(final Object entity) {
                return entity;
            }
        });
        
        final String controllerResponse = this.customerAdminUserAccountsettings
                .changeUser(TestDBMap.getControllerFieldsWrapperFromMem().getRequest(), TestDBMap.getControllerFieldsWrapperFromMem().getResponse());
        Assert.assertTrue(WebCoreConstants.RESPONSE_TRUE.equalsIgnoreCase(controllerResponse));
    }
    
    public final void checkSwitchedCompany() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final WebUser webUser = (WebUser) authentication.getPrincipal();
        
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final Map<String, Object> data = (HashMap<String, Object>) controllerFields.getExpectedResult();
        for (Entry<String, Object> entry : data.entrySet()) {
            final String key = entry.getKey();
            final Object value = entry.getValue();
            switch (key.toLowerCase()) {
                case TIME_ZONE_IN_SESSION_STRING:
                    final String customerTimeZone = webUser.getCustomerTimeZone();
                    Assert.assertTrue(customerTimeZone.equalsIgnoreCase((String) value));
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public Object performAction(Object... objects) {
        switchCompany((String) objects[0]);
        return null;
    }

    @Override
    public Object assertSuccess(Object... objects) {
        checkSwitchedCompany();
        return null;
    }

    @Override
    public Object assertFailure(Object... objects) {
        return null;
    }
    
}
