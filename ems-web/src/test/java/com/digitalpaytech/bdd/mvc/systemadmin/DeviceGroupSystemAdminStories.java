package com.digitalpaytech.bdd.mvc.systemadmin;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.mvc.customeradmin.customeradmindevicegroupcontroller.TestViewDeviceGroups;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.RibbonClientBaseSteps;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.client.FacilitiesManagementClient;
import com.digitalpaytech.client.dto.DeviceGroupFilter;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class DeviceGroupSystemAdminStories extends AbstractStories {
    public DeviceGroupSystemAdminStories() {
        final TestContext ctx = TestContext.getInstance();
        
        ctx.getObjectParser().register(DeviceGroupFilter.class, FacilitiesManagementClient.class);
        
        this.testHandlers.registerTestHandler("groupsfilterconfiguration", TestViewDeviceGroups.class);
        
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(),
                                        new BaseSteps(this.testHandlers),
                                        new ControllerBaseSteps(this.testHandlers),
                                        new RibbonClientBaseSteps(this.testHandlers));
    }    
}
