package com.digitalpaytech.bdd.mvc.customeradmin.customeradminpaystationdetailcontroller;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.client.FacilitiesManagementClient;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.mvc.customeradmin.CustomerAdminPayStationDetailController;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.ribbon.mock.MockClientFactory;
import com.digitalpaytech.service.impl.CustomerAdminServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.PosTelemetryServiceImpl;
import com.digitalpaytech.service.impl.RouteServiceImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;

public class TestPaystationInformation implements JBehaveTestHandler {
    private MockClientFactory client = MockClientFactory.getInstance();
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    private CustomerAdminPayStationDetailController capsdController;
    
    @InjectMocks
    private CustomerAdminServiceImpl customerAdminServiceImpl;
    
    @Mock
    private CommonControllerHelper commonControllerHelper;
    
    @InjectMocks
    private RouteServiceImpl routeService;
    
    @InjectMocks
    private PosTelemetryServiceImpl posTelemetryService;
    
    public TestPaystationInformation() {
        MockitoAnnotations.initMocks(this);
        
        this.client.prepareForSuccessRequest(FacilitiesManagementClient.class, "{}".getBytes());
        
        Mockito.when(this.commonControllerHelper.verifyRandomIdAndReturnActual(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(1);
        
        this.capsdController.init();
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public final Object performAction(final Object... objects) {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        
        final MockHttpServletRequest request = controllerFields.getRequest();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        request.setParameter("payStationID", keyMapping.getRandomString(PointOfSale.class, 1));
        
        final MockHttpServletResponse response = controllerFields.getResponse();
        final ModelMap model = controllerFields.getModel();
        
        final String controllerResponse = this.capsdController.payStationDetailsInformation(request, response, model);
        final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
        wrapperObject.setControllerResponse(controllerResponse);
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... objects) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Object assertFailure(final Object... objects) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
