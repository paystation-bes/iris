package com.digitalpaytech.bdd.mvc.devicegroup;

import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.json.JSON;
import com.fasterxml.jackson.core.type.TypeReference;

import junit.framework.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.client.FacilitiesManagementClient;
import com.digitalpaytech.client.dto.fms.DeviceGroup;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.dao.impl.OpenSessionDaoTest;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.mvc.customeradmin.CustomerAdminDeviceGroupController;
import com.digitalpaytech.mvc.support.impl.CommonControllerHelperImpl;
import com.digitalpaytech.ribbon.mock.MockClientFactory;
import com.digitalpaytech.service.impl.CustomerServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;

@SuppressWarnings({ "checkstyle:multiplestringliterals", "checkstyle:magicnumber" })
public class TestGroupAutoComplete implements JBehaveTestHandler {
    
    private static final String EXPECTED_JSON = "[{\"groupName\":\"testGroupName\",\"groupId\":\"1\",\"groupStatus\":\"active\"}]";
    private TypeReference<Map<String, List<DeviceGroup>>> groupMapType = new TypeReference<Map<String, List<DeviceGroup>>>() {
    };
    private MockClientFactory client = MockClientFactory.getInstance();
    
    @Mock
    private FacilitiesManagementClient facilitiesManagementClient;
    
    @InjectMocks
    private CommonControllerHelperImpl commonControllerHelper;
    
    @InjectMocks
    private CustomerServiceImpl customerService;
    
    @InjectMocks
    private UserAccountServiceImpl userAccountService;
    
    @InjectMocks
    private OpenSessionDaoTest openSessionDao;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private EntityServiceImpl entityService;
    
    private CustomerAdminDeviceGroupController deviceConfig;
    
    public TestGroupAutoComplete() {
        deviceConfig = new CustomerAdminDeviceGroupController();
        deviceConfig.init();
        MockitoAnnotations.initMocks(this);
        
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    
    private String groupDetails() {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        
        final WebSecurityUtil util = new WebSecurityUtil();
        
        util.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                return TestContext.getInstance().getDatabase().findById(UserAccount.class, userAccountId);
            }
            
            public UserAccount findUndeletedUserAccount(final String encodedUserName) {
                return new UserAccount();
            }
        });
        
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(final Object entity) {
                return entity;
            }
        });
        
        final MockHttpServletRequest request = controllerFields.getRequest();
        request.setParameter("search", "test");
        final MockHttpServletResponse response = controllerFields.getResponse();
        
        this.client.prepareForSuccessRequest(FacilitiesManagementClient.class, EXPECTED_JSON.getBytes());
        
        final String controllerResponse = deviceConfig.autocomplete(request, response);
        final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
        wrapperObject.setControllerResponse(controllerResponse);
        return controllerResponse;
    }
    
    @Override
    public final Object assertFailure(final Object... arg0) {
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... arg0) {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final String controllerResponse = controllerFields.getControllerResponse();
        final String expectedGroupListJson = (String) arg0[0];
        
        final JSON json = new JSON();
        
        try {
            final Map<String, List<DeviceGroup>> groupMap = (Map<String, List<DeviceGroup>>) json.deserialize(controllerResponse, groupMapType);
            final List<DeviceGroup> groupList = (ArrayList<DeviceGroup>) groupMap.get("groupSearchResult");
            final Map<String, List<DeviceGroup>> expectedGroupMap = (Map<String, List<DeviceGroup>>) json.deserialize(expectedGroupListJson, groupMapType);
            final List<DeviceGroup> expectedGroupList = (ArrayList<DeviceGroup>) expectedGroupMap.get("groupSearchResult");
            
            for (int x = 0; x < groupList.size(); x++) {
                final DeviceGroup actual = groupList.get(x);
                final DeviceGroup expected = expectedGroupList.get(x);
                Assert.assertEquals(actual.getGroupName(), expected.getGroupName());
                Assert.assertEquals(actual.getGroupStatus(), expected.getGroupStatus());
            }
            
        } catch (JsonException e) {
            Assert.fail(e.getMessage());
        }
        
        return null;
    }
    
    @Override
    public final Object performAction(final Object... arg0) {
        groupDetails();
        return null;
    }
}
