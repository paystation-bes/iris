package com.digitalpaytech.bdd.mvc.systemadmin.merchantaccountcontroller;

import junit.framework.Assert;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Mockito;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestConstants;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.services.CustomerAdminServiceMockImpl;
import com.digitalpaytech.cardprocessing.impl.CardProcessorFactoryImpl;
import com.digitalpaytech.client.PaymentClient;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.ProcessorInfo;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.MerchantAccountController;
import com.digitalpaytech.mvc.systemadmin.support.MerchantAccountEditForm;

import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.ribbon.mock.MockClientFactory;
import com.digitalpaytech.service.CustomerAdminService;

import com.digitalpaytech.service.impl.CustomerAdminServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.ProcessorServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.ReportingUtil;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

public final class TestMerchantAccountDetails implements JBehaveTestHandler {
    public static final String MERCHANT_ACCOUNT_DETAIL_URL = "/systemAdmin/workspace/customers/include/merchantAccountDetail";
    
    @Mock
    private MerchantAccount merchantAccount;
    
    @Mock
    private MessageHelper messages;
    
    @Mock
    private ReportingUtil reportingUtil;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private EntityServiceImpl entityService;
    
    @InjectMocks
    private CustomerAdminServiceImpl customerAdminService;
    
    @Mock
    private CustomerAdminService customerAdminServiceMock;
    
    @InjectMocks
    private ProcessorServiceImpl processorService;
    
    @Mock
    private ProcessorInfo processorInfo;
    
    @InjectMocks
    private CardProcessorFactoryImpl cardProcessorFactory;
    
    @InjectMocks
    private MerchantAccountController maController;
    
    @InjectMocks
    private MerchantAccountServiceImpl merchantAccountService;
    
    MockClientFactory client = new MockClientFactory();
    
    public TestMerchantAccountDetails() {
        MockitoAnnotations.initMocks(this);
        
        Mockito.when(this.customerAdminServiceMock.getCustomerPropertyByCustomerIdAndCustomerPropertyType(Mockito.anyInt(), Mockito.anyInt()))
            .thenAnswer(CustomerAdminServiceMockImpl.createDummyCustomerProperty());    

        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    
    private String viewMerchantAccount() {
        String controllerResponse;
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final MerchantAccountEditForm form = (MerchantAccountEditForm) controllerFields.getForm();
        final MockHttpServletRequest request = controllerFields.getRequest();
        final MockHttpServletResponse response = controllerFields.getResponse();
        final ModelMap model = controllerFields.getModel();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final Customer forCustomer = TestDBMap.findObjectByFieldFromMemory("Mackenzie Parking", Customer.ALIAS_NAME, Customer.class);
        final MerchantAccount ma = TestDBMap.findObjectByFieldFromMemory(form.getName(), MerchantAccount.ALIAS_NAME, MerchantAccount.class);
        
        request.setParameter("merchantAccountId", keyMapping.getRandomString(MerchantAccount.class, ma.getId()));
        form.setCustomerRealId(forCustomer.getId());
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                return TestContext.getInstance().getDatabase().findById(UserAccount.class, userAccountId);
            }
            
            public UserAccount findUndeletedUserAccount(final String encodedUserName) {
                return new UserAccount();
            }
        });
        
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(final Object entity) {
                return entity;
            }
        });

        this.client.prepareForSuccessRequest(PaymentClient.class, "{}".getBytes());
        final WebSecurityForm<MerchantAccountEditForm> webSecurityForm = new WebSecurityForm<MerchantAccountEditForm>(null, form);
        webSecurityForm.setInitToken(TestConstants.POST_TOKEN);
        webSecurityForm.setPostToken(TestConstants.POST_TOKEN);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        controllerResponse = this.maController.viewMerchantAccount(request, response, model);
        TestContext.getInstance().getCache().save(WebSecurityForm.class, TestDBMap.WEB_SECURITY_FORM_ID, webSecurityForm);
        final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
        wrapperObject.setControllerResponse(controllerResponse);
        return controllerResponse;
        
    }
    
    private void assertMerchantAccountDetails() {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        Assert.assertEquals(controllerFields.getControllerResponse(), MERCHANT_ACCOUNT_DETAIL_URL);
    }
    
    @Override
    public Object performAction(final Object... objects) {
        viewMerchantAccount();
        return null;
    }
    
    @Override
    public Object assertSuccess(final Object... objects) {
        assertMerchantAccountDetails();
        return null;
    }
    
    @Override
    public Object assertFailure(final Object... objects) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
