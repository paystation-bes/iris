package com.digitalpaytech.bdd.mvc.systemadmin.systemadminmaincontroller;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.bdd.services.CustomerServiceMockImpl;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestConstants;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerStatusType;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.dto.customeradmin.CustomerDetails;
import com.digitalpaytech.mvc.systemadmin.CustomerDetailsController;
import com.digitalpaytech.mvc.systemadmin.SystemAdminMainController;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerAgreementService;
import com.digitalpaytech.service.CustomerMigrationService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.CustomerSubscriptionService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.RestAccountService;
import com.digitalpaytech.service.WebServiceEndPointService;
import com.digitalpaytech.util.FormFieldConstants;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

import junit.framework.Assert;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class TestSearchForCustomer implements JBehaveTestHandler {
    private static final String CHILD_COMPANIES_STRING = "child companies";
    @Mock
    private CustomerSubscriptionService customerSubscriptionService;
    @Mock
    private CustomerService customerService;
    @Mock
    private CustomerAdminService customerAdminService;
    @Mock
    private MerchantAccountService merchantAccountService;
    @Mock
    private WebServiceEndPointService webServiceEndPointService;
    @Mock
    private RestAccountService restAccountService;
    @Mock
    private CustomerAgreementService customerAgreementService;
    @Mock
    private CustomerMigrationService customerMigrationService;
    @Mock
    private CustomerProperty customerProperty;
    @InjectMocks
    private CustomerDetailsController customerDetailsController;
    @InjectMocks
    private SystemAdminMainController systemAdminMainController;
    
    private void clickToSearch() {
        MockitoAnnotations.initMocks(this);
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final MockHttpServletRequest request = controllerFields.getRequest();
        final MockHttpServletResponse response = controllerFields.getResponse();
        final ModelMap model = controllerFields.getModel();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(controllerFields.getRequest()).getKeyMapping();
        final String searchString = controllerFields.getData();
        when(this.customerSubscriptionService.findByCustomerId(anyInt(), anyBoolean())).thenReturn(new HashSet<CustomerSubscription>());
        final Customer customer = (Customer) TestDBMap.findObjectByFieldFromMemory(searchString, Customer.ALIAS_NAME, Customer.class);
        request.setParameter("randomId", keyMapping.getRandomString(Customer.class, customer.getId()));
        final String redirectResponse = this.systemAdminMainController.goDetails(request, response, model);
        Assert.assertTrue(redirectResponse.contains("redirect:/systemAdmin/workspace/customers/customerDetails.html?customerID="));
        when(this.customerService.findCustomer(anyInt())).thenAnswer(CustomerServiceMockImpl.findCustomer());
        final String custTZ = TestLookupTables.findMatchingTZById(customer.getTimezoneId());
        when(this.customerProperty.getPropertyValue()).thenReturn(custTZ);
        when(this.customerAdminService.getCustomerPropertyByCustomerIdAndCustomerPropertyType(anyInt(),
                                                                                              eq(WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE)))
                                                                                                      .thenReturn(this.customerProperty);
        request.setParameter(FormFieldConstants.PARAMETER_CUSTOMER_ID, keyMapping.getRandomString(Customer.class, customer.getId()));
        
        when(this.customerService.findAllChildCustomers(anyInt())).thenAnswer(CustomerServiceMockImpl.findAllChildCustomers());
        this.customerDetailsController.showCustomerDetails(request, response, model);
        controllerFields.setModel(model);
        TestDBMap.saveControllerFieldsWrapperToMem(controllerFields);
    }
    
    private void assertCustomerDetails() {
                      
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final ModelMap model = controllerFields.getModel();
        final Map<String, Object> dataMap = controllerFields.getExpectedResult();
        final CustomerDetails customerDetails = (CustomerDetails) model.get("customerDetails");
        final List<Customer> childCustomers =
                customerDetails.getChildCustomers() == null ? new ArrayList<Customer>() : customerDetails.getChildCustomers();
        final String name = customerDetails.getCustomer().getName();
        final String timeZone = customerDetails.getTimeZone();
        final CustomerStatusType statusType = customerDetails.getCustomer().getCustomerStatusType();
        Assert.assertEquals(dataMap.get(TestConstants.CUSTOMER_NAME_STRING), name);
        Assert.assertEquals(dataMap.get("time zone"), timeZone);
        Assert.assertEquals(dataMap.get("status"), getStatusTypeName(statusType));
        final String expectedChildString =
                dataMap.get(CHILD_COMPANIES_STRING) == null ? StandardConstants.STRING_EMPTY_STRING : (String) dataMap.get(CHILD_COMPANIES_STRING);
        final String[] expectedChildArray = expectedChildString.isEmpty() ? new String[0] : expectedChildString.split(StandardConstants.STRING_COMMA);
        Assert.assertEquals(expectedChildArray.length, childCustomers.size());
        for (int i = 0; i < expectedChildArray.length; i++) {
            Assert.assertEquals(expectedChildArray[i], childCustomers.get(i).getName());
        }
    }
    
    private String getStatusTypeName(final CustomerStatusType statusType) {
        final String statusName = statusType.getId() == WebCoreConstants.CUSTOMER_STATUS_TYPE_ENABLED ? TestConstants.ENABLED_STRING
                : statusType.getId() == WebCoreConstants.CUSTOMER_STATUS_TYPE_DISABLED ? "trial"
                        : statusType.getId() == WebCoreConstants.CUSTOMER_STATUS_TYPE_DELETED ? "deleted" : "disabled";
        return statusName;
    }

    @Override
    public final Object performAction(final Object... objects) {
        clickToSearch();
        return null;
    }

    @Override
    public final Object assertSuccess(final Object... objects) {
        assertCustomerDetails();
        return null;
    }

    @Override
    public final Object assertFailure(final Object... objects) {
        return null;
    }
}
