package com.digitalpaytech.bdd.mvc.systemadmin.systemadminmaincontroller;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;

import com.digitalpaytech.bdd.services.CustomerServiceMockImpl;
import com.digitalpaytech.bdd.services.SubscriptionTypeServiceMockImpl;
import com.digitalpaytech.bdd.services.SystemAdminServiceMockImpl;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestConstants;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.bdd.util.TestLookupTables;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerStatusType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.ValidationErrorInfo;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.SystemAdminMainController;
import com.digitalpaytech.mvc.systemadmin.support.CustomerEditForm;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.SubscriptionTypeService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.service.systemadmin.SystemAdminService;
import com.digitalpaytech.testing.services.impl.CustomerAdminServiceTestImpl;
import com.digitalpaytech.testing.services.impl.CustomerServiceTestImpl;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.systemadmin.CustomerValidator;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;

import junit.framework.Assert;

@SuppressWarnings({ "PMD.ExcessiveImports", "PMD.UnusedPrivateField", "PMD.BeanMembersShouldSerialize", "PMD.UseVarargs" })
public class TestCustomerSave implements JBehaveTestHandler {
    
    private static Map<String, String> validationMap;
    @Mock
    private CustomerAdminService customerAdminService;
    @Mock
    private SystemAdminService systemAdminService;
    @Mock
    private CustomerService customerService;
    @Mock
    private UserAccountService userAccountService;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private SubscriptionTypeService subscriptionTypeService;
    @InjectMocks
    private SystemAdminMainController systemAdminMainController;
    @InjectMocks
    private CustomerValidator customerValidator;
    
    private ValidationErrorInfo validationErrorInfo;
    
    static {
        validationMap = new HashMap<String, String>();
        validationMap.put("customer user name is required", "userName:error.common.required");
        validationMap.put("customer legal name is required", "legalName:error.common.required");
        validationMap.put("account status is invalid", "accountEnabled:error.common.invalid");
        validationMap.put("confirm password is incorrect", "adminPass:error.common.invalid");
        validationMap.put("password not complex", "adminPass:error.user.password.not.complex");
        validationMap.put("duplicate user name", "userName:error.common.duplicated");
    }
    
    private void parseControllerResponse() {
        this.validationErrorInfo = null;
        final String controllerResponse =
                ((ControllerFieldsWrapper) TestDBMap.findObjectByIdFromMemory(TestDBMap.CONTROLLER_FIELDS_WRAPPER_ID, ControllerFieldsWrapper.class))
                        .getControllerResponse();
        @SuppressWarnings("unchecked")
        final WebSecurityForm<CustomerEditForm> webSecurityForm =
                (WebSecurityForm<CustomerEditForm>) TestDBMap.findObjectByIdFromMemory(TestDBMap.WEB_SECURITY_FORM_ID, WebSecurityForm.class);
        if (!"true".equalsIgnoreCase(controllerResponse) || controllerResponse.startsWith("{")) {
            final XStream xstream = new XStream(new JettisonMappedXmlDriver());
            xstream.alias(WebCoreConstants.JSON_ERROR_STATUS_LABEL, webSecurityForm.getValidationErrorInfo().getClass());
            this.validationErrorInfo = (ValidationErrorInfo) xstream.fromXML(controllerResponse);
        }
    }
    
    private String saveCustomer() {
        MockitoAnnotations.initMocks(this);
        String controllerResponse;
        
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final CustomerEditForm form = (CustomerEditForm) controllerFields.getForm();
        final MockHttpServletRequest request = controllerFields.getRequest();
        final MockHttpServletResponse response = controllerFields.getResponse();
        final BindingResult result = controllerFields.getResult();
        final ModelMap model = controllerFields.getModel();
        
        when(this.passwordEncoder.encodePassword(anyString(), Mockito.anyObject())).then(new Answer<String>() {
            
            @Override
            public String answer(final InvocationOnMock invocation) throws Throwable {
                final Object[] args = invocation.getArguments();
                return (String) args[0];
            }
        });
        when(this.customerService.findCustomer(anyInt())).thenAnswer(CustomerServiceMockImpl.findCustomer());
        when(this.customerAdminService.getTimeZones()).thenReturn(TestLookupTables.getTimeZoneList());
        when(this.systemAdminService.createCustomer(Matchers.<Customer>any(), Matchers.<UserAccount>any(), anyString()))
                .thenAnswer(SystemAdminServiceMockImpl.createCustomer());
        when(this.subscriptionTypeService.loadAll()).then(SubscriptionTypeServiceMockImpl.loadAll());
        this.systemAdminMainController.loadSubscriptionTypes();
        this.systemAdminMainController.setCustomerValidator(createCustomerValidator());
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                return (UserAccount) TestDBMap.findObjectByIdFromMemory(userAccountId, UserAccount.class);
            }
            
            public UserAccount findUndeletedUserAccount(final String encodedUserName) {
                return new UserAccount();
            }
        });
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(final Object entity) {
                return entity;
            }
        });
        
        final WebSecurityForm<CustomerEditForm> webSecurityForm = new WebSecurityForm<CustomerEditForm>(null, form);
        webSecurityForm.setInitToken(TestConstants.POST_TOKEN);
        webSecurityForm.setPostToken(TestConstants.POST_TOKEN);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        if (TestDBMap.findObjectByFieldFromMemory(((CustomerEditForm) form).getLegalName(), Customer.ALIAS_NAME, Customer.class) != null) {
            when(this.userAccountService.findUndeletedUserAccount(isA(String.class))).thenReturn(new UserAccount());
        }
        controllerResponse = this.systemAdminMainController.saveCustomer(webSecurityForm, result, request, response, model);
        TestDBMap.addToMemory(TestDBMap.WEB_SECURITY_FORM_ID, webSecurityForm);
        final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
        wrapperObject.setControllerResponse(controllerResponse);
        return controllerResponse;
    }
    
    private void assertCustomerSaveSuccess() {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final Map<String, Object> dataMap = (Map<String, Object>) controllerFields.getExpectedResult();
        final String customerName = (String) dataMap.get(TestConstants.CUSTOMER_NAME_STRING);
        final Customer customer = (Customer) TestDBMap.findObjectByFieldFromMemory(customerName, Customer.ALIAS_NAME, Customer.class);
        if (!(Boolean) dataMap.get(TestConstants.SUCCESS_STRING)) {
            assertTrue(customer == null);
        } else {
            assertTrue(customer != null);
            for (String key : dataMap.keySet()) {
                switch (key) {
                    case TestConstants.CUSTOMER_NAME_STRING:
                        Assert.assertEquals(dataMap.get(key), customer.getName());
                        break;
                    case TestConstants.CURRENT_TIME_ZONE_STRING:
                        Assert.assertEquals(dataMap.get(key), TestLookupTables.findMatchingTZById(customer.getTimezoneId()));
                        break;
                    case TestConstants.ACCOUNT_STATUS_STRING:
                        Assert.assertEquals(dataMap.get(key), getStatusTypeName(customer.getCustomerStatusType()));
                        break;
                    case TestConstants.PARENT_COMPANY_STRING:
                        Assert.assertEquals(dataMap.get(key), customer.isIsParent());
                        break;
                    case TestConstants.PARENT_COMPANY_NAME_STRING:
                        Assert.assertEquals(dataMap.get(key), customer.getParentCustomer().getName());
                        break;
                    default:
                        break;
                        
                }
            }
        }
    }
    
    private void assertCustomerValidationErrors() {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final Map<String, Object> dataMap = (Map<String, Object>) controllerFields.getExpectedResult();
        final String validationErrors = controllerFields.getData();
        if (!(Boolean) dataMap.get(TestConstants.SUCCESS_STRING)) {
            assertCustomerSaveFailure(validationErrors);
        }
    }
    
    private void assertCustomerSaveFailure(final String validationErrors) {
        parseControllerResponse();
        Assert.assertNotNull(this.validationErrorInfo);
        Assert.assertTrue(!this.validationErrorInfo.getErrorStatus().isEmpty());
        if (validationErrors != null && validationErrors.split(",").length == 1) {
            String expectedValidation = null;
            String expectedFieldName = null;
            String expectedMessage = null;
            try {
                expectedValidation = validationMap.get(validationErrors.toLowerCase());
                expectedFieldName = expectedValidation.split(StandardConstants.STRING_COLON)[0];
                expectedMessage = expectedValidation.split(StandardConstants.STRING_COLON)[1];
            } catch (NullPointerException e) {
                System.out.println("Check story for defined validation messages");
            }
            Assert.assertNotNull(this.validationErrorInfo);
            Assert.assertTrue(!this.validationErrorInfo.getErrorStatus().isEmpty());
            Assert.assertTrue(expectedFieldName.equals(((FormErrorStatus) (this.validationErrorInfo.getErrorStatus().get(0))).getErrorFieldName()));
            Assert.assertTrue(expectedMessage.equals(((FormErrorStatus) (this.validationErrorInfo.getErrorStatus().get(0))).getMessage()));
        } else if (validationErrors != null && validationErrors.split(StandardConstants.STRING_COMMA).length > 1) {
            final ArrayList<String> validationErrorsArray = new ArrayList<String>(Arrays.asList(validationErrors.split(WebCoreConstants.COMMA)));
            
            final ArrayList<String> cloneValidationErrorArray = new ArrayList<String>(validationErrorsArray);
            
            for (String error : validationErrorsArray) {
                final String expectedValidation = validationMap.get(error.trim().toLowerCase());
                final String expectedFieldName = expectedValidation.split(StandardConstants.STRING_COLON)[0];
                final String expectedMessage = expectedValidation.split(StandardConstants.STRING_COLON)[1];
                if (containsInvalidationErrorInfo(expectedFieldName, expectedMessage)) {
                    cloneValidationErrorArray.remove(error);
                }
            }
            if (cloneValidationErrorArray.size() > 0) {
                System.out.println("The following validations were not recieved " + cloneValidationErrorArray.toString());
            }
            Assert.assertNotNull(this.validationErrorInfo);
            Assert.assertTrue(cloneValidationErrorArray.size() == 0);
        } else {
            Assert.fail();
        }
    }
    
    private boolean containsInvalidationErrorInfo(final String expectedFieldName, final String expectedMessage) {
        boolean contains = false;
        for (FormErrorStatus error : this.validationErrorInfo.getErrorStatus()) {
            if (expectedFieldName.equalsIgnoreCase(error.getErrorFieldName()) && expectedMessage.equalsIgnoreCase(error.getMessage())) {
                contains = true;
            }
        }
        return contains;
    }
    
    private String getStatusTypeName(final CustomerStatusType statusType) {
        final String statusName = statusType.getId() == WebCoreConstants.CUSTOMER_STATUS_TYPE_ENABLED ? TestConstants.ENABLED_STRING
                : statusType.getId() == WebCoreConstants.CUSTOMER_STATUS_TYPE_DISABLED ? "trial"
                        : statusType.getId() == WebCoreConstants.CUSTOMER_STATUS_TYPE_DELETED ? "deleted" : "disabled";
        return statusName;
    }
    
    private CustomerValidator createCustomerValidator() {
        final CustomerValidator custValidator = new CustomerValidator() {
            @Override
            public String getMessage(final String code) {
                return code;
            }
            
            @Override
            public String getMessage(final String code, final Object... args) {
                return code;
            }
            
            @Override
            public String getMessage(final String code, final Locale locale) {
                return code;
            }
            
            @Override
            public String getMessage(final String code, final Object[] args, final Locale locale) {
                return code;
            }
        };
        custValidator.setCustomerService(new CustomerServiceTestImpl());
        custValidator.setCustomerAdminService(new CustomerAdminServiceTestImpl());
        return custValidator;
    }
    
    @Override
    public final Object performAction(final Object... objects) {
        saveCustomer();
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... objects) {
        return null;
    }
    
    @Override
    public final Object assertFailure(final Object... objects) {
        assertCustomerValidationErrors();
        return null;
    }
    
}
