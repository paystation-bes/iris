package com.digitalpaytech.bdd.mvc.customer;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.mvc.customeradmin.alertcentercontroller.TestClearActiveAlerts;
import com.digitalpaytech.bdd.mvc.customeradmin.alertcentercontroller.TestViewMaintenance;
import com.digitalpaytech.bdd.mvc.customeradmin.alertcentercontroller.TestViewPaystationActiveAlerts;
import com.digitalpaytech.bdd.mvc.customeradmin.customeradminpaystationdetailcontroller.TestClearPaystationActiveAlerts;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class CheckCustomerPermissionsStories extends AbstractStories {
    
    public CheckCustomerPermissionsStories() {
        super();
        this.testHandlers.registerTestHandler("maintenancetabview", TestViewMaintenance.class);
        this.testHandlers.registerTestHandler("maintenancetabshown", TestViewMaintenance.class);
        this.testHandlers.registerTestHandler("maintenancetabnotshown", TestViewMaintenance.class);
        this.testHandlers.registerTestHandler("paystationalertsview", TestViewPaystationActiveAlerts.class);
        this.testHandlers.registerTestHandler("paystationactivealertsshown", TestViewPaystationActiveAlerts.class);
        this.testHandlers.registerTestHandler("paystationactivealertsnotshown", TestClearActiveAlerts.class);
        this.testHandlers.registerTestHandler("activealertsnotcleared", TestClearActiveAlerts.class);
        this.testHandlers.registerTestHandler("activealertsclear", TestClearActiveAlerts.class);
        this.testHandlers.registerTestHandler("activealertscleared", TestClearActiveAlerts.class);
        this.testHandlers.registerTestHandler("activealertsonpaystationdetailsclear", TestClearPaystationActiveAlerts.class);
        this.testHandlers.registerTestHandler("activealertsonpaystationdetailscleared", TestClearPaystationActiveAlerts.class);
        this.testHandlers.registerTestHandler("activealertsonpaystationdetailsnotcleared", TestClearPaystationActiveAlerts.class);
        
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new ControllerBaseSteps(this.testHandlers));
    }    
}
