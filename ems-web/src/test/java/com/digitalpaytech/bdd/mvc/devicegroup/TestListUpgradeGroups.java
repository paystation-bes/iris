package com.digitalpaytech.bdd.mvc.devicegroup;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestConstants;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.client.FacilitiesManagementClient;
import com.digitalpaytech.client.dto.DeviceGroupFilter;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.dao.impl.OpenSessionDaoTest;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.customeradmin.CustomerAdminDeviceGroupController;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.impl.CommonControllerHelperImpl;
import com.digitalpaytech.ribbon.mock.MockClientFactory;
import com.digitalpaytech.service.impl.CustomerServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.util.WebSecurityUtil;

public class TestListUpgradeGroups implements JBehaveTestHandler {
    
    private MockClientFactory client = MockClientFactory.getInstance();
    
    @Mock
    private FacilitiesManagementClient facilitiesManagementClient;
    
    @InjectMocks
    private CommonControllerHelperImpl commonControllerHelper;
    
    @InjectMocks
    private CustomerServiceImpl customerService;
    
    @InjectMocks
    private UserAccountServiceImpl userAccountService;
    
    @InjectMocks
    private OpenSessionDaoTest openSessionDao;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private EntityServiceImpl entityService;
    
    @InjectMocks
    private CustomerAdminDeviceGroupController deviceConfig;
    
    public TestListUpgradeGroups() {
        MockitoAnnotations.initMocks(this);
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public final Object performAction(final Object... arg0) {
        
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        
        final WebSecurityUtil util = new WebSecurityUtil();
        
        util.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                return TestContext.getInstance().getDatabase().findById(UserAccount.class, userAccountId);
            }
            
            public UserAccount findUndeletedUserAccount(final String encodedUserName) {
                return new UserAccount();
            }
        });
        
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(final Object entity) {
                return entity;
            }
        });
        
        final MockHttpServletRequest request = controllerFields.getRequest();
        final MockHttpServletResponse response = controllerFields.getResponse();
        final ModelMap model = controllerFields.getModel();
        
        final DeviceGroupFilter form = (DeviceGroupFilter) controllerFields.getForm();
        
        final WebSecurityForm<DeviceGroupFilter> webSecurityForm = new WebSecurityForm<DeviceGroupFilter>(null, form);
        webSecurityForm.setInitToken(TestConstants.POST_TOKEN);
        webSecurityForm.setPostToken(TestConstants.POST_TOKEN);
        
        this.deviceConfig.init();
        final String cntrlrResp = this.deviceConfig.listUpgradeGroups(request, response, model);
        
        return cntrlrResp;
    }
    
    @Override
    public final Object assertFailure(final Object... arg0) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... arg0) {
        // TODO Auto-generated method stub
        return null;
    }
}
