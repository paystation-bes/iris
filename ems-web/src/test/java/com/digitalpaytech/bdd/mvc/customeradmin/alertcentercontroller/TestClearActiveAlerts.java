package com.digitalpaytech.bdd.mvc.customeradmin.alertcentercontroller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertTrue;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.customeradmin.AlertCentreController;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.impl.CommonControllerHelperImpl;
import com.digitalpaytech.scheduling.queue.alert.impl.PosEventCurrentProcessorImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.PosEventCurrentServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityUtil;

public class TestClearActiveAlerts implements JBehaveTestHandler {
    public static final String NO_PERMISSION_REDIRECT = "redirect:/";
    private static final String PAYSTATION_SERIAL_IS = "Serial number is ";
    
    @Mock
    private CommonControllerHelper commonControllerHelper;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointofSaleService;
    
    @InjectMocks
    private PosEventCurrentProcessorImpl posEventCurrentProcessor;
    
    @InjectMocks
    private PosEventCurrentServiceImpl posEventCurrentService;
    
    @InjectMocks
    private CommonControllerHelperImpl commonController;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private AlertCentreController acController;
    
    public TestClearActiveAlerts() {
        MockitoAnnotations.initMocks(this);
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    
    private String clearActiveAlerts(final String serialNumber) {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final MockHttpServletRequest request = controllerFields.getRequest();
        final MockHttpServletResponse response = controllerFields.getResponse();
        final ModelMap model = controllerFields.getModel();
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        final PointOfSale pos = TestContext.getInstance().getDatabase()
                .findOne(PointOfSale.class, PointOfSale.ALIAS_NAME, serialNumber.split(PAYSTATION_SERIAL_IS)[1]);
        request.setParameter("activePosAlertId", keyMapping.getRandomString(PosEventCurrent.class, pos.getId()));
        
        final WebSecurityUtil util = new WebSecurityUtil();
        
        util.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                return TestContext.getInstance().getDatabase().findById(UserAccount.class, userAccountId);
            }
            
            public UserAccount findUndeletedUserAccount(final String encodedUserName) {
                return new UserAccount();
            }
        });
        
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(final Object entity) {
                return entity;
            }
        });
        when(this.commonControllerHelper.clearActiveAlert(request, response, true)).thenReturn(WebCoreConstants.RESPONSE_TRUE);
        final String controllerResponse = this.acController.clearActiveAlert(request, response, model);
        final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
        wrapperObject.setControllerResponse(controllerResponse);
        return controllerResponse;
        
    }
    
    private void blockClearActiveAlerts() {
        final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
        assertThat(wrapperObject.getControllerResponse(), containsString(NO_PERMISSION_REDIRECT));
    }
    
    private void assertClearSuccess() {
        final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
        assertTrue(Boolean.parseBoolean(wrapperObject.getControllerResponse()));
    }
    
    @Override
    public final Object performAction(final Object... objects) {
        clearActiveAlerts((String) objects[0]);
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... objects) {
        assertClearSuccess();
        return null;
    }
    
    @Override
    public final Object assertFailure(final Object... objects) {
        blockClearActiveAlerts();
        return null;
    }
    
}
