package com.digitalpaytech.bdd.mvc.customeradmin.alertcentercontroller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

import junit.framework.Assert;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.ui.ModelMap;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestConstants;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.customeradmin.LocationRouteFilterInfo;
import com.digitalpaytech.mvc.customeradmin.AlertCentreController;
import com.digitalpaytech.mvc.customeradmin.support.AlertCentreFilterForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.impl.CommonControllerHelperImpl;
import com.digitalpaytech.service.AlertsService;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.LocationServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.RouteServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

public class TestViewMaintenance implements JBehaveTestHandler {
    public static final String MAINTAINCE_URL = "/alerts/index";
    public static final String NO_PERMISSION_REDIRECT = "redirect:/";
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    private Location location;
    
    @Mock
    private AlertsService alertsService;
    
    @Mock
    private LocationRouteFilterInfo locationRouteFilterInfo;
    
    @Mock
    private UserAccountServiceImpl userAccountService;
    
    @Mock
    private UserAccount userAccount;
    
    @InjectMocks
    private CommonControllerHelperImpl commonControllerHelper;
    
    @InjectMocks
    private LocationServiceImpl locationService;
    
    @InjectMocks
    private RouteServiceImpl routeService;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private AlertCentreController acController;
    
    public TestViewMaintenance() {
        MockitoAnnotations.initMocks(this);
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    
    private String viewMaintenanceTab() {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final MockHttpServletRequest request = controllerFields.getRequest();
        final MockHttpServletResponse response = controllerFields.getResponse();
        final ModelMap model = controllerFields.getModel();
        final AlertCentreFilterForm form = (AlertCentreFilterForm) controllerFields.getForm();
        
        final WebSecurityUtil util = new WebSecurityUtil();
        
        util.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                return TestContext.getInstance().getDatabase().findById(UserAccount.class, userAccountId);
            }
            
            public UserAccount findUndeletedUserAccount(final String encodedUserName) {
                return new UserAccount();
            }
        });
        
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(final Object entity) {
                return entity;
            }
        });
        
        final WebSecurityForm<AlertCentreFilterForm> webSecurityForm = new WebSecurityForm<AlertCentreFilterForm>(null, form);
        webSecurityForm.setInitToken(TestConstants.POST_TOKEN);
        webSecurityForm.setPostToken(TestConstants.POST_TOKEN);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        
        final String controllerResponse = this.acController.alertCentreIndex(request, response, model, webSecurityForm);
        TestContext.getInstance().getCache().save(WebSecurityForm.class, TestDBMap.WEB_SECURITY_FORM_ID, webSecurityForm);
        final ControllerFieldsWrapper wrapperObject = TestDBMap.getControllerFieldsWrapperFromMem();
        wrapperObject.setControllerResponse(controllerResponse);
        return controllerResponse;
    }
    
    private void assertMaintenanceTab() {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        Assert.assertEquals(controllerFields.getControllerResponse(), MAINTAINCE_URL);
    }
    
    private void blockMaintenanceTab() {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        assertThat(controllerFields.getControllerResponse(), containsString(NO_PERMISSION_REDIRECT));
    }
    
    @Override
    public final Object performAction(final Object... objects) {
        viewMaintenanceTab();
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... objects) {
        assertMaintenanceTab();
        return null;
    }
    
    @Override
    public final Object assertFailure(final Object... objects) {
        blockMaintenanceTab();
        return null;
    }
    
}
