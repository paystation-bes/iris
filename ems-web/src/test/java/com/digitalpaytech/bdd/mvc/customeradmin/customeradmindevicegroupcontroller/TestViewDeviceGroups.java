package com.digitalpaytech.bdd.mvc.customeradmin.customeradmindevicegroupcontroller;


import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.support.MessageSourceAccessor;

import com.digitalpaytech.bdd.services.CommonControllerHelperMockImpl;
import com.digitalpaytech.bdd.services.CustomerServiceMockImpl;
import com.digitalpaytech.bdd.services.MessageHelperMockImpl;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestConstants;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.client.FacilitiesManagementClient;
import com.digitalpaytech.client.dto.DeviceGroupFilter;
import com.digitalpaytech.dao.EntityDao;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.UserAccount;

import com.digitalpaytech.mvc.systemadmin.SystemAdminDeviceGroupController;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.ribbon.mock.MockClientFactory;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.customeradmin.DeviceGroupSearchValidator;
import com.digitalpaytech.validation.customeradmin.DeviceGroupValidator;

public class TestViewDeviceGroups implements JBehaveTestHandler {
    @Mock
    private MessageSourceAccessor messageAccessor;
    
    @Mock
    private CommonControllerHelper commonControllerHelper;
    
    @Mock
    private CustomerService customerService;
    
    @InjectMocks
    private UserAccountServiceImpl userAccountService;
    
    @Mock
    private DeviceGroupValidator editValidator;
    
    @Mock
    private DeviceGroupSearchValidator searchValidator;
    
    @InjectMocks
    private SystemAdminDeviceGroupController controller;
    
    private EntityDao entityDao = new EntityDaoTest();
    
    private MockClientFactory client = MockClientFactory.getInstance();
    
    public TestViewDeviceGroups() {
        MockitoAnnotations.initMocks(this);
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
            this.controller.init();
            
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        
        Mockito.when(this.messageAccessor.getMessage(Mockito.anyString())).then(MessageHelperMockImpl.getMessageEcho());
        Mockito.when(this.customerService.findCustomer(Mockito.anyObject())).thenAnswer(CustomerServiceMockImpl.findCustomer());
        Mockito.when(this.commonControllerHelper.resolveCustomerId(Mockito.anyObject(), 
                                                                   Mockito.anyObject())).thenAnswer(CommonControllerHelperMockImpl.resolveCustomerId());
                                                                                                                         
    }
    
    @Override
    public final Object performAction(final Object... objects) {
        final TestContext ctx = TestContext.getInstance();
        
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        
        final WebSecurityUtil util = new WebSecurityUtil();
        
        util.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                return TestContext.getInstance().getDatabase().findById(UserAccount.class, userAccountId);
            }
            
            public UserAccount findUndeletedUserAccount(final String encodedUserName) {
                return new UserAccount();
            }
        });
        
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(final Object entity) {
                return entity;
            }
        });
        
        final DeviceGroupFilter form = new DeviceGroupFilter();
        ctx.getObjectParser().parse((String) objects[0], form);
        
        final WebSecurityForm<DeviceGroupFilter> webSecurityForm = new WebSecurityForm<DeviceGroupFilter>(null, form);
        webSecurityForm.setInitToken(TestConstants.POST_TOKEN);
        webSecurityForm.setPostToken(TestConstants.POST_TOKEN);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        
        this.client.prepareForSuccessRequest(FacilitiesManagementClient.class, "[]".getBytes());
        
        this.controller.list(webSecurityForm, controllerFields.getResult(), controllerFields.getRequest(), controllerFields.getResponse(),
                             controllerFields.getModel());
        
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... objects) {
        return null;
    }
    
    @Override
    public final Object assertFailure(final Object... objects) {
        return null;
    }
}
