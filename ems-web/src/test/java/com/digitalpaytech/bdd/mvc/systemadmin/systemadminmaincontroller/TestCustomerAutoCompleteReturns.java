package com.digitalpaytech.bdd.mvc.systemadmin.systemadminmaincontroller;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import com.digitalpaytech.bdd.services.CustomerServiceMockImpl;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.mvc.systemadmin.SystemAdminMainController;
import com.digitalpaytech.service.CustomerService;

import junit.framework.Assert;

public class TestCustomerAutoCompleteReturns implements JBehaveTestHandler {
    @Mock
    private CustomerService customerService;
    @InjectMocks
    private SystemAdminMainController systemAdminMainController;
    
    private final void enterSearchString(final String searchString) {
        MockitoAnnotations.initMocks(this);
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final MockHttpServletRequest request = controllerFields.getRequest();
        final MockHttpServletResponse response = controllerFields.getResponse();
        when(this.customerService.findAllParentAndChildCustomersBySearchKeyword(anyString(), Matchers.<Integer> anyVararg()))
                .thenAnswer(CustomerServiceMockImpl.findAllParentAndChildCustomersBySearchKeyword());
        final ControllerFieldsWrapper controllerFieldsWrapper = TestDBMap.getControllerFieldsWrapperFromMem();
        request.setParameter("search", searchString);
        controllerFieldsWrapper.setRequest(request);
        controllerFieldsWrapper.setControllerResponse(this.systemAdminMainController.searchCustomerAndPos(request, response));
        TestDBMap.saveControllerFieldsWrapperToMem(controllerFieldsWrapper);
    }
    
    @SuppressWarnings("unused")
    private final void assertReturnValue(final String expectedString) {
        final ControllerFieldsWrapper controllerFieldsWrapper = TestDBMap.getControllerFieldsWrapperFromMem();
        final String autoCompleteResult = controllerFieldsWrapper.getControllerResponse();
        Boolean found = false;
        try {
            final JSONObject resultObject = new JSONObject(autoCompleteResult);
            final JSONObject searchCustomerResult = resultObject.getJSONObject("searchCustomerResult");
            final JSONArray customerSearchResult = searchCustomerResult.optJSONArray("customerSearchResult");
            for (int i = 0; i < customerSearchResult.length(); i++) {
                final JSONObject currentResult = customerSearchResult.optJSONObject(i);
                if (currentResult != null) {
                    final JSONObject curCustomerResult = currentResult.optJSONObject("com.digitalpaytech.dto.SearchCustomerResultSetting");
                    final String name = curCustomerResult.getString("name");
                    final String parentName = curCustomerResult.optString("parentName");
                    if (parentName.isEmpty()) {
                        found = expectedString.equals(name);
                    } else {
                        found = expectedString.equals(name + " (" + parentName + ")");
                    }
                }
                
            }
            //TODO parse the results for POS search
            final JSONArray posSearchResult = searchCustomerResult.optJSONArray("posSearchResult");
            final JSONArray posNameSearchResult = searchCustomerResult.optJSONArray("posNameSearchResult");
            
        } catch (JSONException e) {
            Assert.fail(e.toString() + autoCompleteResult);
        }
        Assert.assertTrue("AutoComplete does not match expected:" + autoCompleteResult, found);
    }
    
    @Override
    public Object performAction(Object... objects) {
        enterSearchString((String) objects[0]);
        return null;
    }
    
    @Override
    public Object assertSuccess(Object... objects) {
        assertReturnValue((String) objects[0]);
        return null;
    }
    
    @Override
    public Object assertFailure(Object... objects) {
        return null;
    }
    
}
