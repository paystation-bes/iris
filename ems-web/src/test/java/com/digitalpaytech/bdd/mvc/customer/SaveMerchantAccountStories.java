package com.digitalpaytech.bdd.mvc.customer;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.mvc.systemadmin.merchantaccountcontroller.TestMerchantAccountDetails;
import com.digitalpaytech.bdd.mvc.systemadmin.merchantaccountcontroller.TestMerchantAccountSave;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.mvc.systemadmin.support.MerchantAccountEditForm;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class SaveMerchantAccountStories extends AbstractStories {
    
    public SaveMerchantAccountStories() {
        super();
        this.testHandlers.registerTestHandler("merchantaccountsave", TestMerchantAccountSave.class);
        this.testHandlers.registerTestHandler("merchantaccountdetailswindowview", TestMerchantAccountDetails.class);
        this.testHandlers.registerTestHandler("merchantaccountdetailsshown", TestMerchantAccountDetails.class);
        TestContext.getInstance().getObjectParser().register(new Class[] { MerchantAccountEditForm.class, Processor.class });
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new ControllerBaseSteps(this.testHandlers));
    }
    
}
