package com.digitalpaytech.bdd.mvc.systemadmin.merchantaccountcontroller;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.ui.ModelMap;

import com.beanstream.Gateway;
import com.beanstream.exceptions.BeanstreamApiException;
import com.beanstream.requests.CardPaymentRequest;
import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestBeanstreamException;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.cardprocessing.impl.CardProcessingManagerImpl;
import com.digitalpaytech.cardprocessing.impl.CardProcessorFactoryImpl;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.systemadmin.MerchantAccountController;
import com.digitalpaytech.service.CommonProcessingService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.impl.CardTypeServiceImpl;
import com.digitalpaytech.service.impl.CreditCardTypeServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.ProcessorServiceImpl;
import com.digitalpaytech.service.impl.CreditCardTestNumberServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.service.impl.WebWidgetHelperServiceImpl;
import com.digitalpaytech.service.kpi.impl.KPIListenerServiceImpl;
import com.digitalpaytech.service.processor.TDMerchantCommunicationService;
import com.digitalpaytech.service.systemadmin.SystemAdminService;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.ReportingUtil;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.impl.TransactionFacadeImpl;
import com.digitalpaytech.validation.systemadmin.MerchantAccountValidator;

@SuppressWarnings({ "PMD.UnusedPrivateField", "PMD.TooManyFields", "PMD.ExcessiveImports" })
public class TestMerchantAccountValidate implements JBehaveTestHandler {
    
    private static final int TEST_HTTP_STATUS_FAIL = 400;
    
    private static final int CATEGORY_ONE = 1;
    
    private static final int TEST_FAIL_MESSAGE_CODE = 7;
    
    private static final String MERCHANT_ACCOUNT_SEARCH_STRING = "name is ";
    
    @Mock
    private MerchantAccountValidator merchantAccountValidator;
    
    @Mock
    private CommonProcessingService commonProcessingService;
    
    @Mock
    private EmsPropertiesService emsPropertiesService;
    
    @Mock
    private CustomerCardTypeService customerCardTypeService;
    
    @Mock
    private EntityService entityService;
    
    @Mock
    private SystemAdminService systemAdminService;
    
    @Mock
    private CustomerService customerService;
    
    @Mock
    private CustomerAdminService customerAdminService;
    
    @Mock
    private ReportingUtil reportingUtil;
    
    @Mock
    private MessageHelper messages;
    
    @Mock
    private TDMerchantCommunicationService tdMerchantCommunicationService;
    
    @InjectMocks
    private CreditCardTypeServiceImpl creditCardTypeService;
    
    @InjectMocks
    private KPIListenerServiceImpl kPIListenerServiceImpl;
    
    @InjectMocks
    private TransactionFacadeImpl transactionFacade;
    
    @InjectMocks
    private CardTypeServiceImpl cardTypeService;
    
    @InjectMocks
    private WebWidgetHelperServiceImpl webWidgetHelperService;
    
    @InjectMocks
    private ProcessorServiceImpl processorService;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    private EntityDaoTest entityDao;
    
    @InjectMocks
    private CardProcessingManagerImpl cardProcessingManager;
    
    @InjectMocks
    private MerchantAccountServiceImpl merchantAccountService;
    
    @InjectMocks
    private CardProcessorFactoryImpl cardProcessorFactory;
    
    @InjectMocks
    private MerchantAccountController merchantAccountController;
    
    @InjectMocks
    private CreditCardTestNumberServiceImpl testCreditCardNumberService;
    
    public TestMerchantAccountValidate() {
        MockitoAnnotations.initMocks(this);
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        
        try {
            Mockito.when(this.tdMerchantCommunicationService.makePayment(Mockito.any(Gateway.class), Mockito.any(CardPaymentRequest.class)))
                    .thenThrow(new TestBeanstreamException(TEST_FAIL_MESSAGE_CODE, CATEGORY_ONE, "", TEST_HTTP_STATUS_FAIL, false));
        } catch (BeanstreamApiException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    private void testMerchantAccount(final String merchantAccountName) {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final MockHttpServletRequest request = controllerFields.getRequest();
        final MockHttpServletResponse response = controllerFields.getResponse();
        final ModelMap model = controllerFields.getModel();
        final MerchantAccount ma = (MerchantAccount) TestContext.getInstance().getDatabase()
                .findOne(MerchantAccount.class, MerchantAccount.ALIAS_NAME, merchantAccountName.split(MERCHANT_ACCOUNT_SEARCH_STRING)[1]);
        request.setParameter("merchantAccountId",
                             SessionTool.getInstance(request).getKeyMapping().getRandomString(MerchantAccount.class, ma.getId()));
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                return (UserAccount) TestDBMap.findObjectByIdFromMemory(userAccountId, UserAccount.class);
            }
            
            public UserAccount findUndeletedUserAccount(final String encodedUserName) {
                return new UserAccount();
            }
        });
        
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(final Object entity) {
                return entity;
            }
        });
        
        controllerFields.setControllerResponse(this.merchantAccountController.testMerchantAccount(request, response, model));
    }
    
    @Override
    public final Object performAction(final Object... objects) {
        testMerchantAccount((String) objects[0]);
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... objects) {
        return null;
    }
    
    @Override
    public final Object assertFailure(final Object... objects) {
        return null;
    }
    
}
