package com.digitalpaytech.bdd.mvc.customeradmin.alertcentrecontroller;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;

import com.digitalpaytech.bdd.util.ControllerFieldsWrapper;
import com.digitalpaytech.bdd.util.JBehaveTestHandler;
import com.digitalpaytech.bdd.util.TestConstants;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.bdd.util.TestDBMap;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.customeradmin.AlertCentreController;
import com.digitalpaytech.mvc.customeradmin.support.AlertCentreFilterForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.impl.CommonControllerHelperImpl;
import com.digitalpaytech.service.impl.AlertsServiceImpl;
import com.digitalpaytech.service.impl.CustomerAdminServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.LocationServiceImpl;
import com.digitalpaytech.service.impl.MobileLicenseServiceImpl;
import com.digitalpaytech.service.impl.MobileSessionTokenServiceImpl;
import com.digitalpaytech.service.impl.PointOfSaleServiceImpl;
import com.digitalpaytech.service.impl.PosAlertStatusServiceImpl;
import com.digitalpaytech.service.impl.PosEventCurrentServiceImpl;
import com.digitalpaytech.service.impl.PosEventHistoryServiceImpl;
import com.digitalpaytech.service.impl.ReportDefinitionServiceImpl;
import com.digitalpaytech.service.impl.RouteServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.customeradmin.AlertCentreFilterValidator;

public class TestPaystationSummary implements JBehaveTestHandler {
    
    private static final int PAGE_NUMBER = 1;
    private static final String ORDER = "ASC";
    private static final String COLUMN_NAME = "pointOfSaleName";
    
    @Mock
    private AlertCentreFilterValidator alertCentreFilterValidator;
    
    @InjectMocks
    private EntityDaoTest entityDaoTest;
    
    @InjectMocks
    private MobileSessionTokenServiceImpl mobileSessionTokenService;
    
    @InjectMocks
    private MobileLicenseServiceImpl mobileAppService;
    
    @InjectMocks
    private LocationServiceImpl locationService;
    
    @InjectMocks
    private RouteServiceImpl routeService;
    
    @InjectMocks
    private PointOfSaleServiceImpl pointOfSaleService;
    
    @InjectMocks
    private AlertsServiceImpl alertsService;
    
    @InjectMocks
    private CustomerAdminServiceImpl customerAdminService;
    
    @InjectMocks
    private CommonControllerHelperImpl commonControllerHelper;
    
    @InjectMocks
    private ReportDefinitionServiceImpl reportDefinitionService;
    
    @InjectMocks
    private PosAlertStatusServiceImpl posAlertStatusService;
    
    @InjectMocks
    private PosEventCurrentServiceImpl posEventCurrentService;
    
    @InjectMocks
    private PosEventHistoryServiceImpl posEventHistoryService;
    
    @InjectMocks
    private MessageHelper messageHelper;
    
    @InjectMocks
    private UserAccountServiceImpl userAccountService;
    
    @InjectMocks
    private AlertCentreController alertsCentreController;
    
    public TestPaystationSummary() {
        MockitoAnnotations.initMocks(this);
        try {
            TestContext.getInstance().autowiresFromTestObject(this);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    
    private void loadPayStationSummary() {
        final ControllerFieldsWrapper controllerFields = TestDBMap.getControllerFieldsWrapperFromMem();
        final MockHttpServletRequest request = controllerFields.getRequest();
        final MockHttpServletResponse response = controllerFields.getResponse();
        final ModelMap model = controllerFields.getModel();
        final AlertCentreFilterForm form = new AlertCentreFilterForm();
        
        form.setColumn(COLUMN_NAME);
        form.setPage(PAGE_NUMBER);
        form.setOrder(ORDER);
        form.setPageNumber(PAGE_NUMBER);
        form.setRandomId(WebCoreConstants.BLANK);
        form.setSeverityRandomId(WebCoreConstants.BLANK);
        
        final WebSecurityUtil util = new WebSecurityUtil();
        
        util.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                return (UserAccount) TestDBMap.findObjectByIdFromMemory(userAccountId, UserAccount.class);
            }
            
            public UserAccount findUndeletedUserAccount(final String encodedUserName) {
                return new UserAccount();
            }
        });
        
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(final Object entity) {
                return entity;
            }
        });
        
        final WebSecurityForm<AlertCentreFilterForm> webSecurityForm = new WebSecurityForm<AlertCentreFilterForm>(null, form);
        webSecurityForm.setInitToken(TestConstants.POST_TOKEN);
        webSecurityForm.setPostToken(TestConstants.POST_TOKEN);
        final BindingResult result = new BeanPropertyBindingResult(form, "alertsForm");
        this.alertsCentreController.getPosSummaryList(webSecurityForm, result, request, response, model);
        
    }
    
    @Override
    public final Object performAction(final Object... objects) {
        loadPayStationSummary();
        return null;
    }
    
    @Override
    public final Object assertSuccess(final Object... objects) {
        
        return null;
    }
    
    @Override
    public final Object assertFailure(final Object... objects) {
        
        return null;
    }
    
}
