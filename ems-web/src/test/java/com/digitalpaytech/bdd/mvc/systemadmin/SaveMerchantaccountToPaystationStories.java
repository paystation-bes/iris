package com.digitalpaytech.bdd.mvc.systemadmin;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.mvc.systemadmin.merchantaccountcontroller.TestMerchantAccountDelete;
import com.digitalpaytech.bdd.mvc.systemadmin.merchantaccountcontroller.TestMerchantAccountSave;
import com.digitalpaytech.bdd.mvc.systemadmin.systemadminpaystationlistdetailcontroller.TestSavePOS;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.mvc.systemadmin.support.MerchantAccountEditForm;
import com.digitalpaytech.mvc.systemadmin.support.SysAdminPayStationEditForm;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class SaveMerchantaccountToPaystationStories extends AbstractStories {
    
    public SaveMerchantaccountToPaystationStories() {
        this.testHandlers.registerTestHandler("paystationsave", TestSavePOS.class);
        this.testHandlers.registerTestHandler("merchantaccountsave", TestMerchantAccountSave.class);
        this.testHandlers.registerTestHandler("merchantaccountdelete", TestMerchantAccountDelete.class);
        TestContext.getInstance().getObjectParser().register(new Class[] { SysAdminPayStationEditForm.class, MerchantAccountEditForm.class });
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        // varargs, can have more that one steps classes
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new ControllerBaseSteps(this.testHandlers));
    }
    
}
