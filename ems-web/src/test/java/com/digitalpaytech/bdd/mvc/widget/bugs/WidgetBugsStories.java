package com.digitalpaytech.bdd.mvc.widget.bugs;

import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.runner.RunWith;

import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.mvc.location.AddLocationSteps;
import com.digitalpaytech.bdd.mvc.widget.WidgetSteps;
import com.digitalpaytech.bdd.util.AbstractSteps;
import com.digitalpaytech.bdd.util.AbstractStories;
import com.digitalpaytech.bdd.util.BaseSteps;

import de.codecentric.jbehave.junit.monitoring.JUnitReportingRunner;

@RunWith(JUnitReportingRunner.class)
public class WidgetBugsStories  extends AbstractStories {
    
    public WidgetBugsStories() {
        JUnitReportingRunner.recommendedControls(configuredEmbedder());
    }
    
    @Override
    public final InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new BaseSteps(this.testHandlers), new WidgetSteps(this.testHandlers),
                new ControllerBaseSteps(this.testHandlers), new AddLocationSteps(this.testHandlers));
    }
}
