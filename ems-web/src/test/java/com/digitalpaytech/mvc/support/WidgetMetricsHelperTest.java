package com.digitalpaytech.mvc.support;

import org.junit.Test;
import static org.junit.Assert.*;
import java.io.*;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.digitalpaytech.dto.LocationType;
import com.digitalpaytech.dto.RevType;
import com.digitalpaytech.dto.RouteType;
import com.digitalpaytech.dto.TxnType;
import com.digitalpaytech.dto.MetricType;
import com.digitalpaytech.dto.TierType;
import com.digitalpaytech.dto.WidgetMetrics;
import com.digitalpaytech.dto.Metric;
import com.digitalpaytech.dto.Range;
import com.digitalpaytech.dto.Tier1;
import com.digitalpaytech.dto.Tier2;
import com.digitalpaytech.dto.Tier3;
import com.digitalpaytech.dto.WidgetSettingsOptions;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.util.WebCoreConstants;

public class WidgetMetricsHelperTest {

	private static Logger log = Logger.getLogger(WidgetMetricsHelperTest.class);

	@Test
	public void convertToJson() {

		WidgetSettingsOptions opts = createWidgetSettingsOptions();
		WidgetMetricsHelper.registerComponents();
		String str = WidgetMetricsHelper.convertToJson(opts, "widgetSettingsOptions", true);
		assertNotNull(str);
//		log.info(str);
	}
	
	
	@Test
	public void loadMetricXml() {
		InputStream is = null;
		try {
			is = loadXml();
			WidgetMetrics widgetMetrics = WidgetMetricsHelper.loadWidgetMetricsXml("/widgetmetrics.xml", is);
			assertNotNull(widgetMetrics);
			
			StringBuilder bdr = new StringBuilder();
			Metric metric;
			Range range;
			Tier1 tier1;
			Tier2 tier2;
			Tier3 tier3;
			Iterator<Tier1> tier1Iter;
			Iterator<Tier2> tier2Iter;
			Iterator<Tier3> tier3Iter;
			
			
			Iterator<Metric> metricIter = widgetMetrics.getMetrics().iterator();			
			while (metricIter.hasNext()) {
				metric = metricIter.next();
    			log.debug("metric name: " + metric.getName() + ", ranges size: " + metric.getRanges().size());
    			Iterator<Range> iter = metric.getRanges().iterator();
    			while (iter.hasNext()) {
    				range = iter.next();
    				bdr.append("range type: ").append(range.getType());
    				if (range.getTier1s() != null) {
    					tier1Iter = range.getTier1s().iterator();
    					while (tier1Iter.hasNext()) {
    						tier1 = tier1Iter.next();
    						bdr.append(", tier 1 type: ").append(tier1.getType());
    						
    						if (tier1.getTier2s() != null) {
    							tier2Iter = tier1.getTier2s().iterator();
    							while (tier2Iter.hasNext()) {
    								tier2 = tier2Iter.next();
    								bdr.append("\ntier 2 type: ").append(tier2.getType());
    								
    								if (tier2.getTier3s() != null) {
    									tier3Iter = tier2.getTier3s().iterator();
    									while (tier3Iter.hasNext()) {
    										tier3 = tier3Iter.next();
    										bdr.append("\ntier 3 type: ").append(tier3.getType());
    									}
    								}
    							}
    						}
    					}
    				}
    				log.debug(bdr.toString());
    				log.debug("\n\n");
    				bdr.replace(0, bdr.length(), "");			
    			}
			}			
		} catch (Exception e) {
			log.error(e);
			fail(e.getMessage());
		
		} finally {
			if (is != null) {
				try { is.close(); } catch (Exception e) { e.printStackTrace(); }
			}
		}
	}
	
	
	private WidgetSettingsOptions createWidgetSettingsOptions() {
		
		WidgetSettingsOptions options = new WidgetSettingsOptions();
		
		MetricType metricType1 = new MetricType();
		metricType1.setName("Revenue");
		metricType1.setStatus(WebCoreConstants.SELECTED);
		metricType1.setRandomId("RANDOM_ID111");
		MetricType metricType2 = new MetricType();
		metricType2.setName("Purchases");
		metricType2.setStatus(WebCoreConstants.ACTIVE);
		metricType2.setRandomId("RANDOM_ID222");
		MetricType metricType3 = new MetricType();
		metricType3.setName("Collections");		
		metricType3.setStatus(WebCoreConstants.IN_ACTIVE);
		metricType3.setRandomId("RANDOM_ID333");
		List<MetricType> metricTypes = new ArrayList<MetricType>();
		metricTypes.add(metricType1);
		metricTypes.add(metricType2);
		metricTypes.add(metricType3);
		options.setMetricTypes(metricTypes);
		
		TierType tierType11 = new TierType();
		tierType11.setName("Hour");
		tierType11.setStatus(WebCoreConstants.SELECTED);
		tierType11.setRandomId("RANDOM_ID 111");
		TierType tierType12 = new TierType();
		tierType12.setName("Route");
		tierType12.setStatus(WebCoreConstants.ACTIVE);
		tierType12.setRandomId("222 RANDOM_ID 222");
		TierType tierType13 = new TierType();
		tierType13.setName("Transaction Type");
		tierType13.setStatus(WebCoreConstants.ACTIVE);
		tierType13.setRandomId("333 RANDOM_ID333");
		List<TierType> tier1Types = new ArrayList<TierType>();
		tier1Types.add(tierType11);
		tier1Types.add(tierType12);
		tier1Types.add(tierType13);
		options.setTier1Types(tier1Types);
		
		TierType tierType21 = new TierType();
		tierType21.setName("Hour");
		TierType tierType22 = new TierType();
		tierType22.setName("Route");
		TierType tierType23 = new TierType();
		tierType23.setName("Transaction Type");
		List<TierType> tier2Types = new ArrayList<TierType>();
		tier2Types.add(tierType21);
		tier2Types.add(tierType22);
		tier2Types.add(tierType23);	
		options.setTier2Types(tier2Types);
		
		TierType tierType31 = new TierType();
		tierType31.setName("Hour");
		TierType tierType32 = new TierType();
		tierType32.setName("Route");
		TierType tierType33 = new TierType();
		tierType33.setName("Transaction Type");
		List<TierType> tier3Types = new ArrayList<TierType>();
		tier3Types.add(tierType31);
		tier3Types.add(tierType32);
		tier3Types.add(tierType33);	
		options.setTier3Types(tier3Types);
		
		LocationType loc1 = new LocationType();
		loc1.setName("123 Test Street");
		LocationType loc2 = new LocationType();
		loc2.setName("1503 Broadway");
		LocationType loc3 = new LocationType();
		loc3.setName("Convention Center");		
		List<LocationType> locations = new ArrayList<LocationType>();
		locations.add(loc1);
		locations.add(loc2);
		locations.add(loc3);
		options.setLocationTypes(locations);
		
		
		List<RouteType> routes = new ArrayList<RouteType>();
		RouteType r1 = new RouteType();
		r1.setName("Collections 1");
		RouteType r2 = new RouteType();
		r2.setName("Maintenance 2");
		routes.add(r1);
		routes.add(r2);
		options.setRouteTypes(routes);
		
		
		List<RevType> revenueTypes = new ArrayList<RevType>();
		RevType rtype1 = new RevType();
		rtype1.setName("Total");
		RevType rtype2 = new RevType();
		rtype2.setName("Bill");
		RevType rtype3 = new RevType();
		rtype3.setName("Credit Card");
		revenueTypes.add(rtype1);
		revenueTypes.add(rtype2);
		revenueTypes.add(rtype3);
		options.setRevenueTypes(revenueTypes);
		
		List<TxnType> transactionTypes = new ArrayList<TxnType>();
		TxnType ttype1 = new TxnType();
		ttype1.setName("AddTime");
		TxnType ttype2 = new TxnType();
		ttype2.setName("Extend by Phone Permit");
		transactionTypes.add(ttype1);
		transactionTypes.add(ttype2);
		options.setTransactionTypes(transactionTypes);
		
		return options;
	}
	
	
	
	private InputStream loadXml() throws Exception {
		InputStream is = WidgetMetricsHelperTest.class.getResourceAsStream("/widgetmetrics.xml");
		return is;
	}
}
