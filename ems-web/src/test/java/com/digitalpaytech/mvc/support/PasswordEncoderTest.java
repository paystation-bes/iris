package com.digitalpaytech.mvc.support;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.authentication.dao .SaltSource;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.beans.factory.annotation.Autowired;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

//import com.digitalpaytech.auth.WebUser;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations={"classpath:ems-test.xml", "classpath:ems-security.xml"})
//@Transactional
public class PasswordEncoderTest {
	
//	private static String passwd = "Password$1";
	private static String passwd = "password";
	
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private SaltSource saltSource;
	
	@Test
	public void encodePassword() {
//		Authentication authentication = createAuthentication();
//		WebUser webUser = (WebUser) authentication.getPrincipal();
//		String encPassword = passwordEncoder.encodePassword(passwd, this.saltSource.getSalt(webUser));
//		System.out.println("--------------- password: " + encPassword);
	}
	
	
//	private Authentication createAuthentication() {
//		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
//		authorities.add(new SimpleGrantedAuthority("Admin"));
//		WebUser user = new WebUser(1, 1, "test", passwd, "salt", true, authorities);
//		return new UsernamePasswordAuthenticationToken(user, passwd, authorities);
//	}
}
