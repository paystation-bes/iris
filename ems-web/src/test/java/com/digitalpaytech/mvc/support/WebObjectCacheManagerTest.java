package com.digitalpaytech.mvc.support;

//import org.junit.Test;
//import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Before;

import com.digitalpaytech.dto.WidgetData;
import com.digitalpaytech.dto.WidgetSeries;
import com.digitalpaytech.util.cache.MemoryObjectCache;

public class WebObjectCacheManagerTest {
    private static final Logger LOG = Logger.getLogger(WebObjectCacheManagerTest.class);
    private WebObjectCacheManager mgr;
    
    @Before
    public void setUp() {
        this.mgr = new WebObjectCacheManager(new MemoryObjectCache());
    }
    
    // @Test
    // public void cacheWidgetData() {
    //         put5WidgetDataToCache();
    //         Statistics stats = CacheManager.create().getCache(WidgetData.class.getName()).getStatistics();
    //         log.info(stats.toString());
    //         
    //         assertNotNull(stats);
    //         if (stats.toString().indexOf("size = 5") == -1) {
    //                 fail();
    //         }
    // }
    
    // @Test
    // public void getWidgetData() {
    //         put5WidgetDataToCache();
    //         WidgetData data = mgr.getWidgetData("CACHEKEY555");
    //         
    //         assertEquals("single", data.getSingleValue());
    //         assertEquals(1000000000, data.getStartTime());
    //         assertEquals(3, data.getSeriesMap().get("test1").getSeriesData().size());
    //         assertEquals(3, data.getTier1Names().size());
    //         
    //         // It's not in cache, wrong cache key.
    //         assertNull(mgr.getWidgetData("CACHE555"));
    // }
    // 
    // @Test
    // public void isWidgetDataInCache() {
    //         put5WidgetDataToCache();
    //         
    //         assertTrue(mgr.isWidgetDataInCache("CACHEKEY222"));
    //         assertTrue(mgr.isWidgetDataInCache("CACHEKEY555"));
    //         assertTrue(mgr.isWidgetDataInCache("CACHEKEY333"));
    //         assertTrue(mgr.isWidgetDataInCache("CACHEKEY111"));
    //         assertTrue(mgr.isWidgetDataInCache("CACHEKEY444"));
    //         
    //         assertFalse(mgr.isWidgetDataInCache("CACHEKEY666"));
    //         assertFalse(mgr.isWidgetDataInCache("CACHEKEY777"));
    // }
    // 
    // 
    // private void put5WidgetDataToCache() {
    //         WidgetData data1 = createWidgetData(1, "seriesName111", "stackName111");
    //         WidgetData data2 = createWidgetData(2, "seriesName222", "stackName222");
    //         WidgetData data3 = createWidgetData(3, "seriesName333", "stackName333");
    //         WidgetData data4 = createWidgetData(4, "seriesName444", "stackName444");
    //         WidgetData data5 = createWidgetData(5, "seriesName555", "stackName555");
    //         
    //         mgr.cacheWidgetData("CACHEKEY111", data1);
    //         mgr.cacheWidgetData("CACHEKEY222", data2);
    //         mgr.cacheWidgetData("CACHEKEY333", data3);
    //         mgr.cacheWidgetData("CACHEKEY444", data4);
    //         mgr.cacheWidgetData("CACHEKEY555", data5);
    // }
    
    private WidgetData createWidgetData(final int id, final String seriesName, final String stackName) {
        final WidgetData data = new WidgetData();
        data.setChartType(id);
        data.setMetricTypeId(id);
        data.setRangeTypeId(id);
        data.setSingleValue("single");
        data.setStartTime(1000000000);
        
        final List<String> tier1Names = new ArrayList<String>();
        tier1Names.add("tier1-1");
        tier1Names.add("tier1-2");
        tier1Names.add("tier1-3");
        data.setTier1Names(tier1Names);
        data.setWidgetName("widgetName");
        
        final WidgetSeries s1 = new WidgetSeries();
        s1.setSeriesName(seriesName);
        s1.setStackName(stackName);
        final List<String> seriesData = new ArrayList<String>();
        seriesData.add("1");
        seriesData.add("2");
        seriesData.add("3");
        s1.setSeriesData(seriesData);
        final Map<String, WidgetSeries> seriesMap = new HashMap<String, WidgetSeries>();
        seriesMap.put("test1", s1);
        data.setSeriesMap(seriesMap);
        return data;
    }
}
