package com.digitalpaytech.mvc;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;

public class SessionPingControllerTest {

	@Test
	public void test_validateSession_true() {
		MockHttpServletRequest request = new MockHttpServletRequest("GET", "/secure/validateSession.html");
		MockHttpSession session = new MockHttpSession();
        //TODO Remove SessionToken
//		session.setAttribute("sessionToken", "ABCDEFG1234567");
		request.setSession(session);
		
		SessionPingController controller = new SessionPingController();
		String result = controller.validateSession(request);
		
		assertNotNull(result);
		assertEquals(result, "true");
	}
	
	@Test
	public void test_validateSession_false_noSession() {
		MockHttpServletRequest request = new MockHttpServletRequest("GET", "/secure/validateSession.html");
		
		SessionPingController controller = new SessionPingController();
		String result = controller.validateSession(request);
		
		assertNotNull(result);
		assertEquals(result, "false");
	}
}

