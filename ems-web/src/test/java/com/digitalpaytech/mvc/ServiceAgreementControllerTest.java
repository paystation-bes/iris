package com.digitalpaytech.mvc;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAgreement;
import com.digitalpaytech.domain.CustomerType;
import com.digitalpaytech.domain.ServiceAgreement;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.support.ServiceAgreementForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.ServiceAgreementService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.CustomerServiceTestImpl;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.ServiceAgreementValidator;

public class ServiceAgreementControllerTest {
	private UserAccount userAccount;
	
	@Before
	public void setUp() throws Exception {
		userAccount = new UserAccount();
		userAccount.setId(2);
		
		Customer customer = new Customer();
		customer.setId(2);
		CustomerType customerType = new CustomerType();
		customerType.setId(2);
		customer.setCustomerType(customerType);
		
		userAccount.setCustomer(customer);
	}
	
	@After
	public void cleanUp() throws Exception {
		userAccount = null;
	}
	
	@Test
	public void test_showServiceAgreement_accept_agreement_has_complex_password() throws IOException {
		
		MockHttpServletRequest request = new MockHttpServletRequest("GET", "/secure/terms.html");
		// Build mock response
		MockHttpServletResponse response = new MockHttpServletResponse();
		
		ServiceAgreementController controller = new ServiceAgreementController();
		
		controller.setUserAccountService(new UserAccountServiceImpl() {
			public UserAccount findUserAccount(int userAccountId) {
				UserAccount ua = new UserAccount();
				ua.setId(2);
				Customer customer = new Customer();
				customer.setId(2);
				CustomerType customerType = new CustomerType();
				customerType.setId(2);
				customer.setCustomerType(customerType);
				
				Set<CustomerAgreement> customerAgreements = new HashSet<CustomerAgreement>();
				customerAgreements.add(new CustomerAgreement());
				customer.setCustomerAgreements(customerAgreements);
				
				ua.setCustomer(customer);
				return ua;
			}
		});
		
		createAuthentication(userAccount, true);
		ModelMap model = new ModelMap();
		String result = controller.showServiceAgreement(request, response, model);
		assertEquals(result, "redirect:/secure/dashboard/index.html");
	}
	
	@Test
	public void test_showServiceAgreement_accept_agreement_has_complex_password_dptAdmin() throws IOException {
		
		MockHttpServletRequest request = new MockHttpServletRequest("GET", "/secure/terms.html");
		// Build mock response
		MockHttpServletResponse response = new MockHttpServletResponse();
		
		ServiceAgreementController controller = new ServiceAgreementController();
		
		controller.setUserAccountService(new UserAccountServiceImpl() {
			public UserAccount findUserAccount(int userAccountId) {
				UserAccount ua = new UserAccount();
				ua.setId(2);
				Customer customer = new Customer();
				customer.setId(2);
				CustomerType customerType = new CustomerType();
				customerType.setId(1);
				customer.setCustomerType(customerType);
				
				Set<CustomerAgreement> customerAgreements = new HashSet<CustomerAgreement>();
				customerAgreements.add(new CustomerAgreement());
				customer.setCustomerAgreements(customerAgreements);
				
				ua.setCustomer(customer);
				return ua;
			}
		});
		
		
		Set<CustomerAgreement> customerAgreements = new HashSet<CustomerAgreement>();
		customerAgreements.add(new CustomerAgreement());
		userAccount.getCustomer().setCustomerAgreements(customerAgreements);
		userAccount.getCustomer().getCustomerType().setId(WebCoreConstants.CUSTOMER_TYPE_DPT);
		
		createAuthentication(userAccount, true);
		ModelMap model = new ModelMap();
		String result = controller.showServiceAgreement(request, response, model);
		assertEquals(result, "redirect:/systemAdmin/index.html");
	}
	
	@Test
	public void test_showServiceAgreement_accept_agreement_has_complex_password_has_temp_password() throws IOException {
		
		MockHttpServletRequest request = new MockHttpServletRequest("GET", "/secure/terms.html");
		// Build mock response
		MockHttpServletResponse response = new MockHttpServletResponse();
		
		ServiceAgreementController controller = new ServiceAgreementController();
		UserAccount userAccount = new UserAccount();
		userAccount.setIsPasswordTemporary(true);
		Customer customer = new Customer();
		userAccount.setCustomer(customer);
		
		controller.setUserAccountService(new UserAccountServiceImpl() {
			public UserAccount findUserAccount(int userAccountId) {
				UserAccount ua = new UserAccount();
				ua.setId(2);
				Customer customer = new Customer();
				customer.setId(2);
				CustomerType customerType = new CustomerType();
				customerType.setId(1);
				customer.setCustomerType(customerType);
				
				Set<CustomerAgreement> customerAgreements = new HashSet<CustomerAgreement>();
				customerAgreements.add(new CustomerAgreement());
				customer.setCustomerAgreements(customerAgreements);
				
				ua.setCustomer(customer);
				ua.setIsPasswordTemporary(true);
				return ua;
			}
		});
		
		ServiceAgreementService serviceAgreementService = EasyMock.createMock(ServiceAgreementService.class);
		ServiceAgreement latestServiceAgreement = new ServiceAgreement();
		latestServiceAgreement.setContent("test".getBytes());
		EasyMock.expect(serviceAgreementService.findLatestServiceAgreement()).andReturn(latestServiceAgreement);
		EasyMock.replay(serviceAgreementService);
		controller.setServiceAgreementService(serviceAgreementService);
		
		createAuthentication(userAccount, true);
		ModelMap model = new ModelMap();
		String result = controller.showServiceAgreement(request, response, model);
		assertEquals(result, "redirect:/secure/password.html");
	}
	
	@Test
	public void test_showServiceAgreement_accept_agreement_has_not_complex_password() throws IOException {
		
		MockHttpServletRequest request = new MockHttpServletRequest("GET", "/secure/terms.html");
		// Build mock response
		MockHttpServletResponse response = new MockHttpServletResponse();
		
		ServiceAgreementController controller = new ServiceAgreementController();
		
		controller.setUserAccountService(new UserAccountServiceImpl() {
			public UserAccount findUserAccount(int userAccountId) {
				UserAccount ua = new UserAccount();
				ua.setId(2);
				Customer customer = new Customer();
				customer.setId(2);
				CustomerType customerType = new CustomerType();
				customerType.setId(1);
				customer.setCustomerType(customerType);
				
				Set<CustomerAgreement> customerAgreements = new HashSet<CustomerAgreement>();
				customerAgreements.add(new CustomerAgreement());
				customer.setCustomerAgreements(customerAgreements);
				
				ua.setCustomer(customer);
				ua.setIsPasswordTemporary(true);
				return ua;
			}
		});
		
		createAuthentication(userAccount, false);
		ModelMap model = new ModelMap();
		String result = controller.showServiceAgreement(request, response, model);
		assertEquals(result, "redirect:/secure/password.html");
	}
	
	@Test
	public void test_showServiceAgreement_accept_agreement_has_not_complex_password_has_temp_password() throws IOException {
		
		MockHttpServletRequest request = new MockHttpServletRequest("GET", "/secure/terms.html");
		// Build mock response
		MockHttpServletResponse response = new MockHttpServletResponse();
		
		ServiceAgreementController controller = new ServiceAgreementController();

		controller.setUserAccountService(new UserAccountServiceImpl() {
			public UserAccount findUserAccount(int userAccountId) {
				UserAccount ua = new UserAccount();
				ua.setId(2);
				Customer customer = new Customer();
				customer.setId(2);
				CustomerType customerType = new CustomerType();
				customerType.setId(1);
				customer.setCustomerType(customerType);
				
				Set<CustomerAgreement> customerAgreements = new HashSet<CustomerAgreement>();
				customerAgreements.add(new CustomerAgreement());
				customer.setCustomerAgreements(customerAgreements);
				
				ua.setCustomer(customer);
				ua.setIsPasswordTemporary(false);
				return ua;
			}
		});
		
		createAuthentication(userAccount, false);
		ModelMap model = new ModelMap();
		String result = controller.showServiceAgreement(request, response, model);
		assertEquals(result, "redirect:/secure/password.html");
	}
	
	@Test
	public void test_showServiceAgreement_self_parent_admin_not_accept_agreement() throws IOException {
		
		MockHttpServletRequest request = new MockHttpServletRequest("GET", "/secure/terms.html");
		// Build mock response
		MockHttpServletResponse response = new MockHttpServletResponse();
		request.getSession(true);
		
		ServiceAgreementController controller = new ServiceAgreementController();

		controller.setUserAccountService(new UserAccountServiceImpl() {
			public UserAccount findUserAccount(int userAccountId) {
				UserAccount ua = new UserAccount();
				ua.setId(2);
				Customer customer = new Customer();
				customer.setId(2);
				CustomerType customerType = new CustomerType();
				customerType.setId(1);
				customer.setCustomerType(customerType);
				
				ua.setCustomer(customer);
				ua.setIsPasswordTemporary(false);
				return ua;
			}
		});
		
		createAuthentication(userAccount, true);
		
		ServiceAgreementService serviceAgreementService = EasyMock.createMock(ServiceAgreementService.class);
		ServiceAgreement latestServiceAgreement = new ServiceAgreement();
		latestServiceAgreement.setContent("test".getBytes());
		EasyMock.expect(serviceAgreementService.findLatestServiceAgreement()).andReturn(latestServiceAgreement);
		EasyMock.replay(serviceAgreementService);
		
		controller.setServiceAgreementService(serviceAgreementService);
		ModelMap model = new ModelMap();
		
		String result = controller.showServiceAgreement(request, response, model);
		assertEquals(result, "terms");
	}
	
	@Test
	public void test_showServiceAgreement_not_self_parent_admin_not_accept_agreement() throws IOException {
		
		MockHttpServletRequest request = new MockHttpServletRequest("GET", "/secure/terms.html");
		// Build mock response
		MockHttpServletResponse response = new MockHttpServletResponse();
		
		ServiceAgreementController controller = new ServiceAgreementController();
		
		controller.setUserAccountService(new UserAccountServiceImpl() {
			public UserAccount findUserAccount(int userAccountId) {
				UserAccount ua = new UserAccount();
				ua.setId(2);
				Customer customer = new Customer();
				customer.setId(2);
				CustomerType customerType = new CustomerType();
				customerType.setId(1);
				customer.setCustomerType(customerType);
				Customer parentCustomer = new Customer();
				parentCustomer.setId(1);
				customer.setParentCustomer(parentCustomer);
				
				ua.setCustomer(customer);
				ua.setIsPasswordTemporary(false);
				return ua;
			}
		});
		
		createAuthentication(userAccount, true);
		
		ServiceAgreementService serviceAgreementService = EasyMock.createMock(ServiceAgreementService.class);
		ServiceAgreement latestServiceAgreement = new ServiceAgreement();
		latestServiceAgreement.setContent("test".getBytes());
		EasyMock.expect(serviceAgreementService.findLatestServiceAgreement()).andReturn(latestServiceAgreement);
		EasyMock.replay(serviceAgreementService);
		
		controller.setCustomerService(new CustomerServiceTestImpl() {
			public Customer findCustomer(Integer id) {
				Customer parentCustomer = new Customer();
				parentCustomer.setId(1);
				return parentCustomer;
			}
		});
		controller.setServiceAgreementService(serviceAgreementService);
		ModelMap model = new ModelMap();
		
		String result = controller.showServiceAgreement(request, response, model);
		assertEquals(result, "redirect:/");
	}
	
	@Test
	public void test_showServiceAgreement_parent_admin_null() throws IOException {
		
		MockHttpServletRequest request = new MockHttpServletRequest("GET", "/secure/terms.html");
		// Build mock response
		MockHttpServletResponse response = new MockHttpServletResponse();
		
		request.getSession();
		
		ServiceAgreementController controller = new ServiceAgreementController();

		controller.setUserAccountService(new UserAccountServiceImpl() {
			public UserAccount findUserAccount(int userAccountId) {
				UserAccount ua = new UserAccount();
				ua.setId(2);
				Customer customer = new Customer();
				customer.setId(2);
				CustomerType customerType = new CustomerType();
				customerType.setId(1);
				customer.setCustomerType(customerType);
				Customer parentCustomer = new Customer();
				parentCustomer.setId(1);
				customer.setParentCustomer(parentCustomer);
				
				ua.setCustomer(customer);
				ua.setIsPasswordTemporary(false);
				return ua;
			}
		});
		
		createAuthentication(userAccount, true);
		
		ServiceAgreementService serviceAgreementService = EasyMock.createMock(ServiceAgreementService.class);
		ServiceAgreement latestServiceAgreement = new ServiceAgreement();
		latestServiceAgreement.setContent("test".getBytes());
		EasyMock.expect(serviceAgreementService.findLatestServiceAgreement()).andReturn(latestServiceAgreement);
		EasyMock.replay(serviceAgreementService);
		
		CustomerService customerService = new CustomerServiceTestImpl() {
			public Customer findCustomer(Integer id) {
				return null;
			}
		};
		
		controller.setCustomerService(customerService);
		controller.setServiceAgreementService(serviceAgreementService);
		ModelMap model = new ModelMap();
		
		String result = controller.showServiceAgreement(request, response, model);
		assertEquals(result, "redirect:/");
	}

	@Test
	public void test_processServiceAgreement_complex_password() throws IOException {
		
		ServiceAgreementValidator serviceAgreementValidator = new ServiceAgreementValidator() {
			@Override
			public void validate(WebSecurityForm<ServiceAgreementForm> command, Errors errors) {
				
			}
		};
		
		WebSecurityForm<ServiceAgreementForm> form = new WebSecurityForm<ServiceAgreementForm>();
		ServiceAgreementForm agreementForm = new ServiceAgreementForm();
		agreementForm.setName("testUser");
		agreementForm.setTitle("testTitle");
		agreementForm.setOrganization("testOrganization");
		form.setWrappedObject(agreementForm);
		
		BindingResult result = new BeanPropertyBindingResult(form, "webSerurityForm");
		MockHttpServletRequest request = new MockHttpServletRequest("GET", "/secure/terms.html");
		request.getSession(true);
		// Build mock response
		MockHttpServletResponse response = new MockHttpServletResponse();
		SessionTool sessionTool = SessionTool.getInstance(request);
		
		ServiceAgreement sa = new ServiceAgreement();
		sa.setId(new Short("2"));
		sessionTool.setAttribute("latestServiceAgreement", sa);
		
		ServiceAgreementController controller = new ServiceAgreementController();
		controller.setServiceAgreementValidator(serviceAgreementValidator);
		UserAccount userAccount = new UserAccount();
		Customer customer = new Customer();
		Customer parentCustomer = new Customer();
		parentCustomer.setId(1);
		customer.setParentCustomer(parentCustomer);
		userAccount.setCustomer(customer);
		CustomerType type = new CustomerType();
		type.setId(2);
		customer.setCustomerType(type);
		CustomerService customerService = new CustomerServiceTestImpl() {
			public Serializable saveCustomerAgreement(CustomerAgreement customerAgreement) {
				return new CustomerAgreement();
			}
		};
		
		controller.setCustomerService(customerService);
		WebSecurityUtil util = new WebSecurityUtil();
		util.setUserAccountService(createStaticUserAccountService());
		util.setEntityService(createEntityService());
		
		createAuthentication(userAccount, true);
		ModelMap model = new ModelMap();
		
		String processResult = controller.processServiceAgreement(form, result, request, response, model);
		assertEquals(processResult, "redirect:/secure/dashboard/index.html");
	}
	
	@Test
	public void test_processServiceAgreement_not_complex_password() throws IOException {
		
		ServiceAgreementValidator serviceAgreementValidator = new ServiceAgreementValidator() {
			@Override
			public void validate(WebSecurityForm<ServiceAgreementForm> command, Errors errors) {
				
			}
		};
		
		WebSecurityForm<ServiceAgreementForm> form = new WebSecurityForm<ServiceAgreementForm>();
		ServiceAgreementForm agreementForm = new ServiceAgreementForm();
		agreementForm.setName("testUser");
		agreementForm.setTitle("testTitle");
		agreementForm.setOrganization("testOrganization");
		form.setWrappedObject(agreementForm);
		
		BindingResult result = new BeanPropertyBindingResult(form, "webSerurityForm");
		MockHttpServletRequest request = new MockHttpServletRequest("GET", "/secure/terms.html");
		request.getSession(true);
		// Build mock response
		MockHttpServletResponse response = new MockHttpServletResponse();
		SessionTool sessionTool = SessionTool.getInstance(request);
		
		ServiceAgreement sa = new ServiceAgreement();
		sa.setId(new Short("2"));
		sessionTool.setAttribute("latestServiceAgreement", sa);
		
		ServiceAgreementController controller = new ServiceAgreementController();
		controller.setServiceAgreementValidator(serviceAgreementValidator);
		UserAccount userAccount = new UserAccount();
		Customer customer = new Customer();
		Customer parentCustomer = new Customer();
		parentCustomer.setId(1);
		customer.setParentCustomer(parentCustomer);
		userAccount.setCustomer(customer);
		
		WebSecurityUtil util = new WebSecurityUtil();
		util.setUserAccountService(createStaticUserAccountService());
		util.setEntityService(createEntityService());
		
		CustomerService customerService = new CustomerServiceTestImpl() {
			public Serializable saveCustomerAgreement(CustomerAgreement customerAgreement) {
				return new CustomerAgreement();
			}
		};
		
		controller.setCustomerService(customerService);
		createAuthentication(userAccount, false);
		ModelMap model = new ModelMap();
		
		String processResult = controller.processServiceAgreement(form, result, request, response, model);
		assertEquals(processResult, "redirect:/secure/password.html");
	}
	
	@Test
	public void test_processServiceAgreement_has_error() throws IOException {
		
		ServiceAgreementValidator serviceAgreementValidator = new ServiceAgreementValidator() {
		    public String getMessage(String code) {
		        return "error.common.required";
		    }
		    
		    public String getMessage(String code, Object... args) {
		        return "error.common.invalid";
		    }
		};
		
		WebSecurityForm<ServiceAgreementForm> form = new WebSecurityForm<ServiceAgreementForm>();
		ServiceAgreementForm agreementForm = new ServiceAgreementForm();
		agreementForm.setName("testUser");
		agreementForm.setTitle("testTitle");
		agreementForm.setOrganization("testOrganization");
		form.setWrappedObject(agreementForm);
		
		BindingResult result = new BeanPropertyBindingResult(form, "webSerurityForm");
		
		MockHttpServletRequest request = new MockHttpServletRequest("GET", "/secure/terms.html");
		request.getSession(true);
		// Build mock response
		MockHttpServletResponse response = new MockHttpServletResponse();
		SessionTool sessionTool = SessionTool.getInstance(request);
		
		ServiceAgreement sa = new ServiceAgreement();
		sa.setContent("test".getBytes());
		sessionTool.setAttribute("latestServiceAgreement", sa);
		
		ServiceAgreementController controller = new ServiceAgreementController();
		controller.setServiceAgreementValidator(serviceAgreementValidator);
		ModelMap model = new ModelMap();
		
		String processResult = controller.processServiceAgreement(form, result, request, response, model);
		assertEquals(processResult, "terms");
	}

	private void createAuthentication(UserAccount userAccount, boolean isComplexPassword) {
		
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("Admin"));
		WebUser user = new WebUser(1,1,"testUser","password","salt", true, authorities);
		user.setIsComplexPassword(isComplexPassword);
		if((userAccount.getCustomer() != null) && (userAccount.getCustomer().getCustomerType() != null)) {
			user.setIsSystemAdmin(WebCoreConstants.CUSTOMER_TYPE_DPT == userAccount.getCustomer().getCustomerType().getId());
		}
		
		Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        SecurityContextHolder.getContext().setAuthentication(authentication);
	}
	
	private UserAccountService createStaticUserAccountService() {
		return new UserAccountServiceImpl() {
			public UserAccount findUserAccount(int userAccountId) {
				UserAccount ua = new UserAccount();
				ua.setId(2);
				return ua;
			}
		};
	}
	
	private EntityService createEntityService() {
		return new EntityServiceImpl() {
			public Object merge(Object entity) {
				UserAccount userAccount = new UserAccount();
				Customer customer = new Customer();
				userAccount.setCustomer(customer);
				CustomerType type = new CustomerType();
				type.setId(2);
				customer.setCustomerType(type);
				return userAccount;
			}
		};
	}
}
