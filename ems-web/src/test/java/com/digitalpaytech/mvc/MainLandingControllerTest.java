package com.digitalpaytech.mvc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerType;
import com.digitalpaytech.domain.Notification;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.Section;
import com.digitalpaytech.domain.Tab;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetListType;
import com.digitalpaytech.dto.LocationType;
import com.digitalpaytech.dto.TabHeading;
import com.digitalpaytech.dto.WidgetSettingsOptions;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.service.DashboardService;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.NotificationService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.DashboardServiceTestImpl;
import com.digitalpaytech.testing.services.impl.PointOfSaleServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

public class MainLandingControllerTest {
    
    private UserAccount userAccount;
    
    @Before
    public void setUp() throws Exception {
        
        userAccount = new UserAccount();
        userAccount.setId(2);
        Set<UserRole> roles = new HashSet<UserRole>();
        UserRole role = new UserRole();
        Role r = new Role();
        Set<RolePermission> rps = new HashSet<RolePermission>();
        
        RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS);
        rp1.setPermission(permission1);
        
        RolePermission rp2 = new RolePermission();
        rp2.setId(2);
        Permission permission2 = new Permission();
        permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT);
        rp2.setPermission(permission2);
        
        rps.add(rp1);
        rps.add(rp2);
        
        r.setId(3);
        r.setRolePermissions(rps);
        
        role.setId(3);
        role.setRole(r);
        roles.add(role);
        userAccount.setUserRoles(roles);
        
        Customer customer = new Customer();
        customer.setId(2);
        CustomerType customerType = new CustomerType();
        customerType.setId(2);
        customer.setCustomerType(customerType);
        
        userAccount.setCustomer(customer);
        createAuthentication(userAccount);
    }
    
    @Test
    public void test_showLandingPage() {
        MockHttpServletRequest request = new MockHttpServletRequest("GET", "/secure/dashboard/index.html");
        request.getSession(true);
        
        MockHttpServletResponse response = new MockHttpServletResponse();
        SessionTool sessionTool = SessionTool.getInstance(request);
        
        MainLandingController controller = new MainLandingController() {
            @Override
            public Tab loadLandingTab(HttpServletRequest request, List<TabHeading> tabHeadings) {
                return createTabSectionsWidgets(1);
            }
        };
        
        ModelMap model = new ModelMap();
        
        NotificationService service = EasyMock.createMock(NotificationService.class);
        List<Notification> resultSet = new ArrayList<Notification>();
        Notification notification = new Notification();
        notification.setId(1);
        notification.setMessage("Test Notification.");
        resultSet.add(notification);
        EasyMock.expect(service.findNotificationByEffectiveDate()).andReturn(resultSet);
        EasyMock.replay(service);
        
        UserAccount userAccount = new UserAccount();
        createAuthentication(userAccount);
        controller.setNotificationService(service);
        
        DashboardService dashboardService = new DashboardServiceTestImpl() {
            @Override
            public Collection<Tab> findTabsByUserAccountId(int id) {
                Collection<Tab> tabs = new ArrayList<Tab>();
                Tab tab = new Tab();
                tab.setId(1234);
                tab.setName("revenue");
                tabs.add(tab);
                return tabs;
            }
        };
        controller.setDashboardService(dashboardService);
        EntityService entityService = new EntityServiceImpl() {
            @Override
            public Object merge(Object entity) {
                return new UserAccount();
            }
        };
        
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            public PlacementMapBordersInfo findMapBordersByCustomerId(Integer customerId) {
                return null;
            }
            
        });
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(entityService);
        
        userAccount.setId(11111);
        
        //TODO Remove SessionToken
        //		sessionTool.setAttribute(WebSecurityConstants.SESSION_TOKEN, "testSession");
        sessionTool.setAttribute(WebCoreConstants.SESSION_WIDGET_SETTINGS_OPTIONS, createWidgetSettingsOptions());
        controller.setWidgetSettingsController(new WidgetSettingsController());
        
        String result = controller.showLandingPage(request, response, model);
        
        assertNotNull(result);
        assertEquals(result, "/dashboard/index");
        assertEquals(((List<Notification>) model.get("notifications")).size(), 1);
    }
    
    @Test
    public void getTabs() {
        MainLandingController controller = new MainLandingController();
        controller = new MainLandingController();
        
        DashboardService dashboardService = new DashboardServiceTestImpl() {
            @Override
            public Collection<Tab> findTabsByUserAccountId(int id) {
                Collection<Tab> tabs = new ArrayList<Tab>();
                Tab tab = new Tab();
                tab.setId(1234);
                tab.setName("revenue");
                tabs.add(tab);
                return tabs;
            }
        };
        controller.setDashboardService(dashboardService);
        
        EntityServiceImpl entityService = new EntityServiceImpl() {
            @Override
            public Object merge(Object entity) {
                return new UserAccount();
            }
        };
        controller.setEntityService(entityService);
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(entityService);
        
        UserAccount userAccount = new UserAccount();
        userAccount.setId(11111);
        
        MockHttpServletRequest req = new MockHttpServletRequest();
        req.getSession(true);
        
        assertEquals(1, controller.getTabs(req, 1).size());
    }
    
    @Test
    public void isPredefinedRole() {
        MainLandingController controller = new MainLandingController();
        // System Admin - Role.Id = 1, Role.Name = 'DPT Admin', Role.CustomerTypeId = 1 (= 'DPT') 
        assertTrue(controller.isPredefinedRole(createSystemAdmin(), 1, 1));
        // Parent Admin - Role.Id = 2, Role.Name = 'Parent Admin', Role.CustomerTypeId = 2 (= 'Parent')
        assertFalse(controller.isPredefinedRole(createSystemAdmin(), 2, 2));
        // Child AdminRole.Id = 3, Role.Name = 'Child Admin', Role.CustomerTypeId = 3 (= 'Child')
        assertFalse(controller.isPredefinedRole(createSystemAdmin(), 3, 3));
        
        // Parent Admin - Role.Id = 2, Role.Name = 'Parent Admin', Role.CustomerTypeId = 2 (= 'Parent')
        assertTrue(controller.isPredefinedRole(createParentAdmin(), 2, 2));
        
        // Child AdminRole.Id = 3, Role.Name = 'Child Admin', Role.CustomerTypeId = 3 (= 'Child')
        assertTrue(controller.isPredefinedRole(createChildAdmin(), 3, 3));
    }
    
    @Test
    public void isPredefinedRoleWithBasicRole() {
        MainLandingController controller = new MainLandingController();
        assertFalse(controller.isPredefinedRole(createBasic(), 1, 1));
    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(1, 1, "testUser", "password", "salt", true, authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD);
        user.setUserPermissions(permissionList);
        
        user.setIsComplexPassword(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private UserAccount createSystemAdmin() {
        return createUserAccount(1, 1);
    }
    
    private UserAccount createParentAdmin() {
        return createUserAccount(2, 2);
    }
    
    private UserAccount createChildAdmin() {
        return createUserAccount(3, 3);
    }
    
    private UserAccount createBasic() {
        Role role = new Role();
        role.setId(4);
        CustomerType custType = new CustomerType();
        custType.setId(3);
        role.setCustomerType(custType);
        role.setIsAdmin(false);
        role.setIsLocked(false);
        
        HashSet<Role> roles = new HashSet<Role>();
        roles.add(role);
        UserAccount ua = new UserAccount();
        ua.setRoles(roles);
        return ua;
    }
    
    private UserAccount createUserAccount(int roleId, int custTypeId) {
        Role role = new Role();
        role.setId(roleId);
        CustomerType custType = new CustomerType();
        custType.setId(custTypeId);
        role.setCustomerType(custType);
        role.setIsAdmin(true);
        role.setIsLocked(true);
        HashSet<Role> roles = new HashSet<Role>();
        roles.add(role);
        UserAccount ua = new UserAccount();
        ua.setRoles(roles);
        return ua;
    }
    
    @Test
    public void loadLandingTab() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MainLandingController controller = new MainLandingController();
        controller.setDashboardService(new DashboardServiceTestImpl() {
            @Override
            public Tab findTabByIdEager(int id) {
                return createTabSectionsWidgets(id);
            }
        });
        
        Tab tab = controller.loadLandingTab(request, createTabHeadings(true));
        assertEquals(new Long(3).longValue(), new Long(tab.getId()).longValue());
        
        tab = controller.loadLandingTab(request, createTabHeadings(false));
        assertEquals(new Long(1).longValue(), new Long(tab.getId()).longValue());
        
    }
    
    private ArrayList<TabHeading> createTabHeadings(boolean hasCurrent) {
        TabHeading heading1 = new TabHeading();
        heading1.setOriginalId(1);
        heading1.setId("TEST1ABCDEFG");
        heading1.setName("Test1");
        
        TabHeading heading2 = new TabHeading();
        heading2.setOriginalId(2);
        heading2.setId("TEST2ABCDEFG");
        heading2.setName("Test2");
        
        TabHeading heading3 = new TabHeading();
        heading3.setOriginalId(3);
        heading3.setId("TEST3ABCDEFG");
        heading3.setName("Test3");
        if (hasCurrent) {
            heading3.setCurrent(Boolean.TRUE.toString());
        }
        
        ArrayList<TabHeading> list = new ArrayList<TabHeading>();
        list.add(heading1);
        list.add(heading2);
        list.add(heading3);
        
        return list;
    }
    
    private Tab createTabSectionsWidgets(int tabid) {
        Widget w = new Widget();
        w.setId(1);
        w.setWidgetListType(new WidgetListType());
        w.getWidgetListType().setId(2);
        ArrayList<Widget> ws = new ArrayList<Widget>();
        ws.add(w);
        
        Section se = new Section();
        se.setId(1);
        se.setWidgets(ws);
        ArrayList<Section> ss = new ArrayList<Section>();
        ss.add(se);
        
        Tab tab = new Tab();
        tab.setId(tabid);
        tab.setSections(ss);
        
        tab.setUserAccount(new UserAccount());
        tab.getUserAccount().setId(2);
        
        return tab;
    }
    
    @Test
    public void findTabNoId() {
        MockHttpServletRequest req = new MockHttpServletRequest();
        MockHttpServletResponse res = new MockHttpServletResponse();
        ModelMap model = new ModelMap();
        SessionTool sessionTool = SessionTool.getInstance(req);
        
        MainLandingController controller = new MainLandingController() {
            @Override
            public List<TabHeading> getTabs(HttpServletRequest request, int id) {
                return createTabHeadings();
            }
        };
        
        controller.setDashboardService(new DashboardServiceTestImpl() {
            @Override
            public Tab findTabByIdEager(int id) {
                return createTabSectionsWidgets();
            }
        });
        
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, createTabHeadings(true));
        
        String path = controller.findTab(req, res, model);
        assertEquals("/dashboard/include/dashboard", path);
        
    }
    
    @Test
    public void findTab() {
        
        UserAccount userAccount = new UserAccount();
        createAuthentication(userAccount);
        
        MockHttpServletRequest req = new MockHttpServletRequest();
        MockHttpServletResponse res = new MockHttpServletResponse();
        SessionTool sessionTool = SessionTool.getInstance(req);
        
        ModelMap model = new ModelMap();
        
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        Tab tab = new Tab();
        tab.setId(2);
        String randomId = keyMapping.getRandomString(tab, "id");
        req.addParameter("id", randomId);
        
        MainLandingController controller = new MainLandingController();
        
        controller.setDashboardService(new DashboardServiceTestImpl() {
            @Override
            public Tab findTabByIdEager(int id) {
                return createTabSectionsWidgets();
            }
        });
        
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, createTabHeadings(true));
        
        String path = controller.findTab(req, res, model);
        assertEquals("/dashboard/include/dashboard", path);
        
        tab = (Tab) model.get("tabTree");
        assertNotNull(tab);
        assertEquals(2, tab.getId().intValue());
        assertNotNull(tab.getSections());
        
        //------------------------------------------------------------------------------
        // Section is null
        controller.setDashboardService(new DashboardServiceTestImpl() {
            @Override
            public Tab findTabByIdEager(int id) {
                return new Tab();
            }
        });
        path = controller.findTab(req, res, model);
        
        String error = (String) model.get("error.no.section");
        assertNotNull(error);
        assertEquals("error.no.section", error);
        
        controller.setDashboardService(new DashboardServiceTestImpl() {
            @Override
            public Tab findTabByIdEager(int id) {
                ArrayList<Section> list = new ArrayList<Section>();
                Tab tab = new Tab();
                tab.setSections(list);
                return tab;
            }
        });
        path = controller.findTab(req, res, model);
        
        error = (String) model.get("error.no.section");
        assertNotNull(error);
        assertEquals("error.no.section", error);
    }
    
    private Tab createTabSectionsWidgets() {
        Widget w = new Widget();
        w.setId(1);
        w.setWidgetListType(new WidgetListType());
        w.getWidgetListType().setId(2);
        ArrayList<Widget> ws = new ArrayList<Widget>();
        ws.add(w);
        
        Section se = new Section();
        se.setId(1);
        se.setWidgets(ws);
        ArrayList<Section> ss = new ArrayList<Section>();
        ss.add(se);
        
        Tab tab = new Tab();
        tab.setId(2);
        tab.setSections(ss);
        
        tab.setUserAccount(new UserAccount());
        tab.getUserAccount().setId(2);
        
        return tab;
    }
    
    private ArrayList<TabHeading> createTabHeadings() {
        Tab t1 = new Tab();
        t1.setId(1);
        t1.setName("Finance");
        TabHeading th1 = new TabHeading(t1);
        
        Tab t2 = new Tab();
        t2.setId(2);
        t2.setName("Operations");
        TabHeading th2 = new TabHeading(t2);
        
        Tab t3 = new Tab();
        t3.setId(3);
        t3.setName("Map");
        TabHeading th3 = new TabHeading(t3);
        
        Tab t4 = new Tab();
        t4.setId(4);
        t4.setName("Test");
        TabHeading th4 = new TabHeading(t4);
        
        ArrayList<TabHeading> list = new ArrayList<TabHeading>();
        list.add(th2);
        list.add(th4);
        list.add(th1);
        list.add(th3);
        
        return list;
    }
    
    private WidgetSettingsOptions createWidgetSettingsOptions() {
        LocationType loc1 = new LocationType();
        loc1.setName("West");
        
        LocationType loc2 = new LocationType();
        loc2.setName("East");
        
        LocationType loc3 = new LocationType();
        loc3.setName("1201 J Street");
        
        List<LocationType> LocationTypes = new ArrayList<LocationType>();
        LocationTypes.add(loc1);
        LocationTypes.add(loc2);
        LocationTypes.add(loc3);
        
        WidgetSettingsOptions options = new WidgetSettingsOptions();
        options.setLocationTypes(LocationTypes);
        
        return options;
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
        };
    }
    
    private EntityService createEntityService() {
        return new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        };
    }
}
