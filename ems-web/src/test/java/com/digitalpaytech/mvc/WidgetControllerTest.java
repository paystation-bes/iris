package com.digitalpaytech.mvc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Section;
import com.digitalpaytech.domain.Tab;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetListType;
import com.digitalpaytech.domain.WidgetMetricType;
import com.digitalpaytech.domain.WidgetTierType;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.DashboardServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

public class WidgetControllerTest {
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private UserAccount userAccount;
    
    @Before
    public void setUp() throws Exception {
        this.request = new MockHttpServletRequest();
        this.request.setSession(new MockHttpSession());
        
        this.response = new MockHttpServletResponse();
        userAccount = new UserAccount();
        Customer customer = new Customer();
        Customer parent = new Customer();
        customer.setId(2);
        customer.setParentCustomer(parent);
        userAccount.setCustomer(customer);
    }
    
    @After
    public void cleanUp() throws Exception {
        request = null;
        response = null;
    }
    
    @Test
    public void test_showWidgetMasterList() {
        
        ModelMap model = new ModelMap();
        
        createAuthentication(userAccount);
        
        EntityServiceImpl serv = new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        };
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(serv);
        
        WidgetController controller = new WidgetController();
        
        controller.setDashboardService(new DashboardServiceTestImpl() {
            public List<Widget> findWidgetMasterList(UserAccount user) {
                Widget masterWidget = new Widget();
                List<Widget> list = new ArrayList<Widget>();
                
                WidgetMetricType widgetMetricType = new WidgetMetricType();
                widgetMetricType.setId(1);
                
                masterWidget.setWidgetMetricType(widgetMetricType);
                masterWidget.setName("test");
                masterWidget.setId(1);
                masterWidget.setDescription("description");
                
                list.add(masterWidget);
                return list;
            }
        });
        
        assertNotNull(controller.showWidgetMasterList(request, response, model));
    }
    
//    @Test
//    public void test_ViewMasterList_AND_CreateWidget() {
//        
//        request.getSession(true);
//        
//        SessionTool sessionTool = SessionTool.getInstance(request);
//        
//        ModelMap model = new ModelMap();
//        createAuthentication(userAccount);
//        
//        WidgetController controller = new WidgetController();
//        
//        controller.setDashboardService(new DashboardServiceTestImpl() {
//            public List<Widget> findWidgetMasterList(UserAccount user) {
//                Widget masterWidget = new Widget();
//                List<Widget> list = new ArrayList<Widget>();
//                
//                WidgetMetricType widgetMetricType = new WidgetMetricType();
//                widgetMetricType.setId(1);
//                
//                masterWidget.setWidgetMetricType(widgetMetricType);
//                masterWidget.setName("test");
//                masterWidget.setId(1);
//                masterWidget.setDescription("description");
//                
//                list.add(masterWidget);
//                return list;
//            }
//        });
//        
//        EntityServiceImpl serv = new EntityServiceImpl() {
//            public Object merge(Object entity) {
//                return userAccount;
//            }
//        };
//        WebSecurityUtil util = new WebSecurityUtil();
//        util.setUserAccountService(createStaticUserAccountService());
//        util.setEntityService(serv);
//        
//        // Gets the widget random Id from the JSON formatted string in the model
//        String json = controller.showWidgetMasterList(request, response, model);
//        
//        int start = json.indexOf("\"id\": \"") + "\"id\": \"".length();
//        String subString = json.substring(start);
//        int end = subString.indexOf("\"");
//        String widgetId = subString.substring(0, end);
//        
//        ConcurrentHashMap<String, Tab> map = new ConcurrentHashMap<String, Tab>();
//        Tab tab = new Tab();
//        tab.setRandomId("ABASDFGHTEMPT123");
//        Section section = new Section();
//        section.setRandomId("ABASDFGHTEMPS124");
//        tab.addSection(section);
//        map.put("ABASDFGHTEMPT123", tab);
//        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, map);
//        
//        request.setParameter("tabID", "ABASDFGHTEMPT123");
//        request.setParameter("sectionID", "ABASDFGHTEMPS124");
//        request.setParameter("widgetTypeID", widgetId);
//        
//        controller.setDashboardService(new DashboardServiceTestImpl() {
//            public Widget findDefaultWidgetByIdAndEvict(int widgetId) {
//                Widget widget = new Widget();
//                
//                WidgetMetricType widgetMetricType = new WidgetMetricType();
//                WidgetTierType widgetTierType = new WidgetTierType();
//                WidgetListType widgetListType = new WidgetListType();
//                
//                widget.setWidgetTierTypeByWidgetTier1Type(widgetTierType);
//                widget.setWidgetTierTypeByWidgetTier2Type(widgetTierType);
//                widget.setWidgetTierTypeByWidgetTier3Type(widgetTierType);
//                widget.setWidgetListType(widgetListType);
//                
//                widgetMetricType.setId(1);
//                widget.setWidgetMetricType(widgetMetricType);
//                widget.setName("test");
//                widget.setId(widgetId);
//                widget.setDescription("description");
//                return widget;
//            }
//        });
//        
//        String result = controller.addWidget(request, response, model);
//        
//        // widgetId = id for widget in master list
//        // model.get("widgetId") = new ID given to new widget
//        assertNotSame(widgetId, model.get("widgetID"));
//        assertNotNull(model.get("widgetID"));
//        assertNotNull(result);
//    }
//    
//    @Test
//    public void test_addWidget() {
//        
//        request.getSession(true);
//        SessionTool sessionTool = SessionTool.getInstance(request);
//        createAuthentication(userAccount);
//        
//        ModelMap model = new ModelMap();
//        request.setParameter("tabID", "ABASDFGHTEMPT123");
//        request.setParameter("sectionID", "ABASDFGHTEMPS124");
//        
//        ConcurrentHashMap<String, Tab> map = new ConcurrentHashMap<String, Tab>();
//        Tab tab = new Tab();
//        tab.setRandomId("ABASDFGHTEMPT123");
//        Section section = new Section();
//        section.setRandomId("ABASDFGHTEMPS124");
//        tab.addSection(section);
//        map.put("ABASDFGHTEMPT123", tab);
//        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, map);
//        
//        Widget widget = new Widget();
//        widget.setId(1);
//        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
//        request.setParameter("widgetTypeID", keyMapping.getRandomString(widget, "id"));
//        
//        WidgetController controller = new WidgetController();
//        EntityServiceImpl serv = new EntityServiceImpl() {
//            public Object merge(Object entity) {
//                Customer cus = new Customer();
//                cus.setId(2);
//                userAccount.setCustomer(cus);
//                Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
//                authorities.add(new SimpleGrantedAuthority("Admin"));
//                WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
//                user.setIsComplexPassword(true);
//                Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
//                SecurityContextHolder.getContext().setAuthentication(authentication);
//                return userAccount;
//            }
//        };
//        WebSecurityUtil util = new WebSecurityUtil();
//        util.setUserAccountService(createStaticUserAccountService());
//        util.setEntityService(serv);
//        controller.setDashboardService(new DashboardServiceTestImpl() {
//            public Widget findDefaultWidgetByIdAndEvict(int widgetId) {
//                
//                Widget widget = new Widget();
//                
//                WidgetMetricType widgetMetricType = new WidgetMetricType();
//                WidgetTierType widgetTierType = new WidgetTierType();
//                WidgetListType widgetListType = new WidgetListType();
//                
//                widget.setWidgetTierTypeByWidgetTier1Type(widgetTierType);
//                widget.setWidgetTierTypeByWidgetTier2Type(widgetTierType);
//                widget.setWidgetTierTypeByWidgetTier3Type(widgetTierType);
//                widget.setWidgetListType(widgetListType);
//                
//                widgetMetricType.setId(1);
//                widget.setWidgetMetricType(widgetMetricType);
//                widget.setName("test");
//                widget.setId(widgetId);
//                widget.setDescription("description");
//                return widget;
//            }
//        });
//        
//        String result = controller.addWidget(request, response, model);
//        
//        assertEquals(result, "/dashboard/include/widgetShell");
//    }
//    
//    @Test
//    public void test_addWidget_checkWidgetOrder() {
//        
//        ModelMap model = new ModelMap();
//        SessionTool sessionTool = SessionTool.getInstance(request);
//        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
//        
//        ConcurrentHashMap<String, Tab> map = new ConcurrentHashMap<String, Tab>();
//        Tab tab = new Tab();
//        tab.setRandomId("ABASDFGHTEMPT123");
//        Section section = new Section();
//        section.setRandomId("ABASDFGHTEMPS124");
//        section.prepareSectionColumns();
//        
//        Widget widget = new Widget();
//        widget.setId(1);
//        widget.setRandomId("ABASDFGHTEMPW125");
//        widget.setOrderNumber(1);
//        
//        section.addWidget(widget);
//        tab.addSection(section);
//        map.put("ABASDFGHTEMPT123", tab);
//        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, map);
//        
//        request.setParameter("tabID", "ABASDFGHTEMPT123");
//        request.setParameter("sectionID", "ABASDFGHTEMPS124");
//        request.setParameter("widgetTypeID", keyMapping.getRandomString(widget, "id"));
//        
//        createAuthentication(userAccount);
//        
//        WidgetController controller = new WidgetController();
//        EntityServiceImpl serv = new EntityServiceImpl() {
//            public Object merge(Object entity) {
//                Customer cus = new Customer();
//                cus.setId(2);
//                Customer parentCus = new Customer();
//                parentCus.setId(1);
//                cus.setParentCustomer(parentCus);
//                userAccount.setCustomer(cus);
//                Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
//                authorities.add(new SimpleGrantedAuthority("Admin"));
//                WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
//                user.setIsComplexPassword(true);
//                Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
//                SecurityContextHolder.getContext().setAuthentication(authentication);
//                return userAccount;
//            }
//        };
//        WebSecurityUtil util = new WebSecurityUtil();
//        util.setUserAccountService(createStaticUserAccountService());
//        util.setEntityService(serv);
//        controller.setDashboardService(new DashboardServiceTestImpl() {
//            public Widget findDefaultWidgetByIdAndEvict(int widgetId) {
//                
//                Widget widget = new Widget();
//                
//                WidgetMetricType widgetMetricType = new WidgetMetricType();
//                WidgetTierType widgetTierType = new WidgetTierType();
//                WidgetListType widgetListType = new WidgetListType();
//                
//                widget.setWidgetTierTypeByWidgetTier1Type(widgetTierType);
//                widget.setWidgetTierTypeByWidgetTier2Type(widgetTierType);
//                widget.setWidgetTierTypeByWidgetTier3Type(widgetTierType);
//                widget.setWidgetListType(widgetListType);
//                
//                widgetMetricType.setId(1);
//                widget.setWidgetMetricType(widgetMetricType);
//                widget.setName("test");
//                widget.setId(widgetId);
//                widget.setDescription("description");
//                return widget;
//            }
//        });
//        String result = controller.addWidget(request, response, model);
//        
//        assertEquals(result, "/dashboard/include/widgetShell");
//    }
//    
//    @Test
//    public void test_addWidget_noTabId() {
//        
//        ModelMap model = new ModelMap();
//        createAuthentication(userAccount);
//        
//        request.setParameter("sectionID", "ABASDFGHTEMPS124");
//        request.setParameter("widgetTypeID", "2");
//        
//        WidgetController controller = new WidgetController();
//        String result = controller.addWidget(request, response, model);
//        
//        assertNull(result);
//        assertEquals(response.getStatus(), HttpStatus.BAD_REQUEST.value());
//    }
//    
//    @Test
//    public void test_addWidget_noSectionId() {
//        
//        ModelMap model = new ModelMap();
//        request.setParameter("tabID", "ABASDFGHTEMPT123");
//        request.setParameter("widgetTypeID", "2");
//        
//        WidgetController controller = new WidgetController();
//        String result = controller.addWidget(request, response, model);
//        
//        assertNull(result);
//        assertEquals(response.getStatus(), HttpStatus.BAD_REQUEST.value());
//    }
//    
//    // @Test
//    // public void test_addWidget_noWidgetTypeId() {
//    // 
//    // 
//    // ModelMap model = new ModelMap();
//    // request.setParameter("tabID", "ABASDFGHTEMPT123");
//    // request.setParameter("sectionID", "ABASDFGHTEMPS124");
//    //
//    // WidgetController controller = new WidgetController();
//    // String result = controller.addWidget(request, response, model);
//    //
//    // assertNull(result);
//    // assertEquals(response.getStatus(), HttpStatus.BAD_REQUEST.value());
//    // }
//    
//    @Test
//    public void test_addWidget_no_Section_in_session() {
//        
//        SessionTool sessionTool = SessionTool.getInstance(request);
//        
//        ModelMap model = new ModelMap();
//        request.setParameter("tabID", "ABASDFGHTEMPT123");
//        request.setParameter("sectionID", "ABASDFGHTEMPS124");
//        request.setParameter("widgetTypeID", "ABASDFGHTEMPS125");
//        createAuthentication(userAccount);
//        
//        ConcurrentHashMap<String, Tab> map = new ConcurrentHashMap<String, Tab>();
//        Tab tab = new Tab();
//        tab.setRandomId("ABASDFGHTEMPT123");
//        map.put("ABASDFGHTEMPT123", tab);
//        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, map);
//        
//        WidgetController controller = new WidgetController();
//        String result = controller.addWidget(request, response, model);
//        
//        assertNull(result);
//        assertEquals(response.getStatus(), HttpStatus.NOT_FOUND.value());
//    }
//    
//    @Test
//    public void test_updateWidgetOrder() {
//        
//        ModelMap model = new ModelMap();
//        request.setParameter("tabID", "ABASDFGHTEMPT123");
//        request.setParameter("section[]", "ABASDFGHTEMPS124");
//        request.setParameter("secABASDFGHTEMPS124col0[]", "ABASDFGHTEMPW126");
//        request.addParameter("secABASDFGHTEMPS124col0[]", "ABASDFGHTEMPW125");
//        
//        SessionTool sessionTool = SessionTool.getInstance(request);
//        
//        ConcurrentHashMap<String, Tab> map = new ConcurrentHashMap<String, Tab>();
//        Tab tab = new Tab();
//        tab.setRandomId("ABASDFGHTEMPT123");
//        Section section = new Section();
//        section.prepareSectionColumns();
//        section.setRandomId("ABASDFGHTEMPS124");
//        Widget widget = new Widget();
//        widget.setRandomId("ABASDFGHTEMPW125");
//        widget.setOrderNumber(1);
//        section.addWidget(widget);
//        tab.addSection(section);
//        Widget widget1 = new Widget();
//        widget1.setRandomId("ABASDFGHTEMPW126");
//        widget1.setOrderNumber(2);
//        section.addWidget(widget1);
//        tab.addSection(section);
//        map.put("ABASDFGHTEMPT123", tab);
//        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, map);
//        
//        WidgetController controller = new WidgetController();
//        String result = controller.updateWidgetOrder(request, response, model);
//        
//        Map sessionMap = (Map) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
//        Tab resultTab = (Tab) sessionMap.get("ABASDFGHTEMPT123");
//        
//        assertEquals(result, "true");
//        assertEquals(resultTab.getSection("ABASDFGHTEMPS124").getWidget("ABASDFGHTEMPW126").getOrderNumber(), 1);
//        assertEquals(resultTab.getSection("ABASDFGHTEMPS124").getWidget("ABASDFGHTEMPW125").getOrderNumber(), 2);
//    }
//    
//    @Test
//    public void test_updateWidgetOrder_secondColumn() {
//        
//        ModelMap model = new ModelMap();
//        request.setParameter("tabID", "ABASDFGHTEMPT123");
//        request.setParameter("section[]", "ABASDFGHTEMPS124");
//        request.setParameter("secABASDFGHTEMPS124col1[]", "ABASDFGHTEMPW126");
//        request.addParameter("secABASDFGHTEMPS124col1[]", "ABASDFGHTEMPW125");
//        
//        SessionTool sessionTool = SessionTool.getInstance(request);
//        
//        ConcurrentHashMap<String, Tab> map = new ConcurrentHashMap<String, Tab>();
//        Tab tab = new Tab();
//        tab.setRandomId("ABASDFGHTEMPT123");
//        Section section = new Section();
//        section.prepareSectionColumns();
//        section.setRandomId("ABASDFGHTEMPS124");
//        Widget widget = new Widget();
//        widget.setRandomId("ABASDFGHTEMPW125");
//        widget.setOrderNumber(1);
//        section.addWidget(widget);
//        tab.addSection(section);
//        Widget widget1 = new Widget();
//        widget1.setRandomId("ABASDFGHTEMPW126");
//        widget1.setOrderNumber(2);
//        section.addWidget(widget1);
//        tab.addSection(section);
//        map.put("ABASDFGHTEMPT123", tab);
//        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, map);
//        
//        WidgetController controller = new WidgetController();
//        String result = controller.updateWidgetOrder(request, response, model);
//        
//        Map sessionMap = (Map) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
//        Tab resultTab = (Tab) sessionMap.get("ABASDFGHTEMPT123");
//        
//        assertEquals(result, "true");
//        assertEquals(resultTab.getSection("ABASDFGHTEMPS124").getWidget("ABASDFGHTEMPW126").getOrderNumber(), 1);
//        assertEquals(resultTab.getSection("ABASDFGHTEMPS124").getWidget("ABASDFGHTEMPW125").getOrderNumber(), 2);
//    }
//    
//    @Test
//    public void test_updateWidgetOrder_no_tabId() {
//        
//        ModelMap model = new ModelMap();
//        request.setParameter("section[]", "ABASDFGHTEMPS124");
//        request.setParameter("secABASDFGHTEMPS124col0[]", "ABASDFGHTEMPW126");
//        request.addParameter("secABASDFGHTEMPS124col0[]", "ABASDFGHTEMPW125");
//        
//        WidgetController controller = new WidgetController();
//        String result = controller.updateWidgetOrder(request, response, model);
//        
//        assertEquals(result, "false");
//    }
//    
//    @Test
//    public void test_updateWidgetOrder_no_sectionIds() {
//        
//        ModelMap model = new ModelMap();
//        request.setParameter("tabID", "ABASDFGHTEMPT123");
//        
//        WidgetController controller = new WidgetController();
//        String result = controller.updateWidgetOrder(request, response, model);
//        
//        assertEquals(result, "false");
//    }
//    
//    @Test
//    public void test_updateWidgetOrder_invalid_tabId() {
//        
//        ModelMap model = new ModelMap();
//        request.setParameter("section[]", "AB<SDFGHTEMPS124");
//        request.setParameter("secABASDFGHTEMPS124col0[]", "ABASDFGHTEMPW126");
//        request.addParameter("secABASDFGHTEMPS124col0[]", "ABASDFGHTEMPW125");
//        
//        WidgetController controller = new WidgetController();
//        String result = controller.updateWidgetOrder(request, response, model);
//        
//        assertEquals(result, "false");
//    }
//    
//    @Test
//    public void test_updateWidgetOrder_invalid_widgetId() {
//        
//        ModelMap model = new ModelMap();
//        request.setParameter("tabID", "ABASDFGHTEMPT123");
//        request.setParameter("section[]", "ABASDFGHTEMPS124");
//        request.setParameter("secABASDFGHTEMPS124col1[]", "A<ASDFGHTEMPW126");
//        request.addParameter("secABASDFGHTEMPS124col1[]", "ABASDFGHTEMPW125");
//        
//        WidgetController controller = new WidgetController();
//        String result = controller.updateWidgetOrder(request, response, model);
//        
//        assertEquals(result, "false");
//    }
//    
//    @Test
//    public void test_deleteWidget() {
//        createAuthentication(userAccount);
//        
//        ModelMap model = new ModelMap();
//        request.setParameter("tabID", "ABASDFGHTEMPT126");
//        request.setParameter("sectionID", "ABASDFGHTEMPS127");
//        request.setParameter("widgetID", "ABASDFGHTEMPW128");
//        request.setParameter("colID", "1");
//        
//        SessionTool sessionTool = SessionTool.getInstance(request);
//        
//        ConcurrentHashMap<String, Tab> map = new ConcurrentHashMap<String, Tab>();
//        Tab tab = new Tab();
//        tab.setRandomId("ABASDFGHTEMPT123");
//        Section section = new Section();
//        section.prepareSectionColumns();
//        section.setRandomId("ABASDFGHTEMPS124");
//        Widget widget = new Widget();
//        widget.setRandomId("ABASDFGHTEMPW125");
//        section.addWidget(widget);
//        tab.addSection(section);
//        map.put("ABASDFGHTEMPT123", tab);
//        
//        Tab tab1 = new Tab();
//        tab1.setRandomId("ABASDFGHTEMPT126");
//        Section section1 = new Section();
//        section1.setRandomId("ABASDFGHTEMPS127");
//        section1.prepareSectionColumns();
//        Widget widget1 = new Widget();
//        widget1.setRandomId("ABASDFGHTEMPW128");
//        widget1.setOrderNumber(1);
//        widget1.setColumnId(1);
//        section1.addWidget(widget1);
//        Widget widget2 = new Widget();
//        widget2.setRandomId("ABASDFGHTEMPW129");
//        widget2.setOrderNumber(2);
//        widget2.setColumnId(1);
//        section1.addWidget(widget2);
//        assertEquals(2, section1.getWidgets().size());
//        
//        tab1.addSection(section1);
//        map.put("ABASDFGHTEMPT126", tab1);
//        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, map);
//        
//        WidgetController controller = new WidgetController();
//        String result = controller.deleteWidget(request, response, model);
//        
//        assertEquals("true", result);
//        Tab resultTab = (Tab) ((Map) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP)).get("ABASDFGHTEMPT126");
//        assertEquals(1, resultTab.getSection("ABASDFGHTEMPS127").getSectionColumn("1").getWidgets().size());
//    }
//    
//    @Test
//    public void test_deleteWidget_no_tabID() {
//        
//        ModelMap model = new ModelMap();
//        request.setParameter("sectionID", "ABASDFGHTEMPS127");
//        request.setParameter("widgetID", "ABASDFGHTEMPW128");
//        
//        WidgetController controller = new WidgetController();
//        String result = controller.deleteWidget(request, response, model);
//        
//        assertEquals(result, "false");
//    }
//    
//    @Test
//    public void test_deleteWidget_no_sectionID() {
//        
//        ModelMap model = new ModelMap();
//        request.setParameter("tabID", "ABASDFGHTEMPT126");
//        request.setParameter("widgetID", "ABASDFGHTEMPW128");
//        
//        WidgetController controller = new WidgetController();
//        String result = controller.deleteWidget(request, response, model);
//        
//        assertEquals(result, "false");
//    }
//    
//    @Test
//    public void test_deleteWidget_no_widgetID() {
//        
//        ModelMap model = new ModelMap();
//        request.setParameter("tabID", "ABASDFGHTEMPT126");
//        request.setParameter("sectionID", "ABASDFGHTEMPS127");
//        
//        WidgetController controller = new WidgetController();
//        String result = controller.deleteWidget(request, response, model);
//        
//        assertEquals(result, "false");
//    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD);
        user.setUserPermissions(permissionList);
        user.setIsComplexPassword(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                
                return userAccount;
            }
        };
    }
}
