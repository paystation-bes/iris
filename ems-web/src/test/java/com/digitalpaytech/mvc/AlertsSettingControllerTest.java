package com.digitalpaytech.mvc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.AlertClassType;
import com.digitalpaytech.domain.AlertThresholdType;
import com.digitalpaytech.domain.AlertType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAlertEmail;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.CustomerEmail;
import com.digitalpaytech.domain.CustomerType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.RoutePOS;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.mvc.support.AlertEditForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.AlertTypeService;
import com.digitalpaytech.service.CustomerAlertTypeService;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.testing.services.impl.CustomerAlertTypeServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObject;
import com.digitalpaytech.validation.AlertSettingValidator;

public class AlertsSettingControllerTest {
    
    private static final String ALERT_ID = "alertID";
    private static final int ID_1 = 1;
    private static final int ID_2 = 2;
    private static final int ID_3 = 3;
    private static final float FLOAT_120 = 120f;
    
    private static final String ORDER_BY = "orderBy";
    private static final String SORT_BY = "sortBy";
    private static final String SORT_ORDER_ASC = "asc";
    private static final String SORT_ORDER_DESC = "desc";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_TYPE = "type";
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private ModelMap model;
    private UserAccount userAccount;
    private AlertsSettingController controller;
    private WebSecurityUtil util;
    
    @Before
    public final void setUp() throws Exception {
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        this.session = new MockHttpSession();
        this.request.setSession(this.session);
        this.model = new ModelMap();
        
        this.userAccount = new UserAccount();
        this.userAccount.setId(ID_2);
        final Set<UserRole> roles = new HashSet<UserRole>();
        final UserRole role = new UserRole();
        final Role r = new Role();
        final Set<RolePermission> rps = new HashSet<RolePermission>();
        
        final RolePermission rp1 = new RolePermission();
        rp1.setId(ID_1);
        final Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS);
        rp1.setPermission(permission1);
        
        final RolePermission rp2 = new RolePermission();
        rp2.setId(ID_2);
        final Permission permission2 = new Permission();
        permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT);
        rp2.setPermission(permission2);
        
        rps.add(rp1);
        rps.add(rp2);
        
        r.setId(ID_3);
        r.setRolePermissions(rps);
        
        role.setId(ID_3);
        role.setRole(r);
        roles.add(role);
        this.userAccount.setUserRoles(roles);
        
        final Customer customer = new Customer();
        customer.setId(ID_2);
        final CustomerType customerType = new CustomerType();
        customerType.setId(ID_2);
        customer.setCustomerType(customerType);
        customer.setIsMigrated(true);
        
        this.userAccount.setCustomer(customer);
        createAuthentication(this.userAccount);
        
        this.controller = new AlertsSettingController();
        this.controller.setAlertTypeService(new AlertTypeServiceTestImpl());
        this.controller.setRouteService(new RouteServiceTestImpl());
        this.controller.setCustomerAlertTypeService(createCustomerAlertTypeService());
        
        this.util = new WebSecurityUtil();
        this.util.setUserAccountService(new UserAccountServiceTestImpl());
        this.util.setEntityService(new EntityServiceTestImpl());
        
        WidgetMetricsHelper.registerComponents();
    }
    
    @After
    public final void cleanUp() throws Exception {
        this.request = null;
        this.response = null;
        this.session = null;
        this.model = null;
        this.userAccount = null;
    }
    
    @Test
    public final void testGetAlertList() {
        
        final String result = this.controller.getAlertListSuper(this.request, this.response, this.model, "/settings/alerts/index");
        assertNotNull(result);
        assertEquals(result, "/settings/alerts/index");
    }
    
    // Permission Checks moved to Customer and System Admin pages
    //    @Test
    //    public void test_getAlertList_role_failure() {
    //        createFailedAuthentication();
    //        
    //        WebSecurityUtil util = new WebSecurityUtil();
    //        util.setUserAccountService(new UserAccountServiceTestImpl());
    //        util.setEntityService(new EntityServiceTestImpl());
    //        
    //        AlertsSettingController
    //        String result = this.controller.getAlertList(this.request, this.response, model, "/settings/alerts/index");
    //        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    //        assertEquals(response.getStatus(), HttpStatus.UNAUTHORIZED.value());
    //    }
    
    @Test
    public final void testSortAlertList() {
        
        final String result = this.controller.sortAlertListSuper(this.request, this.response, this.model);
        assertNotNull(result);
    }
    
    @Test
    public final void testSortAlertListParameterSortByname() {
        this.request.setParameter(SORT_BY, COLUMN_NAME);
        
        final String result = this.controller.sortAlertListSuper(this.request, this.response, this.model);
        assertNotNull(result);
    }
    
    @Test
    public final void testSortAlertListParameterSortBytype() {
        this.request.setParameter(SORT_BY, COLUMN_TYPE);
        
        final String result = this.controller.sortAlertListSuper(this.request, this.response, this.model);
        assertNotNull(result);
    }
    
    @Test
    public final void testSortAlertListParameterSortBynameOrderByAsc() {
        this.request.setParameter(SORT_BY, COLUMN_NAME);
        this.request.setParameter(ORDER_BY, SORT_ORDER_ASC);
        
        final String result = this.controller.sortAlertListSuper(this.request, this.response, this.model);
        assertNotNull(result);
    }
    
    @Test
    public final void testSortAlertListParameterSortBynameOrderByDesc() {
        this.request.setParameter(SORT_BY, COLUMN_NAME);
        this.request.setParameter(ORDER_BY, SORT_ORDER_DESC);
        
        final String result = this.controller.sortAlertListSuper(this.request, this.response, this.model);
        assertNotNull(result);
    }
    
    @Test
    public final void testSortAlertListParameterSortBytypeOrderByAsc() {
        this.request.setParameter(SORT_BY, COLUMN_TYPE);
        this.request.setParameter(ORDER_BY, SORT_ORDER_ASC);
        
        final String result = this.controller.sortAlertListSuper(this.request, this.response, this.model);
        assertNotNull(result);
    }
    
    @Test
    public final void testSortAlertListParameterSortBytypeOrderByDesc() {
        this.request.setParameter(SORT_BY, COLUMN_TYPE);
        this.request.setParameter(ORDER_BY, SORT_ORDER_DESC);
        
        final String result = this.controller.sortAlertListSuper(this.request, this.response, this.model);
        assertNotNull(result);
    }
    
    @Test
    public final void testSortAlertListParameterSortBydifferent() {
        this.request.setParameter(SORT_BY, "aaa");
        
        final String result = this.controller.sortAlertListSuper(this.request, this.response, this.model);
        assertNotNull(result);
    }
    
    @Test
    public final void testViewAlertDetails() {
        final WebObject webObj = getCustomerAlertType();
        this.request.setParameter(ALERT_ID, webObj.getPrimaryKey().toString());
        
        final String result = this.controller.viewAlertDetailsSuper(this.request, this.response, this.model);
        assertNotNull(result);
    }
    
    @Test
    public final void testViewAlertDetailsWithRoute() {
        final WebObject webObj = getCustomerAlertType();
        this.request.setParameter(ALERT_ID, webObj.getPrimaryKey().toString());
        
        final String result = this.controller.viewAlertDetailsSuper(this.request, this.response, this.model);
        assertNotNull(result);
    }
    
    @Test
    public final void testViewAlertDetailsRoleFailure() {
        createFailedAuthentication();
        
        final String result = this.controller.viewAlertDetailsSuper(this.request, this.response, this.model);
        
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public final void testViewAlertDetailsInvalidAlertId() {
        this.request.setParameter(ALERT_ID, "1234567");
        
        final String result = this.controller.viewAlertDetailsSuper(this.request, this.response, this.model);
        
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public final void testSaveAlert() {
        
        this.controller.setAlertSettingValidator(new AlertSettingValidator() {
            @Override
            public void validate(final WebSecurityForm<AlertEditForm> command, final Errors errors, final RandomKeyMapping keyMapping) {
                
            }
            
            public boolean validateMigrationStatus(final WebSecurityForm<AlertEditForm> webSecurityForm) {
                return true;
            }
        });
        //        this.controller.setAlertTypeService(new AlertTypeServiceImpl() {
        //            public EventSeverityType findEventSeverityTypeById(int eventSeverityTypeId) {
        //                return new EventSeverityType();
        //            }
        //        });
        
        final WebSecurityForm<AlertEditForm> webSecurityForm = getUserInputForm(0);
        final String result = this.controller.saveAlertSuper(webSecurityForm, new BeanPropertyBindingResult(webSecurityForm, "alertEditForm"),
                                                             this.request, this.response, this.model);
        
        assertNotNull(result);
        assertTrue(result.contains(WebCoreConstants.RESPONSE_TRUE));
    }
    
    @Test
    public final void testSaveAlertAllPaystations() {
        
        this.controller.setAlertSettingValidator(new AlertSettingValidator() {
            @Override
            public void validate(final WebSecurityForm<AlertEditForm> command, final Errors errors, final RandomKeyMapping keyMapping) {
                
            }
            
            public boolean validateMigrationStatus(final WebSecurityForm<AlertEditForm> webSecurityForm) {
                return true;
            }
            
        });
        
        final WebSecurityForm<AlertEditForm> webSecurityForm = getUserInputForm(0);
        final String result = this.controller.saveAlertSuper(webSecurityForm, new BeanPropertyBindingResult(webSecurityForm, "alertEditForm"),
                                                             this.request, this.response, this.model);
        
        assertNotNull(result);
        assertTrue(result.contains(WebCoreConstants.RESPONSE_TRUE));
    }
    
    // Permission Checks moved to Customer and System Admin pages
    //    @Test
    //    public void test_saveAlert_role_failure() {
    //        createFailedAuthentication();
    //        
    //        AlertsSettingController
    //        WebSecurityUtil util = new WebSecurityUtil();
    //        util.setUserAccountService(new UserAccountServiceTestImpl());
    //        util.setEntityService(new EntityServiceTestImpl());
    //        WidgetMetricsHelper.registerComponents();
    //        
    //        WebSecurityForm webSecurityForm = getUserInputForm(0);
    //        String res = this.controller.saveAlert(webSecurityForm, new BeanPropertyBindingResult(webSecurityForm, "alertEditForm"), 
    //        this.request, this.response, this.model);
    //        
    //        assertEquals(res, WebCoreConstants.RESPONSE_FALSE);
    //        assertEquals(response.getStatus(), HttpStatus.UNAUTHORIZED.value());
    //    }
    
    @Test
    public final void testSaveAlertValidationFailure() {
        
        this.controller.setAlertSettingValidator(new AlertSettingValidator() {
            @Override
            public void validate(final WebSecurityForm<AlertEditForm> webSecurityForm, final Errors errors, final RandomKeyMapping keyMapping) {
                webSecurityForm.removeAllErrorStatus();
                webSecurityForm.addErrorStatus(new FormErrorStatus("postToken", "error.common.invalid.posttoken"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("alertName", "error.common.required"));
            }
            
            public boolean validateMigrationStatus(final WebSecurityForm<AlertEditForm> webSecurityForm) {
                return true;
            }
            
        });
        
        final WebSecurityForm<AlertEditForm> webSecurityForm = getUserInputForm(0);
        final String res = this.controller.saveAlertSuper(webSecurityForm, new BeanPropertyBindingResult(webSecurityForm, "alertEditForm"),
                                                          this.request, this.response, this.model);
        
        assertNotNull(res);
        assertTrue(res.contains("\"errorStatus\":[{\"errorFieldName\":\"postToken\",\"message\":\"error.common.invalid.posttoken\"}"
                                + ",{\"errorFieldName\":\"alertName\",\"message\":\"error.common.required\"}]}"));
    }
    
    @Test
    public final void testSaveAlertEditAlert() {
        
        this.controller.setAlertSettingValidator(new AlertSettingValidator() {
            @Override
            public void validate(final WebSecurityForm<AlertEditForm> webSecurityForm, final Errors errors, final RandomKeyMapping keyMapping) {
                
            }
            
            public boolean validateMigrationStatus(final WebSecurityForm<AlertEditForm> webSecurityForm) {
                return true;
            }
            
        });
        
        final WebSecurityForm<AlertEditForm> webSecurityForm = getUserInputForm(ID_1);
        final String result = this.controller.saveAlertSuper(webSecurityForm, new BeanPropertyBindingResult(webSecurityForm, "alertEditForm"),
                                                             this.request, this.response, this.model);
        
        assertNotNull(result);
        assertTrue(result.contains(WebCoreConstants.RESPONSE_TRUE));
    }
    
    @Test
    public final void testDeleteAlert() {
        final WebObject webObj = getCustomerAlertType();
        this.request.setParameter(ALERT_ID, webObj.getPrimaryKey().toString());
        
        final String result = this.controller.deleteAlertSuper(this.request, this.response, this.model);
        
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_TRUE);
    }
    
    @Test
    public final void testDeleteAlertRoleFailure() {
        createFailedAuthentication();
        
        this.controller.setAlertSettingValidator(new AlertSettingValidator() {
            public String getMessage(final String str) {
                return "TEST";
            }
        });
        
        final String result = this.controller.deleteAlertSuper(this.request, this.response, this.model);
        
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public final void testDeleteAlertInvalidAlertID() {
        this.request.setParameter(ALERT_ID, "123456");
        
        final String result = this.controller.deleteAlertSuper(this.request, this.response, this.model);
        
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    private void createAuthentication(final UserAccount newUserAccount) {
        final Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        final WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsMigrated(true);
        user.setIsComplexPassword(true);
        final Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        final Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private void createFailedAuthentication() {
        final Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        final WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsMigrated(true);
        user.setIsComplexPassword(true);
        final Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        final Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(ID_2);
        permissionList.add(ID_3);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private WebObject getCustomerAlertType() {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(this.request).getKeyMapping();
        final CustomerAlertType type = new CustomerAlertType();
        type.setId(ID_1);
        type.setName("TestCustomerAlertType1");
        return (WebObject) keyMapping.hideProperty(type, "id");
    }
    
    private WebSecurityForm<AlertEditForm> getUserInputForm(final int actionFlag) {
        final WebSecurityForm<AlertEditForm> form = new WebSecurityForm<AlertEditForm>();
        form.setActionFlag(actionFlag);
        final AlertEditForm alertForm = new AlertEditForm();
        alertForm.setAlertName("Bill Over 5 Dollars");
        alertForm.setStatus(true);
        alertForm.setAlertTypeId((short) 2);
        alertForm.setAlertThresholdExceed("140");
        alertForm.setRouteId(ID_2);
        final List<String> alertContacts = new ArrayList<String>();
        alertContacts.add("email@digitalpaytech.com ");
        alertContacts.add("email2@digitalpaytech.com");
        alertContacts.add("email4@digitalpaytech.com");
        alertForm.setAlertContacts(alertContacts);
        form.setWrappedObject(alertForm);
        return form;
    }
    
    private class UserAccountServiceTestImpl implements UserAccountService {
        @Override
        public UserAccount findUserAccount(final int userAccountId) {
            final UserAccount ua = new UserAccount();
            ua.setId(ID_2);
            return ua;
        }
        
        public UserAccount findUserAccountAndEvict(final int userAccountId) {
            return null;
        }
        
        @Override
        public List<Permission> findPermissions(final int userAccountId) {
            return null;
        }
        
        @Override
        public List<Role> findRoles(final int userAccountId) {
            return null;
        }
        
        @Override
        public UserAccount findUserAccount(final String userName) {
            return null;
        }
        
        @Override
        public void updatePassword(final UserAccount newUserAccount) {
            
        }
        
        @Override
        public Collection<UserAccount> findUserAccountsByCustomerIdAndUserAccountTypeId(final Integer customerId) {
            return null;
        }
        
        @Override
        public Collection<UserAccount> findUserAccountsByCustomerIdAndControllerCustomerIdAndUserAccountType(final Integer customerId,
            final Integer controllerCustomerId) {
            return null;
        }
        
        @Override
        public void updateUserAccount(final UserAccount newUserAccount) {
        }
        
        @Override
        public void updateUserAccountAndUserRoles(final UserAccount newUserAccount) {
        }
        
        @Override
        public void saveUserAccountAndUserRole(final UserAccount newUserAccount) {
        }
        
        @Override
        public Collection<UserAccount> findUserAccountByCustomerIdAndRoleId(final Integer customerId, final Integer roleId) {
            return null;
        }
        
        @Override
        public void saveOrUpdateUserAccount(final UserAccount newUserAccount) {
        }
        
        @Override
        public UserAccount findAdminUserAccountByChildCustomerId(final int customerId) {
            return null;
        }
        
        @Override
        public UserAccount findAdminUserAccountByParentCustomerId(final int customerId) {
            return null;
        }
        
        @Override
        public Collection<UserAccount> findUserAccountsByCustomerId(final int customerId) {
            return null;
        }
        
        @Override
        public List<RolePermission> findUserRoleAndPermission(final Integer userAccountId) {
            return null;
        }
        
        @Override
        public List<RolePermission> findUserRoleAndPermissionForCustomerType(final Integer userAccountId, final Integer customerTypeId) {
            return null;
        }
        
        @Override
        public UserAccount findUndeletedUserAccount(final String userName) {
            return null;
        }
        
        @Override
        public UserAccount findUserAccountForLogin(final int userAccountId) {
            return null;
        }
        
        @Override
        public void deleteUserRoles(final UserAccount newUserAccount) {
        }
        
        @Override
        public List<UserAccount> findUserAccountsByCustomerIdAndMobileApplicationTypeId(final int customerId, final int mobileApplicationTypeId) {
            return null;
        }
        
        @Override
        public List<UserAccount> findChildUserAccountsByParentUserId(final Integer parentUserAccountId) {
            return null;
        }
        
        @Override
        public List<UserAccount> findAdminUserAccountsForParent(final Integer customerId, final Integer parentCustomerId) {
            return null;
        }
        
        @Override
        public List<UserAccount> findAdminUserAccountWithIsAllChilds(final Integer customerId) {
            return null;
        }
        
        @Override
        public Collection<UserAccount> findAliasUserAccountsByUserAccountId(final Integer userAccountId) {
            return null;
        }
        
        @Override
        public UserAccount findAliasUserAccountByUserAccountIdAndCustomerId(final Integer userAccountId, final Integer customerId) {
            return null;
        }
        
        @Override
        public UserAccount findUserRoleByUserAccountIdAndRoleId(final Integer roleId, final Integer userAccountId) {
            // TODO Auto-generated method stub
            return null;
        }
        
        @Override
        public List<UserRole> findActiveUserRoleByUserAccountId(final Integer userAccountId) {
            // TODO Auto-generated method stub
            return null;
        }
        
        @Override
        public List<UserAccount> findUserAccountByRoleIdAndCustomerIdList(final Integer roleId, final List<Integer> customerIdList) {
            // TODO Auto-generated method stub
            return null;
        }
        
        @Override
        public List<UserAccount> findUserAccountByRoleIdAndCustomerId(final Integer roleId, final Integer customerId) {
            // TODO Auto-generated method stub
            return null;
        }
        
        @Override
        public Customer findChildCustomer(Integer parentCustomerId, String childCustomerName) {
            // TODO Auto-generated method stub
            return null;
        }
        
        @Override
        public UserAccount findUserAccountByLinkUserName(final String linkUserName) {
            return null;
        }
    }
    
    private class AlertTypeServiceTestImpl implements AlertTypeService {
        private AlertClassType communicationAlert = new AlertClassType(WebCoreConstants.ALERT_CLASS_TYPE_COMMUNICATION, "Communication", new Date(),
                1);
        private AlertClassType collectionAlert = new AlertClassType(WebCoreConstants.ALERT_CLASS_TYPE_COLLECTION, "Collection", new Date(), 1);
        private AlertClassType paystationAlert = new AlertClassType(WebCoreConstants.ALERT_CLASS_TYPE_PAY_STATION_ALERT, "Pay Station Alert",
                new Date(), 1);
        
        private AlertType lastSeenInterval = new AlertType(WebCoreConstants.ALERTTYPE_ID_LASTSEENINTERVAL, this.communicationAlert,
                "Last Seen Interval", true, new Date(), 1);
        private AlertType runningTotal = new AlertType(WebCoreConstants.ALERTTYPE_ID_RUNNINGTOTAL, this.collectionAlert, "Running Total", true,
                new Date(), 1);
        private AlertType coinCanister = new AlertType(WebCoreConstants.ALERTTYPE_ID_COINCANNISTER, this.collectionAlert, "Coin Canister", true,
                new Date(), 1);
        private AlertType billStacker = new AlertType(WebCoreConstants.ALERTTYPE_ID_BILLSTACKER, this.collectionAlert, "Bill Stacker", true,
                new Date(), 1);
        private AlertType unsettledCC = new AlertType(WebCoreConstants.ALERTTYPE_ID_UNSETTLEDCREDITCARD, this.collectionAlert,
                "Unsettled Credit Card", true, new Date(), 1);
        private AlertType psAlert = new AlertType(WebCoreConstants.ALERTTYPE_ID_PAYSTATIONALERT, this.paystationAlert, "Paystation Alert", false,
                new Date(), 1);
        
        @Override
        public Collection<AlertClassType> findAllAlertClassTypes() {
            final Collection<AlertClassType> result = new ArrayList<AlertClassType>();
            result.add(this.communicationAlert);
            result.add(this.collectionAlert);
            result.add(this.paystationAlert);
            return result;
        }
        
        @Override
        public Collection<AlertType> findAlertTypeByIsUserDefined(final boolean isUserDefined) {
            final Collection<AlertType> result = new ArrayList<AlertType>();
            final AlertClassType alertClassType = new AlertClassType();
            alertClassType.setId((byte) 2);
            alertClassType.setName("Collection");
            result.add(this.lastSeenInterval);
            result.add(this.runningTotal);
            result.add(this.coinCanister);
            result.add(this.billStacker);
            result.add(this.unsettledCC);
            result.add(this.psAlert);
            return result;
        }
        
        @Override
        public List<AlertType> findAllAlertTypes() {
            final List<AlertType> result = new ArrayList<AlertType>();
            result.add(this.lastSeenInterval);
            result.add(this.runningTotal);
            result.add(this.coinCanister);
            result.add(this.billStacker);
            result.add(this.unsettledCC);
            result.add(this.psAlert);
            return result;
        }
        
        @Override
        public AlertThresholdType findAlertThresholdTypeById(final int id) {
            AlertType alertType = null;
            String name = "";
            switch (id) {
                case WebCoreConstants.ALERT_THRESHOLD_TYPE_LAST_SEEN_INTERVAL_HOUR:
                    alertType = this.lastSeenInterval;
                    name = "Last Seen Interval Hour";
                    break;
                case WebCoreConstants.ALERT_THRESHOLD_TYPE_RUNNING_TOTAL_DOLLAR:
                    alertType = this.runningTotal;
                    name = "Running Total Dollar";
                    break;
                case WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_COUNT:
                    alertType = this.coinCanister;
                    name = "Coin Canister Count";
                    break;
                case WebCoreConstants.ALERT_THRESHOLD_TYPE_COIN_CANISTER_DOLLARS:
                    alertType = this.coinCanister;
                    name = "Coin Canister Dollar";
                    break;
                case WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_COUNT:
                    alertType = this.billStacker;
                    name = "Bill Staker Count";
                    break;
                case WebCoreConstants.ALERT_THRESHOLD_TYPE_BILL_STACKER_DOLLARS:
                    alertType = this.billStacker;
                    name = "Bill Staker Dollar";
                    break;
                case WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_COUNT:
                    alertType = this.unsettledCC;
                    name = "Unsettled Credit Card Count";
                    break;
                case WebCoreConstants.ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_DOLLARS:
                    alertType = this.unsettledCC;
                    name = "Unsettled Credit Card Dollar";
                    break;
                case WebCoreConstants.ALERT_THRESHOLD_TYPE_PAYSTATION:
                    alertType = this.psAlert;
                    name = "Pay Station Alert";
                    break;
                default:
                    break;
            
            }
            return new AlertThresholdType(id, alertType, name, new Date(), 1);
        }
        
        @Override
        public EventSeverityType findEventSeverityTypeById(final int eventSeverityTypeId) {
            
            return null;
        }
        
        @Override
        public List<String> findAlertThresholdTypeText(final Collection<Integer> ids) {
            
            return null;
        }
        
    }
    
    private class EntityServiceTestImpl implements EntityService {
        public Object merge(final Object entity) {
            return AlertsSettingControllerTest.this.userAccount;
        }
        
        @Override
        public <T> List<T> loadAll(final Class<T> entityClass) {
            
            return null;
        }
        
        @Override
        public void evict(final Object entity) {
            
        }
    }
    
    private CustomerAlertTypeService createCustomerAlertTypeService() {
        
        return new CustomerAlertTypeServiceTestImpl() {
            public CustomerAlertType findCustomerAlertTypeById(final Integer id) {
                final CustomerAlertType type = new CustomerAlertType();
                final AlertClassType classType = new AlertClassType((byte) 1, "TestClassType", new Date(), 1);
                final AlertType alertType = new AlertType((short) 1, classType, "TestAlertType", true, new Date(), 1);
                final AlertThresholdType alerThresholdType = new AlertThresholdType(1, alertType, "TestAlertType", new Date(), 1);
                type.setAlertThresholdType(alerThresholdType);
                type.setThreshold(FLOAT_120);
                final Route r = new Route();
                r.setId(ID_2);
                type.setRoute(r);
                return type;
            }
            
            @Override
            public CustomerAlertType findDefaultCustomerAlertTypeByCustomerIdAndAlertThresholdTypeId(final Integer customerId,
                final int alertThresholdTypeId) {
                final CustomerAlertType type = new CustomerAlertType();
                final AlertClassType classType = new AlertClassType((byte) 1, "TestClassType", new Date(), 1);
                final AlertType alertType = new AlertType((short) 1, classType, "TestAlertType", true, new Date(), 1);
                final AlertThresholdType alerThresholdType = new AlertThresholdType((short) 1, alertType, "TestAlertType", new Date(), 1);
                type.setAlertThresholdType(alerThresholdType);
                type.setThreshold(FLOAT_120);
                type.setId(ID_2);
                
                return type;
            }
            
            @Override
            public Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdAlertThresholdTypeIdAndIsActive(final Integer customerId,
                final Integer alertThresholdTypeId, final boolean isActive) {
                final Collection<CustomerAlertType> list = new ArrayList<CustomerAlertType>();
                list.add(new CustomerAlertType());
                
                return list;
            }
            
            @Override
            public CustomerAlertType findCustomerAlertTypeByIdAndEvict(final Integer id) {
                final CustomerAlertType type = new CustomerAlertType();
                final AlertClassType classType = new AlertClassType((byte) 1, "TestClassType", new Date(), 1);
                final AlertType alertType = new AlertType((short) 1, classType, "TestAlertType", true, new Date(), 1);
                final AlertThresholdType alerThresholdType = new AlertThresholdType((short) 1, alertType, "TestAlertType", new Date(), 1);
                final Route r = new Route();
                r.setId(ID_2);
                r.setName("TestRoute");
                type.setAlertThresholdType(alerThresholdType);
                type.setThreshold(FLOAT_120);
                type.setRoute(r);
                
                final Set<CustomerAlertEmail> alertEmails = new HashSet<CustomerAlertEmail>();
                alertEmails.add(new CustomerAlertEmail(ID_1, new CustomerEmail(userAccount.getCustomer(), "email2@digitalpaytech.com", new Date(), 1,
                        false), type, new Date(), 1, false));
                alertEmails.add(new CustomerAlertEmail(ID_2, new CustomerEmail(userAccount.getCustomer(), "email3@digitalpaytech.com", new Date(), 1,
                        false), type, new Date(), 1, false));
                alertEmails.add(new CustomerAlertEmail(ID_3, new CustomerEmail(userAccount.getCustomer(), "email31@digitalpaytech.com", new Date(),
                        1, false), type, new Date(), 1, false));
                type.setCustomerAlertEmails(alertEmails);
                return type;
            }
            
            public Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdOrderByNameAsc(final Integer customerId, final Integer filterType) {
                final Collection<CustomerAlertType> result = new ArrayList<CustomerAlertType>();
                
                final AlertType alertType = new AlertType();
                alertType.setId((short) 1);
                alertType.setName("aAlert1");
                final Customer customer = new Customer();
                customer.setId(ID_2);
                customer.setName("momomomo");
                final AlertClassType alertClassType1 = new AlertClassType();
                alertClassType1.setId((byte) 1);
                alertClassType1.setName("aClassType1");
                alertType.setAlertClassType(alertClassType1);
                
                final AlertType alertType1 = new AlertType();
                alertType1.setId((short) 2);
                alertType1.setName("bAlert1");
                final AlertClassType alertClassType2 = new AlertClassType();
                alertClassType2.setId((byte) 2);
                alertClassType2.setName("bClassType1");
                alertType1.setAlertClassType(alertClassType2);
                
                final AlertThresholdType alertThresholdType = new AlertThresholdType((short) 1, alertType, "aAlert1", new Date(), 1);
                final AlertThresholdType alertThresholdType1 = new AlertThresholdType((short) 2, alertType1, "bAlert1", new Date(), 1);
                result.add(new CustomerAlertType(alertThresholdType, customer, "aAlert1", StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1, true,
                        false, false, new Date(), 1));
                result.add(new CustomerAlertType(alertThresholdType1, customer, "bAlert1", StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1, true,
                        false, false, new Date(), 1));
                return result;
            }
            
            public Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdOrderByNameDesc(final Integer customerId, final Integer filterType) {
                final Collection<CustomerAlertType> result = new ArrayList<CustomerAlertType>();
                final AlertType alertType = new AlertType();
                alertType.setId((short) 1);
                alertType.setName("aAlert1");
                final Customer customer = new Customer();
                customer.setId(ID_2);
                customer.setName("momomomo");
                final AlertClassType alertClassType1 = new AlertClassType();
                alertClassType1.setId((byte) 1);
                alertClassType1.setName("aClassType1");
                alertType.setAlertClassType(alertClassType1);
                
                final AlertType alertType1 = new AlertType();
                alertType1.setId((short) 2);
                alertType1.setName("bAlert1");
                final AlertClassType alertClassType2 = new AlertClassType();
                alertClassType2.setId((byte) 2);
                alertClassType2.setName("bClassType1");
                alertType1.setAlertClassType(alertClassType2);
                
                final AlertThresholdType alertThresholdType = new AlertThresholdType((short) 1, alertType, "aAlert1", new Date(), 1);
                final AlertThresholdType alertThresholdType1 = new AlertThresholdType((short) 2, alertType1, "bAlert1", new Date(), 1);
                
                result.add(new CustomerAlertType(alertThresholdType1, customer, "bAlert1", StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1, true,
                        false, false, new Date(), 1));
                result.add(new CustomerAlertType(alertThresholdType, customer, "aAlert1", StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1, true,
                        false, false, new Date(), 1));
                
                return result;
            }
            
            public Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdOrderByAlertTypeAsc(final Integer customerId,
                final Integer filterType) {
                final Collection<CustomerAlertType> result = new ArrayList<CustomerAlertType>();
                final AlertType alertType = new AlertType();
                alertType.setId((short) 1);
                alertType.setName("aAlert1");
                final Customer customer = new Customer();
                customer.setId(ID_2);
                customer.setName("momomomo");
                final AlertClassType alertClassType1 = new AlertClassType();
                alertClassType1.setId((byte) 1);
                alertClassType1.setName("aClassType1");
                alertType.setAlertClassType(alertClassType1);
                
                final AlertType alertType1 = new AlertType();
                alertType1.setId((short) 2);
                alertType1.setName("bAlert1");
                final AlertClassType alertClassType2 = new AlertClassType();
                alertClassType2.setId((byte) 2);
                alertClassType2.setName("bClassType1");
                alertType1.setAlertClassType(alertClassType2);
                
                final AlertThresholdType alertThresholdType = new AlertThresholdType((short) 1, alertType, "aAlert1", new Date(), 1);
                final AlertThresholdType alertThresholdType1 = new AlertThresholdType((short) 2, alertType1, "bAlert1", new Date(), 1);
                
                result.add(new CustomerAlertType(alertThresholdType, customer, "aAlert1", StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1, true,
                        false, false, new Date(), 1));
                result.add(new CustomerAlertType(alertThresholdType1, customer, "bAlert1", StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1, true,
                        false, false, new Date(), 1));
                
                return result;
            }
            
            public Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdOrderByAlertTypeDesc(final Integer customerId,
                final Integer filterType) {
                final Collection<CustomerAlertType> result = new ArrayList<CustomerAlertType>();
                final AlertType alertType = new AlertType();
                alertType.setId((short) 1);
                alertType.setName("aAlert1");
                final Customer customer = new Customer();
                customer.setId(ID_2);
                customer.setName("momomomo");
                final AlertClassType alertClassType1 = new AlertClassType();
                alertClassType1.setId((byte) 1);
                alertClassType1.setName("aClassType1");
                alertType.setAlertClassType(alertClassType1);
                
                final AlertType alertType1 = new AlertType();
                alertType1.setId((short) 2);
                alertType1.setName("bAlert1");
                final AlertClassType alertClassType2 = new AlertClassType();
                alertClassType2.setId((byte) 2);
                alertClassType2.setName("bClassType1");
                alertType1.setAlertClassType(alertClassType2);
                
                final AlertThresholdType alertThresholdType = new AlertThresholdType((short) 1, alertType, "aAlert1", new Date(), 1);
                final AlertThresholdType alertThresholdType1 = new AlertThresholdType((short) 2, alertType1, "bAlert1", new Date(), 1);
                
                result.add(new CustomerAlertType(alertThresholdType1, customer, "bAlert1", StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1, true,
                        false, false, new Date(), 1));
                result.add(new CustomerAlertType(alertThresholdType, customer, "aAlert1", StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1, true,
                        false, false, new Date(), 1));
                
                return result;
            }
            
            @Override
            public void saveCustomerAlertType(final CustomerAlertType customerAlertType) {
                
            }
            
            @Override
            public void updateCustomerAlertType(final CustomerAlertType customerAlertType, final Collection<String> emailAddresses) {
                
            }
            
            @Override
            public void deleteCustomerAlertType(final CustomerAlertType customerAlertType, final Integer userAccountId) {
                
            }
            
            @Override
            public int countByRouteId(final Integer... routeIds) {
                
                return 0;
            }
            
            @Override
            public void disableNonDefaultAlert(final Integer customerId, final Date date, final Integer userId) {
                
            }
        };
    }
    
    private class RouteServiceTestImpl implements RouteService {
        public Route findRouteById(final Integer id) {
            final Route route = new Route();
            route.setId(ID_2);
            route.setName("TestRoute");
            return route;
        }
        
        public Collection<Route> findRoutesByCustomerId(final int customerId) {
            final Collection<Route> routes = new ArrayList<Route>();
            final Route route1 = new Route();
            route1.setId(ID_1);
            route1.setName("TestRoute1");
            final Route route2 = new Route();
            route2.setId(ID_2);
            route2.setName("TestRoute2");
            routes.add(route1);
            routes.add(route2);
            return routes;
        }
        
        public List<PointOfSale> findPointOfSalesByRouteId(final int routeId) {
            final List<PointOfSale> pos = new ArrayList<PointOfSale>();
            final PointOfSale p = new PointOfSale();
            p.setId(ID_1);
            p.setName("SamplePoS");
            pos.add(p);
            return pos;
        }
        
        @Override
        public Collection<Route> findRoutesByCustomerIdAndName(final int customerId, final String name) {
            
            return null;
        }
        
        @Override
        public Collection<Route> findRoutesByCustomerIdAndNameUpperCase(final int customerId, final String name) {
            
            return null;
        }
        
        @Override
        public Route findRouteByIdAndEvict(final Integer id) {
            
            return null;
        }
        
        @Override
        public void saveRouteAndRoutePOS(final Route route) {
            
        }
        
        @Override
        public void updateRouteAndRoutePOS(final Route route) {
            
        }
        
        @Override
        public List<Paystation> findPayStationsByRouteId(final int routeId) {
            
            return null;
        }
        
        @Override
        public List<PointOfSale> findPointOfSalesByRouteId(final int routeId, final boolean includeArchived) {
            
            return null;
        }
        
        @Override
        public List<RoutePOS> findRoutePOSesByPointOfSaleId(final Integer pointOfSaleId) {
            
            return null;
        }
        
        @Override
        public List<Route> findRoutesByPointOfSaleId(final int pointOfSaleId) {
            
            return null;
        }
        
        @Override
        public List<Route> findRoutesByPointOfSaleIdAndRouteTypeId(final int pointOfSaleId, final int routeTypeId) {
            
            return null;
        }
        
        @Override
        public void deleteRoute(final Integer routeId) {
            
        }
        
        @Override
        public RoutePOS findRoutePOSByIdAndPointOfSaleId(final Integer routeId, final Integer pointOfSaleId) {
            
            return null;
        }
        
        @Override
        public Collection<Route> findRoutesByCustomerIdAndRouteTypeId(final int customerId, final int routeTypeId) {
            
            return null;
        }
        
        @Override
        public List<RoutePOS> findRoutePOSesByPointOfSaleIdAndRouteTypeId(final Integer pointOfSaleId, final Integer routeTypeId) {
            
            return null;
        }
        
        @Override
        public List<Route> findRoutesWithPointOfSalesByCustomerId(final Integer customerId) {
            
            return null;
        }
        
        @Override
        public List<Route> findRoutesWithPointOfSalesByCustomerIdAndRouteType(final Integer customerId, final Integer routeTypeId) {
            
            return null;
        }
        
        @Override
        public List<Integer> findPointOfSaleIdsByRouteId(int routeId, boolean includeArchived) {
            // TODO Auto-generated method stub
            return null;
        }
    }
    
}
