package com.digitalpaytech.mvc.customeradmin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

public class LocationsExtendByPhoneControllerTest {

	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	private MockHttpSession session;
	private ModelMap model;
	private UserAccount userAccount;
	
	@Before
	public void setUp(){
		this.request = new MockHttpServletRequest();
		this.response = new MockHttpServletResponse();
		session = new MockHttpSession();
		this.request.setSession(session);
		this.model = new ModelMap();
		
		userAccount = new UserAccount();
		userAccount.setId(2);
		Set<UserRole> roles = new HashSet<UserRole>();
		UserRole role = new UserRole();
		Role r = new Role();
		Set<RolePermission> rps = new HashSet<RolePermission>();
		
		RolePermission rp1 = new RolePermission();
		rp1.setId(1);
		Permission permission1 = new Permission();
		permission1.setId(WebSecurityConstants.PERMISSION_VIEW_ROUTES);
		rp1.setPermission(permission1);
		
		RolePermission rp2 = new RolePermission();
		rp2.setId(2);
		Permission permission2 = new Permission();
		permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_ROUTES);
		rp2.setPermission(permission2);

		RolePermission rp3 = new RolePermission();
		rp3.setId(3);
		Permission permission3 = new Permission();
		permission3.setId(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS);
		rp3.setPermission(permission3);
		
		RolePermission rp4 = new RolePermission();
		rp4.setId(4);
		Permission permission4 = new Permission();
		permission4.setId(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS);
		rp4.setPermission(permission4);
		
		RolePermission rp5 = new RolePermission();
		rp5.setId(5);		
		Permission permission5 = new Permission();
		permission5.setId(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS);
		rp5.setPermission(permission5);
		
		RolePermission rp6 = new RolePermission();
		rp6.setId(6);
		Permission permission6 = new Permission();
		permission6.setId(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS);
		rp6.setPermission(permission6);
		
		rps.add(rp1);
		rps.add(rp2);
		rps.add(rp3);
		rps.add(rp4);
		rps.add(rp5);
		rps.add(rp6);
		
		r.setId(3);
		r.setRolePermissions(rps);
		
		role.setId(3);
		role.setRole(r);
		roles.add(role);
		userAccount.setUserRoles(roles);
		
		Customer customer = new Customer();
		customer.setId(3);
		userAccount.setCustomer(customer);
		createAuthentication(userAccount);
		
		WebSecurityUtil util = new WebSecurityUtil();
		util.setUserAccountService(createStaticUserAccountService());
		util.setEntityService(new EntityServiceImpl() {
			public Object merge(Object entity) {
				return userAccount;
			}
		});
	}	

	private void createAuthentication(UserAccount userAccount) {
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("Admin"));
		WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true,
				authorities);
		user.setIsComplexPassword(true);
		Authentication authentication = new UsernamePasswordAuthenticationToken(
				user, "password", authorities);
		SecurityContextHolder.getContext().setAuthentication(authentication);
	}
	
//========================================================================================================================
// "Extend By Phone" tab
//========================================================================================================================

/*
	@Test
	public void showExtendByPhoneLocationList() {
		LocationService serv = new LocationServiceImpl() {
			@Override
			public List<Location> findLocationsByCustomerId(Integer customerId) {
				return createLocations();
			}
		};
		
		LocationsExtendByPhoneController con = new LocationsExtendByPhoneController();
		con.setLocationService(serv);
		con.showExtendByPhoneLocationList(request, response, model);
		List<Location> list = (List<Location>) model.get("locations");
		assertNotNull(list);
		assertEquals(2, list.size());
		
		
		// It should NOT go to 'findLocationsByCustomerId' again.
		serv = new LocationServiceImpl() {
			@Override
			public List<Location> findLocationsByCustomerId(Integer customerId) {
				return null;
			}
		};
		con.setLocationService(serv);
		con.showExtendByPhoneLocationList(request, response, model);
		list = (List<Location>) model.get("locations");
		assertNotNull(list);
		assertEquals(2, list.size());
	}
	
	private List<Location> createLocations() {
		Location loc1 = new Location();
		loc1.setCustomer(userAccount.getCustomer());
		loc1.setId(1);
		loc1.setName("East");
		loc1.setIsParent(true);

		Location loc2 = new Location();
		loc2.setCustomer(userAccount.getCustomer());
		loc2.setId(2);
		loc2.setName("1201 J Street");
		loc2.setIsParent(false);

		Location loc3 = new Location();
		loc3.setCustomer(userAccount.getCustomer());
		loc3.setId(3);
		loc3.setName("Centennial Park");
		loc3.setIsParent(false);
		
		List<Location> list = new ArrayList<Location>();
		list.add(loc1);
		list.add(loc2);
		list.add(loc3);
		return list;
	}
*/
	private UserAccountService createStaticUserAccountService() {
		return new UserAccountServiceImpl() {
			public UserAccount findUserAccount(int userAccountId) {
				UserAccount ua = new UserAccount();
				ua.setId(2);
				return ua;
			}
		};
	}
}
