package com.digitalpaytech.mvc.customeradmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PaystationType;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosAlertStatus;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.RoutePOS;
import com.digitalpaytech.domain.RouteType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.dto.PointOfSaleSearchCriteria;
import com.digitalpaytech.dto.customeradmin.PaystationListInfo;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.dto.customeradmin.RouteSettingInfo;
import com.digitalpaytech.mvc.customeradmin.support.RouteEditForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.LocationServiceImpl;
import com.digitalpaytech.service.impl.RouteTypeServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.PointOfSaleServiceTestImpl;
import com.digitalpaytech.testing.services.impl.RouteServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObject;
import com.digitalpaytech.validation.customeradmin.RouteSettingValidator;

public class CustomerAdminRoutesControllerTest {
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private ModelMap model;
    private UserAccount userAccount;
    
    @Before
    public void setUp() {
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        session = new MockHttpSession();
        this.request.setSession(session);
        this.model = new ModelMap();
        
        userAccount = new UserAccount();
        userAccount.setId(2);
        Set<UserRole> roles = new HashSet<UserRole>();
        UserRole role = new UserRole();
        Role r = new Role();
        Set<RolePermission> rps = new HashSet<RolePermission>();
        
        RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_VIEW_ROUTES);
        rp1.setPermission(permission1);
        
        RolePermission rp2 = new RolePermission();
        rp2.setId(2);
        Permission permission2 = new Permission();
        permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_ROUTES);
        rp2.setPermission(permission2);
        
        rps.add(rp1);
        rps.add(rp2);
        
        r.setId(3);
        r.setRolePermissions(rps);
        
        role.setId(3);
        role.setRole(r);
        roles.add(role);
        userAccount.setUserRoles(roles);
        
        Customer customer = new Customer();
        customer.setId(2);
        userAccount.setCustomer(customer);
        createAuthentication(userAccount);
    }
    
    @Test
    public void test_getRouteListByCustomer() {
        
        CustomerAdminRoutesController controller = new CustomerAdminRoutesController();
        controller.setRouteService(new RouteServiceTestImpl() {
            public Collection<Route> findRoutesByCustomerId(int customerId) {
                List<Route> result = new ArrayList<Route>();
                Route r = new Route();
                r.setId(1);
                RouteType type = new RouteType();
                type.setId(2);
                type.setName("Maintenance");
                r.setRouteType(type);
                result.add(r);
                return result;
            }
        });
        
        controller.setRouteTypeService(new RouteTypeServiceImpl() {
            public Collection<RouteType> findAllRouteType() {
                List<RouteType> types = new ArrayList<RouteType>();
                types.add(new RouteType(new Integer(1).byteValue(), "Collections", 3243, new Date(), 1));
                types.add(new RouteType(new Integer(2).byteValue(), "Maintenance", 5645, new Date(), 1));
                types.add(new RouteType(new Integer(2).byteValue(), "Other", 2343, new Date(), 1));
                return types;
            }
        });
        
        controller.setPointOfSaleService(createPointOfSaleService());
        
        controller.setLocationService(new LocationServiceImpl() {
            public List<Location> findLocationsByCustomerId(Integer customerId) {
                List<Location> locations = new ArrayList<Location>();
                Location loc = new Location();
                Location parentloc = new Location();
                Set<PointOfSale> pointOfSales = new HashSet<PointOfSale>();
                PointOfSale pos = new PointOfSale();
                pos.setId(1);
                pos.setName("TestPointOfSale");
                pointOfSales.add(pos);
                parentloc.setId(2);
                parentloc.setName("TestParentLoc");
                loc.setPointOfSales(pointOfSales);
                loc.setId(1);
                loc.setName("TESTLoc");
                loc.setLocation(parentloc);
                locations.add(loc);
                return locations;
            }
            
            public List<Location> findLocationsByCustomerIdOrderByName(Integer customerId) {
                List<Location> locations = new ArrayList<Location>();
                Location loc = new Location();
                Location parentloc = new Location();
                Set<PointOfSale> pointOfSales = new HashSet<PointOfSale>();
                PointOfSale pos = new PointOfSale();
                pos.setId(1);
                pos.setName("TestPointOfSale");
                pointOfSales.add(pos);
                parentloc.setId(2);
                parentloc.setName("TestParentLoc");
                loc.setPointOfSales(pointOfSales);
                loc.setId(1);
                loc.setName("TESTLoc");
                loc.setLocation(parentloc);
                locations.add(loc);
                return locations;
            }
        });
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        String result = controller.getRouteListByCustomer(request, response, model);
        assertNotNull(result);
        assertEquals(result, "/settings/routes/routeDetails");
        
        List<RouteSettingInfo> routes = (List<RouteSettingInfo>) model.get("routeList");
        assertTrue(!routes.isEmpty());
        assertNotNull(routes.get(0).getRandomizedRouteId());
    }
    
    @Test
    public void test_getRouteListByCustomer_role_failure() {
        
        createFailedAuthentication();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        CustomerAdminRoutesController controller = new CustomerAdminRoutesController();
        String result = controller.getRouteListByCustomer(request, response, model);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
        assertEquals(response.getStatus(), HttpStatus.UNAUTHORIZED.value());
    }
    
    @Test
    public void test_sortRouteList() {
        CustomerAdminRoutesController controller = new CustomerAdminRoutesController();
        controller.setRouteService(createRouteServiceForSort());
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        controller.setRouteService(new RouteServiceTestImpl() {
            public Collection<Route> findRoutesByCustomerId(int customerId) {
                List<Route> result = new ArrayList<Route>();
                Route r = new Route();
                r.setId(1);
                RouteType type = new RouteType();
                type.setId(2);
                type.setName("Maintenance");
                r.setRouteType(type);
                result.add(r);
                return result;
            }
        });
        
        String result = controller.sortRouteList(request, response, model);
        assertNotNull(result);
    }
    
    @Test
    public void test_viewRoutesDetails() {
        
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        List<Route> routes = getRoutes();
        
        List<WebObject> randomized = (List<WebObject>) keyMapping.hideProperties(routes, "id");
        request.setParameter("routeID", randomized.get(1).getPrimaryKey().toString());
        
        CustomerAdminRoutesController controller = new CustomerAdminRoutesController();
        
        controller.setRouteService(new RouteServiceTestImpl() {
            public Route findRouteById(Integer id) {
                Route r2 = new Route();
                RouteType rt2 = new RouteType();
                Set<RoutePOS> routePOSes2 = new HashSet<RoutePOS>();
                RoutePOS rp3 = new RoutePOS();
                RoutePOS rp4 = new RoutePOS();
                PointOfSale pos3 = new PointOfSale();
                PointOfSale pos4 = new PointOfSale();
                Paystation p3 = new Paystation();
                Paystation p4 = new Paystation();
                PaystationType pt3 = new PaystationType();
                PaystationType pt4 = new PaystationType();
                
                p3.setId(3);
                p4.setId(4);
                pt3.setId(3);
                pt4.setId(4);
                p3.setPaystationType(pt3);
                p4.setPaystationType(pt4);
                pos3.setPaystation(p3);
                pos4.setPaystation(p4);
                
                pos3.setPosAlertStatus(new PosAlertStatus());
                pos4.setPosAlertStatus(new PosAlertStatus());
                
                pos3.setId(3);
                pos3.setName("TestPointOfSale3");
                pos3.setLatitude(new BigDecimal(45));
                pos3.setLongitude(new BigDecimal(279));
                pos4.setId(4);
                pos4.setName("TestPointOfSale4");
                pos4.setLatitude(new BigDecimal(74));
                pos4.setLongitude(new BigDecimal(612));
                rp3.setId(3L);
                rp4.setId(4L);
                rp3.setPointOfSale(pos3);
                rp4.setPointOfSale(pos4);
                routePOSes2.add(rp3);
                routePOSes2.add(rp4);
                r2.setId(2);
                r2.setName("TestRoute2");
                rt2.setId(new Byte("2"));
                rt2.setName("Collections");
                r2.setRouteType(rt2);
                r2.setRoutePOSes(routePOSes2);
                
                return r2;
            }
            
            public Collection<Route> findRoutesByCustomerId(int customerId) {
                List<Route> result = new ArrayList<Route>();
                Route r = new Route();
                r.setId(1);
                r.setName("TestRoute");
                RouteType type = new RouteType();
                type.setId(2);
                type.setName("Maintenance");
                r.setRouteType(type);
                result.add(r);
                return result;
            }
            
            public RoutePOS findRoutePOSByIdAndPointOfSaleId(Integer routeId, Integer pointOfSaleId) {
                RoutePOS result = new RoutePOS();
                return result;
            }
            
            public List<RoutePOS> findRoutePOSesByPointOfSaleId(Integer pointOfSaleId) {
                
                List<RoutePOS> resultList = new ArrayList<RoutePOS>();
                
                RoutePOS rp1 = new RoutePOS();
                RoutePOS rp2 = new RoutePOS();
                PointOfSale pos1 = new PointOfSale();
                Route route1 = new Route();
                Route route2 = new Route();
                route1.setId(1);
                route2.setId(2);
                
                rp1.setPointOfSale(pos1);
                rp2.setPointOfSale(pos1);
                rp1.setRoute(route1);
                rp2.setRoute(route2);
                
                resultList.add(rp1);
                resultList.add(rp2);
                
                return resultList;
            }
            
        });
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        controller.setRouteTypeService(new RouteTypeServiceImpl() {
            public Collection<RouteType> findAllRouteType() {
                List<RouteType> types = new ArrayList<RouteType>();
                types.add(new RouteType(new Integer(1).byteValue(), "Collections", 3243, new Date(), 1));
                types.add(new RouteType(new Integer(2).byteValue(), "Maintenance", 5645, new Date(), 1));
                types.add(new RouteType(new Integer(2).byteValue(), "Other", 2343, new Date(), 1));
                return types;
            }
        });
        
        controller.setLocationService(new LocationServiceImpl() {
            @Override
            public List<Location> findLocationsByCustomerId(Integer customerId) {
                List<Location> locations = new ArrayList<Location>();
                Location loc = new Location();
                Location parentloc = new Location();
                Set<PointOfSale> pointOfSales = new HashSet<PointOfSale>();
                PointOfSale pos = new PointOfSale();
                pos.setId(1);
                pos.setName("TestPointOfSale");
                pointOfSales.add(pos);
                parentloc.setId(2);
                parentloc.setName("TestParentLoc");
                loc.setPointOfSales(pointOfSales);
                loc.setId(1);
                loc.setName("TESTLoc");
                loc.setLocation(parentloc);
                locations.add(loc);
                return locations;
            }
        });
        
        controller.setPointOfSaleService(createPointOfSaleService());
        
        controller.setEntityService(new EntityServiceImpl() {
            public Object merge(Object obj) {
                return obj;
            }
        });
        
        controller.setRouteSettingValidator(new RouteSettingValidator() {
            public void validate(WebSecurityForm<RouteEditForm> webSecurityForm, Errors errors) {
                webSecurityForm.removeAllErrorStatus();
                webSecurityForm.addErrorStatus(new FormErrorStatus("actionFlag", "error.common.invalid"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("routeName", "error.common.invalid.lengths"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("routeName", "error.common.invalid.special.character.name"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("PSList", "error.common.invalid"));
            }
        });
        
        WidgetMetricsHelper.registerComponents();
        String result = controller.viewRoutesDetails(request, response);
        assertNotNull(result);
    }
    
    @Test
    public void test_viewRoutesDetails_validation_failure() {
        
        request.setParameter("routeID", "!@#AGDEF");
        CustomerAdminRoutesController controller = new CustomerAdminRoutesController();
        controller.setRouteSettingValidator(new RouteSettingValidator() {
            public void validate(WebSecurityForm<RouteEditForm> webSecurityForm, Errors errors) {
                webSecurityForm.removeAllErrorStatus();
                webSecurityForm.addErrorStatus(new FormErrorStatus("actionFlag", "error.common.invalid"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("routeName", "error.common.invalid.lengths"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("routeName", "error.common.invalid.special.character.name"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("PSList", "error.common.invalid"));
            }
        });
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        String result = controller.viewRoutesDetails(request, response);
        assertNotNull(result);
        assertEquals(result, "false");
    }
    
    @Test
    public void test_viewRoutesDetails_validation_failure_actualRouteID() {
        
        request.setParameter("routeID", "1111111111111111");
        CustomerAdminRoutesController controller = new CustomerAdminRoutesController();
        controller.setRouteSettingValidator(new RouteSettingValidator() {
            public void validate(WebSecurityForm<RouteEditForm> webSecurityForm, Errors errors) {
                webSecurityForm.removeAllErrorStatus();
                webSecurityForm.addErrorStatus(new FormErrorStatus("actionFlag", "error.common.invalid"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("routeName", "error.common.invalid.lengths"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("routeName", "error.common.invalid.special.character.name"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("PSList", "error.common.invalid"));
            }
        });
        controller.setRouteService(new RouteServiceTestImpl() {
            public Route findRouteById(Integer id) {
                Route r2 = new Route();
                RouteType rt2 = new RouteType();
                Set<RoutePOS> routePOSes2 = new HashSet<RoutePOS>();
                RoutePOS rp3 = new RoutePOS();
                RoutePOS rp4 = new RoutePOS();
                PointOfSale pos3 = new PointOfSale();
                PointOfSale pos4 = new PointOfSale();
                pos3.setId(3);
                pos3.setName("TestPointOfSale3");
                pos4.setId(4);
                pos4.setName("TestPointOfSale4");
                rp3.setId(3L);
                rp4.setId(4L);
                rp3.setPointOfSale(pos3);
                rp4.setPointOfSale(pos4);
                routePOSes2.add(rp3);
                routePOSes2.add(rp4);
                r2.setId(2);
                r2.setName("TestRoute2");
                rt2.setId(new Byte("2"));
                rt2.setName("Collections");
                r2.setRouteType(rt2);
                r2.setRoutePOSes(routePOSes2);
                
                return r2;
            }
        });
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        String result = controller.viewRoutesDetails(request, response);
        assertNotNull(result);
        assertEquals(result, "false");
    }
    
    @Test
    public void test_viewRoutesDetails_role_failure() {
        
        createFailedAuthentication();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        CustomerAdminRoutesController controller = new CustomerAdminRoutesController();
        String result = controller.viewRoutesDetails(request, response);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_findRouteForm() {
        CustomerAdminRoutesController controller = new CustomerAdminRoutesController();
        controller.setRouteTypeService(new RouteTypeServiceImpl() {
            public Collection<RouteType> findAllRouteType() {
                List<RouteType> types = new ArrayList<RouteType>();
                types.add(new RouteType(new Integer(1).byteValue(), "Collections", 3243, new Date(), 1));
                types.add(new RouteType(new Integer(2).byteValue(), "Maintenance", 5645, new Date(), 1));
                types.add(new RouteType(new Integer(2).byteValue(), "Other", 2343, new Date(), 1));
                return types;
            }
        });
        
        controller.setLocationService(new LocationServiceImpl() {
            public List<Location> findLocationsByCustomerId(Integer customerId) {
                List<Location> locations = new ArrayList<Location>();
                Location loc = new Location();
                Location parentLoc = new Location();
                Set<PointOfSale> pointOfSales = new HashSet<PointOfSale>();
                PointOfSale pos = new PointOfSale();
                pos.setId(1);
                pos.setName("TestPointOfSale");
                pointOfSales.add(pos);
                parentLoc.setId(2);
                parentLoc.setName("TESTParentLoc");
                loc.setPointOfSales(pointOfSales);
                loc.setId(1);
                loc.setName("TESTLoc");
                loc.setLocation(parentLoc);
                locations.add(loc);
                return locations;
            }
            
            public List<Location> findLocationsByCustomerIdOrderByName(Integer customerId) {
                List<Location> locations = new ArrayList<Location>();
                Location loc = new Location();
                Location parentLoc = new Location();
                Set<PointOfSale> pointOfSales = new HashSet<PointOfSale>();
                PointOfSale pos = new PointOfSale();
                pos.setId(1);
                pos.setName("TestPointOfSale");
                pointOfSales.add(pos);
                parentLoc.setId(2);
                parentLoc.setName("TESTParentLoc");
                loc.setPointOfSales(pointOfSales);
                loc.setId(1);
                loc.setName("TESTLoc");
                loc.setLocation(parentLoc);
                locations.add(loc);
                return locations;
            }
        });
        
        controller.setRouteService(new RouteServiceTestImpl() {
            public Collection<Route> findRoutesByCustomerId(int customerId) {
                List<Route> result = new ArrayList<Route>();
                Route r = new Route();
                r.setId(1);
                RouteType type = new RouteType();
                type.setId(2);
                type.setName("Maintenance");
                r.setRouteType(type);
                result.add(r);
                return result;
            }
            
            public List<RoutePOS> findRoutePOSesByPointOfSaleId(Integer pointOfSaleId) {
                
                List<RoutePOS> resultList = new ArrayList<RoutePOS>();
                
                RoutePOS rp1 = new RoutePOS();
                RoutePOS rp2 = new RoutePOS();
                PointOfSale pos1 = new PointOfSale();
                Route route1 = new Route();
                Route route2 = new Route();
                route1.setId(1);
                route2.setId(2);
                
                rp1.setPointOfSale(pos1);
                rp2.setPointOfSale(pos1);
                rp1.setRoute(route1);
                rp2.setRoute(route2);
                
                resultList.add(rp1);
                resultList.add(rp2);
                
                return resultList;
            }
            
        });
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        controller.setPointOfSaleService(createPointOfSaleService());
        controller.setEntityService(new EntityServiceImpl() {
            public Object merge(Object obj) {
                return obj;
            }
        });
        
        WidgetMetricsHelper.registerComponents();
        String result = controller.getRouteForm(request, response);
        assertNotNull(result);
    }
    
    @Test
    public void test_findRouteForm_with_routeID_parameter() {
        
        request.setParameter("routeID", "1111111111111111");
        CustomerAdminRoutesController controller = new CustomerAdminRoutesController();
        controller.setRouteTypeService(new RouteTypeServiceImpl() {
            public Collection<RouteType> findAllRouteType() {
                List<RouteType> types = new ArrayList<RouteType>();
                types.add(new RouteType(new Integer(1).byteValue(), "Collections", 3243, new Date(), 1));
                types.add(new RouteType(new Integer(2).byteValue(), "Maintenance", 5645, new Date(), 1));
                types.add(new RouteType(new Integer(2).byteValue(), "Other", 2343, new Date(), 1));
                return types;
            }
        });
        
        controller.setLocationService(new LocationServiceImpl() {
            
            @Override
            public List<Location> findLocationsByCustomerId(Integer customerId) {
                List<Location> locations = new ArrayList<Location>();
                Location loc = new Location();
                Location parentloc = new Location();
                parentloc.setId(2);
                parentloc.setName("TESTParentLoc");
                loc.setId(1);
                loc.setName("TESTLoc");
                loc.setLocation(parentloc);
                locations.add(loc);
                return locations;
            }
        });
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        controller.setRouteService(new RouteServiceTestImpl() {
            public Route findRouteById(Integer id) {
                Route r2 = new Route();
                RouteType rt2 = new RouteType();
                Set<RoutePOS> routePOSes2 = new HashSet<RoutePOS>();
                RoutePOS rp3 = new RoutePOS();
                RoutePOS rp4 = new RoutePOS();
                PointOfSale pos3 = new PointOfSale();
                PointOfSale pos4 = new PointOfSale();
                Paystation p3 = new Paystation();
                Paystation p4 = new Paystation();
                PaystationType pt3 = new PaystationType();
                PaystationType pt4 = new PaystationType();
                
                p3.setId(3);
                p4.setId(4);
                pt3.setId(3);
                pt4.setId(4);
                p3.setPaystationType(pt3);
                p4.setPaystationType(pt4);
                pos3.setPaystation(p3);
                pos4.setPaystation(p4);
                
                pos3.setId(3);
                pos3.setName("TestPointOfSale3");
                pos3.setLatitude(new BigDecimal(45));
                pos3.setLongitude(new BigDecimal(279));
                pos4.setId(4);
                pos4.setName("TestPointOfSale4");
                pos4.setLatitude(new BigDecimal(74));
                pos4.setLongitude(new BigDecimal(612));
                rp3.setId(3L);
                rp4.setId(4L);
                rp3.setPointOfSale(pos3);
                rp4.setPointOfSale(pos4);
                routePOSes2.add(rp3);
                routePOSes2.add(rp4);
                r2.setId(2);
                r2.setName("TestRoute2");
                rt2.setId(new Byte("2"));
                rt2.setName("Collections");
                r2.setRouteType(rt2);
                r2.setRoutePOSes(routePOSes2);
                
                return r2;
            }
            
            public Collection<Route> findRoutesByCustomerId(int customerId) {
                List<Route> result = new ArrayList<Route>();
                Route r = new Route();
                r.setId(1);
                RouteType type = new RouteType();
                type.setId(2);
                type.setName("Maintenance");
                r.setRouteType(type);
                result.add(r);
                return result;
            }
            
            public List<RoutePOS> findRoutePOSesByPointOfSaleId(Integer pointOfSaleId) {
                
                List<RoutePOS> resultList = new ArrayList<RoutePOS>();
                
                RoutePOS rp1 = new RoutePOS();
                RoutePOS rp2 = new RoutePOS();
                PointOfSale pos1 = new PointOfSale();
                Route route1 = new Route();
                Route route2 = new Route();
                route1.setId(1);
                route2.setId(2);
                
                rp1.setPointOfSale(pos1);
                rp2.setPointOfSale(pos1);
                rp1.setRoute(route1);
                rp2.setRoute(route2);
                
                resultList.add(rp1);
                resultList.add(rp2);
                
                return resultList;
            }
            
        });
        
        controller.setPointOfSaleService(createPointOfSaleService());
        controller.setEntityService(new EntityServiceImpl() {
            public Object merge(Object obj) {
                return obj;
            }
        });
        
        String result = controller.getRouteForm(request, response);
        assertNotNull(result);
    }
    
    @Test
    public void test_findRouteForm_role_failure() {
        
        createFailedAuthentication();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        CustomerAdminRoutesController controller = new CustomerAdminRoutesController();
        String result = controller.getRouteForm(request, response);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
        assertEquals(response.getStatus(), HttpStatus.UNAUTHORIZED.value());
    }
    
    @Test
    public void test_saveRoute_validation_error() {
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        CustomerAdminRoutesController controller = new CustomerAdminRoutesController();
        WebSecurityForm<RouteEditForm> webSecurityForm = new WebSecurityForm<RouteEditForm>();
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "routeEditForm");
        controller.setRouteSettingValidator(new RouteSettingValidator() {
            public void validate(WebSecurityForm<RouteEditForm> webSecurityForm, Errors errors) {
                webSecurityForm.removeAllErrorStatus();
                webSecurityForm.addErrorStatus(new FormErrorStatus("actionFlag", "error.common.invalid"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("routeName", "error.common.invalid.lengths"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("routeName", "error.common.invalid.special.character.name"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("PSList", "error.common.invalid"));
            }
        });
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        WidgetMetricsHelper.registerComponents();
        String res = controller.saveRoute(webSecurityForm, result, response);
        assertNotNull(res);
    }
    
    @Test
    public void test_saveRoute() {
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        CustomerAdminRoutesController controller = new CustomerAdminRoutesController();
        
        RouteEditForm form = new RouteEditForm();
        form.setRouteId(1);
        form.setRouteTypeId(2);
        List<String> pointOfSaleIds = new ArrayList<String>();
        pointOfSaleIds.add("1");
        pointOfSaleIds.add("2");
        pointOfSaleIds.add("3");
        form.setRandomizedPointOfSaleIds(pointOfSaleIds);
        WebSecurityForm<RouteEditForm> webSecurityForm = new WebSecurityForm<RouteEditForm>(null, form);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "routeEditForm");
        controller.setRouteSettingValidator(new RouteSettingValidator() {
            public void validate(WebSecurityForm<RouteEditForm> webSecurityForm, Errors errors) {
                RouteEditForm routeEditForm = (RouteEditForm) (webSecurityForm.getWrappedObject());
                
                List<Integer> pointOfSaleIds = new ArrayList<Integer>();
                pointOfSaleIds.add(1);
                pointOfSaleIds.add(2);
                pointOfSaleIds.add(3);
                
                routeEditForm.setPointOfSaleIds(pointOfSaleIds);
            }
        });
        
        controller.setRouteService(new RouteServiceTestImpl() {
            public void saveRouteAndRoutePOS(Route route) {
                
            }
            
            public Route findRouteByIdAndEvict(Integer id) {
                Route route = new Route();
                route.setId(1);
                route.setName("TestRoute");
                
                return route;
            }
        });
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        String res = controller.saveRoute(webSecurityForm, result, response);
        assertNotNull(res);
        assertTrue(res.contains("true"));
    }
    
    @Test
    public void test_saveRoute_update() {
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        CustomerAdminRoutesController controller = new CustomerAdminRoutesController();
        
        RouteEditForm form = new RouteEditForm();
        form.setRouteId(1);
        form.setRouteTypeId(2);
        List<String> pointOfSaleIds = new ArrayList<String>();
        pointOfSaleIds.add("1");
        pointOfSaleIds.add("2");
        pointOfSaleIds.add("3");
        form.setRandomizedPointOfSaleIds(pointOfSaleIds);
        WebSecurityForm<RouteEditForm> webSecurityForm = new WebSecurityForm<RouteEditForm>(null, form);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_UPDATE);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "routeEditForm");
        controller.setRouteSettingValidator(new RouteSettingValidator() {
            public void validate(WebSecurityForm<RouteEditForm> webSecurityForm, Errors errors) {
                RouteEditForm routeEditForm = (RouteEditForm) (webSecurityForm.getWrappedObject());
                
                List<Integer> pointOfSaleIds = new ArrayList<Integer>();
                pointOfSaleIds.add(1);
                pointOfSaleIds.add(2);
                pointOfSaleIds.add(3);
                
                routeEditForm.setPointOfSaleIds(pointOfSaleIds);
            }
        });
        
        controller.setRouteService(new RouteServiceTestImpl() {
            public void updateRouteAndRoutePOS(Route route) {
                
            }
            
            public Route findRouteByIdAndEvict(Integer id) {
                Route route = new Route();
                route.setId(1);
                route.setName("TestRoute");
                
                return route;
            }
        });
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        String res = controller.saveRoute(webSecurityForm, result, response);
        assertNotNull(res);
        assertTrue(res.contains("true"));
    }
    
    @Test
    public void test_saveRoute_role_failure() {
        
        createFailedAuthentication();
        
        RouteEditForm form = new RouteEditForm();
        form.setRouteId(1);
        form.setRouteTypeId(2);
        List<String> pointOfSaleIds = new ArrayList<String>();
        pointOfSaleIds.add("1");
        pointOfSaleIds.add("2");
        pointOfSaleIds.add("3");
        form.setRandomizedPointOfSaleIds(pointOfSaleIds);
        WebSecurityForm<RouteEditForm> webSecurityForm = new WebSecurityForm<RouteEditForm>(null, form);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_UPDATE);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "routeEditForm");
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        CustomerAdminRoutesController controller = new CustomerAdminRoutesController();
        WidgetMetricsHelper.registerComponents();
        String res = controller.saveRoute(webSecurityForm, result, response);
        assertEquals(res, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_deleteRoute() {
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        List<Route> routes = getRoutes();
        
        List<WebObject> randomized = (List<WebObject>) keyMapping.hideProperties(routes, "id");
        request.setParameter("routeID", randomized.get(1).getPrimaryKey().toString());
        
        CustomerAdminRoutesController controller = new CustomerAdminRoutesController();
        controller.setRouteService(new RouteServiceTestImpl() {
            public void updateRoute(Route route) {
                
            }
            
            public Route findRouteByIdAndEvict(Integer id) {
                Route route = new Route();
                route.setId(1);
                route.setName("TestRoute");
                
                return route;
            }
            
            public void deleteRoute(Integer routeId) {
            }
        });
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        controller.setRouteSettingValidator(new RouteSettingValidator() {
            public void validate(WebSecurityForm<RouteEditForm> webSecurityForm, Errors errors) {
                webSecurityForm.removeAllErrorStatus();
                webSecurityForm.addErrorStatus(new FormErrorStatus("actionFlag", "error.common.invalid"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("routeName", "error.common.invalid.lengths"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("routeName", "error.common.invalid.special.character.name"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("PSList", "error.common.invalid"));
            }
        });
        
        String res = controller.deleteRoute(request, response);
        assertNotNull(res);
        assertEquals(res, "true");
    }
    
    @Test
    public void test_deleteRoute_routeID_validate_fail() {
        
        request.setParameter("routeID", "11111111111!1111");
        
        CustomerAdminRoutesController controller = new CustomerAdminRoutesController();
        controller.setRouteService(new RouteServiceTestImpl() {
            public void updateRoute(Route route) {
                
            }
            
            public Route findRouteByIdAndEvict(Integer id) {
                Route route = new Route();
                route.setId(1);
                route.setName("TestRoute");
                
                return route;
            }
        });
        controller.setRouteSettingValidator(new RouteSettingValidator() {
            public void validate(WebSecurityForm<RouteEditForm> webSecurityForm, Errors errors) {
                webSecurityForm.removeAllErrorStatus();
                webSecurityForm.addErrorStatus(new FormErrorStatus("actionFlag", "error.common.invalid"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("routeName", "error.common.invalid.lengths"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("routeName", "error.common.invalid.special.character.name"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("PSList", "error.common.invalid"));
            }
        });
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        String res = controller.deleteRoute(request, response);
        assertNotNull(res);
        assertEquals(res, "false");
    }
    
    @Test
    public void test_deleteRoute_actual_routeID_validate_fail() {
        request.setParameter("routeID", "1111111111111111");
        
        CustomerAdminRoutesController controller = new CustomerAdminRoutesController();
        controller.setRouteService(new RouteServiceTestImpl() {
            public void updateRoute(Route route) {
                
            }
            
            public Route findRouteByIdAndEvict(Integer id) {
                Route route = new Route();
                route.setId(1);
                route.setName("TestRoute");
                
                return route;
            }
        });
        
        controller.setRouteSettingValidator(new RouteSettingValidator() {
            public void validate(WebSecurityForm<RouteEditForm> webSecurityForm, Errors errors) {
                webSecurityForm.removeAllErrorStatus();
                webSecurityForm.addErrorStatus(new FormErrorStatus("actionFlag", "error.common.invalid"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("routeName", "error.common.invalid.lengths"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("routeName", "error.common.invalid.special.character.name"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("PSList", "error.common.invalid"));
            }
        });
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        String res = controller.deleteRoute(request, response);
        assertNotNull(res);
        assertEquals(res, "false");
    }
    
    @Test
    public void test_deleteRoute_role_fail() {
        
        createFailedAuthentication();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        CustomerAdminRoutesController controller = new CustomerAdminRoutesController();
        String result = controller.deleteRoute(request, response);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    private List<Route> getRoutes() {
        Route r = new Route();
        RouteType rt = new RouteType();
        Set<RoutePOS> routePOSes = new HashSet<RoutePOS>();
        RoutePOS rp1 = new RoutePOS();
        RoutePOS rp2 = new RoutePOS();
        PointOfSale pos1 = new PointOfSale();
        PointOfSale pos2 = new PointOfSale();
        Paystation p1 = new Paystation();
        Paystation p2 = new Paystation();
        PaystationType pt1 = new PaystationType();
        PaystationType pt2 = new PaystationType();
        
        p1.setId(1);
        p2.setId(2);
        pt1.setId(1);
        pt2.setId(2);
        p1.setPaystationType(pt1);
        p2.setPaystationType(pt2);
        pos1.setPaystation(p1);
        pos2.setPaystation(p2);
        
        pos1.setId(1);
        pos1.setName("TestPointOfSale1");
        pos1.setLatitude(new BigDecimal(14));
        pos1.setLongitude(new BigDecimal(130));
        pos2.setId(2);
        pos2.setName("TestPointOfSale2");
        pos2.setLatitude(new BigDecimal(123));
        pos2.setLongitude(new BigDecimal(875));
        rp1.setId(1L);
        rp2.setId(2L);
        rp1.setPointOfSale(pos1);
        rp2.setPointOfSale(pos2);
        routePOSes.add(rp1);
        routePOSes.add(rp2);
        r.setId(1);
        r.setName("TestRoute1");
        rt.setId(new Byte("1"));
        rt.setName("Collections");
        r.setRouteType(rt);
        r.setRoutePOSes(routePOSes);
        
        Route r2 = new Route();
        RouteType rt2 = new RouteType();
        Set<RoutePOS> routePOSes2 = new HashSet<RoutePOS>();
        RoutePOS rp3 = new RoutePOS();
        RoutePOS rp4 = new RoutePOS();
        PointOfSale pos3 = new PointOfSale();
        PointOfSale pos4 = new PointOfSale();
        Paystation p3 = new Paystation();
        Paystation p4 = new Paystation();
        PaystationType pt3 = new PaystationType();
        PaystationType pt4 = new PaystationType();
        
        p3.setId(3);
        p4.setId(4);
        pt3.setId(3);
        pt4.setId(4);
        p3.setPaystationType(pt3);
        p4.setPaystationType(pt4);
        pos3.setPaystation(p3);
        pos4.setPaystation(p4);
        
        pos3.setId(3);
        pos3.setName("TestPointOfSale3");
        pos3.setLatitude(new BigDecimal(45));
        pos3.setLongitude(new BigDecimal(279));
        pos4.setId(4);
        pos4.setName("TestPointOfSale4");
        pos4.setLatitude(new BigDecimal(74));
        pos4.setLongitude(new BigDecimal(612));
        rp3.setId(3L);
        rp4.setId(4L);
        rp3.setPointOfSale(pos3);
        rp4.setPointOfSale(pos4);
        routePOSes2.add(rp3);
        routePOSes2.add(rp4);
        r2.setId(2);
        r2.setName("TestRoute2");
        rt2.setId(new Byte("2"));
        rt2.setName("Collections");
        r2.setRouteType(rt2);
        r2.setRoutePOSes(routePOSes2);
        
        List<Route> routes = new ArrayList<Route>();
        routes.add(r);
        routes.add(r2);
        
        return routes;
    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        user.setIsMigrated(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_ROUTES);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_ROUTES);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private void createFailedAuthentication() {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        user.setIsMigrated(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(2);
        permissionList.add(3);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private RouteService createRouteServiceForSort() {
        return new RouteServiceTestImpl() {
            public Collection<Route> findRoutesByCustomerIdOrderByNameAsc(Integer customerId, Integer filterType) {
                Collection<Route> result = new ArrayList<Route>();
                RouteType routeType = new RouteType();
                routeType.setId((byte) 1);
                routeType.setName("aRoute1");
                Customer customer = new Customer();
                customer.setId(2);
                customer.setName("momomomo");
                
                RouteType routeType1 = new RouteType();
                routeType1.setId((byte) 2);
                routeType1.setName("bRoute1");
                
                Route r = new Route(routeType, customer, "aRoute1", new Date(), 1);
                r.setId(1);
                
                Route r1 = new Route(routeType, customer, "bRoute1", new Date(), 1);
                r1.setId(2);
                
                result.add(r);
                result.add(r1);
                return result;
            }
            
            public Collection<Route> findRoutesByCustomerIdOrderByNameDesc(Integer customerId, Integer filterType) {
                Collection<Route> result = new ArrayList<Route>();
                RouteType routeType = new RouteType();
                routeType.setId((byte) 1);
                routeType.setName("aRoute1");
                Customer customer = new Customer();
                customer.setId(2);
                customer.setName("momomomo");
                
                RouteType routeType1 = new RouteType();
                routeType1.setId((byte) 2);
                routeType1.setName("bRoute1");
                
                Route r1 = new Route(routeType, customer, "bRoute1", new Date(), 1);
                r1.setId(2);
                
                Route r = new Route(routeType, customer, "aRoute1", new Date(), 1);
                r.setId(1);
                
                result.add(r);
                result.add(r1);
                return result;
            }
            
            public Collection<Route> findRoutesByCustomerIdOrderByRouteTypeAsc(Integer customerId, Integer filterType) {
                Collection<Route> result = new ArrayList<Route>();
                RouteType routeType = new RouteType();
                routeType.setId((byte) 1);
                routeType.setName("aRoute1");
                Customer customer = new Customer();
                customer.setId(2);
                customer.setName("momomomo");
                
                RouteType routeType1 = new RouteType();
                routeType1.setId((byte) 2);
                routeType1.setName("bRoute1");
                
                Route r = new Route(routeType, customer, "aRoute1", new Date(), 1);
                r.setId(1);
                
                Route r1 = new Route(routeType, customer, "bRoute1", new Date(), 1);
                r1.setId(2);
                
                result.add(r);
                result.add(r1);
                return result;
            }
            
            public Collection<Route> findRoutesByCustomerIdOrderByRouteTypeDesc(Integer customerId, Integer filterType) {
                Collection<Route> result = new ArrayList<Route>();
                RouteType routeType = new RouteType();
                routeType.setId((byte) 1);
                routeType.setName("aRoute1");
                Customer customer = new Customer();
                customer.setId(2);
                customer.setName("momomomo");
                
                RouteType routeType1 = new RouteType();
                routeType1.setId((byte) 2);
                routeType1.setName("bRoute1");
                
                Route r1 = new Route(routeType, customer, "bRoute1", new Date(), 1);
                r1.setId(2);
                
                Route r = new Route(routeType, customer, "aRoute1", new Date(), 1);
                r.setId(1);
                
                result.add(r);
                result.add(r1);
                return result;
            }
        };
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
        };
    }
    
    private PointOfSaleService createPointOfSaleService() {
        return new PointOfSaleServiceTestImpl() {
            @Override
            public List<PointOfSale> findPointOfSalesByCustomerId(int customerId) {
                List<PointOfSale> result = new ArrayList<PointOfSale>();
                PointOfSale pos = new PointOfSale();
                Location loc = new Location();
                loc.setId(1);
                loc.setName("TestLoc");
                pos.setId(1);
                pos.setName("TestPos");
                pos.setLocation(loc);
                result.add(pos);
                
                return result;
            }
            
            @Override
            public List<PointOfSale> findValidPointOfSaleByCustomerIdOrderByName(int customerId) {
                List<PointOfSale> result = new ArrayList<PointOfSale>();
                PointOfSale pos = new PointOfSale();
                Location loc = new Location();
                loc.setId(1);
                loc.setName("TestLoc");
                pos.setId(1);
                pos.setName("TestPos");
                pos.setLocation(loc);
                result.add(pos);
                
                return result;
            }
            
            @Override
            public int countUnPlacedByRouteIds(Collection<Integer> routeIds) {
                return 0;
            }
            
            @Override
            public List<PaystationListInfo> findPaystation(PointOfSaleSearchCriteria criteria) {
                ArrayList<PaystationListInfo> result = new ArrayList<PaystationListInfo>();
                
                return result;
            }
            
            @Override
            public PlacementMapBordersInfo findMapBordersByCustomerId(Integer customerId) {
                return null;
            }
            
        };
    }
}
