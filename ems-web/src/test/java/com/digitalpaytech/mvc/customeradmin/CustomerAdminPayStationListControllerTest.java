package com.digitalpaytech.mvc.customeradmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PaystationType;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosAlertStatus;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.domain.PosStatus;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.dto.PointOfSaleSearchCriteria;
import com.digitalpaytech.dto.PosAlertStatusDetailSummary;
import com.digitalpaytech.dto.customeradmin.LocationRouteFilterInfo;
import com.digitalpaytech.dto.customeradmin.PaystationListInfo;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.mvc.customeradmin.support.CustomerAdminPayStationEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.mvc.support.impl.CommonControllerHelperImpl;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.LocationServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.CustomerAdminServiceTestImpl;
import com.digitalpaytech.testing.services.impl.PointOfSaleServiceTestImpl;
import com.digitalpaytech.testing.services.impl.PosAlertStatusServiceTestImpl;
import com.digitalpaytech.testing.services.impl.RouteServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.customeradmin.CustomerAdminPayStationValidator;

public class CustomerAdminPayStationListControllerTest {
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private ModelMap model;
    private UserAccount userAccount;
    private CustomerAdminPayStationListController controller;
    
    @Before
    public void setUp() {
        TimeZone.setDefault(TimeZone.getTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT));
        
        this.controller = new CustomerAdminPayStationListController();
        
        WidgetMetricsHelper.registerComponents();
        
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        session = new MockHttpSession();
        this.request.setSession(session);
        this.model = new ModelMap();
        
        userAccount = new UserAccount();
        userAccount.setId(2);
        Set<UserRole> roles = new HashSet<UserRole>();
        UserRole role = new UserRole();
        Role r = new Role();
        Set<RolePermission> rps = new HashSet<RolePermission>();
        
        RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_VIEW_ROUTES);
        rp1.setPermission(permission1);
        
        RolePermission rp2 = new RolePermission();
        rp2.setId(2);
        Permission permission2 = new Permission();
        permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_ROUTES);
        rp2.setPermission(permission2);
        
        RolePermission rp3 = new RolePermission();
        rp3.setId(3);
        Permission permission3 = new Permission();
        permission3.setId(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS);
        rp3.setPermission(permission3);
        
        RolePermission rp4 = new RolePermission();
        rp4.setId(4);
        Permission permission4 = new Permission();
        permission4.setId(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS);
        rp4.setPermission(permission4);
        
        RolePermission rp5 = new RolePermission();
        rp5.setId(5);
        Permission permission5 = new Permission();
        permission5.setId(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS);
        rp5.setPermission(permission5);
        
        RolePermission rp6 = new RolePermission();
        rp6.setId(6);
        Permission permission6 = new Permission();
        permission6.setId(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS);
        rp6.setPermission(permission6);
        
        rps.add(rp1);
        rps.add(rp2);
        rps.add(rp3);
        rps.add(rp4);
        rps.add(rp5);
        rps.add(rp6);
        
        r.setId(3);
        r.setRolePermissions(rps);
        
        role.setId(3);
        role.setRole(r);
        roles.add(role);
        userAccount.setUserRoles(roles);
        
        Customer customer = new Customer();
        customer.setId(2);
        userAccount.setCustomer(customer);
        createAuthentication(userAccount);
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        controller.setCustomerAdminService(new CustomerAdminServiceTestImpl() {
            public void saveOrUpdatePointOfSale(PointOfSale pointOfSale) {
            }
            
            public void saveOrUpdatePaystation(Paystation payStation) {
            }
            
            public void saveOrUpdatePointOfSaleStatus(PosStatus pointOfSaleStatus) {
            };
        });
    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setCustomerTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT);
        user.setIsComplexPassword(true);
        user.setIsMigrated(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_ROUTES);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_ROUTES);
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS);
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS);
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    //========================================================================================================================
    // "Pay Station List" tab
    //========================================================================================================================
    
    @Test
    public void test_payStationList() {
        
        controller.setLocationService(new LocationServiceImpl() {
            @Override
            public List<Location> findLocationsByCustomerId(Integer customerId) {
                return null;
            }
        });
        controller.setRouteService(new RouteServiceTestImpl() {
            @Override
            public Collection<Route> findRoutesByCustomerId(int customerId) {
                return null;
            }
        });
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            @Override
            public List<PointOfSale> findPointOfSalesByCustomerId(int customerId) {
                return new ArrayList<PointOfSale>();
            }
            
            @Override
            public List<PointOfSale> findPointOfSaleAndPosStatusByCustomerId(int customerId) {
                return new ArrayList<PointOfSale>();
            }
            
            @Override
            public List<PointOfSale> findPointOfSaleAndPosStatusByCustomerIdOrderByName(int customerId) {
                return new ArrayList<PointOfSale>();
            }
            
            @Override
            public PlacementMapBordersInfo findMapBordersByCustomerId(Integer customerId) {
                return null;
            }
            
            @Override
            public List<PaystationListInfo> findPaystation(PointOfSaleSearchCriteria criteria) {
                return new ArrayList<PaystationListInfo>();
            }
        });
        
        controller.setCommonControllerHelper(new CommonControllerHelperImpl() {
            public LocationRouteFilterInfo createLocationRouteFilter(Integer customerId, RandomKeyMapping keyMapping) {
                LocationRouteFilterInfo locationRouteFilter = new LocationRouteFilterInfo();
                return locationRouteFilter;
            }
        });
        
        String returnValue = controller.payStationList(request, response, model);
        assertEquals("/settings/locations/payStationList", returnValue);
    }
    
    @Test
    public void test_payStationDetails() {
        
        PointOfSale pointOfSale = new PointOfSale();
        pointOfSale.setId(1);
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String randomKey = keyMapping.getRandomString(pointOfSale, "id");
        request.setParameter("payStationID", randomKey);
        
        controller.setPosAlertStatusService(new PosAlertStatusServiceTestImpl() {
            
            @Override
            public Map<Integer, PosAlertStatusDetailSummary> findPosAlertDetailSummaryByPointOfSaleId(int pointOfSaleId) {
                
                Map<Integer, PosAlertStatusDetailSummary> pasdsMap = new HashMap<Integer, PosAlertStatusDetailSummary>();
                PosAlertStatusDetailSummary pads = new PosAlertStatusDetailSummary();
                pads.setTotalCritical(1);
                pads.setTotalMajor(1);
                pads.setTotalMinor(1);
                pasdsMap.put(1, pads);
                return pasdsMap;
            }
            
        });
        
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            @Override
            public PointOfSale findPointOfSaleById(Integer pointOfSaleId) {
                PointOfSale pointOfSale = new PointOfSale();
                pointOfSale.setId(1);
                PosAlertStatus posAlertStatus = new PosAlertStatus();
                pointOfSale.setPosAlertStatus(posAlertStatus);
                return pointOfSale;
            }
            
            @Override
            public Paystation findPayStationByPointOfSaleId(int pointOfSaleId) {
                Paystation payStation = new Paystation();
                PaystationType payStationType = new PaystationType();
                payStationType.setId(1);
                payStation.setPaystationType(payStationType);
                return payStation;
            }
            
            @Override
            public PosStatus findPointOfSaleStatusByPOSId(int pointOfSaleId, boolean useCache) {
                PosStatus status = new PosStatus();
                status.setIsActivated(true);
                status.setIsVisible(true);
                return status;
            }
            
            @Override
            public PosHeartbeat findPosHeartbeatByPointOfSaleId(int pointOfSaleId) {
                PosHeartbeat heartbeat = new PosHeartbeat();
                heartbeat.setLastHeartbeatGmt(new Date());
                return heartbeat;
            }
        });
        controller.setLocationService(new LocationServiceImpl() {
            public Location findLocationByPointOfSaleId(int pointOfSaleId) {
                Location location = new Location();
                Location parentLocation = new Location();
                parentLocation.setName("testParent");
                location.setLocation(parentLocation);
                location.setName("testName");
                location.setId(1);
                return location;
            }
        });
        
        String returnValue = controller.payStationDetailPage(request, response, model);
        assertNotNull(returnValue);
    }
    
    @Test
    public void test_savePayStation() {
        
        CustomerAdminPayStationEditForm form = createValidPayStationTestForm();
        
        BindingResult result = new BeanPropertyBindingResult(form, "payStationEditForm");
        WebSecurityForm<CustomerAdminPayStationEditForm> webSecurityForm = new WebSecurityForm<CustomerAdminPayStationEditForm>(null, form);
        webSecurityForm.setPostToken(webSecurityForm.getInitToken());
        model.put("payStationEditForm", webSecurityForm);
        
        CustomerAdminPayStationValidator payStationValidator = new CustomerAdminPayStationValidator();
        //        payStationValidator.setLocationService(new LocationServiceImpl() {
        //            public Location findLocationById(int locationId) {
        //                Location location = new Location();
        //                location.setId(1);
        //                return location;
        //            }
        //        });
        //        payStationValidator.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
        //            public Paystation findPayStationById(int payStationId) {
        //                Paystation payStation = new Paystation();
        //                payStation.setId(1);
        //                return payStation;
        //            }
        //            public PointOfSale findPointOfSaleById(Integer id) {
        //                PointOfSale pointOfSale = new PointOfSale();
        //                pointOfSale.setId(1);
        //                return pointOfSale;
        //            }
        //
        //        });
        controller.setCustomerAdminPayStationValidator(payStationValidator);
        controller.setLocationService(new LocationServiceImpl() {
            public Location findLocationById(int locationId) {
                return new Location();
            }
        });
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            public PosStatus findPointOfSaleStatusByPOSId(int pointOfSaleId, boolean useCache) {
                PosStatus status = new PosStatus();
                status.setIsActivated(true);
                status.setIsVisible(true);
                return status;
            }
            
            public PointOfSale findPointOfSaleById(Integer id) {
                PointOfSale pointOfSale = new PointOfSale();
                pointOfSale.setId(1);
                return pointOfSale;
            }
            
            public Paystation findPayStationById(int id) {
                Paystation payStation = new Paystation();
                payStation.setId(1);
                return payStation;
            }
            
            public PointOfSale findPointOfSaleByPaystationId(int id) {
                PointOfSale pointOfSale = new PointOfSale();
                pointOfSale.setId(1);
                return pointOfSale;
            }
        });
        
        @SuppressWarnings("unchecked")
        String returnValue = controller.savePayStation((WebSecurityForm<CustomerAdminPayStationEditForm>) model.get("payStationEditForm"), result,
                                                       request, response, model);
        
        assertTrue(returnValue.startsWith(WebCoreConstants.RESPONSE_TRUE));
    }
    
    private CustomerAdminPayStationEditForm createValidPayStationTestForm() {
        
        CustomerAdminPayStationEditForm form = new CustomerAdminPayStationEditForm();
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        Location location = new Location();
        location.setId(1);
        String locationRandomKey = keyMapping.getRandomString(location, "id");
        
        PointOfSale payStation = new PointOfSale();
        payStation.setId(1);
        String payStationRandomKey = keyMapping.getRandomString(payStation, "id");
        
        form.setName("Location name");
        form.setRandomId(payStationRandomKey);
        form.setLocationRandomId(locationRandomKey);
        form.setActive(true);
        form.setHidden(false);
        
        return form;
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
        };
    }
}
