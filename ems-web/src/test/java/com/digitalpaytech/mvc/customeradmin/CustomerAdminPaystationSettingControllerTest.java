package com.digitalpaytech.mvc.customeradmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PaystationType;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosServiceState;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.SettingsFile;
import com.digitalpaytech.domain.SettingsFileContent;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.dto.customeradmin.CustomerAdminPayStationSettingInfo;
import com.digitalpaytech.dto.customeradmin.CustomerAdminPayStationSettingPayStationInfo;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.PaystationSettingServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.PointOfSaleServiceTestImpl;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

public class CustomerAdminPaystationSettingControllerTest {
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private ModelMap model;
    private UserAccount userAccount;
    private WebUser webUser;
    private CustomerAdminPaystationSettingController controller;
    private BindingResult result;
    
    private List<SettingsFile> settingsFileList;
    
    private List<PointOfSale> greenList;
    private List<PointOfSale> yellowList;
    private List<PointOfSale> redList;
    
    @Before
    public void setUp() {
        
        userAccount = new UserAccount();
        controller = new CustomerAdminPaystationSettingController();
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        session = new MockHttpSession();
        request.setSession(session);
        model = new ModelMap();
        
        Set<UserRole> roles = new HashSet<UserRole>();
        UserRole role = new UserRole();
        Role r = new Role();
        Set<RolePermission> rps = new HashSet<RolePermission>();
        
        RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_VIEW_PAYSTATION_SETTINGS);
        rp1.setPermission(permission1);
        
        rps.add(rp1);
        
        r.setId(3);
        r.setRolePermissions(rps);
        
        role.setId(3);
        role.setRole(r);
        roles.add(role);
        userAccount.setUserRoles(roles);
        
        Customer customer = new Customer();
        customer.setId(2);
        userAccount.setCustomer(customer);
        createAuthentication(userAccount);
        userAccount.setId(2);
        
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        webUser = new WebUser(2, 3, "testUser", "password", "salt", true, authorities);
        Authentication authentication = new UsernamePasswordAuthenticationToken(webUser, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_PAYSTATION_SETTINGS);
        webUser.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        
        webUser.setCustomerTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT);
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        WidgetMetricsHelper.registerComponents();
        
        createLists();
        
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            public List<PointOfSale> findCurrentPointOfSalesBySettingsFileIdForPayStationSettingsPage(Integer fileSettingId) {
                return returnGreenList(fileSettingId);
            }
            
            public List<PointOfSale> findFuturePointOfSalesBySettingsFileIdForPayStationSettingsPage(Integer fileSettingId) {
                return returnYellowList(fileSettingId);
            }
            
            public List<PointOfSale> findUnAssignedPointOfSalesForPayStationSettingsPage(Integer customerId) {
                return returnRedList();
            }
            
            public PlacementMapBordersInfo findMapBordersByCustomerId(Integer customerId) {
                PlacementMapBordersInfo mapBorders = new PlacementMapBordersInfo();
                mapBorders.setMaxLatitude(new BigDecimal(10));
                mapBorders.setMinLatitude(new BigDecimal(-10));
                mapBorders.setMaxLongitude(new BigDecimal(10));
                mapBorders.setMinLongitude(new BigDecimal(-10));
                return mapBorders;
            }
        });
        
        controller.setPaystationSettingService(new PaystationSettingServiceImpl() {
            public List<SettingsFile> findSettingsFileByCustomerId(Integer customerId) {
                return settingsFileList;
            }
            
            public SettingsFileContent findSettingsFileContentBySettingsFileId(Integer settingsFileId) {
                if (settingsFileId.intValue() == 1)
                    return null;
                else
                    return new SettingsFileContent();
            }
            
            public SettingsFile findSettingsFile(Integer settingsFileId) {
                for (SettingsFile settingsFile : settingsFileList) {
                    if (settingsFile.getId().intValue() == settingsFileId.intValue()) {
                        return settingsFile;
                    }
                }
                return null;
            }
            
            public void deleteSettingsFileContent(SettingsFile settingsFile) {
                return;
            }
            
            public List<CustomerAdminPayStationSettingInfo> findCustomerAdminPayStationSettingInfoListByCustomerId(Integer customerId,
                String timezone, RandomKeyMapping keyMapping) {
                
                SettingsFile greenOnly = settingsFileList.get(0);
                SettingsFile withYellow = settingsFileList.get(1);
                
                List<CustomerAdminPayStationSettingInfo> list = new ArrayList<CustomerAdminPayStationSettingInfo>();
                
                CustomerAdminPayStationSettingInfo green = new CustomerAdminPayStationSettingInfo();
                green.setRandomId(keyMapping.getRandomString(greenOnly, WebCoreConstants.ID_LOOK_UP_NAME));
                green.setName(greenOnly.getName());
                green.setFileDeleted(true);
                green.setIsNotExist(false);
                green.setIsUpdateInProgress(false);
                green.setPayStationCount(2);
                green.setUploadDate(DateUtil.getRelativeTimeString(greenOnly.getUploadGmt(), timezone));
                
                list.add(green);
                
                CustomerAdminPayStationSettingInfo yellow = new CustomerAdminPayStationSettingInfo();
                yellow.setRandomId(keyMapping.getRandomString(withYellow, WebCoreConstants.ID_LOOK_UP_NAME));
                yellow.setName(withYellow.getName());
                yellow.setFileDeleted(false);
                yellow.setIsNotExist(false);
                yellow.setIsUpdateInProgress(true);
                yellow.setPayStationCount(3);
                yellow.setUploadDate(DateUtil.getRelativeTimeString(withYellow.getUploadGmt(), timezone));
                
                list.add(yellow);
                
                CustomerAdminPayStationSettingInfo red = new CustomerAdminPayStationSettingInfo();
                yellow.setRandomId(WebCoreConstants.NOT_MAINTAINED_PAYSTATION_SETTING);
                red.setName(WebCoreConstants.NOT_MAINTAINED_PAYSTATION_SETTING);
                red.setFileDeleted(true);
                red.setIsNotExist(true);
                red.setIsUpdateInProgress(false);
                red.setPayStationCount(2);
                red.setUploadDate(DateUtil.getRelativeTimeString(withYellow.getUploadGmt(), timezone));
                
                list.add(red);
                
                return list;
            }
            
            public CustomerAdminPayStationSettingInfo findCustomerAdminPayStationSettingInfoBySettingFileId(Integer settingFileId, String timezone,
                RandomKeyMapping keyMapping) {
                SettingsFile greenOnly = settingsFileList.get(0);
                SettingsFile withYellow = settingsFileList.get(1);
                if (settingFileId.intValue() == 1) {
                    CustomerAdminPayStationSettingInfo green = new CustomerAdminPayStationSettingInfo();
                    green.setRandomId(keyMapping.getRandomString(greenOnly, WebCoreConstants.ID_LOOK_UP_NAME));
                    green.setName(greenOnly.getName());
                    green.setFileDeleted(true);
                    green.setIsNotExist(false);
                    green.setIsUpdateInProgress(false);
                    green.setPayStationCount(2);
                    green.setUploadDate(DateUtil.getRelativeTimeString(greenOnly.getUploadGmt(), timezone));
                    
                    List<CustomerAdminPayStationSettingPayStationInfo> posList = new ArrayList<CustomerAdminPayStationSettingPayStationInfo>();
                    CustomerAdminPayStationSettingPayStationInfo g1 = new CustomerAdminPayStationSettingPayStationInfo();
                    g1.setDownloadDate(DateUtil.getRelativeTimeString(greenList.get(0).getPosServiceState().getLastPaystationSettingUploadGmt(),
                                                                      timezone));
                    g1.setIsUpdateInProgress(false);
                    g1.setLatitude(greenList.get(0).getLatitude());
                    g1.setLongitude(greenList.get(0).getLongitude());
                    g1.setName(greenList.get(0).getName());
                    g1.setPayStationType(greenList.get(0).getPaystation().getPaystationType().getId());
                    g1.setRandomId(keyMapping.getRandomString(greenList.get(0), WebCoreConstants.ID_LOOK_UP_NAME));
                    g1.setSerialNumber(greenList.get(0).getSerialNumber());
                    
                    CustomerAdminPayStationSettingPayStationInfo g2 = new CustomerAdminPayStationSettingPayStationInfo();
                    g2.setDownloadDate(DateUtil.getRelativeTimeString(greenList.get(1).getPosServiceState().getLastPaystationSettingUploadGmt(),
                                                                      timezone));
                    g2.setIsUpdateInProgress(false);
                    g2.setLatitude(greenList.get(1).getLatitude());
                    g2.setLongitude(greenList.get(1).getLongitude());
                    g2.setName(greenList.get(1).getName());
                    g2.setPayStationType(greenList.get(1).getPaystation().getPaystationType().getId());
                    g2.setRandomId(keyMapping.getRandomString(greenList.get(1), WebCoreConstants.ID_LOOK_UP_NAME));
                    g2.setSerialNumber(greenList.get(1).getSerialNumber());
                    
                    posList.add(g1);
                    posList.add(g2);
                    
                    green.setPayStationList(posList);
                    
                    return green;
                } else {
                    
                    CustomerAdminPayStationSettingInfo yellow = new CustomerAdminPayStationSettingInfo();
                    yellow.setRandomId(keyMapping.getRandomString(withYellow, WebCoreConstants.ID_LOOK_UP_NAME));
                    yellow.setName(withYellow.getName());
                    yellow.setFileDeleted(false);
                    yellow.setIsNotExist(false);
                    yellow.setIsUpdateInProgress(true);
                    yellow.setPayStationCount(3);
                    yellow.setUploadDate(DateUtil.getRelativeTimeString(withYellow.getUploadGmt(), timezone));
                    
                    List<CustomerAdminPayStationSettingPayStationInfo> posList = new ArrayList<CustomerAdminPayStationSettingPayStationInfo>();
                    CustomerAdminPayStationSettingPayStationInfo g3 = new CustomerAdminPayStationSettingPayStationInfo();
                    g3.setDownloadDate(DateUtil.getRelativeTimeString(greenList.get(2).getPosServiceState().getLastPaystationSettingUploadGmt(),
                                                                      timezone));
                    g3.setIsUpdateInProgress(false);
                    g3.setLatitude(greenList.get(2).getLatitude());
                    g3.setLongitude(greenList.get(2).getLongitude());
                    g3.setName(greenList.get(2).getName());
                    g3.setPayStationType(greenList.get(2).getPaystation().getPaystationType().getId());
                    g3.setRandomId(keyMapping.getRandomString(greenList.get(2), WebCoreConstants.ID_LOOK_UP_NAME));
                    g3.setSerialNumber(greenList.get(2).getSerialNumber());
                    
                    CustomerAdminPayStationSettingPayStationInfo y1 = new CustomerAdminPayStationSettingPayStationInfo();
                    y1.setDownloadDate(DateUtil.getRelativeTimeString(yellowList.get(0).getPosServiceState().getLastPaystationSettingUploadGmt(),
                                                                      timezone));
                    y1.setIsUpdateInProgress(true);
                    y1.setLatitude(yellowList.get(0).getLatitude());
                    y1.setLongitude(yellowList.get(0).getLongitude());
                    y1.setName(yellowList.get(0).getName());
                    y1.setPayStationType(yellowList.get(0).getPaystation().getPaystationType().getId());
                    y1.setRandomId(keyMapping.getRandomString(yellowList.get(0), WebCoreConstants.ID_LOOK_UP_NAME));
                    y1.setSerialNumber(yellowList.get(0).getSerialNumber());
                    
                    CustomerAdminPayStationSettingPayStationInfo y2 = new CustomerAdminPayStationSettingPayStationInfo();
                    y2.setDownloadDate(DateUtil.getRelativeTimeString(yellowList.get(1).getPosServiceState().getLastPaystationSettingUploadGmt(),
                                                                      timezone));
                    y2.setIsUpdateInProgress(true);
                    y2.setLatitude(yellowList.get(1).getLatitude());
                    y2.setLongitude(yellowList.get(1).getLongitude());
                    y2.setName(yellowList.get(1).getName());
                    y2.setPayStationType(yellowList.get(1).getPaystation().getPaystationType().getId());
                    y2.setRandomId(keyMapping.getRandomString(yellowList.get(1), WebCoreConstants.ID_LOOK_UP_NAME));
                    y2.setSerialNumber(yellowList.get(1).getSerialNumber());
                    
                    posList.add(g3);
                    posList.add(y1);
                    posList.add(y2);
                    
                    yellow.setPayStationList(posList);
                    
                    return yellow;
                }
                
            }
            
            public CustomerAdminPayStationSettingInfo findNoSettingCustomerAdminPayStationSettingInfoByCustomerId(Integer customerId,
                String timezone, RandomKeyMapping keyMapping) {
                CustomerAdminPayStationSettingInfo red = new CustomerAdminPayStationSettingInfo();
                red.setRandomId(WebCoreConstants.NOT_MAINTAINED_PAYSTATION_SETTING);
                red.setName(WebCoreConstants.NOT_MAINTAINED_PAYSTATION_SETTING);
                red.setFileDeleted(true);
                red.setIsNotExist(true);
                red.setIsUpdateInProgress(false);
                red.setPayStationCount(2);
                red.setUploadDate(DateUtil.getRelativeTimeString(null, timezone));
                
                List<CustomerAdminPayStationSettingPayStationInfo> posList = new ArrayList<CustomerAdminPayStationSettingPayStationInfo>();
                CustomerAdminPayStationSettingPayStationInfo r1 = new CustomerAdminPayStationSettingPayStationInfo();
                r1.setDownloadDate(DateUtil.getRelativeTimeString(redList.get(0).getPosServiceState().getLastPaystationSettingUploadGmt(), timezone));
                r1.setIsUpdateInProgress(false);
                r1.setLatitude(redList.get(0).getLatitude());
                r1.setLongitude(redList.get(0).getLongitude());
                r1.setName(redList.get(0).getName());
                r1.setPayStationType(redList.get(0).getPaystation().getPaystationType().getId());
                r1.setRandomId(keyMapping.getRandomString(redList.get(0), WebCoreConstants.ID_LOOK_UP_NAME));
                r1.setSerialNumber(redList.get(0).getSerialNumber());
                
                CustomerAdminPayStationSettingPayStationInfo r2 = new CustomerAdminPayStationSettingPayStationInfo();
                r2.setDownloadDate(DateUtil.getRelativeTimeString(redList.get(1).getPosServiceState().getLastPaystationSettingUploadGmt(), timezone));
                r2.setIsUpdateInProgress(false);
                r2.setLatitude(redList.get(1).getLatitude());
                r2.setLongitude(redList.get(1).getLongitude());
                r2.setName(redList.get(1).getName());
                r2.setPayStationType(redList.get(1).getPaystation().getPaystationType().getId());
                r2.setRandomId(keyMapping.getRandomString(redList.get(1), WebCoreConstants.ID_LOOK_UP_NAME));
                r2.setSerialNumber(redList.get(1).getSerialNumber());
                
                posList.add(r1);
                posList.add(r2);
                
                red.setPayStationList(posList);
                
                return red;
            }
            
        });
    }
    
    @Test
    public void test_getPaystationSettingList() {
        
        String resp = controller.getPaystationSettingList(request, response, model);
        
        assertNotNull(resp);
        assertEquals(resp, "/settings/locations/payStationSettings");
        
        Object paystationSettingList = model.get("paystationSettingList");
        assertNotNull(paystationSettingList);
        assertEquals(paystationSettingList instanceof ArrayList, true);
        
        Object mapBorders = model.get("mapBorders");
        assertNotNull(mapBorders);
        assertEquals(mapBorders instanceof PlacementMapBordersInfo, true);
        
        List<CustomerAdminPayStationSettingInfo> list = (ArrayList<CustomerAdminPayStationSettingInfo>) paystationSettingList;
        assertEquals(list.size(), 3);
        assertEquals(list.get(0).getPayStationCount().intValue(), 2);
        assertEquals(list.get(1).getPayStationCount().intValue(), 3);
        assertEquals(list.get(2).getPayStationCount().intValue(), 2);
        assertEquals(list.get(0).getIsUpdateInProgress(), false);
        assertEquals(list.get(1).getIsUpdateInProgress(), true);
        assertEquals(list.get(2).getIsUpdateInProgress(), false);
        assertEquals(list.get(0).getIsFileDeleted(), true);
        assertEquals(list.get(1).getIsFileDeleted(), false);
        assertEquals(list.get(2).getIsFileDeleted(), true);
        assertEquals(list.get(0).getIsNotExist(), false);
        assertEquals(list.get(1).getIsNotExist(), false);
        assertEquals(list.get(2).getIsNotExist(), true);
        
    }
    
    @Test
    public void test_getPaystationSettingDetailGreen() {
        
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String randomKey = keyMapping.getRandomString(this.settingsFileList.get(0), "id");
        
        request.setParameter("settingsFileId", randomKey);
        
        String resp = controller.getPayStationSettingsDetail(request, response, model);
        
        assertNotNull(resp);
        assertEquals(resp.indexOf("Green Only") > 0, true);
        assertEquals(resp.indexOf("With Yellow") > 0, false);
        assertEquals(resp.indexOf("Green 1") > 0, true);
        assertEquals(resp.indexOf("Green 2") > 0, true);
        assertEquals(resp.indexOf("Green 3") > 0, false);
        assertEquals(resp.indexOf("Yellow 1") > 0, false);
        assertEquals(resp.indexOf("Yellow 2") > 0, false);
        assertEquals(resp.indexOf("Red 1") > 0, false);
        assertEquals(resp.indexOf("Red 2") > 0, false);
        
        assertEquals(resp.indexOf("isUpdateInProgress\":true") > 0, false);
        assertEquals(resp.indexOf("isNotExist\":true") > 0, false);
    }
    
    @Test
    public void test_getPaystationSettingDetailYellow() {
        
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String randomKey = keyMapping.getRandomString(this.settingsFileList.get(1), "id");
        
        request.setParameter("settingsFileId", randomKey);
        
        String resp = controller.getPayStationSettingsDetail(request, response, model);
        
        assertNotNull(resp);
        assertEquals(resp.indexOf("Green Only") > 0, false);
        assertEquals(resp.indexOf("With Yellow") > 0, true);
        assertEquals(resp.indexOf("Green 1") > 0, false);
        assertEquals(resp.indexOf("Green 2") > 0, false);
        assertEquals(resp.indexOf("Green 3") > 0, true);
        assertEquals(resp.indexOf("Yellow 1") > 0, true);
        assertEquals(resp.indexOf("Yellow 2") > 0, true);
        assertEquals(resp.indexOf("Red 1") > 0, false);
        assertEquals(resp.indexOf("Red 2") > 0, false);
        
        assertEquals(resp.indexOf("isNotExist\":true") > 0, false);
        
    }
    
    @Test
    public void test_getPaystationSettingDetailRed() {
        
        String randomKey = WebCoreConstants.NOT_MAINTAINED_PAYSTATION_SETTING;
        
        request.setParameter("settingsFileId", randomKey);
        
        String resp = controller.getPayStationSettingsDetail(request, response, model);
        
        assertNotNull(resp);
        assertEquals(resp.indexOf("Green Only") > 0, false);
        assertEquals(resp.indexOf("With Yellow") > 0, false);
        assertEquals(resp.indexOf("Green 1") > 0, false);
        assertEquals(resp.indexOf("Green 2") > 0, false);
        assertEquals(resp.indexOf("Green 3") > 0, false);
        assertEquals(resp.indexOf("Yellow 1") > 0, false);
        assertEquals(resp.indexOf("Yellow 2") > 0, false);
        assertEquals(resp.indexOf("Red 1") > 0, true);
        assertEquals(resp.indexOf("Red 2") > 0, true);
        
        assertEquals(resp.indexOf("isNotExist\":false") > 0, false);
    }
    
    @Test
    public void test_deleteSettingsFileContent() {
        
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String randomKey = keyMapping.getRandomString(this.settingsFileList.get(1), "id");
        
        request.setParameter("settingsFileId", randomKey);
        
        String resp = controller.deleteSettingsFileContent(request, response);
        
        assertNotNull(resp);
        assertNotNull(WebCoreConstants.RESPONSE_TRUE);
    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    protected List<PointOfSale> returnGreenList(Integer fileSettingId) {
        List<PointOfSale> newList = new ArrayList<PointOfSale>();
        for (PointOfSale pos : this.greenList) {
            if (pos.getSettingsFile().getId().intValue() == fileSettingId.intValue()) {
                newList.add(pos);
            }
        }
        return newList;
    }
    
    protected List<PointOfSale> returnYellowList(Integer fileSettingId) {
        List<PointOfSale> newList = new ArrayList<PointOfSale>();
        for (PointOfSale pos : this.yellowList) {
            if (pos.getSettingsFile().getId().intValue() == fileSettingId.intValue()) {
                newList.add(pos);
            }
        }
        return newList;
    }
    
    protected List<PointOfSale> returnRedList() {
        return this.redList;
    }
    
    private void createLists() {
        
        this.settingsFileList = new ArrayList<SettingsFile>();
        
        SettingsFile greenOnly = new SettingsFile();
        greenOnly.setId(1);
        greenOnly.setName("Green Only");
        greenOnly.setUploadGmt(new Date());
        
        SettingsFile withYellow = new SettingsFile();
        withYellow.setId(2);
        withYellow.setName("With Yellow");
        withYellow.setUploadGmt(new Date());
        
        this.settingsFileList.add(greenOnly);
        this.settingsFileList.add(withYellow);
        
        this.greenList = new ArrayList<PointOfSale>();
        this.yellowList = new ArrayList<PointOfSale>();
        this.redList = new ArrayList<PointOfSale>();
        
        Location location = new Location();
        location.setId(1);
        location.setName("Name");
        
        PosServiceState posServiceState = new PosServiceState();
        posServiceState.setLastPaystationSettingUploadGmt(new Date());
        
        PaystationType paystationType = new PaystationType();
        paystationType.setId(1);
        Paystation paystation = new Paystation();
        paystation.setPaystationType(paystationType);
        
        PointOfSale green1 = new PointOfSale();
        green1.setId(1);
        green1.setName("Green 1");
        green1.setSerialNumber("Green 1");
        green1.setLocation(location);
        green1.setPosServiceState(posServiceState);
        green1.setPaystation(paystation);
        green1.setSettingsFile(greenOnly);
        
        PointOfSale green2 = new PointOfSale();
        green2.setId(2);
        green2.setName("Green 2");
        green2.setSerialNumber("Green 2");
        green2.setLocation(location);
        green2.setPosServiceState(posServiceState);
        green2.setPaystation(paystation);
        green2.setSettingsFile(greenOnly);
        
        PointOfSale green3 = new PointOfSale();
        green3.setId(3);
        green3.setName("Green 3");
        green3.setSerialNumber("Green 3");
        green3.setLocation(location);
        green3.setPosServiceState(posServiceState);
        green3.setPaystation(paystation);
        green3.setSettingsFile(withYellow);
        
        this.greenList.add(green1);
        this.greenList.add(green2);
        this.greenList.add(green3);
        
        PointOfSale yellow1 = new PointOfSale();
        yellow1.setId(4);
        yellow1.setName("Yellow 1");
        yellow1.setSerialNumber("Yellow 1");
        yellow1.setLocation(location);
        yellow1.setPosServiceState(posServiceState);
        yellow1.setPaystation(paystation);
        yellow1.setSettingsFile(withYellow);
        
        PointOfSale yellow2 = new PointOfSale();
        yellow2.setId(5);
        yellow2.setName("Yellow 2");
        yellow2.setSerialNumber("Yellow 2");
        yellow2.setLocation(location);
        yellow2.setPosServiceState(posServiceState);
        yellow2.setPaystation(paystation);
        yellow2.setSettingsFile(withYellow);
        
        this.yellowList.add(yellow1);
        this.yellowList.add(yellow2);
        
        PointOfSale red1 = new PointOfSale();
        red1.setId(6);
        red1.setName("Red 1");
        red1.setSerialNumber("Red 1");
        red1.setLocation(location);
        red1.setPosServiceState(posServiceState);
        red1.setPaystation(paystation);
        
        PointOfSale red2 = new PointOfSale();
        red2.setId(7);
        red2.setName("Red 2");
        red2.setSerialNumber("Red 2");
        red2.setLocation(location);
        red2.setPosServiceState(posServiceState);
        red2.setPaystation(paystation);
        
        this.redList.add(red1);
        this.redList.add(red2);
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
        };
    }
    
}
