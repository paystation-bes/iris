package com.digitalpaytech.mvc.customeradmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerPropertyType;
import com.digitalpaytech.domain.EventDefinition;
import com.digitalpaytech.domain.EventDeviceType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.EventType;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PaystationType;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.RoutePOS;
import com.digitalpaytech.domain.RouteType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.dto.SummarySearchCriteria;
import com.digitalpaytech.dto.customeradmin.AlertCentreAlertInfo;
import com.digitalpaytech.dto.customeradmin.AlertSummaryEntry;
import com.digitalpaytech.dto.customeradmin.LocationRouteFilterInfo;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.mvc.customeradmin.support.AlertCentreFilterForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.mvc.support.impl.CommonControllerHelperImpl;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.LocationServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.AlertsServiceTestImpl;
import com.digitalpaytech.testing.services.impl.CustomerAdminServiceTestImpl;
import com.digitalpaytech.testing.services.impl.PointOfSaleServiceTestImpl;
import com.digitalpaytech.testing.services.impl.PosEventCurrentServiceTestImpl;
import com.digitalpaytech.testing.services.impl.RouteServiceTestImpl;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObjectId;
import com.digitalpaytech.validation.customeradmin.AlertCentreFilterValidator;


// TODO
// Fix the test cases.


public class AlertCentreControllerTest {
    
    public static final String FORM_ALERTS = "alertsForm";
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private SessionTool sessionTool;
    private ModelMap model;
    private UserAccount userAccount;
    private WebUser webUser;
    private AlertCentreController controller;
    private BindingResult result;
    
    @Before
    public void setUp() {
        
        userAccount = new UserAccount();
        controller = new AlertCentreController();
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        session = new MockHttpSession();
        request.setSession(session);
        model = new ModelMap();
        sessionTool = SessionTool.getInstance(request);
        
        Set<UserRole> roles = new HashSet<UserRole>();
        UserRole role = new UserRole();
        Role r = new Role();
        Set<RolePermission> rps = new HashSet<RolePermission>();
        
        RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS);
        rp1.setPermission(permission1);
        
        rps.add(rp1);
        
        r.setId(3);
        r.setRolePermissions(rps);
        
        role.setId(3);
        role.setRole(r);
        roles.add(role);
        userAccount.setUserRoles(roles);
        
        Customer customer = new Customer();
        customer.setId(2);
        userAccount.setCustomer(customer);
        createAuthentication(userAccount);
        userAccount.setId(2);
        
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        webUser = new WebUser(2, 3, "testUser", "password", "salt", true, authorities);
        Authentication authentication = new UsernamePasswordAuthenticationToken(webUser, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS);
        webUser.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        
        webUser.setCustomerTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT);
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        WidgetMetricsHelper.registerComponents();
        
        controller.setCustomerAdminService(new CustomerAdminServiceTestImpl() {
            public List<CustomerProperty> getCustomerPropertyByCustomerId(int customerId) {
                List<CustomerProperty> customerProperties = new ArrayList<CustomerProperty>();
                CustomerProperty property = new CustomerProperty();
                CustomerPropertyType propertyType = new CustomerPropertyType();
                propertyType.setId(WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
                property.setCustomerPropertyType(propertyType);
                property.setPropertyValue(WebCoreConstants.SERVER_TIMEZONE_GMT);
                customerProperties.add(property);
                return customerProperties;
            }
        });
        controller.setAlertsService(new AlertsServiceTestImpl() {
            public List<EventDeviceType> findAllEventDeviceTypes() {
                return createDeviceTypeList();
            }
            
            public EventDefinition findEventDefinitionByDeviceStatusActionSeverityId(int deviceTypeId, int statusTypeId, int actionTypeId,
                int severityTypeId) {
                EventDefinition definition = new EventDefinition();
                definition.setDescription("Test alert message");
                return definition;
            }
            
            public List<EventSeverityType> findAllEventSeverityTypes() {
                EventSeverityType severityType = new EventSeverityType();
                List<EventSeverityType> severityTypeList = new ArrayList<EventSeverityType>();
                severityType.setName("test Severity");
                severityType.setId(1);
                severityTypeList.add(severityType);
                return severityTypeList;
            }
            
            @Override
            public List<AlertSummaryEntry> findPointOfSalesForAlertSummary(final SummarySearchCriteria criteria, final String timeZone) {
                List<AlertSummaryEntry> list = new ArrayList<AlertSummaryEntry>();
                
                AlertSummaryEntry entry = new AlertSummaryEntry();
                entry.setBatteryLevel((byte) WebCoreConstants.LEVEL_NORMAL);
                entry.setBatteryVoltage(12.6f);
                entry.setLastSeenDate("Yesterday");
                entry.setLastSeenSeverity(1);
                entry.setLocationName("Location");
                entry.setPaperLevel((byte) WebCoreConstants.LEVEL_NORMAL);
                entry.setPointOfSaleName("POS");
                entry.setSerial("555566660001");
                entry.setSeverity(1);
                entry.setPosRandomId(new WebObjectId(PointOfSale.class, 1));
                entry.setLocationRandomId(new WebObjectId(Location.class, 1));
                
                list.add(entry);
                
                return list;
            }
            
        });
        controller.setLocationService(new LocationServiceImpl() {
            public List<Location> findLocationsByCustomerId(Integer customerId) {
                return null;
            }
        });
        controller.setRouteService(new RouteServiceTestImpl() {
            public Collection<Route> findRoutesByCustomerIdAndRouteTypeId(int customerId, int routeTypeId) {
                return null;
            }
        });
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            public List<PointOfSale> findPointOfSalesByCustomerId(int customerId) {
                return createPointOfSaleList();
            }
            
            public List<PointOfSale> findPointOfSalesByLocationId(int locationId) {
                return createPointOfSaleList();
            }
            
            public List<PointOfSale> findPointOfSalesByLocationIdOrderByName(int locationId) {
                return createPointOfSaleList();
            }
            
            public List<PointOfSale> findPointOfSalesByRouteId(int routeId) {
                return createPointOfSaleList();
            }
            
            public PosHeartbeat findPosHeartbeatByPointOfSaleId(int pointOfSaleId) {
                PosHeartbeat heartbeat = new PosHeartbeat();
                heartbeat.setLastHeartbeatGmt(new Date());
                return heartbeat;
            }
            
            public PointOfSale findPointOfSaleById(Integer id) {
                return createPointOfSaleList().get(0);
            }
            
            public PlacementMapBordersInfo findMapBordersByCustomerId(Integer customerId) {
                PlacementMapBordersInfo mapBorders = new PlacementMapBordersInfo();
                mapBorders.setMaxLatitude(new BigDecimal(10));
                mapBorders.setMinLatitude(new BigDecimal(-10));
                mapBorders.setMaxLongitude(new BigDecimal(10));
                mapBorders.setMinLongitude(new BigDecimal(-10));
                return mapBorders;
            }
            
        });
        controller.setAlertCentreFilterValidator(new AlertCentreFilterValidator() {
            public void validate(WebSecurityForm<AlertCentreFilterForm> command, Errors errors) {
                return;
            }
        });
        
        controller.setCommonControllerHelper(new CommonControllerHelperImpl() {
            public LocationRouteFilterInfo createLocationRouteFilter(Integer customerId, RandomKeyMapping keyMapping, Integer routeTypeId) {
                LocationRouteFilterInfo locationRouteFilter = new LocationRouteFilterInfo();
                return locationRouteFilter;
            }
        });
        
        controller.setMessageHelper(new MessageHelper() {
            public String getMessage(String messageKey) {
                return "";
            }
        });
    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    @Test
    public void test_AlertCentreIndex() {
        
        String resp = controller.alertCentreIndex(request, response, model, controller.initAlertsForm(request));
        
        assertEquals("/alerts/index", resp);
        
        Object moduleFilter = model.get("moduleFilter");
        Object locationFilter = model.get("locationFilter");
        Object routeFilter = model.get("routeFilter");
        
        assertNotNull(moduleFilter);
        assertNotNull(locationFilter);
        assertNotNull(routeFilter);
    }
    
    @Test
    public void test_getActiveAlertsByFilter_byLocation() {
        
        Location location = new Location();
        location.setId(1);
        
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        String randomKey = keyMapping.getRandomString(location, "id");
        request.setParameter("locationID", randomKey);
        
        AlertCentreFilterForm form = new AlertCentreFilterForm();
        form.setRandomId(randomKey);
        form.setAlert(true);
        WebSecurityForm<AlertCentreFilterForm> webSecurityForm = new WebSecurityForm<AlertCentreFilterForm>(null, form);
        result = new BeanPropertyBindingResult(form, "alertsForm");
        
        String resp = controller.getActiveAlertsByFilter(webSecurityForm, result, request, response, model);
        assertTrue(resp.contains("{\"alertsList\""));
    }
    
    @Test
    public void test_getActiveAlertsByFilter_byRoute() {
        
        Route route = new Route();
        route.setId(1);
        
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        String randomKey = keyMapping.getRandomString(route, "id");
        
        AlertCentreFilterForm form = new AlertCentreFilterForm();
        form.setRandomId(randomKey);
        form.setAlert(true);
        WebSecurityForm<AlertCentreFilterForm> webSecurityForm = new WebSecurityForm<AlertCentreFilterForm>(null, form);
        result = new BeanPropertyBindingResult(form, "alertsForm");
        
        String resp = controller.getActiveAlertsByFilter(webSecurityForm, result, request, response, model);
        assertTrue(resp.contains("{\"alertsList\""));
    }
    
    @Test
    public void test_getActiveAlertsByFilter_bySeverity() {
        
        EventSeverityType severityType = new EventSeverityType();
        severityType.setId(1);
        
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        String randomKey = keyMapping.getRandomString(severityType, "id");
        
        AlertCentreFilterForm form = new AlertCentreFilterForm();
        form.setSeverityRandomId(randomKey);
        form.setAlert(true);
        WebSecurityForm<AlertCentreFilterForm> webSecurityForm = new WebSecurityForm<AlertCentreFilterForm>(null, form);
        result = new BeanPropertyBindingResult(form, "alertsForm");
        
        String resp = controller.getActiveAlertsByFilter(webSecurityForm, result, request, response, model);
        assertTrue(resp.contains("{\"alertsList\""));
    }
    
    @Test
    public void test_getActiveAlertsByFilter_byModule() {
        
        EventDeviceType device = new EventDeviceType();
        device.setId(1);
        
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        String randomKey = keyMapping.getRandomString(device, "id");
        
        AlertCentreFilterForm form = new AlertCentreFilterForm();
        form.setEventDeviceRandomId(randomKey);
        form.setAlert(true);
        WebSecurityForm<AlertCentreFilterForm> webSecurityForm = new WebSecurityForm<AlertCentreFilterForm>(null, form);
        result = new BeanPropertyBindingResult(form, "alertsForm");
        
        String resp = controller.getActiveAlertsByFilter(webSecurityForm, result, request, response, model);
        assertTrue(resp.contains("{\"alertsList\""));
    }
    
    @Test
    public void test_getActiveAlertsByFilter_byModuleAndSeverity() {
        
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        
        EventDeviceType device = new EventDeviceType();
        device.setId(1);
        String moduleRandomKey = keyMapping.getRandomString(device, "id");
        EventSeverityType severityType = new EventSeverityType();
        severityType.setId(1);
        String severityRandomKey = keyMapping.getRandomString(severityType, "id");
        
        AlertCentreFilterForm form = new AlertCentreFilterForm();
        form.setEventDeviceRandomId(moduleRandomKey);
        form.setSeverityRandomId(severityRandomKey);
        form.setAlert(true);
        WebSecurityForm<AlertCentreFilterForm> webSecurityForm = new WebSecurityForm<AlertCentreFilterForm>(null, form);
        result = new BeanPropertyBindingResult(form, "alertsForm");
        
        String resp = controller.getActiveAlertsByFilter(webSecurityForm, result, request, response, model);
        assertTrue(resp.contains("{\"alertsList\""));
    }
    
    @Test
    public void test_getActiveAlertsByFilter_noFilter() {
        AlertCentreFilterForm form = new AlertCentreFilterForm();
        form.setAlert(true);
        WebSecurityForm<AlertCentreFilterForm> webSecurityForm = new WebSecurityForm<AlertCentreFilterForm>(null, form);
        result = new BeanPropertyBindingResult(form, "alertsForm");
        
        String resp = controller.getActiveAlertsByFilter(webSecurityForm, result, request, response, model);
        assertTrue(resp.contains("{\"alertsList\""));
    }
    
    @Test
    public void test_getAlertCentrePayStationActiveAlerts() {
        
        Paystation payStation = new Paystation();
        payStation.setId(1);
        
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        String randomKey = keyMapping.getRandomString(payStation, "id");
        request.setParameter("pointOfSaleId", randomKey);
        
        controller.setRouteService(new RouteServiceTestImpl() {
            public List<RoutePOS> findRoutePOSesByPointOfSaleId(Integer pointOfSaleId) {
                return createRoutePOSList();
            }
            
            public List<RoutePOS> findRoutePOSesByPointOfSaleIdAndRouteTypeId(Integer pointOfSaleId, Integer routeTypeId) {
                return createRoutePOSList();
            }
            
            public List<Route> findRoutesByPointOfSaleIdAndRouteTypeId(int pointOfSaleId, int routeTypeId) {
                return createRouteList();
            }
            
        });
        
        controller.setPosEventCurrentService(new PosEventCurrentServiceTestImpl() {
            public final List<PosEventCurrent> findMaintenanceCentreDetailByPointOfSaleId(final Integer pointOfSaleId) {
                List<PosEventCurrent> posEventCurrentList = new ArrayList<PosEventCurrent>();
                final EventType eventType = new EventType((byte) 5);
                eventType.setEventDeviceType(new EventDeviceType(4));
                final PosEventCurrent posEventCurrent = new PosEventCurrent(new PointOfSale(1), new EventSeverityType(1), null, eventType, null,true, false);
                posEventCurrentList.add(posEventCurrent);
                return posEventCurrentList;
            }
        });
        
        String resp = controller.getAlertCentrePayStationActiveAlerts(request, response, model);
        assertTrue(resp.contains("{\"payStationInfo\""));
    }
    
    @Test
    public void test_getResolvedAlertsByFilter_byLocation() {
        
        Location location = new Location();
        location.setId(1);
        
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        String randomKey = keyMapping.getRandomString(location, "id");
        
        AlertCentreFilterForm form = new AlertCentreFilterForm();
        form.setRandomId(randomKey);
        form.setAlert(false);
        WebSecurityForm<AlertCentreFilterForm> webSecurityForm = new WebSecurityForm<AlertCentreFilterForm>(null, form);
        result = new BeanPropertyBindingResult(form, "alertsForm");
        
        String resp = controller.getActiveAlertsByFilter(webSecurityForm, result, request, response, model);
        assertTrue(resp.contains("{\"alertsList\""));
    }
    
    @Test
    public void test_getResolvedAlertsByFilter_byRoute() {
        
        Route route = new Route();
        route.setId(1);
        
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        String randomKey = keyMapping.getRandomString(route, "id");
        
        AlertCentreFilterForm form = new AlertCentreFilterForm();
        form.setRandomId(randomKey);
        form.setAlert(false);
        WebSecurityForm<AlertCentreFilterForm> webSecurityForm = new WebSecurityForm<AlertCentreFilterForm>(null, form);
        result = new BeanPropertyBindingResult(form, "alertsForm");
        
        String resp = controller.getActiveAlertsByFilter(webSecurityForm, result, request, response, model);
        assertTrue(resp.contains("{\"alertsList\""));
    }
    
    @Test
    public void test_getResolvedAlertsByFilter_bySeverity() {
        
        EventSeverityType severityType = new EventSeverityType();
        severityType.setId(1);
        
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        String randomKey = keyMapping.getRandomString(severityType, "id");
        
        AlertCentreFilterForm form = new AlertCentreFilterForm();
        form.setSeverityRandomId(randomKey);
        form.setAlert(false);
        WebSecurityForm<AlertCentreFilterForm> webSecurityForm = new WebSecurityForm<AlertCentreFilterForm>(null, form);
        result = new BeanPropertyBindingResult(form, "alertsForm");
        
        String resp = controller.getActiveAlertsByFilter(webSecurityForm, result, request, response, model);
        assertTrue(resp.contains("{\"alertsList\""));
    }
    
    @Test
    public void test_getResolvedAlertsByFilter_byModule() {
        
        EventDeviceType device = new EventDeviceType();
        device.setId(1);
        
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        String randomKey = keyMapping.getRandomString(device, "id");
        
        AlertCentreFilterForm form = new AlertCentreFilterForm();
        form.setEventDeviceRandomId(randomKey);
        form.setAlert(false);
        WebSecurityForm<AlertCentreFilterForm> webSecurityForm = new WebSecurityForm<AlertCentreFilterForm>(null, form);
        result = new BeanPropertyBindingResult(form, "alertsForm");
        
        String resp = controller.getActiveAlertsByFilter(webSecurityForm, result, request, response, model);
        assertTrue(resp.contains("{\"alertsList\""));
    }
    
    @Test
    public void test_getResolvedAlertsByFilter_byModuleAndSeverity() {
        
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        
        EventDeviceType device = new EventDeviceType();
        device.setId(1);
        String moduleRandomKey = keyMapping.getRandomString(device, "id");
        
        EventSeverityType severityType = new EventSeverityType();
        severityType.setId(1);
        String severityRandomKey = keyMapping.getRandomString(severityType, "id");
        
        AlertCentreFilterForm form = new AlertCentreFilterForm();
        form.setEventDeviceRandomId(moduleRandomKey);
        form.setSeverityRandomId(severityRandomKey);
        form.setAlert(false);
        WebSecurityForm<AlertCentreFilterForm> webSecurityForm = new WebSecurityForm<AlertCentreFilterForm>(null, form);
        result = new BeanPropertyBindingResult(form, "alertsForm");
        
        String resp = controller.getActiveAlertsByFilter(webSecurityForm, result, request, response, model);
        assertTrue(resp.contains("{\"alertsList\""));
    }
    
    @Test
    public void test_getResolvedAlertsByFilter_noFilter() {
        
        AlertCentreFilterForm form = new AlertCentreFilterForm();
        form.setAlert(false);
        WebSecurityForm<AlertCentreFilterForm> webSecurityForm = new WebSecurityForm<AlertCentreFilterForm>(null, form);
        result = new BeanPropertyBindingResult(form, "alertsForm");
        
        String resp = controller.getActiveAlertsByFilter(webSecurityForm, result, request, response, model);
        assertTrue(resp.contains("{\"alertsList\""));
    }
    
    @Test
    public void test_getPosSummary() {
        
        String resp = controller.getPosSummary(request, response, model, controller.initAlertsForm(request));
        
        assertEquals("/alerts/summary", resp);
        
        Object moduleFilter = model.get("severityFilter");
        Object locationFilter = model.get("locationFilter");
        Object routeFilter = model.get("routeFilter");
        
        assertNotNull(moduleFilter);
        assertNotNull(locationFilter);
        assertNotNull(routeFilter);
    }
    
    @Test
    public void test_getPosSummaryList() {
        
        EventSeverityType evt = new EventSeverityType();
        evt.setId(WebCoreConstants.SEVERITY_MAJOR);
        
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        String randomKey = keyMapping.getRandomString(evt, "id");
        
        AlertCentreFilterForm form = new AlertCentreFilterForm();
        form.setRandomId(randomKey);
        
        WebSecurityForm<AlertCentreFilterForm> webSecurityForm = new WebSecurityForm<AlertCentreFilterForm>(null, form);
        result = new BeanPropertyBindingResult(form, FORM_ALERTS);
        String resp = controller.getPosSummaryList(webSecurityForm, result, request, response, model);
        assertTrue(resp.contains("alertList"));
    }
    
    protected List<RoutePOS> createRoutePOSList() {
        List<RoutePOS> routePOSes = new ArrayList<RoutePOS>();
        RoutePOS routePOS = new RoutePOS();
        RouteType routeType = new RouteType();
        routeType.setId(WebCoreConstants.ROUTE_TYPE_MAINTENANCE);
        Route route = new Route();
        route.setName("Maintenance route");
        route.setRouteType(routeType);
        routePOS.setRoute(route);
        routePOSes.add(routePOS);
        return routePOSes;
    }
    
    protected List<Route> createRouteList() {
        List<Route> routeList = new ArrayList<Route>();
        RouteType routeType = new RouteType();
        routeType.setId(WebCoreConstants.ROUTE_TYPE_MAINTENANCE);
        Route route = new Route();
        route.setName("Maintenance route");
        route.setRouteType(routeType);
        routeList.add(route);
        return routeList;
    }
    
    protected List<EventDeviceType> createDeviceTypeList() {
        
        List<EventDeviceType> eventDeviceTypes = new ArrayList<EventDeviceType>();
        EventDeviceType eventDeviceType = new EventDeviceType();
        eventDeviceType.setName("module1");
        eventDeviceType.setId(1);
        eventDeviceTypes.add(eventDeviceType);
        return eventDeviceTypes;
    }
    
    protected List<PointOfSale> createPointOfSaleList() {
        
        List<PointOfSale> pointOfSales = new ArrayList<PointOfSale>();
        PointOfSale pointOfSale = new PointOfSale();
        
        Paystation payStation = new Paystation();
        PaystationType payStationType = new PaystationType();
        payStationType.setId(1);
        payStation.setPaystationType(payStationType);
        pointOfSale.setPaystation(payStation);
        
        pointOfSale.setId(1);
        pointOfSale.setName("POS Name");
        pointOfSale.setSerialNumber("123456789abcdefg");
        pointOfSale.setLatitude(new BigDecimal(100));
        pointOfSale.setLongitude(new BigDecimal(100));
        
        Location location = new Location();
        location.setName("Location 1");
        
        pointOfSale.setLocation(location);
        
        Set<RoutePOS> routePOSes = new HashSet<RoutePOS>(createRoutePOSList());
        pointOfSale.setRoutePOSes(routePOSes);
        
        pointOfSales.add(pointOfSale);
        return pointOfSales;
    }
    
    
    protected List<AlertCentreAlertInfo> createActiveAlertInfoList() {
        PointOfSale pos = createPointOfSaleList().get(0);
        
        ArrayList<AlertCentreAlertInfo> result = new ArrayList<AlertCentreAlertInfo>();
        AlertCentreAlertInfo info = new AlertCentreAlertInfo();
        info.setPointOfSaleName(pos.getName());
        info.setPosRandomId(new WebObjectId(PointOfSale.class, pos.getId()));
        info.setLatitude(pos.getLatitude());
        info.setLongitude(pos.getLongitude());
        
        result.add(info);
        
        return result;
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
        };
    }
    
}