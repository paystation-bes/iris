package com.digitalpaytech.mvc.customeradmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.dto.customeradmin.LocationRouteFilterInfo;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.mvc.PayStationPlacementController;
import com.digitalpaytech.mvc.support.impl.CommonControllerHelperImpl;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.LocationServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.CustomerAdminServiceTestImpl;
import com.digitalpaytech.testing.services.impl.DashboardServiceTestImpl;
import com.digitalpaytech.testing.services.impl.PointOfSaleServiceTestImpl;
import com.digitalpaytech.testing.services.impl.RouteServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

public class CustomerAdminPayStationPlacementControllerTest {
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private ModelMap model;
    private UserAccount userAccount;
    private PayStationPlacementController controller;
    
    @Before
    public void setUp() {
        
        controller = new PayStationPlacementController();
        
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        session = new MockHttpSession();
        this.request.setSession(session);
        this.model = new ModelMap();
        
        userAccount = new UserAccount();
        userAccount.setId(2);
        Set<UserRole> roles = new HashSet<UserRole>();
        UserRole role = new UserRole();
        Role r = new Role();
        Set<RolePermission> rps = new HashSet<RolePermission>();
        
        RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_VIEW_ROUTES);
        rp1.setPermission(permission1);
        
        RolePermission rp2 = new RolePermission();
        rp2.setId(2);
        Permission permission2 = new Permission();
        permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_ROUTES);
        rp2.setPermission(permission2);
        
        RolePermission rp3 = new RolePermission();
        rp3.setId(3);
        Permission permission3 = new Permission();
        permission3.setId(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS);
        rp3.setPermission(permission3);
        
        RolePermission rp4 = new RolePermission();
        rp4.setId(4);
        Permission permission4 = new Permission();
        permission4.setId(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS);
        rp4.setPermission(permission4);
        
        RolePermission rp5 = new RolePermission();
        rp5.setId(5);
        Permission permission5 = new Permission();
        permission5.setId(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS);
        rp5.setPermission(permission5);
        
        RolePermission rp6 = new RolePermission();
        rp6.setId(6);
        Permission permission6 = new Permission();
        permission6.setId(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS);
        rp6.setPermission(permission6);
        
        rps.add(rp1);
        rps.add(rp2);
        rps.add(rp3);
        rps.add(rp4);
        rps.add(rp5);
        rps.add(rp6);
        
        r.setId(3);
        r.setRolePermissions(rps);
        
        role.setId(3);
        role.setRole(r);
        roles.add(role);
        userAccount.setUserRoles(roles);
        
        Customer customer = new Customer();
        customer.setId(2);
        userAccount.setCustomer(customer);
        createAuthentication(userAccount);
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_ROUTES);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_ROUTES);
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS);
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    //========================================================================================================================
    // "Pay Station Placement" tab
    //========================================================================================================================
    
    @Test
    public void test_placedPayStations() {
        
        PayStationPlacementController controller = new PayStationPlacementController();
        
        controller.setLocationService(new LocationServiceImpl() {
            public List<Location> findLocationsByCustomerId(Integer customerId) {
                List<Location> locations = new ArrayList<Location>();
                Location location = new Location();
                Location parentLocation = new Location();
                parentLocation.setName("parent");
                location.setId(1);
                location.setName("location 1");
                location.setLocation(parentLocation);
                locations.add(location);
                return locations;
            }
            
            public List<Location> findLocationsWithPointOfSalesByCustomerId(Integer customerId) {
                List<Location> locations = new ArrayList<Location>();
                Location location = new Location();
                Location parentLocation = new Location();
                parentLocation.setName("parent");
                location.setId(1);
                location.setName("location 1");
                location.setLocation(parentLocation);
                locations.add(location);
                return locations;
            }
            
        });
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            @Override
            public List<Paystation> findPayStationsByLocationId(int id) {
                List<Paystation> paystations = new ArrayList<Paystation>();
                paystations.add(new Paystation());
                return paystations;
            }
            
            public PlacementMapBordersInfo findMapBordersByCustomerId(Integer customerId) {
                return null;
            }
            
        });
        controller.setRouteService(new RouteServiceTestImpl() {
            public Collection<Route> findRoutesByCustomerId(int customerId) {
                Collection<Route> routes = new ArrayList<Route>();
                Route route = new Route();
                route.setName("route 1");
                route.setId(1);
                routes.add(route);
                return routes;
            }
            
            @Override
            public List<Paystation> findPayStationsByRouteId(int id) {
                List<Paystation> paystations = new ArrayList<Paystation>();
                paystations.add(new Paystation());
                return paystations;
            }
            
            public List<Route> findRoutesWithPointOfSalesByCustomerId(Integer customerId) {
                List<Route> routes = new ArrayList<Route>();
                Route route = new Route();
                route.setName("route 1");
                route.setId(1);
                routes.add(route);
                return routes;
            }
        });
        
        controller.setCommonControllerHelper(new CommonControllerHelperImpl() {
            public LocationRouteFilterInfo createLocationRouteFilter(Integer customerId, RandomKeyMapping keyMapping) {
                LocationRouteFilterInfo locationRouteFilter = new LocationRouteFilterInfo();
                return locationRouteFilter;
            }
        });
        
        String returnValue = controller.placedPayStations(request, response, model, "/settings/locations/paystationPlacement");
        assertEquals("/settings/locations/paystationPlacement", returnValue);
        assertNotNull(model.get("filterValues"));
    }
    
    @Test
    public void test_payStations() {
        
        controller.setLocationService(new LocationServiceImpl() {
            public List<Location> findLocationsByCustomerId(Integer customerId) {
                List<Location> locations = new ArrayList<Location>();
                Location location = new Location();
                location.setId(1);
                locations.add(location);
                return locations;
            }
        });
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            public List<PointOfSale> findPointOfSalesByCustomerId(int customerId) {
                List<PointOfSale> pointOfSales = new ArrayList<PointOfSale>();
                PointOfSale pointOfSale = new PointOfSale();
                pointOfSale.setId(1);
                return pointOfSales;
            }
            
            public List<PointOfSale> findValidPointOfSaleByCustomerIdOrderByName(int customerId) {
                List<PointOfSale> pointOfSales = new ArrayList<PointOfSale>();
                PointOfSale pointOfSale = new PointOfSale();
                pointOfSale.setId(1);
                return pointOfSales;
            }
        });
        
        controller.setCommonControllerHelper(new CommonControllerHelperImpl() {
            public LocationRouteFilterInfo createLocationRouteFilter(Integer customerId, RandomKeyMapping keyMapping) {
                LocationRouteFilterInfo locationRouteFilter = new LocationRouteFilterInfo();
                return locationRouteFilter;
            }
        });
        
        String returnValue = controller.payStations(request, response, model);
        assertNotNull(returnValue);
    }
    
    @Test
    public void test_pinPayStation() {
        
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            public PointOfSale findPointOfSaleByPaystationId(int id) {
                return new PointOfSale();
            }
            
            public PointOfSale findPointOfSaleById(Integer pointOfSaleId) {
                return new PointOfSale();
            }
        });
        controller.setCustomerAdminService(new CustomerAdminServiceTestImpl() {
            public void saveOrUpdatePointOfSale(PointOfSale pointOfSale) {
            }
        });
        controller.setDashboardService(new DashboardServiceTestImpl() {
            public List<Widget> findMapWidgetsByUserAccountId(int userAccountId) {
                return null;
            }
        });
        
        Paystation payStation = new Paystation();
        payStation.setId(1);
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String randomKey = keyMapping.getRandomString(payStation, "id");
        request.setParameter("payStationID", randomKey);
        request.addParameter("latitude", "45");
        request.addParameter("longitude", "90");
        request.addParameter("altitude", "1");
        
        String returnValue = controller.pinPayStation(request, response, model);
        assertEquals(WebCoreConstants.RESPONSE_TRUE, returnValue);
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
        };
    }
    
}
