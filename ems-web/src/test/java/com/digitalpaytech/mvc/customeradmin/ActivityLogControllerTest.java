package com.digitalpaytech.mvc.customeradmin;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DirectFieldBindingResult;
import org.springframework.validation.Errors;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.ActivityLog;
import com.digitalpaytech.domain.ActivityType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerPropertyType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.customeradmin.support.ActivityLogFilterForm;
import com.digitalpaytech.mvc.support.PropertiesRetrieval;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.impl.CommonControllerHelperImpl;
import com.digitalpaytech.service.impl.ActivityLogServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.CustomerAdminServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.customeradmin.ActivityLogFormValidator;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/ems-web.xml" })
public class ActivityLogControllerTest {
    
    private int userId = 3;
    private int customerId = 2;
    private UserAccount userAccount;
    private CustomerAdminActivityLogController controller;
    private WebSecurityUtil securityUtil;
    private MockHttpServletRequest req;
    private MockHttpServletResponse res;
    private MockHttpSession ses;
    private ModelMap model;
    private static RandomKeyMapping keyMapping;
    ActivityLogFilterForm activityLogFilterForm;
    
    @Autowired
    private ActivityLogFormValidator activityLogFormValidator;
    
    //In seconds
    int oneMinute = 60 * (-1);
    int oneHour = 60 * 60 * (-1);
    int oneDay = 60 * 60 * 24 * (-1);
    int oneMonth = 60 * 60 * 24 * 30 * (-1);
    int oneYear = 60 * 60 * 24 * 365 * (-1);
    
    @Before
    public void setUp() {
        TimeZone.setDefault(TimeZone.getTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT));
        
        controller = new CustomerAdminActivityLogController();
        activityLogFilterForm = new ActivityLogFilterForm();
        activityLogFilterForm.setPage("1");
        userAccount = new UserAccount();
        Customer customer = new Customer();
        customer.setId(customerId);
        userAccount.setId(userId);
        userAccount.setCustomer(customer);
        createAuthentication(userAccount);
        
        securityUtil = new WebSecurityUtil();
        securityUtil.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        securityUtil.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                return userAccount;
            }
        });
        controller.setPropertiesRetrieval(new PropertiesRetrieval() {
            public String getPropertyEN(String messageKey, String[] args) {
                return "Test activity message";
            }
        });
        controller.setCustomerAdminService(new CustomerAdminServiceTestImpl() {
            @Override
            public List<CustomerProperty> getCustomerPropertyByCustomerId(int customerId) {
                return createCustomerProperty();
            }
        });
        controller.setActivityLogFormValidator(new ActivityLogFormValidator() {
            @Override
            public void validate(WebSecurityForm<ActivityLogFilterForm> command, Errors errors, RandomKeyMapping keyMapping) {
            }
        });
        controller.setCommonControllerHelper(new CommonControllerHelperImpl() {
            @Override
            public String getMessage(String key) {
                if (StringUtils.isEmpty(key)) {
                    return "All";
                } else if (key.equalsIgnoreCase("label.date.range.option.LAST_24_HOURS")) {
                    return "Last 24 Hours";
                } else if (key.equalsIgnoreCase("label.date.range.option.THIS_WEEK")) {
                    return "This Week";
                } else if (key.equalsIgnoreCase("label.date.range.option.LAST_WEEK")) {
                    return "Last Week";
                } else if (key.equalsIgnoreCase("label.date.range.option.LAST_2_WEEKS")) {
                    return "Last 2 Weeks";
                } else if (key.equalsIgnoreCase("label.date.range.option.THIS_MONTH")) {
                    return "This Month";
                } else if (key.equalsIgnoreCase("label.date.range.option.LAST_MONTH")) {
                    return "Last Month";
                } else if (key.equalsIgnoreCase("label.date.range.option.LAST_3_MONTHS")) {
                    return "Last 3 Months";
                } else if (key.equalsIgnoreCase("label.date.range.option.LAST_6_MONTHS")) {
                    return "Last 6 Months";
                } else if (key.equalsIgnoreCase("label.date.range.option.LAST_12_MONTH")) {
                    return "Last 12 Months";
                } else if (key.equalsIgnoreCase("label.date.range.option.LAST_30_DAYS")) {
                    return "Last 30 Days";
                } else if (key.equalsIgnoreCase("label.date.range.option.LAST_7_DAYS")) {
                    return "Last 7 Days";
                } else if (key.equalsIgnoreCase("label.date.range.option.LAST_YEAR")) {
                    return "Last Year";
                } else if (key.equalsIgnoreCase("label.date.range.option.CUSTOM_DATE_RANGE")) {
                    return "Custom Date Range";
                }
                return "";
            }
        });
        
        //		controller.setActivityLogFormValidator(activityLogFormValidator);
        
        req = new MockHttpServletRequest();
        res = new MockHttpServletResponse();
        ses = new MockHttpSession();
        req.setSession(ses);
        model = new ModelMap();
        
        keyMapping = SessionTool.getInstance(req).getKeyMapping();
    }
    
    @Test
    public void getActivityLogTest_CurrentTime() {
        
        WebSecurityForm<ActivityLogFilterForm> secForm = new WebSecurityForm<ActivityLogFilterForm>(null, createActivityLogFilterForm(keyMapping));
        //secForm.setWrappedObject(activityLogFilterForm);
        secForm.setInitToken("testSession");
        secForm.setPostToken("testSession");
        Errors errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        
        controller.setActivityLogService(new ActivityLogServiceImpl() {
            @Override
            public List<ActivityLog> findActivityLogWithCriteria(Calendar start, Calendar end, List<Integer> userIds, List<Integer> logTypes,
                String order, String column, int page, int numOfRecord) {
                return createActivityLog("Login", userId, "Test", 0);
            }
            
            @Override
            public ActivityType findActivityTypeByActivityId(int activityTypeId) {
                ActivityType activityType = new ActivityType();
                activityType.setMessagePropertiesKey("testKey");
                return activityType;
            }
        });
        
        String activityLogJson = controller.getActivityLog(secForm, (BindingResult) errors, req, res, model);
        
        assertTrue(activityLogJson.contains("just now"));
        assertTrue(activityLogJson.contains("activityMessage\": \"Test activity message"));
        assertTrue(activityLogJson.contains("userName\": \"Test"));
    }
    
    @Test
    public void getActivityLogTest_1MinuteAgo() {
        
        WebSecurityForm<ActivityLogFilterForm> secForm = new WebSecurityForm<ActivityLogFilterForm>(null, createActivityLogFilterForm(keyMapping));
        //secForm.setWrappedObject(activityLogFilterForm);
        secForm.setInitToken("testSession");
        secForm.setPostToken("testSession");
        Errors errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        
        controller.setActivityLogService(new ActivityLogServiceImpl() {
            @Override
            public List<ActivityLog> findActivityLogWithCriteria(Calendar start, Calendar end, List<Integer> userIds, List<Integer> logTypes,
                String order, String column, int page, int numOfRecord) {
                return createActivityLog("Login", userId, "Test", oneMinute);
            }
            
            @Override
            public ActivityType findActivityTypeByActivityId(int activityTypeId) {
                ActivityType activityType = new ActivityType();
                activityType.setMessagePropertiesKey("testKey");
                return activityType;
            }
        });
        
        String activityLogJson = controller.getActivityLog(secForm, (BindingResult) errors, req, res, model);
        //JOptionPane.showMessageDialog(null, activityLogJson);
        //JOptionPane.showMessageDialog(null, "indexOf = " + activityLogJson.indexOf("1 minute ago"));
        assertTrue(activityLogJson.contains("1 minute ago"));
        assertTrue(activityLogJson.contains("activityMessage\": \"Test activity message"));
        assertTrue(activityLogJson.contains("userName\": \"Test"));
        
    }
    
    @Test
    public void getActivityLogTest_1HourAgo() {
        
        WebSecurityForm<ActivityLogFilterForm> secForm = new WebSecurityForm<ActivityLogFilterForm>(null, createActivityLogFilterForm(keyMapping));
        //secForm.setWrappedObject(activityLogFilterForm);
        secForm.setInitToken("testSession");
        secForm.setPostToken("testSession");
        Errors errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        
        controller.setActivityLogService(new ActivityLogServiceImpl() {
            @Override
            public List<ActivityLog> findActivityLogWithCriteria(Calendar start, Calendar end, List<Integer> userIds, List<Integer> logTypes,
                String order, String column, int page, int numOfRecord) {
                return createActivityLog("Login", userId, "Test", oneHour);
            }
            
            @Override
            public ActivityType findActivityTypeByActivityId(int activityTypeId) {
                ActivityType activityType = new ActivityType();
                activityType.setMessagePropertiesKey("testKey");
                return activityType;
            }
        });
        
        String activityLogJson = controller.getActivityLog(secForm, (BindingResult) errors, req, res, model);
        
        assertTrue(activityLogJson.contains("1 hour ago") || activityLogJson.contains("Yesterday at"));
        assertTrue(activityLogJson.contains("activityMessage\": \"Test activity message"));
        assertTrue(activityLogJson.contains("userName\": \"Test"));
    }
    
    @Test
    public void getActivityLogTest_1DayAgo() {
        
        WebSecurityForm<ActivityLogFilterForm> secForm = new WebSecurityForm<ActivityLogFilterForm>(null, createActivityLogFilterForm(keyMapping));
        //secForm.setWrappedObject(activityLogFilterForm);
        secForm.setInitToken("testSession");
        secForm.setPostToken("testSession");
        Errors errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        
        controller.setActivityLogService(new ActivityLogServiceImpl() {
            @Override
            public List<ActivityLog> findActivityLogWithCriteria(Calendar start, Calendar end, List<Integer> userIds, List<Integer> logTypes,
                String order, String column, int page, int numOfRecord) {
                return createActivityLog("Login", userId, "Test", oneDay);
            }
            
            @Override
            public ActivityType findActivityTypeByActivityId(int activityTypeId) {
                ActivityType activityType = new ActivityType();
                activityType.setMessagePropertiesKey("testKey");
                return activityType;
            }
        });
        
        String activityLogJson = controller.getActivityLog(secForm, (BindingResult) errors, req, res, model);
        
        //assertTrue(activityLogJson.contains("Activity Log<\\/strong> is invalid"));
        assertTrue(activityLogJson.contains("activityMessage\": \"Test activity message"));
        assertTrue(activityLogJson.contains("userName\": \"Test"));
    }
    
    private List<ActivityLog> createActivityLog(String logName, int userId, String userName, int seconds) {
        
        List<ActivityLog> list = new ArrayList<ActivityLog>();
        ActivityLog log = new ActivityLog();
        
        Calendar activityGmt = new GregorianCalendar();
        UserAccount userAccount = new UserAccount();
        ActivityType activityType = new ActivityType();
        ActivityType parentActivityType = new ActivityType();
        
        activityGmt.setTime(new Date());
        activityGmt.add(Calendar.SECOND, seconds);
        
        parentActivityType.setId(0);
        activityType.setName(logName);
        userAccount.setId(userId);
        userAccount.setUserName(userName);
        activityType.setParentActivityType(parentActivityType);
        
        log.setActivityGmt(activityGmt.getTime());
        log.setActivityType(activityType);
        log.setUserAccount(userAccount);
        
        list.add(log);
        
        return list;
    }
    
    private List<CustomerProperty> createCustomerProperty() {
        
        List<CustomerProperty> list = new ArrayList<CustomerProperty>();
        CustomerProperty property = new CustomerProperty();
        
        Integer id = 1;
        
        CustomerPropertyType propertyType = new CustomerPropertyType();
        propertyType.setId(1);
        propertyType.setName("TimeZone");
        
        Customer customer = new Customer();
        customer.setId(3);
        
        property.setId(id);
        property.setCustomerPropertyType(propertyType);
        property.setPropertyValue(WebCoreConstants.SERVER_TIMEZONE_GMT);
        property.setCustomer(customer);
        
        list.add(property);
        
        return list;
    }
    
    private UserAccount createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setCustomerTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT);
        user.setIsComplexPassword(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_SYSTEM_ACTIVITIES);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return userAccount;
    }
    
    private ActivityLogFilterForm createActivityLogFilterForm(RandomKeyMapping keyMapping) {
        ActivityLogFilterForm activityLogFilterForm = new ActivityLogFilterForm();
        
        UserAccount user = new UserAccount();
        Customer customer = new Customer();
        customer.setId(2);
        user.setId(3);
        user.setCustomer(customer);
        
        ActivityType logType = new ActivityType();
        logType.setId(101);
        logType.setName("User Login");
        
        //		logTypes.add(keyMapping.getRandomString(logType, WebCoreConstants.ID_LOOK_UP_NAME));
        //		userIds.add(keyMapping.getRandomString(user, WebCoreConstants.ID_LOOK_UP_NAME));
        
        activityLogFilterForm.setSelectedDateRange("Last 24 Hours");
        activityLogFilterForm.setSelectedLogTypes(keyMapping.getRandomString(logType, WebCoreConstants.ID_LOOK_UP_NAME));
        activityLogFilterForm.setSelectedUserAccounts(keyMapping.getRandomString(user, WebCoreConstants.ID_LOOK_UP_NAME));
        
        activityLogFilterForm.setColumn("date");
        activityLogFilterForm.setOrder("ASC");
        
        activityLogFilterForm.setPage("0");
        activityLogFilterForm.setNumOfRecord("30");
        
        return activityLogFilterForm;
    }
}
