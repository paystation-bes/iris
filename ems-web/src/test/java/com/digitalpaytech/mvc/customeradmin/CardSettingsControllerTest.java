package com.digitalpaytech.mvc.customeradmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.AuthorizationType;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerPropertyType;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.mvc.customeradmin.support.CardSettingsEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.CustomerCardTypeServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.CustomerAdminServiceTestImpl;
import com.digitalpaytech.util.CustomerAdminUtil;
import com.digitalpaytech.util.KeyManager;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.customeradmin.CardSettingsValidator;

public class CardSettingsControllerTest {
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private ModelMap model;
    private UserAccount userAccount;
    private boolean saveCcOfflineRetry;
    private boolean saveMaxOfflineRetry;
    
    @Test
    public void getCardSettingsAndTypesList() {
        CardSettingsController controller = new CardSettingsController();
        controller.setCustomerAdminService(new CustomerAdminServiceTestImpl() {
            @Override
            public List<CustomerPropertyType> getCustomerPropertyTypes() {
                return createCustomerPropertyTypes(true);
            }
        });
        controller.setCustomerCardTypeService(new CustomerCardTypeServiceImpl() {
            @Override
            public List<CustomerCardType> findByCustomerIdIsLocked(Integer customerId, boolean isLocked) {
                return createCustomerCardTypes();
            }
        });
        
        userAccount.getCustomer().setCustomerProperties(createCustomerProperties(true));
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            @Override
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        String path = controller.getCardSettingsAndTypesList(request, response, model);
        assertEquals("/settings/cardSettings/settings", path);
        
        //-----------------------------------
        
        controller.setCustomerAdminService(new CustomerAdminServiceTestImpl() {
            @Override
            public List<CustomerPropertyType> getCustomerPropertyTypes() {
                return createCustomerPropertyTypes(false);
            }
        });
        userAccount.getCustomer().setCustomerProperties(createCustomerProperties(false));
        try {
            controller.getCardSettingsAndTypesList(request, response, model);
            
            // Should NOT be here
            fail("it should throw a RuntimeException");
        } catch (RuntimeException er) {
            // All good, it should throw "RuntimeException: customerPropertyTypeId: 4 doesn't exist in customerAdminService.getCustomerPropertyTypes, size: 3"
        }
    }
    
    @Test
    public void saveCardSettings() {
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        CardSettingsValidator cardSettingsValidator = new CardSettingsValidator() {
            @Override
            public void validate(WebSecurityForm<CardSettingsEditForm> command, Errors errors, RandomKeyMapping keyMapping) {
                
            }
        };
        CardSettingsController controller = new CardSettingsController();
        controller.setCardSettingsValidator(cardSettingsValidator);
        controller.setCustomerAdminService(new CustomerAdminServiceTestImpl() {
            @Override
            public void saveOrUpdateCustomerProperty(CustomerProperty customerProperty) {
                if (customerProperty.getId() == 4 && customerProperty.getPropertyValue().equals("5")) {
                    saveCcOfflineRetry = true;
                } else if (customerProperty.getId() == 5 && customerProperty.getPropertyValue().equals("10")) {
                    saveMaxOfflineRetry = true;
                }
            }
        });
        CardSettingsEditForm form = new CardSettingsEditForm();
        form.setCreditCardOfflineRetry("5");
        form.setMaxOfflineRetry("10");
        WebSecurityForm<CardSettingsEditForm> webSecurityForm = new WebSecurityForm<CardSettingsEditForm>(null, form);
        BindingResult bindingResult = new BeanPropertyBindingResult(form, "cardSettingsEditForm");
        
        CustomerAdminUtil.setCustomerPropertyTypes(createCustomerPropertyTypes(true));
        userAccount.getCustomer().setCustomerProperties(createCustomerProperties(true));
        
        String result = controller.saveCardSettings(webSecurityForm, bindingResult, request, response, new ModelMap());
        if (result == null || result.isEmpty() || result.equals("true")) {
            fail();
        }
    }
    
    @Test
    public void saveCardType() {
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        CustomerAdminServiceTestImpl serv = new CustomerAdminServiceTestImpl() {
            @Override
            public List<CustomerPropertyType> getCustomerPropertyTypes() {
                return createCustomerPropertyTypes(true);
            }
            
            @Override
            public void saveOrUpdateCustomerCardType(CustomerCardType customerCardType) {
                // Do nothing
            }
        };
        List<CardType> list = new ArrayList<CardType>();
        CardType na = new CardType();
        na.setId(0);
        na.setName("N/A");
        list.add(na);
        CardType cc = new CardType();
        cc.setId(1);
        cc.setName("Credit Card");
        list.add(cc);
        CardType sc = new CardType();
        sc.setId(2);
        sc.setName("Smart Card");
        list.add(sc);
        CardType vc = new CardType();
        vc.setId(3);
        vc.setName("Value Card");
        list.add(vc);
        serv.setCardTypes(list);
        
        CardSettingsController controller = new CardSettingsController();
        controller.setCustomerAdminService(serv);
        
        userAccount.getCustomer().setCustomerProperties(createCustomerProperties(true));
        
        CardSettingsValidator cardSettingsValidator = new CardSettingsValidator() {
            @Override
            public void validate(WebSecurityForm<CardSettingsEditForm> command, Errors errors) {
            }
            
            @Override
            public void validateAuthorizationType(WebSecurityForm<CardSettingsEditForm> webSecurityForm, Errors errors,
                                                  List<AuthorizationType> authorizationTypes, RandomKeyMapping keyMapping) {
                
            }
            
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid.posttoken";
            }
        };
        controller.setCardSettingsValidator(cardSettingsValidator);
        
        EntityServiceImpl es = new EntityServiceImpl() {
            @Override
            public Object merge(Object entity) {
                return userAccount;
            }
            
            @Override
            public <T> List<T> loadAll(Class<T> entityClass) {
                AuthorizationType authType0 = new AuthorizationType();
                authType0.setId(0);
                authType0.setName("None");
                
                AuthorizationType authType1 = new AuthorizationType();
                authType1.setId(1);
                authType1.setName("Internal List");
                
                AuthorizationType authType2 = new AuthorizationType();
                authType2.setId(2);
                authType2.setName("External Server");
                
                List list = new ArrayList();
                list.add(authType0);
                list.add(authType1);
                list.add(authType2);
                return list;
            }
        };
        controller.setEntityService(es);
        
        // Saves existing record.
        CardSettingsEditForm form = new CardSettingsEditForm();
        form.setAuthorizationMethod(getRandomKeyMapping(2)); // External Server
        form.setCardTypeName("Test CardType");
        form.setTrack2Pattern("0000000");
        form.setCustomerCardTypeRandomId(getRandomKeyMapping(2));
        WebSecurityForm<CardSettingsEditForm> webSecurityForm = new WebSecurityForm<CardSettingsEditForm>(null, form);
        BindingResult bindingResult = new BeanPropertyBindingResult(form, "cardSettingsEditForm");
        
        CustomerAdminUtil.setCustomerPropertyTypes(createCustomerPropertyTypes(true));
        userAccount.getCustomer().setCustomerProperties(createCustomerProperties(true));
        
        String result = controller.saveCardType(webSecurityForm, bindingResult, request, response, new ModelMap());
        if (result == null || result.isEmpty() || result.equals("true")) {
            fail();
        }
        
        // Saves a new record.
        form.setCustomerCardTypeRandomId(null);
        result = controller.saveCardType(webSecurityForm, bindingResult, request, response, new ModelMap());
        if (result == null || result.isEmpty() || result.equals("true")) {
            fail();
        }
    }
    
    @Test
    public void deleteCardType() {
        CardSettingsController controller = new CardSettingsController();
        controller.setCustomerAdminService(new CustomerAdminServiceTestImpl() {
            @Override
            public List<CustomerPropertyType> getCustomerPropertyTypes() {
                return createCustomerPropertyTypes(true);
            }
        });
        controller.setCustomerCardTypeService(new CustomerCardTypeServiceImpl() {
            @Override
            public List<CustomerCardType> findByCustomerIdIsLocked(Integer customerId, boolean isLocked) {
                return createCustomerCardTypes();
            }
        });
        userAccount.getCustomer().setCustomerProperties(createCustomerProperties(true));
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        CardSettingsValidator cardSettingsValidator = new CardSettingsValidator() {
            @Override
            public void validate(WebSecurityForm<CardSettingsEditForm> command, Errors errors) {
                
            }
        };
        
        controller.setCardSettingsValidator(cardSettingsValidator);
        
        String randomId = getRandomKeyMapping(111);
        request.addParameter("id", randomId);
        
        String result = controller.deleteCardType(request, response, model);
        assertEquals("false", result);
    }
    
    @Before
    public void setUp() {
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        session = new MockHttpSession();
        this.request.setSession(session);
        this.model = new ModelMap();
        
        userAccount = new UserAccount();
        userAccount.setId(2);
        Customer customer = new Customer();
        customer.setId(3);
        userAccount.setCustomer(customer);
        
        Set<UserRole> roles = new HashSet<UserRole>();
        UserRole role = new UserRole();
        Role r = new Role();
        Set<RolePermission> rps = new HashSet<RolePermission>();
        
        RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
        rp1.setPermission(permission1);
        
        RolePermission rp2 = new RolePermission();
        rp2.setId(2);
        Permission permission2 = new Permission();
        permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
        rp2.setPermission(permission2);
        
        RolePermission rp3 = new RolePermission();
        rp3.setId(3);
        Permission permission3 = new Permission();
        permission3.setId(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS);
        rp3.setPermission(permission3);
        
        rps.add(rp1);
        rps.add(rp2);
        rps.add(rp3);
        
        r.setId(3);
        r.setRolePermissions(rps);
        
        role.setId(3);
        role.setRole(r);
        roles.add(role);
        userAccount.setUserRoles(roles);
        
        createAuthentication(userAccount);
        
        createSessionAttributes();
    }
    
    private void createSessionAttributes() {
        RandomKeyMapping rm = (new KeyManager()).createKeyMapping();
        //session.setAttribute(WebCoreConstants.RANDOM_KEY_MAPPING, rm);
        SessionTool sessionTool = SessionTool.getInstance(request);
        sessionTool.setAttribute(WebCoreConstants.RANDOM_KEY_MAPPING, rm);
        
        AuthorizationType type = new AuthorizationType();
        ArrayList<AuthorizationType> list = new ArrayList<AuthorizationType>();
        list.add(type);
        
        sessionTool.setAttribute("authTypes", list);
        //TODO Remove SessionToken
        //		sessionTool.setAttribute(session, WebSecurityConstants.SESSION_TOKEN, "abc", true);
    }
    
    private String getRandomKeyMapping(Integer id) {
        
        Customer cust = new Customer();
        cust.setId(id);
        String key = SessionTool.getInstance(request).getKeyMapping().getRandomString(cust, "id");
        return key;
    }
    
    private Set<CustomerProperty> createCustomerProperties(boolean hasCardSettings) {
        Set<CustomerProperty> set = new HashSet<CustomerProperty>();
        Iterator<CustomerPropertyType> iter = createCustomerPropertyTypes(hasCardSettings).iterator();
        while (iter.hasNext()) {
            CustomerPropertyType type = iter.next();
            CustomerProperty cp = new CustomerProperty();
            cp.setCustomerPropertyType(type);
            if (type.getId() == 1) {
                cp.setId(1);
                cp.setPropertyValue("US/Pacific");
            } else if (type.getId() == 2) {
                cp.setId(2);
                cp.setPropertyValue("1");
            } else if (type.getId() == 3) {
                cp.setId(3);
                cp.setPropertyValue("99");
            } else if (type.getId() == 4) {
                cp.setId(4);
                cp.setPropertyValue("3");
            } else if (type.getId() == 5) {
                cp.setId(5);
                cp.setPropertyValue("5");
            }
            set.add(cp);
        }
        return set;
    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        user.setIsMigrated(true);
        
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_SETUP_CREDIT_CARD_PROCESSING);
        permissionList.add(WebSecurityConstants.PERMISSION_CONFIGURE_CUSTOM_CARDS);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private List<CustomerPropertyType> createCustomerPropertyTypes(boolean hasCardSettings) {
        CustomerPropertyType type1 = new CustomerPropertyType();
        type1.setId(1);
        type1.setName("Timezone");
        
        CustomerPropertyType type2 = new CustomerPropertyType();
        type2.setId(2);
        type2.setName("Query Spaces By");
        
        CustomerPropertyType type3 = new CustomerPropertyType();
        type3.setId(3);
        type3.setName("Max User Accounts");
        
        CustomerPropertyType type4 = new CustomerPropertyType();
        type4.setId(4);
        type4.setName("Credit Card Offline Retry");
        
        CustomerPropertyType type5 = new CustomerPropertyType();
        type5.setId(5);
        type5.setName("Max Offline Retry");
        
        List<CustomerPropertyType> list = new ArrayList<CustomerPropertyType>();
        list.add(type2);
        list.add(type1);
        list.add(type3);
        if (hasCardSettings) {
            list.add(type5);
            list.add(type4);
        }
        return list;
    }
    
    private List<CustomerCardType> createCustomerCardTypes() {
        AuthorizationType authType0 = new AuthorizationType();
        authType0.setId(0);
        authType0.setName("None");
        CustomerCardType t0 = new CustomerCardType();
        t0.setAuthorizationType(authType0);
        t0.setId(1);
        
        AuthorizationType authType1 = new AuthorizationType();
        authType1.setId(1);
        authType1.setName("Internal List");
        CustomerCardType t1 = new CustomerCardType();
        t1.setAuthorizationType(authType1);
        t1.setId(2);
        
        AuthorizationType authType2 = new AuthorizationType();
        authType2.setId(2);
        authType2.setName("External Server");
        CustomerCardType t2 = new CustomerCardType();
        t2.setAuthorizationType(authType2);
        t2.setId(2);
        
        List<CustomerCardType> list = new ArrayList<CustomerCardType>();
        list.add(t0);
        list.add(t1);
        list.add(t2);
        return list;
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
        };
    }
}
