package com.digitalpaytech.mvc.customeradmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerBadCard;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerType;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.customeradmin.CustomerBadCardSearchCriteria;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.DuplicateObjectException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.mvc.customeradmin.support.BannedCardEditForm;
import com.digitalpaytech.mvc.customeradmin.support.BannedCardSearchForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.CardTypeService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.CustomerBadCardService;
import com.digitalpaytech.service.CustomerCardTypeService;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.CardRetryTransactionServiceImpl;
import com.digitalpaytech.service.impl.CardTypeServiceImpl;
import com.digitalpaytech.service.impl.CryptoServiceImpl;
import com.digitalpaytech.service.impl.CustomerBadCardServiceImpl;
import com.digitalpaytech.service.impl.CustomerCardTypeServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.CustomerAdminServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObject;
import com.digitalpaytech.validation.CardNumberValidator;
import com.digitalpaytech.validation.customeradmin.CardManagementEditValidator;
import com.digitalpaytech.validation.customeradmin.CardManagementSearchValidator;

/**
 * 
 * 
 * @author Brian Kim
 * 
 */
public class CardManagementControllerTest {
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private ModelMap model;
    private UserAccount userAccount;
    private RandomKeyMapping keyMapping;
    
    @Before
    public void setUp() throws Exception {
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        session = new MockHttpSession();
        this.request.setSession(session);
        this.model = new ModelMap();
        
        userAccount = new UserAccount();
        userAccount.setId(3);
        Set<UserRole> roles = new HashSet<UserRole>();
        UserRole role = new UserRole();
        Role r = new Role();
        Set<RolePermission> rps = new HashSet<RolePermission>();
        
        RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_VIEW_BANNED_CARDS);
        rp1.setPermission(permission1);
        
        RolePermission rp2 = new RolePermission();
        rp2.setId(2);
        Permission permission2 = new Permission();
        permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_BANNED_CARDS);
        rp2.setPermission(permission2);
        
        rps.add(rp1);
        rps.add(rp2);
        
        r.setId(3);
        r.setRolePermissions(rps);
        
        role.setId(3);
        role.setRole(r);
        roles.add(role);
        userAccount.setUserRoles(roles);
        
        Customer customer = new Customer();
        customer.setId(3);
        CustomerType customerType = new CustomerType();
        customerType.setId(3);
        customer.setCustomerType(customerType);
        customer.setIsMigrated(true);
        userAccount.setCustomer(customer);
        createAuthentication(userAccount);
        
        keyMapping = SessionTool.getInstance(request).getKeyMapping();
    }
    
    @After
    public void tearDown() throws Exception {
        request = null;
        response = null;
        session = null;
        model = null;
        userAccount = null;
    }
    
    @Test
    public void test_showBannedCardFilter() throws CryptoException {
        CardManagementController controller = new CardManagementController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        controller.setCustomerBadCardService(createCustomerBadCardService());
        controller.setCustomerCardTypeService(createCustomerCardTypeService());
        
        String result = controller.showBannedCardFilter(request, response, model, controller.initSearchForm(request),
                                                        controller.initEditForm(request), controller.initUploadForm(request));
        assertNotNull(result);
        assertEquals(result, "/cardManagement/index");
    }
    
    @Test
    public void test_showBannedCardFilter_permission_fail() throws CryptoException {
        createFailedAuthentication();
        
        CardManagementController controller = new CardManagementController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        String result = controller.showBannedCardFilter(request, response, model, controller.initSearchForm(request),
                                                        controller.initEditForm(request), controller.initUploadForm(request));
        assertEquals(result, WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS);
    }
    
    @Test
    public void test_searchBannedCard() throws CryptoException, InvalidDataException {
        CardManagementController controller = new CardManagementController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        controller.setCardManagementSearchValidator(createSuccessCardManagementSearchValidator());
        controller.setCustomerBadCardService(createCustomerBadCardService());
        controller.setCustomerCardTypeService(createCustomerCardTypeService());
        
        WidgetMetricsHelper.registerComponents();
        CustomerCardType cct = new CustomerCardType();
        cct.setId(1);
        CardType type = new CardType();
        type.setId(10);
        type.setName("Test Card");
        cct.setCardType(type);
        BannedCardSearchForm bannedCardSearchForm = new BannedCardSearchForm();
        bannedCardSearchForm.setCustomerCardType(cct);
        bannedCardSearchForm.setFilterCardNumber("2833");
        WebObject obj = keyMapping.hideProperty(cct, "id");
        bannedCardSearchForm.setFilterCardTypeId(obj.getPrimaryKey().toString());
        WebSecurityForm<BannedCardSearchForm> webSecurityForm = new WebSecurityForm<BannedCardSearchForm>(null, bannedCardSearchForm);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_SEARCH);
        
        CardManagementSearchValidator validator = new CardManagementSearchValidator() {
            @Override
            public String getMessage(String code) {
                return "";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        
        validator.setCardNumberValidator(createCardNumberValidator());
        
        controller.setCardManagementSearchValidator(validator);
        controller.setCustomerAdminService(new CustomerAdminServiceTestImpl() {
            @Override
            public CustomerProperty getCustomerPropertyByCustomerIdAndCustomerPropertyType(final int customerId, final int customerPropertyType) {
                final CustomerProperty custProp = new CustomerProperty();
                custProp.setPropertyValue("30");
                return custProp;
            }
        });
        controller.setCardRetryTransactionService(new CardRetryTransactionServiceImpl() {
            @Override
            public List<String> findBadCardHashByPointOfSaleIdsDateAndNumRetries(List<Integer> pointOfSaleIds, int yymm,
                int creditCardOfflineRetryLimit) {
                final List<String> list = new ArrayList<>(1);
                list.add("BAD_CARD_HASH");
                return list;
            }
        });
        
        String res = controller.searchBannedCard(webSecurityForm, new BeanPropertyBindingResult(webSecurityForm, "bannedCardSearchForm"), request,
                                                 response);
        
        assertNotNull(res);
    }
    
    @Test
    public void test_searchBannedCard_permission_fail() throws CryptoException, InvalidDataException {
        createFailedAuthentication();
        
        CardManagementController controller = new CardManagementController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        WidgetMetricsHelper.registerComponents();
        
        WebSecurityForm<BannedCardSearchForm> bannedCardSearchForm = new WebSecurityForm<BannedCardSearchForm>(null, new BannedCardSearchForm());
        bannedCardSearchForm.setActionFlag(WebSecurityConstants.ACTION_SEARCH);
        
        String res = controller.searchBannedCard(bannedCardSearchForm, new BeanPropertyBindingResult(bannedCardSearchForm, "bannedCardSearchForm"),
                                                 request, response);
        
        assertEquals(res, WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS);
    }
    
    /*
     * Search shall not fail !
     * @Test
     * public void test_searchBannedCard_validation_fail() throws CryptoException {
     * CardManagementController controller = new CardManagementController();
     * WebSecurityUtil util = new WebSecurityUtil();
     * util.setUserAccountService(createStaticUserAccountService());
     * util.setEntityService(createEntityService());
     * controller.setCardManagementSearchValidator(createFailedCardManagementSearchValidator());
     * controller.setCustomerBadCardService(createCustomerBadCardService());
     * WebSecurityForm bannedCardSearchForm = new WebSecurityForm(null, new BannedCardSearchForm());
     * bannedCardSearchForm.setActionFlag(WebSecurityConstants.ACTION_SEARCH);
     * String res = controller.searchBannedCard(bannedCardSearchForm,
     * new BeanPropertyBindingResult(bannedCardSearchForm,
     * "bannedCardSearchForm"), request, response);
     * assertNotNull(res);
     * assertTrue(res.contains("errorStatus"));
     * }
     */
    
    @Test
    public void test_viewBannedCardDetail() {
        CardManagementController controller = new CardManagementController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        WebObject obj = getCustomerBadCard();
        request.setParameter("customerBadCardID", obj.getPrimaryKey().toString());
        controller.setCustomerBadCardService(createCustomerBadCardService());
        
        String result = controller.viewBannedCardDetail(request, response, model);
        assertNotNull(result);
    }
    
    @Test
    public void test_viewBannedCardDetail_permission_fail() {
        Set<UserRole> roles = new HashSet<UserRole>();
        UserRole role = new UserRole();
        Role r = new Role();
        
        Set<RolePermission> rps = new HashSet<RolePermission>();
        RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        Permission permission1 = new Permission();
        permission1.setId(23);
        rp1.setPermission(permission1);
        
        rps.add(rp1);
        
        r.setId(2);
        r.setRolePermissions(rps);
        role.setId(2);
        role.setRole(r);
        roles.add(role);
        userAccount.setUserRoles(roles);
        
        CardManagementController controller = new CardManagementController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        String result = controller.viewBannedCardDetail(request, response, model);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_viewBannedCardDetail_invalid_customerBadCardID() {
        CardManagementController controller = new CardManagementController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        request.setParameter("customerBadCardID", "34545454444");
        controller.setCustomerBadCardService(createCustomerBadCardService());
        
        String result = controller.viewBannedCardDetail(request, response, model);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_getBannedCardForm() {
        CardManagementController controller = new CardManagementController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        String result = controller.getBannedCardForm(request, response, model);
        assertNotNull(result);
        assertEquals(result, "/cardManagement/bannedCardForm");
    }
    
    @Test
    public void test_getBannedCardForm_permission_fail() {
        createFailedAuthentication();
        
        CardManagementController controller = new CardManagementController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        String result = controller.getBannedCardForm(request, response, model);
        assertEquals(result, WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS);
    }
    
    @Test
    public void test_saveBannedCard() throws CryptoException {
        CardManagementController controller = new CardManagementController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        controller.setCardManagementEditValidator(createSuccessCardManagementEditValidator());
        controller.setCustomerCardTypeService(createCustomerCardTypeService());
        controller.setCryptoService(createCryptoService());
        controller.setCustomerBadCardService(createCustomerBadCardService());
        
        BannedCardEditForm form = new BannedCardEditForm();
        form.setCardNumber("123435687678");
        form.setCardTypeId(1);
        form.setCardExpiry("0516");
        
        WebSecurityForm<BannedCardEditForm> bannedCardEditForm = new WebSecurityForm<BannedCardEditForm>(null, form);
        bannedCardEditForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        String res = controller.saveBannedCard(bannedCardEditForm, new BeanPropertyBindingResult(bannedCardEditForm, "bannedCardEditForm"), request,
                                               response);
        
        assertNotNull(res);
    }
    
    @Test
    public void test_saveBannedCard_permission_fail() throws CryptoException {
        createFailedAuthentication();
        
        CardManagementController controller = new CardManagementController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        BannedCardEditForm form = new BannedCardEditForm();
        form.setCardNumber("123435687678");
        form.setCardTypeId(1);
        form.setCardExpiry("0516");
        
        WebSecurityForm<BannedCardEditForm> bannedCardEditForm = new WebSecurityForm<BannedCardEditForm>(null, form);
        bannedCardEditForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        String res = controller.saveBannedCard(bannedCardEditForm, new BeanPropertyBindingResult(bannedCardEditForm, "bannedCardEditForm"), request,
                                               response);
        
        assertEquals(res, WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS);
    }
    
    @Test
    public void test_saveBannedCard_validation_fail() throws CryptoException {
        CardManagementController controller = new CardManagementController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        controller.setCardManagementEditValidator(createFailedCardManagementEditValidator());
        
        BannedCardEditForm form = new BannedCardEditForm();
        form.setCardNumber("123435687678");
        form.setCardTypeId(1);
        form.setCardExpiry("0516");
        
        WebSecurityForm<BannedCardEditForm> bannedCardEditForm = new WebSecurityForm<BannedCardEditForm>(null, form);
        bannedCardEditForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        String res = controller.saveBannedCard(bannedCardEditForm, new BeanPropertyBindingResult(bannedCardEditForm, "bannedCardEditForm"), request,
                                               response);
        
        assertNotNull(res);
        assertTrue(res.contains("errorStatus"));
    }
    
    @Test
    public void test_deleteBannedCard() throws CryptoException {
        CardManagementController controller = new CardManagementController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        WebObject obj = getCustomerBadCard();
        request.setParameter("customerBadCardID", obj.getPrimaryKey().toString());
        controller.setCustomerBadCardService(createCustomerBadCardService());
        
        String result = controller.deleteBannedCard(request, response, model);
        
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_TRUE);
    }
    
    @Test
    public void test_deleteBannedCard_permission_fail() throws CryptoException {
        createFailedAuthentication();
        
        CardManagementController controller = new CardManagementController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        WebObject obj = getCustomerBadCard();
        request.setParameter("customerBadCardID", obj.getPrimaryKey().toString());
        controller.setCustomerBadCardService(createCustomerBadCardService());
        
        String result = controller.deleteBannedCard(request, response, model);
        assertEquals(result, WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS);
    }
    
    @Test
    public void test_deleteBannedCard_invalid_CustomerBadCardId() throws CryptoException {
        CardManagementController controller = new CardManagementController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        request.setParameter("customerBadCardID", "4543523454325");
        controller.setCustomerBadCardService(createCustomerBadCardService());
        
        String result = controller.deleteBannedCard(request, response, model);
        
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_getBannedCardImportForm() {
        CardManagementController controller = new CardManagementController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        String result = controller.getBannedCardImportForm(request, response, model);
        assertEquals(result, "/cardManagement/importBannedCardForm");
    }
    
    @Test
    public void test_getBannedCardImportForm_permission_fail() {
        createFailedAuthentication();
        
        CardManagementController controller = new CardManagementController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        String result = controller.getBannedCardImportForm(request, response, model);
        
        assertEquals(result, WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS);
    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Child Admin"));
        WebUser user = new WebUser(3, 3, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        user.setCustomerTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT);
        user.setIsMigrated(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_BANNED_CARDS);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_BANNED_CARDS);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private void createFailedAuthentication() {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        user.setIsMigrated(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(2);
        permissionList.add(3);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private EntityService createEntityService() {
        return new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        };
    }
    
    private CardTypeService createCardTypeService() {
        return new CardTypeServiceImpl() {
            public List<CardType> loadAll() {
                List<CardType> types = new ArrayList<CardType>();
                types.add(new CardType(0, "N/A", new Date(), 1));
                types.add(new CardType(1, "Credit Card", new Date(), 1));
                types.add(new CardType(2, "Smart Card", new Date(), 1));
                types.add(new CardType(3, "Value Card", new Date(), 1));
                return types;
            }
        };
    }
    
    private CustomerBadCardService createCustomerBadCardService() {
        return new CustomerBadCardServiceImpl() {
            public Collection<CustomerBadCard> getCustomerBadCards(Integer customerId, String cardNumber, Integer cardTypeId) {
                Collection<CustomerBadCard> result = new ArrayList<CustomerBadCard>();
                return result;
            }
            
            public CustomerBadCard findCustomerBadCardById(Integer id, boolean isEvict) {
                CustomerBadCard result = new CustomerBadCard();
                CustomerCardType cardType = new CustomerCardType();
                CardType type = new CardType();
                cardType.setId(2);
                type.setId(1);
                type.setName("Credit Card");
                cardType.setCardType(type);
                result.setCustomerCardType(cardType);
                result.setId(12);
                result.setCardNumberOrHash("3423493434");
                return result;
            }
            
            public void createCustomerBadCard(CustomerBadCard newCustomerBadCard) throws DuplicateObjectException {
                
            }
            
            public void deleteCustomerBadCard(CustomerBadCard customerBadCard) {
                
            }
            
            @Override
            public Collection<CustomerBadCard> findCustomerBadCards(CustomerBadCardSearchCriteria criteria) {
                Collection<CustomerBadCard> result = new ArrayList<CustomerBadCard>();
                return result;
            }
            
        };
    }
    
    private CardManagementSearchValidator createSuccessCardManagementSearchValidator() {
        return new CardManagementSearchValidator() {
            @Override
            public void validate(WebSecurityForm<BannedCardSearchForm> command, Errors errors) {
                
            }
        };
    }
    
    private CardManagementSearchValidator createFailedCardManagementSearchValidator() {
        return new CardManagementSearchValidator() {
            @Override
            public void validate(WebSecurityForm<BannedCardSearchForm> webSecurityForm, Errors errors) {
                webSecurityForm.removeAllErrorStatus();
                webSecurityForm.addErrorStatus(new FormErrorStatus("filterCardType", "error.common.invalid"));
            }
        };
    }
    
    private CardManagementEditValidator createSuccessCardManagementEditValidator() {
        return new CardManagementEditValidator() {
            @Override
            public void validate(WebSecurityForm<BannedCardEditForm> webSecurityForm, Errors errors) {
                
            }
        };
    }
    
    private CardManagementEditValidator createFailedCardManagementEditValidator() {
        return new CardManagementEditValidator() {
            @Override
            public void validate(WebSecurityForm<BannedCardEditForm> webSecurityForm, Errors errors) {
                webSecurityForm.removeAllErrorStatus();
                webSecurityForm.addErrorStatus(new FormErrorStatus("cardNumber", "error.common.invalid"));
            }
        };
    }
    
    private WebObject getCustomerBadCard() {
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        CustomerBadCard card = new CustomerBadCard();
        card.setId(12);
        card.setCardNumberOrHash("3423493434");
        return (WebObject) keyMapping.hideProperty(card, "id");
    }
    
    private CustomerCardTypeService createCustomerCardTypeService() {
        return new CustomerCardTypeServiceImpl() {
            public CustomerCardType findCustomerCardTypeForBadCard(Integer customerId, int cardTypeId) {
                CustomerCardType result = new CustomerCardType();
                
                return result;
            }
            
            @Override
            public CustomerCardType getCustomerCardTypeById(int id) {
                CustomerCardType result = new CustomerCardType();
                
                return result;
            }
            
            @Override
            public CustomerCardType getCardType(int id) {
                CustomerCardType result = new CustomerCardType();
                result.setId(id);
                return result;
            }
            
            @Override
            public List<FilterDTO> getBannableCardTypeFilters(List<FilterDTO> result, Integer customerId, RandomKeyMapping keyMapping) {
                return result;
            }
            
            @Override
            public List<FilterDTO> getValueCardTypeFilters(List<FilterDTO> result, Integer customerId, boolean showDeleted,
                RandomKeyMapping keyMapping) {
                return result;
            }
        };
    }
    
    private CryptoService createCryptoService() {
        return new CryptoServiceImpl() {
            public String getSha1Hash(String rawString, int hashType) throws CryptoException {
                return "HashedStr";
            }
            
            public String encryptData(int purpose, String data) throws CryptoException {
                return "EncryptedStr";
            }
        };
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
        };
    }
    
    private MerchantAccountService createMerchantAccountService() {
        return new MerchantAccountServiceImpl() {
            
            @Override
            public List<FilterDTO> getRefundableMerchantAccountFiltersByCustomerId(List<FilterDTO> result, int customerId,
                RandomKeyMapping keyMapping) {
                return result;
            }
            
        };
    }
    
    private CardNumberValidator<BannedCardSearchForm> createCardNumberValidator() {
        CardNumberValidator<BannedCardSearchForm> result = new CardNumberValidator<BannedCardSearchForm>() {
            @Override
            public String getMessage(String code) {
                return "";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
        };
        
        result.setCustomerCardTypeService(createTrueCustomerCardTypeService());
        
        return result;
    }
    
    private CustomerCardTypeService createTrueCustomerCardTypeService() {
        return new CustomerCardTypeServiceImpl() {
            public boolean isCreditCardData(String track2OrAcctNum) {
                return true;
            }
        };
    }
}
