package com.digitalpaytech.mvc.customeradmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerEmail;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PaystationSetting;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.ReportDefinition;
import com.digitalpaytech.domain.ReportEmail;
import com.digitalpaytech.domain.ReportFilterValue;
import com.digitalpaytech.domain.ReportHistory;
import com.digitalpaytech.domain.ReportLocationValue;
import com.digitalpaytech.domain.ReportQueue;
import com.digitalpaytech.domain.ReportRepeatType;
import com.digitalpaytech.domain.ReportRepository;
import com.digitalpaytech.domain.ReportStatusType;
import com.digitalpaytech.domain.ReportType;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.SubscriptionType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.dto.ReportSearchCriteria;
import com.digitalpaytech.dto.ReportUIFilterType;
import com.digitalpaytech.dto.customeradmin.LocationTree;
import com.digitalpaytech.dto.customeradmin.ReportQueueInfo.QueueInfo;
import com.digitalpaytech.dto.customeradmin.ReportRepositoryInfo.RepositoryInfo;
import com.digitalpaytech.mvc.customeradmin.support.ReportEditForm;
import com.digitalpaytech.mvc.support.CommonControllerHelper;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.mvc.support.impl.CommonControllerHelperImpl;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.CustomerCardTypeServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.LocationServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.ReportDefinitionServiceImpl;
import com.digitalpaytech.service.impl.ReportHistoryServiceImpl;
import com.digitalpaytech.service.impl.ReportQueueServiceImpl;
import com.digitalpaytech.service.impl.ReportRepositoryServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.CreditCardTypeServiceTestImpl;
import com.digitalpaytech.testing.services.impl.CustomerAdminServiceTestImpl;
import com.digitalpaytech.testing.services.impl.CustomerServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.impl.ReportingUtilImpl;
import com.digitalpaytech.util.support.RelativeDateTime;
import com.digitalpaytech.validation.customeradmin.ReportValidator;

public class CustomerAdminReportingControllerTest {
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private ModelMap model;
    private UserAccount userAccount;
    private CustomerAdminReportingController controller;
    
    private List<ReportDefinition> definitionList;
    private List<ReportQueue> queueList;
    private List<ReportHistory> historyList;
    private List<ReportRepository> repositoryList;
    
    private List<Location> locationList;
    private List<PointOfSale> posList;
    private List<ReportLocationValue> rlvList;
    private List<PaystationSetting> paystationSettingList;
    private List<Route> routeList;
    private List<FilterDTO> merchantAccountList;
    private List<FilterDTO> ccTypeList;
    private List<FilterDTO> custcTypeList;
    
    private List<ReportEmail> reportEmailList;
    private List<CustomerEmail> customerEmailList;
    
    @Before
    public void setUp() {
        
        this.controller = new CustomerAdminReportingController();
        
        WidgetMetricsHelper.registerComponents();
        
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        this.session = new MockHttpSession();
        this.request.setSession(this.session);
        this.model = new ModelMap();
        
        this.userAccount = new UserAccount();
        this.userAccount.setId(3);
        Set<UserRole> roles = new HashSet<UserRole>();
        UserRole role = new UserRole();
        Role r = new Role();
        Set<RolePermission> rps = new HashSet<RolePermission>();
        
        RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_MANAGE_REPORTS);
        rp1.setPermission(permission1);
        
        rps.add(rp1);
        
        r.setId(3);
        r.setRolePermissions(rps);
        
        role.setId(3);
        role.setRole(r);
        roles.add(role);
        this.userAccount.setUserRoles(roles);
        
        Customer customer = new Customer();
        customer.setId(3);
        
        Set<CustomerSubscription> subscriptions = new HashSet<CustomerSubscription>();
        
        CustomerSubscription cs = new CustomerSubscription();
        cs.setCustomer(customer);
        cs.setSubscriptionType(new SubscriptionType(WebCoreConstants.SUBSCRIPTION_TYPE_STANDARD_REPORT));
        subscriptions.add(cs);
        
        customer.setCustomerSubscriptions(subscriptions);
        
        this.userAccount.setCustomer(customer);
        createAuthentication(this.userAccount);
        
        this.definitionList = new ArrayList<ReportDefinition>();
        ReportDefinition def1 = new ReportDefinition();
        def1.setId(1);
        def1.setCustomer(customer);
        def1.setUserAccount(createStaticUserAccountService().findUserAccount(3));
        def1.setTitle("Title 1");
        Calendar inMinute35 = Calendar.getInstance();
        inMinute35.add(Calendar.MINUTE, 35);
        ReportStatusType statusd1 = new ReportStatusType();
        statusd1.setId(ReportingConstants.REPORT_STATUS_READY);
        def1.setReportStatusType(statusd1);
        def1.setScheduledToRunGmt(inMinute35.getTime());
        
        ReportDefinition def2 = new ReportDefinition();
        def2.setId(2);
        def2.setCustomer(customer);
        def2.setUserAccount(createStaticUserAccountService().findUserAccount(3));
        def2.setTitle("Title 2");
        Calendar inHour15 = Calendar.getInstance();
        inHour15.add(Calendar.HOUR_OF_DAY, 15);
        ReportStatusType statusd2 = new ReportStatusType();
        statusd2.setId(ReportingConstants.REPORT_STATUS_READY);
        def2.setReportStatusType(statusd2);
        def2.setScheduledToRunGmt(inHour15.getTime());
        
        ReportDefinition def3 = new ReportDefinition();
        def3.setId(3);
        def3.setCustomer(customer);
        def3.setUserAccount(createStaticUserAccountService().findUserAccount(3));
        def3.setTitle("Title 3");
        Calendar inHour35 = Calendar.getInstance();
        inHour35.add(Calendar.HOUR_OF_DAY, 35);
        ReportStatusType statusd3 = new ReportStatusType();
        statusd3.setId(ReportingConstants.REPORT_STATUS_READY);
        def3.setReportStatusType(statusd3);
        def3.setScheduledToRunGmt(inHour35.getTime());
        
        this.definitionList.add(def1);
        this.definitionList.add(def2);
        this.definitionList.add(def3);
        
        this.queueList = new ArrayList<ReportQueue>();
        ReportQueue queue1 = new ReportQueue();
        queue1.setId(1);
        queue1.setUserAccount(createStaticUserAccountService().findUserAccount(3));
        queue1.setReportDefinition(def1);
        ReportStatusType statusq1 = new ReportStatusType();
        statusq1.setId(ReportingConstants.REPORT_STATUS_SCHEDULED);
        queue1.setReportStatusType(statusq1);
        
        ReportQueue queue2 = new ReportQueue();
        queue2.setId(2);
        queue2.setUserAccount(createStaticUserAccountService().findUserAccount(3));
        queue2.setReportDefinition(def2);
        ReportStatusType statusq2 = new ReportStatusType();
        statusq2.setId(ReportingConstants.REPORT_STATUS_LAUNCHED);
        queue2.setReportStatusType(statusq2);
        
        this.queueList.add(queue1);
        this.queueList.add(queue2);
        
        this.historyList = new ArrayList<ReportHistory>();
        ReportHistory history1 = new ReportHistory();
        history1.setId(1);
        history1.setUserAccount(createStaticUserAccountService().findUserAccount(3));
        history1.setReportDefinition(def1);
        Calendar min5Ago = Calendar.getInstance();
        min5Ago.add(Calendar.MINUTE, -5);
        history1.setExecutionEndGmt(min5Ago.getTime());
        ReportStatusType statush1 = new ReportStatusType();
        statush1.setId(ReportingConstants.REPORT_STATUS_COMPLETED);
        history1.setReportStatusType(statush1);
        history1.setStatusExplanation(null);
        
        ReportHistory history2 = new ReportHistory();
        history2.setId(2);
        history2.setUserAccount(createStaticUserAccountService().findUserAccount(3));
        history2.setReportDefinition(def2);
        Calendar hour5Ago = Calendar.getInstance();
        hour5Ago.add(Calendar.HOUR_OF_DAY, -5);
        history2.setExecutionEndGmt(hour5Ago.getTime());
        ReportStatusType statush2 = new ReportStatusType();
        statush2.setId(ReportingConstants.REPORT_STATUS_FAILED);
        history2.setReportStatusType(statush2);
        history2.setStatusExplanation("FAILED");
        
        ReportHistory history3 = new ReportHistory();
        history3.setId(3);
        history3.setUserAccount(createStaticUserAccountService().findUserAccount(3));
        history3.setReportDefinition(def3);
        Calendar hour35Ago = Calendar.getInstance();
        hour35Ago.add(Calendar.HOUR_OF_DAY, -35);
        history3.setExecutionEndGmt(hour35Ago.getTime());
        ReportStatusType status3 = new ReportStatusType();
        status3.setId(ReportingConstants.REPORT_STATUS_CANCELLED);
        history3.setReportStatusType(status3);
        history3.setStatusExplanation("CANCELLED");
        
        this.historyList.add(history1);
        this.historyList.add(history2);
        this.historyList.add(history3);
        
        this.repositoryList = new ArrayList<ReportRepository>();
        ReportRepository repo1 = new ReportRepository();
        repo1.setId(1);
        repo1.setReportHistory(history1);
        repo1.setAddedGmt(new Date());
        repo1.setIsReadByUser(false);
        repo1.setIsSoonToBeDeleted(true);
        
        this.repositoryList.add(repo1);
        
        this.locationList = new ArrayList<Location>();
        Location pl1 = new Location();
        pl1.setId(4);
        pl1.setName("Parent Location 1");
        this.locationList.add(pl1);
        Location pl2 = new Location();
        pl2.setId(5);
        pl2.setName("Parent Location 2");
        this.locationList.add(pl2);
        Location l1 = new Location();
        l1.setId(1);
        l1.setName("Location 1");
        l1.setLocation(pl1);
        this.locationList.add(l1);
        Location l2 = new Location();
        l2.setId(2);
        l2.setName("Location 2");
        l2.setLocation(pl1);
        this.locationList.add(l2);
        Location l3 = new Location();
        l3.setId(3);
        l3.setName("Location 3");
        l3.setLocation(pl2);
        this.locationList.add(l3);
        Location pl3 = new Location();
        pl3.setId(6);
        pl3.setName("Parent Location 3");
        this.locationList.add(pl3);
        
        this.posList = new ArrayList<PointOfSale>();
        PointOfSale pos1 = new PointOfSale();
        pos1.setId(1);
        pos1.setName("POS 1 Location 1");
        pos1.setLocation(l1);
        this.posList.add(pos1);
        PointOfSale pos2 = new PointOfSale();
        pos2.setId(2);
        pos2.setName("POS 2 Location 1");
        pos2.setLocation(l1);
        this.posList.add(pos2);
        PointOfSale pos3 = new PointOfSale();
        pos3.setId(3);
        pos3.setName("POS 3 Location 1");
        pos3.setLocation(l1);
        this.posList.add(pos3);
        PointOfSale pos4 = new PointOfSale();
        pos4.setId(4);
        pos4.setName("POS 4 Location 1");
        pos4.setLocation(l1);
        this.posList.add(pos4);
        PointOfSale pos5 = new PointOfSale();
        pos5.setId(5);
        pos5.setName("POS 5 Location 2");
        pos5.setLocation(l2);
        this.posList.add(pos5);
        PointOfSale pos6 = new PointOfSale();
        pos6.setId(6);
        pos6.setName("POS 6 Location 2");
        pos6.setLocation(l2);
        this.posList.add(pos6);
        PointOfSale pos7 = new PointOfSale();
        pos7.setId(7);
        pos7.setName("POS 7 Location 3");
        pos7.setLocation(l3);
        this.posList.add(pos7);
        
        this.rlvList = new ArrayList<ReportLocationValue>();
        /*
         * ReportLocationValue rlv1 = new ReportLocationValue();
         * rlv1.setId(1);
         * rlv1.setObjectId(1);
         * rlv1.setObjectType(Location.class.getName());
         * this.rlvList.add(rlv1);
         * ReportLocationValue rlv2 = new ReportLocationValue();
         * rlv2.setId(2);
         * rlv2.setObjectId(1);
         * rlv2.setObjectType(PointOfSale.class.getName());
         * this.rlvList.add(rlv2);
         * ReportLocationValue rlv3 = new ReportLocationValue();
         * rlv3.setId(3);
         * rlv3.setObjectId(2);
         * rlv3.setObjectType(PointOfSale.class.getName());
         * this.rlvList.add(rlv3);
         */
        this.paystationSettingList = new ArrayList<PaystationSetting>();
        PaystationSetting ls1 = new PaystationSetting();
        ls1.setId(1);
        ls1.setName("PaystationSetting 1");
        this.paystationSettingList.add(ls1);
        PaystationSetting ls2 = new PaystationSetting();
        ls2.setId(2);
        ls2.setName("PaystationSetting 2");
        this.paystationSettingList.add(ls2);
        
        this.routeList = new ArrayList<Route>();
        
        this.merchantAccountList = new ArrayList<FilterDTO>();
        FilterDTO ma1 = new FilterDTO();
        ma1.setValue("1");
        ma1.setLabel("MerchantAccount 1");
        this.merchantAccountList.add(ma1);
        
        this.ccTypeList = new ArrayList<FilterDTO>();
        FilterDTO cct1 = new FilterDTO();
        cct1.setValue("1");
        cct1.setLabel("CreditCardType 1");
        this.ccTypeList.add(cct1);
        
        this.custcTypeList = new ArrayList<FilterDTO>();
        FilterDTO custct1 = new FilterDTO();
        custct1.setValue("1");
        custct1.setLabel("CreditCardType 1");
        this.custcTypeList.add(custct1);
        
        this.reportEmailList = new ArrayList<ReportEmail>();
        this.customerEmailList = new ArrayList<CustomerEmail>();
        
        this.controller.setReportDefinitionService(new ReportDefinitionServiceImpl() {
            @Override
            public List<ReportDefinition> findScheduledReportsByUserAccountId(int userAccountId) {
                List<ReportDefinition> localList = new ArrayList<ReportDefinition>();
                for (ReportDefinition def : definitionList) {
                    if (def.getReportStatusType().getId() == ReportingConstants.REPORT_STATUS_READY)
                        localList.add(def);
                }
                return localList;
            }
            
            @Override
            public ReportDefinition getReportDefinition(int id) {
                for (ReportDefinition definition : definitionList) {
                    if (definition.getId() == id)
                        return definition;
                }
                return null;
            }
            
            @Override
            public void updateReportDefinition(ReportDefinition definition) {
                for (int i = 0; i < definitionList.size(); i++) {
                    ReportDefinition localdefinition = definitionList.get(i);
                    if (localdefinition.getId().intValue() == definition.getId().intValue()) {
                        definitionList.set(i, definition);
                    }
                }
            }
            
            @Override
            public void saveOrUpdateReportDefinition(ReportDefinition reportDefinition, ReportDefinition originalReportDefinition,
                Collection<ReportLocationValue> reportLocationValueList, Set<ReportEmail> emailList) {
                if (originalReportDefinition != null) {
                    for (int i = 0; i < definitionList.size(); i++) {
                        ReportDefinition definition = definitionList.get(i);
                        if (definition.getId().intValue() == originalReportDefinition.getId().intValue())
                            definitionList.set(i, originalReportDefinition);
                    }
                }
                int defId = definitionList.size() + 1;
                reportDefinition.setId(defId);
                definitionList.add(reportDefinition);
                
                for (ReportLocationValue rlv : reportLocationValueList) {
                    rlv.setReportDefinition(reportDefinition);
                }
                rlvList.addAll(reportLocationValueList);
                
                for (ReportEmail reportEmail : emailList) {
                    CustomerEmail customerEmail = reportEmail.getCustomerEmail();
                    CustomerEmail existingEmail = null;
                    for (CustomerEmail oldFromList : customerEmailList) {
                        if (customerEmail.getCustomer().getId().intValue() == oldFromList.getCustomer().getId().intValue()
                            && customerEmail.getEmail().equals(oldFromList.getEmail())) {
                            existingEmail = oldFromList;
                            break;
                        }
                    }
                    if (existingEmail == null) {
                        customerEmail.setId(customerEmailList.size() + 1);
                        customerEmailList.add(customerEmail);
                    } else {
                        reportEmail.setCustomerEmail(existingEmail);
                    }
                    reportEmail.setReportDefinition(reportDefinition);
                    reportEmailList.add(reportEmail);
                }
            }
            
            @Override
            public List<String> findEmailsByReportDefinitionId(int reportDefinitionId) {
                ArrayList<String> result = new ArrayList<String>(reportEmailList.size());
                for (ReportEmail reportEmail : reportEmailList) {
                    result.add(reportEmail.getCustomerEmail().getEmail());
                }
                
                return result;
            }
        });
        
        this.controller.setReportQueueService(new ReportQueueServiceImpl() {
            @Override
            public List<ReportQueue> findQueuedReportsByUserAccountId(int userAccountId) {
                return queueList;
            }
            
            @Override
            public ReportQueue getReportQueue(int id) {
                for (ReportQueue queue : queueList) {
                    if (queue.getId() == id)
                        return queue;
                }
                return null;
            }
            
            @Override
            public boolean cancelReport(int id) {
                ReportQueue queue = getReportQueue(id);
                if (queue == null || queue.getReportStatusType().getId() == ReportingConstants.REPORT_STATUS_LAUNCHED)
                    return false;
                queueList.remove(queue);
                ReportHistory history4 = new ReportHistory();
                history4.setId(4);
                history4.setUserAccount(createStaticUserAccountService().findUserAccount(3));
                history4.setReportDefinition(queue.getReportDefinition());
                history4.setExecutionEndGmt(new Date());
                ReportStatusType statush4 = new ReportStatusType();
                statush4.setId(ReportingConstants.REPORT_STATUS_CANCELLED);
                history4.setReportStatusType(statush4);
                history4.setStatusExplanation("CANCELLED");
                historyList.add(history4);
                return true;
            }
            
            @Override
            public void saveReportQueue(ReportQueue reportQueue) {
                reportQueue.setId(queueList.size());
                queueList.add(reportQueue);
            }
            
            @Override
            public List<QueueInfo> findQueuedReports(ReportSearchCriteria criteria, RandomKeyMapping keyMapping, TimeZone timeZone) {
                List<QueueInfo> results = new ArrayList<QueueInfo>(1);
                for (ReportQueue q : queueList) {
                    QueueInfo qi = new QueueInfo();
                    qi.setRandomId("" + q.getId());
                    qi.setTitle(q.getReportDefinition().getTitle());
                    qi.setStatus(q.getReportStatusType().getId());
                    qi.setTime(new RelativeDateTime(q.getExecutionBeginGmt(), WebCoreConstants.GMT));
                    
                    results.add(qi);
                }
                
                return results;
            }
        });
        
        this.controller.setReportHistoryService(new ReportHistoryServiceImpl() {
            public List<ReportHistory> findUndeletedReportsByUserAccountId(int userAccountId) {
                return historyList;
            }
            
            @Override
            public List<ReportHistory> findFailedAndCancelledReportsByUserAccountId(int userAccountId) {
                List<ReportHistory> localHistoryList = new ArrayList<ReportHistory>();
                for (ReportHistory history : historyList) {
                    if (history.getReportStatusType().getId() == ReportingConstants.REPORT_STATUS_CANCELLED
                        || history.getReportStatusType().getId() == ReportingConstants.REPORT_STATUS_FAILED)
                        localHistoryList.add(history);
                    
                }
                return localHistoryList;
            }
            
            @Override
            public ReportHistory getReportHistory(int id) {
                for (ReportHistory history : historyList) {
                    if (history.getId() == id)
                        return history;
                }
                return null;
            }
            
            @Override
            public void updateReportHistory(ReportHistory history) {
                for (int i = 0; i < historyList.size(); i++) {
                    ReportHistory localHistory = historyList.get(i);
                    if (localHistory.getId().intValue() == history.getId().intValue()) {
                        historyList.set(i, history);
                    }
                }
            }
            
            @Override
            public List<QueueInfo> findFailedReports(ReportSearchCriteria criteria, RandomKeyMapping keyMapping, TimeZone timeZone) {
                List<QueueInfo> result = new ArrayList<QueueInfo>();
                for (ReportHistory history : historyList) {
                    if (history.getReportStatusType().getId() == ReportingConstants.REPORT_STATUS_CANCELLED
                        || history.getReportStatusType().getId() == ReportingConstants.REPORT_STATUS_FAILED) {
                        QueueInfo qi = new QueueInfo();
                        qi.setRandomId("" + history.getId());
                        qi.setTitle(history.getReportDefinition().getTitle());
                        qi.setStatus(history.getReportStatusType().getId());
                        qi.setStatusExplanation(history.getStatusExplanation());
                        qi.setTime(new RelativeDateTime(history.getExecutionBeginGmt(), WebCoreConstants.GMT));
                        
                        result.add(qi);
                    }
                }
                
                return result;
            }
        });
        
        this.controller.setReportRepositoryService(new ReportRepositoryServiceImpl() {
            @Override
            public ReportRepository findByReportHistoryId(int id) {
                ReportRepository repo1 = new ReportRepository();
                repo1.setId(1);
                ReportHistory history1 = new ReportHistory();
                history1.setId(1);
                repo1.setReportHistory(history1);
                repo1.setIsReadByUser(false);
                repo1.setIsSoonToBeDeleted(true);
                return repo1;
            }
            
            @Override
            public List<RepositoryInfo> findCompletedReports(ReportSearchCriteria criteria, RandomKeyMapping keyMapping, TimeZone timeZone) {
                ReportRepository repo = findByReportHistoryId(1);
                
                RepositoryInfo ri = new RepositoryInfo();
                ri.setRandomId("" + repo.getId());
                ri.setReadByUser(repo.isIsReadByUser());
                ri.setToBeDeleted(repo.isIsSoonToBeDeleted());
                
                List<RepositoryInfo> result = new ArrayList<RepositoryInfo>();
                result.add(ri);
                
                return result;
            }
            
            @Override
            public List<ReportRepository> findByUserAccountId(int userAccountId) {
                return repositoryList;
            }
            
            @Override
            public ReportRepository getReportRepository(int id) {
                for (ReportRepository repository : repositoryList) {
                    if (repository.getId() == id)
                        return repository;
                }
                return null;
            }
            
            @Override
            public void deleteReportRepository(int id) {
                for (ReportRepository repository : repositoryList) {
                    if (repository.getId() == id) {
                        repositoryList.remove(repository);
                        break;
                    }
                }
            }
        });
        
        this.controller.setLocationService(new LocationServiceImpl() {
            @Override
            public List<Location> findLocationsByCustomerId(Integer id) {
                List<Location> newLocationList = new ArrayList<Location>();
                for (Location location : locationList) {
                    newLocationList.add(location);
                }
                return newLocationList;
            }
            
            @Override
            public LocationTree getLocationPayStationTreeByCustomerId(int customerId, boolean showActiveOnly, boolean showAssignedOnly,
                boolean showVisibleOnly, RandomKeyMapping keyMapping) {
                return new LocationTree();
            }
            
            @Override
            public LocationTree getRoutePayStationTreeByCustomerId(int customerId, boolean showActiveOnly, boolean showAssignedOnly,
                boolean showVisibleOnly, RandomKeyMapping keyMapping) {
                return new LocationTree();
            }
        });
        
        this.controller.setMerchantAccountService(new MerchantAccountServiceImpl() {
            @Override
            public List<FilterDTO> getMerchantAccountFiltersByCustomerId(List<FilterDTO> result, int customerId, RandomKeyMapping keyMapping) {
                return merchantAccountList;
            }
            
        });
        
        this.controller.setCreditCardTypeService(new CreditCardTypeServiceTestImpl() {
            @Override
            public List<FilterDTO> getFilters(List<FilterDTO> result, RandomKeyMapping keyMapping, boolean showOnlyValid) {
                return ccTypeList;
            }
        });
        
        this.controller.setCustomerCardTypeService(new CustomerCardTypeServiceImpl() {
            @Override
            public List<FilterDTO> getValueCardTypeFilters(List<FilterDTO> result, Integer customerId, boolean showDeleted,
                RandomKeyMapping keyMapping) {
                // TODO Auto-generated method stub
                return super.getValueCardTypeFilters(result, customerId, showDeleted, keyMapping);
            }
        });
        
        this.controller.setCustomerService(new CustomerServiceTestImpl() {
            @Override
            public Customer findCustomer(Integer customerId) {
                // TODO Auto-generated method stub
                return new Customer(2);
            }
        });
        
        this.controller.setCustomerAdminService(new CustomerAdminServiceTestImpl() {
            @Override
            public CustomerProperty getCustomerPropertyByCustomerIdAndCustomerPropertyType(int customerId, int customerPropertyType) {
                // TODO Auto-generated method stub
                CustomerProperty timezone = new CustomerProperty();
                timezone.setPropertyValue("US/Pacific");
                return timezone;
            }
        });
        
        this.controller.setReportingUtil(new ReportingUtilImpl() {
            @Override
            public String getPropertyEN(String key) {
                return "TEST";
            }
            
            @Override
            public List<ReportUIFilterType> getFilterList(int reportType, int filterType) {
                if (getReportFilters() == null) {
                    List<ReportFilterValue> filterValueList = fillFilterList();
                    
                    if (filterValueList.size() != 0) {
                        setReportFilters(new HashMap<Integer, String>());
                        for (ReportFilterValue filterValue : filterValueList) {
                            getReportFilters().put(filterValue.getReportFilterValue(), filterValue.getName());
                        }
                    }
                }
                if (getUiReportFilters() == null) {
                    fillUIReportFilters();
                }
                Map<Integer, List<ReportUIFilterType>> allFilters = getUiReportFilters().get((byte) reportType);
                return allFilters.get(filterType);
            }
            
            @Override
            public void fillUIReportFilters() {
                if (getUiReportFilters() == null) {
                    setUiReportFilters(new HashMap<Byte, Map<Integer, List<ReportUIFilterType>>>());
                    
                    getUiReportFilters().put(REPORT_TYPE_COMMON, fillCommonFilters());
                    getUiReportFilters().put(REPORT_TYPE_TRANSACTION_ALL, fillTransactionFilters(REPORT_TYPE_TRANSACTION_ALL));
                    getUiReportFilters().put(REPORT_TYPE_TRANSACTION_CASH, fillTransactionFilters(REPORT_TYPE_TRANSACTION_CASH));
                    getUiReportFilters().put(REPORT_TYPE_TRANSACTION_CREDIT_CARD, fillTransactionFilters(REPORT_TYPE_TRANSACTION_CREDIT_CARD));
                    getUiReportFilters().put(REPORT_TYPE_TRANSACTION_CC_REFUND, fillTransactionFilters(REPORT_TYPE_TRANSACTION_CC_REFUND));
                    getUiReportFilters().put(REPORT_TYPE_TRANSACTION_CC_PROCESSING, fillTransactionFilters(REPORT_TYPE_TRANSACTION_CC_PROCESSING));
                    getUiReportFilters().put(REPORT_TYPE_TRANSACTION_CC_RETRY, fillTransactionFilters(REPORT_TYPE_TRANSACTION_CC_RETRY));
                    getUiReportFilters().put(REPORT_TYPE_TRANSACTION_PATROLLER_CARD, fillTransactionFilters(REPORT_TYPE_TRANSACTION_PATROLLER_CARD));
                    getUiReportFilters().put(REPORT_TYPE_TRANSACTION_SMART_CARD, fillTransactionFilters(REPORT_TYPE_TRANSACTION_SMART_CARD));
                    getUiReportFilters().put(REPORT_TYPE_TRANSACTION_VALUE_CARD, fillTransactionFilters(REPORT_TYPE_TRANSACTION_VALUE_CARD));
                    getUiReportFilters().put(REPORT_TYPE_TRANSACTION_RATE, fillRateFilters(false));
                    getUiReportFilters().put(REPORT_TYPE_TRANSACTION_RATE_SUMMARY, fillRateFilters(true));
                    getUiReportFilters().put(REPORT_TYPE_STALL_REPORTS, fillStallFilters());
                    getUiReportFilters().put(REPORT_TYPE_COUPON_USAGE_SUMMARY, fillCouponUsageFilters());
                    getUiReportFilters().put(REPORT_TYPE_AUDIT_REPORTS, fillAuditFilters());
                    getUiReportFilters().put(REPORT_TYPE_REPLENISH_REPORTS, fillReplenishFilters());
                    getUiReportFilters().put(REPORT_TYPE_PAY_STATION_SUMMARY, fillPayStationSummaryFilters());
                    getUiReportFilters().put(REPORT_TYPE_TAX_REPORT, fillTaxFilters());
                    
                }
            }
            
            @Override
            public Map<Integer, List<ReportUIFilterType>> fillCommonFilters() {
                Map<Integer, List<ReportUIFilterType>> inputMap = new HashMap<Integer, List<ReportUIFilterType>>();
                
                List<ReportType> reportTypeList = new ArrayList<ReportType>();
                reportTypeList.add(new ReportType((short) 1, "ReportType 1", new Date(), 1));
                reportTypeList.add(new ReportType((short) 2, "ReportType 2", new Date(), 1));
                reportTypeList.add(new ReportType((short) 3, "ReportType 3", new Date(), 1));
                reportTypeList.add(new ReportType((short) 4, "ReportType 4", new Date(), 1));
                List<ReportUIFilterType> reportTypeFilters = new ArrayList<ReportUIFilterType>();
                for (ReportType reportType : reportTypeList) {
                    reportTypeFilters.add(new ReportUIFilterType(reportType.getId(), reportType.getName()));
                }
                inputMap.put(REPORT_FILTER_TYPE_REPORT_TYPE, reportTypeFilters);
                
                List<ReportRepeatType> reportRepeatTypeList = new ArrayList<ReportRepeatType>();
                reportRepeatTypeList.add(new ReportRepeatType((short) 1, "ReportRepeatType 1", new Date(), 1));
                reportRepeatTypeList.add(new ReportRepeatType((short) 2, "ReportRepeatType 2", new Date(), 1));
                reportRepeatTypeList.add(new ReportRepeatType((short) 3, "ReportRepeatType 3", new Date(), 1));
                List<ReportUIFilterType> reportRepeatTypeFilters = new ArrayList<ReportUIFilterType>();
                for (ReportRepeatType reportRepeatType : reportRepeatTypeList) {
                    reportRepeatTypeFilters.add(new ReportUIFilterType(reportRepeatType.getId(), reportRepeatType.getName()));
                }
                inputMap.put(REPORT_FILTER_TYPE_REPORT_REPEAT_TYPE, reportRepeatTypeFilters);
                
                return inputMap;
            }
            
            private List<ReportFilterValue> fillFilterList() {
                List<ReportFilterValue> list = new ArrayList<ReportFilterValue>();
                for (int i = 1; i < 15; i++) {
                    for (int j = (i * 100); j < (i * 100) + 15; j++) {
                        ReportFilterValue rfv = new ReportFilterValue();
                        rfv.setId(i);
                        rfv.setReportFilterValue(i);
                        rfv.setName("Name " + i);
                        list.add(rfv);
                    }
                }
                
                return list;
            }
            
        });
        
    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(3, 3, "testUser", "password", "salt", true, authorities);
        user.setCustomerTimeZone("US/Pacific");
        user.setIsComplexPassword(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_REPORTS_MANAGEMENT);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_REPORTS);
        user.setUserPermissions(permissionList);
        
        List<Integer> subscriptions = new ArrayList<Integer>();
        subscriptions.add(WebCoreConstants.SUBSCRIPTION_TYPE_STANDARD_REPORT);
        user.setCustomerSubscriptions(subscriptions);
        
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    //========================================================================================================================
    // "Reporting List" page
    //========================================================================================================================
    
    private WebSecurityUtil createWebSecurityUtil() {
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        util.setReportRepositoryService(new ReportRepositoryServiceImpl() {
            public int countUnreadReportsByUserAccountId(int userId) {
                return 5;
            }
        });
        
        return util;
    }
    
    @Test
    public void test_ReportingLists() {
        WebSecurityUtil util = createWebSecurityUtil();
        
        String result = this.controller.getReportLists(request, response, model);
        assertNotNull(result);
        assertEquals(result, "/reporting/index");
        assertNotNull(model.get("definitionInfoList"));
        assertNotNull(model.get("queueAndRepositoryInfoList"));
    }
    
    @Test
    public void test_CancellQueuedReport() {
        
        WebSecurityUtil util = createWebSecurityUtil();
        
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        ReportQueue queue1 = queueList.get(0);
        
        request.setParameter("queueID", keyMapping.hideProperty(queue1, "id").getPrimaryKey().toString());
        String result = this.controller.cancelQueuedReport(request, response, model);
        assertNotNull(result);
        
        ReportQueue queue2 = queueList.get(0);
        
        request.setParameter("queueID", keyMapping.hideProperty(queue2, "id").getPrimaryKey().toString());
        result = this.controller.cancelQueuedReport(request, response, model);
        assertNotNull(result);
        assertEquals("" + WebCoreConstants.HTTP_STATUS_CODE_483, response.getHeader(WebCoreConstants.HEADER_CUSTOM_STATUS_CODE));
        
    }
    
    @Test
    public void test_rerunReport() {
        
        WebSecurityUtil util = createWebSecurityUtil();
        
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        ReportHistory history3 = historyList.get(2);
        
        request.setParameter("queueID", keyMapping.hideProperty(history3, "id").getPrimaryKey().toString());
        String result = this.controller.rerunReport(request, response, model);
        assertNotNull(result);
        
        ReportRepository repository1 = repositoryList.get(0);
        
        request.setParameter("repositoryID", keyMapping.hideProperty(repository1, "id").getPrimaryKey().toString());
        result = this.controller.rerunReport(request, response, model);
        assertNotNull(result);
        
    }
    
    @Test
    public void test_runNow() {
        
        WebSecurityUtil util = createWebSecurityUtil();
        
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        ReportDefinition def2 = definitionList.get(1);
        
        request.setParameter("definitionID", keyMapping.hideProperty(def2, "id").getPrimaryKey().toString());
        String result = this.controller.runNow(request, response, model);
        assertNotNull(result);
        
    }
    
    @Test
    public void test_deleteReport() {
        
        WebSecurityUtil util = createWebSecurityUtil();
        
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        ReportRepository repository1 = repositoryList.get(0);
        
        request.setParameter("repositoryID", keyMapping.hideProperty(repository1, "id").getPrimaryKey().toString());
        String result = this.controller.deleteReport(request, response, model);
        assertNotNull(result);
        
    }
    
    @Test
    public void test_deleteReportDefinition() {
        
        WebSecurityUtil util = createWebSecurityUtil();
        
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        ReportDefinition def1 = definitionList.get(0);
        
        request.setParameter("definitionID", keyMapping.hideProperty(def1, "id").getPrimaryKey().toString());
        String result = this.controller.deleteReportDefinition(request, response, model);
        assertNotNull(result);
        
    }
    
    @Test
    public void test_newReport() {
        
        WebSecurityUtil util = createWebSecurityUtil();
        
        ReportEditForm form = new ReportEditForm();
        
        BindingResult result = new BeanPropertyBindingResult(form, "reportEditForm");
        WebSecurityForm<ReportEditForm> webSecurityForm = new WebSecurityForm<ReportEditForm>(null, form);
        webSecurityForm.setPostToken(webSecurityForm.getInitToken());
        model.put("reportEditForm", webSecurityForm);
        
        //        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        //        ReportDefinition def1 = definitionList.get(0);
        
        request.setParameter("reportTypeID", "3");
        
        this.controller.newReportSettings((WebSecurityForm<ReportEditForm>) model.get("reportEditForm"), result, request, response, model);
        assertNotNull(result);
        //        assertEquals(result, "/reporting/formReport");
        
    }
    
    @Test
    public void test_saveReport() {
        
        WebSecurityUtil util = createWebSecurityUtil();
        
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        test_newReport();
        
        String[] locationPOSRIDs = new String[0];
        
        ReportEditForm form = createValidReportTestForm(keyMapping, "0", "0", locationPOSRIDs);
        
        BindingResult result = new BeanPropertyBindingResult(form, "reportEditForm");
        WebSecurityForm<ReportEditForm> webSecurityForm = (WebSecurityForm<ReportEditForm>) model.get("reportEditForm");
        webSecurityForm.setPostToken(webSecurityForm.getInitToken());
        webSecurityForm.setWrappedObject(form);
        
        controller.setReportValidator(new ReportValidator() {
            @Override
            public void validate(WebSecurityForm<ReportEditForm> command, Errors errors) {
            }
        });
        
        CommonControllerHelper commonControllerHelper = new CommonControllerHelperImpl() {
            @Override
            public String getMessage(String key) {
                return "All";
            }
        };
        controller.setCommonControllerHelper(commonControllerHelper);
        
        int initialDefSize = this.definitionList.size();
        int initialRLVSize = this.rlvList.size();
        String returnValue = controller.saveReport(webSecurityForm, result, request, response, model);
        int finalDefSize = this.definitionList.size();
        int finalRLVSize = this.rlvList.size();
        assertTrue(returnValue.contains(WebCoreConstants.RESPONSE_TRUE));
        assertTrue((initialDefSize + 1) == finalDefSize);
        //        assertTrue((initialRLVSize + 4) == finalRLVSize);
    }
    
    @Test
    public void test_editReport() {
        WebSecurityUtil util = createWebSecurityUtil();
        
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        test_saveReport();
        controller.setReportValidator(new ReportValidator() {
            @Override
            public void validate(WebSecurityForm<ReportEditForm> command, Errors errors) {
            }
        });
        
        ReportDefinition def1 = definitionList.get(definitionList.size() - 1);
        
        request.setParameter("definitionID", keyMapping.hideProperty(def1, "id").getPrimaryKey().toString());
        
        String result = this.controller.editReportSettings(request, response, model, new WebSecurityForm((Object) null, new ReportEditForm()));
        assertNotNull(result);
        assertEquals(result, "/reporting/newReport");
        
        WebSecurityForm<ReportEditForm> webSecurityForm = (WebSecurityForm<ReportEditForm>) model.get("reportEditForm");
        webSecurityForm.setPostToken(webSecurityForm.getInitToken());
        
        ReportEditForm form = (ReportEditForm) webSecurityForm.getWrappedObject();
        form.setEmailList(Arrays.asList("test1@gmail.com", "test2@gmail.com", "test4@gmail.com"));
        
        BindingResult bindingResult = new BeanPropertyBindingResult(form, "reportEditForm");
        
        int initialDefSize = this.definitionList.size();
        int initialRLVSize = this.rlvList.size();
        int initialRELSize = this.reportEmailList.size();
        int initialCELSize = this.customerEmailList.size();
        String returnValue = controller.saveReport((WebSecurityForm<ReportEditForm>) model.get("reportEditForm"), bindingResult, request, response,
                                                   model);
        int finalDefSize = this.definitionList.size();
        int finalRLVSize = this.rlvList.size();
        int finalRELSize = this.reportEmailList.size();
        int finalCELSize = this.customerEmailList.size();
        assertTrue(returnValue.contains(WebCoreConstants.RESPONSE_TRUE));
        assertTrue((initialDefSize + 1) == finalDefSize);
        //        assertTrue((initialRLVSize + 4) == finalRLVSize);
        assertTrue((initialRELSize + 3) == finalRELSize);
        assertTrue((initialCELSize + 1) == finalCELSize);
        //        assertTrue((this.definitionList.get(this.definitionList.size() - 2)).getReportStatusType().getId() == ReportingConstants.REPORT_STATUS_DELETED);
        assertTrue((this.definitionList.get(this.definitionList.size() - 1)).getReportStatusType().getId() == ReportingConstants.REPORT_STATUS_READY);
        
    }
    
    private ReportEditForm createValidReportTestForm(RandomKeyMapping keyMapping, String cardTypeRID, String merchantRID, String[] locationPOSRIDs) {
        ReportEditForm form = new ReportEditForm();
        
        form.setReportType(3);
        form.setIsSummary(false);
        form.setPrimaryDateType(ReportingConstants.REPORT_FILTER_DATE_DATE_TIME);
        form.setPrimaryDateBegin("12/20/2012");
        form.setPrimaryTimeBegin("07:30");
        form.setPrimaryDateEnd("12/25/2012");
        form.setPrimaryTimeEnd("17:30");
        
        form.setSpaceNumberType(ReportingConstants.REPORT_FILTER_SPACE_NUMBER_GREATER);
        form.setSpaceNumberBegin(10);
        form.setShowHiddenPayStations(true);
        form.setLocationType(ReportingConstants.REPORT_FILTER_LOCATION_LOCATION_TREE);
        form.setSelectedLocationRandomIds(Arrays.asList(locationPOSRIDs));
        form.setCardNumberType(ReportingConstants.REPORT_FILTER_CARD_NUMBER_ALL);
        form.setCardType(cardTypeRID);
        form.setApprovalStatus(ReportingConstants.REPORT_FILTER_APPROVAL_STATUS_OFFLINE);
        form.setMerchantAccount(merchantRID);
        form.setTicketNumberType(ReportingConstants.REPORT_FILTER_TICKET_NUMBER_BETWEEN);
        form.setTicketNumberBegin(10);
        form.setTicketNumberEnd(1000000);
        form.setCouponNumberType(ReportingConstants.REPORT_FILTER_COUPON_NUMBER_NA);
        form.setOtherParameters(ReportingConstants.REPORT_FILTER_TRANSACTION_ALL);
        form.setGroupBy(ReportingConstants.REPORT_FILTER_GROUP_BY_DAY_OF_WEEK);
        form.setIsPDF(true);
        form.setIsScheduled(true);
        form.setReportTitle("Test report");
        form.setStartDate("12/28/2012");
        form.setStartTime("13:30");
        form.setRepeatType(3);
        form.setRepeatOn(Arrays.asList("1", "3", "5"));
        form.setRepeatEvery(1);
        form.setEmailList(Arrays.asList("test1@gmail.com", "test2@gmail.com", "test3@gmail.com"));
        return form;
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                return userAccount;
            }
        };
    }
}
