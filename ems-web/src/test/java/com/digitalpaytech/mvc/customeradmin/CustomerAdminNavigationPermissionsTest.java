package com.digitalpaytech.mvc.customeradmin;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;

public class CustomerAdminNavigationPermissionsTest {

	CustomerSettingsIndexController indexController = new CustomerSettingsIndexController();
	
	@Test
	public void test_noPermissions(){
		
		MockHttpServletRequest request = new MockHttpServletRequest();
		MockHttpServletResponse response = new MockHttpServletResponse();
		MockHttpSession session = new MockHttpSession();
		request.setSession(session);
		ModelMap model = new ModelMap();

		Collection<Integer> permissionList = new ArrayList<Integer>();
		
		UserAccount userAccount = new UserAccount();
		createAuthentication(userAccount, permissionList);
		
		String res = indexController.getFirstPermittedTab(request, response, model);
        assertEquals(res,"redirect:/secure/settings/global/sysNotification.html");
	}
	
	@Test
	public void test_hasGlobalTabPermissions(){

		MockHttpServletRequest request = new MockHttpServletRequest();
		MockHttpServletResponse response = new MockHttpServletResponse();
		MockHttpSession session = new MockHttpSession();
		request.setSession(session);
		ModelMap model = new ModelMap();

		UserAccount userAccount = new UserAccount();
				
		Collection<Integer> permissionList = new ArrayList<Integer>();
		createAuthentication(userAccount, permissionList);		
		String res = indexController.getFirstPermittedTab(request, response, model);
        assertEquals(res,"redirect:/secure/settings/global/sysNotification.html");
		
		permissionList = new ArrayList<Integer>();
		permissionList.add(WebSecurityConstants.PERMISSION_SYSTEM_SETTINGS);
		createAuthentication(userAccount, permissionList);		
		res = indexController.getFirstPermittedTab(request, response, model);
		assertEquals(res,"redirect:/secure/settings/global/index.html");
		
		permissionList = new ArrayList<Integer>();
		permissionList.add(WebSecurityConstants.PERMISSION_VIEW_NOTIFICATIONS);
		createAuthentication(userAccount, permissionList);		
		res = indexController.getFirstPermittedTab(request, response, model);
		assertEquals(res,"redirect:/secure/settings/global/index.html");
		
		permissionList = new ArrayList<Integer>();
		permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_NOTIFICATIONS);
		createAuthentication(userAccount, permissionList);
		res = indexController.getFirstPermittedTab(request, response, model);
		assertEquals(res,"redirect:/secure/settings/global/index.html");
	}		

	@Test
	public void test_hasLocationsTabPermissions(){

		MockHttpServletRequest request = new MockHttpServletRequest();
		MockHttpServletResponse response = new MockHttpServletResponse();
		MockHttpSession session = new MockHttpSession();
		request.setSession(session);
		ModelMap model = new ModelMap();

		UserAccount userAccount = new UserAccount();
		
		Collection<Integer> permissionList = new ArrayList<Integer>();
		createAuthentication(userAccount, permissionList);		
		String res = indexController.getFirstPermittedTab(request, response, model);
        assertEquals(res,"redirect:/secure/settings/global/sysNotification.html");
		
		permissionList = new ArrayList<Integer>();
		permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS);
		createAuthentication(userAccount, permissionList);		
		res = indexController.getFirstPermittedTab(request, response, model);
		assertEquals(res,"redirect:/secure/settings/locations/index.html");
		
		permissionList = new ArrayList<Integer>();
		permissionList.add(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS);
		createAuthentication(userAccount, permissionList);		
		res = indexController.getFirstPermittedTab(request, response, model);
		assertEquals(res,"redirect:/secure/settings/locations/index.html");
		
		permissionList = new ArrayList<Integer>();
		permissionList.add(WebSecurityConstants.PERMISSION_VIEW_RATES_AND_POLICIES);
		createAuthentication(userAccount, permissionList);		
		res = indexController.getFirstPermittedTab(request, response, model);
		assertEquals(res,"redirect:/secure/settings/locations/index.html");
		
		permissionList = new ArrayList<Integer>();
		permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_RATES_AND_POLICIES);
		createAuthentication(userAccount, permissionList);		
		res = indexController.getFirstPermittedTab(request, response, model);
		assertEquals(res,"redirect:/secure/settings/locations/index.html");
		
		permissionList = new ArrayList<Integer>();
		permissionList.add(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS);
		permissionList.add(WebSecurityConstants.PERMISSION_VIEW_ROUTES);
		permissionList.add(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS);
		createAuthentication(userAccount, permissionList);		
		res = indexController.getFirstPermittedTab(request, response, model);
		assertEquals(res,"redirect:/secure/settings/locations/index.html");
		
		permissionList = new ArrayList<Integer>();
		permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS);
		permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_ROUTES);
		permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS);
		createAuthentication(userAccount, permissionList);		
		res = indexController.getFirstPermittedTab(request, response, model);
		assertEquals(res,"redirect:/secure/settings/locations/index.html");
	}	
	
	@Test
	public void test_hasRoutesTabPersmissions(){

		MockHttpServletRequest request = new MockHttpServletRequest();
		MockHttpServletResponse response = new MockHttpServletResponse();
		MockHttpSession session = new MockHttpSession();
		request.setSession(session);
		ModelMap model = new ModelMap();

		UserAccount userAccount = new UserAccount();

		Collection<Integer> permissionList = new ArrayList<Integer>();
		createAuthentication(userAccount, permissionList);		
		String res = indexController.getFirstPermittedTab(request, response, model);
        assertEquals(res,"redirect:/secure/settings/global/sysNotification.html");
		
		permissionList = new ArrayList<Integer>();
		permissionList.add(WebSecurityConstants.PERMISSION_VIEW_ROUTES);
		createAuthentication(userAccount, permissionList);		
		res = indexController.getFirstPermittedTab(request, response, model);
		assertEquals(res,"redirect:/secure/settings/routes/index.html");
		
		permissionList = new ArrayList<Integer>();
		permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_ROUTES);
		createAuthentication(userAccount, permissionList);		
		res = indexController.getFirstPermittedTab(request, response, model);
		assertEquals(res,"redirect:/secure/settings/routes/index.html");		
	}	

	@Test
	public void test_hasAlertsTabPersmissions(){

		MockHttpServletRequest request = new MockHttpServletRequest();
		MockHttpServletResponse response = new MockHttpServletResponse();
		MockHttpSession session = new MockHttpSession();
		request.setSession(session);
		ModelMap model = new ModelMap();

		UserAccount userAccount = new UserAccount();

		Collection<Integer> permissionList = new ArrayList<Integer>();
		createAuthentication(userAccount, permissionList);		
		String res = indexController.getFirstPermittedTab(request, response, model);
        assertEquals(res,"redirect:/secure/settings/global/sysNotification.html");
		
		permissionList = new ArrayList<Integer>();
		permissionList.add(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS);
		createAuthentication(userAccount, permissionList);		
		res = indexController.getFirstPermittedTab(request, response, model);
		assertEquals(res,"redirect:/secure/settings/alerts/index.html");
		
		permissionList = new ArrayList<Integer>();
		permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT);
		createAuthentication(userAccount, permissionList);		
		res = indexController.getFirstPermittedTab(request, response, model);
		assertEquals(res,"redirect:/secure/settings/alerts/index.html");		
	}	

	@Test
	public void test_hasUsersTabPersmissions(){

		MockHttpServletRequest request = new MockHttpServletRequest();
		MockHttpServletResponse response = new MockHttpServletResponse();
		MockHttpSession session = new MockHttpSession();
		request.setSession(session);
		ModelMap model = new ModelMap();

		UserAccount userAccount = new UserAccount();

		Collection<Integer> permissionList = new ArrayList<Integer>();
		createAuthentication(userAccount, permissionList);		
		String res = indexController.getFirstPermittedTab(request, response, model);
        assertEquals(res,"redirect:/secure/settings/global/sysNotification.html");
		
		permissionList = new ArrayList<Integer>();
		permissionList.add(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
		createAuthentication(userAccount, permissionList);		
		res = indexController.getFirstPermittedTab(request, response, model);
		assertEquals(res,"redirect:/secure/settings/users/index.html");
		
		permissionList = new ArrayList<Integer>();
		permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
		createAuthentication(userAccount, permissionList);		
		res = indexController.getFirstPermittedTab(request, response, model);
		assertEquals(res,"redirect:/secure/settings/users/index.html");		
	}	

	@Test
	public void test_hasCardSettingsPersmissions(){

		MockHttpServletRequest request = new MockHttpServletRequest();
		MockHttpServletResponse response = new MockHttpServletResponse();
		MockHttpSession session = new MockHttpSession();
		request.setSession(session);
		ModelMap model = new ModelMap();

		UserAccount userAccount = new UserAccount();

		Collection<Integer> permissionList = new ArrayList<Integer>();
		createAuthentication(userAccount, permissionList);		
		String res = indexController.getFirstPermittedTab(request, response, model);
        assertEquals(res,"redirect:/secure/settings/global/sysNotification.html");
		
		permissionList = new ArrayList<Integer>();
		permissionList.add(WebSecurityConstants.PERMISSION_SETUP_CREDIT_CARD_PROCESSING);
		createAuthentication(userAccount, permissionList);		
		res = indexController.getFirstPermittedTab(request, response, model);
		assertEquals(res,"redirect:/secure/settings/cardSettings/index.html");
		
		permissionList = new ArrayList<Integer>();
		permissionList.add(WebSecurityConstants.PERMISSION_CONFIGURE_CUSTOM_CARDS);
		createAuthentication(userAccount, permissionList);		
		res = indexController.getFirstPermittedTab(request, response, model);
		assertEquals(res,"redirect:/secure/settings/cardSettings/index.html");		
	}	
	
	private void createAuthentication(UserAccount userAccount, Collection<Integer> permissionList) {
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("Admin"));
		WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true,
				authorities);
		user.setIsComplexPassword(true);
		Authentication authentication = new UsernamePasswordAuthenticationToken(
				user, "password", authorities);
		user.setUserPermissions(permissionList);
		SecurityContextHolder.getContext().setAuthentication(authentication);
	}
}
