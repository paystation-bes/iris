package com.digitalpaytech.mvc.customeradmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.AdditionalAnswers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;

import com.digitalpaytech.bdd.mvc.ControllerBaseSteps;
import com.digitalpaytech.bdd.util.TestContext;
import com.digitalpaytech.client.CardTypeClient;
import com.digitalpaytech.client.dto.ObjectResponse;
import com.digitalpaytech.client.dto.corecps.CardConfiguration;
import com.digitalpaytech.dao.impl.EntityDaoTest;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerBadCard;
import com.digitalpaytech.domain.CustomerCardType;
import com.digitalpaytech.domain.CustomerType;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.dto.BadCardMessage;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.mvc.customeradmin.support.BannedCardEditForm;
import com.digitalpaytech.mvc.customeradmin.support.BannedCardUploadForm;
import com.digitalpaytech.mvc.customeradmin.support.io.BadCardCsvProcessor;
import com.digitalpaytech.mvc.customeradmin.support.io.BadCardManagementCsvValidator;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.ribbon.mock.MockClientFactory;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.crypto.CryptoAlgorithmFactory;
import com.digitalpaytech.service.impl.CardRetryTransactionServiceImpl;
import com.digitalpaytech.service.impl.CardTypeServiceImpl;
import com.digitalpaytech.service.impl.CreditCardTypeServiceImpl;
import com.digitalpaytech.service.impl.CustomerBadCardServiceImpl;
import com.digitalpaytech.service.impl.CustomerCardTypeServiceImpl;
import com.digitalpaytech.service.impl.CustomerServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.LinkBadCardServiceImpl;
import com.digitalpaytech.service.impl.LinkCardTypeServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.service.kpi.KPIListenerService;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.util.kafka.MockMessageProducer;
import com.digitalpaytech.validation.customeradmin.CardManagementEditValidator;
import com.digitalpaytech.validation.customeradmin.CardManagementUploadValidator;

@SuppressWarnings({ "PMD.ExcessiveImports", "PMD.TooManyFields", "PMD.UnusedPrivateField", "PMD.TooManyStaticImports" })
public class CardManagementControllerIntegrationTest {
    
    private static final String CREDIT_CARD = "Credit Card";
    private static final String BLACK_BOARD_CUSTOM_CARD = "BlackBoardCustomCard";
    private static final String PASSCARD = "Passcard";
    private static final String SMART_CARD = "Smart Card";
    private static final String MASTER_CARD = "MasterCard";
    private static final String SHA2HASH = "SHA2HASH";
    private static final String SHA1HASH = "SHA1HASH";
    private static final String TEST_USER = "TestUser";
    private static final String COMMENT = "This is a test comment";
    
    private static final int TEST_CUSTOMER_ID = 1;
    private static final int TEST_CUSTOMER_BAD_CARD_ID = 1;
    
    private final TestContext ctx = TestContext.getInstance();
    
    private final EntityDaoTest entityDao = new EntityDaoTest();
    
    private final MockClientFactory client = MockClientFactory.getInstance();
    
    private final JSON json = new JSON();
    
    @InjectMocks
    private final MockMessageProducer producer = MockMessageProducer.getInstance();
    
    @Mock
    private CardManagementEditValidator cardManagementEditValidator;
    
    @Mock
    private BindingResult bindingResult;
    
    @InjectMocks
    private CardManagementController cardManagementController;
    
    @InjectMocks
    private EntityServiceImpl entityService;
    
    @InjectMocks
    private UserAccountServiceImpl userAccountService;
    
    @InjectMocks
    private WebSecurityUtil webSecurity;
    
    @InjectMocks
    private MerchantAccountServiceImpl merchantAccountService;
    
    @InjectMocks
    private LinkBadCardServiceImpl linkBadCardService;
    
    @InjectMocks
    private LinkCardTypeServiceImpl linkCardTypeService;
    
    @InjectMocks
    private CreditCardTypeServiceImpl creditCardTypeService;
    
    @InjectMocks
    private CustomerBadCardServiceImpl customerBadCardService;
    
    @Mock
    private CryptoAlgorithmFactory cryptoAlgorithmFactory;
    
    @Mock
    private CryptoService cryptoService;
    
    @Mock
    private CardManagementUploadValidator cardManagementUploadValidator;
    
    @InjectMocks
    private CustomerCardTypeServiceImpl customerCardTypeService;
    
    @InjectMocks
    private CardTypeServiceImpl cardTypeService;
    
    @Mock
    private KPIListenerService kpiListenerService;
    
    @Mock
    private EmsPropertiesService emsPropertiesService;
    
    @InjectMocks
    private BadCardCsvProcessor badCardCsvProcessor;
    
    @InjectMocks
    private BadCardManagementCsvValidator badCardManagementCsvValidator;
    
    @Mock
    private MessageSource messageSource;
    
    @InjectMocks
    private CustomerServiceImpl customerService;
    
    @InjectMocks
    private CardRetryTransactionServiceImpl cardRetryTransactionService;
    
    private Customer customer;
    
    @Before
    public void setUp() throws Exception {
        
        final ObjectResponse<List<CardConfiguration>> response = new ObjectResponse<>();
        
        final CardConfiguration masterCard = new CardConfiguration();
        masterCard.setCardType(MASTER_CARD);
        masterCard.setPatterns(new String[] { "5[1-5]\\d{14}=\\d{7,20}", "5[1-5]\\d{14}",
            "(222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9][0-9]\\d{10}=\\d{7,20}",
            "(222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9][0-9]\\d{10}" });
        
        final CardConfiguration diners = new CardConfiguration();
        diners.setCardType("Diners Club");
        diners.setPatterns(new String[] { "309\\d{11}=\\d{7,22}", "36\\d{12}=\\d{7,22}", "38\\d{12}=\\d{7,22}", "39\\d{12}=\\d{7,22}",
            "30[0-5]\\d{11}", "309\\d{11}", "36\\d{12}", "38\\d{12}", "39\\d{12}", "30[0-5]\\d{11}=\\d{7,22}" });
        
        response.setResponse(Arrays.asList(masterCard, diners));
        
        this.client.prepareForSuccessRequest(CardTypeClient.class, this.json.serialize(response).getBytes());
        
        MockitoAnnotations.initMocks(this);
        
        this.ctx.autowiresFromTestObject(this);
        this.customer = new Customer();
        final CustomerType customerType = new CustomerType();
        customerType.setId(WebCoreConstants.CUSTOMER_TYPE_CHILD);
        this.customer.setCustomerType(customerType);
        this.customer.setId(TEST_CUSTOMER_ID);
        this.customer.setIsMigrated(true);
        this.customer.setUnifiId(12);
        this.entityDao.save(this.customer);
        
        final UserAccount user = new UserAccount();
        user.setUserName(TEST_USER);
        user.setCustomer(this.customer);
        this.entityDao.save(user);
        
        final ControllerBaseSteps ctrl = new ControllerBaseSteps(null);
        ctrl.createPermissions(TEST_USER, "add banned cards");
        ctrl.createAuthentication(TEST_USER);
        
        final MerchantAccount merchantAccount = new MerchantAccount();
        merchantAccount.setId(1);
        merchantAccount.setIsLink(true);
        merchantAccount.setCustomer(this.customer);
        this.entityDao.save(merchantAccount);
        
        this.producer.successNext();
        this.linkBadCardService.setLinkMessageProducer(this.producer);
        this.badCardManagementCsvValidator.setMessageSource(this.messageSource);
        
        Mockito.when(this.cardManagementEditValidator.validateMigrationStatus(Mockito.any())).thenReturn(true);
        Mockito.when(this.cardManagementUploadValidator.validateMigrationStatus(Mockito.any())).thenReturn(true);
        
        Mockito.when(this.cryptoService.encryptData(Mockito.anyString())).thenReturn("EncryptedData=");
        Mockito.when(this.cryptoAlgorithmFactory.getSha1Hash(Mockito.anyString(), Mockito.anyInt())).thenReturn(SHA1HASH);
        Mockito.when(this.cryptoAlgorithmFactory.getShaHash(Mockito.anyString(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(SHA2HASH);
        Mockito.when(this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.MAX_UPLOAD_FILE_SIZE_IN_BYTES,
                                                                     EmsPropertiesService.DEFAULT_MAX_UPLOAD_FILE_SIZE_IN_BYTES))
                .thenReturn(StandardConstants.BYTES_PER_MEGABYTE);
        Mockito.when(this.messageSource.getMessage(Mockito.anyString(), Mockito.any(), Mockito.any()))
                .thenAnswer(AdditionalAnswers.returnsFirstArg());
    }
    
    @After
    public void tearDown() throws Exception {
        this.ctx.getDatabase().clear();
        this.producer.clear();
    }
    
    @Test
    public void testCreditCard() throws CryptoException, JsonException {
        
        final CustomerCardType customerCardType = new CustomerCardType();
        final CardType cardType = new CardType();
        cardType.setId(WebCoreConstants.CARD_TYPE_CREDIT_CARD);
        cardType.setName(CREDIT_CARD);
        customerCardType.setCardType(cardType);
        customerCardType.setName(CREDIT_CARD);
        customerCardType.setIsLocked(true);
        customerCardType.setCustomer(this.customer);
        this.entityDao.save(customerCardType);
        
        final String cardNumber = "5454545454545454";
        final String expiry = "1920";
        
        final WebSecurityForm<BannedCardEditForm> webSecurityForm = new WebSecurityForm<>();
        final BannedCardEditForm form = new BannedCardEditForm();
        form.setCardExpiry(expiry);
        form.setCardNumber(cardNumber);
        form.setCardType("CreditCard");
        form.setCardTypeId(customerCardType.getId());
        form.setComment(COMMENT);
        
        webSecurityForm.setWrappedObject(form);
        
        this.cardManagementController.saveBannedCard(webSecurityForm, this.bindingResult, new MockHttpServletRequest(),
                                                     new MockHttpServletResponse());
        assertFalse(this.producer.history().isEmpty());
        final ProducerRecord<String, String> message = this.producer.history().get(0);
        final String messageBody = message.value();
        final BadCardMessage badCard = this.json.deserialize(messageBody, BadCardMessage.class);
        assertEquals(expiry, badCard.getCardExpiry());
        assertEquals(SHA1HASH, badCard.getSha1CardHash());
        assertEquals(SHA2HASH, badCard.getSha256CardHash());
        assertEquals(MASTER_CARD, badCard.getCardType());
        assertEquals(COMMENT, badCard.getComment());
        
        final Collection<CustomerBadCard> customerBadCards = this.customerBadCardService.findCustomerBadCardByCustomerId(1);
        assertTrue(customerBadCards.isEmpty());
    }
    
    @Test
    public void testSmartCard() throws CryptoException, JsonException {
        
        final CustomerCardType customerCardType = new CustomerCardType();
        final CardType cardType = new CardType();
        cardType.setId(WebCoreConstants.CARD_TYPE_SMART_CARD);
        cardType.setName(SMART_CARD);
        customerCardType.setCardType(cardType);
        customerCardType.setName(SMART_CARD);
        customerCardType.setIsLocked(true);
        customerCardType.setCustomer(this.customer);
        this.entityDao.save(customerCardType);
        
        final WebSecurityForm<BannedCardEditForm> webSecurityForm = new WebSecurityForm<>();
        final BannedCardEditForm form = new BannedCardEditForm();
        form.setCardNumber("1234");
        form.setCardType(SMART_CARD);
        form.setCardTypeId(customerCardType.getId());
        form.setComment(COMMENT);
        
        webSecurityForm.setWrappedObject(form);
        
        this.cardManagementController.saveBannedCard(webSecurityForm, this.bindingResult, new MockHttpServletRequest(),
                                                     new MockHttpServletResponse());
        assertFalse(this.producer.history().isEmpty());
        final ProducerRecord<String, String> message = this.producer.history().get(0);
        final String messageBody = message.value();
        final BadCardMessage badCard = this.json.deserialize(messageBody, BadCardMessage.class);
        assertNull(badCard.getCardExpiry());
        assertEquals(SHA1HASH, badCard.getSha1CardHash());
        assertEquals(SHA2HASH, badCard.getSha256CardHash());
        assertEquals(SMART_CARD, badCard.getCardType());
        assertEquals(SMART_CARD, badCard.getCardParentType());
        assertEquals(COMMENT, badCard.getComment());
        
        final Collection<CustomerBadCard> customerBadCards = this.customerBadCardService.findCustomerBadCardByCustomerId(1);
        assertTrue(customerBadCards.isEmpty());
    }
    
    @Test
    public void testCustomCard() throws CryptoException, JsonException {
        
        final CustomerCardType customerCardType = new CustomerCardType();
        final CardType cardType = new CardType();
        cardType.setId(WebCoreConstants.CARD_TYPE_VALUE_CARD);
        cardType.setName(PASSCARD);
        customerCardType.setCardType(cardType);
        customerCardType.setName(BLACK_BOARD_CUSTOM_CARD);
        customerCardType.setCustomer(this.customer);
        customerCardType.setIsPrimary(true);
        this.entityDao.save(customerCardType);
        
        final WebSecurityForm<BannedCardEditForm> webSecurityForm = new WebSecurityForm<>();
        final BannedCardEditForm form = new BannedCardEditForm();
        form.setCardNumber("1");
        form.setCardType(BLACK_BOARD_CUSTOM_CARD);
        form.setCardTypeId(customerCardType.getId());
        form.setComment(COMMENT);
        
        webSecurityForm.setWrappedObject(form);
        
        this.cardManagementController.saveBannedCard(webSecurityForm, this.bindingResult, new MockHttpServletRequest(),
                                                     new MockHttpServletResponse());
        assertFalse(this.producer.history().isEmpty());
        final ProducerRecord<String, String> message = this.producer.history().get(0);
        final String messageBody = message.value();
        final BadCardMessage badCard = this.json.deserialize(messageBody, BadCardMessage.class);
        assertNull(badCard.getCardExpiry());
        assertEquals(SHA1HASH, badCard.getSha1CardHash());
        assertEquals(SHA2HASH, badCard.getSha256CardHash());
        assertEquals(BLACK_BOARD_CUSTOM_CARD, badCard.getCardType());
        assertEquals(PASSCARD, badCard.getCardParentType());
        assertEquals(COMMENT, badCard.getComment());
        
        final Collection<CustomerBadCard> customerBadCards = this.customerBadCardService.findCustomerBadCardByCustomerId(1);
        assertTrue(customerBadCards.isEmpty());
    }
    
    @Test
    public void testImportBadCards() throws CryptoException, JsonException {
        final CustomerCardType customerCardType = new CustomerCardType();
        final CardType cardType = new CardType();
        cardType.setId(WebCoreConstants.CARD_TYPE_VALUE_CARD);
        cardType.setName(PASSCARD);
        customerCardType.setCardType(cardType);
        customerCardType.setName(PASSCARD);
        customerCardType.setCustomer(this.customer);
        customerCardType.setIsPrimary(true);
        this.entityDao.save(customerCardType);
        
        final CustomerCardType customerCardType1 = new CustomerCardType();
        final CardType cardType1 = new CardType();
        cardType1.setId(WebCoreConstants.CARD_TYPE_SMART_CARD);
        cardType1.setName(SMART_CARD);
        customerCardType1.setCardType(cardType1);
        customerCardType1.setName(SMART_CARD);
        customerCardType1.setIsLocked(true);
        customerCardType1.setCustomer(this.customer);
        this.entityDao.save(customerCardType1);
        
        final WebSecurityForm<BannedCardUploadForm> webSecurityForm = new WebSecurityForm<>();
        final BannedCardUploadForm form = new BannedCardUploadForm();
        
        final MockMultipartFile file = new MockMultipartFile("file",
                ("CardType,CardNumber,InternalKey,ExpiryDate,Comment\n" + "Credit Card,5454545454545454,,0921,This is a test comment\n"
                 + "Passcard,123456,,,This is a test comment\n" + "Smart Card,111444666,,,This is a test comment").getBytes());
        form.setFile(file);
        form.setRecordStatus("Merge");
        webSecurityForm.setWrappedObject(form);
        
        this.cardManagementController.importBannedCard(webSecurityForm, this.bindingResult, new MockHttpServletRequest(),
                                                       new MockHttpServletResponse(), new ModelMap());
        
        assertEquals(StandardConstants.CONSTANT_3, this.producer.history().size());
        
        for (ProducerRecord<String, String> message : this.producer.history()) {
            
            final String messageBody = message.value();
            final BadCardMessage badCard = this.json.deserialize(messageBody, BadCardMessage.class);
            
            if (badCard.getCardParentType().equals(CREDIT_CARD)) {
                assertEquals("0921", badCard.getCardExpiry());
                assertEquals(SHA1HASH, badCard.getSha1CardHash());
                assertEquals(SHA2HASH, badCard.getSha256CardHash());
                assertEquals(MASTER_CARD, badCard.getCardType());
                assertEquals(CREDIT_CARD, badCard.getCardParentType());
                assertEquals(COMMENT, badCard.getComment());
            } else if (badCard.getCardParentType().equals(PASSCARD)) {
                assertNull(badCard.getCardExpiry());
                assertEquals(SHA1HASH, badCard.getSha1CardHash());
                assertEquals(SHA2HASH, badCard.getSha256CardHash());
                assertEquals(PASSCARD, badCard.getCardType());
                assertEquals(PASSCARD, badCard.getCardParentType());
                assertEquals(COMMENT, badCard.getComment());
            } else if (badCard.getCardParentType().equals(SMART_CARD)) {
                assertNull(badCard.getCardExpiry());
                assertEquals(SHA1HASH, badCard.getSha1CardHash());
                assertEquals(SHA2HASH, badCard.getSha256CardHash());
                assertEquals(SMART_CARD, badCard.getCardType());
                assertEquals(SMART_CARD, badCard.getCardParentType());
                assertEquals(COMMENT, badCard.getComment());
            } else {
                fail("Message with unknown parent card type produced to Kafka" + badCard.getCardParentType());
            }
        }
        final Collection<CustomerBadCard> customerBadCards = this.customerBadCardService.findCustomerBadCardByCustomerId(1);
        assertTrue(customerBadCards.isEmpty());
    }
    
    @Test
    public void testImportCustomCardsBadData() throws CryptoException, JsonException {
        final WebSecurityForm<BannedCardUploadForm> webSecurityForm = new WebSecurityForm<>();
        final BannedCardUploadForm form = new BannedCardUploadForm();
        
        final MockMultipartFile file = new MockMultipartFile("file",
                ("CardType,CardNumber,InternalKey,ExpiryDate,Comment\n" + "Credit Card,,,0920,TestComment\n"
                 + "Credit Card,5454545454545ASJKDHAKS454,,0921,TestComment1\n" + "Credit Card,5454545454545454545454545454545454,,0921,TestComment1")
                         .getBytes());
        form.setFile(file);
        webSecurityForm.setWrappedObject(form);
        
        this.cardManagementController.importBannedCard(webSecurityForm, this.bindingResult, new MockHttpServletRequest(),
                                                       new MockHttpServletResponse(), new ModelMap());
        
        assertEquals(0, this.producer.history().size());
        final Collection<CustomerBadCard> customerBadCards = this.customerBadCardService.findCustomerBadCardByCustomerId(1);
        assertTrue(customerBadCards.isEmpty());
        
        assertFalse(webSecurityForm.getValidationErrorInfo().getErrorStatus().isEmpty());
        
    }
    
    @Test
    public void testDeleteLinkBadCard() throws CryptoException, JsonException {
        
        final MockHttpServletRequest request = new MockHttpServletRequest();
        final MockHttpServletResponse response = new MockHttpServletResponse();
        final ModelMap model = new ModelMap();
        
        final String paramBadCardUUID = "A94E775C-45B3-FFC6-56C5-68CBE95C1283";
        request.setParameter("customerBadCardID", paramBadCardUUID);
        
        this.cardManagementController.deleteBannedCard(request, response, model);
        
        assertFalse(this.producer.history().isEmpty());
        
        final ProducerRecord<String, String> message = this.producer.history().get(0);
        final String messageBody = message.value();
        final BadCardMessage badCard = this.json.deserialize(messageBody, BadCardMessage.class);
        
        assertEquals(paramBadCardUUID, badCard.getId());
    }
    
    @Test
    public void testDeleteIrisBadCard() throws CryptoException, JsonException {
        
        final CustomerBadCard customerBadCard = new CustomerBadCard();
        customerBadCard.setId(TEST_CUSTOMER_BAD_CARD_ID);
        customerBadCard.setCardNumberOrHash("+K5JoaeB/0TmMcGM5z6JP8pGjO0=");
        customerBadCard.setLast4digitsOfCardNumber((short) 1281);
        customerBadCard.setCardData("006001F6MSF/CbWNrgPxnMmWAb145lGlFsrv7ZmbG2GR5mwZ8=");
        customerBadCard.setCardExpiry((short) 2801);
        customerBadCard.setSource("manual");
        customerBadCard.setComment("4785345791451281 - admin@Oranj");
        this.entityDao.save(customerBadCard);
        
        final MerchantAccount notLinkMerchantAccount = this.entityDao.get(MerchantAccount.class, this.customer.getId());
        notLinkMerchantAccount.setIsLink(false);
        this.entityDao.saveOrUpdate(notLinkMerchantAccount);
        
        final MockHttpSession session = new MockHttpSession();
        final MockHttpServletResponse response = new MockHttpServletResponse();
        final ModelMap model = new ModelMap();
        
        final MockHttpServletRequest request = new MockHttpServletRequest();
        request.setSession(session);
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        request.setParameter("customerBadCardID", keyMapping.getRandomString(CustomerBadCard.class, TEST_CUSTOMER_BAD_CARD_ID));
        
        this.cardManagementController.deleteBannedCard(request, response, model);
        
        final Collection<CustomerBadCard> customerBadCards = this.customerBadCardService.findCustomerBadCardByCustomerId(TEST_CUSTOMER_BAD_CARD_ID);
        
        assertTrue(customerBadCards.isEmpty());
    }
}
