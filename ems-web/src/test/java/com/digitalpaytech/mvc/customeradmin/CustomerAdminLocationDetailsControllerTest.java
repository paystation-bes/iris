package com.digitalpaytech.mvc.customeradmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.CaseLocationLot;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerType;
import com.digitalpaytech.domain.FlexLocationFacility;
import com.digitalpaytech.domain.FlexLocationProperty;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.PermitIssueType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.dto.PointOfSaleSearchCriteria;
import com.digitalpaytech.dto.customeradmin.LocationTree;
import com.digitalpaytech.dto.customeradmin.PaystationListInfo;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.mvc.customeradmin.support.LocationEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.service.CaseService;
import com.digitalpaytech.service.FlexWSService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.FlexWSServiceImpl;
import com.digitalpaytech.service.impl.LocationServiceImpl;
import com.digitalpaytech.service.impl.PermitIssueTypeServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.CaseServiceTestImpl;
import com.digitalpaytech.testing.services.impl.CustomerAdminServiceTestImpl;
import com.digitalpaytech.testing.services.impl.LocationLineGeopointServiceTestImpl;
import com.digitalpaytech.testing.services.impl.PointOfSaleServiceTestImpl;
import com.digitalpaytech.testing.services.impl.PosEventCurrentServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.customeradmin.LocationValidator;

public class CustomerAdminLocationDetailsControllerTest {
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private ModelMap model;
    private UserAccount userAccount;
    private CustomerAdminLocationDetailsController controller;
    
    @Before
    public void setUp() {
        
        controller = new CustomerAdminLocationDetailsController();
        
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        session = new MockHttpSession();
        this.request.setSession(session);
        this.model = new ModelMap();
        
        userAccount = new UserAccount();
        userAccount.setId(2);
        Set<UserRole> roles = new HashSet<UserRole>();
        UserRole role = new UserRole();
        Role r = new Role();
        Set<RolePermission> rps = new HashSet<RolePermission>();
        
        RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_VIEW_ROUTES);
        rp1.setPermission(permission1);
        
        RolePermission rp2 = new RolePermission();
        rp2.setId(2);
        Permission permission2 = new Permission();
        permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_ROUTES);
        rp2.setPermission(permission2);
        
        RolePermission rp3 = new RolePermission();
        rp3.setId(3);
        Permission permission3 = new Permission();
        permission3.setId(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS);
        rp3.setPermission(permission3);
        
        RolePermission rp4 = new RolePermission();
        rp4.setId(4);
        Permission permission4 = new Permission();
        permission4.setId(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS);
        rp4.setPermission(permission4);
        
        RolePermission rp5 = new RolePermission();
        rp5.setId(5);
        Permission permission5 = new Permission();
        permission5.setId(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS);
        rp5.setPermission(permission5);
        
        RolePermission rp6 = new RolePermission();
        rp6.setId(6);
        Permission permission6 = new Permission();
        permission6.setId(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS);
        rp6.setPermission(permission6);
        
        rps.add(rp1);
        rps.add(rp2);
        rps.add(rp3);
        rps.add(rp4);
        rps.add(rp5);
        rps.add(rp6);
        
        r.setId(3);
        r.setRolePermissions(rps);
        
        role.setId(3);
        role.setRole(r);
        roles.add(role);
        userAccount.setUserRoles(roles);
        
        Customer customer = new Customer();
        CustomerType type = new CustomerType();
        type.setId(2);
        customer.setCustomerType(type);
        customer.setId(2);
        userAccount.setCustomer(customer);
        createAuthentication(userAccount);
    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        user.setIsMigrated(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_ROUTES);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_ROUTES);
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS);
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_PAYSTATIONS);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_PAYSTATIONS);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    //========================================================================================================================
    // "Location Details" tab
    //========================================================================================================================
    
    @Test
    public void test_locationTree() {
        
        controller.setCustomerAdminService(new CustomerAdminServiceTestImpl() {
            public List<Customer> findAllChildCustomers(int parentCustomerId) {
                Customer child = new Customer();
                child.setId(3);
                List<Customer> childList = new ArrayList<Customer>();
                childList.add(child);
                return childList;
            }
        });
        controller.setLocationService(new LocationServiceImpl() {
            public List<Location> findLocationsByCustomerId(Integer customerId) {
                Location location = new Location();
                location.setName("testName");
                location.setId(1);
                List<Location> locationList = new ArrayList<Location>();
                locationList.add(location);
                return locationList;
            }
            
            public LocationTree getLocationTreeByCustomerId(Integer customerId, RandomKeyMapping keyMapping) {
                return new LocationTree(); // Empty LocationTree, no one using this !
            }
        });
        controller.setPointOfSaleService(createPointOfSaleService());
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        controller.setPosEventCurrentService(new PosEventCurrentServiceTestImpl());
        
        String result = controller.locationList(request, response, model);
        assertNotNull(result);
        assertEquals(result, "/settings/locations/locationDetails");
        assertNotNull(model.get("locations"));
    }
    
    /*
     * @Test
     * public void test_getLocationEditForm(){
     * controller.setPointOfSaleService(new PointOfSaleServiceTestImpl(){
     * public List<Paystation> findPayStationsByCustomerId(int customerId){
     * List<Paystation> payStations = new ArrayList<Paystation>();
     * Paystation payStation = new Paystation();
     * payStation.setId(1);
     * return payStations;
     * }
     * });
     * String returnValue = controller.getLocationEditForm(request, response, model);
     * assertEquals("/settings/location/include/form",returnValue);
     * assertNotNull(model.get("locationEditForm"));
     * }
     */
    
    @Test
    public void test_getLocationDetails() {
        
        Location location = new Location();
        location.setId(1);
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String randomKey = keyMapping.getRandomString(location, "id");
        request.setParameter("locationID", randomKey);
        
        controller.setLocationService(new LocationServiceImpl() {
            public Location findLocationById(int locationId) {
                Location location = new Location();
                location.setId(2);
                location.setIsParent(true);
                location.setName("Test location");
                location.setDescription("Test description");
                location.setPermitIssueType(new PermitIssueType(0));
                return location;
            }
            
            public List<Location> getLocationsByParentLocationId(int parentLocationId) {
                return null;
            }
            
            public List<Location> findLocationsByCustomerId(Integer customerId) {
                Location location = new Location();
                List<Location> locationList = new ArrayList<Location>();
                locationList.add(location);
                return locationList;
            }
        });
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            @Override
            public List<PointOfSale> findPointOfSalesByCustomerId(int customerId) {
                return null;
            }
            
            @Override
            public List<PointOfSale> findValidPointOfSaleByCustomerIdOrderByName(int customerId) {
                return null;
            }
        });
        controller.setPermitIssueTypeService(new PermitIssueTypeServiceImpl() {
            @Override
            public PermitIssueType findPermitIssueTypeById(Integer permitIssueTypeId) {
                return new PermitIssueType(0);
            }
        });
        controller.setFlexWSService(this.createFlexWSService());
        controller.setCaseService(createCaseService());
        controller.setLocationLineGeopointService(new LocationLineGeopointServiceTestImpl());
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        String returnValue = controller.getLocationDetails(request, response, model);
        assertTrue(returnValue.startsWith("{\"locationFormDetails\":{\"isParent\":true,\"isUnassigned\":false,\"randomId\":\"" + randomKey
                                          + "\",\"name\":\"Test location\",\"description\":\"Test description\""));
    }
    
    @Test
    public void test_verifyAndSaveLocationEditForm() {
        
        LocationEditForm form = createValidLocationTestForm();
        
        BindingResult result = new BeanPropertyBindingResult(form, "locationEditForm");
        WebSecurityForm<LocationEditForm> webSecurityForm = new WebSecurityForm<LocationEditForm>(null, form);
        webSecurityForm.setPostToken(webSecurityForm.getInitToken());
        model.put("locationEditForm", webSecurityForm);
        
        controller.setCustomerAdminService(new CustomerAdminServiceTestImpl() {
            public Customer findCustomerByCustomerId(int customerId) {
                Customer child = new Customer();
                child.setId(3);
                return child;
            }
        });
        controller.setLocationValidator(new LocationValidator() {
            @Override
            public void validate(WebSecurityForm<LocationEditForm> command, Errors errors, boolean isMigrated, int customerId) {
            }
        });
        controller.setLocationService(new LocationServiceImpl() {
            @Override
            public void updateLocationOpenCloseTime(int userAccountId, int locationId, int dayOfWeek, Integer openTime, Integer closeTime,
                Integer closeTimeOfPreviousDay, boolean open, boolean closed) {
            }
            
            @Override
            public void saveOrUpdateLocation(Location location) {
                location.setId(1);
            }
            
            @Override
            public List<Location> getLocationsByParentLocationId(int parentLocationId) {
                return new ArrayList<Location>();
            }
        });
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            @Override
            public List<PointOfSale> findPointOfSalesByLocationId(int id) {
                return null;
            }
            
            @Override
            public List<PointOfSale> findPointOfSalesByLocationIdOrderByName(int id) {
                return null;
            }
            
            //            @Override
            //            public List<PointOfSale> findValidPointOfSaleByCustomerIdOrderByName(int id) {
            //                return null;
            //            }
        });
        controller.setPermitIssueTypeService(new PermitIssueTypeServiceImpl() {
            @Override
            public PermitIssueType findPermitIssueTypeById(Integer permitIssueTypeId) {
                return new PermitIssueType(0);
            }
        });
        
        controller.setCaseService(this.createCaseService());
        controller.setFlexWSService(this.createFlexWSService());
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        String returnValue = controller.saveLocation((WebSecurityForm<LocationEditForm>) model.get("locationEditForm"), result, request, response,
                                                     model);
        assertTrue(returnValue.contains(WebCoreConstants.RESPONSE_TRUE));
    }
    
    @Test
    public void test_verifyDeleteLocation() {
        
        controller.setLocationService(new LocationServiceImpl() {
            @Override
            public Location findLocationById(int locationId) {
                return new Location();
            }
            
            @Override
            public List<Location> getLocationsByParentLocationId(int parentLocationId) {
                return new ArrayList<Location>();
            }
            
            @Override
            public boolean locationContainsPermitsById(int locationId) {
                return false;
            }
        });
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            @Override
            public List<PointOfSale> findPointOfSalesByLocationId(int id) {
                return null;
            }
            
            @Override
            public List<PointOfSale> findPointOfSalesByLocationIdOrderByName(int id) {
                return null;
            }
        });
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        Location location = new Location();
        location.setId(1);
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String randomKey = keyMapping.getRandomString(location, "id");
        request.setParameter("locationID", randomKey);
        
        String returnValue = controller.verifyDeleteLocation(request, response, model);
        assertEquals(WebCoreConstants.RESPONSE_TRUE, returnValue);
    }
    
    @Test
    public void test_DeleteLocation() {
        
        controller.setLocationService(new LocationServiceImpl() {
            @Override
            public Location findLocationById(int locationId) {
                return new Location();
            }
            
            @Override
            public List<Location> getLocationsByParentLocationId(int parentLocationId) {
                return new ArrayList<Location>();
            }
            
            @Override
            public boolean locationContainsPermitsById(int locationId) {
                return false;
            }
            
            @Override
            public void deleteLocationById(int locationId) {
            }
        });
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            @Override
            public List<PointOfSale> findPointOfSalesByLocationId(int id) {
                return null;
            }
            
            @Override
            public List<PointOfSale> findPointOfSalesByLocationIdOrderByName(int id) {
                return null;
            }
        });
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        Location location = new Location();
        location.setId(1);
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String randomKey = keyMapping.getRandomString(location, "id");
        request.setParameter("locationID", randomKey);
        
        String returnValue = controller.deleteLocation(request, response, model);
        assertEquals(WebCoreConstants.RESPONSE_TRUE, returnValue);
    }
    
    private LocationEditForm createValidLocationTestForm() {
        LocationEditForm form = new LocationEditForm();
        
        form.setCapacity("100");
        form.setTargetMonthlyRevenue("1000");
        form.setName("Location name");
        form.setDescription("Location description");
        form.setIsParent(false);
        form.setRandomId(null); //null signifies new location
        form.setParentLocation(null);
        form.setParentLocationId(null);
        form.setPayStations(null);
        form.setChildLocations(null);
        form.setLocationOpen(Arrays.asList(16, 16, 16, 16, 16, 16, 16));
        form.setLocationClose(Arrays.asList(87, 87, 87, 87, 87, 87, 87));
        form.setOpenEntireDay(Arrays.asList(false, false, false, false, false, false, false));
        form.setClosedEntireDay(Arrays.asList(false, false, false, false, false, false, false));
        
        return form;
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
        };
    }
    
    private PointOfSaleService createPointOfSaleService() {
        return new PointOfSaleServiceTestImpl() {
            @Override
            public List<PointOfSale> findPointOfSalesByCustomerId(int customerId) {
                List<PointOfSale> result = new ArrayList<PointOfSale>();
                PointOfSale pos = new PointOfSale();
                Location loc = new Location();
                loc.setId(1);
                loc.setName("TestLoc");
                pos.setId(1);
                pos.setName("TestPos");
                pos.setLocation(loc);
                result.add(pos);
                
                return result;
            }
            
            @Override
            public List<PointOfSale> findValidPointOfSaleByCustomerIdOrderByName(int customerId) {
                List<PointOfSale> result = new ArrayList<PointOfSale>();
                PointOfSale pos = new PointOfSale();
                Location loc = new Location();
                loc.setId(1);
                loc.setName("TestLoc");
                pos.setId(1);
                pos.setName("TestPos");
                pos.setLocation(loc);
                result.add(pos);
                
                return result;
            }
            
            @Override
            public int countUnPlacedByRouteIds(Collection<Integer> routeIds) {
                return 0;
            }
            
            @Override
            public List<PaystationListInfo> findPaystation(PointOfSaleSearchCriteria criteria) {
                ArrayList<PaystationListInfo> result = new ArrayList<PaystationListInfo>();
                
                return result;
            }
            
            @Override
            public PlacementMapBordersInfo findMapBordersByCustomerId(Integer customerId) {
                return null;
            }
            
        };
    }
    
    private FlexWSService createFlexWSService() {
        return new FlexWSServiceImpl() {
            @Override
            public List<FlexLocationFacility> findFlexLocationFacilityByLocationId(Integer locationId) {
                List<FlexLocationFacility> result = new ArrayList<FlexLocationFacility>();
                FlexLocationFacility flf = new FlexLocationFacility();
                Location loc = new Location();
                loc.setId(1);
                loc.setName("TestLoc");
                flf.setId(1);
                flf.setFacCode("TestPos");
                flf.setFacDescription("Test Pos");
                flf.setLocation(loc);
                result.add(flf);
                
                return result;
            }
            
            public List<FlexLocationFacility> findUnselectedFlexLocationFacilityByCustomerId(final Integer customerId, final List<Integer> locationIds) {
                List<FlexLocationFacility> result = new ArrayList<FlexLocationFacility>();
                FlexLocationFacility flf = new FlexLocationFacility();
                Location loc = new Location();
                loc.setId(1);
                loc.setName("TestLoc");
                flf.setId(1);
                flf.setFacCode("TestPos");
                flf.setFacDescription("Test Pos");
                flf.setLocation(loc);
                result.add(flf);
                
                return result;
            }
            
            public List<FlexLocationProperty> findUnselectedFlexLocationPropertyByCustomerId(final Integer customerId, final List<Integer> locationIds) {
                List<FlexLocationProperty> result = new ArrayList<FlexLocationProperty>();
                FlexLocationProperty flp = new FlexLocationProperty();
                Location loc = new Location();
                loc.setId(1);
                loc.setName("TestLoc");
                flp.setId(1);
                flp.setProUid(1);
                flp.setProName("TestPos");
                flp.setLocation(loc);
                result.add(flp);
                
                return result;
            }
            
            public final List<FlexLocationProperty> findFlexLocationPropertyByLocationId(final Integer locationId) {
                List<FlexLocationProperty> result = new ArrayList<FlexLocationProperty>();
                FlexLocationProperty flp = new FlexLocationProperty();
                Location loc = new Location();
                loc.setId(1);
                loc.setName("TestLoc");
                flp.setId(1);
                flp.setProUid(1);
                flp.setProName("TestPos");
                flp.setLocation(loc);
                result.add(flp);
                
                return result;
            }
            
            public void updateFlexLocationProperty(final FlexLocationProperty flexLocationProperty) {
                
            }
            
            public void updateFlexLocationFacility(final FlexLocationFacility flexLocationFacility) {
            }
            
            public void saveFlexLocationProperty(final FlexLocationProperty flexLocationProperty) {
            }
            
        };
    }
    
    private CaseService createCaseService() {
        return new CaseServiceTestImpl() {
            @Override
            public List<CaseLocationLot> findCaseLocationLotByLocationId(final Integer locationId) {
                // TODO Auto-generated method stub
                return null;
            }
        };
    }
}
