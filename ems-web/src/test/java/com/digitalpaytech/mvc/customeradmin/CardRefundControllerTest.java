package com.digitalpaytech.mvc.customeradmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.CreditCardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerType;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.PaymentType;
import com.digitalpaytech.domain.PaystationSetting;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.ProcessorTransactionType;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.TransactionType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.dto.FilterDTO;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.MerchantAccountService;
import com.digitalpaytech.service.PaymentCardService;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.service.PurchaseService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.PaymentCardServiceImpl;
import com.digitalpaytech.service.impl.ProcessorTransactionServiceImpl;
import com.digitalpaytech.service.impl.PurchaseServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

public class CardRefundControllerTest {
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private ModelMap model;
    private UserAccount userAccount;
    
    private static final String NO_PERMISSION_RESULT = "redirect:/secure/settings/global/index.html";
    
    @Before
    public void setUp() throws Exception {
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        session = new MockHttpSession();
        this.request.setSession(session);
        this.model = new ModelMap();
        
        userAccount = new UserAccount();
        userAccount.setId(2);
        Set<UserRole> roles = new HashSet<UserRole>();
        UserRole role = new UserRole();
        Role r = new Role();
        Set<RolePermission> rps = new HashSet<RolePermission>();
        
        RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_ISSUE_REFUNDS);
        rp1.setPermission(permission1);
        
        rps.add(rp1);
        
        r.setId(3);
        r.setRolePermissions(rps);
        
        role.setId(3);
        role.setRole(r);
        roles.add(role);
        userAccount.setUserRoles(roles);
        
        Customer customer = new Customer();
        customer.setId(2);
        CustomerType customerType = new CustomerType();
        customerType.setId(3);
        customer.setCustomerType(customerType);
        customer.setIsMigrated(true);
        userAccount.setCustomer(customer);
        createAuthentication(userAccount);
    }
    
    @After
    public void tearDown() throws Exception {
        request = null;
        response = null;
        session = null;
        model = null;
        userAccount = null;
    }
   
    
    @Test
    public void test_showRefundDetails_permission_fail() {
        createFailedAuthentication();
        CardRefundController controller = new CardRefundController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        //		CardRefundSearchForm cardRefundForm = new CardRefundSearchForm();
        //        BindingResult bindingResult = new BeanPropertyBindingResult(cardRefundForm, "cardRefundForm");
        String result = controller.showRefundDetails(request, response, model, controller.initRefundForm(request));
        
        assertEquals(result, WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS);
    }
    
    @Test
    public void test_showRefundDetails_invalid_request_parameter() {
        CardRefundController controller = new CardRefundController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        request.setParameter("ptID", "111234");
        
        //		CardRefundSearchForm cardRefundForm = new CardRefundSearchForm();
        //        BindingResult bindingResult = new BeanPropertyBindingResult(cardRefundForm, "cardRefundForm");
        String result = controller.showRefundDetails(request, response, model, controller.initRefundForm(request));
        
        assertNull(result);
        assertEquals(response.getStatus(), HttpStatus.BAD_REQUEST.value());
    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        user.setIsMigrated(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_ISSUE_REFUNDS);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private void createFailedAuthentication() {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        user.setIsMigrated(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(2);
        permissionList.add(3);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private EntityService createEntityService() {
        return new EntityServiceImpl() {
            @Override
            public Object merge(Object entity) {
                return userAccount;
            }
        };
    }
    
    private MerchantAccountService createMerchantAccountService() {
        return new MerchantAccountServiceImpl() {
            @Override
            public List<MerchantAccount> findRefundableMerchantAccounts(Integer customerId) {
                List<MerchantAccount> result = new ArrayList<MerchantAccount>();
                MerchantAccount m = new MerchantAccount();
                m.setId(1);
                m.setCustomer(userAccount.getCustomer());
                result.add(m);
                return result;
            }
            
            @Override
            public List<FilterDTO> getRefundableMerchantAccountFiltersByCustomerId(List<FilterDTO> result, int customerId, RandomKeyMapping keyMapping) {
                return result;
            }
        };
    }
    
    private ProcessorTransactionService createProcessorTransactionService() {
        return new ProcessorTransactionServiceImpl() {
            @Override
            public ProcessorTransaction findTransactionPosPurchaseById(Long id) {
                ProcessorTransaction pt = new ProcessorTransaction();
                ProcessorTransactionType ptt = new ProcessorTransactionType();
                Purchase p = new Purchase();
                p.setId((long) 3);
                ptt.setId(2);
                ptt.setName("Real-Time");
                pt.setPurchase(p);
                pt.setPurchasedDate(new Date());
                pt.setProcessorTransactionType(ptt);
                pt.setAuthorizationNumber("TestAuth000001");
                pt.setReferenceNumber("REF1000003");
                pt.setProcessingDate(new Date());
                pt.setLast4digitsOfCardNumber((short) 5973);
                return pt;
            }
        };
    }
    
    private PaymentCardService createPaymentCardService() {
        return new PaymentCardServiceImpl() {
            @Override
            public PaymentCard findPaymentCardByPurchaseIdAndProcessorTransactionId(Long purchaseId, Long processorTransactionId) {
                PaymentCard pc = new PaymentCard();
                CreditCardType type = new CreditCardType();
                type.setId(2);
                type.setName("Amex");
                pc.setId((long) 3);
                
                Purchase p = new Purchase();
                pc.setPurchase(p);
                pc.setCreditCardType(type);
                return pc;
            }
        };
    }
    
    private PurchaseService createPurchaseService() {
        return new PurchaseServiceImpl() {
            @Override
            public Purchase findPurchaseById(Long id, boolean isEvict) {
                Purchase p = new Purchase();
                PaystationSetting ls = new PaystationSetting();
                TransactionType ttype = new TransactionType();
                PaymentType ptype = new PaymentType();
                
                ls.setId(1);
                ls.setName("TestLot1");
                p.setPaystationSetting(ls);
                ttype.setId(2);
                ttype.setName("Regular");
                p.setTransactionType(ttype);
                ptype.setId((byte) 2);
                ptype.setName("Credit Card");
                p.setPaymentType(ptype);
                p.setChargedAmount(250);
                p.setCashPaidAmount(100);
                p.setCardPaidAmount(150);
                return p;
            }
        };
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            @Override
            public UserAccount findUserAccount(int userAccountId) {
                UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
        };
    }
}
