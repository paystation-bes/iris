package com.digitalpaytech.mvc.customeradmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.CollectionSummaryConfig;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerPropertyType;
import com.digitalpaytech.domain.EventSeverityType;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PaystationType;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.RoutePOS;
import com.digitalpaytech.domain.RouteType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.dto.SummarySearchCriteria;
import com.digitalpaytech.dto.customeradmin.CollectionSummaryEntry;
import com.digitalpaytech.dto.customeradmin.LocationRouteFilterInfo;
import com.digitalpaytech.mvc.customeradmin.support.CollectionCentreFilterForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.mvc.support.impl.CommonControllerHelperImpl;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.CollectionSummaryConfigServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.AlertsServiceTestImpl;
import com.digitalpaytech.testing.services.impl.CustomerAdminServiceTestImpl;
import com.digitalpaytech.testing.services.impl.RouteServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.customeradmin.CollectionCentreFilterValidator;

public class CollectionsCentreControllerTest {
    
    public static final String FORM_COLLECTIONS = "collectionsForm";
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private SessionTool sessionTool;
    private ModelMap model;
    private UserAccount userAccount;
    private WebUser webUser;
    private CollectionsCentreController controller;
    private RandomKeyMapping keyMapping;
    private BindingResult result;
    
    
    @Before
    public void setUp() {
        
        this.userAccount = new UserAccount();
        this.controller = new CollectionsCentreController();
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        this.session = new MockHttpSession();
        this.request.setSession(this.session);
        this.model = new ModelMap();
        this.sessionTool = SessionTool.getInstance(this.request);
        this.keyMapping = this.sessionTool.getKeyMapping();
        
        final Set<UserRole> roles = new HashSet<UserRole>();
        final UserRole userRole = new UserRole();
        final Role r = new Role();
        final Set<RolePermission> rps = new HashSet<RolePermission>();
        
        final RolePermission rp1 = new RolePermission();
        rp1.setId(WebSecurityConstants.ROLE_ID_SYSTEM_ADMIN);
        final Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_VIEW_COLLECTION_STATUS);
        rp1.setPermission(permission1);
        
        rps.add(rp1);
        
        r.setId(WebSecurityConstants.ROLE_ID_CHILD_ADMIN);
        r.setRolePermissions(rps);
        
        userRole.setId(3);
        userRole.setRole(r);
        roles.add(userRole);
        this.userAccount.setUserRoles(roles);
        
        final Customer customer = new Customer();
        customer.setId(2);
        this.userAccount.setCustomer(customer);
        createAuthentication(userAccount);
        this.userAccount.setId(2);
        
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        webUser = new WebUser(2, 3, "testUser", "password", "salt", true, authorities);
        Authentication authentication = new UsernamePasswordAuthenticationToken(webUser, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_COLLECTION_STATUS);
        webUser.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        
        webUser.setCustomerTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT);
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        WidgetMetricsHelper.registerComponents();
        
        controller.setCustomerAdminService(new CustomerAdminServiceTestImpl() {
            public List<CustomerProperty> getCustomerPropertyByCustomerId(int customerId) {
                List<CustomerProperty> customerProperties = new ArrayList<CustomerProperty>();
                CustomerProperty property = new CustomerProperty();
                CustomerPropertyType propertyType = new CustomerPropertyType();
                propertyType.setId(WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
                property.setCustomerPropertyType(propertyType);
                property.setPropertyValue(WebCoreConstants.SERVER_TIMEZONE_GMT);
                customerProperties.add(property);
                return customerProperties;
            }
        });
        controller.setAlertsService(new AlertsServiceTestImpl() {
            
            public List<CollectionSummaryEntry> findPointOfSalesWithBalanceAndAlertSeverity(SummarySearchCriteria criteria, String timeZone) {
                return createCollectionSummaryEntryList();
            }
            
            public List<EventSeverityType> findAllEventSeverityTypes() {                
                List<EventSeverityType> severityTypeList = new ArrayList<EventSeverityType>();
                EventSeverityType severityTypeMin = new EventSeverityType();
                severityTypeMin.setName("Minor");
                severityTypeMin.setId(WebCoreConstants.SEVERITY_MINOR);
                severityTypeList.add(severityTypeMin);
                EventSeverityType severityTypeMaj = new EventSeverityType();
                severityTypeMaj.setName("Major");
                severityTypeMaj.setId(WebCoreConstants.SEVERITY_MAJOR);
                severityTypeList.add(severityTypeMaj);
                EventSeverityType severityTypeCrit = new EventSeverityType();
                severityTypeCrit.setName("Critical");
                severityTypeCrit.setId(WebCoreConstants.SEVERITY_CRITICAL);
                severityTypeList.add(severityTypeCrit);
                EventSeverityType severityTypeClear = new EventSeverityType();
                severityTypeClear.setName("Clear");
                severityTypeClear.setId(WebCoreConstants.SEVERITY_CLEAR);
                severityTypeList.add(severityTypeClear);
                return severityTypeList;
            }
            
        });
        controller.setCollectionSummaryConfigService(new CollectionSummaryConfigServiceImpl() {
            
            public CollectionSummaryConfig getCollectionSummaryConfigByUserId(int userAccountId)  {
                return createCollectionSummaryConfig();
            }
                      
        });

        controller.setRouteService(new RouteServiceTestImpl() {
            public Collection<Route> findRoutesByCustomerIdAndRouteTypeId(int customerId, int routeTypeId) {
                return null;
            }
        });

        controller.setCollectionCentreFilterValidator(new CollectionCentreFilterValidator() {
            public void validate(WebSecurityForm<CollectionCentreFilterForm> command, Errors errors) {
                return;
            }
        });
        
        controller.setCommonControllerHelper(new CommonControllerHelperImpl() {
            public LocationRouteFilterInfo createLocationRouteFilter(Integer customerId, RandomKeyMapping keyMapping, Integer routeTypeId) {
                LocationRouteFilterInfo locationRouteFilter = new LocationRouteFilterInfo();
                return locationRouteFilter;
            }
        });
         
    }
    

    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    } 
    
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
        };
    }    
        
    protected List<PointOfSale> createPointOfSaleList() {
        
        List<PointOfSale> pointOfSales = new ArrayList<PointOfSale>();
        PointOfSale pointOfSale = new PointOfSale();
        
        Paystation payStation = new Paystation();
        PaystationType payStationType = new PaystationType();
        payStationType.setId(1);
        payStation.setPaystationType(payStationType);
        pointOfSale.setPaystation(payStation);
        
        pointOfSale.setId(1);
        pointOfSale.setName("POS Name");
        pointOfSale.setSerialNumber("123456789abcdefg");
        pointOfSale.setLatitude(new BigDecimal(100));
        pointOfSale.setLongitude(new BigDecimal(100));
        
        Location location = new Location();
        location.setName("Location 1");
        
        pointOfSale.setLocation(location);
        
        Set<RoutePOS> routePOSes = new HashSet<RoutePOS>(createRoutePOSList());
        pointOfSale.setRoutePOSes(routePOSes);
        
        pointOfSales.add(pointOfSale);
        return pointOfSales;
    }
    
    protected List<CollectionSummaryEntry> createCollectionSummaryEntryList() {
        List<CollectionSummaryEntry> cseList = new ArrayList<CollectionSummaryEntry>();
        CollectionSummaryEntry cse = new CollectionSummaryEntry();
        PointOfSale pos = new PointOfSale();        
        pos.setId(1);
        cse.setPointOfSaleName("Paystation 1");
        cse.setPosRandomId(keyMapping.getKeyObject(keyMapping.getRandomString(pos, "id")));
        List<Route> rl = new ArrayList<Route>();
        Route route = new Route();
        route.setId(1);
        route.setName("Route1");
        rl.add(route);
        cse.setRoutes(rl);
        cse.setSerial("394923842913");
        cse.setSeverity(WebCoreConstants.SEVERITY_MAJOR);
        return cseList;
    }
    
    
    protected List<RoutePOS> createRoutePOSList() {
        List<RoutePOS> routePOSes = new ArrayList<RoutePOS>();
        RoutePOS routePOS = new RoutePOS();
        RouteType routeType = new RouteType();
        routeType.setId(WebCoreConstants.ROUTE_TYPE_MAINTENANCE);
        Route route = new Route();
        route.setName("Maintenance route");
        route.setRouteType(routeType);
        routePOS.setRoute(route);
        routePOSes.add(routePOS);
        return routePOSes;
    }
    
    private CollectionSummaryConfig createCollectionSummaryConfig() {
        CollectionSummaryConfig csc = new CollectionSummaryConfig();
        csc.setBillCountVisible(true);
        csc.setBillTotalVisible(false);
        csc.setCardCountVisible(true);
        csc.setCardTotalVisible(true);
        csc.setCoinCountVisible(true);
        csc.setCoinTotalVisible(false);
        csc.setLocationVisible(true);
        csc.setRouteVisible(true);
        return csc;
    }
  
    
    @Test
    public void test_getPosSummary() {
        
        String resp = controller.getPosSummary(request, response, model, controller.initCollectionsForm(request));
        
        
        assertEquals("/collections/summary", resp);
        
        Object moduleFilter = model.get("severityFilter");
        Object locationFilter = model.get("locationFilter");
        Object routeFilter = model.get("routeFilter");
        
        assertNotNull(moduleFilter);
        assertNotNull(locationFilter);
        assertNotNull(routeFilter);
    }
    
    @Test
    public void test_getPosSummaryList() {
        
        EventSeverityType evt = new EventSeverityType();
        evt.setId(WebCoreConstants.SEVERITY_MAJOR);
        
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        String randomKey = keyMapping.getRandomString(evt, "id");
        
        CollectionCentreFilterForm form = new CollectionCentreFilterForm();
        form.setRandomId(randomKey);        
        form.setSeverity(true);
        
        WebSecurityForm<CollectionCentreFilterForm> webSecurityForm = new WebSecurityForm<CollectionCentreFilterForm>(null, form);
        result = new BeanPropertyBindingResult(form, FORM_COLLECTIONS);
        String resp = controller.getPosSummaryList(webSecurityForm, result, request, response, model);
        assertTrue(resp.contains("collectionsList"));
    }    
    
}
