package com.digitalpaytech.mvc.customeradmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.data.RolePermissionInfo;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerRole;
import com.digitalpaytech.domain.CustomerType;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.RoleStatusType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.domain.UserStatusType;
import com.digitalpaytech.dto.customeradmin.RoleSettingInfo;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.RoleEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.CustomerRoleServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.PermissionServiceImpl;
import com.digitalpaytech.service.impl.RoleServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.CustomerServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObject;
import com.digitalpaytech.validation.RoleSettingValidator;

public class CustomerAdminRoleSettingControllerTest {
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private ModelMap model;
    private UserAccount userAccount;
    
    @Before
    public void setUp() throws Exception {
        
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        session = new MockHttpSession();
        this.request.setSession(session);
        this.model = new ModelMap();
        
        userAccount = new UserAccount();
        userAccount.setId(2);
        
        UserStatusType ust1 = new UserStatusType();
        ust1.setId(2);
        userAccount.setUserStatusType(ust1);
        
        Set<UserRole> roles = new HashSet<UserRole>();
        UserRole role = new UserRole();
        Role r = new Role();
        Set<RolePermission> rps = new HashSet<RolePermission>();
        
        RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
        rp1.setPermission(permission1);
        
        RolePermission rp2 = new RolePermission();
        rp2.setId(2);
        Permission permission2 = new Permission();
        permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
        rp2.setPermission(permission2);
        
        rps.add(rp1);
        rps.add(rp2);
        
        r.setId(3);
        r.setRolePermissions(rps);
        
        role.setId(3);
        role.setRole(r);
        roles.add(role);
        userAccount.setUserRoles(roles);
        
        Customer customer = new Customer();
        customer.setId(2);
        userAccount.setCustomer(customer);
        createAuthentication(userAccount);
    }
    
// TODO
// It's broken.    
    @Test
    public void test_getRoleList() {
        CustomerAdminRoleSettingController controller = new CustomerAdminRoleSettingController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setEntityService(createEntityService());
        util.setUserAccountService(createStaticUserAccountService());
        controller.setCustomerRoleService(new CustomerRoleServiceImpl() {
            public Collection<CustomerRole> findCustomerRoleByCustomerId(Integer customerId) {
                Collection<CustomerRole> roles = new ArrayList<CustomerRole>();
                CustomerRole role1 = new CustomerRole();
                role1.setId(1);
                Role r1 = new Role();
                r1.setId(3);
                r1.setName("Child Admin");
                role1.setRole(r1);
                RoleStatusType type = new RoleStatusType();
                type.setId(1);
                r1.setRoleStatusType(type);
                r1.setCustomerType(new CustomerType(3, "Test", new Date(), 1));
                
                CustomerRole role2 = new CustomerRole();
                role2.setId(2);
                Role r2 = new Role();
                r2.setId(4);
                r2.setName("Basic");
                role2.setRole(r2);
                r2.setRoleStatusType(type);
                r2.setCustomerType(new CustomerType(4, "Test2", new Date(), 1));
                
                roles.add(role1);
                roles.add(role2);
                
                return roles;
            }
        });
        
        controller.setRoleService(new RoleServiceImpl() {
            public void updateRoleAndRolePermission(Role role) {
                
            }
            
            public Role findRoleByRoleIdAndEvict(Integer roleId) {
                Role role = new Role();
                Role cloneRole = new Role();
                RoleStatusType type = new RoleStatusType();
                type.setId(1);
                role.setId(3);
                role.setRoleStatusType(type);
                CustomerType customerType = new CustomerType();
                customerType.setId(WebCoreConstants.CUSTOMER_TYPE_CHILD);
                role.setCustomerType(customerType);
                
                return role;
            }
            
            @Override
            public Role findRoleByName(Integer customerId, String roleName) {
                return null;
            }
        });
        
        String result = controller.getRoleList(request, response, model);
        assertNotNull(result);
        assertEquals(result, "/settings/users/roles");
    }
    
    @Test
    public void test_getRoleList_role_fail() {
        
        EntityServiceImpl serv = new EntityServiceImpl() {
            public Object merge(Object entity) {
                return new UserAccount();
            }
        };
        WebSecurityUtil util = new WebSecurityUtil();
        util.setEntityService(serv);
        util.setUserAccountService(createStaticUserAccountService());
        
        createFailedAuthentication();
        
        CustomerAdminRoleSettingController controller = new CustomerAdminRoleSettingController();
        String result = controller.getRoleList(request, response, model);
        assertEquals(result, WebSecurityConstants.NO_PERMISSION_REDIRECT_URI_GLOBAL_SETTINGS);
    }
    
    @Test
    public void test_viewRoleDetails() {
        List<RoleSettingInfo> infos = (List<RoleSettingInfo>) getRoleSettingInfo();
        request.setParameter("roleID", ((RoleSettingInfo) infos.get(1)).getRandomizedRoleId());
        
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        CustomerAdminRoleSettingController controller = new CustomerAdminRoleSettingController();
        
        controller.setRoleService(new RoleServiceImpl() {
            public void updateRoleAndRolePermission(Role role) {
                
            }
            
            public Role findRoleByRoleIdAndEvict(Integer roleId) {
                Role role = new Role();
                Role cloneRole = new Role();
                RoleStatusType type = new RoleStatusType();
                type.setId(1);
                role.setId(3);
                role.setRoleStatusType(type);
                CustomerType customerType = new CustomerType();
                customerType.setId(WebCoreConstants.CUSTOMER_TYPE_CHILD);
                role.setCustomerType(customerType);
                
                return role;
            }
            
            @Override
            public Role findRoleByName(Integer customerId, String roleName) {
                return null;
            }
        });
        
        controller.setCustomerService(createCustomerService());
        controller.setUserAccountService(createUserAccountService());
        
        EntityServiceImpl serv = new EntityServiceImpl() {
            public Object merge(Object entity) {
                
                UserAccount userAccount = new UserAccount();
                Customer customer = new Customer();
                CustomerType type = new CustomerType();
                customer.setId(3);
                type.setId(3);
                customer.setCustomerType(type);
                userAccount.setCustomer(customer);
                userAccount.setId(2);
                
                UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                userAccount.setUserStatusType(ust1);
                
                Set<UserRole> roles = new HashSet<UserRole>();
                UserRole role = new UserRole();
                Role r = new Role();
                Set<RolePermission> rps = new HashSet<RolePermission>();
                
                RolePermission rp1 = new RolePermission();
                rp1.setId(1);
                Permission permission1 = new Permission();
                permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
                rp1.setPermission(permission1);
                
                RolePermission rp2 = new RolePermission();
                rp2.setId(2);
                Permission permission2 = new Permission();
                permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
                rp2.setPermission(permission2);
                
                rps.add(rp1);
                rps.add(rp2);
                
                r.setId(3);
                r.setRolePermissions(rps);
                
                role.setId(3);
                role.setRole(r);
                roles.add(role);
                userAccount.setUserRoles(roles);
                return userAccount;
            }
        };
        controller.setEntityService(serv);
        controller.setPermissionService(new PermissionServiceImpl() {
            public Collection<RolePermissionInfo> findAllPermissionsByRoleId(Integer roleId) {
                RolePermissionInfo info1 = new RolePermissionInfo();
                info1.setRoleId(3);
                info1.setRoleName("Child Admin");
                info1.setStatus(1);
                info1.setParentPermissionName("Alerts Management");
                info1.setChildPermissionId(201);
                info1.setChildPermissionName("View Current Alerts");
                
                RolePermissionInfo info2 = new RolePermissionInfo();
                info2.setRoleId(3);
                info2.setRoleName("Child Admin");
                info2.setStatus(1);
                info2.setParentPermissionName("Collections Management");
                info2.setChildPermissionId(301);
                info2.setChildPermissionName("View Collection Status");
                
                RolePermissionInfo info3 = new RolePermissionInfo();
                info3.setRoleId(3);
                info3.setRoleName("Child Admin");
                info3.setStatus(1);
                info3.setParentPermissionName("Alerts Management");
                info3.setChildPermissionId(203);
                info3.setChildPermissionName("View Maintenance Center");
                
                Collection<RolePermissionInfo> result = new ArrayList<RolePermissionInfo>();
                result.add(info1);
                result.add(info2);
                result.add(info3);
                return result;
            }
            
            public Collection<RolePermissionInfo> findAllPermissionsByCustomerTypeId(Integer customerTypeId) {
                RolePermissionInfo info1 = new RolePermissionInfo();
                info1.setRoleId(3);
                info1.setRoleName("Child Admin");
                info1.setParentPermissionName("Alerts Management");
                info1.setChildPermissionId(201);
                info1.setChildPermissionName("View Current Alerts");
                
                RolePermissionInfo info2 = new RolePermissionInfo();
                info2.setRoleId(3);
                info2.setRoleName("Child Admin");
                info2.setParentPermissionName("Collections Management");
                info2.setChildPermissionId(301);
                info2.setChildPermissionName("View Collection Status");
                
                RolePermissionInfo info3 = new RolePermissionInfo();
                info3.setRoleId(4);
                info3.setRoleName("Child Admin");
                info3.setParentPermissionName("Account Management");
                info3.setChildPermissionId(501);
                info3.setChildPermissionName("View Custom Cards");
                
                Collection<RolePermissionInfo> result = new ArrayList<RolePermissionInfo>();
                result.add(info1);
                result.add(info2);
                result.add(info3);
                return result;
            }
        });
        WebSecurityUtil util = new WebSecurityUtil();
        util.setEntityService(createEntityService());
        util.setUserAccountService(createStaticUserAccountService());
        
        WidgetMetricsHelper.registerComponents();
        String result = controller.viewRoleDetails(request, response);
        assertNotNull(result);
    }
    
    @Test
    public void test_viewRoleDetails_role_fail() {
        
        Set<UserRole> roles = new HashSet<UserRole>();
        UserRole role = new UserRole();
        Role r = new Role();
        
        Set<RolePermission> rps = new HashSet<RolePermission>();
        RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        Permission permission1 = new Permission();
        permission1.setId(23);
        rp1.setPermission(permission1);
        
        rps.add(rp1);
        
        r.setId(3);
        r.setRolePermissions(rps);
        role.setId(2);
        role.setRole(r);
        roles.add(role);
        userAccount.setUserRoles(roles);
        
        CustomerAdminRoleSettingController controller = new CustomerAdminRoleSettingController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setEntityService(createEntityService());
        util.setUserAccountService(createStaticUserAccountService());
        String result = controller.viewRoleDetails(request, response);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_viewRoleDetails_roleID_validation_fail() {
        List<RoleSettingInfo> infos = (List<RoleSettingInfo>) getRoleSettingInfo();
        request.setParameter("roleID", "11111!111111");
        
        CustomerAdminRoleSettingController controller = new CustomerAdminRoleSettingController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setEntityService(createEntityService());
        util.setUserAccountService(createStaticUserAccountService());
        
        String result = controller.viewRoleDetails(request, response);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_viewRoleDetails_actualRoleID_validation_fail() {
        List<RoleSettingInfo> infos = (List<RoleSettingInfo>) getRoleSettingInfo();
        request.setParameter("roleID", "1111111111111111");
        
        CustomerAdminRoleSettingController controller = new CustomerAdminRoleSettingController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setEntityService(createEntityService());
        util.setUserAccountService(createStaticUserAccountService());
        String result = controller.viewRoleDetails(request, response);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_getUserAccountForm() {
        
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        CustomerAdminRoleSettingController controller = new CustomerAdminRoleSettingController();
        EntityServiceImpl serv = new EntityServiceImpl() {
            public Object merge(Object entity) {
                
                UserAccount userAccount = new UserAccount();
                Customer customer = new Customer();
                CustomerType type = new CustomerType();
                customer.setId(3);
                type.setId(3);
                customer.setCustomerType(type);
                userAccount.setCustomer(customer);
                userAccount.setId(2);
                
                UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                userAccount.setUserStatusType(ust1);
                
                Set<UserRole> roles = new HashSet<UserRole>();
                UserRole role = new UserRole();
                Role r = new Role();
                Set<RolePermission> rps = new HashSet<RolePermission>();
                
                RolePermission rp1 = new RolePermission();
                rp1.setId(1);
                Permission permission1 = new Permission();
                permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
                rp1.setPermission(permission1);
                
                RolePermission rp2 = new RolePermission();
                rp2.setId(2);
                Permission permission2 = new Permission();
                permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
                rp2.setPermission(permission2);
                
                rps.add(rp1);
                rps.add(rp2);
                
                r.setId(3);
                r.setRolePermissions(rps);
                
                role.setId(3);
                role.setRole(r);
                roles.add(role);
                userAccount.setUserRoles(roles);
                return userAccount;
            }
        };
        controller.setEntityService(serv);
        WebSecurityUtil util = new WebSecurityUtil();
        util.setEntityService(createEntityService());
        util.setUserAccountService(createStaticUserAccountService());
        
        controller.setPermissionService(new PermissionServiceImpl() {
            public Collection<RolePermissionInfo> findAllPermissionsByCustomerTypeId(Integer customerTypeId) {
                RolePermissionInfo info1 = new RolePermissionInfo();
                info1.setRoleId(3);
                info1.setRoleName("Child Admin");
                info1.setParentPermissionName("Alerts Management");
                info1.setChildPermissionName("View Current Alerts");
                
                RolePermissionInfo info2 = new RolePermissionInfo();
                info2.setRoleId(3);
                info2.setRoleName("Child Admin");
                info2.setParentPermissionName("Collections Management");
                info2.setChildPermissionName("View Collection Status");
                
                RolePermissionInfo info3 = new RolePermissionInfo();
                info3.setRoleId(4);
                info3.setRoleName("Child Admin");
                info3.setParentPermissionName("Account Management");
                info3.setChildPermissionId(501);
                info3.setChildPermissionName("View Custom Cards");
                
                Collection<RolePermissionInfo> result = new ArrayList<RolePermissionInfo>();
                result.add(info1);
                result.add(info2);
                result.add(info3);
                return result;
            }
            
            public Collection<RolePermissionInfo> findAllPermissionsByRoleId(Integer roleId) {
                RolePermissionInfo info1 = new RolePermissionInfo();
                info1.setRoleName("Child Admin");
                info1.setStatus(1);
                info1.setParentPermissionName("Alerts Management");
                info1.setChildPermissionName("View Current Alerts");
                
                RolePermissionInfo info2 = new RolePermissionInfo();
                info2.setRoleName("Child Admin");
                info2.setStatus(1);
                info2.setParentPermissionName("Collections Management");
                info2.setChildPermissionName("View Collection Status");
                
                Collection<RolePermissionInfo> result = new ArrayList<RolePermissionInfo>();
                result.add(info1);
                result.add(info2);
                return result;
            }
        });
        
        WidgetMetricsHelper.registerComponents();
        String result = controller.getUserRoleForm(request, response, model);
        
        assertNotNull(result);
    }
    
    @Test
    public void test_getUserAccountForm_role_fail() {
        
        createFailedAuthentication();
        
        CustomerAdminRoleSettingController controller = new CustomerAdminRoleSettingController();
        EntityServiceImpl serv = new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        };
        controller.setEntityService(serv);
        WebSecurityUtil util = new WebSecurityUtil();
        util.setEntityService(serv);
        util.setUserAccountService(createStaticUserAccountService());
        
        String result = controller.getUserRoleForm(request, response, model);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
        assertEquals(response.getStatus(), HttpStatus.UNAUTHORIZED.value());
    }
    
    @Test
    public void test_saveRole() {
        CustomerAdminRoleSettingController controller = new CustomerAdminRoleSettingController();
        controller.setRoleSettingValidator(new RoleSettingValidator() {
            public void validate(WebSecurityForm<RoleEditForm> command, Errors errors) {
                
            }
        });
        
        RoleEditForm form = new RoleEditForm();
        form.setRoleId(1);
        form.setRoleName("TestRole");
        form.setStatusEnabled(true);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(1102);
        permissionList.add(1103);
        permissionList.add(1105);
        form.setPermissionIds(permissionList);
        form.setTypeId(WebCoreConstants.CUSTOMER_TYPE_CHILD);
        
        WebSecurityForm<RoleEditForm> webSecurityForm = new WebSecurityForm<RoleEditForm>(null, form);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "roleEditForm");
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                
                UserAccount userAccount = new UserAccount();
                Customer customer = new Customer();
                CustomerType type = new CustomerType();
                customer.setId(3);
                type.setId(3);
                customer.setCustomerType(type);
                userAccount.setCustomer(customer);
                userAccount.setId(2);
                
                UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                userAccount.setUserStatusType(ust1);
                
                Set<UserRole> roles = new HashSet<UserRole>();
                UserRole role = new UserRole();
                Role r = new Role();
                Set<RolePermission> rps = new HashSet<RolePermission>();
                
                RolePermission rp1 = new RolePermission();
                rp1.setId(1);
                Permission permission1 = new Permission();
                permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
                rp1.setPermission(permission1);
                
                RolePermission rp2 = new RolePermission();
                rp2.setId(2);
                Permission permission2 = new Permission();
                permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
                rp2.setPermission(permission2);
                
                rps.add(rp1);
                rps.add(rp2);
                
                r.setId(3);
                r.setRolePermissions(rps);
                
                role.setId(3);
                role.setRole(r);
                roles.add(role);
                userAccount.setUserRoles(roles);
                return userAccount;
            }
        });
        
        controller.setCustomerRoleService(new CustomerRoleServiceImpl() {
            public Collection<CustomerRole> findCustomerRoleByCustomerId(Integer customerId) {
                Collection<CustomerRole> roles = new ArrayList<CustomerRole>();
                CustomerRole role1 = new CustomerRole();
                role1.setId(1);
                Role r1 = new Role();
                r1.setId(3);
                r1.setName("Child Admin");
                role1.setRole(r1);
                
                CustomerRole role2 = new CustomerRole();
                role2.setId(2);
                Role r2 = new Role();
                r2.setId(4);
                r2.setName("Basic");
                role2.setRole(r2);
                
                roles.add(role1);
                roles.add(role2);
                
                return roles;
            }
        });
        
        controller.setRoleService(new RoleServiceImpl() {
            public void saveRoleAndRolePermission(Role role) {
                
            }
            
            @Override
            public Role findRoleByName(Integer customerId, String roleName) {
                return null;
            }
        });
        
        String res = controller.saveRole(webSecurityForm, result, response);
        assertNotNull(res);
        assertTrue(res.contains(WebCoreConstants.RESPONSE_TRUE));
    }
    
    @Test
    public void test_saveRole_update() {
        CustomerAdminRoleSettingController controller = new CustomerAdminRoleSettingController();
        controller.setRoleSettingValidator(new RoleSettingValidator() {
            public void validate(WebSecurityForm<RoleEditForm> command, Errors errors) {
                
            }
        });
        
        RoleEditForm form = new RoleEditForm();
        form.setRoleId(1);
        form.setRoleName("TestRole");
        form.setStatusEnabled(true);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(1102);
        permissionList.add(1103);
        permissionList.add(1105);
        form.setPermissionIds(permissionList);
        form.setTypeId(WebCoreConstants.CUSTOMER_TYPE_CHILD);
        
        WebSecurityForm<RoleEditForm> webSecurityForm = new WebSecurityForm<RoleEditForm>(null, form);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_UPDATE);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "roleEditForm");
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                
                UserAccount userAccount = new UserAccount();
                Customer customer = new Customer();
                CustomerType type = new CustomerType();
                customer.setId(3);
                type.setId(3);
                customer.setCustomerType(type);
                userAccount.setCustomer(customer);
                userAccount.setId(2);
                
                UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                userAccount.setUserStatusType(ust1);
                
                Set<UserRole> roles = new HashSet<UserRole>();
                UserRole role = new UserRole();
                Role r = new Role();
                Set<RolePermission> rps = new HashSet<RolePermission>();
                
                RolePermission rp1 = new RolePermission();
                rp1.setId(1);
                Permission permission1 = new Permission();
                permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
                rp1.setPermission(permission1);
                
                RolePermission rp2 = new RolePermission();
                rp2.setId(2);
                Permission permission2 = new Permission();
                permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
                rp2.setPermission(permission2);
                
                rps.add(rp1);
                rps.add(rp2);
                
                r.setId(3);
                r.setRolePermissions(rps);
                
                role.setId(3);
                role.setRole(r);
                roles.add(role);
                userAccount.setUserRoles(roles);
                return userAccount;
            }
        });
        
        controller.setCustomerRoleService(new CustomerRoleServiceImpl() {
            public Collection<CustomerRole> findCustomerRoleByCustomerId(Integer customerId) {
                Collection<CustomerRole> roles = new ArrayList<CustomerRole>();
                CustomerRole role1 = new CustomerRole();
                role1.setId(1);
                Role r1 = new Role();
                r1.setId(3);
                r1.setName("Child Admin");
                role1.setRole(r1);
                
                CustomerRole role2 = new CustomerRole();
                role2.setId(2);
                Role r2 = new Role();
                r2.setId(4);
                r2.setName("Basic");
                role2.setRole(r2);
                
                roles.add(role1);
                roles.add(role2);
                
                return roles;
            }
        });
        
        controller.setRoleService(new RoleServiceImpl() {
            public void updateRoleAndRolePermission(Role role) {
                
            }
            
            public Role findRoleByRoleIdAndEvict(Integer roleId) {
                Role role = new Role();
                Role cloneRole = new Role();
                RoleStatusType type = new RoleStatusType();
                type.setId(1);
                role.setId(3);
                role.setRoleStatusType(type);
                CustomerType customerType = new CustomerType();
                customerType.setId(WebCoreConstants.CUSTOMER_TYPE_CHILD);
                role.setCustomerType(customerType);
                
                return role;
            }
            
            @Override
            public Role findRoleByName(final Integer customerId, final String roleName) {
                return null;
            }
        });
        
        final String res = controller.saveRole(webSecurityForm, result, response);
        assertNotNull(res);
        assertTrue(res.contains(WebCoreConstants.RESPONSE_TRUE));
    }
    
    @Test
    public void test_saveRole_role_fail() {
        
        createFailedAuthentication();
        
        RoleEditForm form = new RoleEditForm();
        form.setRoleId(1);
        form.setRoleName("TestRole");
        form.setStatusEnabled(true);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(1102);
        permissionList.add(1103);
        permissionList.add(1105);
        form.setPermissionIds(permissionList);
        form.setTypeId(WebCoreConstants.CUSTOMER_TYPE_CHILD);
        
        WebSecurityForm<RoleEditForm> webSecurityForm = new WebSecurityForm<RoleEditForm>(null, form);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "roleEditForm");
        
        CustomerAdminRoleSettingController controller = new CustomerAdminRoleSettingController();
        EntityServiceImpl serv = new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        };
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(serv);
        controller.setRoleSettingValidator(new RoleSettingValidator());
        WidgetMetricsHelper.registerComponents();
        
        String res = controller.saveRole(webSecurityForm, result, response);
        assertEquals(res, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_saveRole_validator_fail() {
        CustomerAdminRoleSettingController controller = new CustomerAdminRoleSettingController();
        controller.setRoleSettingValidator(new RoleSettingValidator() {
            public void validate(WebSecurityForm<RoleEditForm> webSecurityForm, Errors errors) {
                webSecurityForm.removeAllErrorStatus();
                webSecurityForm.addErrorStatus(new FormErrorStatus("roleName", "error.common.required"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("enabled", "error.common.invalid"));
            }
        });
        
        EntityServiceImpl serv = new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        };
        
        controller.setCustomerRoleService(new CustomerRoleServiceImpl() {
            public Collection<CustomerRole> findCustomerRoleByCustomerId(Integer customerId) {
                Collection<CustomerRole> roles = new ArrayList<CustomerRole>();
                CustomerRole role1 = new CustomerRole();
                role1.setId(1);
                Role r1 = new Role();
                r1.setId(3);
                r1.setName("Child Admin");
                role1.setRole(r1);
                
                CustomerRole role2 = new CustomerRole();
                role2.setId(2);
                Role r2 = new Role();
                r2.setId(4);
                r2.setName("Basic");
                role2.setRole(r2);
                
                roles.add(role1);
                roles.add(role2);
                
                return roles;
            }
        });
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(serv);
        
        RoleEditForm form = new RoleEditForm();
        WebSecurityForm<RoleEditForm> webSecurityForm = new WebSecurityForm<RoleEditForm>(null, form);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "roleEditForm");
        WidgetMetricsHelper.registerComponents();
        
        String res = controller.saveRole(webSecurityForm, result, response);
        
        assertNotNull(res);
        assertTrue(res
                .contains("\"errorStatus\":[{\"errorFieldName\":\"roleName\",\"message\":\"error.common.required\"},{\"errorFieldName\":\"enabled\",\"message\":\"error.common.invalid\"}]"));
    }
    
    @Test
    public void test_deleteRole() {
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        Collection<Role> roles = new ArrayList<Role>();
        Role role = new Role();
        role.setId(1123);
        role.setName("TestRole");
        roles.add(role);
        
        List<WebObject> randomized = (List<WebObject>) keyMapping.hideProperties(roles, "id");
        request.setParameter("roleID", randomized.get(0).getPrimaryKey().toString());
        
        CustomerAdminRoleSettingController controller = new CustomerAdminRoleSettingController();
        controller.setRoleService(new RoleServiceImpl() {
            public Role findRoleByRoleIdAndEvict(Integer roleId) {
                Role role = new Role();
                Role cloneRole = new Role();
                RoleStatusType type = new RoleStatusType();
                type.setId(1);
                role.setId(3);
                role.setRoleStatusType(type);
                CustomerType customerType = new CustomerType();
                customerType.setId(WebCoreConstants.CUSTOMER_TYPE_CHILD);
                role.setCustomerType(customerType);
                
                return role;
            }
            
            public void updateRole(Role role) {
                
            }
            
            public void deleteRole(Role role) {
                
            }
        });
        
        controller.setUserAccountService(createUserAccountService());
        
        String result = controller.deleteRole(request, response);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_TRUE);
    }
    
    @Test
    public void test_deleteRole_role_fail() {
        Set<UserRole> roles = new HashSet<UserRole>();
        UserRole role = new UserRole();
        Role r = new Role();
        
        Set<RolePermission> rps = new HashSet<RolePermission>();
        RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        Permission permission1 = new Permission();
        permission1.setId(23);
        rp1.setPermission(permission1);
        
        rps.add(rp1);
        
        r.setId(3);
        r.setRolePermissions(rps);
        role.setId(2);
        role.setRole(r);
        roles.add(role);
        userAccount.setUserRoles(roles);
        
        CustomerAdminRoleSettingController controller = new CustomerAdminRoleSettingController();
        String result = controller.deleteRole(request, response);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_deleteRole_randomized_roleID_validation_fail() {
        request.setParameter("roleID", "111111111111111!");
        
        CustomerAdminRoleSettingController controller = new CustomerAdminRoleSettingController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        String result = controller.deleteRole(request, response);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_deleteRole_actual_roleID_validation_fail() {
        request.setParameter("roleID", "2DFTYU7FREDGFWS4");
        
        CustomerAdminRoleSettingController controller = new CustomerAdminRoleSettingController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        String result = controller.deleteRole(request, response);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private void createFailedAuthentication() {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(2);
        permissionList.add(3);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private Collection<RoleSettingInfo> getRoleSettingInfo() {
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        Collection<Role> roles = new ArrayList<Role>();
        Role r1 = new Role();
        r1.setId(3);
        r1.setName("Child Admin");
        Role r2 = new Role();
        r2.setId(4);
        r2.setName("Basic");
        roles.add(r1);
        roles.add(r2);
        
        Collection<WebObject> objs = keyMapping.hideProperties(roles, WebCoreConstants.ID_LOOK_UP_NAME);
        Collection<RoleSettingInfo> lists = new ArrayList<RoleSettingInfo>();
        
        for (WebObject obj : objs) {
            lists.add(new RoleSettingInfo(obj.getPrimaryKey().toString(), ((Role) obj.getWrappedObject()).getName()));
        }
        
        return lists;
    }
    
    private EntityService createEntityService() {
        return new EntityServiceImpl() {
            public Object merge(Object entity) {
                
                UserAccount userAccount = new UserAccount();
                Customer customer = new Customer();
                CustomerType type = new CustomerType();
                customer.setId(3);
                type.setId(3);
                customer.setCustomerType(type);
                userAccount.setCustomer(customer);
                userAccount.setId(2);
                
                UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                userAccount.setUserStatusType(ust1);
                
                Set<UserRole> roles = new HashSet<UserRole>();
                UserRole role = new UserRole();
                Role r = new Role();
                Set<RolePermission> rps = new HashSet<RolePermission>();
                
                RolePermission rp1 = new RolePermission();
                rp1.setId(1);
                Permission permission1 = new Permission();
                permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
                rp1.setPermission(permission1);
                
                RolePermission rp2 = new RolePermission();
                rp2.setId(2);
                Permission permission2 = new Permission();
                permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
                rp2.setPermission(permission2);
                
                rps.add(rp1);
                rps.add(rp2);
                
                r.setId(3);
                r.setRolePermissions(rps);
                
                role.setId(3);
                role.setRole(r);
                roles.add(role);
                userAccount.setUserRoles(roles);
                return userAccount;
            }
        };
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
            
            @Override
            public List<UserAccount> findUserAccountByRoleIdAndCustomerId(Integer roleId, Integer customerId) {
                List<UserAccount> results = new ArrayList<UserAccount>();
                UserAccount u1 = new UserAccount();
                u1.setId(1);
                u1.setFirstName("John");
                u1.setLastName("Doe1");
                results.add(u1);
                UserAccount u2 = new UserAccount();
                u2.setId(1);
                u2.setFirstName("John2");
                u2.setLastName("Doe2");
                results.add(u2);
                
                return results;
            }
            
        };
    }
    
    private CustomerService createCustomerService() {
        return new CustomerServiceTestImpl() {
            public List<Customer> findAllChildCustomers(final int parentCustomerId) {
                List<Customer> fc = new ArrayList<Customer>();
                Customer c = new Customer();
                c.setId(6);
                fc.add(c);
                return fc;
            }
        };
    }
    
    private UserAccountService createUserAccountService() {
        return new UserAccountServiceImpl() {
            @Override
            public Collection<UserAccount> findUserAccountByCustomerIdAndRoleId(Integer customerId, Integer roleId) {
                Collection<UserAccount> results = new ArrayList<UserAccount>();
                UserAccount u1 = new UserAccount();
                u1.setId(1);
                u1.setFirstName("John");
                u1.setLastName("Doe1");
                Customer customer1 = new Customer();
                CustomerType type = new CustomerType();
                type.setId(3);
                customer1.setCustomerType(type);
                customer1.setId(3);
                customer1.setName("Company1");
                u1.setCustomer(customer1);
                results.add(u1);
                
                UserAccount u2 = new UserAccount();
                u2.setId(1);
                u2.setFirstName("John2");
                u2.setLastName("Doe2");
                Customer customer2 = new Customer();
                customer2.setCustomerType(type);
                customer2.setId(3);
                customer2.setName("Company1");
                u2.setCustomer(customer2);
                results.add(u2);
                
                return results;
            }
            
            @Override
            public List<UserAccount> findUserAccountByRoleIdAndCustomerId(Integer roleId, Integer customerId) {
                List<UserAccount> results = new ArrayList<UserAccount>();
                UserAccount u1 = new UserAccount();
                u1.setId(1);
                u1.setFirstName("John");
                u1.setLastName("Doe1");
                results.add(u1);
                UserAccount u2 = new UserAccount();
                u2.setId(1);
                u2.setFirstName("John2");
                u2.setLastName("Doe2");
                results.add(u2);
                
                return results;
            }
            
            @Override
            public void deleteUserRoles(UserAccount userAccount) {
            }
            
        };
    }
}
