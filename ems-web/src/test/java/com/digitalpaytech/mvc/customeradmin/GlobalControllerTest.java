package com.digitalpaytech.mvc.customeradmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerPropertyType;
import com.digitalpaytech.domain.CustomerStatusType;
import com.digitalpaytech.domain.FlexWSUserAccount;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.customeradmin.support.GlobalPreferencesEditForm;
import com.digitalpaytech.mvc.customeradmin.support.JurisdictionTypeEditForm;
import com.digitalpaytech.mvc.support.FlexHelper;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.FlexWSServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.CustomerAdminServiceTestImpl;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.customeradmin.JurisdictionTypeValidator;
import com.digitalpaytech.validation.customeradmin.GlobalPreferencesValidator;

@SuppressWarnings({ "checkstyle:magicnumber", "PMD.ExcessiveImports", "unchecked" })
public class GlobalControllerTest {
    private static final String SETTINGS_GLOBAL_SETTINGS = "/settings/global/settings";
    private static final Set<Integer> PREFERRED_OPTIONS =
            new HashSet<>(Arrays.asList(PaystationConstants.PAYSTATION_JURISDICTION_PREFERRED_DISABLED,
                                        PaystationConstants.PAYSTATION_JURISDICTION_PREFERRED_BY_CUSTOMER,
                                        PaystationConstants.PAYSTATION_JURISDICTION_PREFERRED_BY_LOCATION));
    private static final Set<Integer> LIMITED_OPTIONS = new HashSet<>(
            Arrays.asList(PaystationConstants.PAYSTATION_JURISDICTION_LIMITED_DISABLED, PaystationConstants.PAYSTATION_JURISDICTION_LIMITED_BY_CUSTOMER,
                          PaystationConstants.PAYSTATION_JURISDICTION_LIMITED_BY_LOCATION));
    
    private static final String TRUE = "true";
    private static final String US_PACIFIC = "US/Pacific";
    private static final String GLOBAL_PREFERENCES_EDIT_FORM = "globalPreferencesEditForm";
    private static final String JURISDICTION_TYPE_EDIT_FORM = "jurisdictionTypeEditForm";
    private final GlobalController controller = new GlobalController();
    private final WebSecurityUtil util = new WebSecurityUtil();
    private final MockHttpServletRequest request = new MockHttpServletRequest();
    private final MockHttpServletResponse response = new MockHttpServletResponse();
    private ModelMap model;
    private BindingResult result;
    private WebSecurityForm<GlobalPreferencesEditForm> form;
    private WebSecurityForm<JurisdictionTypeEditForm> jurisdictionForm;
    private GlobalPreferencesEditForm globalPreferencesEditForm;
    private JurisdictionTypeEditForm jurisdictionTypeEditForm;
    
    @Before
    public void setUp() {
        final String auth = "Admin";
        final String username = "testUser";
        final String password = "password";
        final String salt = "salt";
        
        this.model = new ModelMap();
        this.form = new WebSecurityForm<GlobalPreferencesEditForm>();
        this.globalPreferencesEditForm = new GlobalPreferencesEditForm();
        this.jurisdictionTypeEditForm = new JurisdictionTypeEditForm();
        this.form.setWrappedObject(this.globalPreferencesEditForm);
        this.result = new BeanPropertyBindingResult(this.form, GLOBAL_PREFERENCES_EDIT_FORM);
        
        this.controller.setCustomerAdminService(new CustomerAdminServiceTestImpl() {
            @Override
            public void saveOrUpdateCustomerProperty(final CustomerProperty customerProperty) {
                
            }
            
            @Override
            public CustomerProperty getCustomerPropertyByCustomerIdAndCustomerPropertyType(final int customerId, final int customerPropertyType) {
                if (customerPropertyType == 11) {
                    return new CustomerProperty(new CustomerPropertyType(customerPropertyType), new Customer(customerId), "1", new Date(), 1);
                } else {
                    return new CustomerProperty(new CustomerPropertyType(customerPropertyType), new Customer(customerId), "2", new Date(), 1);
                }
            }
        });
        
        this.controller.setFlexWSService(new FlexWSServiceImpl() {
            @Override
            public FlexWSUserAccount findFlexWSUserAccountByCustomerId(final Integer customerId) {
                return new FlexWSUserAccount();
            }
        });
        
        this.controller.setFlexHelper(new FlexHelper());
        
        this.controller.setGlobalPreferencesValidator(new GlobalPreferencesValidator() {
            @Override
            public void validate(final WebSecurityForm<GlobalPreferencesEditForm> command, final Errors errors) {
                
            }
        });
        
        this.util.setUserAccountService(new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                final UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
            
            public UserAccount findAdminUserAccountByChildCustomerId(final int customerId) {
                final UserAccount ua = new UserAccount();
                ua.setId(3);
                return ua;
            }
            
            public UserAccount findAdminUserAccountByParentCustomerId(final int customerId) {
                final UserAccount ua = new UserAccount();
                ua.setId(3);
                return ua;
            }
            
        });
        
        this.util.setEntityService(new EntityServiceImpl() {
            @Override
            public Object merge(final Object entity) {
                final UserAccount userAccount = new UserAccount();
                userAccount.setId(1);
                final Customer customer = new Customer();
                customer.setId(2);
                
                final CustomerStatusType cst = new CustomerStatusType();
                cst.setId(WebCoreConstants.CUSTOMER_STATUS_TYPE_ENABLED);
                customer.setCustomerStatusType(cst);
                
                userAccount.setCustomer(customer);
                final Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
                authorities.add(new SimpleGrantedAuthority(auth));
                final WebUser user = new WebUser(2, 2, username, password, salt, true, authorities);
                user.setIsComplexPassword(true);
                user.setIsMigrated(true);
                final Authentication authentication = new UsernamePasswordAuthenticationToken(user, password, authorities);
                SecurityContextHolder.getContext().setAuthentication(authentication);
                return userAccount;
            }
        });
        
        this.controller.setMerchantAccountService(new MerchantAccountServiceImpl() {
            @Override
            public List<MerchantAccount> findAllMerchantAccountsByCustomerId(final Integer customerId) {
                final MerchantAccount merchantAccount = new MerchantAccount();
                merchantAccount.setCloseQuarterOfDay((byte) 4);
                final List<MerchantAccount> maList = new ArrayList<MerchantAccount>();
                maList.add(merchantAccount);
                return maList;
            }
            
            @Override
            public void updateMerchantAccount(final MerchantAccount merchantAccount) {
                
            }
        });
        
        this.controller.setJurisdictionTypeValidator(new JurisdictionTypeValidator() {
            @Override
            public void validate(final WebSecurityForm<JurisdictionTypeEditForm> command, final Errors errors) {
            }
            
            @Override
            public void validatePreferredParkerImportForm(final WebSecurityForm<JurisdictionTypeEditForm> command, final int origPreferredValue,
                final Errors errors, final boolean isPrepared) {
            }
        });
        
        final Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority(auth));
        final WebUser user = new WebUser(2, 2, username, password, salt, true, authorities);
        user.setIsComplexPassword(true);
        user.setIsMigrated(true);
        final Authentication authentication = new UsernamePasswordAuthenticationToken(user, password, authorities);
        final Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_SYSTEM_SETTINGS);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        
        WidgetMetricsHelper.registerComponents();
    }
    
    @Test
    public void testIndex() {
        this.request.getSession(true);
        final String res = this.controller.getGlobalPreferences(this.request, this.response, this.model, this.controller.initEditForm(this.request),
                                                                this.controller.initFlexCredentialsForm(this.request),
                                                                this.controller.initJurisdictionTypeEditForm(this.request));
        assertEquals(res, SETTINGS_GLOBAL_SETTINGS);
        final WebSecurityForm<GlobalPreferencesEditForm> webSecurityForm =
                (WebSecurityForm<GlobalPreferencesEditForm>) this.model.get(GLOBAL_PREFERENCES_EDIT_FORM);
        final GlobalPreferencesEditForm returnedglobalPreferencesEditForm = webSecurityForm.getWrappedObject();
        assertNotNull(returnedglobalPreferencesEditForm);
        assertEquals(Integer.valueOf(2), returnedglobalPreferencesEditForm.getQuerySpacesBy());
        assertNotNull(returnedglobalPreferencesEditForm.getCurrentTimeZone());
    }
    
    @Test
    public void testJurisdictionTypeEditForm() {
        this.request.getSession(true);
        final String res = this.controller.getGlobalPreferences(this.request, this.response, this.model, this.controller.initEditForm(this.request),
                                                                this.controller.initFlexCredentialsForm(this.request),
                                                                this.controller.initJurisdictionTypeEditForm(this.request));
        
        assertEquals(res, SETTINGS_GLOBAL_SETTINGS);
        final WebSecurityForm<JurisdictionTypeEditForm> webSecurityForm =
                (WebSecurityForm<JurisdictionTypeEditForm>) this.model.get(JURISDICTION_TYPE_EDIT_FORM);
        final JurisdictionTypeEditForm returnedEditForm = webSecurityForm.getWrappedObject();
        assertNotNull(returnedEditForm);
        assertEquals(Integer.valueOf(1), returnedEditForm.getJurisdictionTypePreferred());
        assertEquals(Integer.valueOf(1), returnedEditForm.getJurisdictionTypeLimited());
    }
    
    @Test
    public void testSaveTimezoneTrue() {
        this.globalPreferencesEditForm.setCurrentTimeZone(US_PACIFIC);
        this.form.setWrappedObject(this.globalPreferencesEditForm);
        final String strResult = this.controller.saveTimeZone(this.form, this.result, this.request, this.response, this.model);
        assertNotNull(strResult);
        assertTrue(strResult.contains(TRUE));
    }
    
    @Test
    public void testSaveTimezoneFalse() {
        this.controller.setGlobalPreferencesValidator(new GlobalPreferencesValidator() {
            @Override
            public void validate(final WebSecurityForm<GlobalPreferencesEditForm> webSecurityForm, final Errors errors) {
                webSecurityForm.addErrorStatus(new FormErrorStatus("selectTimeZone", "error.common.invalid"));
            }
        });
        
        final String strResult = this.controller.saveTimeZone(this.form, this.result, this.request, this.response, this.model);
        assertNotNull(strResult);
        assertTrue(strResult.contains("errorStatus"));
    }
    
    private void setupJurisdictionTypeData() {
        this.jurisdictionForm = new WebSecurityForm<JurisdictionTypeEditForm>();
        this.jurisdictionTypeEditForm = new JurisdictionTypeEditForm();
        this.jurisdictionForm.setWrappedObject(this.jurisdictionTypeEditForm);
        this.request.getSession(true);
    }
    
    @Test
    public void testSaveJurisdictionTypePreferred() {
        setupJurisdictionTypeData();
        this.result = new BeanPropertyBindingResult(this.jurisdictionForm, JURISDICTION_TYPE_EDIT_FORM);
        
        this.jurisdictionTypeEditForm.setJurisdictionTypePreferred(1);
        this.jurisdictionForm.setWrappedObject(this.jurisdictionTypeEditForm);
        final String strResult = this.controller.saveJurisdictionType(this.jurisdictionForm, this.result, this.request, this.response, this.model);
        assertNotNull(strResult);
        assertTrue(strResult.contains(TRUE));
    }
    
    @Test
    public void testSaveJurisdictionTypeInvalidPreferred() {
        setupJurisdictionTypeData();
        this.jurisdictionTypeEditForm.setJurisdictionTypePreferred(3);
        this.jurisdictionForm.setWrappedObject(this.jurisdictionTypeEditForm);
        
        final boolean validPreferred = this.controller.getJurisdictionTypeValidator()
                .validateJurisdictionType(this.jurisdictionTypeEditForm.getJurisdictionTypePreferred(), PREFERRED_OPTIONS);
        
        final boolean validLimited = this.controller.getJurisdictionTypeValidator()
                .validateJurisdictionType(this.jurisdictionTypeEditForm.getJurisdictionTypeLimited(), LIMITED_OPTIONS);
        
        assertFalse(validPreferred);
        assertTrue(validLimited);
    }
    
    @Test
    public void testSaveJurisdictionTypeLimited() {
        setupJurisdictionTypeData();
        this.jurisdictionTypeEditForm.setJurisdictionTypeLimited(2);
        this.jurisdictionForm.setWrappedObject(this.jurisdictionTypeEditForm);
        
        this.form.setWrappedObject(this.globalPreferencesEditForm);
        final String strResult = this.controller.saveJurisdictionType(this.jurisdictionForm, this.result, this.request, this.response, this.model);
        assertNotNull(strResult);
        assertTrue(strResult.contains(TRUE));
    }
    
    @Test
    public void testSaveJurisdictionTypeInvalidLimited() {
        setupJurisdictionTypeData();
        this.jurisdictionTypeEditForm.setJurisdictionTypeLimited(3);
        this.jurisdictionForm.setWrappedObject(this.jurisdictionTypeEditForm);
        final String strResult = this.controller.saveJurisdictionType(this.jurisdictionForm, this.result, this.request, this.response, this.model);
        assertNotNull(strResult);
        assertTrue(strResult.contains(TRUE));
    }
}
