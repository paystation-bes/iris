package com.digitalpaytech.mvc;

import org.junit.*;

import static org.junit.Assert.*;

import com.digitalpaytech.client.util.CommunicationException;
import com.digitalpaytech.exception.CardProcessorException;
import com.digitalpaytech.util.DeviceGroupConstants;
import com.netflix.hystrix.exception.HystrixBadRequestException;

public class DeviceGroupControllerTest {

    @Test
    public void causedByScheduledOrIncompleteStatus() {
        final CommunicationException ce = new CommunicationException(403, "Update cannot be forced");
        final HystrixBadRequestException hbre = new HystrixBadRequestException("Unsuccessful response", ce);
        final RuntimeException re = new RuntimeException(hbre);
        
        final DeviceGroupController controller = new DeviceGroupController();
        assertEquals(DeviceGroupConstants.FAILED_CARD_PROCESSING_SCHEDULED_CANNOT_UPDATE_ERROR_KEY,
                     controller.causedByScheduledOrIncompleteStatus(re,
                                                                    DeviceGroupConstants.FAILED_CARD_PROCESSING_SCHEDULED_CANNOT_UPDATE_ERROR_KEY,
                                                                    DeviceGroupConstants.FAILED_TO_CONFIG_CARD_PROCESSING_ERROR_KEY));

        final CardProcessorException cpe = new CardProcessorException("TEST");
        final HystrixBadRequestException hbreCpe = new HystrixBadRequestException("Unsuccessful response", cpe);
        final RuntimeException reHbre = new RuntimeException(hbreCpe);
        assertEquals(DeviceGroupConstants.FAILED_TO_CONFIG_CARD_PROCESSING_ERROR_KEY,
                     controller.causedByScheduledOrIncompleteStatus(reHbre,
                                                                    DeviceGroupConstants.FAILED_CARD_PROCESSING_SCHEDULED_CANNOT_UPDATE_ERROR_KEY,
                                                                    DeviceGroupConstants.FAILED_TO_CONFIG_CARD_PROCESSING_ERROR_KEY));
    }
}
