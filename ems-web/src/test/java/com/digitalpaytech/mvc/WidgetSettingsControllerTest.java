package com.digitalpaytech.mvc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DirectFieldBindingResult;
import org.springframework.validation.Errors;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.AlertClassType;
import com.digitalpaytech.domain.CardProcessMethodType;
import com.digitalpaytech.domain.CitationType;
import com.digitalpaytech.domain.CollectionType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.RevenueType;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.Section;
import com.digitalpaytech.domain.Tab;
import com.digitalpaytech.domain.TransactionType;
import com.digitalpaytech.domain.UnifiedRate;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.domain.WidgetChartType;
import com.digitalpaytech.domain.WidgetFilterType;
import com.digitalpaytech.domain.WidgetLimitType;
import com.digitalpaytech.domain.WidgetListType;
import com.digitalpaytech.domain.WidgetMetricType;
import com.digitalpaytech.domain.WidgetRangeType;
import com.digitalpaytech.domain.WidgetSubsetMember;
import com.digitalpaytech.domain.WidgetTierType;
import com.digitalpaytech.dto.TabHeading;
import com.digitalpaytech.dto.WidgetMetrics;
import com.digitalpaytech.dto.WidgetSettingsOptions;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.mvc.support.WidgetSettingsForm;
import com.digitalpaytech.service.DashboardService;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.CitationTypeServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.DashboardServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.WidgetConstants;
import com.digitalpaytech.validation.WidgetSettingsValidator;

public class WidgetSettingsControllerTest {
    
    private static Map<Integer, String> durationMap;
    private static Map<Integer, String> tierMap;
    private static Map<Integer, String> metricMap;
    private static Map<Integer, String> filterMap;
    private static Map<Integer, String> chartMap;
    private static Map<Integer, Integer> limitMap;
    
    private UserAccount userAccount;
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    
    @Before
    public void setUp() {
        
        setUpDurationMap();
        setUpTierMap();
        setUpMetricMap();
        setUpFilterMap();
        setUpChartsMap();
        setUpLimitMap();
        
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        userAccount = new UserAccount();
        Customer customer = new Customer();
        Customer parent = new Customer();
        customer.setId(3);
        customer.setParentCustomer(parent);
        userAccount.setCustomer(customer);
    }
    
    @After
    public void cleanUp() throws Exception {
        request = null;
        response = null;
    }
    
    @Test
    public void test_editWidgetSettings() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.getSession(true);
        SessionTool sessionTool = SessionTool.getInstance(request);
        //TODO Remove SessionToken
        //		sessionTool.setAttribute(WebSecurityConstants.SESSION_TOKEN, "testSession");
        //		request.setParameter(WebSecurityConstants.SESSION_TOKEN, "testSession");
        request.setParameter("widgetID", "randomId");
        
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        
        WebSecurityForm<WidgetSettingsForm> secForm = new WebSecurityForm<WidgetSettingsForm>(null, createWidgetSettingsForm(keyMapping));
        secForm.setInitToken("testSession");
        secForm.setPostToken("testSession");
        
        ModelMap model = new ModelMap();
        
        createAuthentication(userAccount);
        
        WidgetMetrics widgetMetrics = null;
        InputStream is = null;
        try {
            is = loadXml();
            widgetMetrics = WidgetMetricsHelper.loadWidgetMetricsXml("/widgetmetrics.xml", is);
            
        } catch (Exception e) {
            fail(e.getMessage());
        }
        
        WidgetSettingsController controller = new WidgetSettingsController() {
            @Override
            public WidgetMetrics getWidgetMetrics() {
                InputStream inputS = null;
                try {
                    inputS = loadXml();
                } catch (Exception e) {
                    fail(e.getMessage());
                }
                return WidgetMetricsHelper.loadWidgetMetricsXml("/widgetmetrics.xml", inputS);
            }
        };
        
        /*
         * Bypass validation, run WidgetSettingsValidatorTest if necessary.
         */
        WidgetSettingsValidator validator = new WidgetSettingsValidator() {
            @Override
            public void validate(WebSecurityForm<WidgetSettingsForm> command, Errors errors, Widget widget, boolean isApply) {
                return;
            }
        };
        DashboardController dashboardCon = new DashboardController() {
            @Test
            Map<String, Tab> loadTabTreeToMapIfNecessary(HttpServletRequest request, String tabId) {
                Tab tab = createTabSectionsWidgets(234, "tabRandomId");
                Map<String, Tab> map = new HashMap<String, Tab>();
                map.put(tab.getRandomId(), tab);
                return map;
            }
        };
        DashboardService dashboardService = new DashboardServiceTestImpl() {
            public <T> T findObjectByClassTypeAndId(Class<T> clazz, int id) {
                return null;
            }
        };
        EntityServiceImpl entityService = new EntityServiceImpl() {
            @Override
            public <T> List<T> loadAll(Class<T> entityClass) {
                return new ArrayList<T>();
            }
        };
        
        controller.setDashboardService(dashboardService);
        controller.setEntityService(entityService);
        controller.setDashboardController(dashboardCon);
        controller.setWidgetMetrics(widgetMetrics);
        controller.setWidgetSettingsValidator(validator);
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(entityService);
        
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, createTabHeadings());
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, dashboardCon.loadTabTreeToMapIfNecessary(request, "tabRandomId"));
        
        Errors errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        String result = null;
        
        try {
            result = controller.applyWidgetSettings(secForm, (BindingResult) errors, request, response, model);
            assertEquals(WebCoreConstants.RESPONSE_TRUE, result);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
    
    @Test
    public void test_updateWidgetSettingsOptions() {
        request.getSession(true);
        SessionTool sessionTool = SessionTool.getInstance(request);
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        
        ModelMap model = new ModelMap();
        //TODO Remove SessionToken
        //		sessionTool.setAttribute(WebSecurityConstants.SESSION_TOKEN, "testSession");
        //		request.setParameter(WebSecurityConstants.SESSION_TOKEN, "testSession");
        request.setParameter("widgetID", "randomId");
        
        WebSecurityForm<WidgetSettingsForm> secForm = createWebSecurityForm(true);
        
        Errors errors = new DirectFieldBindingResult(secForm, "WebSecurityForm");
        WidgetSettingsValidator validator = new WidgetSettingsValidator() {
            @Override
            public void validate(WebSecurityForm<WidgetSettingsForm> command, Errors errors, Widget widget, boolean isApply) {
            }
        };
        WidgetMetrics widgetMetrics = null;
        InputStream is = null;
        try {
            is = loadXml();
            widgetMetrics = WidgetMetricsHelper.loadWidgetMetricsXml("/widgetmetrics.xml", is);
            
        } catch (Exception e) {
            fail(e.getMessage());
        }
        DashboardController dashboardCon = new DashboardController() {
            @Test
            Map<String, Tab> loadTabTreeToMapIfNecessary(HttpServletRequest request, String tabId) {
                Tab tab = createTabSectionsWidgets(234, "tabRandomId");
                Map<String, Tab> map = new HashMap<String, Tab>();
                map.put(tab.getRandomId(), tab);
                return map;
            }
        };
        DashboardServiceTestImpl dashboardService = new DashboardServiceTestImpl() {
            @Override
            public List<Location> findLocationsByCustomerId(int customerId) {
                return new ArrayList<Location>();
            }
            
            @Override
            public List<Route> findRoutesByCustomerId(int customerId) {
                return new ArrayList<Route>();
            }
            
            @Override
            public List<UnifiedRate> findUnifiedRatesByCustomerId(int customerId, int pastMonths) {
                return new ArrayList<UnifiedRate>();
            }
            
            @Override
            public List<WidgetSubsetMember> findSubsetMemberByWidgetIdTierTypeId(Integer id, int widgetTierType) {
                return new ArrayList<WidgetSubsetMember>();
            }
            
            @Override
            public <T> T findObjectByClassTypeAndId(Class<T> clazz, int id) {
                Customer newCustomer = new Customer();
                newCustomer.setId(3);
                newCustomer.setName("Digital Payment Technologies");
                return (T) newCustomer;
            }
            
            @Override
            public List<CardProcessMethodType> findSettledCardProcessMethodTypes() {
                CardProcessMethodType t1 = new CardProcessMethodType();
                t1.setId(1);
                t1.setName("Real-Time");
                
                List<CardProcessMethodType> list = new ArrayList<CardProcessMethodType>();
                list.add(t1);
                return list;
            }
        };
        CitationTypeServiceImpl citationTypeService = new CitationTypeServiceImpl() {
            @Override
            public List<CitationType> findCitationTypeByCustomerId(Integer customerId) {
                List<CitationType> citationTypes = new ArrayList<CitationType>();
                CitationType citationType = new CitationType();
                citationType.setName("merchant");
                citationType.setId(1);
                citationTypes.add(citationType);
                return citationTypes;
            }
            
            @Override
            public List<CitationType> findActiveCitationTypeByCustomerId(Integer customerId) {
                List<CitationType> citationTypes = new ArrayList<CitationType>();
                CitationType citationType = new CitationType();
                citationType.setName("merchant");
                citationType.setId(1);
                citationTypes.add(citationType);
                return citationTypes;
            }
        };
        MerchantAccountServiceImpl merchantAccountService = new MerchantAccountServiceImpl() {
            @Override
            public List<MerchantAccount> findValidMerchantAccountsByCustomerId(Integer customerId) {
                List<MerchantAccount> merchantTypes = new ArrayList<MerchantAccount>();
                MerchantAccount merchantAccount = new MerchantAccount();
                merchantAccount.setName("merchant");
                merchantAccount.setId(1);
                merchantTypes.add(merchantAccount);
                return merchantTypes;
            }
        };
        
        createAuthentication(userAccount);
        
        WidgetSettingsController controller = new WidgetSettingsController();
        //-----------------------------
        List<RevenueType> revenueTypes = new ArrayList<RevenueType>();
        RevenueType revenueType = new RevenueType();
        revenueType.setName("Total");
        revenueType.setId((byte) 1);
        revenueTypes.add(revenueType);
        //-----------------------------
        List<TransactionType> transactionTypes = new ArrayList<TransactionType>();
        TransactionType transactionType = new TransactionType();
        transactionType.setName("Regular");
        transactionType.setId(2);
        transactionTypes.add(transactionType);
        //-----------------------------
        List<WidgetTierType> widgetTierTypes = new ArrayList<WidgetTierType>();
        WidgetTierType widgetTierType1 = new WidgetTierType();
        WidgetTierType widgetTierType2 = new WidgetTierType();
        WidgetTierType widgetTierType3 = new WidgetTierType();
        widgetTierType1.setName("Hour");
        widgetTierType1.setId(WidgetConstants.TIER_TYPE_HOUR);
        widgetTierTypes.add(widgetTierType1);
        widgetTierType2.setName("Rate");
        widgetTierType2.setId(WidgetConstants.TIER_TYPE_RATE);
        widgetTierTypes.add(widgetTierType2);
        widgetTierType3.setName("Collection Type");
        widgetTierType3.setId(WidgetConstants.TIER_TYPE_COLLECTION_TYPE);
        widgetTierTypes.add(widgetTierType3);
        //-----------------------------
        List<WidgetRangeType> widgetRangeTypes = new ArrayList<WidgetRangeType>();
        WidgetRangeType widgetRangeType = new WidgetRangeType();
        widgetRangeType.setName("Last Year");
        widgetRangeType.setId(WidgetConstants.RANGE_TYPE_LAST_YEAR);
        widgetRangeTypes.add(widgetRangeType);
        //-----------------------------
        List<WidgetChartType> widgetChartTypes = new ArrayList<WidgetChartType>();
        WidgetChartType widgetChartType = new WidgetChartType();
        widgetChartType.setName("List");
        widgetChartType.setId(WidgetConstants.CHART_TYPE_LIST);
        widgetChartTypes.add(widgetChartType);
        //-----------------------------
        List<WidgetFilterType> widgetFilterTypes = new ArrayList<WidgetFilterType>();
        WidgetFilterType widgetFilterType = new WidgetFilterType();
        widgetFilterType.setName("All");
        widgetFilterType.setId(WidgetConstants.FILTER_TYPE_ALL);
        widgetFilterTypes.add(widgetFilterType);
        //-----------------------------
        List<WidgetMetricType> widgetMetricTypes = new ArrayList<WidgetMetricType>();
        WidgetMetricType widgetMetricType = new WidgetMetricType();
        widgetMetricType.setName("Revenue");
        widgetMetricType.setId(WidgetConstants.METRIC_TYPE_REVENUE);
        widgetMetricTypes.add(widgetMetricType);
        //-----------------------------
        List<CollectionType> collectionTypes = new ArrayList<CollectionType>();
        CollectionType collectionType = new CollectionType();
        collectionType.setName("All");
        widgetMetricType.setId(1);
        collectionTypes.add(collectionType);
        //-----------------------------
        List<AlertClassType> alertTypes = new ArrayList<AlertClassType>();
        AlertClassType alertClassType = new AlertClassType();
        alertClassType.setName("All");
        alertClassType.setId((byte) 1);
        alertTypes.add(alertClassType);
        //-----------------------------
        List<CardProcessMethodType> cardProcMethodTypes = new ArrayList<CardProcessMethodType>();
        CardProcessMethodType cardType = new CardProcessMethodType();
        cardType.setName("All");
        cardType.setId(1);
        cardProcMethodTypes.add(cardType);
        //-----------------------------
        List<CitationType> citationTypes = new ArrayList<CitationType>();
        CitationType citationType = new CitationType();
        citationType.setName("All");
        citationType.setId(1);
        citationTypes.add(citationType);
        //-----------------------------
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        
        EntityService entityService = EasyMock.createMock(EntityService.class);
        EasyMock.expect(entityService.merge(userAccount)).andReturn(userAccount);
        EasyMock.expect(entityService.loadAll(TransactionType.class)).andReturn(transactionTypes);
        EasyMock.expect(entityService.loadAll(RevenueType.class)).andReturn(revenueTypes);
        EasyMock.expect(entityService.loadAll(CollectionType.class)).andReturn(collectionTypes);
        EasyMock.expect(entityService.loadAll(WidgetMetricType.class)).andReturn(widgetMetricTypes);
        EasyMock.expect(entityService.loadAll(WidgetFilterType.class)).andReturn(widgetFilterTypes);
        EasyMock.expect(entityService.loadAll(WidgetChartType.class)).andReturn(widgetChartTypes);
        EasyMock.expect(entityService.loadAll(WidgetRangeType.class)).andReturn(widgetRangeTypes);
        EasyMock.expect(entityService.loadAll(WidgetTierType.class)).andReturn(widgetTierTypes);
        EasyMock.expect(entityService.loadAll(AlertClassType.class)).andReturn(alertTypes);
        EasyMock.expect(entityService.loadAll(CardProcessMethodType.class)).andReturn(cardProcMethodTypes);
        EasyMock.expect(entityService.loadAll(CitationType.class)).andReturn(citationTypes);
        EasyMock.replay(entityService);
        controller.setEntityService(entityService);
        util.setEntityService(entityService);
        
        WidgetSettingsForm theForm = createWidgetSettingsForm(keyMapping);
        
        createAuthentication(userAccount);
        secForm.setWrappedObject(theForm);
        
        validator.setChartMap(chartMap);
        validator.setDurationMap(durationMap);
        validator.setMetricMap(metricMap);
        validator.setFilterMap(filterMap);
        validator.setTierMap(tierMap);
        validator.setWidgetMetrics(widgetMetrics);
        
        controller.setCitationTypeService(citationTypeService);
        controller.setDashboardService(dashboardService);
        controller.setMerchantAccountService(merchantAccountService);
        controller.setWidgetSettingsValidator(validator);
        controller.setWidgetMetrics(widgetMetrics);
        controller.setWidgetFilterTypesMap(filterMap);
        
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, createTabHeadings());
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, dashboardCon.loadTabTreeToMapIfNecessary(request, "tabRandomId"));
        
        try {
            String json = controller.resetWidgetSettingsOptions(request, response);
            assertNotNull(json);
            
            WidgetSettingsOptions settingsOptions = (WidgetSettingsOptions) sessionTool
                    .getAttribute(WebCoreConstants.SESSION_WIDGET_SETTINGS_OPTIONS);
            assertNotNull(settingsOptions);
            
            json = controller.updateWidgetSettingsOptions(secForm, (BindingResult) errors, request, response, model);
            assertNotNull(json);
            
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
    
    @Test
    public void test_showWidgetSettings() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.getSession(true);
        
        MockHttpServletResponse response = new MockHttpServletResponse();
        
        createAuthentication(userAccount);
        
        SessionTool sessionTool = SessionTool.getInstance(request);
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        
        ModelMap model = new ModelMap();
        
        Widget widget = new Widget();
        widget.setId(1);
        widget.setRandomId(keyMapping.getRandomString(widget, "id"));
        
        request.setParameter("widgetID", widget.getRandomId());
        
        WidgetSettingsController controller = new WidgetSettingsController();
        DashboardServiceTestImpl dashboardService = new DashboardServiceTestImpl() {
            @Override
            public Widget findWidgetById(int widgetId) {
                Widget widget = new Widget();
                WidgetMetricType metric = new WidgetMetricType();
                metric.setRandomId("random");
                widget.setWidgetMetricType(metric);
                return widget;
            }
        };
        
        controller.setDashboardService(dashboardService);
        
        String reply = controller.showWidgetSettings(request, response, model);
        assertEquals("/dashboard/include/widgetSettings", reply);
    }
    
    private InputStream loadXml() throws Exception {
        InputStream is = WidgetSettingsControllerTest.class.getResourceAsStream("/widgetmetrics.xml");
        return is;
    }
    
    private WidgetSettingsForm createWidgetSettingsForm(RandomKeyMapping keyMapping) {
        WidgetRangeType range = new WidgetRangeType();
        WidgetFilterType filter = new WidgetFilterType();
        WidgetTierType tier = new WidgetTierType();
        WidgetChartType chart = new WidgetChartType();
        
        WidgetSettingsForm form = new WidgetSettingsForm();
        
        form.setMetricName("Revenue");
        chart.setId(WidgetConstants.CHART_TYPE_LIST);
        form.setChartType(keyMapping.getRandomString(chart, "id"));
        range.setId(WidgetConstants.RANGE_TYPE_LAST_YEAR);
        form.setRange(keyMapping.getRandomString(range, "id"));
        filter.setId(WidgetConstants.FILTER_TYPE_ALL);
        form.setFilter(keyMapping.getRandomString(filter, "id"));
        tier.setId(WidgetConstants.TIER_TYPE_HOUR);
        tier.setName("Hour");
        form.setTier1(keyMapping.getRandomString(tier, "id"));
        tier.setId(WidgetConstants.TIER_TYPE_RATE);
        tier.setName("Rate");
        form.setTier2(keyMapping.getRandomString(tier, "id"));
        tier.setId(WidgetConstants.TIER_TYPE_COLLECTION_TYPE);
        tier.setName("Collection Type");
        form.setTier3(keyMapping.getRandomString(tier, "id"));
        
        form.setLength(5);
        form.setTargetValue("500");
        form.setRandomId("randomId");
        form.setSectionRandomId("sectionRandomId");
        form.setWidgetName("testWidgetName");
        
        return form;
    }
    
    private WebSecurityForm<WidgetSettingsForm> createWebSecurityForm(boolean validTokenFlag) {
        WebSecurityForm<WidgetSettingsForm> secForm = new WebSecurityForm<WidgetSettingsForm>();
        if (validTokenFlag) {
            secForm.setPostToken("TEST123");
            secForm.setInitToken("TEST123");
        } else {
            secForm.setPostToken("TEST123");
            secForm.setInitToken("TEST1234");
        }
        return secForm;
    }
    
    private Tab createTabSectionsWidgets(int tabid, String tabRandomId) {
        Widget w = new Widget();
        w.setId(1);
        w.setRandomId("randomId");
        
        WidgetListType widgetListType = new WidgetListType();
        widgetListType.setId(3);
        WidgetMetricType widgetMetricType = new WidgetMetricType();
        widgetMetricType.setId(1);
        widgetMetricType.setName("Revenue");
        WidgetRangeType widgetRangeType = new WidgetRangeType();
        widgetRangeType.setId(0);
        widgetRangeType.setName("This Year");
        WidgetTierType widgetTier1Type = new WidgetTierType();
        widgetTier1Type.setId(2);
        widgetTier1Type.setName("Hour");
        WidgetTierType widgetTier2Type = new WidgetTierType();
        widgetTier2Type.setId(0);
        widgetTier2Type.setName("N/A");
        WidgetTierType widgetTier3Type = new WidgetTierType();
        widgetTier3Type.setId(0);
        widgetTier3Type.setName("N/A");
        WidgetFilterType widgetFilterType = new WidgetFilterType();
        widgetFilterType.setId(0);
        WidgetChartType widgetChartType = new WidgetChartType();
        widgetChartType.setId(1);
        WidgetLimitType widgetLimitType = new WidgetLimitType();
        widgetLimitType.setId(0);
        w.setWidgetLimitType(widgetLimitType);
        w.setWidgetListType(widgetListType);
        w.setWidgetMetricType(widgetMetricType);
        w.setWidgetRangeType(widgetRangeType);
        w.setWidgetTierTypeByWidgetTier1Type(widgetTier1Type);
        w.setWidgetTierTypeByWidgetTier2Type(widgetTier2Type);
        w.setWidgetTierTypeByWidgetTier3Type(widgetTier3Type);
        w.setWidgetFilterType(widgetFilterType);
        w.setWidgetChartType(widgetChartType);
        
        ArrayList<Widget> ws = new ArrayList<Widget>();
        ws.add(w);
        
        Section se = new Section();
        se.setId(1);
        se.setWidgets(ws);
        Map<String, Widget> map = new HashMap<String, Widget>();
        map.put(w.getRandomId(), w);
        se.setWidgetMap(map);
        ArrayList<Section> ss = new ArrayList<Section>();
        ss.add(se);
        
        Tab tab = new Tab();
        tab.setId(tabid);
        tab.setRandomId(tabRandomId);
        tab.setSections(ss);
        
        return tab;
    }
    
    private ArrayList<TabHeading> createTabHeadings() {
        TabHeading tabHeading = new TabHeading();
        tabHeading.setCurrent(WebCoreConstants.RESPONSE_TRUE);
        tabHeading.setId("tabRandomId");
        ArrayList<TabHeading> list = new ArrayList<TabHeading>();
        list.add(tabHeading);
        return list;
    }
    
    private UserAccount createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD);
        user.setUserPermissions(permissionList);
        user.setIsComplexPassword(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return userAccount;
    }
    
    private static void setUpChartsMap() {
        chartMap = new HashMap<Integer, String>(8);
        chartMap.put(WidgetConstants.CHART_TYPE_LIST, "List");
        chartMap.put(WidgetConstants.CHART_TYPE_BAR, "Bar");
        chartMap.put(WidgetConstants.CHART_TYPE_COLUMN, "Column");
        chartMap.put(WidgetConstants.CHART_TYPE_LINE, "Line");
        chartMap.put(WidgetConstants.CHART_TYPE_AREA, "Area");
        chartMap.put(WidgetConstants.CHART_TYPE_SINGLE_VALUE, "Single Value");
        chartMap.put(WidgetConstants.CHART_TYPE_PIE, "Pie");
        chartMap.put(WidgetConstants.CHART_TYPE_MAP, "Map");
    }
    
    private static void setUpFilterMap() {
        filterMap = new HashMap<Integer, String>(4);
        filterMap.put(WidgetConstants.FILTER_TYPE_ALL, "All");
        filterMap.put(WidgetConstants.FILTER_TYPE_TOP, "Top");
        filterMap.put(WidgetConstants.FILTER_TYPE_BOTTOM, "Bottom");
        filterMap.put(WidgetConstants.FILTER_TYPE_SUBSET, "Subset");
    }
    
    private static void setUpMetricMap() {
        metricMap = new HashMap<Integer, String>(11);
        metricMap.put(WidgetConstants.METRIC_TYPE_REVENUE, "Revenue");
        metricMap.put(WidgetConstants.METRIC_TYPE_COLLECTIONS, "Collections");
        metricMap.put(WidgetConstants.METRIC_TYPE_SETTLED_CARD, "Settled Card");
        metricMap.put(WidgetConstants.METRIC_TYPE_PURCHASES, "Purchases");
        metricMap.put(WidgetConstants.METRIC_TYPE_PAID_OCCUPANCY, "Paid Occupancy");
        metricMap.put(WidgetConstants.METRIC_TYPE_TURNOVER, "Turnover");
        metricMap.put(WidgetConstants.METRIC_TYPE_UTILIZATION, "Utilization");
        metricMap.put(WidgetConstants.METRIC_TYPE_DURATION, "Duration");
        metricMap.put(WidgetConstants.METRIC_TYPE_ACTIVE_ALERTS, "Active Alerts");
        metricMap.put(WidgetConstants.METRIC_TYPE_ALERT_STATUS, "Alert Status");
        metricMap.put(WidgetConstants.METRIC_TYPE_MAP, "Map");
        metricMap.put(WidgetConstants.METRIC_TYPE_AVERAGE_PRICE, "Average Price per Permit");
        metricMap.put(WidgetConstants.METRIC_TYPE_AVERAGE_REVENUE, "Average Revenue per Space");
        metricMap.put(WidgetConstants.METRIC_TYPE_AVERAGE_DURATION, "Average Duration per Permit");
    }
    
    private static void setUpTierMap() {
        tierMap = new HashMap<Integer, String>(30);
        tierMap.put(WidgetConstants.TIER_TYPE_NA, WebCoreConstants.NOT_APPLICABLE);
        tierMap.put(WidgetConstants.TIER_TYPE_HOUR, "Hour");
        tierMap.put(WidgetConstants.TIER_TYPE_DAY, "Day");
        tierMap.put(WidgetConstants.TIER_TYPE_MONTH, "Month");
        tierMap.put(WidgetConstants.TIER_TYPE_ALL_ORG, "All Organizations");
        tierMap.put(WidgetConstants.TIER_TYPE_ORG, "Organization");
        tierMap.put(WidgetConstants.TIER_TYPE_PARENT_LOCATION, "Parent Location");
        tierMap.put(WidgetConstants.TIER_TYPE_LOCATION, "Location");
        tierMap.put(WidgetConstants.TIER_TYPE_ROUTE, "Route");
        tierMap.put(WidgetConstants.TIER_TYPE_PAYSTATION, "Pay Station");
        tierMap.put(WidgetConstants.TIER_TYPE_RATE, "Rate");
        tierMap.put(WidgetConstants.TIER_TYPE_MERCHANT_ACCOUNT, "Merchant Account");
        tierMap.put(WidgetConstants.TIER_TYPE_REVENUE_TYPE, "Revenue Type");
        tierMap.put(WidgetConstants.TIER_TYPE_TRANSACTION_TYPE, "Transaction Type");
        tierMap.put(WidgetConstants.TIER_TYPE_COLLECTION_TYPE, "Collection Type");
        tierMap.put(WidgetConstants.TIER_TYPE_ALERT_TYPE, "Alert Type");
        tierMap.put(WidgetConstants.TIER_TYPE_CARD_PROCESS_METHOD_TYPE, "Card Processing Method Type");
        tierMap.put(WidgetConstants.TIER_TYPE_CITATION_TYPE, "Citation Type");
    }
    
    private static void setUpDurationMap() {
        durationMap = new HashMap<Integer, String>(14);
        durationMap.put(WidgetConstants.RANGE_TYPE_NOW, "now");
        durationMap.put(WidgetConstants.RANGE_TYPE_TODAY, "Today");
        durationMap.put(WidgetConstants.RANGE_TYPE_YESTERDAY, "Yesterday");
        durationMap.put(WidgetConstants.RANGE_TYPE_LAST_24HOURS, "Last 24 Hours");
        //		durationMap.put(WidgetConstants.RANGE_TYPE_THIS_WEEK, "This Week");
        //		durationMap.put(WidgetConstants.RANGE_TYPE_THIS_MONTH, "This Month");
        //		durationMap.put(WidgetConstants.RANGE_TYPE_THIS_YEAR, "This Year");
        durationMap.put(WidgetConstants.RANGE_TYPE_LAST_WEEK, "Last Week");
        durationMap.put(WidgetConstants.RANGE_TYPE_LAST_7DAYS, "Last 7 days");
        durationMap.put(WidgetConstants.RANGE_TYPE_LAST_MONTH, "Last_Month");
        durationMap.put(WidgetConstants.RANGE_TYPE_LAST_30DAYS, "Last 30 Days");
        durationMap.put(WidgetConstants.RANGE_TYPE_LAST_12MONTHS, "Last 12 Months");
        durationMap.put(WidgetConstants.RANGE_TYPE_LAST_YEAR, "Last Year");
        durationMap.put(WidgetConstants.RANGE_TYPE_YEAR_TO_DATE, "Year to Date");
    }
    
    private static void setUpLimitMap() {
        limitMap = new HashMap<Integer, Integer>(7);
        limitMap.put(WidgetConstants.LIMIT_TYPE_0, 0);
        limitMap.put(WidgetConstants.LIMIT_TYPE_5, 5);
        limitMap.put(WidgetConstants.LIMIT_TYPE_10, 10);
        limitMap.put(WidgetConstants.LIMIT_TYPE_15, 15);
        limitMap.put(WidgetConstants.LIMIT_TYPE_25, 25);
        limitMap.put(WidgetConstants.LIMIT_TYPE_50, 50);
        limitMap.put(WidgetConstants.LIMIT_TYPE_100, 100);
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                
                return userAccount;
            }
        };
    }
}
