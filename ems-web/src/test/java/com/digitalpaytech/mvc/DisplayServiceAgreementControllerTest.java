package com.digitalpaytech.mvc;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

import org.easymock.EasyMock;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAgreement;
import com.digitalpaytech.domain.ServiceAgreement;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerAgreementService;
import com.digitalpaytech.service.ServiceAgreementService;

public class DisplayServiceAgreementControllerTest {

	@Test
	public void test_showServiceAgreement() {
		MockHttpServletRequest request = new MockHttpServletRequest("GET", "/secure/dashboard/index.html");
		ModelMap model = new ModelMap();
		
		UserAccount userAccount = new UserAccount();
		Customer customer = new Customer();
		userAccount.setCustomer(customer);
		
		ServiceAgreementService serviceAgreementService = EasyMock.createMock(ServiceAgreementService.class);
		ServiceAgreement sa = new ServiceAgreement();
		sa.setId(8);
		sa.setContent("testContent".getBytes());
		EasyMock.expect(serviceAgreementService.findLatestServiceAgreementByCustomerId(2)).andReturn(sa);
		EasyMock.replay(serviceAgreementService);
		
		CustomerAgreementService customerAgreementService = EasyMock.createMock(CustomerAgreementService.class);
		CustomerAgreement customerAgreement = new CustomerAgreement();
		customerAgreement.setId(2);
		customerAgreement.setName("sara");
		customerAgreement.setTitle("testTitle");
		customerAgreement.setOrganization("testOrganization");
		EasyMock.expect(customerAgreementService.findCustomerAgreementByCustomerId(2)).andReturn(customerAgreement);
		EasyMock.replay(customerAgreementService);
		
		CustomerAdminService customerAdminService = EasyMock.createMock(CustomerAdminService.class);  
		EasyMock.expect(customerAdminService.findCustomerByCustomerId(2)).andReturn(customer);
		EasyMock.replay(customerAdminService);
		
		DisplayServiceAgreementController controller = new DisplayServiceAgreementController();
		controller.setCustomerAgreementService(customerAgreementService);
		controller.setServiceAgreementService(serviceAgreementService);
		controller.setCustomerAdminService(customerAdminService);
		
		createAuthentication(userAccount, true);
		
		String result = controller.showServiceAgreement(request, model);
		
		assertNotNull(result);
		assertEquals(result, "termsofService");
	}

	private void createAuthentication(UserAccount userAccount,
			boolean isComplexPassword) {

		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("Admin"));
		WebUser user = new WebUser(2, 2, "sara", "password", "salt", true,
				authorities);
		user.setIsComplexPassword(isComplexPassword);
		Authentication authentication = new UsernamePasswordAuthenticationToken(
				user, "password", authorities);
		SecurityContextHolder.getContext().setAuthentication(authentication);
	}
}
