package com.digitalpaytech.mvc.systemadmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.ActivityLog;
import com.digitalpaytech.domain.ActivityType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerEmail;
import com.digitalpaytech.domain.CustomerRole;
import com.digitalpaytech.domain.CustomerStatusType;
import com.digitalpaytech.domain.CustomerType;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.RoleStatusType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.domain.UserStatusType;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.UserAccountEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.ActivityLogServiceImpl;
import com.digitalpaytech.service.impl.CustomerRoleServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.RoleServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.CustomerServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObject;
import com.digitalpaytech.validation.UserSettingValidator;

public class SystemAdminUserAccountSettingControllerTest {
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private ModelMap model;
    private UserAccount userAccount;
    
    @Before
    public void setUp() {
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        this.session = new MockHttpSession();
        this.request.setSession(this.session);
        this.model = new ModelMap();
        
        this.userAccount = new UserAccount();
        this.userAccount.setId(1);
        this.userAccount.setUserName("TestUserName");
        
        final UserStatusType ust1 = new UserStatusType();
        ust1.setId(1);
        this.userAccount.setUserStatusType(ust1);
        
        final Set<UserRole> roles = new HashSet<>();
        final UserRole role = new UserRole();
        final Role r = new Role();
        final Set<RolePermission> rps = new HashSet<>();
        
        final RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        final Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
        rp1.setPermission(permission1);
        
        final RolePermission rp2 = new RolePermission();
        rp2.setId(2);
        final Permission permission2 = new Permission();
        permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
        rp2.setPermission(permission2);
        
        rps.add(rp1);
        rps.add(rp2);
        
        r.setId(3);
        r.setRolePermissions(rps);
        
        role.setId(3);
        role.setRole(r);
        roles.add(role);
        this.userAccount.setUserRoles(roles);
        
        final Customer customer = new Customer();
        customer.setId(2);
        this.userAccount.setCustomer(customer);
        createAuthentication(this.userAccount);
    }
    
    @After
    public void cleanUp() throws Exception {
        this.request = null;
        this.response = null;
        this.session = null;
        this.model = null;
        this.userAccount = null;
    }
    
    @Test
    public void test_getAdminUserAccountList() {
        final SystemAdminUserAccountSettingController controller = new SystemAdminUserAccountSettingController();
        controller.setUserAccountService(new UserAccountServiceImpl() {
            @Override
            public Collection<UserAccount> findUserAccountsByCustomerId(final int customerId) {
                final UserAccount ua1 = new UserAccount();
                ua1.setId(3);
                ua1.setFirstName("TestFirstName1");
                ua1.setLastName("TestLastName1");
                ua1.setUserName("TestUserName1");
                final UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                ua1.setUserStatusType(ust1);
                
                final UserAccount ua2 = new UserAccount();
                ua2.setId(7);
                ua2.setFirstName("TestFirstName2");
                ua2.setLastName("TestLastName2");
                ua2.setUserName("TestUserName1");
                final UserStatusType ust2 = new UserStatusType();
                ust2.setId(2);
                ua2.setUserStatusType(ust2);
                
                final Collection<UserAccount> lists = new ArrayList<>();
                lists.add(ua1);
                lists.add(ua2);
                
                return lists;
            }
            @Override       
            public Collection<UserAccount> findUserAccountsByCustomerIdAndUserAccountTypeId(final Integer customerId) {
                final UserAccount ua1 = new UserAccount();
                ua1.setId(3);
                ua1.setFirstName("TestFirstName1");
                ua1.setLastName("TestLastName1");
                ua1.setUserName("TestUserName1");
                final UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                ua1.setUserStatusType(ust1);
                
                final UserAccount ua2 = new UserAccount();
                ua2.setId(7);
                ua2.setFirstName("TestFirstName2");
                ua2.setLastName("TestLastName2");
                ua2.setUserName("TestUserName1");
                final UserStatusType ust2 = new UserStatusType();
                ust2.setId(2);
                ua2.setUserStatusType(ust2);
                
                final Collection<UserAccount> lists = new ArrayList<>();
                lists.add(ua1);
                lists.add(ua2);
                
                return lists;
            }
        });
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            @Override
            public Object merge(final Object entity) {
                return SystemAdminUserAccountSettingControllerTest.this.userAccount;
            }
        });
        
        final String result = controller.getAdminUserAccountList(this.request, this.response, this.model);
        assertNotNull(result);
        assertNotNull(this.model.get("userAccountList"));
        assertEquals(result, "/systemAdmin/adminUserAccount/adminUserAccount");
    }
    
    @Test
    public void test_getAdminUserAccountList_role_failure() {
        
        createFailedAuthentication();
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            @Override
            public Object merge(final Object entity) {
                return SystemAdminUserAccountSettingControllerTest.this.userAccount;
            }
        });
        
        final SystemAdminUserAccountSettingController controller = new SystemAdminUserAccountSettingController();
        final String result = controller.getAdminUserAccountList(this.request, this.response, this.model);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
        assertEquals(this.response.getStatus(), HttpStatus.UNAUTHORIZED.value());
    }
    
    @Test
    public void test_viewSystemAdminUserDetails() {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(this.request).getKeyMapping();
        
        final Collection<UserAccount> lists = getUserAccountList();
        
        final List<WebObject> randomized = (List<WebObject>) keyMapping.hideProperties(lists, WebCoreConstants.ID_LOOK_UP_NAME);
        this.request.setParameter("userAccountID", randomized.get(1).getPrimaryKey().toString());
        
        final SystemAdminUserAccountSettingController controller = new SystemAdminUserAccountSettingController();
        controller.setUserAccountService(new UserAccountServiceImpl() {
            @Override
            public UserAccount findUserAccount(final int userAccountId) {
                final UserAccount ua = new UserAccount();
                ua.setFirstName("William");
                ua.setLastName("Douglas");
                ua.setCustomerEmail(new CustomerEmail(SystemAdminUserAccountSettingControllerTest.this.userAccount.getCustomer(),
                        "william.douglas@t2-test.com"));
                ua.setUserName("williamd");
                
                final UserStatusType type = new UserStatusType();
                type.setId(1);
                type.setName("Enabled");
                
                final Set<UserRole> roles = new HashSet<>();
                final UserRole role = new UserRole();
                role.setId(1);
                final UserRole role2 = new UserRole();
                role2.setId(2);
                
                final Role r1 = new Role();
                r1.setId(1);
                r1.setName("Child Admin");
                final Role r2 = new Role();
                r2.setId(2);
                r2.setName("Customized Role");
                role.setRole(r1);
                role2.setRole(r2);
                roles.add(role);
                roles.add(role2);
                
                ua.setUserStatusType(type);
                ua.setUserRoles(roles);
                
                return ua;
            }
            
			@Override
            public Collection<UserAccount> findAliasUserAccountsByUserAccountId(final Integer userAccountId) {
				final UserAccount ua = new UserAccount();
				ua.setId(1);
				final Customer c1 = new Customer();
				c1.setId(1);
				ua.setCustomer(c1);
				
				final Collection<UserAccount> cua = new ArrayList<>();
				cua.add(ua);
				
				return cua;
			}            
            
        });
        
        controller.setActivityLogService(new ActivityLogServiceImpl() {
            @Override
            public Collection<ActivityLog> findActivityLogsByUserAccountId(final Integer userAccountId) {
                final Collection<ActivityLog> logs = new ArrayList<>();
                final ActivityLog log = new ActivityLog();
                final ActivityType type = new ActivityType();
                
                type.setId(101);
                type.setName("User Login");
                type.setDescription("test User Login");
                
                log.setId(1L);
                log.setActivityType(type);
                log.setActivityGmt(new GregorianCalendar().getTime());
                
                final ActivityLog log2 = new ActivityLog();
                final ActivityType type2 = new ActivityType();
                
                type2.setId(303);
                type2.setName("Add Coupon");
                type2.setDescription("Test Add Coupon");
                
                log2.setId(2L);
                log2.setActivityType(type2);
                log2.setActivityGmt(new GregorianCalendar().getTime());
                
                logs.add(log2);
                
                return logs;
            }
        });
        
        controller.setCustomerRoleService(new CustomerRoleServiceImpl() {
            @Override
            public Collection<CustomerRole> findCustomerRoleByCustomerId(final Integer customerId) {
                final Collection<CustomerRole> roles = new ArrayList<>();
                final CustomerRole role1 = new CustomerRole();
                role1.setId(1);
                final Role r1 = new Role();
                r1.setId(3);
                r1.setName("Child Admin");
                r1.setCustomerType(new CustomerType(3, "Child", new Date(), 1));
                final RoleStatusType rst = new RoleStatusType();
                rst.setId(1);
                r1.setRoleStatusType(rst);
                role1.setRole(r1);
                
                final CustomerRole role2 = new CustomerRole();
                role2.setId(2);
                final Role r2 = new Role();
                r2.setId(4);
                r2.setName("Basic");
                r2.setCustomerType(new CustomerType(3, "Child", new Date(), 1));
                r2.setRoleStatusType(rst);
                role2.setRole(r2);
                
                roles.add(role1);
                roles.add(role2);
                
                return roles;
            }
        });
        
		controller.setCustomerService(new CustomerServiceTestImpl() {
			@Override
            public List<Customer> findAllChildCustomers(final int customerId) {
				final List<Customer> customers = new ArrayList<>();
				final Customer customer = new Customer();
				customer.setId(1);
				customer.setCustomerType(new CustomerType(3, "Child", new Date(), 1));
				customers.add(customer);
				return customers;
			}
		});		        
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            @Override
            public Object merge(final Object entity) {
                
                final UserAccount userAccount = new UserAccount();
                final Customer customer = new Customer();
                final CustomerType type = new CustomerType();
                customer.setId(3);
                type.setId(3);
                customer.setCustomerType(type);
                userAccount.setCustomer(customer);
                userAccount.setId(2);
                
                final UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                userAccount.setUserStatusType(ust1);
                
                final Set<UserRole> roles = new HashSet<>();
                final UserRole role = new UserRole();
                final Role r = new Role();
                final Set<RolePermission> rps = new HashSet<>();
                
                final RolePermission rp1 = new RolePermission();
                rp1.setId(1);
                final Permission permission1 = new Permission();
                permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
                rp1.setPermission(permission1);
                
                final RolePermission rp2 = new RolePermission();
                rp2.setId(2);
                final Permission permission2 = new Permission();
                permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
                rp2.setPermission(permission2);
                
                rps.add(rp1);
                rps.add(rp2);
                
                r.setId(3);
                r.setRolePermissions(rps);
                
                role.setId(3);
                role.setRole(r);
                roles.add(role);
                userAccount.setUserRoles(roles);
                return userAccount;
            }
        });
        
        WidgetMetricsHelper.registerComponents();
        final String result = controller.viewSystemAdminUserDetails(this.request, this.response);
        assertNotNull(result);
    }
    
    @Test
    public void test_viewSystemAdminUserDetails_role_failure() {
        
        createFailedAuthentication();
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            @Override
            public Object merge(final Object entity) {
                return SystemAdminUserAccountSettingControllerTest.this.userAccount;
            }
        });
        
        final SystemAdminUserAccountSettingController controller = new SystemAdminUserAccountSettingController();
        final String result = controller.viewSystemAdminUserDetails(this.request, this.response);
        assertNotNull(result);
        assertEquals(result, "false");
    }
    
    @Test
    public void test_viewSystemAdminUserDetails_validation_failure() {
        
        this.request.setParameter("userAccountID", "!@#AGDEF");
        final SystemAdminUserAccountSettingController controller = new SystemAdminUserAccountSettingController();
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            @Override
            public Object merge(final Object entity) {
                return SystemAdminUserAccountSettingControllerTest.this.userAccount;
            }
        });
        
        final String result = controller.viewSystemAdminUserDetails(this.request, this.response);
        assertNotNull(result);
        assertEquals(result, "false");
    }
    
    @Test
    public void test_viewSystemAdminUserDetails_validation_failure_actualRouteID() {
        
        this.request.setParameter("userAccountID", "1111111111111111");
        final SystemAdminUserAccountSettingController controller = new SystemAdminUserAccountSettingController();
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            @Override
            public Object merge(final Object entity) {
                return SystemAdminUserAccountSettingControllerTest.this.userAccount;
            }
        });
        
        final String result = controller.viewSystemAdminUserDetails(this.request, this.response);
        assertNotNull(result);
        assertEquals(result, "false");
    }
    
    @Test
    public void test_getUserAccountForm() {
        
        final SystemAdminUserAccountSettingController controller = new SystemAdminUserAccountSettingController();
        final EntityServiceImpl serv = new EntityServiceImpl() {
            @Override
            public Object merge(final Object entity) {
                
                final UserAccount userAccount = new UserAccount();
                final Customer customer = new Customer();
                final CustomerType type = new CustomerType();
                customer.setId(3);
                type.setId(3);
                customer.setCustomerType(type);
                userAccount.setCustomer(customer);
                userAccount.setId(2);
                
                final UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                userAccount.setUserStatusType(ust1);
                
                final Set<UserRole> roles = new HashSet<>();
                final UserRole role = new UserRole();
                final Role r = new Role();
                final Set<RolePermission> rps = new HashSet<>();
                
                final RolePermission rp1 = new RolePermission();
                rp1.setId(1);
                final Permission permission1 = new Permission();
                permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
                rp1.setPermission(permission1);
                
                final RolePermission rp2 = new RolePermission();
                rp2.setId(2);
                final Permission permission2 = new Permission();
                permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
                rp2.setPermission(permission2);
                
                rps.add(rp1);
                rps.add(rp2);
                
                r.setId(3);
                r.setRolePermissions(rps);
                
                role.setId(3);
                role.setRole(r);
                roles.add(role);
                userAccount.setUserRoles(roles);
                return userAccount;
            }
        };
        controller.setEntityService(serv);
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(serv);
        
        controller.setCustomerRoleService(new CustomerRoleServiceImpl() {
            @Override
            public Collection<CustomerRole> findCustomerRoleByCustomerId(final Integer customerId) {
                final Collection<CustomerRole> roles = new ArrayList<>();
                final CustomerRole role1 = new CustomerRole();
                role1.setId(1);
                final Role r1 = new Role();
                r1.setId(3);
                r1.setName("Child Admin");
                r1.setCustomerType(new CustomerType(3, "Child", new Date(), 1));
                final RoleStatusType rst = new RoleStatusType();
                rst.setId(1);
                r1.setRoleStatusType(rst);
                role1.setRole(r1);
                
                final CustomerRole role2 = new CustomerRole();
                role2.setId(2);
                final Role r2 = new Role();
                r2.setId(4);
                r2.setName("Basic");
                r2.setCustomerType(new CustomerType(3, "Child", new Date(), 1));
                r2.setRoleStatusType(rst);
                role2.setRole(r2);
                
                roles.add(role1);
                roles.add(role2);
                
                return roles;
            }
        });
        
		controller.setCustomerService(new CustomerServiceTestImpl() {
			@Override
            public List<Customer> findAllChildCustomers(final int customerId) {
				final List<Customer> customers = new ArrayList<>();
				final Customer customer = new Customer();
				customer.setId(1);
				customer.setCustomerType(new CustomerType(3, "Child", new Date(), 1));
				customers.add(customer);
				return customers;
			}
		});
        
        final String result = controller.getUserAccountForm(this.request, this.response);
        assertNotNull(result);
    }
    
    @Test
    public void test_getUserAccountForm_update_flag() {
        
        final RandomKeyMapping keyMapping = SessionTool.getInstance(this.request).getKeyMapping();
        
        final Collection<UserAccount> lists = getUserAccountList();
        
        final List<WebObject> randomized = (List<WebObject>) keyMapping.hideProperties(lists, WebCoreConstants.ID_LOOK_UP_NAME);
        this.request.setParameter("userAccountID", randomized.get(1).getPrimaryKey().toString());
        
        final SystemAdminUserAccountSettingController controller = new SystemAdminUserAccountSettingController();
        final EntityServiceImpl serv = new EntityServiceImpl() {
            @Override
            public Object merge(final Object entity) {
                
                final UserAccount userAccount = new UserAccount();
                final Customer customer = new Customer();
                final CustomerType type = new CustomerType();
                customer.setId(3);
                type.setId(3);
                customer.setCustomerType(type);
                userAccount.setCustomer(customer);
                userAccount.setId(2);
                
                final UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                userAccount.setUserStatusType(ust1);
                
                final Set<UserRole> roles = new HashSet<>();
                final UserRole role = new UserRole();
                final Role r = new Role();
                final Set<RolePermission> rps = new HashSet<>();
                
                final RolePermission rp1 = new RolePermission();
                rp1.setId(1);
                final Permission permission1 = new Permission();
                permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
                rp1.setPermission(permission1);
                
                final RolePermission rp2 = new RolePermission();
                rp2.setId(2);
                final Permission permission2 = new Permission();
                permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
                rp2.setPermission(permission2);
                
                rps.add(rp1);
                rps.add(rp2);
                
                r.setId(3);
                r.setRolePermissions(rps);
                
                role.setId(3);
                role.setRole(r);
                roles.add(role);
                userAccount.setUserRoles(roles);
                return userAccount;
            }
        };
        controller.setEntityService(serv);
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(serv);
        
        controller.setCustomerRoleService(new CustomerRoleServiceImpl() {
            @Override
            public Collection<CustomerRole> findCustomerRoleByCustomerId(final Integer customerId) {
                final Collection<CustomerRole> roles = new ArrayList<>();
                final CustomerRole role1 = new CustomerRole();
                role1.setId(1);
                final Role r1 = new Role();
                r1.setId(3);
                r1.setName("Child Admin");
                r1.setCustomerType(new CustomerType(3, "Child", new Date(), 1));
                final RoleStatusType rst = new RoleStatusType();
                rst.setId(1);
                r1.setRoleStatusType(rst);
                role1.setRole(r1);
                
                final CustomerRole role2 = new CustomerRole();
                role2.setId(2);
                final Role r2 = new Role();
                r2.setId(4);
                r2.setName("Basic");
                r2.setRoleStatusType(rst);
                r2.setCustomerType(new CustomerType(3, "Child", new Date(), 1));
                role2.setRole(r2);
                
                roles.add(role1);
                roles.add(role2);
                
                return roles;
            }
        });
        
		controller.setCustomerService(new CustomerServiceTestImpl() {
			@Override
            public List<Customer> findAllChildCustomers(final int customerId) {
				final List<Customer> customers = new ArrayList<>();
				final Customer customer = new Customer();
				customer.setId(1);
				customer.setCustomerType(new CustomerType(3, "Child", new Date(), 1));
				customers.add(customer);
				return customers;
			}
		});		        
        
        final String result = controller.getUserAccountForm(this.request, this.response);
        assertNotNull(result);
    }
    
    @Test
    public void test_getUserAccountForm_role_fail() {
        createFailedAuthentication();
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            @Override
            public Object merge(final Object entity) {
                return SystemAdminUserAccountSettingControllerTest.this.userAccount;
            }
        });
        
        final SystemAdminUserAccountSettingController controller = new SystemAdminUserAccountSettingController();
        final String result = controller.getUserAccountForm(this.request, this.response);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
        assertEquals(this.response.getStatus(), HttpStatus.UNAUTHORIZED.value());
    }
    
    @Test
    public void test_saveSystemAdminUser() {
        
        final SystemAdminUserAccountSettingController controller = new SystemAdminUserAccountSettingController();
        final UserAccountEditForm userAccountEditForm = new UserAccountEditForm();
        userAccountEditForm.setFirstName("TEST");
        userAccountEditForm.setLastName("USER");
        userAccountEditForm.setStatus(true);
        userAccountEditForm.setUserName("testuser");
        userAccountEditForm.setUserPassword("password");
        userAccountEditForm.setPasswordConfirm("password");
        final Collection<Integer> roleIds = new ArrayList<>();
        roleIds.add(1);
        roleIds.add(2);
        userAccountEditForm.setRoleIds(roleIds);
        
        final WebSecurityForm<UserAccountEditForm> webSecurityForm = new WebSecurityForm<>(null, userAccountEditForm);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        final BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "userAccountEditForm");
        
        controller.setUserSettingValidator(new UserSettingValidator() {
            @Override
            public void validate(final WebSecurityForm<UserAccountEditForm> command, final Errors errors) {
                
            }
        });
        
        controller.setUserAccountService(new UserAccountServiceImpl() {
            @Override
            public void saveUserAccountAndUserRole(final UserAccount userAccount) {
                
            }
            
            @Override
            public UserAccount findUserAccount(final String userName) {
                return null;
            }
        });
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            @Override
            public Object merge(final Object entity) {
                return SystemAdminUserAccountSettingControllerTest.this.userAccount;
            }
        });
        
        controller.setPasswordEncoder(new ShaPasswordEncoder());
        
        final String res = controller.saveSystemAdminUser(webSecurityForm, result, this.response);
        assertNotNull(res);
        assertTrue(res.contains(WebCoreConstants.RESPONSE_TRUE));
    }
    
    @Test
    public void test_saveSystemAdminUser_update() {
        
        final SystemAdminUserAccountSettingController controller = new SystemAdminUserAccountSettingController();
        final UserAccountEditForm userAccountEditForm = new UserAccountEditForm();
        userAccountEditForm.setFirstName("TEST");
        userAccountEditForm.setLastName("USER");
        userAccountEditForm.setStatus(true);
        userAccountEditForm.setUserName("testuser");
        userAccountEditForm.setUserAccountId(2);
        final Collection<Integer> roleIds = new ArrayList<>();
        roleIds.add(1);
        roleIds.add(2);
        userAccountEditForm.setRoleIds(roleIds);
        
        final WebSecurityForm<UserAccountEditForm> webSecurityForm = new WebSecurityForm<>(null, userAccountEditForm);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_UPDATE);
        final BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "userAccountEditForm");
        
        controller.setUserSettingValidator(new UserSettingValidator() {
            @Override
            public void validate(final WebSecurityForm<UserAccountEditForm> command, final Errors errors) {
                
            }
        });
        
        controller.setUserAccountService(new UserAccountServiceImpl() {
            @Override
            public UserAccount findUserAccountAndEvict(final int userAccountId) {
                final UserAccount userAccount = new UserAccount();
                final Customer customer = new Customer();
                final CustomerStatusType type = new CustomerStatusType();
                customer.setId(3);
                type.setId(3);
                customer.setCustomerStatusType(type);
                userAccount.setCustomer(customer);
                userAccount.setId(2);
                
                final UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                userAccount.setUserStatusType(ust1);
                
                final Set<UserRole> roles = new HashSet<>();
                final UserRole role = new UserRole();
                final Role r = new Role();
                final Set<RolePermission> rps = new HashSet<>();
                
                final RolePermission rp1 = new RolePermission();
                rp1.setId(1);
                final Permission permission1 = new Permission();
                permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
                rp1.setPermission(permission1);
                
                final RolePermission rp2 = new RolePermission();
                rp2.setId(2);
                final Permission permission2 = new Permission();
                permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
                rp2.setPermission(permission2);
                
                rps.add(rp1);
                rps.add(rp2);
                
                r.setId(3);
                r.setRolePermissions(rps);
                
                role.setId(3);
                role.setRole(r);
                roles.add(role);
                userAccount.setUserRoles(roles);
                return userAccount;
            }
            
            @Override
            public Collection<UserAccount> findAliasUserAccountsByUserAccountId(final Integer userAccountId) {
                
                final UserAccount userAccount = new UserAccount();
                final Customer customer = new Customer();
                final CustomerStatusType type = new CustomerStatusType();
                customer.setId(3);
                type.setId(3);
                customer.setCustomerStatusType(type);
                userAccount.setCustomer(customer);
                userAccount.setId(2);
                
                final UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                userAccount.setUserStatusType(ust1);
                
                final Set<UserRole> roles = new HashSet<>();
                final UserRole role = new UserRole();
                final Role r = new Role();
                final Set<RolePermission> rps = new HashSet<>();
                
                final RolePermission rp1 = new RolePermission();
                rp1.setId(1);
                final Permission permission1 = new Permission();
                permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
                rp1.setPermission(permission1);
                
                final RolePermission rp2 = new RolePermission();
                rp2.setId(2);
                final Permission permission2 = new Permission();
                permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
                rp2.setPermission(permission2);
                
                rps.add(rp1);
                rps.add(rp2);
                
                r.setId(3);
                r.setRolePermissions(rps);
                
                role.setId(3);
                role.setRole(r);
                roles.add(role);
                userAccount.setUserRoles(roles);
                
                final Collection<UserAccount> userAccounts = new ArrayList<>();
                userAccounts.add(userAccount);
                return userAccounts;
                
            }
            
            @Override
            public void updateUserAccount(final UserAccount userAccount) {
                
            }            
            
            @Override
            public void deleteUserRoles(final UserAccount userAccount) {
            }            
            
            @Override
            public void updateUserAccountAndUserRoles(final UserAccount userAccount) {
                
            }
            
            @Override
            public UserAccount findUserAccount(final String userName) {
                return null;
            }
            
            @Override
            public UserAccount findUserAccount(final int userAccountId) {
                return SystemAdminUserAccountSettingControllerTest.this.userAccount;
            }
            
            public UserAccount findUserRoleByUserAccountIdAndRoleId(final int roleId, final int userAccountId) {
                return null;
            }            
        });
        
        controller.setRoleService(new RoleServiceImpl() {
            @Override
            public void saveCustomerRoles(final Set<CustomerRole> customerRoles) {
            }
        });        
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            @Override
            public Object merge(final Object entity) {
                return SystemAdminUserAccountSettingControllerTest.this.userAccount;
            }
        });
        
        final String res = controller.saveSystemAdminUser(webSecurityForm, result, this.response);
        assertNotNull(res);
        assertTrue(res.contains(WebCoreConstants.RESPONSE_TRUE));
    }
    
    @Test
    public void test_saveSystemAdminUser_update_password() {
        
        final SystemAdminUserAccountSettingController controller = new SystemAdminUserAccountSettingController();
        final UserAccountEditForm userAccountEditForm = new UserAccountEditForm();
        userAccountEditForm.setFirstName("TEST");
        userAccountEditForm.setLastName("USER");
        userAccountEditForm.setStatus(true);
        userAccountEditForm.setUserName("testuser");
        userAccountEditForm.setUserPassword("password");
        userAccountEditForm.setPasswordConfirm("password");
        userAccountEditForm.setUserAccountId(2);
        final Collection<Integer> roleIds = new ArrayList<>();
        roleIds.add(1);
        roleIds.add(2);
        userAccountEditForm.setRoleIds(roleIds);
        
        final WebSecurityForm<UserAccountEditForm> webSecurityForm = new WebSecurityForm<>(null, userAccountEditForm);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_UPDATE);
        final BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "userAccountEditForm");
        
        controller.setUserSettingValidator(new UserSettingValidator() {
            @Override
            public void validate(final WebSecurityForm<UserAccountEditForm> command, final Errors errors) {
                
            }
        });
        
        controller.setUserAccountService(new UserAccountServiceImpl() {
            @Override
            public UserAccount findUserAccountAndEvict(final int userAccountId) {
                final UserAccount userAccount = new UserAccount();
                final Customer customer = new Customer();
                final CustomerStatusType type = new CustomerStatusType();
                customer.setId(3);
                type.setId(3);
                customer.setCustomerStatusType(type);
                userAccount.setCustomer(customer);
                userAccount.setId(2);
                userAccount.setPasswordSalt("salt");
                
                final UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                userAccount.setUserStatusType(ust1);
                
                final Set<UserRole> roles = new HashSet<>();
                final UserRole role = new UserRole();
                final Role r = new Role();
                final Set<RolePermission> rps = new HashSet<>();
                
                final RolePermission rp1 = new RolePermission();
                rp1.setId(1);
                final Permission permission1 = new Permission();
                permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
                rp1.setPermission(permission1);
                
                final RolePermission rp2 = new RolePermission();
                rp2.setId(2);
                final Permission permission2 = new Permission();
                permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
                rp2.setPermission(permission2);
                
                rps.add(rp1);
                rps.add(rp2);
                
                r.setId(3);
                r.setRolePermissions(rps);
                
                role.setId(3);
                role.setRole(r);
                roles.add(role);
                userAccount.setUserRoles(roles);
                return userAccount;
            }
            
            @Override
            public Collection<UserAccount> findAliasUserAccountsByUserAccountId(final Integer userAccountId) {
                
                final UserAccount userAccount = new UserAccount();
                final Customer customer = new Customer();
                final CustomerStatusType type = new CustomerStatusType();
                customer.setId(3);
                type.setId(3);
                customer.setCustomerStatusType(type);
                userAccount.setCustomer(customer);
                userAccount.setId(2);
                
                final UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                userAccount.setUserStatusType(ust1);
                
                final Set<UserRole> roles = new HashSet<>();
                final UserRole role = new UserRole();
                final Role r = new Role();
                final Set<RolePermission> rps = new HashSet<>();
                
                final RolePermission rp1 = new RolePermission();
                rp1.setId(1);
                final Permission permission1 = new Permission();
                permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
                rp1.setPermission(permission1);
                
                final RolePermission rp2 = new RolePermission();
                rp2.setId(2);
                final Permission permission2 = new Permission();
                permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
                rp2.setPermission(permission2);
                
                rps.add(rp1);
                rps.add(rp2);
                
                r.setId(3);
                r.setRolePermissions(rps);
                
                role.setId(3);
                role.setRole(r);
                roles.add(role);
                userAccount.setUserRoles(roles);
                
                final Collection<UserAccount> userAccounts = new ArrayList<>();
                userAccounts.add(userAccount);
                return userAccounts;
                
            }
            
            @Override
            public void deleteUserRoles(final UserAccount userAccount) {
                
            }
            
            @Override
            public void updateUserAccount(final UserAccount userAccount) {
                
            }            
            
            @Override
            public void updateUserAccountAndUserRoles(final UserAccount userAccount) {
                
            }
            
            @Override
            public UserAccount findUserAccount(final String userName) {
                return null;
            }
            
            @Override
            public UserAccount findUserAccount(final int userAccountId) {
                return SystemAdminUserAccountSettingControllerTest.this.userAccount;
            }
            
            public UserAccount findUserRoleByUserAccountIdAndRoleId(final int roleId, final int userAccountId) {
                return null;
            }            
        });
        
        controller.setRoleService(new RoleServiceImpl() {
            @Override
            public void saveCustomerRoles(final Set<CustomerRole> customerRoles) {
            }
        });        
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            @Override
            public Object merge(final Object entity) {
                return SystemAdminUserAccountSettingControllerTest.this.userAccount;
            }
        });
        
        controller.setPasswordEncoder(new ShaPasswordEncoder());
        
        final String res = controller.saveSystemAdminUser(webSecurityForm, result, this.response);
        assertNotNull(res);
        assertTrue(res.contains(WebCoreConstants.RESPONSE_TRUE));
    }
    
    @Test
    public void test_saveSystemAdminUser_update_status_disabled() {
        
        final SystemAdminUserAccountSettingController controller = new SystemAdminUserAccountSettingController();
        final UserAccountEditForm userAccountEditForm = new UserAccountEditForm();
        userAccountEditForm.setFirstName("TEST");
        userAccountEditForm.setLastName("USER");
        userAccountEditForm.setUserName("testuser");
        userAccountEditForm.setUserAccountId(2);
        final Collection<Integer> roleIds = new ArrayList<>();
        roleIds.add(1);
        roleIds.add(2);
        userAccountEditForm.setRoleIds(roleIds);
        
        final WebSecurityForm<UserAccountEditForm> webSecurityForm = new WebSecurityForm<>(null, userAccountEditForm);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_UPDATE);
        final BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "userAccountEditForm");
        
        controller.setUserSettingValidator(new UserSettingValidator() {
            @Override
            public void validate(final WebSecurityForm<UserAccountEditForm> command, final Errors errors) {
                
            }
        });
        
        controller.setUserAccountService(new UserAccountServiceImpl() {
            @Override
            public UserAccount findUserAccountAndEvict(final int userAccountId) {
                final UserAccount userAccount = new UserAccount();
                final Customer customer = new Customer();
                final CustomerStatusType type = new CustomerStatusType();
                customer.setId(3);
                type.setId(3);
                customer.setCustomerStatusType(type);
                userAccount.setCustomer(customer);
                userAccount.setId(2);
                
                final UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                userAccount.setUserStatusType(ust1);
                
                final Set<UserRole> roles = new HashSet<>();
                final UserRole role = new UserRole();
                final Role r = new Role();
                final Set<RolePermission> rps = new HashSet<>();
                
                final RolePermission rp1 = new RolePermission();
                rp1.setId(1);
                final Permission permission1 = new Permission();
                permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
                rp1.setPermission(permission1);
                
                final RolePermission rp2 = new RolePermission();
                rp2.setId(2);
                final Permission permission2 = new Permission();
                permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
                rp2.setPermission(permission2);
                
                rps.add(rp1);
                rps.add(rp2);
                
                r.setId(3);
                r.setRolePermissions(rps);
                
                role.setId(3);
                role.setRole(r);
                roles.add(role);
                userAccount.setUserRoles(roles);
                return userAccount;
            }
            
            @Override
            public Collection<UserAccount> findAliasUserAccountsByUserAccountId(final Integer userAccountId) {
                
                final UserAccount userAccount = new UserAccount();
                final Customer customer = new Customer();
                final CustomerStatusType type = new CustomerStatusType();
                customer.setId(3);
                type.setId(3);
                customer.setCustomerStatusType(type);
                userAccount.setCustomer(customer);
                userAccount.setId(2);
                
                final UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                userAccount.setUserStatusType(ust1);
                
                final Set<UserRole> roles = new HashSet<>();
                final UserRole role = new UserRole();
                final Role r = new Role();
                final Set<RolePermission> rps = new HashSet<>();
                
                final RolePermission rp1 = new RolePermission();
                rp1.setId(1);
                final Permission permission1 = new Permission();
                permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
                rp1.setPermission(permission1);
                
                final RolePermission rp2 = new RolePermission();
                rp2.setId(2);
                final Permission permission2 = new Permission();
                permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
                rp2.setPermission(permission2);
                
                rps.add(rp1);
                rps.add(rp2);
                
                r.setId(3);
                r.setRolePermissions(rps);
                
                role.setId(3);
                role.setRole(r);
                roles.add(role);
                userAccount.setUserRoles(roles);
                
                final Collection<UserAccount> userAccounts = new ArrayList<>();
                userAccounts.add(userAccount);
                return userAccounts;
                
            }
            
            @Override
            public void deleteUserRoles(final UserAccount userAccount) {
                
            }
            
            @Override
            public void updateUserAccount(final UserAccount userAccount) {
                
            }            
            
            @Override
            public void updateUserAccountAndUserRoles(final UserAccount userAccount) {
                
            }
            
            @Override
            public UserAccount findUserAccount(final String userName) {
                return null;
            }
            
            @Override
            public UserAccount findUserAccount(final int userAccountId) {
                return SystemAdminUserAccountSettingControllerTest.this.userAccount;
            }
            
            public UserAccount findUserRoleByUserAccountIdAndRoleId(final int roleId, final int userAccountId) {
                return null;
            }            
        });
        
        controller.setRoleService(new RoleServiceImpl() {
            @Override
            public void saveCustomerRoles(final Set<CustomerRole> customerRoles) {
            }      
        });             
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            @Override
            public Object merge(final Object entity) {
                return SystemAdminUserAccountSettingControllerTest.this.userAccount;
            }
        });
        
        final String res = controller.saveSystemAdminUser(webSecurityForm, result, this.response);
        assertNotNull(res);
        assertTrue(res.contains(WebCoreConstants.RESPONSE_TRUE));
    }
    
    @Test
    public void test_saveSystemAdminUser_role_fail() {
        
        final UserAccountEditForm userAccountEditForm = new UserAccountEditForm();
        userAccountEditForm.setFirstName("TEST");
        userAccountEditForm.setLastName("USER");
        userAccountEditForm.setUserName("testuser");
        userAccountEditForm.setUserAccountId(2);
        final Collection<Integer> roleIds = new ArrayList<>();
        roleIds.add(1);
        roleIds.add(2);
        userAccountEditForm.setRoleIds(roleIds);
        
        final WebSecurityForm<UserAccountEditForm> webSecurityForm = new WebSecurityForm<>(null, userAccountEditForm);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_UPDATE);
        final BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "userAccountEditForm");
        
        createFailedAuthentication();
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            @Override
            public Object merge(final Object entity) {
                return SystemAdminUserAccountSettingControllerTest.this.userAccount;
            }
        });
        
        final SystemAdminUserAccountSettingController controller = new SystemAdminUserAccountSettingController();
        WidgetMetricsHelper.registerComponents();
        
        final String res = controller.saveSystemAdminUser(webSecurityForm, result, this.response);
        assertEquals(res, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_saveSystemAdminUser_validator_fail() {
        final SystemAdminUserAccountSettingController controller = new SystemAdminUserAccountSettingController();
        final UserAccountEditForm userAccountEditForm = new UserAccountEditForm();
        userAccountEditForm.setFirstName("TEST");
        userAccountEditForm.setLastName("USER");
        userAccountEditForm.setUserName("testuser");
        userAccountEditForm.setUserAccountId(2);
        final Collection<Integer> roleIds = new ArrayList<>();
        roleIds.add(1);
        roleIds.add(2);
        userAccountEditForm.setRoleIds(roleIds);
        
        final WebSecurityForm<UserAccountEditForm> webSecurityForm = new WebSecurityForm<>(null, userAccountEditForm);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_UPDATE);
        final BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "userAccountEditForm");
        
        controller.setUserSettingValidator(new UserSettingValidator() {
            @Override
            public void validate(final WebSecurityForm<UserAccountEditForm> webSecurityForm, final Errors errors) {
                webSecurityForm.addErrorStatus(new FormErrorStatus("postToken", "error.common.invalid.posttoken"));
            }
        });
        
        controller.setUserAccountService(createStaticUserAccountService());
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            @Override
            public Object merge(final Object entity) {
                return SystemAdminUserAccountSettingControllerTest.this.userAccount;
            }
        });
        WidgetMetricsHelper.registerComponents();
        
        final String res = controller.saveSystemAdminUser(webSecurityForm, result, this.response);
        assertNotNull(res);
        assertTrue(res.contains("\"errorStatus\":{\"errorFieldName\":\"postToken\",\"message\":\"error.common.invalid.posttoken\"}}"));
    }
    
    @Test
    public void test_deleteSystemAdminUser() {
        final RandomKeyMapping keyMapping = SessionTool.getInstance(this.request).getKeyMapping();
        final Collection<UserAccount> userAccounts = getUserAccounts();
        
        final List<WebObject> randomized = (List<WebObject>) keyMapping.hideProperties(userAccounts, "id");
        this.request.setParameter("userAccountID", randomized.get(1).getPrimaryKey().toString());
        
        final SystemAdminUserAccountSettingController controller = new SystemAdminUserAccountSettingController();
        controller.setUserAccountService(new UserAccountServiceImpl() {
            @Override
            public UserAccount findUserAccountAndEvict(final int userAccountId) {
                final UserAccount ua2 = new UserAccount();
                ua2.setId(7);
                ua2.setFirstName("TestFirstName2");
                ua2.setLastName("TestLastName2");
                final UserStatusType ust2 = new UserStatusType();
                ust2.setId(2);
                ua2.setUserStatusType(ust2);
                return ua2;
            }
            
			@Override
            public Collection<UserAccount> findAliasUserAccountsByUserAccountId(final Integer userAccountId) {
				
				final Collection<UserAccount> userAccounts = new ArrayList<>();
				final UserAccount ua2 = new UserAccount();
				ua2.setId(7);
				ua2.setFirstName("TestFirstName2");
				ua2.setLastName("TestLastName2");
				final UserStatusType ust2 = new UserStatusType();
				ust2.setId(2);
				ua2.setUserStatusType(ust2);
				userAccounts.add(ua2);
				return userAccounts;
			}                        
            
            @Override
            public void updateUserAccount(final UserAccount userAccount) {
                
            }
            		
			@Override
            public void deleteUserRoles(final UserAccount userAccount) {
			}            
        });
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            @Override
            public Object merge(final Object entity) {
                return SystemAdminUserAccountSettingControllerTest.this.userAccount;
            }
        });
        
        final String res = controller.deleteSystemAdminUser(this.request, this.response);
        assertNotNull(res);
        assertEquals(res, "true");
    }
    
    @Test
    public void test_deleteSystemAdminUser_userAccountID_validation_fail() {
        
        this.request.setParameter("userAccountID", "11111111111!1111");
        
        final SystemAdminUserAccountSettingController controller = new SystemAdminUserAccountSettingController();
        controller.setUserAccountService(new UserAccountServiceImpl() {
            @Override
            public UserAccount findUserAccountAndEvict(final int userAccountId) {
                final UserAccount ua2 = new UserAccount();
                ua2.setId(7);
                ua2.setFirstName("TestFirstName2");
                ua2.setLastName("TestLastName2");
                final UserStatusType ust2 = new UserStatusType();
                ust2.setId(2);
                ua2.setUserStatusType(ust2);
                return ua2;
            }
            
            @Override
            public void updateUserAccount(final UserAccount userAccount) {
                
            }
        });
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            @Override
            public Object merge(final Object entity) {
                return SystemAdminUserAccountSettingControllerTest.this.userAccount;
            }
        });
        
        final String res = controller.deleteSystemAdminUser(this.request, this.response);
        assertNotNull(res);
        assertEquals(res, "false");
    }
    
    @Test
    public void test_deleteSystemAdminUser_actual_userAccountID_validation_fail() {
        
        this.request.setParameter("userAccountID", "1111111111111111");
        
        final SystemAdminUserAccountSettingController controller = new SystemAdminUserAccountSettingController();
        controller.setUserAccountService(new UserAccountServiceImpl() {
            @Override
            public UserAccount findUserAccountAndEvict(final int userAccountId) {
                final UserAccount ua2 = new UserAccount();
                ua2.setId(7);
                ua2.setFirstName("TestFirstName2");
                ua2.setLastName("TestLastName2");
                final UserStatusType ust2 = new UserStatusType();
                ust2.setId(2);
                ua2.setUserStatusType(ust2);
                return ua2;
            }
            
            @Override
            public void updateUserAccount(final UserAccount userAccount) {
                
            }
        });
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            @Override
            public Object merge(final Object entity) {
                return SystemAdminUserAccountSettingControllerTest.this.userAccount;
            }
        });
        
        final String res = controller.deleteSystemAdminUser(this.request, this.response);
        assertNotNull(res);
        assertEquals(res, "false");
    }
    
    @Test
    public void test_deleteSystemAdminUser_role_fail() {
        
        createFailedAuthentication();
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            @Override
            public Object merge(final Object entity) {
                return SystemAdminUserAccountSettingControllerTest.this.userAccount;
            }
        });
        
        final SystemAdminUserAccountSettingController controller = new SystemAdminUserAccountSettingController();
        
        final String res = controller.deleteSystemAdminUser(this.request, this.response);
        assertNotNull(res);
        assertEquals(res, "false");
    }
    
    private void createAuthentication(final UserAccount userAccount) {
        final Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        final WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        user.setIsSystemAdmin(true);
        final Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        final Collection<Integer> permissionList = new ArrayList<>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private void createFailedAuthentication() {
        final Collection<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        final WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        user.setIsSystemAdmin(true);
        final Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        final Collection<Integer> permissionList = new ArrayList<>();
        permissionList.add(2);
        permissionList.add(3);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private Collection<UserAccount> getUserAccountList() {
        final Collection<UserAccount> lists = new ArrayList<>();
        final UserAccount u1 = new UserAccount();
        u1.setId(1);
        u1.setFirstName("John");
        u1.setLastName("Doe1");
        final UserAccount u2 = new UserAccount();
        u2.setId(2);
        u2.setFirstName("John");
        u2.setLastName("Doe2");
        
        lists.add(u1);
        lists.add(u2);
        
        return lists;
    }
    
    private Collection<UserAccount> getUserAccounts() {
        final UserAccount ua1 = new UserAccount();
        ua1.setId(3);
        ua1.setFirstName("TestFirstName1");
        ua1.setLastName("TestLastName1");
        final UserStatusType ust1 = new UserStatusType();
        ust1.setId(1);
        ua1.setUserStatusType(ust1);
        
        final UserAccount ua2 = new UserAccount();
        ua2.setId(7);
        ua2.setFirstName("TestFirstName2");
        ua2.setLastName("TestLastName2");
        final UserStatusType ust2 = new UserStatusType();
        ust2.setId(1);
        ua2.setUserStatusType(ust2);
        
        final Collection<UserAccount> lists = new ArrayList<>();
        lists.add(ua1);
        lists.add(ua2);
        
        return lists;
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            @Override
            public UserAccount findUserAccount(final int userAccountId) {
                final UserAccount ua = new UserAccount();
                ua.setId(2);
                ua.setUserName("TestUserName");
                return ua;
            }
            
            @Override
            public UserAccount findUserAccount(final String userName) {
                return null;
            }
            		
			@Override
            public void deleteUserRoles(final UserAccount userAccount) {
			}
			
            public UserAccount findUserRoleByUserAccountIdAndRoleId(final int roleId, final int userAccountId) {
                return null;
            }			
        };
    }
}
