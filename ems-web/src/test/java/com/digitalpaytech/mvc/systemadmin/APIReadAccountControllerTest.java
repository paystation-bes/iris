package com.digitalpaytech.mvc.systemadmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.WebServiceEndPoint;
import com.digitalpaytech.domain.WebServiceEndPointType;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.support.SOAPAccountEditForm;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.WebServiceEndPointService;
import com.digitalpaytech.service.WebServiceEndPointTypeService;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.CustomerServiceTestImpl;
import com.digitalpaytech.testing.services.impl.WebServiceEndPointServiceTestImpl;
import com.digitalpaytech.testing.services.impl.WebServiceEndPointTypeServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.systemadmin.SOAPAccountValidator;

public class APIReadAccountControllerTest {
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private ModelMap model;
    private UserAccount userAccount;
    private Customer customer;
    private APIReadAccountController controller;
    
    @Before
    public void setUp() throws Exception {
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        this.session = new MockHttpSession();
        this.request.setSession(session);
        this.model = new ModelMap();
        
        this.userAccount = new UserAccount();
        this.userAccount.setId(1);
        
        Customer customer = new Customer();
        customer.setId(2);
        userAccount.setCustomer(customer);
        createAuthentication(userAccount);
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        this.customer = new Customer();
        this.customer.setId(2);
        
        this.controller = createController();
    }
    
    @After
    public void tearDown() throws Exception {
        this.request = null;
        this.response = null;
        this.session = null;
        this.model = null;
        this.userAccount = null;
        this.customer = null;
    }
    
    @Test
    public void test_addSoapAccount() {
        
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String customerRandomId = keyMapping.getRandomString(this.customer, WebCoreConstants.ID_LOOK_UP_NAME);
        
        request.setParameter("customerID", customerRandomId);
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        
        String result = controller.addSoapAccount("STANDARD", new WebSecurityForm<SOAPAccountEditForm>(null, new SOAPAccountEditForm()), request, response, model);
        assertNotNull(result);
        assertEquals(result, "/systemAdmin/workspace/customers/include/soapAccountForm");
    }
    
    @Test
    public void test_deleteSoapAccount() {
        
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String customerRandomId = keyMapping.getRandomString(this.customer, WebCoreConstants.ID_LOOK_UP_NAME);
        
        WebServiceEndPoint wsep = new WebServiceEndPoint();
        wsep.setId(1);
        String raRandomId = keyMapping.getRandomString(wsep, WebCoreConstants.ID_LOOK_UP_NAME);
        
        request.setParameter("customerID", customerRandomId);
        request.setParameter("soapAccountId", raRandomId);
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        
        String result = controller.deleteSoapAccount(request, response, model);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_TRUE);
    }
    
    @Test
    public void test_editSoapAccountWSEPFail() {
        
        SOAPAccountEditForm form = createValidSoapAccountTestForm(2);
        form.setToken("exists");
        
        BindingResult result = new BeanPropertyBindingResult(form, "payStationEditForm");
        WebSecurityForm<SOAPAccountEditForm> soapAccountEditForm = new WebSecurityForm<SOAPAccountEditForm>(null, form);
        soapAccountEditForm.setPostToken(soapAccountEditForm.getInitToken());
        model.put("soapAccountEditForm", soapAccountEditForm);
        
        String resultStr = controller.saveSoapAccount((WebSecurityForm<SOAPAccountEditForm>) model.get("soapAccountEditForm"), result, request,
                                                      response, model);
        assertNotNull(resultStr);
        assertEquals(resultStr.indexOf("errorStatus") > -1, true);
    }
    
    @Test
    public void test_editSoapAccountSuccess() {
        
        SOAPAccountEditForm form = createValidSoapAccountTestForm(1);
        
        BindingResult result = new BeanPropertyBindingResult(form, "payStationEditForm");
        WebSecurityForm<SOAPAccountEditForm> soapAccountEditForm = new WebSecurityForm<SOAPAccountEditForm>(null, form);
        soapAccountEditForm.setPostToken(soapAccountEditForm.getInitToken());
        model.put("soapAccountEditForm", soapAccountEditForm);
        
        String resultStr = controller.saveSoapAccount((WebSecurityForm<SOAPAccountEditForm>) model.get("soapAccountEditForm"), result, request,
                                                      response, model);
        assertNotNull(resultStr);
        assertEquals(resultStr, WebCoreConstants.RESPONSE_TRUE);
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
        };
    }
    
    private WebServiceEndPointService createWSEPService() {
        return new WebServiceEndPointServiceTestImpl() {
            public WebServiceEndPoint findWebServiceEndById(int id) {
                WebServiceEndPoint wsep1 = new WebServiceEndPoint();
                wsep1.setId(1);
                wsep1.setToken("TRALALALALALAA1234567890");
                
                WebServiceEndPointType wsept1 = new WebServiceEndPointType();
                wsept1.setId((byte) 1);
                wsept1.setName("Soap Account Type 1");
                
                wsep1.setWebServiceEndPointType(wsept1);
                return wsep1;
            }
            
            public List<WebServiceEndPoint> findWebServiceEndPointByCustomerAndType(int customerId, byte typeId) {
                if (typeId == 1) {
                    return null;
                } else {
                    List<WebServiceEndPoint> wsepList = new ArrayList<WebServiceEndPoint>();
                    WebServiceEndPoint wsep1 = new WebServiceEndPoint();
                    wsep1.setId(1);
                    wsepList.add(wsep1);
                    return wsepList;
                }
            }
            
            public WebServiceEndPoint findWebServiceEndPointByToken(String token) {
                if (token.equals("exists")) {
                    WebServiceEndPoint wsep1 = new WebServiceEndPoint();
                    wsep1.setId(1);
                    return wsep1;
                } else {
                    return null;
                }
            }
            
            public void saveOrUpdate(WebServiceEndPoint ra) {
                return;
            }
            
            public void delete(WebServiceEndPoint ra) {
                return;
            }
            
        };
    }
    
    private WebServiceEndPointTypeService createWSEPTypeService() {
        return new WebServiceEndPointTypeServiceTestImpl() {
            public List<WebServiceEndPointType> loadAll() {
                List<WebServiceEndPointType> wseptList = new ArrayList<WebServiceEndPointType>();
                WebServiceEndPointType wsept1 = new WebServiceEndPointType();
                wsept1.setId((byte) 1);
                wsept1.setName("Soap Account Type 1");
                wseptList.add(wsept1);
                return wseptList;
            }
        };
    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setCustomerTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT);
        user.setIsComplexPassword(true);
        user.setIsSystemAdmin(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_LICENSES);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_LICENSES);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private CustomerService createCustomerService() {
        return new CustomerServiceTestImpl() {
            @Override
            public Customer findCustomer(Integer customerId) {
                Customer customer = new Customer();
                customer.setId(1);
                customer.setIsMigrated(true);
                return customer;
            }
        };
    }
    
    private APIReadAccountController createController() {
        controller = new APIReadAccountController();
        controller.setSoapAccountValidator(createSOAPAccountValidator());
        
        controller.setWebServiceEndPointService(createWSEPService());
        controller.setWebServiceEndPointTypeService(createWSEPTypeService());
        controller.setCustomerService(createCustomerService());
        
        return controller;
    }
    
    private SOAPAccountEditForm createValidSoapAccountTestForm(int type) {
        
        SOAPAccountEditForm form = new SOAPAccountEditForm();
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        String customerRandomId = keyMapping.getRandomString(this.customer, WebCoreConstants.ID_LOOK_UP_NAME);
        form.setCustomerId(customerRandomId);
        form.setType(type);
        form.setToken("12345678901234567890123456789012");
        return form;
    }
    
    private SOAPAccountValidator createSOAPAccountValidator() {
        return new SOAPAccountValidator() {
            public void validate(WebSecurityForm<SOAPAccountEditForm> command, Errors errors) {
            }
            
            public String getMessage(String code, Object... args) {
                return "TEST";
            }
            
            public String getMessage(String code) {
                return "TEST";
            }
        };
    }
}
