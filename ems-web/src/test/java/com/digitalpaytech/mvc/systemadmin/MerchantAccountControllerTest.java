package com.digitalpaytech.mvc.systemadmin;

import org.junit.Test;
import org.junit.Before;
import org.apache.log4j.Logger;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.util.Iterator;

import com.digitalpaytech.domain.GatewayProcessor;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.mvc.dto.TerminalAccount;

public class MerchantAccountControllerTest {
    private static final Logger LOG = Logger.getLogger(MerchantAccountControllerTest.class);
    private List<GatewayProcessor> gateProcs;
    
    @Before
    public void setGatewayProcessorsList() {
        gateProcs = new ArrayList<GatewayProcessor>();
        // PayPro id=8, AuthNet id=11
        gateProcs.add(new GatewayProcessor(new Integer(1), createProcessorWithIdOnly(8), "NOVA", "NOVA", new Date()));
        gateProcs.add(new GatewayProcessor(new Integer(2), createProcessorWithIdOnly(8), "Paymentech Tampa", "Paymentech Tampa", new Date()));
        gateProcs.add(new GatewayProcessor(new Integer(3), createProcessorWithIdOnly(8), "NOVA", "NOVA", new Date()));
        gateProcs.add(new GatewayProcessor(new Integer(4), createProcessorWithIdOnly(11), "First Data Merchant Services (FDMS)", "First Data Merchant Services (FDMS)", new Date()));
        gateProcs.add(new GatewayProcessor(new Integer(5), createProcessorWithIdOnly(11), "Nova Information Systems", "Nova Information Systems", new Date()));
        gateProcs.add(new GatewayProcessor(new Integer(6), createProcessorWithIdOnly(8), "First Data Merchant Services Nashille", "First Data Merchant Services Nashille", new Date()));
        gateProcs.add(new GatewayProcessor(new Integer(7), createProcessorWithIdOnly(11), "Global Payments", "Global Payments", new Date()));
        gateProcs.add(new GatewayProcessor(new Integer(8), createProcessorWithIdOnly(8), "First Data Merchant Services North", "First Data Merchant Services North", new Date()));
        gateProcs.add(new GatewayProcessor(new Integer(9), createProcessorWithIdOnly(11), "TSYS Acquiring Solutions", "TSYS Acquiring Solutions", new Date()));
        gateProcs.add(new GatewayProcessor(new Integer(10), createProcessorWithIdOnly(11), "RBS Lynk", "RBS Lynk", new Date()));
        gateProcs.add(new GatewayProcessor(new Integer(12), createProcessorWithIdOnly(8), "First Data Merchant Services South", "First Data Merchant Services South", new Date()));
        gateProcs.add(new GatewayProcessor(new Integer(13), createProcessorWithIdOnly(8), "Global East", "Global East", new Date()));
        gateProcs.add(new GatewayProcessor(new Integer(14), createProcessorWithIdOnly(8), "Moneris", "Moneris", new Date()));
        gateProcs.add(new GatewayProcessor(new Integer(15), createProcessorWithIdOnly(20), "Creditcall", "Creditcall", new Date()));
        gateProcs.add(new GatewayProcessor(new Integer(16), createProcessorWithIdOnly(11), "Chase Paymentech Solutions", "Chase Paymentech Solutions", new Date()));
        gateProcs.add(new GatewayProcessor(new Integer(17), createProcessorWithIdOnly(11), "Pay By Touch Payment Solutions", "Pay By Touch Payment Solutions", new Date()));
        gateProcs.add(new GatewayProcessor(new Integer(18), createProcessorWithIdOnly(8), "Vital", "Vital", new Date()));
        gateProcs.add(new GatewayProcessor(new Integer(19), createProcessorWithIdOnly(8), "BCE Emergis", "BCE Emergis", new Date()));
    }
    
    //public final Map<Integer, List<String>> loadGatewayProcessors(final List<GatewayProcessor> gatewayProcessors) {
    
    @Test
    public void loadGatewayProcessors() {
        final MerchantAccountController con = new MerchantAccountController();
        Map<Integer, String[]> map = con.loadGatewayProcessorsMap(gateProcs);
        
        LOG.debug("size: " + map.size());
        final Iterator<Integer> iter = map.keySet().iterator();
        Integer pid;
        String[] values;
        while (iter.hasNext()) {
            pid = iter.next();
            values = map.get(pid);
            
            LOG.debug("processorId / gateways: " + pid + "/ " + showValues(values));
            LOG.debug("-------------------------------------");
            
            if (pid == 8) {
                assertEquals(10, values.length);
            } else if (pid == 11) {
                assertEquals(7, values.length);
            } else if (pid == 99 || pid == 20) {
                assertEquals(1, values.length);
            }
        }
    }
    
    private final String showValues(final String[] values) {
        final StringBuilder bdr = new StringBuilder();
        for (String s : values) {
            bdr.append(s).append(", ");
        }
        return bdr.toString();
    }
    
    private Processor createProcessorWithIdOnly(final int id) {
        Processor p = new Processor();
        p.setId(id);
        return p;
    }
    
    @Test
    private void testConvertTerminalToJson() {
        TerminalAccount ta = new TerminalAccount(); 
        ta.setCustomerName("Oranj");
        ta.setTerminalStatus("Enabled");
        ta.setName("Oranj");
        ta.setId("1");
        ta.setTimeZone("US/Eastern");
        ta.setProcessorType("Creditcall");
        Map<String, String> terminalConfiguration = new HashMap<String, String>();
        terminalConfiguration.put("1", "one");
        terminalConfiguration.put("2", "one");
        ta.setTerminalConfiguration(terminalConfiguration);
        MerchantAccountController controller = new MerchantAccountController();
        String json = null;
     //   json =  controller.convertTerminalToJson(ta);
        System.out.println(json);
    }
    

}
