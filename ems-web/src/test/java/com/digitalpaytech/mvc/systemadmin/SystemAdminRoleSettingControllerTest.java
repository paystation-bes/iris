package com.digitalpaytech.mvc.systemadmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.data.RolePermissionInfo;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerRole;
import com.digitalpaytech.domain.CustomerType;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.RoleStatusType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.domain.UserStatusType;
import com.digitalpaytech.dto.customeradmin.RoleSettingInfo;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.RoleEditForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.CustomerRoleServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.PermissionServiceImpl;
import com.digitalpaytech.service.impl.RoleServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.CustomerServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObject;
import com.digitalpaytech.validation.RoleSettingValidator;

public class SystemAdminRoleSettingControllerTest {
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private ModelMap model;
    private UserAccount userAccount;
    
    @Before
    public void setUp() throws Exception {
        
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        session = new MockHttpSession();
        this.request.setSession(session);
        this.model = new ModelMap();
        
        userAccount = new UserAccount();
        userAccount.setId(1);
        
        UserStatusType ust1 = new UserStatusType();
        ust1.setId(2);
        userAccount.setUserStatusType(ust1);
        
        Set<UserRole> roles = new HashSet<UserRole>();
        UserRole role = new UserRole();
        Role r = new Role();
        Set<RolePermission> rps = new HashSet<RolePermission>();
        
        RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
        rp1.setPermission(permission1);
        
        RolePermission rp2 = new RolePermission();
        rp2.setId(2);
        Permission permission2 = new Permission();
        permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
        rp2.setPermission(permission2);
        
        rps.add(rp1);
        rps.add(rp2);
        
        r.setId(3);
        r.setRolePermissions(rps);
        
        role.setId(3);
        role.setRole(r);
        roles.add(role);
        userAccount.setUserRoles(roles);
        
        Customer customer = new Customer();
        customer.setId(1);
        userAccount.setCustomer(customer);
        createAuthentication(userAccount);
    }
    
    @After
    public void tearDown() throws Exception {
        request = null;
        response = null;
        session = null;
        model = null;
    }
    
    @Test
    public void test_getSystemAdminRoleList() {
        SystemAdminRoleSettingController controller = new SystemAdminRoleSettingController();
        EntityServiceImpl serv = new EntityServiceImpl() {
            public Object merge(Object entity) {
                
                UserAccount userAccount = new UserAccount();
                Customer customer = new Customer();
                CustomerType type = new CustomerType();
                customer.setId(3);
                type.setId(3);
                customer.setCustomerType(type);
                userAccount.setCustomer(customer);
                userAccount.setId(2);
                
                UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                userAccount.setUserStatusType(ust1);
                
                Set<UserRole> roles = new HashSet<UserRole>();
                UserRole role = new UserRole();
                Role r = new Role();
                Set<RolePermission> rps = new HashSet<RolePermission>();
                
                RolePermission rp1 = new RolePermission();
                rp1.setId(1);
                Permission permission1 = new Permission();
                permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
                rp1.setPermission(permission1);
                
                RolePermission rp2 = new RolePermission();
                rp2.setId(2);
                Permission permission2 = new Permission();
                permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
                rp2.setPermission(permission2);
                
                rps.add(rp1);
                rps.add(rp2);
                
                r.setId(3);
                r.setRolePermissions(rps);
                
                role.setId(3);
                role.setRole(r);
                roles.add(role);
                userAccount.setUserRoles(roles);
                return userAccount;
            }
        };
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(serv);
        
        controller.setCustomerRoleService(new CustomerRoleServiceImpl() {
            public Collection<CustomerRole> findCustomerRoleByCustomerId(Integer customerId) {
                Collection<CustomerRole> roles = new ArrayList<CustomerRole>();
                CustomerRole role1 = new CustomerRole();
                role1.setId(1);
                Role r1 = new Role();
                r1.setId(3);
                r1.setName("Child Admin");
                RoleStatusType rst = new RoleStatusType();
                rst.setId(1);
                r1.setRoleStatusType(rst);
                r1.setCustomerType(new CustomerType(1, "Test", new Date(), 1));
                role1.setRole(r1);
                
                CustomerRole role2 = new CustomerRole();
                role2.setId(2);
                Role r2 = new Role();
                r2.setId(4);
                r2.setName("Basic");
                r2.setRoleStatusType(rst);
                r2.setCustomerType(new CustomerType(2, "Test2", new Date(), 1));
                role2.setRole(r2);
                
                roles.add(role1);
                roles.add(role2);
                
                return roles;
            }
        });
        
        String result = controller.getSystemAdminRoleList(request, response, model);
        assertNotNull(result);
        assertEquals(result, "/systemAdmin/adminUserAccount/userRole");
    }
    
    @Test
    public void test_getSystemAdminRoleList_role_fail() {
        
        EntityServiceImpl serv = new EntityServiceImpl() {
            public Object merge(Object entity) {
                return new UserAccount();
            }
        };
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(serv);
        
        createFailedAuthentication();
        
        SystemAdminRoleSettingController controller = new SystemAdminRoleSettingController();
        String result = controller.getSystemAdminRoleList(request, response, model);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
        assertEquals(response.getStatus(), HttpStatus.UNAUTHORIZED.value());
    }
    
    @Test
    public void test_viewSystemAdminRoleDetails() {
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        List<RoleSettingInfo> infos = (List<RoleSettingInfo>) getRoleSettingInfo();
        request.setParameter("roleID", ((RoleSettingInfo) infos.get(1)).getRandomizedRoleId());
        
        SystemAdminRoleSettingController controller = new SystemAdminRoleSettingController();
        
        controller.setRoleService(new RoleServiceImpl() {
            public void updateRoleAndRolePermission(Role role) {
                
            }
            
            public Role findRoleByRoleIdAndEvict(Integer roleId) {
                Role role = new Role();
                Role cloneRole = new Role();
                RoleStatusType type = new RoleStatusType();
                type.setId(1);
                role.setId(3);
                role.setRoleStatusType(type);
                CustomerType customerType = new CustomerType();
                customerType.setId(WebCoreConstants.CUSTOMER_TYPE_DPT);
                role.setCustomerType(customerType);
                
                return role;
            }
            
            @Override
            public Role findRoleByName(final Integer customerId, final String roleName) {
                return null;
            }
        });
        
        controller.setPermissionService(new PermissionServiceImpl() {
            public Collection<RolePermissionInfo> findAllPermissionsByRoleId(Integer roleId) {
                RolePermissionInfo info1 = new RolePermissionInfo();
                info1.setRoleName("Child Admin");
                info1.setStatus(1);
                info1.setParentPermissionName("Alerts Management");
                info1.setChildPermissionName("View Current Alerts");
                
                RolePermissionInfo info2 = new RolePermissionInfo();
                info2.setRoleName("Child Admin");
                info2.setStatus(1);
                info2.setParentPermissionName("Collections Management");
                info2.setChildPermissionName("View Collection Status");
                
                Collection<RolePermissionInfo> result = new ArrayList<RolePermissionInfo>();
                result.add(info1);
                result.add(info2);
                return result;
            }
            
            public Collection<RolePermissionInfo> findAllPermissionsByCustomerTypeId(Integer customerTypeId) {
                RolePermissionInfo info1 = new RolePermissionInfo();
                info1.setRoleName("Child Admin");
                info1.setParentPermissionName("Alerts Management");
                info1.setChildPermissionName("View Current Alerts");
                
                RolePermissionInfo info2 = new RolePermissionInfo();
                info2.setRoleName("Child Admin");
                info2.setParentPermissionName("Collections Management");
                info2.setChildPermissionName("View Collection Status");
                
                Collection<RolePermissionInfo> result = new ArrayList<RolePermissionInfo>();
                result.add(info1);
                result.add(info2);
                return result;
            }
        });
        
        controller.setCustomerService(createCustomerService());
        controller.setUserAccountService(createUserAccountService());
        
        EntityServiceImpl serv = new EntityServiceImpl() {
            public Object merge(Object entity) {
                
                UserAccount userAccount = new UserAccount();
                Customer customer = new Customer();
                CustomerType type = new CustomerType();
                customer.setId(3);
                type.setId(3);
                customer.setCustomerType(type);
                userAccount.setCustomer(customer);
                userAccount.setId(2);
                
                UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                userAccount.setUserStatusType(ust1);
                
                Set<UserRole> roles = new HashSet<UserRole>();
                UserRole role = new UserRole();
                Role r = new Role();
                Set<RolePermission> rps = new HashSet<RolePermission>();
                
                RolePermission rp1 = new RolePermission();
                rp1.setId(1);
                Permission permission1 = new Permission();
                permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
                rp1.setPermission(permission1);
                
                RolePermission rp2 = new RolePermission();
                rp2.setId(2);
                Permission permission2 = new Permission();
                permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
                rp2.setPermission(permission2);
                
                rps.add(rp1);
                rps.add(rp2);
                
                r.setId(3);
                r.setRolePermissions(rps);
                
                role.setId(3);
                role.setRole(r);
                roles.add(role);
                userAccount.setUserRoles(roles);
                return userAccount;
            }
        };
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(serv);
        
        WidgetMetricsHelper.registerComponents();
        String result = controller.viewSystemAdminRoleDetails(request, response);
        assertNotNull(result);
    }
    
    @Test
    public void test_viewSystemAdminRoleDetails_role_fail() {
        
        createFailedAuthentication();
        
        SystemAdminRoleSettingController controller = new SystemAdminRoleSettingController();
        String result = controller.viewSystemAdminRoleDetails(request, response);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_viewSystemAdminRoleDetails_roleID_validation_fail() {
        request.setParameter("roleID", "11111!111111");
        
        SystemAdminRoleSettingController controller = new SystemAdminRoleSettingController();
        
        EntityServiceImpl serv = new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        };
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(serv);
        
        String result = controller.viewSystemAdminRoleDetails(request, response);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_viewSystemAdminRoleDetails_actualRoleID_validation_fail() {
        request.setParameter("roleID", "1111111111111111");
        
        SystemAdminRoleSettingController controller = new SystemAdminRoleSettingController();
        EntityServiceImpl serv = new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        };
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(serv);
        
        String result = controller.viewSystemAdminRoleDetails(request, response);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_getSystemAdminUserAccountForm() {
        
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        SystemAdminRoleSettingController controller = new SystemAdminRoleSettingController();
        EntityServiceImpl serv = new EntityServiceImpl() {
            public Object merge(Object entity) {
                
                UserAccount userAccount = new UserAccount();
                Customer customer = new Customer();
                CustomerType type = new CustomerType();
                customer.setId(3);
                type.setId(3);
                customer.setCustomerType(type);
                userAccount.setCustomer(customer);
                userAccount.setId(2);
                
                UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                userAccount.setUserStatusType(ust1);
                
                Set<UserRole> roles = new HashSet<UserRole>();
                UserRole role = new UserRole();
                Role r = new Role();
                Set<RolePermission> rps = new HashSet<RolePermission>();
                
                RolePermission rp1 = new RolePermission();
                rp1.setId(1);
                Permission permission1 = new Permission();
                permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
                rp1.setPermission(permission1);
                
                RolePermission rp2 = new RolePermission();
                rp2.setId(2);
                Permission permission2 = new Permission();
                permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
                rp2.setPermission(permission2);
                
                rps.add(rp1);
                rps.add(rp2);
                
                r.setId(3);
                r.setRolePermissions(rps);
                
                role.setId(3);
                role.setRole(r);
                roles.add(role);
                userAccount.setUserRoles(roles);
                return userAccount;
            }
        };
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(serv);
        
        controller.setPermissionService(new PermissionServiceImpl() {
            public Collection<RolePermissionInfo> findAllPermissionsByCustomerTypeId(Integer customerTypeId) {
                RolePermissionInfo info1 = new RolePermissionInfo();
                info1.setRoleName("Child Admin");
                info1.setParentPermissionName("Alerts Management");
                info1.setChildPermissionName("View Current Alerts");
                
                RolePermissionInfo info2 = new RolePermissionInfo();
                info2.setRoleName("Child Admin");
                info2.setParentPermissionName("Collections Management");
                info2.setChildPermissionName("View Collection Status");
                
                Collection<RolePermissionInfo> result = new ArrayList<RolePermissionInfo>();
                result.add(info1);
                result.add(info2);
                return result;
            }
        });
        
        WidgetMetricsHelper.registerComponents();
        String result = controller.getSystemAdminUserRoleForm(request, response, model);
        
        assertNotNull(result);
    }
    
    @Test
    public void test_getSystemAdminUserAccountForm_role_fail() {
        
        createFailedAuthentication();
        
        SystemAdminRoleSettingController controller = new SystemAdminRoleSettingController();
        EntityServiceImpl serv = new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        };
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(serv);
        
        String result = controller.getSystemAdminUserRoleForm(request, response, model);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
        assertEquals(response.getStatus(), HttpStatus.UNAUTHORIZED.value());
    }
    
    @Test
    public void test_getSystemAdminUserAccountForm_actionFlag_update() {
        
        request.setParameter("roleID", "1111111111111111");
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        SystemAdminRoleSettingController controller = new SystemAdminRoleSettingController();
        EntityServiceImpl serv = new EntityServiceImpl() {
            public Object merge(Object entity) {
                
                UserAccount userAccount = new UserAccount();
                Customer customer = new Customer();
                CustomerType type = new CustomerType();
                customer.setId(3);
                type.setId(3);
                customer.setCustomerType(type);
                userAccount.setCustomer(customer);
                userAccount.setId(2);
                
                UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                userAccount.setUserStatusType(ust1);
                
                Set<UserRole> roles = new HashSet<UserRole>();
                UserRole role = new UserRole();
                Role r = new Role();
                Set<RolePermission> rps = new HashSet<RolePermission>();
                
                RolePermission rp1 = new RolePermission();
                rp1.setId(1);
                Permission permission1 = new Permission();
                permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
                rp1.setPermission(permission1);
                
                RolePermission rp2 = new RolePermission();
                rp2.setId(2);
                Permission permission2 = new Permission();
                permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
                rp2.setPermission(permission2);
                
                rps.add(rp1);
                rps.add(rp2);
                
                r.setId(3);
                r.setRolePermissions(rps);
                
                role.setId(3);
                role.setRole(r);
                roles.add(role);
                userAccount.setUserRoles(roles);
                return userAccount;
            }
        };
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(serv);
        
        controller.setPermissionService(new PermissionServiceImpl() {
            public Collection<RolePermissionInfo> findAllPermissionsByCustomerTypeId(Integer customerTypeId) {
                RolePermissionInfo info1 = new RolePermissionInfo();
                info1.setRoleName("Child Admin");
                info1.setParentPermissionName("Alerts Management");
                info1.setChildPermissionName("View Current Alerts");
                
                RolePermissionInfo info2 = new RolePermissionInfo();
                info2.setRoleName("Child Admin");
                info2.setParentPermissionName("Collections Management");
                info2.setChildPermissionName("View Collection Status");
                
                Collection<RolePermissionInfo> result = new ArrayList<RolePermissionInfo>();
                result.add(info1);
                result.add(info2);
                return result;
            }
        });
        
        WidgetMetricsHelper.registerComponents();
        String result = controller.getSystemAdminUserRoleForm(request, response, model);
        
        assertNotNull(result);
    }
    
    @Test
    public void test_saveSystemAdminRole() {
        SystemAdminRoleSettingController controller = new SystemAdminRoleSettingController();
        controller.setRoleSettingValidator(new RoleSettingValidator() {
            public void validate(WebSecurityForm<RoleEditForm> command, Errors errors) {
                
            }
        });
        
        RoleEditForm form = new RoleEditForm();
        form.setRoleId(1);
        form.setRoleName("TestRole");
        form.setStatusEnabled(true);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(1102);
        permissionList.add(1103);
        permissionList.add(1105);
        form.setPermissionIds(permissionList);
        form.setTypeId(WebCoreConstants.CUSTOMER_TYPE_DPT);
        
        WebSecurityForm<RoleEditForm> webSecurityForm = new WebSecurityForm<RoleEditForm>(null, form);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "roleEditForm");
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                
                UserAccount userAccount = new UserAccount();
                Customer customer = new Customer();
                CustomerType type = new CustomerType();
                customer.setId(3);
                type.setId(3);
                customer.setCustomerType(type);
                userAccount.setCustomer(customer);
                userAccount.setId(2);
                
                UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                userAccount.setUserStatusType(ust1);
                
                Set<UserRole> roles = new HashSet<UserRole>();
                UserRole role = new UserRole();
                Role r = new Role();
                Set<RolePermission> rps = new HashSet<RolePermission>();
                
                RolePermission rp1 = new RolePermission();
                rp1.setId(1);
                Permission permission1 = new Permission();
                permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
                rp1.setPermission(permission1);
                
                RolePermission rp2 = new RolePermission();
                rp2.setId(2);
                Permission permission2 = new Permission();
                permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
                rp2.setPermission(permission2);
                
                rps.add(rp1);
                rps.add(rp2);
                
                r.setId(3);
                r.setRolePermissions(rps);
                
                role.setId(3);
                role.setRole(r);
                roles.add(role);
                userAccount.setUserRoles(roles);
                return userAccount;
            }
        });
        
        controller.setCustomerRoleService(new CustomerRoleServiceImpl() {
            public Collection<CustomerRole> findCustomerRoleByCustomerId(Integer customerId) {
                Collection<CustomerRole> roles = new ArrayList<CustomerRole>();
                CustomerRole role1 = new CustomerRole();
                role1.setId(1);
                Role r1 = new Role();
                r1.setId(3);
                r1.setName("Child Admin");
                role1.setRole(r1);
                
                CustomerRole role2 = new CustomerRole();
                role2.setId(2);
                Role r2 = new Role();
                r2.setId(4);
                r2.setName("Basic");
                role2.setRole(r2);
                
                roles.add(role1);
                roles.add(role2);
                
                return roles;
            }
        });
        
        controller.setRoleService(new RoleServiceImpl() {
            public void saveRoleAndRolePermission(Role role) {
                
            }
            
            @Override
            public Role findRoleByName(Integer customerId, String roleName) {
                return null;
            }
        });
        
        String res = controller.saveSystemAdminRole(webSecurityForm, result, response);
        assertNotNull(res);
        assertTrue(res.contains(WebCoreConstants.RESPONSE_TRUE));
    }
    
    @Test
    public void test_saveSystemAdminRole_update() {
        SystemAdminRoleSettingController controller = new SystemAdminRoleSettingController();
        controller.setRoleSettingValidator(new RoleSettingValidator() {
            public void validate(WebSecurityForm<RoleEditForm> command, Errors errors) {
                
            }
        });
        
        RoleEditForm form = new RoleEditForm();
        form.setRoleId(1);
        form.setRoleName("TestRole");
        form.setStatusEnabled(true);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(1102);
        permissionList.add(1103);
        permissionList.add(1105);
        form.setPermissionIds(permissionList);
        form.setTypeId(WebCoreConstants.CUSTOMER_TYPE_DPT);
        
        WebSecurityForm<RoleEditForm> webSecurityForm = new WebSecurityForm<RoleEditForm>(null, form);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_UPDATE);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "roleEditForm");
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                
                UserAccount userAccount = new UserAccount();
                Customer customer = new Customer();
                CustomerType type = new CustomerType();
                customer.setId(3);
                type.setId(3);
                customer.setCustomerType(type);
                userAccount.setCustomer(customer);
                userAccount.setId(2);
                
                UserStatusType ust1 = new UserStatusType();
                ust1.setId(2);
                userAccount.setUserStatusType(ust1);
                
                Set<UserRole> roles = new HashSet<UserRole>();
                UserRole role = new UserRole();
                Role r = new Role();
                Set<RolePermission> rps = new HashSet<RolePermission>();
                
                RolePermission rp1 = new RolePermission();
                rp1.setId(1);
                Permission permission1 = new Permission();
                permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
                rp1.setPermission(permission1);
                
                RolePermission rp2 = new RolePermission();
                rp2.setId(2);
                Permission permission2 = new Permission();
                permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
                rp2.setPermission(permission2);
                
                rps.add(rp1);
                rps.add(rp2);
                
                r.setId(3);
                r.setRolePermissions(rps);
                
                role.setId(3);
                role.setRole(r);
                roles.add(role);
                userAccount.setUserRoles(roles);
                return userAccount;
            }
        });
        
        controller.setCustomerRoleService(new CustomerRoleServiceImpl() {
            public Collection<CustomerRole> findCustomerRoleByCustomerId(Integer customerId) {
                Collection<CustomerRole> roles = new ArrayList<CustomerRole>();
                CustomerRole role1 = new CustomerRole();
                role1.setId(1);
                Role r1 = new Role();
                r1.setId(3);
                r1.setName("Child Admin");
                role1.setRole(r1);
                
                CustomerRole role2 = new CustomerRole();
                role2.setId(2);
                Role r2 = new Role();
                r2.setId(4);
                r2.setName("Basic");
                role2.setRole(r2);
                
                roles.add(role1);
                roles.add(role2);
                
                return roles;
            }
        });
        
        controller.setRoleService(new RoleServiceImpl() {
            public Role findRoleByRoleIdAndEvict(Integer roleId) {
                Role role = new Role();
                Role cloneRole = new Role();
                RoleStatusType type = new RoleStatusType();
                type.setId(1);
                role.setId(3);
                role.setRoleStatusType(type);
                return role;
            }
            
            public void updateRoleAndRolePermission(Role role) {
                
            }
            
            @Override
            public Role findRoleByName(Integer customerId, String roleName) {
                return null;
            }
        });
        
        String res = controller.saveSystemAdminRole(webSecurityForm, result, response);
        assertNotNull(res);
        assertTrue(res.contains(WebCoreConstants.RESPONSE_TRUE));
    }
    
    @Test
    public void test_saveSystemAdminRole_role_fail() {
        
        createFailedAuthentication();
        
        RoleEditForm form = new RoleEditForm();
        form.setRoleId(1);
        form.setRoleName("TestRole");
        form.setStatusEnabled(true);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(1102);
        permissionList.add(1103);
        permissionList.add(1105);
        form.setPermissionIds(permissionList);
        form.setTypeId(WebCoreConstants.CUSTOMER_TYPE_DPT);
        
        WebSecurityForm<RoleEditForm> webSecurityForm = new WebSecurityForm<RoleEditForm>(null, form);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "roleEditForm");
        
        SystemAdminRoleSettingController controller = new SystemAdminRoleSettingController();
        EntityServiceImpl serv = new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        };
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(serv);
        
        controller.setRoleSettingValidator(new RoleSettingValidator());
        WidgetMetricsHelper.registerComponents();
        
        String res = controller.saveSystemAdminRole(webSecurityForm, result, response);
        assertEquals(res, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_saveSystemAdminRole_validator_fail() {
        SystemAdminRoleSettingController controller = new SystemAdminRoleSettingController();
        controller.setRoleSettingValidator(new RoleSettingValidator() {
            public void validate(WebSecurityForm<RoleEditForm> webSecurityForm, Errors errors) {
                webSecurityForm.removeAllErrorStatus();
                webSecurityForm.addErrorStatus(new FormErrorStatus("roleName", "error.common.required"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("enabled", "error.common.invalid"));
            }
        });
        
        controller.setCustomerRoleService(new CustomerRoleServiceImpl() {
            public Collection<CustomerRole> findCustomerRoleByCustomerId(Integer customerId) {
                Collection<CustomerRole> roles = new ArrayList<CustomerRole>();
                CustomerRole role1 = new CustomerRole();
                role1.setId(1);
                Role r1 = new Role();
                r1.setId(3);
                r1.setName("Child Admin");
                role1.setRole(r1);
                
                CustomerRole role2 = new CustomerRole();
                role2.setId(2);
                Role r2 = new Role();
                r2.setId(4);
                r2.setName("Basic");
                role2.setRole(r2);
                
                roles.add(role1);
                roles.add(role2);
                
                return roles;
            }
        });
        
        EntityServiceImpl serv = new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        };
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(serv);
        
        RoleEditForm form = new RoleEditForm();
        WebSecurityForm<RoleEditForm> webSecurityForm = new WebSecurityForm<RoleEditForm>(null, form);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "roleEditForm");
        WidgetMetricsHelper.registerComponents();
        
        String res = controller.saveSystemAdminRole(webSecurityForm, result, response);
        
        assertNotNull(res);
        assertTrue(res
                .contains("\"errorStatus\":[{\"errorFieldName\":\"roleName\",\"message\":\"error.common.required\"},{\"errorFieldName\":\"enabled\",\"message\":\"error.common.invalid\"}]"));
    }
    
    @Test
    public void test_deleteSystemAdminRole() {
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        Collection<Role> roles = new ArrayList<Role>();
        Role role = new Role();
        role.setId(1123);
        role.setName("TestRole");
        roles.add(role);
        
        List<WebObject> randomized = (List<WebObject>) keyMapping.hideProperties(roles, "id");
        request.setParameter("roleID", randomized.get(0).getPrimaryKey().toString());
        
        SystemAdminRoleSettingController controller = new SystemAdminRoleSettingController();
        controller.setRoleService(new RoleServiceImpl() {
            public Role findRoleByRoleIdAndEvict(Integer roleId) {
                Role role = new Role();
                Role cloneRole = new Role();
                RoleStatusType type = new RoleStatusType();
                type.setId(1);
                role.setId(3);
                role.setRoleStatusType(type);
                return role;
            }
            
            public void updateRole(Role role) {
                
            }
            
            public void deleteRole(Role role) {
                
            }
        });
        
        controller.setUserAccountService(createUserAccountService());
        
        String result = controller.deleteSystemAdminRole(request, response);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_TRUE);
    }
    
    @Test
    public void test_deleteSystemAdminRole_role_fail() {
        createFailedAuthentication();
        
        SystemAdminRoleSettingController controller = new SystemAdminRoleSettingController();
        String result = controller.deleteSystemAdminRole(request, response);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_deleteSystemAdminRole_randomized_roleID_validation_fail() {
        request.setParameter("roleID", "111111111111111!");
        
        SystemAdminRoleSettingController controller = new SystemAdminRoleSettingController();
        String result = controller.deleteSystemAdminRole(request, response);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_deleteSystemAdminRole_actual_roleID_validation_fail() {
        request.setParameter("roleID", "2DFTYU7FREDGFWS4");
        
        SystemAdminRoleSettingController controller = new SystemAdminRoleSettingController();
        String result = controller.deleteSystemAdminRole(request, response);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        user.setIsSystemAdmin(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_USERS_AND_ROLES);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_USERS_AND_ROLES);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private void createFailedAuthentication() {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        user.setIsSystemAdmin(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(2);
        permissionList.add(3);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private Collection<RoleSettingInfo> getRoleSettingInfo() {
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        Collection<Role> roles = new ArrayList<Role>();
        Role r1 = new Role();
        r1.setId(3);
        r1.setName("Child Admin");
        Role r2 = new Role();
        r2.setId(4);
        r2.setName("Basic");
        roles.add(r1);
        roles.add(r2);
        
        Collection<WebObject> objs = keyMapping.hideProperties(roles, WebCoreConstants.ID_LOOK_UP_NAME);
        Collection<RoleSettingInfo> lists = new ArrayList<RoleSettingInfo>();
        
        for (WebObject obj : objs) {
            lists.add(new RoleSettingInfo(obj.getPrimaryKey().toString(), ((Role) obj.getWrappedObject()).getName()));
        }
        
        return lists;
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
        };
    }
    
    private CustomerService createCustomerService() {
        return new CustomerServiceTestImpl() {
            public List<Customer> findAllChildCustomers(final int parentCustomerId) {
                List<Customer> fc = new ArrayList<Customer>();
                Customer c = new Customer();
                c.setId(6);
                fc.add(c);
                return fc;
            }
        };
    }
    
    private UserAccountService createUserAccountService() {
        return new UserAccountServiceImpl() {
            @Override
            public Collection<UserAccount> findUserAccountByCustomerIdAndRoleId(Integer customerId, Integer roleId) {
                Collection<UserAccount> results = new ArrayList<UserAccount>();
                UserAccount u1 = new UserAccount();
                u1.setId(1);
                u1.setFirstName("John");
                u1.setLastName("Doe1");
                results.add(u1);
                UserAccount u2 = new UserAccount();
                u2.setId(1);
                u2.setFirstName("John2");
                u2.setLastName("Doe2");
                results.add(u2);
                
                return results;
            }
            
            @Override
            public List<UserAccount> findUserAccountByRoleIdAndCustomerId(Integer roleId, Integer customerId) {
                List<UserAccount> results = new ArrayList<UserAccount>();
                UserAccount u1 = new UserAccount();
                u1.setId(1);
                u1.setFirstName("John");
                u1.setLastName("Doe1");
                results.add(u1);
                UserAccount u2 = new UserAccount();
                u2.setId(1);
                u2.setFirstName("John2");
                u2.setLastName("Doe2");
                results.add(u2);
                
                return results;
            }
            
            @Override
            public void deleteUserRoles(UserAccount userAccount) {
            }
            
        };
    }
}
