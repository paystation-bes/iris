package com.digitalpaytech.mvc.systemadmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.EventDefinition;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosSensorInfo;
import com.digitalpaytech.domain.PosSensorState;
import com.digitalpaytech.domain.PosStatus;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.dto.AlertSearchCriteria;
import com.digitalpaytech.dto.customeradmin.AlertInfoEntry;
import com.digitalpaytech.mvc.support.PayStationRecentActivityFilterForm;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.mvc.support.impl.CommonControllerHelperImpl;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.PosSensorInfoServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.AlertsServiceTestImpl;
import com.digitalpaytech.testing.services.impl.CustomerAdminServiceTestImpl;
import com.digitalpaytech.testing.services.impl.PointOfSaleServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.PayStationRecentActivityFilterValidator;

public class SystemAdminPayStationDetailControllerTest {
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private ModelMap model;
    private UserAccount userAccount;
    private BindingResult result;
    private SystemAdminPayStationDetailController controller;
    
    @Before
    public void setUp() {
        
        this.controller = new SystemAdminPayStationDetailController() {
        };
        
        WidgetMetricsHelper.registerComponents();
        
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        session = new MockHttpSession();
        this.request.setSession(session);
        this.model = new ModelMap();
        
        userAccount = new UserAccount();
        userAccount.setId(2);
        Set<UserRole> roles = new HashSet<UserRole>();
        UserRole role = new UserRole();
        Role r = new Role();
        Set<RolePermission> rps = new HashSet<RolePermission>();
        
        RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_VIEW_ROUTES);
        rp1.setPermission(permission1);
        
        RolePermission rp2 = new RolePermission();
        rp2.setId(2);
        Permission permission2 = new Permission();
        permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_ROUTES);
        rp2.setPermission(permission2);
        
        RolePermission rp3 = new RolePermission();
        rp3.setId(3);
        Permission permission3 = new Permission();
        permission3.setId(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS);
        rp3.setPermission(permission3);
        
        RolePermission rp4 = new RolePermission();
        rp4.setId(4);
        Permission permission4 = new Permission();
        permission4.setId(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS);
        rp4.setPermission(permission4);
        
        RolePermission rp5 = new RolePermission();
        rp5.setId(5);
        Permission permission5 = new Permission();
        permission5.setId(WebSecurityConstants.PERMISSION_DPT_VIEW_PAYSTATION);
        rp5.setPermission(permission5);
        
        RolePermission rp6 = new RolePermission();
        rp6.setId(6);
        Permission permission6 = new Permission();
        permission6.setId(WebSecurityConstants.PERMISSION_DPT_MANAGE_PAYSTATION);
        rp6.setPermission(permission6);
        
        rps.add(rp1);
        rps.add(rp2);
        rps.add(rp3);
        rps.add(rp4);
        rps.add(rp5);
        rps.add(rp6);
        
        r.setId(3);
        r.setRolePermissions(rps);
        
        role.setId(3);
        role.setRole(r);
        roles.add(role);
        userAccount.setUserRoles(roles);
        
        Customer customer = new Customer();
        customer.setId(2);
        userAccount.setCustomer(customer);
        createAuthentication(userAccount);
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        controller.setCustomerAdminService(new CustomerAdminServiceTestImpl() {
            public void saveOrUpdatePointOfSale(PointOfSale pointOfSale) {
            }
            
            public void saveOrUpdatePaystation(Paystation payStation) {
            }
            
            public void saveOrUpdatePointOfSaleStatus(PosStatus pointOfSaleStatus) {
            };
            
            public Customer findCustomerByCustomerId(int customerId) {
                Customer customer = new Customer();
                customer.setId(1);
                return customer;
            }
            
            @Override
            public CustomerProperty getCustomerPropertyByCustomerIdAndCustomerPropertyType(int customerId, int customerPropertyType) {
                CustomerProperty cp = new CustomerProperty();
                cp.setPropertyValue("GMT");
                return cp;
            }
            
        });
        
        controller.setCommonControllerHelper(new CommonControllerHelperImpl());
        
    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setCustomerTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT);
        user.setIsComplexPassword(true);
        user.setIsSystemAdmin(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_ROUTES);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_ROUTES);
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS);
        permissionList.add(WebSecurityConstants.PERMISSION_DPT_VIEW_PAYSTATION);
        permissionList.add(WebSecurityConstants.PERMISSION_DPT_MANAGE_PAYSTATION);
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    @Test
    public void test_payStationDetailsCurrentStatusBatteryVoltage_value() {
        
        PointOfSale pointOfSale = new PointOfSale();
        pointOfSale.setId(1);
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String randomKey = keyMapping.getRandomString(pointOfSale, "id");
        request.setParameter("payStationID", randomKey);
        request.setParameter("isGraph", "false");
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            public PosSensorState findPosSensorStateByPointOfSaleId(int pointOfSaleId) {
                PosSensorState sensorState = new PosSensorState();
                sensorState.setBattery1Voltage(Float.valueOf("5"));
                return sensorState;
            }
            
            public PointOfSale findPointOfSaleById(Integer pointOfSaleId) {
                PointOfSale pointOfSale = new PointOfSale();
                pointOfSale.setId(1);
                Customer customer = new Customer();
                customer.setId(1);
                pointOfSale.setCustomer(customer);
                
                return pointOfSale;
            }
        });
        String reply = controller.payStationDetailsCurrentStatusBatteryVoltage(request, response, model);
        assertEquals("{\"float\":5}", reply);
    }
    
    @Test
    public void test_payStationDetailsCurrentStatusInputCurrent() {
        
        PointOfSale pointOfSale = new PointOfSale();
        pointOfSale.setId(1);
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String randomKey = keyMapping.getRandomString(pointOfSale, "id");
        request.setParameter("payStationID", randomKey);
        request.setParameter("isGraph", "false");
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            public PosSensorState findPosSensorStateByPointOfSaleId(int pointOfSaleId) {
                PosSensorState sensorState = new PosSensorState();
                sensorState.setInputCurrent(Float.valueOf("5"));
                return sensorState;
            }
            
            public PointOfSale findPointOfSaleById(Integer pointOfSaleId) {
                PointOfSale pointOfSale = new PointOfSale();
                pointOfSale.setId(1);
                Customer customer = new Customer();
                customer.setId(1);
                pointOfSale.setCustomer(customer);
                
                return pointOfSale;
            }
        });
        String reply = controller.payStationDetailsCurrentStatusInputCurrent(request, response, model);
        assertEquals("{\"float\":5}", reply);
    }
    
    @Test
    public void test_payStationDetailsCurrentStatusAmbientTemperature() {
        
        PointOfSale pointOfSale = new PointOfSale();
        pointOfSale.setId(1);
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String randomKey = keyMapping.getRandomString(pointOfSale, "id");
        request.setParameter("payStationID", randomKey);
        request.setParameter("isGraph", "false");
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            public PosSensorState findPosSensorStateByPointOfSaleId(int pointOfSaleId) {
                PosSensorState sensorState = new PosSensorState();
                sensorState.setAmbientTemperature(Float.valueOf("5"));
                return sensorState;
            }
            
            public PointOfSale findPointOfSaleById(Integer pointOfSaleId) {
                PointOfSale pointOfSale = new PointOfSale();
                pointOfSale.setId(1);
                Customer customer = new Customer();
                customer.setId(1);
                pointOfSale.setCustomer(customer);
                
                return pointOfSale;
            }
        });
        String reply = controller.payStationDetailsCurrentStatusAmbientTemperature(request, response, model);
        assertEquals("{\"float\":5}", reply);
    }
    
    @Test
    public void test_payStationDetailsCurrentStatusRelativeHumidity() {
        
        PointOfSale pointOfSale = new PointOfSale();
        pointOfSale.setId(1);
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String randomKey = keyMapping.getRandomString(pointOfSale, "id");
        request.setParameter("payStationID", randomKey);
        request.setParameter("isGraph", "false");
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            public PosSensorState findPosSensorStateByPointOfSaleId(int pointOfSaleId) {
                PosSensorState sensorState = new PosSensorState();
                sensorState.setRelativeHumidity(Float.valueOf("5"));
                return sensorState;
            }
            
            public PointOfSale findPointOfSaleById(Integer pointOfSaleId) {
                PointOfSale pointOfSale = new PointOfSale();
                pointOfSale.setId(1);
                Customer customer = new Customer();
                customer.setId(1);
                pointOfSale.setCustomer(customer);
                
                return pointOfSale;
            }
        });
        String reply = controller.payStationDetailsCurrentStatusRelativeHumidity(request, response, model);
        assertEquals("{\"float\":5}", reply);
    }
    
    @Test
    public void test_payStationDetailsCurrentStatusControllerTemperature() {
        
        PointOfSale pointOfSale = new PointOfSale();
        pointOfSale.setId(1);
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String randomKey = keyMapping.getRandomString(pointOfSale, "id");
        request.setParameter("payStationID", randomKey);
        request.setParameter("isGraph", "false");
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            public PosSensorState findPosSensorStateByPointOfSaleId(int pointOfSaleId) {
                PosSensorState sensorState = new PosSensorState();
                sensorState.setControllerTemperature(Float.valueOf("5"));
                return sensorState;
            }
            
            public PointOfSale findPointOfSaleById(Integer pointOfSaleId) {
                PointOfSale pointOfSale = new PointOfSale();
                pointOfSale.setId(1);
                Customer customer = new Customer();
                customer.setId(1);
                pointOfSale.setCustomer(customer);
                
                return pointOfSale;
            }
        });
        String reply = controller.payStationDetailsCurrentStatusControllerTemperature(request, response, model);
        assertEquals("{\"float\":5}", reply);
        
    }
    
    @Ignore
    //removed from scope of EMS7
    @Test
    public void test_payStationDetailsCurrentStatus_graph() {
        
        PointOfSale pointOfSale = new PointOfSale();
        pointOfSale.setId(1);
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String randomKey = keyMapping.getRandomString(pointOfSale, "id");
        request.setParameter("payStationID", randomKey);
        request.setParameter("isGraph", "true");
        
        final Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            public PointOfSale findPointOfSaleById(Integer pointOfSaleId) {
                PointOfSale pointOfSale = new PointOfSale();
                pointOfSale.setId(1);
                Customer customer = new Customer();
                customer.setId(1);
                pointOfSale.setCustomer(customer);
                
                return pointOfSale;
            }
        });
        controller.setPosSensorInfoService(new PosSensorInfoServiceImpl() {
            public List<PosSensorInfo> findPosSensorInfoByPointOfSaleIdTypeId(int PointOfSaleId, int TypeId) {
                List<PosSensorInfo> sensorLogs = new ArrayList<PosSensorInfo>();
                PosSensorInfo logEntry = new PosSensorInfo();
                logEntry.setSensorGmt(date);
                logEntry.setAmount(Float.valueOf("5"));
                sensorLogs.add(logEntry);
                return sensorLogs;
            }
        });
        
        String reply = controller.payStationDetailsCurrentStatusBatteryVoltage(request, response, model);
        assertEquals(reply, "{\"sensorLogGraphData\":{\"sensorGraph\":{\"sensorDataPoint\":[" + cal.getTimeInMillis() + ",5]}}}");
        
        reply = controller.payStationDetailsCurrentStatusAmbientTemperature(request, response, model);
        assertEquals(reply, "{\"sensorLogGraphData\":{\"sensorGraph\":{\"sensorDataPoint\":[" + cal.getTimeInMillis() + ",5]}}}");
        
        reply = controller.payStationDetailsCurrentStatusControllerTemperature(request, response, model);
        assertEquals(reply, "{\"sensorLogGraphData\":{\"sensorGraph\":{\"sensorDataPoint\":[" + cal.getTimeInMillis() + ",5]}}}");
        
        reply = controller.payStationDetailsCurrentStatusInputCurrent(request, response, model);
        assertEquals(reply, "{\"sensorLogGraphData\":{\"sensorGraph\":{\"sensorDataPoint\":[" + cal.getTimeInMillis() + ",5]}}}");
        
        reply = controller.payStationDetailsCurrentStatusRelativeHumidity(request, response, model);
        assertEquals(reply, "{\"sensorLogGraphData\":{\"sensorGraph\":{\"sensorDataPoint\":[" + cal.getTimeInMillis() + ",5]}}}");
    }
    
    @Test
    public void test_payStationDetailsAlerts() {
        PointOfSale pointOfSale = new PointOfSale();
        pointOfSale.setId(1);
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String randomKey = keyMapping.getRandomString(pointOfSale, "id");
        request.setParameter("payStationID", randomKey);
        
        PayStationRecentActivityFilterForm form = new PayStationRecentActivityFilterForm();
        form.setRandomId(randomKey);
        form.setStatus("0");
        
        WebSecurityForm<PayStationRecentActivityFilterForm> webSecurityForm = new WebSecurityForm<PayStationRecentActivityFilterForm>(null, form);
        result = new BeanPropertyBindingResult(form, "recentActivityFilterForm");
        
        controller.setAlertsService(new AlertsServiceTestImpl() {
            public EventDefinition findEventDefinitionByDeviceStatusActionSeverityId(int deficeTypeId, int statusTypeId, int actionTypeId,
                int severityTypeId) {
                EventDefinition eventDefinition = new EventDefinition();
                eventDefinition.setDescription("description");
                return eventDefinition;
            }
            
            @Override
            public List<AlertInfoEntry> findHistory(AlertSearchCriteria criteria, TimeZone timezone, String sortItem, String sortOrder) {
                List<AlertInfoEntry> pointOfSaleEvents = new ArrayList<AlertInfoEntry>();
                
                pointOfSaleEvents.add(new AlertInfoEntry(1, 1, new Date(), new Date(), "description", 1, "test module", timezone, true, false,
                        "Minor"));
                return pointOfSaleEvents;
            }
            
        });
        
        controller.setPayStationRecentActivityFilterValidator(new PayStationRecentActivityFilterValidator() {
            public void validate(WebSecurityForm<PayStationRecentActivityFilterForm> command, Errors errors) {
                
            }
        });
        
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            public PosSensorState findPosSensorStateByPointOfSaleId(int pointOfSaleId) {
                PosSensorState sensorState = new PosSensorState();
                sensorState.setBattery1Voltage(Float.valueOf("5"));
                return sensorState;
            }
            
            public PointOfSale findPointOfSaleById(Integer pointOfSaleId) {
                PointOfSale pointOfSale = new PointOfSale();
                pointOfSale.setId(1);
                Customer customer = new Customer();
                customer.setId(2);
                pointOfSale.setCustomer(customer);
                return pointOfSale;
            }
        });
        
        String reply = controller.payStationDetailsAlerts(webSecurityForm, result, request, response);
        assertTrue(reply.startsWith("{\"alertInfo\":{\"alerts\":{\"alertDate\":\"just now\",\""));
        reply = reply.substring(0, reply.lastIndexOf(':') + 1);
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
        };
    }
}
