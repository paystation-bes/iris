package com.digitalpaytech.mvc.systemadmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.MerchantPOS;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PaystationType;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosAlertStatus;
import com.digitalpaytech.domain.PosDate;
import com.digitalpaytech.domain.PosDateType;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.domain.PosStatus;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.dto.PointOfSaleSearchCriteria;
import com.digitalpaytech.dto.PosAlertStatusDetailSummary;
import com.digitalpaytech.dto.customeradmin.LocationRouteFilterInfo;
import com.digitalpaytech.dto.customeradmin.PaystationListInfo;
import com.digitalpaytech.dto.customeradmin.PlacementMapBordersInfo;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.mvc.support.impl.CommonControllerHelperImpl;
import com.digitalpaytech.mvc.systemadmin.support.SysAdminPayStationEditForm;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.LocationServiceImpl;
import com.digitalpaytech.service.impl.MerchantAccountServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.CustomerAdminServiceTestImpl;
import com.digitalpaytech.testing.services.impl.CustomerMigrationServiceTestImpl;
import com.digitalpaytech.testing.services.impl.CustomerServiceTestImpl;
import com.digitalpaytech.testing.services.impl.PointOfSaleServiceTestImpl;
import com.digitalpaytech.testing.services.impl.PosAlertStatusServiceTestImpl;
import com.digitalpaytech.testing.services.impl.RouteServiceTestImpl;
import com.digitalpaytech.testing.services.systemadmin.impl.SystemAdminServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.systemadmin.SysAdminPayStationValidator;

public class SystemAdminPayStationListControllerTest {
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private ModelMap model;
    private UserAccount userAccount;
    private SystemAdminPayStationListController controller;
    
    @Before
    public void setUp() {
        
        this.controller = new SystemAdminPayStationListController();
        
        WidgetMetricsHelper.registerComponents();
        
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        session = new MockHttpSession();
        this.request.setSession(session);
        this.model = new ModelMap();
        
        userAccount = new UserAccount();
        userAccount.setId(2);
        Set<UserRole> roles = new HashSet<UserRole>();
        UserRole role = new UserRole();
        Role r = new Role();
        Set<RolePermission> rps = new HashSet<RolePermission>();
        
        RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_VIEW_ROUTES);
        rp1.setPermission(permission1);
        
        RolePermission rp2 = new RolePermission();
        rp2.setId(2);
        Permission permission2 = new Permission();
        permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_ROUTES);
        rp2.setPermission(permission2);
        
        RolePermission rp3 = new RolePermission();
        rp3.setId(3);
        Permission permission3 = new Permission();
        permission3.setId(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS);
        rp3.setPermission(permission3);
        
        RolePermission rp4 = new RolePermission();
        rp4.setId(4);
        Permission permission4 = new Permission();
        permission4.setId(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS);
        rp4.setPermission(permission4);
        
        RolePermission rp5 = new RolePermission();
        rp5.setId(5);
        Permission permission5 = new Permission();
        permission5.setId(WebSecurityConstants.PERMISSION_DPT_VIEW_PAYSTATION);
        rp5.setPermission(permission5);
        
        RolePermission rp6 = new RolePermission();
        rp6.setId(6);
        Permission permission6 = new Permission();
        permission6.setId(WebSecurityConstants.PERMISSION_DPT_MANAGE_PAYSTATION);
        rp6.setPermission(permission6);
        
        rps.add(rp1);
        rps.add(rp2);
        rps.add(rp3);
        rps.add(rp4);
        rps.add(rp5);
        rps.add(rp6);
        
        r.setId(3);
        r.setRolePermissions(rps);
        
        role.setId(3);
        role.setRole(r);
        roles.add(role);
        userAccount.setUserRoles(roles);
        
        Customer customer = new Customer();
        customer.setId(2);
        userAccount.setCustomer(customer);
        createAuthentication(userAccount);
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        controller.setCustomerAdminService(new CustomerAdminServiceTestImpl() {
            public void saveOrUpdatePointOfSale(PointOfSale pointOfSale) {
            }
            
            public void saveOrUpdatePaystation(Paystation payStation) {
            }
            
            public void saveOrUpdatePointOfSaleStatus(PosStatus pointOfSaleStatus) {
            }
            
            public Customer findCustomerByCustomerId(int customerId) {
                Customer customer = new Customer();
                customer.setId(1);
                return customer;
            }
        });
        controller.setCommonControllerHelper(new CommonControllerHelperImpl() {
            @Override
            public final Customer insertCustomerInModelForSystemAdmin(final HttpServletRequest request, final ModelMap model) {
                return null;
            }
        });
        controller.setCustomerMigrationService(new CustomerMigrationServiceTestImpl() {
        });
        
    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setCustomerTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT);
        user.setIsComplexPassword(true);
        user.setIsSystemAdmin(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_ROUTES);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_ROUTES);
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_LOCATIONS);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_LOCATIONS);
        permissionList.add(WebSecurityConstants.PERMISSION_DPT_VIEW_PAYSTATION);
        permissionList.add(WebSecurityConstants.PERMISSION_DPT_MANAGE_PAYSTATION);
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    //========================================================================================================================
    // "Pay Station List" tab
    //========================================================================================================================
    
    @Test
    public void test_payStationList() {
        
        controller.setLocationService(new LocationServiceImpl() {
            public List<Location> findLocationsByCustomerId(Integer customerId) {
                return null;
            }
        });
        controller.setRouteService(new RouteServiceTestImpl() {
            public Collection<Route> findRoutesByCustomerId(int customerId) {
                return null;
            }
        });
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            @Override
            public List<PointOfSale> findPointOfSalesByCustomerId(int customerId) {
                return new ArrayList<PointOfSale>();
            }
            
            @Override
            public List<PointOfSale> findPointOfSaleAndPosStatusByCustomerId(int customerId) {
                return new ArrayList<PointOfSale>();
            }
            
            @Override
            public List<PointOfSale> findPointOfSaleAndPosStatusByCustomerIdNoCache(int customerId) {
                return new ArrayList<PointOfSale>();
            }
            
            @Override
            public PlacementMapBordersInfo findMapBordersByCustomerId(Integer customerId) {
                return null;
            }
            
            @Override
            public List<PaystationListInfo> findPaystation(PointOfSaleSearchCriteria criteria) {
                return new ArrayList<PaystationListInfo>();
            }
            
        });
        
        controller.setCustomerService(new CustomerServiceTestImpl() {
            public Customer findCustomer(Integer customerId) {
                Customer cust = new Customer();
                cust.setId(customerId);
                cust.setName("TEST");
                return cust;
            }
        });
        controller.setMerchantAccountService(new MerchantAccountServiceImpl() {
            @Override
            public List<MerchantAccount> findMerchantAccounts(int customerId, boolean forValueCard) {
                return new ArrayList<MerchantAccount>();
            }
        });
        
        controller.setCommonControllerHelper(new CommonControllerHelperImpl() {
            public LocationRouteFilterInfo createLocationRouteFilter(Integer customerId, RandomKeyMapping keyMapping) {
                return null;
            }
        });
        
        controller.setCustomerAdminService(new CustomerAdminServiceTestImpl() {
            public CustomerProperty getCustomerPropertyByCustomerIdAndCustomerPropertyType(int customerId, int customerPropertyType) {
                CustomerProperty cp = new CustomerProperty();
                cp.setPropertyValue("US/Pacific");
                return cp;
            }
        });
        
        Customer customer = new Customer();
        customer.setId(1);
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String randomKey = keyMapping.getRandomString(customer, "id");
        request.setParameter("customerID", randomKey);
        
        String returnValue = controller.payStationList(request, response, model);
        assertEquals("/systemAdmin/workspace/payStations/payStationList", returnValue);
    }
    
    @Test
    public void test_payStationDetails() {
        
        PointOfSale pointOfSale = new PointOfSale();
        pointOfSale.setId(1);
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String randomKey = keyMapping.getRandomString(pointOfSale, "id");
        request.setParameter("payStationID", randomKey);
        
        controller.setPosAlertStatusService(new PosAlertStatusServiceTestImpl() {
            
            @Override
            public Map<Integer, PosAlertStatusDetailSummary> findPosAlertDetailSummaryByPointOfSaleId(int pointOfSaleId) {
                
                Map<Integer, PosAlertStatusDetailSummary> pasdsMap = new HashMap<Integer, PosAlertStatusDetailSummary>();
                PosAlertStatusDetailSummary pads = new PosAlertStatusDetailSummary();
                pads.setTotalCritical(1);
                pads.setTotalMajor(1);
                pads.setTotalMinor(1);
                pasdsMap.put(1, pads);
                return pasdsMap;
            }
            
        });
        
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            @Override
            public PointOfSale findPointOfSaleById(Integer pointOfSaleId) {
                PointOfSale pointOfSale = new PointOfSale();
                pointOfSale.setId(1);
                Customer customer = new Customer();
                customer.setId(1);
                pointOfSale.setCustomer(customer);
                
                Location location = new Location();
                Location parentLocation = new Location();
                parentLocation.setName("testParent");
                location.setLocation(parentLocation);
                location.setName("testName");
                location.setId(1);
                pointOfSale.setLocation(location);
                pointOfSale.setPosAlertStatus(new PosAlertStatus());
                return pointOfSale;
            }
            
            @Override
            public Paystation findPayStationByPointOfSaleId(int pointOfSaleId) {
                Paystation payStation = new Paystation();
                PaystationType payStationType = new PaystationType();
                payStationType.setId(1);
                payStation.setPaystationType(payStationType);
                return payStation;
            }
            
            @Override
            public PosStatus findPointOfSaleStatusByPOSId(int pointOfSaleId, boolean useCache) {
                PosStatus status = new PosStatus();
                status.setIsBillableMonthlyOnActivation(true);
                status.setIsDigitalConnect(true);
                status.setIsActivated(true);
                status.setIsVisible(true);
                return status;
            }
            
            public PosHeartbeat findPosHeartbeatByPointOfSaleId(int pointOfSaleId) {
                PosHeartbeat heartbeat = new PosHeartbeat();
                heartbeat.setLastHeartbeatGmt(new Date());
                return heartbeat;
            }
            
            public List<MerchantPOS> findMerchantPOSbyPointOfSaleId(int pointOfSaleId) {
                List<MerchantPOS> list = new ArrayList<MerchantPOS>();
                return list;
            }
            
            public List<MerchantPOS> findMerchPOSByPOSIdNoDeleted(int pointOfSaleId) {
                List<MerchantPOS> list = new ArrayList<MerchantPOS>();
                return list;
            }
        });
        controller.setLocationService(new LocationServiceImpl() {
            public Location findLocationByPointOfSaleId(int pointOfSaleId) {
                Location location = new Location();
                Location parentLocation = new Location();
                parentLocation.setName("testParent");
                location.setLocation(parentLocation);
                location.setName("testName");
                location.setId(1);
                return location;
            }
        });
        
        String returnValue = controller.payStationDetailPage(request, response, model);
        assertNotNull(returnValue);
    }
    
    @Test
    public void test_getPayStationEditForm() {
        
        PointOfSale pointOfSale = new PointOfSale();
        pointOfSale.setId(1);
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        controller.setSystemAdminService(new SystemAdminServiceTestImpl() {
            public List<Customer> findAllNonDPTCustomers() {
                List<Customer> customers = new ArrayList<Customer>();
                Customer customer = new Customer();
                customer.setId(1);
                customers.add(customer);
                return customers;
            }
            
            public List<MerchantAccount> findValidMerchantAccountsByCustomerId(Integer id) {
                MerchantAccount merchantAccount = new MerchantAccount();
                merchantAccount.setName("test name");
                merchantAccount.setId(1);
                List<MerchantAccount> merchantAccounts = new ArrayList<MerchantAccount>();
                merchantAccounts.add(merchantAccount);
                return merchantAccounts;
            }
        });
        controller.setLocationService(new LocationServiceImpl() {
            public Location findLocationByPointOfSaleId(int pointOfSaleId) {
                Location location = new Location();
                location.setName("testName");
                location.setId(1);
                return location;
            }
            
            public List<Location> findLocationsByCustomerId(Integer customerId) {
                Location location = new Location();
                location.setName("testName");
                location.setId(1);
                List<Location> locations = new ArrayList<Location>();
                return locations;
            }
        });
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            public PointOfSale findPointOfSaleById(Integer pointOfSaleId) {
                PointOfSale pointOfSale = new PointOfSale();
                Location location = new Location();
                location.setId(1);
                location.setName("testName");
                pointOfSale.setId(1);
                pointOfSale.setLocation(location);
                Customer customer = new Customer();
                customer.setId(1);
                pointOfSale.setCustomer(customer);
                
                return pointOfSale;
            }
            
            public PosStatus findPointOfSaleStatusByPOSId(int pointOfSaleId) {
                PosStatus status = new PosStatus();
                status.setIsActivated(true);
                status.setIsVisible(true);
                return status;
            }
            
            public List<MerchantPOS> findMerchPOSByPOSIdNoDeleted(int pointOfSaleId) {
                return new ArrayList<MerchantPOS>();
            }
            
            public List<PointOfSale> findAllPointOfSales(List<Integer> customerIds, List<Integer> locationIds, List<Integer> routeIds,
                List<Integer> pointOfSaleIds) {
                
                PointOfSale pointOfSale = new PointOfSale();
                Location location = new Location();
                location.setId(1);
                location.setName("testName");
                pointOfSale.setId(1);
                pointOfSale.setLocation(location);
                PosStatus posStatus = new PosStatus();
                posStatus.setIsDecommissioned(false);
                posStatus.setPointOfSale(pointOfSale);
                pointOfSale.setPosStatus(posStatus);
                Customer customer = new Customer();
                customer.setId(1);
                pointOfSale.setCustomer(customer);
                List<PointOfSale> posList = new ArrayList<PointOfSale>();
                posList.add(pointOfSale);
                return posList;
            }
        });
        controller.setMerchantAccountService(new MerchantAccountServiceImpl() {
            public List<MerchantAccount> findAllMerchantAccountsByCustomerId(Integer id) {
                MerchantAccount merchantAccount = new MerchantAccount();
                merchantAccount.setName("test name");
                merchantAccount.setId(1);
                List<MerchantAccount> merchantAccounts = new ArrayList<MerchantAccount>();
                merchantAccounts.add(merchantAccount);
                return merchantAccounts;
            }
        });
        
        controller.setCustomerService(new CustomerServiceTestImpl() {
            @Override
            public Customer findCustomer(Integer id) {
                Customer customer = new Customer();
                customer.setId(1);
                customer.setIsMigrated(true);
                return customer;
            }
        });
        
        String randomKey = keyMapping.getRandomString(pointOfSale, "id");
        String[] key = { randomKey };
        request.setParameter("payStationID", key);
        
        String returnValue = controller.getPayStationEditForm(request, response, model);
        assertNotNull(returnValue);
        assertTrue(returnValue.contains("payStationForm"));
    }
    
    @Ignore
    @Test
    public void test_savePayStation() {
        
        SysAdminPayStationEditForm form = createValidPayStationTestForm();
        
        BindingResult result = new BeanPropertyBindingResult(form, "payStationEditForm");
        WebSecurityForm<SysAdminPayStationEditForm> webSecurityForm = new WebSecurityForm<SysAdminPayStationEditForm>(null, form);
        webSecurityForm.setPostToken(webSecurityForm.getInitToken());
        model.put("payStationEditForm", webSecurityForm);
        
        SysAdminPayStationValidator payStationValidator = new SysAdminPayStationValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "error.common.invalid.posttoken";
            }
        };
        payStationValidator.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            public Paystation findPayStationById(int payStationId) {
                Paystation payStation = new Paystation();
                payStation.setId(1);
                return payStation;
            }
            
            public PosStatus findPointOfSaleStatusByPOSId(int pointOfSaleId) {
                PosStatus status = new PosStatus();
                status.setIsActivated(true);
                status.setIsVisible(true);
                return status;
            }
            
            public PointOfSale findPointOfSaleById(Integer id) {
                PointOfSale pointOfSale = new PointOfSale();
                pointOfSale.setId(1);
                pointOfSale.setSerialNumber("123456789012");
                Customer customer = new Customer();
                customer.setId(1);
                pointOfSale.setCustomer(customer);
                
                return pointOfSale;
            }
        });
        controller.setSysAdminPayStationValidator(payStationValidator);
        controller.setLocationService(new LocationServiceImpl() {
            public Location findLocationById(int locationId) {
                return new Location();
            }
        });
        controller.setPointOfSaleService(new PointOfSaleServiceTestImpl() {
            public Paystation findPayStationByPointOfSaleId(int pointOfSaleId) {
                Paystation payStation = new Paystation();
                payStation.setId(1);
                payStation.setSerialNumber("123456789012");
                return payStation;
            }
            
            public Paystation findPayStationById(int id) {
                Paystation payStation = new Paystation();
                payStation.setId(1);
                payStation.setSerialNumber("123456789012");
                return payStation;
            }
            
            public PointOfSale findPointOfSaleByPaystationId(int id) {
                PointOfSale pointOfSale = new PointOfSale();
                pointOfSale.setId(1);
                return pointOfSale;
            }
            
            public PosStatus findPointOfSaleStatusByPOSId(int pointOfSaleId) {
                PosStatus status = new PosStatus();
                status.setIsBillableMonthlyOnActivation(true);
                status.setIsDigitalConnect(true);
                status.setIsActivated(true);
                status.setIsVisible(true);
                status.setIsBillableMonthlyOnActivation(true);
                return status;
            }
            
            public PointOfSale findPointOfSaleById(Integer id) {
                PointOfSale pointOfSale = new PointOfSale();
                pointOfSale.setId(1);
                Customer customer = new Customer();
                customer.setId(1);
                pointOfSale.setCustomer(customer);
                return pointOfSale;
            }
            
            public List<MerchantPOS> findMerchantPOSbyPointOfSaleId(int pointOfSaleId) {
                List<MerchantPOS> merchantPOSs = new ArrayList<MerchantPOS>();
                MerchantPOS merchantPOS = new MerchantPOS();
                CardType cardType = new CardType();
                cardType.setId(WebCoreConstants.CREDIT_CARD);
                merchantPOS.setCardType(cardType);
                merchantPOS.setId(1);
                merchantPOSs.add(merchantPOS);
                return merchantPOSs;
            }
            
            public PosDateType findPosDateTypeById(int posDateTypeId) {
                PosDateType posDateType = new PosDateType();
                posDateType.setId(posDateTypeId);
                return posDateType;
            }
            
            public void saveOrUpdatePOSDate(PosDate posDate) {
            }
            
            public PaystationType findPayStationTypeById(int payStationTypeId) {
                return new PaystationType();
            }
        });
        controller.setSystemAdminService(new SystemAdminServiceTestImpl() {
            public MerchantAccount findMerchantAccountById(Integer merchantAccountId) {
                MerchantAccount merchantAccount = new MerchantAccount();
                merchantAccount.setId(1);
                return merchantAccount;
            }
            
            public void saveOrUpdateMerchantPos(MerchantPOS merchantPos) {
            }
            
            public CardType findCardTypeById(int cardTypeId) {
                CardType cardType = new CardType();
                cardType.setId(WebCoreConstants.CREDIT_CARD);
                return cardType;
            }
        });
        
        @SuppressWarnings("unchecked")
        String returnValue = controller.savePayStation((WebSecurityForm<SysAdminPayStationEditForm>) model.get("payStationEditForm"), result,
                                                       request, response, model);
        
        assertTrue(returnValue.startsWith(WebCoreConstants.RESPONSE_TRUE));
    }
    
    private SysAdminPayStationEditForm createValidPayStationTestForm() {
        
        SysAdminPayStationEditForm form = new SysAdminPayStationEditForm();
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        Location location = new Location();
        location.setId(1);
        String locationRandomKey = keyMapping.getRandomString(location, "id");
        form.addLocation("location", locationRandomKey, true);
        form.setLocationRandomId(locationRandomKey);
        
        PointOfSale pos = new PointOfSale();
        pos.setId(1);
        pos.setSerialNumber("123456789012");
        String payStationRandomKey = keyMapping.getRandomString(pos, "id");
        form.addPayStation("pay station", payStationRandomKey, false, false);
        form.setName("pay station");
        form.setRandomId(payStationRandomKey);
        
        Customer customer = new Customer();
        customer.setId(1);
        String customerRandomKey = keyMapping.getRandomString(customer, "id");
        form.addCustomer("customer", customerRandomKey, true);
        form.setCurrentCustomerRandomId(customerRandomKey);
        
        MerchantAccount merchantAccount = new MerchantAccount();
        merchantAccount.setId(1);
        String merchantAccountRandomKey = keyMapping.getRandomString(merchantAccount, "id");
        form.addCreditCardMerchantAccount("merchantAccount", merchantAccountRandomKey, true);
        form.addCustomCardMerchantAccount("merchantAccount", merchantAccountRandomKey, true);
        form.setCreditCardMerchantRandomId(merchantAccountRandomKey);
        form.setCreditCardMerchantRandomId(merchantAccountRandomKey);
        
        form.setBundledData(true);
        form.setHidden(false);
        form.setBillable(true);
        form.setActive(true);
        form.setSerialNumber("123456789012");
        
        return form;
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
        };
    }
}
