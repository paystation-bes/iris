package com.digitalpaytech.mvc.systemadmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Paystation;
import com.digitalpaytech.domain.PaystationType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.domain.RestAccount;
import com.digitalpaytech.domain.RestAccountType;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.systemadmin.support.RESTAccountEditForm;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RestAccountService;
import com.digitalpaytech.service.RestAccountTypeService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.EncryptionServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.RestAccountServiceImpl;
import com.digitalpaytech.service.impl.RestAccountTypeServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.CustomerServiceTestImpl;
import com.digitalpaytech.testing.services.impl.PointOfSaleServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.validation.systemadmin.RESTAccountValidator;

public class RESTAccountControllerTest {
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private ModelMap model;
    private UserAccount userAccount;
    private Customer customer;
    private RESTAccountController controller;
    
    @Before
    public void setUp() throws Exception {
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        this.session = new MockHttpSession();
        this.request.setSession(session);
        this.model = new ModelMap();
        
        this.userAccount = new UserAccount();
        this.userAccount.setId(1);
        
        Customer customer = new Customer();
        customer.setId(2);
        userAccount.setCustomer(customer);
        createAuthentication(userAccount);
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        this.customer = new Customer();
        this.customer.setId(2);
        
        this.controller = createController();
    }
    
    @After
    public void tearDown() throws Exception {
        this.request = null;
        this.response = null;
        this.session = null;
        this.model = null;
        this.userAccount = null;
        this.customer = null;
    }
    
    @Test
    public void test_addRestAccount() {
        
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String customerRandomId = keyMapping.getRandomString(this.customer, WebCoreConstants.ID_LOOK_UP_NAME);
        
        request.setParameter("customerID", customerRandomId);
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        
        String result = controller.addRestAccount(request, response, model);
        assertNotNull(result);
        assertEquals(result, "/systemAdmin/workspace/customers/include/restAccountForm");
    }
    
    @Test
    public void test_deleteRestAccount() {
        
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        String customerRandomId = keyMapping.getRandomString(this.customer, WebCoreConstants.ID_LOOK_UP_NAME);
        
        RestAccount ra = new RestAccount();
        ra.setId(1);
        String raRandomId = keyMapping.getRandomString(ra, WebCoreConstants.ID_LOOK_UP_NAME);
        
        request.setParameter("customerID", customerRandomId);
        request.setParameter("restAccountId", raRandomId);
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        
        String result = controller.deleteRestAccount(request, response, model);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_TRUE);
    }
    
    @Test
    public void test_saveRestAccountRestAccountFail() {
        
        RESTAccountEditForm form = createValidRestAccountTestForm(1, false);
        
        BindingResult result = new BeanPropertyBindingResult(form, "payStationEditForm");
        WebSecurityForm<RESTAccountEditForm> restAccountEditForm = new WebSecurityForm<RESTAccountEditForm>(null, form);
        restAccountEditForm.setPostToken(restAccountEditForm.getInitToken());
        model.put("restAccountEditForm", restAccountEditForm);
        
        String resultStr = controller.saveRestAccount((WebSecurityForm<RESTAccountEditForm>) model.get("restAccountEditForm"), result, request,
                                                      response, model);
        assertNotNull(resultStr);
        //JOptionPane.showMessageDialog(null, "1. resultStr = " + resultStr);
        assertEquals(resultStr.indexOf("token") > -1, true);
    }
    
    @Test
    public void test_saveRestAccountPosFail() {
        
        RESTAccountEditForm form = createValidRestAccountTestForm(2, true);
        
        BindingResult result = new BeanPropertyBindingResult(form, "payStationEditForm");
        WebSecurityForm<RESTAccountEditForm> restAccountEditForm = new WebSecurityForm<RESTAccountEditForm>(null, form);
        restAccountEditForm.setPostToken(restAccountEditForm.getInitToken());
        model.put("restAccountEditForm", restAccountEditForm);
        
        String resultStr = controller.saveRestAccount((WebSecurityForm<RESTAccountEditForm>) model.get("restAccountEditForm"), result, request,
                                                      response, model);
        assertNotNull(resultStr);
        assertEquals(resultStr.indexOf("virtualPOSId") > -1, true);
    }
    
    @Test
    public void test_saveRestAccountSuccess() {
        
        RESTAccountEditForm form = createValidRestAccountTestForm(1, true);
        
        BindingResult result = new BeanPropertyBindingResult(form, "payStationEditForm");
        WebSecurityForm<RESTAccountEditForm> restAccountEditForm = new WebSecurityForm<RESTAccountEditForm>(null, form);
        restAccountEditForm.setPostToken(restAccountEditForm.getInitToken());
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        //String customerRandomId = keyMapping.getRandomString(this.customer, WebCoreConstants.ID_LOOK_UP_NAME);
        //request.setParameter("customerId", customerRandomId);
        model.put("restAccountEditForm", restAccountEditForm);
        String resultStr = controller.saveRestAccount((WebSecurityForm<RESTAccountEditForm>) model.get("restAccountEditForm"), result, request,
                                                      response, model);
        //JOptionPane.showMessageDialog(null, "resultStr = " + resultStr);
        assertNotNull(resultStr);
        assertEquals(resultStr, WebCoreConstants.RESPONSE_TRUE);
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
        };
    }
    
    private PointOfSaleService createPointOfSaleService() {
        return new PointOfSaleServiceTestImpl() {
            public PointOfSale findPointOfSaleById(Integer id) {
                PointOfSale pos = new PointOfSale();
                pos.setId(id);
                pos.setName("TestPos1");
                pos.setCustomer(userAccount.getCustomer());
                Paystation ps = new Paystation();
                PaystationType pst = new PaystationType();
                pst.setId(9);
                ps.setPaystationType(pst);
                pos.setPaystation(ps);
                return pos;
            }
            
            public List<PointOfSale> findVirtualPointOfSalesByCustomerId(int id) {
                List<PointOfSale> posList = new ArrayList<PointOfSale>();
                
                PointOfSale pos = new PointOfSale();
                pos.setId(1);
                pos.setName("VirtualPos1");
                pos.setSerialNumber("VirtualPosSerial");
                pos.setCustomer(userAccount.getCustomer());
                posList.add(pos);
                return posList;
            }
            
            public PosHeartbeat findPosHeartbeatByPointOfSaleId(int id) {
                if (id == 1) {
                    return null;
                } else {
                    PosHeartbeat heartbeat = new PosHeartbeat();
                    PointOfSale pos = new PointOfSale();
                    pos.setId(id);
                    heartbeat.setPointOfSale(pos);
                    return heartbeat;
                }
            }
        };
    }
    
    private RestAccountService createRestAccountService() {
        return new RestAccountServiceImpl() {
            public RestAccount findRestAccountByAccountName(String name) {
                if ("TESTFAIL".equals(name)) {
                    RestAccount ra1 = new RestAccount();
                    ra1.setId(1);
                    ra1.setAccountName("REST AccountnName 1");
                    ra1.setSecretKey("TRALALALALALAA");
                    
                    RestAccountType rat1 = new RestAccountType();
                    rat1.setId(1);
                    rat1.setName("Rest Account Type 1");
                    
                    ra1.setRestAccountType(rat1);
                    PointOfSale pos = new PointOfSale();
                    pos.setId(1);
                    ra1.setPointOfSale(pos);
                    return ra1;
                } else {
                    return null;
                }
                
            }
            
            public List<RestAccount> findNonUniqueRestAccountByAccountName(String name) {
                if ("TESTFAIL".equals(name)) {
                    RestAccount ra1 = new RestAccount();
                    ra1.setId(1);
                    ra1.setAccountName("REST AccountnName 1");
                    ra1.setSecretKey("TRALALALALALAA");
                    
                    RestAccountType rat1 = new RestAccountType();
                    rat1.setId(1);
                    rat1.setName("Rest Account Type 1");
                    
                    ra1.setRestAccountType(rat1);
                    PointOfSale pos = new PointOfSale();
                    pos.setId(1);
                    ra1.setPointOfSale(pos);
                    
                    List<RestAccount> raList = new ArrayList<RestAccount>();
                    raList.add(ra1);
                    return raList;
                } else {
                    return null;
                }
            }
            
            public RestAccount findRestAccountByPointOfSaleId(int posId) {
                if (posId == 2) {
                    RestAccount ra1 = new RestAccount();
                    
                    ra1.setId(1);
                    ra1.setAccountName("REST AccountnName 1");
                    ra1.setSecretKey("TRALALALALALAA");
                    
                    RestAccountType rat1 = new RestAccountType();
                    rat1.setId(1);
                    rat1.setName("Rest Account Type 1");
                    
                    ra1.setRestAccountType(rat1);
                    return ra1;
                } else {
                    return null;
                }
            }
            
            public RestAccount findRestAccountById(int id) {
                RestAccount ra1 = new RestAccount();
                ra1.setId(1);
                ra1.setAccountName("REST AccountnName 1");
                ra1.setSecretKey("TRALALALALALAA");
                
                RestAccountType rat1 = new RestAccountType();
                rat1.setId(1);
                rat1.setName("Rest Account Type 1");
                
                ra1.setRestAccountType(rat1);
                return ra1;
            }
            
            public void saveOrUpdate(RestAccount ra) {
                return;
            }
            
            public void delete(RestAccount ra) {
                return;
            }
            
        };
    }
    
    private RestAccountTypeService createRestAccountTypeService() {
        return new RestAccountTypeServiceImpl() {
            public List<RestAccountType> loadAll() {
                List<RestAccountType> ratList = new ArrayList<RestAccountType>();
                RestAccountType rat1 = new RestAccountType();
                rat1.setId(1);
                rat1.setName("Rest Account Type 1");
                ratList.add(rat1);
                return ratList;
            }
            
            public String getText(int restAccountTypeId) {
                return "TEST";
            }
            
            @Override
            public Map<Integer, RestAccountType> getRestAccountTypesMap() {
                RestAccountType type1 = new RestAccountType();
                type1.setId(1);
                type1.setName("Transaction Push");
                
                RestAccountType type2 = new RestAccountType();
                type2.setId(2);
                type2.setName("Coupon Push");
                
                Map<Integer, RestAccountType> map = new HashMap<Integer, RestAccountType>(2);
                map.put(1, type1);
                map.put(2, type2);
                return map;
            }
        };
    }
    
    private CustomerService createCustomerService() {
        return new CustomerServiceTestImpl() {
            @Override
            public Customer findCustomer(Integer customerId) {
                Customer customer = new Customer();
                customer.setId(1);
                customer.setIsMigrated(true);
                return customer;
            }
        };
    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setCustomerTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT);
        user.setIsComplexPassword(true);
        user.setIsSystemAdmin(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_LICENSES);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_LICENSES);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private RESTAccountController createController() {
        controller = new RESTAccountController();
        controller.setEncryptionService(new EncryptionServiceImpl());
        controller.setRestAccountValidator(createRESTAccountValidator());
        RESTAccountValidator validator = this.createValidator();
        validator.setRestAccountTypeService(createRestAccountTypeService());
        validator.setPointOfSaleService(createPointOfSaleService());
        controller.setRestAccountValidator(validator);
        
        controller.setPointOfSaleService(createPointOfSaleService());
        controller.setRestAccountService(createRestAccountService());
        controller.setCustomerService(createCustomerService());
        controller.setRestAccountTypeService(createRestAccountTypeService());
        
        return controller;
    }
    
    private RESTAccountValidator createValidator() {
        RESTAccountValidator validator = new RESTAccountValidator() {
            @Override
            public String getMessage(String code) {
                return "error.common.invalid.posttoken";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "Test Message!";
            }
        };
        
        return validator;
    }
    
    private RESTAccountEditForm createValidRestAccountTestForm(int posId, boolean passRestAccountName) {
        
        RESTAccountEditForm form = new RESTAccountEditForm();
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        
        form.setAccountName(passRestAccountName ? "TEST REST Account" : "TESTFAIL");
        String customerRandomId = keyMapping.getRandomString(this.customer, WebCoreConstants.ID_LOOK_UP_NAME);
        form.setCustomerId(customerRandomId);
        form.setType(1);
        PointOfSale pos = new PointOfSale();
        pos.setId(posId);
        String posRandomId = keyMapping.getRandomString(pos, WebCoreConstants.ID_LOOK_UP_NAME);
        form.setVirtualPOSId(posRandomId);
        form.setVirtualPosRealId(posId);
        
        return form;
    }
    
    private RESTAccountValidator createRESTAccountValidator() {
        RESTAccountValidator validator = new RESTAccountValidator() {
            @Override
            public String getMessage(String code) {
                return "";
            }
            
            @Override
            public String getMessage(String code, Object... args) {
                return "";
            }
            
            @Override
            protected Date validateTime(WebSecurityForm<RESTAccountEditForm> form, String fieldName, String fieldLabelKey, String value,
                TimeZone timeZone) {
                return super.validateTime(form, fieldName, fieldLabelKey, value, TimeZone.getTimeZone("PST"));
            }
            
            @Override
            public boolean isRestAccountValidAndAvailable(List<RestAccount> restAccounts, int restAccountTypeId) {
                return true;
            }
        };
        
        return validator;
    }
}
