package com.digitalpaytech.mvc.systemadmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.cardprocessing.CardProcessingMaster;
import com.digitalpaytech.cardprocessing.support.CardTransactionZipProcessor;
import com.digitalpaytech.domain.Cluster;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.domain.UserStatusType;
import com.digitalpaytech.dto.EncryptionInfo;
import com.digitalpaytech.dto.systemadmin.CryptoKeyInfo;
import com.digitalpaytech.dto.systemadmin.CryptoKeyListInfo;
import com.digitalpaytech.exception.CryptoException;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.scheduling.task.NightlyTransactionRetryProcessingTask;
import com.digitalpaytech.service.ClusterMemberService;
import com.digitalpaytech.service.CryptoService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.EncryptionService;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.ClusterMemberServiceImpl;
import com.digitalpaytech.service.impl.CryptoServiceImpl;
import com.digitalpaytech.service.impl.EmsPropertiesServiceImpl;
import com.digitalpaytech.service.impl.EncryptionServiceImpl;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.util.MessageHelper;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.crypto.CryptoConstants;

public class SystemAdminSystemStatusControllerTest {
    
    private static final String TRUE = WebCoreConstants.RESPONSE_TRUE;
    private static final String FALSE = WebCoreConstants.RESPONSE_FALSE;
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private ModelMap model;
    private UserAccount userAccount;
    
    private SystemAdminSystemStatusController controller;
    
    @Before
    public final void setUp() throws Exception {
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        this.session = new MockHttpSession();
        this.request.setSession(this.session);
        this.model = new ModelMap();
        
        this.userAccount = new UserAccount();
        this.userAccount.setId(1);
        
        final UserStatusType ust1 = new UserStatusType();
        ust1.setId(1);
        this.userAccount.setUserStatusType(ust1);
        
        final Set<UserRole> roles = new HashSet<UserRole>();
        final UserRole role = new UserRole();
        final Role r = new Role();
        final Set<RolePermission> rps = new HashSet<RolePermission>();
        
        final RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        final Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_VIEW_SERVER_STATUS);
        rp1.setPermission(permission1);
        
        final RolePermission rp2 = new RolePermission();
        rp2.setId(2);
        final Permission permission2 = new Permission();
        permission2.setId(WebSecurityConstants.PERMISSION_SERVER_OPERATIONS);
        rp2.setPermission(permission2);
        
        rps.add(rp1);
        rps.add(rp2);
        
        r.setId(3);
        r.setRolePermissions(rps);
        
        role.setId(3);
        role.setRole(r);
        roles.add(role);
        this.userAccount.setUserRoles(roles);
        
        final Customer customer = new Customer();
        customer.setId(2);
        this.userAccount.setCustomer(customer);
        this.createAuthentication(this.userAccount);
        
        this.controller = new SystemAdminSystemStatusController();
        this.controller.setEncryptionService(createEncryptionService());
        this.controller.setClusterMemberService(createClusterMemberService());
        this.controller.setEmsPropertiesService(createEmsPropertiesService());
        this.controller.setCryptoService(createCryptoService(0));
        this.controller.setNightlyTransactionRetryProcessor(new NightlyTransactionRetryProcessingTask());
        this.controller.getNightlyTransactionRetryProcessor().setCardProcessingMaster(new CardProcessingMaster());
        this.controller.setCardTransactionZipProcessor(createCardTransactionZipProcessor());
        this.controller.setMessages(createMessageHelper());
    }
    
    @After
    public void tearDown() throws Exception {
        this.request = null;
        this.response = null;
        this.session = null;
        this.model = null;
        this.userAccount = null;
    }
    
    @Test
    public void testGetSystemMonitor() {
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        final String result = this.controller.getSystemMonitor(this.request, this.response, this.model);
        assertNotNull(result);
        assertEquals(result, "/systemAdmin/systemStatus/systemMonitor");
        assertNotNull(this.model.get("encryptionInfo"));
    }
    
    @Test
    public void testGetSystemMonitorPermissionFail() {
        createFailedAuthentication();
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        final String result = this.controller.getSystemMonitor(this.request, this.response, this.model);
        
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
        assertEquals(this.response.getStatus(), HttpStatus.UNAUTHORIZED.value());
    }
    
    @Test
    public void testGetServerAdmin() throws CryptoException {
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        this.controller.setNightlyTransactionRetryProcessor(createNightlyTransactionRetryProcessingTask());
        
        final String result = this.controller.getServerAdmin(this.request, this.response, this.model);
        assertNotNull(result);
        assertEquals(result, "/systemAdmin/systemStatus/serverAdmin");
    }
    
    private NightlyTransactionRetryProcessingTask createNightlyTransactionRetryProcessingTask() {
        final NightlyTransactionRetryProcessingTask result = new NightlyTransactionRetryProcessingTask() {
            @Override
            public Map<Integer, Integer> getAllCardRetryQueueSize() {
                return new HashMap<Integer, Integer>();
            }
        };
        
        result.setCardProcessingMaster(createCardProcessingMaster());
        
        return result;
    }
    
    private CardProcessingMaster createCardProcessingMaster() {
        final CardProcessingMaster result = new CardProcessingMaster() {
            
            @Override
            public int getSettlementQueueSize() {
                return 0;
            }
            
            @Override
            public int getStoreForwardQueueSize() {
                return 0;
            }
            
            @Override
            public int getReversalQueueSize() {
                return 0;
            }
        };
        
        return result;
    }
    
    @Test
    public void testGetServerAdminPermissionFail() throws CryptoException {
        createFailedAuthentication();
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        final String result = this.controller.getServerAdmin(this.request, this.response, this.model);
        
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
        assertEquals(this.response.getStatus(), HttpStatus.UNAUTHORIZED.value());
    }
    
    @Test
    public void testPromoteClusterMember() {
        final SystemAdminSystemStatusController cont = new SystemAdminSystemStatusController();
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        WidgetMetricsHelper.registerComponents();
        final String result = cont.promoteClusterMember(this.request);
        assertNotNull(result);
        assertEquals(TRUE, result);
    }
    
    @Test
    public void testPromoteClusterMemberPermissionFail() {
        createFailedAuthentication();
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        final String result = this.controller.promoteClusterMember(this.request);
        assertNotNull(result);
        assertEquals(FALSE, result);
    }
    
    @Test
    public void testProcessUploadedFile() {
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        final String result = this.controller.processUploadedFile();
        
        assertNotNull(result);
        assertEquals(TRUE, result);
    }
    
    @Test
    public void testProcessUploadedFilePermissionFail() {
        createFailedAuthentication();
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        final String result = this.controller.processUploadedFile();
        
        assertNotNull(result);
        assertEquals(FALSE, result);
    }
    
    @Test
    public void testProcessCardRetry() {
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        this.controller.setNightlyTransactionRetryProcessor(new NightlyTransactionRetryProcessingTask() {
            @Override
            public void processNightlyTransactionRetries() {
                
            }
        });
        final String result = this.controller.processCardRetry();
        
        assertNotNull(result);
        assertEquals(TRUE, result);
    }
    
    @Test
    public void testProcessCardRetryPermissionFail() {
        createFailedAuthentication();
        
        final SystemAdminSystemStatusController cont = new SystemAdminSystemStatusController();
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        final String result = cont.processCardRetry();
        
        assertNotNull(result);
        assertEquals(FALSE, result);
    }
    
    @Test
    public void testShutDownTomcatServer() {
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        this.controller.setNightlyTransactionRetryProcessor(new NightlyTransactionRetryProcessingTask());
        this.controller.getNightlyTransactionRetryProcessor().setCardProcessingMaster(new CardProcessingMaster() {
            public synchronized void checkProcessingQueueAndStopServer() {
                
            }
        });
        
        final String result = this.controller.shutDownTomcatServer();
        
        assertNotNull(result);
        assertEquals(TRUE, result);
    }
    
    @Test
    public void testShutDownTomcatServerPermissionFail() {
        createFailedAuthentication();
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        final String result = this.controller.shutDownTomcatServer();
        
        assertNotNull(result);
        assertEquals(FALSE, result);
    }
    
    @Test
    public void testRefreshQueueStatus() {
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        WidgetMetricsHelper.registerComponents();
        
        this.controller.setNightlyTransactionRetryProcessor(createNightlyTransactionRetryProcessingTask());
        
        final String result = this.controller.refreshQueueStatus(this.request);
        
        assertNotNull(result);
    }
    
    @Test
    public void testRefreshQueueStatusFail() {
        createFailedAuthentication();
        
        final WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        final String result = this.controller.refreshQueueStatus(this.request);
        
        assertNotNull(result);
        assertEquals(FALSE, result);
    }
    
    private EncryptionService createEncryptionService() {
        return new EncryptionServiceImpl() {
            public EncryptionInfo getEncryptionInfo() {
                final EncryptionInfo info = new EncryptionInfo();
                info.setCardSerialNumber("123-45678900");
                info.setOperationMode("Development");
                info.setProvider("Bouncy Castle");
                return info;
            }
        };
    }
    
    private EmsPropertiesService createEmsPropertiesService() {
        return new EmsPropertiesServiceImpl() {
            public boolean getPropertyValueAsBoolean(final String propertyName, final boolean defaultValue, final boolean forceGet) {
                return true;
            }
            
            public String getPropertyValue(final String propertyName, final String defaultValue, final boolean forceGet) {
                return "VANAPP01";
            }
            
            public void updatePrimaryServer(final String newPrimaryServer) {
                
            }
        };
    }
    
    private EntityService createEntityService() {
        return new EntityServiceImpl() {
            public Object merge(final Object entity) {
                return userAccount;
            }
        };
    }
    
    private ClusterMemberService createClusterMemberService() {
        return new ClusterMemberServiceImpl() {
            public Collection<Cluster> getAllClusterMembers() {
                final Collection<Cluster> result = new ArrayList<Cluster>();
                result.add(new Cluster(1, "VANAPP01", "eng-tomcat", 8080, new Date(), 1));
                result.add(new Cluster(2, "VANAPP02", "eng-tomcat", 8080, new Date(), 1));
                result.add(new Cluster(3, "VANAPP03", "eng-tomcat", 8080, new Date(), 1));
                return result;
            }
            
            public boolean isClusterMember(final String name) {
                return true;
            }
            
            @Override
            public String getClusterName() {
                return "VANAPP01";
            }
        };
    }
    
    private CardTransactionZipProcessor createCardTransactionZipProcessor() {
        return new CardTransactionZipProcessor() {
            public synchronized void addUnprocessedFileToQueue() {
            }
            
        };
    }
    
    private CryptoService createCryptoService(final int scenario) {
        if (scenario == 0) {
            return new CryptoServiceImpl() {
                @Override
                public CryptoKeyListInfo listActiveKeys(final int purpose) throws CryptoException {
                    final CryptoKeyListInfo result = new CryptoKeyListInfo();
                    
                    result.setCurrentScalaKey(new CryptoKeyInfo());
                    result.getCurrentScalaKey().setCryptoKeyId(1);
                    result.getCurrentScalaKey().setKeyIndex(1);
                    result.getCurrentScalaKey().setKeyType(CryptoService.PURPOSE_CREDIT_CARD_REMOTE);
                    result.getCurrentScalaKey().setHash(CryptoConstants.HASH_ALGORITHM_TYPE_SHA1, "KB+x4Dc2l/sxONg78c+AaZe6tLo=");
                    
                    result.setFutureScalaKey(new CryptoKeyInfo());
                    result.getCurrentScalaKey().setHash(CryptoConstants.HASH_ALGORITHM_TYPE_SHA1, "KBjdklfsl/sxONg78c+AaZe6tLo=");
                    
                    result.setPreviousKeys(new ArrayList<CryptoKeyInfo>(2));
                    
                    final CryptoKeyInfo prevKey = new CryptoKeyInfo();
                    result.getCurrentScalaKey().setCryptoKeyId(2);
                    result.getCurrentScalaKey().setKeyIndex(0);
                    result.getCurrentScalaKey().setKeyType(CryptoService.PURPOSE_CREDIT_CARD_REMOTE);
                    result.getCurrentScalaKey().setHash(CryptoConstants.HASH_ALGORITHM_TYPE_SHA1, "y3rtfEpto5+qTZ/QajsOEwySKZE=");
                    result.getPreviousKeys().add(prevKey);
                    
                    return result;
                }
            };
        } else if (scenario == 1) {
            return new CryptoServiceImpl() {
            };
        } else if (scenario == 2) {
            return new CryptoServiceImpl() {
            };
        } else {
            return new CryptoServiceImpl() {
            };
        }
        
    }
    
    private void createAuthentication(final UserAccount userAcc) {
        final Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        final WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        user.setIsSystemAdmin(true);
        final Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        final Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_SERVER_STATUS);
        permissionList.add(WebSecurityConstants.PERMISSION_SERVER_OPERATIONS);
        permissionList.add(WebSecurityConstants.PERMISSION_SERVER_ADMINISTRATION);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private void createFailedAuthentication() {
        final Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        final WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        user.setIsSystemAdmin(true);
        final Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        final Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(2);
        permissionList.add(3);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(final int userAccountId) {
                final UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
        };
    }
    
    private MessageHelper createMessageHelper() {
        return new MessageHelper() {
            
            @Override
            public String getMessage(final String messageKey) {
                return messageKey;
            }
            
            @Override
            public String getMessage(final String messageKey, final Object... params) {
                String returnValue = messageKey + ": ";
                if (params != null) {
                    for (int i = 0; i < params.length; i++) {
                        returnValue += params[i] + ",";
                    }
                }
                return returnValue;
            }
            
            @Override
            public String getMessageWithKeyParams(final String messageKey, final String... messageKeys) {
                String returnValue = messageKey + ": ";
                if (messageKeys != null) {
                    for (int i = 0; i < messageKeys.length; i++) {
                        returnValue += messageKeys[i] + ",";
                    }
                }
                return returnValue;
            }
            
        };
    }
}
