package com.digitalpaytech.mvc.systemadmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Notification;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.domain.UserStatusType;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.mvc.systemadmin.support.NotificationManagementForm;
import com.digitalpaytech.service.NotificationService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.NotificationServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObject;
import com.digitalpaytech.validation.systemadmin.NotificationManagementValidator;

public class SystemAdminNotificationManagementControllerTest {
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private ModelMap model;
    private UserAccount userAccount;
    
    @Before
    public void setUp() throws Exception {
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        session = new MockHttpSession();
        this.request.setSession(session);
        this.model = new ModelMap();
        
        userAccount = new UserAccount();
        userAccount.setId(1);
        
        UserStatusType ust1 = new UserStatusType();
        ust1.setId(1);
        userAccount.setUserStatusType(ust1);
        
        Set<UserRole> roles = new HashSet<UserRole>();
        UserRole role = new UserRole();
        Role r = new Role();
        Set<RolePermission> rps = new HashSet<RolePermission>();
        
        RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_VIEW_NOTIFICATIONS);
        rp1.setPermission(permission1);
        
        RolePermission rp2 = new RolePermission();
        rp2.setId(2);
        Permission permission2 = new Permission();
        permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_NOTIFICATIONS);
        rp2.setPermission(permission2);
        
        rps.add(rp1);
        rps.add(rp2);
        
        r.setId(3);
        r.setRolePermissions(rps);
        
        role.setId(3);
        role.setRole(r);
        roles.add(role);
        userAccount.setUserRoles(roles);
        
        Customer customer = new Customer();
        customer.setId(2);
        userAccount.setCustomer(customer);
        createAuthentication(userAccount);
    }
    
    @After
    public void tearDown() throws Exception {
        request = null;
        response = null;
        session = null;
        model = null;
        userAccount = null;
    }
    
    @Test
    public void test_getNotificationList() {
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        SystemAdminNotificationManagementController controller = new SystemAdminNotificationManagementController();
        controller.setNotificationService(createNotificationService());
        String result = controller.getNotificationList(request, response, model);
        
        assertNotNull(result);
        assertEquals(result, "/systemAdmin/notification/index");
    }
    
    @Test
    public void test_getNotificationList_role_failure() {
        createFailedAuthentication();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        SystemAdminNotificationManagementController controller = new SystemAdminNotificationManagementController();
        String result = controller.getNotificationList(request, response, model);
        
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
        assertEquals(response.getStatus(), HttpStatus.UNAUTHORIZED.value());
    }
    
    @Test
    public void test_viewNotificationDetails() {
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        Collection<Notification> lists = getNotificationList();
        
        List<WebObject> randomized = (List<WebObject>) keyMapping.hideProperties(lists, WebCoreConstants.ID_LOOK_UP_NAME);
        request.setParameter("notificationID", randomized.get(1).getPrimaryKey().toString());
        
        SystemAdminNotificationManagementController controller = new SystemAdminNotificationManagementController();
        WidgetMetricsHelper.registerComponents();
        controller.setNotificationService(createNotificationService());
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        String result = controller.viewNotificationDetails(request, response);
        assertNotNull(result);
    }
    
    @Test
    public void test_viewNotificationDetails_role_failure() {
        createFailedAuthentication();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        SystemAdminNotificationManagementController controller = new SystemAdminNotificationManagementController();
        String result = controller.viewNotificationDetails(request, response);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_viewNotificationDetails_notificationID_validation_failure() {
        request.setParameter("notificationID", "111111111");
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        SystemAdminNotificationManagementController controller = new SystemAdminNotificationManagementController();
        String result = controller.viewNotificationDetails(request, response);
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_getNotificationForm() {
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        SystemAdminNotificationManagementController controller = new SystemAdminNotificationManagementController();
        String result = controller.getNotificationForm(request, response, model);
        assertNotNull(result);
        assertEquals(result, "/systemAdmin/notificationForm");
    }
    
    @Test
    public void test_getNotificationForm_update() {
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        Collection<Notification> lists = getNotificationList();
        
        List<WebObject> randomized = (List<WebObject>) keyMapping.hideProperties(lists, WebCoreConstants.ID_LOOK_UP_NAME);
        request.setParameter("notificationID", randomized.get(1).getPrimaryKey().toString());
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        SystemAdminNotificationManagementController controller = new SystemAdminNotificationManagementController();
        String result = controller.getNotificationForm(request, response, model);
        assertNotNull(result);
        assertEquals(result, "/systemAdmin/notificationForm");
    }
    
    @Test
    public void test_getNotificationForm_update_invalid_notificationID() {
        request.setParameter("notificationID", "1111111111111111");
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        SystemAdminNotificationManagementController controller = new SystemAdminNotificationManagementController();
        String result = controller.getNotificationForm(request, response, model);
        assertNull(result);
        assertEquals(response.getStatus(), HttpStatus.BAD_REQUEST.value());
    }
    
    @Test
    public void test_getNotificationForm_role_failure() {
        createFailedAuthentication();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        SystemAdminNotificationManagementController controller = new SystemAdminNotificationManagementController();
        String result = controller.getNotificationForm(request, response, model);
        
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
        assertEquals(response.getStatus(), HttpStatus.UNAUTHORIZED.value());
    }
    
    @Test
    public void test_saveNotification() {
        SystemAdminNotificationManagementController controller = new SystemAdminNotificationManagementController();
        NotificationManagementForm form = new NotificationManagementForm();
        form.setTitle("TestTitle");
        form.setMessage("TestMessage");
        form.setLink("http://qa.digitalpaytech.com");
        form.setStartDateTime(new Date());
        form.setEndDateTime(new Date(new Date().getTime() + (1000 * 60 * 24 * 7)));
        
        WebSecurityForm<NotificationManagementForm> webSecurityForm = new WebSecurityForm<NotificationManagementForm>(null, form);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "notificationManagementForm");
        
        controller.setNotificationManagementValidator(new NotificationManagementValidator() {
            public void validate(WebSecurityForm<NotificationManagementForm> command, Errors errors) {
                
            }
        });
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        controller.setNotificationService(createNotificationService());
        
        String res = controller.saveNotification(webSecurityForm, result, request, response);
        assertNotNull(res);
        assertEquals(res, WebCoreConstants.RESPONSE_TRUE);
    }
    
    @Test
    public void test_saveNotification_update() {
        SystemAdminNotificationManagementController controller = new SystemAdminNotificationManagementController();
        NotificationManagementForm form = new NotificationManagementForm();
        form.setTitle("TestTitle");
        form.setMessage("TestMessage");
        form.setLink("http://qa.digitalpaytech.com");
        form.setStartDateTime(new Date());
        form.setEndDateTime(new Date(new Date().getTime() + (1000 * 60 * 24 * 7)));
        
        WebSecurityForm<NotificationManagementForm> webSecurityForm = new WebSecurityForm<NotificationManagementForm>(null, form);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_UPDATE);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "notificationManagementForm");
        
        controller.setNotificationManagementValidator(new NotificationManagementValidator() {
            public void validate(WebSecurityForm<NotificationManagementForm> command, Errors errors) {
                
            }
        });
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        controller.setNotificationService(createNotificationService());
        
        String res = controller.saveNotification(webSecurityForm, result, request, response);
        assertNotNull(res);
        assertEquals(res, WebCoreConstants.RESPONSE_TRUE);
    }
    
    @Test
    public void test_saveNotification_role_failure() {
        createFailedAuthentication();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        NotificationManagementForm form = new NotificationManagementForm();
        form.setTitle("TestTitle");
        form.setMessage("TestMessage");
        form.setLink("http://qa.digitalpaytech.com");
        form.setStartDateTime(new Date());
        form.setEndDateTime(new Date(new Date().getTime() + (1000 * 60 * 24 * 7)));
        
        WebSecurityForm<NotificationManagementForm> webSecurityForm = new WebSecurityForm<NotificationManagementForm>(null, form);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "notificationManagementForm");
        WidgetMetricsHelper.registerComponents();
        
        SystemAdminNotificationManagementController controller = new SystemAdminNotificationManagementController();
        String res = controller.saveNotification(webSecurityForm, result, request, response);
        
        assertEquals(res, WebCoreConstants.RESPONSE_FALSE);
        assertEquals(response.getStatus(), HttpStatus.UNAUTHORIZED.value());
    }
    
    @Test
    public void test_saveNotification_validator_error() {
        SystemAdminNotificationManagementController controller = new SystemAdminNotificationManagementController();
        NotificationManagementForm form = new NotificationManagementForm();
        form.setMessage("TestMessage");
        form.setLink("http://qa.digitalpaytech.com");
        form.setStartDateTime(new Date());
        form.setEndDateTime(new Date(new Date().getTime() + (1000 * 60 * 24 * 7)));
        
        WebSecurityForm<NotificationManagementForm> webSecurityForm = new WebSecurityForm<NotificationManagementForm>(null, form);
        webSecurityForm.setActionFlag(WebSecurityConstants.ACTION_CREATE);
        BindingResult result = new BeanPropertyBindingResult(webSecurityForm, "notificationManagementForm");
        
        controller.setNotificationManagementValidator(new NotificationManagementValidator() {
            public void validate(WebSecurityForm<NotificationManagementForm> webSecurityForm, Errors errors) {
                webSecurityForm.removeAllErrorStatus();
                webSecurityForm.addErrorStatus(new FormErrorStatus("title", "error.common.required"));
            }
        });
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        controller.setNotificationService(createNotificationService());
        WidgetMetricsHelper.registerComponents();
        
        String res = controller.saveNotification(webSecurityForm, result, request, response);
        assertNotNull(res);
        assertTrue(res.contains("\"errorStatus\":{\"errorFieldName\":\"title\",\"message\":\"error.common.required\"}"));
    }
    
    @Test
    public void test_deleteNotification() {
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        Collection<Notification> lists = getNotificationList();
        
        List<WebObject> randomized = (List<WebObject>) keyMapping.hideProperties(lists, WebCoreConstants.ID_LOOK_UP_NAME);
        request.setParameter("notificationID", randomized.get(1).getPrimaryKey().toString());
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        SystemAdminNotificationManagementController controller = new SystemAdminNotificationManagementController();
        controller.setNotificationService(createNotificationService());
        
        String result = controller.deleteNotification(request, response);
        
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_TRUE);
    }
    
    @Test
    public void test_deleteNotification_role_failure() {
        createFailedAuthentication();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        
        SystemAdminNotificationManagementController controller = new SystemAdminNotificationManagementController();
        String result = controller.deleteNotification(request, response);
        
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_deleteNotification_invalid_notificationID() {
        request.setParameter("notificationID", "1111111111111111");
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        });
        SystemAdminNotificationManagementController controller = new SystemAdminNotificationManagementController();
        String result = controller.deleteNotification(request, response);
        
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        user.setCustomerTimeZone(WebCoreConstants.SERVER_TIMEZONE_GMT);
        user.setIsSystemAdmin(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_NOTIFICATIONS);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_NOTIFICATIONS);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private void createFailedAuthentication() {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        user.setIsSystemAdmin(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(2);
        permissionList.add(3);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private NotificationService createNotificationService() {
        return new NotificationServiceImpl() {
            public List<Notification> findNotificationByEffectiveDate() {
                List<Notification> result = new ArrayList<Notification>();
                Notification n1 = new Notification(userAccount, "CurrentTitle1", "CurrentMessage1", new Date(), new Date(new Date().getTime()
                                                                                                                         + (1000 * 60 * 24 * 7)), new Date(),
                        false);
                n1.setId(3);
                Notification n2 = new Notification(userAccount, "CurrentTitle2", "CurrentMessage2", new Date(), new Date(new Date().getTime()
                                                                                                                         + (1000 * 60 * 24 * 7)), new Date(),
                        false);
                n2.setId(4);
                result.add(n1);
                result.add(n2);
                return result;
            }
            
            public List<Notification> findNotificationAllCurrentAndFuture() {
                List<Notification> result = new ArrayList<Notification>();
                Notification n1 = new Notification(userAccount, "CurrentTitle1", "CurrentMessage1", new Date(), new Date(new Date().getTime()
                                                                                                                         + (1000 * 60 * 24 * 7)), new Date(),
                        false);
                n1.setId(3);
                Notification n2 = new Notification(userAccount, "CurrentTitle2", "CurrentMessage2", new Date(), new Date(new Date().getTime()
                                                                                                                         + (1000 * 60 * 24 * 7)), new Date(),
                        false);
                n2.setId(4);
                Notification n3 = new Notification(userAccount, "CurrentTitle3", "CurrentMessage3", new Date(new Date().getTime() + (1000 * 60 * 24 * 7)),
                        new Date(new Date().getTime() + (10000 * 60 * 24 * 7)), new Date(), false);
                n3.setId(5);
                result.add(n1);
                result.add(n2);
                result.add(n3);
                return result;
            }
            
            public List<Notification> findNotificationInPast() {
                List<Notification> result = new ArrayList<Notification>();
                Notification n1 = new Notification(userAccount, "PastTitle1", "PastMessage1", new Date(new Date().getTime() - (1000 * 60 * 24 * 14)), new Date(
                        new Date().getTime() - (1000 * 60 * 24 * 7)), new Date(), false);
                n1.setId(1);
                Notification n2 = new Notification(userAccount, "PastTitle2", "PastMessage2", new Date(new Date().getTime() - (1000 * 60 * 24 * 14)), new Date(
                        new Date().getTime() - (1000 * 60 * 24 * 7)), new Date(), false);
                n2.setId(2);
                result.add(n1);
                result.add(n2);
                return result;
            }
            
            public Notification findNotificationById(Integer id, boolean doEvict) {
                Notification n1 = new Notification(userAccount, "CurrentTitle2", "CurrentMessage2", new Date(), new Date(new Date().getTime()
                                                                                                                         + (1000 * 60 * 24 * 7)), new Date(),
                        false);
                n1.setId(4);
                return n1;
            }
            
            public void saveNotification(Notification notification) {
                
            }
            
            public void updateNotification(Notification notification) {
                
            }
        };
    }
    
    private Collection<Notification> getNotificationList() {
        List<Notification> result = new ArrayList<Notification>();
        Notification n1 = new Notification(userAccount, "CurrentTitle1", "CurrentMessage1", new Date(), new Date(new Date().getTime() + (1000 * 60 * 24 * 7)),
                new Date(), false);
        n1.setId(3);
        Notification n2 = new Notification(userAccount, "CurrentTitle2", "CurrentMessage2", new Date(), new Date(new Date().getTime() + (1000 * 60 * 24 * 7)),
                new Date(), false);
        n2.setId(4);
        result.add(n1);
        result.add(n2);
        return result;
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
        };
    }
}
