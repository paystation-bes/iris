package com.digitalpaytech.mvc.systemadmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.SubscriptionType;
import com.digitalpaytech.domain.TimezoneVId;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.dto.RandomIdNameSetting;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.mvc.systemadmin.support.CustomerEditForm;
import com.digitalpaytech.mvc.systemadmin.support.CustomerSearchForm;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.CustomerAdminServiceTestImpl;
import com.digitalpaytech.testing.services.impl.CustomerServiceTestImpl;
import com.digitalpaytech.testing.services.impl.PointOfSaleServiceTestImpl;
import com.digitalpaytech.testing.services.systemadmin.impl.SystemAdminServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObject;
import com.digitalpaytech.validation.systemadmin.CustomerValidator;

public class SystemAdminMainControllerTest {
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private ModelMap model;
    private UserAccount userAccount;
    
    private static Integer DEFAULT_CUSTOMER_ID = 4;
    private static Integer DEFAULT_UNIFI_ID = 123456;
    private static Integer DEFAULT_UNIFI_ID_2 = 234567;
    private static CustomerValidator DEFAULT_CUSTOMER_VALIDATOR = new CustomerValidator() {
        @Override
        public boolean validateWithMigrated(WebSecurityForm<CustomerEditForm> command, Errors errors) {
            return true;
        }
        
        @Override
        public boolean validateSystemAdminMigrationStatus(WebSecurityForm webSecurityForm, Customer customer) {
            return true;
        }
        
        @Override
        public String getMessage(String code) {
            return getMessage(code, new Object[0], null);
        }
        
        @Override
        public String getMessage(String code, Object... args) {
            return getMessage(code, args, null);
        }
        
        @Override
        public String getMessage(String code, Locale locale) {
            return getMessage(code, new Object[0], locale);
        }
        
        @Override
        public String getMessage(String code, Object[] args, Locale locale) {
            StringBuilder sb = new StringBuilder();
            sb.append(code);
            Stream.of(args).forEach(a -> sb.append("{").append(a).append("}"));
            return sb.append(";").toString();
        }
    };
    
    private static CustomerValidator VALIDATE_MIGRATED_CUSTOMER_VALIDATOR = new CustomerValidator() {        
        @Override
        public boolean validateSystemAdminMigrationStatus(WebSecurityForm webSecurityForm, Customer customer) {
            return true;
        }
        
        @Override
        public String getMessage(String code) {
            return getMessage(code, new Object[0], null);
        }
        
        @Override
        public String getMessage(String code, Object... args) {
            return getMessage(code, args, null);
        }
        
        @Override
        public String getMessage(String code, Locale locale) {
            return getMessage(code, new Object[0], locale);
        }
        
        @Override
        public String getMessage(String code, Object[] args, Locale locale) {
            StringBuilder sb = new StringBuilder();
            sb.append(code);
            Stream.of(args).forEach(a -> sb.append("{").append(a).append("}"));
            return sb.append(";").toString();
        }
    };

    private static UserAccountServiceImpl DEFAULT_USER_ACCOUNT_SERVICE = new UserAccountServiceImpl() {
        public UserAccount findUndeletedUserAccount(String userName) {
            return null;
        }
    };
    
    private static SystemAdminServiceTestImpl DEFAULT_SYSTEM_ADMIN_SERVICE_TEST = new SystemAdminServiceTestImpl() {
        public int createCustomer(Customer customer, UserAccount userAccount, String timeZone) {
            return 1;
        }
    };
    
    private static CustomerAdminServiceTestImpl DEFAULT_CUSTOMER_ADMIN_SERVICE_TEST = new CustomerAdminServiceTestImpl() {
        @Override
        public CustomerProperty getCustomerPropertyByCustomerIdAndCustomerPropertyType(int customerID, int field) {
            CustomerProperty property = new CustomerProperty();
            property.setPropertyValue("PST");
            return property;
        }
        
        public List<TimezoneVId> getTimeZones() {
            TimezoneVId timezoneVId = new TimezoneVId();
            timezoneVId.setId(360);
            timezoneVId.setName("Canada/Atlantic");
            
            TimezoneVId timezoneVId2 = new TimezoneVId();
            timezoneVId2.setId(361);
            timezoneVId2.setName("Canada/Central");
            
            List<TimezoneVId> listTimeZones = new ArrayList<TimezoneVId>();
            listTimeZones.add(timezoneVId);
            listTimeZones.add(timezoneVId2);
            
            return listTimeZones;
        }
    };
    
    @Before
    public void setUp() throws Exception {
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        session = new MockHttpSession();
        this.request.setSession(session);
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        this.model = new ModelMap();
        
        userAccount = new UserAccount();
        userAccount.setId(2);
        Set<UserRole> roles = new HashSet<UserRole>();
        UserRole role = new UserRole();
        Role r = new Role();
        Set<RolePermission> rps = new HashSet<RolePermission>();
        
        RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_DPT_CUSTOMER_ADMINISTRATION);
        rp1.setPermission(permission1);
        
        RolePermission rp2 = new RolePermission();
        rp2.setId(2);
        Permission permission2 = new Permission();
        permission2.setId(WebSecurityConstants.PERMISSION_CREATE_NEW_CUSTOMER);
        rp2.setPermission(permission2);
        
        rps.add(rp1);
        rps.add(rp2);
        
        r.setId(3);
        r.setRolePermissions(rps);
        
        role.setId(3);
        role.setRole(r);
        roles.add(role);
        userAccount.setUserRoles(roles);
        
        Customer customer = new Customer();
        customer.setId(2);
        userAccount.setCustomer(customer);
        createAuthentication(userAccount);
    }
    
    @After
    public void tearDown() throws Exception {
        request = null;
        response = null;
        session = null;
        model = null;
        userAccount = null;
    }
    
    @Test
    public void test_getCustomersAndPaystations() {
        SystemAdminMainController controller = new SystemAdminMainController();
        controller.setPointOfSaleService(this.createPointOfSaleService());
        controller.setCustomerService(this.createCustomerService());
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        String result = controller.getCustomersAndPaystations(request, response, model);
        assertNotNull(result);
        assertEquals(result, "/systemAdmin/index");
    }
    
    @Test
    public void test_getCustomersAndPaystations_role_failure() {
        createFailedAuthentication();
        
        SystemAdminMainController controller = new SystemAdminMainController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        String result = controller.getCustomersAndPaystations(request, response, model);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
        assertEquals(response.getStatus(), HttpStatus.UNAUTHORIZED.value());
    }
    
    @Test
    public void test_searchCustomerAndPos() {
        SystemAdminMainController controller = new SystemAdminMainController();
        controller.setPointOfSaleService(this.createPointOfSaleService());
        controller.setCustomerService(this.createCustomerService());
        request.setParameter("search", "343");
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        WidgetMetricsHelper.registerComponents();
        
        String result = controller.searchCustomerAndPos(request, response);
        assertNotNull(result);
    }
    
    @Test
    public void test_searchCustomer() {
        SystemAdminMainController controller = new SystemAdminMainController();
        controller.setPointOfSaleService(this.createPointOfSaleService());
        controller.setCustomerService(this.createCustomerService());
        request.setParameter("search", "343");
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        WidgetMetricsHelper.registerComponents();
        
        String result = controller.searchCustomer(request, response);
        assertNotNull(result);
    }
    
    @Test
    public void test_searchCustomerAndPos_failure() {
        createFailedAuthentication();
        
        SystemAdminMainController controller = new SystemAdminMainController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        String result = controller.searchCustomerAndPos(request, response);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
        assertEquals(response.getStatus(), HttpStatus.UNAUTHORIZED.value());
    }
    
    @Test
    public void test_searchCustomer_failure() {
        createFailedAuthentication();
        
        SystemAdminMainController controller = new SystemAdminMainController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        String result = controller.searchCustomer(request, response);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
        assertEquals(response.getStatus(), HttpStatus.UNAUTHORIZED.value());
    }
    
    @Test
    public void test_goDetails_customer() {
        Collection<Customer> customers = this.createCustomerService().findAllParentAndChildCustomersBySearchKeyword("test");
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        List<WebObject> objList = (List<WebObject>) keyMapping.hideProperties(customers, "id");
        
        SystemAdminMainController controller = new SystemAdminMainController();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        WebSecurityForm<CustomerSearchForm> form = this.getCustomerSearchForm(2, objList.get(0).getPrimaryKey().toString());
        //TODO remove search form   
        //        String result = controller.goDetails(form, new BeanPropertyBindingResult(form, "customerSearchForm"), request, response, model);
        //        
        //        assertNotNull(result);
        //        assertEquals(result, "redirect:/systemAdmin/workspace/customers/customerDetails.html?customerID=" + objList.get(0).getPrimaryKey().toString());
    }
    
    @Test
    public void test_goDetails_pointofsale() {
        Collection<PointOfSale> posList = this.createPointOfSaleService().findAllPointOfSalesBySerialNumberKeyword("test");
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        List<WebObject> objList = (List<WebObject>) keyMapping.hideProperties(posList, "id");
        WebObject obj = keyMapping.hideProperty(userAccount.getCustomer(), "id");
        
        SystemAdminMainController controller = new SystemAdminMainController();
        controller.setPointOfSaleService(this.createPointOfSaleService());
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        WebSecurityForm<CustomerSearchForm> form = this.getCustomerSearchForm(2, objList.get(0).getPrimaryKey().toString());
        //TODO remove search form   
        //        String result = controller.goDetails(form, new BeanPropertyBindingResult(form, "customerSearchForm"), request, response, model);
        StringBuilder sb = new StringBuilder();
        sb.append("redirect:/systemAdmin/workspace/payStations/index.html?posID=").append(objList.get(0).getPrimaryKey().toString());
        sb.append("&customerID=").append(obj.getPrimaryKey().toString());
        //        assertNotNull(result);
        //        assertEquals(result, sb.toString());
    }
    
    @Test
    public void test_goDetails_role_failure() {
        createFailedAuthentication();
        
        SystemAdminMainController controller = new SystemAdminMainController();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        WebSecurityForm<CustomerSearchForm> form = this.getCustomerSearchForm(2, "dsfsdf");
        //TODO remove search form   
        //        String result = controller.goDetails(form, new BeanPropertyBindingResult(form, "customerSearchForm"), request, response, model);
        //        
        //        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
        //        assertEquals(response.getStatus(), HttpStatus.UNAUTHORIZED.value());
    }
    
    @Test
    public void test_goDetails_objectID_null() {
        SystemAdminMainController controller = new SystemAdminMainController();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        WebSecurityForm<CustomerSearchForm> form = this.getCustomerSearchForm(2, "1111111111");
        //TODO remove search form   
        //        String result = controller.goDetails(form, new BeanPropertyBindingResult(form, "customerSearchForm"), request, response, model);
        //        
        //        assertNotNull(result);
        //        assertTrue(result.contains("errorStatus"));
    }
    
    @Test
    public void test_showNewCustomerForm_create() {
        SystemAdminMainController controller = new SystemAdminMainController();
        controller.setCustomerService(this.createCustomerService());
        controller.setCustomerAdminService(this.createCustomerAdminService());
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        controller.setSubscriptionTypes(getSubscriptionTypes());
        
        String result = controller.showNewCustomerForm(request, response, model);
        assertNotNull(result);
        assertEquals(((Collection<RandomIdNameSetting>) (model.get("parentCustomerList"))).size(), 3);
        assertEquals(result, "/systemAdmin/workspace/customers/include/customerForm");
    }
    
    @Test
    public void test_showNewCustomerForm_update() {
        request.setParameter("customerID", "1111111111111111");
        SystemAdminMainController controller = new SystemAdminMainController();
        controller.setCustomerService(this.createCustomerService());
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        controller.setSubscriptionTypes(getSubscriptionTypes());
        controller.setCustomerAdminService(this.createCustomerAdminService());
        String result = controller.showNewCustomerForm(request, response, model);
        assertNotNull(result);
        assertEquals(((Collection<RandomIdNameSetting>) (model.get("parentCustomerList"))).size(), 3);
        assertEquals(result, "/systemAdmin/workspace/customers/include/customerForm");
    }
    
    @Test
    public void test_showNewCustomerForm_role_failure() {
        createFailedAuthentication();
        SystemAdminMainController controller = new SystemAdminMainController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        String result = controller.showNewCustomerForm(request, response, model);
        
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
        assertEquals(response.getStatus(), HttpStatus.UNAUTHORIZED.value());
    }
    
    @Test
    public void test_saveCustomer() {
        SystemAdminMainController controller = new SystemAdminMainController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        controller.setPasswordEncoder(new ShaPasswordEncoder());
        controller.setCustomerService(this.createCustomerService());
        VALIDATE_MIGRATED_CUSTOMER_VALIDATOR.setCustomerService(new CustomerServiceTestImpl());
        controller.setCustomerAdminService(DEFAULT_CUSTOMER_ADMIN_SERVICE_TEST);
        controller.setCustomerValidator(VALIDATE_MIGRATED_CUSTOMER_VALIDATOR);
        VALIDATE_MIGRATED_CUSTOMER_VALIDATOR.setCustomerService(new CustomerServiceTestImpl());
        controller.setSystemAdminService(DEFAULT_SYSTEM_ADMIN_SERVICE_TEST);
        controller.setUserAccountService(DEFAULT_USER_ACCOUNT_SERVICE);
        
        WebSecurityForm<CustomerEditForm> webSecurityForm = getUserInputForm(0);
        controller.setSubscriptionTypes(getSubscriptionTypes());
        
        BeanPropertyBindingResult resultBinding = new BeanPropertyBindingResult(webSecurityForm, "customerEditForm");
        
        String result = controller.saveCustomer(webSecurityForm, resultBinding, request, response, model);
        
        assertEquals(result, WebCoreConstants.RESPONSE_TRUE);
    }
    
    @Test
    public void test_saveCustomerNullUnifiId() {
        SystemAdminMainController controller = new SystemAdminMainController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        controller.setPasswordEncoder(new ShaPasswordEncoder());
        controller.setCustomerService(this.createCustomerService());
        controller.setCustomerAdminService(DEFAULT_CUSTOMER_ADMIN_SERVICE_TEST);
        controller.setCustomerValidator(VALIDATE_MIGRATED_CUSTOMER_VALIDATOR);
        VALIDATE_MIGRATED_CUSTOMER_VALIDATOR.setCustomerService(new CustomerServiceTestImpl());
        controller.setSystemAdminService(DEFAULT_SYSTEM_ADMIN_SERVICE_TEST);
        controller.setUserAccountService(DEFAULT_USER_ACCOUNT_SERVICE);
        
        WebSecurityForm<CustomerEditForm> webSecurityForm = getUserInputForm(0, null, DEFAULT_CUSTOMER_ID);
        controller.setSubscriptionTypes(getSubscriptionTypes());
        
        BeanPropertyBindingResult resultBinding = new BeanPropertyBindingResult(webSecurityForm, "customerEditForm");
        
        String result = controller.saveCustomer(webSecurityForm, resultBinding, request, response, model);
        
        assertEquals(result, WebCoreConstants.RESPONSE_TRUE);
    }

    @Test
    public void test_saveCustomerExistingCustomerDoesNotHaveUnifiId() {
        // updating a customer which doesn't have a UnifiId yet - should pass
        SystemAdminMainController controller = new SystemAdminMainController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        controller.setPasswordEncoder(new ShaPasswordEncoder());
        controller.setCustomerService(this.createCustomerService());
        controller.setCustomerAdminService(DEFAULT_CUSTOMER_ADMIN_SERVICE_TEST);
        controller.setCustomerValidator(VALIDATE_MIGRATED_CUSTOMER_VALIDATOR);
        VALIDATE_MIGRATED_CUSTOMER_VALIDATOR.setCustomerService(new CustomerServiceTestImpl() {
            @Override
            public Customer findCustomer(Integer id) {
                Customer customer = new Customer();
                customer.setId(DEFAULT_CUSTOMER_ID);
                customer.setIsMigrated(true);
                return customer;
            }
        });
        controller.setSystemAdminService(DEFAULT_SYSTEM_ADMIN_SERVICE_TEST);
        controller.setUserAccountService(DEFAULT_USER_ACCOUNT_SERVICE);
        
        WebSecurityForm<CustomerEditForm> webSecurityForm = getUserInputForm(0);

        controller.setSubscriptionTypes(getSubscriptionTypes());

        BeanPropertyBindingResult resultBinding = new BeanPropertyBindingResult(webSecurityForm, "customerEditForm");
        
        String result = controller.saveCustomer(webSecurityForm, resultBinding, request, response, model);        
        assertEquals(result, WebCoreConstants.RESPONSE_TRUE);
    }

    @Test
    public void test_saveCustomerExistingCustomerAlreadyHasDifferentUnifiId() {
        // updating a customer which already has a UnifiId yet and is different from current being passed in - should fail
        SystemAdminMainController controller = new SystemAdminMainController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        controller.setPasswordEncoder(new ShaPasswordEncoder());
        controller.setCustomerService(this.createCustomerService());
        controller.setCustomerAdminService(DEFAULT_CUSTOMER_ADMIN_SERVICE_TEST);
        controller.setCustomerValidator(VALIDATE_MIGRATED_CUSTOMER_VALIDATOR);
        VALIDATE_MIGRATED_CUSTOMER_VALIDATOR.setCustomerService(new CustomerServiceTestImpl() {
            @Override
            public Customer findCustomer(Integer id) {
                Customer customer = new Customer();
                customer.setId(DEFAULT_CUSTOMER_ID);
                customer.setUnifiId(DEFAULT_UNIFI_ID_2);
                customer.setIsMigrated(true);
                return customer;
            }
        });

        controller.setSystemAdminService(DEFAULT_SYSTEM_ADMIN_SERVICE_TEST);
        controller.setUserAccountService(DEFAULT_USER_ACCOUNT_SERVICE);
        
        WebSecurityForm<CustomerEditForm> webSecurityForm = getUserInputForm(0);

        controller.setSubscriptionTypes(getSubscriptionTypes());

        BeanPropertyBindingResult resultBinding = new BeanPropertyBindingResult(webSecurityForm, "customerEditForm");
        
        String result = controller.saveCustomer(webSecurityForm, resultBinding, request, response, model);        
        
        String errorMessage = DEFAULT_CUSTOMER_VALIDATOR.getMessage("error.common.invalid.update",
                                                                    new Object[] { DEFAULT_CUSTOMER_VALIDATOR.getMessage("label.customer.unifiId") });
        assertTrue(webSecurityForm.getValidationErrorInfo().getErrorStatus().size() == 1);
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getErrorFieldName(), "unifiId");
        assertEquals(((FormErrorStatus) (webSecurityForm.getValidationErrorInfo().getErrorStatus().get(0))).getMessage(), errorMessage);
    }

    @Test
    public void test_saveCustomerExistingCustomerAlreadyHasSameUnifiId() {
        // updating a customer which already has a UnifiId yet and is same as current being passed in - should pass
        SystemAdminMainController controller = new SystemAdminMainController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        controller.setPasswordEncoder(new ShaPasswordEncoder());
        controller.setCustomerService(this.createCustomerService());
        controller.setCustomerAdminService(DEFAULT_CUSTOMER_ADMIN_SERVICE_TEST);
        controller.setCustomerValidator(VALIDATE_MIGRATED_CUSTOMER_VALIDATOR);
        VALIDATE_MIGRATED_CUSTOMER_VALIDATOR.setCustomerService(new CustomerServiceTestImpl() {
            @Override
            public Customer findCustomer(Integer id) {
                Customer customer = new Customer();
                customer.setId(DEFAULT_CUSTOMER_ID);
                customer.setUnifiId(DEFAULT_UNIFI_ID);
                customer.setIsMigrated(true);
                return customer;
            }
        });
        controller.setSystemAdminService(DEFAULT_SYSTEM_ADMIN_SERVICE_TEST);
        controller.setUserAccountService(DEFAULT_USER_ACCOUNT_SERVICE);
        
        WebSecurityForm<CustomerEditForm> webSecurityForm = getUserInputForm(0);

        controller.setSubscriptionTypes(getSubscriptionTypes());

        BeanPropertyBindingResult resultBinding = new BeanPropertyBindingResult(webSecurityForm, "customerEditForm");
        
        String result = controller.saveCustomer(webSecurityForm, resultBinding, request, response, model);                
        assertEquals(result, WebCoreConstants.RESPONSE_TRUE);
    }

    @Test
    public void test_saveCustomer_role_failure() {
        createFailedAuthentication();
        
        SystemAdminMainController controller = new SystemAdminMainController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        WebSecurityForm<CustomerEditForm> webSecurityForm = getUserInputForm(0);
        WidgetMetricsHelper.registerComponents();
        String result = controller.saveCustomer(webSecurityForm, new BeanPropertyBindingResult(webSecurityForm, "customerEditForm"), request,
                                                response, model);
        
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
        assertEquals(response.getStatus(), HttpStatus.UNAUTHORIZED.value());
    }
    
    @Test
    public void test_saveCustomer_validator_error() {
        SystemAdminMainController controller = new SystemAdminMainController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        controller.setPasswordEncoder(new ShaPasswordEncoder());
        controller.setCustomerService(this.createCustomerService());
        controller.setCustomerValidator(new CustomerValidator() {
            @Override
            public boolean validateWithMigrated(WebSecurityForm<CustomerEditForm> webSecurityForm, Errors errors) {
                webSecurityForm.removeAllErrorStatus();
                webSecurityForm.addErrorStatus(new FormErrorStatus("postToken", "error.common.invalid.posttoken"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("customerID", "error.common.invalid"));
                return true;
            }
        });
        WidgetMetricsHelper.registerComponents();
        
        WebSecurityForm<CustomerEditForm> webSecurityForm = getUserInputForm(0);
        String result = controller.saveCustomer(webSecurityForm, new BeanPropertyBindingResult(webSecurityForm, "customerEditForm"), request,
                                                response, model);
        
        assertNotNull(result);
        assertTrue(result
                .contains("\"errorStatus\":[{\"errorFieldName\":\"postToken\",\"message\":\"error.common.invalid.posttoken\"},{\"errorFieldName\":\"customerID\",\"message\":\"error.common.invalid\"}]"));
    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        user.setIsSystemAdmin(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_DPT_CUSTOMER_ADMINISTRATION);
        permissionList.add(WebSecurityConstants.PERMISSION_CREATE_NEW_CUSTOMER);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private void createFailedAuthentication() {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(2);
        permissionList.add(3);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private EntityService createEntityService() {
        return new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        };
    }
    
    private PointOfSaleService createPointOfSaleService() {
        return new PointOfSaleServiceTestImpl() {
            public Collection<PointOfSale> findAllPointOfSalesBySerialNumberKeyword(String keyword) {
                Collection<PointOfSale> lists = new ArrayList<PointOfSale>();
                PointOfSale pos1 = new PointOfSale();
                Customer customer = new Customer();
                customer.setId(3);
                customer.setName("momomomo");
                pos1.setId(1);
                pos1.setSerialNumber("3434234234");
                pos1.setCustomer(customer);
                PointOfSale pos2 = new PointOfSale();
                pos2.setId(2);
                pos2.setSerialNumber("3435234234");
                pos2.setCustomer(customer);
                
                lists.add(pos1);
                lists.add(pos2);
                return lists;
            }
            
            public Collection<PointOfSale> findAllPointOfSalesByPosNameKeyword(String keyword) {
                Collection<PointOfSale> lists = new ArrayList<PointOfSale>();
                PointOfSale pos1 = new PointOfSale();
                Customer customer = new Customer();
                customer.setId(3);
                customer.setName("momomomo");
                pos1.setId(1);
                pos1.setId(1);
                pos1.setName("TestPos1");
                pos1.setCustomer(customer);
                PointOfSale pos2 = new PointOfSale();
                pos2.setId(2);
                pos2.setName("TestPos2");
                pos2.setCustomer(customer);
                
                lists.add(pos1);
                lists.add(pos2);
                return lists;
            }
            
            public PointOfSale findPointOfSaleById(Integer id) {
                PointOfSale pos = new PointOfSale();
                pos.setId(1);
                pos.setName("TestPos1");
                pos.setCustomer(userAccount.getCustomer());
                
                return pos;
            }
        };
    }
    
    private CustomerAdminService createCustomerAdminService() {
        
        return new CustomerAdminServiceTestImpl() {
            public List<TimezoneVId> getTimeZones() {
                TimezoneVId timezoneVId = new TimezoneVId();
                timezoneVId.setId(360);
                timezoneVId.setName("Canada/Atlantic");
                
                TimezoneVId timezoneVId2 = new TimezoneVId();
                timezoneVId2.setId(361);
                timezoneVId2.setName("Canada/Central");
                
                List<TimezoneVId> listTimeZones = new ArrayList<TimezoneVId>();
                listTimeZones.add(timezoneVId);
                listTimeZones.add(timezoneVId2);
                
                return listTimeZones;
                
            }
        };
    }
    
    private CustomerService createCustomerService() {
        return new CustomerServiceTestImpl() {
            
            public Customer findCustomer(Integer id) {
                Customer c1 = new Customer();
                c1.setId(1);
                c1.setIsMigrated(true);
                c1.setName("TestCustomer1");
                return c1;
            }
            
            public boolean update(Customer customer) {
                return true;
            }
            
            public Collection<Customer> findAllParentAndChildCustomersBySearchKeyword(String keyword) {
                Collection<Customer> lists = new ArrayList<Customer>();
                Customer c1 = new Customer();
                c1.setId(1);
                c1.setName("TestCustomer1");
                Customer c2 = new Customer();
                c2.setId(2);
                c2.setName("TestCustomer2");
                
                lists.add(c1);
                lists.add(c2);
                return lists;
            }
            
            public Collection<Customer> findAllParentCustomers() {
                Collection<Customer> lists = new ArrayList<Customer>();
                Customer c1 = new Customer();
                c1.setId(1);
                c1.setName("TestCustomer1");
                Customer c2 = new Customer();
                c2.setId(2);
                c2.setName("TestCustomer2");
                Customer c3 = new Customer();
                c3.setId(3);
                c3.setName("TestCustomer3");
                
                lists.add(c1);
                lists.add(c2);
                lists.add(c3);
                return lists;
            }
            
            @Override
            public Collection<Customer> findAllParentAndChildCustomersBySearchKeyword(String keyword, Integer... customerIds) {
                Collection<Customer> lists = new ArrayList<Customer>();
                Customer c1 = new Customer();
                c1.setId(1);
                c1.setName("TestCustomer1");
                Customer c2 = new Customer();
                c2.setId(2);
                c2.setName("TestCustomer2");
                
                lists.add(c1);
                lists.add(c2);
                return lists;
            }
        };
    }
    
    private WebSecurityForm<CustomerEditForm> getUserInputForm(int actionFlag) {
        return getUserInputForm(actionFlag, DEFAULT_UNIFI_ID, DEFAULT_CUSTOMER_ID);
    }
    
    private WebSecurityForm<CustomerEditForm> getUserInputForm(int actionFlag, Integer unifiId, Integer customerId) {
        WebSecurityForm<CustomerEditForm> form = new WebSecurityForm<CustomerEditForm>();
        form.setActionFlag(actionFlag);
        form.setInitToken("1111111111111");
        form.setPostToken("1111111111111");

        CustomerEditForm customerEditForm = new CustomerEditForm();
        customerEditForm.setAccountStatus("enabled");
        customerEditForm.setCreditCardProcessing(true);
        customerEditForm.setCoupons(true);
        customerEditForm.setIsParentCompany(false);
        customerEditForm.setCustomerId(customerId);
        customerEditForm.setUserName("TestUser");
        customerEditForm.setLegalName("Test Legal Name");
        customerEditForm.setPassword("Password#01");
        customerEditForm.setPasswordConfirmed("Password#01");
        customerEditForm.setUnifiId(unifiId);
        form.setWrappedObject(customerEditForm);
        return form;
    }
    
    private List<SubscriptionType> getSubscriptionTypes() {
        SubscriptionType reportSub = new SubscriptionType();
        reportSub.setId(100);
        reportSub.setName("Standard Reports");
        reportSub.setRandomId("randomId100");
        
        SubscriptionType alertstSub = new SubscriptionType();
        alertstSub.setId(200);
        alertstSub.setName("Alerts");
        alertstSub.setRandomId("randomId200");
        
        SubscriptionType ccSub = new SubscriptionType();
        ccSub.setId(300);
        ccSub.setName("Real-Time Credit Card Procesing");
        ccSub.setRandomId("randomId300");
        
        SubscriptionType batchCcSub = new SubscriptionType();
        batchCcSub.setId(400);
        batchCcSub.setName("Batch Credit Card Processing");
        batchCcSub.setRandomId("randomId400");
        
        List<SubscriptionType> list = new ArrayList<SubscriptionType>();
        list.add(reportSub);
        list.add(alertstSub);
        list.add(ccSub);
        list.add(batchCcSub);
        return list;
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                return userAccount;
            }
        };
    }
    
    private WebSecurityForm<CustomerSearchForm> getCustomerSearchForm(int actionFlag, String randomId) {
        WebSecurityForm<CustomerSearchForm> form = new WebSecurityForm<CustomerSearchForm>();
        form.setActionFlag(actionFlag);
        CustomerSearchForm customerSearchForm = new CustomerSearchForm();
        customerSearchForm.setRandomId(randomId);
        form.setWrappedObject(customerSearchForm);
        return form;
    }    
}
