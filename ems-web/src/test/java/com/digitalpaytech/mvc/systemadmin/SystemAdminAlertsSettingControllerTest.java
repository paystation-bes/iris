package com.digitalpaytech.mvc.systemadmin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.AlertClassType;
import com.digitalpaytech.domain.AlertThresholdType;
import com.digitalpaytech.domain.AlertType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerAlertEmail;
import com.digitalpaytech.domain.CustomerAlertType;
import com.digitalpaytech.domain.CustomerEmail;
import com.digitalpaytech.domain.CustomerType;
import com.digitalpaytech.domain.Permission;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Role;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserRole;
import com.digitalpaytech.mvc.support.AlertEditForm;
import com.digitalpaytech.mvc.support.FormErrorStatus;
import com.digitalpaytech.mvc.support.WebSecurityForm;
import com.digitalpaytech.mvc.support.WidgetMetricsHelper;
import com.digitalpaytech.service.AlertTypeService;
import com.digitalpaytech.service.CustomerAlertTypeService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.EntityService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.testing.services.impl.AlertTypeServiceTestImpl;
import com.digitalpaytech.testing.services.impl.CustomerAlertTypeServiceTestImpl;
import com.digitalpaytech.testing.services.impl.CustomerServiceTestImpl;
import com.digitalpaytech.testing.services.impl.PointOfSaleServiceTestImpl;
import com.digitalpaytech.testing.services.impl.RouteServiceTestImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.util.support.WebObject;
import com.digitalpaytech.validation.AlertSettingValidator;

public class SystemAdminAlertsSettingControllerTest {
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockHttpSession session;
    private ModelMap model;
    private UserAccount userAccount;
    
    @Before
    public void setUp() throws Exception {
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        session = new MockHttpSession();
        this.request.setSession(session);
        this.model = new ModelMap();
        
        userAccount = new UserAccount();
        userAccount.setId(2);
        Set<UserRole> roles = new HashSet<UserRole>();
        UserRole role = new UserRole();
        Role r = new Role();
        Set<RolePermission> rps = new HashSet<RolePermission>();
        
        RolePermission rp1 = new RolePermission();
        rp1.setId(1);
        Permission permission1 = new Permission();
        permission1.setId(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS);
        rp1.setPermission(permission1);
        
        RolePermission rp2 = new RolePermission();
        rp2.setId(2);
        Permission permission2 = new Permission();
        permission2.setId(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT);
        rp2.setPermission(permission2);
        
        rps.add(rp1);
        rps.add(rp2);
        
        r.setId(3);
        r.setRolePermissions(rps);
        
        role.setId(3);
        role.setRole(r);
        roles.add(role);
        userAccount.setUserRoles(roles);
        
        Customer customer = new Customer();
        customer.setId(2);
        CustomerType customerType = new CustomerType();
        customerType.setId(1);
        customer.setCustomerType(customerType);
        
        userAccount.setCustomer(customer);
        createAuthentication(userAccount);
    }
    
    @After
    public void cleanUp() throws Exception {
        request = null;
        response = null;
        session = null;
        model = null;
        userAccount = null;
    }
    
    @Test
    public void test_getAlertList() {
        setCustomerIdParameter();
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        controller.setAlertTypeService(createAlertTypeService());
        controller.setRouteService(createRouteService());
        controller.setCustomerAlertTypeService(createCustomerAlertTypeServiceForSort());
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        String result = controller.getAlertList(request, response, model);
        assertNotNull(result);
        assertEquals(result, "/systemAdmin/workspace/alerts/definedAlerts");
    }
    
    @Test
    public void test_getAlertList_role_failure() {
        createFailedAuthentication();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        String result = controller.getAlertList(request, response, model);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
        assertTrue(response.getStatus() == HttpStatus.UNAUTHORIZED.value());
    }
    
    @Test
    public void test_sortAlertList() {
        setCustomerIdParameter();
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        controller.setCustomerAlertTypeService(createCustomerAlertTypeServiceForSort());
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        WidgetMetricsHelper.registerComponents();
        
        String result = controller.sortAlertList(request, response, model);
        assertNotNull(result);
    }
    
    @Test
    public void test_sortAlertList_parameter_sortBy_name() {
        setCustomerIdParameter();
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        request.setParameter("sortBy", "name");
        controller.setCustomerAlertTypeService(createCustomerAlertTypeServiceForSort());
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        WidgetMetricsHelper.registerComponents();
        
        String result = controller.sortAlertList(request, response, model);
        assertNotNull(result);
    }
    
    @Test
    public void test_sortAlertList_parameter_sortBy_type() {
        setCustomerIdParameter();
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        request.setParameter("sortBy", "type");
        controller.setCustomerAlertTypeService(createCustomerAlertTypeServiceForSort());
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        WidgetMetricsHelper.registerComponents();
        
        String result = controller.sortAlertList(request, response, model);
        assertNotNull(result);
    }
    
    @Test
    public void test_sortAlertList_parameter_sortBy_name_orderBy_asc() {
        setCustomerIdParameter();
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        request.setParameter("sortBy", "name");
        request.setParameter("orderBy", "asc");
        controller.setCustomerAlertTypeService(createCustomerAlertTypeServiceForSort());
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        WidgetMetricsHelper.registerComponents();
        
        String result = controller.sortAlertList(request, response, model);
        assertNotNull(result);
    }
    
    @Test
    public void test_sortAlertList_parameter_sortBy_name_orderBy_desc() {
        setCustomerIdParameter();
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        request.setParameter("sortBy", "name");
        request.setParameter("orderBy", "desc");
        controller.setCustomerAlertTypeService(createCustomerAlertTypeServiceForSort());
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        WidgetMetricsHelper.registerComponents();
        
        String result = controller.sortAlertList(request, response, model);
        assertNotNull(result);
    }
    
    @Test
    public void test_sortAlertList_parameter_sortBy_type_orderBy_asc() {
        setCustomerIdParameter();
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        request.setParameter("sortBy", "type");
        request.setParameter("orderBy", "asc");
        controller.setCustomerAlertTypeService(createCustomerAlertTypeServiceForSort());
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        WidgetMetricsHelper.registerComponents();
        
        String result = controller.sortAlertList(request, response, model);
        assertNotNull(result);
    }
    
    @Test
    public void test_sortAlertList_parameter_sortBy_type_orderBy_desc() {
        setCustomerIdParameter();
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        request.setParameter("sortBy", "type");
        request.setParameter("orderBy", "desc");
        controller.setCustomerAlertTypeService(createCustomerAlertTypeServiceForSort());
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        WidgetMetricsHelper.registerComponents();
        
        String result = controller.sortAlertList(request, response, model);
        assertNotNull(result);
    }
    
    @Test
    public void test_sortAlertList_parameter_sortBy_different() {
        setCustomerIdParameter();
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        request.setParameter("sortBy", "aaa");
        controller.setCustomerAlertTypeService(createCustomerAlertTypeServiceForSort());
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        WidgetMetricsHelper.registerComponents();
        
        String result = controller.sortAlertList(request, response, model);
        assertNotNull(result);
    }
    
    @Test
    public void test_viewAlertDetails() {
        WebObject webObj = getCustomerAlertType();
        request.setParameter("alertID", webObj.getPrimaryKey().toString());
        
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        controller.setCustomerAlertTypeService(createCustomerAlertTypeService());
        controller.setRouteService(createRouteService());
        controller.setAlertTypeService(createAlertTypeService());
        WidgetMetricsHelper.registerComponents();
        
        String result = controller.viewAlertDetails(request, response, model);
        assertNotNull(result);
    }
    
    @Test
    public void test_viewAlertDetails_with_Route() {
        WebObject webObj = getCustomerAlertType();
        request.setParameter("alertID", webObj.getPrimaryKey().toString());
        
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        controller.setCustomerAlertTypeService(createCustomerAlertTypeServiceWithRoute());
        controller.setRouteService(createRouteService());
        controller.setAlertTypeService(createAlertTypeService());
        WidgetMetricsHelper.registerComponents();
        
        String result = controller.viewAlertDetails(request, response, model);
        assertNotNull(result);
    }
    
    @Test
    public void test_viewAlertDetails_role_failure() {
        createFailedAuthentication();
        
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        String result = controller.viewAlertDetails(request, response, model);
        
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_viewAlertDetails_invalid_alertId() {
        request.setParameter("alertID", "1234567");
        
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        String result = controller.viewAlertDetails(request, response, model);
        
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_saveAlert() {
        setCustomerIdParameter();
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        controller.setAlertSettingValidator(new AlertSettingValidator() {
            @Override
            public void validate(WebSecurityForm<AlertEditForm> command, Errors errors, RandomKeyMapping keyMapping) {
                
            }
        });
        
        controller.setAlertTypeService(createAlertTypeService());
        controller.setCustomerService(createCustomerService());
        controller.setRouteService(createRouteService());
        controller.setCustomerAlertTypeService(createCustomerAlertTypeService());
        WebSecurityForm<AlertEditForm> webSecurityForm = getUserInputForm(0);
        String result = controller.saveAlert(webSecurityForm, new BeanPropertyBindingResult(webSecurityForm, "alertEditForm"), request, response,
                                             model);
        
        assertNotNull(result);
        assertTrue(result.contains(WebCoreConstants.RESPONSE_TRUE));
    }
    
    @Test
    public void test_saveAlert_all_paystations() {
        setCustomerIdParameter();
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        controller.setAlertSettingValidator(new AlertSettingValidator() {
            @Override
            public void validate(WebSecurityForm<AlertEditForm> command, Errors errors, RandomKeyMapping keyMapping) {
                
            }
        });
        
        controller.setAlertTypeService(createAlertTypeService());
        controller.setCustomerService(createCustomerService());
        
        controller.setRouteService(createRouteService());
        controller.setCustomerAlertTypeService(createCustomerAlertTypeService());
        WebSecurityForm<AlertEditForm> webSecurityForm = getUserInputForm(0);
        String result = controller.saveAlert(webSecurityForm, new BeanPropertyBindingResult(webSecurityForm, "alertEditForm"), request, response,
                                             model);
        
        assertNotNull(result);
        assertTrue(result.contains(WebCoreConstants.RESPONSE_TRUE));
    }
    
    @Test
    public void test_saveAlert_role_failure() {
        createFailedAuthentication();
        
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        WidgetMetricsHelper.registerComponents();
        
        WebSecurityForm<AlertEditForm> webSecurityForm = getUserInputForm(0);
        String res = controller.saveAlert(webSecurityForm, new BeanPropertyBindingResult(webSecurityForm, "alertEditForm"), request, response, model);
        
        assertEquals(res, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_saveAlert_validation_failure() {
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        controller.setAlertSettingValidator(new AlertSettingValidator() {
            @Override
            public void validate(WebSecurityForm<AlertEditForm> webSecurityForm, Errors errors, RandomKeyMapping keyMapping) {
                webSecurityForm.removeAllErrorStatus();
                webSecurityForm.addErrorStatus(new FormErrorStatus("postToken", "error.common.invalid.posttoken"));
                webSecurityForm.addErrorStatus(new FormErrorStatus("alertName", "error.common.required"));
            }
        });
        WidgetMetricsHelper.registerComponents();
        
        WebSecurityForm<AlertEditForm> webSecurityForm = getUserInputForm(0);
        String res = controller.saveAlert(webSecurityForm, new BeanPropertyBindingResult(webSecurityForm, "alertEditForm"), request, response, model);
        
        assertNotNull(res);
        assertTrue(res
                .contains("\"errorStatus\":[{\"errorFieldName\":\"postToken\",\"message\":\"error.common.invalid.posttoken\"},{\"errorFieldName\":\"alertName\",\"message\":\"error.common.required\"}]}"));
    }
    
    @Test
    public void test_saveAlert_editAlert() {
        setCustomerIdParameter();
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        controller.setAlertSettingValidator(new AlertSettingValidator() {
            @Override
            public void validate(WebSecurityForm<AlertEditForm> command, Errors errors, RandomKeyMapping keyMapping) {
                
            }
        });
        controller.setCustomerService(createCustomerService());
        
        controller.setRouteService(createRouteService());
        controller.setAlertTypeService(createAlertTypeService());
        controller.setCustomerAlertTypeService(createCustomerAlertTypeService());
        WebSecurityForm<AlertEditForm> webSecurityForm = getUserInputForm(1);
        String result = controller.saveAlert(webSecurityForm, new BeanPropertyBindingResult(webSecurityForm, "alertEditForm"), request, response,
                                             model);
        
        assertNotNull(result);
        assertTrue(result.contains(WebCoreConstants.RESPONSE_TRUE));
    }
    
    @Test
    public void test_deleteAlert() {
        setCustomerIdParameter();
        WebObject webObj = getCustomerAlertType();
        request.setParameter("alertID", webObj.getPrimaryKey().toString());
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        controller.setCustomerService(createCustomerService());
        
        controller.setCustomerAlertTypeService(createCustomerAlertTypeService());
        String result = controller.deleteAlert(request, response, model);
        
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_TRUE);
    }
    
    @Test
    public void test_deleteAlert_role_failure() {
        createFailedAuthentication();
        
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        String result = controller.deleteAlert(request, response, model);
        
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    @Test
    public void test_deleteAlert_invalid_alertID() {
        request.setParameter("alertID", "123456");
        SystemAdminAlertsSettingController controller = new SystemAdminAlertsSettingController();
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        util.setEntityService(createEntityService());
        
        controller.setCustomerAlertTypeService(createCustomerAlertTypeService());
        String result = controller.deleteAlert(request, response, model);
        
        assertNotNull(result);
        assertEquals(result, WebCoreConstants.RESPONSE_FALSE);
    }
    
    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        user.setIsSystemAdmin(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_USER_DEFINED_ALERTS);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_USER_DEFINED_ALERT);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private void createFailedAuthentication() {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        user.setIsComplexPassword(true);
        user.setIsSystemAdmin(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(2);
        permissionList.add(3);
        user.setUserPermissions(permissionList);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private AlertTypeService createAlertTypeService() {
        return new AlertTypeServiceTestImpl() {
            AlertClassType communication = new AlertClassType((byte) 1, "Communication", new Date(), 1);
            AlertClassType collection = new AlertClassType((byte) 2, "Collection", new Date(), 1);
            AlertClassType paystationalert = new AlertClassType((byte) 3, "Pay Station Alert", new Date(), 1);
            
            AlertType lastSeenInterval = new AlertType((short) 1, communication, "Last Seen Interval", true, new Date(), 1);
            AlertType runningTotal = new AlertType((short) 2, collection, "Running Total", true, new Date(), 1);
            AlertType coinCanister = new AlertType((short) 3, collection, "Coin Canister", true, new Date(), 1);
            AlertType billStacker = new AlertType((short) 4, collection, "Bill Stacker", true, new Date(), 1);
            AlertType unsettledCC = new AlertType((short) 5, collection, "Unsettled Credit Card", true, new Date(), 1);
            AlertType psAlert = new AlertType((short) 6, paystationalert, "Paystation Alert", false, new Date(), 1);
            
            public Collection<AlertClassType> findAllAlertClassTypes() {
                Collection<AlertClassType> result = new ArrayList<AlertClassType>();
                result.add(new AlertClassType((byte) 1, "Communication", new Date(), 1));
                result.add(new AlertClassType((byte) 2, "Collection", new Date(), 1));
                result.add(new AlertClassType((byte) 3, "Pay Station Alert", new Date(), 1));
                return result;
            }
            
            public Collection<AlertType> findAlertTypeByIsUserDefined(boolean isUserDefined) {
                Collection<AlertType> result = new ArrayList<AlertType>();
                AlertClassType alertClassType = new AlertClassType();
                alertClassType.setId((byte) 2);
                alertClassType.setName("Collection");
                result.add(lastSeenInterval);
                result.add(runningTotal);
                result.add(coinCanister);
                result.add(billStacker);
                result.add(unsettledCC);
                result.add(psAlert);
                return result;
            }
            
            public List<AlertType> findAllAlertTypes() {
                List<AlertType> result = new ArrayList<AlertType>();
                result.add(lastSeenInterval);
                result.add(runningTotal);
                result.add(coinCanister);
                result.add(billStacker);
                result.add(unsettledCC);
                result.add(psAlert);
                return result;
            }
            
            public AlertThresholdType findAlertThresholdTypeById(short id) {
                AlertType alertType = null;
                String name = "";
                switch (id) {
                    case 1:
                        alertType = lastSeenInterval;
                        name = "Last Seen Interval Hour";
                        break;
                    case 2:
                        alertType = runningTotal;
                        name = "Running Total Dollar";
                        break;
                    case 6:
                        alertType = coinCanister;
                        name = "Coin Canister Count";
                        break;
                    case 7:
                        alertType = coinCanister;
                        name = "Coin Canister Dollar";
                        break;
                    case 8:
                        alertType = billStacker;
                        name = "Bill Staker Count";
                        break;
                    case 9:
                        alertType = billStacker;
                        name = "Bill Staker Dollar";
                        break;
                    case 10:
                        alertType = unsettledCC;
                        name = "Unsettled Credit Card Count";
                        break;
                    case 11:
                        alertType = unsettledCC;
                        name = "Unsettled Credit Card Dollar";
                        break;
                    case 12:
                        alertType = psAlert;
                        name = "Pay Station Alert";
                        
                }
                ;
                return new AlertThresholdType(id, alertType, name, new Date(), 1);
            }
        };
    }
    
    private CustomerAlertTypeService createCustomerAlertTypeServiceForSort() {
        return new CustomerAlertTypeServiceTestImpl() {
            public Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdOrderByNameAsc(Integer customerId, Integer filterType) {
                Collection<CustomerAlertType> result = new ArrayList<CustomerAlertType>();
                AlertType alertType = new AlertType();
                alertType.setId((short) 1);
                alertType.setName("aAlert1");
                Customer customer = new Customer();
                customer.setId(2);
                customer.setName("momomomo");
                AlertClassType alertClassType1 = new AlertClassType();
                alertClassType1.setId((byte) 1);
                alertClassType1.setName("aClassType1");
                alertType.setAlertClassType(alertClassType1);
                
                AlertType alertType1 = new AlertType();
                alertType1.setId((short) 2);
                alertType1.setName("bAlert1");
                AlertClassType alertClassType2 = new AlertClassType();
                alertClassType2.setId((byte) 2);
                alertClassType2.setName("bClassType1");
                alertType1.setAlertClassType(alertClassType2);
                
                AlertThresholdType alertThresholdType = new AlertThresholdType((short) 1, alertType, "aAlert1", new Date(), 1);
                AlertThresholdType alertThresholdType1 = new AlertThresholdType((short) 2, alertType1, "bAlert1", new Date(), 1);
                result.add(new CustomerAlertType(alertThresholdType, customer, "aAlert1", StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1, true,
                        false, false, new Date(), 1));
                result.add(new CustomerAlertType(alertThresholdType1, customer, "bAlert1", StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1, true,
                        false, false, new Date(), 1));
                return result;
            }
            
            public CustomerAlertType findDefaultCustomerAlertTypeByCustomerIdAndAlertThresholdTypeId(Integer customerId, short alertThresholdTypeId) {
                CustomerAlertType type = new CustomerAlertType();
                AlertClassType classType = new AlertClassType((byte) 1, "TestClassType", new Date(), 1);
                AlertType alertType = new AlertType((short) 1, classType, "TestAlertType", true, new Date(), 1);
                AlertThresholdType alerThresholdType = new AlertThresholdType((short) 1, alertType, "TestAlertType", new Date(), 1);
                type.setAlertThresholdType(alerThresholdType);
                type.setThreshold(120f);
                type.setId(2);
                
                return type;
            }
            
            public Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdOrderByNameDesc(Integer customerId, Integer filterType) {
                Collection<CustomerAlertType> result = new ArrayList<CustomerAlertType>();
                AlertType alertType = new AlertType();
                alertType.setId((short) 1);
                alertType.setName("aAlert1");
                Customer customer = new Customer();
                customer.setId(2);
                customer.setName("momomomo");
                AlertClassType alertClassType1 = new AlertClassType();
                alertClassType1.setId((byte) 1);
                alertClassType1.setName("aClassType1");
                alertType.setAlertClassType(alertClassType1);
                
                AlertType alertType1 = new AlertType();
                alertType1.setId((short) 2);
                alertType1.setName("bAlert1");
                AlertClassType alertClassType2 = new AlertClassType();
                alertClassType2.setId((byte) 2);
                alertClassType2.setName("bClassType1");
                alertType1.setAlertClassType(alertClassType2);
                
                AlertThresholdType alertThresholdType = new AlertThresholdType((short) 1, alertType, "aAlert1", new Date(), 1);
                AlertThresholdType alertThresholdType1 = new AlertThresholdType((short) 2, alertType1, "bAlert1", new Date(), 1);
                result.add(new CustomerAlertType(alertThresholdType1, customer, "bAlert1", StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1, true,
                        false, false, new Date(), 1));
                result.add(new CustomerAlertType(alertThresholdType, customer, "aAlert1", StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1, true,
                        false, false, new Date(), 1));
                return result;
            }
            
            public Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdOrderByAlertTypeAsc(Integer customerId, Integer filterType) {
                Collection<CustomerAlertType> result = new ArrayList<CustomerAlertType>();
                AlertType alertType = new AlertType();
                alertType.setId((short) 1);
                alertType.setName("aAlert1");
                Customer customer = new Customer();
                customer.setId(2);
                customer.setName("momomomo");
                AlertClassType alertClassType1 = new AlertClassType();
                alertClassType1.setId((byte) 1);
                alertClassType1.setName("aClassType1");
                alertType.setAlertClassType(alertClassType1);
                
                AlertType alertType1 = new AlertType();
                alertType1.setId((short) 2);
                alertType1.setName("bAlert1");
                AlertClassType alertClassType2 = new AlertClassType();
                alertClassType2.setId((byte) 2);
                alertClassType2.setName("bClassType1");
                alertType1.setAlertClassType(alertClassType2);
                
                AlertThresholdType alertThresholdType = new AlertThresholdType((short) 1, alertType, "aAlert1", new Date(), 1);
                AlertThresholdType alertThresholdType1 = new AlertThresholdType((short) 2, alertType1, "bAlert1", new Date(), 1);
                result.add(new CustomerAlertType(alertThresholdType, customer, "aAlert1", StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1, true,
                        false, false, new Date(), 1));
                result.add(new CustomerAlertType(alertThresholdType1, customer, "bAlert1", StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1, true,
                        false, false, new Date(), 1));
                
                return result;
            }
            
            public Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdOrderByAlertTypeDesc(Integer customerId, Integer filterType) {
                Collection<CustomerAlertType> result = new ArrayList<CustomerAlertType>();
                AlertType alertType = new AlertType();
                alertType.setId((short) 1);
                alertType.setName("aAlert1");
                Customer customer = new Customer();
                customer.setId(2);
                customer.setName("momomomo");
                AlertClassType alertClassType1 = new AlertClassType();
                alertClassType1.setId((byte) 1);
                alertClassType1.setName("aClassType1");
                alertType.setAlertClassType(alertClassType1);
                
                AlertType alertType1 = new AlertType();
                alertType1.setId((short) 2);
                alertType1.setName("bAlert1");
                AlertClassType alertClassType2 = new AlertClassType();
                alertClassType2.setId((byte) 2);
                alertClassType2.setName("bClassType1");
                alertType1.setAlertClassType(alertClassType2);
                
                AlertThresholdType alertThresholdType = new AlertThresholdType((short) 1, alertType, "aAlert1", new Date(), 1);
                AlertThresholdType alertThresholdType1 = new AlertThresholdType((short) 2, alertType1, "bAlert1", new Date(), 1);
                result.add(new CustomerAlertType(alertThresholdType1, customer, "bAlert1", StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1, true,
                        false, false, new Date(), 1));
                result.add(new CustomerAlertType(alertThresholdType, customer, "aAlert1", StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1, true,
                        false, false, new Date(), 1));
                
                return result;
            }
        };
    }
    
    private CustomerAlertTypeService createCustomerAlertTypeService() {
        return new CustomerAlertTypeServiceTestImpl() {
            @Override
            public CustomerAlertType findCustomerAlertTypeById(Integer id) {
                CustomerAlertType type = new CustomerAlertType();
                AlertClassType classType = new AlertClassType((byte) 1, "TestClassType", new Date(), 1);
                AlertType alertType = new AlertType((short) 1, classType, "TestAlertType", true, new Date(), 1);
                AlertThresholdType alertThresholdType = new AlertThresholdType((short) 2, alertType, "TestAlertThresholdType", new Date(), 1);
                type.setAlertThresholdType(alertThresholdType);
                type.setThreshold(120f);
                
                Route route = new Route();
                route.setId(3);
                type.setRoute(route);
                return type;
            }
            
            @Override
            public CustomerAlertType findDefaultCustomerAlertTypeByCustomerIdAndAlertThresholdTypeId(Integer customerId, int alertThresholdTypeId) {
                CustomerAlertType type = new CustomerAlertType();
                AlertClassType classType = new AlertClassType((byte) 1, "TestClassType", new Date(), 1);
                AlertType alertType = new AlertType((short) 1, classType, "TestAlertType", true, new Date(), 1);
                AlertThresholdType alerThresholdType = new AlertThresholdType((short) 1, alertType, "TestAlertType", new Date(), 1);
                type.setAlertThresholdType(alerThresholdType);
                type.setThreshold(120f);
                type.setId(2);
                
                return type;
            }
            
            @Override
            public void saveCustomerAlertType(CustomerAlertType customerAlertType) {
                
            }
            
            @Override
            public Collection<CustomerAlertType> findCustomerAlertTypesByCustomerIdAlertThresholdTypeIdAndIsActive(Integer customerId,
                Integer alertThresholdTypeId, boolean isActive) {
                Collection<CustomerAlertType> list = new ArrayList<CustomerAlertType>();
                list.add(new CustomerAlertType());
                
                return list;
            }
            
            @Override
            public void updateCustomerAlertType(CustomerAlertType customerAlertType, Collection<String> emailAddresses) {
                
            }
            
            @Override
            public void deleteCustomerAlertType(CustomerAlertType customerAlertType, Integer userAccountId) {
                
            }
            
            @Override
            public CustomerAlertType findCustomerAlertTypeByIdAndEvict(Integer id) {
                CustomerAlertType type = new CustomerAlertType();
                AlertClassType classType = new AlertClassType((byte) 1, "TestClassType", new Date(), 1);
                AlertType alertType = new AlertType((short) 1, classType, "TestAlertType", true, new Date(), 1);
                AlertThresholdType alertThresholdType = new AlertThresholdType((short) 2, alertType, "TestAlertThresholdType", new Date(), 1);
                type.setAlertThresholdType(alertThresholdType);
                Route r = new Route();
                r.setId(2);
                r.setName("TestRoute");
                type.setThreshold(120f);
                type.setRoute(r);
                
                Set<CustomerAlertEmail> alertEmails = new HashSet<CustomerAlertEmail>();
                alertEmails.add(new CustomerAlertEmail(1, new CustomerEmail(userAccount.getCustomer(), "email2@digitalpaytech.com", new Date(), 1,
                        false), type, new Date(), 1, false));
                alertEmails.add(new CustomerAlertEmail(2, new CustomerEmail(userAccount.getCustomer(), "email3@digitalpaytech.com", new Date(), 1,
                        false), type, new Date(), 1, false));
                alertEmails.add(new CustomerAlertEmail(3, new CustomerEmail(userAccount.getCustomer(), "email31@digitalpaytech.com", new Date(), 1,
                        false), type, new Date(), 1, false));
                type.setCustomerAlertEmails(alertEmails);
                return type;
            }
        };
    }
    
    private CustomerAlertTypeService createCustomerAlertTypeServiceWithRoute() {
        return new CustomerAlertTypeServiceTestImpl() {
            public CustomerAlertType findCustomerAlertTypeById(Integer id) {
                CustomerAlertType type = new CustomerAlertType();
                AlertClassType classType = new AlertClassType((byte) 1, "TestClassType", new Date(), 1);
                AlertType alertType = new AlertType((short) 1, classType, "TestAlertType", true, new Date(), 1);
                AlertThresholdType alertThresholdType = new AlertThresholdType((short) 2, alertType, "TestAlertThresholdType", new Date(), 1);
                type.setAlertThresholdType(alertThresholdType);
                type.setThreshold(120f);
                Route r = new Route();
                r.setId(2);
                type.setRoute(r);
                return type;
            }
        };
    }
    
    private RouteService createRouteService() {
        return new RouteServiceTestImpl() {
            public Route findRouteById(Integer id) {
                Route route = new Route();
                route.setId(2);
                route.setName("TestRoute");
                return route;
            }
            
            public Collection<Route> findRoutesByCustomerId(int customerId) {
                Collection<Route> routes = new ArrayList<Route>();
                Route route1 = new Route();
                route1.setId(1);
                route1.setName("TestRoute1");
                Route route2 = new Route();
                route2.setId(2);
                route2.setName("TestRoute2");
                routes.add(route1);
                routes.add(route2);
                return routes;
            }
            
            public List<PointOfSale> findPointOfSalesByRouteId(int routeId) {
                List<PointOfSale> pos = new ArrayList<PointOfSale>();
                PointOfSale p = new PointOfSale();
                p.setId(1);
                p.setName("SamplePoS");
                pos.add(p);
                return pos;
            }
        };
    }
    
    private EntityService createEntityService() {
        return new EntityServiceImpl() {
            public Object merge(Object entity) {
                return userAccount;
            }
        };
    }
    
    private PointOfSaleService createPointOfSaleService() {
        return new PointOfSaleServiceTestImpl() {
            public List<PointOfSale> findPointOfSalesByCustomerId(int customerId) {
                List<PointOfSale> pos = new ArrayList<PointOfSale>();
                PointOfSale p = new PointOfSale();
                p.setId(1);
                p.setName("SamplePoS");
                PointOfSale p1 = new PointOfSale();
                p1.setId(2);
                p1.setName("SamplePoS2");
                pos.add(p1);
                return pos;
            }
        };
    }
    
    private WebObject getCustomerAlertType() {
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        CustomerAlertType type = new CustomerAlertType();
        type.setId(1);
        type.setName("TestCustomerAlertType1");
        return (WebObject) keyMapping.hideProperty(type, "id");
    }
    
    private WebSecurityForm<AlertEditForm> getUserInputForm(int actionFlag) {
        WebSecurityForm<AlertEditForm> form = new WebSecurityForm<AlertEditForm>();
        form.setActionFlag(actionFlag);
        AlertEditForm alertForm = new AlertEditForm();
        alertForm.setAlertName("Bill Over 5 Dollars");
        alertForm.setStatus(true);
        alertForm.setAlertTypeId((short) 2);
        alertForm.setAlertThresholdExceed("140");
        alertForm.setRouteId(2);
        
        List<String> alertContacts = new ArrayList<String>();
        alertContacts.add("email@digitalpaytech.com");
        alertContacts.add("email2@digitalpaytech.com");
        alertContacts.add("email4@digitalpaytech.com");
        alertForm.setAlertContacts(alertContacts);
        form.setWrappedObject(alertForm);
        return form;
    }
    
    private void setCustomerIdParameter() {
        Customer customer = new Customer();
        customer.setId(2);
        CustomerType customerType = new CustomerType();
        customerType.setId(1);
        customer.setCustomerType(customerType);
        RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
        WebObject webObj = (WebObject) keyMapping.hideProperty(customer, "id");
        request.setParameter("customerID", webObj.getPrimaryKey().toString());
    }
    
    private CustomerService createCustomerService() {
        return new CustomerServiceTestImpl() {
            public Customer findCustomer(Integer id) {
                Customer customer = new Customer();
                customer.setId(10);
                CustomerType customerType = new CustomerType();
                customerType.setId(3);
                customer.setCustomerType(customerType);
                return customer;
            }
        };
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
        };
    }
}
