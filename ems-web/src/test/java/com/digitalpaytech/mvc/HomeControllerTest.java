package com.digitalpaytech.mvc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.WebAttributes;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.UserAccount;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/ems-test.xml"})
public class HomeControllerTest {
	@Autowired
	private ApplicationContext applicationContext;

	@Test
	public void test_showHome_success() {
		MockHttpServletRequest request = new MockHttpServletRequest("GET", "/");
		// Build mock response
		MockHttpServletResponse response = new MockHttpServletResponse();
		
		HomeController controller = new HomeController();
		controller.setApplicationContext(applicationContext);
		
		UserAccount userAccount = new UserAccount();
		createAuthentication(userAccount, true);
		
		ModelAndView result = (ModelAndView) controller.showHome(request, response);
		
		assertEquals(result.getViewName(), "index");
	}
	
	@Test
	public void test_showHome_has_authenticationException() {
		MockHttpServletRequest request = new MockHttpServletRequest("GET", "/");
		// Build mock response
		MockHttpServletResponse response = new MockHttpServletResponse();
		
		HomeController controller = new HomeController();
		controller.setApplicationContext(applicationContext);
		
		UserAccount userAccount = new UserAccount();
		createAuthentication(userAccount, true);
		
		WebUtils.setSessionAttribute(request,
				WebAttributes.AUTHENTICATION_EXCEPTION,
				new ProviderNotFoundException(""));
		
		ModelAndView result = (ModelAndView) controller.showHome(request, response);
		
		assertEquals(result.getViewName(), "index");
		assertTrue(!result.getModel().isEmpty());
	}
	
	@Test
	public void test_showHome_has_parameter_error_1() {
		MockHttpServletRequest request = new MockHttpServletRequest("GET", "/");
		request.setParameter("error", "-1");
		// Build mock response
		MockHttpServletResponse response = new MockHttpServletResponse();
		
		HomeController controller = new HomeController();
		controller.setApplicationContext(applicationContext);
		
		UserAccount userAccount = new UserAccount();
		createAuthentication(userAccount, true);
		
		WebUtils.setSessionAttribute(request,
				WebAttributes.AUTHENTICATION_EXCEPTION,
				new ProviderNotFoundException(""));
		
		ModelAndView result = (ModelAndView) controller.showHome(request, response);
		
		assertEquals(result.getViewName(), "index");
		assertTrue(!result.getModel().get("status").equals("-1"));
	}

	@Test
	public void test_showHome_has_parameter_error_2() {
		MockHttpServletRequest request = new MockHttpServletRequest("GET", "/");
		request.setParameter("error", "-2");
		// Build mock response
		MockHttpServletResponse response = new MockHttpServletResponse();
		
		HomeController controller = new HomeController();
		controller.setApplicationContext(applicationContext);
		
		UserAccount userAccount = new UserAccount();
		createAuthentication(userAccount, true);
		
		WebUtils.setSessionAttribute(request,
				WebAttributes.AUTHENTICATION_EXCEPTION,
				new ProviderNotFoundException(""));
		
		ModelAndView result = (ModelAndView) controller.showHome(request, response);
		
		assertEquals(result.getViewName(), "index");
		assertTrue(!result.getModel().get("status").equals("-2"));
	}
	
	@Test
	public void test_showHome_has_parameter_error_4() {
		MockHttpServletRequest request = new MockHttpServletRequest("GET", "/");
		request.setParameter("error", "-4");
		// Build mock response
		MockHttpServletResponse response = new MockHttpServletResponse();
		
		HomeController controller = new HomeController();
		controller.setApplicationContext(applicationContext);
		
		UserAccount userAccount = new UserAccount();
		createAuthentication(userAccount, true);
		
		WebUtils.setSessionAttribute(request,
				WebAttributes.AUTHENTICATION_EXCEPTION,
				new ProviderNotFoundException(""));
		
		ModelAndView result = (ModelAndView) controller.showHome(request, response);
		
		assertEquals(result.getViewName(), "index");
		assertTrue(!result.getModel().get("status").equals("-4"));
	}
	
	@Test
	public void test_showHome_has_parameter_error_5() {
		MockHttpServletRequest request = new MockHttpServletRequest("GET", "/");
		request.setParameter("error", "-5");
		// Build mock response
		MockHttpServletResponse response = new MockHttpServletResponse();
		
		HomeController controller = new HomeController();
		controller.setApplicationContext(applicationContext);
		
		UserAccount userAccount = new UserAccount();
		createAuthentication(userAccount, true);
		
		WebUtils.setSessionAttribute(request,
				WebAttributes.AUTHENTICATION_EXCEPTION,
				new ProviderNotFoundException(""));
		
		ModelAndView result = (ModelAndView) controller.showHome(request, response);
		
		assertEquals(result.getViewName(), "index");
		assertTrue(!result.getModel().get("status").equals("-5"));
	}
	
	private void createAuthentication(UserAccount userAccount, boolean isComplexPassword) {

		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("Admin"));
		WebUser user = new WebUser(1, 1, "testUser", "password", "salt", true,
				authorities);
		user.setIsComplexPassword(isComplexPassword);
		Authentication authentication = new UsernamePasswordAuthenticationToken(
				user, "password", authorities);
		SecurityContextHolder.getContext().setAuthentication(authentication);
	}
}
