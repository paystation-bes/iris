package com.digitalpaytech.mvc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Section;
import com.digitalpaytech.domain.Tab;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.UserDefaultDashboard;
import com.digitalpaytech.domain.Widget;
import com.digitalpaytech.dto.TabHeading;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.impl.EntityServiceImpl;
import com.digitalpaytech.service.impl.UserAccountServiceImpl;
import com.digitalpaytech.util.RandomKeyMapping;
import com.digitalpaytech.util.SessionTool;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;

public class DashboardControllerTest {
    
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    
    @Before
    public void setUp() throws Exception {
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
    }
    
    @After
    public void cleanUp() throws Exception {
        request = null;
        response = null;
    }
    
    private Tab createTabSectionsWidgets(int id) {
        Widget w1 = new Widget();
        Widget w2 = new Widget();
        w1.setId(1);
        w2.setId(2);
        ArrayList<Widget> ws = new ArrayList<Widget>();
        ws.add(w1);
        ws.add(w2);
        
        Section se = new Section();
        se.setId(1);
        se.setWidgets(ws);
        ArrayList<Section> ss = new ArrayList<Section>();
        ss.add(se);
        
        Tab tab = new Tab();
        tab.setId(id);
        tab.setSections(ss);
        
        return tab;
    }
    
    @Test
    public void createNewTabTest() throws Exception {
        
        request.getSession(true);
        SessionTool sessionTool = SessionTool.getInstance(request);
        
        ModelMap model = new ModelMap();
        
        ConcurrentHashMap<String, Tab> tabTreeMap = new ConcurrentHashMap<String, Tab>();
        ArrayList<TabHeading> tabHeadings = new ArrayList<TabHeading>();
        
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, tabTreeMap);
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, tabHeadings);
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        EntityServiceImpl serv = new EntityServiceImpl() {
            public Object merge(Object entity) {
                UserAccount userAccount = new UserAccount();
                userAccount.setId(2);
                userAccount.setUserDefaultDashboard(new UserDefaultDashboard());
                Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
                authorities.add(new SimpleGrantedAuthority("Admin"));
                WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
                user.setIsComplexPassword(true);
                Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
                SecurityContextHolder.getContext().setAuthentication(authentication);
                return userAccount;
            }
        };
        util.setEntityService(serv);
        
        UserAccount userAccount = new UserAccount();
        userAccount.setId(1);
        createAuthentication(userAccount);
        
        DashboardController controller = new DashboardController();
        
        String tempId = controller.createNewTab(request, response, model);
        assertNotNull(tempId);
        
        tabTreeMap = (ConcurrentHashMap) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        tabHeadings = (ArrayList) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_HEADINGS);
        
        assertFalse(tabTreeMap.isEmpty());
        
        assertTrue(tabTreeMap.containsKey(tempId));
        assertNotNull(tabTreeMap.get(tempId).getSections());
        assertEquals(1, tabHeadings.size());
    }
    
//    @Test
//    public void updateTabOrderTest() throws Exception {
//        
//        request.getSession(true);
//        SessionTool sessionTool = SessionTool.getInstance(request);
//        UserAccount userAccount = new UserAccount();
//        createAuthentication(userAccount);
//        
//        ConcurrentHashMap<String, Tab> tabTreeMap = new ConcurrentHashMap<String, Tab>();
//        ArrayList<TabHeading> tabHeadings = new ArrayList<TabHeading>();
//        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, tabTreeMap);
//        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, tabHeadings);
//        
//        ModelMap model = new ModelMap();
//        DashboardController controller = new DashboardController();
//        
//        controller.setDashboardService(new DashboardServiceTestImpl() {
//            @Override
//            public Tab findTabByIdEager(int id) {
//                Tab tab = new Tab();
//                tab = createTabSectionsWidgets(1);
//                tab.setId(id);
//                return tab;
//            }
//        });
//        
//        //Create a list of tabs
//        Tab t1 = new Tab();
//        t1.setId(1);
//        Tab t2 = new Tab();
//        t2.setId(2);
//        Tab t3 = new Tab();
//        t3.setId(3);
//        List<Tab> tabList = new ArrayList<Tab>();
//        tabList.add(t1);
//        tabList.add(t2);
//        tabList.add(t3);
//        
//        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
//        List<WebObject> randomized = (List<WebObject>) keyMapping.hideProperties(tabList, "id");
//        
//        String r1 = randomized.get(0).getPrimaryKey().toString();
//        String r2 = randomized.get(1).getPrimaryKey().toString();
//        String r3 = randomized.get(2).getPrimaryKey().toString();
//        
//        tabHeadings.add(new TabHeading(t1));
//        tabHeadings.add(new TabHeading(t2));
//        tabHeadings.add(new TabHeading(t3));
//        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, tabHeadings);
//        
//        //set the request parameters to the randomized id values
//        request.setParameter("tabID[]", r1);
//        request.addParameter("tabID[]", r2);
//        request.addParameter("tabID[]", r3);
//        
//        String value = controller.updateTabOrder(request, model, response);
//        assertEquals("true", value);
//        
//        tabTreeMap = (ConcurrentHashMap) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
//        
//        assertTrue(tabTreeMap.containsKey(r1));
//        assertTrue(tabTreeMap.containsKey(r2));
//        assertTrue(tabTreeMap.containsKey(r3));
//        
//        assertEquals(1, tabTreeMap.get(r1).getOrderNumber());
//        assertEquals(2, tabTreeMap.get(r2).getOrderNumber());
//        assertEquals(3, tabTreeMap.get(r3).getOrderNumber());
//        
//        request.setParameter("tabID[]", r3);
//        request.addParameter("tabID[]", r1);
//        request.addParameter("tabID[]", r2);
//        
//        controller.updateTabOrder(request, model, response);
//        
//        tabTreeMap = (ConcurrentHashMap) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
//        
//        assertEquals(2, tabTreeMap.get(r1).getOrderNumber());
//        assertEquals(3, tabTreeMap.get(r2).getOrderNumber());
//        assertEquals(1, tabTreeMap.get(r3).getOrderNumber());
//    }
//    
//    @Test
//    public void editTabTest() throws Exception {
//        
//        request.getSession(true);
//        SessionTool sessionTool = SessionTool.getInstance(request);
//        UserAccount userAccount = new UserAccount();
//        createAuthentication(userAccount);
//        
//        ConcurrentHashMap<String, Tab> tabTreeMap = new ConcurrentHashMap<String, Tab>();
//        ArrayList<TabHeading> tabHeadings = new ArrayList<TabHeading>();
//        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, tabHeadings);
//        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, tabTreeMap);
//        
//        DashboardController controller = new DashboardController();
//        
//        controller.setDashboardService(new DashboardServiceTestImpl() {
//            @Override
//            public Tab findTabByIdEager(int id) {
//                Tab tab = new Tab();
//                tab.setId(id);
//                tab.setName("Finance");
//                return tab;
//            }
//        });
//        
//        Tab tab = new Tab();
//        tab.setId(1);
//        tab.setName("Finance");
//        
//        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
//        String key = keyMapping.getRandomString(tab, "id");
//        
//        tab.setRandomId(key);
//        tabHeadings.add(new TabHeading(tab));
//        
//        request.addParameter("tabID", key);
//        request.addParameter("tabName", "My Finance");
//        
//        String value = controller.editTab(request, response);
//        assertEquals("true", value);
//        
//        tabTreeMap = (ConcurrentHashMap) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
//        
//        assertTrue(tabTreeMap.containsKey(key));
//        assertEquals("My Finance", tabTreeMap.get(key).getName());
//    }
//    
//    @Test
//    public void deleteTabTest() throws Exception {
//        
//        request.getSession(true);
//        SessionTool sessionTool = SessionTool.getInstance(request);
//        UserAccount userAccount = new UserAccount();
//        createAuthentication(userAccount);
//
//        //put map into the session
//        ConcurrentHashMap<String, Tab> tabTreeMap = new ConcurrentHashMap<String, Tab>();
//        ArrayList<TabHeading> tabHeadings = new ArrayList<TabHeading>();
//        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, tabTreeMap);
//        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, tabHeadings);
//        
//        ModelMap model = new ModelMap();
//        DashboardController controller = new DashboardController();
//        
//        controller.setDashboardService(new DashboardServiceTestImpl() {
//            @Override
//            public Tab findTabByIdEager(int id) {
//                Tab tab = new Tab();
//                tab.setId(id);
//                tab.setId(1);
//                tab.setOrderNumber(1);
//                return tab;
//            }
//        });
//        
//        Tab tab = new Tab();
//        tab.setId(1);
//        tab.setOrderNumber(1);
//        tabHeadings.add(new TabHeading(tab));
//        
//        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
//        String key = keyMapping.getRandomString(tab, "id");
//        
//        request.addParameter("id", key);
//        String responseStr = controller.deleteTab(request, model, response);
//        assertEquals("true", responseStr);
//        
//        tabTreeMap = (ConcurrentHashMap) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
//        
//        assertTrue(tabTreeMap.containsKey(key));
//        assertTrue(tabTreeMap.get(key).isToBeDeleted());
//        
//    }
//    
//    @Test
//    public void cancelDashboardTest() throws Exception {
//        
//        UserAccount userAccount = new UserAccount();
//        createAuthentication(userAccount);
//        
//        MockHttpServletRequest request = new MockHttpServletRequest();
//        request.getSession(true);
//        MockHttpServletResponse response = new MockHttpServletResponse();
//        
//        ConcurrentHashMap<String, Tab> tabTreeMap = new ConcurrentHashMap<String, Tab>();
//        ArrayList<TabHeading> tabHeadings = new ArrayList<TabHeading>();
//        
//        final SessionTool sessionTool = SessionTool.getInstance(request);
//        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
//        DashboardController controller = new DashboardController() {
//            @Override
//            protected List<TabHeading> getTabs(HttpServletRequest request, int id) {
//                List<TabHeading> tabHeadings = new ArrayList<TabHeading>();
//                RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
//                Tab tab = new Tab();
//                tab.setId(516482);
//                tab.setRandomId(keyMapping.getRandomString(tab, "id"));
//                tabHeadings.add(new TabHeading(tab));
//                return tabHeadings;
//            }
//        };
//        
//        //new temporary tab
//        Tab tab = new Tab();
//        tab.setId(0);
//        tab.setRandomId(DashboardUtil.getTemporaryId(WebCoreConstants.TEMP_TAB_ID_PREFIX));
//        tab.setRandomId(keyMapping.getRandomString(tab, "randomId"));
//        tabHeadings.add(new TabHeading(tab));
//        tabTreeMap.put(tab.getRandomId(), tab);
//        
//        //tab from the database
//        tab = new Tab();
//        tab.setId(516482);
//        tab.setRandomId(keyMapping.getRandomString(tab, "id"));
//        tabHeadings.add(new TabHeading(tab));
//        tabTreeMap.put(tab.getRandomId(), tab);
//        
//        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, tabHeadings);
//        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, tabTreeMap);
//        
//        assertEquals(2, tabHeadings.size());
//        
//        String reply = controller.reloadDashboard(request, response);
//        assertEquals("true", reply);
//        
//        tabTreeMap = (ConcurrentHashMap) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
//        tabHeadings = (ArrayList) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_HEADINGS);
//        
//        assertEquals(1, tabHeadings.size());
//        assertTrue(tabTreeMap.isEmpty());
//    }
//    
//    @Test
//    public void resetDashboardTest() throws Exception {
//        
//        UserAccount userAccount = new UserAccount();
//        userAccount.setUserDefaultDashboard(new UserDefaultDashboard());
//        createAuthentication(userAccount);
//        
//        MockHttpServletRequest request = new MockHttpServletRequest();
//        request.getSession(true);
//        MockHttpServletResponse response = new MockHttpServletResponse();
//        
//        ConcurrentHashMap<String, Tab> tabTreeMap = new ConcurrentHashMap<String, Tab>();
//        ArrayList<TabHeading> tabHeadings = new ArrayList<TabHeading>();
//        SessionTool sessionTool = SessionTool.getInstance(request);
//        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
//        
//        ModelMap model = new ModelMap();
//        DashboardController controller = new DashboardController() {
//            @Override
//            protected List<TabHeading> getTabs(HttpServletRequest request, int id) {
//                if (id == 2) {
//                    //return default tabs
//                    Tab t1 = createTabSectionsWidgets(1);
//                    Tab t2 = createTabSectionsWidgets(2);
//                    Tab t3 = createTabSectionsWidgets(3);
//                    t1.setName("default");
//                    t2.setName("default");
//                    t3.setName("default");
//                    List<TabHeading> tabHeadings = new ArrayList<TabHeading>();
//                    tabHeadings.add(new TabHeading(t1));
//                    tabHeadings.add(new TabHeading(t2));
//                    tabHeadings.add(new TabHeading(t3));
//                    return tabHeadings;
//                } else {
//                    //return users tabs
//                    Tab t1 = createTabSectionsWidgets(111);
//                    Tab t2 = createTabSectionsWidgets(222);
//                    Tab t3 = createTabSectionsWidgets(333);
//                    t1.setName("custom");
//                    t2.setName("custom");
//                    t3.setName("custom");
//                    List<TabHeading> tabHeadings = new ArrayList<TabHeading>();
//                    tabHeadings.add(new TabHeading(t1));
//                    tabHeadings.add(new TabHeading(t2));
//                    tabHeadings.add(new TabHeading(t3));
//                    return tabHeadings;
//                }
//            }
//        };
//        controller.setDashboardService(new DashboardServiceTestImpl() {
//            @Override
//            public void deleteTabsByUserId(int id) {
//                //delete users tabs from database
//            }
//        });
//        
//        controller.setUserAccountService(new UserAccountServiceImpl() {
//            @Override
//            public void updateUserAccount(UserAccount userAccount) {
//                //delete users tabs from database
//            }
//        });
//        
//        WebSecurityUtil util = new WebSecurityUtil();
//        util.setUserAccountService(createStaticUserAccountService());
//        EntityServiceImpl serv = new EntityServiceImpl() {
//            public Object merge(Object entity) {
//                UserAccount userAccount = new UserAccount();
//                userAccount.setId(2);
//                userAccount.setUserDefaultDashboard(new UserDefaultDashboard());
//                Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
//                authorities.add(new SimpleGrantedAuthority("Admin"));
//                WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
//                user.setIsComplexPassword(true);
//                Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
//                SecurityContextHolder.getContext().setAuthentication(authentication);
//                return userAccount;
//            }
//        };
//        util.setEntityService(serv);
//        
//        //createTabSecionsWidgets populates tab.id values, simulating tabs from the database
//        Tab t1 = createTabSectionsWidgets(111);
//        Tab t2 = createTabSectionsWidgets(222);
//        Tab t3 = createTabSectionsWidgets(333);
//        t1.setRandomId(keyMapping.getRandomString(t1, "id"));
//        t2.setRandomId(keyMapping.getRandomString(t2, "id"));
//        t3.setRandomId(keyMapping.getRandomString(t3, "id"));
//        t1.setName("custom");
//        t2.setName("custom");
//        t3.setName("custom");
//        tabHeadings.add(new TabHeading(t1));
//        tabHeadings.add(new TabHeading(t2));
//        tabHeadings.add(new TabHeading(t3));
//        tabTreeMap.put(t1.getRandomId(), t1);
//        tabTreeMap.put(t2.getRandomId(), t2);
//        tabTreeMap.put(t3.getRandomId(), t3);
//        
//        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, tabHeadings);
//        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, tabTreeMap);
//        
//        String reply = controller.resetDashboard(request, response, model);
//        assertEquals("true", reply);
//        
//        tabTreeMap = (ConcurrentHashMap) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
//        tabHeadings = (ArrayList) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_HEADINGS);
//        
//        assertTrue(tabTreeMap.isEmpty());
//        assertFalse(tabHeadings.isEmpty());
//        assertEquals("default", tabHeadings.get(0).getName());
//        assertEquals("default", tabHeadings.get(1).getName());
//        assertEquals("default", tabHeadings.get(2).getName());
//    }
//    

    private void createAuthentication(UserAccount userAccount) {
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("Admin"));
        WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
        Collection<Integer> permissionList = new ArrayList<Integer>();
        permissionList.add(WebSecurityConstants.PERMISSION_VIEW_DASHBOARD);
        permissionList.add(WebSecurityConstants.PERMISSION_MANAGE_DASHBOARD);
        user.setUserPermissions(permissionList);
        
        user.setIsComplexPassword(true);
        Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    //    @Test
    //    public void setCurrentTabTest() {
    //        MockHttpServletRequest request = new MockHttpServletRequest();
    //        request.getSession(true);
    //        MockHttpServletResponse response = new MockHttpServletResponse();
    //        DashboardController controller = new DashboardController();
    //        
    //        Tab t1 = new Tab();
    //        t1.setId(1);
    //        Tab t2 = new Tab();
    //        t2.setId(2);
    //        List<Tab> tabList = new ArrayList<Tab>();
    //        tabList.add(t1);
    //        tabList.add(t2);
    //        
    //        SessionTool sessionTool = SessionTool.getInstance(request);
    //        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
    //        List<WebObject> randomized = (List<WebObject>) keyMapping.hideProperties(tabList, "id");
    //        
    //        t1.setRandomId(randomized.get(0).getPrimaryKey().toString());
    //        t2.setRandomId(randomized.get(1).getPrimaryKey().toString());
    //        
    //        ArrayList<TabHeading> tabHeadings = new ArrayList<TabHeading>();
    //        tabHeadings.add(new TabHeading(t1));
    //        tabHeadings.add(new TabHeading(t2));
    //        
    //        tabHeadings.get(0).setCurrent("true");
    //        
    //        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_HEADINGS, tabHeadings);
    //        request.addParameter("tabID", t2.getRandomId());
    //        
    //        assertTrue(tabHeadings.get(0).getCurrent().equals("true"));
    //        assertTrue(tabHeadings.get(1).getCurrent().equals("false"));
    //        
    //        String result = controller.setCurrentTab(request, response);
    //        assertEquals("true", result);
    //        assertTrue(tabHeadings.get(0).getCurrent().equals("false"));
    //        assertTrue(tabHeadings.get(1).getCurrent().equals("true"));
    //    }
    //    
    @Test
    public void test_editDashboard_withSession() {
        request.getSession(true);
        SessionTool sessionTool = SessionTool.getInstance(request);
        
        WebSecurityUtil util = new WebSecurityUtil();
        util.setUserAccountService(createStaticUserAccountService());
        EntityServiceImpl serv = new EntityServiceImpl() {
            public Object merge(Object entity) {
                UserAccount userAccount = new UserAccount();
                userAccount.setId(2);
                userAccount.setUserDefaultDashboard(new UserDefaultDashboard());
                Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
                authorities.add(new SimpleGrantedAuthority("Admin"));
                WebUser user = new WebUser(2, 2, "testUser", "password", "salt", true, authorities);
                user.setIsComplexPassword(true);
                Authentication authentication = new UsernamePasswordAuthenticationToken(user, "password", authorities);
                SecurityContextHolder.getContext().setAuthentication(authentication);
                return userAccount;
            }
        };
        util.setEntityService(serv);
        
        UserAccount userAccount = new UserAccount();
        userAccount.setId(1);
        createAuthentication(userAccount);
        
        DashboardController controller = new DashboardController();
        ModelMap model = new ModelMap();
        String result = controller.editDashboard(request, response, model);
        
        assertEquals(result, "true");
        assertNotNull(sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP));
    }
    
    @Test
    public void clearColumn() {
        DashboardController controller = new DashboardController();
        
        UserAccount userAccount = new UserAccount();
        userAccount.setUserDefaultDashboard(new UserDefaultDashboard());
        createAuthentication(userAccount);
        
        /*
         * For the clearColumn.html which will delete all widgets within the specified columns. The query string will be via a Get request:
         * sectionID= "currentSectionID"
         * tabID= "currentTabID"
         * colID[] = "array of colum 'ids'" for example colID[]=1&colID[]=2
         */
        request.getSession(true);
        SessionTool sessionTool = SessionTool.getInstance(request);
        
        // No parameter.
        String result = controller.clearColumn(request, response);
        assertEquals("false", result);
        // No Tab
        request.setParameter("sectionID", "TestSection1");
        result = controller.clearColumn(request, response);
        assertEquals("false", result);
        
        request.setParameter("tabID", "TestTab1");
        
        // No id, no valid id.
        request.setParameter("colID[]", "");
        result = controller.clearColumn(request, response);
        assertEquals("false", result);
        
        String colId = null;
        request.setParameter("colID[]", colId);
        result = controller.clearColumn(request, response);
        assertEquals("false", result);
        
        // sectionID=TestSection1&tabID=TestTab1&colID[]=1&colID[]=2
        request = new MockHttpServletRequest();
        request.getSession(true);
        request.setParameter("sectionID", "TestSection1");
        request.setParameter("tabID", "TestTab1");
        request.addParameter("colID[]", "1");
        request.addParameter("colID[]", "2");
        
        Tab tab = new Tab();
        tab.setRandomId("TestTab1");
        Section sec = createColumnsMap();
        sec.setRandomId("TestSection1");
        tab.addSection(sec);
        ArrayList<Section> secs = new ArrayList<Section>();
        secs.add(sec);
        tab.setSections(secs);
        HashMap<String, Tab> map = new HashMap<String, Tab>();
        map.put("TestTab1", tab);
        
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, map);
        
        result = controller.clearColumn(request, response);
        assertEquals("true", result);
        
        map = (HashMap<String, Tab>) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        tab = map.get("TestTab1");
        assertEquals(0, tab.getSection("TestSection1").getSectionColumn("1").size());
        assertEquals(0, tab.getSection("TestSection1").getSectionColumn("2").size());
        assertEquals(2, tab.getSection("TestSection1").getSectionColumn("0").size());
    }
    
    private Section createColumnsMap() {
        Widget w1 = new Widget();
        w1.setId(1);
        w1.setColumnId(0);
        
        Widget w2 = new Widget();
        w2.setId(2);
        w2.setColumnId(0);
        // --------------------------------
        Widget w3 = new Widget();
        w3.setId(3);
        w3.setColumnId(1);
        // --------------------------------
        Widget w4 = new Widget();
        w4.setId(4);
        w4.setColumnId(2);
        
        Widget w5 = new Widget();
        w5.setId(5);
        w5.setColumnId(2);
        
        Widget w6 = new Widget();
        w6.setId(6);
        w6.setColumnId(2);
        // --------------------------------
        
        ArrayList<Widget> ws = new ArrayList<Widget>();
        ws.add(w1);
        ws.add(w2);
        ws.add(w3);
        ws.add(w4);
        ws.add(w5);
        ws.add(w6);
        
        Section se = new Section();
        se.setId(1);
        se.setWidgets(ws);
        se.setLayoutId(2);
        
        se.prepareSectionColumns();
        return se;
    }
    
    @Test
    public void editSection() {
        /*
         * For the editSection.html in GET request query string:
         * layoutID= "newLayoutID"
         * sectionID= "currentSectionID"
         * tabID= "currentTabID"
         */
        UserAccount userAccount = new UserAccount();
        userAccount.setUserDefaultDashboard(new UserDefaultDashboard());
        createAuthentication(userAccount);
        
        request.getSession(true);
        SessionTool sessionTool = SessionTool.getInstance(request);
        
        // sectionID=TestSection1&tabID=TestTab1&layoutID=0
        request.setParameter("sectionID", "TestSection1");
        request.setParameter("tabID", "TestTab1");
        request.setParameter("layoutID", "0");
        
        Tab tab = new Tab();
        tab.setRandomId("TestTab1");
        Section sec = createColumnsMap();
        sec.setRandomId("TestSection1");
        tab.addSection(sec);
        ArrayList<Section> secs = new ArrayList<Section>();
        secs.add(sec);
        tab.setSections(secs);
        HashMap<String, Tab> map = new HashMap<String, Tab>();
        map.put("TestTab1", tab);
        
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, map);
        
        // Ensures layoutId = 2 before testing
        map = (HashMap) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        assertEquals(2, map.get("TestTab1").getSection("TestSection1").getLayoutId());
        
        DashboardController controller = new DashboardController();
        String responseStr = controller.editSection(request, response);
        assertEquals("true", responseStr);
        
        map = (HashMap) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        assertEquals(0, map.get("TestTab1").getSection("TestSection1").getLayoutId());
    }
    
    @Test
    public void updateSectionOrder() {
        UserAccount userAccount = new UserAccount();
        userAccount.setUserDefaultDashboard(new UserDefaultDashboard());
        createAuthentication(userAccount);
        
        /*
         * Update for the sectionOrder.html the request will be sending via POST
         * tabID = "current Tab ID"
         * section[] = "Array of Section ID's in order of placement on the page"
         */
        Tab tab = new Tab();
        tab.setRandomId("TestTab1");
        
        Section sec1 = createColumnsMap();
        sec1.setRandomId("TestSection1");
        sec1.setOrderNumber(1);
        
        Section sec2 = createColumnsMap();
        sec2.setRandomId("TestSection2");
        sec2.setOrderNumber(2);
        
        Section sec3 = createColumnsMap();
        sec3.setRandomId("TestSection3");
        sec3.setOrderNumber(3);
        
        tab.addSection(sec1);
        tab.addSection(sec2);
        tab.addSection(sec3);
        ArrayList<Section> secs = new ArrayList<Section>();
        secs.add(sec1);
        secs.add(sec2);
        secs.add(sec3);
        tab.setSections(secs);
        HashMap<String, Tab> map = new HashMap<String, Tab>();
        map.put("TestTab1", tab);
        
        request.getSession(true);
        SessionTool sessionTool = SessionTool.getInstance(request);
        
        request.setParameter("tabID", "TestTab1");
        request.addParameter("section[]", "TestSection3");
        request.addParameter("section[]", "TestSection1");
        request.addParameter("section[]", "TestSection2");
        
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, map);
        
        // Ensures section order numbers start from TestSection1, TestSection2 to TestSection3 before testing
        map = (HashMap) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        assertEquals(1, map.get("TestTab1").getSection("TestSection1").getOrderNumber());
        assertEquals(2, map.get("TestTab1").getSection("TestSection2").getOrderNumber());
        assertEquals(3, map.get("TestTab1").getSection("TestSection3").getOrderNumber());
        
        DashboardController controller = new DashboardController();
        String responseStr = controller.updateSectionOrder(request, response);
        assertEquals("true", responseStr);
        
        map = (HashMap) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        assertEquals(1, map.get("TestTab1").getSection("TestSection3").getOrderNumber());
        assertEquals(2, map.get("TestTab1").getSection("TestSection1").getOrderNumber());
        assertEquals(3, map.get("TestTab1").getSection("TestSection2").getOrderNumber());
    }
    
    @Test
    public void addSection() {
        // Ajax /dashboard/addSection.html?id=<TabID> returns /dashboard/include/tabSection.vm.
        
        UserAccount userAccount = new UserAccount();
        userAccount.setUserDefaultDashboard(new UserDefaultDashboard());
        createAuthentication(userAccount);
        
        Tab tab = new Tab();
        tab.setId(123);
        
        SessionTool sessionTool = SessionTool.getInstance(request);
        RandomKeyMapping keyMapping = sessionTool.getKeyMapping();
        
        String key = keyMapping.getRandomString(tab, "id");
        tab.setRandomId(key);
        request.setParameter("id", key);
        
        userAccount.setId(1);
        tab.setUserAccount(userAccount);
        ArrayList<Section> secs = new ArrayList<Section>();
        tab.setSections(secs);
        
        HashMap<String, Tab> map = new HashMap<String, Tab>();
        map.put(tab.getRandomId(), tab);
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, map);
        
        DashboardController controller = new DashboardController();
        ModelMap model = new ModelMap();
        
        String path = controller.addSection(request, response, model);
        assertNotNull(path);
        assertEquals("/dashboard/include/tabSection", path);
        assertNotNull(model.get("section"));
        
        map = (HashMap) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        assertNotNull(map);
        assertNotNull(map.get(key));
        assertEquals(1, map.get(key).getSections().size());
        
    }
    
    @Test
    public void deleteSection() {
        UserAccount userAccount = new UserAccount();
        userAccount.setUserDefaultDashboard(new UserDefaultDashboard());
        createAuthentication(userAccount);
        
        request.getSession(true);
        SessionTool sessionTool = SessionTool.getInstance(request);
        
        Section sec1 = new Section();
        sec1.setRandomId("TestSec1");
        
        Section sec2 = new Section();
        sec2.setRandomId("TestSec2");
        
        List<Section> list = new ArrayList<Section>();
        list.add(sec1);
        list.add(sec2);
        
        Tab tab = new Tab();
        tab.setRandomId("TestTab");
        tab.setSections(list);
        tab.addSectionToMap(sec1);
        tab.addSectionToMap(sec2);
        
        request.setParameter("sectionID", "TestSec1");
        request.setParameter("tabID", "TestTab");
        
        HashMap<String, Tab> map = new HashMap<String, Tab>();
        map.put(tab.getRandomId(), tab);
        sessionTool.setAttribute(WebCoreConstants.SESSION_TAB_TREEMAP, map);
        
        DashboardController controller = new DashboardController();
        controller.deleteSection(request, response);
        
        map = (HashMap) sessionTool.getAttribute(WebCoreConstants.SESSION_TAB_TREEMAP);
        assertNotNull(map.get(tab.getRandomId()));
        assertEquals(1, map.get(tab.getRandomId()).getSections().size());
        assertEquals("TestSec2", map.get(tab.getRandomId()).getSection("TestSec2").getRandomId());
    }
    
    private UserAccountService createStaticUserAccountService() {
        return new UserAccountServiceImpl() {
            public UserAccount findUserAccount(int userAccountId) {
                UserAccount ua = new UserAccount();
                ua.setId(2);
                return ua;
            }
        };
    }
}
