Narrative:
In order to test the switching of customer timezone in the session
As a customer admin user
I want to login as a parent customer and switch to child company and ensure that the
timezone in the session is that of the child company. When the company is switched back to
the parent company the timezone should be updated correctly.

Given there exists a Customer where the
Customer name is Impark Parking
Is a Parent Company
Current Time Zone is Canada/Pacific
Account Status is enabled
And there exists a customer where the
Customer name is Impark Chicago
Parent company name is Impark Parking
Is not a Parent Company
Current Time Zone is US/Central
Account status is enabled
And there is a Impark Parking user Bob@Impark
And I logged in as Bob@Impark
When I switch from company Impark Parking to Impark Chicago
Then the company is switched successfully where the
Active Time Zone in the session is US/Central
When I switch from company Impark Chicago to Impark Parking
Then the company is switched successfully where the
Active Time Zone in the session is Canada/Pacific
When I switch from company Impark Parking to Impark Chicago
Then the company is switched successfully where the
Active Time Zone in the session is US/Central