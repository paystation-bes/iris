Narrative:
When a purchase is processed, the pay station transaction details shows 
appropriate calculations in respect to if refund slip is activated or not

Scenario: Cash purchase with refund slip not activated
Given there exists a customer where the 
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a location where the 
location name is Downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
location is Downtown
pay station type is LUKE II
And there exists a pay station setting where the
name is Student Parking
And there exists a purchase where the
purchase date is 02/01/2015 10:00
pay station setting is Student Parking
location is Downtown
customer name is Mackenzie Parking
serial number is 500000070001
transaction type is regular
payment type is cash
original amount is 300
charged amount is 300
cash paid amount is 1000
excess payment amount is 700
is not refund slip
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And Bob has permission to view paystations
And I logged in as Bob
When I view the paystation transaction details where the
serial number is 500000070001
date is 02/01/2015 10:00
type is regular
payment type is cash
amount is $3.00
Then there exists a pay station transaction report detail screen where the
date purchased is Feb 01, 2015 10:00 AM
cash paid is 10.0
charged amount is 3.0
excess payment is 7.0
refund slip is 0
payment type is cash
location is downtown
pay station name is 500000070001

Scenario: Cash purchase with refund slip activated
Given there exists a customer where the 
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a location where the 
location name is Downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
location is Downtown
pay station type is LUKE II
And there exists a pay station setting where the
name is Student Parking
And there exists a purchase where the
purchase date is 02/01/2015 10:00
pay station setting is Student Parking
location is Downtown
customer name is Mackenzie Parking
serial number is 500000070001
transaction type is regular
payment type is cash
original amount is 300
charged amount is 300
cash paid amount is 1000
excess payment amount is 0
is refund slip
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And Bob has permission to view paystations
And I logged in as Bob
When I view the paystation transaction details where the
serial number is 500000070001
date is 02/01/2015 10:00
type is regular
payment type is cash
amount is $3.00
Then there exists a pay station transaction report detail screen where the
date purchased is Feb 01, 2015 10:00 AM
cash paid is 10.0
charged amount is 3.0
excess payment is 0
refund slip is 7.0
payment type is cash
location is downtown
pay station name is 500000070001 

Scenario: Credit Card and Cash purchase with refund slip activated
Given there exists a customer where the 
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a location where the 
location name is Downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
location is Downtown
pay station type is LUKE II
And there exists a pay station setting where the
name is Student Parking
And there exists a purchase where the
purchase date is 02/01/2015 10:00
pay station setting is Student Parking
location is Downtown
customer name is Mackenzie Parking
serial number is 500000070001
transaction type is regular
payment type is cash/cc (swipe)
original amount is 700
charged amount is 700
cash paid amount is 1000
card paid amount is 500
excess payment amount is 0
is refund slip
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And Bob has permission to view paystations
And I logged in as Bob
When I view the paystation transaction details where the
serial number is 500000070001
date is 02/01/2015 10:00
type is regular
payment type is cash/cc (swipe)
amount is $7.00
Then there exists a pay station transaction report detail screen where the
date purchased is Feb 01, 2015 10:00 AM
charged amount is 7.0
cash paid is 10.0
card paid amount is 5.0
excess payment is 0
refund slip is 8.0
payment type is cash/cc (swipe)
location is downtown
pay station name is 500000070001 

Scenario: Credit Card and Cash purchase with refund slip not activated
Given there exists a customer where the 
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a location where the 
location name is Downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
location is Downtown
pay station type is LUKE II
And there exists a pay station setting where the
name is Student Parking
And there exists a purchase where the
purchase date is 02/01/2015 10:00
pay station setting is Student Parking
location is Downtown
customer name is Mackenzie Parking
serial number is 500000070001
transaction type is regular
payment type is cash/cc (swipe)
original amount is 700
charged amount is 700
cash paid amount is 1000
card paid amount is 500
excess payment amount is 800
is not refund slip
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And Bob has permission to view paystations
And I logged in as Bob
When I view the paystation transaction details where the
serial number is 500000070001
date is 02/01/2015 10:00
type is regular
payment type is cash/cc (swipe)
amount is $7.00
Then there exists a pay station transaction report detail screen where the
date purchased is Feb 01, 2015 10:00 AM
charged amount is 7.0
cash paid is 10.0
card paid amount is 5.0
excess payment is 8.0
refund slip is 0
payment type is cash/cc (swipe)
location is downtown
pay station name is 500000070001 

