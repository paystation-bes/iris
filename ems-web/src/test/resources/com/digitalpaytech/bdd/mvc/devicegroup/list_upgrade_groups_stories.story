Narrative: Testing that, when User search for Device Group, Iris will make a call to Facilities Management Server

Scenario: The target microservice is called when Iris receive the device group list request

Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
unifi id is 201
And there exists a Customer Subscription where the
Type is Online Pay Station Configuration
Customer is Mackenzie Parking
is enabled
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And Bob has permission to manage paystation settings
And Bob has permission to view paystation settings
And I logged in as Bob
And the Facility Management Service response with message 
[{
   "groupName": "name",
   "groupId": "id",
   "groupStatus": "unscheduled",
   "enableIris": true
}]
When I list upgrade groups where the
ordering is asc
Then a listUpgradeGroups request is sent to Facility Management Service

Scenario: The request is rejected when user doesn't have the permission

Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
unifi id is 201
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And I logged in as Bob
And the Facility Management Service response with message 
[{
   "groupName": "name",
   "groupId": "id",
   "groupStatus": "unscheduled",
   "enableIris": true
}]
When I list upgrade groups where the
ordering is asc
Then a listUpgradeGroups request is not sent to Facility Management Service
