Narrative:
In order to select parent location option on occupancy map widget, the master list should contain that option 

Scenario: 'Occupancy Map by Location(P)' option exists under Occupancy Map in the Master List
Given there exists a customer where the
customer name is Mackenzie Parking
account type is child
is migrated
And there exists a user account where the
user name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And Bob has permission to manage the dashboard
And I logged in as Bob
When I click the add widget button
Then the occupancy map by location(p) is visible

