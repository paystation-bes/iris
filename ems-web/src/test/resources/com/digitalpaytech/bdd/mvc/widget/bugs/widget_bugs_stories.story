Narrative:
In order to test bugs
As a developer
I want to ensure that the following bugs are still fixed: EMS-10616

Scenario: EMS-10616  Recently Added Mapped Occupancy markers to not appear on the map
Given there exists a customer where the 
Customer name is Mackenzie Parking
Is not a Parent Company
Given there exists a User Account where the
User Name is admin@Mackenzie Parking
User Account Customer is Mackenzie Parking
And admin@Mackenzie Parking has permission to view the dashboard
And there exists a Location where the 
Location Name is Airport Parking
Capacity is 10
And the Location Airport Parking has a defined parking area
And there are no active permits at Location Airport Parking
And there exists a widget where the
Name is Test
widget metric type is Occupancy Map
widget chart type is Occupancy Map
tier 1 type is Location
Customer is Mackenzie Parking
When the Occupancy Map widget Test is viewed on Mackenzie Parking dashboard
Then the Occupancy Map Markers should be displayed
And the Occupancy Map Markers should be green
