Narrative:
In order to test handling of validation while saving merchant accounts
As a SystemAdmin user
I want to create a merchant account with valid data and verify the account is created

Scenario: Unsuccessfully save a new merchant account with processor CreditCall
Given there is a SystemAdmin user Bob
And Bob has permission to add a merchant account
And I logged in as Bob
And there exists a customer where the 
Customer name is Mackenzie Parking
is migrated
Current Time Zone is Canada/Pacific
And I create a new merchant account where the
name is CreditCall
is enabled
processor is processor.creditcall
Close Time is 16
Time Zone is Canada/Eastern
Field1 is Paymentech Tampa
Field2 is creditcallmerchant
Field3 is abcdefgh
Field4 is abcd1234
Field5 is CAD
When I save the merchant account 
Then there is no merchant account where the
name is CreditCall
merchant account status is enabled
processor is processor.creditcall
Field1 is Paymentech Tampa
Field2 is creditcallmerchant
Field3 is abcdefgh
Field4 is abcd1234
Field5 is CAD
Close Time is 4
Time Zone is Canada/Eastern

Scenario: Successfully save a new merchant account with processor CreditCall
Given there is a SystemAdmin user Bob
And Bob has permission to add a merchant account
And I logged in as Bob
And there exists a customer where the 
Customer name is Mackenzie Parking
is migrated
Current Time Zone is Canada/Pacific
And I create a new merchant account where the
name is CreditCall
is enabled
processor is processor.creditcall
Close Time is 16
Time Zone is Canada/Eastern
Field1 is Paymentech Tampa
Field2 is creditcallmerchant
Field3 is 12345678
Field4 is abcd1234
Field5 is CAD
When I save the merchant account 
Then there exists a merchant account where the
name is CreditCall
merchant account status is enabled
processor is processor.creditcall
Close Time is 4
Time Zone is Canada/Eastern
When I view the merchant account details window
Then the merchant account details is shown

Scenario: Successfully save a new merchant account
Given there is a SystemAdmin user Bob
And Bob has permission to add a merchant account
And I logged in as Bob
And there exists a customer where the 
Customer name is Mackenzie Parking
is migrated
Current Time Zone is Canada/Pacific
And I create a new merchant account where the
name is TestMerchantAccount
is enabled
processor is processor.elavon
Close Time is 16
Time Zone is Canada/Eastern
Field1 is 0017340009997772229869
Field2 is 0017340009997772229869
Field3 is 10
Field4 is 1029
When I save the merchant account 
Then there exists a merchant account where the
name is TestMerchantAccount
merchant account status is enabled
processor is processor.elavon
Field1 is 0017340009997772229869
Field2 is 0017340009997772229869
Field3 is 10
Field4 is 1029
Close Time is 4
Time Zone is Canada/Eastern
When I view the merchant account details window
Then the merchant account details is shown

Scenario: Successfully save a TD Merchant Account
Given there is a SystemAdmin user Bob
And Bob has permission to add a merchant account
And I logged in as Bob
And there exists a customer where the
Customer name is Mackenzie Parking
is migrated
Current Time Zone is Canada/Pacific
And I create a new merchant account where the
name is TestTDAccount
is enabled
processor is processor.tdmerchant
Close Time is 16
Time Zone is Canada/Pacific
Field1 is 17681535
Field2 is D9231CC32Ac1431686121828E1a7e171
Field3 is CAD
When I save the merchant account
Then there exists a merchant account where the
name is TestTDAccount
merchant account status is enabled
processor is processor.tdmerchant
Field1 is 17681535
Field2 is D9231CC32Ac1431686121828E1a7e171
Field3 is CAD
Close Time is 16
Time Zone is Canada/Pacific
When I view the merchant account details window
Then the merchant account details is shown

Scenario: Successfully save a new merchant account
Given there is a SystemAdmin user Bob
And Bob has permission to add a merchant account
And I logged in as Bob
And there exists a customer where the 
Customer name is Mackenzie Parking
is migrated
Current Time Zone is Canada/Pacific
And I create a new merchant account where the
name is TestMerchantAccount
is enabled
processor is processor.elavon
Close Time is 16
Time Zone is Canada/Eastern
Field1 is 0017340009997772229869
Field2 is 0017340009997772229869
Field3 is 10
Field4 is 1029
When I save the merchant account 
Then there exists a merchant account where the
name is TestMerchantAccount
merchant account status is enabled
processor is processor.elavon
Close Time is 4
Time Zone is Canada/Eastern
Field1 is 0017340009997772229869
Field2 is 0017340009997772229869
Field3 is 10
Field4 is 1029
When I view the merchant account details window
Then the merchant account details is shown