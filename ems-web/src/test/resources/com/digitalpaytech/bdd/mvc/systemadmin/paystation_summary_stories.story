Narrative:
In order to test the Paystation Summary page
As a developer
I want to ensure that when an active alert is cleared manually then the paystation summary page is updated

Scenario:  An active Minor user defined alert is replaced with a an active Critical user defined alert
Given there is a child admin user Bob
And Bob has permission to view maintenance center
And Bob has permission to clear active alerts
And I logged in as Bob
And there exists a location where the 
Location Name is ThisIsALocation
And there exists a pay station where the  
name is 500000000001
serial number is 500000000001
is Activated
is Visible
is not Deleted
is not Decommissioned
customer is Bob
pay station type is LUKE II
Location is ThisIsALocation
Critical Pay Station Alerts Count is 1
And there exists a active alert where the 
Event Type is Printer Paper Jam
pay station is 500000000001
severity is Critical
is active
And there exists a heartbeat where the 
pay station is 500000000001
last seen is now
When I check the paystation summary page
Then there exists a pos sensor state where the
pay station is 500000000001
Printer Status is Jam
When I clear the active alert
Then there exists a pos sensor state where the 
pay station is 500000000001
Printer Status is Normal
And the response received is true

