Narrative: Testing that, when User uses autocomplete for Device Group, 
Iris will make a call to Facilities Management Server 
and response is returned un-altered

Scenario: The target microservice is called when Iris receive the autocomplete input from device group page

Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
unifi id is 201
And there exists a Customer Subscription where the
Type is Online Pay Station Configuration
Customer is Mackenzie Parking
is enabled
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And Bob has permission to manage paystation settings
And Bob has permission to view paystation settings
And I logged in as Bob
When I type string test into autocomplete input
Then the autocomplete input returns 
{
  "groupSearchResult": [
    {
      "groupId": "1",
      "groupName": "testGroupName",
      "groupStatus": "active"
    }
  ]
}
