Narrative:
In order to test permit lookups
As a CustomerAdmin user
I want to search for a valid permit using the fields provided to return the list of permit(s)

Scenario:  Using 'Date/Time' field enter a search "between dates" that span no greater than 30 days
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And Bob has permission to manage transaction reports
And I logged in as Bob
And I create a new transaction receipt search where the 
card number is 1234
date type is DATE_TIME_RANGE
start date is today
end date is tomorrow
When I click the search button
Then the result is successful

Scenario:  Using 'Date/Time' field enter a search "between dates" that spans greater than 30 days
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And Bob has permission to manage transaction reports
And I logged in as Bob
And I create a new transaction receipt search where the 
card number is 1234
date type is DATE_TIME_RANGE
start date is today
end date is next year
When I click the search button
Then the result is not successful

Scenario:  Using 'Date/Time' field enter a search "between dates" that is more than 2 years back
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And Bob has permission to manage transaction reports
And I logged in as Bob
And I create a new transaction receipt search where the 
card number is 1234
date type is DATE_TIME_RANGE
start date is 04/01/2014
end date is 04/20/2014
When I click the search button
Then the result is not successful

Scenario:  Using 'Date/Time' field enter a search "between dates" that is more than 2 years back and spans greater than 30 days
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And Bob has permission to manage transaction reports
And I logged in as Bob
And I create a new transaction receipt search where the 
card number is 1234
date type is DATE_TIME_RANGE
start date is 04/01/2014
end date is 06/06/2014
When I click the search button
Then the result is not successful
