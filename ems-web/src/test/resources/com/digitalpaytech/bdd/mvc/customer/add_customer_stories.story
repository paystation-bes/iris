Narrative:
In order to test adding a child profile associated to a parent
As a system admin user
I want to add a child profile and associate it with a parent profile 

Scenario:  Successfully create a new child Customer
Given there is a SystemAdmin user Bob
And Bob has permission to add a customer
And I logged in as Bob
And I create a new customer where the 
Customer name is Mackenzie Parking
Is not a Parent Company
Current Time Zone is Canada/Pacific
Account Status is enabled
When I save the customer
Then there exists a customer where the
Customer name is Mackenzie Parking
Is not a Parent Company
Current Time Zone is Canada/Pacific
Account Status is enabled
When I type string Mackenzie Parking into customer search bar input
Then the customer search bar auto complete returns Mackenzie Parking
When I search for Mackenzie Parking in the Customer Search Bar
Then the profile is shown in the customer profile screen where the 
Customer name is Mackenzie Parking
Time zone is Canada/Pacific
Status is enabled

Scenario:  Successfully create a new parent Customer
Given there is a SystemAdmin user Bob
And Bob has permission to add a customer
And I logged in as Bob
And I create a new customer where the 
Customer name is Mackenzie Parking Parent
Is a Parent Company
Current Time Zone is Canada/Pacific
Account Status is enabled
When I save the customer
Then there exists a customer where the 
Customer name is Mackenzie Parking Parent
When I type string Mackenzie Parking Parent into customer search bar input
Then the customer search bar auto complete returns Mackenzie Parking Parent
When I search for Mackenzie Parking Parent in the Customer Search Bar
Then the profile is shown in the customer profile screen where the
Customer name is Mackenzie Parking Parent
Time zone is Canada/Pacific
Status is enabled

Scenario:  Successfully create a new child Customer that is associated with a parent profile
Given there is a SystemAdmin user Bob
And Bob has permission to add a customer
And I logged in as Bob
And there exists a customer where the 
Customer name is Darth Parking
Is a Parent Company
Current Time Zone is Canada/Pacific
Account Status is enabled
And I create a new customer where the 
Customer name is Luke Parking
Parent Company name is Darth Parking
Is not a Parent Company
Current Time Zone is Canada/Pacific
Account Status is enabled
When I save the customer
Then there exists a customer where the 
Customer name is Luke Parking
When I type string Luke Parking into customer search bar input
Then the customer search bar auto complete returns Luke Parking (Darth Parking)
When I search for Luke Parking in the Customer Search Bar
Then the profile is shown in the customer profile screen where the
Customer name is Luke Parking,
Time zone is Canada/Pacific, and
Status is enabled
When I type string Darth Parking into customer search bar input
Then the customer search bar auto complete returns Darth Parking
When I search for Darth Parking in the Customer Search Bar
Then the profile is shown in the customer profile screen where the
Customer name is Darth Parking
Time zone is Canada/Pacific
Child Companies are Luke Parking and
Status is enabled
