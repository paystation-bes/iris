Narrative: Testing that, when User performs generate boss key, progress boss key or download boss key, Iris will make a call to Facilities Management Server

Scenario: The target microservice is called when Iris receive the request and the user has manage permissions
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
unifi id is 201
And there exists a Customer Subscription where the
Type is Online Pay Station Configuration
Customer is Mackenzie Parking
is enabled
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And Bob has permission to schedule paystation settings update
And I logged in as Bob
When I send a boss key request where the
type is bossKeyGenerate
Then a bossKeyGenerate request is sent to Facility Management Service
When I send a boss key request where the
type is bossKeyProgress
Then a bossKeyProgress request is sent to Facility Management Service
When I send a boss key request where the
type is bossKeyDownload
Then a bossKeyDownload request is sent to Facility Management Service

Scenario: The target microservice is not called when Iris receive the request and the user insufficent permissions view only
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
unifi id is 201
And there exists a Customer Subscription where the
Type is Online Pay Station Configuration
Customer is Mackenzie Parking
is enabled
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And Bob has permission to manage paystation settings
And Bob has permission to view paystation settings
And I logged in as Bob
When I send a boss key request where the
type is bossKeyGenerate
Then a bossKeyGenerate request is not sent to Facility Management Service
When I send a boss key request where the
type is bossKeyProgress
Then a bossKeyProgress request is not sent to Facility Management Service
When I send a boss key request where the
type is bossKeyDownload
Then a bossKeyDownload request is not sent to Facility Management Service

Scenario: The target microservice is not called when Iris receive the request and the user insufficent permissions manage and view
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
unifi id is 201
And there exists a Customer Subscription where the
Type is Online Pay Station Configuration
Customer is Mackenzie Parking
is enabled
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And Bob has permission to view paystation settings
And Bob has permission to manage paystation settings
And I logged in as Bob
When I send a boss key request where the
type is bossKeyGenerate
Then a bossKeyGenerate request is not sent to Facility Management Service
When I send a boss key request where the
type is bossKeyProgress
Then a bossKeyProgress request is not sent to Facility Management Service
When I send a boss key request where the
type is bossKeyDownload
Then a bossKeyDownload request is not sent to Facility Management Service