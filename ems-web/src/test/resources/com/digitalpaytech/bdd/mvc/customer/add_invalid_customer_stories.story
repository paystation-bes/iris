Narrative:
In order to test handling of validation while adding customers
As a SystemAdmin user
I want to create customer profiles with invalid/missing data and verify if the customer is created

Scenario:  Create Customer with missing Account Status
Given there is a SystemAdmin user Bob
And Bob has permission to add a customer
And I logged in as Bob
And I create a new customer where the 
admin user name is admin@mckenzie
Is not a Parent Company
Current Time Zone is Canada/Pacific
Account Status is left blank
When I save the customer
Then I receive validation errors account status is invalid and the customer is not saved

Scenario:  Create Customer with missing Legal Name
Given there is a SystemAdmin user Bob
And Bob has permission to add a customer
And I logged in as Bob
And I create a new customer where the 
customer name is left blank
Is not a Parent Company
Current Time Zone is Canada/Pacific
Account Status is enabled
When I save the customer
Then I receive validation errors Customer Legal Name is required and the customer is not saved

Scenario:  Create Customer with missing User Name
Given there is a SystemAdmin user Bob
And Bob has permission to add a customer
And I logged in as Bob
And I create a new customer where the 
admin user name is left blank
Is not a Parent Company
Current Time Zone is Canada/Pacific
Account Status is enabled
When I save the customer
Then I receive validation errors Customer User Name is required and the customer is not saved

Scenario:  Create Customer with missing Account Status
Given there is a SystemAdmin user Bob
And Bob has permission to add a customer
And I logged in as Bob
And I create a new customer where the 
admin user name is admin@mckenzieparking
Is not a Parent Company
Current Time Zone is Canada/Pacific
Account Status is left blank
When I save the customer
Then I receive validation errors Account Status is invalid and the customer is not saved

Scenario:  Create customer with non matching passwords
Given there is a SystemAdmin user Bob
And Bob has permission to add a customer
And I logged in as Bob
And I create a new customer where the 
admin user name is admin@mckenzieparking
Is not a Parent Company
Current Time Zone is Canada/Pacific
Account Status is enabled
password is Password$1
confirm password is Password$2
When I save the customer
Then I receive validation errors confirm password is incorrect and the customer is not saved

Scenario:  Create customer with unsecure passwords
Given there is a SystemAdmin user Bob
And Bob has permission to add a customer
And I logged in as Bob
And I create a new customer where the 
admin user name is admin@mckenzieparking
Is not a Parent Company
Current Time Zone is Canada/Pacific
Account Status is enabled
password is Passworddollar1
confirm password is Passworddollar1
When I save the customer
Then I receive validation errors password not complex and the customer is not saved

Scenario:  Create customer with multiple fields missing
Given there is a SystemAdmin user Bob
And Bob has permission to add a customer
And I logged in as Bob
And I create a new customer where the 
admin user name is left blank
Is not a Parent Company
Current Time Zone is Canada/Pacific
Account Status is left blank
customer name is left blank
password is Passworddollar1
confirm password is Passworddollar1
When I save the customer
Then I receive validation errors password not complex, customer legal name is required, account status is invalid, customer user name is required and the customer is not saved

Scenario:  Create customer with all fields set correctly
Given there is a SystemAdmin user Bob
And Bob has permission to add a customer
And I logged in as Bob
And I create a new customer where the 
admin user name is admin@mckenzieparking
customer name is Mckenzie Parking
Is a Parent Company
Current Time Zone is Canada/Pacific
Account Status is enabled
password is Password$1
confirm password is Password$1
When I save the customer
Then there exists a customer where the 
Customer name is Mckenzie Parking
Is a Parent Company
Current Time Zone is Canada/Pacific
Account Status is enabled
And there exists a User Account where the 
user name is admin%40mckenzieparking
password is Password$1
When I save the customer
Then I receive validation errors duplicate user name and the customer is not saved




