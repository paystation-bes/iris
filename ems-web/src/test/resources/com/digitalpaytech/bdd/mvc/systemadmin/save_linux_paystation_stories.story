Narrative:
In order to test saving a linux paystation
As a system admin user
I want to test validation of the POSServiceState.

Scenario: Change a PS to linux
Given there exists a customer where the
customer Name is Mackenzie Parking
unifi id is 1111
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
is activated
is not linux
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And I logged in as Bob
And I create a new pay station edit where the 
point of sale is 500000070001
is billable
is linux
location is downtown
customer is Mackenzie Parking
When I save the paystation 
Then there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
is linux
And the message is sent to kafka's topic 'User Message MitreId'
And the message is sent to kafka's topic 'Device Update LDCMS'
And a createDevice request is sent to Telemetry Service

Scenario: Change a PS to linux when PS is already linux
Given there exists a customer where the
customer Name is Mackenzie Parking
unifi id is 1111
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
is activated
is linux
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And I logged in as Bob
And I create a new pay station edit where the 
point of sale is 500000070001
is billable
is linux
location is downtown
customer is Mackenzie Parking
When I save the paystation 
Then there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
is linux
And a createDevice request is not sent to Telemetry Service
!-- And the message is not sent to kafka's topic 'User Message MitreId'
!-- And the message is not sent to kafka's topic 'Device Update LDCMS'

Scenario: recieve is linux false when PS is already linux
Given there exists a customer where the
customer Name is Mackenzie Parking
unifi id is 1111
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
is activated
is linux
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And I logged in as Bob
And I create a new pay station edit where the 
point of sale is 500000070001
is billable
is not linux
location is downtown
customer is Mackenzie Parking
When I save the paystation 
Then there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
is linux
!-- And a createDevice request is sent to Telemetry Service


Scenario: recieve is linux false when PS is not linux
Given there exists a customer where the
customer Name is Mackenzie Parking
unifi id is 1111
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
is activated
is not linux
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And I logged in as Bob
And I create a new pay station edit where the 
point of sale is 500000070001
is billable
is not linux
location is downtown
customer is Mackenzie Parking
When I save the paystation 
Then there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
is not linux
And a createDevice request is not sent to Telemetry Service