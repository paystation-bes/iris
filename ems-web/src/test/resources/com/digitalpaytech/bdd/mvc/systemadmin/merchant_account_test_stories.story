Narrative:
In order to test validating a merchant account
As a system admin user
I want to test validation of various merchant accounts as desired.

Scenario:  Asserting the failure message while testing the alliance processor
Given there exists a customer where the 
customer name is vic parking
And there is a SystemAdmin user Bob
And Bob has permission to add a customer
And Bob has permission to edit a customer
And I logged in as Bob
And there exists a merchant account where the 
customer is vic parking
processor is processor.alliance
merchant account status is enabled
name is testmerchant
Field1 is DUMMY
Field2 is TESTDGPT0101
When I test the merchant account where the
name is testmerchant
Then the response received is false:Empty response.

Scenario:  Asserting that TD Merchant services can be tested by the systemadmin
Given there exists a customer where the 
customer name is vic parking
And there is a SystemAdmin user Bob
And Bob has permission to add a customer
And Bob has permission to edit a customer
And I logged in as Bob
And there exists a merchant account where the 
customer is vic parking
processor is processor.tdmerchant
merchant account status is enabled
name is testtdmerchant
Field1 is 17681535
Field2 is D9231CC32Ac1431686121828E1a7e171
When I test the merchant account where the
name is testtdmerchant
Then the response received is true