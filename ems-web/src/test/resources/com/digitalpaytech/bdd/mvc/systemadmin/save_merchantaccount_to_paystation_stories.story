Narrative:
In order to test saving a merchant account to pay station
As a system admin user
I want to test validation of the POSServiceState.

Scenario: Update an existing credit card merchant account with a new one to Pay station
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a merchant account where the  
name is MonerisMerchant
customer is Mackenzie Parking
processor is processor.moneris
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is CAD
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
is activated
is not linux
And there exists a merchant account where the
name is AllianceMerchant
customer is Mackenzie Parking
processor is processor.alliance
field1 is 1234556
field2 is 123455612345561234556
And there exists a merchant pos where the
name is AllianceMerchant
pay station is 500000070001
card type is credit card
is not deleted
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And I logged in as Bob
And I create a new pay station edit where the 
point of sale is 500000070001
is not linux
is billable
credit card merchant account is MonerisMerchant
location is downtown
customer is Mackenzie Parking
When I save the paystation 
Then there exists a merchant pos where the 
name is MonerisMerchant
pay station is 500000070001
card type is credit card
is not deleted

Scenario: Unassign a credit card merchant account from Pay station
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a merchant account where the  
name is TDMerchant
customer is Mackenzie Parking
processor is processor.tdmerchant
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is CAD
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
is activated
is not linux
And there exists a merchant pos where the
name is TDMerchant
pay station is 500000070001
card type is credit card
is not deleted
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And I logged in as Bob
And I create a new pay station edit where the 
point of sale is 500000070001
is not linux
is billable
credit card merchant account is null
location is downtown
customer is Mackenzie Parking
is deleted
When I save the paystation
Then there exists a merchant pos where the
name is TDMerchant
pay station is 500000070001
is deleted

Scenario: Assigning a new credit card merchant account to Pay station
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a merchant account where the  
name is Elavon
customer is Mackenzie Parking
processor is processor.elavon
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is CAD
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
is activated
is not linux
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And I logged in as Bob
And I create a new pay station edit where the 
point of sale is 500000070001
is not linux
is billable
credit card merchant account is Elavon
location is downtown
customer is Mackenzie Parking
When I save the paystation 
Then there exists a merchant pos where the
card type is credit card
is not deleted
pay station is 500000070001
name is Elavon

Scenario: Re-Assigning a new credit card merchant account to Pay station
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a merchant account where the  
name is Elavon
customer is Mackenzie Parking
processor is processor.elavon
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is CAD
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
is activated
is not linux
And there exists a merchant pos where the
name is Elavon
pay station is 500000070001
card type is credit card
is deleted
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And I logged in as Bob
And I create a new pay station edit where the 
point of sale is 500000070001
is not linux
is billable
credit card merchant account is Elavon
location is downtown
customer is Mackenzie Parking
When I save the paystation 
Then there exists a merchant pos where the
card type is credit card
is not deleted
pay station is 500000070001
name is Elavon

Scenario: Saving a EMV merchant account to POS
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a merchant account where the  
name is EmvMerchant
customer is Mackenzie Parking
processor is processor.creditcall
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is CAD
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
is activated
is not linux
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And I logged in as Bob
And I create a new pay station edit where the 
point of sale is 500000070001
is not linux
is billable
credit card merchant account is EmvMerchant
location is downtown
customer is Mackenzie Parking
When I save the paystation 
Then there exists a pos service state where the
point of sale is 500000070001
is new merchant account
merchant account changed GMT greater than 3 minute ago
merchant account requested gmt is null
merchant account uploaded gmt is null

Scenario: Saving a merchant account to POS that is not EMV
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a merchant account where the  
name is Elavon
customer is Mackenzie Parking
processor is processor.elavon
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is CAD
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
is activated
is not linux
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And I logged in as Bob
And I create a new pay station edit where the 
point of sale is 500000070001
is not linux
is billable
credit card merchant account is Elavon
location is downtown
customer is Mackenzie Parking
When I save the paystation 
Then there exists a pos service state where the
point of sale is 500000070001
is not new merchant account
merchant account changed GMT is null
merchant account requested gmt is null
merchant account uploaded gmt is null


Scenario: Assigning an EMV merchant account to a Pay station that is already assigned to a Non-EMV merchant account
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a merchant account where the  
name is Elavon
customer is Mackenzie Parking
processor is processor.elavon
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is CAD
And there exists a merchant account where the  
name is EmvMerchant
customer is Mackenzie Parking
processor is processor.creditcall
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is CAD
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
Credit card Merchant Account is Elavon
is activated
is not linux
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And I logged in as Bob
And I create a new pay station edit where the 
point of sale is 500000070001
is not linux
is billable
credit card merchant account is EmvMerchant
location is downtown
customer is Mackenzie Parking
When I save the paystation 
Then there exists a pos service state where the
point of sale is 500000070001
is new merchant account
merchant account changed GMT greater than 3 minute ago
merchant account requested gmt is null
merchant account uploaded gmt is null

Scenario: Assigning an non-EMV merchant account to a Pay station that is already assigned to an EMV merchant account
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a merchant account where the  
name is Elavon
customer is Mackenzie Parking
processor is processor.elavon
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is CAD
And there exists a merchant account where the  
name is EmvMerchant
customer is Mackenzie Parking
processor is processor.creditcall
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is CAD
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
Credit card Merchant Account is EmvMerchant
is activated
is not linux
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And I logged in as Bob
And I create a new pay station edit where the 
point of sale is 500000070001
is not linux
is billable
credit card merchant account is Elavon
location is downtown
customer is Mackenzie Parking
When I save the paystation 
Then there exists a pos service state where the
point of sale is 500000070001
is not new merchant account
merchant account changed GMT is null
merchant account requested gmt is null
merchant account uploaded gmt is null

Scenario: Assigning a new EMV merchant account to a Pay station that is already assigned to an EMV merchant account
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a merchant account where the  
name is EmvMerchant
customer is Mackenzie Parking
processor is processor.creditcall
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is CAD
And there exists a merchant account where the  
name is EmvMerchant2
customer is Mackenzie Parking
processor is processor.creditcall
field1 is 117681536
field2 is D9231CC32Ac1431686121828E1a7e172
field3 is CAD
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
Credit card Merchant Account is EmvMerchant
is activated
is not linux
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And I logged in as Bob
And I create a new pay station edit where the 
point of sale is 500000070001
is not linux
is billable
credit card merchant account is EmvMerchant2
location is downtown
customer is Mackenzie Parking
When I save the paystation 
Then there exists a pos service state where the
point of sale is 500000070001
is new merchant account
merchant account changed GMT greater than 3 minute ago
merchant account requested gmt is null
merchant account uploaded gmt is null

Scenario: Assigning an non-EMV merchant account to a Pay station that is already assigned to a non-EMV merchant account
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a merchant account where the  
name is Elavon
customer is Mackenzie Parking
processor is processor.elavon
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is CAD
And there exists a merchant account where the  
name is MonerisMerchant
customer is Mackenzie Parking
processor is processor.moneris
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is CAD
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
Credit card Merchant Account is Elavon
is activated
is not linux
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And I logged in as Bob
And I create a new pay station edit where the 
point of sale is 500000070001
is not linux
is billable
credit card merchant account is MonerisMerchant
location is downtown
customer is Mackenzie Parking
When I save the paystation 
Then there exists a pos service state where the
point of sale is 500000070001
is not new merchant account
merchant account changed GMT is null
merchant account requested gmt is null
merchant account uploaded gmt is null

Scenario: Deleting an EMV merchant account from POS
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a merchant account where the  
name is EmvMerchant
customer is Mackenzie Parking
processor is processor.creditcall
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is CAD
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
Credit card Merchant Account is EmvMerchant
is activated
is not linux
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And I logged in as Bob
And I create a new pay station edit where the 
point of sale is 500000070001
is deleted
is not linux
is billable
location is downtown
customer is Mackenzie Parking
When I save the paystation 
Then there exists a pos service state where the
point of sale is 500000070001
is not new merchant account
merchant account changed GMT is null
merchant account requested gmt is null
merchant account uploaded gmt is null

Scenario: Deleting a non-EMV merchant account from POS
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a merchant account where the  
name is Elavon
customer is Mackenzie Parking
processor is processor.elavon
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is CAD
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
Credit card Merchant Account is Elavon
is activated
is not linux
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And I logged in as Bob
And I create a new pay station edit where the 
point of sale is 500000070001
is deleted
is not linux
is billable
location is downtown
customer is Mackenzie Parking
When I save the paystation 
Then there exists a pos service state where the
point of sale is 500000070001
is not new merchant account
merchant account changed GMT is null
merchant account requested gmt is null
merchant account uploaded gmt is null

Scenario: Disabling an assigned EMV merchant account
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a merchant account where the  
name is EmvMerchant
customer is Mackenzie Parking
processor is processor.creditcall
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is 12345678
field4 is 87654321
field5 is testtest
merchant account status is Enabled
terminal token is terminaltoken
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
Credit card Merchant Account is EmvMerchant
is activated
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And Bob has permission to edit a customer
And I logged in as Bob
And I create a new merchant account where the
customer is Mackenzie Parking
processor is processor.creditcall
is not Enabled
Close Time is 1
Time Zone is Canada/Pacific
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is 12345678
field4 is 87654321
field5 is testtest
name is EmvMerchant
When I save the merchant account 
Then there exists a pos service state where the
point of sale is 500000070001
is new merchant account
merchant account changed GMT greater than 3 minute ago
merchant account requested gmt is null
merchant account uploaded gmt is null

Scenario: Enabling an assigned EMV merchant account
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a merchant account where the  
name is EmvMerchant
customer is Mackenzie Parking
processor is processor.creditcall
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is 12345678
field4 is 87654321
field5 is testtest
merchant account status is Disabled
terminal token is terminaltoken
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
Credit card Merchant Account is EmvMerchant
is activated
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And Bob has permission to edit a customer
And I logged in as Bob
And I create a new merchant account where the
customer is Mackenzie Parking
processor is processor.creditcall
is Enabled
Close Time is 1
Time Zone is Canada/Pacific
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is 12345678
field4 is 87654321
field5 is testtest
name is EmvMerchant
When I save the merchant account 
Then there exists a pos service state where the
point of sale is 500000070001
is new merchant account
merchant account changed GMT greater than 3 minute ago
merchant account requested gmt is null
merchant account uploaded gmt is null

Scenario: Deleting an assigned EMV merchant account
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a merchant account where the  
name is EmvMerchant
customer is Mackenzie Parking
processor is processor.creditcall
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is CAD
merchant account status is Disabled
terminal token is terminaltoken
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
Credit card Merchant Account is EmvMerchant
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And Bob has permission to edit a customer
And I logged in as Bob
When I delete the merchant account where the
name is EmvMerchant
Then there exists a pos service state where the
point of sale is 500000070001
is new merchant account
merchant account changed GMT greater than 3 minute ago
merchant account requested gmt is null
merchant account uploaded gmt is null