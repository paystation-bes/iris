Narrative:
In order to test permissions of a child admin
As a child admin user
I want to ensure that the child admin has the abilities claimed by the permissions the user account is associated with

Scenario: Child Admin with Clear Active Alerts can access the Maintenance tab to clear active paystation alerts. Removed a when-then because 'findLocationsWithPointOfSalesByCustomerId' uses sql syntax that isn't supported in HSQL.

Given there exists a permission where the
permission name is clear active alerts
Id is 204
And there exists a location where the 
location Name is Downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
location is Downtown
pay station type is LUKE II
major Pay Station Alerts Count is 1
And there exists a heartbeat where the 
pay station is 500000070001
last seen is 2 hours ago
And there exists a active alert where the 
event type is Paper Status
pay station is 500000070001
severity is Critical
is Active
action type is alerted
And there exists a route where the 
name is Route201
customer is Mackenzie Parking
And there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And Bob has permission to clear active alerts
And I logged in as Bob
When I view the pay station alerts where the
Serial number is 500000070001
Then the pay station active alerts is shown
When I clear the active alerts where the
Serial number is 500000070001
Then the active alerts is cleared

Scenario:  Child Admin with 'View Maintenance Center' has access to view the Maintenance Center but cannot clear active alerts. Removed a when-then because 'findLocationsWithPointOfSalesByCustomerId' uses sql syntax that isn't supported in HSQL.

Given there exists a permission where the
permission name is View Maintenance Center
Id is 203
And there exists a location where the 
location Name is Downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
location is Downtown
pay station type is LUKE II
major Pay Station Alerts Count is 1
And there exists a heartbeat where the 
pay station is 500000070001
last seen is 2 hours ago
And there exists a active alert where the 
event type is Paper Status
pay station is 500000070001
severity is Major
is Active
And there exists a route where the 
name is Route201
customer is Mackenzie Parking
And there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And Bob has permission to view maintenance Center
And I logged in as Bob
When I view the pay station alerts where the
Serial number is 500000070001
Then the pay station active alerts is shown
When I clear the active alerts where the
Serial number is 500000070001
Then the active alerts is not cleared

Scenario: Child Admin has no permission to View Maintenance Center, cannot view maintenance tab

Given there exists a customer where the
Customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And I logged in as Bob
When I view the maintenance tab
Then the maintenance tab is not shown

Scenario: Child Admin has no permissions to clear active alerts, cannot view the Maintenance tab and clear active paystation alerts

Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And I logged in as Bob
When I view the maintenance tab
Then the maintenance tab is not shown
And the pay station active alerts is not shown

Scenario: Child Admin has no permissions to clear active alerts, cannot  clear pay station active alerts in pay station details

Given there exists a customer where the
Customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
location is Downtown
pay station type is LUKE II
major Pay Station Alerts Count is 1
And there exists a active alert where the 
event type is Paper Status
pay station is 500000070001
severity is Critical
is Active
action type is alerted
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And I logged in as Bob
When I clear the active alerts on pay station details where the
Serial number is 500000070001
Then the active alerts on pay station details is not cleared

Scenario: Child Admin with Clear Active Alerts can access Pay station to clear pay station active alerts

Given there exists a permission where the
permission name is clear active alerts
Id is 204
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
location is Downtown
pay station type is LUKE II
major Pay Station Alerts Count is 1
And there exists a heartbeat where the 
pay station is 500000070001
last seen is 2 hours ago
And there exists a customer where the
Customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And Bob has permission to clear active alerts
And I logged in as Bob
When I clear the active alerts on pay station details where the
Serial number is 500000070001
Then the active alerts on pay station details is cleared
