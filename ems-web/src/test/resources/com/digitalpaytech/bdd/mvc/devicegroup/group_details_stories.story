Narrative:
In order to test the group details page
As a developer
I want to ensure that when a group is clicked, the correct details are presented

Scenario:  Group details are clicked and user has permissions
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
unifi id is 201
And there exists a Customer Subscription where the
Type is Online Pay Station Configuration
Customer is Mackenzie Parking
is enabled
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And Bob has permission to manage paystation settings
And Bob has permission to view paystation settings
And I logged in as Bob
And there exists a location where the 
Location Name is Downtown
And there exists a pay station where the  
serial number is 500000070001
latitude is 49.28600334159728
longitude is -123.12086105346685
is Activated
customer is Mackenzie Parking
pay station type is LUKE II
Location is Downtown
Minor Pay Station Alerts Count is 1
Paper Level is 1
And the Facility Management Service response with message
{
  "groupId": "1",
  "groupName": "Inactive Settings",
  "settingsId": "settingsId1", 
  "deviceList": [
    {
      "deviceId": "500000070001",
      "upgradeStatus": "completed",
      "lastModified": "2017-01-01T13:16:06.000-0800"
    },
    {
      "deviceId": "500000070010",
      "upgradeStatus": "completed",
      "lastModified": "2017-01-01T13:16:06.000-0800"
    }
  ],
  "settingsReceivedDate": "2017-01-01T13:16:06.000-0800",
  "groupConfiguration": {
    "cardProcessingOverride": true,
    "irisSettings": {
      "cardProcessing": {
        "enableOnlineComm": true,
        "enableOnlineProcess": true,
	"acceptCardsOffline": true,
        "acceptPassCard": true
      }
    },
    "bossSettings": {
      "cardProcessing": {
        "enableOnlineComm": false,
        "enableOnlineProcess": false,
	"acceptCardsOffline": false,
        "acceptPassCard": false
      }
    }
  }
}
When I visit the group is clicked for test group
Then the json response is
{  
  "groupName":"Inactive Settings",
  "settingsId":"settingsId1",
  "deviceList":[  
    {  
      "upgradeStatus":"completed",
      "serialNumber":"500000070001",
      "isPosActivated":true,
      "isPosDecommissioned":false,
      "isPosHidden":false,
      "paystationType":5
    }
  ],
  "settingsReceivedDate":"2017-01-01T13:16:06.000-0800",
  "groupConfiguration":{  
    "cardProcessingOverride":true,
    "irisSettings":{  
      "cardProcessing":{  
        "enableOnlineComm":true,
        "enableOnlineProcess":true,
        "acceptCardsOffline":true,
        "acceptPassCard":true
      }
    },
    "bossSettings":{  
      "cardProcessing":{  
        "enableOnlineComm":false,
        "enableOnlineProcess":false,
        "acceptCardsOffline":false,
        "acceptPassCard":false
      }
    }
  },
  "unrecognizedDevices":[  
    {  
      "deviceId":"500000070010",
      "upgradeStatus":"completed"
    }
  ],
  "mapInfo":[  
    {  
      "payStationType":5,
      "alertIcon":0,
      "serialNumber":"500000070001",
      "name":"500000070001",
      "latitude":49.28600334159728,
      "longitude":-123.12086105346685,
      "locationName":"Downtown",
      "paperStatus":2,
      "lastCollection":null
    }
  ]
}