Narrative:
When paystation information tab is displayed a call is made to facilities management micro-service

Scenario: a call is made to facilities management micro-service
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
unifi id is 201
And there exists a location where the
location name is Downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
location is Downtown
pay station type is LUKE II
is activated
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And Bob has permission to view paystations
And Bob has permission to manage paystations
And I logged in as Bob
When I view the paystation information where the
serial number is 500000070001
Then a groupInfoFromDevice request is sent to Facility Management Service

Scenario: a call is not made to facilities management micro-service when user doesn't have the permission
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
unifi id is 201
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
location is Downtown
pay station type is LUKE II
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And Bob has permission to view paystations
And Bob has permission to manage paystations
And I logged in as Bob
When I view the paystation information where the
serial number is 500000070001
Then a groupInfoFromDevice request is not sent to Facility Management Service