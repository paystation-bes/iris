Narrative:
Test that a system admin is able to
view device groups of a customer.

Scenario: The target microservice is called when Iris receive the device group list request
Given there is a SystemAdmin user Bob
And Bob has permission to view device groups
And I logged in as Bob
And there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
unifi id is 201
And there exists a Customer Subscription where the
Type is Online Pay Station Configuration
Customer is Mackenzie Parking
is enabled
When I filter configuration groups where the
status is blank
page is 1
Then a groupList request is sent to Facility Management Service

Scenario: The target microservice is not called when the user has invalid permission
Given there is a SystemAdmin user Bob
And I logged in as Bob
And there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
unifi id is 201
And there exists a Customer Subscription where the
Type is Online Pay Station Configuration
Customer is Mackenzie Parking
is enabled
When I filter configuration groups where the
status is blank
page is 1
Then a groupList request is not sent to Facility Management Service