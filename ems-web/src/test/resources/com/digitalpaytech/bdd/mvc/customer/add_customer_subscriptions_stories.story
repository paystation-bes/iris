Narrative:
In order to test handling of subscription while adding customers
As a SystemAdmin user
I want to create customer profiles with certain services and verify that the correct services are assigned

Scenario:  Create Parent Customer with No services
Given there is a SystemAdmin user Bob
And Bob has permission to add a customer
And I logged in as Bob
And I create a new customer where the 
Customer name is mckenzieparking
Is a Parent Company
Current Time Zone is Canada/Pacific
Account Status is enabled
Services Assigned are none
When I save the customer
Then there exists a customer where the 
Customer name is mckenzieparking
When the customer details page is verified for created customer mckenzieparking
Then the customer is returned successfully where the services activated are none

Scenario:  Create Parent Customer with all services
Given there is a SystemAdmin user Bob
And Bob has permission to add a customer
And I logged in as Bob
And I create a new customer where the 
admin user name is admin@mckenzieparking
customer name is mckenzieparking
Is a Parent Company
services assigned are all
When I save the customer
Then there exists a customer where the 
Customer name is mckenzieparking
When the customer details page is verified for created customer mckenzieparking
Then the customer is returned successfully where the services activated are all

Scenario:  Create Parent Customer with certain services
Given there is a SystemAdmin user Bob
And Bob has permission to add a customer
And I logged in as Bob
And I create a new customer where the 
admin user name is admin@mckenzieparking
customer name is mckenzieparking
Is a Parent Company
services assigned are Passcards,Extend-By-Phone,FLEX Integration,Smart Cards
When I save the customer
Then there exists a customer where the 
Customer name is mckenzieparking
When the customer details page is verified for created customer mckenzieparking
Then the customer is returned successfully where the services activated are Passcards,Extend-By-Phone,FLEX Integration,Smart Cards

Scenario:  Create stand alone Customer with certain services
Given there is a SystemAdmin user Bob
And Bob has permission to add a customer
And I logged in as Bob
And I create a new customer where the 
Customer name is Mackenzie Parking
Is not a Parent Company
services assigned are Passcards,Extend-By-Phone,FLEX Integration,Smart Cards
Current Time Zone is Canada/Pacific
Account Status is enabled
When I save the customer
Then there exists a customer where the 
Customer name is Mackenzie Parking
Is not a Parent Company
Current Time Zone is Canada/Pacific
Account Status is enabled
When I type string Mackenzie Parking into customer search bar input
Then the customer search bar auto complete returns Mackenzie Parking
When I search for Mackenzie Parking in the Customer Search Bar
Then the profile is shown in the customer profile screen where the 
Customer name is Mackenzie Parking
Time zone is Canada/Pacific
Status is enabled
When the customer details page is verified for created customer Mackenzie Parking
Then the customer is returned successfully where the services activated are Passcards,Extend-By-Phone,FLEX Integration,Smart Cards