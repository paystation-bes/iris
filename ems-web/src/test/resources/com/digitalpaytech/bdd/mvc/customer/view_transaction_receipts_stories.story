Narrative:
A user can view emv labels on receipt if it is an emv transaction

Scenario: An emv transaction will display fields cvm, aid and apl
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a merchant account where the  
name is TestCreditCall
merchant account status is enabled
customer is Mackenzie Parking
processor is processor.creditcall
Field1 is Elavon
Field2 is merchantID
Field3 is 11223344
Field4 is TRANSACTION
Field5 is CAD
And there exists a pay station setting where the
name is Student Parking
And there exists a location where the 
location name is downtown
And there exists a pay station where the  
serial number is 500000070001
name is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
Credit card Merchant Account is TestCreditCall
is new merchant account
And there exists a unified rate where the
name is Valid For One Hour
And there exists a purchase where the
purchase id is 1
purchase date is 08/18/2016 10:00
customer name is Mackenzie Parking
card paid amount is 300
serial number is 500000070001
pay station setting is Student Parking
location is Downtown
transaction type is regular
payment type is CC (Swipe)
original amount is 300
charged amount is 300
unified rate is Valid For One Hour
merchant account is TestCreditCall
is approved processor transaction
authorization number is A323412
processor transaction type is Real-Time
card type is VISA
processing date is 08/18/2016 10:00
And there exists a payment card where the
card type is credit card
merchant is TestCreditCall
processor transaction for is Mackenzie Parking
purchase customer is  Mackenzie Parking
And there exists a transaction card token where the
processor transaction authorization number is A323412
cvm is NO CARDHOLDER VERIFICATION
apl is VISA
aid is A100000303034
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And Bob has permission to manage transaction reports
And I logged in as Bob
When I view the transaction receipt
Then the receipt with emv is shown

