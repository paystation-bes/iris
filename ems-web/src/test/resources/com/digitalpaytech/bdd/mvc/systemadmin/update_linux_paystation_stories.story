Narrative:
In order to test updating a linux paystation
As a system admin user
I want to test validation of the POSServiceState.

Scenario: Change the name of a linux PS
Given there exists a customer where the
customer Name is Mackenzie Parking
unifi id is 1111
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
is activated
is linux
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070002
serial number is 500000070002
pay station type is LUKE II
is activated
is linux
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And I logged in as Bob
And I create a new pay station edit where the 
point of sale is 500000070001
is billable
is linux
location is downtown
customer is Mackenzie Parking
name is 500000070002
When I save the paystation 
Then there exists a pay station where the
customer is Mackenzie Parking
name is 500000070002
serial number is 500000070001
pay station type is LUKE II
is linux
Then the message is sent to kafka's topic 'Device Update LDCMS'

Scenario: Change a PS to linux then change the serial number
Given there exists a customer where the
customer Name is Mackenzie Parking
unifi id is 1111
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
is activated
is linux
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070002
serial number is 500000070002
pay station type is LUKE II
is activated
is linux
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And I logged in as Bob
Given I create a new pay station edit where the 
point of sale is 500000070001
serial number is 500000070002
is billable
is linux
location is downtown
customer is Mackenzie Parking
When I save the paystation 
Then a delete request is sent to Telemetry Service
And the message is sent to kafka's topic 'Device Update LDCMS'
And the message is sent to kafka's topic 'User Message MitreId'
Then the message is sent to kafka's topic 'User Message MitreId'
And the message is sent to kafka's topic 'Device Update LDCMS'
And a createDevice request is sent to Telemetry Service

Scenario: Move a linux paystation to a new customer, can't verify in DB because stored procedure is used
!-- Disabled because the final behavior still in discussion with the Product team. as mentioned on [IRIS-3827]
Meta:
@skip    
@ignored true
Given there exists a customer where the
unifi id is 1111
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a customer where the
customer Name is Competitor Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a location where the 
location name is downtown
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
pay station type is LUKE II
is activated
is linux
And there is a SystemAdmin user Bob
And Bob has permission to manage paystations
And Bob has permission to move paystations
And I logged in as Bob
Given I create a new pay station edit where the 
point of sale is 500000070001
is billable
is linux
location is downtown
customer is Mackenzie Parking
new customer is Competitor Parking
name is 500000070001
When I save the paystation 
Then a delete request is sent to Telemetry Service
And a deleteDeviceUser request is sent to Auth Service
And the message is sent to kafka's topic 'Device Update LDCMS'
Then a createDevice request is sent to Telemetry Service
And a createDeviceUser request is sent to Auth Service
And the message is sent to kafka's topic 'Device Update LDCMS'
