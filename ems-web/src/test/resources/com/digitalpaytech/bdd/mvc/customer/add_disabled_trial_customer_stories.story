Narrative:
In order to test handling of disabled & customers in trial 
As a sysadmin user
I want to create customer profiles with disabled and trial status and verify if the same in the customer details page

Scenario:  Create Customer with Disabled account
Given there is a SystemAdmin user Bob
And Bob has permission to add a customer
And I logged in as Bob
And I create a new customer where the 
Customer name is mckenzieparking
Is a Parent Company
Current Time Zone is Canada/Pacific
Account Status is disabled
When I save the customer
Then there exists a customer where the 
Customer name is mckenzieparking
When the customer details page is verified for created customer mckenzieparking
Then the customer is returned successfully where the
account status is disabled

Scenario:  Create Customer with Enabled account
Given there is a SystemAdmin user Bob
And Bob has permission to add a customer
And I logged in as Bob
And I create a new customer where the 
Customer name is mckenzieparking
Is a Parent Company
Current Time Zone is Canada/Pacific
Account Status is enabled
When I save the customer
Then there exists a customer where the 
Customer name is mckenzieparking
When the customer details page is verified for created customer mckenzieparking
Then the customer is returned successfully where the
account status is enabled

Scenario:  Create Customer with Trial account
Given there is a SystemAdmin user Bob
And Bob has permission to add a customer
And I logged in as Bob
And I create a new customer where the 
Customer name is mckenzieparking
Is a Parent Company
Current Time Zone is Canada/Pacific
Account Status is trial
trial end date is 12/1/3000
When I save the customer
Then there exists a customer where the 
Customer name is mckenzieparking
When the customer details page is verified for created customer mckenzieparking
Then the customer is returned successfully where the
Account Status is trial
trial end date is Mon Dec 01 23:59:59 PST 3000