Narrative:
In order to test td merchant card refunds
As an user
I want to ensure that the credit card refunds are handled appropriately 

Scenario: Successfully refund an CoreCPS transaction
Given there exists a customer where the 
customer name is Mackenzie Parking
account type is child
unifi id is 1
is migrated
query spaces by is 1
is subscribed to Real-Time Credit Card Processing
And there exists a location where the 
location name is Downtown
And there exists a merchant account where the
name is TestLinkMerchantAccount
customer is Mackenzie Parking
merchant account status is enabled
is link
terminal token is 9f4bfda9-1b9b-4bbe-87ae-827e9ca38a12
processor is processor.paymentech.link
And there exists a pay station setting where the
name is Student Parking
And there exists a location where the 
location name is downtown
And there exists a pay station where the  
serial number is 500000070001
name is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
Credit card Merchant Account is TestLinkMerchantAccount
is new merchant account
And there exists a unified rate where the
name is Valid For One Hour
And there exists a processor transaction where the
purchase customer is Mackenzie Parking
amount is 300 
card type is VISA
merchant account is TestLinkMerchantAccount
paystation is 500000070001
processor transaction id is 1234
processor transaction type is Real-Time
And there exists a purchase where the
purchase id is 1
purchase date is 08/18/2016 10:00
customer name is Mackenzie Parking
card paid amount is 300
serial number is 500000070001
pay station setting is Student Parking
location is Downtown
transaction type is regular
payment type is CC (Swipe)
original amount is 300
charged amount is 300
unified rate is Valid For One Hour
merchant account is TestLinkMerchantAccount
is approved processor transaction
processor transaction type is Real-Time
card type is VISA
processing date is 08/18/2016 10:00
last4digits is 0005
authorization number is 1234
ticket number is 1234
And there exists a payment card where the
card type is credit card
merchant is TestLinkMerchantAccount
processor transaction for is Mackenzie Parking
purchase customer is  Mackenzie Parking
And there exists a transaction card token where the
processor transaction authorization number is 1234
token is 5724387b-b4e5-4a24-be9a-5848527ca98d
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And I create a new card refund where the 
card number is 0005
refund card expire is 0719
refund amount is 300
processor transaction is 1234
And Bob has permission to issue refunds
And I logged in as Bob
Given the Core CPS response with message 
{
  "status": {
    "responseStatus": "SUCCESS",
    "errors": null
  },
  "response": {
    "status": {
      "responseStatus": "Accepted",
      "errors": null
    },
    "chargeToken": null,
    "cardToken": null,
    "authorizationNumber": "      ",
    "cardType": null,
    "last4Digits": null,
    "referenceNumber": "000897",
    "processorTransactionId": "00000012",
    "processedDate": "2018-03-01T19:15:29.734Z",
    "terminalToken": null,
    "optionalData": null,
    "refundToken": "b6b51a57-4d11-4dbe-80b6-980801b643cc"
  }
}
When I click refund where the
card number is XXXXXXXXXXXX0005
card expire date is 0719
transaction date is 10:00
amount is 300
Then there exists a processor transaction where the
purchase customer is Mackenzie Parking
amount is 300 
card type is VISA
merchant account is TestLinkMerchantAccount
paystation is 500000070001
is approved
processor transaction type is Refund

Scenario: Refund is denied by CoreCPS
Given there exists a customer where the 
customer name is Mackenzie Parking
account type is child
unifi id is 1
is migrated
query spaces by is 1
is subscribed to Real-Time Credit Card Processing
And there exists a location where the 
location name is Downtown
And there exists a merchant account where the
name is TestLinkMerchantAccount
customer is Mackenzie Parking
merchant account status is enabled
is link
terminal token is 9f4bfda9-1b9b-4bbe-87ae-827e9ca38a12
processor is processor.paymentech.link
And there exists a pay station setting where the
name is Student Parking
And there exists a location where the 
location name is downtown
And there exists a pay station where the  
serial number is 500000070001
name is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
Credit card Merchant Account is TestLinkMerchantAccount
is new merchant account
And there exists a unified rate where the
name is Valid For One Hour
And there exists a processor transaction where the
purchase customer is Mackenzie Parking
amount is 300 
card type is VISA
merchant account is TestLinkMerchantAccount
paystation is 500000070001
processor transaction id is 1234
processor transaction type is Real-Time
And there exists a purchase where the
purchase id is 1
purchase date is 08/18/2016 10:00
customer name is Mackenzie Parking
card paid amount is 300
serial number is 500000070001
pay station setting is Student Parking
location is Downtown
transaction type is regular
payment type is CC (Swipe)
original amount is 300
charged amount is 300
unified rate is Valid For One Hour
merchant account is TestLinkMerchantAccount
is approved processor transaction
processor transaction type is Real-Time
card type is VISA
processing date is 08/18/2016 10:00
last4digits is 0005
authorization number is 1234
ticket number is 1234
And there exists a payment card where the
card type is credit card
merchant is TestLinkMerchantAccount
processor transaction for is Mackenzie Parking
purchase customer is  Mackenzie Parking
And there exists a transaction card token where the
processor transaction authorization number is 1234
token is 5724387b-b4e5-4a24-be9a-5848527ca98d
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And I create a new card refund where the 
card number is 0005
refund card expire is 0719
refund amount is 300
processor transaction is 1234
And Bob has permission to issue refunds
And I logged in as Bob
Given the Core CPS response with message 
{
  "status": {
    "responseStatus": "SUCCESS",
    "errors": null
  },
  "response": {
    "status": {
      "responseStatus": "Denied",
      "errors": null
    },
    "chargeToken": null,
    "cardToken": null,
    "authorizationNumber": "      ",
    "cardType": null,
    "last4Digits": null,
    "referenceNumber": "000897",
    "processorTransactionId": "00000012",
    "processedDate": "2018-03-01T19:15:29.734Z",
    "terminalToken": null,
    "optionalData": null,
    "refundToken": "b6b51a57-4d11-4dbe-80b6-980801b643cd"
  }
}
When I click refund where the
card number is XXXXXXXXXXXX0005
card expire date is 0719
transaction date is 10:00
amount is 300
Then there exists a processor transaction where the
purchase customer is Mackenzie Parking
amount is 300 
card type is VISA
merchant account is TestLinkMerchantAccount
paystation is 500000070001
is not approved
processor transaction type is Refund

Scenario: Error is returned by CoreCPS
Given there exists a customer where the 
customer name is Mackenzie Parking
account type is child
unifi id is 1
is migrated
query spaces by is 1
is subscribed to Real-Time Credit Card Processing
And there exists a location where the 
location name is Downtown
And there exists a merchant account where the
name is TestLinkMerchantAccount
customer is Mackenzie Parking
merchant account status is enabled
is link
terminal token is 9f4bfda9-1b9b-4bbe-87ae-827e9ca38a12
processor is processor.paymentech.link
And there exists a pay station setting where the
name is Student Parking
And there exists a location where the 
location name is downtown
And there exists a pay station where the  
serial number is 500000070001
name is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
Credit card Merchant Account is TestLinkMerchantAccount
is new merchant account
And there exists a unified rate where the
name is Valid For One Hour
And there exists a processor transaction where the
purchase customer is Mackenzie Parking
amount is 300 
card type is VISA
merchant account is TestLinkMerchantAccount
paystation is 500000070001
processor transaction id is 1234
processor transaction type is Real-Time
And there exists a purchase where the
purchase id is 1
purchase date is 08/18/2016 10:00
customer name is Mackenzie Parking
card paid amount is 300
serial number is 500000070001
pay station setting is Student Parking
location is Downtown
transaction type is regular
payment type is CC (Swipe)
original amount is 300
charged amount is 300
unified rate is Valid For One Hour
merchant account is TestLinkMerchantAccount
is approved processor transaction
processor transaction type is Real-Time
card type is VISA
processing date is 08/18/2016 10:00
last4digits is 0005
authorization number is 1234
ticket number is 1234
And there exists a payment card where the
card type is credit card
merchant is TestLinkMerchantAccount
processor transaction for is Mackenzie Parking
purchase customer is  Mackenzie Parking
And there exists a transaction card token where the
processor transaction authorization number is 1234
token is 5724387b-b4e5-4a24-be9a-5848527ca98d
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And I create a new card refund where the 
card number is 0005
refund card expire is 0719
refund amount is 300
processor transaction is 1234
And Bob has permission to issue refunds
And I logged in as Bob
Given the Core CPS response with message 
{
  "status": {
    "responseStatus": "SUCCESS",
    "errors": null
  },
  "response": {
    "status": {
      "responseStatus": "Denied",
      "errors": null
When I click refund where the
card number is XXXXXXXXXXXX0005
card expire date is 0719
transaction date is 10:00
amount is 300
Then there exists a processor transaction where the
purchase customer is Mackenzie Parking
amount is 300 
card type is VISA
merchant account is TestLinkMerchantAccount
paystation is 500000070001
is not approved
processor transaction type is Refund

Scenario: Successfully refund an emv transaction by a pay station that has its
credit card merchant account set to Creditcall
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a merchant account where the  
name is TestCreditCall
merchant account status is enabled
customer is Mackenzie Parking
processor is processor.creditcall
Field1 is Elavon
Field2 is merchantID
Field3 is 11223344
Field4 is TRANSACTION
Field5 is CAD
And there exists a pay station setting where the
name is Student Parking
And there exists a location where the 
location name is downtown
And there exists a pay station where the  
serial number is 500000070001
name is 500000070001
is activated
location is Downtown 
customer is Mackenzie Parking
Credit card Merchant Account is TestCreditCall
is new merchant account
And there exists a unified rate where the
name is Valid For One Hour
And there exists a purchase where the
purchase id is 1
purchase date is 08/18/2016 10:00
customer name is Mackenzie Parking
card paid amount is 300
serial number is 500000070001
pay station setting is Student Parking
location is Downtown
transaction type is regular
payment type is CC (Swipe)
original amount is 300
charged amount is 300
unified rate is Valid For One Hour
merchant account is TestCreditCall
is approved processor transaction
processor transaction type is Real-Time
card type is VISA
processing date is 08/18/2016 10:00
last4digits is 0005
authorization number is ABCABC
ticket number is 1234
And there exists a payment card where the
card type is credit card
merchant is TestCreditCall
processor transaction for is Mackenzie Parking
purchase customer is  Mackenzie Parking
And there exists a transaction card token where the
processor transaction authorization number is ABCABC
cvm is NO CARDHOLDER VERIFICATION
apl is VISA
aid is A100000303034
token is fsg89df7g8923j83jkdfy8
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And I create a new card refund where the 
card number is 0005
refund card expire is 0719
refund amount is 300
is emv
processor transaction is ABCABC
And Bob has permission to issue refunds
And I logged in as Bob
Given the payment server response with message 
{
  "status": {
    "code": "accepted"
  },
  "referenceId": "",
  "authorizationNumber": "AAAAA",
  "processorTransactionId": "123456789",
  "processedDate": "2016-08-26T15:14:41.000Z"
}
When I click refund where the
card number is XXXXXXXXXXXX0005
card expire date is 0719
transaction date is 10:00
amount is 300
Then there exists a processor transaction where the
purchase customer is Mackenzie Parking
amount is 300 
card type is VISA
merchant account is TestCreditCall
paystation is 500000070001
processor transaction id is 123456789
processor transaction type is Refund

Scenario:  A refund transaction is successfully refunded by a pay station that has its 
credit card merchant account set to TD Merchant Services
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And there exists a merchant account where the  
name is TDMerchantServices
customer is Mackenzie Parking
processor is processor.tdmerchant
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is CAD
And there exists a purchase where the
customer name is Mackenzie Parking
cash paid amount is 200
serial number is 500000070001
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
location is Downtown
pay station type is LUKE II
is activated
credit card merchant account is TDMerchantServices
And there exists a customer card type where the 
customer is Mackenzie Parking
name is Credit Card
card type is Credit Card
And there exists a processor transaction where the 
id is 1
purchase customer is Mackenzie Parking
authorization number is ABC123
card hash is dummyhash
ticket number is 2
paystation is 500000070001
amount is 200 
merchant account is TDMerchantServices
card type is VISA
purchase customer is Mackenzie Parking
processor transaction type is Real-Time
And I create a new card refund where the 
card number is 4030000010001234
refund card expire is 0719
refund amount is 200
is not emv
processor transaction is ABC123
And Bob has permission to issue refunds
And I logged in as Bob
When I click refund where the
card number is XXXXXXXXXXXX1234
card expire date is 0719
transaction date is 10:00
amount is 200
Then there exists a processor transaction where the
purchase customer is Mackenzie Parking
amount is 200 
card type is VISA
merchant account is TDMerchantServices
paystation is 500000070001
processor transaction type is Refund

Scenario:  A refund transaction is declined by a pay station that has its 
credit card merchant account set to TD Merchant Services
Given there exists a customer where the
customer Name is Mackenzie Parking
current Time Zone is Canada/Pacific
account type is child
is migrated
And there exists a user account where the
user Name is Bob
user status type is enabled
user account customer is Mackenzie Parking
And there exists a merchant account where the  
name is TDMerchantServices
customer is Mackenzie Parking
processor is processor.tdmerchant
field1 is 117681535
field2 is D9231CC32Ac1431686121828E1a7e171
field3 is CAD
And there exists a purchase where the
customer name is Mackenzie Parking
cash paid amount is 700
serial number is 500000070001
And there exists a pay station where the
customer is Mackenzie Parking
name is 500000070001
serial number is 500000070001
location is Downtown
pay station type is LUKE II
is activated
credit card merchant account is TDMerchantServices
And there exists a customer card type where the 
customer is Mackenzie Parking
name is Credit Card
card type is Credit Card
And there exists a processor transaction where the 
id is 1
authorization number is ABC123
ticket number is 2
card hash is dummyhash
paystation is 500000070001
preauth transaction data is 1
amount is 700 
merchant account is TDMerchantServices
card type is VISA
purchase customer is Mackenzie Parking
processor transaction type is Real-Time
And I create a new card refund where the 
card number is 4030000010001234
refund card expire is 0719
refund amount is 700
is not emv
processor transaction is ABC123
And Bob has permission to issue refunds
And I logged in as Bob
When I click refund where the
card number is XXXXXXXXXXXX1234
card expire date is 0719
transaction date is 10:00
amount is 700
Then the refund is not processed
