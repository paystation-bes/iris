Narrative:
In order to test saving a bad smart card
As a developer
I want to ensure that the bad smart card is persisted successfully when the card number is 4 digits long

Scenario:  Adding a bad smart card where length is 4 digits

Given there is a parent admin user Bob
And Bob has permission to add banned cards
And I logged in as Bob
And there exists a customer card type where the 
id is 1
customer is Bob
name is Smart Card
card type is Smart Card
And I create a new banned card where the 
card type id is 1
card number is 4444
When I save the bad card
Then there exists a bad card where the
card number is 4444
