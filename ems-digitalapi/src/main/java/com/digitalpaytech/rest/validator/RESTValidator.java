package com.digitalpaytech.rest.validator;

import com.digitalpaytech.domain.RestAccount;
import com.digitalpaytech.domain.RestSessionToken;
import com.digitalpaytech.dto.rest.RESTPermit;
import com.digitalpaytech.exception.ApplicationException;

public interface RESTValidator {
    void validateTimestamp(String timestamp, String methodName) throws ApplicationException;
    
    String validateAccountName(String accountName, String methodName) throws ApplicationException;
    
    RestSessionToken validateSessionToken(String sessionToken, String methodName) throws ApplicationException;
    
    void validateSignatureVersion(String signatureVersion) throws ApplicationException;
    
    void validateCreatePermit(RESTPermit request, String methodName) throws ApplicationException;
    
    void validateUpdatePermit(RESTPermit request, String methodName) throws ApplicationException;
    
    void validatePermitNumberForGetPermit(String permitNumber, String methodName) throws ApplicationException;

    boolean validatePOS(RestAccount restAccount) throws ApplicationException;

    boolean checkIfPermitExists(RESTPermit permit, RestAccount restAccount) throws ApplicationException;

}
