package com.digitalpaytech.rest.validator.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
//import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.RestAccount;
import com.digitalpaytech.domain.RestSessionToken;
import com.digitalpaytech.dto.rest.RESTPayment;
import com.digitalpaytech.dto.rest.RESTPayments;
import com.digitalpaytech.dto.rest.RESTPermit;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.IncorrectCredentialsException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.InvalidPermitNumberException;
import com.digitalpaytech.rest.validator.RESTValidator;
import com.digitalpaytech.service.PurchaseService;
import com.digitalpaytech.service.RestPropertyService;
import com.digitalpaytech.service.RestSessionTokenService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RestCoreConstants;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Component("restValidator")
@SuppressWarnings("PMD.GodClass")
public class RESTValidatorImpl implements RESTValidator {
    private static final Logger LOGGER = Logger.getLogger(RESTValidatorImpl.class);
    
    private static final String INVALID_TIMESTAMP = " Invalid Timestamp. (";
    private static final String INPUT_PERMITNUMBER_VALUE = " (Input PermitNumber Value: ";
    private static final String PERMITNUMBER_INT_PARSE_EXCEPTION = " <PermitNumber> Integer parsing exception in ";
    private static final String INPUT_STALLNUMBER = " (Input StallNumber Value: ";
    private static final String INPUT_PLATENUMBER = " (Input PlateNumber Value: ";
    private static final String INPUT_REGION = " (Input RegionName Value: ";
    private static final String SINGLE_QUOTES_SPACE_AND_COMMA = "', '";
    
    private static final Set<String> ACCEPTABLE_CARD_TYPES = new HashSet<String>();
    
    @Autowired
    private RestSessionTokenService restSessionTokenService;
    
    @Autowired
    private RestPropertyService restPropertyService;
    
    @Autowired
    private PurchaseService purchaseService;
    
    static {
        ACCEPTABLE_CARD_TYPES.add(CardProcessingConstants.NAME_AMEX);
        ACCEPTABLE_CARD_TYPES.add(CardProcessingConstants.NAME_DINERS);
        ACCEPTABLE_CARD_TYPES.add(CardProcessingConstants.NAME_DISCOVER);
        ACCEPTABLE_CARD_TYPES.add(CardProcessingConstants.NAME_MASTERCARD);
        ACCEPTABLE_CARD_TYPES.add(CardProcessingConstants.NAME_VISA);
    }
    
    public final void setRestSessionTokenService(final RestSessionTokenService restSessionTokenService) {
        this.restSessionTokenService = restSessionTokenService;
    }
    
    public final RestSessionTokenService getRestSessionTokenService() {
        return this.restSessionTokenService;
    }
    
    public final void setRestPropertyService(final RestPropertyService restPropertyService) {
        this.restPropertyService = restPropertyService;
    }
    
    public final RestPropertyService getRestPropertyService() {
        return this.restPropertyService;
    }
    
    @Override
    public final void validateTimestamp(final String timestamp, final String methodName) throws ApplicationException {
        try {
            if (timestamp == null) {
                LOGGER.error("#### <Timestamp is NULL and should not be NULL in " + methodName + StandardConstants.STRING_EMPTY_SPACE
                             + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new IncorrectCredentialsException();
            }
            
            if (timestamp.trim().isEmpty()) {
                LOGGER.error(StandardConstants.STRING_FOUR_NUMBER_SIGNS + INVALID_TIMESTAMP + methodName + StandardConstants.STRING_CLOSE_PARENTHESIS
                             + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new IncorrectCredentialsException();
            }
            
            DateUtil.convertFromRESTDateString(timestamp);
        } catch (InvalidDataException e) {
            LOGGER.error("#### Exception in converting Timestamp to Date type. (" + methodName + StandardConstants.STRING_CLOSE_PARENTHESIS
                         + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new IncorrectCredentialsException(e);
        }
    }
    
    @Override
    public final String validateAccountName(final String accountName, final String methodName) throws ApplicationException {
        final String newAccountName = StringEscapeUtils.escapeHtml(accountName);
        if (newAccountName == null) {
            LOGGER.error("#### Empty Account Name. (" + methodName + StandardConstants.STRING_CLOSE_PARENTHESIS
                         + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException();
        }
        
        if (newAccountName.trim().isEmpty() || newAccountName.trim().length() > RestCoreConstants.MAX_LENGTH_255) {
            LOGGER.error("#### Invalid length of Account Name. (" + methodName + StandardConstants.STRING_CLOSE_PARENTHESIS
                         + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException();
        }
        
        if (!newAccountName.matches(WebCoreConstants.REGEX_PRINTABLE_TEXT)) {
            LOGGER.error("#### Account Name (" + newAccountName + "} has invalid character. (" + methodName
                         + StandardConstants.STRING_CLOSE_PARENTHESIS + StandardConstants.STRING_EMPTY_SPACE
                         + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException();
        }
        return newAccountName;
    }
    
    @Override
    public final boolean validatePOS(final RestAccount restAccount) throws ApplicationException {
        if (!restAccount.getPointOfSale().getPosStatus().isIsActivated()) {
            return false;
        }
        return true;
    }
    
    @Override
    public final boolean checkIfPermitExists(final RESTPermit permit, final RestAccount restAccount) throws ApplicationException {
        
        final Date purchaseDate = DateUtil.convertFromRESTDateString(permit.getPurchasedDate());
        final Purchase purchase = this.purchaseService.findPurchaseByPointOfSaleIdAndPurchaseNumberAndpPurchaseGmt(restAccount.getPointOfSale()
                .getId(), Integer.parseInt(permit.getPermitNumber()), purchaseDate);
        
        if (purchase == null) {
            return false;
        }
        return true;
    }
    
    @Override
    public final RestSessionToken validateSessionToken(final String sessionToken, final String methodName) throws ApplicationException {
        if (sessionToken == null) {
            LOGGER.error("#### Empty session token. (" + methodName + StandardConstants.STRING_CLOSE_PARENTHESIS
                         + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new IncorrectCredentialsException();
        }
        
        if (sessionToken.trim().isEmpty() || sessionToken.trim().length() != RestCoreConstants.MAX_LENGTH_32) {
            LOGGER.error("#### Invalid length of session token. (" + methodName + StandardConstants.STRING_CLOSE_PARENTHESIS
                         + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new IncorrectCredentialsException();
        }
        
        final List<RestSessionToken> lists = this.restSessionTokenService.findRestSessionTokenBySessionToken(sessionToken);
        
        if (lists == null || lists.isEmpty()) {
            throw new IncorrectCredentialsException();
        }
        final Date currDate = new Date();
        for (RestSessionToken restSessionToken : lists) {
            if (restSessionToken.getExpiryDate().before(currDate)) {
                LOGGER.error("#### SessionToken (" + restSessionToken.getSessionToken() + ") has already been expired in " + methodName
                             + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new IncorrectCredentialsException();
            }
        }
        
        return lists.get(0);
    }
    
    @Override
    public final void validateSignatureVersion(final String signatureVersion) throws ApplicationException {
        final String versionStr = this.restPropertyService.getPropertyValue(RestPropertyService.REST_SIGNATURE_VERSION,
                                                                            RestPropertyService.REST_SIGNATURE_VERSION_DEFAULT);
        final String[] versions = versionStr.trim().split(RestCoreConstants.MULTI_VALUE_SEPERATOR);
        
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("++ pass in signatureVersion: " + signatureVersion + ", supported signatureVersions: " + versionStr + " +++");
        }
        
        for (String version : versions) {
            if (signatureVersion.trim().equals(version.trim())) {
                return;
            }
        }
        LOGGER.error("++++++ signature version: " + signatureVersion + " not supported +++++");
        throw new IncorrectCredentialsException();
    }
    
    @Override
    public final void validateCreatePermit(final RESTPermit request, final String methodName) throws ApplicationException {
        validatePermitNumber(request, methodName);
        validatePurchasedDate(request, methodName);
        validateStallNumber(request, methodName);
        validatePlateNumber(request, methodName);
        validateRateName(request, methodName);
        validateRegionName(request, methodName);
        validateExpiryDate(request, methodName);
        validatePermitAmount(request, methodName);
        validatePayment(request, methodName);
        
    }
    
    @Override
    public final void validateUpdatePermit(final RESTPermit request, final String methodName) throws ApplicationException {
        this.validatePermitNumber(request, methodName);
        this.validatePurchasedDate(request, methodName);
        
        if (request.getExpiryDate() != null && !request.getExpiryDate().isEmpty()) {
            this.validateExpiryDate(request, methodName);
        }
        
        if (request.getPermitAmount() != null) {
            this.validatePermitAmount(request, methodName);
        }
        
        if (request.getPayments() != null) {
            this.validatePayment(request, methodName);
        }
    }
    
    private void validatePermitNumber(final RESTPermit request, final String methodName) throws ApplicationException {
        final String permitNumber = request.getPermitNumber();
        try {
            if (permitNumber == null) {
                LOGGER.error(StandardConstants.STRING_FOUR_NUMBER_SIGNS + " <PermitNumber> is NULL and should not be NULL in " + methodName
                             + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new InvalidDataException();
            }
            
            if (Integer.parseInt(permitNumber) < RestCoreConstants.MIN_LENGTH_1) {
                LOGGER.error(StandardConstants.STRING_FOUR_NUMBER_SIGNS + " <PermitNumber> has invalid length in " + methodName
                             + INPUT_PERMITNUMBER_VALUE + permitNumber + StandardConstants.STRING_CLOSE_PARENTHESIS
                             + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new InvalidDataException();
            }
        } catch (NumberFormatException e) {
            LOGGER.error(StandardConstants.STRING_FOUR_NUMBER_SIGNS + PERMITNUMBER_INT_PARSE_EXCEPTION + methodName + INPUT_PERMITNUMBER_VALUE
                         + permitNumber + StandardConstants.STRING_CLOSE_PARENTHESIS + StandardConstants.STRING_EMPTY_SPACE
                         + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException(e);
        }
    }
    
    @Override
    public final void validatePermitNumberForGetPermit(final String permitNumber, final String methodName) throws ApplicationException {
        try {
            if (permitNumber == null) {
                LOGGER.error("#### <PermitNumber> is NULL and should not be NULL in " + methodName + StandardConstants.STRING_EMPTY_SPACE
                             + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new InvalidPermitNumberException();
            }
            
            if (Integer.parseInt(permitNumber) < RestCoreConstants.MIN_LENGTH_1) {
                LOGGER.error("#### <PermitNumber> has invalid length in " + methodName + INPUT_PERMITNUMBER_VALUE + permitNumber
                             + StandardConstants.STRING_CLOSE_PARENTHESIS + StandardConstants.STRING_EMPTY_SPACE
                             + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new InvalidPermitNumberException();
            }
        } catch (NumberFormatException e) {
            LOGGER.error("#### <PermitNumber> Integer parsing exception in " + methodName + INPUT_PERMITNUMBER_VALUE + permitNumber
                         + StandardConstants.STRING_CLOSE_PARENTHESIS + StandardConstants.STRING_EMPTY_SPACE
                         + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidPermitNumberException(e);
        }
    }
    
    private void validatePurchasedDate(final RESTPermit request, final String methodName) throws ApplicationException {
        try {
            final String purchasedDate = request.getPurchasedDate();
            
            if (purchasedDate == null) {
                LOGGER.error("#### <PurchasedDate> is NULL and should not be NULL in " + methodName + StandardConstants.STRING_EMPTY_SPACE
                             + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new InvalidDataException();
            }
            
            if (purchasedDate.trim().isEmpty()) {
                LOGGER.error(StandardConstants.STRING_FOUR_NUMBER_SIGNS + INVALID_TIMESTAMP + methodName + StandardConstants.STRING_CLOSE_PARENTHESIS
                             + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new InvalidDataException();
            }
            
            DateUtil.convertFromRESTDateString(purchasedDate);
        } catch (InvalidDataException e) {
            LOGGER.error("#### Exception in converting PurchasedDate to Date type. (" + methodName + StandardConstants.STRING_CLOSE_PARENTHESIS
                         + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException(e);
        }
    }
    
    private void validateStallNumber(final RESTPermit request, final String methodName) throws ApplicationException {
        final String stallNumber = request.getStallNumber();
        try {
            if (stallNumber != null && !stallNumber.isEmpty()) {
                if (stallNumber.length() > RestCoreConstants.MIN_LENGTH_8) {
                    LOGGER.error("#### <StallNumber> value exceeded the maximum number in " + methodName + INPUT_STALLNUMBER + stallNumber
                                 + StandardConstants.STRING_CLOSE_PARENTHESIS + StandardConstants.STRING_EMPTY_SPACE
                                 + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                    throw new InvalidDataException();
                }
                
                if (Integer.parseInt(stallNumber) < RestCoreConstants.MIN_LENGTH_1
                    || Integer.parseInt(stallNumber) > RestCoreConstants.MAX_MEDIUM_INT) {
                    LOGGER.error("#### <StallNumber> can't be less than 1 or more than 16777215 in " + methodName + INPUT_STALLNUMBER + stallNumber
                                 + StandardConstants.STRING_CLOSE_PARENTHESIS + StandardConstants.STRING_EMPTY_SPACE
                                 + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                    throw new InvalidDataException();
                }
            }
        } catch (NumberFormatException e) {
            LOGGER.error("#### <StallNumber> Integer parsing exception in " + methodName + INPUT_STALLNUMBER + stallNumber
                         + StandardConstants.STRING_CLOSE_PARENTHESIS + StandardConstants.STRING_EMPTY_SPACE
                         + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException(e);
        }
    }
    
    private void validatePlateNumber(final RESTPermit request, final String methodName) throws ApplicationException {
        final String plateNumber = request.getPlateNumber();
        if (plateNumber != null) {
            if (!(plateNumber.length() >= RestCoreConstants.MIN_LENGTH_1 && plateNumber.length() <= RestCoreConstants.MAX_LENGTH_10)) {
                LOGGER.error("#### <PlateNumber> should be 1~10 characters in " + methodName + INPUT_PLATENUMBER + plateNumber
                             + StandardConstants.STRING_CLOSE_PARENTHESIS + StandardConstants.STRING_EMPTY_SPACE
                             + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new InvalidDataException();
            }
            
            if (!plateNumber.matches(WebCoreConstants.REGEX_ALPHANUMERIC)) {
                LOGGER.error("#### <PlateNumber> RegExp Exception in " + methodName + INPUT_PLATENUMBER + plateNumber
                             + StandardConstants.STRING_CLOSE_PARENTHESIS + StandardConstants.STRING_EMPTY_SPACE
                             + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new InvalidDataException();
            }
        }
    }
    
    private void validateRateName(final RESTPermit request, final String methodName) throws ApplicationException {
        final String rateName = request.getRateName();
        if (rateName == null) {
            request.setRateName(RestCoreConstants.THIRD_PARTY);
        } else {
            if (!(rateName.length() >= RestCoreConstants.MIN_LENGTH_1 && rateName.length() <= RestCoreConstants.MAX_LENGTH_20)) {
                LOGGER.error("#### <RateName> should be 1~20 characters in " + methodName + " (Input RateName Value: " + rateName
                             + StandardConstants.STRING_CLOSE_PARENTHESIS + StandardConstants.STRING_EMPTY_SPACE
                             + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new InvalidDataException();
            }
        }
    }
    
    private void validateRegionName(final RESTPermit request, final String methodName) throws ApplicationException {
        final String regionName = request.getRegionName();
        if (regionName != null) {
            if (regionName.length() < WebCoreConstants.VALIDATION_MIN_LENGTH_4 || regionName.length() > WebCoreConstants.VALIDATION_MAX_LENGTH_25) {
                LOGGER.error("#### <RegionName> should be 4~25 characters in " + methodName + INPUT_REGION + regionName
                             + StandardConstants.STRING_CLOSE_PARENTHESIS + StandardConstants.STRING_EMPTY_SPACE
                             + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new InvalidDataException();
            }
            
            if (!regionName.matches(WebCoreConstants.REGEX_LOCATION_TEXT)) {
                LOGGER.error("#### <RegionName> RegExp Exception in " + methodName + INPUT_REGION + regionName
                             + StandardConstants.STRING_CLOSE_PARENTHESIS + StandardConstants.STRING_EMPTY_SPACE
                             + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new InvalidDataException();
            }
        }
    }
    
    private void validateExpiryDate(final RESTPermit request, final String methodName) throws ApplicationException {
        try {
            final String expiryDate = request.getExpiryDate();
            
            if (expiryDate == null) {
                LOGGER.error("#### <ExpiryDate> is NULL and should not be NULL in " + methodName + StandardConstants.STRING_EMPTY_SPACE
                             + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new InvalidDataException();
            }
            
            if (expiryDate.trim().isEmpty()) {
                LOGGER.error("#### Invalid Timestamp. (" + methodName + StandardConstants.STRING_CLOSE_PARENTHESIS
                             + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new InvalidDataException();
            }
            
            final Date inputTime = DateUtil.convertFromRESTDateString(expiryDate);
            if (inputTime.before(DateUtil.convertFromRESTDateString(request.getPurchasedDate()))) {
                LOGGER.error("#### Invalid ExpiryDate. ExpiryDate should be greater than PurchasedDate. (" + methodName
                             + StandardConstants.STRING_CLOSE_PARENTHESIS + StandardConstants.STRING_EMPTY_SPACE
                             + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new InvalidDataException();
            }
        } catch (InvalidDataException e) {
            LOGGER.error("#### Exception in converting ExpiryDate to Date type. (" + methodName + StandardConstants.STRING_CLOSE_PARENTHESIS
                         + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException(e);
        }
    }
    
    private void validatePermitAmount(final RESTPermit request, final String methodName) throws ApplicationException {
        final BigDecimal permitAmount = request.getPermitAmount();
        if (permitAmount == null) {
            LOGGER.error("#### <PermitAmount> is NULL and should not be NULL in " + methodName + StandardConstants.STRING_EMPTY_SPACE
                         + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException();
        }
        
        if (permitAmount.setScale(2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1)).intValue() < 0) {
            LOGGER.error("#### <PermitAmount> should be greater than or equal to 0 in " + methodName + StandardConstants.STRING_EMPTY_SPACE
                         + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException();
        }
        
    }
    
    private void validatePayment(final RESTPermit request, final String methodName) throws ApplicationException {
        final RESTPayments payments = request.getPayments();
        
        if (request.getPermitAmount() != null && request.getPermitAmount().doubleValue() > 0.0 && (payments == null || payments.getPayment() == null)) {
            LOGGER.error("#### <Payments> or <Payment> is NULL and should not be NULL when <PermitAmount> is greater than 0 in " + methodName
                         + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException();
        }
        
        if (payments != null) {
            final List<RESTPayment> list = payments.getPayment();
            if (list != null && !list.isEmpty()) {
                int totalPayment = 0;
                
                final BigDecimal dollarsInCents = new BigDecimal(StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1);
                
                for (RESTPayment payment : list) {
                    
                    checkPayment(payment, methodName);
                    
                    totalPayment += payment.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(dollarsInCents).intValue();
                    
                }
                
                checkPermitAmount(request, methodName, totalPayment);
            }
        }
    }
    
    private void checkPayment(final RESTPayment payment, final String methodName) throws InvalidDataException {
        if (payment.getType() == null) {
            LOGGER.error("#### Type attribute in the <Payment> is NULL and should not be NULL in " + methodName
                         + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException();
        }
        
        if (payment.getType().equals(RestCoreConstants.PAYMENT_TYPE_CREDITCARD)) {
            
            checkCardType(payment, methodName);
            
        } else {
            LOGGER.error("#### Payment Type should be 'CreditCard' in " + methodName + " (Type Value: " + payment.getType()
                         + StandardConstants.STRING_CLOSE_PARENTHESIS + StandardConstants.STRING_EMPTY_SPACE
                         + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException();
        }
        
        try {
            if (payment.getPaymentDate() != null && !payment.getPaymentDate().isEmpty()) {
                @SuppressWarnings("unused")
                final Date inputTime = DateUtil.convertFromRESTDateString(payment.getPaymentDate());
            }
        } catch (InvalidDataException e) {
            LOGGER.error("#### Exception in converting PaymentDate to Date type. (" + methodName + StandardConstants.STRING_CLOSE_PARENTHESIS
                         + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException(e);
        }
        
        if (payment.getAmount() == null) {
            LOGGER.error("#### <Amount> is NULL and should not be NULL in " + methodName + StandardConstants.STRING_EMPTY_SPACE
                         + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException();
        }
        
        if (payment.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal(StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1)).intValue() < 0) {
            LOGGER.error("#### <Amount> should be greater than or equal to 0 in " + methodName + StandardConstants.STRING_EMPTY_SPACE
                         + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException();
        }
        
    }
    
    private void checkPermitAmount(final RESTPermit request, final String methodName, final int totalPayment) throws InvalidDataException {
        if (request.getPermitAmount() != null
            && totalPayment != request.getPermitAmount().setScale(2, BigDecimal.ROUND_HALF_UP)
                    .multiply(new BigDecimal(StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1)).intValue()) {
            LOGGER.error("#### The value of <PermitAmount> and total <Amount> do not match. (" + methodName
                         + StandardConstants.STRING_CLOSE_PARENTHESIS + StandardConstants.STRING_EMPTY_SPACE
                         + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException();
        }
        
    }
    
    private void checkCardType(final RESTPayment payment, final String methodName) throws InvalidDataException {
        if (payment.getCardType() == null) {
            LOGGER.error("#### <CardType> is NULL and should not be NULL in " + methodName + StandardConstants.STRING_EMPTY_SPACE
                         + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException();
        }
        
        if (!ACCEPTABLE_CARD_TYPES.contains(payment.getCardType())) {
            LOGGER.error("#### <CardType> should be one of the following: '" + CardProcessingConstants.NAME_VISA + SINGLE_QUOTES_SPACE_AND_COMMA
                         + CardProcessingConstants.NAME_MASTERCARD + SINGLE_QUOTES_SPACE_AND_COMMA + CardProcessingConstants.NAME_AMEX
                         + SINGLE_QUOTES_SPACE_AND_COMMA + CardProcessingConstants.NAME_DINERS + "', and '" + CardProcessingConstants.NAME_DISCOVER
                         + "' in " + methodName + " (Input CardType Value: " + payment.getCardType() + StandardConstants.STRING_CLOSE_PARENTHESIS
                         + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException();
        }
        
        if (payment.getLast4DigitsOfCard() == null) {
            LOGGER.error("#### <Last4DigitsOfCard> is NULL and should not be NULL in " + methodName + StandardConstants.STRING_EMPTY_SPACE
                         + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException();
        }
        
        if (payment.getLast4DigitsOfCard().length() != RestCoreConstants.CREDITCARD_LAST4DIGIT_LENGTH) {
            LOGGER.error("#### <Last4DigitsOfCard> should be 4 characters in " + methodName + " (Input Last4DigitsOfCard Value: "
                         + payment.getLast4DigitsOfCard() + StandardConstants.STRING_CLOSE_PARENTHESIS + StandardConstants.STRING_EMPTY_SPACE
                         + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException();
        }
        
        if (payment.getCardAuthorizationId() == null || payment.getCardAuthorizationId().length() == 0) {
            LOGGER.error("#### <CardAuthorizationId> is NULL and should not be NULL in " + methodName + StandardConstants.STRING_EMPTY_SPACE
                         + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException();
        }
        
        if (payment.getCardAuthorizationId().length() > RestCoreConstants.MAX_LENGTH_30) {
            LOGGER.error("#### <CardAuthorizationId> should be max " + RestCoreConstants.MAX_LENGTH_30 + " characters in " + methodName
                         + " (Input CardAuthorizationId Value: " + payment.getCardAuthorizationId() + StandardConstants.STRING_CLOSE_PARENTHESIS
                         + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException();
        }
        
    }
    
}
