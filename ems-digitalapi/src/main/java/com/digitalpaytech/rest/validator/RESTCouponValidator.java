package com.digitalpaytech.rest.validator;

import com.digitalpaytech.dto.rest.RESTCoupon;
import com.digitalpaytech.dto.rest.RESTCoupons;
import com.digitalpaytech.dto.rest.RESTCouponCodes;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.RestSessionToken;
import com.digitalpaytech.exception.RESTCouponPushException;

import java.util.List;

public interface RESTCouponValidator {
    public void validateCreateCoupon(RESTCoupon request, String methodName, Customer customer) throws RESTCouponPushException;
    public void validateUpdateCoupon(RESTCoupon request, String methodName) throws RESTCouponPushException;
    
    public void validateCreateCoupons(RESTCoupons request, String methodName, Customer customer) throws RESTCouponPushException;
    public void validateUpdateCoupons(RESTCoupons request, String methodName) throws RESTCouponPushException;
    public void validateGetCoupons(List<String> couponCodes, String methodName) throws RESTCouponPushException;
    public void validateGetAllCouponsSize(int couponCodesSize, String methodName) throws RESTCouponPushException;
    public void validateCouponCodes(List<String> couponCodes, String methodName) throws RESTCouponPushException;
    public void validateDeleteCoupons(RESTCouponCodes request, String methodName) throws RESTCouponPushException;

    public void validateSignatureVersion(String signatureVersion) throws RESTCouponPushException;
    public void validateCouponCode(String couponCode, String methodName) throws RESTCouponPushException;
    public void validateTimestamp(String timestamp, String methodName) throws RESTCouponPushException;
    public RestSessionToken validateSessionToken(String sessionToken, String methodName) throws RESTCouponPushException;
    public void validateMaxRequestSize(int requestSize) throws RESTCouponPushException;
    public void validateMaxCouponNumbers(int couponNumbers) throws RESTCouponPushException;
    public Location validateLocation(String locationName, int customerId) throws RESTCouponPushException;
    public boolean checkCouponCodeUnique(RESTCoupon dtoCoupon, String methodName, Customer customer);
}
