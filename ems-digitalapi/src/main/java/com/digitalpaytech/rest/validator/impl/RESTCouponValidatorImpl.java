package com.digitalpaytech.rest.validator.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.RestSessionToken;
import com.digitalpaytech.dto.rest.RESTCoupon;
import com.digitalpaytech.dto.rest.RESTCoupons;
import com.digitalpaytech.dto.rest.RESTCouponCodes;
import com.digitalpaytech.rest.validator.RESTCouponValidator;
import com.digitalpaytech.rest.validator.RESTValidator;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.RESTCouponPushException;
import com.digitalpaytech.service.CouponService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RestCoreConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;

@Component("restCouponValidator")
public class RESTCouponValidatorImpl implements RESTCouponValidator {
    private static Logger log = Logger.getLogger(RESTCouponValidatorImpl.class);
    
    @Autowired
    private RESTValidator restValidator;
    
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    @Autowired
    private LocationService locationService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    @Autowired
    private CouponService couponService;    
    
    // =========================================================================================
    // Create Single-Coupon 
    // =========================================================================================
    @Override
    public void validateCreateCoupon(RESTCoupon dtoCoupon, String methodName, Customer customer) throws RESTCouponPushException {
        StringBuilder bdr = null;                
        
        // CouponCode
        validateCouponCode(dtoCoupon.getCouponCode(), methodName);
        // DiscountType, DiscountAmt, DiscountPercent
        if (StringUtils.isBlank(dtoCoupon.getDiscountType())) {
            bdr = new StringBuilder();
            bdr.append("#### <DiscountType> is NULL and should not be NULL in ").append(this.getClass().getName()).append(".").append(methodName)
                    .append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        } else if (dtoCoupon.getDiscountAmt() == null && dtoCoupon.getDiscountPercent() == null) {
            bdr = new StringBuilder();
            bdr.append("#### <DiscountAmt> or <DiscountPercent> is NULL and should not be NULL in ").append(this.getClass().getName()).append(".")
                    .append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        } else if (dtoCoupon.getDiscountType().equalsIgnoreCase(WebCoreConstants.COUPON_DISCOUNT_TYPE_DOLLAR)
                   && (dtoCoupon.getDiscountAmt() == null || dtoCoupon.getDiscountAmt().signum() == -1 || dtoCoupon.getDiscountAmt().doubleValue() < 0)) {
            // DiscountAmt is not a number, not positive or is 0.
            bdr = new StringBuilder();
            bdr.append("#### <DiscountAmt> is not positive in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        } else if (dtoCoupon.getDiscountType().equalsIgnoreCase(WebCoreConstants.COUPON_DISCOUNT_TYPE_DOLLAR)
                   && (dtoCoupon.getDiscountAmt().doubleValue() > WebCoreConstants.COUPON_DOLLARS_DISCOUNT_MAX || dtoCoupon.getDiscountAmt().scale() > 2)) {
            // DiscountAmt is more than $999.99
            bdr = new StringBuilder();
            bdr.append("#### <DiscountAmt> is between 0 and $999.99 in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        } else if (dtoCoupon.getDiscountType().equalsIgnoreCase(WebCoreConstants.COUPON_DISCOUNT_TYPE_PERCENT)
                   && ((dtoCoupon.getDiscountPercent() == null || dtoCoupon.getDiscountPercent().signum() == -1 || dtoCoupon.getDiscountPercent().doubleValue() < 0))) {
            // DiscountPercent is not a number, not positive or is 0.
            bdr = new StringBuilder();
            bdr.append("#### <DiscountPercent> is not positive in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        } else if (dtoCoupon.getDiscountType().equalsIgnoreCase(WebCoreConstants.COUPON_DISCOUNT_TYPE_PERCENT)
                   && (dtoCoupon.getDiscountPercent().scale() > 0 || dtoCoupon.getDiscountPercent().doubleValue() > WebCoreConstants.COUPON_DISCOUNT_MAX)) {
            // DiscountPercent is not a number, not positive or is 0.
            bdr = new StringBuilder();
            bdr.append("#### <DiscountPercent> is between 0 and 100 in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        
        // Description
        if (StringUtils.isNotBlank(dtoCoupon.getDescription()) && dtoCoupon.getDescription().length() > WebCoreConstants.COUPON_DESCRIPTION_LENGTH_MAX) {
            bdr = new StringBuilder();
            bdr.append("#### <Description> is not too long in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        } else if (StringUtils.isNotBlank(dtoCoupon.getDescription()) && !validateRegularExpression(dtoCoupon.getDescription())) {
            bdr = new StringBuilder();
            bdr.append("#### <Description> contains invalid characters in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        
        // Start/end dates
        if (hasDateValue(dtoCoupon.getStartDate()) && !validateDateFormat(dtoCoupon.getStartDate())) {
            bdr = new StringBuilder();
            bdr.append("#### <StartDate> is not valid format in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        if (hasDateValue(dtoCoupon.getEndDate()) && !validateDateFormat(dtoCoupon.getEndDate())) {
            bdr = new StringBuilder();
            bdr.append("#### <EndDate> is not valid format in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        if (hasDateValue(dtoCoupon.getStartDate()) && hasDateValue(dtoCoupon.getEndDate()) && !validateDateFormat(dtoCoupon.getStartDate(), dtoCoupon.getEndDate())) {
            bdr = new StringBuilder();
            bdr.append("#### <StartDate>, <EndDate> are not valid format in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        
        // Location String up to 50 characters
        if (StringUtils.isNotBlank(dtoCoupon.getLocation()) && dtoCoupon.getLocation().length() > WebCoreConstants.COUPON_MAX_LOCATION_LENGTH) {
            bdr = new StringBuilder();
            bdr.append("#### <Location> exceeds ").append(WebCoreConstants.COUPON_MAX_LOCATION_LENGTH).append(" characters in ")
                    .append(this.getClass().getName()).append(".");
            bdr.append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        
        // NumUses is between 0 - Short.Max
        if (StringUtils.isNotBlank(dtoCoupon.getUses()) && !WebCoreUtil.isInteger(dtoCoupon.getUses())) {
            bdr = new StringBuilder();
            bdr.append("#### <Uses> must be a number starts from 0 in ").append(this.getClass().getName()).append(".");
            bdr.append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        } else if (StringUtils.isNotBlank(dtoCoupon.getUses())
                   && (Integer.parseInt(dtoCoupon.getUses()) < 0 || Integer.parseInt(dtoCoupon.getUses()) > WebCoreConstants.COUPON_USES_MAX)) {
            bdr = new StringBuilder();
            bdr.append("#### <Uses> must be a number starts from 0 or number is too big in ").append(this.getClass().getName()).append(".");
            bdr.append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        
        if (StringUtils.isBlank(dtoCoupon.getOperatingMode())) {
            bdr = new StringBuilder();
            bdr.append("#### <OperatingMode> is NULL and should not be NULL in ").append(this.getClass().getName()).append(".").append(methodName)
                    .append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        } else if (!containValidData(dtoCoupon.getOperatingMode(), WebCoreConstants.COUPON_OPERATING_MODES)) {
            bdr = new StringBuilder();
            bdr.append("#### <OperatingMode> contains invalid data in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            bdr.append("\nCan be any combination of 'PND', 'PBS', or 'PBL' separated by commas");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        
        // The range of spaces this coupon is valid for if the <OperatingMopde> includes "PBS".
        if (dtoCoupon.getOperatingMode().indexOf(WebCoreConstants.COUPON_CODE_PAY_BY_SPACE) != -1 && StringUtils.isNotBlank(dtoCoupon.getSpaceRange())
            && dtoCoupon.getSpaceRange().trim().length() > WebCoreConstants.COUPON_MAX_SPACE_RANGE) {
            bdr = new StringBuilder();
            bdr.append("#### <SpaceRange> exceeds ").append(WebCoreConstants.COUPON_MAX_SPACE_RANGE).append(" characters in ")
                    .append(this.getClass().getName()).append(".");
            bdr.append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        } else if (dtoCoupon.getOperatingMode().indexOf(WebCoreConstants.COUPON_CODE_PAY_BY_SPACE) != -1 && StringUtils.isNotBlank(dtoCoupon.getSpaceRange())) {
            if (!WebCoreUtil.isValidStallRange(dtoCoupon.getSpaceRange())) {
                bdr = new StringBuilder();
                bdr.append("#### <SpaceRange> is not valid in ").append(this.getClass().getName()).append(".");
                bdr.append(methodName).append(" ####");
                log.error(bdr.toString());
                throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
            }
        }
        // DailySingleUse needs to be YES, NO or blank.
        if (StringUtils.isNotBlank(dtoCoupon.getValidForNumOfDay()) && !dtoCoupon.getValidForNumOfDay().equalsIgnoreCase(WebCoreConstants.YES)
            && !dtoCoupon.getValidForNumOfDay().equalsIgnoreCase(WebCoreConstants.NO)) {
            bdr = new StringBuilder();
            bdr.append("#### <ValidForNumOfDay> has invalid characters in ").append(this.getClass().getName()).append(".");
            bdr.append(methodName).append(", integer or empty space only. ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }                
    }    
    
    // =========================================================================================
    // Update Single-Coupon 
    // =========================================================================================
    @Override
    public void validateUpdateCoupon(RESTCoupon request, String methodName) throws RESTCouponPushException {
        StringBuilder bdr = null;
        
        // CouponCode
        validateCouponCode(request.getCouponCode(), methodName);
        
        if (StringUtils.isBlank(request.getDiscountType())){
            // DiscountType has not been included
            bdr = new StringBuilder();
            bdr.append("#### <DiscountType> cannot be blank ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);         
        } else if (WebCoreConstants.COUPON_DISCOUNT_TYPE_DOLLAR.equalsIgnoreCase(request.getDiscountType()) && 
                   (request.getDiscountAmt() == null || request.getDiscountAmt().doubleValue() > WebCoreConstants.COUPON_DOLLARS_DISCOUNT_MAX || request.getDiscountAmt().scale() > 2)) {
            // DiscountAmt is more than $999.99
            bdr = new StringBuilder();
            bdr.append("#### <DiscountAmt> is between 0 and $999.99 in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        } else if (WebCoreConstants.COUPON_DISCOUNT_TYPE_PERCENT.equalsIgnoreCase(request.getDiscountType()) && 
                   ((request.getDiscountPercent() == null || request.getDiscountPercent().signum() == -1 || request.getDiscountPercent().doubleValue() < 0))) {
            // DiscountPercent is not a number, not positive or is 0.
            bdr = new StringBuilder();
            bdr.append("#### <DiscountPercent> is not positive in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        } else if (WebCoreConstants.COUPON_DISCOUNT_TYPE_PERCENT.equalsIgnoreCase(request.getDiscountType()) && request.getDiscountPercent() != null
                   && (request.getDiscountPercent().scale() > 0 || request.getDiscountPercent().doubleValue() > WebCoreConstants.COUPON_DISCOUNT_MAX)) {
            // DiscountPercent is not a number, not positive or is 0.
            bdr = new StringBuilder();
            bdr.append("#### <DiscountPercent> is between 0 and 100 in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        
        // Description
        if (StringUtils.isNotBlank(request.getDescription()) && request.getDescription().length() > WebCoreConstants.COUPON_DESCRIPTION_LENGTH_MAX) {
            bdr = new StringBuilder();
            bdr.append("#### <Description> is not too long in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        } else if (StringUtils.isNotBlank(request.getDescription()) && !validateRegularExpression(request.getDescription())) {
            bdr = new StringBuilder();
            bdr.append("#### <Description> contains invalid characters in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        
        // Start/end dates
        if (hasDateValue(request.getStartDate()) && !validateDateFormat(request.getStartDate())) {
            bdr = new StringBuilder();
            bdr.append("#### <StartDate> is not valid format in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        if (hasDateValue(request.getEndDate()) && !validateDateFormat(request.getEndDate())) {
            bdr = new StringBuilder();
            bdr.append("#### <EndDate> is not valid format in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        if (hasDateValue(request.getStartDate()) && hasDateValue(request.getEndDate()) && !validateDateFormat(request.getStartDate(), request.getEndDate())) {
            bdr = new StringBuilder();
            bdr.append("#### <StartDate>, <EndDate> are not valid format in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        
        // Location String up to 50 characters
        if (StringUtils.isNotBlank(request.getLocation()) && request.getLocation().length() > WebCoreConstants.COUPON_MAX_LOCATION_LENGTH) {
            bdr = new StringBuilder();
            bdr.append("#### <Location> exceeds ").append(WebCoreConstants.COUPON_MAX_LOCATION_LENGTH).append(" characters in ")
                    .append(this.getClass().getName()).append(".");
            bdr.append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        
        // NumUses is between 0 - Short.Max
        if (StringUtils.isNotBlank(request.getUses()) && !WebCoreUtil.isInteger(request.getUses())) {
            bdr = new StringBuilder();
            bdr.append("#### <Uses> must be a number starts from 0 in ").append(this.getClass().getName()).append(".");
            bdr.append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        } else if (StringUtils.isNotBlank(request.getUses())
                   && (Integer.parseInt(request.getUses()) < 0 || Integer.parseInt(request.getUses()) > WebCoreConstants.COUPON_USES_MAX)) {
            bdr = new StringBuilder();
            bdr.append("#### <Uses> must be a number starts from 0 or number is too big in ").append(this.getClass().getName()).append(".");
            bdr.append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        
        if (StringUtils.isNotBlank(request.getOperatingMode())){
	        if (!containValidData(request.getOperatingMode(), WebCoreConstants.COUPON_OPERATING_MODES)) {
	            bdr = new StringBuilder();
	            bdr.append("#### <OperatingMode> contains invalid data in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
	            bdr.append("\nCan be any combination of 'PND', 'PBS', or 'PBL' separated by commas");
	            log.error(bdr.toString());
	            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
	        }
	        
	        // The range of spaces this coupon is valid for if the <OperatingMopde> includes "PBS".
	        if (request.getOperatingMode().indexOf(WebCoreConstants.COUPON_CODE_PAY_BY_SPACE) != -1 && StringUtils.isNotBlank(request.getSpaceRange())
	            && request.getSpaceRange().trim().length() > WebCoreConstants.COUPON_MAX_SPACE_RANGE) {
	            bdr = new StringBuilder();
	            bdr.append("#### <SpaceRange> exceeds ").append(WebCoreConstants.COUPON_MAX_SPACE_RANGE).append(" characters in ")
	                    .append(this.getClass().getName()).append(".");
	            bdr.append(methodName).append(" ####");
	            log.error(bdr.toString());
	            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
	        } else if (request.getOperatingMode().indexOf(WebCoreConstants.COUPON_CODE_PAY_BY_SPACE) != -1 && StringUtils.isNotBlank(request.getSpaceRange())) {
	            if (!WebCoreUtil.isValidStallRange(request.getSpaceRange())) {
	                bdr = new StringBuilder();
	                bdr.append("#### <SpaceRange> is not valid in ").append(this.getClass().getName()).append(".");
	                bdr.append(methodName).append(" ####");
	                log.error(bdr.toString());
	                throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
	            }
	        }
        }
        // DailySingleUse needs to be YES, NO or blank.
        if (StringUtils.isNotBlank(request.getValidForNumOfDay()) && !request.getValidForNumOfDay().equalsIgnoreCase(WebCoreConstants.YES)
            && !request.getValidForNumOfDay().equalsIgnoreCase(WebCoreConstants.NO)) {
            bdr = new StringBuilder();
            bdr.append("#### <ValidForNumOfDay> has invalid characters in ").append(this.getClass().getName()).append(".");
            bdr.append(methodName).append(", must be 'Yes' or 'No'. ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
    }    
    
    @Override
    public void validateCouponCode(String couponCode, String methodName) throws RESTCouponPushException {
        StringBuilder bdr = null;
        if (StringUtils.isBlank(couponCode)) {
            bdr = new StringBuilder();
            bdr.append("#### <CouponCode> is NULL or empty in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        } else if (couponCode.length() > WebCoreConstants.COUPON_LENGTH_MAX) {
            bdr = new StringBuilder();
            bdr.append("#### <CouponCode> exceeds max length: ").append(WebCoreConstants.COUPON_LENGTH_MAX).append(" in ").append(this.getClass().getName())
                    .append(".");
            bdr.append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        } else if (!couponCode.matches(WebCoreConstants.REGULAR_EXPRESSION_FORMAT)) {
            bdr = new StringBuilder();
            bdr.append("#### <CouponCode> has invalid character. Allowed characters are 0-9, a-z and A-Z");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
    }
    
    // =========================================================================================
    // Multi-Coupons 
    // =========================================================================================
    @Override
    public void validateCreateCoupons(RESTCoupons request, String methodName, Customer customer) throws RESTCouponPushException {
        validateIfHasSubCoupons(request, methodName);
        if (request.getCoupons().size() > WebCoreConstants.COUPON_MAX_MULTI_COUPON_CREATION) {
            log.error(getCreateUpdateErrorMessage(WebCoreConstants.COUPON_MAX_MULTI_COUPON_CREATION, methodName));
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        for (RESTCoupon c : request.getCoupons()) {
            validateCreateCoupon(c, methodName, customer);
        }
    }
    
    @Override
    public void validateUpdateCoupons(RESTCoupons request, String methodName) throws RESTCouponPushException {
        validateIfHasSubCoupons(request, methodName);
        if (request.getCoupons().size() > WebCoreConstants.COUPON_MAX_MULTI_COUPON_UPDATE) {
            log.error(getCreateUpdateErrorMessage(WebCoreConstants.COUPON_MAX_MULTI_COUPON_UPDATE, methodName));
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        for (RESTCoupon c : request.getCoupons()) {
            validateUpdateCoupon(c, methodName);
        }
    }
    
    @Override
    public void validateGetCoupons(List<String> couponCodes, String methodName) throws RESTCouponPushException {
        int couponRestMax = getCouponRestMaxGetRecords();
        if (couponCodes.size() > couponRestMax) {
            log.error(getRetrievalDeletionErrorMessage(couponRestMax, methodName));
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_REQUEST_URI);
        }
        validateCouponCodes(couponCodes, methodName);
    }
    
    @Override
    public void validateGetAllCouponsSize(int couponCodesSize, String methodName) throws RESTCouponPushException {
        int couponRestMaxAll = getCouponRestMaxGetAll();
        if (couponCodesSize > couponRestMaxAll) {
            log.error(getRetrievalDeletionErrorMessage(couponRestMaxAll, methodName));
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_REQUEST_ENTITIES);
        }
    }
    
    @Override
    public void validateDeleteCoupons(RESTCouponCodes request, String methodName) throws RESTCouponPushException {
        if (request.getCouponCodes().size() > WebCoreConstants.COUPON_MAX_MULTI_COUPON_DELETION) {
            log.error(getRetrievalDeletionErrorMessage(WebCoreConstants.COUPON_MAX_MULTI_COUPON_DELETION, methodName));
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        validateCouponCodes(request.getCouponCodes(), methodName);
    }
    
    @Override
    public void validateCouponCodes(List<String> couponCodes, String methodName) throws RESTCouponPushException {
        StringBuilder bdr = null;
        if (couponCodes == null || couponCodes.isEmpty()) {
            bdr = new StringBuilder();
            bdr.append("#### <CouponCodes> is NULL or empty in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        
        int size = 0;
        for (String couponCode : couponCodes) {
            try {
                validateCouponCode(couponCode, methodName);
            } catch (RESTCouponPushException restpe) {
                size++;
            }
        }
        if (size > 0) {
            bdr = new StringBuilder();
            bdr.append("#### <CouponCodes> contains empty <CouponCode> in ").append(this.getClass().getName()).append(".").append(methodName).append(" ####");
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
    }
    
    // =========================================================================================
    // Coupon common validation
    // =========================================================================================
    @Override
    public void validateTimestamp(String timestamp, String methodName) throws RESTCouponPushException {
        try {
            restValidator.validateTimestamp(timestamp, methodName);
        } catch (ApplicationException ae) {
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_TIMESTAMP);
        }
    }
    
    @Override
    public RestSessionToken validateSessionToken(String sessionToken, String methodName) throws RESTCouponPushException {
        try {
            return restValidator.validateSessionToken(sessionToken, methodName);
        } catch (ApplicationException ae) {
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_TOKEN);
        }
    }
    
    @Override
    public void validateSignatureVersion(String signatureVersion) throws RESTCouponPushException {
        try {
            restValidator.validateSignatureVersion(signatureVersion);
        } catch (ApplicationException ae) {
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INCORRECT_CREDENTIALS);
        }
    }
    
    @Override
    public void validateMaxRequestSize(int requestSize) throws RESTCouponPushException {
        int couponRestMaxSize = getCouponRestMaxSize();
        if (requestSize > couponRestMaxSize) {
            StringBuilder bdr = new StringBuilder();
            bdr.append("Input request size: ").append(requestSize).append(". ");
            bdr.append("Reject any request if the request body is larger than: ").append(couponRestMaxSize);
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
    }
    
    @Override
    public void validateMaxCouponNumbers(int couponNumbers) throws RESTCouponPushException {
        int couponRestMax = getCouponRestMaxNumbers();
        if (couponNumbers > couponRestMax) {
            StringBuilder bdr = new StringBuilder();
            bdr.append("Input coupon numbers: ").append(couponNumbers).append(". ");
            bdr.append("Reject bulk operations request if the number of coupons affected exceeds: ").append(couponRestMax);
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
    }
    
    @Override
    public Location validateLocation(String locationName, int customerId) throws RESTCouponPushException {
        if (StringUtils.isBlank(locationName) || locationName.equalsIgnoreCase("All")) {
            return null;
        }

        Location loc = locationService.findLocationByCustomerIdAndName(customerId, locationName);
        if (loc != null) {
            return loc;
        }
        StringBuilder bdr = new StringBuilder();
        bdr.append("#### Cannot find parent location with locationName: ").append(locationName).append(" ####");
        log.error(bdr.toString());
        throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
    }
          
    // =========================================================================================
    // Retrieve data from EmsProperties database table.
    // =========================================================================================
    private int getCouponRestMaxGetRecords() {
        String maxNum = emsPropertiesService.getPropertyValue(EmsPropertiesService.COUPON_REST_MAX_GET_RECORDS);
        if (StringUtils.isBlank(maxNum)) {
            return EmsPropertiesService.DEFAULT_COUPON_REST_MAX_GET_RECORDS;
        }
        try {
            return Integer.parseInt(maxNum);
        } catch (NumberFormatException nfe) {
            log.warn("getCouponRestGetMaxRecords is not an integer, return default value: " + EmsPropertiesService.DEFAULT_COUPON_REST_MAX_GET_RECORDS);
            return EmsPropertiesService.DEFAULT_COUPON_REST_MAX_GET_RECORDS;
        }
    }
    
    private int getCouponRestMaxGetAll() {
        String maxNum = emsPropertiesService.getPropertyValue(EmsPropertiesService.COUPON_REST_MAX_GET_ALL);
        if (StringUtils.isBlank(maxNum)) {
            return EmsPropertiesService.DEFAULT_COUPON_REST_MAX_GET_ALL;
        }
        try {
            return Integer.parseInt(maxNum);
        } catch (NumberFormatException nfe) {
            log.warn("getCouponRestMaxGetAll is not an integer, return default value: " + EmsPropertiesService.DEFAULT_COUPON_REST_MAX_GET_ALL);
            return EmsPropertiesService.DEFAULT_COUPON_REST_MAX_GET_ALL;
        }
    }
    
    private int getCouponRestMaxSize() {
        String maxSize = emsPropertiesService.getPropertyValue(EmsPropertiesService.COUPON_REST_MAX_SIZE);
        if (StringUtils.isBlank(maxSize)) {
            return EmsPropertiesService.DEFAULT_COUPON_REST_MAX_SIZE;
        }
        try {
            return Integer.parseInt(maxSize);
        } catch (NumberFormatException nfe) {
            log.warn("getCouponRestMaxSize is not an integer, return default value: " + EmsPropertiesService.DEFAULT_COUPON_REST_MAX_SIZE);
            return EmsPropertiesService.DEFAULT_COUPON_REST_MAX_SIZE;
        }
    }
    
    private int getCouponRestMaxNumbers() {
        String maxNum = emsPropertiesService.getPropertyValue(EmsPropertiesService.COUPON_REST_MAX_NUMBERS);
        if (StringUtils.isBlank(maxNum)) {
            return EmsPropertiesService.DEFAULT_COUPON_REST_MAX_NUMBERS;
        }
        try {
            return Integer.parseInt(maxNum);
        } catch (NumberFormatException nfe) {
            log.warn("getCouponRestMaxNumbers is not an integer, return default value: " + EmsPropertiesService.DEFAULT_COUPON_REST_MAX_NUMBERS);
            return EmsPropertiesService.DEFAULT_COUPON_REST_MAX_NUMBERS;
        }
    }
    
    // =========================================================================================
    private String getCreateUpdateErrorMessage(int maxCouponsSize, String methodName) {
        StringBuilder bdr = new StringBuilder();
        bdr.append("#### <Coupons> exceeds ").append(maxCouponsSize).append(" <Coupon> in ").append(this.getClass().getName()).append(".");
        bdr.append(methodName).append(" ####");
        return bdr.toString();
    }
    
    private void validateIfHasSubCoupons(RESTCoupons coupons, String methodName) throws RESTCouponPushException {
        if (coupons == null || coupons.getCoupons() == null || coupons.getCoupons().isEmpty()) {
            StringBuilder bdr = new StringBuilder();
            bdr.append("#### <Coupon> is NULL and should not be NULL in <Coupons> ").append(this.getClass().getName()).append(".").append(methodName);
            log.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
    }
    
    private boolean validateRegularExpression(String s) {
        if (StringUtils.isNotBlank(s) && !s.matches(WebSecurityConstants.EMS_REGULAR_EXP)) {
            return false;
        }
        return true;
    }
    
    private boolean validateDateFormat(String dateString) {
        dateString = WebCoreUtil.appendHoursMinutesIfNecessary(dateString, "00", "00");
        
        try {
            Date date = DateUtil.convertFromRESTDateString(dateString);
            if (date.before(WebCoreConstants.COUPON_DATE_MIN) || date.after(WebCoreConstants.COUPON_DATE_MAX)) {
                return false;
            }
        } catch (InvalidDataException ide) {
            return false;
        }
        return true;
    }
    
    private boolean validateDateFormat(String start, String end) {
        start = WebCoreUtil.appendHoursMinutesIfNecessary(start, "00", "00");
        end = WebCoreUtil.appendHoursMinutesIfNecessary(end, "23", "59");
        
        try {
            Date startDate = DateUtil.convertFromRESTDateString(start);
            Date endDate = DateUtil.convertFromRESTDateString(end);
            if (endDate.before(startDate)) {
                return false;
            }
        } catch (InvalidDataException ide) {
            return false;
        }
        return true;
    }
    
    private boolean hasDateValue(String value) {
        if (StringUtils.isBlank(value) || value.equalsIgnoreCase(WebCoreConstants.NOT_APPLICABLE)) {
            return false;
        }
        return true;
    }
    
    private boolean containValidData(String value, String[] validData) {
        for (String s : validData) {
            if (value.indexOf(s) != -1) {
                return true;
            }
        }
        return false;
    }
    
    private String getRetrievalDeletionErrorMessage(int maxCouponsSize, String methodName) {
        StringBuilder bdr = new StringBuilder();
        bdr.append("#### <CouponCodes> exceeds ").append(maxCouponsSize).append(" <CouponCode> in ").append(this.getClass().getName()).append(".");
        bdr.append(methodName).append(" ####");
        return bdr.toString();
    }
    
    @Override
    public boolean checkCouponCodeUnique(RESTCoupon dtoCoupon, String methodName, Customer customer) {
        Coupon c = couponService.findCouponByCustomerIdAndCouponCode(customer.getId(), dtoCoupon.getCouponCode());
        if (c != null) {        	        	
        	return false;
        }        
        return true;
    }

    public void setRestValidator(RESTValidator restValidator) {
        this.restValidator = restValidator;
    }

    public void setEmsPropertiesService(EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }

    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }

    public void setPointOfSaleService(PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
}
