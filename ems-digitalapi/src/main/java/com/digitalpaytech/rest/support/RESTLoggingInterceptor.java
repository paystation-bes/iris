package com.digitalpaytech.rest.support;

import javax.servlet.ServletResponseWrapper;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.digitalpaytech.domain.RestAccount;
import com.digitalpaytech.rest.validator.RESTValidator;
import com.digitalpaytech.service.RestAccountService;
import com.digitalpaytech.service.RestLogService;
import com.digitalpaytech.service.RestLogTotalCallService;
import com.digitalpaytech.servlet.DPTHttpServletResponseWrapper;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RestCoreConstants;

@Component
public class RESTLoggingInterceptor extends HandlerInterceptorAdapter {
    
    private final static Logger logger = Logger.getLogger(RESTLoggingInterceptor.class);
    
    @Autowired
    private RestAccountService restAccountService;
    
    @Autowired
    private RestLogService restLogService;
    
    @Autowired
    private RestLogTotalCallService restLogTotalCallService;
    
    @Autowired
    protected RESTValidator restValidator;
    
    public void setRestAccountService(RestAccountService restAccountService) {
        this.restAccountService = restAccountService;
    }
    
    public void setRestLogService(RestLogService restLogService) {
        this.restLogService = restLogService;
    }
    
    public void setRestLogTotalCallService(RestLogTotalCallService restLogTotalCallService) {
        this.restLogTotalCallService = restLogTotalCallService;
    }
    
    public void setRestValidator(RESTValidator restValidator) {
        this.restValidator = restValidator;
    }
    
    public RESTLoggingInterceptor() {
    }
    
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        
        String path = request.getPathInfo();
        String method = request.getMethod();
        String token = request.getParameter("Token");
        
        RestAccount restAccount = null;
        if (RestCoreConstants.PATH_TOKEN.equals(path)) {
            if (RestCoreConstants.HTTP_METHOD_GET.equals(method)) {
                String account = request.getParameter("Account");
                account = this.restValidator.validateAccountName(account, "RestLoggingInterceptor.preHandle()");
                restAccount = this.restAccountService.findRestAccountByAccountName(account);
            } else if (RestCoreConstants.HTTP_METHOD_PUT.equals(method)) {
                restAccount = this.restAccountService.findRestAccountBySessionToken(token);
            }
        } else {
            restAccount = this.restAccountService.findRestAccountBySessionToken(token);
        }
        
        if (restAccount == null) {
            restAccount = this.restAccountService.findRestAccountByAccountName(RestCoreConstants.UNKNOWN_ACCOUNT_NAME);
        }
        
        StringBuilder bdr = new StringBuilder();
        if (restAccount != null) {
            int totalCalls = this.restLogTotalCallService.saveRestLogTotalCall(restAccount);
            bdr.append("#### [REST API Call Logging] AccountName: ").append(restAccount.getAccountName()).append(", AccountName after html escape: ").append(method);
            bdr.append(", LoggingDate: ").append(DateUtil.createDateOnlyString(DateUtil.getCurrentGmtDate())).append(", TotalCalls(Company Level): ");
            bdr.append(totalCalls).append(" ####");
            logger.info(bdr.toString());
        } else {
            bdr.append("Cannot find RESTAccount, path: ").append(path).append(", method: ").append(method).append(", token: ").append(token);
            logger.warn(bdr.toString());
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception e) throws Exception {
        
        String path = request.getPathInfo();
        String method = request.getMethod();
        String token = request.getParameter("Token");
        String endPointName = ((HandlerMethod) handler).getMethod().getName();
        
        RestAccount restAccount = null;
        if (RestCoreConstants.PATH_TOKEN.equals(path)) {
            if (RestCoreConstants.HTTP_METHOD_GET.equals(method)) {
                String account = request.getParameter("Account");
                account = this.restValidator.validateAccountName(account, "RestLoggingInterceptor.afterCompletion()");
                restAccount = this.restAccountService.findRestAccountByAccountName(account);
            } else if (RestCoreConstants.HTTP_METHOD_PUT.equals(method)) {
                restAccount = this.restAccountService.findRestAccountBySessionToken(token);
            }
        } else {
            restAccount = this.restAccountService.findRestAccountBySessionToken(token);
        }
        
        boolean isError = false;
        int responseStatus = ((DPTHttpServletResponseWrapper)response).getStatus();
        if (responseStatus >= 300) {
            isError = true;
        }
        
        if (isError) {
            if (restAccount == null) {
                restAccount = this.restAccountService.findRestAccountByAccountName(RestCoreConstants.UNKNOWN_ACCOUNT_NAME);
            }
            StringBuilder bdr = new StringBuilder();
            if (restAccount != null) {
                this.restLogService.saveRestLog(restAccount, endPointName, isError);
                
                bdr.append("#### [REST API Call Logging] ERROR (AccountName: ").append(restAccount.getAccountName()).append(", CustomerId: ").append(restAccount.getCustomer().getId());
                bdr.append(", CustomerName: ").append(restAccount.getCustomer().getName()).append(", EndPoint: ").append(endPointName);
                bdr.append(", ErrorCode: ").append(responseStatus).append(") ####");
                logger.info(bdr.toString()); 
            } else {
                bdr.append("Cannot get RESTAccount using: ").append(RestCoreConstants.UNKNOWN_ACCOUNT_NAME);
                logger.warn(bdr.toString());
            }
            
        } else {
            this.restLogService.saveRestLog(restAccount, endPointName, isError);
            
        }
    }

}
