package com.digitalpaytech.rest.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.domain.RestAccount;
import com.digitalpaytech.domain.RestSessionToken;
import com.digitalpaytech.dto.rest.RESTToken;
import com.digitalpaytech.dto.rest.RESTTokenQueryParamList;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.IncorrectCredentialsException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.service.RestPropertyService;
import com.digitalpaytech.util.RestCoreConstants;

@Controller
@RequestMapping(value = "/Token")
public class RESTTokenController extends RESTBaseController {
    
    private final static Logger logger = Logger.getLogger(RESTTokenController.class);
    
    @RequestMapping(method = RequestMethod.GET, produces = "application/xml")
    @ResponseBody
    public RESTToken getToken(@RequestParam("Signature") String signature, @RequestParam("SignatureVersion") String signatureVersion,
                              @RequestParam("Timestamp") String timestamp, @RequestParam("Account") String account) throws ApplicationException {
        
        String methodName = this.getClass().getName() + ".getToken()";
        logger.debug("#### START " + methodName + " ####");
        
        try {
            super.restValidator.validateTimestamp(timestamp, methodName);
            account = super.restValidator.validateAccountName(account, methodName);
            super.restValidator.validateSignatureVersion(signatureVersion);
            
            RestAccount restAccount = super.restAccountService.findRestAccountByAccountName(account);
            
            if (restAccount == null) {
                logger.error("#### No RESTAccount has been found in getToken() operation for the AccountName: " + account + " ####");
                throw new InvalidDataException();
            } else if (!super.isSubscribed(restAccount.getCustomer().getId())) {
                logger.error("####  Not Subscribe to Digital API Write ####");
                throw new IncorrectCredentialsException();
            } else if (!super.getIsServiceAgreementSigned(restAccount.getCustomer().getId(), false)) {
                logger.error("####  Service Agreement not signed ####");
                throw new IncorrectCredentialsException();
            } else if (!super.restValidator.validatePOS(restAccount)) {
                logger.error("#### PS: " + restAccount.getPointOfSale().getSerialNumber() + " is deactivated.");
                throw new InvalidDataException(RestCoreConstants.EMS_UNAVAILABLE);
            }
            
            String secretKey = restAccount.getSecretKey();
            
            RESTTokenQueryParamList param = new RESTTokenQueryParamList();
            param.setAccount(account);
            param.setSignature(signature);
            param.setSignatureVersion(signatureVersion);
            param.setTimestamp(timestamp);
            /* verify the payload by calling the method in encryptionService. */
            String hostName = this.restPropertyService.getPropertyValue(RestPropertyService.REST_DOMAIN_NAME, RestPropertyService.REST_DOMAIN_NAME_DEFAULT);
            if (!super.encryptionService.verifyHMACSignature(hostName, RestCoreConstants.HTTP_METHOD_GET, param.getSignature(), param, null, secretKey)) {
                throw new IncorrectCredentialsException();
            }
            
            /* in case of no issue, create a unique session token. */
            RestSessionToken tokenObj = super.restSessionTokenService.findRestSessionTokenByRestAccountId(restAccount.getId());
            if (tokenObj == null) {
                tokenObj = new RestSessionToken();
            }
            
            String sessionToken = null;
            
            Date currentTime = new Date();
            Date expiryDate = super.createExpiryDate(currentTime);
            
            synchronized (this) {
                /*
                 * if session token already exists in the DB, set the existing token.
                 * Otherwise, create the new session token.
                 */
                if (tokenObj.getExpiryDate() == null || tokenObj.getExpiryDate().before(currentTime)) {
                    while (true) {
                        sessionToken = RandomStringUtils.randomAlphanumeric(RestCoreConstants.SESSION_TOKEN_LENGTH);
                        int resultCount = super.restSessionTokenService.findCountBySessionToken(sessionToken);
                        if (resultCount == 0) {
                            break;
                        }
                    }
                    tokenObj.setRestAccount(restAccount);
                    tokenObj.setCreationDate(currentTime);
                    tokenObj.setSessionToken(sessionToken);
                    tokenObj.setExpiryDate(expiryDate);
                    tokenObj.setLastModifiedGmt(currentTime);
                    tokenObj.setLastModifiedByUserId(restAccount.getId());
                    super.restSessionTokenService.saveRestSessionToken(tokenObj);
                } else {
                    sessionToken = tokenObj.getSessionToken();
                    tokenObj.setExpiryDate(expiryDate);
                    tokenObj.setLastModifiedGmt(currentTime);
                    tokenObj.setLastModifiedByUserId(restAccount.getId());
                    super.restSessionTokenService.updateRestSessionToken(tokenObj);
                }
            }
            
            /* Initiate the response. */
            RESTToken response = new RESTToken();
            response.setSessionToken(sessionToken);
            logger.debug("#### END " + methodName + " ####");
            
            return response;
        } catch (ApplicationException ae) {
            throw ae;
        } catch (Exception e) {
            logger.error("#### Exception in " + methodName + " ####", e);
            throw new ApplicationException(e);
        }
    }
    
    @RequestMapping(method = RequestMethod.PUT)
    public void releaseToken(HttpServletResponse response, @RequestParam("Signature") String signature,
                             @RequestParam("SignatureVersion") String signatureVersion, @RequestParam("Timestamp") String timestamp,
                             @RequestParam("Token") String token) throws ApplicationException {
        
        String methodName = this.getClass().getName() + ".releaseToken()";
        logger.debug("#### START " + methodName + " ####");
        try {
            /* validate input parameters. */
            super.restValidator.validateTimestamp(timestamp, methodName);
            super.restValidator.validateSignatureVersion(signatureVersion);
            RestSessionToken tokenObj = super.restValidator.validateSessionToken(token, methodName);
            /* retrieve the RESTAccount information from DB to obtain the secret key. */
            RestAccount restAccount = super.restAccountService.findRestAccountByAccountName(tokenObj.getRestAccount().getAccountName());
            
            if (restAccount == null) {
                logger.error("#### No RESTAccount has been found in releaseToken() operation for the AccountName: "
                             + tokenObj.getRestAccount().getAccountName() + " ####");
                throw new InvalidDataException();
            } else if (!super.isSubscribed(restAccount.getCustomer().getId())) {
                logger.error("####  Not Subscribe to Digital API Write ####");
                throw new IncorrectCredentialsException();
            } else if (!super.getIsServiceAgreementSigned(restAccount.getCustomer().getId(), false)) {
                logger.error("####  Service Agreement not signed ####");
                throw new IncorrectCredentialsException();
            } else if (!super.restValidator.validatePOS(restAccount)) {
                logger.error("#### PS: " + restAccount.getPointOfSale().getSerialNumber() + " is deactivated.");
                throw new InvalidDataException(RestCoreConstants.EMS_UNAVAILABLE);
            }
            
            RESTTokenQueryParamList param = new RESTTokenQueryParamList();
            param.setToken(token);
            param.setSignature(signature);
            param.setSignatureVersion(signatureVersion);
            param.setTimestamp(timestamp);
            /* verify the payload by calling the method in encryptionService. */
            String hostName = this.restPropertyService.getPropertyValue(RestPropertyService.REST_DOMAIN_NAME, RestPropertyService.REST_DOMAIN_NAME_DEFAULT);
            if (!super.encryptionService.verifyHMACSignature(hostName, RestCoreConstants.HTTP_METHOD_PUT, param.getSignature(), param, null,
                                                             restAccount.getSecretKey())) {
                throw new IncorrectCredentialsException();
            }
            
            /* release the session token. */
            List<RestSessionToken> restSessionTokenList = super.restSessionTokenService.findRestSessionTokenBySessionToken(token);
            for (RestSessionToken restSessionToken : restSessionTokenList) {
                if (restSessionToken.getExpiryDate().after(new Date())) {
                    restSessionToken.setExpiryDate(new Date());
                    this.restSessionTokenService.saveRestSessionToken(restSessionToken);
                } else {
                    logger.debug("#### Session token already expired. (" + methodName + ") ####");
                }
            }
            
            logger.debug("#### END " + methodName + " ####");
            response.setStatus(RestCoreConstants.SUCCESS_RELEASE_TOKEN);
        } catch (ApplicationException ae) {
            throw ae;
        } catch (Exception e) {
            logger.error("#### Exception in " + methodName + " ####", e);
            throw new ApplicationException(e);
        }
        
    }
}
