package com.digitalpaytech.rest.controller;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.domain.Coupon;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.RestAccount;
import com.digitalpaytech.domain.RestSessionToken;
import com.digitalpaytech.dto.rest.RESTCoupon;
import com.digitalpaytech.dto.rest.RESTCouponCodes;
import com.digitalpaytech.dto.rest.RESTCouponQueryParamList;
import com.digitalpaytech.dto.rest.RESTCoupons;
import com.digitalpaytech.dto.rest.RESTMultiCouponsResult;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.DuplicateObjectException;
import com.digitalpaytech.exception.IncorrectCredentialsException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.RESTCouponPushException;
import com.digitalpaytech.rest.validator.RESTCouponValidator;
import com.digitalpaytech.service.CouponService;
import com.digitalpaytech.service.RestPropertyService;
import com.digitalpaytech.service.kpi.KPIListenerService;
import com.digitalpaytech.servlet.DPTHttpServletRequestWrapper;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.RestCoreConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;

@Controller
@RequestMapping(value = "/Coupons")
public class RESTCouponController extends RESTBaseController {
    private static Logger logger = Logger.getLogger(RESTCouponController.class);
    
    @Autowired
    private RESTCouponValidator restCouponValidator;
    
    @Autowired
    private CouponService couponService;
    
    @Autowired
    private KPIListenerService kpiListenerService;
    
    /**
     * Single Coupon Retrieval
     * URL: GET https://www.intellapay.com/REST/Coupons/[CouponCode]
     * Use @RequestMapping(...value) & @PathVariable to inject the value of URI parameter.
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{couponCode}", produces = WebCoreConstants.CONTENT_TYPE_APPLICATION_XML)
    @ResponseBody
    public RESTCoupon getCoupon(HttpServletRequest request, HttpServletResponse response, @PathVariable String couponCode,
                                @RequestParam("Signature") String signature, @RequestParam("SignatureVersion") String signatureVersion,
                                @RequestParam("Timestamp") String timestamp, @RequestParam("Token") String token) throws ApplicationException {
        
        StringBuilder bdr = null;
        String methodName = ".getCoupon()";
        if (logger.isDebugEnabled()) {
            bdr = new StringBuilder();
            bdr.append("#### START ").append(this.getClass().getName()).append(methodName);
            logger.debug(bdr.toString());
        }
                                
        validateParametersPayloadAndExtendSession(createRESTCouponQueryParamList(signature, signatureVersion, timestamp, token, couponCode),
                                                  RestCoreConstants.HTTP_METHOD_GET, null, methodName, false);
        restCouponValidator.validateCouponCode(couponCode, methodName);
        
        // Load existing Coupon object.
        Coupon coupon = getCouponObject(token, couponCode);
        RESTCoupon restCoupon = createRESTCoupon(coupon);
        
        response.setStatus(RestCoreConstants.SUCCESS);
        
        if (logger.isDebugEnabled()) {
            bdr.replace(0, bdr.length(), "");
            bdr.append("#### END ").append(this.getClass().getName()).append(methodName);
            logger.debug(bdr.toString());
        }
        return restCoupon;
    }
    
    /**
     * Single Coupon Creation
     * URL: PUT https://www.intellapay.com/REST/Coupons/[CouponCode]
     */
    @RequestMapping(method = RequestMethod.POST, value = "/{couponCode}", produces = WebCoreConstants.CONTENT_TYPE_APPLICATION_XML, consumes = WebCoreConstants.CONTENT_TYPE_APPLICATION_XML)
    public void createCoupon(HttpServletRequest request, HttpServletResponse response, @PathVariable String couponCode, @RequestBody RESTCoupon restCoupon,
                             @RequestParam("Signature") String signature, @RequestParam("SignatureVersion") String signatureVersion,
                             @RequestParam("Timestamp") String timestamp, @RequestParam("Token") String token) throws ApplicationException {
        
        StringBuilder bdr = null;
        String methodName = ".createCoupon()";
        if (logger.isDebugEnabled()) {
            bdr = new StringBuilder();
            bdr.append("#### START ").append(this.getClass().getName()).append(methodName);
            logger.debug(bdr.toString());
        }
        
        RestAccount restAcct = validateParametersPayloadAndExtendSession(createRESTCouponQueryParamList(signature, signatureVersion, timestamp, token,
                                                                                                        couponCode), RestCoreConstants.HTTP_METHOD_POST,
                                                                         ((DPTHttpServletRequestWrapper) request).getBody(), methodName, true);
        validateSameValues(couponCode, restCoupon.getCouponCode(), methodName);
        restCouponValidator.validateCouponCode(couponCode, methodName);
        Customer customer = getCustomer(token);
        restCouponValidator.validateCreateCoupon(restCoupon, methodName, customer);
        if(!restCouponValidator.checkCouponCodeUnique(restCoupon, methodName, customer)){
        	logger.error("#### <Coupon> " + restCoupon.getCouponCode() + " is not unique and cannot be created.");
        	throw new RESTCouponPushException(RestCoreConstants.FAILURE_OBJECT_ALREADY_EXISTS);
        }
        
        try {            
            /* check if the location is the lowest location with pay station. */
            Location loc = restCouponValidator.validateLocation(restCoupon.getLocation(), customer.getId());
            
            /* Check if the coupon already exists */
            Coupon coupon  = new Coupon();
            coupon.setCoupon(restCoupon.getCouponCode().toUpperCase());
            coupon.setCustomer(customer);                        
            coupon = populateCoupon(coupon, loc, restCoupon, restAcct, methodName);
            
            this.couponService.saveOrUpdateCoupon(coupon);
        } catch (Exception e) {
            logger.error("#### Exception in " + this.getClass().getName() + methodName, e);
            response.setStatus(RestCoreConstants.FAILURE_INVALID_PARAMETER);
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        
        response.setStatus(RestCoreConstants.SUCCESS);
        
        if (logger.isDebugEnabled()) {
            bdr.replace(0, bdr.length(), "");
            bdr.append("#### END ").append(this.getClass().getName()).append(methodName);
            logger.debug(bdr.toString());
        }
    }
    
    /**
     * Single Coupon Update
     * URL: PUT https://www.intellapay.com/REST/Coupons/[CouponCode]
     */
    @RequestMapping(method = RequestMethod.PUT, value = "/{couponCode}", produces = WebCoreConstants.CONTENT_TYPE_APPLICATION_XML, consumes = WebCoreConstants.CONTENT_TYPE_APPLICATION_XML)
    public void updateCoupon(HttpServletResponse response, HttpServletRequest request, @PathVariable String couponCode, @RequestBody RESTCoupon restCoupon,
                             @RequestParam("Signature") String signature, @RequestParam("SignatureVersion") String signatureVersion,
                             @RequestParam("Timestamp") String timestamp, @RequestParam("Token") String token) throws ApplicationException {
        
        StringBuilder bdr = null;
        String methodName = ".updateCoupon()";
        if (logger.isDebugEnabled()) {
            bdr = new StringBuilder();
            bdr.append("#### START ").append(this.getClass().getName()).append(methodName);
            logger.debug(bdr.toString());
        }
        
        RestAccount restAcct = validateParametersPayloadAndExtendSession(createRESTCouponQueryParamList(signature, signatureVersion, timestamp, token,
                                                                                                        couponCode), RestCoreConstants.HTTP_METHOD_PUT,
                                                                         ((DPTHttpServletRequestWrapper) request).getBody(), methodName, true);
        validateSameValues(couponCode, restCoupon.getCouponCode(), methodName);
        restCouponValidator.validateCouponCode(couponCode, methodName);
        restCouponValidator.validateUpdateCoupon(restCoupon, methodName);
        
        Customer customer = getCustomer(token);
        /* check if the location is the lowest location with pay station. */
        Location loc = restCouponValidator.validateLocation(restCoupon.getLocation(), customer.getId());
        // Load existing Coupon object.
        Coupon coupon = getCouponObject(token, restCoupon.getCouponCode());
        coupon = populateCouponForUpdate(coupon, loc, restCoupon, restAcct, methodName);
        couponService.saveOrUpdateCoupon(coupon);
        
        response.setStatus(RestCoreConstants.SUCCESS);
        
        if (logger.isDebugEnabled()) {
            bdr.replace(0, bdr.length(), "");
            bdr.append("#### END ").append(this.getClass().getName()).append(methodName);
            logger.debug(bdr.toString());
        }
    }
    
    /**
     * Single Coupon Deletion
     * URL: DELETE https://www.intellapay.com/REST/Coupons/[CouponCode]
     * Use @PathVariable to inject the value of URI parameter.
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/{couponCode}", produces = WebCoreConstants.CONTENT_TYPE_APPLICATION_XML)
    public void deleteCoupon(HttpServletResponse response, HttpServletRequest request, @PathVariable String couponCode,
                             @RequestParam("Signature") String signature, @RequestParam("SignatureVersion") String signatureVersion,
                             @RequestParam("Timestamp") String timestamp, @RequestParam("Token") String token) throws ApplicationException {
        
        StringBuilder bdr = null;
        String methodName = ".deleteCoupon()";
        if (logger.isDebugEnabled()) {
            bdr = new StringBuilder();
            bdr.append("#### START ").append(this.getClass().getName()).append(methodName);
            logger.debug(bdr.toString());
        }
        validateParametersPayloadAndExtendSession(createRESTCouponQueryParamList(signature, signatureVersion, timestamp, token, couponCode),
                                                  RestCoreConstants.HTTP_METHOD_DELETE, null, methodName, true);
        restCouponValidator.validateCouponCode(couponCode, methodName);
        // Load existing Coupon object, populate RESTCoupon and delete it from the database.
        Coupon coupon = getCouponObject(token, couponCode);
        couponService.deleteCoupon(coupon);
        
        response.setStatus(RestCoreConstants.SUCCESS);
        
        if (logger.isDebugEnabled()) {
            bdr.replace(0, bdr.length(), "");
            bdr.append("#### END ").append(this.getClass().getName()).append(methodName);
            logger.debug(bdr.toString());
        }
    }
    
    /**
     * Multi-Coupon Retrieval
     * URL: GET https://www.intellapay.com/REST/Coupons
     * 
     * @RequestMapping - 'produces' annotations indicate that getCoupons method returns (or produces) an XML formatted string. Since there is no request body,
     *                 no need to define 'consumes'.
     */
    @RequestMapping(method = RequestMethod.GET, produces = WebCoreConstants.CONTENT_TYPE_APPLICATION_XML)
    @ResponseBody
    public RESTCoupons getCoupons(HttpServletRequest request, HttpServletResponse response, @RequestParam("Signature") String signature,
                                  @RequestParam("SignatureVersion") String signatureVersion, @RequestParam("Timestamp") String timestamp,
                                  @RequestParam("Token") String token) throws ApplicationException {
        
        StringBuilder bdr = null;
        String methodName = ".getCoupons()";
        if (logger.isDebugEnabled()) {
            bdr = new StringBuilder();
            bdr.append("#### START ").append(this.getClass().getName()).append(methodName);
            logger.debug(bdr.toString());
        }
        
        String couponCodes = request.getParameter("CouponCodes");
        
        validateParametersPayloadAndExtendSession(createRESTCouponsQueryParamList(signature, signatureVersion, timestamp, token, couponCodes),
                                                  RestCoreConstants.HTTP_METHOD_GET, null, methodName, false);
                
        RESTCoupons coupons = new RESTCoupons();
        if (StringUtils.isNotBlank(couponCodes)) {
            List<String> codes = parseCouponCodes(couponCodes);
            restCouponValidator.validateGetCoupons(codes, methodName);
            
            int notFoundSize = 0;
            List<RESTCoupon> list = new ArrayList<RESTCoupon>(codes.size());
            for (String couponCode : codes) {
                Coupon coupon = null;
                try {
                    coupon = getCouponObject(token, couponCode);
                } catch (RESTCouponPushException rcpe) {
                    if (StringUtils.isNotBlank(rcpe.getMessage()) && rcpe.getMessage().indexOf(String.valueOf(RestCoreConstants.FAILURE_NOT_FOUND)) != -1) {
                        notFoundSize++;
                        coupon = getInvalidCouponObject(token, couponCode);
                    } else {
                        throw rcpe;
                    }
                }
                list.add(createRESTCoupon(coupon));
            }
            if (notFoundSize == codes.size()) {
                logger.error("getCoupons - didn't find any Coupon record, codes are: " + codes);
                response.setStatus(RestCoreConstants.FAILURE_NOT_FOUND);
                throw new RESTCouponPushException(RestCoreConstants.FAILURE_NOT_FOUND);
            }
            
            coupons.setCoupons(list);
        } else {
            int customerId = getCustomer(token).getId();
            int totalCouponSize = couponService.findTotalCouponSize(customerId);
            // Validate total coupon size.
            restCouponValidator.validateGetAllCouponsSize(totalCouponSize, methodName);
            
            List<Coupon> couponObjs = couponService.findCouponsByCustomerId(customerId);
            List<RESTCoupon> list = new ArrayList<RESTCoupon>(couponObjs.size());
            for (Coupon c : couponObjs) {
                list.add(createRESTCoupon(c));
            }
            coupons.setCoupons(list);
        }
        
        response.setStatus(RestCoreConstants.SUCCESS);
        
        if (logger.isDebugEnabled()) {
            bdr.replace(0, bdr.length(), "");
            bdr.append("#### END ").append(this.getClass().getName()).append(methodName).append(", RESTCoupons size: ").append(coupons.getCoupons().size());
            logger.debug(bdr.toString());
        }
        return coupons;
    }
    
    /**
     * Multi-Coupon Creation
     * URL: POST https://www.intellapay.com/REST/Coupons
     * 
     * @RequestMapping - produces and consumes annotations indicate that the createCoupons() method accepts (or consumes) XML in its body parameter and returns
     *                 (or produces) a XML formatted string.
     */
    @RequestMapping(method = RequestMethod.POST, produces = WebCoreConstants.CONTENT_TYPE_APPLICATION_XML, consumes = WebCoreConstants.CONTENT_TYPE_APPLICATION_XML)
    @ResponseBody
    public RESTMultiCouponsResult createCoupons(HttpServletRequest request, HttpServletResponse response, @RequestBody RESTCoupons restCoupons,
                                                @RequestParam("Signature") String signature, @RequestParam("SignatureVersion") String signatureVersion,
                                                @RequestParam("Timestamp") String timestamp, @RequestParam("Token") String token) throws ApplicationException {
        
        StringBuilder bdr = null;
        String methodName = ".createCoupons()";
        if (logger.isDebugEnabled()) {
            bdr = new StringBuilder();
            bdr.append("#### START ").append(this.getClass().getName()).append(methodName);
            logger.debug(bdr.toString());
        }
        
        if (kpiListenerService.getKPIService() != null) {
            kpiListenerService.getKPIService().addDigitalAPICouponRecord(restCoupons.getCoupons().size());
        }
        
        RestAccount restAcct = validateParametersPayloadAndExtendSession(createRESTCouponQueryParamList(signature, signatureVersion, timestamp, token, null),
                                                                         RestCoreConstants.HTTP_METHOD_POST,
                                                                         ((DPTHttpServletRequestWrapper) request).getBody(), methodName, true);
        Customer cust = getCustomer(token);
        restCouponValidator.validateCreateCoupons(restCoupons, methodName, cust);
        
        
        // Get location from first coupon and check if the location is the lowest location with pay station.
        Location loc = restCouponValidator.validateLocation(restCoupons.getCoupons().get(0).getLocation(), cust.getId());
        // Save coupons information.
        List<String> oks = new ArrayList<String>();
        List<String> faileds = new ArrayList<String>();
        
        for (RESTCoupon restCoupon : restCoupons.getCoupons()) {
            if(!restCouponValidator.checkCouponCodeUnique(restCoupon, methodName, cust)){
            	logger.error("#### <Coupon> " + restCoupon.getCouponCode() + " is not unique and cannot be created.");
            	continue;
            }
            Coupon coupon = new Coupon();
            coupon.setCustomer(cust);
            coupon.setCoupon(restCoupon.getCouponCode().toUpperCase());
            coupon = populateCoupon(coupon, loc, restCoupon, restAcct, methodName);            
            if(!restCouponValidator.checkCouponCodeUnique(restCoupon, methodName, cust)){
            	faileds.add(coupon.getCoupon());
           		logger.error("#### <Coupon> " + restCoupon.getCouponCode() + " is not unique and cannot be created.");
           		continue;
            }            
            try {
                couponService.saveOrUpdateCoupon(coupon);
                oks.add(coupon.getCoupon());
            } catch (DuplicateObjectException doe) {
                logger.error("#### Exception in " + this.getClass().getName() + methodName, doe);
                faileds.add(coupon.getCoupon());
            }
        }
        
        if (logger.isDebugEnabled()) {
            bdr.replace(0, bdr.length(), "");
            bdr.append("#### END ").append(this.getClass().getName()).append(methodName);
            logger.debug(bdr.toString());
        }
             
        RESTMultiCouponsResult result = buildRESTMultiCouponsResult(oks, faileds);
        response.setStatus(RestCoreConstants.SUCCESS);
        return result;
    }
    
    /**
     * Multi-Coupon Update
     * URL: PUT https://www.intellapay.com/REST/Coupons
     * 
     * @RequestMapping - produces and consumes annotations indicate that the updateCoupons() method accepts (or consumes) XML in its body parameter and returns
     *                 (or produces) a
     *                 XML formatted string.
     */
    @RequestMapping(method = RequestMethod.PUT, produces = WebCoreConstants.CONTENT_TYPE_APPLICATION_XML, consumes = WebCoreConstants.CONTENT_TYPE_APPLICATION_XML)
    @ResponseBody
    public RESTMultiCouponsResult updateCoupons(HttpServletRequest request, HttpServletResponse response, @RequestBody RESTCoupons restCoupons,
                                                @RequestParam("Signature") String signature, @RequestParam("SignatureVersion") String signatureVersion,
                                                @RequestParam("Timestamp") String timestamp, @RequestParam("Token") String token) throws ApplicationException {
        
        StringBuilder bdr = null;
        String methodName = ".updateCoupons()";
        if (logger.isDebugEnabled()) {
            bdr = new StringBuilder();
            bdr.append("#### START ").append(this.getClass().getName()).append(methodName);
            logger.debug(bdr.toString());
        }
        
        RestAccount restAcct = validateParametersPayloadAndExtendSession(createRESTCouponQueryParamList(signature, signatureVersion, timestamp, token, null),
                                                                         RestCoreConstants.HTTP_METHOD_PUT,
                                                                         ((DPTHttpServletRequestWrapper) request).getBody(), methodName, true);
        restCouponValidator.validateUpdateCoupons(restCoupons, methodName);
        
        Customer cust = getCustomer(token);
        // Get location from first coupon and check if the location is the lowest location with pay station.
        Location loc = restCouponValidator.validateLocation(restCoupons.getCoupons().get(0).getLocation(), cust.getId());
        // Save coupons information.
        List<String> oks = new ArrayList<String>();
        List<String> faileds = new ArrayList<String>();
        int notFoundSize = 0;
        List<String> invalidCodes = new ArrayList<String>();
        
        for (RESTCoupon restCoupon : restCoupons.getCoupons()) {
            Coupon coupon = null;
            try {
                coupon = getCouponObject(token, restCoupon.getCouponCode());
                coupon = populateCouponForUpdate(coupon, loc, restCoupon, restAcct, methodName);
                
                couponService.saveOrUpdateCoupon(coupon);
                oks.add(coupon.getCoupon());
            } catch (RESTCouponPushException rcpe) {
                if (StringUtils.isNotBlank(rcpe.getMessage()) && rcpe.getMessage().indexOf(String.valueOf(RestCoreConstants.FAILURE_NOT_FOUND)) != -1) {
                    notFoundSize++;
                    faileds.add(restCoupon.getCouponCode());
                    invalidCodes.add(restCoupon.getCouponCode());
                } else {
                    throw rcpe;
                }
            } catch (Exception e) {
                logger.error("#### Exception in " + this.getClass().getName() + methodName, e);
                faileds.add(coupon.getCoupon());
            }
        }
        
        if (notFoundSize == restCoupons.getCoupons().size()) {
            logger.error("updateCoupons - didn't find any Coupon record, codes are: " + invalidCodes);
            response.setStatus(RestCoreConstants.FAILURE_NOT_FOUND);
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_NOT_FOUND);
        }
        
        if (logger.isDebugEnabled()) {
            bdr.replace(0, bdr.length(), "");
            bdr.append("#### END ").append(this.getClass().getName()).append(methodName);
            logger.debug(bdr.toString());
        }
        
        RESTMultiCouponsResult result = buildRESTMultiCouponsResult(oks, faileds);
        response.setStatus(RestCoreConstants.SUCCESS);
        return result;
    }
    
    /**
     * Multi-Coupon Deletion
     * URL: DELETE https://www.intellapay.com/REST/Coupons
     * 
     * @ConsumeMime and @ProduceMime annotations indicate that the deleteCoupons() method accepts (or consumes) XML in its body parameter and returns (or
     *              produces) an XML formatted string.
     */
    @RequestMapping(method = RequestMethod.DELETE, produces = WebCoreConstants.CONTENT_TYPE_APPLICATION_XML, consumes = WebCoreConstants.CONTENT_TYPE_APPLICATION_XML)
    @ResponseBody
    public RESTMultiCouponsResult deleteCoupons(HttpServletResponse response, HttpServletRequest request, @RequestBody RESTCouponCodes restCouponCodes,
                                                @RequestParam("Signature") String signature, @RequestParam("SignatureVersion") String signatureVersion,
                                                @RequestParam("Timestamp") String timestamp, @RequestParam("Token") String token) throws ApplicationException {
        
        StringBuilder bdr = null;
        String methodName = ".deleteCoupon()";
        if (logger.isDebugEnabled()) {
            bdr = new StringBuilder();
            bdr.append("#### START ").append(this.getClass().getName()).append(methodName);
            logger.debug(bdr.toString());
        }
        validateParametersPayloadAndExtendSession(createRESTCouponQueryParamList(signature, signatureVersion, timestamp, token, null),
                                                  RestCoreConstants.HTTP_METHOD_DELETE, ((DPTHttpServletRequestWrapper) request).getBody(), methodName, true);
        restCouponValidator.validateDeleteCoupons(restCouponCodes, methodName);
        // Save coupons information.
        List<String> oks = new ArrayList<String>();
        List<String> faileds = new ArrayList<String>();
        int notFoundSize = 0;
        
        for (String couponCode : restCouponCodes.getCouponCodes()) {
            try {
                Coupon coupon = getCouponObject(token, couponCode);
                couponService.deleteCoupon(coupon);
                oks.add(coupon.getCoupon());
            } catch (RESTCouponPushException rcpe) {
                if (StringUtils.isNotBlank(rcpe.getMessage()) && rcpe.getMessage().indexOf(String.valueOf(RestCoreConstants.FAILURE_NOT_FOUND)) != -1) {
                    notFoundSize++;
                    faileds.add(couponCode);
                } else {
                    throw rcpe;
                }
            } catch (Exception e) {
                logger.error("#### Exception in " + this.getClass().getName() + methodName, e);
                faileds.add(couponCode);
            }
        }
        
        if (notFoundSize == restCouponCodes.getCouponCodes().size()) {
            logger.error("deleteCoupons - didn't find any Coupon record, codes are: " + restCouponCodes.getCouponCodes());
            response.setStatus(RestCoreConstants.FAILURE_NOT_FOUND);
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_NOT_FOUND);
        }
        
        if (logger.isDebugEnabled()) {
            bdr.replace(0, bdr.length(), "");
            bdr.append("#### END ").append(this.getClass().getName()).append(methodName);
            logger.debug(bdr.toString());
        }
        
        response.setStatus(RestCoreConstants.SUCCESS);
        RESTMultiCouponsResult result = buildRESTMultiCouponsResult(oks, faileds);
        return result;
    }
    
    private RestAccount validateParametersPayloadAndExtendSession(RESTCouponQueryParamList param, String httpMethod, String inputStreamString, String methodName, boolean checkMigration)
            throws ApplicationException {
        /* validate input parameters. */
        restCouponValidator.validateTimestamp(param.getTimestamp(), methodName);
        restCouponValidator.validateSignatureVersion(param.getSignatureVersion());
        RestSessionToken tokenObj = this.restCouponValidator.validateSessionToken(param.getToken(), methodName);
        
        /* retrieve the RESTAccount information from DB to obtain the secret key. */
        RestAccount restAccount = this.restAccountService.findRestAccountByAccountName(tokenObj.getRestAccount().getAccountName());
        if (restAccount == null) {
            StringBuilder bdr = new StringBuilder();
            bdr.append("#### No RESTAccount has been found in ").append(methodName).append(" operation for the AccountName: ")
                    .append(tokenObj.getRestAccount().getAccountName());
            logger.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_TOKEN);
        } else if (!super.isSubscribed(restAccount.getCustomer().getId())) {
            logger.error("####  Not Subscribe to Digital API Write ####");
            throw new IncorrectCredentialsException();
        } else if (!super.getIsServiceAgreementSigned(restAccount.getCustomer().getId(), checkMigration)) {
            logger.error("####  Service Agreement not signed ####");
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        } else if (!super.restValidator.validatePOS(restAccount)) {            	
            logger.error("#### PS: " + restAccount.getPointOfSale().getSerialNumber() + " is deactivated.");
            throw new InvalidDataException(RestCoreConstants.EMS_UNAVAILABLE);            	
        }
        
        /* Extend session expiry date. */
        try {
            super.extendToken(tokenObj, restAccount.getId());
        } catch (Exception e) {
            logger.error(e);
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_TOKEN);
        }
        
        /* verify the payload by calling the method in encryptionAppService. */
        String hostName = this.restPropertyService.getPropertyValue(RestPropertyService.REST_DOMAIN_NAME, RestPropertyService.REST_DOMAIN_NAME_DEFAULT);
        if (!super.encryptionService.verifyHMACSignature(hostName, httpMethod, param.getSignature(), param, inputStreamString, restAccount.getSecretKey())) {
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_TOKEN);
        }
        return restAccount;
    }
    
    private RESTCouponQueryParamList createRESTCouponQueryParamList(String signature, String signatureVersion, String timestamp, String token, String couponCode) {
        RESTCouponQueryParamList param = new RESTCouponQueryParamList();
        param.setToken(token);
        param.setSignature(signature);
        param.setSignatureVersion(signatureVersion);
        param.setTimestamp(timestamp);
        param.setCouponCode(couponCode);
        return param;
    }
    
    private RESTCouponQueryParamList createRESTCouponsQueryParamList(String signature, String signatureVersion, String timestamp, String token, String couponCodes) {
        RESTCouponQueryParamList param = new RESTCouponQueryParamList();
        param.setToken(token);
        param.setSignature(signature);
        param.setSignatureVersion(signatureVersion);
        param.setTimestamp(timestamp);
        param.setCouponCodes(couponCodes);
        return param;
    }    
    
    private RESTMultiCouponsResult buildRESTMultiCouponsResult(List<String> oks, List<String> faileds) {
    	
        RESTMultiCouponsResult result = new RESTMultiCouponsResult();
        result.setCouponCodes(oks);
        result.setFailedCouponCodes(faileds);
        
        return result;
    }
        
    private RESTCoupon createRESTCoupon(Coupon coupon) {
        String locName = null;
        if (coupon.getLocation() == null) {
            locName = "";
        } else {
            locName = coupon.getLocation().getName();
        }
        RESTCoupon response = new RESTCoupon();
        response.setCouponCode(coupon.getCoupon());
        response.setDescription(coupon.getDescription());
        if (coupon.getDollarDiscountAmount() == 0 && coupon.getPercentDiscount() == 0) {
            response.setDiscountType("");
        } else if (coupon.getDollarDiscountAmount() == 0) {
            response.setDiscountType(WebCoreConstants.COUPON_DISCOUNT_TYPE_PERCENT);
            response.setDiscountPercent(new BigDecimal(coupon.getPercentDiscount()));
        } else {
            response.setDiscountType(WebCoreConstants.COUPON_DISCOUNT_TYPE_DOLLAR);
            response.setDiscountAmt(CardProcessingUtil.getCentsAmountInDollars(coupon.getDollarDiscountAmount()));
        }
        response.setEndDate(getDateDisplay(coupon.getEndDateLocal()));
        response.setStartDate(getDateDisplay(coupon.getStartDateLocal()));
        response.setLocation(locName);
        response.setOperatingMode(createOperatingMode(coupon));
        response.setSpaceRange(coupon.getSpaceRange());
        if (coupon.getNumberOfUsesRemaining() == null || coupon.getNumberOfUsesRemaining() == Short.MAX_VALUE) {
            response.setUses(WebCoreConstants.UNLIMITED);
        } else if (coupon.getNumberOfUsesRemaining() != null) {
            response.setUses(String.valueOf(coupon.getNumberOfUsesRemaining()));
        }
        
        if (coupon.getValidForNumOfDay() > 0) {
            response.setValidForNumOfDay(WebCoreConstants.YES);
        } else {
            response.setValidForNumOfDay(WebCoreConstants.NO);
        }
        return response;
    }
    
    private Coupon populateCoupon(Coupon coupon, Location location, RESTCoupon request, RestAccount restAccount, String methodName) {
        coupon.setDescription(request.getDescription());
        if (request.getDiscountType().equalsIgnoreCase(WebCoreConstants.COUPON_DISCOUNT_TYPE_PERCENT)) {
            coupon.setPercentDiscount(request.getDiscountPercent().shortValue());
            coupon.setDollarDiscountAmount(0);
        } else {
            coupon.setDollarDiscountAmount(request.getDiscountAmt().multiply(new BigDecimal(100)).intValue());
            coupon.setPercentDiscount((short) 0);
        }
        
        try {
            String start;
            if (StringUtils.isBlank(request.getStartDate()) || request.getStartDate().equalsIgnoreCase(WebCoreConstants.NOT_APPLICABLE)) {
                start = WebCoreConstants.COUPON_N_A_START;
            } else {
              
            	start = request.getStartDate();
            	
                start = WebCoreUtil.appendHoursMinutesIfNecessary(start, "00", "00");
            	
            }
            coupon.setStartDateLocal(DateUtil.convertFromRESTDateString(start));
            
            String end;
            if (StringUtils.isBlank(request.getEndDate()) || request.getEndDate().equalsIgnoreCase(WebCoreConstants.NOT_APPLICABLE)) {
                end = WebCoreConstants.COUPON_N_A_END;
            } else {
                end = request.getEndDate();
                end = WebCoreUtil.appendHoursMinutesIfNecessary(end, "23", "59");
            }
            coupon.setEndDateLocal(DateUtil.convertFromRESTDateString(end));
        } catch (InvalidDataException e) {
            logger.error("#### Exception in " + this.getClass().getName() + methodName, e);
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        
        if (request.getOperatingMode().indexOf(WebCoreConstants.COUPON_CODE_PAY_AND_DISPLAY) != -1
            || request.getOperatingMode().indexOf(WebCoreConstants.COUPON_CODE_PAY_BY_LICENSE_PLATE) != -1) {
            coupon.setIsPndEnabled(true);
        } else {
            coupon.setIsPndEnabled(false);
        }
        if (request.getOperatingMode().indexOf(WebCoreConstants.COUPON_CODE_PAY_BY_SPACE) != -1) {
            coupon.setIsPbsEnabled(true);
        } else {
            coupon.setIsPbsEnabled(false);
        }
        
        if (StringUtils.isBlank(request.getUses())) {
            coupon.setNumberOfUsesRemaining(Short.MAX_VALUE);
        } else {
            coupon.setNumberOfUsesRemaining(Short.parseShort(request.getUses()));
        }
        
        coupon.setSpaceRange(request.getSpaceRange());
        coupon.setLocation(location);
        
        if (StringUtils.isBlank(request.getValidForNumOfDay()) || request.getValidForNumOfDay().equalsIgnoreCase(WebCoreConstants.NO)) {
            coupon.setValidForNumOfDay(0);
            coupon.setSingleUseDate(null);
        } else {
            coupon.setValidForNumOfDay(1);
        }
        coupon.setLastModifiedGmt(new Date());
        coupon.setLastModifiedByUserId(restAccount.getId());
        return coupon;
    }    
    
    private Coupon populateCouponForUpdate(Coupon coupon, Location location, RESTCoupon request, RestAccount restAccount, String methodName) {
    	if(request.getDescription() != null){
    		coupon.setDescription(request.getDescription());
    	}
        if (WebCoreConstants.COUPON_DISCOUNT_TYPE_PERCENT.equalsIgnoreCase(request.getDiscountType())) {
        	if(request.getDiscountPercent() != null){
        		coupon.setPercentDiscount(request.getDiscountPercent().shortValue());
        	}
            coupon.setDollarDiscountAmount(0);
        } else if (WebCoreConstants.COUPON_DISCOUNT_TYPE_DOLLAR.equalsIgnoreCase(request.getDiscountType())){
        	if(request.getDiscountAmt() != null){
        		coupon.setDollarDiscountAmount(request.getDiscountAmt().multiply(new BigDecimal(100)).intValue());
        	}
            coupon.setPercentDiscount((short) 0);
        }
        
        try {
        	if(request.getStartDate() != null){
        		String start;
        		if (StringUtils.isBlank(request.getStartDate()) || request.getStartDate().equalsIgnoreCase(WebCoreConstants.NOT_APPLICABLE)) {
	                start = WebCoreConstants.COUPON_N_A_START;
	            } else {
	                start = request.getStartDate();
	                start = WebCoreUtil.appendHoursMinutesIfNecessary(start, "00", "00");
	            }
	            coupon.setStartDateLocal(DateUtil.convertFromRESTDateString(start));
	        }
	        if(request.getEndDate() != null){
	            String end;
	            if (StringUtils.isBlank(request.getEndDate()) || request.getEndDate().equalsIgnoreCase(WebCoreConstants.NOT_APPLICABLE)) {
	                end = WebCoreConstants.COUPON_N_A_END;
	            } else {
	                end = request.getEndDate();
	                end = WebCoreUtil.appendHoursMinutesIfNecessary(end, "23", "59");
	            }
	            coupon.setEndDateLocal(DateUtil.convertFromRESTDateString(end));
        	}
        } catch (InvalidDataException e) {
            logger.error("#### Exception in " + this.getClass().getName() + methodName, e);
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        
        if(request.getOperatingMode() != null){
	        if (request.getOperatingMode().indexOf(WebCoreConstants.COUPON_CODE_PAY_AND_DISPLAY) != -1
	            || request.getOperatingMode().indexOf(WebCoreConstants.COUPON_CODE_PAY_BY_LICENSE_PLATE) != -1) {
	            coupon.setIsPndEnabled(true);
	        } else {
	            coupon.setIsPndEnabled(false);
	        }
	        if (request.getOperatingMode().indexOf(WebCoreConstants.COUPON_CODE_PAY_BY_SPACE) != -1) {
	            coupon.setIsPbsEnabled(true);
	        } else {
	            coupon.setIsPbsEnabled(false);
	        }
        }        
        
        if(request.getUses() != null){
	        if (StringUtils.isBlank(request.getUses())) {
	            coupon.setNumberOfUsesRemaining(Short.MAX_VALUE);
	        } else {
	            coupon.setNumberOfUsesRemaining(Short.parseShort(request.getUses()));
	        }
        }
        
        if(request.getSpaceRange() != null){
        	coupon.setSpaceRange(request.getSpaceRange());
        }
        
        if(request.getLocation() != null){
        	coupon.setLocation(location);
        }
                
        if(request.getValidForNumOfDay() != null){
	        if (StringUtils.isBlank(request.getValidForNumOfDay()) || request.getValidForNumOfDay().equalsIgnoreCase(WebCoreConstants.NO)) {        	
	            coupon.setValidForNumOfDay(0);
	            coupon.setSingleUseDate(null);        	
	        } else {
	            coupon.setValidForNumOfDay(1);
	        }
        }
        coupon.setLastModifiedGmt(new Date());
        coupon.setLastModifiedByUserId(restAccount.getId());
        return coupon;
    }
    
    private String createOperatingMode(Coupon coupon) {
        StringBuilder bdr = new StringBuilder();
        if (coupon.isIsPbsEnabled()) {
            // PAYSTATION_CONTACT_URL_SEPARATOR) = ','
            bdr.append(WebCoreConstants.COUPON_CODE_PAY_BY_SPACE).append(WebCoreConstants.PAYSTATION_CONTACT_URL_SEPARATOR);
        }
        if (coupon.isIsPndEnabled()) {
            // ROOT_PATH = '/'
            bdr.append(WebCoreConstants.COUPON_CODE_PAY_AND_DISPLAY).append(WebCoreConstants.ROOT_PATH)
                    .append(WebCoreConstants.COUPON_CODE_PAY_BY_LICENSE_PLATE);
        }
        String mode = bdr.toString();
        if (mode.endsWith(WebCoreConstants.PAYSTATION_CONTACT_URL_SEPARATOR)) {
            mode = mode.substring(0, mode.length() - 1);
        }
        return mode;
    }
    
    private Coupon getCouponObject(String sessionToken, String couponCode) {
        int custId = getCustomer(sessionToken).getId();
        Coupon c = couponService.findCouponByCustomerIdAndCouponCode(custId, couponCode);
        if (c == null) {
            StringBuilder bdr = new StringBuilder();
            bdr.append("#### No Coupon has been found in getCouponObject() operation for couponCode: ").append(couponCode).append(", customer id: ");
            bdr.append(custId);
            logger.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_NOT_FOUND);
        }
        return c;
    }
    
    private Coupon getInvalidCouponObject(String sessionToken, String couponCode) {
        Coupon coupon = new Coupon();
        coupon.setCoupon(couponCode);
        coupon.setCustomer(getCustomer(sessionToken));
        coupon.setNumberOfUsesRemaining((short) -1);
        return coupon;
    }
    
    private Customer getCustomer(String sessionToken) {
        RestSessionToken tokenObj = this.restCouponValidator.validateSessionToken(sessionToken, "getCustomerId");
        return tokenObj.getRestAccount().getCustomer();
    }
    
    private void validateSameValues(String str1, String str2, String methodName) {
        StringBuilder bdr = null;
        if (StringUtils.isBlank(str1) || StringUtils.isBlank(str2)) {
            bdr = new StringBuilder();
            bdr.append("#### ").append(methodName).append(" variable 1: ").append(str1).append(" or variable 2: ").append(str2)
                    .append(" is null or empty. ###");
            logger.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        if (!str1.equalsIgnoreCase(str2)) {
            bdr = new StringBuilder();
            bdr.append("#### ").append(methodName).append(" variable 1: ").append(str1).append(" and variable 2: ").append(str2)
                    .append(" should be the same. ###");
            logger.error(bdr.toString());
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
    }
    
    /**
     * @param couponCode
     *            String, separated by comma, e.g. 11111, 22222,33333, 44444
     * @return List<String> Coupon code String values
     */
    private List<String> parseCouponCodes(String couponCodes) {
        String decodedCodes = null;
        try {
            decodedCodes = URLDecoder.decode(couponCodes, RestCoreConstants.HTTP_REST_ENCODE_CHARSET);
        } catch (UnsupportedEncodingException uee) {
            logger.error("Cannot decode coupon codes in the url for getCoupons, coupn codes: " + couponCodes, uee);
            throw new RESTCouponPushException(RestCoreConstants.FAILURE_INVALID_PARAMETER);
        }
        String[] codes = decodedCodes.split(WebCoreConstants.PAYSTATION_CONTACT_URL_SEPARATOR);
        List<String> list = new ArrayList<String>(codes.length);
        for (String code : codes) {
            list.add(code.trim());
        }
        return list;
    }
    
    private String getDateDisplay(Date date) {
        if (date == null || DateUtil.createDateOnlyString(date).equals(DateUtil.createDateOnlyString(WebCoreConstants.NO_START_DATE))
            || DateUtil.createDateOnlyString(date).equals(DateUtil.createDateOnlyString(WebCoreConstants.NO_END_DATE))) {
            
            return WebCoreConstants.NOT_APPLICABLE;
        }
        return DateUtil.createDateOnlyString(date);
    }
    
    public void setRESTCouponValidator(RESTCouponValidator restCouponValidator) {
        this.restCouponValidator = restCouponValidator;
    }
    
    public void setCouponService(CouponService couponService) {
        this.couponService = couponService;
    }
}
