package com.digitalpaytech.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.RestAccount;
import com.digitalpaytech.domain.RestSessionToken;
import com.digitalpaytech.dto.rest.RESTRegionQueryParamList;
import com.digitalpaytech.dto.rest.RESTRegions;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.DataNotFoundException;
import com.digitalpaytech.exception.IncorrectCredentialsException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.RestPropertyService;
import com.digitalpaytech.util.RestCoreConstants;

@Controller
@RequestMapping(value = "/Region")
public class RESTRegionController extends RESTBaseController {
    
    private final static Logger logger = Logger.getLogger(RESTRegionController.class);
    
    @Autowired
    private LocationService locationService;
    
    public void setLocationService(LocationService locationService) {
        this.locationService = locationService;
    }
    
    @RequestMapping(method = RequestMethod.GET, produces = "application/xml")
    @ResponseBody
    public RESTRegions getRegion(@RequestParam("Signature") String signature, @RequestParam("SignatureVersion") String signatureVersion,
                                 @RequestParam("Timestamp") String timestamp, @RequestParam("Token") String token) throws ApplicationException {
        
        String methodName = this.getClass().getName() + ".getRegion()";
        logger.debug("#### START " + methodName + " ####");
        
        try {
            super.restValidator.validateTimestamp(timestamp, methodName);
            super.restValidator.validateSignatureVersion(signatureVersion);
            RestSessionToken tokenObj = super.restValidator.validateSessionToken(token, methodName);
            /* retrieve the RESTAccount information from DB to obtain the secret key. */
            RestAccount restAccount = this.restAccountService.findRestAccountByAccountName(tokenObj.getRestAccount().getAccountName());
            
            if (restAccount == null) {
                logger.error("#### No RESTAccount has been found in getToken() operation for the AccountName: " + tokenObj.getRestAccount().getAccountName()
                             + " ####");
                throw new InvalidDataException();
            } else if (!super.isSubscribed(restAccount.getCustomer().getId())) {
                logger.error("####  Not Subscribe to Digital API Write ####");
                throw new IncorrectCredentialsException();
            } else if (!super.getIsServiceAgreementSigned(restAccount.getCustomer().getId(), false)) {
                logger.error("####  Service Agreement not signed ####");
                throw new IncorrectCredentialsException();
            } else if (!super.restValidator.validatePOS(restAccount)) {            	
                logger.error("#### PS: " + restAccount.getPointOfSale().getSerialNumber() + " is deactivated.");
                throw new InvalidDataException(RestCoreConstants.EMS_UNAVAILABLE);            	
            }
            
            /* Extend session expiry date. */
            super.extendToken(tokenObj, restAccount.getId());
            
            String secretKey = restAccount.getSecretKey();
            
            RESTRegionQueryParamList param = new RESTRegionQueryParamList();
            param.setToken(token);
            param.setSignature(signature);
            param.setSignatureVersion(signatureVersion);
            param.setTimestamp(timestamp);
            /* verify the payload by calling the method in encryptionService. */
            String hostName = this.restPropertyService.getPropertyValue(RestPropertyService.REST_DOMAIN_NAME, RestPropertyService.REST_DOMAIN_NAME_DEFAULT);
            if (!this.encryptionService.verifyHMACSignature(hostName, RestCoreConstants.HTTP_METHOD_GET, param.getSignature(), param, null, secretKey)) {
                throw new IncorrectCredentialsException();
            }
            
            /* retrieve region data with the customer ID. */
            List<Location> locationList = this.locationService.findChildLocationsByCustomerId(restAccount.getCustomer().getId());
            if (locationList == null || locationList.isEmpty()) {
                logger.error("#### No Region data has been found in getRegion() operation for the CustomerId: " + restAccount.getCustomer().getId() + " ####");
                throw new DataNotFoundException();
            }
            
            /* set the region name in the response. */
            RESTRegions response = new RESTRegions();
            List<String> regionNames = new ArrayList<String>(locationList.size());
            for (Location location : locationList) {
                regionNames.add(location.getName());
            }
            
            response.setRegionName(regionNames);
            
            logger.debug("#### END " + this.getClass().getName() + ".getRegion() ####");
            
            return response;
        } catch (ApplicationException ae) {
            throw ae;
        } catch (Exception e) {
            logger.error("#### Exception in " + methodName + " ####", e);
            throw new ApplicationException(e);
        }
    }
    
}
