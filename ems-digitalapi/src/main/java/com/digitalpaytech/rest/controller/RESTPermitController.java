package com.digitalpaytech.rest.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.RestAccount;
import com.digitalpaytech.domain.RestSessionToken;
import com.digitalpaytech.dto.rest.RESTPermit;
import com.digitalpaytech.dto.rest.RESTPermitQueryParamList;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.DuplicateObjectException;
import com.digitalpaytech.exception.IncorrectCredentialsException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.rest.helper.RESTPermitHelper;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RestPropertyService;
import com.digitalpaytech.servlet.DPTHttpServletRequestWrapper;
import com.digitalpaytech.util.RestCoreConstants;
import com.digitalpaytech.util.StandardConstants;

@Controller
@RequestMapping(value = "/Permit")
//Exception is caught to handle unexpected errors gracefully
@SuppressWarnings("checkstyle:illegalcatch")
public class RESTPermitController extends RESTBaseController {
    
    private static final Logger LOGGER = Logger.getLogger(RESTPermitController.class);
    private static final String NO_REST_ACCOUNT = " No RESTAccount has been found in getToken() operation for the AccountName: ";
    private static final String START = " START ";
    private static final String END = " END ";
    private static final String NO_DIGITALAPI_SUB = " Not Subscribe to Digital API Write ";
    private static final String SERVICE_NOT_SIGNED = " Service Agreement not signed ";
    private static final String PS = " PS: ";
    private static final String DEACTIVATED = " is deactivated.";
    private static final String EXCEPTION_IN = " Exception in ";
    
    @Autowired
    protected RESTPermitHelper restPermitHelper;
    
    @Autowired
    private LocationService locationService;
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    
    public final void setRESTPermitHelper(final RESTPermitHelper aRestPermitHelper) {
        this.restPermitHelper = aRestPermitHelper;
    }
    
    public final void setLocationService(final LocationService locationService) {
        this.locationService = locationService;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    @RequestMapping(method = RequestMethod.POST, consumes = "application/xml")
    @SuppressWarnings({"PMD.ConfusingTernary", "PMD.AvoidRethrowingException"})
    public final void createPermit(final HttpServletResponse response, final HttpServletRequest request, @RequestBody final RESTPermit permit,
        @RequestParam("Signature") final String signature, @RequestParam("SignatureVersion") final String signatureVersion,
        @RequestParam("Timestamp") final String timestamp, @RequestParam("Token") final String token) throws ApplicationException {
        
        final String methodName = this.getClass().getName() + ".createPermit()";
        LOGGER.debug(StandardConstants.STRING_FOUR_NUMBER_SIGNS + START + methodName + StandardConstants.STRING_EMPTY_SPACE
                     + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
        
        try {
            super.restValidator.validateTimestamp(timestamp, methodName);
            super.restValidator.validateSignatureVersion(signatureVersion);
            final RestSessionToken tokenObj = super.restValidator.validateSessionToken(token, methodName);
            super.restValidator.validateCreatePermit(permit, methodName);
            /* retrieve the RESTAccount information from DB to obtain the secret key. */
            final RestAccount restAccount = this.restAccountService.findRestAccountByAccountName(tokenObj.getRestAccount().getAccountName());
            if (restAccount == null) {
                LOGGER.error(StandardConstants.STRING_FOUR_NUMBER_SIGNS + NO_REST_ACCOUNT + tokenObj.getRestAccount().getAccountName()
                             + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new InvalidDataException();
            } else if (!super.isSubscribed(restAccount.getCustomer().getId())) {
                LOGGER.error(StandardConstants.STRING_FOUR_NUMBER_SIGNS + NO_DIGITALAPI_SUB + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new IncorrectCredentialsException();
            } else if (!super.getIsServiceAgreementSigned(restAccount.getCustomer().getId(), true)) {
                LOGGER.error(StandardConstants.STRING_FOUR_NUMBER_SIGNS + SERVICE_NOT_SIGNED + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new IncorrectCredentialsException();
            } else if (!super.restValidator.validatePOS(restAccount)) {
                LOGGER.error(StandardConstants.STRING_FOUR_NUMBER_SIGNS + PS + StandardConstants.STRING_EMPTY_SPACE
                             + restAccount.getPointOfSale().getSerialNumber() + DEACTIVATED);
                throw new InvalidDataException(RestCoreConstants.EMS_UNAVAILABLE);
            } else if (super.restValidator.checkIfPermitExists(permit, restAccount)) {
                LOGGER.error("#### Permit already exists. ####");
                throw new DuplicateObjectException(String.valueOf(RestCoreConstants.FAILURE_OBJECT_ALREADY_EXISTS));
            }
            
            //checkDataValidation(restAccount);
            
            /* Extend session expiry date. */
            super.extendToken(tokenObj, restAccount.getId());
            
            final RESTPermitQueryParamList param = new RESTPermitQueryParamList();
            param.setToken(token);
            param.setSignature(signature);
            param.setSignatureVersion(signatureVersion);
            param.setTimestamp(timestamp);
            
            /* verify the payload by calling the method in encryptionAppService. */
            
            final String hostName = this.restPropertyService.getPropertyValue(RestPropertyService.REST_DOMAIN_NAME,
                                                                              RestPropertyService.REST_DOMAIN_NAME_DEFAULT);
            if (!super.encryptionService.verifyHMACSignature(hostName, RestCoreConstants.HTTP_METHOD_POST, signature, param,
                                                             ((DPTHttpServletRequestWrapper) request).getBody(), restAccount.getSecretKey())) {
                throw new IncorrectCredentialsException();
            }
            
            /* check if the region is the lowest region with pay station. */
            final String locationName = permit.getRegionName();
            final Location location = checkLocation(restAccount, locationName);
            
            /* save the permit information. */
            this.restPermitHelper.createPermit(permit, restAccount, location);
            
            LOGGER.debug(StandardConstants.STRING_FOUR_NUMBER_SIGNS + StandardConstants.STRING_EMPTY_SPACE + END + methodName
                         + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            response.setStatus(RestCoreConstants.SUCCESS_PERMIT);
        } catch (ApplicationException ae) {
            throw ae;
        } catch (DuplicateObjectException doe) {
            throw doe;
        } catch (Exception e) {
            LOGGER.error(StandardConstants.STRING_FOUR_NUMBER_SIGNS + EXCEPTION_IN + methodName + StandardConstants.STRING_EMPTY_SPACE
                         + StandardConstants.STRING_FOUR_NUMBER_SIGNS, e);
            throw new ApplicationException(e);
        }
        
    }
    
    private Location checkLocation(final RestAccount restAccount, final String locationName) throws InvalidDataException {
        // TODO Auto-generated method stub
        Location location = null;
        if (locationName != null && !locationName.isEmpty()) {
            location = this.locationService.findLocationByCustomerIdAndName(restAccount.getCustomer().getId(), locationName);
            
            if (location == null) {
                LOGGER.error("#### The RegionName is not a child location. (Input RegionName: " + locationName
                             + StandardConstants.STRING_CLOSE_PARENTHESIS + StandardConstants.STRING_EMPTY_SPACE
                             + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new InvalidDataException();
            }
        }
        return location;
    }
    
    @RequestMapping(method = RequestMethod.GET, produces = "application/xml")
    @ResponseBody
    @SuppressWarnings({"PMD.ConfusingTernary", "PMD.AvoidRethrowingException"})
    public final RESTPermit getPermit(final HttpServletRequest request, @RequestParam("PermitNumber") final String permitNumber,
        @RequestParam("Signature") final String signature, @RequestParam("SignatureVersion") final String signatureVersion,
        @RequestParam("Timestamp") final String timestamp, @RequestParam("Token") final String token) throws ApplicationException {
        
        final String methodName = this.getClass().getName() + ".getPermit()";
        LOGGER.debug(StandardConstants.STRING_FOUR_NUMBER_SIGNS + methodName + StandardConstants.STRING_EMPTY_SPACE
                     + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
        
        try {
            super.restValidator.validateTimestamp(timestamp, methodName);
            super.restValidator.validateSignatureVersion(signatureVersion);
            final RestSessionToken tokenObj = super.restValidator.validateSessionToken(token, methodName);
            super.restValidator.validatePermitNumberForGetPermit(permitNumber, methodName);
            /* retrieve the RESTAccount information from DB to obtain the secret key. */
            final RestAccount restAccount = this.restAccountService.findRestAccountByAccountName(tokenObj.getRestAccount().getAccountName());
            
            if (restAccount == null) {
                LOGGER.error(StandardConstants.STRING_FOUR_NUMBER_SIGNS + NO_REST_ACCOUNT + tokenObj.getRestAccount().getAccountName()
                             + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new InvalidDataException();
            } else if (!super.isSubscribed(restAccount.getCustomer().getId())) {
                LOGGER.error(StandardConstants.STRING_FOUR_NUMBER_SIGNS + NO_DIGITALAPI_SUB + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new IncorrectCredentialsException();
            } else if (!super.getIsServiceAgreementSigned(restAccount.getCustomer().getId(), false)) {
                LOGGER.error(StandardConstants.STRING_FOUR_NUMBER_SIGNS + SERVICE_NOT_SIGNED + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                throw new IncorrectCredentialsException();
            } else if (!super.restValidator.validatePOS(restAccount)) {
                LOGGER.error(StandardConstants.STRING_FOUR_NUMBER_SIGNS + PS + StandardConstants.STRING_EMPTY_SPACE
                             + restAccount.getPointOfSale().getSerialNumber() + DEACTIVATED);
                throw new InvalidDataException(RestCoreConstants.EMS_UNAVAILABLE);
            }
            
            /* Extend session expiry date. */
            super.extendToken(tokenObj, restAccount.getId());
            
            final RESTPermitQueryParamList param = new RESTPermitQueryParamList();
            param.setToken(token);
            param.setSignature(signature);
            param.setSignatureVersion(signatureVersion);
            param.setTimestamp(timestamp);
            param.setPermitNumber(permitNumber);
            
            /* verify the payload by calling the method in encryptionAppService. */
            
            final String hostName = this.restPropertyService.getPropertyValue(RestPropertyService.REST_DOMAIN_NAME,
                                                                              RestPropertyService.REST_DOMAIN_NAME_DEFAULT);
            if (!super.encryptionService.verifyHMACSignature(hostName, RestCoreConstants.HTTP_METHOD_GET, signature, param, null,
                                                             restAccount.getSecretKey())) {
                throw new IncorrectCredentialsException();
            }
            
            /* save the permit information. */
            final RESTPermit response = this.restPermitHelper.getPermit(restAccount, permitNumber);
            
            LOGGER.debug(StandardConstants.STRING_FOUR_NUMBER_SIGNS + StandardConstants.STRING_EMPTY_SPACE + END + methodName
                         + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            return response;
        } catch (ApplicationException ae) {
            throw ae;
        } catch (Exception e) {
            LOGGER.error(StandardConstants.STRING_FOUR_NUMBER_SIGNS + EXCEPTION_IN + methodName + StandardConstants.STRING_EMPTY_SPACE
                         + StandardConstants.STRING_FOUR_NUMBER_SIGNS, e);
            throw new ApplicationException(e);
        }
        
    }
    
    @RequestMapping(method = RequestMethod.PUT, consumes = "application/xml")
    @SuppressWarnings({"PMD.AvoidRethrowingException"})
    public final void updatePermit(final HttpServletResponse response, final HttpServletRequest request, @RequestBody final RESTPermit permit,
        @RequestParam("Signature") final String signature, @RequestParam("SignatureVersion") final String signatureVersion,
        @RequestParam("Timestamp") final String timestamp, @RequestParam("Token") final String token) throws ApplicationException {
        
        final String methodName = this.getClass().getName() + ".updatePermit()";
        LOGGER.debug(StandardConstants.STRING_FOUR_NUMBER_SIGNS + START + methodName + StandardConstants.STRING_EMPTY_SPACE
                     + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
        
        try {
            super.restValidator.validateTimestamp(timestamp, methodName);
            super.restValidator.validateSignatureVersion(signatureVersion);
            final RestSessionToken tokenObj = super.restValidator.validateSessionToken(token, methodName);
            super.restValidator.validateUpdatePermit(permit, methodName);
            /* retrieve the RESTAccount information from DB to obtain the secret key. */
            final RestAccount restAccount = this.restAccountService.findRestAccountByAccountName(tokenObj.getRestAccount().getAccountName());
            
            validateUserData(restAccount, permit, tokenObj);
            
            /* Extend session expiry date. */
            super.extendToken(tokenObj, restAccount.getId());
            
            if (permit.getPermitAmount() == null
                && (permit.getPayments() == null || permit.getPayments().getPayment() == null || permit.getPayments().getPayment().isEmpty())) {
                LOGGER.debug("#### Just returned Success (201) as there are no <PermitAmount> and <Payment>. " + this.getClass().getName()
                             + ".updatePermit() ####");
                response.setStatus(RestCoreConstants.SUCCESS_PERMIT);
                return;
            }
            
            final RESTPermitQueryParamList param = new RESTPermitQueryParamList();
            param.setToken(token);
            param.setSignature(signature);
            param.setSignatureVersion(signatureVersion);
            param.setTimestamp(timestamp);
            
            /* verify the payload by calling the method in encryptionAppService. */
            
            final String hostName = this.restPropertyService.getPropertyValue(RestPropertyService.REST_DOMAIN_NAME,
                                                                              RestPropertyService.REST_DOMAIN_NAME_DEFAULT);
            
            if (!super.encryptionService.verifyHMACSignature(hostName, RestCoreConstants.HTTP_METHOD_PUT, signature, param,
                                                             ((DPTHttpServletRequestWrapper) request).getBody(), restAccount.getSecretKey())) {
                throw new IncorrectCredentialsException();
            }
            
            /* check if the region is the lowest region with pay station. */
            final String locationName = permit.getRegionName();
            final Location location = checkLocation(restAccount, locationName);
            
            /* save the permit information. */
            this.restPermitHelper.updatePermit(permit, restAccount, location, methodName);
            
            LOGGER.debug(StandardConstants.STRING_FOUR_NUMBER_SIGNS + StandardConstants.STRING_EMPTY_SPACE + END + methodName
                         + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            response.setStatus(RestCoreConstants.SUCCESS_PERMIT);
        } catch (ApplicationException ae) {
            throw ae;
        } catch (Exception e) {
            LOGGER.error(StandardConstants.STRING_FOUR_NUMBER_SIGNS + EXCEPTION_IN + methodName + StandardConstants.STRING_EMPTY_SPACE
                         + StandardConstants.STRING_FOUR_NUMBER_SIGNS, e);
            throw new ApplicationException(e);
        }
    }
    
    @SuppressWarnings("PMD.ConfusingTernary")
    private void validateUserData(final RestAccount restAccount, final RESTPermit permit, final RestSessionToken tokenObj) throws Exception {
        if (restAccount == null) {
            LOGGER.error(StandardConstants.STRING_FOUR_NUMBER_SIGNS + NO_REST_ACCOUNT + tokenObj.getRestAccount().getAccountName()
                         + StandardConstants.STRING_EMPTY_SPACE + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException();
        } else if (!super.isSubscribed(restAccount.getCustomer().getId())) {
            LOGGER.error(StandardConstants.STRING_FOUR_NUMBER_SIGNS + NO_DIGITALAPI_SUB + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new IncorrectCredentialsException();
        } else if (!super.getIsServiceAgreementSigned(restAccount.getCustomer().getId(), true)) {
            LOGGER.error(StandardConstants.STRING_FOUR_NUMBER_SIGNS + SERVICE_NOT_SIGNED + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new IncorrectCredentialsException();
        } else if (!super.restValidator.validatePOS(restAccount)) {
            LOGGER.error(StandardConstants.STRING_FOUR_NUMBER_SIGNS + PS + StandardConstants.STRING_EMPTY_SPACE
                         + restAccount.getPointOfSale().getSerialNumber() + DEACTIVATED);
            throw new InvalidDataException(RestCoreConstants.EMS_UNAVAILABLE);
        } else if (!super.restValidator.checkIfPermitExists(permit, restAccount)) {
            LOGGER.error("#### Duplicate permit for: " + restAccount.getPointOfSale().getSerialNumber() + ".");
            throw new DuplicateObjectException("duplicate");
        }
        
    }
}
