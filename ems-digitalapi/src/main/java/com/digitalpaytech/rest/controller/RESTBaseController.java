package com.digitalpaytech.rest.controller;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.InvalidTimeoutException;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.RestSessionToken;
import com.digitalpaytech.domain.ServiceAgreement;
import com.digitalpaytech.exception.AuthorizationFailedException;
import com.digitalpaytech.exception.DataNotFoundException;
import com.digitalpaytech.exception.DuplicateObjectException;
import com.digitalpaytech.exception.IncorrectCredentialsException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.exception.InvalidPermitNumberException;
import com.digitalpaytech.exception.RESTCouponPushException;
import com.digitalpaytech.rest.validator.RESTValidator;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.CustomerSubscriptionService;
import com.digitalpaytech.service.EncryptionService;
import com.digitalpaytech.service.RestAccountService;
import com.digitalpaytech.service.RestPropertyService;
import com.digitalpaytech.service.RestSessionTokenService;
import com.digitalpaytech.service.ServiceAgreementService;
import com.digitalpaytech.util.RestCoreConstants;
import com.digitalpaytech.util.WebCoreConstants;

@Controller
public class RESTBaseController {
    
    private static final Logger LOGGER = Logger.getLogger(RESTBaseController.class);
    
    @Autowired
    protected RESTValidator restValidator;
     
    @Autowired
    protected RestSessionTokenService restSessionTokenService;
    
    @Autowired
    protected RestPropertyService restPropertyService;
    
    @Autowired
    protected RestAccountService restAccountService;
    
    @Autowired
    protected EncryptionService encryptionService;
    
    @Autowired
    private ServiceAgreementService serviceAgreementService;
    
    @Autowired
    private CustomerSubscriptionService customerSubscriptionService;
    
    @Autowired
    private CustomerService customerService;
     
    
    
    public final void setRestValidator(final RESTValidator restValidator) {
        this.restValidator = restValidator;
    }
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final void setRestSessionTokenService(final RestSessionTokenService restSessionTokenService) {
        this.restSessionTokenService = restSessionTokenService;
    }
    
    public final void setRestPropertyService(final RestPropertyService restPropertyService) {
        this.restPropertyService = restPropertyService;
    }
    
    public final void setRestAccountService(final RestAccountService restAccountService) {
        this.restAccountService = restAccountService;
    }
    
    public final void setEncryptionService(final EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }
    
    public final void setServiceAgreementService(final ServiceAgreementService serviceAgreementService) {
        this.serviceAgreementService = serviceAgreementService;
    }
    
    protected final Date createExpiryDate(final Date currentTime) throws Exception {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(currentTime);
        final int interval = this.restPropertyService.getPropertyValueAsInt(RestPropertyService.REST_SESSION_TOKEN_EXPIRY_INTERVAL,
                                                                      RestCoreConstants.SESSION_TOKEN_EXPIRY_INTERVAL);
        cal.add(Calendar.MINUTE, interval);
        return cal.getTime();
    }
    
    protected final void extendToken(final RestSessionToken restSessionToken, final int restAccountId) throws Exception {
        final Date currentTime = new Date();
        restSessionToken.setExpiryDate(createExpiryDate(currentTime));
        restSessionToken.setLastModifiedGmt(currentTime);
        restSessionToken.setLastModifiedByUserId(restAccountId);
        
        this.restSessionTokenService.updateRestSessionToken(restSessionToken);
    }
    
    @ExceptionHandler({ Exception.class, RuntimeException.class })
    public final void handleExceptions(final Exception e, final HttpServletResponse response) {
        int status;
        if (e instanceof InvalidDataException) {
            response.setStatus(RestCoreConstants.FAILURE_INVALID_PARAMETER);
            status = RestCoreConstants.FAILURE_INVALID_PARAMETER;
        } else if (e instanceof InvalidTimeoutException) {
            response.setStatus(RestCoreConstants.FAILURE_INVALID_TIMESTAMP);
            status = RestCoreConstants.FAILURE_INVALID_TIMESTAMP;
        } else if (e instanceof InvalidPermitNumberException) {
            response.setStatus(RestCoreConstants.FAILURE_INVALID_PERMIT_NUMBER);
            status = RestCoreConstants.FAILURE_INVALID_PERMIT_NUMBER;
        } else if (e instanceof AuthorizationFailedException) {
            response.setStatus(RestCoreConstants.FAILURE_NOT_AUTHORIZED_OPERATION);
            status = RestCoreConstants.FAILURE_NOT_AUTHORIZED_OPERATION;
        } else if (e instanceof IncorrectCredentialsException) {
            response.setStatus(RestCoreConstants.FAILURE_INCORRECT_CREDENTIALS);
            status = RestCoreConstants.FAILURE_INCORRECT_CREDENTIALS;
        } else if (e instanceof DataNotFoundException) {
            response.setStatus(RestCoreConstants.FAILURE_OBJECT_NOT_FOUND);
            status = RestCoreConstants.FAILURE_OBJECT_NOT_FOUND;
        } else if (e instanceof DuplicateObjectException) {
            response.setStatus(RestCoreConstants.FAILURE_OBJECT_ALREADY_EXISTS);
            status = RestCoreConstants.FAILURE_OBJECT_ALREADY_EXISTS;
        } else if (e instanceof RESTCouponPushException) {
            status = Integer.parseInt(e.getMessage());
            response.setStatus(status);
        } else {
            response.setStatus(RestCoreConstants.FAILURE_INTERNAL_ERROR);
            status = RestCoreConstants.FAILURE_INTERNAL_ERROR;
        }
        try {
            if (RestCoreConstants.EMS_UNAVAILABLE.equals(e.getMessage())) {
                final StringBuffer sb = new StringBuffer(new Integer(status).toString());
                sb.append(": ");
                sb.append(RestCoreConstants.EMS_UNAVAILABLE);
                response.getWriter().print(sb.toString());
       
            } else {
                response.getWriter().print(status);
            }
        } catch (IOException ioe) {
            LOGGER.error(e.getMessage(), ioe);
        }
    }
     
    
    protected final boolean getIsServiceAgreementSigned(int customerId, final boolean checkMigrated) {
        final Customer customer = this.customerService.findCustomer(customerId);
        if (customer.getParentCustomer() != null) {
            customerId = customer.getParentCustomer().getId();
        }
        final ServiceAgreement agreement = this.serviceAgreementService.findLatestServiceAgreementByCustomerId(customerId);
        return !((agreement == null) || (checkMigrated && !customer.isIsMigrated()));
    }
    
    protected final boolean isSubscribed(final int customerId) {
        return this.customerSubscriptionService.hasOneOfSubscriptions(customerId, WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_API_WRITE);
    }
    
}
