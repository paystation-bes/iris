package com.digitalpaytech.rest.helper;

import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.RestAccount;
import com.digitalpaytech.dto.rest.RESTPermit;
import com.digitalpaytech.exception.ApplicationException;

public interface RESTPermitHelper {
    
    void createPermit(RESTPermit restPermit, RestAccount restAccount, Location location) throws ApplicationException;
    
    RESTPermit getPermit(RestAccount restAccount, String permitNumber) throws ApplicationException;
    
    void updatePermit(RESTPermit restPermit, RestAccount restAccount, Location location, String methodName) throws ApplicationException;
}
