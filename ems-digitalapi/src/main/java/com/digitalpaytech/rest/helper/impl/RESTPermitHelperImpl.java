package com.digitalpaytech.rest.helper.impl;

import java.math.BigDecimal;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.domain.CardType;
import com.digitalpaytech.domain.CreditCardType;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.LicencePlate;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.PaymentType;
import com.digitalpaytech.domain.PaystationSetting;
import com.digitalpaytech.domain.Permit;
import com.digitalpaytech.domain.PermitIssueType;
import com.digitalpaytech.domain.PermitType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.ProcessorTransaction;
import com.digitalpaytech.domain.ProcessorTransactionType;
import com.digitalpaytech.domain.Purchase;
import com.digitalpaytech.domain.RestAccount;
import com.digitalpaytech.domain.TransactionType;
import com.digitalpaytech.domain.UnifiedRate;
import com.digitalpaytech.dto.rest.RESTPayment;
import com.digitalpaytech.dto.rest.RESTPayments;
import com.digitalpaytech.dto.rest.RESTPermit;
import com.digitalpaytech.exception.ApplicationException;
import com.digitalpaytech.exception.DataNotFoundException;
import com.digitalpaytech.exception.DuplicateObjectException;
import com.digitalpaytech.exception.IncorrectCredentialsException;
import com.digitalpaytech.exception.InvalidDataException;
import com.digitalpaytech.rest.helper.RESTPermitHelper;
import com.digitalpaytech.service.LicensePlateService;
import com.digitalpaytech.service.PaymentCardService;
import com.digitalpaytech.service.PaystationSettingService;
import com.digitalpaytech.service.PermitIssueTypeService;
import com.digitalpaytech.service.PermitService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.ProcessorTransactionService;
import com.digitalpaytech.service.PurchaseService;
import com.digitalpaytech.service.TransactionService;
import com.digitalpaytech.service.UnifiedRateService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.CardProcessingUtil;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.ReportingConstants;
import com.digitalpaytech.util.RestCoreConstants;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;

@Component("restPermitHelper")
public class RESTPermitHelperImpl implements RESTPermitHelper {
    private static final Logger LOGGER = Logger.getLogger(RESTPermitHelperImpl.class);
    
    @Autowired
    private PointOfSaleService pointOfSaleService;
    @Autowired
    private PermitService permitService;
    @Autowired
    private PurchaseService purchaseService;
    @Autowired
    private ProcessorTransactionService processorTransactionService;
    @Autowired
    private UnifiedRateService unifiedRateService;
    @Autowired
    private LicensePlateService licencePlateService;
    @Autowired
    private PermitIssueTypeService permitIssueTypeService;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private PaymentCardService paymentCardService;
    @Autowired
    private PaystationSettingService paystationSettingService;
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final void setPermitService(final PermitService permitService) {
        this.permitService = permitService;
    }
    
    public final void setPurchaseService(final PurchaseService purchaseService) {
        this.purchaseService = purchaseService;
    }
    
    public final void setProcessorTransactionService(final ProcessorTransactionService processorTransactionService) {
        this.processorTransactionService = processorTransactionService;
    }
    
    public final void setUnifiedRateService(final UnifiedRateService unifiedRateService) {
        this.unifiedRateService = unifiedRateService;
    }
    
    public final void setLicencePlateService(final LicensePlateService licencePlateService) {
        this.licencePlateService = licencePlateService;
    }
    
    public final void setPermitIssueTypeService(final PermitIssueTypeService permitIssueTypeService) {
        this.permitIssueTypeService = permitIssueTypeService;
    }
    
    public final void setTransactionService(final TransactionService transactionService) {
        this.transactionService = transactionService;
    }
    
    public final void setPaymentCardService(final PaymentCardService paymentCardService) {
        this.paymentCardService = paymentCardService;
    }
    
    public final void setPaystationSettingService(final PaystationSettingService paystationSettingService) {
        this.paystationSettingService = paystationSettingService;
    }
    
    public final void createPermit(final RESTPermit restPermit, final RestAccount restAccount, final Location location) throws ApplicationException {
        final PointOfSale pos = this.pointOfSaleService.findPointOfSaleById(restAccount.getPointOfSale().getId());
        if (pos == null) {
            LOGGER.error("#### Exception occurred because no pay station has been found. (restPermit PS number: "
                         + restAccount.getPointOfSale().getId() + StandardConstants.STRING_CLOSE_PARENTHESIS);
            throw new IncorrectCredentialsException();
        }
        Location newLocation = location;
        if (newLocation == null) {
            newLocation = pos.getLocation();
        }
        
        final Permit permit = new Permit();
        final Purchase purchase = new Purchase();
        final ProcessorTransaction proctrans = new ProcessorTransaction();
        final UnifiedRate rate = getUnifiedRate(pos.getCustomer(), restPermit.getRateName());
        
        final PaymentCard paymentCard = new PaymentCard();
        final PaystationSetting paystationSetting = this.paystationSettingService.findByCustomerIdAndLotName(restAccount.getCustomer(), "None");
        
        /* create Purchase */
        purchase.setUnifiedRate(rate);
        purchase.setCustomer(restAccount.getCustomer());
        purchase.setPointOfSale(pos);
        purchase.setPurchaseNumber(Integer.parseInt(restPermit.getPermitNumber()));
        purchase.setLocation(newLocation);
        purchase.setPaystationSetting(paystationSetting);
        final TransactionType transactionType = new TransactionType();
        transactionType.setId(ReportingConstants.TRANSACTION_TYPE_REGULAR);
        purchase.setTransactionType(transactionType);
        purchase.setPurchaseGmt(DateUtil.convertFromRESTDateString(restPermit.getPurchasedDate()));
        purchase.setChargedAmount(restPermit.getPermitAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
        purchase.setOriginalAmount(restPermit.getPermitAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
        purchase.setRateAmount(restPermit.getPermitAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
        purchase.setRateRevenueAmount(restPermit.getPermitAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED)
                .intValue());
        
        final List<RESTPayment> paymentList = restPermit.getPayments().getPayment();
        if (paymentList != null && !paymentList.isEmpty()) {
            for (RESTPayment payment : paymentList) {
                if (payment.getType().equals(RestCoreConstants.PAYMENT_TYPE_CREDITCARD)) {
                    paymentCard.setAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
                    final CardType cardType = new CardType();
                    cardType.setId(WebCoreConstants.CREDIT_CARD);
                    paymentCard.setCardType(cardType);
                    final CreditCardType creditCardType = new CreditCardType();
                    creditCardType.setId(CardProcessingUtil.getCreditCardTypeId(payment.getCardType()));
                    paymentCard.setCreditCardType(creditCardType);
                    paymentCard.setAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
                    paymentCard.setCardLast4digits(Short.parseShort(payment.getLast4DigitsOfCard()));
                    final Date paymentDate =
                            DateUtil.convertFromRESTDateString(((RESTPayment) restPermit.getPayments().getPayment().get(0)).getPaymentDate());
                    paymentCard.setCardProcessedGmt(paymentDate);
                    paymentCard.setIsUploadedFromBoss(false);
                    paymentCard.setIsRfid(false);
                    paymentCard.setIsApproved(true);
                    paymentCard.setPurchase(purchase);
                    
                    purchase.setCardPaidAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED)
                            .intValue());
                    final PaymentType paymentType = new PaymentType();
                    paymentType.setId((byte) WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_EXTERNAL);
                    purchase.setPaymentType(paymentType);
                    
                    final ProcessorTransactionType procTransType = new ProcessorTransactionType();
                    procTransType.setId(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_EXTERNAL);
                    proctrans.setProcessorTransactionType(procTransType);
                    paymentCard.setProcessorTransactionType(procTransType);
                    
                    proctrans.setCreatedGmt(DateUtil.getCurrentGmtDate());
                    
                    proctrans.setPurchase(purchase);
                    proctrans.setPointOfSale(pos);
                    proctrans.setPurchasedDate(DateUtil.convertFromRESTDateString(restPermit.getPurchasedDate()));
                    proctrans.setTicketNumber(Integer.parseInt(restPermit.getPermitNumber()));
                    proctrans.setAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
                    proctrans.setCardType(payment.getCardType());
                    proctrans.setLast4digitsOfCardNumber(Short.parseShort(payment.getLast4DigitsOfCard()));
                    proctrans.setProcessingDate(paymentDate);
                    proctrans.setAuthorizationNumber(payment.getCardAuthorizationId());
                    proctrans.setIsApproved(true);
                    
                    proctrans.setMerchantAccount(null);
                    proctrans.setCardChecksum(0);
                    proctrans.setProcessorTransactionId(WebCoreConstants.EMPTY_STRING);
                    proctrans.setReferenceNumber(WebCoreConstants.EMPTY_STRING);
                    proctrans.setCardHash(WebCoreConstants.EMPTY_STRING);
                    proctrans.setIsUploadedFromBoss(false);
                    proctrans.setIsRfid(false);
                    
                    paymentCard.setProcessorTransaction(proctrans);
                    paymentCard.setProcessorTransactionType(proctrans.getProcessorTransactionType());
                }
            }
        }
        
        permit.setPurchase(purchase);
        permit.setPermitNumber(Integer.parseInt(restPermit.getPermitNumber()));
        permit.setLocation(newLocation);
        final PermitType permitType = new PermitType();
        permitType.setId(ReportingConstants.PERMIT_TYPE_REGULAR);
        permit.setPermitType(permitType);
        permit.setPermitBeginGmt(DateUtil.convertFromRESTDateString(restPermit.getPurchasedDate()));
        
        permit.setPermitExpireGmt(DateUtil.convertFromRESTDateString(restPermit.getExpiryDate()));
        permit.setPermitOriginalExpireGmt(DateUtil.convertFromRESTDateString(restPermit.getExpiryDate()));
        
        if (restPermit.getStallNumber() != null) {
            permit.setSpaceNumber(Integer.parseInt(restPermit.getStallNumber()));
        } else {
            permit.setSpaceNumber(0);
        }
        if (restPermit.getPlateNumber() != null && !StringUtils.isEmpty(restPermit.getPlateNumber())) {
            final String licencePlateNumber = restPermit.getPlateNumber().toUpperCase();
            LicencePlate licencePlate = this.licencePlateService.findByNumber(licencePlateNumber);
            if (licencePlate == null) {
                licencePlate = new LicencePlate();
                licencePlate.setNumber(licencePlateNumber);
                licencePlate.setLastModifiedGmt(new Date());
            }
            permit.setLicencePlate(licencePlate);
        } else {
            permit.setLicencePlate(null);
        }
        final PermitIssueType permitIssueType = this.permitIssueTypeService
                .findPermitIssueType(restPermit.getPlateNumber() == null ? WebCoreConstants.EMPTY_STRING : restPermit.getPlateNumber(),
                                     restPermit.getStallNumber() == null ? WebCoreConstants.EMPTY_STRING : restPermit.getStallNumber());
        permit.setPermitIssueType(permitIssueType);
        
        this.transactionService.createTransaction(permit, purchase, paymentCard, proctrans);
    }
    
    private UnifiedRate getUnifiedRate(final Customer customer, final String rateNames) {
        
        String newRateName = rateNames;
        if (newRateName == null || StringUtils.isBlank(newRateName)) {
            newRateName = WebCoreConstants.UNKNOWN;
        }
        
        UnifiedRate rate = this.unifiedRateService.findRateByNameAndCustomerId(newRateName, customer.getId());
        if (rate == null) {
            rate = new UnifiedRate();
            rate.setCustomer(customer);
            rate.setLastModifiedByUserId(WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID);
            rate.setLastModifiedGmt(DateUtil.getCurrentGmtDate());
            rate.setName(newRateName);
        }
        return rate;
    }
    
    @Override
    public final RESTPermit getPermit(final RestAccount restAccount, final String permitNumber) throws ApplicationException {
        
        final Permit originalPermit = this.permitService.findLatestOriginalPermitByPointOfSaleIdAndPermitNumber(restAccount.getPointOfSale().getId(),
                                                                                                                Integer.parseInt(permitNumber));
        if (originalPermit == null) {
            throw new DataNotFoundException();
        }
        final Purchase originalPurchase = originalPermit.getPurchase();
        if (originalPurchase == null) {
            throw new DataNotFoundException();
        }
        
        final RESTPermit restPermit = new RESTPermit();
        
        restPermit.setPermitNumber(String.valueOf(originalPermit.getPermitNumber()));
        restPermit.setPurchasedDate(DateUtil.createNewPs2DateString(originalPurchase.getPurchaseGmt()));
        if (originalPermit.getSpaceNumber() != 0) {
            restPermit.setStallNumber(String.valueOf(originalPermit.getSpaceNumber()));
        }
        
        if (originalPermit.getLicencePlate() != null) {
            restPermit.setPlateNumber(originalPermit.getLicencePlate().getNumber());
        }
        
        restPermit.setRegionName(originalPermit.getLocation().getName());
        restPermit.setRateName(originalPurchase.getUnifiedRate().getName());
        restPermit.setExpiryDate(DateUtil.createNewPs2DateString(originalPermit.getPermitExpireGmt()));
        
        final RESTPayments restPayments = new RESTPayments();
        final List<RESTPayment> paymentList = new ArrayList<RESTPayment>();
        
        final RESTPayment originalRestPayment = new RESTPayment();
        
        final Set<ProcessorTransaction> processorTransactions = originalPurchase.getProcessorTransactions();
        if (processorTransactions == null || processorTransactions.isEmpty()) {
            throw new DataNotFoundException();
        }
        final ProcessorTransaction originalProcessorTransaction = processorTransactions.toArray(new ProcessorTransaction[1])[0];
        
        final Set<PaymentCard> paymentCards = originalPurchase.getPaymentCards();
        if (paymentCards == null || paymentCards.isEmpty()) {
            throw new DataNotFoundException();
        }
        final PaymentCard originalPaymentCard = paymentCards.toArray(new PaymentCard[1])[0];
        BigDecimal totalAmount = new BigDecimal(0);
        // to keep response identical to EMS6
        if (originalPurchase.getChargedAmount() > 0) {
            BigDecimal originalAmount = new BigDecimal(originalPurchase.getChargedAmount()).divide(WebCoreUtil.HUNDRED);
            originalAmount = originalAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
            originalRestPayment.setAmount(originalAmount);
            originalRestPayment.setCardAuthorizationId(originalProcessorTransaction.getSanitizedAuthorizationNumber());
            originalRestPayment.setCardType(originalPaymentCard.getCreditCardType().getName());
            originalRestPayment.setLast4DigitsOfCard(String.valueOf(originalProcessorTransaction.getLast4digitsOfCardNumber()));
            originalRestPayment.setPaymentDate(DateUtil.createNewPs2DateString(originalProcessorTransaction.getProcessingDate()));
            originalRestPayment.setType(CardProcessingConstants.NAME_CREDIT_CARD);
            totalAmount = originalAmount;
            paymentList.add(originalRestPayment);
        }
        
        if (originalPermit.getNumberOfExtensions() != 0) {
            final List<Permit> extentionList = this.permitService.findExtentionsByOriginalPermitId(originalPermit.getId());
            for (Permit extention : extentionList) {
                
                // to keep response identical to EMS6
                if (extention.getPurchase().getChargedAmount() > 0) {
                    final RESTPayment extentionRestPayment = new RESTPayment();
                    
                    final Set<ProcessorTransaction> extentionProcessorTransactions = extention.getPurchase().getProcessorTransactions();
                    if (extentionProcessorTransactions == null || extentionProcessorTransactions.isEmpty()) {
                        throw new DataNotFoundException();
                    }
                    final ProcessorTransaction extentionProcessorTransaction = extentionProcessorTransactions.toArray(new ProcessorTransaction[1])[0];
                    
                    final Set<PaymentCard> extentionPaymentCards = extention.getPurchase().getPaymentCards();
                    if (extentionPaymentCards == null || extentionPaymentCards.isEmpty()) {
                        throw new DataNotFoundException();
                    }
                    final PaymentCard extentionPaymentCard = extentionPaymentCards.toArray(new PaymentCard[1])[0];
                    
                    BigDecimal amount = new BigDecimal(extention.getPurchase().getChargedAmount()).divide(WebCoreUtil.HUNDRED);
                    amount = amount.setScale(2, BigDecimal.ROUND_HALF_UP);
                    if (extention.getPurchase().getTransactionType().getId() == ReportingConstants.TRANSACTION_TYPE_REFUND_3RD_PARTY) {
                        amount = amount.negate();
                    }
                    extentionRestPayment.setAmount(amount);
                    
                    extentionRestPayment.setCardAuthorizationId(extentionProcessorTransaction.getSanitizedAuthorizationNumber());
                    extentionRestPayment.setCardType(extentionPaymentCard.getCreditCardType().getName());
                    extentionRestPayment.setLast4DigitsOfCard(String.valueOf(extentionProcessorTransaction.getLast4digitsOfCardNumber()));
                    extentionRestPayment.setPaymentDate(DateUtil.createNewPs2DateString(extentionProcessorTransaction.getProcessingDate()));
                    extentionRestPayment.setType(CardProcessingConstants.NAME_CREDIT_CARD);
                    totalAmount = totalAmount.add(extentionRestPayment.getAmount());
                    paymentList.add(extentionRestPayment);
                }
                
            }
        }
        
        if (paymentList.size() > 0) {
            restPayments.setPayment(paymentList);
        }
        restPermit.setPayments(restPayments);
        restPermit.setPermitAmount(totalAmount);
        return restPermit;
    }
    
    @Override
    public final void updatePermit(final RESTPermit restPermit, final RestAccount restAccount, final Location location, final String methodName)
        throws ApplicationException {
        
        final int posId = restAccount.getPointOfSale().getId();
        final int customerId = restAccount.getCustomer().getId();
        final int permitNumber = Integer.parseInt(restPermit.getPermitNumber());
        final Date purchaseDate = DateUtil.convertFromRESTDateString(restPermit.getPurchasedDate());
        final Purchase purchase = this.purchaseService.findUniquePurchaseForSms(customerId, posId, purchaseDate, permitNumber);
        
        if (purchase == null) {
            LOGGER.error("#### Original purchase record does not exist in " + methodName + StandardConstants.STRING_EMPTY_SPACE
                         + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException();
        }
        
        final Permit permit = this.permitService.findPermitByPurchaseId(purchase.getId());
        if (permit == null) {
            LOGGER.error("#### Original permit record does not exist in " + methodName + StandardConstants.STRING_EMPTY_SPACE
                         + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidDataException();
        }
        
        final List<RESTPayment> paymentList = restPermit.getPayments().getPayment();
        
        boolean isDuplicated = true;
        for (int i = 0; i < paymentList.size(); i++) {
            final RESTPayment restPayment = paymentList.get(i);
            Date paymentDate = null;
            if (i == 0) {
                paymentDate = DateUtil.convertFromRESTDateString(restPermit.getPurchasedDate());
            } else {
                paymentDate = DateUtil.convertFromRESTDateString(restPayment.getPaymentDate());
                
            }
            
            final Purchase duplicatedPurchase = this.purchaseService.findUniquePurchaseForSms(customerId, posId, paymentDate, permitNumber);
            
            if (duplicatedPurchase == null) {
                if (restPayment.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue() >= 0) {
                    /* Add time */
                    processAddTime(restAccount.getPointOfSale(), purchase, permit, location, restPermit, restPayment);
                    isDuplicated = false;
                } else {
                    /* This part should be replaced with the else statement below when refund is included. */
                    LOGGER.error("#### <Amount> should be greater than or equal to 0. ####");
                    //throw new InvalidDataException();
                    throw new DuplicateObjectException("");
                }
                
                /* This refund operation commented out as this is out of EMS 6.3.7. This else statement should be replaced with the one above. */
                // } else {
                //     /* Refund */
                //     if (restPayment.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue() == 0) {
                //           /* Do nothing as the payment amount is $0.00 */
                //         isDuplicated = false;
                //     } else {
                //         /* check if the previous transaction is refund transaction.(no double refund allowed.) */
                //         if (i == 0 || paymentList.get(i-1).getAmount().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue() < 0) {
                //             LOGGER.error("#### Refund transaction cannot be repeated." + restAccount.getAccountName() + StandardConstants.STRING_EMPTY_SPACE+StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                //             throw new InvalidDataException();
                //         }
                //         Date previousPaymentDate = DateUtil.convertFromRESTDateString(paymentList.get(i - 0).getPaymentDate());
                //                       
                //         Purchase previousPurchase = this.purchaseService.findUniquePurchaseForSms(customerId, posId, previousPaymentDate, permitNumber);
                //                       
                //         if (previousPurchase == null) {
                //             LOGGER.error("#### Previous transaction record does not exist in " + methodName 
                //                           + StandardConstants.STRING_EMPTY_SPACE+StandardConstants.STRING_FOUR_NUMBER_SIGNS);
                //             throw new InvalidDataException();
                //         }
                //                       
                //         processRefund(purchase.getPointOfSale(), previousPurchase, permit, restPermit, restPayment);
                //         isDuplicated = false;
                //     }
                // }
            } else {
                if (duplicatedPurchase.getChargedAmount() > restPayment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP)
                        .multiply(WebCoreUtil.HUNDRED).intValue()) {
                    /* partial settlement. */
                    Permit currentPermit = permit;
                    if (!purchase.equals(duplicatedPurchase)) {
                        currentPermit = this.permitService.findPermitByPurchaseId(duplicatedPurchase.getId());
                    }
                    
                    processPartialSettlement(duplicatedPurchase, currentPermit, restPermit, restPayment);
                    isDuplicated = false;
                } else if (duplicatedPurchase.getChargedAmount() == restPayment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP)
                        .multiply(WebCoreUtil.HUNDRED).intValue()) {
                    
                    /* compare <Payment> location. if it is last one and its expiry date is different than user input, do partial settlement */
                    if (i == paymentList.size() - 1 && (restPermit.getExpiryDate() != null && !restPermit.getExpiryDate().isEmpty())
                        && permit.getPermitExpireGmt().getTime() != DateUtil.convertFromRESTDateString(restPermit.getExpiryDate()).getTime()) {
                        /* partial settlement because expiry date is different and needs to be updated. */
                        processPartialSettlement(duplicatedPurchase, permit, restPermit, restPayment);
                        isDuplicated = false;
                    }
                    
                    /* Do nothing as this is same as original transaction. */
                    LOGGER.debug("#### No change in transaction as it is full settlement. ####");
                } else {
                    LOGGER.error("#### Requested <Amount> for partial settlement cannot be greater than charged amount. ####");
                    throw new InvalidDataException();
                }
            }
        }
        
        if (isDuplicated) {
            LOGGER.error("#### Exception in updatePermit() - Data already exists in DB. (" + purchase.toString() + ") ####");
            throw new DuplicateObjectException("Purchase with Id " + purchase.getId() + "already exists");
        }
        
    }
    
    private void processAddTime(final PointOfSale pos, final Purchase purchase, final Permit permit, final Location location,
        final RESTPermit restPermit, final RESTPayment payment) throws InvalidDataException {
        
        final Permit newPermit = new Permit();
        final Purchase newPurchase = new Purchase();
        final ProcessorTransaction proctrans = new ProcessorTransaction();
        final UnifiedRate rate = purchase.getUnifiedRate();
        final PaymentCard paymentCard = new PaymentCard();
        
        final Date paymentDate = DateUtil.convertFromRESTDateString(payment.getPaymentDate());
        
        final Date currentDate = new Date();
        
        Location newLocation = location;
        if (newLocation == null) {
            newLocation = permit.getLocation();
        }
        
        /* create Purchase */
        newPurchase.setUnifiedRate(rate);
        newPurchase.setCustomer(purchase.getCustomer());
        newPurchase.setPointOfSale(pos);
        newPurchase.setPurchaseNumber(purchase.getPurchaseNumber());
        newPurchase.setLocation(newLocation);
        newPurchase.setPaystationSetting(purchase.getPaystationSetting());
        final TransactionType transactionType = new TransactionType();
        transactionType.setId(ReportingConstants.TRANSACTION_TYPE_ADDTIME);
        newPurchase.setTransactionType(transactionType);
        newPurchase.setPurchaseGmt(paymentDate);
        newPurchase.setChargedAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
        newPurchase.setOriginalAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
        newPurchase.setRateAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
        newPurchase.setRateRevenueAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
        
        if (payment.getType().equals(RestCoreConstants.PAYMENT_TYPE_CREDITCARD)) {
            paymentCard.setAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
            final CardType cardType = new CardType();
            cardType.setId(WebCoreConstants.CREDIT_CARD);
            paymentCard.setCardType(cardType);
            final CreditCardType creditCardType = new CreditCardType();
            creditCardType.setId(CardProcessingUtil.getCreditCardTypeId(payment.getCardType()));
            paymentCard.setCreditCardType(creditCardType);
            paymentCard.setAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
            paymentCard.setCardLast4digits(Short.parseShort(payment.getLast4DigitsOfCard()));
            paymentCard.setCardProcessedGmt(paymentDate);
            paymentCard.setIsUploadedFromBoss(false);
            paymentCard.setIsRfid(false);
            paymentCard.setIsApproved(true);
            paymentCard.setPurchase(newPurchase);
            paymentCard.setProcessorTransaction(proctrans);
            
            newPurchase.setCardPaidAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
            final PaymentType paymentType = new PaymentType();
            paymentType.setId((byte) WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_EXTERNAL);
            newPurchase.setPaymentType(paymentType);
            
            final ProcessorTransactionType procTransType = new ProcessorTransactionType();
            procTransType.setId(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_EXTERNAL);
            proctrans.setProcessorTransactionType(procTransType);
            paymentCard.setProcessorTransactionType(procTransType);
            
            proctrans.setCreatedGmt(DateUtil.getCurrentGmtDate());
            
            proctrans.setPurchase(newPurchase);
            proctrans.setPointOfSale(pos);
            proctrans.setPurchasedDate(paymentDate);
            proctrans.setTicketNumber(purchase.getPurchaseNumber());
            proctrans.setAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
            proctrans.setCardType(payment.getCardType());
            proctrans.setLast4digitsOfCardNumber(Short.parseShort(payment.getLast4DigitsOfCard()));
            proctrans.setProcessingDate(paymentDate);
            proctrans.setAuthorizationNumber(payment.getCardAuthorizationId());
            proctrans.setIsApproved(true);
            
            proctrans.setMerchantAccount(null);
            proctrans.setCardChecksum(0);
            proctrans.setProcessorTransactionId(WebCoreConstants.EMPTY_STRING);
            proctrans.setReferenceNumber(WebCoreConstants.EMPTY_STRING);
            proctrans.setCardHash(WebCoreConstants.EMPTY_STRING);
            proctrans.setIsUploadedFromBoss(false);
            proctrans.setIsRfid(false);
            
        }
        
        newPermit.setPurchase(newPurchase);
        newPermit.setPermitNumber(permit.getPermitNumber());
        newPermit.setLocation(newLocation);
        final PermitType permitType = new PermitType();
        permitType.setId(ReportingConstants.PERMIT_TYPE_ADDTIME);
        newPermit.setPermitType(permitType);
        newPermit.setPermitBeginGmt(permit.getPermitExpireGmt());
        newPermit.setPermitExpireGmt(DateUtil.convertFromRESTDateString(restPermit.getExpiryDate()));
        newPermit.setPermitOriginalExpireGmt(DateUtil.convertFromRESTDateString(restPermit.getExpiryDate()));
        
        // Update original Permit
        permit.setPermitExpireGmt(newPermit.getPermitExpireGmt());
        permit.setNumberOfExtensions(permit.getNumberOfExtensions() + 1);
        newPermit.setPermit(permit);
        
        newPermit.setSpaceNumber(permit.getSpaceNumber());
        newPermit.setLicencePlate(permit.getLicencePlate());
        newPermit.setPermitIssueType(permit.getPermitIssueType());
        
        this.transactionService.createTransaction(newPermit, newPurchase, paymentCard, proctrans);
    }
    
    public final void processPartialSettlement(final Purchase purchase, final Permit permit, final RESTPermit restPermit, final RESTPayment payment)
        throws ApplicationException {
        try {
            
            final Date paymentDate = purchase.getPurchaseGmt();
            
            final ProcessorTransaction proctrans = this.processorTransactionService
                    .findPartialSettlementProcessorTransaction(purchase.getPointOfSale().getId(), paymentDate, purchase.getPurchaseNumber(), true);
            
            if (proctrans == null) {
                LOGGER.error("#### ProcessorTransaction does not exist. ####");
                throw new InvalidDataException();
            }
            
            final PaymentCard paymentCard =
                    this.paymentCardService.findPaymentCardByPurchaseIdAndProcessorTransactionId(purchase.getId(), proctrans.getId());
            
            if (restPermit.getExpiryDate() != null && !restPermit.getExpiryDate().isEmpty()) {
                
                Date restExpireDate = DateUtil.convertFromRESTDateString(restPermit.getExpiryDate());
                permit.setPermitExpireGmt(restExpireDate);
                
                if (permit.getPermit() != null) {
                    permit.getPermit().setPermitExpireGmt(permit.getPermitExpireGmt());
                }
            }
            
            purchase.setChargedAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
            purchase.setRateRevenueAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
            purchase.setRateAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
            
            proctrans.setAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
            purchase.setCardPaidAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
            final PaymentType paymentType = new PaymentType();
            paymentType.setId((byte) WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_EXTERNAL);
            purchase.setPaymentType(paymentType);
            paymentCard.setAmount(proctrans.getAmount());
            
            this.transactionService.createTransaction(permit, purchase, paymentCard, proctrans);
        } catch (InvalidDataException e) {
            LOGGER.error("#### Exception due to multiple ProcessorTransaction found in processPartialSettlement. (" + restPermit.toString()
                         + StandardConstants.STRING_CLOSE_PARENTHESIS);
            throw new InvalidDataException();
        }
    }
    
    // No Permit is created for processRefund however the original PermitExpiry date is updated. 
    // Additional permits will not add up to the original permits expiryGMT. 
    public final void processRefund(final PointOfSale pos, final Purchase purchase, final Permit permit, final Location location,
        final RESTPermit restPermit, final RESTPayment payment) throws ApplicationException {
        
        final Purchase newPurchase = new Purchase();
        final ProcessorTransaction proctrans = new ProcessorTransaction();
        final UnifiedRate rate = purchase.getUnifiedRate();
        final PaymentCard paymentCard = new PaymentCard();
        
        final Date paymentDate = DateUtil.convertFromRESTDateString(payment.getPaymentDate());
        
        Location newLocation = location;
        if (newLocation == null) {
            newLocation = permit.getLocation();
        }
        
        /* create Purchase */
        newPurchase.setUnifiedRate(rate);
        newPurchase.setCustomer(purchase.getCustomer());
        newPurchase.setPointOfSale(pos);
        newPurchase.setPurchaseNumber(purchase.getPurchaseNumber());
        newPurchase.setLocation(newLocation);
        newPurchase.setPaystationSetting(purchase.getPaystationSetting());
        final TransactionType transactionType = new TransactionType();
        transactionType.setId(ReportingConstants.TRANSACTION_TYPE_REFUND_3RD_PARTY);
        newPurchase.setTransactionType(transactionType);
        newPurchase.setPurchaseGmt(paymentDate);
        newPurchase.setChargedAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
        newPurchase.setOriginalAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
        newPurchase.setRateAmount(0);
        newPurchase.setRateRevenueAmount(0);
        newPurchase.setIsRefundSlip(true);
        
        if (payment.getType().equals(RestCoreConstants.PAYMENT_TYPE_CREDITCARD)) {
            paymentCard.setAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
            final CardType cardType = new CardType();
            cardType.setId(WebCoreConstants.CREDIT_CARD);
            paymentCard.setCardType(cardType);
            final CreditCardType creditCardType = new CreditCardType();
            creditCardType.setId(CardProcessingUtil.getCreditCardTypeId(payment.getCardType()));
            paymentCard.setCreditCardType(creditCardType);
            paymentCard.setAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
            paymentCard.setCardLast4digits(Short.parseShort(payment.getLast4DigitsOfCard()));
            paymentCard.setCardProcessedGmt(new Date());
            paymentCard.setIsUploadedFromBoss(false);
            paymentCard.setIsRfid(false);
            paymentCard.setIsApproved(true);
            paymentCard.setPurchase(newPurchase);
            paymentCard.setProcessorTransaction(proctrans);
            
            newPurchase.setCardPaidAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
            final PaymentType paymentType = new PaymentType();
            paymentType.setId((byte) WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_EXTERNAL);
            newPurchase.setPaymentType(paymentType);
            
            final ProcessorTransactionType procTransType = new ProcessorTransactionType();
            procTransType.setId(CardProcessingConstants.PROCESSOR_TRANSACTION_TYPE_CHARGE_EXTERNAL_WITH_REFUND);
            proctrans.setProcessorTransactionType(procTransType);
            paymentCard.setProcessorTransactionType(procTransType);
            
            proctrans.setCreatedGmt(DateUtil.getCurrentGmtDate());
            
            proctrans.setPurchase(newPurchase);
            proctrans.setPointOfSale(pos);
            proctrans.setPurchasedDate(paymentDate);
            proctrans.setTicketNumber(purchase.getPurchaseNumber());
            proctrans.setAmount(payment.getAmount().abs().setScale(2, BigDecimal.ROUND_HALF_UP).multiply(WebCoreUtil.HUNDRED).intValue());
            proctrans.setCardType(payment.getCardType());
            proctrans.setLast4digitsOfCardNumber(Short.parseShort(payment.getLast4DigitsOfCard()));
            proctrans.setProcessingDate(new Date());
            proctrans.setAuthorizationNumber(payment.getCardAuthorizationId());
            proctrans.setIsApproved(true);
            
            proctrans.setMerchantAccount(null);
            proctrans.setCardChecksum(0);
            proctrans.setProcessorTransactionId(WebCoreConstants.EMPTY_STRING);
            proctrans.setReferenceNumber(WebCoreConstants.EMPTY_STRING);
            proctrans.setCardHash(WebCoreConstants.EMPTY_STRING);
            proctrans.setIsUploadedFromBoss(false);
            proctrans.setIsRfid(false);
            
        }
        
        // Update original Permit
        if (restPermit.getExpiryDate() != null && !restPermit.getExpiryDate().isEmpty()) {
            permit.setPermitExpireGmt(DateUtil.convertFromRESTDateString(restPermit.getExpiryDate()));
        }
        
        ProcessorTransaction originPt = null;
        
        originPt = this.processorTransactionService.findPartialSettlementProcessorTransaction(purchase.getPointOfSale().getId(), paymentDate,
                                                                                              purchase.getPurchaseNumber(), false);
        
        if (originPt == null) {
            LOGGER.error("#### No old ProcessorTransaction record exists. (PaysationId: " + purchase.getPointOfSale().getId() + ", PurchasedDate: "
                         + purchase.getPurchaseGmt() + ", TicketNumber: " + purchase.getPurchaseNumber() + StandardConstants.STRING_EMPTY_SPACE
                         + StandardConstants.STRING_FOUR_NUMBER_SIGNS);
            throw new InvalidParameterException();
        }
        
        this.transactionService.createTransaction(permit, newPurchase, paymentCard, proctrans);
    }
    
}
