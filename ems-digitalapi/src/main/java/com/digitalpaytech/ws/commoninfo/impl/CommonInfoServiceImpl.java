package com.digitalpaytech.ws.commoninfo.impl;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.ActivePermit;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.DigitalAPIUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.ws.BaseWsServiceImpl;
import com.digitalpaytech.ws.commoninfo.CommonInfoService;
import com.digitalpaytech.ws.commoninfo.GroupRequest;
import com.digitalpaytech.ws.commoninfo.GroupResponse;
import com.digitalpaytech.ws.commoninfo.GroupType;
import com.digitalpaytech.ws.commoninfo.InfoServiceFault;
import com.digitalpaytech.ws.commoninfo.InfoServiceFaultMessage;
import com.digitalpaytech.ws.commoninfo.PaystationRequest;
import com.digitalpaytech.ws.commoninfo.PaystationResponse;
import com.digitalpaytech.ws.commoninfo.PaystationType;
import com.digitalpaytech.ws.commoninfo.RegionRequest;
import com.digitalpaytech.ws.commoninfo.RegionResponse;
import com.digitalpaytech.ws.commoninfo.RegionType;
import com.digitalpaytech.ws.commoninfo.SettingRequest;
import com.digitalpaytech.ws.commoninfo.SettingResponse;
import com.digitalpaytech.ws.commoninfo.SettingType;

@SuppressWarnings({"PMD.GodClass", "PMD.ExcessiveImports", "PMD.AvoidInstantiatingObjectsInLoops"})
public class CommonInfoServiceImpl extends BaseWsServiceImpl implements CommonInfoService {
    private static final Logger LOGGER = Logger.getLogger(CommonInfoServiceImpl.class);
    
    public CommonInfoServiceImpl() {
        super();
    }
    
    public CommonInfoServiceImpl(final int endPointType) {
        super();
        super.webServiceEndPointType = endPointType;
    }
    
    public final InfoServiceFault createInfoServiceFault(final String message) {
        final InfoServiceFault fault = new InfoServiceFault();
        fault.setShortErrorMessage(message);
        return fault;
    }
    
    public final InfoServiceFault createInfoServiceFault(final String message, final Object... args) {
        final InfoServiceFault fault = new InfoServiceFault();
        fault.setShortErrorMessage(MessageFormat.format(message, args));
        return fault;
    }
    
    public final String createInfoServiceFaultString(final String message, final Object... args) {
        return MessageFormat.format(message, args);
    }
    
    protected final Authentication authenticationCheck(final String messageName, final String token) throws InfoServiceFaultMessage {
        final Authentication auth = authorize(token);
        if (auth == null) {
            throw new InfoServiceFaultMessage(messageName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        return auth;
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    public RegionResponse getRegions(final RegionRequest regionRequest) throws InfoServiceFaultMessage {
        LOGGER.debug("++++++++++++++ CommonServiceImpl.getRegions(RegionRequest regionRequest) ++++++++++++");
        final Authentication auth = authenticationCheck(GET_REGIONS, regionRequest.getToken());
        
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage("getRegions Failure", createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        this.checkToken(regionRequest.getToken(), webUser.getCustomerId(), GET_REGIONS);
        final int customerId = webUser.getCustomerId();
        return getLocations(customerId);
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    public GroupResponse getGroups(final GroupRequest groupRequest) throws InfoServiceFaultMessage {
        LOGGER.debug("++++++++++++++ CommonServiceImpl.getGroups(GroupRequest groupRequest) ++++++++++++");
        final Authentication auth = authenticationCheck(GET_GROUPS, groupRequest.getToken());
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage("getGroups Failure", createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        this.checkToken(groupRequest.getToken(), webUser.getCustomerId(), GET_GROUPS);
        final int customerId = webUser.getCustomerId();
        return getRoutes(customerId);
    }
    
    @SuppressWarnings("checkstyle:designforextension")
    public PaystationResponse getPaystations(final PaystationRequest paystationRequest) throws InfoServiceFaultMessage {
        LOGGER.debug("++++++++++++++ CommonServiceImpl.getPaystations(PaystationRequest paystationRequest) ++++++++++++");
        final Authentication auth = authenticationCheck(GET_PAYSTATIONS, paystationRequest.getToken());
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage("getPaystations Failure", createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        // check the token
        this.checkToken(paystationRequest.getToken(), webUser.getCustomerId(), GET_PAYSTATIONS);
        
        final int customerId = webUser.getCustomerId();
        return getPaystations(customerId);
    }
    
    protected final PaystationResponse getPaystations(final int customerId) {
        final List<PointOfSale> posList = super.pointOfSaleService.findPointOfSalesByCustomerId(customerId, true);
        final PaystationResponse response = new PaystationResponse();
        for (PointOfSale pos : posList) {
            final PaystationType paystationInfoType = convertToInfoType(pos);
            response.getPaystations().add(paystationInfoType);
        }
        return response;
        
    }
    
    private PaystationType convertToInfoType(final PointOfSale pos) {
        
        final PaystationType paystationInfoType = new PaystationType();
        paystationInfoType.setSerialNumber(pos.getSerialNumber());
        paystationInfoType.setPaystationName(pos.getName());
        
        final List<Route> routeList = super.routeService.findRoutesByPointOfSaleId(pos.getId());
        for (Route route : routeList) {
            paystationInfoType.getPaystationGroup().add(route.getName());
        }
        paystationInfoType.setRegionName(pos.getLocation().getName());
        paystationInfoType.setRegionId(pos.getLocation().getId());
        
        return paystationInfoType;
    }
    
    public final SettingResponse getSettings(final SettingRequest settingRequest) throws InfoServiceFaultMessage {
        LOGGER.debug("++++++++++++++ CommonServiceImpl.getSettings(SettingRequest settingRequest) ++++++++++++");
        final Authentication auth = authenticationCheck(GET_SETTINGS, settingRequest.getToken());
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage("getSettings Failure", createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        // check the token
        this.checkToken(settingRequest.getToken(), webUser.getCustomerId(), GET_SETTINGS);
        
        final int customerId = webUser.getCustomerId();
        return getSettings(customerId);
    }
    
    protected final GroupResponse getRoutes(final int customerId) {
        final Collection<Route> routeList = super.routeService.findRoutesByCustomerId(customerId);
        
        final GroupResponse response = new GroupResponse();
        GroupType groupType;
        for (Route route : routeList) {
            groupType = new GroupType();
            groupType.setGroupName(route.getName());
            response.getGroupName().add(groupType);
        }
        return response;
    }
    
    protected final RegionResponse getLocations(final int customerId) {
        final List<Location> locationList = getLocationsWithPaystations(customerId);
        
        final RegionResponse response = new RegionResponse();
        
        for (Location location : locationList) {
            
            final RegionType regionType = new RegionType();
            
            regionType.setRegionName(location.getName());
            regionType.setDescription(location.getDescription());
            response.getRegions().add(regionType);
        }
        return response;
    }
    
    protected final SettingResponse getSettings(final int customerId) {
        final List<ActivePermit> activePermitList = super.activePermitService.getPaystationSettingsFromLast14Days(customerId);
        
        final SettingResponse response = new SettingResponse();
        
        for (ActivePermit activePermit : activePermitList) {
            final SettingType settingType = new SettingType();
            
            settingType.setSettingName(activePermit.getPaystationSettingName());
            response.getSettings().add(settingType);
        }
        
        return response;
    }
    
    public final void checkToken(final String token, final int customerId, final String methodName) throws InfoServiceFaultMessage {
        if (token == null || !DigitalAPIUtil.validateString(token)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_PARAMETER_MISSING_OR_INVALID,
                                                                                                 new Object[] { "token" }));
        }
        
        if (!isTokenValid(token, customerId)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_TOKEN_INVALID));
        }
    }
    
    public final void checkPurchasedDate(final Date fromDate, final Date toDate, final String methodName) throws InfoServiceFaultMessage {
        // modified in case of empty date
        if (fromDate == null || toDate == null) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE,
                    createInfoServiceFault(MESSAGE_DATE_PARAMETER_MISSING_OR_INVALID, new Object[] { MESSAGE_PURCHASEDDATEFROM_PURCHASEDDATETO }));
        } else if (fromDate.equals(WebCoreConstants.EMPTY_DATE) || toDate.equals(WebCoreConstants.EMPTY_DATE)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE,
                    createInfoServiceFault(MESSAGE_DATE_PARAMETER_MISSING_OR_INVALID, new Object[] { MESSAGE_PURCHASEDDATEFROM_PURCHASEDDATETO }));
        }
        if (toDate.before(fromDate)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_RANGE_INVALID, new Object[] {
                PURCHASED_DATE_FROM, PURCHASED_DATE_TO, }));
        }
        final String fromDay = DateUtil.createDateOnlyString(fromDate);
        final String toDay = DateUtil.createDateOnlyString(toDate);
        if (!fromDay.equals(toDay)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_ONEDAY_RANGE, new Object[] {
                PURCHASED_DATE_FROM, PURCHASED_DATE_TO, }));
        }
    }
    
    // modified in case of empty date
    public final void checkSettlementDate(final Date fromDate, final Date toDate, final String methodName) throws InfoServiceFaultMessage {
        if (fromDate == null || toDate == null) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE,
                    createInfoServiceFault(MESSAGE_DATE_PARAMETER_MISSING_OR_INVALID, new Object[] { MESSAGE_SETTLEMENTDATEFROM_SETTLEMENTDATETO }));
        } else if (fromDate.equals(WebCoreConstants.EMPTY_DATE) || toDate.equals(WebCoreConstants.EMPTY_DATE)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE,
                    createInfoServiceFault(MESSAGE_DATE_PARAMETER_MISSING_OR_INVALID, new Object[] { MESSAGE_SETTLEMENTDATEFROM_SETTLEMENTDATETO }));
        }
        if (toDate.before(fromDate)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_RANGE_INVALID, new Object[] {
                SETTLEMENT_DATE_FROM, SETTLEMENT_DATE_TO, }));
        }
        final String fromDay = DateUtil.createDateOnlyString(fromDate);
        final String toDay = DateUtil.createDateOnlyString(toDate);
        if (!fromDay.equals(toDay)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_ONEDAY_RANGE, new Object[] {
                SETTLEMENT_DATE_FROM, SETTLEMENT_DATE_TO, }));
        }
    }
    
    @SuppressWarnings("PMD.InefficientEmptyStringCheck")
    public final void checkStallRange(final String fromStall, final String toStall, final String methodName) throws InfoServiceFaultMessage {
        if (fromStall == null || toStall == null) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE,
                    createInfoServiceFault(MESSAGE_PARAMETER_MISSING_OR_INVALID, new Object[] { MESSAGE_STALLNUMBERFROM_STALLNUMBERTO }));
        }
        String temp;
        
        if (fromStall.trim().length() == 0 || toStall.trim().length() == 0) {
            temp = fromStall.trim() + toStall.trim();
            if (temp.length() != 0) {
                throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE,
                        createInfoServiceFault(MESSAGE_PARAMETER_MISSING_OR_INVALID, new Object[] { MESSAGE_STALLNUMBERFROM_STALLNUMBERTO }));
            }
        } else {
            final int stallNumberFrom = Integer.parseInt(fromStall.trim());
            final int stallNumberTo = Integer.parseInt(toStall.trim());
            if (stallNumberFrom > stallNumberTo) {
                throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_RANGE_INVALID, new Object[] {
                    "stallNumberFrom", "stallNumberTo", }));
            }
        }
    }
    
    public final void checkSerialNumber(final String serialNumber, final String methodName) throws InfoServiceFaultMessage {
        if (serialNumber == null || serialNumber.isEmpty() || !DigitalAPIUtil.validateString(serialNumber)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_PARAMETER_MISSING_OR_INVALID,
                                                                                                 new Object[] { "serialNumber" }));
        }
    }
    
    protected final String getCurrentMethodName(final int depth) {
        /*
         * depth 0: the name of the method itself. In here, returns getCurrentMethodName.
         * depth 1: the name of the method which invoked this method.
         */
        final StackTraceElement[] stackTraceElements = new Throwable().getStackTrace();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("++++ getCurrentMethodName(): " + stackTraceElements);
        }
        return stackTraceElements[depth].getMethodName();
    }
    
    protected final void validateRouteName(final String groupName, final String label) throws InfoServiceFaultMessage {
        if (StringUtils.isEmpty(groupName)) {
            throw new InfoServiceFaultMessage(getCurrentMethodName(2) + SPACE_FAILURE, createInfoServiceFault(MESSAGE_PARAMETER_MISSING_OR_INVALID,
                                                                                                              new Object[] { label }));
        }
        
        if (groupName.length() < WebCoreConstants.VALIDATION_MIN_LENGTH_1 || groupName.length() > WebCoreConstants.VALIDATION_MAX_LENGTH_25) {
            throw new InfoServiceFaultMessage(getCurrentMethodName(2) + SPACE_FAILURE, createInfoServiceFault(MESSAGE_INVALID_LENGTH, new Object[] {
                label, WebCoreConstants.VALIDATION_MIN_LENGTH_1, WebCoreConstants.VALIDATION_MAX_LENGTH_25, }));
        }
        
        if (!groupName.matches(WebCoreConstants.REGEX_STANDARD_TEXT)) {
            throw new InfoServiceFaultMessage(getCurrentMethodName(2) + SPACE_FAILURE,
                    createInfoServiceFault(MESSAGE_INVALID_CHARACTER, new Object[] { label, MESSAGE_LETTERS_DIGITS_SPACE_DOT_COMMA_DASH, }));
        }
    }
    
    protected final void validateLocationName(final String regionName, final String label) throws InfoServiceFaultMessage {
        if (StringUtils.isEmpty(regionName)) {
            throw new InfoServiceFaultMessage(getCurrentMethodName(2) + SPACE_FAILURE, createInfoServiceFault(MESSAGE_PARAMETER_MISSING_OR_INVALID,
                                                                                                              new Object[] { label }));
        }
        
        if (regionName.length() < WebCoreConstants.VALIDATION_MIN_LENGTH_4 || regionName.length() > WebCoreConstants.VALIDATION_MAX_LENGTH_25) {
            throw new InfoServiceFaultMessage(getCurrentMethodName(2) + SPACE_FAILURE, createInfoServiceFault(MESSAGE_INVALID_LENGTH, new Object[] {
                label, WebCoreConstants.VALIDATION_MIN_LENGTH_4, WebCoreConstants.VALIDATION_MAX_LENGTH_25, }));
        }
        
        if (!regionName.matches(WebCoreConstants.REGEX_LOCATION_TEXT)) {
            throw new InfoServiceFaultMessage(getCurrentMethodName(2) + SPACE_FAILURE,
                    createInfoServiceFault(MESSAGE_INVALID_CHARACTER, new Object[] { label, MESSAGE_LETTERS_DIGITS_SPACE_DOT_COMMA_DASH, }));
        }
    }
    
}
