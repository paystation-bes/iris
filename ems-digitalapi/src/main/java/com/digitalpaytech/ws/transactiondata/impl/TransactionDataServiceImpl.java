package com.digitalpaytech.ws.transactiondata.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.ProcessorTransactionType;
import com.digitalpaytech.dto.paystation.TransactionInfoDto;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.PaymentTypeService;
import com.digitalpaytech.service.ProcessorTransactionTypeService;
import com.digitalpaytech.service.TransactionTypeService;
import com.digitalpaytech.service.paystation.TransactionInfoDtoService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.DigitalAPIUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.ws.BaseWsServiceImpl;
import com.digitalpaytech.ws.transactiondata.InfoServiceFault;
import com.digitalpaytech.ws.transactiondata.InfoServiceFaultMessage;
import com.digitalpaytech.ws.transactiondata.LocationRequest;
import com.digitalpaytech.ws.transactiondata.LocationResponse;
import com.digitalpaytech.ws.transactiondata.LocationType;
import com.digitalpaytech.ws.transactiondata.PaymentType;
import com.digitalpaytech.ws.transactiondata.PaymentTypeRequest;
import com.digitalpaytech.ws.transactiondata.PaymentTypeResponse;
import com.digitalpaytech.ws.transactiondata.PaystationRequest;
import com.digitalpaytech.ws.transactiondata.PaystationResponse;
import com.digitalpaytech.ws.transactiondata.PaystationType;
import com.digitalpaytech.ws.transactiondata.ProcessingStatusType;
import com.digitalpaytech.ws.transactiondata.ProcessingStatusTypeRequest;
import com.digitalpaytech.ws.transactiondata.ProcessingStatusTypeResponse;
import com.digitalpaytech.ws.transactiondata.TransactionByUpdateDateRequest;
import com.digitalpaytech.ws.transactiondata.TransactionDataService;
import com.digitalpaytech.ws.transactiondata.TransactionDataType;
import com.digitalpaytech.ws.transactiondata.TransactionResponse;
import com.digitalpaytech.ws.transactiondata.TransactionType;
import com.digitalpaytech.ws.transactiondata.TransactionTypeRequest;
import com.digitalpaytech.ws.transactiondata.TransactionTypeResponse;
import com.digitalpaytech.ws.transactiondata.VersionType;
import com.digitalpaytech.ws.util.TransactionInfoServiceUtil;

@Component("transactionDataService")
@WebService(serviceName = "TransactionDataService", targetNamespace = "http://ws.digitalpaytech.com/transactionData", endpointInterface = "com.digitalpaytech.ws.transactiondata.TransactionDataService")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
@SuppressWarnings({ "PMD.AvoidUsingShortType", "PMD.GodClass", "PMD.ExcessiveImports" })
public class TransactionDataServiceImpl extends BaseWsServiceImpl implements TransactionDataService {
    
    public static final byte PAYMENT_TYPE_CREDIT_CARD_SWIPE = 2;
    public static final byte PAYMENT_TYPE_CASH_CREDIT_SWIPE = 5;
    
    public static final byte PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS = 11;
    public static final byte PAYMENT_TYPE_CASH_CREDIT_CL = 12;
    public static final byte PAYMENT_TYPE_CREDIT_CARD_CHIP = 13;
    public static final byte PAYMENT_TYPE_CASH_CREDIT_CH = 14;
    public static final byte PAYMENT_TYPE_CREDIT_CARD_EXTERNAL = 15;
    
    private static final Logger LOGGER = Logger.getLogger(TransactionDataServiceImpl.class);
    
    private static final String GETTRANSACTION_BY_UPDATE_DATE = "getTransactionByUpdateDate";
    
    @Autowired
    protected TransactionInfoDtoService transactionInfoDtoService;
    
    @Autowired
    protected TransactionTypeService transactionTypeService;
    
    @Autowired
    protected PaymentTypeService paymentTypeService;
    
    @Autowired
    protected ProcessorTransactionTypeService processorTransactionTypeService;
    
    // Legacy PaymentTypes
    private PaymentTypeResponse legacyPaymentTypeResponse = new PaymentTypeResponse();
    
    public TransactionDataServiceImpl() {
        super();
        webServiceEndPointType = WebCoreConstants.END_POINT_ID_TRANSACTION_DATA;
        
        final PaymentTypeResponse paymentTypeResponse = new PaymentTypeResponse();
        final List<com.digitalpaytech.domain.PaymentType> paymentTypeList = getLegacyPaymentTypes();
        
        for (com.digitalpaytech.domain.PaymentType pt : paymentTypeList) {
            final PaymentType paymentType = new PaymentType();
            paymentType.setId(pt.getId());
            paymentType.setName(pt.getName());
            paymentType.setDescription(pt.getDescription());
            paymentTypeResponse.getPaymentType().add(paymentType);
        }
        this.legacyPaymentTypeResponse = paymentTypeResponse;
        
    }
    
    public final TransactionInfoDtoService getTransactionInfoDtoService() {
        return this.transactionInfoDtoService;
    }
    
    public final void setTransactionInfoDtoService(final TransactionInfoDtoService transactionInfoDtoService) {
        this.transactionInfoDtoService = transactionInfoDtoService;
    }
    
    public final TransactionTypeService getTransactionTypeService() {
        return this.transactionTypeService;
    }
    
    public final void setTransactionTypeService(final TransactionTypeService transactionTypeService) {
        this.transactionTypeService = transactionTypeService;
    }
    
    public final PaymentTypeService getPaymentTypeService() {
        return this.paymentTypeService;
    }
    
    public final void setPaymentTypeService(final PaymentTypeService paymentTypeService) {
        this.paymentTypeService = paymentTypeService;
    }
    
    public final ProcessorTransactionTypeService getProcessorTransactionTypeService() {
        return this.processorTransactionTypeService;
    }
    
    public final void setProcessorTransactionTypeService(final ProcessorTransactionTypeService processorTransactionTypeService) {
        this.processorTransactionTypeService = processorTransactionTypeService;
    }
    
    public final PaymentTypeResponse getLegacyPaymentTypeResponse() {
        return this.legacyPaymentTypeResponse;
    }
    
    public final void setLegacyPaymentTypeResponse(final PaymentTypeResponse legacyPaymentTypeResponse) {
        this.legacyPaymentTypeResponse = legacyPaymentTypeResponse;
    }
    
    protected final InfoServiceFault createInfoServiceFault(final String message) {
        final InfoServiceFault fault = new InfoServiceFault();
        fault.setShortErrorMessage(message);
        return fault;
    }
    
    protected final InfoServiceFault createInfoServiceFault(final String message, final Object... args) {
        final InfoServiceFault fault = new InfoServiceFault();
        fault.setShortErrorMessage(MessageFormat.format(message, args));
        return fault;
    }
    
    private List<com.digitalpaytech.domain.PaymentType> getLegacyPaymentTypes() {
        
        final List<com.digitalpaytech.domain.PaymentType> paymentTypeList = new ArrayList<com.digitalpaytech.domain.PaymentType>();
        paymentTypeList.add(new com.digitalpaytech.domain.PaymentType((byte) WebCoreConstants.PAYMENT_TYPE_CASH, "Cash", "Cash Only", DateUtil
                .getCurrentGmtDate(), WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID));
        paymentTypeList.add(new com.digitalpaytech.domain.PaymentType((byte) WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_SWIPE, "Credit Card",
                "Credit Card Only", DateUtil.getCurrentGmtDate(), WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID));
        paymentTypeList.add(new com.digitalpaytech.domain.PaymentType((byte) WebCoreConstants.PAYMENT_TYPE_SMART_CARD, "Smart Card",
                "Smart Card Only", DateUtil.getCurrentGmtDate(), WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID));
        paymentTypeList.add(new com.digitalpaytech.domain.PaymentType((byte) WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_SWIPE, "Cash/CC",
                "Cash and Credit Card", DateUtil.getCurrentGmtDate(), WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID));
        paymentTypeList.add(new com.digitalpaytech.domain.PaymentType((byte) WebCoreConstants.PAYMENT_TYPE_CASH_SMART, "Cash/SC",
                "Cash and Smart Card", DateUtil.getCurrentGmtDate(), WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID));
        paymentTypeList.add(new com.digitalpaytech.domain.PaymentType((byte) WebCoreConstants.PAYMENT_TYPE_VALUE_CARD, "Passcard", "Passcard Only",
                DateUtil.getCurrentGmtDate(), WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID));
        paymentTypeList.add(new com.digitalpaytech.domain.PaymentType((byte) WebCoreConstants.PAYMENT_TYPE_CASH_VALUE, "Cash/Passcard",
                "Cash and Passcard", DateUtil.getCurrentGmtDate(), WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID));
        paymentTypeList.add(new com.digitalpaytech.domain.PaymentType((byte) WebCoreConstants.PAYMENT_TYPE_UNKNOWN, "Unknown",
                "Unknown Payment Type", DateUtil.getCurrentGmtDate(), WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID));
        return paymentTypeList;
    }
    
    private Authentication authenticationCheck(final String messageName, final String token) throws InfoServiceFaultMessage {
        final Authentication auth = authorize(WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_API_XCHANGE, token);
        if (auth == null) {
            throw new InfoServiceFaultMessage(messageName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        return auth;
    }
    
    private void checkToken(final String token, final int customerId, final String methodName) throws InfoServiceFaultMessage {
        if (token == null || !DigitalAPIUtil.validateString(token)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_PARAMETER_MISSING_OR_INVALID,
                                                                                                 new Object[] { "token" }));
        }
        
        if (!isTokenValidPrivate(token, customerId)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_TOKEN_INVALID));
        }
    }
    
    private String verifyVersion(final VersionType versionType) {
        String version = "";
        if (versionType == null || versionType.value().trim().equals(WebCoreConstants.EMPTY_STRING)) {
            version = WS_VERSION_1_0;
        } else {
            version = versionType.value();
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.info("+++ version: " + version + " +++");
        }
        return version;
    }
    
    @Override
    public final PaystationResponse getPaystations(final PaystationRequest paystationRequest) throws InfoServiceFaultMessage {
        final Authentication auth = authenticationCheck(GET_PAYSTATIONS, paystationRequest.getToken());
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYSTATIONS + SPACE_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        
        // check the token, will throw InfoServiceFaultMessage if invalid
        checkToken(paystationRequest.getToken(), customerId, GET_PROCESSINGSTATUSTYPES);
        
        return getPaystations(customerId);
    }
    
    protected final PaystationResponse getPaystations(final int customerId) {
        final List<PointOfSale> posList = super.pointOfSaleService.findPointOfSalesByCustomerId(customerId, true);
        final PaystationResponse response = new PaystationResponse();
        for (PointOfSale pos : posList) {
            final PaystationType paystationInfoType = convertToInfoType(pos);
            response.getPaystations().add(paystationInfoType);
        }
        return response;
    }
    
    private PaystationType convertToInfoType(final PointOfSale pos) {
        
        final PaystationType paystationInfoType = new PaystationType();
        paystationInfoType.setSerialNumber(pos.getSerialNumber());
        paystationInfoType.setPaystationName(pos.getName());
        paystationInfoType.setLocationId(pos.getLocation().getId());
        return paystationInfoType;
    }
    
    @Override
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    public final ProcessingStatusTypeResponse getProcessingStatusTypes(final ProcessingStatusTypeRequest processingStatusTypeRequest)
        throws InfoServiceFaultMessage {
        final Authentication auth = authenticationCheck(GET_PROCESSINGSTATUSTYPES, processingStatusTypeRequest.getToken());
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PROCESSINGSTATUSTYPES + SPACE_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        
        // check the token, will throw InfoServiceFaultMessage if invalid
        checkToken(processingStatusTypeRequest.getToken(), customerId, GET_PROCESSINGSTATUSTYPES);
        
        final ProcessingStatusTypeResponse processingStatusTypeResponse = new ProcessingStatusTypeResponse();
        final List<ProcessorTransactionType> ptList = this.processorTransactionTypeService.loadAll();
        for (ProcessorTransactionType pst : ptList) {
            final ProcessingStatusType procType = new ProcessingStatusType();
            procType.setId((byte) pst.getId());
            procType.setName(pst.getStatusName());
            procType.setDescription(pst.getDescription());
            processingStatusTypeResponse.getProcessingStatusType().add(procType);
        }
        return processingStatusTypeResponse;
    }
    
    @Override
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    public final TransactionTypeResponse getTransactionTypes(final TransactionTypeRequest transactionTypeRequest) throws InfoServiceFaultMessage {
        final Authentication auth = authenticationCheck(GET_TRANSACTIONTYPES, transactionTypeRequest.getToken());
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_TRANSACTIONTYPES + SPACE_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        
        // check the token, will throw InfoServiceFaultMessage if invalid
        checkToken(transactionTypeRequest.getToken(), customerId, GET_TRANSACTIONTYPES);
        
        final TransactionTypeResponse transactionTypeResponse = new TransactionTypeResponse();
        final List<com.digitalpaytech.domain.TransactionType> transactionTypeList = this.transactionTypeService.loadAll();
        for (com.digitalpaytech.domain.TransactionType tt : transactionTypeList) {
            final TransactionType transType = new TransactionType();
            transType.setId(tt.getId().byteValue());
            transType.setName(tt.getName());
            transType.setDescription(tt.getDescription());
            transactionTypeResponse.getTransactionType().add(transType);
        }
        return transactionTypeResponse;
    }
    
    @Override
    public final LocationResponse getLocations(final LocationRequest locationRequest) throws InfoServiceFaultMessage {
        // verify input parameters
        final String token = locationRequest.getToken();
        final Authentication auth = authenticationCheck(GET_LOCATIONS, token);
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_LOCATIONS + SPACE_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        
        // check the token, will throw InfoServiceFaultMessage if invalid
        checkToken(token, customerId, GET_LOCATIONS);
        
        return getLocations(customerId);
    }
    
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    protected final LocationResponse getLocations(final int customerId) {
        final List<Location> locationList = getLocationsWithPaystations(customerId);
        final LocationResponse response = new LocationResponse();
        for (Location location : locationList) {
            final LocationType locationType = new LocationType();
            locationType.setLocationId(location.getId());
            locationType.setLocationName(location.getName());
            locationType.setDescription(location.getDescription());
            response.getLocations().add(locationType);
        }
        return response;
    }
    
    @Override
    @SuppressWarnings("PMD.AvoidInstantiatingObjectsInLoops")
    public final PaymentTypeResponse getPaymentTypes(final PaymentTypeRequest paymentTypeRequest) throws InfoServiceFaultMessage {
        final Authentication auth = authenticationCheck(GET_PAYMENTTYPES, paymentTypeRequest.getToken());
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYMENTTYPES + SPACE_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        
        // verify input parameters
        final String token = paymentTypeRequest.getToken();
        
        // check the token, will throw InfoServiceFaultMessage if invalid
        checkToken(token, customerId, GET_PAYMENTTYPES);
        
        final String version = this.verifyVersion(paymentTypeRequest.getVersion());
        
        if (TransactionInfoServiceUtil.isEqualOrGreaterTo(WS_VERSION_1_1_LUKEII, version)) {
            final PaymentTypeResponse paymentTypeResponse = new PaymentTypeResponse();
            final List<com.digitalpaytech.domain.PaymentType> paymentTypeList = this.paymentTypeService.loadAll();
            for (com.digitalpaytech.domain.PaymentType pt : paymentTypeList) {
                final PaymentType paymentType = new PaymentType();
                paymentType.setId(pt.getId());
                paymentType.setName(pt.getName());
                paymentType.setDescription(pt.getDescription());
                paymentTypeResponse.getPaymentType().add(paymentType);
            }
            return paymentTypeResponse;
        } else {
            return this.legacyPaymentTypeResponse;
        }
    }
    
    @Override
    public final TransactionResponse getTransactionByUpdateDate(final TransactionByUpdateDateRequest transactionByUpdateDateRequest)
        throws InfoServiceFaultMessage {
        final Authentication auth = authenticationCheck(GETTRANSACTION_BY_UPDATE_DATE, transactionByUpdateDateRequest.getToken());
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage("getTransactionByUpdateDate Failure", createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        
        final Date updateDateFrom = transactionByUpdateDateRequest.getUpdateDateFrom();
        final Date updateDateTo = transactionByUpdateDateRequest.getUpdateDateTo();
        
        // check purchased Date range, will throw InfoServiceFaultMessage if invalid
        checkUpdateDate(updateDateFrom, updateDateTo, GETTRANSACTION_BY_UPDATE_DATE);
        // check the token, will throw InfoServiceFaultMessage if invalid
        checkToken(transactionByUpdateDateRequest.getToken(), customerId, GETTRANSACTION_BY_UPDATE_DATE);
        
        final String version = this.verifyVersion(transactionByUpdateDateRequest.getVersion());
        
        TransactionResponse transactionResponse = new TransactionResponse();
        final Object[] args = new Object[] { customerId, updateDateFrom, updateDateTo };
        transactionResponse = getTransaction(args, WebCoreConstants.GETTRANSACTIONDATABYUPDATEDATE_TYPE, GETTRANSACTION_BY_UPDATE_DATE, version);
        return transactionResponse;
    }
    
    private void checkUpdateDate(final Date fromDate, final Date toDate, final String methodName) throws InfoServiceFaultMessage {
        // modified in case of empty date
        if (fromDate == null || toDate == null || fromDate.equals(WebCoreConstants.EMPTY_DATE) || toDate.equals(WebCoreConstants.EMPTY_DATE)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE,
                    createInfoServiceFault(MESSAGE_DATE_PARAMETER_MISSING_OR_INVALID, new Object[] { MESSAGE_UPDATEDATEFROM_UPDATEDATETO }));
        }
        if (toDate.before(fromDate)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_RANGE_INVALID, new Object[] {
                UPDATE_DATE_FROM, UPDATE_DATE_TO, }));
        }
        
        if (toDate.getTime() - fromDate.getTime() > StandardConstants.DAYS_IN_MILLIS_1) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_24HOURS_RANGE, new Object[] {
                UPDATE_DATE_FROM, UPDATE_DATE_TO, }));
        }
    }
    
    private TransactionResponse getTransaction(final Object[] args, final int reqType, final String methodName, final String version)
        throws InfoServiceFaultMessage {
        final int maxCount = super.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.TRANSACTION_DATA_MAX_RECORDS,
                                                                              EmsPropertiesService.TRANSACTION_DATA_MAX_RECORDS_DEFAULT);
        final Object[] tmpArgs = args;
        final int totalSize = this.transactionInfoDtoService.getTransactionListCount(reqType, tmpArgs);
        
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Total TransactionData Records: " + totalSize);
        }
        final TransactionResponse transactionResponse = new TransactionResponse();
        
        if (totalSize > 0) {
            if (totalSize > maxCount) {
                LOGGER.info("Total TransactionData Records : " + totalSize + " is greater than max allowed records: " + maxCount);
                throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE,
                        createInfoServiceFault("Please use more specific parameters as too many records are returned."));
            }
            final List<TransactionInfoDto> transactionList = this.transactionInfoDtoService.getTransactionList(reqType, tmpArgs);
            convertResultToTransactionDataType(transactionList, transactionResponse.getTransactionDataTypes(), version);
        }
        return transactionResponse;
    }
    
    private byte findPaymentType(final byte realPaymentTypeId, final String version) {
        if (TransactionInfoServiceUtil.isEqualOrGreaterTo(WS_VERSION_1_1_LUKEII, version)) {
            return realPaymentTypeId;
        } else {
            switch (realPaymentTypeId) {
                case PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS:
                case PAYMENT_TYPE_CREDIT_CARD_CHIP:
                case PAYMENT_TYPE_CREDIT_CARD_EXTERNAL:
                    return PAYMENT_TYPE_CREDIT_CARD_SWIPE;
                case PAYMENT_TYPE_CASH_CREDIT_CL:
                case PAYMENT_TYPE_CASH_CREDIT_CH:
                    return PAYMENT_TYPE_CASH_CREDIT_SWIPE;
                default:
                    return realPaymentTypeId;
            }
        }
    }
    
    //Mapping method breaking it apart would make it harder to maintain.
    @SuppressWarnings({ "checkstyle:cyclomaticcomplexity", "checkstyle:npathcomplexity", "PMD.AvoidInstantiatingObjectsInLoops" })
    private void convertResultToTransactionDataType(final List<TransactionInfoDto> transactionList, final List<TransactionDataType> transList,
        final String version) {
        
        for (TransactionInfoDto transaction : transactionList) {
            TransactionDataType transItem = new TransactionDataType();
            
            transItem = new TransactionDataType();
            transItem.setSerialNumber(transaction.getSerialNumber());
            transItem.setPaystationName(transaction.getPaystationName());
            transItem.setSettingName(transaction.getSettingName());
            transItem.setPaystationType(transaction.getPaystationType());
            transItem.setLocationId(transaction.getLocationId());
            transItem.setTicketNumber(transaction.getTicketNumber());
            final Integer stallNumber = transaction.getSpaceNumber();
            if (stallNumber == null || stallNumber == 0) {
                transItem.setStallNumber("");
            } else {
                transItem.setStallNumber(String.valueOf(stallNumber));
            }
            transItem.setPurchasedDate(transaction.getPurchaseGMT());
            transItem.setExpirationDate(transaction.getPermitExpireGMT());
            transItem.setPaymentType(findPaymentType(transaction.getPaymentType(), version));
            transItem.setTransactionType(transaction.getTransactionType());
            if (transaction.getCardType() != null && !(transaction.getCardType()).isEmpty()) {
                transItem.setCardType(transaction.getCardType());
            }
            
            if (transaction.getLast4DigitsOfCardNumber() != null && !(transaction.getLast4DigitsOfCardNumber().toString()).isEmpty()) {
                final int digs = transaction.getLast4DigitsOfCardNumber();
                transItem.setCardNumber(WebCoreUtil.prefixZeroes((short) digs, WebCoreConstants.MAX_NUMBERS_LAST_DIGITS));
            }
            
            if (transaction.getAuthorizationNumber() != null && !transaction.getAuthorizationNumber().isEmpty()) {
                transItem.setCardProcessingAuthorizationNumber(transaction.getAuthorizationNumber());
            }
            
            if (transaction.getProcessingDate() != null && !(transaction.getProcessingDate().toString()).isEmpty()) {
                transItem.setCardProcessingDate(transaction.getProcessingDate());
            }
            
            if (transaction.getProcessorTransactionId() != null && !(transaction.getProcessorTransactionType().toString()).isEmpty()) {
                transItem.setCardProcessingStatus(transaction.getProcessorTransactionType().toString());
            }
            
            if (transaction.getReferenceNumber() != null && !(transaction.getReferenceNumber()).isEmpty()) {
                transItem.setCardProcessingReferenceNumber(transaction.getReferenceNumber());
            }
            
            if (transaction.getMerchantAccountName() != null && !(transaction.getMerchantAccountName()).isEmpty()) {
                transItem.setCardProcessingMerchantAccount(transaction.getMerchantAccountName());
            }
            
            if (transaction.getNumRetries() != null && !(transaction.getNumRetries().toString()).isEmpty()) {
                transItem.setCardProcessingRetries(transaction.getNumRetries());
            }
            
            if (transaction.getProcessorTransactionId() != null && !(transaction.getProcessorTransactionId()).isEmpty()) {
                transItem.setCardProcessingTransactionId(transaction.getProcessorTransactionId());
            }
            
            if (transaction.getCouponNumber() != null) {
                transItem.setCouponNumber(transaction.getCouponNumber());
                if (transaction.getPercentDiscount() != null && transaction.getPercentDiscount() > 0) {
                    transItem.setCouponDiscountPercent(transaction.getPercentDiscount().intValue());
                }
                if (transaction.getDollarDiscountAmount() != null && transaction.getDollarDiscountAmount().floatValue() > 0.0f) {
                    transItem
                            .setCouponDiscountDollar(transaction.getDollarDiscountAmount().floatValue() / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1);
                }
            }
            
            transItem.setExcessAmount(transaction.getExcessPaymentAmount().floatValue() / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1);
            transItem.setChargedAmount(transaction.getChargedAmount().floatValue() / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1);
            transItem.setTotalCashAmount(transaction.getCashPaidAmount().floatValue() / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1);
            transItem.setTotalCardAmount(transaction.getCardPaidAmount().floatValue() / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1);
            final float changeDispensed = transaction.getChangeDispensedAmount().floatValue() / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1;
            final byte isRefundSlip = transaction.isRefundSlip();
            transItem.setRefundChangeIssued(changeDispensed);
            if (isRefundSlip == 1) {
                final float refundTicketAmount = transItem.getTotalCashAmount() + transItem.getTotalCardAmount() - transItem.getChargedAmount()
                                                 - changeDispensed - transItem.getExcessAmount();
                transItem.setRefundTicketAmount(Math.round(refundTicketAmount * StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1)
                                                / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
            }
            // Process Value Card Type
            final String customCardData = transaction.getCustomCardData();
            final String customCardType = transaction.getCustomCardType();
            
            if ((transItem.getCardType() == null || transItem.getCardType().length() == 0) && customCardType != null && customCardType.length() != 0) {
                transItem.setCardType(customCardType);
                transItem.setCardNumber(Track2Card.getLast4DigitsOfAccountNumber(customCardData, Track2Card.TRACK_2_DELIMITER));
            }
            
            transItem.setRateId(transaction.getRateId().longValue());
            transItem.setRateName(transaction.getRateName());
            transItem.setRateValue(transaction.getRateAmount().floatValue() / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1);
            
            transItem.setPlateNumber(transaction.getLicencePlateNumber() == null ? WebCoreConstants.EMPTY_STRING : transaction
                    .getLicencePlateNumber());
            
            transList.add(transItem);
        }
    }
    
}
