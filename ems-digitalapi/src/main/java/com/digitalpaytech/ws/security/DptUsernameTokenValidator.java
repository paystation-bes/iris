package com.digitalpaytech.ws.security;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;
import org.apache.ws.security.WSSecurityException;
import org.apache.ws.security.handler.RequestData;
import org.apache.ws.security.message.token.UsernameToken;
import org.apache.ws.security.util.Base64;
import org.apache.ws.security.validate.UsernameTokenValidator;

public class DptUsernameTokenValidator extends UsernameTokenValidator {
    
    private static org.apache.commons.logging.Log log = 
        org.apache.commons.logging.LogFactory.getLog(DptUsernameTokenValidator.class);
    
    @Override
    protected void verifyDigestPassword(UsernameToken usernameToken,
                                        RequestData data) throws WSSecurityException {
        if (data.getCallbackHandler() == null) {
            throw new WSSecurityException(WSSecurityException.FAILURE, "noCallback");
        }
        
        String user = null;
        try{
        	user = URLEncoder.encode(usernameToken.getName(), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
        	log.debug(ex);
        }
        usernameToken.setName(user);
        String password = usernameToken.getPassword();
        String nonce = usernameToken.getNonce();
        String createdTime = usernameToken.getCreated();
        String pwType = usernameToken.getPasswordType();
        boolean passwordsAreEncoded = usernameToken.getPasswordsAreEncoded();
        
        
        WSPasswordCallback pwCb = 
            new WSPasswordCallback(user, password, pwType, WSPasswordCallback.USERNAME_TOKEN, data);
        try {
            data.getCallbackHandler().handle(new Callback[]{pwCb});
        } catch (IOException e) {
            if (log.isDebugEnabled()) {
                log.debug(e);
            }
            throw new WSSecurityException(
                WSSecurityException.FAILED_AUTHENTICATION, null, null, e
            );
        } catch (UnsupportedCallbackException e) {
            if (log.isDebugEnabled()) {
                log.debug(e);
            }
            throw new WSSecurityException(
                WSSecurityException.FAILED_AUTHENTICATION, null, null, e
            );
        }
        String origPassword = pwCb.getPassword();
        if (origPassword == null) {
            if (log.isDebugEnabled()) {
                log.debug("Callback supplied no password for: " + user);
            }
            throw new WSSecurityException(WSSecurityException.FAILED_AUTHENTICATION);
        }
        if (usernameToken.isHashed()) {
            String passDigest;
            if (passwordsAreEncoded) {
                passDigest = UsernameToken.doPasswordDigest(nonce, createdTime, Base64.decode(origPassword));
            } else {
                passDigest = UsernameToken.doPasswordDigest(nonce, createdTime, origPassword);
            }
            if (!passDigest.equals(password)) {
                throw new WSSecurityException(WSSecurityException.FAILED_AUTHENTICATION);
            }
        } else {
            if (!origPassword.equals(password)) {
                throw new WSSecurityException(WSSecurityException.FAILED_AUTHENTICATION);
            }
        }
    }
    
    /**
     * Verify a UsernameToken containing no password. This does nothing - but is in a separate
     * method to allow the end-user to override validation easily. 
     * @param usernameToken The UsernameToken instance to verify
     * @throws WSSecurityException on a failed authentication.
     */
    protected void verifyUnknownPassword(UsernameToken usernameToken,
                                         RequestData data) throws WSSecurityException {
        //
    }
   
}
