/* 
 * Copyright (c) 1997 - 2009  DPT  All rights reserved.
 *
 * Created on Mar 2, 2009 by rexz
 * 
 * $Header:  $
 * 
 * $Log:  $ 
 *
 */
package com.digitalpaytech.ws.security;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;

/**
 * PasswordHandler
 * 
 * @author rexz
 * @since
 * 
 */
public class PasswordHandler implements CallbackHandler
{

	public PasswordHandler()
	{
	}

	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException
	{

		// There is nothing we could actually do here...
		WSPasswordCallback pc = (WSPasswordCallback)callbacks[0];
		String uid = pc.getIdentifier();

	}
}
