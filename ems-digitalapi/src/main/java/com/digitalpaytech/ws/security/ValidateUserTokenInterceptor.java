/* 
 * Copyright (c) 1997 - 2009  DPT  All rights reserved.
 *
 * Created on Mar 2, 2009 by rexz
 * 
 * $Header:  $
 * 
 * $Log:  $ 
 *
 */

package com.digitalpaytech.ws.security;

import java.util.List;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.ws.security.WSConstants;
import org.apache.ws.security.WSSecurityEngineResult;
import org.apache.ws.security.WSUsernameTokenPrincipal;
import org.apache.ws.security.handler.WSHandlerConstants;
import org.apache.ws.security.handler.WSHandlerResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * ValidateUserTokenInterceptor calls security manager to authenticate the user.
 * 
 * @author rexz
 * @since
 * 
 */
public class ValidateUserTokenInterceptor extends AbstractPhaseInterceptor {
    
    @Autowired
    private AuthenticationManager authenticationManager;
    
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }
    
    public ValidateUserTokenInterceptor(String s) {
        super(s);
    }

    public void handleMessage(Message message) throws Fault {
        List<WSHandlerResult> result = (List<WSHandlerResult>) message.getContextualProperty(WSHandlerConstants.RECV_RESULTS);
        if (result == null) {
            throw new IllegalArgumentException(WSHandlerConstants.RECV_RESULTS + " Property not found in MessageContext?!");
        }
        
        for (int i = 0; i < result.size(); i++) {
            WSHandlerResult res = (WSHandlerResult) result.get(i);
            for (int j = 0; j < res.getResults().size(); j++) {
                WSSecurityEngineResult secRes = res.getResults().get(j);
                
                int action = ((Integer) secRes.get(WSSecurityEngineResult.TAG_ACTION)).intValue();
                // USER TOKEN
                if ((action & WSConstants.UT) > 0) {
                    WSUsernameTokenPrincipal principal = (WSUsernameTokenPrincipal) secRes.get(WSSecurityEngineResult.TAG_PRINCIPAL);
                    
                    String username = principal.getName();
                    
                    UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username, principal.getPassword());
                    authRequest.setDetails("EMS"); // Customer name
                    
                    Authentication authentication = null;
                    try {
                        authentication = this.authenticationManager.authenticate(authRequest);
                    } catch (AuthenticationException ex) {
                        throw new RuntimeException("Security processing failed");
                    }
                    // Set the Acegi Security Context
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
        }
    }
}
