package com.digitalpaytech.ws.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.WebCoreConstants;

/**
 * Filter used to block access to ProvisionService and LicenseService WSDL
 * 
 * @author pault
 * 
 */

public class InternalWsdlFilter implements Filter {
    private static final Logger LOGGER = Logger.getLogger(InternalWsdlFilter.class);
    private static final String WSDL_URL_PATTERN = "/services";
    private static final String SECURE_HTTP = "https://";
    
    private EmsPropertiesService emsPropertiesService;
    
    public final void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException,
        ServletException {
        final HttpServletRequest httpRequest = (HttpServletRequest) request;
        
        /* check out the referer value. */
        final String referer = httpRequest.getHeader(WebCoreConstants.HTTP_HEADER_REFERER);
        if (referer != null) {
            LOGGER.debug("++++ Referer information in WSDL services page: " + referer + " ++++");
            
            /* retrieve server URL from EmsProperties. */
            String serverURL = this.emsPropertiesService.getPropertyValue(EmsPropertiesService.LICENSE_SERVER_URL);
            if (referer.contains(SECURE_HTTP)) {
                if (serverURL.indexOf(WebCoreConstants.HTTP_SUFFIX) != -1) {
                    serverURL = serverURL.substring(serverURL.indexOf(WebCoreConstants.HTTP_SUFFIX) + WebCoreConstants.HTTP_SUFFIX.length());
                }
                serverURL = SECURE_HTTP + serverURL;
            }
            
            final StringBuffer urlBuffer = new StringBuffer(serverURL);
            urlBuffer.append(WSDL_URL_PATTERN);
            
            if (referer.endsWith(WebCoreConstants.ROOT_PATH)) {
                urlBuffer.append(WebCoreConstants.ROOT_PATH);
            }
            
            if (!referer.equals(urlBuffer.toString())) {
                LOGGER.error("++++ Wrong referer in WSDL services page: " + referer + " (expected: " + urlBuffer.toString() + " )++++");
                ((HttpServletResponse) response).sendError(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }
        }
        
        if ("wsdl".equalsIgnoreCase(httpRequest.getQueryString()) && httpRequest.getRequestURI().endsWith("SigningServiceV2")) {
            // block access to wsdl
            ((HttpServletResponse) response).sendError(HttpServletResponse.SC_NOT_FOUND);
        } else {
            chain.doFilter(request, response);
        }
    }
    
    public void destroy() {
        
    }
    
    public final void init(final FilterConfig config) throws ServletException {
        final WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());
        this.emsPropertiesService = (EmsPropertiesService) context.getBean("emsPropertiesService");
    }
}
