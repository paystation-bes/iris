package com.digitalpaytech.ws.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.http.server.ServletServerHttpRequest;

import com.digitalpaytech.servlet.DPTCopyableResponseWrapper;
import com.digitalpaytech.servlet.DPTResettableRequestWrapper;
import com.digitalpaytech.servlet.LoggingFilterUtil;

/**
 * Filter used to log WSDL api calls
 * 
 * @author abhaybhatia
 * 
 */

public class InternalWsdlLoggingFilter implements Filter {
    private static final Logger LOGGER = Logger.getLogger(InternalWsdlLoggingFilter.class);
    
    public final void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
        throws IOException, ServletException {
        ServletRequest requestWrapper = request;
        ServletResponse responseWrapper = response;
        long startTime = 0;
        long endTime = 0;
        
        if (LOGGER.isDebugEnabled()) {
            try {
                requestWrapper = new DPTResettableRequestWrapper((HttpServletRequest) request);
                responseWrapper = LoggingFilterUtil.initAndWrapResponse(response);
                logRequest((DPTResettableRequestWrapper) requestWrapper);
            } catch (Exception e) {
                LOGGER.error("Exception thrown while logging Api request. CorrelationId:" + LoggingFilterUtil.getCorrelationId() + " Exception: "
                             + e.getMessage());
                
                // reset request response if there were issues setting it so main call doesn't fail     
                requestWrapper = requestWrapper == null ? request : requestWrapper;
                responseWrapper = responseWrapper == null ? response : responseWrapper;
            }
            startTime = System.currentTimeMillis();
        }
        
        chain.doFilter(requestWrapper, responseWrapper);
        
        if (LOGGER.isDebugEnabled()) {
            endTime = System.currentTimeMillis();
            long timeTaken = endTime - startTime;
            try {
                if (responseWrapper instanceof DPTCopyableResponseWrapper) {
                    logResponse((DPTCopyableResponseWrapper) responseWrapper, timeTaken);
                } else {
                    LOGGER.error("Response was not wrapped correctly, hence not logging response. CorrelationId: "
                                 + LoggingFilterUtil.getCorrelationId());
                }
            } catch (Exception e) {
                LOGGER.error("Exception thrown while logging Api response. CorrelationId:" + LoggingFilterUtil.getCorrelationId() + " Exception: "
                             + e.getMessage());
            }
        }
    }
    
    public void destroy() {
    }
    
    public final void init(final FilterConfig config) throws ServletException {
    }
    
    private void logRequest(DPTResettableRequestWrapper requestWrapper) {
        StringBuilder requestBuilder = new StringBuilder();
        
        requestBuilder.append("CorrelationId:" + LoggingFilterUtil.getCorrelationId()).append(LoggingFilterUtil.DELIMITER);
        requestBuilder.append("URI:" + LoggingFilterUtil.getFullURL(requestWrapper)).append(LoggingFilterUtil.DELIMITER);
        requestBuilder.append("Method:" + requestWrapper.getMethod()).append(LoggingFilterUtil.DELIMITER);
        requestBuilder.append("ContentType:" + requestWrapper.getContentType()).append(LoggingFilterUtil.DELIMITER);
        requestBuilder.append("Headers:" + new ServletServerHttpRequest(requestWrapper).getHeaders()).append(LoggingFilterUtil.DELIMITER);
        requestBuilder.append("RequestBody:" + requestWrapper.getBodyAndResetStream());
        
        LOGGER.debug("Api Request: " + requestBuilder.toString());
    }
    
    private void logResponse(DPTCopyableResponseWrapper responseWrapper, long timeTaken) {
        StringBuilder responseBuilder = new StringBuilder();
        responseBuilder.append("CorrelationId:" + LoggingFilterUtil.getCorrelationId()).append(LoggingFilterUtil.DELIMITER);
        responseBuilder.append("Status:" + responseWrapper.getStatus()).append(LoggingFilterUtil.DELIMITER);
        responseBuilder.append("ContentType:" + responseWrapper.getContentType()).append(LoggingFilterUtil.DELIMITER);
        responseBuilder.append("Time taken (ms):" + timeTaken).append(LoggingFilterUtil.DELIMITER);
        responseBuilder.append("Headers:" + responseWrapper.getHeaders()).append(LoggingFilterUtil.DELIMITER);
        responseBuilder.append("ResponseBody:" + responseWrapper.getBody());
        
        LOGGER.debug("Api Response: " + responseBuilder.toString());
    }
}
