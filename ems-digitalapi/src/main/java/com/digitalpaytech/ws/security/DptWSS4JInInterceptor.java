package com.digitalpaytech.ws.security;

import java.util.List;
import java.util.Map;

import org.apache.cxf.ws.security.wss4j.WSS4JInInterceptor;
import org.apache.ws.security.WSSConfig;
import org.apache.ws.security.WSSecurityEngine;
import org.apache.ws.security.WSSecurityEngineResult;

public class DptWSS4JInInterceptor extends WSS4JInInterceptor {
    public DptWSS4JInInterceptor() {
        super();
    }
    
    public DptWSS4JInInterceptor(Map<String, Object> properties) {
        super(properties);
        WSSecurityEngine engine = secEngine;
        WSSConfig wsconfig = engine.getWssConfig();
        wsconfig.setHandleCustomPasswordTypes(true);
        engine.setWssConfig(wsconfig);
    }
    
    @Override
    protected boolean checkReceiverResults(List<WSSecurityEngineResult> wsResult, List<Integer> actions) {
        // EMS-1103, EMS-1863, EMS-1865
        // Fixed element order issue with .NET client
        return true;
    }
}