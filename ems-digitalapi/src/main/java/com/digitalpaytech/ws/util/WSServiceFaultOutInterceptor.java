/* 
 * Copyright (c) 1997 - 2009  DPT  All rights reserved.
 *
 * Created on Mar 3, 2009 by rexz
 * 
 * $Header:  $
 * 
 * $Log:  $ 
 *
 */

package com.digitalpaytech.ws.util;

import org.apache.cxf.binding.soap.SoapFault;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.FaultOutInterceptor;
import org.apache.cxf.message.Message;

import org.springframework.transaction.CannotCreateTransactionException;
import org.hibernate.exception.GenericJDBCException;

import java.sql.SQLException;

/**
 * General Fault Out Interceptor
 */
public class WSServiceFaultOutInterceptor extends FaultOutInterceptor
{
    public final static String WS_ERROR_TYPE_FORMAT = "Format";
    public final static String WS_ERROR_TYPE_FORMAT_MESSAGE = "Format=> CustomerName:UserName";
    public final static String WS_GENERAL_FAULT_MESSAGE = "Please contact Digital Payment Technologies for technical support";
    public final static String WS_SPRING_FRAMEWORK_CAUSE = "org.springframework.transaction";
    private final static String WS_SPRING_FRAMEWORK_CANNOT_HIBERNATE = "could not open hibernate session";
    private final static String WS_HIBERNATE_CANNOT_OPEN_CONN = "cannot open connection";
    private final static String WS_SQL_EXCEPTION_CONN_TIMEOUT = "checkout a connection has timed out";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.cxf.jaxws.interceptors.WebFaultOutInterceptor#handleMessage(org.apache.cxf.message.Message)
	 */
	@Override
	public void handleMessage(Message arg0) throws Fault
	{
		Exception exception = arg0.getExchange().getOutFaultMessage().getContent(Exception.class);
		String msg = "";
		boolean isSpringFault = false;
		if(exception != null && exception.getCause() != null){
			msg = exception.getCause().toString();
			if(msg.indexOf(WS_SPRING_FRAMEWORK_CAUSE) == 0){
				isSpringFault = true;
			} else if (isCannotOpenConnectionException(exception, msg)) {
			    isSpringFault = true;
			}
		}
		if (exception instanceof SoapFault || isSpringFault)
		{
			arg0.getExchange().getOutFaultMessage().setContent(Exception.class,
					new Fault(new Throwable(WS_GENERAL_FAULT_MESSAGE)));
			
			super.handleMessage(arg0);			
		}
	}
	
	private boolean isCannotOpenConnectionException(Exception exp, String exceptionString) {
	    String msg = exceptionString.toLowerCase();
	    if (exp instanceof CannotCreateTransactionException
	            && msg.indexOf(WS_SPRING_FRAMEWORK_CANNOT_HIBERNATE) != -1) {
	        return true;
	    
	    } else if (exp instanceof GenericJDBCException
	            && msg.indexOf(WS_HIBERNATE_CANNOT_OPEN_CONN) != -1) {
	        return true;

	    } else if (exp instanceof SQLException 
	            && msg.indexOf(WS_SQL_EXCEPTION_CONN_TIMEOUT) != -1) {
	        return true;
	    
	    } else if (exp instanceof Fault
	            && hasCannotOpenConnectionString(msg)) {
	        return true;
	    }
	    return false;
	}
	
	private boolean hasCannotOpenConnectionString(String message) {
	    if (message.indexOf(WS_SPRING_FRAMEWORK_CANNOT_HIBERNATE) != -1
	            || message.indexOf(WS_HIBERNATE_CANNOT_OPEN_CONN) != -1
	            || message.indexOf(WS_SQL_EXCEPTION_CONN_TIMEOUT) != -1) {
	        return true;
	    }
	    return false;
	}
}
