package com.digitalpaytech.ws.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;

import com.digitalpaytech.util.WebCoreConstants;

public class DateTimeAdapter {
    private static Logger logger = Logger.getLogger(DateTimeAdapter.class);
    
    /**
     * Parse time with empty check
     * if empty, take Date(0)
     * 
     * @param s
     *            string
     * @return Date
     * 
     * @since v6.2.1
     */
    public static Date parseDateTime(String s) {
        if (s.length() == 0) {
            return WebCoreConstants.EMPTY_DATE;
        } else if (s.length() <= 10) {
            return null; // Indicate not correct time portion
        }
        return DatatypeConverter.parseDateTime(s).getTime();
    }
    
    public static String printDateTime(Date dt) {
        if (dt == null)
            return "";
        if (dt.compareTo(WebCoreConstants.EMPTY_DATE) == 0)
            return "";
        Calendar cal = new GregorianCalendar();
        cal.setTime(dt);
        return DatatypeConverter.printDateTime(cal);
    }
}
