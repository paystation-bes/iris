package com.digitalpaytech.ws.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.apache.cxf.frontend.FaultInfoException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.util.StandardConstants;

@Aspect
@Component
public class WSLoggingInterceptor {
    @Autowired
    private EmsPropertiesService emsPropertiesService;
    
    private static final Logger LOG = Logger.getLogger(WSLoggingInterceptor.class);
    
    private String STALLINFO_TARGET = "com.digitalpaytech.ws.stallinfo.impl.StallInfoServiceImpl";
    private String PAYSTATIONINFO_TARGET = "com.digitalpaytech.ws.paystationinfo.impl.PaystationInfoServiceImpl";
    private String TRANSACTIONDATA_TARGET = "com.digitalpaytech.ws.transactiondata.impl.TransactionDataServiceImpl";
    private String TRANSACTIONINFO_TARGET = "com.digitalpaytech.ws.transactioninfo.impl.TransactionInfoServiceImpl";
    private String PLATEINFO_TARGET = "com.digitalpaytech.ws.plateinfo.impl.PlateInfoServiceImpl";
    private String AUDITINFO_TARGET = "com.digitalpaytech.ws.auditinfo.impl.AuditInfoServiceImpl";
    
    private Map<String, Integer> wsTokenMap = new HashMap<>();
    
    private Calendar startTime;
    private int maxCount;
    public WSLoggingInterceptor() {
    }
    
    @Pointcut("execution(public * *(..))")
    private void anyPublicMethod() {
    }
    
    @Pointcut("execution(* get*(..))")
    private void anyGetMethod() {
    }
    
    @Pointcut("within(com.digitalpaytech.ws..*)")
    private void anyInWSPath() {
    }
    
    @Pointcut("bean(*InfoService)")
    private void anyInfoServiceClass() {
    }

    @Pointcut("bean(*DataService)")
    private void anyDataServiceClass() {
    }

    @Pointcut("anyPublicMethod() && anyGetMethod() && anyInWSPath() && (anyInfoServiceClass() || anyDataServiceClass())")
    private void anyWSCall() {
    }

    private void setMaxCount() {
        if (this.maxCount == StandardConstants.CONSTANT_0) {
            this.maxCount = this.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.DIGITAL_API_READ_MAX_COUNT,
                                                                            EmsPropertiesService.DIGITAL_API_READ_MAX_COUNT_DEFAULT);
        }
    }
    
    @Before("anyWSCall()")
    public void before(JoinPoint joinPoint) throws Throwable {
        setMaxCount();
        Object[] args = joinPoint.getArgs();
        Object target = joinPoint.getTarget();
        Signature method = joinPoint.getSignature();

        LOG.debug("WS AOP Beginning method: " + method.getName());
        if (needCheckToken(target)) {
            String token = getToken(args, "token");
            if ((token == null) || (token.length() == 0)) {
                throwFaultMessage(target, method, "Token not found");
            }
            if (!addToken(token)) {
                throwFaultMessage(target, method, "Token not available");
            }
            startTime = Calendar.getInstance();
            startTime.setTime(new Date());
        }
    }
    
    @AfterReturning("anyWSCall()")
    public void afterReturning(JoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        Object target = joinPoint.getTarget();
        Signature method = joinPoint.getSignature();
        
        LOG.debug("WS AOP Ending method: " + method.getName());
        if (needCheckToken(target)) {
            String token = getToken(args, "token");
            if (token == null) {
                throwFaultMessage(target, method, "Token not found");
            }
            Calendar endTime = Calendar.getInstance();
            endTime.setTime(new Date());
            removeToken(token);
            LOG.debug(method.getName() + " Time elapse :" + (endTime.getTimeInMillis() - startTime.getTimeInMillis()) + " ms");
        }
    }
    
    @AfterThrowing(pointcut="anyWSCall()", throwing="ex")
    public void afterThrowing(JoinPoint joinPoint, Throwable ex) throws Throwable {
        Object[] args = joinPoint.getArgs();
        Object target = joinPoint.getTarget();
        Signature method = joinPoint.getSignature();
        
        LOG.debug("WS AOP Exception in method: " + method.getName() + " Exception is: " + ex.getMessage());
        if (needCheckToken(target)) {
            String token = getToken(args, "token");
            if (token != null) {
                removeToken(token);
            }
        }
    }
    
    private boolean needCheckToken(Object target) {
        String targetName = target.getClass().getName();
        if ((targetName.equals(STALLINFO_TARGET)) || (targetName.equals(PAYSTATIONINFO_TARGET)) || (targetName.equals(TRANSACTIONINFO_TARGET))
            || targetName.equals(PLATEINFO_TARGET) || targetName.equals(AUDITINFO_TARGET) || targetName.equals(TRANSACTIONDATA_TARGET)) {
            return true;
        }
        return false;
    }
    
    private String getToken(Object[] args, String tokenName) {
        if ((args != null) && (args.length > 0)) {
            try {
                // get the property's value
                Object value = PropertyUtils.getProperty(args[0], tokenName);
                return (String) value;
            } catch (Exception ex) {
                LOG.info("Can not find token in " + args[0].getClass().getName());
                return null;
            }
        }
        return null;
    }
    
    private boolean addToken(String token) {
        synchronized (this.wsTokenMap) {
            if (!this.wsTokenMap.containsKey(token)) {
                printTokenAndCount(token, StandardConstants.CONSTANT_1, "addToken - return true for new token.");
                this.wsTokenMap.put(token, new Integer(StandardConstants.CONSTANT_1));
                return true;
            }
            
            int count = this.wsTokenMap.get(token);
            // Max is reached and reject after that with a generic exception.
            if (count == this.maxCount) {
                printTokenAndCount(token, count, "addToken - return false.");
                return false;
            
            } else if (count > this.maxCount) {
                // ERROR - count shouldn't be more than maxCount value.
                printTokenAndCount(token, count, "addToken - ERROR - count shouldn't be more than " + this.maxCount + ", remove token.");
                removeToken(token);
                return false;
            
            } else {
                // Increase count by 1.
                final int newCount = ++count;
                printTokenAndCount(token, newCount, "return true.");
                this.wsTokenMap.put(token, newCount);
                return true;
            }
        }
    }
    
    private boolean removeToken(String token) {
        synchronized (this.wsTokenMap) {
            if (!this.wsTokenMap.containsKey(token)) {
                return true;
            }
            
            int count = this.wsTokenMap.get(token);
            // Remove when count after decrement is reached 0 or ERROR - count shouldn't be more than maxCount value..
            if (count == StandardConstants.CONSTANT_1 || count > this.maxCount) {
                printTokenAndCount(token, count, "removeToken - remove from the map and return true.");
                this.wsTokenMap.remove(token);
                return true;
            
            } else {
                // Decrease count by 1.
                final int newCount = --count;
                printTokenAndCount(token, newCount, "return true.");
                this.wsTokenMap.put(token, newCount);
                return true;
            }
        }
    }
    
    private void throwFaultMessage(Object target, Signature method, String shortMessage) throws Throwable {
        String targetName = target.getClass().getName();
        String methodName = method.getName();
        if (targetName.equals(STALLINFO_TARGET)) {
            com.digitalpaytech.ws.stallinfo.InfoServiceFault fault = new com.digitalpaytech.ws.stallinfo.InfoServiceFault();
            fault.setShortErrorMessage(shortMessage);
            throw new com.digitalpaytech.ws.stallinfo.InfoServiceFaultMessage(methodName + " Failure", fault);
        } else if (targetName.equals(PAYSTATIONINFO_TARGET)) {
            com.digitalpaytech.ws.paystationinfo.InfoServiceFault fault = new com.digitalpaytech.ws.paystationinfo.InfoServiceFault();
            fault.setShortErrorMessage(shortMessage);
            throw new com.digitalpaytech.ws.paystationinfo.InfoServiceFaultMessage(methodName + " Failure", fault);
        } else if (targetName.equals(TRANSACTIONDATA_TARGET)) {
            com.digitalpaytech.ws.transactiondata.InfoServiceFault fault = new com.digitalpaytech.ws.transactiondata.InfoServiceFault();
            fault.setShortErrorMessage(shortMessage);
            throw new com.digitalpaytech.ws.transactiondata.InfoServiceFaultMessage(methodName + " Failure", fault);
        } else if (targetName.equals(TRANSACTIONINFO_TARGET)) {
            com.digitalpaytech.ws.transactioninfo.InfoServiceFault fault = new com.digitalpaytech.ws.transactioninfo.InfoServiceFault();
            fault.setShortErrorMessage(shortMessage);
            throw new com.digitalpaytech.ws.transactioninfo.InfoServiceFaultMessage(methodName + " Failure", fault);
        } else if (targetName.equals(PLATEINFO_TARGET)) {
            com.digitalpaytech.ws.plateinfo.InfoServiceFault fault = new com.digitalpaytech.ws.plateinfo.InfoServiceFault();
            fault.setShortErrorMessage(shortMessage);
            throw new com.digitalpaytech.ws.plateinfo.InfoServiceFaultMessage(methodName + " Failure", fault);
        } else if (targetName.equals(AUDITINFO_TARGET)) {
            com.digitalpaytech.ws.commoninfo.InfoServiceFault fault = new com.digitalpaytech.ws.commoninfo.InfoServiceFault();
            fault.setShortErrorMessage(shortMessage);
            throw new com.digitalpaytech.ws.commoninfo.InfoServiceFaultMessage(methodName + " Failure", fault);
        } else {
            throw new FaultInfoException(methodName + " Failure: " + shortMessage);
        }
    }
    
    private void printTokenAndCount(final String token, final int count, final String msg) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("========= DIGITAL API ==== " + token + ", count: " + count + ", " + msg);
        }
    }
}
