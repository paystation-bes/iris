package com.digitalpaytech.ws.util;

public final class TransactionInfoServiceUtil {
    
    public static final String VERSION_TAG = "v";
    
    private TransactionInfoServiceUtil() {
    }
    
    /**
     * Compares two version numbers, input value could start with 'v', e.g. v1.1.
     * 
     * @param version1
     * @param version2
     * @return boolean true if both the values are equal or first version (version1) object is less than the second (version2).
     *         Otherwise it returns false when first version (version1) is greater.
     */
    public static boolean isEqualOrGreaterTo(final String version1, final String version2) {
        
        final Float num1 = getFloat(version1);
        final Float num2 = getFloat(version2);
        
        /*
         * To compare a Float object with another Float object use int compareTo(Float f) method.
         * - It returns 0 if both the values are equal.
         * - It returns value less than 0 if first Float object (num2) is less than the second (num1).
         * - It returns value grater than 0 if first Float object (num2) is grater than second (num1).
         */
        final int result = num2.compareTo(num1);
        if (result >= 0) {
            return true;
        }
        return false;
    }
    
    public static Float getFloat(final String version) {
        Float num;
        if (version.startsWith(VERSION_TAG)) {
            num = new Float(version.substring(1, version.length()));
        } else {
            num = new Float(version);
        }
        return num;
    }
}
