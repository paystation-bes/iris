package com.digitalpaytech.ws.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.bind.DatatypeConverter;

public class DateAdapter
{
	public static Date parseDate(String s)
	{
		return DatatypeConverter.parseDateTime(s).getTime();
	}

	public static String printDate(Date dt)
	{
		if (dt == null)
			return "";
		if(dt.compareTo(new Date(0)) == 0)
			return "";
		Calendar cal = new GregorianCalendar();
		cal.setTime(dt);
		return DatatypeConverter.printDateTime(cal);
	}
}
