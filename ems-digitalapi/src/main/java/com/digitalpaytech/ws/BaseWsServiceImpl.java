package com.digitalpaytech.ws;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.CustomerSubscription;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.RolePermission;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.domain.ServiceAgreement;
import com.digitalpaytech.domain.UserAccount;
import com.digitalpaytech.domain.WebServiceEndPoint;
import com.digitalpaytech.service.ActivePermitService;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.CustomerService;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.LocationService;
import com.digitalpaytech.service.PaystationSettingService;
import com.digitalpaytech.service.PointOfSaleService;
import com.digitalpaytech.service.RestAccountService;
import com.digitalpaytech.service.RouteService;
import com.digitalpaytech.service.ServiceAgreementService;
import com.digitalpaytech.service.UserAccountService;
import com.digitalpaytech.service.WebServiceCallLogService;
import com.digitalpaytech.service.WebServiceEndPointService;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.util.WebSecurityUtil;
import com.digitalpaytech.ws.plateinfo.InfoServiceFault;
import com.digitalpaytech.ws.plateinfo.InfoServiceFaultMessage;

@Component("baseWsService")
@SuppressWarnings({ "PMD.GodClass", "PMD.ExcessiveImports" })
public class BaseWsServiceImpl {
    
    public static final String WS_VERSION_1_0 = "v1.0";
    public static final String WS_VERSION_1_1_LUKEII = "v1.1";
    public static final String WS_VERSION_1_2 = "v1.2";
    public static final String WS_VERSION_1_3 = "v1.3";
    public static final String WS_VERSION_1_4 = "v1.4";
    
    public static final String WS_CERBERUS_VERSION_1_0 = "v1.0.0";
    public static final String WS_CERBERUS_VERSION_1_1_1 = "v1.1.1";
    
    public static final String MESSAGE_UNSUPPORT_METHOD = "Unsupport method";
    public static final String MESSAGE_TOKEN_INVALID = "Token invalid";
    public static final String MESSAGE_INPUT_PARAMETERS_MISSING_OR_INVALID = "Input parameters missing or invalid";
    public static final String MESSAGE_AUTHENTICATION_FAILED = "Authentication Failed";
    public static final String MESSAGE_PARAMETER_MISSING_OR_INVALID = "{0} parameter missing or invalid";
    public static final String MESSAGE_DATE_PARAMETER_MISSING_OR_INVALID = "{0} parameter missing or invalid, correct format is yyyy-mm-ddTHH:MM:SS";
    public static final String MESSAGE_RANGE_INVALID = "{0} must be before or the same as the {1}";
    public static final String MESSAGE_ONEDAY_RANGE = "{0} and {1} must be in same day ";
    public static final String MESSAGE_INVALID_LENGTH = "{0} must be between {1} to {2} characters long.";
    public static final String MESSAGE_INVALID_CHARACTER = "Special characters not permitted in {0}. (Allowed characters: {1})";
    public static final String MESSAGE_VERSION_INVALID = "Version must be v1.1 for LUKEII event";
    public static final String MESSAGE_24HOURS_RANGE = "Difference between {0} and {1} must be at most 24 hours ";
    public static final String MESSAGE_GRACEPERIOD_NOT_POSITIVE = "Grace period value was {0}. Grace period must be a positive integer.";
    public static final String MESSAGE_GRACEPERIOD_OVER_999 = "Grace period value was {0}. Grace period must be between 0 and 999.";
    
    public static final String MESSAGE_LETTERS_DIGITS_SPACE_DOT_COMMA_DASH = "letters, digits, space, dot, comma, dash";
    public static final String MESSAGE_STALLNUMBERFROM_STALLNUMBERTO = "stallNumberFrom or stallNumberTo";
    public static final String MESSAGE_PURCHASEDDATEFROM_PURCHASEDDATETO = "purchasedDateFrom or purchasedDateTo";
    public static final String MESSAGE_SETTLEMENTDATEFROM_SETTLEMENTDATETO = "settlementDateFrom or settlementDateTo";
    public static final String MESSAGE_UPDATEDATEFROM_UPDATEDATETO = "updateDateFrom or updateDateTo";
    
    public static final int MAX_GRACE_PERIOD = 999;
    
    public static final String SPACE_FAILURE = " Failure";
    public static final String REGION_NAME = "regionName";
    public static final String GROUP_NAME = "groupName";
    public static final String COLLECTION_DATE_RANGE_START = "collectionDateRangeStart";
    public static final String COLLECTION_DATE_RANGE_END = "collectionDateRangeEnd";
    public static final String PURCHASED_DATE_FROM = "purchasedDateFrom";
    public static final String PURCHASED_DATE_TO = "purchasedDateTo";
    public static final String SETTLEMENT_DATE_FROM = "settlementDateFrom";
    public static final String SETTLEMENT_DATE_TO = "settlementDateTo";
    public static final String STALL_NUMBER_FROM = "StallNumberFrom";
    public static final String STALL_NUMBER_TO = "StallNumberTo";
    public static final String UPDATE_DATE_FROM = "updateDateFrom";
    public static final String UPDATE_DATE_TO = "updateDateTo";
    
    public static final String GET_GROUPS = "getGroups";
    public static final String GET_PAYSTATIONS = "getPaystations";
    public static final String GET_SETTINGS = "getSettings";
    public static final String GET_REGIONS = "getRegions";
    public static final String GET_PAYMENTTYPES = "getPaymentTypes";
    public static final String GET_TRANSACTIONTYPES = "getTransactionTypes";
    public static final String GET_PROCESSINGSTATUSTYPES = "getProcessingStatusTypes";
    public static final String GET_LOCATIONS = "getLocations";
    protected int webServiceEndPointType;
    
    @Autowired
    protected LocationService locationService;
    @Autowired
    protected EmsPropertiesService emsPropertiesService;
    @Autowired
    protected RouteService routeService;
    @Autowired
    protected WebServiceEndPointService webServiceEndPointService;
    @Autowired
    protected PointOfSaleService pointOfSaleService;
    @Autowired
    protected RestAccountService restAccountService;
    @Autowired
    protected PaystationSettingService paystationSettingService;
    @Autowired
    protected ActivePermitService activePermitService;
    @Autowired
    protected UserAccountService userAccountService;
    @Autowired
    protected CustomerService customerService;
    @Autowired
    protected CustomerAdminService customerAdminService;
    
    @Autowired
    private WebServiceCallLogService webServiceCallLogService;
    @Autowired
    private ServiceAgreementService serviceAgreementService;
    
    public final int getWebServiceEndPointType() {
        return this.webServiceEndPointType;
    }
    
    public final void setWebServiceEndPointType(final int webServiceEndPointType) {
        this.webServiceEndPointType = webServiceEndPointType;
    }
    
    public final LocationService getLocationService() {
        return this.locationService;
    }
    
    public final void setLocationService(final LocationService locationService) {
        this.locationService = locationService;
    }
    
    public final EmsPropertiesService getEmsPropertiesService() {
        return this.emsPropertiesService;
    }
    
    public final void setEmsPropertiesService(final EmsPropertiesService emsPropertiesService) {
        this.emsPropertiesService = emsPropertiesService;
    }
    
    public final RouteService getRouteService() {
        return this.routeService;
    }
    
    public final void setRouteService(final RouteService routeService) {
        this.routeService = routeService;
    }
    
    public final WebServiceEndPointService getWebServiceEndPointService() {
        return this.webServiceEndPointService;
    }
    
    public final void setWebServiceEndPointService(final WebServiceEndPointService webServiceEndPointService) {
        this.webServiceEndPointService = webServiceEndPointService;
    }
    
    public final PointOfSaleService getPointOfSaleService() {
        return this.pointOfSaleService;
    }
    
    public final void setPointOfSaleService(final PointOfSaleService pointOfSaleService) {
        this.pointOfSaleService = pointOfSaleService;
    }
    
    public final RestAccountService getRestAccountService() {
        return this.restAccountService;
    }
    
    public final void setRestAccountService(final RestAccountService restAccountService) {
        this.restAccountService = restAccountService;
    }
    
    public final PaystationSettingService getPaystationSettingService() {
        return this.paystationSettingService;
    }
    
    public final void setPaystationSettingService(final PaystationSettingService paystationSettingService) {
        this.paystationSettingService = paystationSettingService;
    }
    
    public final ActivePermitService getActivePermitService() {
        return this.activePermitService;
    }
    
    public final void setActivePermitService(final ActivePermitService activePermitService) {
        this.activePermitService = activePermitService;
    }
    
    public final UserAccountService getUserAccountService() {
        return this.userAccountService;
    }
    
    public final void setUserAccountService(final UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }
    
    public final CustomerService getCustomerService() {
        return this.customerService;
    }
    
    public final void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }
    
    public final CustomerAdminService getCustomerAdminService() {
        return this.customerAdminService;
    }
    
    public final void setCustomerAdminService(final CustomerAdminService customerAdminService) {
        this.customerAdminService = customerAdminService;
    }
    
    public final WebServiceCallLogService getWebServiceCallLogService() {
        return this.webServiceCallLogService;
    }
    
    public final void setWebServiceCallLogService(final WebServiceCallLogService webServiceCallLogService) {
        this.webServiceCallLogService = webServiceCallLogService;
    }
    
    public final ServiceAgreementService getServiceAgreementService() {
        return this.serviceAgreementService;
    }
    
    public final void setServiceAgreementService(final ServiceAgreementService serviceAgreementService) {
        this.serviceAgreementService = serviceAgreementService;
    }
    
    /**
     * check that if the token is valid or not
     * 
     * @param token
     * @param customerId
     * @return true if valid, false otherwise
     */
    protected final boolean isTokenValid(final String tokenString, final int customerId) {
        
        if (tokenString == null || tokenString.isEmpty()) {
            return false;
        }
        
        boolean isTokenValid = false;
        
        final CustomerProperty timeZoneProperty = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        
        final List<WebServiceEndPoint> tokenList = this.webServiceEndPointService.findWebServiceEndPointsByCustomer(customerId);
        
        if (tokenList != null && !tokenList.isEmpty()) {
            for (WebServiceEndPoint token : tokenList) {
                if (token.getToken().equals(tokenString) && this.webServiceEndPointType == token.getWebServiceEndPointType().getId()) {
                    isTokenValid = this.webServiceCallLogService.logWsCall(token, customerId, timeZoneProperty.getPropertyValue());
                    break;
                }
            }
        }
        return isTokenValid;
    }
    
    protected final boolean isTokenValidPrivate(final String tokenString, final int customerId) {
        
        if (tokenString == null || tokenString.isEmpty()) {
            return false;
        }
        
        boolean isTokenValid = false;
        
        final CustomerProperty timeZoneProperty = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_TIMEZONE);
        
        final List<WebServiceEndPoint> tokenList = this.webServiceEndPointService.findPrivateWebServiceEndPointsByCustomer(customerId);
        
        if (tokenList != null && !tokenList.isEmpty()) {
            for (WebServiceEndPoint token : tokenList) {
                if (token.getToken().equals(tokenString) && this.webServiceEndPointType == token.getWebServiceEndPointType().getId()) {
                    isTokenValid = this.webServiceCallLogService.logWsCall(token, customerId, timeZoneProperty.getPropertyValue());
                    break;
                }
            }
        }
        return isTokenValid;
    }
    
    protected final int findLocationIdOfRequestLocation(final int customerId, final String requestLocation) {
        final String[] csvLocations = requestLocation.split("-");
        final Location location = this.locationService.findLocationByCustomerIdAndName(customerId, csvLocations[csvLocations.length - 1]);
        
        if (location == null) {
            return -1;
        }
        
        if (checkLocations(csvLocations, location)) {
            return location.getId();
        }
        return -1;
    }
    
    protected final boolean checkLocations(final String[] csvLocations, final Location loc) {
        Location location = loc;
        for (int i = csvLocations.length - 1; i >= 0; i--) {
            // If we are at the root region too early
            // or csv region name does not match region tree
            if (location == null || !csvLocations[i].equals(location.getName())) {
                return false;
            }
            if (location.getLocation() != null) {
                location = this.locationService.findLocationById(location.getLocation().getId());
            }
        }
        return true;
    }
    
    protected final boolean checkServiceAggrementAssigned(final int customerId) {
        final Customer customer = this.customerAdminService.findCustomerByCustomerId(customerId);
        int usableCustomerId = 0;
        if (customer.getParentCustomer() == null) {
            usableCustomerId = customer.getId();
        } else {
            usableCustomerId = customer.getParentCustomer().getId();
        }
        final ServiceAgreement agree = this.serviceAgreementService.findLatestServiceAgreementByCustomerId(usableCustomerId);
        
        return agree != null;
    }
    
    protected final List<Location> getLocationsWithPaystations(final int customerId) {
        return this.locationService.findLocationsWithPointOfSalesByCustomerId(customerId);
    }
    
    protected final List<PointOfSale> getPosListByRouteName(final int customerId, final String routeName) {
        final Collection<Route> routeCollection = this.routeService.findRoutesByCustomerIdAndName(customerId, routeName);
        final List<PointOfSale> posList = new ArrayList<PointOfSale>();
        
        // only query database when there is at least one record
        if (routeCollection != null && !routeCollection.isEmpty()) {
            for (Route route : routeCollection) {
                final List<PointOfSale> routePosList = this.pointOfSaleService.findPointOfSalesByRouteId(route.getId());
                posList.addAll(routePosList);
            }
        }
        return posList;
    }
    
    /**
     * Authentication that validates permission and a specific subscription.
     * 
     * @param subscriptionType
     *            e.g. "ID: 1900, Name: Digital API: XChange" or "ID: 700, Name: Passcards".
     * @return org.springframework.security.core.Authentication Spring security framework which encapsulates Iris WebUser object.
     *         Returns null if permission or subscription doesn't match.
     */
    protected final Authentication authorize(final int subscriptionType, final String token) {
        return authorizeWebUser(subscriptionType, token);
    }
    
    /**
     * Default authentication which validates permission and SUBSCRIPTION_TYPE_DIGITAL_API_READ subscription.
     * 
     * @return org.springframework.security.core.Authentication Spring security framework which encapsulates Iris WebUser object.
     *         Returns null if permission or subscription doesn't match.
     */
    protected final Authentication authorize(final String token) {
        return authorizeWebUser(WebCoreConstants.SUBSCRIPTION_TYPE_DIGITAL_API_READ, token);
    }
    
    private void findAliasUser(final WebUser webUser, final String token) {
        final WebServiceEndPoint wsep = this.webServiceEndPointService.findWebServiceEndPointByToken(token);
        if (wsep != null && wsep.getWebServiceEndPointType().getId() == (byte) this.webServiceEndPointType) {
            final List<UserAccount> aliasAccounts = this.userAccountService.findChildUserAccountsByParentUserId(webUser.getUserAccountId());
            if (aliasAccounts != null && !aliasAccounts.isEmpty()) {
                for (UserAccount childAccount : aliasAccounts) {
                    if (childAccount.getCustomer().getId().intValue() == wsep.getCustomer().getId().intValue()) {
                        webUser.setSwitchToAliasUserAccount(childAccount.getId());
                        WebSecurityUtil.changeUserOnLogin(webUser);
                    }
                }
            }
        }
    }
    
    private void addPermissionsAnsSubscriptions(final WebUser webUser) {
        final Customer customer = this.customerService.findCustomer(webUser.getCustomerId());
        
        final Set<Integer> permissionIds = new HashSet<Integer>();
        final List<RolePermission> rolePermissions = this.userAccountService.findUserRoleAndPermission(webUser.getUserAccountId());
        for (RolePermission rp : rolePermissions) {
            if (!webUser.isSystemAdmin() && rp.getRole().getCustomerType().getId() == WebSecurityConstants.CUSTOMER_TYPE_ID_SYSTEM_ADMIN) {
                webUser.setIsSystemAdmin(true);
            }
            
            if (rp.getRole().getCustomerType().getId() == customer.getCustomerType().getId()) {
                permissionIds.add(rp.getPermission().getId());
                if (rp.getPermission().getPermission() != null) {
                    permissionIds.add(rp.getPermission().getPermission().getId());
                }
            }
        }
        
        webUser.setUserPermissions(permissionIds);
        
        final Set<CustomerSubscription> subscriptions = customer.getCustomerSubscriptions();
        final Collection<Integer> customerSubscriptions = new ArrayList<Integer>();
        for (CustomerSubscription subscription : subscriptions) {
            if (subscription.isIsEnabled()) {
                customerSubscriptions.add(subscription.getSubscriptionType().getId());
            }
        }
        webUser.setCustomerSubscriptions(customerSubscriptions);
        
    }
    
    private Authentication authorizeWebUser(final int subscriptionType, final String token) {
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            if (StringUtils.isBlank(auth.getName())) {
                return null;
            } else {
                final WebUser webUser = (WebUser) auth.getPrincipal();
                
                final Customer originalCustomer = this.customerService.findCustomer(webUser.getCustomerId());
                
                if (originalCustomer.isIsParent()) {
                    findAliasUser(webUser, token);
                }
                
                addPermissionsAnsSubscriptions(webUser);
                
                if (!webUser.hasAccess(WebSecurityConstants.PERMISSION_ACCESS_DIGITAL_API) || !webUser.hasSubscription(subscriptionType)) {
                    return null;
                }
            }
        }
        
        return auth;
    }
}
