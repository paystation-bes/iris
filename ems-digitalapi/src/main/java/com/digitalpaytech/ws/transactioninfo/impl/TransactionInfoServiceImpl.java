package com.digitalpaytech.ws.transactioninfo.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.data.Track2Card;
import com.digitalpaytech.domain.ActivePermit;
import com.digitalpaytech.domain.Customer;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PaystationSetting;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.ProcessorTransactionType;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.dto.paystation.TransactionInfoDto;
import com.digitalpaytech.service.EmsPropertiesService;
import com.digitalpaytech.service.PaymentTypeService;
import com.digitalpaytech.service.ProcessorTransactionTypeService;
import com.digitalpaytech.service.TransactionTypeService;
import com.digitalpaytech.service.paystation.TransactionInfoDtoService;
import com.digitalpaytech.util.CardProcessingConstants;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.DigitalAPIUtil;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.util.WebCoreUtil;
import com.digitalpaytech.util.WebSecurityConstants;
import com.digitalpaytech.ws.BaseWsServiceImpl;
import com.digitalpaytech.ws.transactioninfo.GroupRequest;
import com.digitalpaytech.ws.transactioninfo.GroupResponse;
import com.digitalpaytech.ws.transactioninfo.GroupType;
import com.digitalpaytech.ws.transactioninfo.InfoServiceFault;
import com.digitalpaytech.ws.transactioninfo.InfoServiceFaultMessage;
import com.digitalpaytech.ws.transactioninfo.PaymentType;
import com.digitalpaytech.ws.transactioninfo.PaymentTypeRequest;
import com.digitalpaytech.ws.transactioninfo.PaymentTypeResponse;
import com.digitalpaytech.ws.transactioninfo.PaystationRequest;
import com.digitalpaytech.ws.transactioninfo.PaystationResponse;
import com.digitalpaytech.ws.transactioninfo.PaystationType;
import com.digitalpaytech.ws.transactioninfo.ProcessingStatusType;
import com.digitalpaytech.ws.transactioninfo.ProcessingStatusTypeRequest;
import com.digitalpaytech.ws.transactioninfo.ProcessingStatusTypeResponse;
import com.digitalpaytech.ws.transactioninfo.RegionRequest;
import com.digitalpaytech.ws.transactioninfo.RegionResponse;
import com.digitalpaytech.ws.transactioninfo.RegionType;
import com.digitalpaytech.ws.transactioninfo.SettingRequest;
import com.digitalpaytech.ws.transactioninfo.SettingResponse;
import com.digitalpaytech.ws.transactioninfo.SettingType;
import com.digitalpaytech.ws.transactioninfo.TransactionByGroupRequest;
import com.digitalpaytech.ws.transactioninfo.TransactionByPurchasedDateRequest;
import com.digitalpaytech.ws.transactioninfo.TransactionByRegionRequest;
import com.digitalpaytech.ws.transactioninfo.TransactionBySerialRequest;
import com.digitalpaytech.ws.transactioninfo.TransactionBySettingRequest;
import com.digitalpaytech.ws.transactioninfo.TransactionBySettlementDateRequest;
import com.digitalpaytech.ws.transactioninfo.TransactionByStallRequest;
import com.digitalpaytech.ws.transactioninfo.TransactionInfoService;
import com.digitalpaytech.ws.transactioninfo.TransactionInfoType;
import com.digitalpaytech.ws.transactioninfo.TransactionResponse;
import com.digitalpaytech.ws.transactioninfo.TransactionType;
import com.digitalpaytech.ws.transactioninfo.TransactionTypeRequest;
import com.digitalpaytech.ws.transactioninfo.TransactionTypeResponse;
import com.digitalpaytech.ws.transactioninfo.VersionType;
import com.digitalpaytech.ws.util.TransactionInfoServiceUtil;

@Component("transactionInfoService")
@WebService(serviceName = "TransactionInfoService", targetNamespace = "http://ws.digitalpaytech.com/transactionInfo", endpointInterface = "com.digitalpaytech.ws.transactioninfo.TransactionInfoService")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
@SuppressWarnings({ "PMD.AvoidUsingShortType", "PMD.GodClass", "PMD.ExcessiveImports", "PMD.AvoidInstantiatingObjectsInLoops" })
public class TransactionInfoServiceImpl extends BaseWsServiceImpl implements TransactionInfoService {
    
    public static final int TRANSACTION_INFO_MAX_RECORDS_DEFAULT = 30000;
    
    public static final byte PAYMENT_TYPE_CREDIT_CARD_SWIPE = 2;
    public static final byte PAYMENT_TYPE_CASH_CREDIT_SWIPE = 5;
    
    public static final byte PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS = 11;
    public static final byte PAYMENT_TYPE_CASH_CREDIT_CL = 12;
    public static final byte PAYMENT_TYPE_CREDIT_CARD_CHIP = 13;
    public static final byte PAYMENT_TYPE_CASH_CREDIT_CH = 14;
    public static final byte PAYMENT_TYPE_CREDIT_CARD_EXTERNAL = 15;
    
    private static final Logger LOGGER = Logger.getLogger(TransactionInfoServiceImpl.class);
    
    private static final String STRING_GETTRANSACTIONBYSTALL = "getTransactionByStall";
    private static final String STRING_GETTRANSACTIONBYPURCHASEDDATE = "getTransactionByPurchasedDate";
    private static final String STRING_GETTRANSACTIONBYSERIALNUMBER = "getTransactionBySerialNumber";
    private static final String STRING_GETTRANSACTIONBYSETTLEMENTDATE = "getTransactionBySettlementDate";
    private static final String STRING_GETTRANSACTIONBYREGION = "getTransactionByRegion";
    private static final String STRING_GETTRANSACTIONBYSETTING = "getTransactionBySetting";
    private static final String STRING_GETTRANSACTIONBYGROUP = "getTransactionByGroup";
    
    @Autowired
    protected TransactionInfoDtoService transactionInfoDtoService;
    
    @Autowired
    protected TransactionTypeService transactionTypeService;
    
    @Autowired
    protected PaymentTypeService paymentTypeService;
    
    @Autowired
    protected ProcessorTransactionTypeService processorTransactionTypeService;
    
    // Legacy PaymentTypes
    private PaymentTypeResponse legacyPaymentTypeResponse = new PaymentTypeResponse();
    
    public TransactionInfoServiceImpl() {
        super();
        webServiceEndPointType = WebCoreConstants.END_POINT_ID_TRANSCTION_INFO;
        
        final PaymentTypeResponse paymentTypeResponse = new PaymentTypeResponse();
        final List<com.digitalpaytech.domain.PaymentType> paymentTypeList = getLegacyPaymentTypes();
        
        for (com.digitalpaytech.domain.PaymentType pt : paymentTypeList) {
            final PaymentType paymentType = new PaymentType();
            paymentType.setId(pt.getId());
            paymentType.setName(pt.getName());
            paymentType.setDescription(pt.getDescription());
            paymentTypeResponse.getPaymentType().add(paymentType);
        }
        this.legacyPaymentTypeResponse = paymentTypeResponse;
        
    }
    
    public final TransactionInfoDtoService getTransactionInfoDtoService() {
        return this.transactionInfoDtoService;
    }
    
    public final void setTransactionInfoDtoService(final TransactionInfoDtoService transactionInfoDtoService) {
        this.transactionInfoDtoService = transactionInfoDtoService;
    }
    
    public final TransactionTypeService getTransactionTypeService() {
        return this.transactionTypeService;
    }
    
    public final void setTransactionTypeService(final TransactionTypeService transactionTypeService) {
        this.transactionTypeService = transactionTypeService;
    }
    
    public final PaymentTypeService getPaymentTypeService() {
        return this.paymentTypeService;
    }
    
    public final void setPaymentTypeService(final PaymentTypeService paymentTypeService) {
        this.paymentTypeService = paymentTypeService;
    }
    
    public final ProcessorTransactionTypeService getProcessorTransactionTypeService() {
        return this.processorTransactionTypeService;
    }
    
    public final void setProcessorTransactionTypeService(final ProcessorTransactionTypeService processorTransactionTypeService) {
        this.processorTransactionTypeService = processorTransactionTypeService;
    }
    
    public final PaymentTypeResponse getLegacyPaymentTypeResponse() {
        return this.legacyPaymentTypeResponse;
    }
    
    public final void setLegacyPaymentTypeResponse(final PaymentTypeResponse legacyPaymentTypeResponse) {
        this.legacyPaymentTypeResponse = legacyPaymentTypeResponse;
    }
    
    protected final InfoServiceFault createInfoServiceFault(final String message) {
        final InfoServiceFault fault = new InfoServiceFault();
        fault.setShortErrorMessage(message);
        return fault;
    }
    
    protected final InfoServiceFault createInfoServiceFault(final String message, final Object... args) {
        final InfoServiceFault fault = new InfoServiceFault();
        fault.setShortErrorMessage(MessageFormat.format(message, args));
        return fault;
    }
    
    private List<com.digitalpaytech.domain.PaymentType> getLegacyPaymentTypes() {
        
        final List<com.digitalpaytech.domain.PaymentType> paymentTypeList = new ArrayList<com.digitalpaytech.domain.PaymentType>();
        paymentTypeList.add(new com.digitalpaytech.domain.PaymentType((byte) WebCoreConstants.PAYMENT_TYPE_CASH, "Cash", "Cash Only", DateUtil
                .getCurrentGmtDate(), WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID));
        paymentTypeList.add(new com.digitalpaytech.domain.PaymentType((byte) WebCoreConstants.PAYMENT_TYPE_CREDIT_CARD_SWIPE, "Credit Card",
                "Credit Card Only", DateUtil.getCurrentGmtDate(), WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID));
        paymentTypeList.add(new com.digitalpaytech.domain.PaymentType((byte) WebCoreConstants.PAYMENT_TYPE_SMART_CARD, "Smart Card",
                "Smart Card Only", DateUtil.getCurrentGmtDate(), WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID));
        paymentTypeList.add(new com.digitalpaytech.domain.PaymentType((byte) WebCoreConstants.PAYMENT_TYPE_CASH_CREDIT_SWIPE, "Cash/CC",
                "Cash and Credit Card", DateUtil.getCurrentGmtDate(), WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID));
        paymentTypeList.add(new com.digitalpaytech.domain.PaymentType((byte) WebCoreConstants.PAYMENT_TYPE_CASH_SMART, "Cash/SC",
                "Cash and Smart Card", DateUtil.getCurrentGmtDate(), WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID));
        paymentTypeList.add(new com.digitalpaytech.domain.PaymentType((byte) WebCoreConstants.PAYMENT_TYPE_VALUE_CARD, "Passcard", "Passcard Only",
                DateUtil.getCurrentGmtDate(), WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID));
        paymentTypeList.add(new com.digitalpaytech.domain.PaymentType((byte) WebCoreConstants.PAYMENT_TYPE_CASH_VALUE, "Cash/Passcard",
                "Cash and Passcard", DateUtil.getCurrentGmtDate(), WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID));
        paymentTypeList.add(new com.digitalpaytech.domain.PaymentType((byte) WebCoreConstants.PAYMENT_TYPE_UNKNOWN, "Unknown",
                "Unknown Payment Type", DateUtil.getCurrentGmtDate(), WebSecurityConstants.DPT_ADMIN_USER_ACCOUNT_ID));
        return paymentTypeList;
    }
    
    private Authentication authenticationCheck(final String messageName, final String token) throws InfoServiceFaultMessage {
        final Authentication auth = authorize(token);
        if (auth == null) {
            throw new InfoServiceFaultMessage(messageName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        return auth;
    }
    
    public final RegionResponse getRegions(final RegionRequest regionRequest) throws InfoServiceFaultMessage {
        final Authentication auth = authenticationCheck(GET_REGIONS, regionRequest.getToken());
        if (auth == null) {
            throw new InfoServiceFaultMessage(GET_REGIONS + SPACE_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        
        // verify input parameters
        if (regionRequest.getToken() == null) {
            throw new InfoServiceFaultMessage(GET_REGIONS + SPACE_FAILURE, createInfoServiceFault(MESSAGE_INPUT_PARAMETERS_MISSING_OR_INVALID));
        }
        
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_REGIONS + SPACE_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        // check the token
        if (!isTokenValid(regionRequest.getToken(), webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_REGIONS + SPACE_FAILURE, createInfoServiceFault(MESSAGE_TOKEN_INVALID));
        }
        
        final int customerId = webUser.getCustomerId();
        return getLocations(customerId);
    }
    
    public final PaystationResponse getPaystations(final PaystationRequest paystationRequest) throws InfoServiceFaultMessage {
        final Authentication auth = authenticationCheck(GET_PAYSTATIONS, paystationRequest.getToken());
        if (auth == null) {
            throw new InfoServiceFaultMessage(GET_PAYSTATIONS + SPACE_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        
        // verify input parameters
        if (paystationRequest.getToken() == null) {
            throw new InfoServiceFaultMessage(GET_PAYSTATIONS + SPACE_FAILURE, createInfoServiceFault(MESSAGE_INPUT_PARAMETERS_MISSING_OR_INVALID));
        }
        
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYSTATIONS + SPACE_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        // check the token
        if (!isTokenValid(paystationRequest.getToken(), webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYSTATIONS + SPACE_FAILURE, createInfoServiceFault(MESSAGE_TOKEN_INVALID));
        }
        
        final int customerId = webUser.getCustomerId();
        return getPaystations(customerId);
    }
    
    public final SettingResponse getSettings(final SettingRequest settingRequest) throws InfoServiceFaultMessage {
        final Authentication auth = authenticationCheck("getSettings", settingRequest.getToken());
        if (auth == null) {
            throw new InfoServiceFaultMessage(GET_SETTINGS + SPACE_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        
        // verify input parameters
        if (settingRequest.getToken() == null) {
            throw new InfoServiceFaultMessage(GET_SETTINGS + SPACE_FAILURE, createInfoServiceFault(MESSAGE_INPUT_PARAMETERS_MISSING_OR_INVALID));
        }
        
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_SETTINGS + SPACE_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        // check the token
        if (!isTokenValid(settingRequest.getToken(), webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_SETTINGS + SPACE_FAILURE, createInfoServiceFault(MESSAGE_TOKEN_INVALID));
        }
        
        final int customerId = webUser.getCustomerId();
        return getSettings(customerId);
    }
    
    /**
     * Retrieve groups based on user token
     * 
     * @param groupRequest
     *            GroupRequest
     * @return groupResponse GroupResponse
     * @throws InfoServiceFaultMessage
     * 
     * @since v6.2.1
     */
    public final GroupResponse getGroups(final GroupRequest groupRequest) throws InfoServiceFaultMessage {
        final Authentication auth = authenticationCheck(GET_GROUPS, groupRequest.getToken());
        if (auth == null) {
            throw new InfoServiceFaultMessage(GET_GROUPS + SPACE_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        
        // verify input parameters
        if (groupRequest.getToken() == null) {
            throw new InfoServiceFaultMessage(GET_GROUPS + SPACE_FAILURE, createInfoServiceFault(MESSAGE_INPUT_PARAMETERS_MISSING_OR_INVALID));
        }
        
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_GROUPS + SPACE_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        // check the token
        if (!isTokenValid(groupRequest.getToken(), webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_GROUPS + SPACE_FAILURE, createInfoServiceFault(MESSAGE_TOKEN_INVALID));
        }
        
        final int customerId = webUser.getCustomerId();
        return getRoutes(customerId);
    }
    
    protected final GroupResponse getRoutes(final int customerId) {
        final Collection<Route> routeList = this.routeService.findRoutesByCustomerId(customerId);
        final GroupResponse response = new GroupResponse();
        GroupType gType;
        for (Route route : routeList) {
            gType = new GroupType();
            gType.setGroupName(route.getName());
            response.getGroupName().add(gType);
        }
        return response;
    }
    
    protected final RegionResponse getLocations(final int customerId) {
        final List<Location> locationList = getLocationsWithPaystations(customerId);
        final RegionResponse response = new RegionResponse();
        for (Location location : locationList) {
            final RegionType regionType = new RegionType();
            regionType.setRegionName(location.getName());
            regionType.setDescription(location.getDescription());
            response.getRegions().add(regionType);
        }
        return response;
    }
    
    protected final SettingResponse getSettings(final int customerId) {
        
        final List<ActivePermit> activePermitList = super.activePermitService.getPaystationSettingsFromLast14Days(customerId);
        
        final SettingResponse response = new SettingResponse();
        
        for (ActivePermit activePermit : activePermitList) {
            final SettingType settingType = new SettingType();
            
            settingType.setSettingName(activePermit.getPaystationSettingName());
            response.getSettings().add(settingType);
        }
        
        return response;
    }
    
    protected final PaystationResponse getPaystations(final int customerId) {
        final List<PointOfSale> posList = super.pointOfSaleService.findPointOfSalesByCustomerId(customerId, true);
        final PaystationResponse response = new PaystationResponse();
        for (PointOfSale pos : posList) {
            final PaystationType paystationInfoType = convertToInfoType(pos);
            response.getPaystations().add(paystationInfoType);
        }
        return response;
    }
    
    private PaystationType convertToInfoType(final PointOfSale pos) {
        
        final PaystationType paystationInfoType = new PaystationType();
        paystationInfoType.setSerialNumber(pos.getSerialNumber());
        paystationInfoType.setPaystationName(pos.getName());
        paystationInfoType.setRegionName(pos.getLocation().getName());
        paystationInfoType.setRegionId(pos.getLocation().getId());
        return paystationInfoType;
    }
    
    private void checkToken(final String token, final int customerId, final String methodName) throws InfoServiceFaultMessage {
        if (token == null || !DigitalAPIUtil.validateString(token)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_PARAMETER_MISSING_OR_INVALID,
                                                                                                 new Object[] { "token" }));
        }
        
        if (!isTokenValid(token, customerId)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_TOKEN_INVALID));
        }
    }
    
    private void checkPurchasedDate(final Date fromDate, final Date toDate, final String methodName) throws InfoServiceFaultMessage {
        // modified in case of empty date
        if (fromDate == null || toDate == null) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE,
                    createInfoServiceFault(MESSAGE_DATE_PARAMETER_MISSING_OR_INVALID, new Object[] { MESSAGE_PURCHASEDDATEFROM_PURCHASEDDATETO }));
        } else if (fromDate.equals(WebCoreConstants.EMPTY_DATE) || toDate.equals(WebCoreConstants.EMPTY_DATE)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE,
                    createInfoServiceFault(MESSAGE_DATE_PARAMETER_MISSING_OR_INVALID, new Object[] { MESSAGE_PURCHASEDDATEFROM_PURCHASEDDATETO }));
        }
        if (toDate.before(fromDate)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_RANGE_INVALID, new Object[] {
                PURCHASED_DATE_FROM, PURCHASED_DATE_TO, }));
        }
        final String fromDay = DateUtil.createDateOnlyString(fromDate);
        final String toDay = DateUtil.createDateOnlyString(toDate);
        if (!fromDay.equals(toDay)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_ONEDAY_RANGE, new Object[] {
                PURCHASED_DATE_FROM, PURCHASED_DATE_TO, }));
        }
    }
    
    // modified in case of empty date
    private void checkSettlementDate(final Date fromDate, final Date toDate, final String methodName) throws InfoServiceFaultMessage {
        if (fromDate == null || toDate == null) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE,
                    createInfoServiceFault(MESSAGE_DATE_PARAMETER_MISSING_OR_INVALID, new Object[] { MESSAGE_SETTLEMENTDATEFROM_SETTLEMENTDATETO }));
        } else if (fromDate.equals(WebCoreConstants.EMPTY_DATE) || toDate.equals(WebCoreConstants.EMPTY_DATE)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE,
                    createInfoServiceFault(MESSAGE_DATE_PARAMETER_MISSING_OR_INVALID, new Object[] { MESSAGE_SETTLEMENTDATEFROM_SETTLEMENTDATETO }));
        }
        if (toDate.before(fromDate)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_RANGE_INVALID, new Object[] {
                SETTLEMENT_DATE_FROM, SETTLEMENT_DATE_TO, }));
        }
        final String fromDay = DateUtil.createDateOnlyString(fromDate);
        final String toDay = DateUtil.createDateOnlyString(toDate);
        if (!fromDay.equals(toDay)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_ONEDAY_RANGE, new Object[] {
                SETTLEMENT_DATE_FROM, SETTLEMENT_DATE_TO, }));
        }
    }
    
    @SuppressWarnings("PMD.InefficientEmptyStringCheck")
    private void checkStallRange(final String fromStall, final String toStall, final String methodName) throws InfoServiceFaultMessage {
        if (fromStall == null || toStall == null) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE,
                    createInfoServiceFault(MESSAGE_PARAMETER_MISSING_OR_INVALID, new Object[] { MESSAGE_STALLNUMBERFROM_STALLNUMBERTO }));
        }
        String temp;
        
        if (fromStall.trim().length() == 0 || toStall.trim().length() == 0) {
            temp = fromStall.trim() + toStall.trim();
            if (temp.length() != 0) {
                throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE,
                        createInfoServiceFault(MESSAGE_PARAMETER_MISSING_OR_INVALID, new Object[] { MESSAGE_STALLNUMBERFROM_STALLNUMBERTO }));
            }
        } else {
            final int stallNumberFrom = Integer.parseInt(fromStall.trim());
            final int stallNumberTo = Integer.parseInt(toStall.trim());
            if (stallNumberFrom > stallNumberTo) {
                throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_RANGE_INVALID, new Object[] {
                    STALL_NUMBER_FROM, STALL_NUMBER_TO, }));
            }
        }
    }
    
    private void checkSerialNumber(final String serialNumber, final String methodName) throws InfoServiceFaultMessage {
        if (serialNumber == null || !DigitalAPIUtil.validateString(serialNumber)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_PARAMETER_MISSING_OR_INVALID,
                                                                                                 new Object[] { "serialNumber" }));
        }
    }
    
    private byte findPaymentType(final byte realPaymentTypeId, final String version) {
        if (TransactionInfoServiceUtil.isEqualOrGreaterTo(WS_VERSION_1_4, version)) {
            return realPaymentTypeId;
        } else {
            switch (realPaymentTypeId) {
                case PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS:
                case PAYMENT_TYPE_CREDIT_CARD_CHIP:
                case PAYMENT_TYPE_CREDIT_CARD_EXTERNAL:
                    return PAYMENT_TYPE_CREDIT_CARD_SWIPE;
                case PAYMENT_TYPE_CASH_CREDIT_CL:
                case PAYMENT_TYPE_CASH_CREDIT_CH:
                    return PAYMENT_TYPE_CASH_CREDIT_SWIPE;
                default:
                    return realPaymentTypeId;
            }
        }
    }
    
    @SuppressWarnings({ "checkstyle:methodlength", "checkstyle:cyclomaticcomplexity", "checkstyle:npathcomplexity",
        "PMD.AvoidInstantiatingObjectsInLoops", "PMD.NcssMethodCount" })
    private void convertResultToTransactionInfoType(final List<TransactionInfoDto> transactionList, final List<TransactionInfoType> transList,
        final String groupName, final String version) {
        
        for (TransactionInfoDto transaction : transactionList) {
            TransactionInfoType transItem = new TransactionInfoType();
            
            transItem = new TransactionInfoType();
            transItem.setSerialNumber(transaction.getSerialNumber());
            transItem.setPaystationName(transaction.getPaystationName());
            transItem.setSettingName(transaction.getSettingName());
            transItem.setRegionName(transaction.getLocationName());
            transItem.setTicketNumber(transaction.getTicketNumber());
            final Integer stallNumber = transaction.getSpaceNumber();
            if (stallNumber == null || stallNumber == 0) {
                transItem.setStallNumber("");
            } else {
                transItem.setStallNumber(String.valueOf(stallNumber));
            }
            transItem.setPurchasedDate(transaction.getPurchaseGMT());
            transItem.setExpirationDate(transaction.getPermitExpireGMT());
            transItem.setPaymentType(findPaymentType(transaction.getPaymentType(), version));
            transItem.setTransactionType(transaction.getTransactionType());
            if (TransactionInfoServiceUtil.isEqualOrGreaterTo(WS_VERSION_1_1_LUKEII, version)) {
                if (transaction.getCardType() != null && !(transaction.getCardType()).isEmpty()) {
                    transItem.setCardType(transaction.getCardType());
                }
                
                if (transaction.getLast4DigitsOfCardNumber() != null && !(transaction.getLast4DigitsOfCardNumber().toString()).isEmpty()) {
                    final int digs = transaction.getLast4DigitsOfCardNumber();
                    transItem.setCardNumber(WebCoreUtil.prefixZeroes((short) digs, WebCoreConstants.MAX_NUMBERS_LAST_DIGITS));
                }
                
                if (transaction.getAuthorizationNumber() != null && !transaction.getAuthorizationNumber().isEmpty()) {
                    transItem.setCardProcessingAuthorizationNumber(transaction.getAuthorizationNumber());
                }
                
                if (transaction.getProcessingDate() != null && !(transaction.getProcessingDate().toString()).isEmpty()) {
                    transItem.setCardProcessingDate(transaction.getProcessingDate());
                }
                
                if (transaction.getProcessorTransactionId() != null && !(transaction.getProcessorTransactionType().toString()).isEmpty()) {
                    transItem.setCardProcessingStatus(transaction.getProcessorTransactionType().toString());
                }
                
                if (transaction.getReferenceNumber() != null && !(transaction.getReferenceNumber()).isEmpty()) {
                    transItem.setCardProcessingReferenceNumber(transaction.getReferenceNumber());
                }
                
                if (transaction.getMerchantAccountName() != null && !(transaction.getMerchantAccountName()).isEmpty()) {
                    transItem.setCardProcessingMerchantAccount(transaction.getMerchantAccountName());
                }
                
                if (transaction.getNumRetries() != null && !(transaction.getNumRetries().toString()).isEmpty()) {
                    transItem.setCardProcessingRetries(transaction.getNumRetries());
                }
                
                if (transaction.getProcessorTransactionId() != null && !(transaction.getProcessorTransactionId()).isEmpty()) {
                    transItem.setCardProcessingTransactionId(transaction.getProcessorTransactionId());
                }
            } else {
                transItem.setCardType(transaction.getCardType() == null ? CardProcessingConstants.NAME_CREDIT_CARD : transaction.getCardType().toString());
                String cardNum = null;
                if (transaction.getAuthorizationNumber() == null && transaction.getLast4DigitsOfCardNumber() != null) {
                    cardNum = WebCoreConstants.EMPTY_STRING;
                } else if (transaction.getAuthorizationNumber() == null && transaction.getLast4DigitsOfCardNumber() == null) {
                    cardNum = WebCoreUtil.prefixZeroes((short) 0, WebCoreConstants.MAX_NUMBERS_LAST_DIGITS);
                } else {
                    final int digs = transaction.getLast4DigitsOfCardNumber();
                    cardNum = WebCoreUtil.prefixZeroes((short) digs, WebCoreConstants.MAX_NUMBERS_LAST_DIGITS);
                }
                transItem.setCardNumber(cardNum);
                transItem.setCardProcessingAuthorizationNumber(transaction.getAuthorizationNumber() == null ? WebCoreConstants.EMPTY_STRING
                        : transaction.getAuthorizationNumber());
                
                if (transaction.getProcessingDate() == null) {
                    transItem.setCardProcessingDate(new Date(0));
                } else {
                    transItem.setCardProcessingDate(transaction.getProcessingDate());
                }
                transItem.setCardProcessingStatus(transaction.getProcessorTransactionType() == null ? WebCoreConstants.EMPTY_STRING : transaction
                        .getProcessorTransactionType().toString());
                transItem.setCardProcessingReferenceNumber(transaction.getReferenceNumber() == null ? WebCoreConstants.EMPTY_STRING : transaction
                        .getReferenceNumber());
                transItem.setCardProcessingMerchantAccount(transaction.getMerchantAccountName() == null ? WebCoreConstants.EMPTY_STRING : transaction
                        .getMerchantAccountName());
                transItem.setCardProcessingRetries(transaction.getNumRetries() == null ? 0 : transaction.getNumRetries());
                transItem.setCardProcessingTransactionId(transaction.getProcessorTransactionId() == null ? WebCoreConstants.EMPTY_STRING
                        : transaction.getProcessorTransactionId());
            }
            
            transItem.setGroupName(groupName);
            transItem.setCitationNumber(WebCoreConstants.EMPTY_STRING);
            
            if (TransactionInfoServiceUtil.isEqualOrGreaterTo(WS_VERSION_1_3, version)) {
                if (transaction.getCouponNumber() != null) {
                    transItem.setCouponNumber(transaction.getCouponNumber());
                    if (transaction.getPercentDiscount() != null && transaction.getPercentDiscount() > 0) {
                        transItem.setCouponDiscountPercent(transaction.getPercentDiscount());
                    }
                    if (transaction.getDollarDiscountAmount() != null && transaction.getDollarDiscountAmount() > 0.0f) {
                        transItem.setCouponDiscountDollar(transaction.getDollarDiscountAmount().floatValue()
                                                          / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1);
                    }
                }
            } else {
                transItem.setCouponNumber(transaction.getCouponNumber() == null ? WebCoreConstants.EMPTY_STRING : transaction.getCouponNumber());
                transItem.setCouponDiscountAmount(transaction.getPercentDiscount() == null ? 0 : transaction.getPercentDiscount());
            }
            
            transItem.setExcessAmount(transaction.getExcessPaymentAmount().floatValue() / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1);
            transItem.setChargedAmount(transaction.getChargedAmount().floatValue() / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1);
            transItem.setTotalCashAmount(transaction.getCashPaidAmount().floatValue() / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1);
            transItem.setTotalCardAmount(transaction.getCardPaidAmount().floatValue() / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1);
            final float changeDispensed = transaction.getChangeDispensedAmount().floatValue() / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1;
            final byte isRefundSlip = transaction.isRefundSlip();
            transItem.setRefundChangeIssued(changeDispensed);
            if (isRefundSlip == 1) {
                final float refundTicketAmount = transItem.getTotalCashAmount() + transItem.getTotalCardAmount() - transItem.getChargedAmount()
                                                 - changeDispensed - transItem.getExcessAmount();
                transItem.setRefundTicketAmount(Math.round(refundTicketAmount * StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1)
                                                / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1);
            }
            // Process Value Card Type
            final String customCardData = transaction.getCustomCardData();
            final String customCardType = transaction.getCustomCardType();
            
            if ((transItem.getCardType() == null || transItem.getCardType().length() == 0) && customCardType != null && customCardType.length() != 0) {
                transItem.setCardType(customCardType);
                transItem.setCardNumber(Track2Card.getLast4DigitsOfAccountNumber(customCardData, Track2Card.TRACK_2_DELIMITER));
            }
            
            if (StringUtils.isEmpty(groupName)) {
                transItem.setGroupName(transaction.getGroupName());
            } else {
                transItem.setGroupName(groupName);
            }
            
            // Includes rate data if version variable is >= v1.1.
            if (TransactionInfoServiceUtil.isEqualOrGreaterTo(WS_VERSION_1_1_LUKEII, version)) {
                transItem.setRateId(transaction.getRateId().longValue());
                transItem.setRateName(transaction.getRateName());
                transItem.setRateValue(transaction.getRateAmount().floatValue() / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1);
            } else {
                transItem.setRateId((long) 0);
                transItem.setRateName(WebCoreConstants.EMPTY_STRING);
                transItem.setRateValue(0.0f / StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1);
                
            }
            
            // Includes plate number if version variable is >= v1.2.
            if (TransactionInfoServiceUtil.isEqualOrGreaterTo(WS_VERSION_1_2, version)) {
                transItem.setPlateNumber(transaction.getLicencePlateNumber() == null ? WebCoreConstants.EMPTY_STRING : transaction
                        .getLicencePlateNumber());
            } else {
                transItem.setPlateNumber(null);
            }
            
            transList.add(transItem);
        }
    }
    
    private TransactionResponse getTransaction(final Object[] args, final int reqType, final String methodName, final String version)
        throws InfoServiceFaultMessage {
        int maxCount;
        Object[] tmpArgs;
        final String groupName = "";
        maxCount = super.emsPropertiesService.getPropertyValueAsInt(EmsPropertiesService.TRANSACTION_INFO_MAX_RECORDS,
                                                                    TRANSACTION_INFO_MAX_RECORDS_DEFAULT);
        
        tmpArgs = args;
        final int totalSize = this.transactionInfoDtoService.getTransactionListCount(reqType, tmpArgs);
        
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Total TransactionInfo Records: " + totalSize);
        }
        final TransactionResponse transactionResponse = new TransactionResponse();
        if (totalSize > 0) {
            if (totalSize > maxCount) {
                LOGGER.info("Total TransactionInfo Records : " + totalSize + " is greater than max allowed records: " + maxCount);
                throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE,
                        createInfoServiceFault("Please use more specific parameters as too many records are returned."));
            }
            final List<TransactionInfoDto> transactionList = this.transactionInfoDtoService.getTransactionList(reqType, tmpArgs);
            convertResultToTransactionInfoType(transactionList, transactionResponse.getTransactionInfoTypes(), groupName, version);
        }
        return transactionResponse;
    }
    
    @SuppressWarnings("PMD.InefficientEmptyStringCheck")
    public final TransactionResponse getTransactionByStall(final TransactionByStallRequest transactionByStallRequest) throws InfoServiceFaultMessage {
        final Authentication auth = authenticationCheck(STRING_GETTRANSACTIONBYSTALL, transactionByStallRequest.getToken());
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage("getTransactionByStall Failure", createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        
        // verify input parameters
        final String token = transactionByStallRequest.getToken();
        final Date purchasedDateFrom = transactionByStallRequest.getPurchasedDateFrom();
        final Date purchasedDateTo = transactionByStallRequest.getPurchasedDateTo();
        final String stallFrom = transactionByStallRequest.getStallNumberFrom();
        final String stallTo = transactionByStallRequest.getStallNumberTo();
        final String version = this.verifyVersion(transactionByStallRequest.getVersion());
        
        // check stall range, will throw InfoServiceFaultMessage if invalid
        checkStallRange(stallFrom, stallTo, STRING_GETTRANSACTIONBYSTALL);
        // check purchased Date range, will throw InfoServiceFaultMessage if invalid
        checkPurchasedDate(purchasedDateFrom, purchasedDateTo, STRING_GETTRANSACTIONBYSTALL);
        // check the token, will throw InfoServiceFaultMessage if invalid
        checkToken(token, customerId, STRING_GETTRANSACTIONBYSTALL);
        
        int stallNumberFrom;
        int stallNumberTo;
        if (stallFrom.trim().length() == 0) {
            stallNumberFrom = 0;
        } else {
            stallNumberFrom = Integer.parseInt(stallFrom);
        }
        if (stallTo.trim().length() == 0) {
            stallNumberTo = 0;
        } else {
            stallNumberTo = Integer.parseInt(stallTo);
        }
        
        TransactionResponse transactionResponse = new TransactionResponse();
        
        final Object[] args = new Object[] { customerId, purchasedDateFrom, purchasedDateTo, stallNumberFrom, stallNumberTo };
        transactionResponse = getTransaction(args, WebCoreConstants.GETTRANSACTIONINFOBYSTALL_TYPE, STRING_GETTRANSACTIONBYSTALL, version);
        return transactionResponse;
    }
    
    public final TransactionResponse getTransactionByPurchasedDate(final TransactionByPurchasedDateRequest transactionByPurchasedDateRequest)
        throws InfoServiceFaultMessage {
        final Authentication auth = authenticationCheck(STRING_GETTRANSACTIONBYPURCHASEDDATE, transactionByPurchasedDateRequest.getToken());
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage("getTransactionByPurchasedDate Failure", createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        
        // verify input parameters
        final String token = transactionByPurchasedDateRequest.getToken();
        final Date purchasedDateFrom = transactionByPurchasedDateRequest.getPurchasedDateFrom();
        final Date purchasedDateTo = transactionByPurchasedDateRequest.getPurchasedDateTo();
        final String version = this.verifyVersion(transactionByPurchasedDateRequest.getVersion());
        
        // check purchased Date range, will throw InfoServiceFaultMessage if invalid
        checkPurchasedDate(purchasedDateFrom, purchasedDateTo, STRING_GETTRANSACTIONBYPURCHASEDDATE);
        // check the token, will throw InfoServiceFaultMessage if invalid
        checkToken(token, customerId, STRING_GETTRANSACTIONBYPURCHASEDDATE);
        
        TransactionResponse transactionResponse = new TransactionResponse();
        final Object[] args = new Object[] { customerId, purchasedDateFrom, purchasedDateTo };
        transactionResponse = getTransaction(args, WebCoreConstants.GETTRANSACTIONINFOBYPURCHASEDDATE_TYPE, STRING_GETTRANSACTIONBYPURCHASEDDATE,
                                             version);
        return transactionResponse;
    }
    
    public final TransactionResponse getTransactionBySerialNumber(final TransactionBySerialRequest transactionBySerialRequest)
        throws InfoServiceFaultMessage {
        final Authentication auth = authenticationCheck(STRING_GETTRANSACTIONBYSERIALNUMBER, transactionBySerialRequest.getToken());
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage("getTransactionBySerialNumber Failure", createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        
        // verify input parameters
        final String token = transactionBySerialRequest.getToken();
        final Date purchasedDateFrom = transactionBySerialRequest.getPurchasedDateFrom();
        final Date purchasedDateTo = transactionBySerialRequest.getPurchasedDateTo();
        final String serialNumber = transactionBySerialRequest.getSerialNumber();
        final String version = this.verifyVersion(transactionBySerialRequest.getVersion());
        
        // check serial number, will throw InfoServiceFaultMessage if invalid
        checkSerialNumber(serialNumber, STRING_GETTRANSACTIONBYSERIALNUMBER);
        // check purchased Date range, will throw InfoServiceFaultMessage if invalid
        checkPurchasedDate(purchasedDateFrom, purchasedDateTo, STRING_GETTRANSACTIONBYSERIALNUMBER);
        // check the token, will throw InfoServiceFaultMessage if invalid
        checkToken(token, customerId, STRING_GETTRANSACTIONBYSERIALNUMBER);
        
        TransactionResponse transactionResponse = new TransactionResponse();
        final PointOfSale pos = super.pointOfSaleService.findPointOfSaleBySerialNumber(serialNumber);
        if (pos != null) {
            final Object[] args = new Object[] { customerId, purchasedDateFrom, purchasedDateTo, pos.getId() };
            transactionResponse = getTransaction(args, WebCoreConstants.GETTRANSACTIONINFOBYSERIALNUMBER_TYPE, STRING_GETTRANSACTIONBYSERIALNUMBER,
                                                 version);
        }
        return transactionResponse;
    }
    
    public final TransactionResponse getTransactionBySettlementDate(final TransactionBySettlementDateRequest transactionBySettlementDateRequest)
        throws InfoServiceFaultMessage {
        final Authentication auth = authenticationCheck(STRING_GETTRANSACTIONBYSETTLEMENTDATE, transactionBySettlementDateRequest.getToken());
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage("getTransactionBySettlementDate Failure", createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        // verify input parameters
        final String token = transactionBySettlementDateRequest.getToken();
        final Date settlementDateFrom = transactionBySettlementDateRequest.getSettlementDateFrom();
        final Date settlementDateTo = transactionBySettlementDateRequest.getSettlementDateTo();
        final String version = this.verifyVersion(transactionBySettlementDateRequest.getVersion());
        
        // check purchased Date range, will throw InfoServiceFaultMessage if invalid
        checkSettlementDate(settlementDateFrom, settlementDateTo, STRING_GETTRANSACTIONBYSETTLEMENTDATE);
        // check the token, will throw InfoServiceFaultMessage if invalid
        checkToken(token, customerId, STRING_GETTRANSACTIONBYSETTLEMENTDATE);
        
        TransactionResponse transactionResponse = new TransactionResponse();
        final Object[] args = new Object[] { customerId, settlementDateFrom, settlementDateTo };
        transactionResponse = getTransaction(args, WebCoreConstants.GETTRANSACTIONINFOBYSETTLEMENTDATE_TYPE, STRING_GETTRANSACTIONBYSETTLEMENTDATE,
                                             version);
        
        return transactionResponse;
    }
    
    public final TransactionResponse getTransactionByRegion(final TransactionByRegionRequest transactionByRegionRequest)
        throws InfoServiceFaultMessage {
        final Authentication auth = authenticationCheck(STRING_GETTRANSACTIONBYREGION, transactionByRegionRequest.getToken());
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage("getTransactionByRegion Failure", createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        
        // verify input parameters
        final String token = transactionByRegionRequest.getToken();
        final Date purchasedDateFrom = transactionByRegionRequest.getPurchasedDateFrom();
        final Date purchasedDateTo = transactionByRegionRequest.getPurchasedDateTo();
        final String regionName = transactionByRegionRequest.getRegionName();
        final String version = this.verifyVersion(transactionByRegionRequest.getVersion());
        
        // check purchased Date range, will throw InfoServiceFaultMessage if invalid
        checkPurchasedDate(purchasedDateFrom, purchasedDateTo, STRING_GETTRANSACTIONBYREGION);
        // check the token, will throw InfoServiceFaultMessage if invalid
        checkToken(token, customerId, STRING_GETTRANSACTIONBYREGION);
        
        // get region id
        final int locationId = super.findLocationIdOfRequestLocation(customerId, regionName);
        
        TransactionResponse transactionResponse = new TransactionResponse();
        if (locationId > 0) {
            final Object[] args = new Object[] { customerId, purchasedDateFrom, purchasedDateTo, locationId };
            transactionResponse = getTransaction(args, WebCoreConstants.GETTRANSACTIONINFOBYREGION_TYPE, STRING_GETTRANSACTIONBYREGION, version);
        }
        return transactionResponse;
    }
    
    /**
     * Retrieve transactions based on group name
     * 
     * @param transactionByGroupRequest
     *            TransactionByGroupRequest
     * @return response TransactionResponse
     * @throws InfoServiceFaultMessage
     * 
     * @since v6.2.1
     */
    public final TransactionResponse getTransactionByGroup(final TransactionByGroupRequest transactionByGroupRequest) throws InfoServiceFaultMessage {
        final Authentication auth = authenticationCheck(STRING_GETTRANSACTIONBYGROUP, transactionByGroupRequest.getToken());
        if (auth == null) {
            throw new InfoServiceFaultMessage(STRING_GETTRANSACTIONBYGROUP + SPACE_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(STRING_GETTRANSACTIONBYGROUP + SPACE_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        
        // verify input parameters
        final String token = transactionByGroupRequest.getToken();
        final Date purchasedDateFrom = transactionByGroupRequest.getPurchasedDateFrom();
        final Date purchasedDateTo = transactionByGroupRequest.getPurchasedDateTo();
        final String groupName = transactionByGroupRequest.getGroupName();
        
        // check purchased Date range, will throw InfoServiceFaultMessage if invalid
        checkPurchasedDate(purchasedDateFrom, purchasedDateTo, STRING_GETTRANSACTIONBYGROUP);
        // check the token, will throw InfoServiceFaultMessage if invalid
        checkToken(token, customerId, STRING_GETTRANSACTIONBYGROUP);
        // check group name
        if (groupName == null || groupName.trim().equals(WebCoreConstants.EMPTY_STRING)) {
            throw new InfoServiceFaultMessage("TransactionByGroupRequest Failure",
                    createInfoServiceFault(MESSAGE_INPUT_PARAMETERS_MISSING_OR_INVALID));
        }
        final String version = this.verifyVersion(transactionByGroupRequest.getVersion());
        
        TransactionResponse transactionResponse = new TransactionResponse();
        
        final List<PointOfSale> posList = super.getPosListByRouteName(customerId, groupName);
        if (posList != null && !posList.isEmpty()) {
            final Object[] args = new Object[] { customerId, purchasedDateFrom, purchasedDateTo, posList, groupName };
            transactionResponse = getTransaction(args, WebCoreConstants.GETTRANSACTIONINFOBYGROUP_TYPE, STRING_GETTRANSACTIONBYGROUP, version);
        }
        
        return transactionResponse;
    }
    
    public final TransactionResponse getTransactionBySetting(final TransactionBySettingRequest transactionBySettingRequest)
        throws InfoServiceFaultMessage {
        final Authentication auth = authenticationCheck(STRING_GETTRANSACTIONBYSETTING, transactionBySettingRequest.getToken());
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage("getTransactionBySetting Failure", createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        
        // verify input parameters
        final String token = transactionBySettingRequest.getToken();
        final Date purchasedDateFrom = transactionBySettingRequest.getPurchasedDateFrom();
        final Date purchasedDateTo = transactionBySettingRequest.getPurchasedDateTo();
        final String settingName = transactionBySettingRequest.getSettingName();
        final String version = this.verifyVersion(transactionBySettingRequest.getVersion());
        
        // check purchased Date range, will throw InfoServiceFaultMessage if invalid
        checkPurchasedDate(purchasedDateFrom, purchasedDateTo, STRING_GETTRANSACTIONBYSETTING);
        // check the token, will throw InfoServiceFaultMessage if invalid
        checkToken(token, customerId, STRING_GETTRANSACTIONBYSETTING);
        
        // get paystation list
        final Customer customer = new Customer();
        customer.setId(customerId);
        final PaystationSetting paystationSetting = super.paystationSettingService.findByCustomerIdAndLotName(customer, settingName);
        
        TransactionResponse transactionResponse = new TransactionResponse();
        if (paystationSetting != null) {
            final Object[] args = new Object[] { customerId, purchasedDateFrom, purchasedDateTo, paystationSetting.getId() };
            transactionResponse = getTransaction(args, WebCoreConstants.GETTRANSACTIONINFOBYSETTING_TYPE, STRING_GETTRANSACTIONBYSETTING, version);
        }
        return transactionResponse;
    }
    
    public final PaymentTypeResponse getPaymentTypes(final PaymentTypeRequest paymentTypeRequest) throws InfoServiceFaultMessage {
        final Authentication auth = authenticationCheck(GET_PAYMENTTYPES, paymentTypeRequest.getToken());
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYMENTTYPES + SPACE_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        
        // verify input parameters
        final String token = paymentTypeRequest.getToken();
        
        // check the token, will throw InfoServiceFaultMessage if invalid
        checkToken(token, customerId, GET_PAYMENTTYPES);
        
        final String version = this.verifyVersion(paymentTypeRequest.getVersion());
        
        if (TransactionInfoServiceUtil.isEqualOrGreaterTo(WS_VERSION_1_4, version)) {
            final PaymentTypeResponse paymentTypeResponse = new PaymentTypeResponse();
            final List<com.digitalpaytech.domain.PaymentType> paymentTypeList = this.paymentTypeService.loadAll();
            for (com.digitalpaytech.domain.PaymentType pt : paymentTypeList) {
                final PaymentType paymentType = new PaymentType();
                paymentType.setId(pt.getId());
                paymentType.setName(pt.getName());
                paymentType.setDescription(pt.getDescription());
                paymentTypeResponse.getPaymentType().add(paymentType);
            }
            return paymentTypeResponse;
        } else {
            return this.legacyPaymentTypeResponse;
        }
    }
    
    public final TransactionTypeResponse getTransactionTypes(final TransactionTypeRequest transactionTypeRequest) throws InfoServiceFaultMessage {
        final Authentication auth = authenticationCheck(GET_TRANSACTIONTYPES, transactionTypeRequest.getToken());
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_TRANSACTIONTYPES + SPACE_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        
        // verify input parameters
        final String token = transactionTypeRequest.getToken();
        
        // check the token, will throw InfoServiceFaultMessage if invalid
        checkToken(token, customerId, GET_TRANSACTIONTYPES);
        
        final TransactionTypeResponse transactionTypeResponse = new TransactionTypeResponse();
        final List<com.digitalpaytech.domain.TransactionType> transactionTypeList = this.transactionTypeService.loadAll();
        for (com.digitalpaytech.domain.TransactionType tt : transactionTypeList) {
            final TransactionType transType = new TransactionType();
            transType.setId(tt.getId().byteValue());
            transType.setName(tt.getName());
            transType.setDescription(tt.getDescription());
            transactionTypeResponse.getTransactionType().add(transType);
        }
        return transactionTypeResponse;
    }
    
    public final ProcessingStatusTypeResponse getProcessingStatusTypes(final ProcessingStatusTypeRequest processingStatusTypeRequest)
        throws InfoServiceFaultMessage {
        final Authentication auth = authenticationCheck(GET_PROCESSINGSTATUSTYPES, processingStatusTypeRequest.getToken());
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PROCESSINGSTATUSTYPES + SPACE_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        
        // verify input parameters
        final String token = processingStatusTypeRequest.getToken();
        
        // check the token, will throw InfoServiceFaultMessage if invalid
        checkToken(token, customerId, GET_PROCESSINGSTATUSTYPES);
        
        final ProcessingStatusTypeResponse processingStatusTypeResponse = new ProcessingStatusTypeResponse();
        final List<ProcessorTransactionType> ptList = this.processorTransactionTypeService.loadAll();
        for (ProcessorTransactionType pst : ptList) {
            final ProcessingStatusType procType = new ProcessingStatusType();
            procType.setId((byte) pst.getId());
            procType.setName(pst.getStatusName());
            procType.setDescription(pst.getDescription());
            processingStatusTypeResponse.getProcessingStatusType().add(procType);
        }
        return processingStatusTypeResponse;
    }
    
    private String verifyVersion(final VersionType versionType) {
        String version = "";
        if (versionType == null || versionType.value().trim().equals(WebCoreConstants.EMPTY_STRING)) {
            version = WS_VERSION_1_0;
        } else {
            version = versionType.value();
        }
        LOGGER.info("+++ version: " + version + " +++");
        return version;
    }
}
