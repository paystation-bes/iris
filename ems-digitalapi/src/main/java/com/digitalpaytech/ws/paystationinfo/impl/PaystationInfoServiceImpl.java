package com.digitalpaytech.ws.paystationinfo.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.EventActionType;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.PosEventCurrent;
import com.digitalpaytech.domain.PosHeartbeat;
import com.digitalpaytech.domain.PosSensorInfo;
import com.digitalpaytech.domain.PosSensorState;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.dto.paystation.PaystationEventInfo;
import com.digitalpaytech.service.EventActionTypeService;
import com.digitalpaytech.service.EventDeviceTypeService;
import com.digitalpaytech.service.EventSeverityTypeService;
import com.digitalpaytech.service.PosAlertStatusService;
import com.digitalpaytech.service.PosEventCurrentService;
import com.digitalpaytech.service.PosSensorInfoService;
import com.digitalpaytech.service.paystation.PaystationEventInfoService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.ws.BaseWsServiceImpl;
import com.digitalpaytech.ws.paystationinfo.InfoServiceFault;
import com.digitalpaytech.ws.paystationinfo.InfoServiceFaultMessage;
import com.digitalpaytech.ws.paystationinfo.PaystationEventsByGroupRequest;
import com.digitalpaytech.ws.paystationinfo.PaystationEventsByRegionRequest;
import com.digitalpaytech.ws.paystationinfo.PaystationEventsBySerialRequest;
import com.digitalpaytech.ws.paystationinfo.PaystationEventsRequest;
import com.digitalpaytech.ws.paystationinfo.PaystationEventsResponse;
import com.digitalpaytech.ws.paystationinfo.PaystationEventsType;
import com.digitalpaytech.ws.paystationinfo.PaystationGroupsRequest;
import com.digitalpaytech.ws.paystationinfo.PaystationGroupsResponse;
import com.digitalpaytech.ws.paystationinfo.PaystationGroupsType;
import com.digitalpaytech.ws.paystationinfo.PaystationInfoService;
import com.digitalpaytech.ws.paystationinfo.PaystationRequest;
import com.digitalpaytech.ws.paystationinfo.PaystationResponse;
import com.digitalpaytech.ws.paystationinfo.PaystationSerialListType;
import com.digitalpaytech.ws.paystationinfo.PaystationStatusByGroupRequest;
import com.digitalpaytech.ws.paystationinfo.PaystationStatusByRegionRequest;
import com.digitalpaytech.ws.paystationinfo.PaystationStatusBySerialRequest;
import com.digitalpaytech.ws.paystationinfo.PaystationStatusEventType;
import com.digitalpaytech.ws.paystationinfo.PaystationStatusRequest;
import com.digitalpaytech.ws.paystationinfo.PaystationStatusResponse;
import com.digitalpaytech.ws.paystationinfo.PaystationStatusType;
import com.digitalpaytech.ws.paystationinfo.PaystationType;
import com.digitalpaytech.ws.paystationinfo.VersionType;
import com.digitalpaytech.ws.util.TransactionInfoServiceUtil;

@Component("paystationInfoService")
@WebService(serviceName = "PaystationInfoService", targetNamespace = "http://ws.digitalpaytech.com/paystationInfo", endpointInterface = "com.digitalpaytech.ws.paystationinfo.PaystationInfoService")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
@SuppressWarnings({ "PMD.GodClass", "PMD.ExcessiveImports", "PMD.UseConcurrentHashMap", "PMD.AvoidInstantiatingObjectsInLoops" })
public class PaystationInfoServiceImpl extends BaseWsServiceImpl implements PaystationInfoService {
    
    public static final int MAX_EVENTS_NUMBER = 100;
    
    private static final Logger LOGGER = Logger.getLogger(PaystationInfoServiceImpl.class);
    private static final String GET_PAYSTATIONS_FAILURE = "GetPaystations Failure";
    private static final String GET_GROUPS_FAILURE = "getGroups Failure";
    private static final String GET_PAYSTATION_STATUS_FAILURE = "GetPaystationStatus Failure";
    private static final String GET_PAYSTATION_STATUS_BY_REGION_FAILURE = "GetPaystationStatusByRegion Failure";
    private static final String GET_PAYSTATION_STATUS_BY_SERIAL_FAILURE = "getPaystationStatusBySerial Failure";
    private static final String GET_PAYSTATION_STATUS_BY_GROUP_FAILURE = "getPaystationStatusByGroup Failure";
    private static final String GET_PAYSTATION_EVENTS = "GetPaystationEvents";
    private static final String GET_PAYSTATION_EVENTS_FAILURE = "GetPaystationEvents Failure";
    private static final String GET_PAYSTATION_EVENTS_BY_REGION_FAILURE = "GetPaystationEventsByRegion Failure";
    private static final String GET_PAYSTATION_EVENTS_BY_SERIAL_FAILURE = "getPaystationEventsBySerial Failure";
    private static final String GET_PAYSTATION_EVENTS_BY_GROUP_FAILURE = "getPaystationEventsByGroup Failure";
    
    private static final String SPACE_AND_THREE_PLUSES = " +++";
    private static final String N_A_IN_BRACKETS = "(n/a)";
    private static final String SPACE_FAILURE = " Failure";
    private static final String DATE_FROM_OR_DATE_TO = "dateFrom or dateTo";
    private static final String DATE_FROM = "dateFrom";
    private static final String DATE_TO = "dateTo";
    
    private static final Map<Integer, String> EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP = new HashMap<Integer, String>();
    private static final Map<String, Collection<Integer>> TYPE_TO_EVENT_DEVICE_TYPE_IDS_MAP = new HashMap<String, Collection<Integer>>();
    private static final Map<String, Integer> LUKEII_EVENT_MAP = new HashMap<String, Integer>();
    
    // INJECTED
    
    @Autowired
    private EventDeviceTypeService eventDeviceTypeService;
    @Autowired
    private PosSensorInfoService posSensorInfoService;
    @Autowired
    private PaystationEventInfoService paystationEventInfoService;
    @Autowired
    private EventSeverityTypeService eventSeverityTypeService;
    @Autowired
    private PosAlertStatusService posAlertStatusService;
    @Autowired
    private PosEventCurrentService posEventCurrentService;
    @Autowired
    private EventActionTypeService eventActionTypeService;
    
    public PaystationInfoServiceImpl() {
        super();
        webServiceEndPointType = WebCoreConstants.END_POINT_ID_PAY_STATION_INFO;
    }
    
    @PostConstruct
    public final void initializeMaps() {
        
        LUKEII_EVENT_MAP.put(this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_CANISTER),
                             WebCoreConstants.EVENT_DEVICE_TYPE_COIN_CANISTER);
        LUKEII_EVENT_MAP.put(this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_ESCROW),
                             WebCoreConstants.EVENT_DEVICE_TYPE_COIN_ESCROW);
        
        EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP.put(WebCoreConstants.EVENT_DEVICE_TYPE_BATTERY,
                                             this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_BATTERY));
        EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP.put(WebCoreConstants.EVENT_DEVICE_TYPE_BATTERY2,
                                             this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_BATTERY2));
        EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP.put(WebCoreConstants.EVENT_DEVICE_TYPE_BILL_ACCEPTOR,
                                             this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_BILL_ACCEPTOR));
        EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP.put(WebCoreConstants.EVENT_DEVICE_TYPE_BILL_STACKER,
                                             this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_BILL_STACKER));
        EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP.put(WebCoreConstants.EVENT_DEVICE_TYPE_CARD_READER,
                                             this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_CARD_READER));
        EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP.put(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_ACCEPTOR,
                                             this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_ACCEPTOR));
        EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP.put(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_CHANGER,
                                             this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_CHANGER));
        EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP.put(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_HOPPER1,
                                             this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_HOPPER1));
        EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP.put(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_HOPPER2,
                                             this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_HOPPER2));
        EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP.put(WebCoreConstants.EVENT_DEVICE_TYPE_DOOR,
                                             this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_DOOR));
        EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP.put(WebCoreConstants.EVENT_DEVICE_TYPE_DOOR_LOWER,
                                             this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_DOOR_LOWER));
        EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP.put(WebCoreConstants.EVENT_DEVICE_TYPE_DOOR_UPPER,
                                             this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_DOOR_UPPER));
        EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP.put(WebCoreConstants.EVENT_DEVICE_TYPE_PAY_STATION,
                                             this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_PAY_STATION));
        EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP.put(WebCoreConstants.EVENT_DEVICE_TYPE_PRINTER,
                                             this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_PRINTER));
        EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP.put(WebCoreConstants.EVENT_DEVICE_TYPE_SHOCK_ALARM,
                                             this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_SHOCK_ALARM));
        EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP.put(WebCoreConstants.EVENT_DEVICE_TYPE_PCM,
                                             this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_PCM));
        
        /* Luke2 support */
        
        EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP.put(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_CANISTER,
                                             this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_CANISTER));
        EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP.put(WebCoreConstants.EVENT_DEVICE_TYPE_MAINTENANCE_DOOR,
                                             this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_MAINTENANCE_DOOR));
        EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP.put(WebCoreConstants.EVENT_DEVICE_TYPE_CASH_VAULT_DOOR,
                                             this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_CASH_VAULT_DOOR));
        EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP.put(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_ESCROW,
                                             this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_ESCROW));
        
        Collection<Integer> deviceIds = new ArrayList<Integer>();
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_BILL_ACCEPTOR);
        TYPE_TO_EVENT_DEVICE_TYPE_IDS_MAP.put(this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_BILL_ACCEPTOR),
                                              deviceIds);
        
        deviceIds = new ArrayList<Integer>();
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_BILL_STACKER);
        TYPE_TO_EVENT_DEVICE_TYPE_IDS_MAP.put(this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_BILL_STACKER), deviceIds);
        
        deviceIds = new ArrayList<Integer>();
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_CARD_READER);
        TYPE_TO_EVENT_DEVICE_TYPE_IDS_MAP.put(this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_CARD_READER), deviceIds);
        
        deviceIds = new ArrayList<Integer>();
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_ACCEPTOR);
        TYPE_TO_EVENT_DEVICE_TYPE_IDS_MAP.put(this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_ACCEPTOR),
                                              deviceIds);
        
        deviceIds = new ArrayList<Integer>();
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_CHANGER);
        TYPE_TO_EVENT_DEVICE_TYPE_IDS_MAP.put(this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_CHANGER), deviceIds);
        
        deviceIds = new ArrayList<Integer>();
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_HOPPER1);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_HOPPER2);
        TYPE_TO_EVENT_DEVICE_TYPE_IDS_MAP.put(this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_HOPPER1), deviceIds);
        
        /* Luke2 support */
        
        deviceIds = new ArrayList<Integer>();
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_CANISTER);
        TYPE_TO_EVENT_DEVICE_TYPE_IDS_MAP.put(this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_CANISTER),
                                              deviceIds);
        
        deviceIds = new ArrayList<Integer>();
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_ESCROW);
        TYPE_TO_EVENT_DEVICE_TYPE_IDS_MAP.put(this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_ESCROW), deviceIds);
        
        /* Default String : "Paystation" */
        deviceIds = new ArrayList<Integer>();
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_BATTERY);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_BATTERY2);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_DOOR);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_DOOR_LOWER);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_DOOR_UPPER);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_PAY_STATION);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_SHOCK_ALARM);
        
        /* Luke2 support */
        
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_CASH_VAULT_DOOR);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_MAINTENANCE_DOOR);
        TYPE_TO_EVENT_DEVICE_TYPE_IDS_MAP.put(this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_BATTERY), deviceIds);
        
        deviceIds = new ArrayList<Integer>();
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_PCM);
        TYPE_TO_EVENT_DEVICE_TYPE_IDS_MAP.put(this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_PCM), deviceIds);
        
        deviceIds = new ArrayList<Integer>();
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_PRINTER);
        TYPE_TO_EVENT_DEVICE_TYPE_IDS_MAP.put(this.eventDeviceTypeService.getTextNoSpace(WebCoreConstants.EVENT_DEVICE_TYPE_PRINTER), deviceIds);
        
        deviceIds = new ArrayList<Integer>();
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_BILL_ACCEPTOR);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_BILL_STACKER);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_CARD_READER);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_ACCEPTOR);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_CHANGER);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_HOPPER1);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_HOPPER2);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_BATTERY);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_BATTERY2);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_DOOR);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_DOOR_LOWER);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_DOOR_UPPER);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_PAY_STATION);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_SHOCK_ALARM);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_PCM);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_PRINTER);
        
        /* Luke2 support */
        
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_CANISTER);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_MAINTENANCE_DOOR);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_CASH_VAULT_DOOR);
        deviceIds.add(WebCoreConstants.EVENT_DEVICE_TYPE_COIN_ESCROW);
        TYPE_TO_EVENT_DEVICE_TYPE_IDS_MAP.put(WebCoreConstants.ALL_STRING, deviceIds);
    }
    
    private String getEventType(final PaystationEventInfo evt) {
        return EVENT_DEVICE_TYPE_ID_TO_TYPE_MAP.get(evt.getEventDeviceTypeId());
    }
    
    private Collection<Integer> getDeviceIdsForType(final String eventType) {
        return TYPE_TO_EVENT_DEVICE_TYPE_IDS_MAP.get(eventType);
    }
    
    @SuppressWarnings("checkstyle:cyclomaticcomplexity")
    private String getEventAction(final PaystationEventInfo evt, final String version) {
        
        switch (evt.getEventDeviceTypeId()) {
            case WebCoreConstants.EVENT_DEVICE_TYPE_BATTERY:
            case WebCoreConstants.EVENT_DEVICE_TYPE_BATTERY2:
                return getBatteryAction(evt);
            case WebCoreConstants.EVENT_DEVICE_TYPE_BILL_ACCEPTOR:
                return getBillAcceptorAction(evt);
            case WebCoreConstants.EVENT_DEVICE_TYPE_BILL_STACKER:
                return getBillStackerAction(evt);
            case WebCoreConstants.EVENT_DEVICE_TYPE_CARD_READER:
                return getCardReaderAction(evt, version);
            case WebCoreConstants.EVENT_DEVICE_TYPE_COIN_ACCEPTOR:
                return getCoinAcceptorAction(evt);
            case WebCoreConstants.EVENT_DEVICE_TYPE_COIN_CHANGER:
                return getCoinChangerAction(evt);
            case WebCoreConstants.EVENT_DEVICE_TYPE_COIN_HOPPER1:
            case WebCoreConstants.EVENT_DEVICE_TYPE_COIN_HOPPER2:
                return getCoinHopperAction(evt);
            case WebCoreConstants.EVENT_DEVICE_TYPE_DOOR:
            case WebCoreConstants.EVENT_DEVICE_TYPE_DOOR_LOWER:
            case WebCoreConstants.EVENT_DEVICE_TYPE_DOOR_UPPER:
            case WebCoreConstants.EVENT_DEVICE_TYPE_MAINTENANCE_DOOR:
            case WebCoreConstants.EVENT_DEVICE_TYPE_CASH_VAULT_DOOR:
                return getDoorAction(evt);
            case WebCoreConstants.EVENT_DEVICE_TYPE_PAY_STATION:
                return getPaystationAction(evt);
            case WebCoreConstants.EVENT_DEVICE_TYPE_PRINTER:
                return getPrinterAction(evt);
            case WebCoreConstants.EVENT_DEVICE_TYPE_SHOCK_ALARM:
                return getShockAlarmAction(evt);
            case WebCoreConstants.EVENT_DEVICE_TYPE_PCM:
                return getPcmAction(evt);
            case WebCoreConstants.EVENT_DEVICE_TYPE_COIN_CANISTER:
                return getCoinCanisterAction(evt);
            case WebCoreConstants.EVENT_DEVICE_TYPE_COIN_ESCROW:
                return getCoinEscrowAction(evt);
            default:
                return WebCoreConstants.EVENT_ACTION_NORMAL_STRING;
        }
    }
    
    private String getBatteryAction(final PaystationEventInfo evt) {
        if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_BATTERY_LEVEL && evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_MAJOR) {
            return WebCoreConstants.EVENT_ACTION_VOLTAGE_LOW_STRING;
        } else {
            return WebCoreConstants.EVENT_ACTION_VOLTAGE_STRING;
        }
    }
    
    @SuppressWarnings("checkstyle:cyclomaticcomplexity")
    private String getBillAcceptorAction(final PaystationEventInfo evt) {
        
        if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_BILLACCEPTOR_LEVEL
            && evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_MAJOR) {
            return WebCoreConstants.EVENT_ACTION_UNABLE_TO_STACK_STRING;
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_BILLACCEPTOR_LEVEL
                   && evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_CLEAR) {
            return WebCoreConstants.EVENT_ACTION_UNABLE_TO_STACK_CLEARED_STRING;
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_BILLACCEPTOR_JAM
                   && evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_MAJOR) {
            return WebCoreConstants.EVENT_ACTION_JAM_STRING;
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_BILLACCEPTOR_JAM
                   && evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_CLEAR) {
            return WebCoreConstants.EVENT_ACTION_JAM_CLEARED_STRING;
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_BILLACCEPTOR_PRESENT && evt.getIsActive() == 0) {
            return WebCoreConstants.EVENT_ACTION_PRESENT_STRING;
        } else {
            return WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING;
            
        }
    }
    
    private String getBillStackerAction(final PaystationEventInfo evt) {
        switch (evt.getEventTypeId()) {
            case WebCoreConstants.EVENT_TYPE_BILLSTACKER_PRESENT:
                if (evt.getEventActionTypeId() != null && evt.getEventActionTypeId() == WebCoreConstants.EVENT_ACTION_TYPE_SET) {
                    return WebCoreConstants.EVENT_ACTION_PRESENT_STRING;
                } else {
                    return WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING;
                }
            case WebCoreConstants.EVENT_TYPE_BILLSTACKER_LEVEL:
                if (evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_MAJOR) {
                    return WebCoreConstants.EVENT_ACTION_FULL_STRING;
                } else if (evt.getEventActionTypeId() != null && evt.getEventActionTypeId() == WebCoreConstants.EVENT_ACTION_TYPE_NORMAL) {
                    return WebCoreConstants.EVENT_ACTION_NORMAL_STRING;
                } else {
                    return WebCoreConstants.EVENT_ACTION_FULL_CLEARED_STRING;
                }
            default:
                if (evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_MINOR) {
                    return WebCoreConstants.EVENT_ACTION_REMOVED_STRING;
                } else {
                    return WebCoreConstants.EVENT_ACTION_INSERTED_STRING;
                    
                }
                
        }
    }
    
    private String getCardReaderAction(final PaystationEventInfo evt, final String version) {
        final String eventAction;
        if (evt.getEventActionTypeId() != null && WS_VERSION_1_2.equals(version)) {
            final EventActionType eventActionType = eventActionTypeService.findEventActionType(evt.getEventActionTypeId());
            eventAction = eventActionType.getName();
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_CARDREADER_PRESENT
                   && evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_CLEAR) {
            eventAction = WebCoreConstants.EVENT_ACTION_PRESENT_STRING;
        } else {
            eventAction = WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING;
        }
        return eventAction;
    }
    
    private String getCoinAcceptorAction(final PaystationEventInfo evt) {
        if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_COINACCEPTOR_JAM && evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_MAJOR) {
            return WebCoreConstants.EVENT_ACTION_JAM_STRING;
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_COINACCEPTOR_JAM
                   && evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_CLEAR) {
            return WebCoreConstants.EVENT_ACTION_JAM_CLEARED_STRING;
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_COINACCEPTOR_PRESENT && evt.getIsActive() == 0) {
            return WebCoreConstants.EVENT_ACTION_PRESENT_STRING;
        } else {
            return WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING;
        }
    }
    
    private String getCoinEscrowAction(final PaystationEventInfo evt) {
        if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_COINESCROW_JAM && evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_MAJOR) {
            return WebCoreConstants.EVENT_ACTION_JAM_STRING;
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_COINESCROW_JAM
                   && evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_CLEAR) {
            return WebCoreConstants.EVENT_ACTION_JAM_CLEARED_STRING;
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_COINESCROW_PRESENT && evt.getIsActive() == 0) {
            return WebCoreConstants.EVENT_ACTION_PRESENT_STRING;
        } else {
            return WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING;
        }
    }
    
    @SuppressWarnings("checkstyle:cyclomaticcomplexity")
    private String getCoinChangerAction(final PaystationEventInfo evt) {
        if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_COINCHANGER_LEVEL
            && evt.getEventActionTypeId() == WebCoreConstants.EVENT_ACTION_TYPE_EMPTY) {
            return WebCoreConstants.EVENT_ACTION_EMPTY_STRING;
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_COINCHANGER_LEVEL
                   && evt.getEventActionTypeId() == WebCoreConstants.EVENT_ACTION_TYPE_LOW) {
            return WebCoreConstants.EVENT_ACTION_LOW_STRING;
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_COINCHANGER_LEVEL) {
            return WebCoreConstants.EVENT_ACTION_NORMAL_STRING;
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_COINCHANGER_JAM
                   && evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_MAJOR) {
            return WebCoreConstants.EVENT_ACTION_JAM_STRING;
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_COINCHANGER_JAM
                   && evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_CLEAR) {
            return WebCoreConstants.EVENT_ACTION_JAM_CLEARED_STRING;
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_COINCHANGER_PRESENT && evt.getIsActive() == 0) {
            return WebCoreConstants.EVENT_ACTION_PRESENT_STRING;
        } else {
            return WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING;
        }
        
    }
    
    private String getCoinHopperAction(final PaystationEventInfo evt) {
        if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_COINHOPPER1_LEVEL
            && evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_MAJOR) {
            return WebCoreConstants.EVENT_ACTION_EMPTY_STRING;
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_COINHOPPER1_LEVEL
                   && evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_CLEAR) {
            return WebCoreConstants.EVENT_ACTION_EMPTY_CLEARED_STRING;
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_COINHOPPER1_PRESENT && evt.getIsActive() == 0) {
            
            return WebCoreConstants.EVENT_ACTION_PRESENT_STRING;
        } else {
            return WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING;
        }
    }
    
    private String getDoorAction(final PaystationEventInfo evt) {
        if (evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_MINOR) {
            return WebCoreConstants.EVENT_ACTION_DOOR_OPENED_STRING;
        } else {
            return WebCoreConstants.EVENT_ACTION_DOOR_CLOSED_STRING;
        }
    }
    
    private String getPaystationAction(final PaystationEventInfo evt) {
        if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_LOWER_POWER_SHUTDOWN) {
            if (evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_CRITICAL) {
                return WebCoreConstants.EVENT_ACTION_LOW_POWER_SHUTDOWN_ON_STRING;
            } else {
                return WebCoreConstants.EVENT_ACTION_LOW_POWER_SHUTDOWN_OFF_STRING;
            }
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_PAYSTATION_PUBLICKEY) {
            return WebCoreConstants.EVENT_ACTION_PUBLIC_KEY_UPDATE_STRING;
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_PAYSTATION_SERVICEMODE) {
            if (evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_MINOR) {
                return WebCoreConstants.EVENT_ACTION_SERVICE_MODE_ENABLED_STRING;
            } else {
                return WebCoreConstants.EVENT_ACTION_SERVICE_MODE_DISABLED_STRING;
            }
        } else {
            return evt.getStatusName();
        }
    }
    
    @SuppressWarnings("checkstyle:cyclomaticcomplexity")
    private String getPrinterAction(final PaystationEventInfo evt) {
        if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_PRINTER_CUTTERERROR) {
            if (evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_MINOR) {
                return WebCoreConstants.EVENT_ACTION_CUTTER_ERROR_STRING;
            } else {
                return WebCoreConstants.EVENT_ACTION_CUTTER_ERROR_CLEARED_STRING;
            }
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_PRINTER_HEADERROR) {
            if (evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_MINOR) {
                return WebCoreConstants.EVENT_ACTION_HEAD_ERROR_STRING;
            } else {
                return WebCoreConstants.EVENT_ACTION_HEAD_ERROR_CLEARED_STRING;
            }
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_PRINTER_LEVELDISENGAGED) {
            if (evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_MAJOR) {
                return WebCoreConstants.EVENT_ACTION_LEVER_DISENGAGED_STRING;
            } else {
                return WebCoreConstants.EVENT_ACTION_LEVER_DISENGAGED_CLEARED_STRING;
            }
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_PRINTER_TEMPERATUREERROR) {
            if (evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_MINOR) {
                return WebCoreConstants.EVENT_ACTION_TEMPERATURE_ERROR_STRING;
            } else {
                return WebCoreConstants.EVENT_ACTION_TEMPERATURE_ERROR_CLEARED_STRING;
            }
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_PRINTER_VOLTAGEERROR) {
            if (evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_MINOR) {
                return WebCoreConstants.EVENT_ACTION_VOLTAGE_ERROR_STRING;
            } else {
                return WebCoreConstants.EVENT_ACTION_VOLTAGE_ERROR_CLEARED_STRING;
            }
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_PRINTER_PAPERSTATUS) {
            if (evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_CRITICAL) {
                return WebCoreConstants.EVENT_ACTION_PAPER_OUT_STRING;
            } else if (evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_MAJOR) {
                return WebCoreConstants.EVENT_ACTION_PAPER_LOW_STRING;
            } else {
                return WebCoreConstants.EVENT_ACTION_PAPER_OUT_CLEARED_STRING;
            }
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_PRINTER_PRESENT) {
            if (evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_CRITICAL) {
                return WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING;
            } else {
                return WebCoreConstants.EVENT_ACTION_PRESENT_STRING;
            }
        } else {
            if (evt.getEventSeverityTypeId() > WebCoreConstants.SEVERITY_CLEAR) {
                return WebCoreConstants.EVENT_ACTION_PRESENT_STRING;
            } else {
                return WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING;
            }
        }
    }
    
    private String getShockAlarmAction(final PaystationEventInfo evt) {
        if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_SHOCK_ALARM && evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_CRITICAL) {
            return WebCoreConstants.EVENT_ACTION_SHOCK_ON_STRING;
        } else {
            return WebCoreConstants.EVENT_ACTION_SHOCK_OFF_STRING;
        }
    }
    
    private String getPcmAction(final PaystationEventInfo evt) {
        if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_PCM_UPGRADER) {
            return WebCoreConstants.EVENT_ACTION_UPGRADE_STRING;
        } else {
            return WebCoreConstants.EVENT_ACTION_WIRELESS_SIGNAL_STRENGTH_STRING;
        }
        
    }
    
    private String getCoinCanisterAction(final PaystationEventInfo evt) {
        if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_COINCANISTER_REMOVED
            && evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_MINOR) {
            return WebCoreConstants.EVENT_ACTION_REMOVED_STRING;
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_COINCANISTER_REMOVED
                   && evt.getEventSeverityTypeId() == WebCoreConstants.SEVERITY_CLEAR) {
            return WebCoreConstants.EVENT_ACTION_INSERTED_STRING;
        } else if (evt.getEventTypeId() == WebCoreConstants.EVENT_TYPE_COINCANISTER_PRESENT && evt.getIsActive() == 0) {
            return WebCoreConstants.EVENT_ACTION_PRESENT_STRING;
        } else {
            return WebCoreConstants.EVENT_ACTION_NOT_PRESENT_STRING;
        }
    }
    
    public final void setEventDeviceTypeService(final EventDeviceTypeService eventDeviceTypeService) {
        this.eventDeviceTypeService = eventDeviceTypeService;
    }
    
    public final void setPosSensorInfoService(final PosSensorInfoService posSensorInfoService) {
        this.posSensorInfoService = posSensorInfoService;
    }
    
    public final void setPaystationEventInfoService(final PaystationEventInfoService paystationEventInfoService) {
        this.paystationEventInfoService = paystationEventInfoService;
    }
    
    public final void setEventSeverityTypeService(final EventSeverityTypeService eventSeverityTypeService) {
        this.eventSeverityTypeService = eventSeverityTypeService;
    }
    
    public final EventDeviceTypeService getEventDeviceTypeService() {
        return this.eventDeviceTypeService;
    }
    
    public final PosSensorInfoService getPosSensorInfoService() {
        return this.posSensorInfoService;
    }
    
    public final PaystationEventInfoService getPaystationEventInfoService() {
        return this.paystationEventInfoService;
    }
    
    public final EventSeverityTypeService getEventSeverityTypeService() {
        return this.eventSeverityTypeService;
    }
    
    protected final InfoServiceFault createInfoServiceFault(final String message) {
        final InfoServiceFault fault = new InfoServiceFault();
        fault.setShortErrorMessage(message);
        return fault;
    }
    
    protected final InfoServiceFault createInfoServiceFault(final String message, final Object... args) {
        final InfoServiceFault fault = new InfoServiceFault();
        fault.setShortErrorMessage(MessageFormat.format(message, args));
        return fault;
    }
    
    public final PaystationResponse getPaystations(final PaystationRequest paystationRequest) throws InfoServiceFaultMessage {
        final Authentication auth = authorize(paystationRequest.getToken());
        if (auth == null || auth.getName() == null || auth.getName().length() == 0) {
            throw new InfoServiceFaultMessage(GET_PAYSTATIONS_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        
        // verify input parameters
        if (paystationRequest.getToken() == null) {
            throw new InfoServiceFaultMessage(GET_PAYSTATIONS_FAILURE, createInfoServiceFault(MESSAGE_INPUT_PARAMETERS_MISSING_OR_INVALID));
        }
        
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYSTATIONS_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        // check the token
        if (!isTokenValid(paystationRequest.getToken(), webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYSTATIONS_FAILURE, createInfoServiceFault(MESSAGE_TOKEN_INVALID));
        }
        
        final int customerId = webUser.getCustomerId();
        return getPaystations(customerId);
    }
    
    protected final PaystationResponse getPaystations(final int customerId) {
        final List<PointOfSale> posList = super.pointOfSaleService.findPointOfSalesByCustomerId(customerId, true);
        final PaystationResponse response = new PaystationResponse();
        for (PointOfSale pos : posList) {
            final PaystationType paystationInfoType = convertToInfoType(pos);
            response.getPaystations().add(paystationInfoType);
        }
        return response;
    }
    
    /**
     * Retrieve groups based on user token
     * 
     * @param groupRequest
     *            PaystationGroupsRequest
     * @return groupResponse PaystationGroupsResponse
     * @throws InfoServiceFaultMessage
     * 
     * @since v6.2.1
     */
    public final PaystationGroupsResponse getGroups(final PaystationGroupsRequest request) throws InfoServiceFaultMessage {
        final Authentication auth = authorize(request.getToken());
        if (auth == null || auth.getName() == null || auth.getName().length() == 0) {
            throw new InfoServiceFaultMessage(GET_GROUPS_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        // verify input parameters
        if (request.getToken() == null) {
            throw new InfoServiceFaultMessage(GET_GROUPS_FAILURE, createInfoServiceFault(MESSAGE_INPUT_PARAMETERS_MISSING_OR_INVALID));
        }
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_GROUPS_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        // check the token
        if (!isTokenValid(request.getToken(), webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_GROUPS_FAILURE, createInfoServiceFault(MESSAGE_TOKEN_INVALID));
        }
        final int customerId = webUser.getCustomerId();
        return getRoutes(customerId);
    }
    
    protected final PaystationGroupsResponse getRoutes(final int customerId) {
        final Collection<Route> routeList = this.routeService.findRoutesByCustomerId(customerId);
        
        final PaystationGroupsResponse response = new PaystationGroupsResponse();
        PaystationGroupsType groupType;
        for (Route route : routeList) {
            groupType = new PaystationGroupsType();
            groupType.setGroupName(route.getName());
            response.getPaystationgroup().add(groupType);
        }
        return response;
    }
    
    private PaystationType convertToInfoType(final PointOfSale pos) {
        
        final PaystationType paystationInfoType = new PaystationType();
        paystationInfoType.setSerialNumber(pos.getSerialNumber());
        paystationInfoType.setPaystationName(pos.getName());
        
        final List<Route> routeList = super.routeService.findRoutesByPointOfSaleId(pos.getId());
        for (Route route : routeList) {
            paystationInfoType.getPaystationGroup().add(route.getName());
        }
        paystationInfoType.setRegionName(pos.getLocation().getName());
        paystationInfoType.setRegionId(pos.getLocation().getId());
        
        return paystationInfoType;
    }
    
    /**
     * Find all pay station status,
     * 
     * which may include multiple events for each paystation
     * 
     * @param PaystationStatusRequest
     * 
     * @return PaystationStatusResponse
     * 
     * @since v6.2.1
     */
    public final PaystationStatusResponse getPaystationStatus(final PaystationStatusRequest paystationStatusRequest) throws InfoServiceFaultMessage {
        final Authentication auth = authorize(paystationStatusRequest.getToken());
        if (auth == null) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_STATUS_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        
        // verify input parameters
        if (paystationStatusRequest.getToken() == null) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_STATUS_FAILURE, createInfoServiceFault(MESSAGE_INPUT_PARAMETERS_MISSING_OR_INVALID));
        }
        
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_STATUS_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        // check the token
        if (!isTokenValid(paystationStatusRequest.getToken(), webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_STATUS_FAILURE, createInfoServiceFault(MESSAGE_TOKEN_INVALID));
        }
        
        final List<PointOfSale> posList = super.pointOfSaleService.findPointOfSalesByCustomerId(webUser.getCustomerId());
        return getStatusResponse(posList);
    }
    
    private PaystationStatusResponse getStatusResponse(final List<PointOfSale> posList) {
        final PaystationStatusResponse response = new PaystationStatusResponse();
        
        for (PointOfSale pointOfSale : posList) {
            final PosSensorState sensorState = super.pointOfSaleService.findPosSensorStateByPointOfSaleId(pointOfSale.getId());
            final List<PosEventCurrent> pStatusEvents = this.posEventCurrentService.findActivePosEventCurrentByPointOfSaleId(pointOfSale.getId());
            final PosHeartbeat lastHeartBeatGmt = super.pointOfSaleService.findPosHeartbeatByPointOfSaleId(pointOfSale.getId());
            final PaystationStatusType paystationStatusType = convertToStatusType(sensorState, pStatusEvents, lastHeartBeatGmt.getLastHeartbeatGmt());
            paystationStatusType.setSerialNumber(pointOfSale.getSerialNumber());
            response.getPaystationstatus().add(paystationStatusType);
        }
        return response;
        
    }
    
    /**
     * Find all pay station status by region name,
     * 
     * which may include multiple events for each paystation
     * 
     * @param PaystationStatusByRegionRequest
     * 
     * @return PaystationStatusResponse
     * 
     * @since v6.2.1
     */
    public final PaystationStatusResponse getPaystationStatusByRegion(final PaystationStatusByRegionRequest request) throws InfoServiceFaultMessage {
        final Authentication auth = authorize(request.getToken());
        if (auth == null) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_STATUS_BY_REGION_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        
        // verify input parameters
        if (request.getToken() == null || request.getRegionName() == null) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_STATUS_BY_REGION_FAILURE,
                    createInfoServiceFault(MESSAGE_INPUT_PARAMETERS_MISSING_OR_INVALID));
        }
        
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_STATUS_BY_REGION_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        // check the token
        if (!isTokenValid(request.getToken(), webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_STATUS_BY_REGION_FAILURE, createInfoServiceFault(MESSAGE_TOKEN_INVALID));
        }
        
        final int locationId = super.findLocationIdOfRequestLocation(webUser.getCustomerId(), request.getRegionName());
        
        if (locationId > 0) {
            final List<PointOfSale> posList = super.pointOfSaleService.findPointOfSalesByLocationId(locationId);
            return getStatusResponse(posList);
        }
        return new PaystationStatusResponse();
    }
    
    /**
     * Find all pay station status by serial number,
     * 
     * which may include multiple events for each paystation
     * 
     * @param PaystationStatusBySerialRequest
     * 
     * @return PaystationStatusResponse
     * 
     * @since v6.2.1
     */
    public final PaystationStatusResponse getPaystationStatusBySerial(final PaystationStatusBySerialRequest request) throws InfoServiceFaultMessage {
        final Authentication auth = authorize(request.getToken());
        if (auth == null) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_STATUS_BY_SERIAL_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        
        // verify input parameters
        if (request.getToken() == null || request.getPaystationstatus().isEmpty()) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_STATUS_BY_SERIAL_FAILURE,
                    createInfoServiceFault(MESSAGE_INPUT_PARAMETERS_MISSING_OR_INVALID));
        }
        
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_STATUS_BY_SERIAL_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        // check the token
        if (!isTokenValid(request.getToken(), webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_STATUS_BY_SERIAL_FAILURE, createInfoServiceFault(MESSAGE_TOKEN_INVALID));
        }
        final List<PaystationSerialListType> paystationSerialList = request.getPaystationstatus();
        final List<String> serialNumberList = new ArrayList<String>();
        for (PaystationSerialListType pssList : paystationSerialList) {
            serialNumberList.add(pssList.getSerialNumber());
        }
        
        final List<PointOfSale> posList =
                super.pointOfSaleService.findPointOfSalesBySerialNumberAndCustomerId(webUser.getCustomerId(), serialNumberList);
        
        return getStatusResponse(posList);
    }
    
    /**
     * Find all pay station status based on group name,
     * 
     * which may include multiple events for each paystation
     * 
     * @param PaystationStatusByGroupRequest
     * 
     * @return PaystationStatusResponse
     * 
     * @since v6.2.1
     */
    public final PaystationStatusResponse getPaystationStatusByGroup(final PaystationStatusByGroupRequest request) throws InfoServiceFaultMessage {
        final Authentication auth = authorize(request.getToken());
        if (auth == null) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_STATUS_BY_GROUP_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        
        // verify input parameters
        if (request.getToken() == null || request.getGroupName() == null || request.getGroupName().trim().equals(WebCoreConstants.EMPTY_STRING)) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_STATUS_BY_GROUP_FAILURE,
                    createInfoServiceFault(MESSAGE_INPUT_PARAMETERS_MISSING_OR_INVALID));
        }
        
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_STATUS_BY_GROUP_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        // check the token
        if (!isTokenValid(request.getToken(), webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_STATUS_BY_GROUP_FAILURE, createInfoServiceFault(MESSAGE_TOKEN_INVALID));
        }
        
        final List<PointOfSale> posList = getPosListByRouteName(webUser.getCustomerId(), request.getGroupName().trim());
        return getStatusResponse(posList);
    }
    
    @SuppressWarnings({ "checkstyle:cyclomaticcomplexity", })
    private PaystationStatusType convertToStatusType(final PosSensorState sensorState, final List<PosEventCurrent> pStatusEvents,
        final Date lastHeartBeatGmt) {
        
        int severity = 0;
        
        final PaystationStatusType paystationStatusType = new PaystationStatusType();
        if (sensorState.getBattery1Voltage().equals(WebCoreConstants.BATTERY_N_A_VOLTAGE_DEFAULT)) {
            paystationStatusType.setBattery1Voltage(WebCoreConstants.BATTERY_N_A_VOLTAGE_ZERO);
        } else {
            paystationStatusType.setBattery1Voltage(sensorState.getBattery1Voltage());
        }
        if (sensorState.getBattery2Voltage().equals(WebCoreConstants.BATTERY_N_A_VOLTAGE_DEFAULT)) {
            paystationStatusType.setBattery2Voltage(WebCoreConstants.BATTERY_N_A_VOLTAGE_ZERO);
        } else {
            paystationStatusType.setBattery2Voltage(sensorState.getBattery2Voltage());
        }
        
        paystationStatusType.setLastHeartBeat(lastHeartBeatGmt);
        
        if (pStatusEvents != null && !pStatusEvents.isEmpty()) {
            PaystationStatusEventType sEventType;
            for (PosEventCurrent posEventCurrent : pStatusEvents) {
                sEventType = new PaystationStatusEventType();
                
                severity = (severity < posEventCurrent.getEventSeverityType().getId()) ? posEventCurrent.getEventSeverityType().getId() : severity;
                
                sEventType.setEventSeverity(posEventCurrent.getEventSeverityType().getName());
                sEventType.setEventTimeStamp(posEventCurrent.getAlertGmt());
                
                final String alarmMsg = posEventCurrent.getAlertInfo();
                sEventType.setEventDetail(alarmMsg);
                
                paystationStatusType.getPaystationStatusEvent().add(sEventType);
                
                if (posEventCurrent.getEventType().getId() == WebCoreConstants.EVENT_TYPE_PAYSTATION_SERVICEMODE
                    && posEventCurrent.getEventSeverityType().getId() == WebCoreConstants.SEVERITY_MINOR) {
                    paystationStatusType.setIsInServiceMode(true);
                }
                if ((posEventCurrent.getEventType().getId() == WebCoreConstants.EVENT_TYPE_DOOR_OPEN
                     || posEventCurrent.getEventType().getId() == WebCoreConstants.EVENT_TYPE_DOORLOWER_OPEN
                     || posEventCurrent.getEventType().getId() == WebCoreConstants.EVENT_TYPE_DOORUPPER_OPEN)
                    && posEventCurrent.getEventSeverityType().getId() == 1) {
                    paystationStatusType.setIsDoorOpen(true);
                    sEventType.setEventDetail(WebCoreConstants.EVENT_TYPE_PAYSTATION + ":" + WebCoreConstants.EVENT_ACTION_DOOR_OPENED_STRING);
                    
                }
                
                if (sEventType.getEventDetail() == null) {
                    sEventType.setEventDetail(WebCoreConstants.EMPTY_STRING);
                }
            }
        }
        paystationStatusType.setAlarmState(this.eventSeverityTypeService.findEventSeverityType(severity).getName());
        return paystationStatusType;
    }
    
    public final PaystationEventsResponse getPaystationEvents(final PaystationEventsRequest request) throws InfoServiceFaultMessage {
        final Authentication auth = authorize(request.getToken());
        if (auth == null) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_EVENTS_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        // verify input parameters
        if (request.getToken() == null || request.getEventType() == null) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_EVENTS_FAILURE, createInfoServiceFault(MESSAGE_INPUT_PARAMETERS_MISSING_OR_INVALID));
        }
        
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_EVENTS_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        // check the token
        if (!isTokenValid(request.getToken(), webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_EVENTS_FAILURE, createInfoServiceFault(MESSAGE_TOKEN_INVALID));
        }
        // Check fromDate and toDate,
        final Date dateFrom = request.getDateFrom();
        final Date dateTo = request.getDateTo();
        checkFromToDate(dateFrom, dateTo, GET_PAYSTATION_EVENTS);
        
        final String version = verifyVersion(request.getVersion());
        
        final int customerId = webUser.getCustomerId();
        final List<PointOfSale> posList = super.pointOfSaleService.findPointOfSalesByCustomerId(customerId, true);
        
        final String eventType = request.getEventType().value();
        return getEventResponse(posList, eventType, dateFrom, dateTo, true, version, webUser);
    }
    
    @SuppressWarnings("checkstyle:cyclomaticcomplexity")
    private PaystationEventsResponse getEventResponse(final List<PointOfSale> posList, final String eventType, final Date dateFrom, final Date dateTo,
        final boolean isAll, final String version, final WebUser webUser) throws InfoServiceFaultMessage {
        final PaystationEventsResponse response = new PaystationEventsResponse();
        if (posList != null && !posList.isEmpty()) {
            final Map<Integer, String> posMap = new HashMap<Integer, String>();
            // save paystationid and serial number pair for convertToEventsType() use
            for (PointOfSale pos : posList) {
                posMap.put(Integer.valueOf(pos.getId()), pos.getSerialNumber());
            }
            
            // get battery voltages if eventtype = "All" && "Battery"
            if (isAll && eventType.equals(WebCoreConstants.ALL_STRING) || eventType.equals(WebCoreConstants.BATTERY)) {
                addBatteryVoltageData(response, dateFrom, dateTo, posMap);
            }
            if (isAll && version.equals(WS_VERSION_1_0) && LUKEII_EVENT_MAP.get(eventType.trim()) != null) {
                LOGGER.warn("+++ " + webUser.getUsername() + " tried to get LUKEII events, but ws version is not " + WS_VERSION_1_1_LUKEII
                            + SPACE_AND_THREE_PLUSES);
                throw new InfoServiceFaultMessage(GET_PAYSTATION_EVENTS_FAILURE, createInfoServiceFault(MESSAGE_VERSION_INVALID));
            }
            
            // get events
            final List<PaystationEventInfo> eventList = this.paystationEventInfoService
                    .findEventsforPosList(posMap.keySet(), getDeviceIdsForType(eventType), dateFrom, dateTo, MAX_EVENTS_NUMBER);
            
            for (PaystationEventInfo event : eventList) {
                final PaystationEventsType paystationEventsType =
                        convertToEventsType(event, posMap.get(Integer.valueOf(event.getPointOfSaleId())), version);
                if (paystationEventsType != null) {
                    if (paystationEventsType.getInformation() == null) {
                        paystationEventsType.setInformation("");
                    }
                    response.getPaystationevents().add(paystationEventsType);
                }
            }
        }
        return response;
        
    }
    
    /**
     * @param version
     * @return
     * @throws InfoServiceFaultMessage
     */
    private String verifyVersion(final VersionType versionT) throws InfoServiceFaultMessage {
        String version = "";
        if (versionT == null || versionT.value().trim().equals(WebCoreConstants.EMPTY_STRING)) {
            version = WS_VERSION_1_0;
        } else {
            version = versionT.value();
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("+++ version: " + version + SPACE_AND_THREE_PLUSES);
        }
        return version;
    }
    
    public final PaystationEventsResponse getPaystationEventsByRegion(final PaystationEventsByRegionRequest request) throws InfoServiceFaultMessage {
        final Authentication auth = authorize(request.getToken());
        if (auth == null) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_EVENTS_BY_REGION_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        
        // verify input parameters
        if (request.getToken() == null || request.getRegionName() == null) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_EVENTS_BY_REGION_FAILURE,
                    createInfoServiceFault(MESSAGE_INPUT_PARAMETERS_MISSING_OR_INVALID));
        }
        
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_EVENTS_BY_REGION_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        // check the token
        if (!isTokenValid(request.getToken(), webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_EVENTS_BY_REGION_FAILURE, createInfoServiceFault(MESSAGE_TOKEN_INVALID));
        }
        // Check fromDate and toDate,
        final Date dateFrom = request.getDateFrom();
        final Date dateTo = request.getDateTo();
        checkFromToDate(dateFrom, dateTo, GET_PAYSTATION_EVENTS);
        final String version = verifyVersion(request.getVersion());
        
        final int locationId = super.findLocationIdOfRequestLocation(webUser.getCustomerId(), request.getRegionName());
        final String eventType = WebCoreConstants.ALL_STRING;
        
        if (locationId > 0) {
            final List<PointOfSale> posList = super.pointOfSaleService.findPointOfSalesByLocationId(locationId);
            return getEventResponse(posList, eventType, dateFrom, dateTo, false, version, webUser);
        }
        
        return new PaystationEventsResponse();
        
    }
    
    public final PaystationEventsResponse getPaystationEventsBySerial(final PaystationEventsBySerialRequest request) throws InfoServiceFaultMessage {
        final Authentication auth = authorize(request.getToken());
        if (auth == null) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_EVENTS_BY_SERIAL_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        
        // verify input parameters
        if (request.getToken() == null || request.getPaystationevents().isEmpty()) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_EVENTS_BY_SERIAL_FAILURE,
                    createInfoServiceFault(MESSAGE_INPUT_PARAMETERS_MISSING_OR_INVALID));
        }
        
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_EVENTS_BY_SERIAL_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        // check the token
        if (!isTokenValid(request.getToken(), webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_EVENTS_BY_SERIAL_FAILURE, createInfoServiceFault(MESSAGE_TOKEN_INVALID));
        }
        
        // Check fromDate and toDate,
        final Date dateFrom = request.getDateFrom();
        final Date dateTo = request.getDateTo();
        checkFromToDate(dateFrom, dateTo, GET_PAYSTATION_EVENTS);
        final String version = verifyVersion(request.getVersion());
        
        final List<PaystationSerialListType> paystationSerialList = request.getPaystationevents();
        final List<String> serialNumberList = new ArrayList<String>();
        for (PaystationSerialListType pssList : paystationSerialList) {
            serialNumberList.add(pssList.getSerialNumber());
        }
        final String eventType = WebCoreConstants.ALL_STRING;
        // get paystations
        final List<PointOfSale> posList =
                super.pointOfSaleService.findPointOfSalesBySerialNumberAndCustomerId(webUser.getCustomerId(), serialNumberList);
        return getEventResponse(posList, eventType, dateFrom, dateTo, false, version, webUser);
        
    }
    
    /**
     * Find all pay station events based on group name,
     * 
     * 
     * @param request
     *            PaystationEventsByGroupRequest
     * 
     * @return PaystationEventsResponse
     * 
     * @since v6.2.1
     */
    public final PaystationEventsResponse getPaystationEventsByGroup(final PaystationEventsByGroupRequest request) throws InfoServiceFaultMessage {
        final Authentication auth = authorize(request.getToken());
        if (auth == null) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_EVENTS_BY_GROUP_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        
        // verify input parameters
        if (request.getToken() == null || request.getGroupName() == null || request.getGroupName().trim().equals(WebCoreConstants.EMPTY_STRING)) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_EVENTS_BY_GROUP_FAILURE,
                    createInfoServiceFault(MESSAGE_INPUT_PARAMETERS_MISSING_OR_INVALID));
        }
        
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_EVENTS_BY_GROUP_FAILURE, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        // check the token
        if (!isTokenValid(request.getToken(), webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(GET_PAYSTATION_EVENTS_BY_GROUP_FAILURE, createInfoServiceFault(MESSAGE_TOKEN_INVALID));
        }
        // Check fromDate and toDate,
        final Date dateFrom = request.getDateFrom();
        final Date dateTo = request.getDateTo();
        checkFromToDate(dateFrom, dateTo, GET_PAYSTATION_EVENTS);
        final String version = verifyVersion(request.getVersion());
        final String eventType = WebCoreConstants.ALL_STRING;
        final List<PointOfSale> posList = getPosListByRouteName(webUser.getCustomerId(), request.getGroupName().trim());
        
        return getEventResponse(posList, eventType, dateFrom, dateTo, false, version, webUser);
    }
    
    private void addBatteryVoltageData(final PaystationEventsResponse response, final Date fromDate, final Date toDate,
        final Map<Integer, String> posMap) {
        final List<PosSensorInfo> sensorInfoList = this.posSensorInfoService
                .findPosSensorInfoByPointOfSaleIdAndTypeIdAndDateRange(posMap.keySet(), WebCoreConstants.SENSOR_INFO_BATTERY_VOLTAGE, fromDate,
                                                                       toDate, MAX_EVENTS_NUMBER);
        
        for (PosSensorInfo sensorInfo : sensorInfoList) {
            final PaystationEventsType paystationEventsType =
                    convertToEventsTypeByBatteryLog(sensorInfo, posMap.get(Integer.valueOf(sensorInfo.getPointOfSale().getId())));
            response.getPaystationevents().add(paystationEventsType);
        }
    }
    
    @SuppressWarnings({ "checkstyle:cyclomaticcomplexity", })
    private PaystationEventsType convertToEventsType(final PaystationEventInfo event, final String psSerialNumber, final String version) {
        if (version.equals(WS_VERSION_1_0)) {
            if (LUKEII_EVENT_MAP.get(getEventType(event).trim()) != null) {
                return null;
            }
            
            if (isLukeIIDevice(event.getEventDeviceTypeId())) {
                return null;
            }
        }
        final PaystationEventsType paystationEventsType = new PaystationEventsType();
        paystationEventsType.setSerialNumber(psSerialNumber);
        paystationEventsType.setType(getEventType(event));
        final String actionEvent = getEventAction(event, version);
        if (actionEvent != null) {
            paystationEventsType.setAction(actionEvent);
        }
        paystationEventsType.setSeverity(WebCoreConstants.N_A_STRING.equals(event.getSeverityName()) ? "Clear" : event.getSeverityName());
        
        if (paystationEventsType.getAction() != null && paystationEventsType.getAction().equals(WebCoreConstants.EVENT_ACTION_DOOR_CLOSED_STRING)
            || paystationEventsType.getAction().equals(WebCoreConstants.EVENT_ACTION_DOOR_OPENED_STRING)) {
            if (TransactionInfoServiceUtil.isEqualOrGreaterTo(WS_VERSION_1_1_LUKEII, version.trim())) {
                if (event.getEventDeviceTypeId() == WebCoreConstants.EVENT_DEVICE_TYPE_CASH_VAULT_DOOR) {
                    paystationEventsType.setInformation(WebCoreConstants.INFO_CASH_VAULT);
                } else if (event.getEventDeviceTypeId() == WebCoreConstants.EVENT_DEVICE_TYPE_MAINTENANCE_DOOR) {
                    paystationEventsType.setInformation(WebCoreConstants.INFO_MAINTENANCE);
                } else {
                    paystationEventsType.setInformation(N_A_IN_BRACKETS);
                }
            } else {
                paystationEventsType.setInformation(N_A_IN_BRACKETS);
            }
        } else {
            paystationEventsType.setInformation(event.getAlertInfo());
        }
        paystationEventsType.setTimeStamp(event.getAlertGmt());
        return paystationEventsType;
    }
    
    private PaystationEventsType convertToEventsTypeByBatteryLog(final PosSensorInfo sensorInfo, final String posSerialNumber) {
        final PaystationEventsType paystationEventsType = new PaystationEventsType();
        paystationEventsType.setSerialNumber(posSerialNumber);
        paystationEventsType.setType(WebCoreConstants.BATTERY);
        paystationEventsType.setAction(WebCoreConstants.EMPTY_STRING);
        final String severity = sensorInfo.getAmount() <= WebCoreConstants.BATTERY_LOW_VOLTAGE_THRESHOLD ? WebCoreConstants.BATTERY_LOW
                : WebCoreConstants.BATTERY_NORMAL;
        paystationEventsType.setSeverity(severity);
        paystationEventsType.setInformation((sensorInfo.getSensorInfoType().getId() == 0 ? 1 : 2) + ": " + sensorInfo.getAmount());
        paystationEventsType.setTimeStamp(sensorInfo.getSensorGmt());
        return paystationEventsType;
    }
    
    private void checkFromToDate(final Date fromDate, final Date toDate, final String methodName) throws InfoServiceFaultMessage {
        // date is null?
        if (fromDate == null || toDate == null) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE,
                    createInfoServiceFault(MESSAGE_DATE_PARAMETER_MISSING_OR_INVALID, new Object[] { DATE_FROM_OR_DATE_TO }));
        } else if (fromDate.equals(WebCoreConstants.EMPTY_DATE) || toDate.equals(WebCoreConstants.EMPTY_DATE)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE,
                    createInfoServiceFault(MESSAGE_DATE_PARAMETER_MISSING_OR_INVALID, new Object[] { DATE_FROM_OR_DATE_TO }));
        }
        if (toDate.before(fromDate)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE,
                    createInfoServiceFault(MESSAGE_RANGE_INVALID, new Object[] { DATE_FROM, DATE_TO, }));
        }
        final String fromDay = DateUtil.createDateOnlyString(fromDate);
        final String toDay = DateUtil.createDateOnlyString(toDate);
        if (!fromDay.equals(toDay)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE,
                    createInfoServiceFault(MESSAGE_ONEDAY_RANGE, new Object[] { DATE_FROM, DATE_TO, }));
        }
    }
    
    private boolean isLukeIIDevice(final int deviceId) {
        return deviceId == WebCoreConstants.EVENT_DEVICE_TYPE_COIN_CANISTER || deviceId == WebCoreConstants.EVENT_DEVICE_TYPE_MAINTENANCE_DOOR
               || deviceId == WebCoreConstants.EVENT_DEVICE_TYPE_CASH_VAULT_DOOR || deviceId == WebCoreConstants.EVENT_DEVICE_TYPE_COIN_ESCROW;
    }
}
