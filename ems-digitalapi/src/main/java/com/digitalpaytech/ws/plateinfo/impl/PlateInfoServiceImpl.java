package com.digitalpaytech.ws.plateinfo.impl;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.CustomerProperty;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.dto.paystation.LicencePlateInfo;
import com.digitalpaytech.service.CustomerAdminService;
import com.digitalpaytech.service.paystation.LicencePlateInfoService;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.ws.BaseWsServiceImpl;
import com.digitalpaytech.ws.plateinfo.ExpiredPlateInfoByGroupRequest;
import com.digitalpaytech.ws.plateinfo.ExpiredPlateInfoByRegionRequest;
import com.digitalpaytech.ws.plateinfo.ExpiredPlateInfoRequest;
import com.digitalpaytech.ws.plateinfo.GroupRequest;
import com.digitalpaytech.ws.plateinfo.GroupResponse;
import com.digitalpaytech.ws.plateinfo.GroupType;
import com.digitalpaytech.ws.plateinfo.InfoServiceFault;
import com.digitalpaytech.ws.plateinfo.InfoServiceFaultMessage;
import com.digitalpaytech.ws.plateinfo.PlateInfoByGroupRequest;
import com.digitalpaytech.ws.plateinfo.PlateInfoByPlateRequest;
import com.digitalpaytech.ws.plateinfo.PlateInfoByPlateResponse;
import com.digitalpaytech.ws.plateinfo.PlateInfoByRegionRequest;
import com.digitalpaytech.ws.plateinfo.PlateInfoRequest;
import com.digitalpaytech.ws.plateinfo.PlateInfoResponse;
import com.digitalpaytech.ws.plateinfo.PlateInfoService;
import com.digitalpaytech.ws.plateinfo.PlateInfoType;
import com.digitalpaytech.ws.plateinfo.RegionRequest;
import com.digitalpaytech.ws.plateinfo.RegionResponse;
import com.digitalpaytech.ws.plateinfo.RegionType;
import com.digitalpaytech.ws.plateinfo.StatusType;
import com.digitalpaytech.ws.plateinfo.UpdatedPlateInfoByGroupRequest;
import com.digitalpaytech.ws.plateinfo.UpdatedPlateInfoByRegionRequest;
import com.digitalpaytech.ws.plateinfo.UpdatedPlateInfoRequest;

@Component("plateInfoService")
@WebService(serviceName = "PlateInfoService", portName = "PlateInfohttpPort", targetNamespace = "http://ws.digitalpaytech.com/plateInfo", endpointInterface = "com.digitalpaytech.ws.plateinfo.PlateInfoService")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
@SuppressWarnings({ "PMD.GodClass", "PMD.ExcessiveImports", "PMD.AvoidInstantiatingObjectsInLoops" })
public class PlateInfoServiceImpl extends BaseWsServiceImpl implements PlateInfoService {
    
    private static final String GETVALID_UPDATEDPLATES_BYGROUP_FAILURE = "getUpdatedValidPlatesByGroup Failure";
    private static final String GETVALID_PLATES_FAILURE = "getValidPlates Failure";
    private static final String GETVALID_UPDATEDPLATES_BYREGION_FAILURE = "getUpdatedValidPlatesByRegion Failure";
    private static final String GETVALID_PLATES_BYREGION_FAILURE = "getValidPlatesByRegion Failure";
    private static final String GET_PLATE_INFO_FAILURE = "getPlateInfo Failure";
    private static final String GETVALID_UPDATEDPLATES_FAILURE = "getUpdatedValidPlates Failure";
    private static final String GETEXPIRED_PLATES_BYREGION_FAILURE = "getExpiredPlatesByRegion Failure";
    private static final String GETEXPIRED_PLATES_FAILURE = "getExpiredPlates Failure";
    private static final String GETVALID_PLATES_BYGROUP_FAILURE = "getValidPlatesByGroup Failure";
    private static final String GETEXPIRED_PLATES_BYGROUP_FAILURE = "getExpiredPlatesByGroup Failure";
    private static final String GETGROUPS_FAILURE = "getGroups Failure";
    private static final String GETLOCATIONS_FAILURE = "getLocations Failure";
    private static final String ALLOWABLE_CHARS = "letters, digits, space, dot, comma, dash";
    private static final String LASTCALLDATE = "lastCallDate";
    private static final String GROUPNAME = "groupName";
    private static final String REGION = "region";
    
    @Autowired
    private LicencePlateInfoService licencePlateInfoService;
    
    @Autowired
    private CustomerAdminService customerAdminService;
    
    public PlateInfoServiceImpl() {
        super();
        super.webServiceEndPointType = WebCoreConstants.END_POINT_ID_PLATE_INFO;
    }
    
    public final void setLicencePlateInfoService(final LicencePlateInfoService licencePlateInfoService) {
        this.licencePlateInfoService = licencePlateInfoService;
    }
    
    public final LicencePlateInfoService getLicencePlateInfoService() {
        return this.licencePlateInfoService;
    }
    
    protected final InfoServiceFault createInfoServiceFault(final String message) {
        final InfoServiceFault fault = new InfoServiceFault();
        fault.setShortErrorMessage(message);
        return fault;
    }
    
    protected final InfoServiceFault createInfoServiceFault(final String message, final Object... args) {
        final InfoServiceFault fault = new InfoServiceFault();
        fault.setShortErrorMessage(MessageFormat.format(message, args));
        return fault;
    }
    
    private PlateInfoType convertToSchemaType(final LicencePlateInfo plateInfo, final boolean isValid) {
        if (plateInfo == null) {
            return null;
        } else {
            final PlateInfoType schemaType = new PlateInfoType();
            schemaType.setExpiryDate(plateInfo.getPermitExpireGmt());
            schemaType.setPlateNumber(plateInfo.getLicencePlateNumber());
            schemaType.setPurchasedDate(plateInfo.getPermitBeginGmt());
            schemaType.setRegionName(plateInfo.getLocationName());
            schemaType.setStatus(isValid ? StatusType.VALID : StatusType.EXPIRED);
            return schemaType;
        }
    }
    
    /*
     * (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getUpdatedValidPlatesByGroup(com.digitalpaytech.ws.plateinfo.UpdatedPlateInfoByGroupRequest
     * updatedPlateInfoByGroupRequest )*
     */
    public final PlateInfoResponse getUpdatedValidPlatesByGroup(final UpdatedPlateInfoByGroupRequest updatedPlateInfoByGroupRequest)
        throws InfoServiceFaultMessage {
        
        // check the purchased from date
        final Date lastModifiedGmt = updatedPlateInfoByGroupRequest.getLastCallDate();
        
        // error date format, throws exception
        if (lastModifiedGmt == null) {
            throw new InfoServiceFaultMessage(GETVALID_UPDATEDPLATES_BYGROUP_FAILURE,
                    createInfoServiceFault(MESSAGE_DATE_PARAMETER_MISSING_OR_INVALID, new Object[] { LASTCALLDATE }));
        }
        
        // verify input parameters
        validateRouteName(updatedPlateInfoByGroupRequest.getGroupName(), "getUpdatedValidPlatesByGroup", GROUPNAME);
        
        final int customerId = this.validateSecurity(updatedPlateInfoByGroupRequest.getToken(), GETVALID_UPDATEDPLATES_BYGROUP_FAILURE);
        final CustomerProperty custProp = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_QUERY_SPACES_BY);
        // Use default if 'querySpaceBy' is not defined.
        int querySpaceBy = PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME;
        if (custProp != null) {
            querySpaceBy = Integer.parseInt(custProp.getPropertyValue());
        }
        
        final PlateInfoResponse response = new PlateInfoResponse();
        
        final List<PointOfSale> posList = getPosListByRouteName(customerId, updatedPlateInfoByGroupRequest.getGroupName().trim());
        if (posList != null && !posList.isEmpty()) {
            final List<LicencePlateInfo> plateInfoList = this.licencePlateInfoService.getValidPlateListFromPurchaseTimeByPosList(customerId, posList,
                                                                                                                                 lastModifiedGmt,
                                                                                                                                 querySpaceBy);
            if (plateInfoList != null && !plateInfoList.isEmpty()) {
                for (LicencePlateInfo info : plateInfoList) {
                    final PlateInfoType schemaType = convertToSchemaType(info, true);
                    if (schemaType != null) {
                        response.getPlateInfos().add(schemaType);
                    }
                }
            }
        }
        return response;
    }
    
    /*
     * (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getValidPlates(com.digitalpaytech.ws.plateinfo.PlateInfoRequest
     * plateInfoRequest )*
     */
    public final PlateInfoResponse getValidPlates(final PlateInfoRequest plateInfoRequest) throws InfoServiceFaultMessage {
        
        final int gracePeriod = getGracePeriod(plateInfoRequest.getGracePeriod());
        this.validateGracePeriod(gracePeriod, GETVALID_PLATES_FAILURE);
        final int customerId = this.validateSecurity(plateInfoRequest.getToken(), GETVALID_PLATES_FAILURE);
        final CustomerProperty custProp = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_QUERY_SPACES_BY);
        // Use default if 'querySpaceBy' is not defined.
        int querySpaceBy = PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME;
        if (custProp != null) {
            querySpaceBy = Integer.parseInt(custProp.getPropertyValue());
        }
        
        final List<LicencePlateInfo> plateInfoList = this.licencePlateInfoService.getValidPlateListByCustomer(customerId, querySpaceBy, gracePeriod);
        final PlateInfoResponse response = new PlateInfoResponse();
        if (plateInfoList != null && !plateInfoList.isEmpty()) {
            final Date currentDate = new Date();
            for (LicencePlateInfo info : plateInfoList) {
                final PlateInfoType schemaType = convertToSchemaType(info, true);
                if (info.getPermitExpireGmt().before(currentDate)) {
                    schemaType.setStatus(StatusType.EXPIRED);
                }
                if (schemaType != null) {
                    response.getPlateInfos().add(schemaType);
                }
            }
        }
        return response;
    }
    
    /*
     * (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getUpdatedValidPlatesByRegion(com.digitalpaytech.ws.plateinfo.UpdatedPlateInfoByRegionRequest
     * updatedPlateInfoByRegionRequest )*
     */
    public final PlateInfoResponse getUpdatedValidPlatesByRegion(final UpdatedPlateInfoByRegionRequest updatedPlateInfoByRegionRequest)
        throws InfoServiceFaultMessage {
        
        // check the purchased from date
        final Date lastModifiedGmt = updatedPlateInfoByRegionRequest.getLastCallDate();
        
        // error date format, throws exception
        if (lastModifiedGmt == null) {
            throw new InfoServiceFaultMessage(GETVALID_UPDATEDPLATES_BYREGION_FAILURE,
                    createInfoServiceFault(MESSAGE_DATE_PARAMETER_MISSING_OR_INVALID, new Object[] { LASTCALLDATE }));
        }
        
        // verify input parameters
        String region = updatedPlateInfoByRegionRequest.getRegion();
        validateLocationName(region, "getUpdatedValidPlatesByRegion", REGION);
        region = region.trim();
        
        final int customerId = this.validateSecurity(updatedPlateInfoByRegionRequest.getToken(), GETVALID_UPDATEDPLATES_BYREGION_FAILURE);
        final int locationId = findLocationIdOfRequestLocation(customerId, updatedPlateInfoByRegionRequest.getRegion());
        
        final PlateInfoResponse response = new PlateInfoResponse();
        
        if (locationId > 0) {
            final List<LicencePlateInfo> plateInfoList = this.licencePlateInfoService.getValidPlateListFromPurchaseTimeByLocation(customerId,
                                                                                                                                  locationId,
                                                                                                                                  lastModifiedGmt);
            if (plateInfoList != null && !plateInfoList.isEmpty()) {
                for (LicencePlateInfo info : plateInfoList) {
                    final PlateInfoType schemaType = convertToSchemaType(info, true);
                    if (schemaType != null) {
                        response.getPlateInfos().add(schemaType);
                    }
                }
            }
        }
        return response;
    }
    
    /*
     * (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getValidPlatesByRegion(com.digitalpaytech.ws.plateinfo.PlateInfoByRegionRequest
     * plateInfoByRegionRequest )*
     */
    public final PlateInfoResponse getValidPlatesByRegion(final PlateInfoByRegionRequest plateInfoByRegionRequest) throws InfoServiceFaultMessage {
        
        // verify input parameters
        String region = plateInfoByRegionRequest.getRegion();
        this.validateLocationName(region, "getValidPlatesByRegion", REGION);
        region = region.trim();
        
        final int customerId = this.validateSecurity(plateInfoByRegionRequest.getToken(), GETVALID_PLATES_BYREGION_FAILURE);
        final int locationId = findLocationIdOfRequestLocation(customerId, plateInfoByRegionRequest.getRegion());
        
        final PlateInfoResponse response = new PlateInfoResponse();
        
        if (locationId > 0) {
            final int gracePeriod = getGracePeriod(plateInfoByRegionRequest.getGracePeriod());
            this.validateGracePeriod(gracePeriod, GETVALID_PLATES_BYREGION_FAILURE);
            
            final List<LicencePlateInfo> plateInfoList = this.licencePlateInfoService
                    .getValidPlateListByLocation(customerId, locationId, gracePeriod);
            if (plateInfoList != null && !plateInfoList.isEmpty()) {
                final Date currentDate = new Date();
                for (LicencePlateInfo info : plateInfoList) {
                    final PlateInfoType schemaType = convertToSchemaType(info, true);
                    if (info.getPermitExpireGmt().before(currentDate)) {
                        schemaType.setStatus(StatusType.EXPIRED);
                    }
                    if (schemaType != null) {
                        response.getPlateInfos().add(schemaType);
                    }
                }
            }
        }
        return response;
    }
    
    /*
     * (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getPlateInfo(com.digitalpaytech.ws.plateinfo.PlateInfoByPlateRequest
     * plateInfoByPlateRequest )*
     */
    public final PlateInfoByPlateResponse getPlateInfo(final PlateInfoByPlateRequest plateInfoByPlateRequest) throws InfoServiceFaultMessage {
        
        // verify input parameters
        String plateNumber = plateInfoByPlateRequest.getPlateNumber();
        if (plateNumber != null) {
            // keep letters and digits only
            plateNumber = plateNumber.replaceAll(WebCoreConstants.REGEX_NON_ALPHANUMERIC, WebCoreConstants.EMPTY_STRING);
        }
        if (plateNumber == null || plateNumber.isEmpty()) {
            throw new InfoServiceFaultMessage(GET_PLATE_INFO_FAILURE, createInfoServiceFault(MESSAGE_PARAMETER_MISSING_OR_INVALID,
                                                                                             new Object[] { "plateNumber" }));
        }
        
        final int customerId = this.validateSecurity(plateInfoByPlateRequest.getToken(), GET_PLATE_INFO_FAILURE);
        final CustomerProperty custProp = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_QUERY_SPACES_BY);
        // Use default if 'querySpaceBy' is not defined.
        int querySpaceBy = PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME;
        if (custProp != null) {
            querySpaceBy = Integer.parseInt(custProp.getPropertyValue());
        }
        
        final PlateInfoByPlateResponse response = new PlateInfoByPlateResponse();
        
        final LicencePlateInfo plateInfo = this.licencePlateInfoService.getValidPlateByCustomerAndPlate(customerId, plateNumber, querySpaceBy);
        if (plateInfo != null) {
            final PlateInfoType schemaType = convertToSchemaType(plateInfo, new Date().after(plateInfo.getPermitExpireGmt()) ? false : true);
            if (schemaType != null) {
                response.setPlateInfo(schemaType);
            }
        }
        
        return response;
    }
    
    /*
     * (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getUpdatedValidPlates(com.digitalpaytech.ws.plateinfo.UpdatedPlateInfoRequest
     * updatedPlateInfoRequest )*
     */
    public final PlateInfoResponse getUpdatedValidPlates(final UpdatedPlateInfoRequest updatedPlateInfoRequest) throws InfoServiceFaultMessage {
        
        // check the purchased from date
        final Date lastModifiedGmt = updatedPlateInfoRequest.getLastCallDate();
        
        // error date format, throws exception
        if (lastModifiedGmt == null) {
            throw new InfoServiceFaultMessage(GETVALID_UPDATEDPLATES_FAILURE, createInfoServiceFault(MESSAGE_DATE_PARAMETER_MISSING_OR_INVALID,
                                                                                                     new Object[] { LASTCALLDATE }));
        }
        
        final int customerId = this.validateSecurity(updatedPlateInfoRequest.getToken(), GETVALID_UPDATEDPLATES_FAILURE);
        final CustomerProperty custProp = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_QUERY_SPACES_BY);
        // Use default if 'querySpaceBy' is not defined.
        int querySpaceBy = PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME;
        if (custProp != null) {
            querySpaceBy = Integer.parseInt(custProp.getPropertyValue());
        }
        
        final List<LicencePlateInfo> plateInfoList = this.licencePlateInfoService.getValidPlateListFromPurchaseTimeByCustomer(customerId,
                                                                                                                              lastModifiedGmt,
                                                                                                                              querySpaceBy);
        final PlateInfoResponse response = new PlateInfoResponse();
        if (plateInfoList != null && !plateInfoList.isEmpty()) {
            for (LicencePlateInfo info : plateInfoList) {
                final PlateInfoType schemaType = convertToSchemaType(info, true);
                if (schemaType != null) {
                    response.getPlateInfos().add(schemaType);
                }
            }
        }
        return response;
    }
    
    /*
     * (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getExpiredPlatesByRegion(com.digitalpaytech.ws.plateinfo.ExpiredPlateInfoByRegionRequest
     * expiredPlateInfoByRegionRequest )*
     */
    public final PlateInfoResponse getExpiredPlatesByRegion(final ExpiredPlateInfoByRegionRequest expiredPlateInfoByRegionRequest)
        throws InfoServiceFaultMessage {
        
        // verify input parameters
        String region = expiredPlateInfoByRegionRequest.getRegion();
        this.validateLocationName(region, "getExpiredPlatesByRegion", REGION);
        region = region.trim();
        
        final int customerId = this.validateSecurity(expiredPlateInfoByRegionRequest.getToken(), GETEXPIRED_PLATES_BYREGION_FAILURE);
        final int locationId = findLocationIdOfRequestLocation(customerId, expiredPlateInfoByRegionRequest.getRegion());
        
        final PlateInfoResponse response = new PlateInfoResponse();
        
        if (locationId > 0) {
            final int gracePeriod = getGracePeriod(expiredPlateInfoByRegionRequest.getGracePeriod());
            this.validateGracePeriod(gracePeriod, GETEXPIRED_PLATES_BYREGION_FAILURE);
            final List<LicencePlateInfo> plateInfoList = this.licencePlateInfoService.getExpiredPlateListFromExpireTimeByLocation(customerId,
                                                                                                                                  locationId,
                                                                                                                                  gracePeriod);
            if (plateInfoList != null && !plateInfoList.isEmpty()) {
                final Date currentDate = new Date();
                for (LicencePlateInfo info : plateInfoList) {
                    final PlateInfoType schemaType = convertToSchemaType(info, false);
                    if (info.getPermitExpireGmt().after(currentDate)) {
                        schemaType.setStatus(StatusType.VALID);
                    }
                    if (schemaType != null) {
                        response.getPlateInfos().add(schemaType);
                    }
                }
            }
        }
        return response;
    }
    
    /*
     * (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getExpiredPlates(com.digitalpaytech.ws.plateinfo.ExpiredPlateInfoRequest
     * expiredPlateInfoRequest )*
     */
    public final PlateInfoResponse getExpiredPlates(final ExpiredPlateInfoRequest expiredPlateInfoRequest) throws InfoServiceFaultMessage {
        
        final PlateInfoResponse response = new PlateInfoResponse();
        final int gracePeriod = getGracePeriod(expiredPlateInfoRequest.getGracePeriod());
        this.validateGracePeriod(gracePeriod, GETEXPIRED_PLATES_FAILURE);
        final int customerId = this.validateSecurity(expiredPlateInfoRequest.getToken(), GETEXPIRED_PLATES_FAILURE);
        final List<LicencePlateInfo> plateInfoList = this.licencePlateInfoService
                .getExpiredPlateListFromExpireTimeByCustomer(customerId, gracePeriod);
        if (plateInfoList != null && !plateInfoList.isEmpty()) {
            final Date currentDate = new Date();
            for (LicencePlateInfo info : plateInfoList) {
                final PlateInfoType schemaType = convertToSchemaType(info, false);
                if (info.getPermitExpireGmt().after(currentDate)) {
                    schemaType.setStatus(StatusType.VALID);
                }
                if (schemaType != null) {
                    response.getPlateInfos().add(schemaType);
                }
            }
        }
        
        return response;
    }
    
    /*
     * (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getValidPlatesByGroup(com.digitalpaytech.ws.plateinfo.PlateInfoByGroupRequest
     * plateInfoByGroupRequest )*
     */
    public final PlateInfoResponse getValidPlatesByGroup(final PlateInfoByGroupRequest plateInfoByGroupRequest) throws InfoServiceFaultMessage {
        
        // verify input parameters
        validateRouteName(plateInfoByGroupRequest.getGroupName(), "getValidPlatesByGroup", GROUPNAME);
        
        final int customerId = this.validateSecurity(plateInfoByGroupRequest.getToken(), GETVALID_PLATES_BYGROUP_FAILURE);
        final CustomerProperty custProp = this.customerAdminService
                .getCustomerPropertyByCustomerIdAndCustomerPropertyType(customerId, WebCoreConstants.CUSTOMER_PROPERTY_TYPE_QUERY_SPACES_BY);
        // Use default if 'querySpaceBy' is not defined.
        int querySpaceBy = PaystationConstants.PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME;
        if (custProp != null) {
            querySpaceBy = Integer.parseInt(custProp.getPropertyValue());
        }
        
        final PlateInfoResponse response = new PlateInfoResponse();
        
        final List<PointOfSale> posList = getPosListByRouteName(customerId, plateInfoByGroupRequest.getGroupName().trim());
        if (posList != null && !posList.isEmpty()) {
            final int gracePeriod = getGracePeriod(plateInfoByGroupRequest.getGracePeriod());
            this.validateGracePeriod(gracePeriod, GETVALID_PLATES_BYGROUP_FAILURE);
            final List<LicencePlateInfo> plateInfoList = this.licencePlateInfoService.getValidPlateListByPosList(customerId, posList, querySpaceBy,
                                                                                                                 gracePeriod);
            if (plateInfoList != null && !plateInfoList.isEmpty()) {
                final Date currentDate = new Date();
                for (LicencePlateInfo info : plateInfoList) {
                    final PlateInfoType schemaType = convertToSchemaType(info, true);
                    if (info.getPermitExpireGmt().before(currentDate)) {
                        schemaType.setStatus(StatusType.EXPIRED);
                    }
                    if (schemaType != null) {
                        response.getPlateInfos().add(schemaType);
                    }
                }
            }
        }
        return response;
    }
    
    /*
     * (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getExpiredPlatesByGroup(com.digitalpaytech.ws.plateinfo.ExpiredPlateInfoByGroupRequest
     * expiredPlateInfoByGroupRequest )*
     */
    public final PlateInfoResponse getExpiredPlatesByGroup(final ExpiredPlateInfoByGroupRequest expiredPlateInfoByGroupRequest)
        throws InfoServiceFaultMessage {
        
        // verify input parameters
        validateRouteName(expiredPlateInfoByGroupRequest.getGroupName(), "getExpiredPlatesByGroup", GROUPNAME);
        
        final PlateInfoResponse response = new PlateInfoResponse();
        
        final int customerId = this.validateSecurity(expiredPlateInfoByGroupRequest.getToken(), GETEXPIRED_PLATES_BYGROUP_FAILURE);
        final List<PointOfSale> posList = getPosListByRouteName(customerId, expiredPlateInfoByGroupRequest.getGroupName().trim());
        if (posList != null && !posList.isEmpty()) {
            final int gracePeriod = getGracePeriod(expiredPlateInfoByGroupRequest.getGracePeriod());
            this.validateGracePeriod(gracePeriod, GETEXPIRED_PLATES_BYGROUP_FAILURE);
            final List<LicencePlateInfo> plateInfoList = this.licencePlateInfoService.getExpiredPlateListFromExpireTimeByPosList(customerId, posList,
                                                                                                                                 gracePeriod);
            if (plateInfoList != null && !plateInfoList.isEmpty()) {
                final Date currentDate = new Date();
                for (LicencePlateInfo info : plateInfoList) {
                    final PlateInfoType schemaType = convertToSchemaType(info, false);
                    if (info.getPermitExpireGmt().after(currentDate)) {
                        schemaType.setStatus(StatusType.VALID);
                    }
                    if (schemaType != null) {
                        response.getPlateInfos().add(schemaType);
                    }
                }
            }
        }
        return response;
    }
    
    /*
     * (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getGroups(com.digitalpaytech.ws.plateinfo.GroupRequest
     * groupRequest )*
     */
    public final GroupResponse getGroups(final GroupRequest groupRequest) throws InfoServiceFaultMessage {
        final int customerId = this.validateSecurity(groupRequest.getToken(), GETGROUPS_FAILURE);
        return getRoutes(customerId);
    }
    
    /*
     * (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getRegions(com.digitalpaytech.ws.plateinfo.RegionRequest
     * regionRequest )*
     */
    public final RegionResponse getRegions(final RegionRequest regionRequest) throws InfoServiceFaultMessage {
        final int customerId = this.validateSecurity(regionRequest.getToken(), GETLOCATIONS_FAILURE);
        return getLocations(customerId);
    }
    
    protected final GroupResponse getRoutes(final int customerId) {
        final Collection<Route> routeList = this.routeService.findRoutesByCustomerId(customerId);
        
        final GroupResponse response = new GroupResponse();
        GroupType groupType;
        for (Route route : routeList) {
            groupType = new GroupType();
            groupType.setGroupName(route.getName());
            response.getGroups().add(groupType);
        }
        return response;
    }
    
    protected final RegionResponse getLocations(final int customerId) {
        final List<Location> locationList = getLocationsWithPaystations(customerId);
        
        final RegionResponse response = new RegionResponse();
        
        for (Location location : locationList) {
            
            final RegionType regionType = new RegionType();
            
            regionType.setRegionName(location.getName());
            regionType.setDescription(location.getDescription());
            response.getRegions().add(regionType);
        }
        return response;
    }
    
    protected final void validateRouteName(final String groupName, final String methodName, final String label) throws InfoServiceFaultMessage {
        if (StringUtils.isEmpty(groupName)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_PARAMETER_MISSING_OR_INVALID,
                                                                                                 new Object[] { label }));
        }
        
        if (groupName.length() < WebCoreConstants.VALIDATION_MIN_LENGTH_1 || groupName.length() > WebCoreConstants.VALIDATION_MAX_LENGTH_25) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_INVALID_LENGTH, new Object[] { label,
                WebCoreConstants.VALIDATION_MIN_LENGTH_1, WebCoreConstants.VALIDATION_MAX_LENGTH_25, }));
        }
        
        if (!groupName.matches(WebCoreConstants.REGEX_STANDARD_TEXT)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_INVALID_CHARACTER, new Object[] { label,
                ALLOWABLE_CHARS, }));
        }
    }
    
    protected final void validateLocationName(final String regionName, final String methodName, final String label) throws InfoServiceFaultMessage {
        if (StringUtils.isEmpty(regionName)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_PARAMETER_MISSING_OR_INVALID,
                                                                                                 new Object[] { label }));
        }
        
        if (regionName.length() < WebCoreConstants.VALIDATION_MIN_LENGTH_4 || regionName.length() > WebCoreConstants.VALIDATION_MAX_LENGTH_25) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_INVALID_LENGTH, new Object[] { label,
                WebCoreConstants.VALIDATION_MIN_LENGTH_4, WebCoreConstants.VALIDATION_MAX_LENGTH_25, }));
        }
        
        if (!regionName.matches(WebCoreConstants.REGEX_LOCATION_TEXT)) {
            throw new InfoServiceFaultMessage(methodName + SPACE_FAILURE, createInfoServiceFault(MESSAGE_INVALID_CHARACTER, new Object[] { label,
                ALLOWABLE_CHARS, }));
        }
    }
    
    protected final void validateGracePeriod(final int gracePeriod, final String msgSource) throws InfoServiceFaultMessage {
        if (gracePeriod < 0) {
            throw new InfoServiceFaultMessage(msgSource, createInfoServiceFault(MESSAGE_GRACEPERIOD_NOT_POSITIVE, new Object[] { gracePeriod, }));
        }
        if (gracePeriod > MAX_GRACE_PERIOD) {
            throw new InfoServiceFaultMessage(msgSource, createInfoServiceFault(MESSAGE_GRACEPERIOD_OVER_999, new Object[] { gracePeriod, }));
        }
    }
    
    protected final int validateSecurity(final String token, final String msgFailureSrc) throws InfoServiceFaultMessage {
        final Authentication auth = authorize(token);
        if (auth == null) {
            throw new InfoServiceFaultMessage(msgFailureSrc, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(msgFailureSrc, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        // verify input parameters
        if (token == null) {
            throw new InfoServiceFaultMessage(msgFailureSrc, createInfoServiceFault(MESSAGE_INPUT_PARAMETERS_MISSING_OR_INVALID));
        }
        // check the token
        final int customerId = webUser.getCustomerId();
        if (!isTokenValid(token, customerId)) {
            throw new InfoServiceFaultMessage(msgFailureSrc, createInfoServiceFault(MESSAGE_TOKEN_INVALID));
        }
        
        return customerId;
    }
    
    private int getGracePeriod(final Integer gracePeriod) {
        return gracePeriod == null ? 0 : gracePeriod;
    }
    
}
