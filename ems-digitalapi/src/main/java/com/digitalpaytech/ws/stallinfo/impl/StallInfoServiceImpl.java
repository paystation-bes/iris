package com.digitalpaytech.ws.stallinfo.impl;

import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.ActivePermit;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.PointOfSale;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.dto.paystation.SpaceInfo;
import com.digitalpaytech.service.paystation.SpaceInfoService;
import com.digitalpaytech.util.DateUtil;
import com.digitalpaytech.util.PaystationConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.ws.BaseWsServiceImpl;
import com.digitalpaytech.ws.stallinfo.Bystatus;
import com.digitalpaytech.ws.stallinfo.GroupRequest;
import com.digitalpaytech.ws.stallinfo.GroupResponse;
import com.digitalpaytech.ws.stallinfo.GroupType;
import com.digitalpaytech.ws.stallinfo.InfoServiceFault;
import com.digitalpaytech.ws.stallinfo.InfoServiceFaultMessage;
import com.digitalpaytech.ws.stallinfo.RegionRequest;
import com.digitalpaytech.ws.stallinfo.RegionResponse;
import com.digitalpaytech.ws.stallinfo.RegionType;
import com.digitalpaytech.ws.stallinfo.SettingRequest;
import com.digitalpaytech.ws.stallinfo.SettingResponse;
import com.digitalpaytech.ws.stallinfo.SettingType;
import com.digitalpaytech.ws.stallinfo.StallInfoByGroupRequest;
import com.digitalpaytech.ws.stallinfo.StallInfoByRegionRequest;
import com.digitalpaytech.ws.stallinfo.StallInfoBySettingRequest;
import com.digitalpaytech.ws.stallinfo.StallInfoRequest;
import com.digitalpaytech.ws.stallinfo.StallInfoResponse;
import com.digitalpaytech.ws.stallinfo.StallInfoService;
import com.digitalpaytech.ws.stallinfo.StallInfoType;

@Component("stallInfoService")
@WebService(serviceName = "StallInfoService", targetNamespace = "http://ws.digitalpaytech.com/stallInfo", endpointInterface = "com.digitalpaytech.ws.stallinfo.StallInfoService")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
@SuppressWarnings({ "PMD.GodClass", "PMD.ExcessiveImports", "PMD.AvoidInstantiatingObjectsInLoops" })
public class StallInfoServiceImpl extends BaseWsServiceImpl implements StallInfoService {
    
    public static final int MAX_STALL_NUMBER = 99999;
    public static final int MIN_STALL_NUMBER = 0;
    
    public static final String NOAUTH_FAULT_TEXT = "Authentication Security Context lost in message transport?";
    
    private static final String STATUS_EXPIRED = "EXPIRED";
    private static final String STATUS_VALID = "VALID";
    private static final String STATUS_NO_DATA = "NO DATA";
    private static final String STATUS_ALL = "ALL";
    private static final String GETSTALLINFO_FAILURE = "getStallInfo Failure";
    private static final String GETSTALLINFOBYREGION_FAILURE = "getStallInfoByRegion Failure";
    private static final String GETGROUPS_FAILURE = "getGroups Failure";
    private static final String GETLOCATIONS_FAILURE = "getLocations Failure";
    private static final String GETSETTINGS_FAILURE = "getSettings Failure";
    private static final String GETSTALLINFOBYSETTING_FAILURE = "getStallInfoBySetting Failure";
    private static final String GETSTALLINFOBYGROUP_FAILURE = "getStallInfoByGroup Failure";
    private static final String DATETIMESTAMP = "datetimeStamp";
    
    @Autowired
    protected SpaceInfoService spaceInfoService;
    
    public StallInfoServiceImpl() {
        super();
        webServiceEndPointType = WebCoreConstants.END_POINT_ID_STALL_INFO;
    }
    
    public final SpaceInfoService getSpaceInfoService() {
        return this.spaceInfoService;
    }
    
    public final void setSpaceInfoService(final SpaceInfoService spaceInfoService) {
        this.spaceInfoService = spaceInfoService;
    }
    
    protected final InfoServiceFault createInfoServiceFault(final String message) {
        final InfoServiceFault fault = new InfoServiceFault();
        fault.setShortErrorMessage(message);
        return fault;
    }
    
    protected final InfoServiceFault createInfoServiceFault(final String message, final Object... args) {
        final InfoServiceFault fault = new InfoServiceFault();
        fault.setShortErrorMessage(MessageFormat.format(message, args));
        return fault;
    }
    
    /**
     * Find the occupancy level for a given stall range based on user special request
     * 
     * @param StallInfoRequest
     * 
     * @return StallInfoResponse
     * 
     * @since v6.2.1
     */
    public final StallInfoResponse getStallInfo(final StallInfoRequest stallInfoRequest) throws InfoServiceFaultMessage {
        
        final int stallType = convertInfotypeToStallType(stallInfoRequest.getStallstatus());
        int gracePeriod = getGracePeriod(stallInfoRequest.getGracePeriod(), stallType);
        this.validateGracePeriod(gracePeriod, GETSTALLINFO_FAILURE);
        gracePeriod = flipPosNeg(gracePeriod, stallType);
        
        // check the request date
        // error date format, throws exception
        if (stallInfoRequest.getDatetimeStamp() == null) {
            throw new InfoServiceFaultMessage(GETSTALLINFO_FAILURE, createInfoServiceFault(MESSAGE_DATE_PARAMETER_MISSING_OR_INVALID,
                    new Object[] { DATETIMESTAMP }));
        }
        
        final Date datetimeStamp = DateUtil.addMinutesToDate(stallInfoRequest.getDatetimeStamp(), gracePeriod);
        
        final int customerId = this.validateSecurity(stallInfoRequest.getToken(), GETSTALLINFO_FAILURE);
        final List<SpaceInfo> stalls =
                getStallStatusByCustomer(customerId, stallInfoRequest.getStallfrom(), stallInfoRequest.getStallto(), datetimeStamp, stallType);
        
        final StallInfoResponse response = new StallInfoResponse();
        for (SpaceInfo stallinfo : stalls) {
            final StallInfoType stallInfoType = convertToSchemaType(stallinfo, datetimeStamp);
            if (stallType == PaystationConstants.STALL_TYPE_VALID) {
                if (stallinfo.getEndDate().before(stallInfoRequest.getDatetimeStamp())) {
                    stallInfoType.setStatus(STATUS_EXPIRED);
                }
            } else if (stallType == PaystationConstants.STALL_TYPE_EXPIRED) {
                if (stallinfo.getEndDate().after(stallInfoRequest.getDatetimeStamp())) {
                    stallInfoType.setStatus(STATUS_VALID);
                }
            }
            addElementToList(response.getStallInfos(), stallInfoType);
        }
        return response;
    }
    
    private List<SpaceInfo> getStallStatusByCustomer(final int customerId, final int startSpace, final int endSpace, final Date date,
        final int stallType) throws DataAccessException {
        return this.spaceInfoService.getSpaceInfoListByCustomerAndSpaceRangeForPosDate(customerId, startSpace, endSpace, stallType, date,
                stallType == PaystationConstants.STALL_TYPE_VALID);
    }
    
    private void addElementToList(final List<StallInfoType> stallInfos, final StallInfoType stallInfoType) {
        // EMS-1000: filter out "No Data" elements
        if (!stallInfoType.getStatus().equals(STATUS_NO_DATA)) {
            stallInfos.add(stallInfoType);
        }
    }
    
    /**
     * Find the occupancy level for a given stall range based on Location
     * 
     * @param StallInfoByRegionRequest
     * 
     * @return StallInfoResponse
     * 
     * @since v6.2.1
     */
    public final StallInfoResponse getStallInfoByRegion(final StallInfoByRegionRequest request) throws InfoServiceFaultMessage {
        
        final int stallType = convertInfotypeToStallType(request.getStallstatus());
        int gracePeriod = getGracePeriod(request.getGracePeriod(), stallType);
        this.validateGracePeriod(gracePeriod, GETSTALLINFOBYREGION_FAILURE);
        gracePeriod = flipPosNeg(gracePeriod, stallType);
        
        // check the request date
        // error date format, throws exception
        if (request.getDatetimeStamp() == null) {
            throw new InfoServiceFaultMessage(GETSTALLINFOBYREGION_FAILURE, createInfoServiceFault(MESSAGE_DATE_PARAMETER_MISSING_OR_INVALID,
                                                                                                   new Object[] { DATETIMESTAMP }));
        }
        final Date datetimeStamp = DateUtil.addMinutesToDate(request.getDatetimeStamp(), gracePeriod);
        
        // verify input parameters
        if (!isInputParametersValid(request)) {
            throw new InfoServiceFaultMessage(GETSTALLINFOBYREGION_FAILURE, createInfoServiceFault(MESSAGE_INPUT_PARAMETERS_MISSING_OR_INVALID));
        }
        
        final StallInfoResponse response = new StallInfoResponse();
        
        final int customerId = this.validateSecurity(request.getToken(), GETSTALLINFOBYREGION_FAILURE);
        final int locationId = findLocationIdOfRequestLocation(customerId, request.getRegion());
        
        if (locationId > 0) {
            final List<SpaceInfo> stalls =
                    getStallStatusByRegion(customerId, locationId, request.getStallfrom(), request.getStallto(), datetimeStamp, stallType);
            // 
            for (SpaceInfo stallinfo : stalls) {
                final StallInfoType stallInfoType = convertToSchemaType(stallinfo, datetimeStamp);
                if (stallType == PaystationConstants.STALL_TYPE_VALID) {
                    if (stallinfo.getEndDate().before(request.getDatetimeStamp())) {
                        stallInfoType.setStatus(STATUS_EXPIRED);
                    }
                } else if (stallType == PaystationConstants.STALL_TYPE_EXPIRED) {
                    if (stallinfo.getEndDate().after(request.getDatetimeStamp())) {
                        stallInfoType.setStatus(STATUS_VALID);
                    }
                }
                addElementToList(response.getStallInfos(), stallInfoType);
            }
        }
        return response;
    }
    
    private List<SpaceInfo> getStallStatusByRegion(final int customerId, final int locationId, final int startSpace, final int endSpace,
        final Date date, final int stallType) throws DataAccessException {
        
        return this.spaceInfoService.getSpaceInfoListByLocationAndSpaceRangeForPosDate(customerId, locationId, startSpace, endSpace, stallType, date,
                stallType == PaystationConstants.STALL_TYPE_VALID);
    }
    
    private boolean isInputParametersValid(final StallInfoByRegionRequest request) {
        return request.getToken() != null && request.getRegion() != null && isStallNumberValid(request.getStallfrom())
                && isStallNumberValid(request.getStallto()) && request.getStallstatus() != null;
    }
    
    private boolean isInputParametersValid(final StallInfoByGroupRequest request) {
        return request.getToken() != null && request.getGroupName() != null && !request.getGroupName().trim().equals(WebCoreConstants.EMPTY_STRING)
                && isStallNumberValid(request.getStallfrom()) && isStallNumberValid(request.getStallto()) && request.getStallstatus() != null;
    }
    
    private boolean isStallNumberValid(final int stallNumber) {
        return stallNumber >= MIN_STALL_NUMBER && stallNumber <= MAX_STALL_NUMBER;
    }
    
    private StallInfoType convertToSchemaType(final SpaceInfo stallinfo, final Date datetimeStamp) {
        final StallInfoType stallInfoType = new StallInfoType();
        stallInfoType.setSetting(stallinfo.getPaystationSettingName());
        stallInfoType.setStallNumber(stallinfo.getStallNumber());
        stallInfoType.setPurchaseDate(stallinfo.getStartDate());
        stallInfoType.setExpiryDate(stallinfo.getEndDate());
        stallInfoType.setStatus(getvalideExpiredStatus(stallinfo.getTimeZone(), stallinfo.getStartDate(), stallinfo.getEndDate(), datetimeStamp));
        return stallInfoType;
    }
    
    /**
     * Get stall status
     * 
     * modified based on user input date
     * 
     * @since v6.2.1
     */
    private String getvalideExpiredStatus(final String timezone, final Date startDate, final Date endDate, final Date datetimeStamp) {
        if (timezone == null) {
            return STATUS_NO_DATA;
        }
        Date timeStamp;
        // change current time to customer local time
        if (datetimeStamp == null || datetimeStamp.equals(WebCoreConstants.EMPTY_DATE)) {
            final Calendar cal = new GregorianCalendar(TimeZone.getTimeZone(timezone));
            cal.setTimeInMillis(System.currentTimeMillis());
            timeStamp = cal.getTime();
        } else {
            timeStamp = datetimeStamp;
        }
        if (startDate == null || endDate == null) {
            return STATUS_NO_DATA;
        } else if (endDate.after(timeStamp)) {
            return STATUS_VALID;
        } else {
            return STATUS_EXPIRED;
        }
        
    }
    
    private int convertInfotypeToStallType(final Bystatus statusType) {
        if (statusType == null) {
            return PaystationConstants.STALL_TYPE_ALL;
        } else if (statusType.name().equals(STATUS_ALL)) {
            return PaystationConstants.STALL_TYPE_ALL;
        } else if (statusType.name().equals(STATUS_VALID)) {
            return PaystationConstants.STALL_TYPE_VALID;
        } else if (statusType.name().equals(STATUS_EXPIRED)) {
            return PaystationConstants.STALL_TYPE_EXPIRED;
        } else {
            return -1;
        }
    }
    
    /**
     * Retrieve groups based on user token
     * 
     * @param groupRequest
     *            GroupRequest
     * @return groupResponse GroupResponse
     * @throws InfoServiceFaultMessage
     * 
     * @since v6.2.1
     */
    public final GroupResponse getGroups(final GroupRequest groupRequest) throws InfoServiceFaultMessage {
        final int customerId = this.validateSecurity(groupRequest.getToken(), GETGROUPS_FAILURE);
        return getRoutes(customerId);
    }
    
    public final RegionResponse getRegions(final RegionRequest regionRequest) throws InfoServiceFaultMessage {
        final int customerId = this.validateSecurity(regionRequest.getToken(), GETLOCATIONS_FAILURE);
        return getLocations(customerId);
    }
    
    public final SettingResponse getSettings(final SettingRequest settingRequest) throws InfoServiceFaultMessage {
        final int customerId = this.validateSecurity(settingRequest.getToken(), GETSETTINGS_FAILURE);
        return getSettings(customerId);
    }
    
    protected final GroupResponse getRoutes(final int customerId) {
        final Collection<Route> routeList = super.routeService.findRoutesByCustomerId(customerId);
        
        final GroupResponse response = new GroupResponse();
        GroupType groupType;
        for (Route route : routeList) {
            groupType = new GroupType();
            groupType.setGroupName(route.getName());
            response.getGroups().add(groupType);
        }
        return response;
    }
    
    protected final RegionResponse getLocations(final int customerId) {
        final List<Location> locationList = getLocationsWithPaystations(customerId);
        
        final RegionResponse response = new RegionResponse();
        
        for (Location location : locationList) {
            
            final RegionType regionType = new RegionType();
            
            regionType.setRegionName(location.getName());
            regionType.setDescription(location.getDescription());
            response.getRegions().add(regionType);
        }
        return response;
    }
    
    protected final SettingResponse getSettings(final int customerId) {
        final List<ActivePermit> activePermitList = super.activePermitService.getPaystationSettingsFromLast14Days(customerId);
        
        final SettingResponse response = new SettingResponse();
        
        for (ActivePermit activePermit : activePermitList) {
            final SettingType settingType = new SettingType();
            
            settingType.setSettingName(activePermit.getPaystationSettingName());
            response.getSettings().add(settingType);
        }
        
        return response;
    }
    
    /**
     * Find the occupancy level for a given stall range based on user setting
     * 
     * @param StallInfoBySettingRequest
     * 
     * @return StallInfoResponse
     * 
     * @since v6.2.1
     */
    public final StallInfoResponse getStallInfoBySetting(final StallInfoBySettingRequest request) throws InfoServiceFaultMessage {
        // Valid the lotsettting name
        final int stallType = convertInfotypeToStallType(request.getStallstatus());
        int gracePeriod = getGracePeriod(request.getGracePeriod(), stallType);
        this.validateGracePeriod(gracePeriod, GETSTALLINFOBYSETTING_FAILURE);
        gracePeriod = flipPosNeg(gracePeriod, stallType);
        
        // check the request date
        // error date format, throws exception
        if (request.getDatetimeStamp() == null) {
            throw new InfoServiceFaultMessage(GETSTALLINFOBYSETTING_FAILURE, createInfoServiceFault(MESSAGE_DATE_PARAMETER_MISSING_OR_INVALID,
                    new Object[] { DATETIMESTAMP }));
        }
        
        // verify input parameters
        if (!isInputParametersValid(request)) {
            throw new InfoServiceFaultMessage(GETSTALLINFOBYSETTING_FAILURE, createInfoServiceFault(MESSAGE_INPUT_PARAMETERS_MISSING_OR_INVALID));
        }
        
        final Date datetimeStamp = DateUtil.addMinutesToDate(request.getDatetimeStamp(), gracePeriod);
        
        final int customerId = this.validateSecurity(request.getToken(), GETSTALLINFOBYSETTING_FAILURE);
        final List<SpaceInfo> stallList =
                getStallStatusByLot(customerId, request.getSetting(), request.getStallfrom(), request.getStallto(), datetimeStamp, stallType);
        final StallInfoResponse response = new StallInfoResponse();
        for (SpaceInfo stall : stallList) {
            final StallInfoType stallInfoType = convertToSchemaType(stall, datetimeStamp);
            if (stallType == PaystationConstants.STALL_TYPE_VALID) {
                if (stall.getEndDate().before(request.getDatetimeStamp())) {
                    stallInfoType.setStatus(STATUS_EXPIRED);
                }
            } else if (stallType == PaystationConstants.STALL_TYPE_EXPIRED) {
                if (stall.getEndDate().after(request.getDatetimeStamp())) {
                    stallInfoType.setStatus(STATUS_VALID);
                }
            }
            addElementToList(response.getStallInfos(), stallInfoType);
        }
        return response;
        
    }
    
    @SuppressWarnings("deprecation")
    private List<SpaceInfo> getStallStatusByLot(final int customerId, final String paystationSettingName, final int startSpace, final int endSpace,
        final Date date, final int stallType) throws DataAccessException {
        
        return this.spaceInfoService.getSpaceInfoListByPaystationSettingAndSpaceRangeForPosDate(customerId, paystationSettingName, startSpace,
                endSpace, stallType, date, stallType == PaystationConstants.STALL_TYPE_VALID);
    }
    
    /**
     * Find the occupancy level for a given stall range based on pasystation grouping
     * 
     * @param StallInfoByGroupRequest
     * 
     * @return StallInfoResponse
     * 
     * @since v6.2.1
     */
    public final StallInfoResponse getStallInfoByGroup(final StallInfoByGroupRequest request) throws InfoServiceFaultMessage {
        
        // Valid the lotsettting name
        final int stallType = convertInfotypeToStallType(request.getStallstatus());
        int gracePeriod = getGracePeriod(request.getGracePeriod(), stallType);
        this.validateGracePeriod(gracePeriod, GETSTALLINFOBYGROUP_FAILURE);
        gracePeriod = flipPosNeg(gracePeriod, stallType);
        
        // check the request date
        // error date format, throws exception
        if (request.getDatetimeStamp() == null) {
            throw new InfoServiceFaultMessage(GETSTALLINFOBYGROUP_FAILURE, createInfoServiceFault(MESSAGE_DATE_PARAMETER_MISSING_OR_INVALID,
                                                                                                  new Object[] { DATETIMESTAMP }));
        }
        
        // verify input parameters
        if (!isInputParametersValid(request)) {
            throw new InfoServiceFaultMessage(GETSTALLINFOBYGROUP_FAILURE, createInfoServiceFault(MESSAGE_INPUT_PARAMETERS_MISSING_OR_INVALID));
        }
        
        final Date datetimeStamp = DateUtil.addMinutesToDate(request.getDatetimeStamp(), gracePeriod);
        
        final StallInfoResponse response = new StallInfoResponse();
        
        final int customerId = this.validateSecurity(request.getToken(), GETSTALLINFOBYGROUP_FAILURE);
        final List<PointOfSale> posList = getPosListByRouteName(customerId, request.getGroupName());
        
        // only query database when there is at least one record
        if (posList != null && !posList.isEmpty()) {
            final List<SpaceInfo> stallList =
                    getStallStatusByGroup(posList, request.getStallfrom(), request.getStallto(), datetimeStamp, stallType, customerId);
            
            for (SpaceInfo stall : stallList) {
                final StallInfoType stallInfoType = convertToSchemaType(stall, datetimeStamp);
                if (stallType == PaystationConstants.STALL_TYPE_VALID) {
                    if (stall.getEndDate().before(request.getDatetimeStamp())) {
                        stallInfoType.setStatus(STATUS_EXPIRED);
                    }
                } else if (stallType == PaystationConstants.STALL_TYPE_EXPIRED) {
                    if (stall.getEndDate().after(request.getDatetimeStamp())) {
                        stallInfoType.setStatus(STATUS_VALID);
                    }
                }
                addElementToList(response.getStallInfos(), stallInfoType);
            }
            
        }
        return response;
    }
    
    private List<SpaceInfo> getStallStatusByGroup(final List<PointOfSale> posList, final int startSpace, final int endSpace, final Date date,
        final int stallType, final int customerId) throws DataAccessException {
        return this.spaceInfoService.getSpaceInfoListByPosListAndSpaceRangeForPosDate(customerId, posList, startSpace, endSpace, stallType, date,
                stallType == PaystationConstants.STALL_TYPE_VALID);
    }
    
    private boolean isInputParametersValid(final StallInfoBySettingRequest request) {
        return request.getToken() != null && request.getSetting() != null && isStallNumberValid(request.getStallfrom())
                && isStallNumberValid(request.getStallto()) && request.getStallstatus() != null;
    }
    
    protected final void validateGracePeriod(final int gracePeriod, final String msgSource) throws InfoServiceFaultMessage {
        if (gracePeriod < 0) {
            throw new InfoServiceFaultMessage(msgSource, createInfoServiceFault(MESSAGE_GRACEPERIOD_NOT_POSITIVE, new Object[] { gracePeriod, }));
        }
        if (gracePeriod > MAX_GRACE_PERIOD) {
            throw new InfoServiceFaultMessage(msgSource, createInfoServiceFault(MESSAGE_GRACEPERIOD_OVER_999, new Object[] { gracePeriod, }));
        }
    }
    
    protected final int validateSecurity(final String token, final String msgFailureSrc) throws InfoServiceFaultMessage {
        final Authentication auth = authorize(token);
        if (auth == null) {
            throw new InfoServiceFaultMessage(msgFailureSrc, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage(msgFailureSrc, createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        // verify input parameters
        if (token == null) {
            throw new InfoServiceFaultMessage(msgFailureSrc, createInfoServiceFault(MESSAGE_INPUT_PARAMETERS_MISSING_OR_INVALID));
        }
        // check the token
        final int customerId = webUser.getCustomerId();
        if (!isTokenValid(token, customerId)) {
            throw new InfoServiceFaultMessage(msgFailureSrc, createInfoServiceFault(MESSAGE_TOKEN_INVALID));
        }
        
        return customerId;
    }
    
    protected final int getGracePeriod(final Integer gracePeriod, final int stallType) {
        int retGracePeriod = gracePeriod == null || stallType == PaystationConstants.STALL_TYPE_ALL ? 0 : gracePeriod;
        return retGracePeriod;
    }
    
    protected final int flipPosNeg(final int gracePeriod, final int stallType) {
        if (stallType == PaystationConstants.STALL_TYPE_VALID) {
            return gracePeriod * -1;
        }
        return gracePeriod;
    }
}
