package com.digitalpaytech.ws.auditinfo.impl;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.digitalpaytech.auth.WebUser;
import com.digitalpaytech.domain.Location;
import com.digitalpaytech.domain.Route;
import com.digitalpaytech.dto.paystation.AuditInfoDto;
import com.digitalpaytech.dto.paystation.CollectionInfoDto;
import com.digitalpaytech.service.paystation.AuditInfoDtoService;
import com.digitalpaytech.service.paystation.CollectionInfoDtoService;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.WebCoreConstants;
import com.digitalpaytech.ws.auditinfo.AuditInfoByDateRequest;
import com.digitalpaytech.ws.auditinfo.AuditInfoByDateResponse;
import com.digitalpaytech.ws.auditinfo.AuditInfoByGroupRequest;
import com.digitalpaytech.ws.auditinfo.AuditInfoByGroupResponse;
import com.digitalpaytech.ws.auditinfo.AuditInfoByPaystationRequest;
import com.digitalpaytech.ws.auditinfo.AuditInfoByPaystationResponse;
import com.digitalpaytech.ws.auditinfo.AuditInfoByRegionRequest;
import com.digitalpaytech.ws.auditinfo.AuditInfoByRegionResponse;
import com.digitalpaytech.ws.auditinfo.AuditInfoService;
import com.digitalpaytech.ws.auditinfo.AuditInfoType;
import com.digitalpaytech.ws.auditinfo.CollectionInfoByDateRequest;
import com.digitalpaytech.ws.auditinfo.CollectionInfoByDateResponse;
import com.digitalpaytech.ws.auditinfo.CollectionInfoByGroupRequest;
import com.digitalpaytech.ws.auditinfo.CollectionInfoByGroupResponse;
import com.digitalpaytech.ws.auditinfo.CollectionInfoByPaystationRequest;
import com.digitalpaytech.ws.auditinfo.CollectionInfoByPaystationResponse;
import com.digitalpaytech.ws.auditinfo.CollectionInfoByRegionRequest;
import com.digitalpaytech.ws.auditinfo.CollectionInfoByRegionResponse;
import com.digitalpaytech.ws.auditinfo.CollectionInfoType;
import com.digitalpaytech.ws.auditinfo.CollectionType;
import com.digitalpaytech.ws.commoninfo.GroupRequest;
import com.digitalpaytech.ws.commoninfo.GroupResponse;
import com.digitalpaytech.ws.commoninfo.InfoServiceFaultMessage;
import com.digitalpaytech.ws.commoninfo.PaystationRequest;
import com.digitalpaytech.ws.commoninfo.PaystationResponse;
import com.digitalpaytech.ws.commoninfo.RegionRequest;
import com.digitalpaytech.ws.commoninfo.RegionResponse;
import com.digitalpaytech.ws.commoninfo.impl.CommonInfoServiceImpl;

@Component("auditInfoService")
@WebService(serviceName = "AuditInfoService", targetNamespace = "http://ws.digitalpaytech.com/auditInfo", endpointInterface = "com.digitalpaytech.ws.auditinfo.AuditInfoService")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
@SuppressWarnings({ "PMD.GodClass", "PMD.ExcessiveImports", "PMD.AvoidInstantiatingObjectsInLoops" })
public class AuditInfoServiceImpl extends CommonInfoServiceImpl implements AuditInfoService {
    private static final Logger LOGGER = Logger.getLogger(AuditInfoServiceImpl.class);
    
    @Autowired
    private AuditInfoDtoService auditInfoDtoService;
    @Autowired
    private CollectionInfoDtoService collectionInfoDtoService;
    
    public AuditInfoServiceImpl() {
        super(WebCoreConstants.END_POINT_ID_AUDIT_INFO);
    }
    
    public final AuditInfoDtoService getAuditInfoDtoService() {
        return this.auditInfoDtoService;
    }
    
    public final void setAuditInfoDtoService(final AuditInfoDtoService auditInfoDtoService) {
        this.auditInfoDtoService = auditInfoDtoService;
    }
    
    public final CollectionInfoDtoService getCollectionInfoDtoService() {
        return this.collectionInfoDtoService;
    }
    
    public final void setCollectionInfoDtoService(final CollectionInfoDtoService collectionInfoDtoService) {
        this.collectionInfoDtoService = collectionInfoDtoService;
    }
    
    public final RegionResponse getRegions(final RegionRequest regionRequest) throws InfoServiceFaultMessage {
        return super.getRegions(regionRequest);
    }
    
    public final GroupResponse getGroups(final GroupRequest groupRequest) throws InfoServiceFaultMessage {
        return super.getGroups(groupRequest);
    }
    
    public final PaystationResponse getPaystations(final PaystationRequest paystationRequest) throws InfoServiceFaultMessage {
        return super.getPaystations(paystationRequest);
    }
    
    public final AuditInfoByDateResponse getAuditByDate(final AuditInfoByDateRequest auditInfoByDateRequest) throws InfoServiceFaultMessage {
        /* Check out the authentication. */
        final Authentication auth = super.authenticationCheck(getCurrentMethodName(1), auditInfoByDateRequest.getToken());
        
        /* Validate token. */
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage("getAuditByDate Failure", createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        super.checkToken(auditInfoByDateRequest.getToken(), customerId, getCurrentMethodName(1));
        
        /* Validate collectionDateRangeStart and collectionDateRangeEnd. */
        final Date collectionDateStart = auditInfoByDateRequest.getCollectionDateRangeStart();
        final Date collectionDateEnd = auditInfoByDateRequest.getCollectionDateRangeEnd();
        this.validateCollectionDate(collectionDateStart, collectionDateEnd);
        
        /* Return the response object from service. */
        return getAuditByDate(customerId, collectionDateStart, collectionDateEnd);
    }
    
    protected final AuditInfoByDateResponse getAuditByDate(final int customerId, final Date collectionDateRangeStart,
        final Date collectionDateRangeEnd) {
        final List<AuditInfoDto> auditWsInfos = this.auditInfoDtoService.getAuditListByDate(customerId, collectionDateRangeStart,
                                                                                            collectionDateRangeEnd);
        
        if (auditWsInfos == null || auditWsInfos.isEmpty()) {
            return new AuditInfoByDateResponse();
        }
        
        final AuditInfoByDateResponse response = new AuditInfoByDateResponse();
        
        try {
            for (AuditInfoDto auditWsInfo : auditWsInfos) {
                final AuditInfoType auditInfoType = new AuditInfoType();
                BeanUtils.copyProperties(auditInfoType, auditWsInfo);
                auditInfoType.setPaystationRegion(auditWsInfo.getPaystationLocation());
                response.getAuditInfoTypes().add(auditInfoType);
            }
            return response;
        } catch (IllegalAccessException e) {
            LOGGER.error(e.getClass().getSimpleName() + StandardConstants.STRING_COLON, e);
        } catch (InvocationTargetException e) {
            LOGGER.error(e.getClass().getSimpleName() + StandardConstants.STRING_COLON, e);
        }
        return response;
    }
    
    public final AuditInfoByPaystationResponse getAuditByPaystation(final AuditInfoByPaystationRequest auditInfoByPaystationRequest)
        throws InfoServiceFaultMessage {
        /* Check out the authentication. */
        final Authentication auth = super.authenticationCheck(getCurrentMethodName(1), auditInfoByPaystationRequest.getToken());
        
        /* Validate token. */
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage("getAuditByPaystation Failure", createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        super.checkToken(auditInfoByPaystationRequest.getToken(), customerId, getCurrentMethodName(1));
        
        /* Validate serial number. */
        final String serialNumber = auditInfoByPaystationRequest.getSerialNumber();
        super.checkSerialNumber(serialNumber, getCurrentMethodName(1));
        
        /* Validate collectionDateRangeStart and collectionDateRangeEnd. */
        final Date collectionDateStart = auditInfoByPaystationRequest.getCollectionDateRangeStart();
        final Date collectionDateEnd = auditInfoByPaystationRequest.getCollectionDateRangeEnd();
        this.validateCollectionDate(collectionDateStart, collectionDateEnd);
        
        /* Return the response object from service. */
        return getAuditByPaystation(customerId, serialNumber, collectionDateStart, collectionDateEnd);
    }
    
    protected final AuditInfoByPaystationResponse getAuditByPaystation(final int customerId, final String serialNumber,
        final Date collectionDateRangeStart, final Date collectionDateRangeEnd) {
        /* Retrieve the data. */
        final List<AuditInfoDto> auditWsInfos = this.auditInfoDtoService.getAuditListBySerialNumber(customerId, collectionDateRangeStart,
                                                                                                    collectionDateRangeEnd, serialNumber);
        
        if (auditWsInfos == null || auditWsInfos.isEmpty()) {
            return new AuditInfoByPaystationResponse();
        }
        
        final AuditInfoByPaystationResponse response = new AuditInfoByPaystationResponse();
        
        try {
            for (AuditInfoDto auditWsInfo : auditWsInfos) {
                final AuditInfoType auditInfoType = new AuditInfoType();
                BeanUtils.copyProperties(auditInfoType, auditWsInfo);
                auditInfoType.setPaystationRegion(auditWsInfo.getPaystationLocation());
                response.getAuditInfoTypes().add(auditInfoType);
            }
            return response;
        } catch (IllegalAccessException e) {
            LOGGER.error(e.getClass().getSimpleName() + StandardConstants.STRING_COLON, e);
        } catch (InvocationTargetException e) {
            LOGGER.error(e.getClass().getSimpleName() + StandardConstants.STRING_COLON, e);
        }
        return response;
    }
    
    public final AuditInfoByRegionResponse getAuditByRegion(final AuditInfoByRegionRequest auditInfoByRegionRequest) throws InfoServiceFaultMessage {
        /* Check out the authentication. */
        final Authentication auth = super.authenticationCheck(getCurrentMethodName(1), auditInfoByRegionRequest.getToken());
        
        /* Validate token. */
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage("getAuditByRegion Failure", createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        super.checkToken(auditInfoByRegionRequest.getToken(), customerId, getCurrentMethodName(1));
        
        /* Validate region name. */
        final String regionName = auditInfoByRegionRequest.getRegionName();
        super.validateLocationName(regionName, REGION_NAME);
        
        /* Validate collectionDateRangeStart and collectionDateRangeEnd. */
        final Date collectionDateStart = auditInfoByRegionRequest.getCollectionDateRangeStart();
        final Date collectionDateEnd = auditInfoByRegionRequest.getCollectionDateRangeEnd();
        this.validateCollectionDate(collectionDateStart, collectionDateEnd);
        
        /* Return the response object from service. */
        return getAuditByRegion(customerId, regionName, collectionDateStart, collectionDateEnd);
    }
    
    protected final AuditInfoByRegionResponse getAuditByRegion(final int customerId, final String regionName, final Date collectionDateRangeStart,
        final Date collectionDateRangeEnd) {
        final Location location = super.locationService.findLocationByCustomerIdAndName(customerId, regionName);
        if (location == null) {
            return new AuditInfoByRegionResponse();
        }
        
        /* Retrieve the data. */
        final List<AuditInfoDto> auditWsInfos = this.auditInfoDtoService.getAuditListByLocationId(customerId, collectionDateRangeStart,
                                                                                                  collectionDateRangeEnd, location.getId());
        
        if (auditWsInfos == null || auditWsInfos.isEmpty()) {
            return new AuditInfoByRegionResponse();
        }
        
        final AuditInfoByRegionResponse response = new AuditInfoByRegionResponse();
        
        try {
            for (AuditInfoDto auditWsInfo : auditWsInfos) {
                final AuditInfoType auditInfoType = new AuditInfoType();
                BeanUtils.copyProperties(auditInfoType, auditWsInfo);
                response.getAuditInfoTypes().add(auditInfoType);
            }
            return response;
        } catch (IllegalAccessException e) {
            LOGGER.error(e.getClass().getSimpleName() + StandardConstants.STRING_COLON, e);
        } catch (InvocationTargetException e) {
            LOGGER.error(e.getClass().getSimpleName() + StandardConstants.STRING_COLON, e);
        }
        return response;
    }
    
    public final AuditInfoByGroupResponse getAuditByGroup(final AuditInfoByGroupRequest auditInfoByGroupRequest) throws InfoServiceFaultMessage {
        /* Check out the authentication. */
        final Authentication auth = super.authenticationCheck(getCurrentMethodName(1), auditInfoByGroupRequest.getToken());
        
        /* Validate token. */
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage("getAuditByGroup Failure", createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        super.checkToken(auditInfoByGroupRequest.getToken(), customerId, getCurrentMethodName(1));
        
        /* Validate group name. */
        final String groupName = auditInfoByGroupRequest.getGroupName();
        super.validateRouteName(groupName, GROUP_NAME);
        
        /* Validate collectionDateRangeStart and collectionDateRangeEnd. */
        final Date collectionDateStart = auditInfoByGroupRequest.getCollectionDateRangeStart();
        final Date collectionDateEnd = auditInfoByGroupRequest.getCollectionDateRangeEnd();
        this.validateCollectionDate(collectionDateStart, collectionDateEnd);
        
        return getAuditByGroup(customerId, groupName, collectionDateStart, collectionDateEnd);
    }
    
    protected final AuditInfoByGroupResponse getAuditByGroup(final int customerId, final String groupName, final Date collectionDateRangeStart,
        final Date collectionDateRangeEnd) {
        
        final Collection<Route> routeList = super.routeService.findRoutesByCustomerIdAndName(customerId, groupName);
        if (routeList == null || routeList.size() != 1) {
            return new AuditInfoByGroupResponse();
        }
        // There is only one route in the collection
        Route route = null;
        for (Route r : routeList) {
            route = r;
        }
        
        /* Retrieve the data. */
        final List<AuditInfoDto> auditWsInfos = this.auditInfoDtoService.getAuditListByRouteId(customerId, collectionDateRangeStart,
                                                                                               collectionDateRangeEnd, route.getId());
        
        if (auditWsInfos == null || auditWsInfos.isEmpty()) {
            return new AuditInfoByGroupResponse();
        }
        
        final AuditInfoByGroupResponse response = new AuditInfoByGroupResponse();
        
        try {
            for (AuditInfoDto auditWsInfo : auditWsInfos) {
                final AuditInfoType auditInfoType = new AuditInfoType();
                BeanUtils.copyProperties(auditInfoType, auditWsInfo);
                response.getAuditInfoTypes().add(auditInfoType);
            }
            return response;
        } catch (IllegalAccessException e) {
            LOGGER.error(e.getClass().getSimpleName() + StandardConstants.STRING_COLON, e);
        } catch (InvocationTargetException e) {
            LOGGER.error(e.getClass().getSimpleName() + StandardConstants.STRING_COLON, e);
        }
        return response;
    }
    
    private void validateCollectionDate(final Date collectionDateStart, final Date collectionDateEnd) throws InfoServiceFaultMessage {
        if (collectionDateStart == null || collectionDateStart.getTime() == 0L) {
            throw new InfoServiceFaultMessage(getCurrentMethodName(2) + SPACE_FAILURE,
                    createInfoServiceFault(MESSAGE_DATE_PARAMETER_MISSING_OR_INVALID, new Object[] { COLLECTION_DATE_RANGE_START }));
        }
        
        if (collectionDateEnd == null || collectionDateEnd.getTime() == 0L) {
            throw new InfoServiceFaultMessage(getCurrentMethodName(2) + SPACE_FAILURE,
                    createInfoServiceFault(MESSAGE_DATE_PARAMETER_MISSING_OR_INVALID, new Object[] { COLLECTION_DATE_RANGE_END }));
        }
        
        if (collectionDateStart.getTime() > collectionDateEnd.getTime()) {
            throw new InfoServiceFaultMessage(getCurrentMethodName(2) + SPACE_FAILURE, createInfoServiceFault(MESSAGE_RANGE_INVALID, new Object[] {
                COLLECTION_DATE_RANGE_START, COLLECTION_DATE_RANGE_END, }));
        }
    }
    
    public final CollectionInfoByRegionResponse getCollectionByRegion(final CollectionInfoByRegionRequest collectionInfoByRegionRequest)
        throws InfoServiceFaultMessage {
        /* Check out the authentication. */
        final Authentication auth = super.authenticationCheck(getCurrentMethodName(1), collectionInfoByRegionRequest.getToken());
        
        /* Validate token. */
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage("getCollectionByRegion Failure", createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        
        final int customerId = webUser.getCustomerId();
        super.checkToken(collectionInfoByRegionRequest.getToken(), customerId, getCurrentMethodName(1));
        
        /* Validate region name. */
        final String regionName = collectionInfoByRegionRequest.getRegionName();
        validateLocationName(regionName, REGION_NAME);
        
        /* Validate collectionDateRangeStart and collectionDateRangeEnd. */
        final Date collectionDateStart = collectionInfoByRegionRequest.getCollectionDateRangeStart();
        final Date collectionDateEnd = collectionInfoByRegionRequest.getCollectionDateRangeEnd();
        this.validateCollectionDate(collectionDateStart, collectionDateEnd);
        
        /* Return the response object from service. */
        return getCollectionByRegion(customerId, regionName, collectionDateStart, collectionDateEnd);
    }
    
    protected final CollectionInfoByRegionResponse getCollectionByRegion(final int customerId, final String regionName,
        final Date collectionDateRangeStart, final Date collectionDateRangeEnd) {
        
        final Location location = super.locationService.findLocationByCustomerIdAndName(customerId, regionName);
        if (location == null) {
            return new CollectionInfoByRegionResponse();
        }
        
        final List<CollectionInfoDto> collectionWsInfos = this.collectionInfoDtoService.getCollectionListByLocationId(customerId,
                                                                                                                      collectionDateRangeStart,
                                                                                                                      collectionDateRangeEnd,
                                                                                                                      location.getId());
        
        if (collectionWsInfos == null || collectionWsInfos.isEmpty()) {
            return new CollectionInfoByRegionResponse();
        }
        
        final CollectionInfoByRegionResponse response = new CollectionInfoByRegionResponse();
        
        try {
            for (CollectionInfoDto collectionWsInfo : collectionWsInfos) {
                final CollectionInfoType collectionInfoType = new CollectionInfoType();
                copyBeans(collectionInfoType, collectionWsInfo);
                collectionInfoType.setCollectionType(CollectionType.valueOf(collectionWsInfo.getCollectionTypeName().toUpperCase()));
                response.getCollectionInfoTypes().add(collectionInfoType);
            }
            return response;
        } catch (IllegalAccessException e) {
            LOGGER.error(e.getClass().getSimpleName() + StandardConstants.STRING_COLON, e);
        } catch (InvocationTargetException e) {
            LOGGER.error(e.getClass().getSimpleName() + StandardConstants.STRING_COLON, e);
        }
        return response;
    }
    
    public final CollectionInfoByDateResponse getCollectionByDate(final CollectionInfoByDateRequest collectionInfoByDateRequest)
        throws InfoServiceFaultMessage {
        /* Check out the authentication. */
        final Authentication auth = super.authenticationCheck(getCurrentMethodName(1), collectionInfoByDateRequest.getToken());
        
        /* Validate token. */
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage("getCollectionByDate Failure", createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        super.checkToken(collectionInfoByDateRequest.getToken(), customerId, getCurrentMethodName(1));
        
        /* Validate collectionDateRangeStart and collectionDateRangeEnd. */
        final Date collectionDateStart = collectionInfoByDateRequest.getCollectionDateRangeStart();
        final Date collectionDateEnd = collectionInfoByDateRequest.getCollectionDateRangeEnd();
        this.validateCollectionDate(collectionDateStart, collectionDateEnd);
        
        /* Return the response object from service. */
        return getCollectionByDate(customerId, collectionDateStart, collectionDateEnd);
    }
    
    protected final CollectionInfoByDateResponse getCollectionByDate(final int customerId, final Date collectionDateRangeStart,
        final Date collectionDateRangeEnd) {
        final List<CollectionInfoDto> collectionWsInfos = this.collectionInfoDtoService.getCollectionListByDate(customerId, collectionDateRangeStart,
                                                                                                                collectionDateRangeEnd);
        
        if (collectionWsInfos == null || collectionWsInfos.isEmpty()) {
            return new CollectionInfoByDateResponse();
        }
        
        final CollectionInfoByDateResponse response = new CollectionInfoByDateResponse();
        
        try {
            for (CollectionInfoDto collectionWsInfo : collectionWsInfos) {
                final CollectionInfoType collectionInfoType = new CollectionInfoType();
                copyBeans(collectionInfoType, collectionWsInfo);
                collectionInfoType.setCollectionType(CollectionType.valueOf(collectionWsInfo.getCollectionTypeName().toUpperCase()));
                response.getCollectionInfoTypes().add(collectionInfoType);
            }
            return response;
        } catch (IllegalAccessException e) {
            LOGGER.error(e.getClass().getSimpleName() + StandardConstants.STRING_COLON, e);
        } catch (InvocationTargetException e) {
            LOGGER.error(e.getClass().getSimpleName() + StandardConstants.STRING_COLON, e);
        }
        
        return response;
    }
    
    public final CollectionInfoByPaystationResponse getCollectionByPaystation(
        final CollectionInfoByPaystationRequest collectionInfoByPaystationRequest) throws InfoServiceFaultMessage {
        /* Check out the authentication. */
        final Authentication auth = super.authenticationCheck(getCurrentMethodName(1), collectionInfoByPaystationRequest.getToken());
        
        /* Validate token. */
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage("getCollectionByPaystation Failure", createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        super.checkToken(collectionInfoByPaystationRequest.getToken(), customerId, getCurrentMethodName(1));
        
        /* Validate serial number. */
        final String serialNumber = collectionInfoByPaystationRequest.getSerialNumber();
        super.checkSerialNumber(serialNumber, getCurrentMethodName(1));
        
        /* Validate collectionDateRangeStart and collectionDateRangeEnd. */
        final Date collectionDateStart = collectionInfoByPaystationRequest.getCollectionDateRangeStart();
        final Date collectionDateEnd = collectionInfoByPaystationRequest.getCollectionDateRangeEnd();
        this.validateCollectionDate(collectionDateStart, collectionDateEnd);
        
        /* Return the response object from service. */
        return getCollectionByPaystation(customerId, serialNumber, collectionDateStart, collectionDateEnd);
    }
    
    protected final CollectionInfoByPaystationResponse getCollectionByPaystation(final int customerId, final String serialNumber,
        final Date collectionDateRangeStart, final Date collectionDateRangeEnd) {
        /* Retrieve the data. */
        
        final List<CollectionInfoDto> collectionWsInfos = this.collectionInfoDtoService.getCollectionListBySerialNumber(customerId,
                                                                                                                        collectionDateRangeStart,
                                                                                                                        collectionDateRangeEnd,
                                                                                                                        serialNumber);
        
        if (collectionWsInfos == null || collectionWsInfos.isEmpty()) {
            return new CollectionInfoByPaystationResponse();
        }
        
        final CollectionInfoByPaystationResponse response = new CollectionInfoByPaystationResponse();
        
        try {
            for (CollectionInfoDto collectionWsInfo : collectionWsInfos) {
                final CollectionInfoType collectionInfoType = new CollectionInfoType();
                copyBeans(collectionInfoType, collectionWsInfo);
                collectionInfoType.setCollectionType(CollectionType.valueOf(collectionWsInfo.getCollectionTypeName().toUpperCase()));
                response.getCollectionInfoTypes().add(collectionInfoType);
            }
            return response;
        } catch (IllegalAccessException e) {
            LOGGER.error(e.getClass().getSimpleName() + StandardConstants.STRING_COLON, e);
        } catch (InvocationTargetException e) {
            LOGGER.error(e.getClass().getSimpleName() + StandardConstants.STRING_COLON, e);
        }
        return response;
    }
    
    public final CollectionInfoByGroupResponse getCollectionByGroup(final CollectionInfoByGroupRequest collectionInfoByGroupRequest)
        throws InfoServiceFaultMessage {
        /* Check out the authentication. */
        final Authentication auth = super.authenticationCheck(getCurrentMethodName(1), collectionInfoByGroupRequest.getToken());
        
        /* Validate token. */
        final WebUser webUser = (WebUser) auth.getPrincipal();
        if (!this.checkServiceAggrementAssigned(webUser.getCustomerId())) {
            throw new InfoServiceFaultMessage("getCollectionByGroup Failure", createInfoServiceFault(MESSAGE_AUTHENTICATION_FAILED));
        }
        final int customerId = webUser.getCustomerId();
        super.checkToken(collectionInfoByGroupRequest.getToken(), customerId, getCurrentMethodName(1));
        
        /* Validate group name. */
        final String groupName = collectionInfoByGroupRequest.getGroupName();
        super.validateRouteName(groupName, GROUP_NAME);
        
        /* Validate collectionDateRangeStart and collectionDateRangeEnd. */
        final Date collectionDateStart = collectionInfoByGroupRequest.getCollectionDateRangeStart();
        final Date collectionDateEnd = collectionInfoByGroupRequest.getCollectionDateRangeEnd();
        this.validateCollectionDate(collectionDateStart, collectionDateEnd);
        
        return getCollectionByGroup(customerId, groupName, collectionDateStart, collectionDateEnd);
    }
    
    protected final CollectionInfoByGroupResponse getCollectionByGroup(final int customerId, final String groupName,
        final Date collectionDateRangeStart, final Date collectionDateRangeEnd) {
        final Collection<Route> routeList = super.routeService.findRoutesByCustomerIdAndName(customerId, groupName);
        if (routeList == null || routeList.size() != 1) {
            return new CollectionInfoByGroupResponse();
        }
        // There is only one route in the collection
        Route route = null;
        for (Route r : routeList) {
            route = r;
        }
        
        final Collection<CollectionInfoDto> collectionWsInfos = this.collectionInfoDtoService.getCollectionListByRouteId(customerId,
                                                                                                                         collectionDateRangeStart,
                                                                                                                         collectionDateRangeEnd,
                                                                                                                         route.getId());
        
        if (collectionWsInfos == null || collectionWsInfos.isEmpty()) {
            return new CollectionInfoByGroupResponse();
        }
        
        final CollectionInfoByGroupResponse response = new CollectionInfoByGroupResponse();
        
        try {
            for (CollectionInfoDto collectionWsInfo : collectionWsInfos) {
                final CollectionInfoType collectionInfoType = new CollectionInfoType();
                copyBeans(collectionInfoType, collectionWsInfo);
                collectionInfoType.setCollectionType(CollectionType.valueOf(collectionWsInfo.getCollectionTypeName().toUpperCase()));
                response.getCollectionInfoTypes().add(collectionInfoType);
            }
            return response;
        } catch (IllegalAccessException e) {
            LOGGER.error(e.getClass().getSimpleName() + StandardConstants.STRING_COLON, e);
        } catch (InvocationTargetException e) {
            LOGGER.error(e.getClass().getSimpleName() + StandardConstants.STRING_COLON, e);
        }
        return response;
    }
    
    private void copyBeans(final Object obj1, final Object obj2) throws IllegalAccessException, InvocationTargetException {
        final BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
        beanUtilsBean.getConvertUtils().register(new org.apache.commons.beanutils.converters.BigDecimalConverter(null), BigDecimal.class);
        beanUtilsBean.copyProperties(obj1, obj2);
    }
}
