
package org.w3._2001.xmlschema;

import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class Adapter1
    extends XmlAdapter<String, Date>
{


    public Date unmarshal(String value) {
        return (com.digitalpaytech.ws.util.DateTimeAdapter.parseDateTime(value));
    }

    public String marshal(Date value) {
        return (com.digitalpaytech.ws.util.DateTimeAdapter.printDateTime(value));
    }

}
