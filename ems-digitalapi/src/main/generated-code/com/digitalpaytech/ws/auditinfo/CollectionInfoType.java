
package com.digitalpaytech.ws.auditinfo;

import java.math.BigDecimal;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.w3._2001.xmlschema.Adapter1;


/**
 * <p>Java class for CollectionInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CollectionInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="paystationSerialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="paystationName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="paystationRegion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="collectionPeriodStartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="collectionPeriodEndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="collectionReportNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="collectionType" type="{http://ws.digitalpaytech.com/auditInfo}CollectionType"/>
 *         &lt;element name="startPermitNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="endPermitNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numberPermitsSold" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="billStacker1" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="billStacker2" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="billStacker5" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="billStacker10" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="billStacker20" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="billStacker50" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="creditCardVisa" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="creditCardMasterCard" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="creditCardAMEX" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="creditCardDiscover" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="creditCardDiners" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="creditCardOther" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="smartCard" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="coinCanister005" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="coinCanister010" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="coinCanister025" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="coinCanister100" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="coinCanister200" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CollectionInfoType", propOrder = {
    "paystationSerialNumber",
    "paystationName",
    "paystationRegion",
    "collectionPeriodStartDate",
    "collectionPeriodEndDate",
    "collectionReportNumber",
    "collectionType",
    "startPermitNumber",
    "endPermitNumber",
    "numberPermitsSold",
    "billStacker1",
    "billStacker2",
    "billStacker5",
    "billStacker10",
    "billStacker20",
    "billStacker50",
    "creditCardVisa",
    "creditCardMasterCard",
    "creditCardAMEX",
    "creditCardDiscover",
    "creditCardDiners",
    "creditCardOther",
    "smartCard",
    "coinCanister005",
    "coinCanister010",
    "coinCanister025",
    "coinCanister100",
    "coinCanister200"
})
public class CollectionInfoType {

    @XmlElement(required = true)
    protected String paystationSerialNumber;
    @XmlElement(required = true)
    protected String paystationName;
    @XmlElement(required = true)
    protected String paystationRegion;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date collectionPeriodStartDate;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date collectionPeriodEndDate;
    protected int collectionReportNumber;
    @XmlElement(required = true)
    protected CollectionType collectionType;
    protected int startPermitNumber;
    protected int endPermitNumber;
    protected int numberPermitsSold;
    protected BigDecimal billStacker1;
    protected BigDecimal billStacker2;
    protected BigDecimal billStacker5;
    protected BigDecimal billStacker10;
    protected BigDecimal billStacker20;
    protected BigDecimal billStacker50;
    protected BigDecimal creditCardVisa;
    protected BigDecimal creditCardMasterCard;
    protected BigDecimal creditCardAMEX;
    protected BigDecimal creditCardDiscover;
    protected BigDecimal creditCardDiners;
    protected BigDecimal creditCardOther;
    protected BigDecimal smartCard;
    protected BigDecimal coinCanister005;
    protected BigDecimal coinCanister010;
    protected BigDecimal coinCanister025;
    protected BigDecimal coinCanister100;
    protected BigDecimal coinCanister200;

    /**
     * Gets the value of the paystationSerialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaystationSerialNumber() {
        return paystationSerialNumber;
    }

    /**
     * Sets the value of the paystationSerialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaystationSerialNumber(String value) {
        this.paystationSerialNumber = value;
    }

    /**
     * Gets the value of the paystationName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaystationName() {
        return paystationName;
    }

    /**
     * Sets the value of the paystationName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaystationName(String value) {
        this.paystationName = value;
    }

    /**
     * Gets the value of the paystationRegion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaystationRegion() {
        return paystationRegion;
    }

    /**
     * Sets the value of the paystationRegion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaystationRegion(String value) {
        this.paystationRegion = value;
    }

    /**
     * Gets the value of the collectionPeriodStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getCollectionPeriodStartDate() {
        return collectionPeriodStartDate;
    }

    /**
     * Sets the value of the collectionPeriodStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollectionPeriodStartDate(Date value) {
        this.collectionPeriodStartDate = value;
    }

    /**
     * Gets the value of the collectionPeriodEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getCollectionPeriodEndDate() {
        return collectionPeriodEndDate;
    }

    /**
     * Sets the value of the collectionPeriodEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollectionPeriodEndDate(Date value) {
        this.collectionPeriodEndDate = value;
    }

    /**
     * Gets the value of the collectionReportNumber property.
     * 
     */
    public int getCollectionReportNumber() {
        return collectionReportNumber;
    }

    /**
     * Sets the value of the collectionReportNumber property.
     * 
     */
    public void setCollectionReportNumber(int value) {
        this.collectionReportNumber = value;
    }

    /**
     * Gets the value of the collectionType property.
     * 
     * @return
     *     possible object is
     *     {@link CollectionType }
     *     
     */
    public CollectionType getCollectionType() {
        return collectionType;
    }

    /**
     * Sets the value of the collectionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CollectionType }
     *     
     */
    public void setCollectionType(CollectionType value) {
        this.collectionType = value;
    }

    /**
     * Gets the value of the startPermitNumber property.
     * 
     */
    public int getStartPermitNumber() {
        return startPermitNumber;
    }

    /**
     * Sets the value of the startPermitNumber property.
     * 
     */
    public void setStartPermitNumber(int value) {
        this.startPermitNumber = value;
    }

    /**
     * Gets the value of the endPermitNumber property.
     * 
     */
    public int getEndPermitNumber() {
        return endPermitNumber;
    }

    /**
     * Sets the value of the endPermitNumber property.
     * 
     */
    public void setEndPermitNumber(int value) {
        this.endPermitNumber = value;
    }

    /**
     * Gets the value of the numberPermitsSold property.
     * 
     */
    public int getNumberPermitsSold() {
        return numberPermitsSold;
    }

    /**
     * Sets the value of the numberPermitsSold property.
     * 
     */
    public void setNumberPermitsSold(int value) {
        this.numberPermitsSold = value;
    }

    /**
     * Gets the value of the billStacker1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBillStacker1() {
        return billStacker1;
    }

    /**
     * Sets the value of the billStacker1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBillStacker1(BigDecimal value) {
        this.billStacker1 = value;
    }

    /**
     * Gets the value of the billStacker2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBillStacker2() {
        return billStacker2;
    }

    /**
     * Sets the value of the billStacker2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBillStacker2(BigDecimal value) {
        this.billStacker2 = value;
    }

    /**
     * Gets the value of the billStacker5 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBillStacker5() {
        return billStacker5;
    }

    /**
     * Sets the value of the billStacker5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBillStacker5(BigDecimal value) {
        this.billStacker5 = value;
    }

    /**
     * Gets the value of the billStacker10 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBillStacker10() {
        return billStacker10;
    }

    /**
     * Sets the value of the billStacker10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBillStacker10(BigDecimal value) {
        this.billStacker10 = value;
    }

    /**
     * Gets the value of the billStacker20 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBillStacker20() {
        return billStacker20;
    }

    /**
     * Sets the value of the billStacker20 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBillStacker20(BigDecimal value) {
        this.billStacker20 = value;
    }

    /**
     * Gets the value of the billStacker50 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBillStacker50() {
        return billStacker50;
    }

    /**
     * Sets the value of the billStacker50 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBillStacker50(BigDecimal value) {
        this.billStacker50 = value;
    }

    /**
     * Gets the value of the creditCardVisa property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditCardVisa() {
        return creditCardVisa;
    }

    /**
     * Sets the value of the creditCardVisa property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditCardVisa(BigDecimal value) {
        this.creditCardVisa = value;
    }

    /**
     * Gets the value of the creditCardMasterCard property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditCardMasterCard() {
        return creditCardMasterCard;
    }

    /**
     * Sets the value of the creditCardMasterCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditCardMasterCard(BigDecimal value) {
        this.creditCardMasterCard = value;
    }

    /**
     * Gets the value of the creditCardAMEX property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditCardAMEX() {
        return creditCardAMEX;
    }

    /**
     * Sets the value of the creditCardAMEX property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditCardAMEX(BigDecimal value) {
        this.creditCardAMEX = value;
    }

    /**
     * Gets the value of the creditCardDiscover property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditCardDiscover() {
        return creditCardDiscover;
    }

    /**
     * Sets the value of the creditCardDiscover property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditCardDiscover(BigDecimal value) {
        this.creditCardDiscover = value;
    }

    /**
     * Gets the value of the creditCardDiners property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditCardDiners() {
        return creditCardDiners;
    }

    /**
     * Sets the value of the creditCardDiners property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditCardDiners(BigDecimal value) {
        this.creditCardDiners = value;
    }

    /**
     * Gets the value of the creditCardOther property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditCardOther() {
        return creditCardOther;
    }

    /**
     * Sets the value of the creditCardOther property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditCardOther(BigDecimal value) {
        this.creditCardOther = value;
    }

    /**
     * Gets the value of the smartCard property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSmartCard() {
        return smartCard;
    }

    /**
     * Sets the value of the smartCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSmartCard(BigDecimal value) {
        this.smartCard = value;
    }

    /**
     * Gets the value of the coinCanister005 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCoinCanister005() {
        return coinCanister005;
    }

    /**
     * Sets the value of the coinCanister005 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoinCanister005(BigDecimal value) {
        this.coinCanister005 = value;
    }

    /**
     * Gets the value of the coinCanister010 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCoinCanister010() {
        return coinCanister010;
    }

    /**
     * Sets the value of the coinCanister010 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoinCanister010(BigDecimal value) {
        this.coinCanister010 = value;
    }

    /**
     * Gets the value of the coinCanister025 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCoinCanister025() {
        return coinCanister025;
    }

    /**
     * Sets the value of the coinCanister025 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoinCanister025(BigDecimal value) {
        this.coinCanister025 = value;
    }

    /**
     * Gets the value of the coinCanister100 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCoinCanister100() {
        return coinCanister100;
    }

    /**
     * Sets the value of the coinCanister100 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoinCanister100(BigDecimal value) {
        this.coinCanister100 = value;
    }

    /**
     * Gets the value of the coinCanister200 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCoinCanister200() {
        return coinCanister200;
    }

    /**
     * Sets the value of the coinCanister200 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoinCanister200(BigDecimal value) {
        this.coinCanister200 = value;
    }

}
