
package com.digitalpaytech.ws.auditinfo;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CollectionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CollectionType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Coin"/>
 *     &lt;enumeration value="Bill"/>
 *     &lt;enumeration value="Card"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CollectionType")
@XmlEnum
public enum CollectionType {

    @XmlEnumValue("Coin")
    COIN("Coin"),
    @XmlEnumValue("Bill")
    BILL("Bill"),
    @XmlEnumValue("Card")
    CARD("Card");
    private final String value;

    CollectionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CollectionType fromValue(String v) {
        for (CollectionType c: CollectionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
