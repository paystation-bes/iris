
package com.digitalpaytech.ws.auditinfo;

import java.math.BigDecimal;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.w3._2001.xmlschema.Adapter1;


/**
 * <p>Java class for AuditInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AuditInfoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="paystationSerialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="paystationName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="paystationRegion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="auditPeriodStartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="auditPeriodEndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="auditReportNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numberPermitsSold" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="totalCollection" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="totalRevenue" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="overpayment" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="changeIssued" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="refundsIssued" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="totalCoinBag" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="totalBillStacker" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="billStacker1" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="billStacker2" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="billStacker5" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="billStacker10" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="billStacker20" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="billStacker50" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="totalCreditCard" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="creditCardVisa" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="creditCardMasterCard" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="creditCardAMEX" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="creditCardDiscover" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="creditCardDiners" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="creditCardOther" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="totalSmartCard" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="smartCardCharge" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="smartCardRecharge" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="coinChangerReplenished" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="coinChangerOverfillBag" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="coinChangerAcceptedFloat" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="coinChangerDispensed" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="coinChangerTestDispensed" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="coinHopper1Type" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="coinHopper1Replenished" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="coinHopper1Dispensed" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="coinHopper1TestDispensed" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="coinHopper2Type" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="coinHopper2Replenished" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="coinHopper2Dispensed" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="coinHopper2TestDispensed" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AuditInfoType", propOrder = {
    "paystationSerialNumber",
    "paystationName",
    "paystationRegion",
    "auditPeriodStartDate",
    "auditPeriodEndDate",
    "auditReportNumber",
    "numberPermitsSold",
    "totalCollection",
    "totalRevenue",
    "overpayment",
    "changeIssued",
    "refundsIssued",
    "totalCoinBag",
    "totalBillStacker",
    "billStacker1",
    "billStacker2",
    "billStacker5",
    "billStacker10",
    "billStacker20",
    "billStacker50",
    "totalCreditCard",
    "creditCardVisa",
    "creditCardMasterCard",
    "creditCardAMEX",
    "creditCardDiscover",
    "creditCardDiners",
    "creditCardOther",
    "totalSmartCard",
    "smartCardCharge",
    "smartCardRecharge",
    "coinChangerReplenished",
    "coinChangerOverfillBag",
    "coinChangerAcceptedFloat",
    "coinChangerDispensed",
    "coinChangerTestDispensed",
    "coinHopper1Type",
    "coinHopper1Replenished",
    "coinHopper1Dispensed",
    "coinHopper1TestDispensed",
    "coinHopper2Type",
    "coinHopper2Replenished",
    "coinHopper2Dispensed",
    "coinHopper2TestDispensed"
})
public class AuditInfoType {

    @XmlElement(required = true)
    protected String paystationSerialNumber;
    @XmlElement(required = true)
    protected String paystationName;
    @XmlElement(required = true)
    protected String paystationRegion;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date auditPeriodStartDate;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date auditPeriodEndDate;
    protected int auditReportNumber;
    protected int numberPermitsSold;
    @XmlElement(required = true)
    protected BigDecimal totalCollection;
    @XmlElement(required = true)
    protected BigDecimal totalRevenue;
    @XmlElement(required = true)
    protected BigDecimal overpayment;
    @XmlElement(required = true)
    protected BigDecimal changeIssued;
    @XmlElement(required = true)
    protected BigDecimal refundsIssued;
    @XmlElement(required = true)
    protected BigDecimal totalCoinBag;
    @XmlElement(required = true)
    protected BigDecimal totalBillStacker;
    @XmlElement(required = true)
    protected BigDecimal billStacker1;
    @XmlElement(required = true)
    protected BigDecimal billStacker2;
    @XmlElement(required = true)
    protected BigDecimal billStacker5;
    @XmlElement(required = true)
    protected BigDecimal billStacker10;
    @XmlElement(required = true)
    protected BigDecimal billStacker20;
    @XmlElement(required = true)
    protected BigDecimal billStacker50;
    @XmlElement(required = true)
    protected BigDecimal totalCreditCard;
    @XmlElement(required = true)
    protected BigDecimal creditCardVisa;
    @XmlElement(required = true)
    protected BigDecimal creditCardMasterCard;
    @XmlElement(required = true)
    protected BigDecimal creditCardAMEX;
    @XmlElement(required = true)
    protected BigDecimal creditCardDiscover;
    @XmlElement(required = true)
    protected BigDecimal creditCardDiners;
    @XmlElement(required = true)
    protected BigDecimal creditCardOther;
    @XmlElement(required = true)
    protected BigDecimal totalSmartCard;
    @XmlElement(required = true)
    protected BigDecimal smartCardCharge;
    @XmlElement(required = true)
    protected BigDecimal smartCardRecharge;
    @XmlElement(required = true)
    protected BigDecimal coinChangerReplenished;
    @XmlElement(required = true)
    protected BigDecimal coinChangerOverfillBag;
    @XmlElement(required = true)
    protected BigDecimal coinChangerAcceptedFloat;
    @XmlElement(required = true)
    protected BigDecimal coinChangerDispensed;
    @XmlElement(required = true)
    protected BigDecimal coinChangerTestDispensed;
    @XmlElement(required = true)
    protected BigDecimal coinHopper1Type;
    @XmlElement(required = true)
    protected BigDecimal coinHopper1Replenished;
    @XmlElement(required = true)
    protected BigDecimal coinHopper1Dispensed;
    @XmlElement(required = true)
    protected BigDecimal coinHopper1TestDispensed;
    @XmlElement(required = true)
    protected BigDecimal coinHopper2Type;
    @XmlElement(required = true)
    protected BigDecimal coinHopper2Replenished;
    @XmlElement(required = true)
    protected BigDecimal coinHopper2Dispensed;
    @XmlElement(required = true)
    protected BigDecimal coinHopper2TestDispensed;

    /**
     * Gets the value of the paystationSerialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaystationSerialNumber() {
        return paystationSerialNumber;
    }

    /**
     * Sets the value of the paystationSerialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaystationSerialNumber(String value) {
        this.paystationSerialNumber = value;
    }

    /**
     * Gets the value of the paystationName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaystationName() {
        return paystationName;
    }

    /**
     * Sets the value of the paystationName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaystationName(String value) {
        this.paystationName = value;
    }

    /**
     * Gets the value of the paystationRegion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaystationRegion() {
        return paystationRegion;
    }

    /**
     * Sets the value of the paystationRegion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaystationRegion(String value) {
        this.paystationRegion = value;
    }

    /**
     * Gets the value of the auditPeriodStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getAuditPeriodStartDate() {
        return auditPeriodStartDate;
    }

    /**
     * Sets the value of the auditPeriodStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuditPeriodStartDate(Date value) {
        this.auditPeriodStartDate = value;
    }

    /**
     * Gets the value of the auditPeriodEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getAuditPeriodEndDate() {
        return auditPeriodEndDate;
    }

    /**
     * Sets the value of the auditPeriodEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuditPeriodEndDate(Date value) {
        this.auditPeriodEndDate = value;
    }

    /**
     * Gets the value of the auditReportNumber property.
     * 
     */
    public int getAuditReportNumber() {
        return auditReportNumber;
    }

    /**
     * Sets the value of the auditReportNumber property.
     * 
     */
    public void setAuditReportNumber(int value) {
        this.auditReportNumber = value;
    }

    /**
     * Gets the value of the numberPermitsSold property.
     * 
     */
    public int getNumberPermitsSold() {
        return numberPermitsSold;
    }

    /**
     * Sets the value of the numberPermitsSold property.
     * 
     */
    public void setNumberPermitsSold(int value) {
        this.numberPermitsSold = value;
    }

    /**
     * Gets the value of the totalCollection property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalCollection() {
        return totalCollection;
    }

    /**
     * Sets the value of the totalCollection property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalCollection(BigDecimal value) {
        this.totalCollection = value;
    }

    /**
     * Gets the value of the totalRevenue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalRevenue() {
        return totalRevenue;
    }

    /**
     * Sets the value of the totalRevenue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalRevenue(BigDecimal value) {
        this.totalRevenue = value;
    }

    /**
     * Gets the value of the overpayment property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOverpayment() {
        return overpayment;
    }

    /**
     * Sets the value of the overpayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOverpayment(BigDecimal value) {
        this.overpayment = value;
    }

    /**
     * Gets the value of the changeIssued property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getChangeIssued() {
        return changeIssued;
    }

    /**
     * Sets the value of the changeIssued property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setChangeIssued(BigDecimal value) {
        this.changeIssued = value;
    }

    /**
     * Gets the value of the refundsIssued property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRefundsIssued() {
        return refundsIssued;
    }

    /**
     * Sets the value of the refundsIssued property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRefundsIssued(BigDecimal value) {
        this.refundsIssued = value;
    }

    /**
     * Gets the value of the totalCoinBag property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalCoinBag() {
        return totalCoinBag;
    }

    /**
     * Sets the value of the totalCoinBag property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalCoinBag(BigDecimal value) {
        this.totalCoinBag = value;
    }

    /**
     * Gets the value of the totalBillStacker property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalBillStacker() {
        return totalBillStacker;
    }

    /**
     * Sets the value of the totalBillStacker property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalBillStacker(BigDecimal value) {
        this.totalBillStacker = value;
    }

    /**
     * Gets the value of the billStacker1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBillStacker1() {
        return billStacker1;
    }

    /**
     * Sets the value of the billStacker1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBillStacker1(BigDecimal value) {
        this.billStacker1 = value;
    }

    /**
     * Gets the value of the billStacker2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBillStacker2() {
        return billStacker2;
    }

    /**
     * Sets the value of the billStacker2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBillStacker2(BigDecimal value) {
        this.billStacker2 = value;
    }

    /**
     * Gets the value of the billStacker5 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBillStacker5() {
        return billStacker5;
    }

    /**
     * Sets the value of the billStacker5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBillStacker5(BigDecimal value) {
        this.billStacker5 = value;
    }

    /**
     * Gets the value of the billStacker10 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBillStacker10() {
        return billStacker10;
    }

    /**
     * Sets the value of the billStacker10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBillStacker10(BigDecimal value) {
        this.billStacker10 = value;
    }

    /**
     * Gets the value of the billStacker20 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBillStacker20() {
        return billStacker20;
    }

    /**
     * Sets the value of the billStacker20 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBillStacker20(BigDecimal value) {
        this.billStacker20 = value;
    }

    /**
     * Gets the value of the billStacker50 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBillStacker50() {
        return billStacker50;
    }

    /**
     * Sets the value of the billStacker50 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBillStacker50(BigDecimal value) {
        this.billStacker50 = value;
    }

    /**
     * Gets the value of the totalCreditCard property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalCreditCard() {
        return totalCreditCard;
    }

    /**
     * Sets the value of the totalCreditCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalCreditCard(BigDecimal value) {
        this.totalCreditCard = value;
    }

    /**
     * Gets the value of the creditCardVisa property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditCardVisa() {
        return creditCardVisa;
    }

    /**
     * Sets the value of the creditCardVisa property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditCardVisa(BigDecimal value) {
        this.creditCardVisa = value;
    }

    /**
     * Gets the value of the creditCardMasterCard property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditCardMasterCard() {
        return creditCardMasterCard;
    }

    /**
     * Sets the value of the creditCardMasterCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditCardMasterCard(BigDecimal value) {
        this.creditCardMasterCard = value;
    }

    /**
     * Gets the value of the creditCardAMEX property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditCardAMEX() {
        return creditCardAMEX;
    }

    /**
     * Sets the value of the creditCardAMEX property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditCardAMEX(BigDecimal value) {
        this.creditCardAMEX = value;
    }

    /**
     * Gets the value of the creditCardDiscover property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditCardDiscover() {
        return creditCardDiscover;
    }

    /**
     * Sets the value of the creditCardDiscover property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditCardDiscover(BigDecimal value) {
        this.creditCardDiscover = value;
    }

    /**
     * Gets the value of the creditCardDiners property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditCardDiners() {
        return creditCardDiners;
    }

    /**
     * Sets the value of the creditCardDiners property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditCardDiners(BigDecimal value) {
        this.creditCardDiners = value;
    }

    /**
     * Gets the value of the creditCardOther property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditCardOther() {
        return creditCardOther;
    }

    /**
     * Sets the value of the creditCardOther property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditCardOther(BigDecimal value) {
        this.creditCardOther = value;
    }

    /**
     * Gets the value of the totalSmartCard property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalSmartCard() {
        return totalSmartCard;
    }

    /**
     * Sets the value of the totalSmartCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalSmartCard(BigDecimal value) {
        this.totalSmartCard = value;
    }

    /**
     * Gets the value of the smartCardCharge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSmartCardCharge() {
        return smartCardCharge;
    }

    /**
     * Sets the value of the smartCardCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSmartCardCharge(BigDecimal value) {
        this.smartCardCharge = value;
    }

    /**
     * Gets the value of the smartCardRecharge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSmartCardRecharge() {
        return smartCardRecharge;
    }

    /**
     * Sets the value of the smartCardRecharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSmartCardRecharge(BigDecimal value) {
        this.smartCardRecharge = value;
    }

    /**
     * Gets the value of the coinChangerReplenished property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCoinChangerReplenished() {
        return coinChangerReplenished;
    }

    /**
     * Sets the value of the coinChangerReplenished property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoinChangerReplenished(BigDecimal value) {
        this.coinChangerReplenished = value;
    }

    /**
     * Gets the value of the coinChangerOverfillBag property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCoinChangerOverfillBag() {
        return coinChangerOverfillBag;
    }

    /**
     * Sets the value of the coinChangerOverfillBag property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoinChangerOverfillBag(BigDecimal value) {
        this.coinChangerOverfillBag = value;
    }

    /**
     * Gets the value of the coinChangerAcceptedFloat property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCoinChangerAcceptedFloat() {
        return coinChangerAcceptedFloat;
    }

    /**
     * Sets the value of the coinChangerAcceptedFloat property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoinChangerAcceptedFloat(BigDecimal value) {
        this.coinChangerAcceptedFloat = value;
    }

    /**
     * Gets the value of the coinChangerDispensed property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCoinChangerDispensed() {
        return coinChangerDispensed;
    }

    /**
     * Sets the value of the coinChangerDispensed property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoinChangerDispensed(BigDecimal value) {
        this.coinChangerDispensed = value;
    }

    /**
     * Gets the value of the coinChangerTestDispensed property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCoinChangerTestDispensed() {
        return coinChangerTestDispensed;
    }

    /**
     * Sets the value of the coinChangerTestDispensed property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoinChangerTestDispensed(BigDecimal value) {
        this.coinChangerTestDispensed = value;
    }

    /**
     * Gets the value of the coinHopper1Type property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCoinHopper1Type() {
        return coinHopper1Type;
    }

    /**
     * Sets the value of the coinHopper1Type property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoinHopper1Type(BigDecimal value) {
        this.coinHopper1Type = value;
    }

    /**
     * Gets the value of the coinHopper1Replenished property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCoinHopper1Replenished() {
        return coinHopper1Replenished;
    }

    /**
     * Sets the value of the coinHopper1Replenished property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoinHopper1Replenished(BigDecimal value) {
        this.coinHopper1Replenished = value;
    }

    /**
     * Gets the value of the coinHopper1Dispensed property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCoinHopper1Dispensed() {
        return coinHopper1Dispensed;
    }

    /**
     * Sets the value of the coinHopper1Dispensed property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoinHopper1Dispensed(BigDecimal value) {
        this.coinHopper1Dispensed = value;
    }

    /**
     * Gets the value of the coinHopper1TestDispensed property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCoinHopper1TestDispensed() {
        return coinHopper1TestDispensed;
    }

    /**
     * Sets the value of the coinHopper1TestDispensed property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoinHopper1TestDispensed(BigDecimal value) {
        this.coinHopper1TestDispensed = value;
    }

    /**
     * Gets the value of the coinHopper2Type property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCoinHopper2Type() {
        return coinHopper2Type;
    }

    /**
     * Sets the value of the coinHopper2Type property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoinHopper2Type(BigDecimal value) {
        this.coinHopper2Type = value;
    }

    /**
     * Gets the value of the coinHopper2Replenished property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCoinHopper2Replenished() {
        return coinHopper2Replenished;
    }

    /**
     * Sets the value of the coinHopper2Replenished property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoinHopper2Replenished(BigDecimal value) {
        this.coinHopper2Replenished = value;
    }

    /**
     * Gets the value of the coinHopper2Dispensed property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCoinHopper2Dispensed() {
        return coinHopper2Dispensed;
    }

    /**
     * Sets the value of the coinHopper2Dispensed property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoinHopper2Dispensed(BigDecimal value) {
        this.coinHopper2Dispensed = value;
    }

    /**
     * Gets the value of the coinHopper2TestDispensed property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCoinHopper2TestDispensed() {
        return coinHopper2TestDispensed;
    }

    /**
     * Sets the value of the coinHopper2TestDispensed property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCoinHopper2TestDispensed(BigDecimal value) {
        this.coinHopper2TestDispensed = value;
    }

}
