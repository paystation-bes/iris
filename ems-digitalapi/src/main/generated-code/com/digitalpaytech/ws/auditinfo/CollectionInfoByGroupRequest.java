
package com.digitalpaytech.ws.auditinfo;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.w3._2001.xmlschema.Adapter1;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="token" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="groupName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="collectionDateRangeStart" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="collectionDateRangeEnd" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "token",
    "groupName",
    "collectionDateRangeStart",
    "collectionDateRangeEnd"
})
@XmlRootElement(name = "CollectionInfoByGroupRequest")
public class CollectionInfoByGroupRequest {

    @XmlElement(required = true)
    protected String token;
    @XmlElement(required = true)
    protected String groupName;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date collectionDateRangeStart;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date collectionDateRangeEnd;

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Gets the value of the groupName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * Sets the value of the groupName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupName(String value) {
        this.groupName = value;
    }

    /**
     * Gets the value of the collectionDateRangeStart property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getCollectionDateRangeStart() {
        return collectionDateRangeStart;
    }

    /**
     * Sets the value of the collectionDateRangeStart property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollectionDateRangeStart(Date value) {
        this.collectionDateRangeStart = value;
    }

    /**
     * Gets the value of the collectionDateRangeEnd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getCollectionDateRangeEnd() {
        return collectionDateRangeEnd;
    }

    /**
     * Sets the value of the collectionDateRangeEnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollectionDateRangeEnd(Date value) {
        this.collectionDateRangeEnd = value;
    }

}
