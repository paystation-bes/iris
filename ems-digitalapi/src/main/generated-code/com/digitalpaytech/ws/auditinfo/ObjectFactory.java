
package com.digitalpaytech.ws.auditinfo;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.digitalpaytech.ws.auditinfo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.digitalpaytech.ws.auditinfo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CollectionInfoByPaystationRequest }
     * 
     */
    public CollectionInfoByPaystationRequest createCollectionInfoByPaystationRequest() {
        return new CollectionInfoByPaystationRequest();
    }

    /**
     * Create an instance of {@link AuditInfoByRegionRequest }
     * 
     */
    public AuditInfoByRegionRequest createAuditInfoByRegionRequest() {
        return new AuditInfoByRegionRequest();
    }

    /**
     * Create an instance of {@link CollectionInfoByPaystationResponse }
     * 
     */
    public CollectionInfoByPaystationResponse createCollectionInfoByPaystationResponse() {
        return new CollectionInfoByPaystationResponse();
    }

    /**
     * Create an instance of {@link CollectionInfoType }
     * 
     */
    public CollectionInfoType createCollectionInfoType() {
        return new CollectionInfoType();
    }

    /**
     * Create an instance of {@link CollectionInfoByRegionRequest }
     * 
     */
    public CollectionInfoByRegionRequest createCollectionInfoByRegionRequest() {
        return new CollectionInfoByRegionRequest();
    }

    /**
     * Create an instance of {@link CollectionInfoByGroupRequest }
     * 
     */
    public CollectionInfoByGroupRequest createCollectionInfoByGroupRequest() {
        return new CollectionInfoByGroupRequest();
    }

    /**
     * Create an instance of {@link AuditInfoByPaystationResponse }
     * 
     */
    public AuditInfoByPaystationResponse createAuditInfoByPaystationResponse() {
        return new AuditInfoByPaystationResponse();
    }

    /**
     * Create an instance of {@link AuditInfoType }
     * 
     */
    public AuditInfoType createAuditInfoType() {
        return new AuditInfoType();
    }

    /**
     * Create an instance of {@link CollectionInfoByDateResponse }
     * 
     */
    public CollectionInfoByDateResponse createCollectionInfoByDateResponse() {
        return new CollectionInfoByDateResponse();
    }

    /**
     * Create an instance of {@link AuditInfoByRegionResponse }
     * 
     */
    public AuditInfoByRegionResponse createAuditInfoByRegionResponse() {
        return new AuditInfoByRegionResponse();
    }

    /**
     * Create an instance of {@link AuditInfoByDateRequest }
     * 
     */
    public AuditInfoByDateRequest createAuditInfoByDateRequest() {
        return new AuditInfoByDateRequest();
    }

    /**
     * Create an instance of {@link AuditInfoByDateResponse }
     * 
     */
    public AuditInfoByDateResponse createAuditInfoByDateResponse() {
        return new AuditInfoByDateResponse();
    }

    /**
     * Create an instance of {@link CollectionInfoByDateRequest }
     * 
     */
    public CollectionInfoByDateRequest createCollectionInfoByDateRequest() {
        return new CollectionInfoByDateRequest();
    }

    /**
     * Create an instance of {@link CollectionInfoByGroupResponse }
     * 
     */
    public CollectionInfoByGroupResponse createCollectionInfoByGroupResponse() {
        return new CollectionInfoByGroupResponse();
    }

    /**
     * Create an instance of {@link AuditInfoByPaystationRequest }
     * 
     */
    public AuditInfoByPaystationRequest createAuditInfoByPaystationRequest() {
        return new AuditInfoByPaystationRequest();
    }

    /**
     * Create an instance of {@link AuditInfoByGroupRequest }
     * 
     */
    public AuditInfoByGroupRequest createAuditInfoByGroupRequest() {
        return new AuditInfoByGroupRequest();
    }

    /**
     * Create an instance of {@link AuditInfoByGroupResponse }
     * 
     */
    public AuditInfoByGroupResponse createAuditInfoByGroupResponse() {
        return new AuditInfoByGroupResponse();
    }

    /**
     * Create an instance of {@link CollectionInfoByRegionResponse }
     * 
     */
    public CollectionInfoByRegionResponse createCollectionInfoByRegionResponse() {
        return new CollectionInfoByRegionResponse();
    }

}
