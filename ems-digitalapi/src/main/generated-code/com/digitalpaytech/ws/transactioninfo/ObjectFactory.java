
package com.digitalpaytech.ws.transactioninfo;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.digitalpaytech.ws.transactioninfo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.digitalpaytech.ws.transactioninfo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PaymentType }
     * 
     */
    public PaymentType createPaymentType() {
        return new PaymentType();
    }

    /**
     * Create an instance of {@link TransactionByRegionRequest }
     * 
     */
    public TransactionByRegionRequest createTransactionByRegionRequest() {
        return new TransactionByRegionRequest();
    }

    /**
     * Create an instance of {@link RegionType }
     * 
     */
    public RegionType createRegionType() {
        return new RegionType();
    }

    /**
     * Create an instance of {@link ProcessingStatusTypeRequest }
     * 
     */
    public ProcessingStatusTypeRequest createProcessingStatusTypeRequest() {
        return new ProcessingStatusTypeRequest();
    }

    /**
     * Create an instance of {@link TransactionByGroupRequest }
     * 
     */
    public TransactionByGroupRequest createTransactionByGroupRequest() {
        return new TransactionByGroupRequest();
    }

    /**
     * Create an instance of {@link TransactionBySettlementDateRequest }
     * 
     */
    public TransactionBySettlementDateRequest createTransactionBySettlementDateRequest() {
        return new TransactionBySettlementDateRequest();
    }

    /**
     * Create an instance of {@link TransactionInfoType }
     * 
     */
    public TransactionInfoType createTransactionInfoType() {
        return new TransactionInfoType();
    }

    /**
     * Create an instance of {@link SettingType }
     * 
     */
    public SettingType createSettingType() {
        return new SettingType();
    }

    /**
     * Create an instance of {@link TransactionResponse }
     * 
     */
    public TransactionResponse createTransactionResponse() {
        return new TransactionResponse();
    }

    /**
     * Create an instance of {@link PaymentTypeResponse }
     * 
     */
    public PaymentTypeResponse createPaymentTypeResponse() {
        return new PaymentTypeResponse();
    }

    /**
     * Create an instance of {@link PaystationType }
     * 
     */
    public PaystationType createPaystationType() {
        return new PaystationType();
    }

    /**
     * Create an instance of {@link InfoServiceFault }
     * 
     */
    public InfoServiceFault createInfoServiceFault() {
        return new InfoServiceFault();
    }

    /**
     * Create an instance of {@link SettingResponse }
     * 
     */
    public SettingResponse createSettingResponse() {
        return new SettingResponse();
    }

    /**
     * Create an instance of {@link ProcessingStatusTypeResponse }
     * 
     */
    public ProcessingStatusTypeResponse createProcessingStatusTypeResponse() {
        return new ProcessingStatusTypeResponse();
    }

    /**
     * Create an instance of {@link ProcessingStatusType }
     * 
     */
    public ProcessingStatusType createProcessingStatusType() {
        return new ProcessingStatusType();
    }

    /**
     * Create an instance of {@link RegionResponse }
     * 
     */
    public RegionResponse createRegionResponse() {
        return new RegionResponse();
    }

    /**
     * Create an instance of {@link RegionRequest }
     * 
     */
    public RegionRequest createRegionRequest() {
        return new RegionRequest();
    }

    /**
     * Create an instance of {@link PaystationResponse }
     * 
     */
    public PaystationResponse createPaystationResponse() {
        return new PaystationResponse();
    }

    /**
     * Create an instance of {@link TransactionByStallRequest }
     * 
     */
    public TransactionByStallRequest createTransactionByStallRequest() {
        return new TransactionByStallRequest();
    }

    /**
     * Create an instance of {@link GroupType }
     * 
     */
    public GroupType createGroupType() {
        return new GroupType();
    }

    /**
     * Create an instance of {@link TransactionByPurchasedDateRequest }
     * 
     */
    public TransactionByPurchasedDateRequest createTransactionByPurchasedDateRequest() {
        return new TransactionByPurchasedDateRequest();
    }

    /**
     * Create an instance of {@link TransactionTypeRequest }
     * 
     */
    public TransactionTypeRequest createTransactionTypeRequest() {
        return new TransactionTypeRequest();
    }

    /**
     * Create an instance of {@link TransactionType }
     * 
     */
    public TransactionType createTransactionType() {
        return new TransactionType();
    }

    /**
     * Create an instance of {@link TransactionBySerialRequest }
     * 
     */
    public TransactionBySerialRequest createTransactionBySerialRequest() {
        return new TransactionBySerialRequest();
    }

    /**
     * Create an instance of {@link SettingRequest }
     * 
     */
    public SettingRequest createSettingRequest() {
        return new SettingRequest();
    }

    /**
     * Create an instance of {@link GroupResponse }
     * 
     */
    public GroupResponse createGroupResponse() {
        return new GroupResponse();
    }

    /**
     * Create an instance of {@link TransactionTypeResponse }
     * 
     */
    public TransactionTypeResponse createTransactionTypeResponse() {
        return new TransactionTypeResponse();
    }

    /**
     * Create an instance of {@link TransactionBySettingRequest }
     * 
     */
    public TransactionBySettingRequest createTransactionBySettingRequest() {
        return new TransactionBySettingRequest();
    }

    /**
     * Create an instance of {@link PaystationRequest }
     * 
     */
    public PaystationRequest createPaystationRequest() {
        return new PaystationRequest();
    }

    /**
     * Create an instance of {@link GroupRequest }
     * 
     */
    public GroupRequest createGroupRequest() {
        return new GroupRequest();
    }

    /**
     * Create an instance of {@link PaymentTypeRequest }
     * 
     */
    public PaymentTypeRequest createPaymentTypeRequest() {
        return new PaymentTypeRequest();
    }

}
