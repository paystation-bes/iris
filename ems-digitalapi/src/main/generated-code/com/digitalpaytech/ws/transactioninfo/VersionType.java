
package com.digitalpaytech.ws.transactioninfo;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VersionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="VersionType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NCName">
 *     &lt;enumeration value="v1.0"/>
 *     &lt;enumeration value="v1.1"/>
 *     &lt;enumeration value="v1.2"/>
 *     &lt;enumeration value="v1.3"/>
 *     &lt;enumeration value="v1.4"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "VersionType")
@XmlEnum
public enum VersionType {

    @XmlEnumValue("v1.0")
    V_1_0("v1.0"),
    @XmlEnumValue("v1.1")
    V_1_1("v1.1"),
    @XmlEnumValue("v1.2")
    V_1_2("v1.2"),
    @XmlEnumValue("v1.3")
    V_1_3("v1.3"),
    @XmlEnumValue("v1.4")
    V_1_4("v1.4");
    private final String value;

    VersionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static VersionType fromValue(String v) {
        for (VersionType c: VersionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
