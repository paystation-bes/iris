
package com.digitalpaytech.ws.transactioninfo;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.w3._2001.xmlschema.Adapter1;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="token" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stallNumberFrom" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stallNumberTo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="purchasedDateFrom" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="purchasedDateTo" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="version" type="{http://ws.digitalpaytech.com/transactionInfo}VersionType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "token",
    "stallNumberFrom",
    "stallNumberTo",
    "purchasedDateFrom",
    "purchasedDateTo",
    "version"
})
@XmlRootElement(name = "TransactionByStallRequest")
public class TransactionByStallRequest {

    @XmlElement(required = true)
    protected String token;
    @XmlElement(required = true)
    protected String stallNumberFrom;
    @XmlElement(required = true)
    protected String stallNumberTo;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date purchasedDateFrom;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date purchasedDateTo;
    protected VersionType version;

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Gets the value of the stallNumberFrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStallNumberFrom() {
        return stallNumberFrom;
    }

    /**
     * Sets the value of the stallNumberFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStallNumberFrom(String value) {
        this.stallNumberFrom = value;
    }

    /**
     * Gets the value of the stallNumberTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStallNumberTo() {
        return stallNumberTo;
    }

    /**
     * Sets the value of the stallNumberTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStallNumberTo(String value) {
        this.stallNumberTo = value;
    }

    /**
     * Gets the value of the purchasedDateFrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getPurchasedDateFrom() {
        return purchasedDateFrom;
    }

    /**
     * Sets the value of the purchasedDateFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchasedDateFrom(Date value) {
        this.purchasedDateFrom = value;
    }

    /**
     * Gets the value of the purchasedDateTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getPurchasedDateTo() {
        return purchasedDateTo;
    }

    /**
     * Sets the value of the purchasedDateTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchasedDateTo(Date value) {
        this.purchasedDateTo = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link VersionType }
     *     
     */
    public VersionType getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link VersionType }
     *     
     */
    public void setVersion(VersionType value) {
        this.version = value;
    }

}
