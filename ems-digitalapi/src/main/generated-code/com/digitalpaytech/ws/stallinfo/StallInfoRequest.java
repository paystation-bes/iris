
package com.digitalpaytech.ws.stallinfo;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.w3._2001.xmlschema.Adapter1;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="token" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stallfrom" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="stallto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="stallstatus" type="{http://ws.digitalpaytech.com/stallInfo}Bystatus"/>
 *         &lt;element name="datetimeStamp" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="gracePeriod" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "token",
    "stallfrom",
    "stallto",
    "stallstatus",
    "datetimeStamp",
    "gracePeriod"
})
@XmlRootElement(name = "StallInfoRequest")
public class StallInfoRequest {

    @XmlElement(required = true)
    protected String token;
    protected int stallfrom;
    protected int stallto;
    @XmlElement(required = true)
    protected Bystatus stallstatus;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date datetimeStamp;
    protected Integer gracePeriod;

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Gets the value of the stallfrom property.
     * 
     */
    public int getStallfrom() {
        return stallfrom;
    }

    /**
     * Sets the value of the stallfrom property.
     * 
     */
    public void setStallfrom(int value) {
        this.stallfrom = value;
    }

    /**
     * Gets the value of the stallto property.
     * 
     */
    public int getStallto() {
        return stallto;
    }

    /**
     * Sets the value of the stallto property.
     * 
     */
    public void setStallto(int value) {
        this.stallto = value;
    }

    /**
     * Gets the value of the stallstatus property.
     * 
     * @return
     *     possible object is
     *     {@link Bystatus }
     *     
     */
    public Bystatus getStallstatus() {
        return stallstatus;
    }

    /**
     * Sets the value of the stallstatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link Bystatus }
     *     
     */
    public void setStallstatus(Bystatus value) {
        this.stallstatus = value;
    }

    /**
     * Gets the value of the datetimeStamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getDatetimeStamp() {
        return datetimeStamp;
    }

    /**
     * Sets the value of the datetimeStamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatetimeStamp(Date value) {
        this.datetimeStamp = value;
    }

    /**
     * Gets the value of the gracePeriod property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGracePeriod() {
        return gracePeriod;
    }

    /**
     * Sets the value of the gracePeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGracePeriod(Integer value) {
        this.gracePeriod = value;
    }

}
