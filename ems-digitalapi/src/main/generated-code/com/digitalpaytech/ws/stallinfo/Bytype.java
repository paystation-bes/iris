
package com.digitalpaytech.ws.stallinfo;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Bytype.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Bytype">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NCName">
 *     &lt;enumeration value="Setting"/>
 *     &lt;enumeration value="Region"/>
 *     &lt;enumeration value="Customer"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Bytype")
@XmlEnum
public enum Bytype {

    @XmlEnumValue("Setting")
    SETTING("Setting"),
    @XmlEnumValue("Region")
    REGION("Region"),
    @XmlEnumValue("Customer")
    CUSTOMER("Customer");
    private final String value;

    Bytype(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Bytype fromValue(String v) {
        for (Bytype c: Bytype.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
