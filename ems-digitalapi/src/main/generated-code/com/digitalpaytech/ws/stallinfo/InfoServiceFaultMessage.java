
package com.digitalpaytech.ws.stallinfo;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.7.0
 * 2015-09-04T15:15:35.109-07:00
 * Generated source version: 2.7.0
 */

@WebFault(name = "InfoServiceFault", targetNamespace = "http://ws.digitalpaytech.com/stallInfo")
public class InfoServiceFaultMessage extends Exception {
    
    private com.digitalpaytech.ws.stallinfo.InfoServiceFault infoServiceFault;

    public InfoServiceFaultMessage() {
        super();
    }
    
    public InfoServiceFaultMessage(String message) {
        super(message);
    }
    
    public InfoServiceFaultMessage(String message, Throwable cause) {
        super(message, cause);
    }

    public InfoServiceFaultMessage(String message, com.digitalpaytech.ws.stallinfo.InfoServiceFault infoServiceFault) {
        super(message);
        this.infoServiceFault = infoServiceFault;
    }

    public InfoServiceFaultMessage(String message, com.digitalpaytech.ws.stallinfo.InfoServiceFault infoServiceFault, Throwable cause) {
        super(message, cause);
        this.infoServiceFault = infoServiceFault;
    }

    public com.digitalpaytech.ws.stallinfo.InfoServiceFault getFaultInfo() {
        return this.infoServiceFault;
    }
}
