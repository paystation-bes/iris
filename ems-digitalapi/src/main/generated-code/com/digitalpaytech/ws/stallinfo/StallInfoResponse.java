
package com.digitalpaytech.ws.stallinfo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="stallInfos" type="{http://ws.digitalpaytech.com/stallInfo}StallInfoType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "stallInfos"
})
@XmlRootElement(name = "StallInfoResponse")
public class StallInfoResponse {

    protected List<StallInfoType> stallInfos;

    /**
     * Gets the value of the stallInfos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the stallInfos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStallInfos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StallInfoType }
     * 
     * 
     */
    public List<StallInfoType> getStallInfos() {
        if (stallInfos == null) {
            stallInfos = new ArrayList<StallInfoType>();
        }
        return this.stallInfos;
    }

}
