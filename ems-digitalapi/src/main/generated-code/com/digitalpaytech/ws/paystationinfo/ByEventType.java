
package com.digitalpaytech.ws.paystationinfo;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ByEventType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ByEventType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NCName">
 *     &lt;enumeration value="All"/>
 *     &lt;enumeration value="Battery"/>
 *     &lt;enumeration value="BillAcceptor"/>
 *     &lt;enumeration value="BillStacker"/>
 *     &lt;enumeration value="CardReader"/>
 *     &lt;enumeration value="CoinAcceptor"/>
 *     &lt;enumeration value="CoinChanger"/>
 *     &lt;enumeration value="CoinHopper"/>
 *     &lt;enumeration value="Paystation"/>
 *     &lt;enumeration value="Printer"/>
 *     &lt;enumeration value="CoinCanister"/>
 *     &lt;enumeration value="CoinEscrow"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ByEventType")
@XmlEnum
public enum ByEventType {

    @XmlEnumValue("All")
    ALL("All"),
    @XmlEnumValue("Battery")
    BATTERY("Battery"),
    @XmlEnumValue("BillAcceptor")
    BILL_ACCEPTOR("BillAcceptor"),
    @XmlEnumValue("BillStacker")
    BILL_STACKER("BillStacker"),
    @XmlEnumValue("CardReader")
    CARD_READER("CardReader"),
    @XmlEnumValue("CoinAcceptor")
    COIN_ACCEPTOR("CoinAcceptor"),
    @XmlEnumValue("CoinChanger")
    COIN_CHANGER("CoinChanger"),
    @XmlEnumValue("CoinHopper")
    COIN_HOPPER("CoinHopper"),
    @XmlEnumValue("Paystation")
    PAYSTATION("Paystation"),
    @XmlEnumValue("Printer")
    PRINTER("Printer"),
    @XmlEnumValue("CoinCanister")
    COIN_CANISTER("CoinCanister"),
    @XmlEnumValue("CoinEscrow")
    COIN_ESCROW("CoinEscrow");
    private final String value;

    ByEventType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ByEventType fromValue(String v) {
        for (ByEventType c: ByEventType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
