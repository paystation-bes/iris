
package com.digitalpaytech.ws.paystationinfo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.w3._2001.xmlschema.Adapter1;


/**
 * <p>Java class for PaystationStatusType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaystationStatusType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="serialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="battery1voltage" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="battery2voltage" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="alarmState" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="paystationStatusEvent" type="{http://ws.digitalpaytech.com/paystationInfo}PaystationStatusEventType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="isInServiceMode" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="isDoorOpen" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="lastHeartBeat" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaystationStatusType", propOrder = {
    "serialNumber",
    "battery1Voltage",
    "battery2Voltage",
    "alarmState",
    "paystationStatusEvent",
    "isInServiceMode",
    "isDoorOpen",
    "lastHeartBeat"
})
public class PaystationStatusType {

    @XmlElement(required = true)
    protected String serialNumber;
    @XmlElement(name = "battery1voltage")
    protected float battery1Voltage;
    @XmlElement(name = "battery2voltage")
    protected float battery2Voltage;
    @XmlElement(required = true)
    protected String alarmState;
    protected List<PaystationStatusEventType> paystationStatusEvent;
    protected boolean isInServiceMode;
    protected boolean isDoorOpen;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date lastHeartBeat;

    /**
     * Gets the value of the serialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * Sets the value of the serialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerialNumber(String value) {
        this.serialNumber = value;
    }

    /**
     * Gets the value of the battery1Voltage property.
     * 
     */
    public float getBattery1Voltage() {
        return battery1Voltage;
    }

    /**
     * Sets the value of the battery1Voltage property.
     * 
     */
    public void setBattery1Voltage(float value) {
        this.battery1Voltage = value;
    }

    /**
     * Gets the value of the battery2Voltage property.
     * 
     */
    public float getBattery2Voltage() {
        return battery2Voltage;
    }

    /**
     * Sets the value of the battery2Voltage property.
     * 
     */
    public void setBattery2Voltage(float value) {
        this.battery2Voltage = value;
    }

    /**
     * Gets the value of the alarmState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlarmState() {
        return alarmState;
    }

    /**
     * Sets the value of the alarmState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlarmState(String value) {
        this.alarmState = value;
    }

    /**
     * Gets the value of the paystationStatusEvent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paystationStatusEvent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaystationStatusEvent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaystationStatusEventType }
     * 
     * 
     */
    public List<PaystationStatusEventType> getPaystationStatusEvent() {
        if (paystationStatusEvent == null) {
            paystationStatusEvent = new ArrayList<PaystationStatusEventType>();
        }
        return this.paystationStatusEvent;
    }

    /**
     * Gets the value of the isInServiceMode property.
     * 
     */
    public boolean isIsInServiceMode() {
        return isInServiceMode;
    }

    /**
     * Sets the value of the isInServiceMode property.
     * 
     */
    public void setIsInServiceMode(boolean value) {
        this.isInServiceMode = value;
    }

    /**
     * Gets the value of the isDoorOpen property.
     * 
     */
    public boolean isIsDoorOpen() {
        return isDoorOpen;
    }

    /**
     * Sets the value of the isDoorOpen property.
     * 
     */
    public void setIsDoorOpen(boolean value) {
        this.isDoorOpen = value;
    }

    /**
     * Gets the value of the lastHeartBeat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getLastHeartBeat() {
        return lastHeartBeat;
    }

    /**
     * Sets the value of the lastHeartBeat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastHeartBeat(Date value) {
        this.lastHeartBeat = value;
    }

}
