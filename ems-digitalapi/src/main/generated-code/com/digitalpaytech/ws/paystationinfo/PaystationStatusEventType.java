
package com.digitalpaytech.ws.paystationinfo;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.w3._2001.xmlschema.Adapter1;


/**
 * <p>Java class for PaystationStatusEventType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaystationStatusEventType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="eventSeverity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="eventDetail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="eventTimeStamp" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaystationStatusEventType", propOrder = {
    "eventSeverity",
    "eventDetail",
    "eventTimeStamp"
})
public class PaystationStatusEventType {

    @XmlElement(required = true)
    protected String eventSeverity;
    @XmlElement(required = true)
    protected String eventDetail;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date eventTimeStamp;

    /**
     * Gets the value of the eventSeverity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventSeverity() {
        return eventSeverity;
    }

    /**
     * Sets the value of the eventSeverity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventSeverity(String value) {
        this.eventSeverity = value;
    }

    /**
     * Gets the value of the eventDetail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventDetail() {
        return eventDetail;
    }

    /**
     * Sets the value of the eventDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventDetail(String value) {
        this.eventDetail = value;
    }

    /**
     * Gets the value of the eventTimeStamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getEventTimeStamp() {
        return eventTimeStamp;
    }

    /**
     * Sets the value of the eventTimeStamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventTimeStamp(Date value) {
        this.eventTimeStamp = value;
    }

}
