
package com.digitalpaytech.ws.paystationinfo;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.digitalpaytech.ws.paystationinfo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.digitalpaytech.ws.paystationinfo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PaystationStatusByGroupRequest }
     * 
     */
    public PaystationStatusByGroupRequest createPaystationStatusByGroupRequest() {
        return new PaystationStatusByGroupRequest();
    }

    /**
     * Create an instance of {@link PaystationStatusResponse }
     * 
     */
    public PaystationStatusResponse createPaystationStatusResponse() {
        return new PaystationStatusResponse();
    }

    /**
     * Create an instance of {@link PaystationStatusType }
     * 
     */
    public PaystationStatusType createPaystationStatusType() {
        return new PaystationStatusType();
    }

    /**
     * Create an instance of {@link InfoServiceFault }
     * 
     */
    public InfoServiceFault createInfoServiceFault() {
        return new InfoServiceFault();
    }

    /**
     * Create an instance of {@link PaystationEventsRequest }
     * 
     */
    public PaystationEventsRequest createPaystationEventsRequest() {
        return new PaystationEventsRequest();
    }

    /**
     * Create an instance of {@link PaystationEventsResponse }
     * 
     */
    public PaystationEventsResponse createPaystationEventsResponse() {
        return new PaystationEventsResponse();
    }

    /**
     * Create an instance of {@link PaystationEventsType }
     * 
     */
    public PaystationEventsType createPaystationEventsType() {
        return new PaystationEventsType();
    }

    /**
     * Create an instance of {@link PaystationStatusByRegionRequest }
     * 
     */
    public PaystationStatusByRegionRequest createPaystationStatusByRegionRequest() {
        return new PaystationStatusByRegionRequest();
    }

    /**
     * Create an instance of {@link PaystationEventsByGroupRequest }
     * 
     */
    public PaystationEventsByGroupRequest createPaystationEventsByGroupRequest() {
        return new PaystationEventsByGroupRequest();
    }

    /**
     * Create an instance of {@link PaystationRequest }
     * 
     */
    public PaystationRequest createPaystationRequest() {
        return new PaystationRequest();
    }

    /**
     * Create an instance of {@link PaystationGroupsResponse }
     * 
     */
    public PaystationGroupsResponse createPaystationGroupsResponse() {
        return new PaystationGroupsResponse();
    }

    /**
     * Create an instance of {@link PaystationGroupsType }
     * 
     */
    public PaystationGroupsType createPaystationGroupsType() {
        return new PaystationGroupsType();
    }

    /**
     * Create an instance of {@link PaystationResponse }
     * 
     */
    public PaystationResponse createPaystationResponse() {
        return new PaystationResponse();
    }

    /**
     * Create an instance of {@link PaystationType }
     * 
     */
    public PaystationType createPaystationType() {
        return new PaystationType();
    }

    /**
     * Create an instance of {@link PaystationEventsBySerialRequest }
     * 
     */
    public PaystationEventsBySerialRequest createPaystationEventsBySerialRequest() {
        return new PaystationEventsBySerialRequest();
    }

    /**
     * Create an instance of {@link PaystationSerialListType }
     * 
     */
    public PaystationSerialListType createPaystationSerialListType() {
        return new PaystationSerialListType();
    }

    /**
     * Create an instance of {@link PaystationStatusBySerialRequest }
     * 
     */
    public PaystationStatusBySerialRequest createPaystationStatusBySerialRequest() {
        return new PaystationStatusBySerialRequest();
    }

    /**
     * Create an instance of {@link PaystationStatusRequest }
     * 
     */
    public PaystationStatusRequest createPaystationStatusRequest() {
        return new PaystationStatusRequest();
    }

    /**
     * Create an instance of {@link PaystationGroupsRequest }
     * 
     */
    public PaystationGroupsRequest createPaystationGroupsRequest() {
        return new PaystationGroupsRequest();
    }

    /**
     * Create an instance of {@link PaystationEventsByRegionRequest }
     * 
     */
    public PaystationEventsByRegionRequest createPaystationEventsByRegionRequest() {
        return new PaystationEventsByRegionRequest();
    }

    /**
     * Create an instance of {@link PaystationStatusEventType }
     * 
     */
    public PaystationStatusEventType createPaystationStatusEventType() {
        return new PaystationStatusEventType();
    }

}
