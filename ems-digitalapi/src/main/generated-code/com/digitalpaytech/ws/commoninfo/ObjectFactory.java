
package com.digitalpaytech.ws.commoninfo;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.digitalpaytech.ws.commoninfo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.digitalpaytech.ws.commoninfo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PaystationResponse }
     * 
     */
    public PaystationResponse createPaystationResponse() {
        return new PaystationResponse();
    }

    /**
     * Create an instance of {@link PaystationType }
     * 
     */
    public PaystationType createPaystationType() {
        return new PaystationType();
    }

    /**
     * Create an instance of {@link RegionResponse }
     * 
     */
    public RegionResponse createRegionResponse() {
        return new RegionResponse();
    }

    /**
     * Create an instance of {@link RegionType }
     * 
     */
    public RegionType createRegionType() {
        return new RegionType();
    }

    /**
     * Create an instance of {@link InfoServiceFault }
     * 
     */
    public InfoServiceFault createInfoServiceFault() {
        return new InfoServiceFault();
    }

    /**
     * Create an instance of {@link SettingRequest }
     * 
     */
    public SettingRequest createSettingRequest() {
        return new SettingRequest();
    }

    /**
     * Create an instance of {@link RegionRequest }
     * 
     */
    public RegionRequest createRegionRequest() {
        return new RegionRequest();
    }

    /**
     * Create an instance of {@link GroupRequest }
     * 
     */
    public GroupRequest createGroupRequest() {
        return new GroupRequest();
    }

    /**
     * Create an instance of {@link SettingResponse }
     * 
     */
    public SettingResponse createSettingResponse() {
        return new SettingResponse();
    }

    /**
     * Create an instance of {@link SettingType }
     * 
     */
    public SettingType createSettingType() {
        return new SettingType();
    }

    /**
     * Create an instance of {@link PaystationRequest }
     * 
     */
    public PaystationRequest createPaystationRequest() {
        return new PaystationRequest();
    }

    /**
     * Create an instance of {@link GroupResponse }
     * 
     */
    public GroupResponse createGroupResponse() {
        return new GroupResponse();
    }

    /**
     * Create an instance of {@link GroupType }
     * 
     */
    public GroupType createGroupType() {
        return new GroupType();
    }

}
