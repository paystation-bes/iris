
package com.digitalpaytech.ws.transactiondata;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.digitalpaytech.ws.transactiondata package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.digitalpaytech.ws.transactiondata
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PaymentType }
     * 
     */
    public PaymentType createPaymentType() {
        return new PaymentType();
    }

    /**
     * Create an instance of {@link InfoServiceFault }
     * 
     */
    public InfoServiceFault createInfoServiceFault() {
        return new InfoServiceFault();
    }

    /**
     * Create an instance of {@link LocationRequest }
     * 
     */
    public LocationRequest createLocationRequest() {
        return new LocationRequest();
    }

    /**
     * Create an instance of {@link ProcessingStatusTypeResponse }
     * 
     */
    public ProcessingStatusTypeResponse createProcessingStatusTypeResponse() {
        return new ProcessingStatusTypeResponse();
    }

    /**
     * Create an instance of {@link TransactionByUpdateDateRequest }
     * 
     */
    public TransactionByUpdateDateRequest createTransactionByUpdateDateRequest() {
        return new TransactionByUpdateDateRequest();
    }

    /**
     * Create an instance of {@link TransactionType }
     * 
     */
    public TransactionType createTransactionType() {
        return new TransactionType();
    }

    /**
     * Create an instance of {@link TransactionDataType }
     * 
     */
    public TransactionDataType createTransactionDataType() {
        return new TransactionDataType();
    }

    /**
     * Create an instance of {@link TransactionTypeRequest }
     * 
     */
    public TransactionTypeRequest createTransactionTypeRequest() {
        return new TransactionTypeRequest();
    }

    /**
     * Create an instance of {@link LocationResponse }
     * 
     */
    public LocationResponse createLocationResponse() {
        return new LocationResponse();
    }

    /**
     * Create an instance of {@link ProcessingStatusTypeRequest }
     * 
     */
    public ProcessingStatusTypeRequest createProcessingStatusTypeRequest() {
        return new ProcessingStatusTypeRequest();
    }

    /**
     * Create an instance of {@link PaystationResponse }
     * 
     */
    public PaystationResponse createPaystationResponse() {
        return new PaystationResponse();
    }

    /**
     * Create an instance of {@link TransactionTypeResponse }
     * 
     */
    public TransactionTypeResponse createTransactionTypeResponse() {
        return new TransactionTypeResponse();
    }

    /**
     * Create an instance of {@link PaystationRequest }
     * 
     */
    public PaystationRequest createPaystationRequest() {
        return new PaystationRequest();
    }

    /**
     * Create an instance of {@link PaymentTypeRequest }
     * 
     */
    public PaymentTypeRequest createPaymentTypeRequest() {
        return new PaymentTypeRequest();
    }

    /**
     * Create an instance of {@link PaymentTypeResponse }
     * 
     */
    public PaymentTypeResponse createPaymentTypeResponse() {
        return new PaymentTypeResponse();
    }

    /**
     * Create an instance of {@link LocationType }
     * 
     */
    public LocationType createLocationType() {
        return new LocationType();
    }

    /**
     * Create an instance of {@link ProcessingStatusType }
     * 
     */
    public ProcessingStatusType createProcessingStatusType() {
        return new ProcessingStatusType();
    }

    /**
     * Create an instance of {@link TransactionResponse }
     * 
     */
    public TransactionResponse createTransactionResponse() {
        return new TransactionResponse();
    }

    /**
     * Create an instance of {@link PaystationType }
     * 
     */
    public PaystationType createPaystationType() {
        return new PaystationType();
    }

}
