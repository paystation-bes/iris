
package com.digitalpaytech.ws.transactiondata;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.w3._2001.xmlschema.Adapter1;


/**
 * <p>Java class for TransactionDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionDataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="serialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="paystationName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="settingName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="paystationType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="locationId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ticketNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="stallNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="purchasedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="paymentType" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="transactionType" type="{http://www.w3.org/2001/XMLSchema}byte"/>
 *         &lt;element name="cardType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardProcessingAuthorizationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardProcessingDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="cardProcessingStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardProcessingReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardProcessingMerchantAccount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cardProcessingRetries" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="cardProcessingTransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="couponNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="couponDiscountPercent" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="couponDiscountDollar" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="excessAmount" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="refundTicketAmount" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="refundChangeIssued" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="chargedAmount" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="totalCashAmount" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="totalCardAmount" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="rateId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="rateName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rateValue" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/>
 *         &lt;element name="plateNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionDataType", propOrder = {
    "serialNumber",
    "paystationName",
    "settingName",
    "paystationType",
    "locationId",
    "ticketNumber",
    "stallNumber",
    "purchasedDate",
    "expirationDate",
    "paymentType",
    "transactionType",
    "cardType",
    "cardNumber",
    "cardProcessingAuthorizationNumber",
    "cardProcessingDate",
    "cardProcessingStatus",
    "cardProcessingReferenceNumber",
    "cardProcessingMerchantAccount",
    "cardProcessingRetries",
    "cardProcessingTransactionId",
    "couponNumber",
    "couponDiscountPercent",
    "couponDiscountDollar",
    "excessAmount",
    "refundTicketAmount",
    "refundChangeIssued",
    "chargedAmount",
    "totalCashAmount",
    "totalCardAmount",
    "rateId",
    "rateName",
    "rateValue",
    "plateNumber"
})
public class TransactionDataType {

    @XmlElement(required = true)
    protected String serialNumber;
    @XmlElement(required = true)
    protected String paystationName;
    @XmlElement(required = true)
    protected String settingName;
    @XmlElement(required = true)
    protected String paystationType;
    protected int locationId;
    protected int ticketNumber;
    @XmlElement(required = true)
    protected String stallNumber;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date purchasedDate;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date expirationDate;
    protected byte paymentType;
    protected byte transactionType;
    protected String cardType;
    protected String cardNumber;
    protected String cardProcessingAuthorizationNumber;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Date cardProcessingDate;
    protected String cardProcessingStatus;
    protected String cardProcessingReferenceNumber;
    protected String cardProcessingMerchantAccount;
    protected Integer cardProcessingRetries;
    protected String cardProcessingTransactionId;
    @XmlElement(required = true)
    protected String couponNumber;
    protected Integer couponDiscountPercent;
    protected Float couponDiscountDollar;
    protected float excessAmount;
    protected float refundTicketAmount;
    protected float refundChangeIssued;
    protected float chargedAmount;
    protected float totalCashAmount;
    protected Float totalCardAmount;
    protected Long rateId;
    protected String rateName;
    protected Float rateValue;
    protected String plateNumber;

    /**
     * Gets the value of the serialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * Sets the value of the serialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerialNumber(String value) {
        this.serialNumber = value;
    }

    /**
     * Gets the value of the paystationName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaystationName() {
        return paystationName;
    }

    /**
     * Sets the value of the paystationName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaystationName(String value) {
        this.paystationName = value;
    }

    /**
     * Gets the value of the settingName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSettingName() {
        return settingName;
    }

    /**
     * Sets the value of the settingName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSettingName(String value) {
        this.settingName = value;
    }

    /**
     * Gets the value of the paystationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaystationType() {
        return paystationType;
    }

    /**
     * Sets the value of the paystationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaystationType(String value) {
        this.paystationType = value;
    }

    /**
     * Gets the value of the locationId property.
     * 
     */
    public int getLocationId() {
        return locationId;
    }

    /**
     * Sets the value of the locationId property.
     * 
     */
    public void setLocationId(int value) {
        this.locationId = value;
    }

    /**
     * Gets the value of the ticketNumber property.
     * 
     */
    public int getTicketNumber() {
        return ticketNumber;
    }

    /**
     * Sets the value of the ticketNumber property.
     * 
     */
    public void setTicketNumber(int value) {
        this.ticketNumber = value;
    }

    /**
     * Gets the value of the stallNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStallNumber() {
        return stallNumber;
    }

    /**
     * Sets the value of the stallNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStallNumber(String value) {
        this.stallNumber = value;
    }

    /**
     * Gets the value of the purchasedDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getPurchasedDate() {
        return purchasedDate;
    }

    /**
     * Sets the value of the purchasedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPurchasedDate(Date value) {
        this.purchasedDate = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpirationDate(Date value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the paymentType property.
     * 
     */
    public byte getPaymentType() {
        return paymentType;
    }

    /**
     * Sets the value of the paymentType property.
     * 
     */
    public void setPaymentType(byte value) {
        this.paymentType = value;
    }

    /**
     * Gets the value of the transactionType property.
     * 
     */
    public byte getTransactionType() {
        return transactionType;
    }

    /**
     * Sets the value of the transactionType property.
     * 
     */
    public void setTransactionType(byte value) {
        this.transactionType = value;
    }

    /**
     * Gets the value of the cardType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * Sets the value of the cardType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardType(String value) {
        this.cardType = value;
    }

    /**
     * Gets the value of the cardNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * Sets the value of the cardNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardNumber(String value) {
        this.cardNumber = value;
    }

    /**
     * Gets the value of the cardProcessingAuthorizationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardProcessingAuthorizationNumber() {
        return cardProcessingAuthorizationNumber;
    }

    /**
     * Sets the value of the cardProcessingAuthorizationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardProcessingAuthorizationNumber(String value) {
        this.cardProcessingAuthorizationNumber = value;
    }

    /**
     * Gets the value of the cardProcessingDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Date getCardProcessingDate() {
        return cardProcessingDate;
    }

    /**
     * Sets the value of the cardProcessingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardProcessingDate(Date value) {
        this.cardProcessingDate = value;
    }

    /**
     * Gets the value of the cardProcessingStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardProcessingStatus() {
        return cardProcessingStatus;
    }

    /**
     * Sets the value of the cardProcessingStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardProcessingStatus(String value) {
        this.cardProcessingStatus = value;
    }

    /**
     * Gets the value of the cardProcessingReferenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardProcessingReferenceNumber() {
        return cardProcessingReferenceNumber;
    }

    /**
     * Sets the value of the cardProcessingReferenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardProcessingReferenceNumber(String value) {
        this.cardProcessingReferenceNumber = value;
    }

    /**
     * Gets the value of the cardProcessingMerchantAccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardProcessingMerchantAccount() {
        return cardProcessingMerchantAccount;
    }

    /**
     * Sets the value of the cardProcessingMerchantAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardProcessingMerchantAccount(String value) {
        this.cardProcessingMerchantAccount = value;
    }

    /**
     * Gets the value of the cardProcessingRetries property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCardProcessingRetries() {
        return cardProcessingRetries;
    }

    /**
     * Sets the value of the cardProcessingRetries property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCardProcessingRetries(Integer value) {
        this.cardProcessingRetries = value;
    }

    /**
     * Gets the value of the cardProcessingTransactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardProcessingTransactionId() {
        return cardProcessingTransactionId;
    }

    /**
     * Sets the value of the cardProcessingTransactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardProcessingTransactionId(String value) {
        this.cardProcessingTransactionId = value;
    }

    /**
     * Gets the value of the couponNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCouponNumber() {
        return couponNumber;
    }

    /**
     * Sets the value of the couponNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCouponNumber(String value) {
        this.couponNumber = value;
    }

    /**
     * Gets the value of the couponDiscountPercent property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCouponDiscountPercent() {
        return couponDiscountPercent;
    }

    /**
     * Sets the value of the couponDiscountPercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCouponDiscountPercent(Integer value) {
        this.couponDiscountPercent = value;
    }

    /**
     * Gets the value of the couponDiscountDollar property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getCouponDiscountDollar() {
        return couponDiscountDollar;
    }

    /**
     * Sets the value of the couponDiscountDollar property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setCouponDiscountDollar(Float value) {
        this.couponDiscountDollar = value;
    }

    /**
     * Gets the value of the excessAmount property.
     * 
     */
    public float getExcessAmount() {
        return excessAmount;
    }

    /**
     * Sets the value of the excessAmount property.
     * 
     */
    public void setExcessAmount(float value) {
        this.excessAmount = value;
    }

    /**
     * Gets the value of the refundTicketAmount property.
     * 
     */
    public float getRefundTicketAmount() {
        return refundTicketAmount;
    }

    /**
     * Sets the value of the refundTicketAmount property.
     * 
     */
    public void setRefundTicketAmount(float value) {
        this.refundTicketAmount = value;
    }

    /**
     * Gets the value of the refundChangeIssued property.
     * 
     */
    public float getRefundChangeIssued() {
        return refundChangeIssued;
    }

    /**
     * Sets the value of the refundChangeIssued property.
     * 
     */
    public void setRefundChangeIssued(float value) {
        this.refundChangeIssued = value;
    }

    /**
     * Gets the value of the chargedAmount property.
     * 
     */
    public float getChargedAmount() {
        return chargedAmount;
    }

    /**
     * Sets the value of the chargedAmount property.
     * 
     */
    public void setChargedAmount(float value) {
        this.chargedAmount = value;
    }

    /**
     * Gets the value of the totalCashAmount property.
     * 
     */
    public float getTotalCashAmount() {
        return totalCashAmount;
    }

    /**
     * Sets the value of the totalCashAmount property.
     * 
     */
    public void setTotalCashAmount(float value) {
        this.totalCashAmount = value;
    }

    /**
     * Gets the value of the totalCardAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getTotalCardAmount() {
        return totalCardAmount;
    }

    /**
     * Sets the value of the totalCardAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setTotalCardAmount(Float value) {
        this.totalCardAmount = value;
    }

    /**
     * Gets the value of the rateId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRateId() {
        return rateId;
    }

    /**
     * Sets the value of the rateId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRateId(Long value) {
        this.rateId = value;
    }

    /**
     * Gets the value of the rateName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateName() {
        return rateName;
    }

    /**
     * Sets the value of the rateName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateName(String value) {
        this.rateName = value;
    }

    /**
     * Gets the value of the rateValue property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getRateValue() {
        return rateValue;
    }

    /**
     * Sets the value of the rateValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setRateValue(Float value) {
        this.rateValue = value;
    }

    /**
     * Gets the value of the plateNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlateNumber() {
        return plateNumber;
    }

    /**
     * Sets the value of the plateNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlateNumber(String value) {
        this.plateNumber = value;
    }

}
