
package com.digitalpaytech.ws.transactiondata;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.7.0
 * 2016-04-08T11:11:17.225-07:00
 * Generated source version: 2.7.0
 */

@WebFault(name = "InfoServiceFault", targetNamespace = "http://ws.digitalpaytech.com/transactionData")
public class InfoServiceFaultMessage extends Exception {
    
    private com.digitalpaytech.ws.transactiondata.InfoServiceFault infoServiceFault;

    public InfoServiceFaultMessage() {
        super();
    }
    
    public InfoServiceFaultMessage(String message) {
        super(message);
    }
    
    public InfoServiceFaultMessage(String message, Throwable cause) {
        super(message, cause);
    }

    public InfoServiceFaultMessage(String message, com.digitalpaytech.ws.transactiondata.InfoServiceFault infoServiceFault) {
        super(message);
        this.infoServiceFault = infoServiceFault;
    }

    public InfoServiceFaultMessage(String message, com.digitalpaytech.ws.transactiondata.InfoServiceFault infoServiceFault, Throwable cause) {
        super(message, cause);
        this.infoServiceFault = infoServiceFault;
    }

    public com.digitalpaytech.ws.transactiondata.InfoServiceFault getFaultInfo() {
        return this.infoServiceFault;
    }
}
