
package com.digitalpaytech.ws.plateinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="shortErrorMessage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="techImplementationDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="errCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "shortErrorMessage",
    "techImplementationDetails",
    "errCode"
})
@XmlRootElement(name = "InfoServiceFault")
public class InfoServiceFault {

    @XmlElement(required = true)
    protected String shortErrorMessage;
    protected String techImplementationDetails;
    @XmlElement(required = true)
    protected String errCode;

    /**
     * Gets the value of the shortErrorMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShortErrorMessage() {
        return shortErrorMessage;
    }

    /**
     * Sets the value of the shortErrorMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShortErrorMessage(String value) {
        this.shortErrorMessage = value;
    }

    /**
     * Gets the value of the techImplementationDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTechImplementationDetails() {
        return techImplementationDetails;
    }

    /**
     * Sets the value of the techImplementationDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTechImplementationDetails(String value) {
        this.techImplementationDetails = value;
    }

    /**
     * Gets the value of the errCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrCode() {
        return errCode;
    }

    /**
     * Sets the value of the errCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrCode(String value) {
        this.errCode = value;
    }

}
