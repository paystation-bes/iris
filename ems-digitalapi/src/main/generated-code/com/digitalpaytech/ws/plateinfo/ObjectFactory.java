
package com.digitalpaytech.ws.plateinfo;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.digitalpaytech.ws.plateinfo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.digitalpaytech.ws.plateinfo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UpdatedPlateInfoByRegionRequest }
     * 
     */
    public UpdatedPlateInfoByRegionRequest createUpdatedPlateInfoByRegionRequest() {
        return new UpdatedPlateInfoByRegionRequest();
    }

    /**
     * Create an instance of {@link UpdatedPlateInfoRequest }
     * 
     */
    public UpdatedPlateInfoRequest createUpdatedPlateInfoRequest() {
        return new UpdatedPlateInfoRequest();
    }

    /**
     * Create an instance of {@link InfoServiceFault }
     * 
     */
    public InfoServiceFault createInfoServiceFault() {
        return new InfoServiceFault();
    }

    /**
     * Create an instance of {@link PlateInfoByPlateRequest }
     * 
     */
    public PlateInfoByPlateRequest createPlateInfoByPlateRequest() {
        return new PlateInfoByPlateRequest();
    }

    /**
     * Create an instance of {@link GroupRequest }
     * 
     */
    public GroupRequest createGroupRequest() {
        return new GroupRequest();
    }

    /**
     * Create an instance of {@link ExpiredPlateInfoByGroupRequest }
     * 
     */
    public ExpiredPlateInfoByGroupRequest createExpiredPlateInfoByGroupRequest() {
        return new ExpiredPlateInfoByGroupRequest();
    }

    /**
     * Create an instance of {@link PlateInfoRequest }
     * 
     */
    public PlateInfoRequest createPlateInfoRequest() {
        return new PlateInfoRequest();
    }

    /**
     * Create an instance of {@link PlateInfoByGroupRequest }
     * 
     */
    public PlateInfoByGroupRequest createPlateInfoByGroupRequest() {
        return new PlateInfoByGroupRequest();
    }

    /**
     * Create an instance of {@link GroupResponse }
     * 
     */
    public GroupResponse createGroupResponse() {
        return new GroupResponse();
    }

    /**
     * Create an instance of {@link GroupType }
     * 
     */
    public GroupType createGroupType() {
        return new GroupType();
    }

    /**
     * Create an instance of {@link RegionResponse }
     * 
     */
    public RegionResponse createRegionResponse() {
        return new RegionResponse();
    }

    /**
     * Create an instance of {@link RegionType }
     * 
     */
    public RegionType createRegionType() {
        return new RegionType();
    }

    /**
     * Create an instance of {@link PlateInfoByPlateResponse }
     * 
     */
    public PlateInfoByPlateResponse createPlateInfoByPlateResponse() {
        return new PlateInfoByPlateResponse();
    }

    /**
     * Create an instance of {@link PlateInfoType }
     * 
     */
    public PlateInfoType createPlateInfoType() {
        return new PlateInfoType();
    }

    /**
     * Create an instance of {@link RegionRequest }
     * 
     */
    public RegionRequest createRegionRequest() {
        return new RegionRequest();
    }

    /**
     * Create an instance of {@link ExpiredPlateInfoByRegionRequest }
     * 
     */
    public ExpiredPlateInfoByRegionRequest createExpiredPlateInfoByRegionRequest() {
        return new ExpiredPlateInfoByRegionRequest();
    }

    /**
     * Create an instance of {@link PlateInfoByRegionRequest }
     * 
     */
    public PlateInfoByRegionRequest createPlateInfoByRegionRequest() {
        return new PlateInfoByRegionRequest();
    }

    /**
     * Create an instance of {@link UpdatedPlateInfoByGroupRequest }
     * 
     */
    public UpdatedPlateInfoByGroupRequest createUpdatedPlateInfoByGroupRequest() {
        return new UpdatedPlateInfoByGroupRequest();
    }

    /**
     * Create an instance of {@link PlateInfoResponse }
     * 
     */
    public PlateInfoResponse createPlateInfoResponse() {
        return new PlateInfoResponse();
    }

    /**
     * Create an instance of {@link ExpiredPlateInfoRequest }
     * 
     */
    public ExpiredPlateInfoRequest createExpiredPlateInfoRequest() {
        return new ExpiredPlateInfoRequest();
    }

}
