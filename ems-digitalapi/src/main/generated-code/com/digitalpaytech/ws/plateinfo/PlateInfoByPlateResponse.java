
package com.digitalpaytech.ws.plateinfo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="plateInfo" type="{http://ws.digitalpaytech.com/plateInfo}PlateInfoType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "plateInfo"
})
@XmlRootElement(name = "PlateInfoByPlateResponse")
public class PlateInfoByPlateResponse {

    @XmlElement(required = true)
    protected PlateInfoType plateInfo;

    /**
     * Gets the value of the plateInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PlateInfoType }
     *     
     */
    public PlateInfoType getPlateInfo() {
        return plateInfo;
    }

    /**
     * Sets the value of the plateInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlateInfoType }
     *     
     */
    public void setPlateInfo(PlateInfoType value) {
        this.plateInfo = value;
    }

}
