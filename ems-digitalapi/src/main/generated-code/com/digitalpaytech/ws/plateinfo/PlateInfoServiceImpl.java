
/**
 * Please modify this class to meet your needs
 * This class is not complete
 */

package com.digitalpaytech.ws.plateinfo;

import java.util.logging.Logger;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 2.7.0
 * 2015-09-04T15:15:34.850-07:00
 * Generated source version: 2.7.0
 * 
 */

@javax.jws.WebService(
                      serviceName = "PlateInfoService",
                      portName = "PlateInfohttpPort",
                      targetNamespace = "http://ws.digitalpaytech.com/plateInfo",
                      wsdlLocation = "file:/C:/dev/projects/Sprint10/ems-digitalapi/src/main/resources/plateInfo.wsdl",
                      endpointInterface = "com.digitalpaytech.ws.plateinfo.PlateInfoService")
                      
public class PlateInfoServiceImpl implements PlateInfoService {

    private static final Logger LOG = Logger.getLogger(PlateInfoServiceImpl.class.getName());

    /* (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getUpdatedValidPlatesByRegion(com.digitalpaytech.ws.plateinfo.UpdatedPlateInfoByRegionRequest  updatedPlateInfoByRegionRequest )*
     */
    public com.digitalpaytech.ws.plateinfo.PlateInfoResponse getUpdatedValidPlatesByRegion(UpdatedPlateInfoByRegionRequest updatedPlateInfoByRegionRequest) throws InfoServiceFaultMessage    { 
        LOG.info("Executing operation getUpdatedValidPlatesByRegion");
        System.out.println(updatedPlateInfoByRegionRequest);
        try {
            com.digitalpaytech.ws.plateinfo.PlateInfoResponse _return = null;
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new InfoServiceFaultMessage("infoServiceFaultMessage...");
    }

    /* (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getUpdatedValidPlates(com.digitalpaytech.ws.plateinfo.UpdatedPlateInfoRequest  updatedPlateInfoRequest )*
     */
    public com.digitalpaytech.ws.plateinfo.PlateInfoResponse getUpdatedValidPlates(UpdatedPlateInfoRequest updatedPlateInfoRequest) throws InfoServiceFaultMessage    { 
        LOG.info("Executing operation getUpdatedValidPlates");
        System.out.println(updatedPlateInfoRequest);
        try {
            com.digitalpaytech.ws.plateinfo.PlateInfoResponse _return = null;
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new InfoServiceFaultMessage("infoServiceFaultMessage...");
    }

    /* (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getUpdatedValidPlatesByGroup(com.digitalpaytech.ws.plateinfo.UpdatedPlateInfoByGroupRequest  updatedPlateInfoByGroupRequest )*
     */
    public com.digitalpaytech.ws.plateinfo.PlateInfoResponse getUpdatedValidPlatesByGroup(UpdatedPlateInfoByGroupRequest updatedPlateInfoByGroupRequest) throws InfoServiceFaultMessage    { 
        LOG.info("Executing operation getUpdatedValidPlatesByGroup");
        System.out.println(updatedPlateInfoByGroupRequest);
        try {
            com.digitalpaytech.ws.plateinfo.PlateInfoResponse _return = null;
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new InfoServiceFaultMessage("infoServiceFaultMessage...");
    }

    /* (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getValidPlatesByRegion(com.digitalpaytech.ws.plateinfo.PlateInfoByRegionRequest  plateInfoByRegionRequest )*
     */
    public com.digitalpaytech.ws.plateinfo.PlateInfoResponse getValidPlatesByRegion(PlateInfoByRegionRequest plateInfoByRegionRequest) throws InfoServiceFaultMessage    { 
        LOG.info("Executing operation getValidPlatesByRegion");
        System.out.println(plateInfoByRegionRequest);
        try {
            com.digitalpaytech.ws.plateinfo.PlateInfoResponse _return = null;
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new InfoServiceFaultMessage("infoServiceFaultMessage...");
    }

    /* (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getPlateInfo(com.digitalpaytech.ws.plateinfo.PlateInfoByPlateRequest  plateInfoByPlateRequest )*
     */
    public com.digitalpaytech.ws.plateinfo.PlateInfoByPlateResponse getPlateInfo(PlateInfoByPlateRequest plateInfoByPlateRequest) throws InfoServiceFaultMessage    { 
        LOG.info("Executing operation getPlateInfo");
        System.out.println(plateInfoByPlateRequest);
        try {
            com.digitalpaytech.ws.plateinfo.PlateInfoByPlateResponse _return = null;
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new InfoServiceFaultMessage("infoServiceFaultMessage...");
    }

    /* (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getValidPlates(com.digitalpaytech.ws.plateinfo.PlateInfoRequest  plateInfoRequest )*
     */
    public com.digitalpaytech.ws.plateinfo.PlateInfoResponse getValidPlates(PlateInfoRequest plateInfoRequest) throws InfoServiceFaultMessage    { 
        LOG.info("Executing operation getValidPlates");
        System.out.println(plateInfoRequest);
        try {
            com.digitalpaytech.ws.plateinfo.PlateInfoResponse _return = null;
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new InfoServiceFaultMessage("infoServiceFaultMessage...");
    }

    /* (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getExpiredPlatesByGroup(com.digitalpaytech.ws.plateinfo.ExpiredPlateInfoByGroupRequest  expiredPlateInfoByGroupRequest )*
     */
    public com.digitalpaytech.ws.plateinfo.PlateInfoResponse getExpiredPlatesByGroup(ExpiredPlateInfoByGroupRequest expiredPlateInfoByGroupRequest) throws InfoServiceFaultMessage    { 
        LOG.info("Executing operation getExpiredPlatesByGroup");
        System.out.println(expiredPlateInfoByGroupRequest);
        try {
            com.digitalpaytech.ws.plateinfo.PlateInfoResponse _return = null;
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new InfoServiceFaultMessage("infoServiceFaultMessage...");
    }

    /* (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getExpiredPlates(com.digitalpaytech.ws.plateinfo.ExpiredPlateInfoRequest  expiredPlateInfoRequest )*
     */
    public com.digitalpaytech.ws.plateinfo.PlateInfoResponse getExpiredPlates(ExpiredPlateInfoRequest expiredPlateInfoRequest) throws InfoServiceFaultMessage    { 
        LOG.info("Executing operation getExpiredPlates");
        System.out.println(expiredPlateInfoRequest);
        try {
            com.digitalpaytech.ws.plateinfo.PlateInfoResponse _return = null;
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new InfoServiceFaultMessage("infoServiceFaultMessage...");
    }

    /* (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getExpiredPlatesByRegion(com.digitalpaytech.ws.plateinfo.ExpiredPlateInfoByRegionRequest  expiredPlateInfoByRegionRequest )*
     */
    public com.digitalpaytech.ws.plateinfo.PlateInfoResponse getExpiredPlatesByRegion(ExpiredPlateInfoByRegionRequest expiredPlateInfoByRegionRequest) throws InfoServiceFaultMessage    { 
        LOG.info("Executing operation getExpiredPlatesByRegion");
        System.out.println(expiredPlateInfoByRegionRequest);
        try {
            com.digitalpaytech.ws.plateinfo.PlateInfoResponse _return = null;
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new InfoServiceFaultMessage("infoServiceFaultMessage...");
    }

    /* (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getGroups(com.digitalpaytech.ws.plateinfo.GroupRequest  groupRequest )*
     */
    public com.digitalpaytech.ws.plateinfo.GroupResponse getGroups(GroupRequest groupRequest) throws InfoServiceFaultMessage    { 
        LOG.info("Executing operation getGroups");
        System.out.println(groupRequest);
        try {
            com.digitalpaytech.ws.plateinfo.GroupResponse _return = null;
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new InfoServiceFaultMessage("infoServiceFaultMessage...");
    }

    /* (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getRegions(com.digitalpaytech.ws.plateinfo.RegionRequest  regionRequest )*
     */
    public com.digitalpaytech.ws.plateinfo.RegionResponse getRegions(RegionRequest regionRequest) throws InfoServiceFaultMessage    { 
        LOG.info("Executing operation getRegions");
        System.out.println(regionRequest);
        try {
            com.digitalpaytech.ws.plateinfo.RegionResponse _return = null;
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new InfoServiceFaultMessage("infoServiceFaultMessage...");
    }

    /* (non-Javadoc)
     * @see com.digitalpaytech.ws.plateinfo.PlateInfoService#getValidPlatesByGroup(com.digitalpaytech.ws.plateinfo.PlateInfoByGroupRequest  plateInfoByGroupRequest )*
     */
    public com.digitalpaytech.ws.plateinfo.PlateInfoResponse getValidPlatesByGroup(PlateInfoByGroupRequest plateInfoByGroupRequest) throws InfoServiceFaultMessage    { 
        LOG.info("Executing operation getValidPlatesByGroup");
        System.out.println(plateInfoByGroupRequest);
        try {
            com.digitalpaytech.ws.plateinfo.PlateInfoResponse _return = null;
            return _return;
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
        //throw new InfoServiceFaultMessage("infoServiceFaultMessage...");
    }

}
