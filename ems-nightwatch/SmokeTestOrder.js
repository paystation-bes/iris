require('events').EventEmitter.defaultMaxListeners = 100;

var childProc = require('child_process');

var DEFAULT_TEST_GROUPS = 
	[
	 "settings.paystationDetails",
	 "sysadmin.customerDetails.timeZone",
	 "locations.flex.facilities.properties",
	 "customer.accounts.coupons",
	 "customer.accounts.customeraccounts",
	 "customer.accounts.passcard",
	 "customer.cardmanagement.bannedcards",
	 "customer.dashboard",
	 "customer.user.role",
	 //Commenting this folder out as it only has a bug level script in it 
	 //"customer.reports",
	 "customer.flex.credentials",
	 "customer.settings.alerts",
	 "customer.settings.cardsettings",
	 "customer.settings.global.settings",
	 "customer.settings.paystations.paystationplacement",
	 "customer.settings.users",
	 "flex.subscriptions",
	 "T2namechange",
	 "webservices"
	 ];

/* The following are the codes to aggregate all the tests into one big test case to feed nightwatch */

var isWin = /^win/.test(process.platform);

var nightCmd = (isWin) ? "nightwatch.cmd" : "./nightwatch";
var path = (((!process.argv[2]) || (process.argv[2].trim().length <= 0)) ? "." : process.argv[2])  + "/";
var testTag = ((!process.argv[3]) || (process.argv[3].trim().length <= 0)) ? "smoketag" : process.argv[3];

function executeTests(tests, idx) {
   if (idx < tests.length) {
       childProc.execFile(nightCmd, createNightOpts(testTag, tests[idx]), function(err, stdOut, stdErr) {
         console.log(stdOut);
         if (err) {
            console.log("There are some test error(s) while executing '" + tests[idx] + "': " + err);
            console.log(stdErr);
         }
         
         executeTests(tests, idx + 1);
      });
       
   }
}

function executeTestsSync(tests) {
	var idx = -1;
	while (++idx < tests.length) {
		try {
			console.log(childProc.execFileSync(nightCmd, createNightOpts(testTag, tests[idx])).toString());
		} catch (error) {
			console.log(error.stdout.toString())
			console.log("There are test error(s) while executing '" + tests[idx] + "'...");
			console.log(error.stderr.toString());
		}
	}
}

function createNightOpts(testTag, testGroup) {
	return [ "-g", path + testGroup, "-a", testTag ];
}

function createTestGroups(testGroupsStr) {
	var result = [];
	if (testGroupsStr && (testGroupsStr.length > 0)) {
		var buffer = testGroupsStr.split(",");
		var elem;
		var idx = buffer.length;
		while (--idx >= 0) {
			elem = buffer[idx].trim();
			if (elem.length > 0) {
				result.unshift(elem);
			}
		}
	}
	
	return (result.length > 0) ? result : DEFAULT_TEST_GROUPS;
}

executeTestsSync(createTestGroups(process.argv[4]));
