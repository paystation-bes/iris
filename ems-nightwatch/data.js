var xmlinfo = function(info) {
	var testServerURL = "https://dev.digitalpaytech.com:8447";
	
	var configs = {
		URL: testServerURL,
		ServicesURL: testServerURL + "/services",
		AdminUserName: "admin@oranj",
		SysAdminUserName: "admin@qa1admin",
		ParentUserName: "qaAutomation1@parent",
		ChildUserName: "admin@qa1child",
		ChildCustomer: "qa1child",
		ChildUserName2: "qaAutomation2@child",
		Parent2UserName: "qaAutomation2@parent",
		ChildUserName3: "admin@qa1child2",
		ChildUserName4: "admin@qa2child",
		ChildUserName5: "admin@qa2child2",
		LizaUserName: "liza@oranj",
		NoahUserName: "noah@oranj",
		ThomasUserName: "thomas@childuserEDIT",
		FlexParentName: "oranjparentqa",
		FlexChildName: "oranjchildqa",
	
		Password: "Password$1",
		Password2: "Password$2",
		DefaultPassword: "password",
		ChildRole: "01SmokeTestChildRole",
		ParentRole: "01SmokeTestParentRole",
		EditedChildRole: "01ChildRoleEDITED",
		EditedParentRole: "01ParentRoleEDITED",
		SmokeEmail: "SmokeTest@qaAutomation",
		PaystationCommAddress : "500000070005",
		PaystationCommAddress2 : "500000070001",
		PaystationCommAddress3 : "500000080001",
		CardNumber: "4275330012345675",
		CardExpiry: "0717"
	};
	
	return configs[info];
};
	
module.exports = xmlinfo;
