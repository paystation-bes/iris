@SETLOCAL enabledelayedexpansion

@IF EXIST "node_modules" (
  echo "Found node_modules directory."
) ELSE (
  mkdir node_modules
)

@echo off
FOR /F "tokens=*" %%M IN (dependencies) DO (
  @SET MODULE_INSTALL="false"
  FOR /F "USEBACKQ tokens=*" %%F IN (`npm ls %%M -parseable=true`) DO (
    IF [%%F] == [] (
      echo "Could not find module %%M..."
    ) ELSE (
      @SET MODULE_INSTALL="true"
    )
  )
  
  IF !MODULE_INSTALL! EQU "false" (
    call npm install %%M
  ) ELSE (
    echo "Module '%%M' is already installed"
  )
)
@echo on

@IF EXIST "%~dp0\node.exe" (
  "%~dp0\node.exe"  "%~dp0\node_modules\nightwatch\bin\nightwatch" %*
) ELSE (
  @SETLOCAL
  @SET PATHEXT=%PATHEXT:;.JS;=;%
  node  "%~dp0\node_modules\nightwatch\bin\nightwatch" %*
)