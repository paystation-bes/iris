/**
* @summary Flex: Flex Credentials: Successful Flex Credentials Edit
* @since 7.3.5
* @version 1.0
* @author Kianoush Nesvaderani
* @requires 
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("FlexChildName");
var password = data("Password");


module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	/**
	*@description Logs the user into Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	/**
	*@description Navigates the user to the Global tab
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigate to Global tab" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//img[@alt='Settings']", 3000)
		.click("//img[@alt='Settings']")
		.waitForFlex()
	},
	
	/**
	*@description Enters the Flex Credentials to the Edit window but Cancel
	*@param browser - The web browser object
	*@returns void
	**/
	"Enter Flex Credentials but Cancel" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("#btnEditFlexCredentials", 20000)
		.click("#btnEditFlexCredentials")
		.waitForElementVisible("#formUsername", 2000)
		.click(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.btnCancel")
		.pause(2000)
		.assert.hidden("#testNewFlexCredentials");
	},
	
	/**
	*@description Enters the Flex Credentials to the Edit window and Save
	*@param browser - The web browser object
	*@returns void
	**/
	"Enter Flex Credentials and Save" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("#btnEditFlexCredentials", 10000)
		.click("#btnEditFlexCredentials")
		.waitForElementVisible("#formUsername", 2000)
		.click(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active")
		.pause(3000);
	},
	
	/**
	*@description Verifies the success message produced
	*@param browser - The web browser object
	*@returns void
	**/
	"Verify the Success Message" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("article.scrollToTop > strong", 10000)
		.assert.containsText("article.scrollToTop > strong", "The provided Flex credentials have successfully been tested and saved to the Customer Account.")
	},
	
	/**
	*@description Logs the user out of Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Logout" : function (browser) {
		browser.logout();
	},
};
