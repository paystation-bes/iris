/**
* @summary Flex: Flex Credentials: Successful Flex Credentials Connection Test
* @since 7.3.5
* @version 1.0
* @author Kianoush Nesvaderani
* @requires 
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("FlexChildName");
var password = data("Password");


module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	/**
	*@description Logs the user into Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	/**
	*@description Navigates the user to the Global tab
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigate to Global tab" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//img[@alt='Settings']", 1000)
		.click("//img[@alt='Settings']")
		.waitForElementVisible("//a[contains(text(),'Global')]", 1000)
		.click("//a[contains(text(),'Global')]")
		.pause(10000);
	},
	
	/**
	*@description Clicks on the Connection Test button
	*@param browser - The web browser object
	*@returns void
	**/
	"Click Test Connection Button" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("#flexTestButton", 10000)
		.click("#flexTestButton")
		.pause(10000);
	},
	
	/**
	*@description Verifies the success icon and message produced
	*@param browser - The web browser object
	*@returns void
	**/
	"Verify the Success Message and Success Icon" : function (browser) {
		browser
		.useCss()
		.assert.elementPresent("section.flexTestIcon.success")
		.assert.containsText("#flexTestButtonResult", "Successful Connection");
	},
	
	/**
	*@description Logs the user out of Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Logout" : function (browser) {
		browser.logout();
	},
};
