/**
* @summary Flex: Flex Credentials: Successful Flex Credentials Edit (System Admin)
* @since 7.3.5
* @version 1.0
* @author Kianoush Nesvaderani
* @requires 
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	/**
	*@description Logs the user into Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - System Administration');
	},
	
	/**
	*@description Searches the customer and navigates to its page
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigate to Customer Admin" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("#search", 5000)
		.setValue("#search", "oranj")
		.useXpath()
		.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'oranj')]", 2000)
		.click("//a[@class='ui-corner-all'][contains(text(),'oranj')]")
		.click("//a[@id='btnGo']");
	},
	
	/**
	*@description Navigates to Flex Credentials Page
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigate to Flex Credentials Section" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[contains(text(),'Licenses')]", 5000)
		.click("//a[contains(text(),'Licenses')]")
		.waitForElementVisible("//a[contains(text(),'Integrations')]", 5000)
		.click("//a[contains(text(),'Integrations')]");
	},
	
	/**
	*@description Enters the Flex Credentials to the Edit window but Cancel
	*@param browser - The web browser object
	*@returns void
	**/
	"Edit but Cancel" : function (browser) {
		browser
		.useCss()
		.pause(5000)
		.click("#btnEditFlexCredentialsSysAdmin")
		.waitForElementVisible("#formUsername", 10000)
		.pause(3000)

		.useXpath()
		.assert.visible("//section[@id= 'flexCredentialsFormPanel']//a[contains(text(),'Cancel')]")
		.click("//section[@id= 'flexCredentialsFormPanel']//a[contains(text(),'Cancel')]")
		//.click(".linkButton.textButton.cancel")
		.pause(2000)

		.useCss()
		.assert.hidden("#testNewFlexCredentials");
	},
	
	/**
	*@description Enters the Flex Credentials to the Edit window and Save
	*@param browser - The web browser object
	*@returns void
	**/
	"Edit and Save" : function (browser) {
		browser
		.useCss()
		.pause(3000)
		.click("#btnEditFlexCredentialsSysAdmin")
		.waitForElementVisible("#formUsername", 10000)
		.click(".linkButtonFtr.textButton.save")
		.pause(5000);
	},
	
	/**
	*@description Verifies the success message produced
	*@param browser - The web browser object
	*@returns void
	**/
	"Verify Credential Information" : function (browser) {
		browser
		.useCss()
		.assert.containsText("h2", "FLEX SERVER CREDENTIALS")
		.useXpath()
		.assert.elementPresent("//dt[@class='detailLabel' and contains(text(),'Username:')]/following-sibling::dd[@class='detailValue' and contains(text(),'dptws')]")

	},
	
	/**
	*@description Logs the user out of Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Logout" : function (browser) {
		browser.logout();
	},
};