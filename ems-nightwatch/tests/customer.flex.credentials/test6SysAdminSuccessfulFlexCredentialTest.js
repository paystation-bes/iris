/**
* @summary Flex: Flex Credentials: Successful Flex Credentials Connection Test (System Admin)
* @since 7.3.5
* @version 1.0
* @author Kianoush Nesvaderani
* @requires 
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	/**
	*@description Logs the user into Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - System Administration');
	},
	
	/**
	*@description Searches the customer and navigates to its page
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigate to Customer Admin" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("#search", 1000)
		.click("#search")
		.setValue("#search", "oranj")
		.useXpath()
		.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'oranj')]", 2000)
		.click("//a[@class='ui-corner-all'][contains(text(),'oranj')]")
		.click("//a[@id='btnGo']");
	},
	
	/**
	*@description Navigates to the Flex Credentials page
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigate to Flex Credentials Section" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[contains(text(),'Licenses')]", 10000)
		.click("//a[contains(text(),'Licenses')]")
		.pause(1000)
		.click("//a[contains(text(),'Integrations')]");
	},
	
	/**
	*@description Clicks on the Connection Test button
	*@param browser - The web browser object
	*@returns void
	**/
	"Test Connection" : function (browser) {
		browser
		.useCss()
		.pause(5000)
		.click("#flexTestButton")
		.pause(10000)
	},
	
	/**
	*@description Verifies the success icon and message produced
	*@param browser - The web browser object
	*@returns void
	**/
	"Verify the Success Icon and Message" : function (browser) {
		browser
		.useCss()
		.assert.elementPresent("section.flexTestIcon.success")
		.assert.containsText("#flexTestButtonResult", "Successful Connection");
	},
	
	/**
	*@description Logs the user out of Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Logout" : function (browser) {
		browser.logout();
	},
};