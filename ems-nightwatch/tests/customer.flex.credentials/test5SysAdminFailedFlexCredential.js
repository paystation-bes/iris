/**
* @summary Flex: Flex Credentials: Failed Flex Credentials Edit (System Admin)
* @since 7.3.5
* @version 1.0
* @author Kianoush Nesvaderani
* @requires 
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	/**
	*@description Logs the user into Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - System Administration');
	},
	
	/**
	*@description Searches the customer and navigates to its page
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigate to Customer Admin" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("#search", 1000)
		.click("#search")
		.setValue("#search", "oranj")
		.useXpath()
		.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'oranj')]", 2000)
		.click("//a[@class='ui-corner-all'][contains(text(),'oranj')]")
		.click("//a[@id='btnGo']");
	},
	
	/**
	*@description Navigates to the Flex Credentials page
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigate to Flex Credentials Section" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[contains(text(),'Licenses')]", 10000)
		.click("//a[contains(text(),'Licenses')]")
		.pause(1000)
		.click("//a[contains(text(),'Integrations')]");
	},
	
	/**
	*@description Enters the wrong Flex Credentials to the Edit window and Save
	*@param browser - The web browser object
	*@returns void
	**/
	"Edit and Save" : function (browser) {
		browser
		.useCss()
		.pause(5000)
		.click("#btnEditFlexCredentialsSysAdmin")
		.pause(3000)
		.waitForElementVisible("#formUsername", 10000)
		.setValue("#formPassword", "123")
		.pause(2000)
		.click(".linkButtonFtr.textButton.save")
		.pause(10000);
	},
	
	/**
	*@description Verifies the failure message produced
	*@param browser - The web browser object
	*@returns void
	**/
	"Verify the Error Message" : function (browser) {
		browser
		.useCss()
		.assert.containsText("article.scrollToTop", "We are sorry but the provided Flex credentials did not pass the connection tests. Please check that the provided information is correct and try again.");
	},
	
	/**
	*@description Logs the user out of Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Logout" : function (browser) {
		browser.logout();
	},
};