/**
* @summary Flex: Flex Credentials: Failed Flex Credentials Edit
* @since 7.3.5
* @version 1.0
* @author Kianoush Nesvaderani
* @requires 
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("FlexChildName");
var password = data("Password");


module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	/**
	*@description Logs the user into Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	/**
	*@description Navigates the user to the Global tab
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigate to Global tab" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//img[@alt='Settings']", 5000)
		.click("//img[@alt='Settings']")
		.waitForFlex()
	},
	
	/**
	*@description Enters the wrong Flex Credentials to the Edit window and Save
	*@param browser - The web browser object
	*@returns void
	**/
	"Enter Flex Credentials and Save" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("#btnEditFlexCredentials", 10000)
		.click("#btnEditFlexCredentials")
		.waitForElementVisible("#formUsername", 1000)
		.setValue("#formPassword", "123")
		.click(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active")
		.pause(5000);
	},
	
	/**
	*@description Verifies the failure message produced
	*@param browser - The web browser object
	*@returns void
	**/
	"Verify the Failure Message" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("article.scrollToTop", 10000)
		.assert.containsText("article.scrollToTop", "We are sorry but the provided Flex credentials did not pass the connection tests. Please check that the provided information is correct and try again.");
	},
	
	/**
	*@description Logs the user out of Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Logout" : function (browser) {
		browser.logout();
	},
};
