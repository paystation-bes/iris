/*
Prerequisite of EditCardType and DeleteCard Type:
AddCardType.js
 */

var data = require('../../data.js');

var serverURL = data("URL");
var username = data("ChildUserName");
var password = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) {
		browser
		.url(serverURL)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Entering Settings" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='bottomSection']/a[@title='Settings']", 1000)
		.click("//section[@class='bottomSection']/a[@title='Settings']"); 
		
	},
	
	"Click on Card Settings" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[contains(text(),'Card Settings')]", 1000)
		.click("//a[contains(text(),'Card Settings')]");
	},
	
	"Click Add Card Type" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@id='btnAddCardType']", 1000)
		.click("//a[@id='btnAddCardType']")	
	},
	
	"Entering Card Type Info" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementVisible("//input[@id='formCardTypeName']", 3000)
		.pause(1000)
		.setValue("//input[@id='formCardTypeName']", 'Blackboard External Srvr')
		.assert.visible("//input[@id='formCardTypeTrack2']")
		.setValue("//input[@id='formCardTypeTrack2']", '123456789123456')
		.assert.visible("//input[@id='formCardTypeAuthMethod']")
		.click("//input[@id='formCardTypeAuthMethod']")
		.waitForElementVisible("//a[contains(text(),'Internal List')]", 1000)
		.click("//a[contains(text(),'Internal List')]")
		.pause(1000);
		},
	
	"Add Card Type Save" : function (browser) {
		browser
		.useXpath()
		.click("(//button[@type='button'])[2]")
		.pause(3000)
		.waitForElementVisible("//p[contains(text(),'123456789123456')]",3000)
		},
		
			"Logout" : function (browser) {
		browser.logout();
	},
};