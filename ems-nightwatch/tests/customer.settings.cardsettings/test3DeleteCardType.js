/*Requires prerequisites Add Card Type and Edit Card Type*/

var data = require('../../data.js');

var serverURL = data("URL");
var username = data("ChildUserName");
var password = data("Password");

GLOBAL.widgetId = "";
GLOBAL.sectionId = "";

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) 
	{
		browser	
			.url(serverURL)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
		"Click on Settings in the Sidebar" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//nav[@id='main']/section[@class='bottomSection']/a[@title='Settings']/img", 2000)
			.click("//nav[@id='main']/section[@class='bottomSection']/a[@title='Settings']/img")
			.pause(1000)
	},

			"Click on the Card Settings tab" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='secondNavArea']//a[@title='Card Settings']", 2000)
			.click("//section[@id='secondNavArea']//a[@title='Card Settings']")
			.pause(1000)
	},
	
		"Assert existence of Track 2 pattern 123456789123456" : function (browser)
		
	{
		browser
			.useXpath()
			.waitForElementVisible("//ul[@id='cardType']//p[contains (.,'123456789123456')]", 2000)
			.pause(1000)
			
	},	
	
		"Click on the Option Menu for the Card Type with Track 2 pattern 123456789123456" : function (browser)
		
	{
		browser
			.useXpath()
			.waitForElementVisible("//ul[@id='cardType']//p[contains (.,'123456789123456')]/../../a[@title='Option Menu']", 2000)
			.click("//ul[@id='cardType']//p[contains (.,'123456789123456')]/../../a[@title='Option Menu']")
			
	},	

		"Click Delete within the Option Menu" : function (browser)
		
	{
		browser
			.useXpath()
			.waitForElementVisible("//ul[@id='cardType']//p[contains (.,'123456789123456')]/../../following-sibling::section[@class='ddMenu optionMenu']/section[@class='menuOptions innerBorder']/a[@title='Delete']", 2000)
			.click("//ul[@id='cardType']//p[contains (.,'123456789123456')]/../../following-sibling::section[@class='ddMenu optionMenu']/section[@class='menuOptions innerBorder']/a[@title='Delete']")
			.pause(1000)
			
	},		
	
		"Confirm the delete" : function (browser)
	
		{
		browser
			.useXpath()
			.waitForElementVisible("//body[@id='settings']//button[@type='button']/span[contains (text(),'Delete')]", 2000)
			.click("//body[@id='settings']//button[@type='button']/span[contains (text(),'Delete')]")
			.pause(5000)
			
	},

		"Assert that the Card Type was deleted" : function (browser)
		
	{
		browser
			.useXpath()
			.assert.elementNotPresent("//ul[@id='cardType']//p[contains (.,'123456789123456')]")
			.pause(1000)
			
	},	

		"Logout" : function (browser) 
	{
		browser.logout();
	},
	
};	