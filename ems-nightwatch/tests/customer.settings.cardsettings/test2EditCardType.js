/*
Prerequisite Scripts:
test1AddCardType.js
 */

var data = require('../../data.js');

var serverURL = data("URL");
var username = data("ChildUserName");
var password = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) {
		browser
		.url(serverURL)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Navigate to Settings" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("img[alt='Settings']", 2000)
		.click("img[alt='Settings']")
	},
	"Navigate to Card Settings" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[contains(text(),'Card Settings')]", 2000)
		.click("//a[contains(text(),'Card Settings')]")
		.pause(1000)
	},
	
	"Choose and Edit a Card" : function (browser) {
		browser
		.useCss()
		.click("img[title='Option Menu']")
		.click("a.edit.btnEditCardType")
		.setValue("#formCardTypeName", '1')
		.useXpath()
		.click("(//button[@type='button'])[2]")
		.pause(5000);
	},
	
	"Edit and Save a Card" : function (browser) {
		browser
		.useCss()
		.assert.containsText("#cardType>li>div.col1>p", "1Blackboard External Srvr");
        
	},
	
	"Logout" : function (browser) {
		browser.logout();
	},
};