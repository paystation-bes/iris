/**
 * @summary Example of how to use navigateTo command
 * @since 7.3.5
 * @version 1.0
 * @author  Thomas C
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
var SmokeTag = data('SmokeTag');

module.exports = {
	tags: [SmokeTag, 'sanity'],
	
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"Login" : function (browser) {
			browser
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - Main (Dashboard)');
		},

		/**
		 * @description Navigates to Passcards
		 * @param browser - The web browser object
		 * @returns void
		 **/
		"Navigate to Passcards" : function (browser) {
			browser.navigateTo("Accounts");
			browser.navigateTo("Passcards");
		},
		
		
		/**
		 * @description Navigates to Extend by Phone
		 * @param browser - The web browser object
		 * @returns void
		 **/
		"Navigate to Extend By Phone" : function (browser) {
			browser.navigateTo("Settings");
			browser.navigateTo("Locations");
			browser.navigateTo("Extend By Phone");
			},

	}
