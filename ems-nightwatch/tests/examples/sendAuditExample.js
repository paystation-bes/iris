/**
 * @summary Example of how to Start a New Script
 * @since 7.4.0
 * @version 1.0
 * @author  Thomas C
 **/


var data = require('../../data.js');

//save anything you need from the data file into a variable
var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");

module.exports = {
  
    "Encrypt Card": function(browser) {
        browser.sendCCPreAuth('4.00','4275330012345675');
    },

};