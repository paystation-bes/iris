/**
 * @summary Example of how to Start a New Script
 * @since 7.4.0
 * @version 1.0
 * @author  Thomas C
 **/


var data = require('../../data.js');

//save anything you need from the data file into a variable
var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");

module.exports = {
    tags: ['smoke'],
    /**
     * @description Logs the user into Iris
     * @param browser - The web browser object
     * @returns void
     **/
    "ExampleScript: Login": function(browser) {
        browser
            .url(devurl)
            .windowMaximize('current')
            .login(username, password)
            .assert.title('Digital Iris - Main (Dashboard)');
    },

    /*
     * Your Code Goes Here
     */


    /**
     * @description Logs the user out of Iris
     * @param browser - The web browser object
     * @returns void
     **/
    "ExampleScript: Logout": function(browser) {
        browser.logout();
    },

}