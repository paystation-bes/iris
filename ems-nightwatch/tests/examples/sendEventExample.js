/**
 * @summary Example of how to use navigateTo command
 * @since 7.3.5
 * @version 1.0
 * @author  Thomas C
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
var billCanisterRemoved = {
		type : 'BillCanister',
		action : 'Removed',
		information : ''
	};

	var coinEscrowJam = {
		type : 'CoinEscrow',
		action : 'Jam',
		information : ''
	};

	var coinEscrowJamCleared = {
		type : 'CoinEscrow',
		action : 'JamCleared',
		information : ''
	};
module.exports = {
	
	
	tags : ['smoke'],

	

	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)')
	},
	/**
	 * @description Sends all required events for this script
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"Send all events for this script" : function (browser) {
		browser
		//Event objects are packed into an array
		.sendEvent([billCanisterRemoved, coinEscrowJam])
	},

	"Navigate to Settings" : function (browser) {
		browser.
		navigateTo('Settings');
	},

	"Send all events for this script2" : function (browser) {

		browser
		//Event objects are packed into an array
		.sendEvent([coinEscrowJamCleared])

	},
}
