/**
 * @summary Testing triggering and clearing user-defined communication and collection alerts on pay station details page
 * @since 7.4.3
 * @version 1.0
 * @author Nadine B
 * @see EMS-9617
 **/

var data = require('../../data.js');

var devurl = data("URL");

var customerUsername = data("AdminUserName");
var password = data("Password");

var defaultPassword = "password";

var randomNumber = Math.floor(Math.random() * 100) + 1;

var collectionRouteName = "collectionRoute" + randomNumber;
var collectionAlertName = "collectionAlert" + randomNumber;
var maintenanceRouteName = "maintenanceRoute" + randomNumber;
var payStationAlertName = "payStationAlert" + randomNumber;
var addRoute = ".//*[@id='btnAddRoute']";
var addAlert = ".//*[@id='btnAddAlert']";
var routeFormName = "//*[@id='formRouteName']";
var alertFormName = "//*[@id='formAlertName']";
var routeFormType = ".//*[@id='formRouteType']";
var alertFormType = ".//*[@id='formAlertType']";
var alertCoinExceeds = ".//*[@id='formDisplayExceedAmount']";
var alertCounts = ".//li[@class='ui-menu-item']/a[contains(text(), 'Count')]";
var alertFormRoute = ".//*[@id='formAlertRoute']";
var saveRouteButton = ".//*[@id='routeSettings']/section[3]/a[contains(text(), 'Save')]";
var saveAlertButton = ".//*[@id='alertSettings']/section[2]/a[contains(text(), 'Save')]";

var payStation1 = "500000070001";
var payStation2 = "500000070002";

var payStationList = ".//*[@id='paystationList']";
var payStationElement = payStationList + "/span/li/section/span[contains(text(), '";
var payStationDetails = ".//*[@id='detailHeading' and contains(text(), 'Pay Station Details')]";
var recentActivity = ".//*[@id='alertsBtn']/a[contains(text(), 'Recent Activity')]";
var recentAlert = "//ul[@id='crntAlertList']/span/li/section/div[contains(@class, 'col1')]/p[contains(text(), '";
var recentAlertActive = "')]/../../div[contains(@class, 'col4')]/p[contains(text(), 'Activ')]";
var recentAlertCleared = "')]/../../div[contains(@class, 'col4')]/p[contains(text(), 'Resolved')]";
var recentAlertClearedByAdmin = "//div[contains(.,'Coin Canister Removed')]/../div[contains(.,'Administrator')]";
var allPayStations = [payStation1, payStation2];

module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	 *@description Sign into user account
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test1TriggerAndClearUserDefinedPayStationAndCollAlertsPayStationDetails: Sign into customer account" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(customerUsername, password, defaultPassword);

	},

	/**
	 *@description Create a Collection route and assign pay stations to it
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test1TriggerAndClearUserDefinedPayStationAndCollAlertsPayStationDetails: Create a collection route" : function (browser) {

		browser
		.useXpath()
		.pause(1000)
		.navigateTo("Settings")
		.pause(1000)
		.navigateTo("Routes")
		.pause(1000)
		.waitForElementVisible(addRoute, 4000)
		.click(addRoute)

		.pause(1000)
		.waitForElementVisible(routeFormName, 4000)
		.setValue(routeFormName, collectionRouteName)
		.pause(1000)
		.setValue(routeFormType, "Collections")
		.pause(1000)
		.waitForElementVisible(".//li[@class='ui-menu-item']/a[contains(text(), 'Collections')]", 4000)
		.click(".//li[@class='ui-menu-item']/a[contains(text(), 'Collections')]")
		.pause(1000)
		.waitForElementVisible(".//ul[@id='routePSFullList']/li[contains(text(), '" + payStation1 + "')]", 2000)
		.click(".//ul[@id='routePSFullList']/li[contains(text(), '" + payStation1 + "')]")
		.pause(1000)
		.waitForElementVisible(".//ul[@id='routePSFullList']/li[contains(text(), '" + payStation2 + "')]", 2000)
		.click(".//ul[@id='routePSFullList']/li[contains(text(), '" + payStation2 + "')]")
		.pause(1000)
		.click(saveRouteButton)

		.pause(2000)
		.waitForElementVisible(".//*[@id='routeList']/li/div/p[contains(text(), '" + collectionRouteName + "')]", 2000)
		.assert.elementPresent(".//*[@id='routeList']/li/div/p[contains(text(), '" + collectionRouteName + "')]");
	},

	/**
	 *@description Create a Maintenance route and assign pay stations to it
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test1TriggerAndClearUserDefinedPayStationAndCollAlertsPayStationDetails: Create a maintenance route" : function (browser) {

		browser
		.useXpath()
		.pause(1000)
		.waitForElementVisible(addRoute, 4000)
		.click(addRoute)

		.pause(1000)
		.waitForElementVisible(routeFormName, 4000)
		.setValue(routeFormName, maintenanceRouteName)
		.pause(1000)
		.setValue(routeFormType, "Maintenance")
		.pause(1000)
		.waitForElementVisible(".//li[@class='ui-menu-item']/a[contains(text(), 'Maintenance')]", 4000)
		.click(".//li[@class='ui-menu-item']/a[contains(text(), 'Maintenance')]")
		.pause(1000)
		.waitForElementVisible(".//ul[@id='routePSFullList']/li[contains(text(), '" + payStation1 + "')]", 2000)
		.click(".//ul[@id='routePSFullList']/li[contains(text(), '" + payStation1 + "')]")
		.pause(1000)
		.waitForElementVisible(".//ul[@id='routePSFullList']/li[contains(text(), '" + payStation2 + "')]", 2000)
		.click(".//ul[@id='routePSFullList']/li[contains(text(), '" + payStation2 + "')]")
		.pause(1000)
		.click(saveRouteButton)
		.pause(2000)
		.waitForElementVisible(".//*[@id='routeList']/li/div/p[contains(text(), '" + maintenanceRouteName + "')]", 3000)
		.assert.elementPresent(".//*[@id='routeList']/li/div/p[contains(text(), '" + maintenanceRouteName + "')]");
	},

	/**
	 *@description Create a collection alert
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test1TriggerAndClearUserDefinedPayStationAndCollAlertsPayStationDetails: Create a collection alert" : function (browser) {

		browser
		.useXpath()
		.pause(1000)
		.navigateTo("Alerts")
		.pause(1000)
		.waitForElementVisible(addAlert, 4000)
		.click(addAlert)

		.pause(1000)
		.waitForElementVisible(alertFormName, 4000)
		.setValue(alertFormName, collectionAlertName)
		.pause(1000)
		.click(alertFormType)
		.waitForElementPresent(".//li[@class='ui-menu-item']/a[contains(text(), 'Coin Canister')]", 4000)
		.click(".//li[@class='ui-menu-item']/a[contains(text(), 'Coin Canister')]")
		.pause(1000)
		.click(alertCoinExceeds)
		.setValue(alertCoinExceeds, "10")
		.pause(1000)
		.click(".//*[@id='formDisplayExceedTypeExpand']")

		.waitForElementPresent(alertCounts, 4000)
		.click(alertCounts)

		.pause(1000)
		.click(alertFormRoute)
		.waitForElementVisible("//li[@class='ui-menu-item']/a[contains(text(), '" + collectionRouteName + "')]", 4000)
		.click("//li[@class='ui-menu-item']/a[contains(text(), '" + collectionRouteName + "')]")

		.click(saveAlertButton)
		
		.waitForElementPresent(".//*[@id='alertList']/li/div/p[contains(text(), '" + collectionAlertName + "')]", 3000)
		.assert.elementPresent(".//*[@id='alertList']/li/div/p[contains(text(), '" + collectionAlertName + "')]");
	},

	/**
	 *@description Create a pay station alert
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test1TriggerAndClearUserDefinedPayStationAndCollAlertsPayStationDetails: Create a pay station alert" : function (browser) {

		browser
		.useXpath()

		.pause(1000)
		.click(addAlert)

		.pause(1000)
		.waitForElementVisible(alertFormName, 4000)
		.setValue(alertFormName, payStationAlertName)
		.pause(1000)
		.click(alertFormType)
		.waitForElementPresent(".//li[@class='ui-menu-item']/a[contains(text(), 'Pay Station Alert')]", 4000)
		.click(".//li[@class='ui-menu-item']/a[contains(text(), 'Pay Station Alert')]")

		.pause(1000)
		.click(alertFormRoute)
		.waitForElementVisible("//li[@class='ui-menu-item']/a[contains(text(), '" + maintenanceRouteName + "')]", 4000)
		.click("//li[@class='ui-menu-item']/a[contains(text(), '" + maintenanceRouteName + "')]")

		.click(saveAlertButton)

		.pause(2000)
		.assert.elementPresent(".//*[@id='alertList']/li/div/p[contains(text(), '" + payStationAlertName + "')]");
	},

	//NOTE: You cannot set an earlier timestamp with a Heartbeat so you cannot test communication alerts

	//NOTE: Cannot test collection alerts because the widget values are cached for 5 minutes


	/**
	 *@description Sends a pay station event for 'coin cannister removed'
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test1TriggerAndClearUserDefinedPayStationAndCollAlertsPayStationDetails: Send a 'Coin Canister Removed' event" : function (browser) {

		for (i = 0; i < allPayStations.length; i++) {
			browser.createNewPayStationAlert(devurl, '', allPayStations[i], 'Event', 'CoinCanister', 'Removed', '');

		}

	},

	/**
	 *@description Verify that the pay station alert got triggered
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test1TriggerAndClearUserDefinedPayStationAndCollAlertsPayStationDetails: Verify that the pay station alert was triggered" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.navigateTo("Alerts")
		.pause(2000)
		.navigateTo("Pay Stations")
		.pause(2000)

		for (i = 0; i < allPayStations.length; i++) {
			browser
			.useXpath()
			.waitForElementPresent(payStationElement + allPayStations[i] + "')]/..", 3000)
			.assert.elementPresent(payStationElement + allPayStations[i] + "')]/..");

			browser.getAttribute(payStationElement + allPayStations[i] + "')]/..", "class", function (result) {
				//The severity should not change because pay station alerts' severity < collection alerts
				console.log("Expected severity = severityLow");
				console.log("Actual severity = " + result.value);
				browser.assert.equal(result.value, 'severityLow');
			});

			browser
			.pause(1000)
			.click(payStationElement + allPayStations[i] + "')]")
			.waitForElementPresent(payStationDetails, 4000)
			.waitForElementPresent(recentActivity, 4000)
			.click(recentActivity)

			//Assume most recent alerts show up at the top
			//Only the default pay station alert name will show up here
			.waitForElementPresent(recentAlert + 'Coin Canister Removed' + recentAlertActive, 4000)
			.assert.elementPresent(recentAlert + 'Coin Canister Removed' + recentAlertActive);
			
			//NOTE: Uncomment when UI is done

			var size = 0;
			//Assert that there is only one coin canister alert active
			browser.elements("xpath", recentAlert + 'Coin Canister Removed' + recentAlertActive, function (result) {
				size += result.value.length;
				console.log("SIZE IS: " + result.value.length);
				
				browser.assert.equal(result.value.length, 1);
			});
			
			browser.navigateTo("Clear")
			.pause(1000)
			.click("//a[text()='Reload']")
			.pause(2000)
			.waitForElementVisible(recentAlertClearedByAdmin, 3000)
			.assert.elementNotPresent(recentAlert + 'Coin Canister Removed' + recentAlertActive);
		}

	},

		
	/**
	 *@description Logs the customer out
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test1TriggerAndClearUserDefinedPayStationAndCollAlertsPayStationDetails: Logs the customer out" : function (browser) {

		browser.logout();
	}
};
