/**
 * @summary Testing triggering and clearing user-defined communication and collection alerts on location details and maintenance page
 * @since 7.4.3
 * @version 1.0
 * @author Johnson N, Nadine B
 * @see EMS-9617
 **/

var data = require('../../data.js');

var devurl = data("URL");

var adminUsername = data("AdminUserName");

var adminPassword = data("Password");
var defaultPassword = data("DefaultPassword");

var randomNumber = Math.floor(Math.random() * 100) + 1;
var locationName = "Location" + randomNumber;

var payStation1 = "500000070001";
var payStation2 = "500000070002";

var paystationList = ".//*[@id='paystationList']";
var locationList = ".//*[@id='locationList']";
var payStationElement = paystationList + "/span/li/section/span[contains(text(), '";
var addLocation = ".//*[@id = 'btnAddLocation']";
var locationNameField = ".//*[@id='formLocationName']";
var operatingModeDropbox = ".//*[@id='formOperatingMode']"
var capacityField = ".//*[@id='formCapacity']";
var saveLocationButton = ".//*[@id='locationForm']/section[7]/a[contains(text(), 'Save')]";
var maintenanceLocationSearch = "//*[@id='locationRouteAC']";

var allPayStations = [payStation1, payStation2];

module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	"test3TriggerAndClearPaystationAlertsLocationDetails: Sign into customer account and checks that the test pay stations are present" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(adminUsername, adminPassword, defaultPassword)
		.navigateTo("Settings")
		.navigateTo("Pay Stations")
		.pause(1000)
		.assert.elementPresent(paystationList);
		//assert all pay stations are there
		for (i = 0; i < allPayStations.length; i++) {
			browser.assert.elementPresent(payStationElement + allPayStations[i] + "')]");
		}
	},

	/**
	 *@description Sends heartbeats to old pay stations
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test3TriggerAndClearPaystationAlertsLocationDetails: Send a heartbeat alert to make sure there are no communication alerts currently triggered" : function (browser) {

		for (i = 0; i < allPayStations.length; i++) {
			browser
			.createNewPayStationAlert(devurl, '', allPayStations[i], 'HeartBeat', '', '')

			//NOTE: Uncomment when UI is fixed
			browser.getAttribute(payStationElement + allPayStations[i] + "')]/..", "class", function (result) {
				browser.assert.equal(result.value, 'severityNull')
			});
		}

	},

	/**
	 *@description Navigate to Locations page, create new Locations for Paystations
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test3TriggerAndClearPaystationAlertsLocationDetails: Navigate to Locations Page and make new Location" : function (browser) {
		browser
		.navigateTo("Locations")
		.pause(1000)
		.assert.elementPresent(locationList)
		.useXpath()
		.waitForElementVisible(addLocation, 4000)
		.click(addLocation)

		.pause(1000)
		.waitForElementVisible(locationNameField, 4000)
		.setValue(locationNameField, locationName)
		.pause(1000)
		.setValue(operatingModeDropbox, "Multiple")
		.pause(1000)
		.setValue(capacityField, "100")
		.pause(1000)
		.click(".//ul[@id='locPSFullList']/li[contains(text(), '" + payStation1 + "')]")
		.pause(1000)
		.click(".//ul[@id='locPSFullList']/li[contains(text(), '" + payStation2 + "')]")
		.pause(1000)
		.click(saveLocationButton)
		.pause(5000)
		.assert.elementPresent(".//ul[@id='locationList']/li[contains(@title, '" + locationName + "')]");
	},

	/**
	 *@description Send Maintenance Door Open Alert to Paystation 1
	 *@param browser - The web browser object
	 *@returns void
	 **/

	"test3TriggerAndClearPaystationAlertsLocationDetails: Send Alert - Maintenance Door Open to Paystation 1" : function (browser) {

		browser.createNewPayStationAlert(devurl, '', payStation1, 'Event', 'Paystation', 'DoorOpened', 'Maintenance');
	},

	/**
	 *@description Refresh Locations Page and check severity for Location
	 *@param browser - The web browser object
	 *@returns void
	 **/

	"test3TriggerAndClearPaystationAlertsLocationDetails: Send Alert - Refresh Location Page and check Location Severity 1" : function (browser) {

		browser
		.navigateTo("Pay Stations")
		.pause(1000)
		.navigateTo("Locations")
		.pause(1000)
		.assert.elementPresent(".//ul[contains(@id, 'locationList')]/li[contains(@title, '" + locationName + "')]/section[contains(@class, 'severityFlag1')]");

	},

	/**
	 *@description Verify Location Severity in Maintenance page
	 *@param browser - The web browser object
	 *@returns void
	 **/

	"test3TriggerAndClearPaystationAlertsLocationDetails: ***Send Alert - Verify the Location Severity is 1 in Maintenance Page" : function (browser) {

		var searchLocation = "//li[contains(@class, 'ui-menu-item')]/a[contains(text(), '";
		var locationPos = "//ul[contains(@id, 'activeList')]/span/li/section[contains(@class, 'severityLow')]/h3[contains(text(), '";
		var locPosMaintenance = "//ul[contains(@id, 'activeList')]/span/li";
		browser
		.pause(1000)
		.navigateTo("Maintenance")
		.pause(1000)
		.waitForElementVisible(maintenanceLocationSearch, 4000)
		.pause(1000)
		.click(maintenanceLocationSearch)
		.setValue(maintenanceLocationSearch, locationName)
		.pause(1000)
		.waitForElementVisible(searchLocation + locationName + "')]", 4000)
		.click(searchLocation + locationName + "')]")
		.pause(1500)
		.assert.elementPresent(locationPos + payStation1 + "')]");

		browser.elements("xpath", locPosMaintenance, function (result) {
			browser.assert.equal(result.value.length, 1);
		})
		browser
		.pause(1000)
		.navigateTo("Settings");

	},

	/**
	 *@description Send Bill Stacker Full Alert to Paystation 2
	 *@param browser - The web browser object
	 *@returns void
	 **/

	"test3TriggerAndClearPaystationAlertsLocationDetails: Send Alert - Bill Stacker Full to Paystation 2" : function (browser) {

		browser.createNewPayStationAlert(devurl, '', payStation2, 'Event', 'BillStacker', 'Full', '');

	},

	/**
	 *@description Refresh Locations Page and check severity for Location
	 *@param browser - The web browser object
	 *@returns void
	 **/

	"test3TriggerAndClearPaystationAlertsLocationDetails: Send Alert - Refresh Location Page and check Location Severity 2" : function (browser) {

		browser
		.navigateTo("Pay Stations")
		.pause(1000)
		.navigateTo("Locations")
		.pause(1000)
		.assert.elementPresent(".//ul[contains(@id, 'locationList')]/li[contains(@title, '" + locationName + "')]/section[contains(@class, 'severityFlag2')]");

	},

	/**
	 *@description Verify Location Severity in Maintenance page
	 *@param browser - The web browser object
	 *@returns void
	 **/

	"test3TriggerAndClearPaystationAlertsLocationDetails: ***Send Alert - Verify the Location Severity is now 2 for pay station 2 in Maintenance Page" : function (browser) {

		var searchLocation = "//li[contains(@class, 'ui-menu-item')]/a[contains(text(), '";
		var locationPos = "//ul[contains(@id, 'activeList')]/span/li/section[contains(@class, '";
		var severityMedium = "severityMedium')]/h3[contains(text(), '";
		var severityLow = "severityLow')]/h3[contains(text(), '";
		var locPosMaintenance = "//ul[contains(@id, 'activeList')]/span/li";
		browser
		.pause(1000)
		.navigateTo("Maintenance")
		.pause(1000)
		.waitForElementVisible(maintenanceLocationSearch, 4000)
		.pause(1000)
		.click(maintenanceLocationSearch)
		.setValue(maintenanceLocationSearch, locationName)
		.pause(1000)
		.waitForElementVisible(searchLocation + locationName + "')]", 4000)
		.click(searchLocation + locationName + "')]")
		.pause(1500)
		.assert.elementPresent(locationPos + severityLow + payStation1 + "')]")
		.pause(1000)
		.assert.elementPresent(locationPos + severityMedium + payStation2 + "')]");

		browser.elements("xpath", locPosMaintenance, function (result) {
			browser.assert.equal(result.value.length, 2);
		})
		browser
		.pause(1000)
		.navigateTo("Settings");

	},
	/**
	 *@description Send Shock Alarm On Alert to Paystation 1
	 *@param browser - The web browser object
	 *@returns void
	 **/

	"test3TriggerAndClearPaystationAlertsLocationDetails: Send Alert - Shock Alarm On to Paystation 1" : function (browser) {

		browser.createNewPayStationAlert(devurl, '', payStation1, 'Event', 'Paystation', 'ShockOn', '');

	},

	/**
	 *@description Refresh Locations Page and check severity for Location
	 *@param browser - The web browser object
	 *@returns void
	 **/

	"test3TriggerAndClearPaystationAlertsLocationDetails: Send Alert - Refresh Location Page and check Location Severity 3" : function (browser) {

		browser
		.navigateTo("Pay Stations")
		.pause(1000)
		.navigateTo("Locations")
		.pause(1000)
		.assert.elementPresent(".//ul[contains(@id, 'locationList')]/li[contains(@title, '" + locationName + "')]/section[contains(@class, 'severityFlag3')]");

	},

	/**
	 *@description Verify Location Severity in Maintenance page
	 *@param browser - The web browser object
	 *@returns void
	 **/

	"test3TriggerAndClearPaystationAlertsLocationDetails: ***Send Alert - Verify the Location Severity is now 3 for pay station 1 in Maintenance Page" : function (browser) {

		var searchLocation = "//li[contains(@class, 'ui-menu-item')]/a[contains(text(), '";
		var locationPos = "//ul[contains(@id, 'activeList')]/span/li/section[contains(@class, '";
		var severityHigh = "severityHigh')]/h3[contains(text(), '";
		var severityMedium = "severityMedium')]/h3[contains(text(), '";
		var locPosMaintenance = "//ul[contains(@id, 'activeList')]/span/li";
		browser
		.pause(1000)
		.navigateTo("Maintenance")
		.pause(1000)
		.waitForElementVisible(maintenanceLocationSearch, 4000)
		.pause(1000)
		.click(maintenanceLocationSearch)
		.setValue(maintenanceLocationSearch, locationName)
		.pause(1000)
		.waitForElementVisible(searchLocation + locationName + "')]", 4000)
		.click(searchLocation + locationName + "')]")
		.pause(1500)
		.assert.elementPresent(locationPos + severityHigh + payStation1 + "')]")
		.pause(1000)
		.assert.elementPresent(locationPos + severityMedium + payStation2 + "')]");

		browser.elements("xpath", locPosMaintenance, function (result) {
			browser.assert.equal(result.value.length, 2);
		})
		browser
		.pause(1000)
		.navigateTo("Settings");

	},

	/**
	 *@description Send Shock Alarm Off Alert to Paystation 1
	 *@param browser - The web browser object
	 *@returns void
	 **/

	"test3TriggerAndClearPaystationAlertsLocationDetails: Send Alert - Shock Alarm Off to Paystation 1" : function (browser) {

		browser.createNewPayStationAlert(devurl, '', payStation1, 'Event', 'Paystation', 'ShockOff', '');

	},

	/**
	 *@description Refresh Locations Page and check severity for Location
	 *@param browser - The web browser object
	 *@returns void
	 **/

	"test3TriggerAndClearPaystationAlertsLocationDetails: Send Alert - Refresh Location Page and check Location Severity 4" : function (browser) {

		browser
		.navigateTo("Pay Stations")
		.pause(1000)
		.navigateTo("Locations")
		.pause(1000)
		.assert.elementPresent(".//ul[contains(@id, 'locationList')]/li[contains(@title, '" + locationName + "')]/section[contains(@class, 'severityFlag2')]");

	},

	/**
	 *@description Verify Location Severity in Maintenance page
	 *@param browser - The web browser object
	 *@returns void
	 **/

	"test3TriggerAndClearPaystationAlertsLocationDetails: ***Send Alert - Verify the Location Severity is back to 1 for pay station 1 in Maintenance Page" : function (browser) {

		var searchLocation = "//li[contains(@class, 'ui-menu-item')]/a[contains(text(), '";
		var locationPos = "//ul[contains(@id, 'activeList')]/span/li/section[contains(@class, '";
		var severityMedium = "severityMedium')]/h3[contains(text(), '";
		var severityLow = "severityLow')]/h3[contains(text(), '";
		var locPosMaintenance = "//ul[contains(@id, 'activeList')]/span/li";
		browser
		.pause(1000)
		.navigateTo("Maintenance")
		.pause(1000)
		.waitForElementVisible(maintenanceLocationSearch, 4000)
		.pause(1000)
		.click(maintenanceLocationSearch)
		.setValue(maintenanceLocationSearch, locationName)
		.pause(1000)
		.waitForElementVisible(searchLocation + locationName + "')]", 4000)
		.click(searchLocation + locationName + "')]")
		.pause(1500)
		.assert.elementPresent(locationPos + severityLow + payStation1 + "')]")
		.pause(1000)
		.assert.elementPresent(locationPos + severityMedium + payStation2 + "')]");

		browser.elements("xpath", locPosMaintenance, function (result) {
			browser.assert.equal(result.value.length, 2);
		})
		browser
		.pause(1000)
		.navigateTo("Settings");

	},

	/**
	 *@description Send Bill Stacker Full Cleared Alert to Paystation 2
	 *@param browser - The web browser object
	 *@returns void
	 **/

	"test3TriggerAndClearPaystationAlertsLocationDetails: Send Alert - Bill Stacker Full Cleared to Paystation 2" : function (browser) {

		browser.createNewPayStationAlert(devurl, '', payStation2, 'Event', 'BillStacker', 'FullCleared', '');

	},

	/**
	 *@description Refresh Locations Page and check severity for Location
	 *@param browser - The web browser object
	 *@returns void
	 **/

	"test3TriggerAndClearPaystationAlertsLocationDetails: Send Alert - Refresh Location Page and check Location Severity 5" : function (browser) {

		browser
		.navigateTo("Pay Stations")
		.pause(1000)
		.navigateTo("Locations")
		.pause(1000)
		.assert.elementPresent(".//ul[contains(@id, 'locationList')]/li[contains(@title, '" + locationName + "')]/section[contains(@class, 'severityFlag1')]");

	},

	/**
	 *@description Verify Location Severity in Maintenance page
	 *@param browser - The web browser object
	 *@returns void
	 **/

	"test3TriggerAndClearPaystationAlertsLocationDetails: ***Send Alert - Verify the Location Severity is now 0 for pay station 2 in Maintenance Page" : function (browser) {

		var searchLocation = "//li[contains(@class, 'ui-menu-item')]/a[contains(text(), '";
		var locationPos = "//ul[contains(@id, 'activeList')]/span/li/section[contains(@class, '";
		var severityLow = "severityLow')]/h3[contains(text(), '";
		var severityNull = "severityNull')]/h3[contains(text(), '";
		var locPosMaintenance = "//ul[contains(@id, 'activeList')]/span/li";
		browser
		.pause(1000)
		.navigateTo("Maintenance")
		.pause(1000)
		.waitForElementVisible(maintenanceLocationSearch, 4000)
		.pause(1000)
		.click(maintenanceLocationSearch)
		.setValue(maintenanceLocationSearch, locationName)
		.pause(1000)
		.waitForElementVisible(searchLocation + locationName + "')]", 4000)
		.click(searchLocation + locationName + "')]")
		.pause(1500)
		.assert.elementPresent(locationPos + severityLow + payStation1 + "')]")
		.pause(1000)
		.assert.elementNotPresent(locationPos + severityNull + payStation2 + "')]");

		browser.elements("xpath", locPosMaintenance, function (result) {
			browser.assert.equal(result.value.length, 1);
		})
		browser
		.pause(1000)
		.navigateTo("Settings");

	},

	/**
	 *@description Send Maintenance Door Closed Alert to Paystation 1
	 *@param browser - The web browser object
	 *@returns void
	 **/

	"test3TriggerAndClearPaystationAlertsLocationDetails: Send Alert - Maintenance Door Closed to Paystation 1" : function (browser) {

		browser.createNewPayStationAlert(devurl, '', payStation1, 'Event', 'Paystation', 'DoorClosed', 'Maintenance');

	},

	/**
	 *@description Refresh Locations Page and check severity for Location
	 *@param browser - The web browser object
	 *@returns void
	 **/

	"test3TriggerAndClearPaystationAlertsLocationDetails: Send Alert - Refresh Location Page and check Location Severity 6" : function (browser) {

		browser
		.navigateTo("Pay Stations")
		.pause(1000)
		.navigateTo("Locations")
		.pause(1000)
		.assert.elementPresent(".//ul[contains(@id, 'locationList')]/li[contains(@title, '" + locationName + "')]/section[contains(@class, 'severityFlag0')]");

	},

	/**
	 *@description Verify Location Severity in Maintenance page
	 *@param browser - The web browser object
	 *@returns void
	 **/

	"test3TriggerAndClearPaystationAlertsLocationDetails: ***Send Alert - Verify the Location doesn't have any alerts in Maintenance Page" : function (browser) {

		var searchLocation = "//li[contains(@class, 'ui-menu-item')]/a[contains(text(), '";
		var locationPos = "//ul[contains(@id, 'activeList')]/span/li/section[contains(@class, '";
		var severityNull = "severityNull')]/h3[contains(text(), '";
		var locPosMaintenance = "//ul[contains(@id, 'activeList')]/span/li";
		browser
		.pause(1000)
		.navigateTo("Maintenance")
		.pause(1000)
		.waitForElementVisible(maintenanceLocationSearch, 4000)
		.pause(1000)
		.click(maintenanceLocationSearch)
		.setValue(maintenanceLocationSearch, locationName)
		.pause(1000)
		.waitForElementVisible(searchLocation + locationName + "')]", 4000)
		.click(searchLocation + locationName + "')]")
		.pause(1500)
		.assert.elementNotPresent(locationPos + severityNull + payStation1 + "')]")
		.assert.elementPresent("//*[@id='activeList']/span/li[contains(text(), 'No alerts')]");

		//'No alerts' message is an element
		browser.elements("xpath", locPosMaintenance, function (result) {
			browser.assert.equal(result.value.length, 1);
		});

	},

	/**
	 *@description Logs the customer out
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test3TriggerAndClearPaystationAlertsLocationDetails: Logs the customer out" : function (browser) {

		browser.logout();
	}
};
