/**
 * @summary Alert testing set up
 * @since 7.4.1
 * @version 1.0
 * @author Nadine B
 * @see EMS-9617
 **/

var data = require('../../data.js');

var devurl = data("URL");



var customerUsername = data("AdminUserName");
var password = data("Password");
var newUserPassword = "password";

var payStationList = ".//*[@id='paystationList']";
var payStationElement = payStationList + "/span/li/section/span[contains(text(), '";

var allPayStations = [];

module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	 *@description Sign into user account and navigate to Pay Station Details
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test0SetUp: Sign into customer account and checks that the test pay stations are present" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		
		.initialLogin(customerUsername, password, newUserPassword)
		
		.navigateTo("Settings")
		.navigateTo("Pay Stations")
		.pause(1000)
		.assert.elementPresent(payStationList);
		
		var size = 0;
		//populate pay station list
		browser.elements("xpath", "//ul[contains(@id, 'paystationList')]/span/li", function (result) {
			size += result.value.length;
			console.log("SIZE IS: " + size);
			console.log(result.value);
			
			for (var i = 1; i <= size; i++ ) {
			//add all pay stations
				browser
				.useXpath()
				.getText("//ul[contains(@id, 'paystationList')]/span/li[" + i + "]/section/span", function (pos) {
					allPayStations.push(pos.value);
					console.log("PUSHED: " + pos.value);
				});
			}
		});
	},
	
	
	/**
	 *@description Sends heartbeats to old pay stations
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test0SetUp: Send a heartbeat alert to make sure there are no communication alerts currently triggered" : function (browser) {
		
		for	(i = 0; i < allPayStations.length; i++)	{
			browser
			.createNewPayStationAlert(devurl, '', allPayStations[i], 'HeartBeat', '', '', '');
		}
	},
	
	
	/**
	 *@description Check what other alerts might be present
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test0SetUp: Check if any other alerts are present" : function (browser) {
		
		browser
		.useXpath()
		.pause(1000)
		.assert.elementPresent(payStationList);
	
		for	(i = 0; i < allPayStations.length; i++)	{
			browser.detectAndClearAlertsForPayStation(allPayStations[i]);	
		}
	},
	
	
	/**
	 *@description Verify that all pay station alerts got cleared
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test0SetUp: Verify that all pay station alerts are cleared" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.navigateTo("Locations")
		.pause(1500)
		.navigateTo("Pay Stations")
		.pause(1500)

		for	(i = 0; i < allPayStations.length; i++)	{
			browser.getAttribute(payStationElement + allPayStations[i] + "')]/..", "class", function(result) {
				browser.assert.equal(result.value, 'severityNull')
			  });
		}
		
	},
	
	
	/**
	 *@description Logs the user out
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test0SetUp: Logs out of the customer account" : function (browser) {
		browser.logout();
	}
};