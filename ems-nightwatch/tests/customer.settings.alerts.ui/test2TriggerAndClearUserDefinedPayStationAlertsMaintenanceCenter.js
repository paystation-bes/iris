/**
 * @summary Testing triggering and clearing user-defined communication and
 *          collection alerts on Maintenance Center page
 * @since 7.4.3
 * @version 1.0
 * @author Nadine B
 * @see EMS-9617
 * @depends on test1TiggerAndClearUserDefinedPayStationAlerts
 */

var data = require('../../data.js');

var devurl = data("URL");
var customerUsername = data("AdminUserName");
var password = data("Password");

var defaultPassword = "password";

var payStation1 = "500000070001";
var payStation2 = "500000070002";

var payStationList = ".//*[@id='paystationList']";
var payStationElement = payStationList + "/span/li/section/span[contains(text(), '";

var noAlertsMessage = ".//*[@id='activeList']/span/li[contains(text(), 'No alerts found')]";
var activePos = "//ul[@id='activeList']/span/li/section[contains(@class, 'severityLow')]/h3[contains(text(), '";
var coinCanisterAlert = "')]/../span[contains(@class, 'alertType') and contains(text(), 'Coin Canister')]";
var recentAlert = "//ul[@id='crntAlertList']/span/li/section/div[contains(@class, 'col1')]/p[contains(text(), '";
var recentAlertActive = "')]/../../div[contains(@class, 'col4')]/p[contains(text(), 'Activ')]";
var recentAlertCleared = "')]/../../div[contains(@class, 'col4')]/p[contains(text(), 'Clear')]";
var historyAlertStart = ".//ul[@id='recentList']/span/li/section/h3[contains(text(), '";
var historyAlertEnd = "')]/../span[contains(@class, 'alertHistory') and contains(text(), '";
var maintenanceAlertStart = "//ul[@id='activeList']/span/li/section/h3[contains(text(), '";
var maintenanceAlertEnd = "')]/../span[contains(text(), '";
var recentAlertClearedBySystem = "//div[contains(.,'Coin Canister Removed')]/../div[contains(.,'System')]";
var recentAlertClearedByAdmin = "//div[contains(.,'Coin Canister Removed')]/../div[contains(.,'Administrator')]";
var allPayStations = [payStation1, payStation2];

module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	 * @description Sign into user account
	 * @param browser -
	 *            The web browser object
	 * @returns void
	 */
	"test2TriggerAndClearUserDefinedPayStationAlertsMaintenanceCenter: Sign into customer account" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')

		.initialLogin(customerUsername, password, defaultPassword);

	},
	
	/**
	 * @description Sends a pay station event for 'coin cannister removed'
	 * @param browser -
	 *            The web browser object
	 * @returns void
	 */
	"test1TriggerAndClearUserDefinedPayStationAndCollAlertsPayStationDetails: Send a 'Coin Canister Removed' event 2" : function (browser) {

		for (i = 0; i < allPayStations.length; i++) {
			browser.createNewPayStationAlert(devurl, '', allPayStations[i], 'Event', 'CoinCanister', 'Removed', '');

		}

	},
	
	/**
	 * @description Verify that the appropriate pay stations appear in
	 *              Maintenance Center
	 * @param browser -
	 *            The web browser object
	 * @returns void
	 */
	"test2TriggerAndClearUserDefinedPayStationAlertsMaintenanceCenter: Verify all the alerts got triggered in the Maintenance Center" : function (browser) {

		browser
		.useXpath()
		.pause(1000)
		.navigateTo('Maintenance')
		.waitForElementNotPresent(noAlertsMessage, 2000)
		.assert.elementNotPresent(noAlertsMessage);

		for (i = 0; i < allPayStations.length; i++) {
			browser
			.pause(1000)
			.assert.elementPresent(activePos + allPayStations[i] + "')]")
			.waitForElementPresent(maintenanceAlertStart + allPayStations[i] + maintenanceAlertEnd + 'Coin Canister' + "')]", 3000)
			.assert.elementPresent(maintenanceAlertStart + allPayStations[i] + maintenanceAlertEnd + 'Coin Canister' + "')]")
// .assert.elementPresent(activePos + allPayStations[i] + coinCanisterAlert +
// "')]");
			
			// Uncomment when UI is done
			browser
			.navigateTo('Resolved Alerts')
			.pause(1000)
			.waitForElementNotPresent(historyAlertStart + allPayStations[i] + historyAlertEnd + coinCanisterAlert + "')]", 2000)
			.assert.elementNotPresent(historyAlertStart + allPayStations[i] + historyAlertEnd + coinCanisterAlert + "')]")
			.pause(1000)
			.navigateTo('Maintenance');
			
		}

	},

	/**
	 * @description Clear alert for payStation1
	 * @param browser -
	 *            The web browser object
	 * @returns void
	 */
	"test2TriggerAndClearUserDefinedPayStationAlertsMaintenanceCenter: Clear alert for the first pay station" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.navigateTo('Settings')
		.pause(1000)
		.navigateTo('Pay Stations')
		.pause(1000)
		.waitForElementPresent(".//*[@id='paystationList']/span/li/section/span[contains(text(), '500000070001')]/..", 3000)
		.detectAndClearAlertsForPayStation(payStation1);

	},

	/**
	 * @description Verify that all pay station alerts got cleared
	 * @param browser -
	 *            The web browser object
	 * @returns void
	 */
	"test2TriggerAndClearUserDefinedPayStationAlertsMaintenanceCenter: Verify that the alerts on the first pay station got cleared" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.navigateTo("Locations")
		.pause(2000)
		.navigateTo("Pay Stations")
		.pause(1000);
		browser.getAttribute(payStationElement + allPayStations[0] + "')]/..", "class", function (result) {
			browser.assert.equal(result.value, 'severityNull');
		});

		for (i = 1; i < allPayStations.length; i++) {
			browser.getAttribute(payStationElement + allPayStations[i] + "')]/..", "class", function (result) {
				browser.assert.equal(result.value, 'severityLow');
			});

		}
		
		
		// CLICK ON THE FIRST PAY STATION AND GO RECENT ACTIVITY
		browser
		.pause(1000)
		.click(payStationElement + allPayStations[0] + "')]")
		.pause(1000)
		.navigateTo('Recent Activity')
		.waitForElementPresent(recentAlertClearedBySystem, 2000)
		.assert.elementPresent(recentAlertClearedBySystem)
		.assert.elementNotPresent(recentAlert + 'Coin Canister Removed' + recentAlertActive);
		},

	/**
	 * @description Verify that the appropriate pay stations appear in
	 *              Maintenance Center
	 * @param browser -
	 *            The web browser object
	 * @returns void
	 */
	"test2TriggerAndClearUserDefinedPayStationAlertsMaintenanceCenter: Verify only the first pay station's alerts are cleared in the Maintenance Center" : function (browser) {

		browser
		.useXpath()
		.pause(1000)
		.navigateTo('Maintenance')
		.pause(1000)
		.waitForElementNotPresent(noAlertsMessage, 2000)
		.assert.elementNotPresent(noAlertsMessage);

		browser
		.waitForElementNotPresent(activePos + allPayStations[0] + "')]", 3000)
		.assert.elementNotPresent(activePos + allPayStations[0] + "')]")
		.assert.elementNotPresent(activePos + allPayStations[0] + coinCanisterAlert);

		for (i = 1; i < allPayStations.length; i++) {
			browser
			.assert.elementPresent(activePos + allPayStations[i] + "')]")
			.assert.elementPresent(activePos + allPayStations[i] + coinCanisterAlert);
		}
		
		
		// GO TO RESOLVED ALERTS
		
		
		
		// Uncomment when UI is done
		browser
		.navigateTo('Resolved Alerts')
// .assert.elementPresent(clearedResolvedAlertStart + allPayStations[0] +
// clearedResolvedAlertEnd)
// .assert.elementNotPresent(activePos + allPayStations[0] + coinCanisterAlert)
		.waitForElementPresent("//ul[@id='recentList']//h3[contains(text(), '"+ allPayStations[0] + "')]/../span[contains(@class, 'alertType') and contains(text(), 'Coin Canister')]", 4000)
		.assert.elementPresent("//ul[@id='recentList']//h3[contains(text(), '"+ allPayStations[0] + "')]/../span[contains(@class, 'alertType') and contains(text(), 'Coin Canister')]")
		
// .elements("xpath", recentAlert + 'Coin Canister Removed' + recentAlertActive,
// function (result) {
// size += result.value.length;
// console.log("SIZE IS: " + size);
//			
// browser.assert.equal(size, 0);
// });

	},
		
	
	/**
	 * @description Clears an active alert and verifies the Resolved Alert
	 *              Details
	 * @param browser -
	 *            The web browser object
	 * @returns void
	 */
	"test2TriggerAndClearUserDefinedPayStationAlertsMaintenanceCenter: Clear alert and verify resolved alert details" : function (browser) {
		browser
		.useXpath()
		.navigateTo("Current Alerts")
		.pause(1000)
		.click("//section[@class='severityLow clickable']")
		.pause(1000)
		.navigateTo("Resolve")
		.pause(1000)
		.navigateTo("Resolved Alerts")
		.waitForElementPresent("//section[@class='severityLow clickable']", 2000)
		.click("//section[@class='severityLow clickable']")
		.waitForElementVisible("//ul[@id='alertDetailTable']//dd[contains(text(),'Administrator')]", 3000)
		.assert.visible("//ul[@id='alertDetailTable']//dd[contains(text(),'Administrator')]");
	},

	
	
	/**
	 * @description Check what alerts are present
	 * @param browser -
	 *            The web browser object
	 * @returns void
	 */
	"test2TriggerAndClearUserDefinedPayStationAlertsMaintenanceCenter: Check pay stations with active alerts and clear them all" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.navigateTo('Settings')
		.pause(1000)
		.navigateTo('Pay Stations')
		.pause(1000)
		.assert.elementPresent(payStationList);

		for (i = 0; i < allPayStations.length; i++) {

			browser.detectAndClearAlertsForPayStation(allPayStations[i]);
		}

	},

	/**
	 * @description Verify that the appropriate pay stations appear in
	 *              Maintenance Center
	 * @param browser -
	 *            The web browser object
	 * @returns void
	 */
	"test2TriggerAndClearUserDefinedPayStationAlertsMaintenanceCenter: Verify all the alerts are cleared in the Maintenance Center" : function (browser) {

		browser
		.useXpath()
		.pause(1000)
		.navigateTo('Maintenance')
		.waitForElementPresent(noAlertsMessage, 2000)
		.assert.elementPresent(noAlertsMessage);

		for (i = 0; i < allPayStations.length; i++) {
			browser
			.pause(1000)
			.assert.elementNotPresent(maintenanceAlertStart + allPayStations[i] + maintenanceAlertEnd + 'Coin Canister' + "')]")
			
			.assert.elementNotPresent(activePos + allPayStations[i] + "')]")
			.assert.elementNotPresent(activePos + allPayStations[i] + coinCanisterAlert);
			
			browser
			.navigateTo('Resolved Alerts')
			.waitForElementPresent("//ul[@id='recentList']//li//h3[contains(text(),'" + allPayStations[i] + coinCanisterAlert, 4000)
			.assert.elementPresent("//ul[@id='recentList']//li//h3[contains(text(),'" + allPayStations[i] + coinCanisterAlert)
			.pause(1000)
			.navigateTo('Maintenance')
			
			.pause(1000)
			
// browser.elements("xpath", maintenanceAlertStart + allPayStations[i] +
// maintenanceAlertEnd + 'Coin Canister' + "')][1]", function (result) {
//				
// console.log("SIZE IS: " + result.value.length);
//				
// browser.assert.equal(result.value.length, 0);
// });
			
		}

	},

	/**
	 * @description Logs the customer out
	 * @param browser -
	 *            The web browser object
	 * @returns void
	 */
	"test2TriggerAndClearUserDefinedPayStationAlertsMaintenanceCenter: Logs the customer out" : function (browser) {

		browser.logout();
	}
};
