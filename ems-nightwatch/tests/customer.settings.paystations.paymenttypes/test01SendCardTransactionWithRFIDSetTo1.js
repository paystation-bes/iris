/**
 * @summary Extend credit card processing to identify the credit card payment mode
 * @since 7.5
 * @version 1.0
 * @author Johnson N
 * @see US-1475
 **/
 
var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
var defaultpassword = data("DefaultPassword");
var paystationSerialNumber = "500000070001";
module.exports = {
	tags : ['smoketag', 'completetesttag'],
	
	/**
	*@description Sends Card Transaction with RFID set to 1
	*@param browser - The web browser object
	*@returns void
	**/
	"Send Pre-Auth and Post-auth card transaction with RFID tag set to 1": function(browser){
		browser
		.sendCCPreAuthRFID("20.00", "5175750492432123", paystationSerialNumber, "1", function(result){
			var CCtrans = {
				licenseNum : '',
				stallNum : '1',
				type : 'Regular',
				originalAmount : '2000',
				chargedAmount : '20.00',
				cashPaid : '0.00',
				cardPaid : '20.00',
				cardAuthorizationId: result.authID,
				emsPreAuthId: result.emsPreAuthID,
			}
			
		browser.sendCCPostAuth([CCtrans], paystationSerialNumber);	
			
			
		});
	},
	
	/**
	*@description Logs the user into Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Login the User to Iris": function(browser){
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, defaultpassword)
		.assert.title('Digital Iris - Main (Dashboard)');
	},
	
		/**
	*@description Navigates to the Pay Station
	*@param browser - The web browser object
	*@returns void
	**/
	
	"Navigates to Pay Station's Transaction Reports" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[contains(text(),'Settings')]", 5000)
		.click("//a[contains(text(),'Settings')]")
		.waitForElementVisible("//a[contains(text(),'Pay Stations')]", 5000)
		.click("//a[contains(text(),'Pay Stations')]")
		.waitForElementVisible("//span[contains(text(),'" + paystationSerialNumber +"')]", 5000)
		.click("//span[contains(text(),'" + paystationSerialNumber + "')]")
		.useCss()
		.waitForElementVisible("#reportsBtn > a.tab", 5000)
		.click("#reportsBtn > a.tab")
		.useXpath()
		.waitForElementVisible("//a[contains(text(),'Transaction Reports')]", 5000)
		.click("//a[contains(text(),'Transaction Reports')]")
		.click("//a[contains(text(),'Transaction Reports')]")
		.click("//a[contains(text(),'Transaction Reports')]")
		.click("//a[contains(text(),'Transaction Reports')]")
		.click("//a[contains(text(),'Transaction Reports')]");
	},
		
	"Verify the transaction made it to Iris with correct Payment Type in both list and report details" : function(browser){
		browser
		.useXpath()
		.waitForElementVisible("//p[contains(text(),'CC(Tap)')]", 5000)
		.click("//p[contains(text(),'CC(Tap)')]")
		.waitForElementVisible("//section[@class='reportBody']", 5000)
		.assert.visible("//section[@class='reportBody']/dl/dd[contains(text(), 'CC(Tap)')]");
	},
		
	/**
	*@description Logs the user out of Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Logout" : function (browser) {
		browser.logout();
	},
};