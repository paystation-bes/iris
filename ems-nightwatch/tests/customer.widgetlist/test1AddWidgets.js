/**
 * @summary Customer Admin: Citation widgets added and appear
 * @since 7.3.6
 * @version 1.0
 * @author Nadine B
 * @see US-897: TC1151, TC1152, TC1153, TC1154
 * 
 * Prerequisites: None
 **/

var data = require('../../data.js');

var devurl = data("URL");
var adminUsername = data("SysAdminUserName");
var password = data("Password");
var customer = "oranj";
var customerName = "Oranj Parent";
var customerLogin = data("AdminUserName");

/** Xpath constants **/
var customerSearchResults = "//*[@id='ui-id-2']/span[@class='info'][contains(text(),'";
var flexCheckbox = "//*[@id='subscriptionList:1700']";
var flexCheckboxChecked = "//*[@id='subscriptionList:1700' and contains(@class, 'checked')]";
var flexCheckboxUncheckedOrChecked = "//*[@id='subscriptionList:1700' and contains(@class, 'checkBox')]";
var updateCustomer = "//div[@class='ui-dialog-buttonset']/button/span[@class='ui-button-text'][contains(text(), 'Update Customer')]";
var citationElement = "//section[@class='widgetListContainer']/ul[@id='metricList14']/li/header[contains(text(),";
var citationMapElement = "//section[@class='widgetListContainer']/ul[@id='metricList15']/li/header[contains(text(),";
var citationWidgetOption = "//section[@class='widgetListContainer']/h3[contains(text(), 'Citations')]";
var citationMapWidgetOption = "//section[@class='widgetListContainer']/h3[contains(text(), 'Citation Map')]";
var citationWidget = "//section[@class='widgetObject']/section[@class='widget']/header/span[contains(text(),";
var addWidgetButton = "//section[@class='layout5 layout']/section[@class='sectionTools'][1]/section/a[@class='linkButtonIcn addWidget']";
var editDashboardButton = "//*[@id='headerTools']/a[@class='linkButtonFtr edit']";


module.exports = {
		tags: ['smoketag', 'completetesttag'],
		/**
		*@description Logs in as System Admin into Iris and changes customer's permissions to make sure FLEX is enabled
		*@param browser - The web browser object
		*@returns void
		**/
		"test1AddWidget: Change customer's FLEX permissions - ON" : function (browser) {
			browser.changeCustomersFlexPermissions(adminUsername, password, customer, customerName, true);
		},
		
		
		/**
		*@description Logs the customer with FLEX permissions into Iris
		*@param browser - The web browser object
		*@returns void
		**/
		"test1AddWidget: Login as customer with FLEX permissions" : function (browser) {
			browser
			.url(devurl)
			.windowMaximize('current')
			
			.pause(1000)
			.assert.title('Digital Iris - Login.')
			
			.pause(1000)
			.login(customerLogin, password)
			.pause(1000)
			.assert.title('Digital Iris - Main (Dashboard)');
		},
		
		/**
		*@description Edits the customer dashboard
		*@param browser - The web browser object
		*@returns void
		**/
		"test1AddWidget: Edit Dashboard - add Citation widget" : function (browser) {
			browser
			.pause(500)
			.useXpath()
			.waitForElementVisible(editDashboardButton, 2000)
			.click(editDashboardButton)
			
			.pause(1000)
			.waitForElementVisible(addWidgetButton, 2000)
			.click(addWidgetButton)
			
			.pause(1000)
			
			.waitForElementVisible(citationWidgetOption, 2000)
			.click(citationWidgetOption)
		},
		
		/**
		*@description Verifies citation options are present
		*@param browser - The web browser object
		*@returns void
		**/
		"test1AddWidget: Verify citation options" : function (browser) {
			browser
			.useXpath()
			.pause(1000)
			.assert.elementPresent(citationElement + " 'Citation by Hour')]")
			.assert.elementPresent(citationElement + " 'Citation by Day')]")
			.assert.elementPresent(citationElement + " 'Citation by Month')]")
			.assert.elementPresent(citationElement + " 'Citations by Citation Type')]")
			
		},
			
		/**
		*@description Clicks on 'Citation by Hour' option and selects that widget
		*@param browser - The web browser object
		*@returns void
		**/
		"test1AddWidget: Select 'Citation by Hour' from the menu" : function (browser) {
			browser
			.useXpath()
			.pause(1000)
			.click(citationElement + " 'Citation by Hour')]")
			
			.pause(500)
			.click("//div[@class='ui-dialog-buttonset']/button/span[contains(text(), 'Add widget')]")
			
			.pause(500)
			
		},
		
		/**
		*@description Clicks on 'Citation by Day' option and selects that widget
		*@param browser - The web browser object
		*@returns void
		**/
		"test1AddWidget: Select 'Citation by Day' from the menu" : function (browser) {
			browser.addCitationWidget(citationElement, "Citation by Day");
			
		},
		
		/**
		*@description Clicks on 'Citation by Month' option and selects that widget
		*@param browser - The web browser object
		*@returns void
		**/
		"test1AddWidget: Select 'Citation by Month' from the menu" : function (browser) {
			browser.addCitationWidget(citationElement, "Citation by Month");	
		},
		
		
		/**
		*@description Clicks on 'Citations by Citation Type' option and selects that widget
		*@param browser - The web browser object
		*@returns void
		**/
		"test1AddWidget: Select 'Citations by Citation Type' from the menu" : function (browser) {
			browser.addCitationWidget(citationElement, "Citations by Citation Type");	
		},
		
		
		/**
		*@description Verifies that all the widgets were added successfully
		*@param browser - The web browser object
		*@returns void
		**/
		"test1AddWidget: Verify widgets" : function(browser) {
			browser
			.useXpath()
			.pause(500)
			.assert.elementPresent(citationWidget + " 'Citation by Hour')]")
			.assert.elementPresent(citationWidget + " 'Citation by Day')]")
			.assert.elementPresent(citationWidget + " 'Citation by Month')]")
			.assert.elementPresent(citationWidget + " 'Citations by Citation Type')]")
			.pause(1000)
		},
		
		
		/**
		*@description Clicks on 'Citation Map' option, expands/minimizes/expands it, and selects that widget
		*@param browser - The web browser object
		*@returns void
		**/
		"test1AddWidget: Select 'Citation Map' from the menu" : function (browser) {
			browser
			.pause(1000)
			.useXpath()
			
			.pause(1000)
			.waitForElementVisible(addWidgetButton, 2000)
			.click(addWidgetButton)
			
			.pause(1000)
			
			.waitForElementVisible("//section[@class='widgetListContainer']/h3[contains(text()," + " 'Citation Map')]", 2000)
			
			//Expand widget
			.click(citationMapWidgetOption)
			.pause(500)
			.assert.visible(citationMapElement + " 'Citation Map')]")
			.elements("xpath","//section[@class='widgetListContainer']/ul[@id='metricList15']/li", function(result) {
				
				console.log("The number of elements is: " + result.value.length);
				browser.assert.equal(result.value.length, "1")
			});
			
			//Collapse widget
			browser
			.pause(500)
			.click(citationMapWidgetOption)
			.pause(500)
			.assert.hidden(citationMapElement + " 'Citation Map')]")
			
			//Expand widget again
			.click(citationMapWidgetOption)
			
			
			.pause(1000)
			.click(citationMapElement + " 'Citation Map')]")
			
			.pause(500)
			.click("//div[@class='ui-dialog-buttonset']/button/span[contains(text(), 'Add widget')]")
			
			.pause(500)
			.click("//a[@class='linkButtonFtr save']")
			.pause(500)
			
		},
		
		/**
		*@description Verifies that Citation Map is added
		*@param browser - The web browser object
		*@returns void
		**/
		"test1AddWidget: Verify Citation Map Widget is present" : function(browser) {
			browser
			.useXpath()
			.pause(500)
			.assert.elementPresent(citationWidget + " 'Citation Map')]")
			.pause(1000)
		},
		
		/**
		*@description Logs the user out of Iris
		*@param browser - The web browser object
		*@returns void
		**/
		"test1AddWidget: Log out" : function(browser) {
			browser.logout();
		}
		
		
};