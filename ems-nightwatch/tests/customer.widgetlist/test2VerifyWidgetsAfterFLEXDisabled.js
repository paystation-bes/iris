/**
 * @summary Customer Admin: Verifies that widgets cannot be readded after Flex disabled and deletes Citation Map widget
 * @since 7.3.6
 * @version 1.0
 * @author Nadine B
 * @see US-897: TC1157, TC1158, TC1161, TC1162
 * 
 * Prerequisites: addWidgets
 **/

var data = require('../../data.js');

var devurl = data("URL");
var adminUsername = data("SysAdminUserName");
var password = data("Password");
var customer = "oranj";
var customerName = "Oranj Parent";
var customerLogin = data("AdminUserName");

/** Xpath constants **/
var customerSearchResults = "//*[@id='ui-id-2']/span[@class='info'][contains(text(),'";
var flexCheckbox = "//*[@id='subscriptionList:1700']";
var flexCheckboxChecked = "//*[@id='subscriptionList:1700' and contains(@class, 'checked')]";
var flexCheckboxUncheckedOrChecked = "//*[@id='subscriptionList:1700' and contains(@class, 'checkBox')]";
var updateCustomer = "//div[@class='ui-dialog-buttonset']/button/span[@class='ui-button-text'][contains(text(), 'Update Customer')]";
var citationElement = "//section[@class='widgetListContainer']/ul[@id='metricList14']/li/header[contains(text(),";
var citationMapElement = "//section[@class='widgetListContainer']/ul[@id='metricList15']/li/header[contains(text(),";
var citationWidgetOption = "//section[@class='widgetListContainer']/h3[contains(text(), 'Citations')]";
var citationMapWidgetOption = "//section[@class='widgetListContainer']/h3[contains(text(), 'Citation Map')]";
var addWidgetButton = "//section[@class='layout5 layout']/section[@class='sectionTools'][1]/section/a[@class='linkButtonIcn addWidget']";
var editDashboardButton = "//*[@id='headerTools']/a[@class='linkButtonFtr edit']";

module.exports = {
		tags: ['smoketag', 'completetesttag'],
		/**
		*@description Logs in as System Admin into Iris and changes customer's permissions to make sure FLEX is enabled
		*@param browser - The web browser object
		*@returns void
		**/
		"test2VerifyWidgetsAfterFLEXDisabled: Change customer's FLEX permissions - ON" : function (browser) {
			browser.changeCustomersFlexPermissions(adminUsername, password, customer, customerName, true);
		},
		
		
		/**
		*@description Logs the customer with FLEX permissions into Iris
		*@param browser - The web browser object
		*@returns void
		**/
		"test2VerifyWidgetsAfterFLEXDisabled: Log in as the customer with FLEX permissions enabled" : function (browser) {
			browser
			.url(devurl)
			.windowMaximize('current')
			
			.pause(1000)
			.assert.title('Digital Iris - Login.')
			
			.pause(1000)
			.login(customerLogin, password)
			.pause(1000)
			.assert.title('Digital Iris - Main (Dashboard)');
		},
		
		
		/**
		*@description Verify that the Citation widget option exists for the customer
		*@param browser - The web browser object
		*@returns void
		**/
		"test2VerifyWidgetsAfterFLEXDisabled: Edit Dashboard - verify Citation widget is present" : function (browser) {
			browser
			.pause(500)
			.useXpath()
			.waitForElementVisible(editDashboardButton, 2000)
			.click(editDashboardButton)
			
			.pause(1000)
			.waitForElementVisible(addWidgetButton, 2000)
			.click(addWidgetButton)
			
			.pause(1000)
			
			.waitForElementVisible(citationWidgetOption, 2000)
			.click(citationWidgetOption)
			
			.pause(1000)
			.assert.elementPresent(citationElement + " 'Citation by Hour')]")
			.assert.elementPresent(citationElement + " 'Citation by Day')]")
			.assert.elementPresent(citationElement + " 'Citation by Month')]")
			.assert.elementPresent(citationElement + " 'Citations by Citation Type')]")
			
			.pause(1000)
		},
				
		
		/**
		*@description Logs the FLEX-enabled user out of Iris
		*@param browser - The web browser object
		*@returns void
		**/
		"test2VerifyWidgetsAfterFLEXDisabled: Log out of the FLEX customer account" : function(browser) {
			browser
			.logout()

		},
		
		
		/**
		*@description Logs in as System Admin into Iris and changes customer's permissions to make sure FLEX is DISabled
		*@param browser - The web browser object
		*@returns void
		**/
		"test2VerifyWidgetsAfterFLEXDisabled: Change customer's FLEX permissions - OFF" : function (browser) {
			browser.changeCustomersFlexPermissions(adminUsername, password, customer, customerName, false);
		},
		
		
		/**
		*@description Logs the customer without FLEX permissions into Iris
		*@param browser - The web browser object
		*@returns void
		**/
		"test2VerifyWidgetsAfterFLEXDisabled: Login as customer without FLEX permissions" : function (browser) {
			browser
			.url(devurl)
			.windowMaximize('current')
			
			.pause(1000)
			.assert.title('Digital Iris - Login.')
			
			.pause(1000)
			.login(customerLogin, password)
			.pause(2000)
			.assert.title('Digital Iris - Main (Dashboard)')
			.pause(1000);
		},
		
		
		/**
		*@description Edits the customer dashboard - attempts to add the Citation widget (should be missing)
		*@param browser - The web browser object
		*@returns void
		**/
		"test2VerifyWidgetsAfterFLEXDisabled: Edit Dashboard - attempt to add a Citation widget (unable to do so)" : function (browser) {
			browser
			.pause(1000)
			.useXpath()
			.waitForElementVisible(editDashboardButton, 2000)
			.click(editDashboardButton)
			
			.pause(1000)
			.waitForElementVisible(addWidgetButton, 2000)
			.click(addWidgetButton)
			
			.pause(1000)
			
			.assert.elementNotPresent(citationWidgetOption)
			
			.waitForElementPresent("//div[@class='ui-dialog-buttonset']/button/span[contains(text(), 'Cancel')]", 4000)
			.click("//div[@class='ui-dialog-buttonset']/button/span[contains(text(), 'Cancel')]")
			.waitForElementPresent("//a[@class='linkButton cancel']", 4000)

			.pause(1000)
			
			
		},

		
		/**
		*@description Deletes the Citation Map widget
		*@param browser - The web browser object
		*@returns void
		**/
		"test2VerifyWidgetsAfterFLEXDisabled: Delete Citation Map widget for the non-FLEX customer" : function(browser) {
			browser
			.deleteCitationWidget("Citation Map")
			.click("//a[@class='linkButtonFtr save']")
			.pause(1000)
		},
		
		
		/**
		*@description Verifies that Citation Map cannot be added back
		*@param browser - The web browser object
		*@returns void
		**/
		"test2VerifyWidgetsAfterFLEXDisabled: Verifies re-adding Citation Map is impossible" : function(browser) {
			browser
			.useXpath()
			.waitForElementVisible(editDashboardButton, 2000)
			.click(editDashboardButton)
			
			.pause(1000)
			.waitForElementVisible(addWidgetButton, 2000)
			.click(addWidgetButton)
			
			.pause(1000)
			
			.assert.elementNotPresent(citationMapWidgetOption)
			
			.waitForElementPresent("//div[@class='ui-dialog-buttonset']/button/span[contains(text(), 'Cancel')]", 4000)
			.click("//div[@class='ui-dialog-buttonset']/button/span[contains(text(), 'Cancel')]")
			.waitForElementPresent("//a[@class='linkButton cancel']", 4000)
			.click("//a[@class='linkButton cancel']")
			.pause(1000)
		},
		
		
		/**
		*@description Logs the user out of Iris
		*@param browser - The web browser object
		*@returns void
		**/
		"test2VerifyWidgetsAfterFLEXDisabled: Log out" : function(browser) {
			browser.logout();
		}
		
		
};