/**
 * @summary Customer Admin: Deletes Citations widgets and verifies other widgets are not affected
 * @since 7.3.6
 * @version 1.0
 * @author Nadine B
 * @see US-897: TC1163
 * 
 * Prerequisites: addWidgets, VerifyWidgetAfterFLEXDisabled
 **/

var data = require('../../data.js');

var devurl = data("URL");
var adminUsername = data("SysAdminUserName");
var password = data("Password");
var customer = "oranj";
var customerName = "Oranj Parent";
var customerLogin = data("AdminUserName");

var count = 0;
var allWidgets = [];
var allWidgetNames = [];
var citationNames = ["Citation by Hour", "Citation by Day", "Citation by Month", "Citations by Citation Type"]

/** Xpath constants **/
var customerSearchResults = "//*[@id='ui-id-2']/span[@class='info'][contains(text(),'";
var flexCheckbox = "//*[@id='subscriptionList:1700']";
var flexCheckboxChecked = "//*[@id='subscriptionList:1700' and contains(@class, 'checked')]";
var flexCheckboxUncheckedOrChecked = "//*[@id='subscriptionList:1700' and contains(@class, 'checkBox')]";
var updateCustomer = "//div[@class='ui-dialog-buttonset']/button/span[@class='ui-button-text'][contains(text(), 'Update Customer')]";
var citationElement = "//section[@class='widgetListContainer']/ul[@id='metricList14']/li/header[contains(text(),";
var citationWidgetOption = "//section[@class='widgetListContainer']/h3[contains(text(), 'Citations')]";
var citationWidget = "//section[@class='widgetObject']/section[@class='widget']/header/span[contains(text(),";
var addWidgetButton = "//section[@class='layout5 layout']/section[@class='sectionTools'][1]/section/a[@class='linkButtonIcn addWidget']";
var editDashboardButton = "//*[@id='headerTools']/a[@class='linkButtonFtr edit']";

module.exports = {
		tags: ['smoketag', 'completetesttag'],
		/**
		*@description Logs in as System Admin into Iris and changes customer's permissions to make sure FLEX is DISabled
		*@param browser - The web browser object
		*@returns void
		**/
		"test3VerifyOtherWidgets: Verify customer's FLEX permissions - OFF" : function (browser) {
			browser.changeCustomersFlexPermissions(adminUsername, password, customer, customerName, false);
		},
			
		
		/**
		*@description Logs the customer with FLEX permissions into Iris
		*@param browser - The web browser object
		*@returns void
		**/
		"test3VerifyOtherWidgets: Log in as the customer without FLEX permissions" : function (browser) {
			browser
			.url(devurl)
			.windowMaximize('current')
			
			.pause(1000)
			.assert.title('Digital Iris - Login.')
			
			.pause(1000)
			.login(customerLogin, password)
			
			.pause(1000)
			.assert.title('Digital Iris - Main (Dashboard)');
		},
		
		
		/**
		*@description Takes down the count and the names of the widgets currently present on the page
		*@param browser - The web browser object
		*@returns void
		**/
		"test3VerifyOtherWidgets: Count and note all other widgets" : function (browser) {
	
			browser
			.pause(500)
			.useXpath()
			
			
			.elements("xpath", "//section[@class='widget']", function (result) {
				count += result.value.length;
				console.log(count)	
	
			})

		},
		
		
		/**
		*@description Verify that the Citation widget option exists for the customer
		*@param browser - The web browser object
		*@returns void
		**/
		"test3VerifyOtherWidgets: Verify previous Citation widgets are present" : function (browser) {
			browser
			.pause(500)
			.useXpath()
			
			.assert.elementPresent(citationWidget + " 'Citation by Hour')]")
			.assert.elementPresent(citationWidget + " 'Citation by Day')]")
			.assert.elementPresent(citationWidget + " 'Citation by Month')]")
			.assert.elementPresent(citationWidget + " 'Citations by Citation Type')]")
			.pause(1000)
			
		},
		
		/**
		*@description Deletes Citations by Citation Type widget
		*@param browser - The web browser object
		*@returns void
		**/
		"test3VerifyOtherWidgets: Delete Citations by Citation Type from dashboard" : function(browser) {
			browser
			.waitForElementVisible(editDashboardButton, 2000)
			.click(editDashboardButton)
			.deleteCitationWidget("Citations by Citation Type")

		},
		
		
		/**
		*@description Deletes Citation by Month widget
		*@param browser - The web browser object
		*@returns void
		**/
		"test3VerifyOtherWidgets: Delete Citation by Month from dashboard" : function(browser) {
			browser.deleteCitationWidget("Citation by Month")
		},
		
		
		/**
		*@description Deletes Citation by Day widget
		*@param browser - The web browser object
		*@returns void
		**/
		"test3VerifyOtherWidgets: Delete Citation by Day from dashboard" : function(browser) {
			browser.deleteCitationWidget("Citation by Day")
		},
		
		/**
		*@description Deletes Citation by Hour widget
		*@param browser - The web browser object
		*@returns void
		**/
		"test3VerifyOtherWidgets: Delete Citation by Hour from dashboard" : function(browser) {
			browser
			.deleteCitationWidget("Citation by Hour")
			.click("//a[@class='linkButtonFtr save']")
		},
		
		
		/**
		*@description Verify that the Citation widget option exists for the customer
		*@param browser - The web browser object
		*@returns void
		**/
		"test3VerifyOtherWidgets: Verify Citation widgets are not present" : function (browser) {
			browser
			.pause(500)
			.useXpath()
			.assert.elementNotPresent(citationWidget + " 'Citation by Hour')]")
			.assert.elementNotPresent(citationWidget + " 'Citation by Day')]")
			.assert.elementNotPresent(citationWidget + " 'Citation by Month')]")
			.assert.elementNotPresent(citationWidget + " 'Citations by Citation Type')]")
			.pause(1000)
			
		},
		
		
		/**
		*@description Verify that the Citation widget option exists for the customer
		*@param browser - The web browser object
		*@returns void
		**/
		"test3VerifyOtherWidgets: Verify all other widgets are still present" : function (browser) {
			browser
			.pause(500)
			.useXpath()
			var widgetsLeft = 0;
			
			browser.elements("xpath", "//section[@class='widget']", function (result) {
				widgetsLeft += result.value.length;
				console.log(widgetsLeft)
				
				//Verify that the number of widgets is reduced by the number of
				//widgets we deleted (i.e., by 4)
				browser.assert.equal(widgetsLeft, (count - 4))
				
				
			})

			
		},
		
		
		/**
		*@description Logs the user out of Iris
		*@param browser - The web browser object
		*@returns void
		**/
		"test3VerifyOtherWidgets: Log out" : function(browser) {
			browser.logout();
		}
		
		
};