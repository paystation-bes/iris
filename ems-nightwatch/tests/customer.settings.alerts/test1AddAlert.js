var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";

var alertName = "Alert1";
var email = "thomas.chisholm@digitalpaytech.com";

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	"Click on Settings in the Sidebar" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@title='Settings']", 2000)
		.click("//a[@title='Settings']")
		.pause(1000)
		.waitForFlex()
	},

	"Click on 'Alert' Tab" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@class='tab' and text()='Alerts']", 1000)
		.click("//a[@class='tab' and text()='Alerts']")
		.pause(1000)
	},

	"Click on '+' Button" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible(".//*[@id='btnAddAlert']/img", 1000)
		.click(".//*[@id='btnAddAlert']/img")
		.pause(1000)
		.waitForElementVisible(".//*[@id='addHeading']", 1000)
	},

	"Enter Alert Name" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//input[@id='formAlertName']", 2000)
		.setValue("//input[@id='formAlertName']", alertName)
	},

	"Choose Threshold = Last Seen Interval" : function (browser) {
		browser
		.useCss()
		.verify.visible("#formAlertTypeExpand")
		.click("#formAlertTypeExpand")
		.pause(2000)

		.useXpath()
		.waitForElementVisible("//a[text()='Last Seen Interval']", 2000)
		.click("//a[text()='Last Seen Interval']")
		.pause(2000)
	},

	"Enter exceeds value" : function (browser) {
		browser
		.useCss()
		.verify.visible("#formDisplayExceedAmount")
		.setValue("#formDisplayExceedAmount", "2")
	},

	"Choose Route" : function (browser) {
		browser
		.useCss()
		.verify.visible("#formAlertRouteExpand")
		.click("#formAlertRouteExpand")

		.useXpath()
		.waitForElementVisible("//a[text()='All Pay Stations']", 2000)
		.click("//a[text()='All Pay Stations']")
	},

	"Add Notification Email" : function (browser) {
		browser
		.useCss()
		.pause(1000)
		.verify.visible("#newNotificationEmail")
		.setValue("#newNotificationEmail", email)
		.click("#alertEmailAddBtn")

		.useXpath()
		.waitForElementVisible("//ul[@id='formDisplayAlertNotifyList']//span[text()='thomas.chisholm@digitalpaytech.com']", 2000)
	},

	"Click Save" : function (browser) {
		var xpath = "//ul[@id='alertList']//p[text()='" + alertName + "']";

		browser
		.useXpath()
		.pause(1000)
		.assert.visible("//form[@id='alertSettings']/section[@class='btnSet']/a[text()='Save']")
		.assert.visible("//form[@id='alertSettings']/section[@class='btnSet']/a[text()='Cancel']")
		.useCss()
		.click(".linkButtonFtr.textButton.save")
		.pause(10000)
	},

	"Verify Details" : function (browser) {
		var detailName = "//dd[@id='detailAlertName' and text()='" + alertName + "']";
		var emailPath = "//ul[@id='detailNotificationList' and text()='" + email + "']";
		browser
		.useXpath()
		.pause(1000)
		.waitForElementVisible("//p[text()='Last Seen Interval']", 5000)
		.click("//p[text()='Last Seen Interval']")

		.waitForElementVisible(".//*[@id='alertDetails']", 2000)
		.waitForElementVisible(detailName, 1000)
		.waitForElementVisible("//dd[@id='detailAlertStatus' and text()='Enabled']", 1000)
		.waitForElementVisible(emailPath, 1000)
		.waitForElementVisible("//dd[@id='detailAlertRoute' and text()='All Pay Stations']", 1000)
		.waitForElementVisible("//strong[@id='detailAlertThresholdExceeds' and text()='2']", 1000)

	},

	"Logout" : function (browser) {
		browser.logout();
	},
};
