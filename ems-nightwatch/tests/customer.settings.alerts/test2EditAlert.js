var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";

var alertName = "Alert1";
var editedAlertName = "Alert1Edited";
var email = "thomas.chisholm@digitalpaytech.com";
module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	"Click on Settings in the Sidebar" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@title='Settings']", 2000)
		.click("//a[@title='Settings']")
		.pause(3000)
		.waitForFlex()
	},


	"Click on 'Alert' Tab" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@class='tab' and text()='Alerts']", 1500)
		.click("//a[@class='tab' and text()='Alerts']")
		.pause(5000)
	},

	"Click on Options Button" : function (browser) {
		var xpath = "(//ul[@id='alertList']//p[text()='" + alertName + "'])[1]/../following-sibling::a[@title='Option Menu']";
		browser
		.useXpath()
		.assert.visible(xpath)
		.click(xpath)
	},

	"Click Edit" : function (browser) {
	var xpath = "//ul[@id='alertList']//p[text()='" + alertName +"']/../../following-sibling::section[@class='ddMenu optionMenu']//section/a[@title='Edit']"
		browser
		.useXpath()
		.assert.visible("//ul[@id='alertList']//a[@title='Edit']")
		.click("//ul[@id='alertList']//a[@title='Edit']")
		.pause(3000)
	},

	"Edit Name" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("#formAlertName", 3000)
		.clearValue("#formAlertName")
		.setValue("#formAlertName", editedAlertName)
	},

	"Edit Threshold" : function (browser) {
		browser
		.useCss()
		.assert.visible("#formDisplayExceedAmount")
		.clearValue("#formDisplayExceedAmount")
		.setValue("#formDisplayExceedAmount", "4")
	},

	"Edit Route" : function (browser) {
		browser
		.useCss()
		.assert.visible("#formAlertRouteExpand")
		.click("#formAlertRouteExpand")

		.useXpath()
		.waitForElementVisible("//a[text()='Airport Collections']", 3000)
		.click("//a[text()='Airport Collections']")
	},

	"Remove Email Address" : function (browser) {
		browser
		.useXpath()
		.assert.visible("//ul[@id='formDisplayAlertNotifyList']/li[@class='selected']/span[text()='" + email + "']")
		.click("//ul[@id='formDisplayAlertNotifyList']/li[@class='selected']/span[text()='" + email + "']")
	},


	"Click Save" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.assert.visible("//form[@id='alertSettings']/section[@class='btnSet']/a[text()='Save']")
		.assert.visible("//form[@id='alertSettings']/section[@class='btnSet']/a[text()='Cancel']")
		.click("//form[@id='alertSettings']/section[@class='btnSet']/a[text()='Save']")
		.pause(5000)
	},
	
	"Verify Details" : function (browser) {
		var detailName = "//dd[@id='detailAlertName' and text()='" + editedAlertName + "']";
		var emailPath = "//ul[@id='detailNotificationList' and text()='" + email + "']";
		browser
		.useXpath()
		.pause(3000)
		.waitForElementVisible("//p[text()='Last Seen Interval']", 3000)
		.pause(3000)
		.waitForElementVisible(".//*[@id='alertDetails']", 2000)
		.waitForElementVisible(detailName, 3000)
		.waitForElementVisible("//dd[@id='detailAlertStatus' and text()='Enabled']", 3000)
		.waitForElementVisible("//dd[@id='detailAlertRoute' and text()='Airport Collections']", 3000)
		.waitForElementVisible("//strong[@id='detailAlertThresholdExceeds' and text()='4']", 3000)

	},

	"Logout" : function (browser) {
		browser.logout();
	},

};
