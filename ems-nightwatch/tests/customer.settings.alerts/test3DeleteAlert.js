var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";

var alertId;

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	"Click on Settings in the Sidebar" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@title='Settings']", 2000)
		.click("//a[@title='Settings']")
		.pause(1000)
		.waitForFlex()
	},


	"Click on 'Alert' Tab" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@class='tab' and text()='Alerts']", 1000)
		.click("//a[@class='tab' and text()='Alerts']")
		.pause(5000)
	},

	"Get Id Of Soon To Be Deleted Alert" : function (browser){
		browser.getAttribute("//ul[@id='alertList']/li[1]", "id", function(result){
			console.log(result.value);
			alertId = result.value;
		})
	},

	"Click on Options Button" : function (browser) {
		var xpath = "//li[@id='" + alertId + "']/a[@class='menuListButton menu']"
		browser
		.useXpath()
		.assert.visible(xpath)
		.click(xpath)
	},

	"Click Delete" : function (browser) {
	var xpath = "//li[@id='" + alertId + "']/following-sibling::section[1]/section/a[@class='delete btnDeleteRoute']";
		browser
		.useXpath()
		.click(xpath)
		.pause(1000)
	},

	"Confirm Delete" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//section[@id='messageResponseAlertBox']//article[text()='Are you sure you would like to delete this alert?']", 1000)
		.waitForElementVisible("//span[text()='Delete']", 1000)
		.click("//span[text()='Delete']")
		.pause(5000)
		.useCss()
		.assert.elementNotPresent("#" + alertId)
	},
	
	"Logout" : function (browser) {
		browser.logout();
	},
};
