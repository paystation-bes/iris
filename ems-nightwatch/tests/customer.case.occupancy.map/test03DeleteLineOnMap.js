/**
* @summary Settings: Location: Map Occupancy: EMS-7259
* @since 7.4.1
* @version 1.0
* @author Kianoush Nesvaderani
* @requires 
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("FlexChildName");
var password = data("Password");


module.exports = {
	tags : ['smoketag', 'completetesttag', 'featuretesttag'],
	/**
	*@description Logs the user into Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
		.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	/**
	*@description Navigates to Locations
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigates to Location" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//img[@alt='Settings']", 5000)
		.click("//img[@alt='Settings']")
		.waitForElementVisible("//a[contains(text(),'Locations')]", 5000)
		.click("//a[contains(text(),'Locations')]");
	},
	
	/**
	*@description Navigates the user to Edit Parking Area
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigate to Edit Parking Area" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//li[contains(@title, 'Mapped Occupancy Location ')]", 5000)
		.click("//li[contains(@title, 'Mapped Occupancy Location ')]")
		.useCss()
		.waitForElementVisible("#opBtn_pageContent", 5000)
		.click("#opBtn_pageContent")
		.waitForElementVisible("#menu_pageContent > section.menuOptions.innerBorder > a.edit.btnEditParkingLocation", 5000)
		.click("#menu_pageContent > section.menuOptions.innerBorder > a.edit.btnEditParkingLocation")
		.pause(5000);
	},
	
	/**
	*@description Deletes the line from the map
	*@param browser - The web browser object
	*@returns void
	**/
	"Delete the line from the map and Save" : function (browser) {
		browser
		.useCss()
		.click("#removeParking-Edit")
		.pause(3000)
		.click("path.leaflet-clickable")
		.click("#removeParking-Save")
		.click("#mapingComplete")
		.pause(5000);
	},
	
	/**
	*@description Verifies the deleted line does not show up
	*@param browser - The web browser object
	*@returns void
	**/
	"Assert the deletion" : function (browser) {
		browser
		.useCss()
		.assert.elementPresent("#mapHolder")
		.assert.elementNotPresent("svg.leaflet-zoom-animated");
	},
	
	/**
	*@description Logs the user out of Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Logout" : function (browser) {
		browser.logout();
	},
};
