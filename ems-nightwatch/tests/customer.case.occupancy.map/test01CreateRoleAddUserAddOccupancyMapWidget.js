/**
 * @summary CASE Integration: Occupancy Map Widget and Counts In The Cloud Data
 * @since 7.4.1
 * @version 1.0
 * @author Nadine B
 * @see US-53: EMS-7212
 **/

var data = require('../../data.js');

var devurl = data("URL");

var adminUsername = data("SysAdminUserName");
var customerUsername = data("AdminUserName");
var customer = "oranj";
var customerName = "Oranj Parent";
var password = data("Password");

var randomNumber = Math.floor((Math.random() * 100) + 1);
var newRoleName = "ManageLocationsRole" + randomNumber;
var newUserFirstName = "User" + randomNumber;
var newUserLastName = "Oranj" + randomNumber;
var newCustomerParent = "Oranj Parent";
var newCustomerTimeZone = "Canada/Pacific";
var newUserEmail = "user" + randomNumber + "@oranj";
var newCustomer = "customer" + randomNumber;
var newUserPassword = "Password$2";


var caseAccountSearchResults = "//*[@id='ui-id-5']/span[@class='info'][contains(text(),'";
var caseAccountBeginning = "Texas";
var caseAccount = "Texas A&M University";
var caseCredentialsGlobalSettings = "//*[@id='globalPreferences']/section/h3[contains(text(), 'Auto')]/strong[contains(text(), 'Count Credentials')]";
var caseCredentialsAccountName = "//dd[@id='autoCountCustName' and contains(text(), '" + caseAccount + "')]";
var caseAccountSearch = "//li[@class='ui-menu-item']/a[@class='ui-corner-all' and contains(text(), '" + caseAccount + "')]";


var settingsTab = "//a[contains(@title, 'Settings')]";
var locationsTab = "//a[contains(@title, 'Locations')]";
var licensesTab = "//a[contains(@title, 'Licenses')]";
var mobileLicensesTab = "//a[contains(@title, 'Mobile Licenses')]";
var integrationsTab = "//a[contains(@title, 'Integrations')]";
var usersTab = "//a[contains(@title, 'Users')]";
var rolesTab = "//a[contains(@title, 'Roles')]";
var locationDetailsTab = "//a[contains(@title, 'Location Details')]";
var locationsHeader = "//h2[contains(text(), 'Locations')]";

var addRole = "//*[@id='btnAddRole']";

module.exports = {
	tags : ['smoketag', 'completetesttag', 'featuretesttag'],

	/**
	 *@description Logs the child admin user into Iris and make sure the case subscription is on
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01CreateRoleAddUserAddOccupancyMapWidget: Make sure Customer's CASE Subscription is enabled" : function (browser) {
		browser.changeCustomersCasePermissions(true, adminUsername, password, customer, customer, true, false);
	},

	/**
	 *@description Navigates to Integrations tab
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01CreateRoleAddUserAddOccupancyMapWidget: Go to Licenses > Integrations" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@title='Licenses']", 2000)
		.click("//a[@title='Licenses']")

		.pause(2000)
		.waitForElementVisible("//a[@title='Integrations']", 4000)
		.click("//a[@title='Integrations']");
	},

	/**
	 *@description Assign a CASE account
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01CreateRoleAddUserAddOccupancyMapWidget: Assign CASE account 'Texas'" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.waitForElementVisible(licensesTab, 2000)
		.click(licensesTab)

		.pause(2000)
		.waitForElementVisible(integrationsTab, 4000)
		.click(integrationsTab)

		.pause(2000)
		.click("//*[@id='btnEditAutoCountCredentials']")
		.pause(1000)
		.waitForElementVisible("//input[@id='autoCountSearch']", 4000)

		//Clear the previous value from the search box
		.getValue("xpath", "//input[@id='autoCountSearch']", function (result) {
			if (result.value.length > 0) {
				browser.clearValue("//input[@id='autoCountSearch']");
			}
		});

		browser
		.pause(2000)
		.setValue("//input[@id='autoCountSearch']", caseAccountBeginning)
		.pause(1000)
		.waitForElementVisible(caseAccountSearch, 4000)
		.click(caseAccountSearch)
		.pause(1000)
		.click("//*[@id='autoCountSaveBtn']")
		.pause(2000)

		.assert.elementPresent(caseCredentialsAccountName)
		.pause(1000);

	},

	/**
	 *@description Switches tabs to verify that CASE account remains assigned
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01CreateRoleAddUserAddOccupancyMapWidget: Verify that the CASE account name is present even after switching the tabs" : function (browser) {
		browser
		.pause(1000)
		.useXpath()
		.click(mobileLicensesTab)
		.pause(1000)
		.click(licensesTab)
		.pause(1000)
		.waitForElementVisible(integrationsTab, 4000)
		.click(integrationsTab)
		.pause(2000)
		.assert.elementPresent(caseCredentialsAccountName)
		.pause(1000);
	},

	/**
	 *@description Logs out of System admin and logs into customer account
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01CreateRoleAddUserAddOccupancyMapWidget: Log out of customer Admin account and log into user account" : function (browser) {
		browser
		.useXpath()
        .waitForElementVisible("//a[@title='Logout']", 4000)
        .click("//a[@title='Logout']")
        .pause(1000)
        
		.initialLogin(customerUsername, password, "password");
		
		browser.pause(1000);
	},
	

	/**
	 *@description Navigates to Roles
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01CreateRoleAddUserAddOccupancyMapWidget: Navigate to Settings > Users > Roles" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.waitForElementPresent(settingsTab, 4000)
		.click(settingsTab)
		
		.waitForElementPresent(usersTab, 4000)
		.click(usersTab)
		
		.waitForElementPresent(rolesTab, 4000)
		.click(rolesTab)
		.pause(1000);
		
	},

	/**
	 *@description Create a role with permissions
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01CreateRoleAddUserAddOccupancyMapWidget: Creates a role with 'Manage Locations' permissions" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.waitForElementPresent(addRole, 4000)
		.click(addRole)

		.pause(2000)
		
		.waitForElementVisible("//h2[contains(text(),'Add Role')]", 5000)

		var roleNameIn = "//input[@id='formRoleName']";
		browser
		.pause(1000)
		.waitForElementVisible(roleNameIn, 4000)
		.setValue(roleNameIn, newRoleName);
	},


	/**
	 * @description Processes permissions
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test01CreateRoleAddUserAddOccupancyMapWidget: Choose All Role permissions - including 'manage locations'" : function (browser) {
		var allRoles = "//ul[@id='childPermissionList']/li[@class='Child']";
		browser.useXpath();
		//Child here does not refer to Parent or Child company
		browser.elements('xpath', allRoles, function (result) {
			console.log(result.value.length)
			for (var i = 1; i <= result.value.length; i++) {
				browser
				//assert that the role is visible
				.assert.visible(allRoles + "[" + i + "]/label")
				//clicking the label activates the checkbox
				.click(allRoles + "[" + i + "]/label");
			}
		});
	},

	/**
	 * @description Saves selections
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test01CreateRoleAddUserAddOccupancyMapWidget: Click Save" : function (browser) {
		var saveBtn = "//section[@class='btnSet']/a[text()='Save']";
		var error = "//li[contains(text(), 'same Role Name')]";
		browser
		.useXpath()
		.assert.visible(saveBtn)
		.click(saveBtn)
		.pause(1000)
		.assert.elementNotPresent(error)
		.pause(1000);
	},

	/**
	 * @description Verifies the details are saved correctly
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test01CreateRoleAddUserAddOccupancyMapWidget: Verify Role Details" : function (browser) {
		var addedRole = "//span[contains(text(), '" + newRoleName + "')]";
		browser
		.useXpath()
		.pause(2000)
		.waitForElementVisible(addedRole, 3000)
		.click(addedRole)
		.waitForElementVisible("//dd[@id='detailRoleName']", 3000)
		.pause(1000);

		browser.elements('xpath', "//ul[@id='detailPermissionList']/li", function (result) {
			for (var n = 1; n <= result.value.length; n++) {
				var paths = "//ul[@id='detailPermissionList']/li[" + n + "]";
				browser.assert.visible(paths);
			}
		})

		browser.assert.visible("//ul[@id='userChildList']/li[text()='No users currently assigned.']");
	},

	/**
	 *@description Adds new User
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01CreateRoleAddUserAddOccupancyMapWidget: Add a new user and assign them the newly created role" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.execute('scrollTo(3000, 0)')
		.waitForElementPresent(usersTab, 2000)
		.click(usersTab)

		.pause(1000)
		.click("//a[@id='btnAddUser']")
		.pause(1000)
		.enterUserDetials(newUserFirstName, newUserLastName, newUserEmail, "Password$2", true);
	},

	/**
	 * @description Selects user roles
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test01CreateRoleAddUserAddOccupancyMapWidget: Select Roles" : function (browser) {
		var parentXpath = "//ul[@id='userAllRoles']/li[@class='active ' and text()='" + newRoleName + "']";
		var parentXpath = "//ul[@id='userAllRoles']/li[contains(text(),'" + newRoleName + "')]";
		var selectedRoleXpath = "//ul[@id='userSelectedRoles']/li[@class='active  selected' and contains(text(),'" + newRoleName + "')]";
		var fullName = newUserFirstName + " " + newUserLastName;
			browser
			.useXpath()
			.pause(2000)
			.assert.visible(parentXpath)
			.click(parentXpath)
			.pause(2000)
			.waitForElementVisible(selectedRoleXpath, 5000);
			
		var saveBtn = "//a[@class='linkButtonFtr textButton save']";
		var error = "//li[contains(text(),'same Email Address')]";
		var xpath = "//span[text()='" + fullName + "']";
		browser
		.useXpath()
		.assert.visible(saveBtn)
		.click(saveBtn)
		.assert.elementNotPresent(error)
		.waitForElementVisible(xpath, 3000);
	},

	/**
	 * @description Verifies the result details
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test01CreateRoleAddUserAddOccupancyMapWidget: Verify User Details" : function (browser) {
		var xpath = "//span[contains(text(),'" + newUserFirstName + "')]";
		var name = "//dd[@id='detailUserDisplayName' and contains(text(),'" + newUserFirstName + "')]";
		var status = "//dd[@id='detailUserStatus' and text()='Enabled']";
		var uname = "//dd[@id='detailUserName' and contains(text(),'" + newUserEmail + "')]";
		var selectedUserRole = "//ul[@id='roleChildList']/li[contains(text(),'" + newRoleName.trim() + "')]";
		browser
		.useXpath()
		.pause(1000)
		.click(xpath)
		.waitForElementVisible(name, 2000)
		.assert.visible(status)
		.assert.visible(uname)
		.assert.visible(selectedUserRole);
	},

	/**
	 *@description Logs out of customer account, logs into system admin
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01CreateRoleAddUserAddOccupancyMapWidget: Log out of customer admin account and log into user account" : function (browser) {
		browser
		.useXpath()
        .waitForElementVisible("//a[@title='Logout']", 1000)
        .click("//a[@title='Logout']")
        .pause(1000)
        
		browser
		.useCss()
        .waitForElementVisible('input[id=username]', 1000)
        .setValue('input[id=username]', newUserEmail)
        .waitForElementVisible('input[id=password]', 1000)
        .setValue('input[id=password]', newUserPassword)
        .waitForElementVisible('input[name=submit]', 1000)
        .click('input[name=submit]')
        .pause(2000)
		
		.useXpath()

		.waitForElementPresent("//input[@id='newPassword']", 100000)
		.setValue("//input[@id='newPassword' and @class='left toolTip reqField']", password)
		.pause(1000)

		.waitForElementVisible("//input[@id='c_newPassword' and @class='left reqField']", 10000)
		.setValue("//input[@id='c_newPassword' and @class='left reqField']", password)
		.pause(1000)

		.waitForElementVisible("//input[@id='changePassword' and @class='linkButtonFtr textButton']", 10000)
		.click("//input[@id='changePassword' and @class='linkButtonFtr textButton']")
		.pause(2000)

		.assert.title('Digital Iris - Main (Dashboard)')
		.pause(1000);
		
	},

	/**
	 *@description Add a new Occupancy Map widget
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01CreateRoleAddUserAddOccupancyMapWidget: Add a Occupancy Map widget" : function (browser) {
		var widgetEditButton = "//*[@id='headerTools']/a[contains(@title, 'Edit')]";
		var addWidgetButton = "//a[contains(@class, 'addWidget')]";
		var occupancyMapOpt = "//h3[contains(@id,'metric') and contains(text(), 'Occupancy Map')]";
		var occupancyMap = "//ul/li/header[contains(text(), 'Occupancy Map')]";
		var addWidget = "//div[contains(@class, 'ui-dialog-buttonset')]/button/span[contains(text(), 'Add widget')]";
		
		browser
		.useXpath()
		.pause(1000)
		.waitForElementVisible(widgetEditButton, 4000)
		.click(widgetEditButton)
		
		.pause(1000)
		.waitForElementVisible(addWidgetButton, 4000)
		.click(addWidgetButton)
		
		.pause(1000)
		.execute('scrollTo(0, 3000)')
		.waitForElementVisible(occupancyMapOpt, 4000)
		.click(occupancyMapOpt)
		
		.pause(1000)
		.waitForElementVisible(occupancyMap, 4000)
		.click(occupancyMap)
		
		.pause(1000)
		.waitForElementVisible(addWidget, 4000)
		.click(addWidget)
		.pause(1000);
		


	},

	/**
	 *@description Verify that the widget was added
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01CreateRoleAddUserAddOccupancyMapWidget: Verify that the widget was added successfully after saving it" : function (browser) {
		var occupancyMapWidget = "//section[contains(@class, 'widget')]/header/span[contains(text(), 'Occupancy Map')]";
		var saveWidget = "//*[@id='headerTools']/a[contains(@title, 'Save')]";
		
		browser
		.useXpath()
		.pause(1000)
		.assert.elementPresent(occupancyMapWidget)
		
		.pause(1000)
		.waitForElementVisible(saveWidget, 4000)
		.click(saveWidget)
		
		.pause(1000)
		.assert.elementPresent(occupancyMapWidget)
		.pause(1000);
		
	},
	
	
	/**
	 *@description Logs out of System admin and logs into customer account
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01CreateRoleAddUserAddOccupancyMapWidget: Log out of user account and log back into the user account" : function (browser) {
		browser
		.logoutAndLogin(newUserEmail, password)
		.pause(1000);
	},
	
	
	/**
	 *@description Verify that the widget still exists
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01CreateRoleAddUserAddOccupancyMapWidget: Verify that the widget persists" : function (browser) {
		var occupancyMapWidget = "//section[contains(@class, 'widget')]/header/span[contains(text(), 'Occupancy Map')]";
		
		browser
		.useXpath()
		.pause(1000)
		.assert.elementPresent(occupancyMapWidget)
		.pause(1000);
		
	},

	/**
	 *@description Logs the user out
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01SubscriptionEnabledForNewAndExistingCustomers: Log out of the new user account" : function (browser) {
		browser.logout();
	}

};
