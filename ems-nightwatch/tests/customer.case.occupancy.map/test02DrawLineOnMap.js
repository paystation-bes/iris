/**
* @summary Settings: Location: Map Occupancy: EMS-7259
* @since 7.4.1
* @version 1.0
* @author Kianoush Nesvaderani
* @requires 
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("FlexChildName");
var password = data("Password");

var x = Math.floor((Math.random() * 250) + 1);
var y = Math.floor((Math.random() * 200) + 1);


module.exports = {
	tags : ['smoketag', 'completetesttag', 'featuretesttag'],
	/**
	*@description Logs the user into Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
		.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	/**
	*@description Navigates to Locations
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigates to Location" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//img[@alt='Settings']", 5000)
		.click("//img[@alt='Settings']")
		.waitForElementVisible("//a[contains(text(),'Locations')]", 6000)
		.click("//a[contains(text(),'Locations')]");
	},
	
	/**
	*@description Adds a new location
	*@param browser - The web browser object
	*@returns void
	**/
	"Add a new location" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@id='btnAddLocation']", 6000)
		.click("//a[@id='btnAddLocation']")
		.waitForElementVisible("//input[@id='formLocationName']", 5000)
		.setValue("//input[@id='formLocationName']", "Mapped Occupancy Location")
		.useCss()
		.click("#formOperatingMode")
		.useXpath()
		.click("//a[contains(text(),'Multiple')]")
		.setValue("//input[@id='formCapacity']", "10")
		.click("//ul[@id='locLotFullList']/li")
		.useCss()
		.click(".linkButtonFtr.textButton.save");
	},
	
	/**
	*@description Navigates the user to Edit Parking Area
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigate to Edit Parking Area" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//li[contains(@title, 'Mapped Occupancy Location ')]", 5000)
		.click("//li[contains(@title, 'Mapped Occupancy Location ')]")
		.useCss()
		.waitForElementVisible("#opBtn_pageContent", 5000)
		.click("#opBtn_pageContent")
		.waitForElementVisible("#menu_pageContent > section.menuOptions.innerBorder > a.edit.btnEditParkingLocation", 5000)
		.click("#menu_pageContent > section.menuOptions.innerBorder > a.edit.btnEditParkingLocation")
		.pause(5000);
		for (i = 0; i < 3; i++) {
			browser.click("a.leaflet-control-zoom-in");
		}
	},
	
	/**
	*@description Draws a line on the map and saves the location
	*@param browser - The web browser object
	*@returns void
	**/
	"Draw a line on the map and Save" : function (browser) {
		browser
		.useXpath()
		.click("//div[@id='streetParking-Draw']")
		.pause(3000)
		.click("//div[@class='leaflet-marker-pane']/div")
		.moveToElement("//div[@class='leaflet-marker-pane']/div", x, y)
		.doubleClick()
		.useCss()
		.click("#streetParking-Save")
		.click("#mapingComplete")
		.pause(5000);
	},
	
	/**
	*@description Verifies the line appears on the map
	*@param browser - The web browser object
	*@returns void
	**/
	"Assert the drawing" : function (browser) {
		browser
		.useCss()
		.assert.elementPresent("#mapHolder")
		.assert.elementPresent("svg.leaflet-zoom-animated");
	},
	
	/**
	*@description Logs the user out of Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Logout" : function (browser) {
		browser.logout();
	},
};
