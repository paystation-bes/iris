// Edit Parent timezone from system admin account

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var username2 = data("ParentUserName");
var password2 = data("Password2");

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
	"Search for and select Customer" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//input[@id='search']", 2000)
			.setValue("//input[@id='search']",'qa1parent')
			.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'qa1parent')]", 2000)
			.click("//a[@class='ui-corner-all'][contains(text(),'qa1parent')]")
			.click("//a[@id='btnGo']")
			.pause(1000);
	},
	
	"View Customer Details" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//dt[@class='detailLabel'][contains(text(),'Current Time Zone:')]", 1000)
			.waitForElementVisible("//dd[@class='detailValue'][contains(text(),'Canada/Pacific')]", 1000)
			.pause(1000);
	},
	
	"Click Edit button" : function (browser)
	{browser
			.useXpath()
			.click("//a[@id='btnEditCustomer']")
			.waitForElementVisible("//label[@id='formCustomerTimzone']", 2000)
			.assert.visible("//input[@id='selectTimeZone']")
			.assert.visible("//a[@id='selectTimeZoneExpand']")
			.click("//a[@id='selectTimeZoneExpand']")
			.pause(3000)
			.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'Choose')]", 1000)
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'Canada/Atlantic')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'Canada/Central')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'Canada/East-Saskatchewan')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'Canada/Eastern')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'Canada/Mountain')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'Canada/Newfoundland')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'Canada/Pacific')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'Canada/Saskatchewan')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'Canada/Yukon')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'GMT')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Alaska')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Aleutian')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Arizona')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Central')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/East-Indiana')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Eastern')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Hawaii')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Indiana-Starke')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Michigan')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Mountain')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Pacific')]");
	},
	
	"Select 'Canada/Atlantic'" : function (browser)
	{browser
			.useXpath()
			.click("//a[@class='ui-corner-all'][contains(text(),'Canada/Atlantic')]")
			.pause(3000)
			.assert.visible("//span[@class='ui-button-text'][contains(text(),'Update Customer')]")
			.click("//span[@class='ui-button-text'][contains(text(),'Update Customer')]")
			.pause(5000)
			.assert.visible("//dd[@class='detailValue'][contains(text(),'Canada/Atlantic')]");
	},
	
	"Verify Change to Canada/Atlantic made in Customer Account: Logout" : function (browser)
	{browser.logout();
	},
	
	"Verify Change to Canada/Atlantic made in Customer Account: Login" : function (browser)
	{browser
			.url(devurl)
			.windowMaximize('current')
			.initialLogin(username2, password, password2)
			.assert.title('Digital Iris - Main (Dashboard)');;
	},
	
	"Verify Change to Canada/Atlantic made in Customer Account: Verify" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//a[@href='/secure/settings/index.html?']", 2000)
			.click("//a[@href='/secure/settings/index.html?']")
			.pause(1000)
			.waitForElementVisible("//section[@id='timeZone']/h3/label[contains(text(),'Current Time Zone:')]", 1000)
			.assert.visible("//span[@id='curTimeZone'][contains(text(),'Canada/Atlantic')]");
	},

	"Logout" : function (browser) 
	{
		browser.logout();
	},
	
	"Revert Change-Login SysAdmin" : function (browser)
	{browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
	"Revert Change-Search for and select Customer" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//input[@id='search']", 2000)
			.setValue("//input[@id='search']",'qa1parent')
			.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'qa1parent')]", 2000)
			.click("//a[@class='ui-corner-all'][contains(text(),'qa1parent')]")
			.click("//a[@id='btnGo']")
			.pause(1000);
	},
	
	"Revert Change-Click Edit button" : function (browser)
	{browser
			.useXpath()
			.click("//a[@id='btnEditCustomer']")
			.waitForElementVisible("//label[@id='formCustomerTimzone']", 2000)
			.assert.visible("//input[@id='selectTimeZone']")
			.assert.visible("//a[@id='selectTimeZoneExpand']")
			.click("//a[@id='selectTimeZoneExpand']")
			.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'Choose')]", 1000);
	},
	
	"Revert Change-Select 'Canada/Pacific'" : function (browser)
	{browser
			.useXpath()
			.click("//a[@class='ui-corner-all'][contains(text(),'Canada/Pacific')]")
			.pause(1000)
			.assert.visible("//span[@class='ui-button-text'][contains(text(),'Update Customer')]")
			.click("//span[@class='ui-button-text'][contains(text(),'Update Customer')]")
			.pause(3000)
			.assert.visible("//dd[@class='detailValue'][contains(text(),'Canada/Pacific')]");
	},
	
	"Revert Change-Logout" : function (browser)
	{browser.logout();
	},
	
	"Revert Change-Verify Change to Canada/Pacific made in Customer Account: Login" : function (browser)
	{browser
			.url(devurl)
			.windowMaximize('current')
			.login(username2, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Revert Change-Verify Change to Canada/Pacific made in Customer Account: Verify" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//a[@href='/secure/settings/index.html?']", 2000)
			.click("//a[@href='/secure/settings/index.html?']")
			.pause(1000)
			.waitForElementVisible("//section[@id='timeZone']/h3/label[contains(text(),'Current Time Zone:')]", 1000)
			.assert.visible("//span[@id='curTimeZone'][contains(text(),'Canada/Pacific')]");
	},
	
	"Revert Change-Verify Change to Canada/Pacific made in Customer Account: Logout" : function (browser) 
	{
		browser.logout();
	},
	
};	