// test cancelling customer edit from system admin account

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
	"Search for and select Customer" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//input[@id='search']", 2000)
			.setValue("//input[@id='search']",'qa1child')
			.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'qa1child')]", 2000)
			.click("//a[@class='ui-corner-all'][contains(text(),'qa1child')]")
			.click("//a[@id='btnGo']")
			.pause(5000);
	},
	
	"View Customer Details" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//dt[@class='detailLabel'][contains(text(),'Current Time Zone:')]", 3000)
			.waitForElementVisible("//dd[@class='detailValue'][contains(text(),'Canada/Pacific')]", 3000)
			.pause(3000);
	},
	"Click Edit button" : function (browser)
	{browser
			.useXpath()
			.click("//a[@id='btnEditCustomer']")
			.pause(2000)
			.waitForElementVisible("//label[@id='formCustomerTimzone']", 4000)
			.assert.visible("//input[@id='selectTimeZone']")
			.assert.visible("//a[@id='selectTimeZoneExpand']")
			.click("//a[@id='selectTimeZoneExpand']")
			.pause(3000)
			.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'Choose')]", 1000)
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'Canada/Atlantic')]");
	},
	
	"Select 'Canada/Atlantic'" : function (browser)
	{browser
			.useXpath()
			.click("//a[@class='ui-corner-all'][contains(text(),'Canada/Atlantic')]")
			.pause(1000);
	},
			
	"Select 'Cancel' 1" : function (browser)
	{browser
			.useXpath()
			.assert.visible("//span[@class='ui-button-text'][contains(text(),'Cancel')]")
			.click("//span[@class='ui-button-text'][contains(text(),'Cancel')]")
			.waitForElementVisible("//article[@class='scrollToTop'][contains(text(),'Are you sure you would like to Cancel now and lose your changes?')]", 1000);
	},

	"Select 'No, Please Go Back'" : function (browser)
	{browser
			.useXpath()
			.assert.visible("//span[@class='ui-button-text'][contains(text(),'No, please go back')]")
			.click("//span[@class='ui-button-text'][contains(text(),'No, please go back')]")		
			.waitForElementNotVisible("//article[@class='scrollToTop'][contains(text(),'Are you sure you would like to Cancel now and lose your changes?')]", 1000);
	},

	"Select 'Cancel' 2" : function (browser)
	{browser
			.useXpath()
			.assert.visible("//span[@class='ui-button-text'][contains(text(),'Cancel')]")
			.click("//span[@class='ui-button-text'][contains(text(),'Cancel')]")
			.waitForElementVisible("//article[@class='scrollToTop'][contains(text(),'Are you sure you would like to Cancel now and lose your changes?')]", 1000);
	},
		
	"Select 'Yes, Cancel Now'" : function (browser)
	{browser
			.useXpath()
			.assert.visible("//span[@class='ui-button-text'][contains(text(),'Yes, cancel now')]")
			.click("//span[@class='ui-button-text'][contains(text(),'Yes, cancel now')]")
			.waitForElementVisible("//dd[@class='detailValue'][contains(text(),'Canada/Pacific')]", 1000);
	},	

	"Logout" : function (browser) 
	{
		browser.logout();
	},
};	