// Edit customer timezone from system admin account

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
	"Search for and select Customer" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//input[@id='search']", 2000)
			.setValue("//input[@id='search']",'qa1child')
			.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'qa1child')]", 2000)
			.click("//a[@class='ui-corner-all'][contains(text(),'qa1child')]")
			.click("//a[@id='btnGo']")
			.pause(5000);
	},
	
	"View Customer Details" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//dt[@class='detailLabel'][contains(text(),'Current Time Zone:')]", 3000)
			.pause(1000)
			.waitForElementVisible("//dd[@class='detailValue'][contains(text(),'Canada/Pacific')]", 3000)
			.pause(1000);
	},
	"Click Edit button" : function (browser)
	{browser
			.useXpath()
			.click("//a[@id='btnEditCustomer']")
			.waitForElementVisible("//label[@id='formCustomerTimzone']", 2000)
			.assert.visible("//input[@id='selectTimeZone']")
			.assert.visible("//a[@id='selectTimeZoneExpand']")
			.click("//input[@id='selectTimeZone']")
			.pause(100)
			// .click("//input[@id='selectTimeZone']")
			// .pause(100)
			// .click("//input[@id='selectTimeZone']")
			.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'Choose')]", 500)
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'Canada/Atlantic')]");
	},
	
	"Clear field" : function (browser)
	{browser
			.useXpath()
			.clearValue("//input[@id='selectTimeZone']")
			.pause(1000);
	},

	"Enter 'U' Value" : function (browser)
	{browser
			.useXpath()
			.setValue("//input[@id='selectTimeZone']", 'U')
			.pause(1000)
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'Canada/Mountain')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'Canada/Newfoundland')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'Canada/Yukon')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Alaska')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Aleutian')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Arizona')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Central')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/East-Indiana')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Eastern')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Hawaii')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Indiana-Starke')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Michigan')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Mountain')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Pacific')]")		
			.waitForElementNotPresent("//a[@class='ui-corner-all'][contains(text(),'GMT')]",1000);
	},	

	"Enter 'S' Value" : function (browser)
	{browser
			.useXpath()
			.setValue("//input[@id='selectTimeZone']",'S')
			.pause(1000)
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Alaska')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Aleutian')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Arizona')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Central')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/East-Indiana')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Eastern')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Hawaii')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Indiana-Starke')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Michigan')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Mountain')]")
			.assert.visible("//a[@class='ui-corner-all'][contains(text(),'US/Pacific')]")		
			.waitForElementNotPresent("//a[@class='ui-corner-all'][contains(text(),'GMT')]",1000)
			.waitForElementNotPresent("//a[@class='ui-corner-all'][contains(text(),'Canada/Mountain')]",1000);
	},
	
	"Enter 'Alaska' Value and Click save": function (browser)
	{browser
			.useXpath()
			.clearValue("//input[@id='selectTimeZone']")
			.setValue("//input[@id='selectTimeZone']",'Alaska')
			.pause(3000)
			.assert.elementPresent("//a[@class='ui-corner-all'][contains(text(),'US/Alaska')]")
			.pause(2000)
			//.click("//a[@class='ui-corner-all'][contains(text(),'US/Alaska')]")
			.clearValue("//input[@id='selectTimeZone']")
			.setValue("//input[@id='selectTimeZone']",'US/Alaska')
			.pause(5000)
			.click("//span[@class='ui-button-text'][contains(text(),'Update Customer')]")
			.pause(5000)
			.assert.visible("//dd[@class='detailValue'][contains(text(),'US/Alaska')]");
	},
	
	// "Save Updates"  : function (browser)
	// {browser
	// 		.useXpath()
	// 		.click("//a[@class='ui-corner-all'][contains(text(),'US/Alaska')]")
	// 		.click("//span[@class='ui-button-text'][contains(text(),'Update Customer')]")
	// 		.pause(2000)
	// 		.assert.visible("//dd[@class='detailValue'][contains(text(),'US/Alaska')]");
	// },
	
	"Revert Change-Click Edit button" : function (browser)
	{browser
			.useXpath()
			.click("//a[@id='btnEditCustomer']")
			.waitForElementVisible("//label[@id='formCustomerTimzone']", 2000)
			.assert.visible("//input[@id='selectTimeZone']")
			.assert.visible("//a[@id='selectTimeZoneExpand']")
			.click("//a[@id='selectTimeZoneExpand']")
			.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'Choose')]", 1000);
	},
	
	"Revert Change-Select 'Canada/Pacific'" : function (browser)
	{browser
			.useXpath()
			.click("//a[@class='ui-corner-all'][contains(text(),'Canada/Pacific')]")
			.pause(1000)
			.assert.visible("//span[@class='ui-button-text'][contains(text(),'Update Customer')]")
			.click("//span[@class='ui-button-text'][contains(text(),'Update Customer')]")
			.pause(5000)
			.assert.visible("//dd[@class='detailValue'][contains(text(),'Canada/Pacific')]");
	},
	
	"Logout" : function (browser) 
	{
		browser.logout();
	},
	
};	