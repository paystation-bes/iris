// view customer timezone from system admin account

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
	"Search for and select Customer" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//input[@id='search']", 2000)
			.setValue("//input[@id='search']",'qa1child')
			.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'qa1child')]", 2000)
			.click("//a[@class='ui-corner-all'][contains(text(),'qa1child')]")
			.click("//a[@id='btnGo']")
			.pause(1000);
	},
	
	"View Customer Details" : function (browser)
	{browser
			.useXpath()
			.pause(1000)
			.assert.containsText("//dt[@class='detailLabel'][contains(text(),'Current Time Zone:')]", "Current Time Zone:")
			.assert.containsText("//dd[@class='detailValue'][contains(text(),'Canada/Pacific')]", "Canada/Pacific")
			.pause(1000);
	},
	"Confirm Edit button registers Mouse-over" : function (browser)
	{
			browser.moveToElement("//a[@id='btnEditCustomer']", 100,100, function(){
			browser.pause(500)
			browser.waitForElementVisible("//a[@id='btnEditCustomer']/img", 1000)
			});
	},
	
	
	"Logout" : function (browser) 
	{
		browser.logout();
	},
	
};	
	
	