/**
 * @summary Generate MEID for 18 digit CCID of a CDMA modem types
 * @since 7.3.6
 * @version 1.0
 * @author Nadine B
 * @see US-955: TC1199, TC1201, TC1202
 **/

var data = require('../../data.js');

var devurl = data("URL");
var paystationCommAddress = data("PaystationCommAddress");
var username = data("AdminUserName");
var adminUsername = data("SysAdminUserName");
var password = data("Password");
var customer = "oranj";
var customerName = "Oranj Parent";

var ccid = '';

var min = 1;
var max = 9;

var lowerBoundFirst = 1000000000;
var lowerBoundLast = 10000000;

var upperBoundFirst = 4294967295;
var upperBoundLast = 16777215;

var allModemTypes = ['Unknown', 'WiFi', 'CellularModem', '', 'CDMA', 'Ethernet', 'GSM'];

var invalid17digitCCID = '12345123451234512';
var invalid19digitCCID = '1234512345123451234';


module.exports = {
		tags: ['featuretesttag', 'completetesttag'],
	
		/**
		 * @description Generate CCID's
		 * @param void
		 * @returns void
		 **/
		"testMEIDCustomer: Create CCID" : function (browser) {

			var numFirst = Math.floor(Math.random() * (upperBoundFirst - lowerBoundFirst + 1)) + lowerBoundFirst;
			var numLast = Math.floor(Math.random() * (upperBoundLast - lowerBoundLast + 1)) + lowerBoundLast;
			ccid = ccid + numFirst + numLast
			
			console.log("Generated CCID = " + ccid)
			
			browser.assert.equal(ccid.length, 18);
			

		},
		
		
		/**
		 * @description Sends all required events for all modem types
		 * @param browser - The web browser object
		 * @returns void
		 **/
		"testMEIDCustomer: Send all events for this script" : function (browser) {
			
			for(i = 0; i < allModemTypes.length; i++) {
				browser.runPaystationEvent(adminUsername, password, customer, customerName, username, allModemTypes[i], ccid);
			}

		},
		
		
		/**
		 * @description Verifies CDMA modem with CCID < 18
		 * @param browser - The web browser object
		 * @returns void
		 **/
		"testMEIDCustomer: Verify that CCID < 18 in length will not get an MEID" : function (browser) {
			
			browser.runPaystationEvent(adminUsername, password, customer, customerName, username, 'CDMA', invalid17digitCCID);

		},
		

		/**
		 * @description Verifies CDMA modem with CCID > 18
		 * @param browser - The web browser object
		 * @returns void
		 **/
		"testMEIDCustomer: Verify that CCID > 18 in length will not get an MEID" : function (browser) {
			
			browser.runPaystationEvent(adminUsername, password, customer, customerName, username, 'CDMA', invalid19digitCCID);
			browser.end();

		},
		
};
