/**
* @summary Customer Admin- Test that removal of Flex Role Permissions on a new user account removes display of Flex information
* @since 7.3.4
* @version 1.0
* @author Lonney McD
* @see US718
* @requires FlexSub03CustAdminCreateFlexRole.js
* @requires FlexSub05CustAdminRolePermissionsDisable.js
* ***NOTE*** requires final elements for Properties and Facilities to complete script
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var username2 = data("FlexChildName");
var username3 = data("FlexParentName");
var password2 = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	"Login Customer Account With Flex User Role" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.useCss()
			.click("#username")
			.setValue("#username", "FlexRole")
			.click("#password")
			.setValue("#password", "Password$1")
			.click("#submit")
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Navigate to Settings> Global> Settings" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//a[@href='/secure/settings/index.html?']", 2000)
			.click("//a[@href='/secure/settings/index.html?']")
			.pause(5000)
			;
	},

	"Verify Flex Credentials not present" : function (browser)
	{browser
			.useXpath()
			.assert.elementNotPresent("//h3[contains(text(),'Flex Server Credentials')]")
			.assert.elementPresent("//li[@tooltip='FLEX Integration is Activated']")
			.useCss()
			.assert.elementNotPresent("#btnEditFlexCredentials")
			;
	},
	
	"Navigate to Settings> Locations> Location Details" : function (browser)
	{browser
			.useXpath()
			.click("//a[@title='Locations']")
			.waitForElementPresent("//section[@class='locName'][contains(text(),'Airport')]", 2000)
			.click("//section[@class='locName'][contains(text(),'Airport')]")
			.waitForElementPresent("//h2[@id='locationName'][contains(text(),'Airport')]", 2000)
			.assert.elementPresent("//section[@class='locName'][contains(text(),'Airport')]")
			;
	},
	
	"Verify that Flex Properties and Facilities Not Present 1" : function (browser)
	{browser
			.useXpath()
	// NOTE 1: These values can only be tested when Flex Facilities and Properties have been assigned, which is not yet set up for an automation account, and so has been disabled	
	// NOTE 2: Further development notes that these elements will remain when flex role permissions are removed, when verified, this section can be removed
	//		.assert.elementNotPresent("//section[@id='FacilitiesChildren'][contains(.,'Facilities')]/h3[@class='detailList']")
	//		.assert.elementNotPresent("//section[@id='PropertiesChildren'][contains(.,'Properties')]/h3[@class='detailList']")
			;
	},
	
	"Click Edit Location" : function (browser)
	{browser
			.useXpath()
			.click("//header/a[@title='Option Menu']")
			.waitForElementPresent("//section[@class='column second']//section[@class='menuOptions innerBorder']/a[@class='edit btnEditLocation'][@title='Edit']", 1000)
			.click("//section[@class='column second']//section[@class='menuOptions innerBorder']/a[@class='edit btnEditLocation'][@title='Edit']")
			.pause(2000)
			;
	},
	
	"Verify that Flex Properties and Facilities Not Present 2" : function (browser)
	{browser
			.useXpath()
	// NOTE 2: Further development notes that these elements will remain when flex role permissions are removed, when verified, this section can be removed
	//		.assert.elementNotPresent("//section[@id='PropertiesChildren']/h3[@class='detailList'][contains(text(),'Properties')]")
	//		.assert.elementNotPresent("//section[@id='FacilitiesChildren']/h3[@class='detailList'][contains(text(),'Facilities')]")			
			
			;
	},
	
	"Edit Location- click Cancel" : function (browser)
	{browser
			.useXpath()
			.click("//a[@class='linkButton textButton cancel'][contains(text(),'Cancel')]")
			.pause(1000)
			;
	},
	
	"Logout" : function (browser) 
	{
		browser.logout();
	},
	
	"Delete User Role- Login" : function (browser)
	{browser
			.url(devurl)
			.windowMaximize('current')
			.login(username2, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Delete User Account- Navigate to User Account" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//a[@href='/secure/settings/index.html?']", 2000)
			.click("//a[@href='/secure/settings/index.html?']")
			.waitForElementVisible("//a[@title='Users']", 4000)
			.click("//a[@title='Users']")
			.pause(2000)
			;
	},
		
	"Delete User Role- Select User Account" : function (browser)
	{browser
			.useXpath()
			.assert.elementPresent("//ul[@id='userList']//span[contains(text(),'Flex Role')]")
			.click("//ul[@id='userList']//span[contains(text(),'Flex Role')]")
			.pause(2000)
			;
	},
		
	"Delete User Role- Select User Account Option" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//a[@id='opBtn_pageContent']/img[@title='Option Menu']", 2000)
			.click("//a[@id='opBtn_pageContent']/img[@title='Option Menu']")
			.pause(2000)
			;
	},
	
	"Delete User Role- Delete User Account" : function (browser)
	{browser
			.useXpath()
			.click("//section[@id='menu_pageContent']/section/a[@class='delete btnDeleteLocation']")
			.waitForElementPresent("//body[@id='settings']/div/div/div/button/span[contains(text(),'Delete')]",2000)
			.click("//body[@id='settings']/div/div/div/button/span[contains(text(),'Delete')]")
			.pause(2000)
			;
	},
	
	"Delete User Role- Verify User Account Deleted" : function (browser)
	{browser
			.useXpath()
			.assert.elementNotPresent("//ul[@id='userList']//span[contains(text(),'Flex Role')]")
			;
	},

	"Delete User Role- Navigate to User Roles" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//a[@title='Roles']", 2000)
			.click("//a[@title='Roles']")
			.waitForElementVisible("//section[@class='groupBox menuBox userList']//h2[contains(text(),'Roles')]", 2000)
			;
	},
	
	"Delete User Role- Select User Role" : function (browser)
	{browser
			.useXpath()
			.assert.elementPresent("//ul[@id='roleList']//span[contains(text(),'FlexRole')]")
			.click("//ul[@id='roleList']//span[contains(text(),'FlexRole')]")
			.pause(2000)
			;
	},
	
	"Delete User Role- Select User Role Option" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//a[@id='opBtn_pageContent']/img[@title='Option Menu']", 4000)
			.click("//a[@id='opBtn_pageContent']/img[@title='Option Menu']")
			.pause(2000)
			;
	},
	
	"Delete User Role- Delete User Role" : function (browser)
	{browser
			.useXpath()
			.click("//section[@id='menu_pageContent']/section/a[@class='delete btnDeleteLocation']")
			.waitForElementPresent("//body[@id='settings']/div/div/div/button/span[contains(text(),'Delete')]",2000)
			.click("//body[@id='settings']/div/div/div/button/span[contains(text(),'Delete')]")
			.pause(2000)
	},
	
	"Delete User Role- Verify User Role Deleted" : function (browser)
	{browser
			.useXpath()
			.assert.elementNotPresent("//ul[@id='userList']//span[contains(text(),'FlexRole')]")
			;
	},
	
	"Delete User Role- Logout" : function (browser) 
	{
		browser.logout();
	},
};	