/**
* @summary Customer Admin- Test that Flex Role Permissions Can be removed 
* @since 7.3.4
* @version 1.0
* @author Lonney McD
* @see US718
* @requires FlexSub03CustAdminCreateFlexRole.js
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var username2 = data("FlexChildName");
var username3 = data("FlexParentName");
var password2 = data("Password");


module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	"Login Customer Account 1" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username2, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Navigate to Settings>Users>Roles 1" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//a[@href='/secure/settings/index.html?']", 2000)
			.click("//a[@href='/secure/settings/index.html?']")
			.waitForElementVisible("//a[@title='Users']", 4000)
			.click("//a[@title='Users']")
			.waitForElementVisible("//a[@title='Roles']", 2000)
			.click("//a[@title='Roles']")
			.waitForElementVisible("//section[@class='groupBox menuBox userList']//h2[contains(text(),'Roles')]", 2000)
			;
	},

	"Select Flex Role" : function (browser)
	{browser
			.useXpath()
			.assert.elementPresent("//ul[@id='roleList']//span[contains(text(),'FlexRole')]")
			.click("//ul[@id='roleList']//span[contains(text(),'FlexRole')]")
			.waitForElementPresent("//h2[@id='roleDispName'][contains(text(),'FlexRole')]", 2000)
			;
	},

	"Edit Flex Role" : function (browser)
	{browser
			.useXpath()
			.click(".//header/a[@title='Option Menu']")
			.waitForElementPresent("//section[@id='menu_pageContent']/section/a[@title='Edit']", 1000)
			.click("//section[@id='menu_pageContent']/section/a[@title='Edit']")
			;
	},
	
	"Remove Flex Permissions from Role" : function (browser)
	{browser
			.useXpath()
			// below permissions have not been finalized yet, and names may change
			
			.assert.elementPresent("//label[contains(.,'View Flex WS Credentials')]/a[@class='checkBox checked']") 
			.keys("\ue010'")
			.pause(3000)
			.click("//label[contains(.,'View Flex WS Credentials')]/a[@class='checkBox checked']")
			.pause(2000)
			.assert.elementPresent("//label[contains(.,'View Flex WS Credentials')]/a[@class='checkBox']")
			
			.assert.elementPresent("//label[contains(.,'Manage Flex WS Credentials')]/a[@class='checkBox checked']")
			.click("//label[contains(.,'Manage Flex WS Credentials')]/a[@class='checkBox checked']")
			.pause(2000)
			.assert.elementPresent("//label[contains(.,'Manage Flex WS Credentials')]/a[@class='checkBox']")
			;
	},
	
	"Save Role" : function (browser)
	{browser
			.useXpath()
			.click("//a[@class='linkButtonFtr textButton save']")
			.pause(3000)
			;
	},
	
	"Verify Role" : function (browser)
	{browser
			.useXpath()
			.assert.elementPresent("//ul[@id='roleList']//span[contains(text(),'FlexRole')]")
			;
	},
	
	"Logout" : function (browser) 
	{
		browser.logout();
	},	
};	