/**
* @summary Customer Admin- Widgets- Test that Flex widgets do not display data when Flex Subscription is disabled
* @since 7.3.4
* @version 1.0
* @author Lonney McD
* @see US718
* @requires FlexSub09SysAdminDisableFlexSub.js
* ***NOTE*** requires Flex Widgets to be implemented to complete script, but script was tested and verified by substituting existing widget ids
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var username2 = data("FlexChildName");
var username3 = data("FlexParentName");
var password2 = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	"Login Customer Child Admin" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username2, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Edit Dashboard" : function (browser)
	{browser
			.useCss()
			.click("a.linkButtonFtr.edit")
			.useXpath()
			.waitForElementVisible("//section[@class='layout5 layout']/section/section/a[@class='linkButtonIcn addWidget'][@title='Add Widget']", 2000)
			;
	},

	"Click Add Widget 1" : function (browser)
	{browser
			.useXpath()
			.click("//section[@class='layout5 layout']/section/section/a[@class='linkButtonIcn addWidget'][@title='Add Widget']")
			.waitForElementVisible("//span[@class='ui-dialog-title'][contains(text(),'Add Widget')]", 2000)
			.pause(2000)
			;
	},
		
	"Expand List 1" : function (browser)
	{browser
			.useXpath()
// The following steps may not be correct, and will rely on actual implementation of Flex Widgets on site			
	//		.assert.elementPresent("//h3[contains(text(),'Flex')]")
	//		.click("//h3[contains(text(),'Flex')]")
			.pause(2000)
			;
	},
	
	"Select Flex Citations Widget" : function (browser)
	{browser
			.useXpath()
	//		.assert.elementPresent("//li/header[contains(text(),'Flex Citations')]")
	// 		.click("//li/header[contains(text(),'Flex Citations')]")
			.pause(2000)
			;
	},
	
	"Add Widget/Close Menu 1" : function (browser)
	{browser
			.useXpath()
			.click("//span[contains(text(),'Add widget')]")
			.pause(2000)
			;
	},
	
	"Click Add Widget 2" : function (browser)
	{browser
			.useXpath()
			.click("//section[@class='layout5 layout']/section/section/a[@class='linkButtonIcn addWidget'][@title='Add Widget']")
			.waitForElementVisible("//span[@class='ui-dialog-title'][contains(text(),'Add Widget')]", 2000)
			.pause(2000)
			;
	},

	"Expand List 2" : function (browser)
	{browser
			.useXpath()
// The following steps may not be correct, and will rely on actual implementation of Flex Widgets on site			
	//		.assert.elementPresent("//h3[contains(text(),'Flex')]")
	//		.click("//h3[contains(text(),'Flex')]")
			.pause(2000)
			;
	},

	"Select Flex Citation Map Widget" : function (browser)
	{browser
			.useXpath()
	//		.assert.elementPresent("//li/header[contains(text(),'Flex Citation Map')]")
	// 		.click("//li/header[contains(text(),'Flex Citation Map')]")
			.pause(2000)
			;
	},

	"Add Widget/Close Menu 2" : function (browser)
	{browser
			.useXpath()
			.click("//span[contains(text(),'Add widget')]")
			.pause(2000)
			;
	},
	
	"Click Save Dashboard Button" : function (browser)
	{browser
			.useXpath()
			.click("//a[@title='Save']")
			.pause(2000)
			;
	},
	
	"Verify Flex Citation Widget No Data" : function (browser)
	{browser
			.useXpath()
// The following steps may not be correct, and will rely on actual implementation of Flex Widgets on site			
	//		.waitForElementPresent("//span[contains(text(),'Flex Citations')]", 2000)
	//		.assert.elementPresent("//span[contains(text(),'Flex Citations')]/../../section/section/span[contains(text(),'There is currently no data returned for this widget.')]")
			;
	},

	"Verify Flex Citation Map Widget No Data" : function (browser)
	{browser
			.useXpath()
// The following steps may not be correct, and will rely on actual implementation of Flex Widgets on site			
	//		.waitForElementPresent("//span[contains(text(),'Flex Citation Map')]", 2000)
	//		.assert.elementPresent("//span[contains(text(),'Flex Citation Map')]/../../section/section/span[contains(text(),'There is currently no data returned for this widget.')]")
			;
	},
		
	"Logout 1" : function (browser) 
	{
		browser.logout();
	},	
};	