/**
* @summary Customer Admin- Parent Account- Settings> Global Preferences: Flex Credentials Section is displayed and selectable when Flex Subscription is re-enabled
* @since 7.3.4
* @version 1.0
* @author Lonney McD
* @see US718
* @requires FlexSub09SysAdminDisableFlexSub.js
* @requires FlexSub17SysAdminEnableFlexSub.js
* ***NOTE*** requires final elements for Properties and Facilities to complete script
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var username2 = data("FlexChildName");
var username3 = data("FlexParentName");
var password2 = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	"Login Parent Customer" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username3, password)
			.pause(2000)
			.assert.title('Digital Iris - Main (Dashboard)');
	},

	"Select Child Account from Parent/Child Bar" : function (browser)
	{browser
			.useCss()
			.click("#childCoSwitchAC")
			.setValue("#childCoSwitchAC", "oranj")
			.click("#custSwitchBtn")
			.pause(3000)
			;
	},
	
	"Navigate to Settings>Global>Settings" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//a[@href='/secure/settings/index.html?']", 2000)
			.click("//a[@href='/secure/settings/index.html?']")
			.pause(5000)
			;
	},

	"Verify Flex Credentials section present" : function (browser)
	{browser
			.useXpath()
			.assert.elementPresent("//h3[contains(text(),'Flex Server Credentials')]")
			.assert.elementPresent("//li[@tooltip='FLEX Integration is Activated']")
			.useCss()
			.assert.elementPresent("#btnEditFlexCredentials")
			;
	},
	
	"Navigate to Settings> Locations> Location Details" : function (browser)
	{browser
			.useXpath()
			.click("//a[@title='Locations']")
			.pause(3000)
			.waitForElementPresent("//section[@class='locName'][contains(text(),'Airport')]", 2000)
			;
	},

	"Select a Location" : function (browser)
	{browser
			.useXpath()
			.click("//section[@class='locName'][contains(text(),'Airport')]")
			.waitForElementPresent("//h2[@id='locationName'][contains(text(),'Airport')]", 2000)
			.assert.elementPresent("//section[@class='locName'][contains(text(),'Airport')]")
			;
	},
	
	"Verify that Flex Properties and Facilities Not Present 1" : function (browser)
	{browser
			.useCss()
			.assert.hidden("#propChildList")
			.assert.hidden("#facChildList")
			;
	},
	
	"Click Edit Location" : function (browser)
	{browser
			.useXpath()
			.click("//header/a[@title='Option Menu']")
			.waitForElementVisible("//section[@class='column second']//section[@class='menuOptions innerBorder']/a[@class='edit btnEditLocation'][@title='Edit']", 1000)
			.click("//section[@class='column second']//section[@class='menuOptions innerBorder']/a[@class='edit btnEditLocation'][@title='Edit']")
			.pause(2000)
			;
	},
	
	"Verify that Flex Properties and Facilities Not Present 2" : function (browser)
	{browser
			.useCss()
			.assert.elementPresent("#locPropFullList")
			.assert.visible("#locPropFullList")
			.assert.visible("#locFacFullList")
			;
	},
	
	"Edit Location- click Cancel;" : function (browser)
	{browser
			.useXpath()
			.click("//a[@class='linkButton textButton cancel'][contains(text(),'Cancel')]")
			.pause(1000)
			;
	},
	
	"Logout 1" : function (browser) 
	{
		browser.logout();
	},	
};	