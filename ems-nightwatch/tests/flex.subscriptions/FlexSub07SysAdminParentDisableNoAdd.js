/**
* @summary Customer Admin- Parent- Test that Flex Subscription cannot be added to child account when disabled in parent
* @since 7.3.4
* @version 1.0
* @author Lonney McD
* @see US718
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var username2 = data("FlexChildName");
var username3 = data("FlexParentName");
var password2 = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	"Login System Admin 1" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
	"Search and Select Parent Customer 1" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//input[@id='search']", 2000)
			.setValue("//input[@id='search']",'Oranj Parent')
			.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'Oranj Parent')]", 2000)
			.click("//a[@class='ui-corner-all'][contains(text(),'Oranj Parent')]")
			.click("//a[@id='btnGo']")
			.pause(1000);
			;
	},

	"Click Edit Parent Customer 1" : function (browser)
	{browser
			.useXpath()
			.click("//a[@id='btnEditCustomer']")
			;
	},
		
	"Verify Flex Subscription 1" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//label[contains(.,'FLEX Integration')]/a[@id='subscriptionList:1700']", 2000)
			.assert.elementPresent("//label[contains(.,'FLEX Integration')]/a[@class='checkBox checked']")
			;
	},
	
	"Deselect Flex and click Update Customer" : function (browser)
	{browser
			.useXpath()
			.click("//a[@id='subscriptionList:1700']")
			.assert.elementPresent("//label[contains(.,'FLEX Integration')]/a[@class='checkBox']")
			.assert.elementNotPresent("//label[contains(.,'FLEX Integration')]/a[@class='checkBox checked']")
			.click("//span[@class='ui-button-text'][contains(text(),'Update Customer')]")
			.pause(3000)
			;
	},

	"Search and Select Child Customer 1" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//input[@id='search']", 2000)
			.setValue("//input[@id='search']",'oranj')
			.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'oranj')]", 2000)
			.click("//a[@class='ui-corner-all'][contains(text(),'oranj')]")
			.click("//a[@id='btnGo']")
			.pause(1000);
			;
	},

	"Click Edit Child Customer 2" : function (browser)
	{browser
			.useXpath()
			.click("//a[@id='btnEditCustomer']")
			;
	},
		
	"Verify No Flex Subscription Available" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//label[contains(.,'FLEX Integration')]/a[@id='subscriptionList:1700']", 2000)
			.assert.elementPresent("//label[@tooltip='This subscription is not available until the Parent Company has activated this subscription.']")
			;
	},
	
	"Click Cancel Button 1" : function (browser)
	{browser
			.useXpath()
			.click("//span[@class='ui-button-text'][contains(text(),'Cancel')]")
			;
	},
	
	"Verify No Flex Subscription listed in Services" : function (browser)
	{browser
			.useXpath()
			.assert.elementNotPresent("//li[@title=' FLEX Integration is Available']")
			.assert.elementNotPresent("//li[@title=' FLEX Integration is Activated']")
			;
	},
		
	"Logout 1" : function (browser) 
	{
		browser.logout();
	},
	
	"Login Child Account 1" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username2, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Navigate to Settings> Global> Settings" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//a[@href='/secure/settings/index.html?']", 2000)
			.click("//a[@href='/secure/settings/index.html?']")
			.pause(5000)
			;
	},

	"Verify No Flex Credentials" : function (browser)
	{browser
			.useXpath()
			.assert.elementNotPresent("//h3[contains(text(),'Flex Server Credentials')]")
			;
	},
	
	"Verify No Flex Subscription" : function (browser)
	{browser
			.useXpath()
			.assert.elementNotPresent("//li[@title=' FLEX Integration is Available']")
			.assert.elementNotPresent("//li[@title=' FLEX Integration is Activated']")			;
	},
		
	"Logout 2" : function (browser) 
	{
		browser.logout();
	},	
};	