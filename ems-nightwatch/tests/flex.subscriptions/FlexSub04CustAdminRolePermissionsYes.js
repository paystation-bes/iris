/**
* @summary Customer Admin- Test that new Flex Role Permissions on a new user account allows Flex information to display
* @since 7.3.4
* @version 1.0
* @author Lonney McD
* @see US718
* @requires FlexSub03CustAdminCreateFlexRole.js
* ***NOTE*** requires final elements for Properties and Facilities to complete script
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var username2 = data("FlexChildName");
var username3 = data("FlexParentName");
var password2 = data("Password");


module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	"Login Customer Account With Flex User Role" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.useCss()
			.click("#username")
			.setValue("#username", "FlexRole")
			.click("#password")
			.setValue("#password", "Password$1")
			.click("#submit")
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Navigate to Settings> Global> Settings" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//a[@href='/secure/settings/index.html?']", 2000)
			.click("//a[@href='/secure/settings/index.html?']")
			.pause(5000)
			;
	},

	"Verify Flex Credentials" : function (browser)
	{browser
			.useXpath()
			.assert.elementPresent("//h3[contains(text(),'Flex Server Credentials')]")
			.assert.elementPresent("//li[@tooltip='FLEX Integration is Activated']")
			;
	},
	
	"Verify Flex Credentials Edit button functions" : function (browser)
	{browser
			.useCss()
			.click("#btnEditFlexCredentials")
			.pause(5000)
			.useXpath()
			.waitForElementPresent("//span[contains(text(),'Flex Server Credentials')]", 2000)
			.click("//span[@class='ui-button-text'][contains(text(),'Cancel')]")
			.pause(2000)
			;
	},	
	
	"Navigate to Settings> Locations> Location Details" : function (browser)
	{browser
			.useXpath()
			.click("//a[@title='Locations']")
			.waitForElementPresent("//section[@class='locName'][contains(text(),'Airport')]", 5000)
			.click("//section[@class='locName'][contains(text(),'Airport')]")
			.waitForElementPresent("//h2[@id='locationName'][contains(text(),'Airport')]", 2000)
			.assert.elementPresent("//section[@class='locName'][contains(text(),'Airport')]")
			;
	},
	
	
	"Click Edit Location" : function (browser)
	{browser
			.useXpath()
			.click("//header/a[@title='Option Menu']")
			.waitForElementPresent("//section[@class='column second']//section[@class='menuOptions innerBorder']/a[@class='edit btnEditLocation'][@title='Edit']", 1000)
			.click("//section[@class='column second']//section[@class='menuOptions innerBorder']/a[@class='edit btnEditLocation'][@title='Edit']")
			.pause(2000)
			;
	},
	
	"Verify that Flex Properties and Facilities present 2" : function (browser)
	// NOTE 2: Further development notes that these elements will remain when flex role permissions are removed, when verified, this section can be removed
	{browser
			.useXpath()
			.assert.elementPresent("//section[@id='formPropertyChildren']/h3[@class='detailList'][contains(text(),'Properties')]")
			.assert.elementPresent("//section[@id='formFacilityChildren']/h3[@class='detailList'][contains(text(),'Facilities')]")			
			;
	},
	
	"Edit Location- click Cancel;" : function (browser)
	{browser
			.useXpath()
			.click("//a[@class='linkButton textButton cancel'][contains(text(),'Cancel')]")
			.pause(1000)
			;
	},
	
	"Logout" : function (browser) 
	{
		browser.logout();
	},	
};	