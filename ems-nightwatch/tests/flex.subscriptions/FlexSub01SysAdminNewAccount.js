/**
* @summary System Admin- Test That Flex Subscription displays, and selection can be saved on new account
* @since 7.3.4
* @version 1.0
* @author Lonney McD
* @see US718
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
	"Click New Customer Button" : function (browser)
	{browser
			.useCss()
			.waitForElementVisible("#btnAddNewCustomer", 1000)
			.click("#btnAddNewCustomer")
			.pause(3000);
	},
	
	"Enter Customer Name, Admin User Name, and Passwords" : function (browser)
	{browser
			.useCss()
			.waitForElementVisible("#formCustomerName", 1000)
			.click("#formCustomerName")
			.setValue("#formCustomerName", "TestFlex")
			.click("#formUserName")
			.setValue("#formUserName", "TestFlex")
			.click("#newPassword")
			.setValue("#newPassword", "Password$2")
			.click("#c_newPassword")
			.setValue("#c_newPassword", "Password$2")
			.useXpath()
			.click("//a[@id='accountStatus:enabled']")
			;
	},
	
	"Select Services, Including Flex" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//label[contains(.,'FLEX Integration')]/a[@id='subscriptionList:1700']", 2000)
			.click("//a[@id='subscriptionList:1200']")
			.click("//a[@id='subscriptionList:200']")
			.click("//a[@id='subscriptionList:400']")
			.click("//a[@id='subscriptionList:600']")
			.click("//a[@id='subscriptionList:1000']")
			.click("//a[@id='subscriptionList:1100']")
			.click("//a[@id='subscriptionList:900']")
			.click("//a[@id='subscriptionList:1300']")
			.click("//a[@id='subscriptionList:1600']")
			.click("//a[@id='subscriptionList:700']")
			.click("//a[@id='subscriptionList:500']")
			.click("//a[@id='subscriptionList:300']")
			.click("//a[@id='subscriptionList:800']")
			.click("//a[@id='subscriptionList:100']")
	//		button seems to need 3 clicks for 'checkbox checked' to register in next step
			.click("//a[@id='subscriptionList:1700']")
			.click("//a[@id='subscriptionList:1700']")
			.click("//a[@id='subscriptionList:1700']")
			;
	},
	
	"Verify Flex Service selected" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//label[contains(.,'FLEX Integration')]/a[@id='subscriptionList:1700']", 2000)
			.assert.elementPresent("//label[contains(.,'FLEX Integration')]/a[@class='checkBox checked']")
			;
	},
	
	"Click Cancel Button" : function (browser)
	{browser
			.useXpath()
			.click("//span[@class='ui-button-text'][contains(text(),'Cancel')]")
			;
	},
	
	"Logout" : function (browser) 
	{
		browser.logout();
	},
};	