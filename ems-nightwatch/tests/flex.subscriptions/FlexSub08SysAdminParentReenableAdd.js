/**
* @summary Customer Admin- Parent- Test that Flex Subscription can be added to child account when re-enabled in parent
* @since 7.3.4
* @version 1.0
* @author Lonney McD
* @see US718
* @requires FlexSub07SysAdminParentDisableNoAdd.js
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var username2 = data("FlexChildName");
var username3 = data("FlexParentName");
var password2 = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	"Login System Admin 1" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
	"Search and Select Parent Customer 1" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//input[@id='search']", 2000)
			.setValue("//input[@id='search']",'Oranj Parent')
			.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'Oranj Parent')]", 2000)
			.click("//a[@class='ui-corner-all'][contains(text(),'Oranj Parent')]")
			.click("//a[@id='btnGo']")
			.pause(1000);
			;
	},

	"Click Edit Parent Customer 1" : function (browser)
	{browser
			.useXpath()
			.click("//a[@id='btnEditCustomer']")
			.pause(2000)
			;
	},
		
	"Enable Flex 1" : function (browser)
	{browser
			.useXpath()
			.click("//a[@id='subscriptionList:1700']")
			.pause(2000)
			;
	},
	
	"Update Customer 1" : function (browser)
	{browser
			.useXpath()
			.click("//span[@class='ui-button-text'][contains(text(),'Update Customer')]")
			.pause(3000)
			;
	},
	
	"Verify Subscription activated 1" : function (browser)
	{browser
			.useXpath()
			.assert.elementPresent("//li[@title='FLEX Integration is Activated']")
			;
	},

	"Search and Select Child Customer 1" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//input[@id='search']", 2000)
			.setValue("//input[@id='search']",'oranj')
			.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'oranj')]", 2000)
			.click("//a[@class='ui-corner-all'][contains(text(),'oranj')]")
			.click("//a[@id='btnGo']")
			.pause(1000);
			;
	},

	"Click Edit Customer 2" : function (browser)
	{browser
			.useXpath()
			.click("//a[@id='btnEditCustomer']")
			;
	},
		
	"Verify Flex Subscription Available" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//label[contains(.,'FLEX Integration')]/a[@id='subscriptionList:1700']", 2000)
			.assert.elementNotPresent("//label[contains(.,'FLEX Integration')]/a[@class='checkBox checked']")
			;
	},
	
	"Enable Flex 2" : function (browser)
	{browser
			.useXpath()
			.click("//label[contains(.,'FLEX Integration')]/a[@id='subscriptionList:1700']")
			.pause(1000)
			;
	},
			
	"Update Customer 2" : function (browser)
	{browser
			.useXpath()
			.click("//span[@class='ui-button-text'][contains(text(),'Update Customer')]")
			.pause(2000)
			;
	},
	
	"Verify Subscription activated 2" : function (browser)
	{browser
			.useXpath()
			.waitForElementPresent("//li[@title='FLEX Integration is Activated']", 5000)
			;
	},
	
	"Logout 1" : function (browser) 
	{
		browser.logout();
	},
	
	"Login Child Account 1" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username2, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Navigate to Settings> Global> Settings" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//a[@href='/secure/settings/index.html?']", 2000)
			.click("//a[@href='/secure/settings/index.html?']")
			.pause(5000)
			;
	},

	"Verify Flex Credentials" : function (browser)
	{browser
			.useXpath()
			.assert.elementPresent("//h3[contains(text(),'Flex Server Credentials')]")
			;
	},
	
	"Verify Flex Subscription" : function (browser)
	{browser
			.useXpath()
			.assert.elementPresent("//li[@tooltip='FLEX Integration is Activated']")
	},
	
	"Logout 2" : function (browser)
	{
		browser.logout();
	},
};	