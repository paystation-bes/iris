/**
* @summary System Admin- Test That Flex Subscription displays, and selection can be saved on existing account
* @since 7.3.4
* @version 1.0
* @author Lonney McD
* @see US718
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var username2 = data("FlexChildName");
var username3 = data("FlexParentName");
var password2 = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	"Login System Admin 1" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
	"Search and Select Customer Oranj - 1" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//input[@id='search']", 2000)
			.setValue("//input[@id='search']",'oranj')
			.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'oranj')]", 2000)
			.click("//a[@class='ui-corner-all'][contains(text(),'oranj')]")
			.click("//a[@id='btnGo']")
			.pause(3000);
	},

	"Click Edit Customer Oranj - 1" : function (browser)
	{browser
			.useXpath()
			.assert.visible("//a[@id='btnEditCustomer']")
			.click("//a[@id='btnEditCustomer']")
			;
	},
		
	"Verify Flex Subscription 1" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//label[contains(.,'FLEX Integration')]/a[@id='subscriptionList:1700']", 2000)
			.assert.elementPresent("//label[contains(.,'FLEX Integration')]/a[@class='checkBox checked']")
			;
	},
	
	"Deselect Flex and verify deselection" : function (browser)
	{browser
			.useXpath()
			.click("//a[@id='subscriptionList:1700']")
			.assert.elementPresent("//label[contains(.,'FLEX Integration')]/a[@class='checkBox']")
			.assert.elementNotPresent("//label[contains(.,'FLEX Integration')]/a[@class='checkBox checked']")
			;
	},
	
	"Click Update Customer 1" : function (browser)
	{browser
			.useXpath()
			.click("//span[@class='ui-button-text'][contains(text(),'Update Customer')]")
			.pause(3000)
			;
	},
	
	"Verify No Flex Subscription 1" : function (browser)
	{browser
			.useXpath()
			.assert.elementPresent("//li[@title=' FLEX Integration is Available']")
			;
	},
	
	"Logout 1" : function (browser) 
	{
		browser.logout();
	},
	
	"Login Customer Account 1" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username2, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Navigate to Settings>Global>Settings 1" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//a[@href='/secure/settings/index.html?']", 2000)
			.click("//a[@href='/secure/settings/index.html?']")
			;
	},
	
	"Verify Flex No Subscription 2" : function (browser)
	{browser
			.useXpath()
			.assert.elementPresent("//li[@tooltip='FLEX Integration is Available']")
			;
	},

	"Logout 2" : function (browser) 
	{
		browser.logout();
	},
		
	"Login System Admin 2" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},

	"Search and Select Customer 2" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//input[@id='search']", 2000)
			.setValue("//input[@id='search']",'oranj')
			.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'oranj')]", 2000)
			.click("//a[@class='ui-corner-all'][contains(text(),'oranj')]")
			.click("//a[@id='btnGo']")
			.pause(1000);
	},

	"Click Edit Customer 3" : function (browser)
	{browser
			.useXpath()
			.click("//a[@id='btnEditCustomer']")
			;
	},

	"Re-select Flex and Verify Selection" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//label[contains(.,'FLEX Integration')]/a[@id='subscriptionList:1700']", 2000)
			.click("//a[@id='subscriptionList:1700']")
			.pause(1000)
			;
	},

	"Click Update Customer 2" : function (browser)
	{browser
			.useXpath()
			.click("//span[@class='ui-button-text'][contains(text(),'Update Customer')]")
			.pause(3000)
			;
	},

	"Verify Flex Subscription 3" : function (browser)
	{browser
			.useXpath()
			.assert.elementPresent("//li[@title='FLEX Integration is Activated']")
			;
	},

	"Logout 3" : function (browser) 
	{
		browser.logout();
	},

	"Login Customer Account 2" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username2, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Navigate to Settings>Global>Settings 2" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//a[@href='/secure/settings/index.html?']", 2000)
			.click("//a[@href='/secure/settings/index.html?']")
			.pause(2000)
			;
	},
	
	"Verify Flex Subscription 4" : function (browser)
	{browser
			.useXpath()
			.waitForElementPresent("//li[@tooltip='FLEX Integration is Activated']", 2000)
			;
	},

	"Logout 4" : function (browser) 
	{
		browser.logout();
	},
	
};	