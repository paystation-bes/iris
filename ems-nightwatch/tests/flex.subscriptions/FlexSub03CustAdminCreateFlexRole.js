/**
* @summary Customer Admin- Test that a role can be created containing Flex Permissions
* @since 7.3.4
* @version 1.0
* @author Lonney McD
* @see US718
* ***NOTES*** This test case requires updated flex roles to be added to build, before automation can be finalized
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var username2 = data("FlexChildName");
var username3 = data("FlexParentName");
var password2 = data("Password");


module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	"Login Customer Account oranjchildqa - 1" : function (browser) 
	{
		browser
			.url(devurl)
			.windowMaximize('current')
			.login(username2, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Navigate to Settings>Users>Roles 1" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//a[@href='/secure/settings/index.html?']", 2000)
			.click("//a[@href='/secure/settings/index.html?']")
			.waitForElementVisible("//a[@title='Users']", 4000)
			.click("//a[@title='Users']")
			.waitForElementVisible("//a[@title='Roles']", 2000)
			.click("//a[@title='Roles']")
			.waitForElementVisible("//section[@class='groupBox menuBox userList']//h2[contains(text(),'Roles')]", 2000)
			;
	},

	"Click Add Role" : function (browser)
	{browser
			.useXpath()
			.click("//a[@id='btnAddRole']")
			.waitForElementVisible("//h2[@id='addHeading'][contains(text(),'Add Role')]", 2000)
			;
	},
		
	"Add Name For Role, and Select all permissions, Including Flex permissions" : function (browser)
	{browser
			.useCss()
			.click("#formRoleName")
			.setValue("#formRoleName", "FlexRole")
			.useXpath()
			.click("//label[contains(.,'Manage Reports')]/a[@class='checkBox']")
			.click("//label[contains(.,'Search for Transactions in Permit Lookup')]/a[@class='checkBox']")
			.click("//label[contains(.,'View Current Alerts')]/a[@class='checkBox']")
			.click("//label[contains(.,'Manage User Defined Alerts')]/a[@class='checkBox']")
			.click("//label[contains(.,'View Collection Status')]/a[@class='checkBox']")
			.click("//label[contains(.,'Issue Refunds')]/a[@class='checkBox']")
			.click("//label[contains(.,'View Banned Cards')]/a[@class='checkBox']")
			.click("//label[contains(.,'Manage Banned Cards')]/a[@class='checkBox']")
			.click("//label[contains(.,'Process Card Charges From BOSS')]/a[@class='checkBox']")
			.click("//label[contains(.,'Setup Credit Card Processing')]/a[@class='checkBox']")
			.click("//label[contains(.,'View Campus Cards')]/a[@class='checkBox']")
			.click("//label[contains(.,'Manage Campus Cards')]/a[@class='checkBox']")
			.click("//label[contains(.,'Configure Campus Cards')]/a[@class='checkBox']")
			.click("//label[contains(.,'View Passcards')]/a[@class='checkBox']")
			.click("//label[contains(.,'Manage Passcards')]/a[@class='checkBox']")
			.click("//label[contains(.,'Configure Passcards')]/a[@class='checkBox']")
			.click("//label[contains(.,'View Coupons')]/a[@class='checkBox']")
			.click("//label[contains(.,'Manage Coupons')]/a[@class='checkBox']")
			.click("//label[contains(.,'View Customer Accounts')]/a[@class='checkBox']")
			.click("//label[contains(.,'Manage Customer Accounts')]/a[@class='checkBox']")
			.click("//label[contains(.,'View Rates & Policies')]/a[@class='checkBox']")
			.click("//label[contains(.,'Manage Rates & Policies')]/a[@class='checkBox']")
			.click("//label[contains(.,'View Users & Roles')]/a[@class='checkBox']")
			.click("//label[contains(.,'Manage Users & Roles')]/a[@class='checkBox']")
			.click("//label[contains(.,'Upload Configurations from BOSS')]/a[@class='checkBox']")
			.click("//label[contains(.,'View System Activities')]/a[@class='checkBox']")
			.click("//label[contains(.,'View Locations')]/a[@class='checkBox']")
			.click("//label[contains(.,'Manage Locations')]/a[@class='checkBox']")
			.click("//label[contains(.,'View Pay Stations')]/a[@class='checkBox']")
			.click("//label[contains(.,'Manage Pay Stations')]/a[@class='checkBox']")
			.click("//label[contains(.,'View Routes')]/a[@class='checkBox']")
			.click("//label[contains(.,'Manage Routes')]/a[@class='checkBox']")
			.click("//label[contains(.,'System Settings')]/a[@class='checkBox']")
			.click("//label[contains(.,'View Pay Station Settings')]/a[@class='checkBox']")
			.click("//label[contains(.,'Manage Pay Station Settings')]/a[@class='checkBox']")
			.click("//label[contains(.,'Manage Pay Station Placement')]/a[@class='checkBox']")
			.click("//label[contains(.,'Access Digital API')]/a[@class='checkBox']")
			.click("//label[contains(.,'PDA Access')]/a[@class='checkBox']")
			.click("//label[contains(.,'View Dashboard')]/a[@class='checkBox']")
			.click("//label[contains(.,'Manage Dashboard')]/a[@class='checkBox']")
			.click("//label[contains(.,'View Mobile Devices')]/a[@class='checkBox']")
			.click("//label[contains(.,'Manage Mobile Devices')]/a[@class='checkBox']")
			.click("//label[contains(.,'View Online Rates')]/a[@class='checkBox']")
			.click("//label[contains(.,'Manage Online Rates')]/a[@class='checkBox']")
			.assert.elementPresent("//label[contains(.,'Flex WS Credentials')]/a[@class='checkBox']")
			.click("//label[contains(.,'Flex WS Credentials')]/a[@class='checkBox']")
			.assert.elementPresent("//label[contains(.,'Flex WS Credentials')]/a[@class='checkBox checked']")
			.assert.elementPresent("//label[contains(.,'Manage Flex WS Credentials')]/a[@class='checkBox']")
			.click("//label[contains(.,'Manage Flex WS Credentials')]/a[@class='checkBox']")
			.assert.elementPresent("//label[contains(.,'Manage Flex WS Credentials')]/a[@class='checkBox checked']")
			.pause(2000)
			;
	},
	
	"Save Role" : function (browser)
	{browser
			.useXpath()
			.click("//a[@class='linkButtonFtr textButton save']")
			.pause(5000)
			;
	},
	
	"Verify Role" : function (browser)
	{browser
			.useXpath()
			.assert.elementPresent("//ul[@id='roleList']//span[contains(text(),'FlexRole')]")
			;
	},
	
	"Navigate to Settings> Users> User Account" : function (browser)
	{browser
			.useXpath()
			.click("//a[@title='User Account']")
			.waitForElementVisible("//section[@class='groupBox menuBox userList']//h2[contains(text(),'Users')]", 5000)
			;
	},
	
	"Click Add User" : function (browser)
	{browser
			.useCss()
			.click("#btnAddUser")
			.pause(2000)
			.useXpath()
			.assert.elementPresent("//h2[@id='addHeading'][contains(text(),'Add User')]")
			;
	},
	
	"Enter User Name, Email, Password, Active" : function (browser) 
	{browser
			.useCss()
			.click("#formUserFirstName")
			.setValue("#formUserFirstName", "Flex")
			.click("#formUserLastName")
			.setValue("#formUserLastName", "Role")
			.click("#formUserName")
			.setValue("#formUserName", "FlexRole")
			.click("#newPassword")
			.setValue("#newPassword", "Password$2")
			.click("#c_newPassword")
			.setValue("#c_newPassword", "Password$2")
			.useXpath()
			.click("//a[@id='statusCheck']")
			.assert.elementPresent("//a[@class='checkBox checked']")
			;
	},
	
	"Add Flex Role" : function (browser) 
	{browser
			.assert.elementPresent("//ul[@id='userAllRoles']/li[contains(.,'FlexRole')]/a[@class='checkBox']")
			.click("//ul[@id='userAllRoles']/li[contains(.,'FlexRole')]/a[@class='checkBox']")
			.pause(1000)
			.assert.elementPresent("//ul[@id='userSelectedRoles']/li[contains(.,'FlexRole')]/a[@class='checkBox']")
			;
	},
	
	"Save Flex User Role" : function (browser)
	{browser
			.useXpath()
			.click("//a[@class='linkButtonFtr textButton save']")
			.pause(3000)
			;
	},
	
	"Verify Flex User Role" : function (browser)
	{browser
			.useXpath()
			.assert.elementPresent("//li[@class='actv-true']/span[contains(text(),'Flex Role')]")
			;
	},

	"Logout 1" : function (browser) 
	{
		browser.logout();
	},	
	
	"Login Customer Account With Flex User Role" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.useCss()
			.click("#username")
			.setValue("#username", "FlexRole")
			.click("#password")
			.setValue("#password", "Password$2")
			.click("#submit")
			.assert.title('Digital Iris - Change Password');
	},
	
	"Update Password" : function (browser)
	{browser
			.useCss()
			.click("#newPassword")
			.setValue("#newPassword", "Password$1")
			.click("#c_newPassword")
			.setValue("#c_newPassword", "Password$1")
			.click("#changePassword")
			.pause(3000)
			.assert.title('Digital Iris - Main (Dashboard)')
			;
	},
	
		"Logout 2" : function (browser) 
	{
		browser.logout();
	},	
};	