/**
* @summary System Admin- test that flex subscription can be enabled
* @since 7.3.4
* @version 1.0
* @author Lonney McD
* @see US718
* @requires FlexSub09SysAdminDisableFlexSub.js
* @prerequisite for FlexSub18CustAdminWidgetDataYes.js, FlexSub19CustAdminTestVisibleYes.js, FlexSub20CustAdminLocationYes.js, FlexSub21SysAdminFlexCredYes.js, FlexSub22ChildFlexCredYes.js, FlexSub23ParentFlexCredYes.js
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var username2 = data("FlexChildName");
var username3 = data("FlexParentName");
var password2 = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	"Login System Admin 1" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
	"Search and Select Customer 1" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//input[@id='search']", 2000)
			.setValue("//input[@id='search']",'oranj')
			.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'oranj')]", 2000)
			.click("//a[@class='ui-corner-all'][contains(text(),'oranj')]")
			.click("//a[@id='btnGo']")
			.pause(1000);
			;
	},

	"Click Edit Child Customer 1" : function (browser)
	{browser
			.useXpath()
			.click("//a[@id='btnEditCustomer']")
			.pause(2000)
			;
	},
		
	"Add Flex Subscription1" : function (browser)
	{browser
			.useXpath()
			.click("//a[@id='subscriptionList:1700']")
			.pause(2000)
			;
	},

	"Click Update Customer1" : function (browser)
	{browser
			.useXpath()
			.click("//span[@class='ui-button-text'][contains(text(),'Update Customer')]")
			.pause(2000)
			;
	},
	
	"Verify Flex Subscription 1" : function (browser)
	{browser
			.useXpath()
			.assert.elementPresent("//li[@title='FLEX Integration is Activated']")
			;
	},
	
	"Logout 1" : function (browser) 
	{
		browser.logout();
	},	
};	