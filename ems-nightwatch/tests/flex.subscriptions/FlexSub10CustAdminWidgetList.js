/**
* @summary Customer Admin- Widgets- Test that Flex widgets remain in selection list when Flex subscription is disabled
* @since 7.3.4
* @version 1.0
* @author Lonney McD
* @see US718
* @requires FlexSub09SysAdminDisableFlexSub.js
* ***NOTE*** requires Flex Widgets to be implemented to complete script, but script was tested and verified by substituting existing widget ids
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var username2 = data("FlexChildName");
var username3 = data("FlexParentName");
var password2 = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	"Login Customer Admin" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username2, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Edit Dashboard" : function (browser)
	{browser
			.useCss()
			.click("a.linkButtonFtr.edit")
			.useXpath()
			.waitForElementVisible("//section[@class='layout5 layout']/section/section/a[@class='linkButtonIcn addWidget'][@title='Add Widget']", 2000)
			;
	},

	"Click Add Widget" : function (browser)
	{browser
			.useXpath()
			.click("//section[@class='layout5 layout']/section/section/a[@class='linkButtonIcn addWidget'][@title='Add Widget']")
			.waitForElementVisible("//span[@class='ui-dialog-title'][contains(text(),'Add Widget')]", 2000)
			.pause(2000)
			;
	},
	
	"Expand List" : function (browser)
	{browser
			.useXpath()
// The following steps may not be correct, and will rely on actual implementation of Flex Widgets on site			
	//		.assert.elementPresent("//h3[contains(text(),'Flex')]")
	//		.click("//h3[contains(text(),'Flex')]")
			.pause(2000)
			;
	},
	
	"Verify Flex Widgets in List" : function (browser)
	{browser
			.useXpath()
// The following steps may not be correct, and will rely on actual implementation of Flex Widgets on site			
	//		.assert.elementVisible("//li/header[contains(text(),'Flex Citations')]")
	//		.assert.elementVisible("//li/header[contains(text(),'Flex Citation Map')]")
			;
	},
	
	"Click Cancel Button 1" : function (browser)
	{browser
			.useXpath()
			.click("//span[contains(text(),'Cancel')]")
			.pause(1000)
			;
	},
	
	"Logout 1" : function (browser) 
	{
		browser.logout();
	},	
};	