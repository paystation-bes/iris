/**
* @summary System Admin- Test that Flex Credentials Tab is visible and selectable when Flex Subscription is re-enabled
* @since 7.3.4
* @version 1.0
* @author Lonney McD
* @see US718
* @requires FlexSub09SysAdminDisableFlexSub.js
* @requires FlexSub17SysAdminEnableFlexSub.js
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var username2 = data("FlexChildName");
var username3 = data("FlexParentName");
var password2 = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	"Login System Admin 1" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
	"Search and Select Customer 1" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//input[@id='search']", 2000)
			.setValue("//input[@id='search']",'oranj')
			.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'oranj')]", 2000)
			.click("//a[@class='ui-corner-all'][contains(text(),'oranj')]")
			.click("//a[@id='btnGo']")
			.pause(1000);
			;
	},

	"Navigate to Licenses" : function (browser)
	{browser
			.useXpath()
			.assert.elementPresent("//a[@title='Licenses']")
			.click("//a[@title='Licenses']")
			.pause(2000)
			;
	},
		
	"Verify Integrations tab present" : function (browser)
	{browser
			.useXpath()
			.assert.elementPresent("//a[@title='Digital API']")
			.assert.elementPresent("//a[@title='Mobile Licenses']")
			.assert.elementPresent("//a[@title='Integrations']")
			;
	},
	
	"Select Integrations tab" : function (browser)
	{browser
			.useXpath()
			.click("//a[@title='Integrations']")
			.pause(2000)
			;
	},
	
	"Verify Flex Credentials Test section present" : function (browser)
	{browser
			.useXpath()
			.waitForFlex()
			;
	},
	
	"Verify Flex Server Credentials section present" : function (browser)
	{browser
			.useXpath()
			.assert.elementPresent("//section[@id='flexIntegration']//h2[contains(text(),'Flex Server Credentials')]")
			;
	},
	
	"Logout 1" : function (browser) 
	{
		browser.logout();
	},	
};	