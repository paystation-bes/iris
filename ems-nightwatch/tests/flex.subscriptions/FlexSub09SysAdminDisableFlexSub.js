/**
* @summary System Admin- Test That Flex Subscription can be disabled
* @since 7.3.4
* @version 1.0
* @author Lonney McD
* @see US718
* @prerequisite for FlexSub10CustAdminWidgetList.js, FlexSub11CustAdminWidgetDataNo.js, FlexSub12CustAdminTestVisibleNo.js,	FlexSub13CustAdminLocationNo.js, FlexSub14SysAdminFlexCredNo.js, FlexSub15ChildFlexCredNo.js, FlexSub16ParentFlexCredNo
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var username2 = data("FlexChildName");
var username3 = data("FlexParentName");
var password2 = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	"Login System Admin 1" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
	"Search and Select Child Customer 1" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//input[@id='search']", 2000)
			.setValue("//input[@id='search']",'oranj')
			.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'oranj')]", 2000)
			.click("//a[@class='ui-corner-all'][contains(text(),'oranj')]")
			.click("//a[@id='btnGo']")
			.pause(1000);
			;
	},

	"Click Edit Child Customer 1" : function (browser)
	{browser
			.useXpath()
			.click("//a[@id='btnEditCustomer']")
			;
	},
		
	"Verify Flex Subscription 1" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//label[contains(.,'FLEX Integration')]/a[@id='subscriptionList:1700']", 2000)
			;
	},
	
	"Deselect Flex 1" : function (browser)
	{browser
			.useXpath()
			.click("//a[@id='subscriptionList:1700']")
			.pause(1000)
			;
	},
	
	"Click Update Customer 1" : function (browser)
	{browser
			.useXpath()
			.click("//span[@class='ui-button-text'][contains(text(),'Update Customer')]")
			.pause(2000)
			;
	},
	
	"Verify No Flex Subscription 1" : function (browser)
	{browser
			.useXpath()
			.assert.elementPresent("//li[@title=' FLEX Integration is Available']")
			;
	},
	
	"Logout 1" : function (browser) 
	{
		browser.logout();
	},	
};	