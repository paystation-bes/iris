/**
* @summary Customer Admin- Locations- Test that Facilities and Properties cannot be viewed or edited when Flex Subscription is disabled
* @since 7.3.4
* @version 1.0
* @author Lonney McD
* @see US718
* @requires FlexSub09SysAdminDisableFlexSub.js
* ***NOTE*** requires final elements for Properties and Facilities to complete script
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var username2 = data("FlexChildName");
var username3 = data("FlexParentName");
var password2 = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	"Login Customer Admin 1" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username2, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Navigate to Settings>Locations>Location Details" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//a[@href='/secure/settings/index.html?']", 2000)
			.click("//a[@href='/secure/settings/index.html?']")
			.pause(5000)
			.click("//a[@title='Locations']")
			.waitForElementVisible("//section[@class='locName'][contains(text(),'Airport')]", 2000)
			;
	},

	"Select a Location" : function (browser)
	{browser
			.useXpath()
			.click("//section[@class='locName'][contains(text(),'Airport')]")
			.waitForElementPresent("//h2[@id='locationName'][contains(text(),'Airport')]", 2000)
			.assert.elementPresent("//section[@class='locName'][contains(text(),'Airport')]")
			;
	},
	
	"Verify No Flex Properties or Facilities 1" : function (browser)
	{browser
			.useCss()
			.assert.hidden("#propChildList")
			.assert.hidden("#facChildList")
			;
	},
	
	"Edit Location" : function (browser)
	{browser
			.useXpath()
			.click("//header/a[@title='Option Menu']")
			.waitForElementPresent("//section[@class='column second']//section[@class='menuOptions innerBorder']/a[@class='edit btnEditLocation'][@title='Edit']", 1000)
			.click("//section[@class='column second']//section[@class='menuOptions innerBorder']/a[@class='edit btnEditLocation'][@title='Edit']")
			.pause(2000)
			;
	},
	
	"Verify No Flex Properties or Facilities 2" : function (browser)
	{browser
			.useCss()
			.assert.hidden("#locPropFullList")	
			.assert.hidden("#locFacFullList")	
			;			
	},
	
	"Click Cancel" : function (browser)
	{browser
			.useXpath()
			.click("//a[@class='linkButton textButton cancel'][contains(text(),'Cancel')]")
			.pause(1000)
			;
	},
	
	"Logout 1" : function (browser) 
	{
		browser.logout();
	},	
};	