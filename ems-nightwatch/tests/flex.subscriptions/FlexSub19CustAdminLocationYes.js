/**
* @summary Customer Admin- Locations- Test that Facilities and Properties can be viewed and edited when Flex Subscription is enabled
* @since 7.3.4
* @version 1.0
* @author Lonney McD
* @see US718
* @requires FlexSub09SysAdminDisableFlexSub.js
* @requires FlexSub17SysAdminEnableFlexSub.js
* ***NOTE*** requires final elements for Properties and Facilities to complete script
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var username2 = data("FlexChildName");
var username3 = data("FlexParentName");
var password2 = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	"Login Customer Admin 1" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username2, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Navigate to Settings>Locations>Location Details" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//a[@href='/secure/settings/index.html?']", 2000)
			.click("//a[@href='/secure/settings/index.html?']")
			.pause(5000)
			.click("//a[@title='Locations']")
			.waitForElementVisible("//section[@class='locName'][contains(text(),'Airport')]", 2000)
			;
	},

	"Select a Location" : function (browser)
	{browser
			.useXpath()
			.click("//section[@class='locName'][contains(text(),'Airport')]")
			.waitForElementPresent("//h2[@id='locationName'][contains(text(),'Airport')]", 2000)
			.assert.elementPresent("//section[@class='locName'][contains(text(),'Airport')]")
			;
	},
	
	"Verify Flex Properties and  Facilities Present 1" : function (browser)
	// This step requires that the location have facilities and properties already assigned, and will fail if that data is not set up
	 {browser
	// 		.useCss()
	// 		.assert.visible("#propChildList")
	// 		.assert.visible("#facChildList")
	// 		;
	},
	
	"Edit Location" : function (browser)
	{browser
			.useXpath()
			.click("//header/a[@title='Option Menu']")
			.waitForElementPresent("//section[@class='column second']//section[@class='menuOptions innerBorder']/a[@class='edit btnEditLocation'][@title='Edit']", 1000)
			.click("//section[@class='column second']//section[@class='menuOptions innerBorder']/a[@class='edit btnEditLocation'][@title='Edit']")
			.pause(2000)
			;
	},
	
	"Verify Flex Properties and  Facilities Present 2" : function (browser)
	{browser
			.useCss()
			.assert.elementPresent("#locPropFullList")
			.assert.visible("#locPropFullList")
			.assert.visible("#locFacFullList")
			;			
	},
	
	"Click Cancel" : function (browser)
	{browser
			.useXpath()
			.click("//a[@class='linkButton textButton cancel'][contains(text(),'Cancel')]")
			.pause(1000)
			;
	},
	
	"Logout 1" : function (browser) 
	{
		browser.logout();
	},	
};	