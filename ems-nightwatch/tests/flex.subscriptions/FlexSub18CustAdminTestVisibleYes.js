/**
* @summary Customer Admin- Customer Admin- Test That Flex Subscription is visible on settings page with Flex Subscription re-enabled
* @since 7.3.4
* @version 1.0
* @author Lonney McD
* @see US718
* @requires FlexSub09SysAdminDisableFlexSub.js
* @requires FlexSub17SysAdminEnableFlexSub.js
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var username2 = data("FlexChildName");
var username3 = data("FlexParentName");
var password2 = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag', 'featuretesttag'],
	"Login Customer Child Admin 1" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username2, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Navigate to Settings>Global>Settings" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//a[@href='/secure/settings/index.html?']", 2000)
			.click("//a[@href='/secure/settings/index.html?']")
			.pause(5000)
			;
	},

	"Verify Flex Credentials Section" : function (browser)
	{browser
			.useXpath()
			.assert.elementPresent("//h3[contains(text(),'Flex Server Credentials')]")
			;
	},
	
	"Verify Flex Subscription" : function (browser)
	{browser
			.useXpath()
			.assert.elementPresent("//li[@tooltip='FLEX Integration is Activated']")
			;
	},
	
	"Logout 1" : function (browser) 
	{
		browser.logout();
	},	
};	