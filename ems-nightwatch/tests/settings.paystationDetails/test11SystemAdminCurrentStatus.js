/*
* @Credentials: Customer Admin
* @Description: Verifies the Current Status page in a Paystation Details
*
*/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";

module.exports = {
	tags: ['smoketag', 'completetesttag'],

	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
	"Search for Customer Oranj" : function (browser)
	{
		browser
			.useXpath()
			.setValue("//input[@id='search' and @class='autoText acWidthSearch stndrdChrctr hiddenLabel ui-autocomplete-input' and @type='text']", 'oranj')
			.pause(5000)
			.click("//a[@id='btnGo' and @class='linkButtonFtr textButton search']") 
			.pause(1000)
	},

	"Click on 'Pay Stations' Tab" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='tab' and @title='Pay Stations']", 2000)
			.click("//a[@class='tab' and @title='Pay Stations']")
			.pause(2000)
	},
	
	"Click on a Pay Station" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//ul[@id='paystationList']//li[1]//span", 3000)
			.click("//ul[@id='paystationList']//li[1]//span")
			.pause(1000)
	},
	
	"Verify Overall Contents of 'Current Status' tab" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//img[@width='15' and @height='19' and @title='filter']", 200)
			.waitForElementVisible("//a[@class='linkButton reload right clear refreshPOSCurrentTab' and text()='Reload']", 200)
	},
	
	"Verify Contents of 'Battery Voltage' Widget" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//span[@class='wdgtName' and text()='Battery Voltage']", 200)
			.waitForElementVisible("//section[@id='batteryVoltageWdgt' and @class='groupBox psWidget']/section[@class='groupBoxBody']/section[@class='snglValueIcon']/img[@width='125' and @height='125']", 200)
			.waitForElementVisible("//h1[@class='valueDisplay' and text()='N/AV']", 200)
			.waitForElementVisible("//section[@id='batteryVoltageWdgt' and @class='groupBox psWidget']//section[@class='smallHeading' and text()='Recent Sensor Data']", 200)
	},
	
	"Verify Content of 'Charging Current' Widget" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//span[@class='wdgtName' and text()='Charging Current']", 200)
			.waitForElementVisible("//section[@id='inputCurrentWdgt' and @class='groupBox psWidget']/section[@class='groupBoxBody']/section[@class='snglValueIcon']/img[@width='125' and @height='125']", 200)
			.waitForElementVisible("//h1[@class='valueDisplay' and text()='N/AmA']", 200)
			.waitForElementVisible("//section[@id='inputCurrentWdgt' and @class='groupBox psWidget']/section[@class='smallHeading' and text()='Recent Sensor Data']", 200)
	},
	
	"Verify Content of 'Temperature' Widget" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//span[@class='wdgtName' and text()='Temperature']", 200)
			.waitForElementVisible("//section[@id='ControllerTemperatureWdgt' and @class='groupBox psWidget']/section[@class='groupBoxBody']/section[@class='snglValueIcon']/img[@width='125' and @height='125']", 200)
			.waitForElementVisible("//h1[@class='valueDisplay' and text()='N/A° C']", 200)
			.waitForElementVisible("//section[@id='ControllerTemperatureWdgt' and @class='groupBox psWidget']/section[@class='smallHeading' and text()='Recent Sensor Data']", 200)
	},
	
	"Verify Content of 'Relative Humidity' Widget" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//span[@class='wdgtName' and text()='Relative Humidity']", 200)
			.waitForElementVisible("//section[@id='relativeHumidityWdgt' and @class='groupBox psWidget']/section[@class='groupBoxBody']/section[@class='snglValueIcon']/img[@width='125' and @height='125']", 200)
			.waitForElementVisible("//h1[@class='valueDisplay' and text()='N/A%']", 200)
			.waitForElementVisible("//section[@id='relativeHumidityWdgt' and @class='groupBox psWidget']/section[@class='smallHeading' and text()='Recent Sensor Data']", 200)	
	},
	
	"Verify Content of 'System Load' Widget" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//span[@class='systemLoadName' and text()='System Load']", 200)
			.waitForElementVisible("//section[@id='systemLoadWdgt' and @class='groupBox psWidget']/section[@class='groupBoxBody']/section[@class='snglValueIcon']/img[@width='125' and @height='125']", 200)
			.waitForElementVisible("//h1[@class='valueDisplay' and text()='N/AmA']", 200)
			.waitForElementVisible("//section[@id='systemLoadWdgt' and @class='groupBox psWidget']/section[@class='smallHeading' and text()='Recent Sensor Data']", 200)	
	},
	
	"Logout" : function (browser) 
	{
		browser.logout();
	}
};
