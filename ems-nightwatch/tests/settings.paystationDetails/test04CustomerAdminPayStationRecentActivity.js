/*
* @Credentials: Customer Admin
* @Description: Verifies the first listed Paystation's Recent Activity page by sending Alerts and clearing them. Verifies the Filter.
*
*  NOTE: MUST UPDATE SCRIPT ONCE EMS-9616 IS MERGED INTO DEV. 
* 	- Update Status: "Cleared" to "Resolved"
*
*/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";

var payStationNum;

var BillCanisterRemoved = {
            type : 'BillCanister',
            action : 'Removed'
        }
var CoinEscrowJam = {
            type : 'CoinEscrow',
            action : 'Jam'
        }
var PrinterJam = {
			type: 'Printer',
			action: 'PaperJam'
}

var billCanisterAlertID;
var coinEscrowAlertID;
var printerAlertID;

module.exports = {
	tags: ['smoketag', 'completetesttag', 'done'],

	/*
	* Logs into Iris with System Admin Account
	*/
	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	/*
	* Click on Settings
	*/	
	"Click on Settings" : function (browser)
	{
		browser
			.useXpath()
			.click("//a[contains(text(),'Settings')]")
			.pause(1000)
			.assert.title("Digital Iris - Settings - Global : Settings")
			.waitForFlex()


	},

	/*
	* Navigates to Pay Stations
	*/	
	"Click on 'Pay Stations' Tab" : function (browser)
	{
		browser
			.useXpath()
			.click("//a[@class='tab' and @title='Pay Stations']")
			.pause(1000)
			.assert.title("Digital Iris - Settings - Pay Stations : Pay Station List")
			.pause(2000)
	},

	/*
	* Get Serial Number of first listed Pay Station
	*/
	"Get Serial Number of First listed Pay Station" : function (browser){
		browser

		.getText("//ul[@id='paystationList']//li[1]//span", function(result){
			payStationNum = result.value;
		})

		.perform(function(client, done) {
   			//payStationNum = payStationNum.slice(0,12);
			console.log("Using Pay Station: " + payStationNum);
   			done();
   		 });
		
	},

	/*
	* Use the custom command sendEvent2 to send the events specified uptop to a specified pay station
	*/
	"Send Alerts to Pay Station" : function(browser){
		console.log("Sending events to Pay Station");
		browser.sendEvent2([BillCanisterRemoved, CoinEscrowJam, PrinterJam], payStationNum);

	},

	/*
	* Select the Pay Station 
	*/	
	"Click on the Pay Station" : function (browser)
	{
		browser
			.useXpath()
			.click("//span[@class='container' and text()='" + payStationNum + "']")
			.pause(1000)
	},

	/*
	* Navigate to it's Recent Activity tab
	*/	
	"Click on the 'Recent Activity' tab" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='hdrButtonIcn menu' and @title='Option Menu']", 1000)
			.click("//a[@class='tab' and @title='Recent Activity']")
			.pause(3000)
			.assert.elementPresent("//p[contains(text(),'Printer Paper Jam')]/../following-sibling::div//p[contains(text(), 'Active')]")
			.assert.elementPresent("//p[contains(text(),'Coin Escrow Jam')]/../following-sibling::div//p[contains(text(), 'Active')]")
			.assert.elementPresent("//p[contains(text(),'Bill Canister Removed')]/../following-sibling::div//p[contains(text(), 'Active')]")
	},

	/*
	* Gets the IDs of the sent Alerts
	*/
	"Get ID of sent Alerts" : function (browser){
		browser

		// Get Printer Paper Jam Alert ID
		.getAttribute("//p[contains(text(),'Printer Paper Jam')]/../following-sibling::div//p[contains(text(), 'Active')]/../..", "id", function(result){
			printerAlertID = result.value;
			console.log(printerAlertID);
		});

		browser
		// Get Coin Escrow Jam Alert ID
		.getAttribute("//p[contains(text(),'Coin Escrow Jam')]/../following-sibling::div//p[contains(text(), 'Active')]/../..", "id", function(result){
			coinEscrowAlertID = result.value;
			console.log(coinEscrowAlertID);
		});

		browser
		// Get Bill Canister Removed Alert ID
		.getAttribute("//p[contains(text(),'Bill Canister Removed')]/../following-sibling::div//p[contains(text(), 'Active')]/../..", "id", function(result){
			billCanisterAlertID = result.value;
			console.log(billCanisterAlertID);
		});


	},

	/*
	* Set Alert Filter to only show Active Alerts
	*/
	"Set Status Filter to Active" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//label[@for='filterUser' and text()='Status:']", 1000)
			.click("//label[@for='filterUser' and text()='Status:']")
			.click("//li[@id='1' and text()='Active']")
			.pause(2000)
			.assert.elementPresent("//section[@id='" + printerAlertID + "']")
			.assert.elementPresent("//section[@id='" + coinEscrowAlertID + "']")
			.assert.elementPresent("//section[@id='" + billCanisterAlertID + "']")
	},

	/*
	* Set Alert filter to show only Cleared Alerts. Assert that your sent alerts are not present on this list
	*/
	"Set Status Filter to Cleared" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//label[@for='filterUser' and text()='Status:']", 1000)
			.click("//label[@for='filterUser' and text()='Status:']")
			.click("//li[@id='0' and text()='Cleared']")
			.pause(2000)
			.assert.elementNotPresent("//section[@id='" + printerAlertID + "']")
			.assert.elementNotPresent("//section[@id='" + coinEscrowAlertID + "']")
			.assert.elementNotPresent("//section[@id='" + billCanisterAlertID + "']")
	},

	/*
	* Set Alert filter to show all alerts
	*/
	"Set Status Filter to All" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//label[@for='filterUser' and text()='Status:']", 1000)
			.click("//label[@for='filterUser' and text()='Status:']")
			.click("//li[@id='All' and text()='All']")
			.pause(5000)
			.assert.elementPresent("//section[@id='" + printerAlertID + "']")
			.assert.elementPresent("//section[@id='" + coinEscrowAlertID + "']")
			.assert.elementPresent("//section[@id='" + billCanisterAlertID + "']")
	},

	/*
	* Clear the sent alerts and click the reload button
	*/
	"Clear Alerts and reload" : function (browser){
		browser

		// Click clear buttons
		.click("//a[@id='clear" + printerAlertID.slice(6, 33) + "']")
		.click("//a[@id='clear" + coinEscrowAlertID.slice(6, 33) + "']")
		.click("//a[@id='clear" + billCanisterAlertID.slice(6, 33) + "']")
		.pause(2000)

		// Click Reload
		.click("//article[@id='alertsArea']/a[@class='linkButton reload right clear refreshPOSCurrentTab']")
		.pause(2000)
	},

	/*
	* Get the ID of cleared Alerts since once you clear the Alerts their IDs are changed
	*/
	"Get ID of cleared Alerts" : function (browser){

		browser
		// Get Printer Paper Jam Alert ID
		.getAttribute("//p[contains(text(),'Printer Paper Jam')]/../following-sibling::div//p[contains(text(), 'just now')]/../..", "id", function(result){
			printerAlertID = result.value;
			console.log(printerAlertID);
		});

		browser
		// Get Coin Escrow Jam Alert ID
		.getAttribute("//p[contains(text(),'Coin Escrow Jam')]/../following-sibling::div//p[contains(text(), 'just now')]/../..", "id", function(result){
			coinEscrowAlertID = result.value;
			console.log(coinEscrowAlertID);
		});

		browser
		// Get Bill Canister Removed Alert ID
		.getAttribute("//p[contains(text(),'Bill Canister Removed')]/../following-sibling::div//p[contains(text(), 'just now')]/../..", "id", function(result){
			billCanisterAlertID = result.value;
			console.log(billCanisterAlertID);
		});
	},

	/*
	* Set alert filter to Cleared to show your cleared alerts
	*/
		"Set Status Filter to Cleared, Verify Alerts are present" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//label[@for='filterUser' and text()='Status:']", 1000)
			.click("//label[@for='filterUser' and text()='Status:']")
			.click("//li[@id='0' and text()='Cleared']")
			.pause(2000)
			.assert.elementPresent("//section[@id='" + printerAlertID + "']")
			.assert.elementPresent("//section[@id='" + coinEscrowAlertID + "']")
			.assert.elementPresent("//section[@id='" + billCanisterAlertID + "']")
	},

	/*
	* Set Alert filter to show minor alerts. Assert Bill Canister Alert is present here
	*/
	"Set Severity Filter to Minor" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//label[@for='filterUser' and text()='Severity:']", 1000)
			.click("//label[@for='filterUser' and text()='Severity:']")
			.click("//li[text()='Minor']")
			.pause(2000)
			.assert.elementPresent("//section[@id='" + billCanisterAlertID + "']//p[contains(text(),'Cleared')]")
	},

	/*
	* Set Alert filter to show major alerts. Assert Coin Escrow Alert is present here
	*/	
	"Set Severity Filter to Major" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//label[@for='filterUser' and text()='Severity:']", 1000)
			.click("//label[@for='filterUser' and text()='Severity:']")
			.click("//li[text()='Major']")
			.pause(2000)
			.assert.elementPresent("//section[@id='" + coinEscrowAlertID + "']//p[contains(text(),'Cleared')]")
	},

	/*
	* Set Alert filter to show critical alerts. Assert Printer Alert is present here
	*/
	"Set Severity Filter to Critical" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//label[@for='filterUser' and text()='Severity:']", 1000)
			.click("//label[@for='filterUser' and text()='Severity:']")
			.click("//li[text()='Critical']")
			.pause(2000)
			.assert.elementPresent("//section[@id='" + printerAlertID + "']//p[contains(text(),'Cleared')]")
	},
	
	/*
	* Set Alert filter to show all alerts. Assert all alerts are present here
	*/
	"Set Severity Filter to All" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//label[@for='filterUser' and text()='Severity:']", 1000)
			.click("//label[@for='filterUser' and text()='Severity:']")
			.useCss()
			.click("#selectedSeverityList > #All")
			.useXpath()
			.pause(3000)
			.assert.elementPresent("//section[@id='" + printerAlertID + "']//p[contains(text(),'Cleared')]")
			.assert.elementPresent("//section[@id='" + coinEscrowAlertID + "']//p[contains(text(),'Cleared')]")
			.assert.elementPresent("//section[@id='" + billCanisterAlertID + "']//p[contains(text(),'Cleared')]")
	}, 
	
	/*
	* Select the Bill Stacker module. Assert that your Bill Canister Alert is present here
	*/
	"Select Bill Stacker Module, Verify Bill Canister Removed Alert present" : function(browser){
		browser
		.click("//label[@for='filterUser' and text()='Module:']")
		.pause(1000)
		.click("//ul[@id='selectedModuleList']/li[contains(text(),'Bill Stacker')]")
		.pause(2000)
		.assert.elementPresent("//section[@id='" + billCanisterAlertID + "']//p[contains(text(),'Cleared')]")

	},

	/*
	* Select the Coin Escrow module. Assert that your Coin Escrow Jam Alert is present here
	*/
	"Select Coin Escrow Module, Verify Coin Escrow Jam Alert Present" : function(browser){
		browser
		// Clicks the Module filter section
		.click("(//section[@class='filterMenu'])[3]")
		// Move down the drop down menu to see Coin Escrow option
		.keys("\uE00F")
		.keys("\uE00F")
		.keys("\uE00F")
		.keys("\uE00F")
		.keys("\uE00F")
		browser
		.pause(1000)
		.click("//ul[@id='selectedModuleList']/li[contains(text(),'Coin Escrow')]")
		.pause(2000)
		.assert.elementPresent("//section[@id='" + coinEscrowAlertID + "']//p[contains(text(),'Cleared')]")
	},

	/*
	* Select the Printer module. Assert that your Printer Paper Jam alert is present here
	*/
	"Select Printer Module, Verify Printer Paper Jam Alert Present" : function(browser){
		browser
		.click("//label[@for='filterUser' and text()='Module:']")
		.pause(1000)
		.click("//ul[@id='selectedModuleList']/li[contains(text(),'Printer')]")
		.pause(2000)
		.assert.elementPresent("//section[@id='" + printerAlertID + "']//p[contains(text(),'Cleared')]")
	},

	/*
	* Assert that the Export buttons are present
	*/
	"Check that Export Buttons are Present" : function (browser)
	{
		browser
			.useXpath()
			.assert.elementPresent("//a[@class='linkButton export right' and text()='Export']")
			.assert.elementPresent("//a[@class='linkButton export right clear' and text()='Export']")
	},
	
	/*
	* Logs user out of Iris
	*/
	"Logout" : function (browser) 
	{
		browser.logout();
	}

};