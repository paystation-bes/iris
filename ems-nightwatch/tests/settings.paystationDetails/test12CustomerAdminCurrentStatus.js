var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/*
	* Logs into Iris with System Admin Account
	*/
	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	/*
	* Click on Settings
	*/	
	"Click on Settings" : function (browser)
	{
		browser
			.useXpath()
			.click("//a[contains(text(),'Settings')]")
			.pause(1000)
			.waitForFlex()
			.assert.title("Digital Iris - Settings - Global : Settings");
	},

	/*
	* Navigates to Pay Stations
	*/	
	"Click on 'Pay Stations' Tab" : function (browser)
	{
		browser
			.useXpath()
			.click("//a[@class='tab' and @title='Pay Stations']")
			.pause(1000)
			.assert.title("Digital Iris - Settings - Pay Stations : Pay Station List")
			.pause(2000)
	},
	
	"Click on a Pay Station" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//ul[@id='paystationList']//li[1]//span", 1000)
			.click("//ul[@id='paystationList']//li[1]//span")
			.pause(1000)
	},
	
	"Verify Overall Contents of 'Current Status' tab" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//img[@width='15' and @height='19' and @title='filter']", 200)
			.waitForElementVisible("//a[@class='linkButton reload right clear refreshPOSCurrentTab' and text()='Reload']", 200)
	},
	
	"Verify Contents of 'Battery Voltage' Widget" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//span[@class='wdgtName' and text()='Battery Voltage']", 200)
			.waitForElementVisible("//section[@id='batteryVoltageWdgt' and @class='groupBox psWidget']/section[@class='groupBoxBody']/section[@class='snglValueIcon']/img[@width='125' and @height='125']", 200)
			.waitForElementVisible("//h1[@class='valueDisplay' and text()='N/AV']", 200)
			.waitForElementVisible("//section[@id='batteryVoltageWdgt' and @class='groupBox psWidget']//section[@class='smallHeading' and text()='Recent Sensor Data']", 200)
	},
	
	"Verify Content of 'Charging Current' Widget" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//span[@class='wdgtName' and text()='Charging Current']", 200)
			.waitForElementVisible("//section[@id='inputCurrentWdgt' and @class='groupBox psWidget']/section[@class='groupBoxBody']/section[@class='snglValueIcon']/img[@width='125' and @height='125']", 200)
			.waitForElementVisible("//h1[@class='valueDisplay' and text()='N/AmA']", 200)
			.waitForElementVisible("//section[@id='inputCurrentWdgt' and @class='groupBox psWidget']/section[@class='smallHeading' and text()='Recent Sensor Data']", 200)
	},
	
	"Verify Content of 'Temperature' Widget" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//span[@class='wdgtName' and text()='Temperature']", 200)
			.waitForElementVisible("//section[@id='ControllerTemperatureWdgt' and @class='groupBox psWidget']/section[@class='groupBoxBody']/section[@class='snglValueIcon']/img[@width='125' and @height='125']", 200)
			.waitForElementVisible("//h1[@class='valueDisplay' and text()='N/A° C']", 200)
			.waitForElementVisible("//section[@id='ControllerTemperatureWdgt' and @class='groupBox psWidget']/section[@class='smallHeading' and text()='Recent Sensor Data']", 200)
	},
	
	
	
	"Verify Content of 'Relative Humidity' Widget" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//span[@class='wdgtName' and text()='Relative Humidity']", 200)
			.waitForElementVisible("//section[@id='relativeHumidityWdgt' and @class='groupBox psWidget']/section[@class='groupBoxBody']/section[@class='snglValueIcon']/img[@width='125' and @height='125']", 200)
			.waitForElementVisible("//h1[@class='valueDisplay' and text()='N/A%']", 200)
			.waitForElementVisible("//section[@id='relativeHumidityWdgt' and @class='groupBox psWidget']/section[@class='smallHeading' and text()='Recent Sensor Data']", 200)	
	},
	
	"Logout" : function (browser) 
	{
		browser.logout();
	},
	
	
};