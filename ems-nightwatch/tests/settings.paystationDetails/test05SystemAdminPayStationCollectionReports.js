/*
* @Credentials: System Admin
* @Description: Verifies the first listed Paystation's Collection Reports tab with sending CC(Swipe), Cash/CC(Swipe), CC(Tap), Cash/CC(Tap) transactions
*
*/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";
var payStationNum;

module.exports = {
	tags: ['smoketag', 'completetesttag'],

	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},

	"Search for Customer Oranj" : function (browser)
	{
		browser
			.useXpath()
			.setValue("//input[@id='search' and @class='autoText acWidthSearch stndrdChrctr hiddenLabel ui-autocomplete-input' and @type='text']", 'oranj')
			.pause(5000)
			.click("//a[@id='btnGo' and @class='linkButtonFtr textButton search']") 
			.pause(1000)
	},
	
	"Click on 'Pay Stations' Tab" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='tab' and @title='Pay Stations']", 2000)
			.click("//a[@class='tab' and @title='Pay Stations']")
			.pause(4000)
	},

	"Get Serial Number of First listed Pay Station" : function (browser){
		browser

		.getText("//ul[@id='paystationList']/span/li[1]//span[@class='textLine']", function(result){
			payStationNum = result.value;
		})

		.perform(function(client, done) {
   			payStationNum = payStationNum.slice(0,12);
			console.log("Using Pay Station: " + payStationNum);
   			done();
   		 });
		
	},

	"Click on the Pay Station" : function (browser)
	{
		browser
			.useXpath()
			.click("//span[@class='textLine' and text()='" + payStationNum + " ']")
			.pause(1000)
	},
	
	"Click on the 'Reports' Tab" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='tab' and @title='Reports' and @href='#']", 2000)
			.click("//a[@class='tab' and @title='Reports' and @href='#']")
			.pause(1000)
	},
	
	"Send a Coin Collection" : function (browser)
	{
		browser
		.sendCoinCollection([1,2,3,4,5], payStationNum)
	},

	"Click Reload and Verify that the Coin Collection went through" : function(browser)
	{
		browser
		.useXpath()
		.assert.visible("//article[@id='reportsArea']/a[contains(text(),'Reload')]")
		.click("//article[@id='reportsArea']/a[contains(text(),'Reload')]")
		.pause(3000)
		.assert.visible("//ul[@id='collectionReportList']//li[1]/div[@class='col2']/p[contains(text(),'Coin')]")
		.assert.visible("//ul[@id='collectionReportList']//li[1]/div[@class='col3']/p[contains(text(),'$15.00')]")
	},

	"Click the Coin Collection Report" : function(browser)
	{
		browser
		.useXpath()
		.click("//ul[@id='collectionReportList']//li[1]/div[@class='col2']/p[contains(text(),'Coin')]")
		.pause(3000)
	},

	"Verify Coin Collection Report" : function(browser)
	{
		browser
		.useXpath()
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'Collection Type')]/following-sibling::dd[contains(text(),'Coin')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'$0.05')]/following-sibling::dd[contains(text(),'$0.05')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'$0.10')]/following-sibling::dd[contains(text(),'$0.20')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'$0.25')]/following-sibling::dd[contains(text(),'$0.75')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'$1.00')]/following-sibling::dd[contains(text(),'$4.00')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'$2.00')]/following-sibling::dd[contains(text(),'$10.00')]")
		.assert.visible("//section[@class='reportBody']//dt/strong[contains(text(),'Total')]/../following-sibling::dd/strong[contains(text(),'$15.00')]")

	},
	
	"Close the Coin Collection Report" : function (browser)
	{
		browser
			.useCss()
			.assert.visible(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")
			.click(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")
			.pause(1000)
	},

	"Send a Bill Collection" : function (browser)
	{
		browser
		.sendBillCollection([1,2,3,4,5,6], payStationNum)
	},

	"Click Reload and Verify that the Bill Collection went through" : function(browser)
	{
		browser
		.useXpath()
		.assert.visible("//article[@id='reportsArea']/a[contains(text(),'Reload')]")
		.click("//article[@id='reportsArea']/a[contains(text(),'Reload')]")
		.pause(3000)
		.assert.visible("//ul[@id='collectionReportList']//li[1]/div[@class='col2']/p[contains(text(),'Bill')]")
		.assert.visible("//ul[@id='collectionReportList']//li[1]/div[@class='col3']/p[contains(text(),'$460.00')]")
	},

	"Click the Bill Collection Report" : function(browser)
	{
		browser
		.useXpath()
		.click("//ul[@id='collectionReportList']//li[1]/div[@class='col2']/p[contains(text(),'Bill')]")
		.pause(3000)
	},

	"Verify Bill Collection Report" : function(browser)
	{
		browser
		.useXpath()
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'Collection Type')]/following-sibling::dd[contains(text(),'Bill')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'$1')]/following-sibling::dd[contains(text(),'$1.00')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'$2')]/following-sibling::dd[contains(text(),'$4.00')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'$5')]/following-sibling::dd[contains(text(),'$15.00')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'$10')]/following-sibling::dd[contains(text(),'$40.00')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'$20')]/following-sibling::dd[contains(text(),'$100.00')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'$50')]/following-sibling::dd[contains(text(),'$300.00')]")
		.assert.visible("//section[@class='reportBody']//dt/strong[contains(text(),'Total')]/../following-sibling::dd/strong[contains(text(),'$460.00')]")

	},
	
	"Close the Bill Collection Report" : function (browser)
	{
		browser
			.useCss()
			.assert.visible(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")
			.click(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")
			.pause(1000)
	},

	"Send a Card Collection" : function (browser)
	{
		browser
		.sendCardCollection([1,2,3,4,5,6,7], payStationNum)
	},

	"Click Reload and Verify that the Card Collection went through" : function(browser)
	{
		browser
		.useXpath()
		.assert.visible("//article[@id='reportsArea']/a[contains(text(),'Reload')]")
		.click("//article[@id='reportsArea']/a[contains(text(),'Reload')]")
		.pause(3000)
		.assert.visible("//ul[@id='collectionReportList']//li[1]/div[@class='col2']/p[contains(text(),'Card')]")
		.assert.visible("//ul[@id='collectionReportList']//li[1]/div[@class='col3']/p[contains(text(),'$30.00')]")
	},

	"Click the Card Collection Report" : function(browser)
	{
		browser
		.useXpath()
		.click("//ul[@id='collectionReportList']//li[1]/div[@class='col2']/p[contains(text(),'Card')]")
		.pause(3000)
	},

	"Verify Card Collection Report" : function(browser)
	{
		browser
		.useXpath()
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'Collection Type')]/following-sibling::dd[contains(text(),'Card')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'Visa')]/following-sibling::dd[contains(text(),'$3.00')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'MasterCard')]/following-sibling::dd[contains(text(),'$4.00')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'Amex')]/following-sibling::dd[contains(text(),'$5.00')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'Discover')]/following-sibling::dd[contains(text(),'$7.00')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'Diners Club')]/following-sibling::dd[contains(text(),'$6.00')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'Smart Cards')]/following-sibling::dd[contains(text(),'$2.00')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'Other')]/following-sibling::dd[contains(text(),'$1.00')]")
		.assert.visible("//section[@class='reportBody']//dt/strong[contains(text(),'Total')]/../following-sibling::dd/strong[contains(text(),'$28.00')]")

	},
	
	"Close the Card Collection Report" : function (browser)
	{
		browser
			.useCss()
			.assert.visible(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")
			.click(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")
			.pause(1000)
	},

	"Send an All Collection" : function (browser)
	{
		browser
		.sendAllCollection(payStationNum)
	},

	"Click Reload and Verify that the All Collection went through" : function(browser)
	{
		browser
		.useXpath()
		.assert.visible("//article[@id='reportsArea']/a[contains(text(),'Reload')]")
		.click("//article[@id='reportsArea']/a[contains(text(),'Reload')]")
		.pause(3000)
		.assert.visible("//ul[@id='collectionReportList']//li[1]/div[@class='col2']/p[contains(text(),'All')]")
		.assert.visible("//ul[@id='collectionReportList']//li[1]/div[@class='col3']/p[contains(text(),'$184.23')]")
	},

	"Click the All Collection Report" : function(browser)
	{
		browser
		.useXpath()
		.click("//ul[@id='collectionReportList']//li[1]/div[@class='col2']/p[contains(text(),'All')]")
		.pause(3000)
	},

	"Verify All Collection Report" : function(browser)
	{
		browser
		.useXpath()
		.assert.visible("//h2[contains(text(),'Coin Tube Status')]")
		.assert.visible("//h2[contains(text(),'Coin Changer')]")
		.assert.visible("//h2[contains(text(),'Coin Hopper Status')]")
		.assert.visible("//h2[contains(text(),'Coin Hoppers')]")
		.assert.visible("//h2[contains(text(),'Coin Bag')]")
		.assert.visible("//h2[contains(text(),'Bill Stacker')]")
		.assert.visible("//h2[contains(text(),'Credit Cards')]")
		.assert.visible("//h2[contains(text(),'Smart Card')]")
		.assert.visible("//h2[contains(text(),'OVERPAYMENT')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'COLLECTIONS')]/following-sibling::dd[contains(text(),'$186.23')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'CHANGE ISSUED')]/following-sibling::dd[contains(text(),'$2.00')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'REFUNDS ISSUED')]/following-sibling::dd[contains(text(),'$0.00')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'REVENUE')]/following-sibling::dd[contains(text(),'$184.23')]")

	},
	
	"Close the All Collection Report" : function (browser)
	{
		browser
			.useCss()
			.assert.visible(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")
			.click(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")
			.pause(1000)
	},

	"Logout" : function (browser) 
	{
		browser.logout();
	}
};