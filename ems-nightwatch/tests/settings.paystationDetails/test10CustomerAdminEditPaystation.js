/*
* @Credentials: System Admin
* @Description: Verifies the functionality of editing a Paystation's information by changing it's name.
*
*/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";
var payStationNum;
var payStationID;

module.exports = {
	tags: ['smoketag', 'completetesttag'],

	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Click on Settings in the Sidebar" : function (browser) 
	{
		browser
			.useXpath()
			.assert.visible("//section[@class='bottomSection']/a[@title='Settings']")
			.click("//section[@class='bottomSection']/a[@title='Settings']")
			.waitForFlex();
	},
	
	"Click on 'Pay Stations' Tab" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='tab' and @title='Pay Stations']", 2000)
			.click("//a[@class='tab' and @title='Pay Stations']")
			.pause(3000)
	},

	"Get Serial Number of First listed Pay Station" : function (browser){
		browser

		.getText("//ul[@id='paystationList']//li[1]//span", function(result){
			payStationNum = result.value;
		})

		.perform(function(client, done) {
   			payStationNum = payStationNum.slice(0,12);
			console.log("Using Pay Station: " + payStationNum);
   			done();
   		 });
		
	},

	"Get ID of Pay Station" : function (browser)
	{
		browser
		.useXpath()
		.getAttribute("//span[contains(text(), '" + payStationNum +"')]/../..", "id", function(result){
			payStationID = result.value;
		});
	},


	"Click Edit for the Paystation" : function (browser)
	{
			browser
			.useXpath()
			.click("//li[@id='" + payStationID + "']/a[@title='Edit']")
			.pause(3000)

			.useCss()
			.assert.visible("#formPayStationName")
	},

	"Edit Paystation name" : function (browser)
	{
		browser
		.useCss()
		.click("#formPayStationName")
		.clearValue("#formPayStationName")
		.setValue("#formPayStationName", "AutomationEdit")
	},

	"Click Save" : function(browser)
	{
		browser
		.useCss()
		.click(".linkButtonFtr.textButton.save")
		.pause(7000)
	},

	"Verify Paystation name change" : function(browser)
	{
		browser
		.useXpath()
		.assert.elementPresent("//li[@id='" + payStationID + "']//span[contains(text(),'AutomationEdit')]")
		.pause(1000)
		.assert.elementPresent("//h1[contains(text(),'AutomationEdit')]")

	},

	"Click Edit for the Paystation 2" : function (browser)
	{
			browser
			.useXpath()
			.click("//li[@id='" + payStationID + "']/a[@title='Edit']")
			.pause(3000)

			.useCss()
			.assert.visible("#formPayStationName")
	},
	
	"Revert Paystation name and click save" : function (browser)
	{
		browser
		.useCss()
		.click("#formPayStationName")
		.clearValue("#formPayStationName")
		.setValue("#formPayStationName", payStationNum)
		.pause(3000)

		.useCss()
		.click(".linkButtonFtr.textButton.save")
		.pause(3000)
	},

	"Verify Paystation name change 2" : function(browser)
	{
		browser
		.useXpath()
		.assert.elementPresent("//li[@id='" + payStationID + "']//span[contains(text(),'" + payStationNum + "')]")
		.assert.elementPresent("//h1[contains(text(),'" + payStationNum + "')]")

	},


	"Logout" : function (browser) 
	{
		browser.logout();
	},
		
};