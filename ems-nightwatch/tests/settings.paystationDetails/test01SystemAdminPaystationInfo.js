/*
* @Credentials: System Admin
* @Description: Creates a new Paystation and verifies it's information tab
*/


var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";
var customerName = "oranj";
var paystationSerialNumberToCreate = "500000070050";


module.exports = {
	tags: ['smoketag', 'completetesttag', 'done'],
	

	"Login to System Admin" : function (browser) 
	{
		console.log("User: " + username + "/" + password);
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
	"Search for oranj (Oranj Parent)" : function (browser) 
	{
		browser
			.useCss()
			.waitForElementVisible("#search", 2000)
			.setValue("#search", customerName)
			.pause(2000)
			.assert.visible("#btnGo")
			.click("#btnGo");

	},
	
	"Navigate to Pay Stations Tab" : function (browser)
	{
			browser
			.useXpath()
			.waitForElementVisible(".//a[@title='Pay Stations']", 3000)
			.click(".//a[@title='Pay Stations']");
	},


	"Click Add Paystation(s) button" : function (browser){
			browser
			.useCss()
			.waitForElementVisible("#btnAddPayStation", 2000)
			.click("#btnAddPayStation")
			.pause(1000)
			.assert.visible("#formSerialNumbers");
		},
		
	"Enter Paystation Serial Number(s) into Add Paystation form": function(browser){
			browser
			.useCss()
			.setValue("#formSerialNumbers", paystationSerialNumberToCreate)
			.pause(3000)
			.useXpath()
			.click("//span[contains(text(), 'Add Pay Station') and @class='ui-button-text'] ")
			.pause(5000)
		
		
		},

	"Verify Pay Station is now on list" : function(browser)
	{
			browser
			.useXpath()
			.pause(3000)
			.assert.visible("//span[contains(text(),'" + paystationSerialNumberToCreate +"')]")
			.pause(2000)
	},
		

	"Click on the Pay Station listing" : function(browser)
	{
		browser
		.useXpath()
		.click("//span[contains(text(),'" + paystationSerialNumberToCreate +"')]")
		.pause(5000)
	},

	"Click on Information tab" : function(browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//a[@title='Information']", 3000)
		.click("//a[@title='Information']")
		.pause(2000)
	},

	"Verify Pay Station Information" : function(browser)
	{
		browser
		.useXpath()
		.assert.visible("//dd[@id='psName' and contains(text(),'" + paystationSerialNumberToCreate + "')]")
		.assert.visible("//dd[@id='psSerialNumber' and contains(text(),'" + paystationSerialNumberToCreate + "')]")
		.assert.visible("//dd[@id='psType' and contains(text(),'LUKE II')]")
		.assert.visible("//dd[@id='psLocation']/a[contains(text(),'Unassigned')]")
		.assert.visible("//dd[@id='psStatus' and contains(text(),'Inactive')]")
	},

	"Activate Pay Station" : function(browser)
	{
		browser
		.useXpath()
		.click("(//span[contains(text(),'" + paystationSerialNumberToCreate + "')])[1]/../../../a[@title='Edit']")
		.pause(2000)

		.useCss()
		.waitForElementVisible("#btnPayStationActivate", 2000)
		.click("#btnPayStationActivate")
		.pause(1000)
		.assert.visible(".linkButtonFtr.textButton.save")
		.click(".linkButtonFtr.textButton.save")
		.pause(5000)

	},

	"Click on Information tab again" : function(browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//a[@title='Information']", 3000)
		.click("//a[@title='Information']")
		.pause(5000)
	},

	"Verify Activated Pay Station" : function(browser)
	{
		browser
		.useXpath()
		.assert.visible("//dd[@id='psName' and contains(text(),'" + paystationSerialNumberToCreate + "')]")
		.assert.visible("//dd[@id='psSerialNumber' and contains(text(),'" + paystationSerialNumberToCreate + "')]")
		.assert.visible("//dd[@id='psType' and contains(text(),'LUKE II')]")
		.assert.visible("//dd[@id='psLocation']/a[contains(text(),'Unassigned')]")
		.assert.visible("//dd[@id='psStatus' and contains(text(),'Active')]")
	},
	
	"Logout" : function (browser) 
	{
		browser.logout();
	}
};