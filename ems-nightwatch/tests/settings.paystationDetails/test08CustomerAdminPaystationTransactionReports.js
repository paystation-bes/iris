/*
* @Credentials: Customer Admin
* @Description: Verifies the first listed Paystation's Transaction Reports tab with sending CC(Swipe), Cash/CC(Swipe), CC(Tap), Cash/CC(Tap) transactions
*
*/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";
var payStationNum;
var CardNumber = data("CardNumber");

module.exports = {
	tags: ['smoketag', 'completetesttag', 'skip'],

	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Click on Settings in the Sidebar" : function (browser) 
	{
		browser
			.useXpath()
			.assert.visible("//section[@class='bottomSection']/a[@title='Settings']")
			.click("//section[@class='bottomSection']/a[@title='Settings']")
			.pause(3000)
			.waitForFlex()
	},
	
	"Click on 'Pay Stations' Tab" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='tab' and @title='Pay Stations']", 2000)
			.click("//a[@class='tab' and @title='Pay Stations']")
			.pause(3000)
	},

	"Get Serial Number of First listed Pay Station" : function (browser){
		browser

		.getText("//ul[@id='paystationList']//li[1]//span", function(result){
			payStationNum = result.value;
		})

		.perform(function(client, done) {
   			payStationNum = payStationNum.slice(0,12);
			console.log("Using Pay Station: " + payStationNum);
   			done();
   		 });
		
	},

	"Click on the Pay Station" : function (browser)
	{
		browser
			.useXpath()
			.click("//span[text()='" + payStationNum + "']")
			.pause(4000)
	},
	
	"Click on the 'Reports' Tab" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='tab' and @title='Reports' and @href='#']", 2000)
			.click("//a[@class='tab' and @title='Reports' and @href='#']")
			.pause(1000)
	},


/* Note: 	
* UNCOMMENT IF WE NEED THE TRANSACTIONS TO BE REAL-TIME
* Should be Unesseccary because test07SystemAdminPaystationTransactionReports should already have assigned the merchant
*account to the first listed Paystation
*/


// /**
// 	 *@description Opens 'Add Merchant Account' dialog
// 	 *@param browser - The web browser object
// 	 *@returns void
// 	 **/
// 	"test01MerchantSetUp: Opens 'Add Merchant Account' dialog" : function (browser)
// 	{
// 		browser
// 			.useCss()
// 			.waitForElementVisible("#btnAddMrchntAccnt", 4000)
// 			.click("#btnAddMrchntAccnt")
// 	},
	
// 	/**
// 	 *@description Sets Merchant Account Criteria
// 	 *@param browser - The web browser object
// 	 *@returns void
// 	 **/
// 	"test01MerchantSetUp: enters values for all merchant account fields" : function (browser) 
// 	{
// 			browser
// 			.useCss()
// 			.waitForElementVisible("#account", 2000)
// 			.setValue("#account", "Paymentech")
// 			.assert.visible("#statusCheck")
// 			.click("#statusCheck")
// 			.assert.visible("#processorsExpand")
// 			.click("#processorsExpand")
// 			.pause(1000)
			
// 			.useXpath()
// 			.assert.visible("//a[contains(text(),'Paymentech')]")
// 			.click("//a[contains(text(),'Paymentech')]")
// 			.pause(2000)
			
// 			.useCss()
// 			.waitForElementVisible("#field1", 2000)
// 			.setValue("#field1", "700000002018")
// 			.assert.visible("#field2")
// 			.setValue("#field2", "001")
// 			.assert.visible("#field3")
// 			.setValue("#field3", "digitech1")
// 			.assert.visible("#field4")
// 			.setValue("#field4", "digpaytec01")
// 			.assert.visible("#field5")
// 			.setValue("#field5", "0002")		
// 			.useXpath()
// 			.assert.visible("//span[@class='ui-button-text'][contains(text(), 'Add Account')]")
// 			.click("//span[@class='ui-button-text'][contains(text(), 'Add Account')]")
// 			.pause(4000)
			
// 	},	
	
	
// 	/**
// 	 *@description Navigates to Paystation page
// 	 *@param browser - The web browser object
// 	 *@returns void
// 	 **/
// 	"test01MerchantSetUp: Navigates to Pay Station page" : function (browser)
// 	{
// 		browser
// 			.useXpath()
// 			browser.navigateTo("Pay Stations");
// 			browser.pause(5000);			
// 	},
	
// 	/*
// 	* Get Serial Number of first listed Pay Station
// 	*/
// 	"Get Serial Number of First listed Pay Station" : function (browser){
// 		browser

// 		.getText("//ul[@id='paystationList']/span/li[1]//span[@class='textLine']", function(result){
// 			payStationNum = result.value;
// 		})

// 		.perform(function(client, done) {
//    			payStationNum = payStationNum.slice(0,12);
// 			console.log("Using Pay Station: " + payStationNum);
//    			done();
//    		 });
		
// 	},

// 	/**
// 	 *@description Selects to edit pay station
// 	 *@param browser - The web browser object
// 	 *@returns void
// 	 **/
// 	 "test01MerchantSetUp: Select edit button for Pay Station" : function (browser) 
// 	{
// 		browser
// 			.useXpath()
// 			.assert.visible("(//ul[@id='paystationList']//a[@class='menuListButton edit'])[1]")
// 			.click("(//ul[@id='paystationList']//a[@class='menuListButton edit'])[1]")
// 			.pause(2000)
// 			.useCss()
// 			.assert.visible("#editHeading")
// 	},

// 	/**
// 	 *@description Selects merchant account for pay station
// 	 *@param browser - The web browser object
// 	 *@returns void
// 	 **/
// 	 "test01MerchantSetUp: Selects Merchant Account for Pay Station, and saves update" : function (browser) 
// 	{
// 		browser
// 			.useCss()
// 			.assert.visible("#formCreditCardAccountExpand")
// 			.click("#formCreditCardAccountExpand")
			
// 			.useXpath()
// 			.assert.visible("//a[contains(text(),'Paymentech')]")
// 			.click("//a[contains(text(),'Paymentech')]")
			
// 			.useCss()
// 			.assert.visible(".linkButtonFtr.textButton.save")
// 			.click(".linkButtonFtr.textButton.save")
// 			.pause(4000)
// 	},

	"Click on the 'Reports' Tab" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='tab' and @title='Reports' and @href='#']", 2000)
			.click("//a[@class='tab' and @title='Reports' and @href='#']")
			.pause(3000)
	},


	"Click on the Transaction Reports Tab" : function (browser)
	{
		browser
		.useCss()
		.assert.visible("#transactionReportBtn")
		.click("#transactionReportBtn")
		.pause(3000)
	},


	"Test02TransactionSetUp: Custom Command places a CC (swipe) transaction to pay station" : function (browser) 
	{
		
		 browser.sendCCPreAuthRFID("4.00", CardNumber, payStationNum, "0", function (result) {
			var CCTrans = {
				licenceNum : 'AAA111',
				stallNum : '',
				type : 'Regular',
				originalAmount : '400',
				chargedAmount: '4.00',
				cardPaid : '4.00',
				cashPaid : '0.00',
				cardAuthorizationId: result.authID,
				emsPreAuthId: result.emsPreAuthID,
			}
			browser.pause(1000);
			browser.sendCashCCPostAuth([CCTrans], payStationNum);
		});
		
	},	
	
	"Click Reload and verify that the CC(Swipe) transaction went through" : function (browser)
	{
		browser
		.useXpath()
		.assert.visible("//article[@id='reportsArea']/a[contains(text(),'Reload')]")
		.click("//article[@id='reportsArea']/a[contains(text(),'Reload')]")
		.pause(5000)
		.assert.visible("//ul[@id='transactionReportList']//li[1]/div[@class='col3']/p[contains(text(),'CC (Swipe)')]")
		.assert.visible("//ul[@id='transactionReportList']//li[1]/div[@class='col4']/p[contains(text(),'$4.00')]")
	},

	"Click the CC (Swipe) transaction Report" : function(browser)
	{
		browser
		.useXpath()
		.click("//ul[@id='transactionReportList']//li[1]/div[@class='col3']/p[contains(text(),'CC (Swipe)')]")
		.pause(3000)
	},

	"Verify CC (Swipe) Transaction Report" : function(browser)
	{
		browser
		.useXpath()
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'Payment Type')]/following-sibling::dd[contains(text(),'CC (Swipe)')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'Charged Amount')]/following-sibling::dd[contains(text(),'$4.00')]")
	},
	
	"Close the CC (Swipe) Transaction Report" : function (browser)
	{
		browser
			.useCss()
			.assert.visible(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")
			.click(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")
			.pause(1000)
	},

		"Test02TransactionSetUp: Custom Command places a CC (tap) transaction to pay station" : function (browser) 
	{
		
		 browser.sendCCPreAuthRFID("4.00", CardNumber, payStationNum, "1", function (result) {
			var CCTrans = {
				licenceNum : 'AAA222',
				stallNum : '',
				type : 'Regular',
				originalAmount : '400',
				chargedAmount: '4.00',
				cardPaid : '4.00',
				cashPaid : '0.00',
				cardAuthorizationId: result.authID,
				emsPreAuthId: result.emsPreAuthID,
			}
			
			browser.sendCashCCPostAuth([CCTrans], payStationNum);
		});
		
	},	

	"Click Reload and verify that the CC(Tap) transaction went through" : function (browser)
	{
		browser
		.useXpath()
		.assert.visible("//article[@id='reportsArea']/a[contains(text(),'Reload')]")
		.click("//article[@id='reportsArea']/a[contains(text(),'Reload')]")
		.pause(3000)
		.assert.visible("//ul[@id='transactionReportList']//li[1]/div[@class='col3']/p[contains(text(),'CC (Tap)')]")
		.assert.visible("//ul[@id='transactionReportList']//li[1]/div[@class='col4']/p[contains(text(),'$4.00')]")
	},

	"Click the CC (Tap) transaction Report" : function(browser)
	{
		browser
		.useXpath()
		.click("//ul[@id='transactionReportList']//li[1]/div[@class='col3']/p[contains(text(),'CC (Tap)')]")
		.pause(3000)
	},

	"Verify CC (Tap) Transaction Report" : function(browser)
	{
		browser
		.useXpath()
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'Payment Type')]/following-sibling::dd[contains(text(),'CC (Tap)')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'Charged Amount')]/following-sibling::dd[contains(text(),'$4.00')]")
	},
	
	"Close the CC (Tap) Transaction Report" : function (browser)
	{
		browser
			.useCss()
			.assert.visible(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")
			.click(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")
			.pause(1000)
	},

	"Test02TransactionSetUp: Custom Command places a Cash/CC (swipe) transaction to pay station" : function (browser)
	{
		
		 browser.sendCCPreAuthRFID("4.00", CardNumber, payStationNum, "0", function (result) {
			var CCTrans = {
				stallNum : '1',
				type : 'Regular',
				originalAmount : '400',
				chargedAmount: '4.00',
				cashPaid : '2.00',
				cardPaid : '2.00',
				cardAuthorizationId: result.authID,
				emsPreAuthId: result.emsPreAuthID,
			}
   
			browser.sendCashCCPostAuth([CCTrans], payStationNum);
		});
		
	},
	
	"Click Reload and verify that the Cash/CC (Swipe) transaction went through" : function (browser)
	{
		browser
		.useXpath()
		.assert.visible("//article[@id='reportsArea']/a[contains(text(),'Reload')]")
		.click("//article[@id='reportsArea']/a[contains(text(),'Reload')]")
		.pause(3000)
		.assert.visible("//ul[@id='transactionReportList']//li[1]/div[@class='col3']/p[contains(text(),'Cash/CC (Swipe)')]")
		.assert.visible("//ul[@id='transactionReportList']//li[1]/div[@class='col4']/p[contains(text(),'$4.00')]")
	},

	"Click the Cash/CC (Swipe) transaction Report" : function(browser)
	{
		browser
		.useXpath()
		.click("//ul[@id='transactionReportList']//li[1]/div[@class='col3']/p[contains(text(),'Cash/CC (Swipe)')]")
		.pause(3000)
	},

	"Verify Cash/CC (Swipe) Transaction Report" : function(browser)
	{
		browser
		.useXpath()
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'Payment Type')]/following-sibling::dd[contains(text(),'Cash/CC (Swipe)')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'Charged Amount')]/following-sibling::dd[contains(text(),'$4.00')]")
	},
	
	"Close the Cash/CC (Swipe) Transaction Report" : function (browser)
	{
		browser
			.useCss()
			.assert.visible(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")
			.click(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")
			.pause(1000)
	},

	"Test02TransactionSetUp: Custom Command places a Cash/CC (tap) transaction to pay station" : function (browser)
	{
		
		 browser.sendCCPreAuthRFID("4.00", CardNumber, payStationNum, "1", function (result) {
			var CCTrans = {
				stallNum : '2',
				type : 'Regular',
				originalAmount : '400',
				chargedAmount: '4.00',
				cashPaid : '2.00',
				cardPaid : '2.00',
				cardAuthorizationId: result.authID,
				emsPreAuthId: result.emsPreAuthID,
			}
   
			browser.sendCashCCPostAuth([CCTrans], payStationNum);
		});
		
	},

	"Click Reload and verify that the Cash/CC (Tap) transaction went through" : function (browser)
	{
		browser
		.useXpath()
		.assert.visible("//article[@id='reportsArea']/a[contains(text(),'Reload')]")
		.click("//article[@id='reportsArea']/a[contains(text(),'Reload')]")
		.pause(3000)
		.assert.visible("//ul[@id='transactionReportList']//li[1]/div[@class='col3']/p[contains(text(),'Cash/CC (Tap)')]")
		.assert.visible("//ul[@id='transactionReportList']//li[1]/div[@class='col4']/p[contains(text(),'$4.00')]")
	},

	"Click the Cash/CC (Tap) transaction Report" : function(browser)
	{
		browser
		.useXpath()
		.click("//ul[@id='transactionReportList']//li[1]/div[@class='col3']/p[contains(text(),'Cash/CC (Tap)')]")
		.pause(3000)
	},

	"Verify Cash/CC (Tap) Transaction Report" : function(browser)
	{
		browser
		.useXpath()
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'Payment Type')]/following-sibling::dd[contains(text(),'Cash/CC (Tap)')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'Charged Amount')]/following-sibling::dd[contains(text(),'$4.00')]")
	},
	
	"Close the Cash/CC (Tap) Transaction Report" : function (browser)
	{
		browser
			.useCss()
			.assert.visible(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")
			.click(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")
			.pause(1000)
	},

	"Send a Cash transaction" : function(browser)
	{
		var cashTrans = {

            licenceNum : '496AAA',
            stallNum : '1',
            type : 'Regular',
            originalAmount : '400',
            chargedAmount: '4.00',
            cashPaid : '4.00',
            isRefundSlip: 'false',
            purchaseDate : '2015:05:11:16:23:51:GMT',
            expiryDate : '2015:05:11:16:53:51:GMT',
        }
 
		browser.sendCashTrans([cashTrans], payStationNum);
	},

		"Click Reload and verify that the Cash transaction went through" : function (browser)
	{
		browser
		.useXpath()
		.assert.visible("//article[@id='reportsArea']/a[contains(text(),'Reload')]")
		.click("//article[@id='reportsArea']/a[contains(text(),'Reload')]")
		.pause(3000)
		.assert.visible("//ul[@id='transactionReportList']//li[1]/div[@class='col3']/p[contains(text(),'Cash')]")
		.assert.visible("//ul[@id='transactionReportList']//li[1]/div[@class='col4']/p[contains(text(),'$4.00')]")
	},

	"Click the Cash transaction Report" : function(browser)
	{
		browser
		.useXpath()
		.click("//ul[@id='transactionReportList']//li[1]/div[@class='col3']/p[contains(text(),'Cash')]")
		.pause(3000)
	},

	"Verify Cash Transaction Report" : function(browser)
	{
		browser
		.useXpath()
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'Payment Type')]/following-sibling::dd[contains(text(),'Cash')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'Charged Amount')]/following-sibling::dd[contains(text(),'$4.00')]")
		.assert.visible("//section[@class='reportBody']//dt[contains(text(),'Type')]/following-sibling::dd[contains(text(),'Regular')]")
	},
	
	"Close the Cash Transaction Report" : function (browser)
	{
		browser
			.useCss()
			.assert.visible(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")
			.click(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")
			.pause(1000)
	},


	"Logout" : function (browser) 
	{
		browser.logout();
	},

};