/*
* @Credentials: System Admin
* @Description: Verifies the functionality of editing a Paystation's information by changing it's name.
* Also verfies the Notes tab of the Paystation by making a note while editing
*
*/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";
var payStationNum;

module.exports = {
	tags: ['smoketag', 'completetesttag'],

	"Login to System Admin" : function (browser) 
	{
		console.log("User: " + username + "/" + password);
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
	"Search for oranj (Oranj Parent)" : function (browser) 
	{
		browser
			.useCss()
			.waitForElementVisible("#search", 2000)
			.setValue("#search", "Oranj")
			.pause(2000)
			.assert.visible("#btnGo")
			.click("#btnGo");

	},
	
	"Navigate to Pay Stations Tab" : function (browser)
	{
			browser
			.useXpath()
			.waitForElementVisible(".//a[@title='Pay Stations']", 3000)
			.click(".//a[@title='Pay Stations']")
			.pause(3000)
			.assert.title("Digital Iris - System Administration - oranj : Pay Station Details")
	},

	"Get Serial Number of First listed Pay Station" : function (browser){
		browser

		.getText("//ul[@id='paystationList']/span/li[1]//span[@class='textLine']", function(result){
			payStationNum = result.value;
		})

		.perform(function(client, done) {
   			payStationNum = payStationNum.slice(0,12);
			console.log("Using Pay Station: " + payStationNum);
   			done();
   		 });
		
	},

	"Click Edit for the Paystation" : function (browser)
	{
			browser
			.useXpath()
			.click("//label[@for=" + payStationNum + "]/../../../a[@title='Edit']")
			.pause(3000)

			.useCss()
			.assert.visible("#formPayStationName")
	},

	"Edit Paystation name and click save" : function (browser)
	{
		browser
		.useCss()
		.click("#formPayStationName")
		.clearValue("#formPayStationName")
		.setValue("#formPayStationName", "AutomationEdit")
		.pause(2000)

		.useCss()
		.click(".linkButtonFtr.textButton.save")
		.pause(7000)
	},

	"Verify Paystation name change" : function(browser)
	{
		browser
		.useXpath()
		.assert.elementPresent("//span[contains(text(), '" + payStationNum + "')]/span[contains(text(),'(AutomationEdit)')]")
		.pause(2000)
		.assert.elementPresent("//h1[contains(text(),'AutomationEdit')]")

	},

	"Click Edit for the Paystation 2" : function (browser)
	{
			browser
			.useXpath()
			.click("//label[@for=" + payStationNum + "]/../../../a[@title='Edit']")
			.pause(3000)

			.useCss()
			.assert.visible("#formPayStationName")
	},
	
	"Revert Paystation name" : function (browser)
	{
		browser
		.useCss()
		.click("#formPayStationName")
		.clearValue("#formPayStationName")
		.setValue("#formPayStationName", payStationNum)
	},

	"Make a note and click save" : function(browser)
	{
		browser
		.useCss()
		.click("#formComment")
		.setValue("#formComment", "AutomationNote")
		.pause(3000)

		.useCss()
		.click(".linkButtonFtr.textButton.save")
		.pause(10000)
	},

	"Verify Paystation name change 2" : function(browser)
	{
		browser
		.useXpath()
		.assert.elementPresent("//span[contains(text(), '" + payStationNum + "')]/span[contains(text(),'(" + payStationNum +")')]")
		.assert.elementPresent("//h1[contains(text(),'" + payStationNum +"')]")

	},

	"Navigate to Notes" : function(browser)
	{
		browser
		.useXpath()
		.click("//section[@id='paystationNavArea']//a[@title='Reports']")
		.pause(2000)

		.useCss()
		.click("#notesHistoryBtn")
		.pause(3000)

	},

	"Verify Note" : function(browser)
	{
		browser
		.useXpath()
		.assert.elementPresent("//ul[@id='noteList']//li[1]/div[@class='col1']/p[contains(text(),'" + username + "')]")
		.assert.elementPresent("//ul[@id='noteList']//li[1]/div[@class='col2']/p[contains(text(),'AutomationNote')]")
	},

	"Logout" : function (browser) 
	{
		browser.logout();
	},
		
};