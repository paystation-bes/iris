/*
* @Credentials: Customer Admin
* @Description: Verifies newly created Paystation's information tab
* @Requires: test01SystemAdminPaystationInfo
*/


var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";
var paystationSerialNumberToCreate = "500000070050";

module.exports = {
	tags: ['smoketag', 'completetesttag'],

	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Click on Settings in the Sidebar" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@title='Settings']", 2000)
			.click("//a[@title='Settings']")
			.pause(1000)
			.waitForFlex();
	},
	
	"Click on 'Pay Stations' Tab" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='tab' and @title='Pay Stations']", 2000)
			.click("//a[@class='tab' and @title='Pay Stations']")
			.pause(1000)
	},
	
	"Verify Pay Station is on list" : function(browser)
	{
			browser
			.useXpath()
			.pause(3000)
			.assert.visible("//span[contains(text(),'" + paystationSerialNumberToCreate +"')]")
			.pause(2000)
	},
		

	"Click on the Pay Station listing" : function(browser)
	{
		browser
		.useXpath()
		.click("(//span[contains(text(),'" + paystationSerialNumberToCreate +"')])[1]")
		.pause(2000)
	},

	"Click on Information tab" : function(browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//a[@title='Information']", 3000)
		.click("//a[@title='Information']")
		.pause(2000)
	},

	"Verify Activated Pay Station" : function(browser)
	{
		browser
		.useXpath()
		.assert.visible("//dd[@id='psName' and contains(text(),'" + paystationSerialNumberToCreate + "')]")
		.assert.visible("//dd[@id='psSerialNumber' and contains(text(),'" + paystationSerialNumberToCreate + "')]")
		.assert.visible("//dd[@id='psType' and contains(text(),'LUKE II')]")
		.assert.visible("//dd[@id='psLocation']/a[contains(text(),'Unassigned')]")
		.assert.visible("//dd[@id='psStatus' and contains(text(),'Active')]")
	},

	"Logout" : function (browser) 
	{
		browser
			.logout();
	}
	
};