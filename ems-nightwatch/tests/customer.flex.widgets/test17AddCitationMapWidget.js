/**
 * @summary Flex Widgets: Citation Map Widget: Add Citation Map widget and verify UI/data shows up
 * @since 7.3.6
 * @version 1.0
 * @author Mandy F
 * @see US599
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("FlexChildName");
var password = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test17AddCitationMapWidget: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
	},
	
	/**
	 * @description Go to dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test17AddCitationMapWidget: Go to Dashboard" : function (browser) {
		browser
		.navigateTo("Dashboard")
		.pause(1000)
	},
	
	/**
	 * @description Edit dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test17AddCitationMapWidget: Edit Dashboard" : function (browser) {
		var editBtn = "//section[@id='headerTools']//a[@title='Edit']";
		browser
		.useXpath()
		.waitForElementVisible(editBtn, 1000)
		.click(editBtn)
		.pause(1000)

	},
	
	/**
	 * @description Open add widget list
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test17AddCitationMapWidget: Open Add Widget List" : function (browser) {
		var addBtn = "//section[@class='sectionTools']//a[@title='Add Widget']";
		browser
		.useXpath()
		.waitForElementVisible(addBtn, 1000)
		.click(addBtn)
		.pause(1000)

	},
	
	/**
	 * @description Add Citation Map widget
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test17AddCitationMapWidget: Add Citation Map Widget" : function (browser) {
		var citationMapList = "//section[@class='widgetListContainer']//h3[text() = 'Citation Map']";
		var citationMapWidget = "//section[@class='widgetListContainer']//li//header[text() = 'Citation Map']";
		var addBtn = "//button//span[text() = 'Add widget']";
		browser
		.useXpath()
		.waitForElementVisible(citationMapList, 1000)
		.click(citationMapList)
		.waitForElementVisible(citationMapWidget, 1000)
		.click(citationMapWidget)
		.pause(1000)
		.waitForElementVisible(addBtn, 1000)
		.click(addBtn)
		.pause(1000)
	},
	
	/**
	 * @description Save dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test17AddCitationMapWidget: Save Dashboard" : function (browser) {
		var saveBtn = "//section[@id='headerTools']//a[@title='Save']";
		browser
		.useXpath()
		.waitForElementVisible(saveBtn, 1000)
		.click(saveBtn)
		.pause(3000)
	},
	
	/**
	 * @description Verifies widget is added successfully
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test17AddCitationMapWidget: Verify Widget Add Success" : function (browser) {
		var widgetName = "(//section[@class='widget']//span[@class='wdgtName'])[text()='Citation Map']";
		var optionMenu = widgetName + "//..//a[@title='Option Menu']";
		var noDataReturned = widgetName + "//..//..//section[@class='noWdgtData']//span[text() = 'There is currently no data returned for this widget.']";
		var zoomBtns = widgetName + "//..//..//div[@class='leaflet-control-zoom leaflet-bar leaflet-control']";
		var updateBar = widgetName + "//..//..//section[@id='statusDate']";
		var heatMapToggle = widgetName + "//..//..//section[@id='heatMapOverlay']";
		browser
		.useXpath()
		.assert.visible(widgetName)
		.assert.visible(optionMenu)
		.assert.visible(zoomBtns)
		.assert.visible(updateBar)
		.assert.visible(heatMapToggle)
		.assert.elementNotPresent(noDataReturned)
		.pause(1000)
	},
	
	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test17AddCitationMapWidget: Logout" : function (browser) {
		browser.logout();
	},

};
