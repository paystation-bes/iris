/**
 * @summary Flex Widgets: Citation by Month Widget: Move Citation by Month widget
 * @since 7.3.6
 * @version 1.0
 * @author Mandy F
 * @see US598 
 * @require test09AddCitationByMonthWidget.js
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("FlexChildName");
var password = data("Password");

// offset variables for the widget
var widgetXOffset = 0;
var widgetYOffset = 0;
var xOffset = 0 ;

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test10MoveCitationByMonthWidget: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
	},
	
	/**
	 * @description Go to dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test10MoveCitationByMonthWidget: Go to Dashboard" : function (browser) {
		browser
		.navigateTo("Dashboard")
		.pause(1000)
	},
	
	/**
	 * @description Edit dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test10MoveCitationByMonthWidget: Edit Dashboard" : function (browser) {
		var editBtn = "//section[@id='headerTools']//a[@title='Edit']";
		var secondCol = "//section[@class='column second ui-sortable']";
		browser
		.useXpath()
		.waitForElementVisible(editBtn, 1000)
		.click(editBtn)
		.pause(1000)
		.waitForElementVisible(secondCol, 1000)
		.getElementSize(secondCol, function (result) {
			xOffset = Math.floor(result.value.width / 2);
		})
		.pause(1000);
	},	
	
	/**
	 * @description Gets the size of the widget to calculate offsets
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test10MoveCitationByMonthWidget: Calculate Widget Offsets" : function (browser) {	
		var moveBtn = "(//section[@class='widget']//span[@class='wdgtName'])[text()='Citation by Month']//..//..//span[@title='Move']";
		browser
		.useXpath()
		.waitForElementVisible(moveBtn, 1000)
		.getElementSize(moveBtn, function (result) {
			widgetXOffset = Math.floor(result.value.width / 2);
			widgetYOffset = Math.floor(result.value.height / 2);
		})
		.pause(1000);
	},
	
	/**
	 * @description Moves the widget
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test10MoveCitationByMonthWidget: Move Widget" : function (browser) {
		var moveBtn = "(//section[@class='widget']//span[@class='wdgtName'])[text()='Citation by Month']//..";
		var secondCol = "//section[@class='column second ui-sortable']";
		browser
		.useXpath()
		.waitForElementVisible(moveBtn, 1000)
		.moveTo(moveBtn, widgetXOffset, widgetYOffset)
		.dragAndDrop(moveBtn, widgetXOffset, widgetYOffset, secondCol, xOffset, 0)
		.pause(1000)

	},
	
	/**
	 * @description Save dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test10MoveCitationByMonthWidget: Save Dashboard" : function (browser) {
		var saveBtn = "//section[@id='headerTools']//a[@title='Save']";
		browser
		.useXpath()
		.waitForElementVisible(saveBtn, 1000)
		.click(saveBtn)
		.pause(3000)
	},
	
	/**
	 * @description Verifies widget is moved successfully
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test10MoveCitationByMonthWidget: Verify Widget Move Success" : function (browser) {
		var widgetName = "(//section[@class='widget']//span[@class='wdgtName'])[text()='Citation by Month']";
		var optionMenu = widgetName + "//..//a[@title='Option Menu']";
		var noDataReturned = widgetName + "//..//..//section[@class='noWdgtData']//span[text() = 'There is currently no data returned for this widget.']";
		var dataReturned = widgetName + "//..//..//section[@class='widgetContent chartGraph']//div[@class='highcharts-container']";
		var columnOneGone = "//section[@class='column first']//section[@class='widget']//span[@class='wdgtName'][text()='Citation by Month']";
		var columnTwoContains = "//section[@class='column second']//section[@class='widget']//span[@class='wdgtName'][text()='Citation by Month']";
		browser
		.useXpath()
		.assert.visible(widgetName)
		.assert.visible(optionMenu)
		.assert.visible(dataReturned)
		.assert.visible(columnTwoContains)
		.assert.elementNotPresent(noDataReturned)
		.assert.elementNotPresent(columnOneGone)
		.pause(1000)
	},
	
	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test10MoveCitationByMonthWidget: Logout" : function (browser) {
		browser.logout();
	},

};
