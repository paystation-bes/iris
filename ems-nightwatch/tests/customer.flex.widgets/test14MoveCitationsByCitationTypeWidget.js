/**
 * @summary Flex Widgets: Citations by Citation Type Widget: Move Citations by Citation Type widget
 * @since 7.3.6
 * @version 1.0
 * @author Mandy F
 * @see US598 
 * @require test13AddCitationsByCitationTypeWidget.js
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("FlexChildName");
var password = data("Password");

// offset variables for the widget
var widgetXOffset = 0;
var widgetYOffset = 0;
var xOffset = 0 ;

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test14MoveCitationsByCitationTypeWidget: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
	},
	
	/**
	 * @description Go to dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test14MoveCitationsByCitationTypeWidget: Go to Dashboard" : function (browser) {
		browser
		.navigateTo("Dashboard")
		.pause(1000)
	},
	
	/**
	 * @description Edit dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test14MoveCitationsByCitationTypeWidget: Edit Dashboard" : function (browser) {
		var editBtn = "//section[@id='headerTools']//a[@title='Edit']";
		var secondCol = "//section[@class='column second ui-sortable']";
		browser
		.useXpath()
		.waitForElementVisible(editBtn, 1000)
		.click(editBtn)
		.pause(1000)
		.waitForElementVisible(secondCol, 1000)
		.getElementSize(secondCol, function (result) {
			xOffset = Math.floor(result.value.width / 2);
		})
		.pause(1000);
	},	
	
	/**
	 * @description Gets the size of the widget to calculate offsets
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test14MoveCitationsByCitationTypeWidget: Calculate Widget Offsets" : function (browser) {	
		var moveBtn = "(//section[@class='widget']//span[@class='wdgtName'])[text()='Citations by Citation Type']//..//..//span[@title='Move']";
		browser
		.useXpath()
		.waitForElementVisible(moveBtn, 1000)
		.getElementSize(moveBtn, function (result) {
			widgetXOffset = Math.floor(result.value.width / 2);
			widgetYOffset = Math.floor(result.value.height / 2);
		})
		.pause(1000);
	},
	
	/**
	 * @description Moves the widget
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test14MoveCitationsByCitationTypeWidget: Move Widget" : function (browser) {
		var moveBtn = "(//section[@class='widget']//span[@class='wdgtName'])[text()='Citations by Citation Type']//..";
		var secondCol = "//section[@class='column second ui-sortable']";
		browser
		.useXpath()
		.waitForElementVisible(moveBtn, 1000)
		.moveTo(moveBtn, widgetXOffset, widgetYOffset)
		.dragAndDrop(moveBtn, widgetXOffset, widgetYOffset, secondCol, xOffset, 0)
		.pause(1000)

	},
	
	/**
	 * @description Save dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test14MoveCitationsByCitationTypeWidget: Save Dashboard" : function (browser) {
		var saveBtn = "//section[@id='headerTools']//a[@title='Save']";
		browser
		.useXpath()
		.waitForElementVisible(saveBtn, 1000)
		.click(saveBtn)
		.pause(3000)
	},
	
	/**
	 * @description Verifies widget is moved successfully
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test14MoveCitationsByCitationTypeWidget: Verify Widget Move Success" : function (browser) {
		var widgetName = "(//section[@class='widget']//span[@class='wdgtName'])[text()='Citations by Citation Type']";
		var optionMenu = widgetName + "//..//a[@title='Option Menu']";
		var noDataReturned = widgetName + "//..//..//section[@class='noWdgtData']//span[text() = 'There is currently no data returned for this widget.']";
		var dataReturned = widgetName + "//..//..//section[@class='widgetContent chartGraph']//div[@class='highcharts-container']";
		var columnOneGone = "//section[@class='column first']//section[@class='widget']//span[@class='wdgtName'][text()='Citations by Citation Type']";
		var columnTwoContains = "//section[@class='column second']//section[@class='widget']//span[@class='wdgtName'][text()='Citations by Citation Type']";
		browser
		.useXpath()
		.assert.visible(widgetName)
		.assert.visible(optionMenu)
		.assert.visible(dataReturned)
		.assert.visible(columnTwoContains)
		.assert.elementNotPresent(noDataReturned)
		.assert.elementNotPresent(columnOneGone)
		.pause(1000)
	},
	
	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test14MoveCitationsByCitationTypeWidget: Logout" : function (browser) {
		browser.logout();
	},

};
