/**
 * @summary Flex Widgets: Citations by Citation Type Widget: Edit Citations by Citation Type widget and verify settings
 * @since 7.3.6
 * @version 1.0
 * @author Mandy F
 * @see US598
 * @require test13AddCitationsByCitationTypeWidget.js
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("FlexChildName");
var password = data("Password");

// edited name of the widget
var widgetNameEdited = "Citation Types EDITED";

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test15EditCitationsByCitationTypeWidget: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
	},
	
	/**
	 * @description Go to dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test15EditCitationsByCitationTypeWidget: Go to Dashboard" : function (browser) {
		browser
		.navigateTo("Dashboard")
		.pause(1000)
	},
	
	/**
	 * @description Edit dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test15EditCitationsByCitationTypeWidget: Edit Dashboard" : function (browser) {
		var editBtn = "//section[@id='headerTools']//a[@title='Edit']";
		var secondCol = "//section[@class='column second ui-sortable']";
		browser
		.useXpath()
		.waitForElementVisible(editBtn, 1000)
		.click(editBtn)
		.pause(1000)
		.waitForElementVisible(secondCol, 1000)
		.getElementSize(secondCol, function (result) {
			xOffset = Math.floor(result.value.width / 2);
		})
		.pause(1000);
	},	
	
	/**
	 * @description Open the Settings menu
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test15EditCitationsByCitationTypeWidget: Open Settings Menu" : function (browser) {
		var widget = "(//section[@class='widget']//span[@class='wdgtName'])[text()='Citations by Citation Type']//..//..";
		var optionMenu = widget + "//a[@title='Option Menu']";
		var settingsMenu = widget + "//..//a[contains(@class,'settings')]";
		browser
		.useXpath()
		.waitForElementVisible(optionMenu, 1000)
		.click(optionMenu)
		.pause(250)
		.waitForElementVisible(settingsMenu, 3000)
		.click(settingsMenu)
		.pause(1000);
	},
	
	/**
	 * @description Change widget name
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test15EditCitationsByCitationTypeWidget: Change Widget Name" : function (browser) {
		var nameForm = "//input[@id='widgetName']";
		browser
		.useXpath()
		.waitForElementVisible(nameForm, 1000)
		.clearValue(nameForm)
		.setValue(nameForm, widgetNameEdited)
		.pause(1000)
	},	
	
	/**
	 * @description Verify time frames
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test15EditCitationsByCitationTypeWidget: Verify Time Frames" : function (browser) {
		var timeFrameExpand = "//a[@id='rangeMenuExpand']";
		var timeFrame = "//li[@role='presentation']//a[text() = '";
		browser
		.useXpath()
		.waitForElementVisible(timeFrameExpand, 1000)
		.click(timeFrameExpand)
		.waitForElementVisible(timeFrame + "Last 24 Hours']", 1000)
		.assert.visible(timeFrame + "Today']")
		.assert.visible(timeFrame + "Yesterday']")
		.assert.visible(timeFrame + "Last 24 Hours']")
		.assert.visible(timeFrame + "Last 7 Days']")
		.assert.visible(timeFrame + "Last 30 Days']")
		.assert.visible(timeFrame + "Last 12 Months']")
		.assert.visible(timeFrame + "Last Week']")
		.assert.visible(timeFrame + "Last Month']")
		.assert.visible(timeFrame + "Last Year']")
		.assert.visible(timeFrame + "Year to Date']")
		.click(timeFrame + "Last 24 Hours']")
		.pause(1000)
	},	
	
	/**
	 * @description Verify refinements
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test15EditCitationsByCitationTypeWidget: Verify Refinements" : function (browser) {
		var unselectCitation = "(//ul[@class='selectedList']//li[@class='selected'])";
		var itemsNotSelected = "//ul[@class='fullList']//li[@class='']";
		browser
		.useXpath()
		.waitForElementVisible(unselectCitation + "[1]", 1000)
		.click(unselectCitation + "[1]")
		.pause(1000)
		.waitForElementVisible(unselectCitation + "[2]", 1000)
		.click(unselectCitation + "[2]")
		.pause(1000)
		.waitForElementVisible(unselectCitation + "[3]", 1000)
		.click(unselectCitation + "[3]")
		.pause(1000)
		.waitForElementVisible(itemsNotSelected, 1000)
		.assert.visible(itemsNotSelected)
		.pause(1000)
	},	
	
	/**
	 * @description Verify displays
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test15EditCitationsByCitationTypeWidget: Verify Displays" : function (browser) {
		var displayTypes = "//ul[@id='chartDesign']//section[text() = '";
		browser
		.useXpath()
		.assert.visible(displayTypes + "Area']")
		.assert.visible(displayTypes + "Column']")
		.assert.visible(displayTypes + "List']")
		.assert.visible(displayTypes + "Bar']")
		.assert.visible(displayTypes + "Line']")
		.assert.visible(displayTypes + "Pie']")
		.pause(1000)
	},	
	
	/**
	 * @description Verify no target value
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test15EditCitationsByCitationTypeWidget: Verify No Target Value" : function (browser) {
		var noTargetValue = "//section[@id='targetSwitch' and @style='display: none;']";
		browser
		.useXpath()
		.assert.elementPresent(noTargetValue)
		.pause(1000)
	},	
	
	/**
	 * @description Apply changes
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test15EditCitationsByCitationTypeWidget: Apply Changes" : function (browser) {
		var applyBtn = "//button[@type='button']//span[text() = 'Apply changes']";
		browser
		.useXpath()
		.waitForElementVisible(applyBtn, 1000)
		.click(applyBtn)
		.pause(3000)
	},
	
	/**
	 * @description Save dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test15EditCitationsByCitationTypeWidget: Save Dashboard" : function (browser) {
		var saveBtn = "//section[@id='headerTools']//a[@title='Save']";
		browser
		.useXpath()
		.waitForElementVisible(saveBtn, 1000)
		.click(saveBtn)
		.pause(3000)
	},
	
	/**
	 * @description Verifies widget is edited successfully
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test15EditCitationsByCitationTypeWidget: Verify Widget Edit Success" : function (browser) {
		var widgetName = "(//section[@class='widget']//span[@class='wdgtName'])[text()='" + widgetNameEdited + "']";
		var optionMenu = widgetName + "//..//a[@title='Option Menu']";
		var noDataReturned = widgetName + "//..//..//section[@class='noWdgtData']//span[text() = 'There is currently no data returned for this widget.']";
		var dataReturned = widgetName + "//..//..//section[@class='widgetContent chartGraph']//div[@class='highcharts-container']";
		browser
		.useXpath()
		.assert.visible(widgetName)
		.assert.visible(optionMenu)
		.assert.visible(dataReturned)
		.assert.elementNotPresent(noDataReturned)
		.pause(1000)
	},
	
	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test15EditCitationsByCitationTypeWidget: Logout" : function (browser) {
		browser.logout();
	},

};
