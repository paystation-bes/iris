/**
 * @summary Flex Widgets: Citation by Hour Widget: Edit Citation by Hour widget and verify settings
 * @since 7.3.6
 * @version 1.0
 * @author Mandy F
 * @see US598
 * @require test01AddCitationByHourWidget.js
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("FlexChildName");
var password = data("Password");

// edited name of the widget
var widgetNameEdited = "Citation by Hour EDITED";

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test03EditCitationByHourWidget: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
	},
	
	/**
	 * @description Go to dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test03EditCitationByHourWidget: Go to Dashboard" : function (browser) {
		browser
		.navigateTo("Dashboard")
		.pause(1000)
	},
	
	/**
	 * @description Edit dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test03EditCitationByHourWidget: Edit Dashboard" : function (browser) {
		var editBtn = "//section[@id='headerTools']//a[@title='Edit']";
		var secondCol = "//section[@class='column second ui-sortable']";
		browser
		.useXpath()
		.waitForElementVisible(editBtn, 1000)
		.click(editBtn)
		.pause(1000)
		.waitForElementVisible(secondCol, 1000)
		.getElementSize(secondCol, function (result) {
			xOffset = Math.floor(result.value.width / 2);
		})
		.pause(1000);
	},	
	
	/**
	 * @description Open the Settings menu
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test03EditCitationByHourWidget: Open Settings Menu" : function (browser) {
		var widget = "(//section[@class='widget']//span[@class='wdgtName'])[text()='Citation by Hour']//..//..";
		var optionMenu = widget + "//a[@title='Option Menu']";
		var settingsMenu = widget + "//..//a[contains(@class,'settings')]";
		browser
		.useXpath()
		.waitForElementVisible(optionMenu, 1000)
		.click(optionMenu)
		.pause(250)
		.waitForElementVisible(settingsMenu, 3000)
		.click(settingsMenu)
		.pause(1000);
	},
	
	/**
	 * @description Change widget name
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test03EditCitationByHourWidget: Change Widget Name" : function (browser) {
		var nameForm = "//input[@id='widgetName']";
		browser
		.useXpath()
		.waitForElementVisible(nameForm, 1000)
		.clearValue(nameForm)
		.setValue(nameForm, widgetNameEdited)
		.pause(1000)
	},	
	
	/**
	 * @description Verify time frames
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test03EditCitationByHourWidget: Verify Time Frames" : function (browser) {
		var timeFrameExpand = "//a[@id='rangeMenuExpand']";
		var timeFrame = "//li[@role='presentation']//a[text() = '";
		browser
		.useXpath()
		.waitForElementVisible(timeFrameExpand, 1000)
		.click(timeFrameExpand)
		.waitForElementVisible(timeFrame + "Today']", 1000)
		.assert.visible(timeFrame + "Today']")
		.assert.visible(timeFrame + "Yesterday']")
		.assert.visible(timeFrame + "Last 24 Hours']")
		.click(timeFrame + "Today']")
		.pause(1000)
	},	
	
	/**
	 * @description Verify refinements
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test03EditCitationByHourWidget: Verify Refinements" : function (browser) {
		var refinement = "//a[@id='tier2Check']";
		var refinementExpand = "//a[@id='tier2MenuExpand']";
		var citationType = "//li[@role='presentation']//a[text() = 'Citation Type']";
		var selectCitation = "(//ul[@class='fullList']//li)";
		var itemsSelected = "//ul[@class='selectedList']//li[@class='active selected']";
		browser
		.useXpath()
		.waitForElementVisible(refinement, 1000)
		.click(refinement)
		.waitForElementVisible(refinementExpand, 1000)
		.click(refinementExpand)
		.waitForElementVisible(citationType, 1000)
		.click(citationType)
		.waitForElementVisible(selectCitation + "[1]", 1000)
		.click(selectCitation + "[1]")
		.waitForElementVisible(selectCitation + "[2]", 1000)
		.click(selectCitation + "[2]")
		.waitForElementVisible(selectCitation + "[3]", 1000)
		.click(selectCitation + "[3]")
		.pause(1000)
		.assert.visible(itemsSelected)
		.pause(1000)
	},	
	
	/**
	 * @description Verify displays
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test03EditCitationByHourWidget: Verify Displays" : function (browser) {
		var displayTypes = "//ul[@id='chartDesign']//section[text() = '";
		browser
		.useXpath()
		.assert.visible(displayTypes + "Area']")
		.assert.visible(displayTypes + "Column']")
		.assert.visible(displayTypes + "List']")
		.assert.visible(displayTypes + "Bar']")
		.assert.visible(displayTypes + "Line']")
		.assert.visible(displayTypes + "Pie']")
		.pause(1000)
	},	
	
	/**
	 * @description Verify no target value
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test03EditCitationByHourWidget: Verify No Target Value" : function (browser) {
		var noTargetValue = "//section[@id='targetSwitch' and @style='display: none;']";
		browser
		.useXpath()
		.assert.elementPresent(noTargetValue)
		.pause(1000)
	},	
	
	/**
	 * @description Apply changes
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test03EditCitationByHourWidget: Apply Changes" : function (browser) {
		var applyBtn = "//button[@type='button']//span[text() = 'Apply changes']";
		browser
		.useXpath()
		.waitForElementVisible(applyBtn, 1000)
		.click(applyBtn)
		.pause(3000)
	},
	
	/**
	 * @description Save dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test03EditCitationByHourWidget: Save Dashboard" : function (browser) {
		var saveBtn = "//section[@id='headerTools']//a[@title='Save']";
		browser
		.useXpath()
		.waitForElementVisible(saveBtn, 1000)
		.click(saveBtn)
		.pause(3000)
	},
	
	/**
	 * @description Verifies widget is edited successfully
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test03EditCitationByHourWidget: Verify Widget Edit Success" : function (browser) {
		var widgetName = "(//section[@class='widget']//span[@class='wdgtName'])[text()='" + widgetNameEdited + "']";
		var optionMenu = widgetName + "//..//a[@title='Option Menu']";
		var noDataReturned = widgetName + "//..//..//section[@class='noWdgtData']//span[text() = 'There is currently no data returned for this widget.']";
		var dataReturned = widgetName + "//..//..//section[@class='widgetContent chartGraph']//div[@class='highcharts-container']";
		browser
		.useXpath()
		.assert.visible(widgetName)
		.assert.visible(optionMenu)
		.assert.visible(dataReturned)
		.assert.elementNotPresent(noDataReturned)
		.pause(1000)
	},
	
	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test03EditCitationByHourWidget: Logout" : function (browser) {
		browser.logout();
	},

};
