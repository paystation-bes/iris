/**
 * @summary Flex Widgets: Citation Map Widget: Edit Citation Map widget and verify settings
 * @since 7.3.6
 * @version 1.0
 * @author Mandy F
 * @see US598
 * @require test17AddCitationMapWidget.js
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("FlexChildName");
var password = data("Password");

// edited name of the widget
var widgetNameEdited = "Citation Map EDITED";

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test19EditCitationMapWidget: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
	},
	
	/**
	 * @description Go to dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test19EditCitationMapWidget: Go to Dashboard" : function (browser) {
		browser
		.navigateTo("Dashboard")
		.pause(1000)
	},
	
	/**
	 * @description Edit dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test19EditCitationMapWidget: Edit Dashboard" : function (browser) {
		var editBtn = "//section[@id='headerTools']//a[@title='Edit']";
		var secondCol = "//section[@class='column second ui-sortable']";
		browser
		.useXpath()
		.waitForElementVisible(editBtn, 1000)
		.click(editBtn)
		.pause(1000)
		.waitForElementVisible(secondCol, 1000)
		.getElementSize(secondCol, function (result) {
			xOffset = Math.floor(result.value.width / 2);
		})
		.pause(1000);
	},	
	
	/**
	 * @description Open the Settings menu
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test19EditCitationMapWidget: Open Settings Menu" : function (browser) {
		var widget = "(//section[@class='widget']//span[@class='wdgtName'])[text()='Citation Map']//..//..";
		var optionMenu = widget + "//a[@title='Option Menu']";
		var settingsMenu = widget + "//..//a[contains(@class,'settings')]";
		browser
		.useXpath()
		.waitForElementVisible(optionMenu, 1000)
		.click(optionMenu)
		.pause(250)
		.waitForElementVisible(settingsMenu, 3000)
		.click(settingsMenu)
		.pause(1000);
	},
	
	/**
	 * @description Change widget name
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test19EditCitationMapWidget: Change Widget Name" : function (browser) {
		var nameForm = "//input[@id='widgetName']";
		browser
		.useXpath()
		.waitForElementVisible(nameForm, 1000)
		.clearValue(nameForm)
		.setValue(nameForm, widgetNameEdited)
		.pause(1000)
	},	
	
	/**
	 * @description Verify time frames
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test19EditCitationMapWidget: Verify Time Frames" : function (browser) {
		var timeFrameExpand = "//a[@id='rangeMenuExpand']";
		var timeFrame = "//li[@role='presentation']//a[text() = '";
		browser
		.useXpath()
		.waitForElementVisible(timeFrameExpand, 1000)
		.click(timeFrameExpand)
		.waitForElementVisible(timeFrame + "Today']", 1000)
		.assert.visible(timeFrame + "Today']")
		.assert.visible(timeFrame + "Yesterday']")
		.assert.visible(timeFrame + "Last 24 Hours']")
		.assert.visible(timeFrame + "Last 7 Days']")
		.click(timeFrame + "Today']")
		.pause(1000)
	},	
	
	/**
	 * @description Verify refinements
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test19EditCitationMapWidget: Verify Refinements" : function (browser) {
		var unselectCitation = "(//ul[@class='selectedList']//li[@class='selected'])";
		var itemsNotSelected = "//ul[@class='fullList']//li[@class='']";
		browser
		.useXpath()
		.waitForElementVisible(unselectCitation + "[1]", 1000)
		.click(unselectCitation + "[1]")
		.pause(1000)
		.waitForElementVisible(unselectCitation + "[2]", 1000)
		.click(unselectCitation + "[2]")
		.pause(1000)
		.waitForElementVisible(unselectCitation + "[3]", 1000)
		.click(unselectCitation + "[3]")
		.pause(1000)
		.waitForElementVisible(itemsNotSelected, 1000)
		.assert.visible(itemsNotSelected)
		.pause(1000)
	},	

	/**
	 * @description Verify no target value
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test19EditCitationMapWidget: Verify No Target Value" : function (browser) {
		var noTargetValue = "//section[@id='targetSwitch' and @style='display: none;']";
		browser
		.useXpath()
		.assert.elementPresent(noTargetValue)
		.pause(1000)
	},	
	
	/**
	 * @description Apply changes
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test19EditCitationMapWidget: Apply Changes" : function (browser) {
		var applyBtn = "//button[@type='button']//span[text() = 'Apply changes']";
		browser
		.useXpath()
		.waitForElementVisible(applyBtn, 1000)
		.click(applyBtn)
		.pause(3000)
	},
	
	/**
	 * @description Save dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test19EditCitationMapWidget: Save Dashboard" : function (browser) {
		var saveBtn = "//section[@id='headerTools']//a[@title='Save']";
		browser
		.useXpath()
		.waitForElementVisible(saveBtn, 1000)
		.click(saveBtn)
		.pause(3000)
	},
	
	/**
	 * @description Verifies widget is edited successfully
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test19EditCitationMapWidget: Verify Widget Edit Success" : function (browser) {
		var widgetName = "(//section[@class='widget']//span[@class='wdgtName'])[text()='" + widgetNameEdited + "']";
		var optionMenu = widgetName + "//..//a[@title='Option Menu']";
		var noDataReturned = widgetName + "//..//..//section[@class='noWdgtData']//span[text() = 'There is currently no data returned for this widget.']";
		var zoomBtns = widgetName + "//..//..//div[@class='leaflet-control-zoom leaflet-bar leaflet-control']";
		var updateBar = widgetName + "//..//..//section[@id='statusDate']";
		var heatMapToggle = widgetName + "//..//..//section[@id='heatMapOverlay']";
		browser
		.useXpath()
		.assert.visible(widgetName)
		.assert.visible(optionMenu)
		.assert.visible(zoomBtns)
		.assert.visible(updateBar)
		.assert.visible(heatMapToggle)
		.assert.elementNotPresent(noDataReturned)
		.pause(1000)
	},
	
	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test19EditCitationMapWidget: Logout" : function (browser) {
		browser.logout();
	},

};
