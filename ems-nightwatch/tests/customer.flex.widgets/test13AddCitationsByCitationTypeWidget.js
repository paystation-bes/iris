/**
 * @summary Flex Widgets: Citations by Citation Type Widget: Add Citations by Citation Type widget and verify UI/data shows up
 * @since 7.3.6
 * @version 1.0
 * @author Mandy F
 * @see US598
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("FlexChildName");
var password = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test13AddCitationsByCitationTypeWidget: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
	},
	
	/**
	 * @description Go to dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test13AddCitationsByCitationTypeWidget: Go to Dashboard" : function (browser) {
		browser
		.navigateTo("Dashboard")
		.pause(1000)
	},
	
	/**
	 * @description Edit dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test13AddCitationsByCitationTypeWidget: Edit Dashboard" : function (browser) {
		var editBtn = "//section[@id='headerTools']//a[@title='Edit']";
		browser
		.useXpath()
		.waitForElementVisible(editBtn, 1000)
		.click(editBtn)
		.pause(1000)

	},
	
	/**
	 * @description Open add widget list
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test13AddCitationsByCitationTypeWidget: Open Add Widget List" : function (browser) {
		var addBtn = "//section[@class='sectionTools']//a[@title='Add Widget']";
		browser
		.useXpath()
		.waitForElementVisible(addBtn, 1000)
		.click(addBtn)
		.pause(1000)

	},
	
	/**
	 * @description Add Citations by Citation Type widget
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test13AddCitationsByCitationTypeWidget: Add Citations by Citation Type Widget" : function (browser) {
		var citationsList = "//section[@class='widgetListContainer']//h3[text() = 'Citations']";
		var citationsByCitationTypeWidget = "//section[@class='widgetListContainer']//li//header[text() = 'Citations by Citation Type']";
		var addBtn = "//button//span[text() = 'Add widget']";
		browser
		.useXpath()
		.waitForElementVisible(citationsList, 1000)
		.click(citationsList)
		.waitForElementVisible(citationsByCitationTypeWidget, 1000)
		.click(citationsByCitationTypeWidget)
		.pause(1000)
		.waitForElementVisible(addBtn, 1000)
		.click(addBtn)
		.pause(1000)
	},
	
	/**
	 * @description Save dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test13AddCitationsByCitationTypeWidget: Save Dashboard" : function (browser) {
		var saveBtn = "//section[@id='headerTools']//a[@title='Save']";
		browser
		.useXpath()
		.waitForElementVisible(saveBtn, 1000)
		.click(saveBtn)
		.pause(3000)
	},
	
	/**
	 * @description Verifies widget is added successfully
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test13AddCitationsByCitationTypeWidget: Verify Widget Add Success" : function (browser) {
		var widgetName = "(//section[@class='widget']//span[@class='wdgtName'])[text()='Citations by Citation Type']";
		var optionMenu = widgetName + "//..//a[@title='Option Menu']";
		var noDataReturned = widgetName + "//..//..//section[@class='noWdgtData']//span[text() = 'There is currently no data returned for this widget.']";
		var dataReturned = widgetName + "//..//..//section[@class='widgetContent chartGraph']//div[@class='highcharts-container']";
		browser
		.useXpath()
		.assert.visible(widgetName)
		.assert.visible(optionMenu)
		.assert.visible(dataReturned)
		.assert.elementNotPresent(noDataReturned)
		.pause(1000)
	},
	
	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test13AddCitationsByCitationTypeWidget: Logout" : function (browser) {
		browser.logout();
	},

};
