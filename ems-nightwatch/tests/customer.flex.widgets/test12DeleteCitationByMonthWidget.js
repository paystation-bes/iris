/**
 * @summary Flex Widgets: Citation by Month Widget: Delete Citation by Month widget
 * @since 7.3.6
 * @version 1.0
 * @author Mandy F
 * @see US598
 * @require test09AddCitationByMonthWidget.js, test11EditCitationByMonthWidget.js
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("FlexChildName");
var password = data("Password");

// edited name of the widget
var widgetNameEdited = "Citation by Month EDITED";

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12DeleteCitationByMonthWidget: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
	},
	
	/**
	 * @description Go to dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12DeleteCitationByMonthWidget: Go to Dashboard" : function (browser) {
		browser
		.navigateTo("Dashboard")
		.pause(1000)
	},
	
	/**
	 * @description Edit dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12DeleteCitationByMonthWidget: Edit Dashboard" : function (browser) {
		var editBtn = "//section[@id='headerTools']//a[@title='Edit']";
		var secondCol = "//section[@class='column second ui-sortable']";
		browser
		.useXpath()
		.waitForElementVisible(editBtn, 1000)
		.click(editBtn)
		.pause(1000)
		.waitForElementVisible(secondCol, 1000)
		.getElementSize(secondCol, function (result) {
			xOffset = Math.floor(result.value.width / 2);
		})
		.pause(1000);
	},	
	
	/**
	 * @description Open the Delete menu
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12DeleteCitationByMonthWidget: Open Delete Menu" : function (browser) {
		var widget = "(//section[@class='widget']//span[@class='wdgtName'])[text()='" + widgetNameEdited + "']//..//..";
		var optionMenu = widget + "//a[@title='Option Menu']";
		var deleteMenu = widget + "//..//a[contains(@class,'delete')]";
		browser
		.useXpath()
		.waitForElementVisible(optionMenu, 1000)
		.click(optionMenu)
		.pause(250)
		.waitForElementVisible(deleteMenu, 3000)
		.click(deleteMenu)
		.pause(1000);
	},
	
	/**
	 * @description Confirm delete
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12DeleteCitationByMonthWidget: Confirm Delete" : function (browser) {
		var confirmDelete = "//button[@type='button']//span[text() = 'Delete widget']";
		browser
		.useXpath()
		.waitForElementVisible(confirmDelete, 1000)
		.click(confirmDelete)
		.pause(1000);
	},
	
	/**
	 * @description Save dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12DeleteCitationByMonthWidget: Save Dashboard" : function (browser) {
		var saveBtn = "//section[@id='headerTools']//a[@title='Save']";
		browser
		.useXpath()
		.waitForElementVisible(saveBtn, 1000)
		.click(saveBtn)
		.pause(3000)
	},
	
	/**
	 * @description Verifies widget is deleted successfully
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12DeleteCitationByMonthWidget: Verify Widget Delete Success" : function (browser) {
		var widgetName = "(//section[@class='widget']//span[@class='wdgtName'])[text()='" + widgetNameEdited + "']";
		var optionMenu = widgetName + "//..//a[@title='Option Menu']";
		var noDataReturned = widgetName + "//..//..//section[@class='noWdgtData']//span[text() = 'There is currently no data returned for this widget.']";
		var dataReturned = widgetName + "//..//..//section[@class='widgetContent chartGraph']//div[@class='highcharts-container']";
		browser
		.useXpath()
		.assert.elementNotPresent(widgetName)
		.assert.elementNotPresent(optionMenu)
		.assert.elementNotPresent(dataReturned)
		.assert.elementNotPresent(noDataReturned)
		.pause(1000)
	},
	
	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12DeleteCitationByMonthWidget: Logout" : function (browser) {
		browser.logout();
	},

};
