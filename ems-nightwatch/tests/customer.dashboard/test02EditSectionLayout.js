var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
GLOBAL.sectionId;

module.exports = {
	tags: ['smoketag', 'completetesttag'],

	"test02EditSectionLayout: Login" : function (browser) {
		browser
			.url(devurl)
			.login(username, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"test02EditSectionLayout: Click Edit Dashboard Button" : function (browser) {
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='headerTools']/a[@title='Edit']", 1000)
			.click("//section[@id='headerTools']/a[@title='Edit']")
			.pause(2000);
	},
	
	/*"Click 3 Column Layout button" : function (browser) {
		browser
			.useXpath()
			.waitForElementVisible("//a[@name='layout4' and @class='linkButtonSM colOptn layout4 ']", 1000)
			.click("//a[@name='layout4' and @class='linkButtonSM colOptn layout4 ']")
			.pause (2000);

	},*/
	
	"test02EditSectionLayout: Elements Test" : function (browser)
	{
	    var domElementId;
	    browser.elements("xpath", "//section[contains(@id, 'section_')]", function(result) {
	                    domElementId = result.value[0].ELEMENT;
	                    console.info(domElementId);
	                    browser.elementIdAttribute(domElementId, "id", function(result) {
	                                    sectionId = result.value;
		console.info(result.value);
	                    })
	    });
	},

	"test02EditSectionLayout: Change Layout" : function (browser)
	{
	    browser
	    .waitForElementVisible("//section[@id='" + sectionId + "']/section/section/a[@class='linkButtonSM colOptn layout4 ' and @name='layout4']",5000)
		.click ("//section[@id='" + sectionId + "']/section/section/a[@class='linkButtonSM colOptn layout4 ' and @name='layout4']")
		.pause (1000)
	},

	"test02EditSectionLayout: Save": function (browser)
		{
			browser
				.useXpath()
				.waitForElementVisible("//a[@title='Save']", 1000)
				.click("//a[@title='Save']")
				.pause(1000)
				.refresh()
				.pause(3000)
				.assert.attributeEquals("//section[@class= 'empty']", "class","empty", "New Section Present");
		},


"test02EditSectionLayout: Click Edit Dashboard Button 2" : function (browser) {
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='headerTools']/a[@title='Edit']", 1000)
			.click("//section[@id='headerTools']/a[@title='Edit']")
			.pause(2000);
	},


	"test02EditSectionLayout: Elements Test 2" : function (browser)
	{
	    var domElementId;
	    browser.elements("xpath", "//section[contains(@id, 'section_')]", function(result) {
	                    domElementId = result.value[0].ELEMENT;
	                    console.info(domElementId);
	                    browser.elementIdAttribute(domElementId, "id", function(result) {
	                                    sectionId = result.value;
		console.info(result.value);
	                    })
	    });
	},

	"test02EditSectionLayout: Change Layout 2" : function (browser)
	{
	    browser
	    .waitForElementVisible("//section[@id='" + sectionId + "']/section/section/a[@class='linkButtonSL colOptn layout1 ' and @name='layout1']",1000)
		.click ("//section[@id='" + sectionId + "']/section/section/a[@class='linkButtonSL colOptn layout1 ' and @name='layout1']")
		.pause (1000)
	},

	"test02EditSectionLayout: Save 2": function (browser)
		{
			browser
				.useXpath()
				.waitForElementVisible("//a[@title='Save']", 1000)
				.click("//a[@title='Save']")
				.pause(1000)
				.refresh()
				.pause(3000)
				.assert.attributeEquals("//section[@class= 'empty']", "class","empty", "New Section Present");
		},

	"test02EditSectionLayout: Logout" : function (browser) {
		browser.logout();
	},
		
};






