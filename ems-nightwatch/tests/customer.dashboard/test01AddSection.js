var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
var sectionCountBefore;
var sectionCountAfter;
module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) {
		browser
			.url(devurl)
			.login(username, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},

	"Save sectionCountBefore": function (browser) {
		
		browser.elements("xpath", "//section[contains(@id, 'section_')]", function(result){
			sectionCountBefore = result.value.length;
			console.log("Number of Sections Before: " + result.value.length);

		});
	},

	"test01AddSection: Click Edit Dashboard Button" : function (browser) {
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='headerTools']/a[@title='Edit']", 1000)
			.click("//section[@id='headerTools']/a[@title='Edit']")
			.pause(2000);
	},
	

	
	"test01AddSection: Click Add Section Button" : function (browser) {
		browser
			.useXpath()
			.waitForElementVisible ("//a[text()='ADD SECTION']", 1000)
			.click ("//a[text()='ADD SECTION']")
			.pause(2000);
	},
	
	"test01AddSection: Save": function (browser) {
		browser
			.useXpath()
			.waitForElementVisible("//a[@title='Save']", 1000)
			.click("//a[@title='Save']")
			.pause(1000)
	},

	"Assert New Section is added" : function(browser){
		browser.elements("xpath", "//section[contains(@id, 'section_')]", function(result){
			sectionCountAfter = result.value.length;
			console.log("Number of Sections Before: " + sectionCountBefore);
			browser.assert.equal(sectionCountBefore + 1, sectionCountAfter)

		});
		
	},
	
	"test01AddSection: Logout" : function (browser) {
		browser.logout();
	},
		
};






