var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";
GLOBAL.locationId = "";

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"test04Edit2TierWidget: Login" : function (browser) 
	{
		browser
			.url(devurl)
			.login(username, password)
			.windowMaximize()
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"test04Edit2TierWidget: Click Edit Dashboard Button" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='headerTools']/a[@title='Edit']", 3000)
			.click("//section[@id='headerTools']/a[@title='Edit']")
			.pause(2000);
	},
	
	"test04Edit2TierWidget: Click Options Menu for Widget" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//span[@class='wdgtName' and text ()='Total Revenue by Day']", 3000)
			.getAttribute("//span[@class='wdgtName' and text ()='Total Revenue by Day']/../../..", "id", function(result) {
				console.log(result.value);
				widgetId = result.value;
				widgetId = widgetId.replace("widgetID_", "");
				console.log(widgetId);
				browser	
					.waitForElementVisible("//a[@id='opBtn_" + widgetId + "']", 3000)
					.click("//a[@id='opBtn_" + widgetId + "']") 
					.pause(2000);
			})
	},
			
	"test04Edit2TierWidget: Click Settings for Widget" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='widgetID_" + widgetId + "']/section[@class='ddMenu']/section[@class='editMenuOptions innerBorder']/a[@title='Settings']", 3000)
				.click("//section[@id='widgetID_" + widgetId + "']/section[@class='ddMenu']/section[@class='editMenuOptions innerBorder']/a[@title='Settings']")
				.pause(3000)
		},
		
	"test04Edit2TierWidget: Change 'Name' Input Box Value" : function (browser) 
	{	
		browser
			.useXpath()
			.waitForElementVisible("//span[@class='ui-dialog-title' and text()='Widget settings']", 3000)
				.click("//input[@id='widgetName']")
				.pause(3000)
				.clearValue("//input[@id='widgetName']") 
				.setValue("//input[@id='widgetName']", "Automation")
				
	}, 
	
	"test04Edit2TierWidget: Change 'Revenue by Day for' Value" : function (browser) 
	{
		browser
			.useXpath()
			.click("//input[@id='rangeMenu']")
			.pause(3000)
			.useXpath()
			.click("//a[@class='ui-corner-all' and text()='Last Week']")
			
	},
	
	"test04Edit2TierWidget: 'add refinement' Check Box" : function (browser) 
	{
		browser 
			.useXpath()
			//.click("//a[@id='tier2Check']")
			.waitForElementVisible("//label[@class='left' and text()='by']", 3000)
			
			
	},
			
			
	"test04Edit2TierWidget: Expand Refinement Drop Down Menu" : function (browser) 
	{
		browser
			.useXpath()
			.click("//a[@id='tier2MenuExpand']")
			.waitForElementVisible("/html/body/ul[3]/li[1]/a", 3000)
			
	},
			
	
	"test04Edit2TierWidget: Select 'Location' as Refinement" : function (browser)
	{
		browser
			.useXpath()
			.click("//a[@class='ui-corner-all' and text()='Location']")
			.pause(3000)
	},
	
	"test04Edit2TierWidget: Click 'Select All' for Location Options" : function (browser)
	{
		browser
			.useXpath()
			.click("//section[@id='tier2Area']//section[@class='selectedListHeader']/a")
			.click("//a[@id='tier2OpsAll' and @class='checkBox']")
			.pause(3000)
	},
	
	// "test04Edit2TierWidget: 'show target value' checkbox" : function (browser)
	// {
	// 	browser	
	// 		.useXpath()
	// 		.click("/html/body/div[3]/section/form/section[7]/label/a")
	// },
	
	"test04Edit2TierWidget: Input 'Target' Input Box Value" : function (browser)
	{
		browser	
			.useXpath()
				// .assert.visible("//a[@id='targetCheck']")
				// .pause(3000)
				// .click("//a[@id='targetCheck']")
				// .pause(3000)
				.assert.visible("//input[@name='targetValueDisplay']")
				.pause(3000)
				.clearValue("//input[@name='targetValueDisplay']")
				.pause(3000)
				.setValue("//input[@name='targetValueDisplay']", "500")
				.pause(3000)
	},
	
	"test04Edit2TierWidget: Select 'Column' Display" : function (browser)
	{
		browser	
			.useXpath()
			.click("//ul[@id='chartDesign']/li[3]//img")
			.pause(3000)
	},
	
	
	"test04Edit2TierWidget: Click 'Apply Changes'" : function (browser)
	{
		browser
			.useXpath()
			.click("//span[contains(text(),'Apply changes')]")
			.pause(2000)
	},
			
	"test04Edit2TierWidget: Click Options Menu for Widget 2" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//span[@class='wdgtName' and text ()='Automation']", 3000)
			.getAttribute("//span[@class='wdgtName' and text ()='Automation']/../../..", "id", function(result) {
				console.log(result.value);
				widgetId = result.value;
				widgetId = widgetId.replace("widgetID_", "");
				console.log(widgetId);
				browser	
					.waitForElementVisible("//a[@id='opBtn_" + widgetId + "']", 3000)
					.click("//a[@id='opBtn_" + widgetId + "']") 
					.pause(2000);
			})
	},
			
	"test04Edit2TierWidget: Click Settings for Widget 2" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='widgetID_" + widgetId + "']/section[@class='ddMenu']/section[@class='editMenuOptions innerBorder']/a[@title='Settings']", 3000)
				.click("//section[@id='widgetID_" + widgetId + "']/section[@class='ddMenu']/section[@class='editMenuOptions innerBorder']/a[@title='Settings']")
				.pause(3000)
		},

	"test04Edit2TierWidget: Assert changes" : function (browser) 
	{
		browser
			.useCss()
			.assert.elementPresent("#tier2OpsAll")
			.assert.value("#targetValueDisplay", "500")

},

	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test04Edit2TierWidget: Logout" : function (browser) {
		browser.logout();
	},

	
};
				
			
			
			