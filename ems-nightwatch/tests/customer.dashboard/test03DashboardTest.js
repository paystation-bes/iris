var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";
GLOBAL.widget = "Total Settled Yesterday";

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"test03DashboardTest: Login" : function (browser) {
		browser
			.url(devurl)
			.login(username, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"test03DashboardTest: Elements Test" : function (browser) {
		var domElementId;
		browser.elements("xpath", "//section[contains(@id, 'section_')]", function(result) {
			domElementId = result.value[0].ELEMENT;
			console.info(result.value[0].ELEMENT);
			browser.elementIdAttribute(domElementId, "id", function(result) {
				console.info(result.value);
			})
		});
	},
	
	"test03DashboardTest: Click Edit Dashboard Button" : function (browser) {
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='headerTools']/a[@title='Edit']", 1000, false)
			.click("//section[@id='headerTools']/a[@title='Edit']")
			.pause(2000)
			.assert.visible("//a[@title='Reset Dashboard']")
			.assert.visible("//a[@title='Save']")
			.assert.visible("//a[@title='Cancel']")
			.assert.visible("//a[@title='Cancel']")
			.assert.visible("//section[@class='layoutOptions']/h5[text()='Add Widget:']")
			.assert.visible("//a[@title='Add Widget']")
			.assert.visible("//section[@class='layoutOptions']/h5[text()='Layout:']")
			.assert.visible("//a[@name='layout1' and @title='1 Column Layout']")
			.assert.visible("//a[@name='layout2' and @title='2 Column Layout']")
			.assert.visible("//a[@name='layout3' and @title='3 Column Layout']")
			.assert.visible("//a[@name='layout4' and @title='2 Column Layout']")
			.assert.visible("//a[@name='layout5' and @title='2 Column Layout']")
			.assert.visible("//a[@name='layout5' and @title='2 Column Layout']")
			.assert.visible("//section[@class='sectionOptions']/h5[text()='Move:']")
			.assert.visible("//a[@title='Move Section Up']")
			.assert.visible("//a[@title='Move Section Down']")
			.assert.visible("//section[@class='sectionOptions']/h5[text()='Delete:']")
			.assert.visible("//a[@title='Delete Section']")
			.assert.visible("//a[text()='ADD SECTION']");
	},
	
	"test03DashboardTest: Add New Dashboard Tab" : function (browser) {
		browser
			.useXpath()
			.waitForElementVisible("//a[@title='New Tab']", 1000, false)
			.click("//a[@title='New Tab']")
			.pause(2000)
			.assert.visible("//section[@class='tabSection current new']")
			.waitForElementVisible("//section[@class='tabSection current new']/input[@class='tabInput']", 1000, false)
			.keys("Nightwatch Test")
			.pause(500)
			.keys(browser.Keys.ENTER)
			.pause(3000)
			.assert.containsText("//section[@class='tabSection current new']/a[@class='tab']", "Nightwatch Test");
	},
	
	"test03DashboardTest: Add New Widget" : function (browser) {
		//var widget = "Total Settled Yesterday";
		browser
			.useXpath()
			// Clicks the Add Widget button
			.waitForElementVisible("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']", 1000, false)
			.click("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']")
			.pause(3000)
			.assert.visible("//div[@aria-describedby='widgetList' and @role='dialog']")
			.assert.visible("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']/span[text()='Add widget']")
			.assert.visible("//button/span[text()='Cancel']")
			
			// Gets the currently selected widget list.
			.waitForElementPresent("//section[@class='widgetListContainer']/ul[@style='display: block;']", 1000, false)
			.getAttribute("//section[@class='widgetListContainer']/ul[@style='display: block;']", "id", function(result) {
				var selectedWidgetList = "";
				var desiredWidgetList = "";
				console.log("Selected Widget List: " + result.value);
				selectedWidgetList = result.value;
				browser
					// Gets the widget list where the desired widget can be found.
					.waitForElementPresent("//ul[@class='widgetSelection']/li/header[text()='" + widget + "']", 1000, false)
					.getAttribute("//ul[@class='widgetSelection']/li/header[text()='" + widget + "']/../..", "id", function(result) {
						console.log("Desired Widget List: " + result.value);
						desiredWidgetList = result.value;
						CompareWidgetIds(selectedWidgetList, desiredWidgetList);
					})
			})
		
		// Compares the two widget list ids and clicks the widget list if it is not already selected
		function CompareWidgetIds(selectedWidgetList, desiredWidgetList) {
			if (selectedWidgetList != desiredWidgetList) {
				var widgetListNumber = "";
				// Remove text from widget list ID so you are left with just the number
				widgetListNumber = desiredWidgetList.replace("metricList", "");
				console.log("Widget List Number: " + widgetListNumber);
				ClickWidgetList(widgetListNumber);
			}
			SelectAndAddWidget();
		}
		
		// Clicks the specified widget list
		function ClickWidgetList(widgetListNumber) {
			browser
				.waitForElementPresent("//section[@class='widgetListContainer']/h3[@id='metric" + widgetListNumber + "']", 1000, false)
				.click("//section[@class='widgetListContainer']/h3[@id='metric" + widgetListNumber + "']")
				.pause(1000)
				.assert.visible("//ul[@class='widgetSelection']/li/header[text()='" + widget + "']");
		}
		
		// Selects and Adds the widget
		function SelectAndAddWidget() {
			browser
				.waitForElementPresent("//header[text()='" + widget + "']", 1000, false)
				.click("//header[text()='" + widget + "']")
				.pause(1000)
				.assert.visible("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only active']/span[text()='Add widget']")
				.waitForElementVisible("//button/span[text()='Add widget']", 1000, false)
				.click("//button/span[text()='Add widget']")
				.pause(1000)
				.assert.visible("//section[@class='widget']/header/span[@class='wdgtName' and text()='" + widget + "']");
		}
	},
	
	"test03DashboardTest: Save Dashboard After Add" : function (browser) {
		browser
			.useXpath()
			.waitForElementVisible("//a[@title='Save']", 1000, false)
			.click("//a[@title='Save']")
			.pause(3000)
			.refresh()
			.pause(3000)
			//.assert.visible("//section[@class='tabSection']/a[@class='tab' and text()='Nightwatch Test']");
			.assert.visible("//a[contains(text(),'Nightwatch Test')]")
	},
	
	"test03DashboardTest: Go to Added Tab to Verify Data" : function (browser) {
		browser
			.useXpath()
			//.waitForElementVisible("//section[@class='tabSection']/a[@class='tab' and text()='Nightwatch Test']", 1000, false)
			.waitForElementVisible("//a[contains(text(),'Nightwatch Test')]", 2000)
			//.click("//section[@class='tabSection']/a[@class='tab' and text()='Nightwatch Test']")
			.click("//a[contains(text(),'Nightwatch Test')]")
			.pause(1000)
			//.assert.containsText("//section[@class='widget']/header/span[@class='wdgtName']", "Total Settled Yesterday")
			.getAttribute("//section[@class='widget']/header/span[@class='wdgtName' and text()='Total Settled Yesterday']/../../..", "id", function (result) {
				widgetId = result.value;
				console.log(widgetId);
				widgetId = widgetId.replace("widgetID_", "");
				console.log("Replacing...");
				console.log(widgetId);
				browser
					// .assert.visible("//section[@class='widget']/header/a[@class='hdrButtonIcn menu' and @id='opBtn_" + widget + "']")
					// .click("//section[@class='widget']/header/a[@class='hdrButtonIcn menu' and @id='opBtn_" + widget + "']");
					.assert.visible("//section[@class='widget']/header/a[@class='hdrButtonIcn menu' and @id='opBtn_" + widgetId + "']")
					.click("//section[@class='widget']/header/a[@class='hdrButtonIcn menu' and @id='opBtn_" + widgetId + "']");
			})
	},
	
	"test03DashboardTest: Click Edit Dashboard to Move Widget" : function (browser) {
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='headerTools']/a[@title='Edit']", 1000, false)
			.click("//section[@id='headerTools']/a[@title='Edit']")
			.pause(2000)
			.assert.visible("//a[@title='Reset Dashboard']")
			.assert.visible("//a[@title='Save']")
			.assert.visible("//a[@title='Cancel']")
			.assert.visible("//a[@title='Cancel']")
			.assert.visible("//section[@class='layoutOptions']/h5[text()='Add Widget:']")
			.assert.visible("//a[@title='Add Widget']")
			.assert.visible("//section[@class='layoutOptions']/h5[text()='Layout:']")
			.assert.visible("//a[@name='layout1' and @title='1 Column Layout']")
			.assert.visible("//a[@name='layout2' and @title='2 Column Layout']")
			.assert.visible("//a[@name='layout3' and @title='3 Column Layout']")
			.assert.visible("//a[@name='layout4' and @title='2 Column Layout']")
			.assert.visible("//a[@name='layout5' and @title='2 Column Layout']")
			.assert.visible("//a[@name='layout5' and @title='2 Column Layout']")
			.assert.visible("//section[@class='sectionOptions']/h5[text()='Move:']")
			.assert.visible("//a[@title='Move Section Up']")
			.assert.visible("//a[@title='Move Section Down']")
			.assert.visible("//section[@class='sectionOptions']/h5[text()='Delete:']")
			.assert.visible("//a[@title='Delete Section']")
			.assert.visible("//a[text()='ADD SECTION']");
	},
	
	"test03DashboardTest: Rename Tab" : function (browser) {
		browser
			.useXpath()
			.waitForElementVisible("//a[text()='Nightwatch Test']", 1000, false)
			.click("//a[text()='Nightwatch Test']")
			.pause(2000)
			.click("//a[text()='Nightwatch Test']")
			.pause(1000)
			.clearValue("//input[@value='Nightwatch Test']")
			.pause(1000)
			.setValue("//input[@value='Nightwatch Test']", "Trevor")
			.pause(2000);
	},
	
	"test03DashboardTest: Move Widget" : function (browser) {
		browser
			.useXpath()
			.waitForElementVisible("//span[@class='wdgtName' and text()='Total Settled Yesterday']", 1000, false)
			.getAttribute("//span[@class='wdgtName' and text()='Total Settled Yesterday']/../../..", "id", function(result) {
				console.log(result.value);
				widgetId = result.value;
				widgetId = widgetId.replace("widgetID_", "");
				console.log(widgetId);
				browser
					.waitForElementVisible("//section[@id='widgetID_" + widgetId + "\']/section[@class='widget']/header/span[@class='move']", 1000, false)
					.moveToElement("//section[@id='widgetID_" + widgetId + "\']/section[@class='widget']/header/span[@class='move']", 0, 0)
					.pause(1000)
					.mouseButtonDown(0)
					.pause(1000)
					.waitForElementVisible("//section[@id='" + result.value + "']/..", 1000, false)
					.getAttribute("//section[@id='" + result.value + "']/..", "id", function(result) {
						sectionId = result.value;
						sectionId = sectionId.replace("col0", "col1")
						browser
							.waitForElementVisible("//section[@class='column second ui-sortable' and @id='" + sectionId + "']", 1000, false)
							.moveToElement("//section[@class='column second ui-sortable' and @id='" + sectionId + "']", 0, 0)
							.pause(1000)
							.mouseButtonUp(0)
							.pause(1000);
					});
			});
	},
	
	"test03DashboardTest: Save Dashboard After Move" : function (browser) {
		browser
			.useXpath()
			.waitForElementVisible("//a[@title='Save']", 1000, false)
			.click("//a[@title='Save']")
			.pause(1000)
			.refresh()
			.pause(3000);
	},
	
	"test03DashboardTest: Click Edit Dashboard to Delete Widget" : function (browser) {
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='headerTools']/a[@title='Edit']", 1000, false)
			.click("//section[@id='headerTools']/a[@title='Edit']")
			.pause(2000)
			.assert.visible("//a[@title='Reset Dashboard']")
			.assert.visible("//a[@title='Save']")
			.assert.visible("//a[@title='Cancel']")
			.assert.visible("//a[@title='Cancel']")
			.assert.visible("//section[@class='layoutOptions']/h5[text()='Add Widget:']")
			.assert.visible("//a[@title='Add Widget']")
			.assert.visible("//section[@class='layoutOptions']/h5[text()='Layout:']")
			.assert.visible("//a[@name='layout1' and @title='1 Column Layout']")
			.assert.visible("//a[@name='layout2' and @title='2 Column Layout']")
			.assert.visible("//a[@name='layout3' and @title='3 Column Layout']")
			.assert.visible("//a[@name='layout4' and @title='2 Column Layout']")
			.assert.visible("//a[@name='layout5' and @title='2 Column Layout']")
			.assert.visible("//a[@name='layout5' and @title='2 Column Layout']")
			.assert.visible("//section[@class='sectionOptions']/h5[text()='Move:']")
			.assert.visible("//a[@title='Move Section Up']")
			.assert.visible("//a[@title='Move Section Down']")
			.assert.visible("//section[@class='sectionOptions']/h5[text()='Delete:']")
			.assert.visible("//a[@title='Delete Section']")
			.assert.visible("//a[text()='ADD SECTION']");
	},
	
	"test03DashboardTest: Delete Widget" : function (browser) {
		browser
			.useXpath()
			.waitForElementVisible("//a[text()='Trevor']", 1000, false)
			.click("//a[text()='Trevor']")
			.pause(1000)
			.waitForElementVisible("//span[@class='wdgtName' and text()='Total Settled Yesterday']", 2000, false)
			.getAttribute("//span[@class='wdgtName' and text()='Total Settled Yesterday']/../../..", "id", function(result) {
				console.log(result.value);
				widgetId = result.value;
				widgetId = widgetId.replace("widgetID_", "");
				console.log(widgetId);
				browser
					.waitForElementVisible("//a[@id='opBtn_" + widgetId + "']", 1000, false)
					.click("//a[@id='opBtn_" + widgetId + "']")
					.waitForElementVisible("//section[@id='widgetID_" + widgetId + "']/section[@class='ddMenu']/section[@class='editMenuOptions innerBorder']/a[@title='Delete']", 3000, false)
					.click("//section[@id='widgetID_" + widgetId + "']/section[@class='ddMenu']/section[@class='editMenuOptions innerBorder']/a[@title='Delete']")
					.pause(1000)
					.waitForElementVisible("//button/span[@class='ui-button-text' and text()='Delete widget']", 1000, false)
					.click("//button/span[@class='ui-button-text' and text()='Delete widget']")
					.pause(1000);
			});
	},
	
	"test03DashboardTest: Delete Tab" : function (browser) {
		browser
			.useXpath()
			//.waitForElementVisible("//section[@class='tabSection  current']/a[@class='tabDelete']", 1000, false)
			.waitForElementVisible("//input[@value='Trevor']/following-sibling::a[1]", 1000, false)
			//.click("//section[@class='tabSection  current']/a[@class='tabDelete']")
			.click("//input[@value='Trevor']/following-sibling::a[1]")
			.pause(1000)
			.waitForElementVisible("//button/span[@class='ui-button-text' and text()='Delete tab']", 1000, false)
			.click("//button/span[@class='ui-button-text' and text()='Delete tab']")
			.pause(1000);
	},
	
	"test03DashboardTest: Save Dashboard After Widget Deleted" : function (browser) {
		browser
			.useXpath()
			.waitForElementVisible("//a[@title='Save']", 1000, false)
			.click("//a[@title='Save']")
			.pause(1000)
			.refresh()
			.pause(3000);
	},
	
	"test03DashboardTest: Logout" : function (browser) {
		browser.logout();
	}
};
