var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";
GLOBAL.locationId = "";

module.exports = 
{

	tags: ['smoketag', 'completetesttag'],

	"test05Edit3TierWidget: Login" : function (browser) 
	{
		browser
			.url(devurl)
			.login(username, password)
			.windowMaximize()
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"test05Edit3TierWidget: Click Edit Dashboard Button" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='headerTools']/a[@title='Edit']", 2000)
			.click("//section[@id='headerTools']/a[@title='Edit']")
			.pause(2000);
	},
	
	"test05Edit3TierWidget: Click Options Menu for Widget" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//span[@class='wdgtName' and text ()='Total Revenue by Day']", 2000)
			.getAttribute("//span[@class='wdgtName' and text ()='Total Revenue by Day']/../../..", "id", function(result) {
				console.log(result.value);
				widgetId = result.value;
				widgetId = widgetId.replace("widgetID_", "");
				console.log(widgetId);
				browser	
					.waitForElementVisible("//a[@id='opBtn_" + widgetId + "']", 2000)
					.click("//a[@id='opBtn_" + widgetId + "']") 
					.pause(2000);
			})
	},
			
	"test05Edit3TierWidget: Click Settings for Widget" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='widgetID_" + widgetId + "']/section[@class='ddMenu']/section[@class='editMenuOptions innerBorder']/a[@title='Settings']", 3000)
				.click("//section[@id='widgetID_" + widgetId + "']/section[@class='ddMenu']/section[@class='editMenuOptions innerBorder']/a[@title='Settings']")
				.pause(1000)
		},
		
	"test05Edit3TierWidget: Change 'Name' Input Box Value" : function (browser) 
	{	
		browser
			.useXpath()
			.waitForElementVisible("//span[@class='ui-dialog-title' and text()='Widget settings']", 1000)
				.click("//input[@id='widgetName']")
				.pause(1000)
				.clearValue("//input[@id='widgetName']") 
				.setValue("//input[@id='widgetName']", "Automation")
				
	}, 
	
	"test05Edit3TierWidget: Change 'Revenue by Day for' Value" : function (browser) 
	{
		browser
			.useXpath()
			.click("//input[@id='rangeMenu']")
			.pause(1000)
			.useXpath()
			.click("//a[@class='ui-corner-all' and text()='Last Week']")
			.assert.attributeEquals("//a[@class='ui-corner-all' and text()='Last Week']", "class","ui-corner-all");
			
	},
	
	"test05Edit3TierWidget: 'add refinement' Check Box" : function (browser) 
	{
		browser 
			.useXpath()
			//.click("//a[@id='tier2Check']")
			.waitForElementVisible("//label[@class='left' and text()='by']", 3000)
	},
			
	"test05Edit3TierWidget: Expand Refinement Drop Down Menu" : function (browser) 
	{
		browser
			.useXpath()
			.click("//a[@id='tier2MenuExpand']")
			.waitForElementVisible("/html/body/ul[3]/li[1]/a", 3000)
	},
	
	"test05Edit3TierWidget: Select 'Location' as Refinement" : function (browser)
	{
		browser
			.useXpath()
			.click("//a[@class='ui-corner-all' and text()='Location']")
			.pause(3000)			
	},
	/*
	"Wait for Location selection options to appear" : function (browser)
	{
		browser
			.useXpath()
			
			//Different attempts to wait for the Location Options list to be visible:
			
			/* 1) Wait for 'Selected Options' to appear
			.waitForElementVisible("//section[@class='left' and text()='Selected options:']", 2000) */
			
			/* 2) Wait for the 'Clear' option to be visible
			.waitForElementVisible("//a[@id='clearSelected' and text()='Clear']", 1000) 
			
	}, */
	
	"test05Edit3TierWidget: Select Location - NO ASSERTIONS" : function (browser)
	{
		browser
			.useXpath()
			//Click Clear
			.click("//section[@id='tier2Area']//section[@class='selectedListHeader']/a")
			// Select Airport
			.click("//ul[@class='fullList']/li[contains(text(), 'Airport')]/a")
			.assert.elementPresent("//ul[@class='selectedList']/li[contains(text(),'Airport') and @class='selected']")
			.pause(3000)
	},
	
	
			
	/*
			//Get the value of the 'Airport' location
			.getAttribute("//a[@class='checkBox' and text()=' Airport']", "id", function(result) 
				{
				console.log(result.value);
				locationId = result.value;
				locationId = locationId.replace("locationID_", "");
				console.log(locationId);
				browser
					//Select the 'Airport' location
					.waitForElementVisible("//a[@class='checkBox' and text()=' Airport']")
					.click("//a[@class='checkBox' and text()=' Airport']")
					.pause(1000)
			
				});
	},
	*/
	
	"test05Edit3TierWidget: 'add refinement' 2 Check Box" : function (browser)
	{
		browser 
			.useCss()
			.assert.visible("#tier3Check")
			.click("#tier3Check")
			.pause(3000)
			.assert.visible("#tier3Menu")

			//.useXpath()
			//.click("//a[@id='tier3Check]")
			//.waitForElementVisible("//input[@id='tier3Menu']", 1000)
			
	},
			
	"test05Edit3TierWidget: Expand Refinement 2 Drop Down Menu and Select Revenue Type as " : function (browser)
	{
		browser
			.useXpath()
			.click("//a[@id='tier3MenuExpand']")
			.pause(5000)
			.assert.visible("//input[@id='tier3Menu']")
	},
	
	
	"test05Edit3TierWidget: Select 'Revenue Type' as Refinement" : function (browser)
	{
		browser	
			.clearValue("//input[@id='tier3Menu']")
			.setValue("//input[@id='tier3Menu']", 'Revenue Type')
			.click("//section[@id='tier3Area']")
			.waitForElementVisible("//li[contains(text(),'Cash')]", 3000)

	},
			
	"test05Edit3TierWidget: Select 'Cash' as Revenue Type" : function (browser)
	{
		browser
			.useXpath()
			.click("//ul[@class='fullList']/li[text()[normalize-space()='Cash']]/a")
			.pause(2000)
			.assert.visible("//ul[@class='selectedList']/li[text()[normalize-space()='Cash']]")
			//.click("/html/body/div[3]/section/form/section[6]/section[2]/ul[1]/li[5]")
			//.waitForElementVisible("/html/body/div[3]/section/form/section[6]/section[2]/ul[2]/li[5]", 1500)
	},
	
	
	"test05Edit3TierWidget: 'show target value' checkbox and input value" : function (browser)
	{
		browser	
			.useXpath()
			// .assert.visible("//a[@id='targetCheck']")
			// .click("//a[@id='targetCheck']")
			.pause(2000)
			.assert.visible("//input[@id='targetValueDisplay']")
			.setValue("//input[@id='targetValueDisplay']", "500")
			//.click("/html/body/div[3]/section/form/section[7]/label/a")
			//.waitForElementVisible("/html/body/div[3]/section/form/section[8]/label", 1000)
	},
	
	
	/*This is a duplicate for reference
	"Change 'Revenue by Day for' Value" : function (browser) 
	{
		browser
			.useXpath()
			.click("//input[@id='rangeMenu']")
			.pause(1000)
			.useXpath()
			.click("//a[@class='ui-corner-all' and text()='Last Week']")
			.assert.attributeEquals("//a[@class='ui-corner-all' and text()='Last Week']", "class","ui-corner-all");
			
	},*/
	
	"test05Edit3TierWidget: Select 'Column' Display" : function (browser)
	{
		browser	
			.useXpath()
			.assert.elementPresent("//section[contains(text(),'Column')]")
			.click("//section[contains(text(),'Column')]")
			//.click("/html/body/div[3]/section/form/ul/li[3]/section")
			/*.assert.containsText("/html/body/div[3]/section/form/section[8]/input[2]", "500")*/
			.pause(1000)
	},
	
	
	"test05Edit3TierWidget: Click 'Apply Changes'" : function (browser)
	{
		browser
			.useXpath()
			.assert.elementPresent("//span[contains(text(),'Apply changes')]/..")
			.click("//span[contains(text(),'Apply changes')]/..")
			//.click("/html/body/div[3]/div[2]/div/button[1]")
			.pause(3000)
	},

	// "test05Edit3TierWidget: Save Dashboard" : function (browser)
	// {
	// 	browser
	// 		.assert.visible("//section[@id='headerTools']/a[contains(text(),'Save')]")
	// 		.click("//section[@id='headerTools']/a[contains(text(),'Save')]")
	// 		.pause(5000)
	// 		.assert.hidden("//section[@id='headerTools']/a[contains(text(),'Save')]")
	// },

	"test05Edit3TierWidget: Verify Widget Name" : function(browser)
	{
		browser
			.assert.elementPresent("//span[@class='wdgtName' and contains(text(), 'Automation')]")
	},
			

	"test05Edit3TierWidget: Logout" : function (browser)
	{
		browser.logout()
	},



};