var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
GLOBAL.section1 = "";
GLOBAL.widget = "Total Settled Yesterday";
var sectionCountBefore;
var sectionCountAfter;

module.exports = {
	tags: ['smoketag', 'completetesttag', 'skip'],
	"test06DeleteSection: Login" : function (browser) {
		browser
			.url(devurl)
			.login(username, password)
			.windowMaximize()
			.assert.title('Digital Iris - Main (Dashboard)');
	},

	"Save sectionCountBefore": function (browser) {
		
		browser.elements("xpath", "//section[contains(@id, 'section_')]", function(result){
			sectionCountBefore = result.value.length;
			console.log("Number of Sections Before: " + result.value.length);

		});
	},	
	
	"test06DeleteSection: Click Edit Dashboard Button" : function (browser) {
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='headerTools']/a[@title='Edit']", 1000)
			.click("//section[@id='headerTools']/a[@title='Edit']")
			.pause(2000);
	},
	
	"test03DashboardTest: Add New Widget" : function (browser) {
		//var widget = "Total Settled Yesterday";
		browser
			.useXpath()
			// Clicks the Add Widget button
			.waitForElementVisible("(//section[@style='display: block;']/section/section/section/a[@title='Add Widget'])[last()]", 1000, false)
			.click("(//section[@style='display: block;']/section/section/section/a[@title='Add Widget'])[last()]")
			.pause(3000)
			.assert.visible("//div[@aria-describedby='widgetList' and @role='dialog']")
			.assert.visible("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']/span[text()='Add widget']")
			.assert.visible("//button/span[text()='Cancel']")
			
			// Gets the currently selected widget list.
			.waitForElementPresent("//section[@class='widgetListContainer']/ul[@style='display: block;']", 1000, false)
			.getAttribute("//section[@class='widgetListContainer']/ul[@style='display: block;']", "id", function(result) {
				var selectedWidgetList = "";
				var desiredWidgetList = "";
				console.log("Selected Widget List: " + result.value);
				selectedWidgetList = result.value;
				browser
					// Gets the widget list where the desired widget can be found.
					.waitForElementPresent("//ul[@class='widgetSelection']/li/header[text()='" + widget + "']", 1000, false)
					.getAttribute("//ul[@class='widgetSelection']/li/header[text()='" + widget + "']/../..", "id", function(result) {
						console.log("Desired Widget List: " + result.value);
						desiredWidgetList = result.value;
						CompareWidgetIds(selectedWidgetList, desiredWidgetList);
					})
			})
		
		// Compares the two widget list ids and clicks the widget list if it is not already selected
		function CompareWidgetIds(selectedWidgetList, desiredWidgetList) {
			if (selectedWidgetList != desiredWidgetList) {
				var widgetListNumber = "";
				// Remove text from widget list ID so you are left with just the number
				widgetListNumber = desiredWidgetList.replace("metricList", "");
				console.log("Widget List Number: " + widgetListNumber);
				ClickWidgetList(widgetListNumber);
			}
			SelectAndAddWidget();
		}
		
		// Clicks the specified widget list
		function ClickWidgetList(widgetListNumber) {
			browser
				.waitForElementPresent("//section[@class='widgetListContainer']/h3[@id='metric" + widgetListNumber + "']", 1000, false)
				.click("//section[@class='widgetListContainer']/h3[@id='metric" + widgetListNumber + "']")
				.pause(1000)
				.assert.visible("//ul[@class='widgetSelection']/li/header[text()='" + widget + "']");
		}
		
		// Selects and Adds the widget
		function SelectAndAddWidget() {
			browser
				.waitForElementPresent("//header[text()='" + widget + "']", 1000, false)
				.click("//header[text()='" + widget + "']")
				.pause(1000)
				.assert.visible("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only active']/span[text()='Add widget']")
				.waitForElementVisible("//button/span[text()='Add widget']", 1000, false)
				.click("//button/span[text()='Add widget']")
				.pause(1000)
				.assert.visible("//section[@class='widget']/header/span[@class='wdgtName' and text()='" + widget + "']");
		}
	},





	"test06DeleteSection: Save SectionId": function (browser) {
		browser
			.useXpath()
			.waitForElementVisible ("(//section[contains(@id, 'section_')])[last()]", 1000)
			.getAttribute("(//section[contains(@id, 'section_')])[last()]", "id", function(result) {
					section1 = result.value;
					console.log("Section1Id: " + section1);
				}
			)
	},
	
	"test06DeleteSection: Click Delete Section Button": function (browser) {
		browser
			.useXpath()
			.waitForElementVisible ("(//a[@title='Delete Section'])[last()]", 1000)
			.click ("(//a[@title='Delete Section'])[last()]")
			.pause(2000);
	},
	
	"test06DeleteSection: Confirm Delete Section" : function (browser) {
		browser
			.useXpath()
			.waitForElementVisible ("//button/span[text()='Delete section']", 3000)
			.click ("//button/span[text()='Delete section']")
			.pause(2000);
	},
	
	"test06DeleteSection: Save": function (browser) {
		browser
			.useXpath()
			.waitForElementVisible("//a[@title='Save']", 1000)
			.click("//a[@title='Save']")
			.pause(1000)
			.refresh()
			.pause(3000)
			.assert.elementNotPresent("//section[@id = '" + section1 + "']");
	},

	"Assert New Section is added" : function(browser){
		browser.elements("xpath", "//section[contains(@id, 'section_')]", function(result){
			sectionCountAfter = result.value.length;
			console.log("Number of Sections Before: " + sectionCountBefore);
			browser.assert.equal(sectionCountBefore - 1, sectionCountAfter)

		});
		
	},

	"test06DeleteSection: Logout" : function (browser) {
		browser.logout();
	},
	
		
};






