/**
* @summary Settings: Dashboard: Widgets: Revenu by the Revenue Type: EMS-10437
* @since 7.5
* @version 1.0
* @author Kianoush Nesvaderani
* @requires 
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("FlexChildName");
var password = data("Password");


module.exports = {
	tags : ['smoketag', 'completetesttag'],
	/**
	*@description Logs the user into Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
		.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	/**
	*@description Adds and Saves the Widget
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigates to Location" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("a.linkButtonFtr.edit", 5000)
		.click("a.linkButtonFtr.edit")
		.useXpath()
		.waitForElementVisible("//span[contains(text(),'Total Revenue by Revenue Type')]/..//a[@title='Option Menu']", 5000)
		.click("//span[contains(text(),'Total Revenue by Revenue Type')]/..//a[@title='Option Menu']")
		.waitForElementVisible("//a[contains(text(),'Delete')]", 5000)
		.click("//a[contains(text(),'Delete')]")
		.waitForElementVisible("//span[contains(text(),'Delete widget')]", 5000)
		.click("//span[contains(text(),'Delete widget')]")
		.click("//a[contains(text(),'Save')]");
	},
	
	/**
	*@description Verifies the Widget Added Successfully
	*@param browser - The web browser object
	*@returns void
	**/
	"Assert the drawing" : function (browser) {
		browser
		.useXpath()
		.pause(3000)
		.assert.elementNotPresent("//span[contains(text(),'Total Revenue by Revenue Type')]");
	},
	
	/**
	*@description Logs the user out of Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Logout" : function (browser) {
		browser.logout();
	},
};
