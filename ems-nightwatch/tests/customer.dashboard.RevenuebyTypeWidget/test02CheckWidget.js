/**
* @summary Settings: Dashboard: Widgets: Revenu by the Revenue Type: EMS-10437
* @since 7.5
* @version 1.0
* @author Kianoush Nesvaderani
* @requires 
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("FlexChildName");
var password = data("Password");


module.exports = {
	tags : ['smoketag', 'completetesttag'],
	/**
	*@description Logs the user into Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"test02CheckWidget: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
		.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	/**
	*@description Click Edit, Click the options for widget, Click Settings
	*@param browser - The web browser object
	*@returns void
	**/
	"test02CheckWidget: Navigate to Edit Widget" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("a.linkButtonFtr.edit", 5000)
		.click("a.linkButtonFtr.edit")
		.useXpath()
		.waitForElementVisible("//span[contains(text(),'Total Revenue by Revenue Type')]/..//a[@title='Option Menu']", 5000)
		.click("//span[contains(text(),'Total Revenue by Revenue Type')]/..//a[@title='Option Menu']")
		.useCss()
		.waitForElementVisible("a.settings.settingsWdgt", 5000)
		.click("a.settings.settingsWdgt");
	},
	
	/**
	*@description Verify the Payment Type options
	*@param browser - The web browser object
	*@returns void
	**/
	"test02CheckWidget: Verify Payment Type Options" : function (browser) {
		browser
		.pause(3000)
		.useCss()
		.click("#clearSelected")
		.useXpath()
		.assert.elementPresent("//li[contains(text(),'Cash')]")
		.assert.elementPresent("//li[contains(text(),'Bill')]")
		.assert.elementPresent("//li[contains(text(),'Coin')]")
		.assert.elementPresent("//li[contains(text(),'Legacy Cash')]")
		.assert.elementPresent("//li[contains(text(),'Credit Card')]")
		.assert.elementPresent("//li[contains(text(),'CC External')]")
		.assert.elementPresent("//li[contains(text(),'Credit Card Chip')]")
		.assert.elementPresent("//li[contains(text(),'Credit Card Swipe')]")
		.assert.elementPresent("//li[contains(text(),'Credit Card Tap')]")
		.assert.elementPresent("//li[contains(text(),'OtherCards')]")
		.assert.elementPresent("//li[contains(text(),'Passcard')]")
		.assert.elementPresent("//li[contains(text(),'Smart Card')]");
	},


		/**
	*@description Verify that the selection of Cash and Child Payment Types options restrict the correct options
	*@param browser - The web browser object
	*@returns void
	**/
	"test02CheckWidget: Verify Restriction of Payment Type Options: Cash as Parent" : function (browser) {

		// Cash as parent
		browser
		// .useCss()
		// .click("#clearSelected")

		.useXpath()
		.click("//li[contains(text(),'Cash')]")
		.click("//li[contains(text(),'CC External')]")
		.click("//li[contains(text(),'Passcard')]")
		.assert.elementPresent("//li[@class='level2 selected' and contains(text(), 'Cash')]")
		.assert.hidden("//li[contains(text(),'Bill')]")
		.assert.hidden("//li[contains(text(),'Coin')]")
		.assert.hidden("//li[contains(text(),'Legacy Cash')]")
		.assert.elementPresent("//li[@class='level2 inactive notclickable' and contains(text(), 'Credit Card')]")
		.assert.elementPresent("//li[@class='active level2 inactive notclickable' and contains(text(), 'OtherCards')]")

		.click("//button/span[contains(text(),'Apply changes')]")

		.waitForElementVisible("//span[contains(text(),'Total Revenue by Revenue Type')]/..//a[@title='Option Menu']", 3000)
		.click("//span[contains(text(),'Total Revenue by Revenue Type')]/..//a[@title='Option Menu']")

		.useCss()
		.waitForElementVisible("a.settings.settingsWdgt", 3000)
		.click("a.settings.settingsWdgt")
		.pause(2000)

		.useXpath()
		.assert.elementPresent("//li[@class='selected level2' and contains(text(), 'Cash')]")
		.assert.hidden("//li[contains(text(),'Bill')]")
		.assert.hidden("//li[contains(text(),'Coin')]")
		.assert.hidden("//li[contains(text(),'Legacy Cash')]")
		.assert.elementPresent("//li[@class='active level2 inactive notclickable' and contains(text(), 'Credit Card')]")
		.assert.elementPresent("//li[@class='active level2 inactive notclickable' and contains(text(), 'OtherCards')]")

	},
	
			/**
	*@description Verify that the selection of Credit Card and Child Payment Types options restrict the correct options
	*@param browser - The web browser object
	*@returns void
	**/
	"test02CheckWidget: Verify Restriction of Payment Type Options: Credit Card as Parent" : function (browser) {

		// Credit Card as Parent
		browser
		.useCss()
		.click("#clearSelected")

		.useXpath()
		.click("//li[contains(text(),'Credit Card')]")
		.click("//li[contains(text(),'Bill')]")
		.click("//li[contains(text(),'Smart Card')]")
		.assert.elementPresent("//li[@class='active level2 selected' and contains(text(), 'Credit Card')]")
		.assert.hidden("//li[contains(text(),'CC External')]")
		.assert.hidden("//li[contains(text(),'Credit Card Chip')]")
		.assert.hidden("//li[contains(text(),'Credit Card Swipe')]")
		.assert.hidden("//li[contains(text(),'Credit Card Tap')]")
		.assert.elementPresent("//li[@class='level2 inactive notclickable' and contains(text(), 'Cash')]")
		.assert.elementPresent("//li[@class='active level2 inactive notclickable' and contains(text(), 'OtherCards')]")

		.click("//button/span[contains(text(),'Apply changes')]")

		.waitForElementVisible("//span[contains(text(),'Total Revenue by Revenue Type')]/..//a[@title='Option Menu']", 3000)
		.click("//span[contains(text(),'Total Revenue by Revenue Type')]/..//a[@title='Option Menu']")

		.useCss()
		.waitForElementVisible("a.settings.settingsWdgt", 3000)
		.click("a.settings.settingsWdgt")
		.pause(2000)

		.useXpath()
		.assert.elementPresent("//li[@class='selected level2' and contains(text(), 'Credit Card')]")
		.assert.hidden("//li[contains(text(),'CC External')]")
		.assert.hidden("//li[contains(text(),'Credit Card Chip')]")
		.assert.hidden("//li[contains(text(),'Credit Card Swipe')]")
		.assert.hidden("//li[contains(text(),'Credit Card Tap')]")
		.assert.elementPresent("//li[@class='active level2 inactive notclickable' and contains(text(), 'Cash')]")
		.assert.elementPresent("//li[@class='active level2 inactive notclickable' and contains(text(), 'OtherCards')]")


	},
			/**
	*@description Verify that the selection of OtherCards and Child Payment Types options restrict the correct options
	*@param browser - The web browser object
	*@returns void
	**/
	"test02CheckWidget: Verify Restriction of Payment Type Options: OtherCards as Parent" : function (browser) {


		// OtherCards as parent
		browser
		.useCss()
		.click("#clearSelected")

		.useXpath()
		.click("//li[contains(text(),'OtherCards')]")
		.click("//li[contains(text(),'Coin')]")
		.click("//li[contains(text(),'Credit Card Chip')]")
		.assert.elementPresent("//li[@class='active level2 selected' and contains(text(), 'OtherCards')]")
		.assert.hidden("//li[contains(text(),'Passcard')]")
		.assert.hidden("//li[contains(text(),'Smart Card')]")
		.assert.elementPresent("//li[@class='active level2 inactive notclickable' and contains(text(), 'Cash')]")
		.assert.elementPresent("//li[@class='level2 inactive notclickable' and contains(text(), 'Credit Card')]")


		.click("//button/span[contains(text(),'Apply changes')]")

		.waitForElementVisible("//span[contains(text(),'Total Revenue by Revenue Type')]/..//a[@title='Option Menu']", 3000)
		.click("//span[contains(text(),'Total Revenue by Revenue Type')]/..//a[@title='Option Menu']")

		.useCss()
		.waitForElementVisible("a.settings.settingsWdgt", 3000)
		.click("a.settings.settingsWdgt")
		.pause(2000)

		.useXpath()
		.assert.elementPresent("//li[@class='selected level2' and contains(text(), 'OtherCards')]")
		.assert.hidden("//li[contains(text(),'Passcard')]")
		.assert.hidden("//li[contains(text(),'Smart Card')]")
		.assert.elementPresent("//li[@class='active level2 inactive notclickable' and contains(text(), 'Cash')]")
		.assert.elementPresent("//li[@class='active level2 inactive notclickable' and contains(text(), 'Credit Card')]")

	},

	/**
	*@description Logs the user out of Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Logout" : function (browser) {
		browser.logout();
	},
};
