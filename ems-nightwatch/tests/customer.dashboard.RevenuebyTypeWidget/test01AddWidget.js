/**
* @summary Settings: Dashboard: Widgets: Revenu by the Revenue Type: EMS-10437
* @since 7.5
* @version 1.0
* @author Kianoush Nesvaderani
* @requires 
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("FlexChildName");
var password = data("Password");


module.exports = {
	tags : ['smoketag', 'completetesttag'],
	/**
	*@description Logs the user into Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
		.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	/**
	*@description Adds and Saves the Widget
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigates to Location" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("a.linkButtonFtr.edit", 5000)
		.click("a.linkButtonFtr.edit")
		.waitForElementVisible("a.linkButtonIcn.addWidget > img", 5000)
		.click("a.linkButtonIcn.addWidget > img")
		.waitForElementVisible("#metric11", 5000)
		.click("#metric11")
		.useXpath()
		.waitForElementVisible("//header[contains(text(),'Total Revenue by Revenue Type')]", 5000)
		.click("//header[contains(text(),'Total Revenue by Revenue Type')]")
		.waitForElementVisible("//span[contains(text(),'Add widget')]", 5000)
		.click("//span[contains(text(),'Add widget')]")
		.waitForElementVisible("//section[@id='headerTools']/a[contains(text(),'Save')]", 5000)
		.click("//section[@id='headerTools']/a[contains(text(),'Save')]");
	},
	
	/**
	*@description Verifies the Widget Added Successfully
	*@param browser - The web browser object
	*@returns void
	**/
	"Assert the drawing" : function (browser) {
		browser
		.useCss()
		.pause(3000)
		.assert.containsText("span.wdgtName", "TOTAL REVENUE BY REVENUE TYPE");
	},
	
	/**
	*@description Logs the user out of Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Logout" : function (browser) {
		browser.logout();
	},
};
