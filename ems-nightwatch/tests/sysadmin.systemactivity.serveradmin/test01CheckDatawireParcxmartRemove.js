/**
* @summary System Admin- Server Admin- Test That Datawire and Parcxmart are not listed 
* @since 7.3.4
* @version 1.0
* @author Lonney McD
* @see US982 - DE49
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login System Admin 1" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},

	"Navigate to System Activity> Server Admin " : function (browser)
	{		
        browser.navigateTo("System Activity");
		browser.navigateTo("Server Admin");
	},

	"Verify Paymentech visible in Card Processing Queue Status list" : function (browser)
	{
		browser
			.useXpath()
			.assert.elementPresent("//ul[@id='cardProcessingQueue']//p[contains(text(),'Paymentech')]");
	},
		
	"Verify Datawire not present in Card Processing Queue Status list" : function (browser)
	{
		browser
			.useXpath()
			.assert.elementNotPresent("//ul[@id='cardProcessingQueue']//p[contains(text(),'Datawire')]");
	},
	
	"Verify Parcxmart not present in Card Processing Queue Status list" : function (browser)
	{
		browser
			.useXpath()
			.assert.elementNotPresent("//ul[@id='cardProcessingQueue']//p[contains(text(),'Parcxmart')]");
	},
	
	"Logout" : function (browser) 
	{
		browser.logout();
	},
	
};	