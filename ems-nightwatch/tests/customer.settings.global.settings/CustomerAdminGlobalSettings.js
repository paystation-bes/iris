var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	
	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.initialLogin(username, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Click on Settings in the Sidebar" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//nav[@id='main']/section[@class='bottomSection']/a/img", 7000)
			.click("//nav[@id='main']/section[@class='bottomSection']/a/img")
			.waitForFlex();
	},

	"Click on the Global tab within Settings" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='secondNavArea']//a[@title='Global']", 7000)
			.click("//section[@id='secondNavArea']//a[@title='Global']")
			.waitForFlex();
	},	
	
	"Click on Settings under the Global tab within Settings" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='thirdNavArea']/nav/a[@title='Settings']", 7000)
			.click("//section[@id='thirdNavArea']/nav/a[@title='Settings']")
			.waitForFlex();		
	},		
	
	"Change Time Zone button to US/Eastern" : function (browser)
	{
		var tzSelector = "//section[@id='timeZone']//input[@id='selectTimeZone']";
		var targetTz = "//li[@class='ui-menu-item']/a[text()='US/Eastern']";
		browser
			.clientclick("a#btnEditTimeZone")
			.useXpath()
			.waitForElementVisible(tzSelector, 7000)
			.click(tzSelector)
			.pause(1000)
			.waitForElementVisible(targetTz, 7000)
			.click(targetTz);
	},
	
	"Click on the Save button for Edit Time Zone" : function (browser)
	{
		browser
			.scrollTop()
			.clientclick("a#btnSaveTimeZone")
			.waitForDelay();			
	},
	
	"Assert that the Time Zone has changed to US/Eastern" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='timeZone']//span[@id='curTimeZone' and text()='US/Eastern']", 10000);
	},
	
	"Change Permits Enforced By to Organization - Expiry Time of Most Recent Permit" : function (browser)
	{
		browser
			.clientclick("a#btnEditQuerySpacesBy")
			.useXpath()
			.waitForElementVisible("//section[@id='querySpacesBy']//input[@id='selectQuerySpacesBy']", 7000)
			.click("//section[@id='querySpacesBy']//input[@id='selectQuerySpacesBy']")
			.pause(1000)
			.waitForElementVisible("//li[@class='ui-menu-item']//a[text()='Organization - Expiry Time of Most Recent Permit']", 7000)
			.click("//li[@class='ui-menu-item']//a[text()='Organization - Expiry Time of Most Recent Permit']");
	},

	"Click on the Save button for Permits Enforced By" : function (browser)
	{
		browser
			.scrollTop()
			.clientclick("a#btnSaveQuerySpacesBy")
			.waitForDelay();
	},
	
	"Assert that Permits Enforced By has changed to Organization - Expiry Time of Most Recent Permit" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='querySpacesBy']//span[@id='curQuerySpacesBy' and text()='Organization - Expiry Time of Most Recent Permit']", 7000);
	},
	
	"Change the Current Time Zone back to Canada/Pacific" : function (browser)
	{
		browser
			.clientclick("a#btnEditTimeZone")
			.useXpath()
			.waitForElementVisible("//section[@id='timeZone']//input[@id='selectTimeZone']", 7000)
			.click("//section[@id='timeZone']//input[@id='selectTimeZone']")
			.pause(1000)
			.waitForElementVisible("//li[@class='ui-menu-item']/a[text()='Canada/Pacific']", 7000)
			.click("//li[@class='ui-menu-item']/a[text()='Canada/Pacific']");
	},
	
	"Click on the Save button for Edit Time Zone again" : function (browser)
	{
		browser
			.scrollTop()
			.clientclick("a#btnSaveTimeZone")
			.waitForDelay();
	},
	
	"Assert that the Time Zone has changed back to Canada/Pacific" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='timeZone']//span[@id='curTimeZone' and text()='Canada/Pacific']", 7000);
	},
	
	"Change Permits Enforced By back to Location - Latest Expiry Time" : function (browser)
	{
		browser
			.clientclick("a#btnEditQuerySpacesBy")
			.useXpath()
			.waitForElementVisible("//section[@id='querySpacesBy']//input[@id='selectQuerySpacesBy']", 7000)
			.click("//section[@id='querySpacesBy']//input[@id='selectQuerySpacesBy']")
			.pause(1000)
			.waitForElementVisible("//li[@class='ui-menu-item']//a[text()='Location - Latest Expiry Time']", 7000)
			.click("//li[@class='ui-menu-item']//a[text()='Location - Latest Expiry Time']");
	},

	"Click on the Save button for Permits Enforced By again" : function (browser)
	{
		browser
			.scrollTop()
			.clientclick("a#btnSaveQuerySpacesBy")
			.waitForDelay();
	},
	
	"Assert that Permits Enforced By has changed back to Location - Latest Expiry Time" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='querySpacesBy']//span[@id='curQuerySpacesBy' and text()='Location - Latest Expiry Time']", 7000);
	},
	
	"Logout" : function (browser)
	{
		browser.logout();
	}
};