/**
 * @summary WebServices: SigningServiceV2 should not be visible in /services page
 * @since 7.3.4
 * @version 1.0
 * @author Mandy F
 * @see US913: DE272
 **/

var data = require('../../data.js');

var servurl = data("ServicesURL");

// xpath for the beginning of every endpoint address
var endpointAddress = "(//span[@class='value'])[contains(text(), '" + servurl + "/";
// xpath for the beginning of every WSDL
var wsdl = "//a[contains(text(), '{http://ws.digitalpaytech.com/";
// xpath for the beginning of every target namespace
var targetNamespace = "(//span[@class='value'])[contains(text(), 'http://ws.digitalpaytech.com/";

module.exports = {
	tags: ['smoketag', 'featuretesttag', 'completetesttag'],
	/**
	 * @description Opens the services page
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"SigningServicesV2NotVisible: Open Services Page" : function (browser) {
		browser
		.url(servurl)
		.windowMaximize('current')
		.assert.title('CXF - Service list');
	},
	
	/**
	 * @description Verifies that AuditInfoService is visible on page
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"SigningServicesV2NotVisible: Verify AuditInfoService Visible" : function (browser) {
		var auditInfoTableHeader = "(//span[@class='porttypename'])[contains(text(), 'AuditInfoService')]";
		var auditInfoServiceList = auditInfoTableHeader + "//following-sibling::*//li[contains(text(),";
		browser
		.useXpath()
		.assert.visible(auditInfoTableHeader)
		// assert all the following services are visible under AuditInfoService
		.assert.visible(auditInfoServiceList + " 'getGroups')]")
		.assert.visible(auditInfoServiceList + " 'getCollectionByDate')]")
		.assert.visible(auditInfoServiceList + " 'getAuditByDate')]")
		.assert.visible(auditInfoServiceList + " 'getCollectionByRegion')]")
		.assert.visible(auditInfoServiceList + " 'getRegions')]")
		.assert.visible(auditInfoServiceList + " 'getCollectionByPaystation')]")
		.assert.visible(auditInfoServiceList + " 'getAuditByGroup')]")
		.assert.visible(auditInfoServiceList + " 'getAuditByPaystation')]")
		.assert.visible(auditInfoServiceList + " 'getCollectionByGroup')]")
		.assert.visible(auditInfoServiceList + " 'getPaystations')]")
		.assert.visible(auditInfoServiceList + " 'getAuditByRegion')]")
		// assert endpoint address, WSDL, target namespace are visible for AuditInfoService
		.assert.visible(endpointAddress + "AuditInfoService')]")
		.assert.visible(wsdl + "auditInfo}AuditInfoService')]")
		.assert.visible(targetNamespace + "auditInfo')]")
		.pause(1000);
	},
		
	/**
	 * @description Verifies that PaystationInfoService is visible on page
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"SigningServicesV2NotVisible: Verify PaystationInfoService Visible" : function (browser) {
		var PaystationInfoTableHeader = "(//span[@class='porttypename'])[contains(text(), 'PaystationInfoService')]";
		var PaystationInfoServiceList = PaystationInfoTableHeader + "//following-sibling::*//li[contains(text(),";
		browser
		.useXpath()
		.assert.visible(PaystationInfoTableHeader)
		// assert all the following services are visible under PaystationInfoService
		.assert.visible(PaystationInfoServiceList + " 'getPaystationEventsByGroup')]")
		.assert.visible(PaystationInfoServiceList + " 'getPaystationStatusByGroup')]")
		.assert.visible(PaystationInfoServiceList + " 'getPaystations')]")
		.assert.visible(PaystationInfoServiceList + " 'getPaystationStatus')]")
		.assert.visible(PaystationInfoServiceList + " 'getPaystationStatusBySerial')]")
		.assert.visible(PaystationInfoServiceList + " 'getPaystationEventsByRegion')]")
		.assert.visible(PaystationInfoServiceList + " 'getGroups')]")
		.assert.visible(PaystationInfoServiceList + " 'getPaystationEvents')]")
		.assert.visible(PaystationInfoServiceList + " 'getPaystationStatusByRegion')]")
		.assert.visible(PaystationInfoServiceList + " 'getPaystationEventsBySerial')]")
		// assert endpoint address, WSDL, target namespace are visible for PaystationInfoService
		.assert.visible(endpointAddress + "PaystationInfoService')]")
		.assert.visible(wsdl + "paystationInfo}PaystationInfoService')]")
		.assert.visible(targetNamespace + "paystationInfo')]")
		.pause(1000);
	},
	
	/**
	 * @description Verifies that PlateInfoService is visible on page
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"SigningServicesV2NotVisible: Verify PlateInfoService Visible" : function (browser) {
		var PlateInfoTableHeader = "(//span[@class='porttypename'])[contains(text(), 'PlateInfoService')]";
		var PlateInfoServiceList = PlateInfoTableHeader + "//following-sibling::*//li[contains(text(),";
		browser
		.useXpath()
		.assert.visible(PlateInfoTableHeader)
		// assert all the following services are visible under PlateInfoService
		.assert.visible(PlateInfoServiceList + " 'getUpdatedValidPlatesByRegion')]")
		.assert.visible(PlateInfoServiceList + " 'getUpdatedValidPlates')]")
		.assert.visible(PlateInfoServiceList + " 'getUpdatedValidPlatesByGroup')]")
		.assert.visible(PlateInfoServiceList + " 'getValidPlatesByRegion')]")
		.assert.visible(PlateInfoServiceList + " 'getPlateInfo')]")
		.assert.visible(PlateInfoServiceList + " 'getValidPlates')]")
		.assert.visible(PlateInfoServiceList + " 'getExpiredPlatesByGroup')]")
		.assert.visible(PlateInfoServiceList + " 'getGroups')]")
		.assert.visible(PlateInfoServiceList + " 'getExpiredPlatesByRegion')]")
		.assert.visible(PlateInfoServiceList + " 'getExpiredPlates')]")
		.assert.visible(PlateInfoServiceList + " 'getRegions')]")
		.assert.visible(PlateInfoServiceList + " 'getValidPlatesByGroup')]")
		// assert endpoint address, WSDL, target namespace are visible for PlateInfoService
		.assert.visible(endpointAddress + "PlateInfoService')]")
		.assert.visible(wsdl + "plateInfo}PlateInfoService')]")
		.assert.visible(targetNamespace + "plateInfo')]")
		.pause(1000);
	},
	
	/**
	 * @description Verifies that StallInfoService is visible on page
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"SigningServicesV2NotVisible: Verify StallInfoService Visible" : function (browser) {
		var StallInfoTableHeader = "(//span[@class='porttypename'])[contains(text(), 'StallInfoService')]";
		var StallInfoServiceList = StallInfoTableHeader + "//following-sibling::*//li[contains(text(),";
		browser
		.useXpath()
		.assert.visible(StallInfoTableHeader)
		// assert all the following services are visible under StallInfoService
		.assert.visible(StallInfoServiceList + " 'getRegions')]")
		.assert.visible(StallInfoServiceList + " 'getSettings')]")
		.assert.visible(StallInfoServiceList + " 'getStallInfoByGroup')]")
		.assert.visible(StallInfoServiceList + " 'getStallInfo')]")
		.assert.visible(StallInfoServiceList + " 'getGroups')]")
		.assert.visible(StallInfoServiceList + " 'getStallInfoBySetting')]")
		.assert.visible(StallInfoServiceList + " 'getStallInfoByRegion')]")
		// assert endpoint address, WSDL, target namespace are visible for StallInfoService
		.assert.visible(endpointAddress + "StallInfoService')]")
		.assert.visible(wsdl + "stallInfo}StallInfoService')]")
		.assert.visible(targetNamespace + "stallInfo')]")
		.pause(1000);
	},
	
	/**
	 * @description Verifies that TransactionInfoService is visible on page
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"SigningServicesV2NotVisible: Verify TransactionInfoService Visible" : function (browser) {
		var TransactionInfoTableHeader = "(//span[@class='porttypename'])[contains(text(), 'TransactionInfoService')]";
		var TransactionInfoServiceList = TransactionInfoTableHeader + "//following-sibling::*//li[contains(text(),";
		browser
		.useXpath()
		.assert.visible(TransactionInfoTableHeader)
		// assert all the following services are visible under TransactionInfoTableHeader
		.assert.visible(TransactionInfoServiceList + " 'getSettings')]")
		.assert.visible(TransactionInfoServiceList + " 'getGroups')]")
		.assert.visible(TransactionInfoServiceList + " 'getTransactionBySettlementDate')]")
		.assert.visible(TransactionInfoServiceList + " 'getProcessingStatusTypes')]")
		.assert.visible(TransactionInfoServiceList + " 'getTransactionByStall')]")
		.assert.visible(TransactionInfoServiceList + " 'getRegions')]")
		.assert.visible(TransactionInfoServiceList + " 'getTransactionBySetting')]")
		.assert.visible(TransactionInfoServiceList + " 'getTransactionByGroup')]")
		.assert.visible(TransactionInfoServiceList + " 'getTransactionByPurchasedDate')]")
		.assert.visible(TransactionInfoServiceList + " 'getPaymentTypes')]")
		.assert.visible(TransactionInfoServiceList + " 'getTransactionByRegion')]")
		.assert.visible(TransactionInfoServiceList + " 'getPaystations')]")
		.assert.visible(TransactionInfoServiceList + " 'getTransactionTypes')]")
		.assert.visible(TransactionInfoServiceList + " 'getTransactionBySerialNumber')]")
		// assert endpoint address, WSDL, target namespace are visible for TransactionInfoService
		.assert.visible(endpointAddress + "TransactionInfoService')]")
		.assert.visible(wsdl + "transactionInfo}TransactionInfoService')]")
		.assert.visible(targetNamespace + "transactionInfo')]")
		.pause(1000);
	},
	
	/**
	 * @description Verifies that SigningServiceV2 is not visible on page
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"SigningServicesV2NotVisible: Verify SigningServiceV2 Not Visible" : function (browser) {
		var SigningServiceV2TableHeader = "(//span[@class='porttypename'])[contains(text(), 'SigningServiceV2')]";
		var SigningServiceV2List = SigningServiceV2TableHeader + "//following-sibling::*//li[contains(text(),";
		browser
		.useXpath()
		.assert.elementNotPresent(SigningServiceV2TableHeader)
		// assert all the following services are not visible under SigningServiceV2
		.assert.elementNotPresent(SigningServiceV2List + " 'getSignString')]")
		.assert.elementNotPresent(SigningServiceV2List + " 'getSignBytes')]")
		// assert endpoint address, WSDL, target namespace are not visible for SigningServiceV2
		.assert.elementNotPresent(endpointAddress + "SigningServiceV2')]")
		.assert.elementNotPresent(wsdl + "SigningServiceV2/}SigningServiceV2')]")
		.assert.elementNotPresent(targetNamespace + "SigningServiceV2')]")
		.pause(1000);
	},
	
	/**
	 * @description Closes the services page
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"SigningServicesV2NotVisible: Close Page" : function (browser) {
		browser
        .pause(1000)
        .end()
		.closeWindow();
	},
};
