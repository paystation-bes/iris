/*Prerequisite of Edit Coupon and Delete Coupon*/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ChildUserName");
var password = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) 
	{
		console.log("User: " + username + "/" + password);
		browser	
			.url(devurl)
			.windowMaximize('current')
			.initialLogin(username, password, "password")
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
		"Click on Accounts in the Sidebar" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//nav[@id='main']/section[@class='section']/a[@title='Accounts']/img", 2000)
			.click("//nav[@id='main']/section[@class='section']/a[@title='Accounts']/img")
			.pause(1000)
	},

		"Click on the Coupon tab within Settings" : function (browser)
		
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='secondNavArea']//a[@title='Coupons']", 2000)
			.click("//section[@id='secondNavArea']//a[@title='Coupons']")
			.pause(1000)
			
	},	
	

		"Click on the Option Menu within Coupons" : function (browser)
		
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='couponPanel']//a[@id='opBtn_couponMenu']", 2000)
			.click("//section[@id='couponPanel']//a[@id='opBtn_couponMenu']")
			.pause(1000)
			
	},	
	
		"Click on Add within the Option Menu" : function (browser)
		
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='menu_couponMenu']/section/a[@class='add']", 2000)
			.click("//section[@id='menu_couponMenu']/section/a[@class='add']")
			.pause(1000)
			
	},		
	
		"Fill in the Coupon fields" : function (browser)
	
		{
		browser
			.useXpath()
			.waitForElementVisible("//input[@id='formCouponCode']", 2000)
			.setValue("//input[@id='formCouponCode']", 'ADDTEST01')
			.assert.visible("//input[@id='formDiscountValue']")
			.setValue("//input[@id='formDiscountValue']", '25')
			.assert.visible("//label[@id='pndEnabled_true']/a[@class='checkBox']")
			.click("//label[@id='pndEnabled_true']/a[@class='checkBox']")
			.pause(1000)
			
	},

		"Click on SAVE to save the Coupon" : function (browser)
		
	{
		browser
			.useXpath()
			.waitForElementVisible("//form[@id='couponForm']/section/section[@class='btnSet']/a[@class='linkButtonFtr textButton save']", 2000)
			.click("//form[@id='couponForm']/section/section[@class='btnSet']/a[@class='linkButtonFtr textButton save']")
			.pause(1000)
			
	},	
	
		"Click to get the details of Coupon Code ADDTEST01" : function (browser)
		
	{
		browser
			.useXpath()
			.pause(2000)
			.waitForElementVisible("//ul[@id='couponListContainer']//p[contains(text(),'ADDTEST01')]", 4000)
			.click("//ul[@id='couponListContainer']//p[contains(text(),'ADDTEST01')]")
			.pause(1000)
			
	},	
	
		"Assert the details of Coupon Code ADDTEST01" : function (browser)
		
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='couponView' and 'Coupon Details']/header/h2", 4000)
			.assert.visible("//section[@id='couponView' and 'Coupon Details']/header/h2")
			.assert.visible("//section[@id='couponView']//dd[contains(text(),'ADDTEST01')]")
			.assert.visible("//section[@id='couponView']//dd[contains(text(),'25 %')]")
			.assert.visible("//section[@id='couponView']//dd[contains(text(),'Pay and Display/Pay by License Plate')]")
			.pause(1000)
			
	},	

		"Logout" : function (browser) 
	{
		browser.logout();
	},
	
};	