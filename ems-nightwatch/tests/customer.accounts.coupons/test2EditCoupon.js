/*Prerequisite Scripts: Add Coupon*/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ChildUserName");
var password = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
		"Click on Accounts in the Sidebar" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//nav[@id='main']/section[@class='section']/a[@title='Accounts']/img", 5000)
			.click("//nav[@id='main']/section[@class='section']/a[@title='Accounts']/img")
			.pause(1000)
	},

		"Click on the Coupon tab within Settings" : function (browser)
		
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='secondNavArea']//a[@title='Coupons']", 5000)
			.click("//section[@id='secondNavArea']//a[@title='Coupons']")
			.pause(1000)
			
	},	
	

		"Click on the Option Menu and edit within Coupons List" : function (browser)
		
	{
		browser
			.useXpath()
			.waitForElementVisible("//li[@class='clickable']//p[contains(text(),'ADDTEST01')]/../..//a[@title='Option Menu']", 5000)
			.click("//li[@class='clickable']//p[contains(text(),'ADDTEST01')]/../..//a[@title='Option Menu']")
			.waitForElementVisible("//p[contains(text(),'ADDTEST01')]/../../following-sibling::section[1]/section/a[@class='edit']",5000)
			.click("//p[contains(text(),'ADDTEST01')]/../../following-sibling::section[1]/section/a[@class='edit']")
			.pause(1000)
			
	},	
	
		"Change the value of Percent Discount" : function (browser)
		
	{
		browser
			.useXpath()
			.waitForElementVisible("//input[@id='formDiscountValue']",3000)
			.clearValue("//input[@id='formDiscountValue']")
			.setValue("//input[@id='formDiscountValue']",'40')
			.setValue("//input[@id='formStartDate']",'01/01/2014')
			.setValue("//input[@id='formEndDate']",'12/31/2018')
			.clearValue("//input[@id='formLocation']")
			.setValue("//input[@id='formLocation']",'Airport')			
			.pause(1000)
			
	},		

		"Click on SAVE to save the Coupon" : function (browser)
		
	{
		browser
			.useXpath()
			.waitForElementVisible("//form[@id='couponForm']/section/section[@class='btnSet']/a[@class='linkButtonFtr textButton save']", 5000)
			.click("//form[@id='couponForm']/section/section[@class='btnSet']/a[@class='linkButtonFtr textButton save']")
			.pause(1000)
			
	},	
	
		
		"Assert the details of Coupon Code ADDTEST01" : function (browser)
		
	{
		browser
			.useXpath()
			.pause(2000)
			.waitForElementVisible("//section[@id='couponView' and 'Coupon Details']/header/h2", 3000)
			.assert.visible("//section[@id='couponView' and 'Coupon Details']/header/h2")
			.assert.visible("//section[@id='couponView']//dd[contains(text(),'ADDTEST01')]")
			.assert.visible("//section[@id='couponView']//dd[contains(text(),'40 %')]")
			.assert.visible("//section[@id='couponView']//dd[contains(text(),'01/01/2014')]")
			.assert.visible("//section[@id='couponView']//dd[contains(text(),'12/31/2018')]")
			.assert.visible("//section[@id='couponView']//dd[contains(text(),'Airport')]")
			.pause(1000)
			
	},	

		"Logout" : function (browser) 
	{
		browser.logout();
	},
};	