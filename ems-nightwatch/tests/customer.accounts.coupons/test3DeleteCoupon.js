/*Prerequisite Scripts: AddCoupon and EditCoupon*/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ChildUserName");
var password = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser)
	{browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	"Click on Accounts in the Sidebar" : function (browser)
	{browser
		.useXpath()
		.waitForElementVisible("//nav[@id='main']/section[@class='section']/a[@title='Accounts']/img", 2000)
		.click("//nav[@id='main']/section[@class='section']/a[@title='Accounts']/img")
		.pause(1000)
	},
	
	"Click on the Coupon tab within Settings" : function (browser)
	{browser
		.useXpath()
		.waitForElementVisible("//section[@id='secondNavArea']//a[@title='Coupons']", 2000)
		.click("//section[@id='secondNavArea']//a[@title='Coupons']")
		.pause(1000)
	},	
	
	"Assert existence of of Coupon Code ADDTEST01": function (browser)
	{browser
		.useXpath()
		.waitForElementVisible("//ul[@id='couponListContainer']//p[contains (text(),'ADDTEST01')]",4000)
		.pause(1000)
	},
	
	"Click options link for ADDTEST01 coupon" : function (browser)
	{browser
		.useXpath()
		.waitForElementVisible("//li[@class='clickable']//p[contains (text(),'ADDTEST01')]/../../a[@title='Option Menu']", 4000)
		.click("//li[@class='clickable']//p[contains (text(),'ADDTEST01')]/../../a[@title='Option Menu']")
		.pause(1000)
	},
	
	"Click on DELETE button for ADDTEST01 coupon" : function (browser)
	{browser
		.useXpath()
		.waitForElementVisible("//p[contains (text(),'ADDTEST01')]/../../following-sibling::section[1]/section/a[@class='delete']", 4000)
		//.click("//span[@style='display: block;']//li[@class='clickable']//p[contains (text(),'ADDTEST01')]/../../../..//section[@class='ddMenu optionMenu']//section[@class='menuOptions innerBorder']//a[@class='delete']")
		.click("//p[contains (text(),'ADDTEST01')]/../../following-sibling::section[1]/section/a[@class='delete']")
		.pause(1000)
	},
	
	"Click OK on Delete Confirmation pop-up" : function (browser)
	{browser
		.useXpath()
		.waitForElementVisible("//button[@type='button']//span[contains (text(),'Ok')]", 4000)
		.click("//button[@type='button']//span[contains (text(),'Ok')]")
		.pause(1000)
	},
	
	"Verify ADDTEST01 coupon is deleted" : function (browser)
		{browser
		.useXpath()
		.waitForElementNotVisible("//p[contains (text(),'ADDTEST01')]",4000)
		.pause(1000)
	},
	
	"Logout" : function (browser) 
	{	browser.logout();
	},
};