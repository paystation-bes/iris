/**
 * @summary Child Customer: Card Management: Banned Cards: Delete Banned Smart Card
 * @since 7.3.4
 * @version 1.0
 * @author Mandy F
 * @see US914
 * @requires test2AddBannedSmartCard.js
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ChildUserName2");
var password = data("Password");

// save the first card number in a variable to verify the deletion after
var saveFirstCardNumber = new String();

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	* @description Logs the user into Iris
	* @param browser - The web browser object
	* @returns void
	**/
	"test6DeleteBannedSmartCard: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	* @description Enters into the Banned Card page
	* @param browser - The web browser object
	* @returns void
	**/
	"test6DeleteBannedSmartCard: Entering Banned Cards" : function (browser) {
		var cardManagementLink = "//nav[@id='main']//a[@title='Card Management']";
		var bannedCardsLink = "//a[@title='Banned Cards']";
		browser
		.useXpath()
		.waitForElementVisible(cardManagementLink, 3000)
		.click(cardManagementLink)
		.waitForElementVisible(bannedCardsLink, 3000)
		.click(bannedCardsLink)
		.pause(1000);
	},
	
	/**
	* @description Finds and deletes the first available card in the list
	* @param browser - The web browser object
	* @returns void
	**/
	"test6DeleteBannedSmartCard: Delete First Banned Card" : function (browser) {
		var firstCardInfo = "//ul[@id='bannedCardListContainer']//li[1]";
		var firstCardNumber = firstCardInfo + "//div[@class='col1']//p";
		var firstDeleteButton = firstCardInfo + "//a[@title='Delete']";
		var okButton = "//button[@type='button']//span[contains(text(),'Ok')]";
		browser
		.useXpath()
		.waitForElementVisible(firstDeleteButton, 2000)
		.assert.visible(firstDeleteButton)
		// Obtain string of the Card Number to delete and save in variable declared globally
		.getText(firstCardNumber, function (result) {
			saveFirstCardNumber = result.value;
		})
		.click(firstDeleteButton)
		.waitForElementVisible(okButton, 1000)
		.click(okButton)
		.pause(2000);
	},
	
	/**
	* @description Verifies that the first available banned card is now deleted
	* @param browser - The web browser object
	* @returns void
	**/
	"test6DeleteBannedSmartCard: Verify Banned Card Deleted" : function (browser) {
		browser
		.useXpath()
		// Search the whole list of cards to find that the saved card number is deleted
		.assert.elementNotPresent("//p[contains(text(), '" + saveFirstCardNumber + "')]")
		.pause(1000);
	},
	
	/**
	* @description Logs the user out of Iris
	* @param browser - The web browser object
	* @returns void
	**/
	"test6DeleteBannedSmartCard: Logout" : function (browser) {
		browser.logout();
	},
};