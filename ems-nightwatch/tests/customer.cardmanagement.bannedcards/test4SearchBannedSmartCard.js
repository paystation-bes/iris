/**
 * @summary Child Customer: Card Management: Banned Cards: Search Banned Smart Card
 * @since 7.3.4
 * @version 1.0
 * @author MorganM, Mandy F
 * @see US914
 * @requires test2AddBannedSmartCard.js
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ChildUserName2");
var password = data("Password");

// variables to save verifying data in
var savedCCNum = new String();
var savedSCNum = new String();

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test4SearchBannedSmartCard: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)')
	},

	/**
	* @description Enters into the Banned Card page
	* @param browser - The web browser object
	* @returns void
	**/
	"test4SearchBannedSmartCard: Entering Banned Cards" : function (browser) {
		var cardManagementLink = "//nav[@id='main']//a[@title='Card Management']";
		var bannedCardsLink = "//a[@title='Banned Cards']";
		browser
		.useXpath()
		.waitForElementVisible(cardManagementLink, 1000)
		.click(cardManagementLink)
		.waitForElementVisible(bannedCardsLink, 1000)
		.click(bannedCardsLink)
		.pause(1000);
	},
	
	/**
	* @description Saves data in variables for one Credit Card and one Smart Card before search
	* @param browser - The web browser object
	* @returns void
	**/
	"test4SearchBannedSmartCard: Saving Banned Cards Information" : function (browser) {
		var firstCCNum = "//p[contains(text(), 'Credit Card')]//parent::*//parent::*//div[@class='col1']//p";
		var firstSCNum = "//p[contains(text(), 'Smart Card')]//parent::*//parent::*//div[@class='col1']//p";
		browser
		.useXpath()
		.waitForElementVisible(firstCCNum, 1000)
		.waitForElementVisible(firstSCNum, 1000)
		// get the credit card number to assert later it has not appeared
		.getText(firstCCNum, function (result) {
			savedCCNum = result.value.trim();
		})
		// get smart card number to search and to assert later that it has appeared
		.getText(firstSCNum, function (result) {
			savedSCNum = result.value.trim();
		})
		.pause(1000);
	},
	

	/**
	 * @description Enters the first Smart Card number into search field
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test4SearchBannedSmartCard: Search Banned Smart Card Information" : function (browser) {
		var cardNumberField = "//input[@id='filterCardNumber']"
		browser
		.useXpath()
		.waitForElementVisible(cardNumberField, 1000)
		.click(cardNumberField)
		.setValue(cardNumberField, savedSCNum)
		// click the search button
		.click("//a[contains(text(), 'Search')]")
		.pause(2000);
	},
	
	/**
	 * @description Verifies correct Smart Card information after searching
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test4SearchBannedSmartCard: Verify Searched Smart Card Information" : function (browser) {
		var bannedCard = "//p[contains(text(), 'Smart Card')]//parent::*//parent::*";
		browser
		.useXpath()
		.assert.visible(bannedCard)
		// assert card number is visible
		.assert.visible(bannedCard + "//p[contains(text(),'" + savedSCNum + "')]")
		// assert card type is visible
		.assert.visible(bannedCard + "//p[contains(text(),'Smart Card')]")
		// assert expiry date is visible
		.assert.visible(bannedCard + "//p[contains(text(),'N/A')]")
		// assert entry source is visible
		.assert.visible(bannedCard + "//p[contains(text(),'manual')]")
		.pause(1000);

	},
	
	/**
	 * @description Verifies Credit Card not present after searching 
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test4SearchBannedSmartCard: Verify Credit Card Not Present" : function (browser) {
		var bannedCard = "//p[contains(text(), 'Credit Card')]//parent::*//parent::*";
		browser
		.useXpath()
		.assert.elementNotPresent(bannedCard + "//p[contains(text(),'" + savedCCNum + "')]")
		.pause(1000);
	},

	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test4SearchBannedSmartCard: Logout" : function (browser) {
		browser.logout();
	},
};
