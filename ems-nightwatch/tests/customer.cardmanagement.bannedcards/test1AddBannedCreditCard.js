/**
 * @summary Child Customer: Card Management: Banned Cards: Add Banned Credit Card
 * @since 7.3.4
 * @version 1.0
 * @author Mandy F
 * @see US914
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ChildUserName2");
var password = data("Password");

// generate a non-expired expiry date for credit card
var d = new Date();
var year = (d.getFullYear() + 1).toString().substring(2,4);
var date = "12" + year;

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	* @description Logs the user into Iris
	* @param browser - The web browser object
	* @returns void
	**/
	"test1AddBannedCreditCard: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	* @description Enters into the Banned Card page
	* @param browser - The web browser object
	* @returns void
	**/
	"test1AddBannedCreditCard: Entering Banned Cards" : function (browser) {
		var cardManagementLink = "//nav[@id='main']//a[@title='Card Management']";
		var bannedCardsLink = "//a[@title='Banned Cards']";
		browser
		.useXpath()
		.waitForElementVisible(cardManagementLink, 1000)
		.click(cardManagementLink)
		.waitForElementVisible(bannedCardsLink, 1000)
		.click(bannedCardsLink)
		.pause(1000);
	},
	
	/**
	* @description Opens pop up to add a banned card
	* @param browser - The web browser object
	* @returns void
	**/
	"test1AddBannedCreditCard: Open Add Banned Card Pop Up" : function (browser) {
		var bannedCardsOptions = "//a[@id='opBtn_bannedCard']";
		var addBannedCard = "//a[@class='add']";
		browser
		.useXpath()
		.waitForElementVisible(bannedCardsOptions, 1000)
		.click(bannedCardsOptions)
		.waitForElementVisible(addBannedCard, 1000)
		.click(addBannedCard)
		.pause(1000);
	},

	/**
	* @description Fills in the information to add a banned Credit card
	* @param browser - The web browser object
	* @returns void
	**/
	"test1AddBannedCreditCard: Add Banned Card Information" : function (browser) {
		var expandCardTypeOptions = "//a[@id='formBannedCardTypeExpand']";
		var creditCardOption = "//a[contains(text(),'Credit Card')]";
		var numberForm = "//input[@id='formBannedCardNumber']";
		var expiryDateForm = "//label[@id='formBannedCardExpiryLabel']";
		var commentForm = "//textarea[@id='formBannedCardComment']";
		var saveButton = "(//button[@type='button'])[2]";
		browser
		.useXpath()
		.waitForElementVisible(expandCardTypeOptions, 1000)
		.click(expandCardTypeOptions)
		.waitForElementVisible(creditCardOption, 1000)
		.click(creditCardOption)
		.waitForElementVisible(numberForm, 1000)
		.click(numberForm)
		.setValue(numberForm, '4275330012345675')
		.waitForElementVisible(expiryDateForm, 1000)
		.click(expiryDateForm)
		.setValue(expiryDateForm, date)
		.waitForElementVisible(commentForm, 1000)
		.click(commentForm)
		.setValue(commentForm, 'XXXXXXXXXXXX5675 is banned')
		.waitForElementVisible(saveButton, 2000)
		.click(saveButton)
		.pause(3000);
	},

	/**
	* @description Verifies the information is correct after adding
	* @param browser - The web browser object
	* @returns void
	**/
	"test1AddBannedCreditCard: Verify Banned Card Information" : function (browser) {
		var bannedCard = "//li[@comment='XXXXXXXXXXXX5675 is banned']";
		browser
		.useXpath()
		.assert.visible(bannedCard)
		// assert banned card number is in the list
		.assert.visible(bannedCard + "//p[contains(text(),'XXXXXXXXXXXX5675')]")
		// assert banned card type is in the list
		.assert.visible(bannedCard + "//p[contains(text(),'Credit Card')]")
		// assert banned card expiry date is in the list
		.assert.visible(bannedCard + "//p[contains(text(),'" + date + "')]")
		// assert banned card source is in the list
		.assert.visible(bannedCard + "//p[contains(text(),'manual')]")
		.pause(1000);
	},
	
	/**
	* @description Logs the user out of Iris
	* @param browser - The web browser object
	* @returns void
	**/
	"test1AddBannedCreditCard: Logout" : function (browser) {
		browser.logout();
	},
};