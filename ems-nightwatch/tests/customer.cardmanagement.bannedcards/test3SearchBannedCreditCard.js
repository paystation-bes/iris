/**
 * @summary Child Customer: Card Management: Banned Cards: Search Banned Credit Card
 * @since 7.3.4
 * @version 1.0
 * @author Mandy F
 * @see US914
 * @requires test1AddBannedCreditCard.js
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ChildUserName2");
var password = data("Password");

// variables to save verifying data in
var savedCCNum = new String();
var savedLastDigitsCC = new String();
var savedCCExp = new String();
var savedSCNum = new String();

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test3SearchBannedCreditCard: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)')
	},

	/**
	* @description Enters into the Banned Card page
	* @param browser - The web browser object
	* @returns void
	**/
	"test3SearchBannedCreditCard: Entering Banned Cards" : function (browser) {
		var cardManagementLink = "//nav[@id='main']//a[@title='Card Management']";
		var bannedCardsLink = "//a[@title='Banned Cards']";
		browser
		.useXpath()
		.waitForElementVisible(cardManagementLink, 1000)
		.click(cardManagementLink)
		.waitForElementVisible(bannedCardsLink, 1000)
		.click(bannedCardsLink)
		.pause(1000);
	},
	
	/**
	* @description Saves data in variables for one Credit Card and one Smart Card before search
	* @param browser - The web browser object
	* @returns void
	**/
	"test3SearchBannedCreditCard: Saving Banned Cards Information" : function (browser) {
		var firstCC = "//p[contains(text(), 'Credit Card')]//parent::*//parent::*";
		var firstCCNum = firstCC + "//div[@class='col1']//p";
		var firstCCExp = firstCC + "//div[@class='col3']//p";
		var firstSCNum = "//p[contains(text(), 'Smart Card')]//parent::*//parent::*//div[@class='col1']//p";
		browser
		.useXpath()
		.waitForElementVisible(firstCCNum, 1000)
		.waitForElementVisible(firstCCExp, 1000)
		.waitForElementVisible(firstSCNum, 1000)
		// get the whole censored credit card number and get last four numbers to search because search function does not take in the censored X's 
		.getText(firstCCNum, function (result) {
			savedCCNum = result.value.trim();
			savedLastDigitsCC = result.value.substring(12, 16);
		})
		// get credit card expiry date to assert later it has appeared
		.getText(firstCCExp, function (result) {
			savedCCExp = result.value.trim();
		})
		// get smart card number to assert later that it has not appeared
		.getText(firstSCNum, function (result) {
			savedSCNum = result.value.trim();
		})
		.pause(1000);
	},
	

	/**
	 * @description Enters the first Credit Card number into search field
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test3SearchBannedCreditCard: Search Banned Credit Card Information" : function (browser) {
		var cardNumberField = "//input[@id='filterCardNumber']"
		browser
		.useXpath()
		.waitForElementVisible(cardNumberField, 1000)
		.click(cardNumberField)
		.setValue(cardNumberField, savedLastDigitsCC)
		// click the search button
		.click("//a[contains(text(), 'Search')]")
		.pause(2000);
	},
	
	/**
	 * @description Verifies correct Credit Card information after searching
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test3SearchBannedCreditCard: Verify Searched Credit Card Information" : function (browser) {
		var bannedCard = "//p[contains(text(), 'Credit Card')]//parent::*//parent::*";
		browser
		.useXpath()
		.assert.visible(bannedCard)
		// assert card number is visible
		.assert.visible(bannedCard + "//p[contains(text(),'" + savedCCNum + "')]")
		// assert card type is visible
		.assert.visible(bannedCard + "//p[contains(text(),'Credit Card')]")
		// assert expiry date is visible
		.assert.visible(bannedCard + "//p[contains(text(),'" + savedCCExp + "')]")
		// assert entry source is visible
		.assert.visible(bannedCard + "//p[contains(text(),'manual')]")
		.pause(1000);

	},
	
	/**
	 * @description Verifies Smart Card visibility depending if numbers match Credit Card numbers after searching 
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test3SearchBannedCreditCard: Verify Smart Card Visibility Depending on Credit Card" : function (browser) {
		var bannedCard = "//p[contains(text(), 'Smart Card')]//parent::*//parent::*";
		// indexOf() is used because there is no contains method in JavaScript
		if (savedSCNum.indexOf(savedLastDigitsCC) !== -1) {
			// if SC numbers match with the last 4 CC numbers then assert SC visible
			browser
			.useXpath()
			.assert.visible(bannedCard + "//p[contains(text(),'" + savedSCNum + "')]")
			.pause(1000);
		} else {
			// if SC numbers do not match with the last 4 CC numbers then assert not present
			browser
			.useXpath()
			.assert.elementNotPresent(bannedCard + "//p[contains(text(),'" + savedSCNum + "')]")
			.pause(1000);
		}
	},

	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test3SearchBannedCreditCard: Logout" : function (browser) {
		browser.logout();
	},
};
