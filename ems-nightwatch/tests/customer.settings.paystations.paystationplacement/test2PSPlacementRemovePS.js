/**
 * @summary Child Customer: Settings: Pay Station: Pay Station
 * @since 7.3.4
 * @version 1.0
 * @author Thomas C,
 * @requires test1PSPlacementAddPS
 */
 
var data = require('../../data.js');

var devurl = data("URL");
var username = data("ChildUserName2");
var password = data("Password");
var serialNum = new String();

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser -  The web browser object
	 * @returns void
	 */
	"test2PSPlacementRemovePS: Log In User" : function(browser) {
		browser.url(devurl);
		browser.windowMaximize('current');
		browser.login(username, password);
		},

	/**
	 * @description Navigates to Settings
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test2PSPlacementRemovePS: Click on Settings in the Sidebar" : function(browser) {
		var settings = "//section[@class='bottomSection']/a[@title='Settings']";
		browser.useXpath();
		browser.waitForElementVisible(settings, 3000);
		browser.click(settings);
		browser.pause(1000);
	},

	/**
	 * @description Navigates to Pay Stations
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test2PSPlacementRemovePS: Click on Pay Stations tab" : function(browser) {
		var payStations = "//a[@title='Pay Stations']";
		browser.useXpath();
		browser.waitForElementVisible(payStations, 3000);
		browser.click(payStations);
		browser.pause(3000);
	},

	/**
	 * @description Navigates to Pay Station Placement
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test2PSPlacementRemovePS: Click on Pay Stations Placement" : function(browser) {
		var payStations = "//a[@title='Pay Station Placement']";
		browser.useXpath()
		browser.waitForElementVisible(payStations, 3000);
		browser.click(payStations);
		browser.pause(1000);
	},

	/**
	 * @description Click Remove Button
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test2PSPlacementRemovePS: Remove Pay Station" : function(browser) {
		var placedPayStation = "//ul[@id='placedList']//li[@class='placed ui-draggable']";
		var pinnedPayStation = "//ul[@id='placedList']//li[@class='ui-draggable pin2map']";
		var editButton = placedPayStation + "/a[@class='menuListButton edit']";
		var removeButton = pinnedPayStation + "/a[@class='menuListButton close']";
		var map = "//section[@id='map']";
				
		browser.useXpath();
		
		browser.waitForElementVisible(placedPayStation, 5000);
		browser.assert.visible(placedPayStation);
		
		//set serial number to placed PS
		browser.getText(placedPayStation, function(result) {
			serialNum = result.value;
		});

		browser.assert.visible(editButton);
		browser.click(editButton);
		
		browser.pause(3000);
		
		browser.waitForElementVisible(removeButton, 3000);
		browser.assert.visible(removeButton);
		browser.click(removeButton);
		browser.pause(3000);
	},

	/**
	 * @description Confirm remove warning
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test2PSPlacementRemovePS: Confirm Remove" : function(browser) {
		var remove = "//span[text()='Remove']";
		browser.useXpath();
		browser.waitForElementVisible(remove, 3000);
		browser.assert.visible(remove);
		browser.click(remove);
		browser.pause(3000);
	},
	
	/**
	 * @description Verify pay station Removed
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test2PSPlacementRemovePS: Verify Removed" : function(browser) {
		var removedPS = "//ul[@id='unplacedListArea']//li[text()='"+ serialNum + "']";
		browser.useXpath();
		browser.waitForElementVisible(removedPS, 3000);
		browser.assert.visible(removedPS);
	},
	
	/**
	 * @description Logs the user out of Iris
	 * @param browser -  The web browser object
	 * @returns void
	 */
	"test2PSPlacementRemovePS: Log Out" : function(browser) {
		browser.logout();
	},
	
}
