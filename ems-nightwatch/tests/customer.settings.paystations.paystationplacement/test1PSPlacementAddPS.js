/**
 * @summary Child Customer: Settings: Pay Station: Pay Station
 * @since 7.3.4
 * @version 1.0
 * @author Thomas C,
 */

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ChildUserName2");
var password = data("Password");
var serialNum = new String();
var unplacedPayStation = "//li[@class='unplaced ui-draggable']";
var map = "//section[@id='map']";
var xOffset = 0;
var yOffset = 0;
var psXOffset = 0;
var psYOffset = 0; 

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser -  The web browser object
	 * @returns void
	 */
	"test1PSPlacementAddPS: Log In User" : function(browser) {
		browser.url(devurl);
		browser.windowMaximize('current');
		browser.login(username, password);
		},

	/**
	 * @description Navigates to Settings
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test1PSPlacementAddPS: Click on Settings in the Sidebar" : function(browser) {
		var settings = "//section[@class='bottomSection']/a[@title='Settings']";
		browser.useXpath();
		browser.waitForElementVisible(settings, 3000);
		browser.click(settings);
		browser.pause(3000);
	},

	/**
	 * @description Navigates to Pay Stations
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test1PSPlacementAddPS: Click on Pay Stations tab" : function(browser) {
		var payStations = "//a[@title='Pay Stations']";
		browser.useXpath();
		browser.waitForElementVisible(payStations, 3000);
		browser.click(payStations);
		browser.pause(3000);
	},

	/**
	 * @description Navigates to Pay Station Placement
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test1PSPlacementAddPS: Click on Pay Stations Placement" : function(browser) {
		var payStations = "//a[@title='Pay Station Placement']";
		browser.useXpath()
		browser.waitForElementVisible(payStations, 3000);
		browser.click(payStations);
		browser.pause(3000);
	},

	/**
	 * @description Gets size of map in pixels 
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test1PSPlacementAddPS: Get Map Details" : function(browser) {
		browser.useXpath();
		
		browser.waitForElementVisible(map, 10000);
		browser.assert.visible(map);
		
		//get the size in px of map and calc offset
		browser.getElementSize(map, function(result){
			xOffset = Math.floor((Math.random() * result.value.width) + 1);
			yOffset = Math.floor((Math.random() * result.value.height) + 1);
		});	
		browser.pause(3000);
},

	/**
	 * @description Gets Serial Number from PS
	 * @param browser - The web browser object
	 * @returns void
	 */
"test1PSPlacementAddPS: Get Pay Station Serial Number" : function(browser) {
	browser.useXpath();
		
	browser.waitForElementVisible(unplacedPayStation, 3000);
	browser.assert.visible(unplacedPayStation);

	//set serial number to placed PS
	browser.getText(unplacedPayStation, function(result) {
		serialNum = result.value;
	});
	
},

	/**
	 * @description Gets size in pixels of li for dragAndDrop offset
	 * @param browser - The web browser object
	 * @returns void
	 */
"test1PSPlacementAddPS: Get Pay Station List Item Size" : function(browser) {
	browser.useXpath();
		
	browser.waitForElementVisible(unplacedPayStation, 3000);
	browser.assert.visible(unplacedPayStation);
	
	//get PS size
	browser.getElementSize(unplacedPayStation, function(result){
				
		psXOffset = Math.floor(result.value.width / 2);
		psYOffset = Math.floor(result.value.height / 2);
	});
	browser.pause(3000);
},

	/**
	 * @description Drag and Drop PS onto map
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test1PSPlacementAddPS: Drag and Drop" : function(browser) {
		browser.dragAndDrop(unplacedPayStation, psXOffset, psYOffset, map, xOffset, yOffset);
		browser.pause(3000);
		
	},

	/**
	 * @description Verify pay station placed
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test1PSPlacementAddPS: Verify Placement" : function(browser) {
		var pinnedPS = "//li[@class='ui-draggable pined2map']";
		var saveButton = "//li[@class='ui-draggable pined2map']//a[@title='Save']";
		var mappedPS = "//ul[@id='placedListArea']//li[text()='"+ serialNum + "']";
		
		browser.waitForElementVisible(pinnedPS, 3000);
		browser.assert.visible(pinnedPS);
		
		browser.pause(3000);
		browser.waitForElementVisible(saveButton, 3000);
		browser.assert.visible(saveButton);
		
		browser.click(saveButton);
		browser.pause(3000);
		
		browser.waitForElementNotPresent(saveButton, 3000);
		
		browser.waitForElementVisible(mappedPS, 4000);
		browser.assert.visible(mappedPS);
	},
	
	/**
	 * @description Logs the user out of Iris
	 * @param browser -  The web browser object
	 * @returns void
	 */
	"test1PSPlacementAddPS: Log Out" : function(browser) {
		browser.logout();
	},
	
	
}
