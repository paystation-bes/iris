/**
 * @summary Users: Edits a parent child user
 * @since 7.3.6
 * @version 1.1
 * @author Thomas C
 * @requires AddParentChildRole
 * @requires AddParentParentRole
 * @requires AddParentChildUser
 * @requires EditParentChildRole
 * @requires EditParentParentRole
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ParentUserName");
var password = data("Password");

var firstName = "Smoke";
var lastName = "Test";
var email = data("SmokeEmail");
var password = data("Password");
var password2 = data("Password2");
var childEmail = "qa1child";
var fullName = firstName + " " + lastName;

var editName = "SmokeEdited";
var editLastName = "TestEdited";
var editEmail = "EDITED" + data("SmokeEmail");

var editedChildRoleName = data("EditedChildRole");
var editedParentRoleName = data("EditedParentRole");

var fullNameEdit = editName + " " + editLastName;
var childRole = "Administrator";
var parentRole = "Corporate Administrator";

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 *@description Logs the user into Iris
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test07EditParentChildUser: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	 *@description Navigates to Users
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test07EditParentChildUser: Navigates to Users" : function (browser) {
		browser
		.navigateTo("Settings")
		.navigateTo("Users")
		.pause(1000)
	},

	/**
	 *@description Clicks on Option menu
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test07EditParentChildUser: Click Options Menu" : function (browser) {
		var optionsMenu = "//li[@class='actv-true']/span[contains(text(),'" + fullName + "')]/../a[@title='Option Menu']";
		browser
		.useXpath()
		.waitForElementVisible(optionsMenu, 2000)
		.click(optionsMenu)
	},

	/**
	 *@description Clicks on Edit menu
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test07EditParentChildUser: Click Edit Button" : function (browser) {
		var editButton = "//li[@class='actv-true']//span[contains(text(),'" + fullName + "')]/following::a[text()='Edit'][1]";
		browser
		.useXpath()
		.assert.visible(editButton)
		.click(editButton)
	},

	/**
	 *@description Edits user details
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test07EditParentChildUser: Edit Details" : function (browser) {
		var firstNameInput = "//input[@id='formUserFirstName']";
		var lastNameInput = "//input[@id='formUserLastName']";
		var emailInput = "//input[@id='formUserName']";
		browser
		.useXpath()
		.waitForElementVisible(firstNameInput, 2000)
		.clearValue(firstNameInput)
		.setValue(firstNameInput, editName)
		.pause(250)
		.clearValue(lastNameInput)
		.setValue(lastNameInput, editLastName)
		.pause(250)
		.clearValue(emailInput)
		.setValue(emailInput, editEmail)
	},

	/**
	 *@description Adds new roles
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test07EditParentChildUser: Add New Roles" : function (browser) {

		var childRole = "//ul[@id='userAllRoles']/li[contains(text(),'" + editedChildRoleName + "')]";
		var selectedChildRole = "//ul[@id='userSelectedRoles']/li[contains(text(),'" + editedChildRoleName + "')]";
		var parentRole1 = "//ul[@id='userAllRoles']/li[contains(text(),'" + editedParentRoleName + "')]";
		var selectedParentRole = "//ul[@id='userSelectedRoles']/li[contains(text(),'" + editedParentRoleName + "')]";
		browser
		.useXpath()
		.waitForElementVisible(childRole, 2000)
		.click(childRole)
		.waitForElementVisible(selectedChildRole, 2000)
		.pause(1000)
		.waitForElementVisible(parentRole1, 2000)
		.click(parentRole1)
		.waitForElementVisible(selectedParentRole, 2000)
	},

	/**
	 *@description Remove old Admin Roles
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test07EditParentChildUser: Remove Old Roles" : function (browser) {
		var childXpath = "//ul[@id='userAllRoles']//li[contains(text(),'" + childRole + "') and contains(text(), 'Child')]";
		var selectedChildXpath = "//ul[@id='userSelectedRoles']//li[contains(text(),'" + childRole + "') and contains(text(), 'Child')]";
		var parentXpath = "//ul[@id='userAllRoles']//li[contains(text(),'" + parentRole + "') and contains(text(), 'Parent')]";
		var selectedParentXpath = "//ul[@id='userSelectedRoles']//li[contains(text(),'" + parentRole + "') and contains(text(), 'Parent')]";
		browser
		.useXpath()
		.waitForElementVisible(selectedChildXpath, 2000)
		.click(selectedChildXpath)
		.waitForElementVisible(childXpath, 2000)
		.waitForElementVisible(selectedParentXpath, 2000)
		.click(selectedParentXpath)
		.waitForElementVisible(parentXpath, 2000)
		.pause(2000)
	},

	/**
	 *@description Saves edited user
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test07EditParentChildUser: Click Save" : function (browser) {
		var xpath = "//span[text()='" + fullNameEdit + "']";
		browser
		.useXpath()
		.assert.visible("//a[@class='linkButtonFtr textButton save']")
		.click("//a[@class='linkButtonFtr textButton save']")
		.assert.elementNotPresent("//li[text()='User with the same Email Address already exists, please try a different Email Address.']")
		.pause(1000)
	},

	/**
	 *@description Verifies that the edit was successful
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test07EditParentChildUser: Verify Details" : function (browser) {
		var name = "//dd[@id='detailUserDisplayName' and contains(text(),'" + fullNameEdit + "')]";
		var status = "//dd[@id='detailUserStatus' and text()='Enabled']";
		var uname = "//dd[@id='detailUserName' and contains(text(),'" + editEmail + "')]";
		var addedChildRole = "//ul[@id='roleChildList']/li[contains(text(),'" + editedChildRoleName + "')]";
		var addedParentRole = "//ul[@id='roleChildList']/li[contains(text(),'" + editedParentRoleName + "')]";
		var removedParentRole = "//ul[@id='roleChildList']/li[contains(text(),'" + parentRole + "')]";
		var removedChildRole = "//ul[@id='roleChildList']/li[contains(text(),'" + childRole + "')]";
		browser
		.useXpath()
		.pause(1000)
		.waitForElementVisible(name, 1000)
		.assert.visible(status)
		.assert.visible(uname)
		.assert.visible(addedChildRole)
		.assert.visible(addedParentRole)
		.assert.elementNotPresent(removedParentRole)
		.assert.elementNotPresent(removedChildRole)

	},

	/**
	 *@description logs out of Iris
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test07EditParentChildUser: Logout" : function (browser) {
		browser.logout();
	},
};
