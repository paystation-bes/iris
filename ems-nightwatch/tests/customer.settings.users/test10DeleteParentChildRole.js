/**
 * @summary Users: Edits a parent child user
 * @since 7.3.6
 * @version 1.1
 * @author Thomas C
 * @requires AddParentChildRole
 * @requires AddParentParentRole
 * @requires AddParentChildUser
 * @requires EditParentChildRole
 * @requires EditParentParentRole
 * @requires EditParentChildUser
 * @requires DeleteParentChildUser
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ParentUserName");
var password = data("Password");
var childRole = data("ChildRole");
var parentRole = data("ParentRole");
var editedChildRoleName = data("EditedChildRole");
var editedParentRoleName = data("EditedParentRole");

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 *@description Logs the user into Iris
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10DeleteParentChildRole : Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	 *@description Navigates to Roles
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10DeleteParentChildRole : Navigate to Roles" : function (browser) {
		browser
		.navigateTo("Settings")
		.navigateTo("Users")
		.navigateTo("Roles")
	},

	/**
	 *@description Opens child role's options menu
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10DeleteParentChildRole : Click Child Role Options Button" : function (browser) {
		var xpath = "//li/span[contains(text(),'" + editedChildRoleName + "')]/../a[@title='Option Menu']";
		browser
		.useXpath()
		.waitForElementVisible(xpath, 1000)
		.click(xpath)
	},

	/**
	 *@description Clicks on child role's delete button
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10DeleteParentChildRole : Click Delete Child Role" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='menuOptions innerBorder']/a[text()='Delete']", 2000)
		.click("//section[@class='menuOptions innerBorder']/a[text()='Delete']")
	},

	/**
	 *@description Confirms  the deletion of child role
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10DeleteParentChildRole : Confirm Delete Child Role" : function (browser) {
		var xpath = "//li/span[contains(text(),'" + editedChildRoleName + "')]";
		browser
		.useXpath()
		.waitForElementVisible(" //span[text()='Delete']", 2000)
		.click("//span[text()='Delete']")
		.pause(2000)
		.assert.elementNotPresent(xpath)
	},

	/**
	 *@description Opens parent role's options menu
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10DeleteParentChildRole : Click Parent Role Options Button" : function (browser) {
		var xpath = "//li/span[contains(text(),'" + editedParentRoleName + "')]/../a[@title='Option Menu']";
		browser
		.useXpath()
		.waitForElementVisible(xpath, 1000)
		.click(xpath)
	},
	/**
	 *@description Clicks on parent role's delete button
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10DeleteParentChildRole : Click Delete Parent Role" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='menuOptions innerBorder']/a[text()='Delete']", 2000)
		.click("//section[@class='menuOptions innerBorder']/a[text()='Delete']")
	},

	/**
	 *@description Confirms  the deletion of parent role
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10DeleteParentChildRole : Confirm Delete Parent Role" : function (browser) {
		var xpath = "//li/span[contains(text(),'" + editedParentRoleName + "')]";
		browser
		.useXpath()
		.waitForElementVisible(" //span[text()='Delete']", 2000)
		.click("//span[text()='Delete']")
		.pause(2000)
		.assert.elementNotPresent(xpath)
	},

	/**
	 *@description Logs out of Iris
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10DeleteParentChildRole : Logout" : function (browser) {
		browser.logout();
	},

};
