/**
 * @summary Users: Adds a new Parent Company 
 * @since 7.3
 * @version 1.1
 * @author Kianoush N, Thomas C
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var tempPassword = data("Password2")
var id = Math.floor((Math.random() * 1000) + 1);
var roleName = "SmokeTestParentRole";
var userName = "SmokeTest@parent";
var newPassword = password;

module.exports = {
	tags: ['completetesttag'],
	/**
	 *@description Logs the user into Iris
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01AddNewParentPassword: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - System Administration');
	},
	/**
	 *@description Creates a parent customer
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01AddNewParentPassword: Create a parent customer" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("id('btnAddNewCustomer')", 2000)
		.click("id('btnAddNewCustomer')")
		.waitForElementVisible("id('parentFlagCheck')", 2000)
		.click("id('parentFlagCheck')")
		.setValue("id('formCustomerName')", roleName)
		.click("id('accountStatus:enabled')")
		.setValue("id('formUserName')", userName)
		.setValue("id('newPassword')", tempPassword)
		.setValue("id('c_newPassword')", tempPassword)
		.click("(//button[@type='button'])[2]")
		.pause(2000)
		.waitForElementVisible("//a[@title='Logout']", 2000)
        .click("//a[@title='Logout']")
        .pause(3000);
	},
	
	/**
	 *@description Logs in with the new parent
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01AddNewParentPassword: Login with new parent" : function (browser) {
		browser
		.login(userName, tempPassword)
		.assert.title('Digital Iris - Service Agreement');
	},
	
	/**
	 *@description Sccepts the T&C
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01AddNewParentPassword: Accept terms and conditions" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("id('serviceAgreementArea')", 2000)
		.click("id('saDetails')")
		.keys(browser.Keys.END)
		.keys(browser.Keys.NULL)
		.setValue("id('name')", 'QA Parent')
		.setValue("id('title')", 'QA')
		.setValue("id('organization')", 'T2')
		.click("id('saAccept')")
		.pause(2000);
	},
	
	/**
	 *@description Sets the new password
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01AddNewParentPassword: Set the new password" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("id('newPassword')", 2000)
		.setValue("id('newPassword')", newPassword)
		.setValue("id('c_newPassword')", newPassword)
		.click("id('changePassword')")
		.pause(1000)
		.assert.title('Digital Iris - Main (Dashboard)')
		},
		
		/**
	 *@description Logs out of Iris 
	 *@param browser - The web browser object
	 *@returns void
	 **/
		"test01AddNewParentPassword: Logout" : function (browser) {
		browser.logout();
	},
};