/**
 * @summary Users: Edits a parent child user
 * @since 7.3.6
 * @version 1.1
 * @author Thomas C
 * @requires AddParentChildRole
 * @requires AddParentParentRole
 * @requires AddParentChildUser
 * @requires EditParentChildRole
 * @requires EditParentParentRole
 * @requires EditParentChildUser
 **/
 
var data = require('../../data.js');

var devurl = data("URL");
var username = "EDITED" + data("SmokeEmail");
var password = data("Password");
var password2 = data("Password2")

	module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 *@description Logs the user into Iris
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test08VerifyParentChildUserData: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, password2)
	},

	/**
	 *@description Switches to the child company
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test08VerifyParentChildUserData: Switch to Child Company" : function (browser) {
		browser
		.useXpath()
		.assert.elementNotPresent("//a[@title='Reports']")
		.waitForElementVisible("//input[@id='childCoSwitchAC']", 2000)
		.click("//input[@id='childCoSwitchAC']")
		.pause(2000)
		.waitForElementVisible("//ul/li[@class='ui-menu-item']/a[text()='qa1child']", 2000)
		.click("//ul/li[@class='ui-menu-item']/a[text()='qa1child']")
		.pause(2000)
		.assert.visible("//a[@id='custSwitchBtn']")
		.click("//a[@id='custSwitchBtn']")
		.pause(2000)
	},

	/**
	 *@description Navigates to settings
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test08VerifyParentChildUserData: Navigate to Settings" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.assert.elementNotPresent("//a[@title='Reports']")
		.assert.elementNotPresent("//a[@title='Maintenance']")

		.assert.visible("//a[@title='Settings']")
		.assert.visible("//a[@title='Collections']")
		.assert.visible("//a[@title='Card Management']")
		.assert.visible("//a[@title='Accounts']")
		.assert.visible("//a[@title='Dashboard']")

		.navigateTo("Settings")

		.assert.elementNotPresent("//a[@title='Alerts']")
		.assert.visible("//a[@title='Global']")
		.assert.visible("//a[@title='Rates']")
		.assert.visible("//a[@title='Locations']")
		.assert.visible("//a[@title='Pay Stations']")
		.assert.visible("//a[@title='Mobile Devices']")
		.assert.visible("//a[@title='Routes']")
		.assert.visible("//a[@title='Users']")
		.assert.visible("//a[@title='Card Settings']")
	},

	/**
	 *@description Switches back to the parent company
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test08VerifyParentChildUserData: Switch back to Parent Company" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//input[@id='childCoSwitchAC']", 2000)
		.click("//input[@id='childCoSwitchAC']")
		.waitForElementVisible("//ul/li[@class='ui-menu-item']/a[text()='qa1parent']", 2000)
		.click("//ul/li[@class='ui-menu-item']/a[text()='qa1parent']")
		.assert.visible("//a[@id='custSwitchBtn']")
		.click("//a[@id='custSwitchBtn']")
	},

	/**
	 *@description Verifies that the parent has the correct permissions
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test08VerifyParentChildUserData: Verify Parent Data" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.assert.elementNotPresent("//a[@title='Reports']")

	},

	/**
	 *@description Logs the user out of Iris
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test08VerifyParentChildUserData: Logout" : function (browser) {
		browser.logout();
	},

};
