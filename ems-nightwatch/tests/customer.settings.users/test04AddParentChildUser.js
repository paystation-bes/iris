/**
 * @summary Users: Add User to Parent Customer account with Parent and Child Role
 * @since 7.3.6
 * @version 1.1
 * @author Thomas C
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ParentUserName");
var password = data("Password");
var password2 = data("Password2");
var firstName = "Smoke";
var lastName = "Test";
var email = data("SmokeEmail");
var childEmail = "qa1child";
var childRole = "Administrator";
var parentRole = "Corporate Administrator";
var fullName = firstName + " " + lastName;

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 *@description Logs the user into Iris
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test04AddParentChildUser: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	 *@description Navigates to Users
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test04AddParentChildUser: Navigates to Users" : function (browser) {
		browser
		.navigateTo("Settings")
		.navigateTo("Users")
	},

	/**
	 *@description Initiates add user
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test04AddParentChildUser: Click on '+' button" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@id='btnAddUser']", 2000)
		.click("//a[@id='btnAddUser']")
	},

	/**
	 *@description Enters user details
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test04AddParentChildUser: Enter Details" : function (browser) {
		browser
		.enterUserDetials(firstName, lastName, email, password2, true)
	},

	/**
	 *@description Adds parent and child roles to user
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test04AddParentChildUser: Select Roles" : function (browser) {
		var childXpath = "//ul[@id='userAllRoles']//li[contains(text(),'" + childRole + "')]//em[contains(text(), 'Child')]";
		var selectedChildXpath = "//ul[@id='userSelectedRoles']//li[contains(text(),'" + childRole + "')]//em[contains(text(), 'Child')]";
		var parentXpath = "//ul[@id='userAllRoles']//li[contains(text(),'" + parentRole + "')]//em[contains(text(), 'Parent')]";
		var selectedParentXpath = "//ul[@id='userSelectedRoles']//li[contains(text(),'" + parentRole + "')]//em[contains(text(), 'Parent')]";
		browser
		.useXpath()
		.waitForElementVisible(childXpath, 3000)
		.click(childXpath)
		.waitForElementVisible(selectedChildXpath, 3000)
		.assert.visible(parentXpath)
		.click(parentXpath)
		.waitForElementVisible(selectedParentXpath, 3000)
		.pause(1000)
	},

	/**
	 *@description Adds child account permissions to user
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test04AddParentChildUser: Select Child Account" : function (browser) {
		var xpath = "//li[contains(text(),'" + childEmail + "')]";
		browser
		.useXpath()
		.waitForElementVisible("//ul[@id='companyAllRoles']", 3000)
		.waitForElementVisible(xpath, 3000)
		.click(xpath)
		.pause(2000)
	},

	/**
	 *@description Saves new user
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test04AddParentChildUser: Click Save" : function (browser) {
		var xpath = "//span[text()='" + fullName + "']";
		browser
		.useXpath()
		.assert.visible("//a[@class='linkButtonFtr textButton save']")
		.click("//a[@class='linkButtonFtr textButton save']")
		.assert.elementNotPresent("//li[text()='User with the same Email Address already exists, please try a different Email Address.']")
		.waitForElementVisible(xpath, 3000)
	},

	/**
	 *@description Verifies details of user
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test04AddParentChildUser: Verify Details" : function (browser) {
		var xpath = "//span[text()='" + fullName + "']";
		var name = "//dd[@id='detailUserDisplayName' and text()='" + fullName + "']";
		var status = "//dd[@id='detailUserStatus' and text()='Enabled']";
		var uname = "//dd[@id='detailUserName' and text()='" + email + "']";
		var selectedChildRole = "//ul[@id='roleChildList']/li[contains(text(),'" + childRole + "')]";
		var selectedParentRole = "//ul[@id='roleChildList']/li[contains(text(),'" + parentRole + "')]";
		browser
		.useXpath()
		.pause(1000)
		.click(xpath)
		.waitForElementVisible(name, 3000)
		.assert.visible(status)
		.assert.visible(uname)
		.assert.visible(selectedChildRole)
		.assert.visible(selectedParentRole)
	},

	/**
	 *@description Logs out of Iris
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test04AddParentChildUser: Logout" : function (browser) {
		browser.logout();
	},
};
