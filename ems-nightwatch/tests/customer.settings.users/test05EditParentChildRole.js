/**
 * @summary Users: Edits a parent child role
 * @since 7.3.6
 * @version 1.1
 * @author Thomas C
 * @requires test02AddParentChildRole
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ParentUserName");
var password = data("Password");
var childRole = data("ChildRole");
var parentRole = data("ParentRole");
var editedChildRoleName = data("EditedChildRole");
var editedParentRoleName = data("EditedParentRole");

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 *@description Logs the user into Iris
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test05EditParentChildRole: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	 *@description Navigates to Roles
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test05EditParentChildRole: Navigate to Roles" : function (browser) {
		browser
		.navigateTo("Settings")
		.navigateTo("Users")
		.navigateTo("Roles")
	},

	/**
	 *@description Opens options menu
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test05EditParentChildRole: Click Options Button" : function (browser) {
		var optionsButton = "//li/span[contains(text(),'" + childRole + "')]/../a[@title='Option Menu']";
		browser
		.useXpath()
		.waitForElementVisible(optionsButton, 2000)
		.click(optionsButton)
	},

	/**
	 *@description Opens the edit menu
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test05EditParentChildRole: Click Edit" : function (browser) {
		var editButton = "//li[contains(span, '" + childRole + "')]/following::a[text()='Edit'][1]"
			browser
			.useXpath()
			.waitForElementVisible(editButton, 2000)
			.click(editButton)
	},

	/**
	 *@description Changes the Role Name
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test05EditParentChildRole: Edit Parent Role Name" : function (browser) {
		var roleNameInput = "//input[@id='formRoleName']";
		browser
		.useXpath()
		.waitForElementVisible(roleNameInput, 1000)
		.clearValue(roleNameInput)
		.setValue(roleNameInput, editedChildRoleName)
	},

	/**
	 *@description Removes some permissions from the role
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test05EditParentChildRole: Edit Child Role Permissions" : function (browser) {
		var permissions = "//ul[@id='childPermissionList']/li[@class='Child']";
		var manageReports = "//ul[@id='childPermissionList']/li[@class='Child']//label[contains(text(), 'Manage Reports')]";
		var permitLookup = "//ul[@id='childPermissionList']/li[@class='Child']//label[contains(text(), 'Search for Transactions in Permit Lookup')]";
		var viewAlerts = "//ul[@id='childPermissionList']/li[@class='Child']//label[contains(text(), 'View Current Alerts')]";
		var manageAlerts = "//ul[@id='childPermissionList']/li[@class='Child']//label[contains(text(), 'Manage User Defined Alerts')]";

		browser
		.useXpath()
		browser
		.waitForElementVisible(manageReports, 2000)
		.click(manageReports)
		.pause(2000)
		.waitForElementVisible(permitLookup, 2000)
		.click(permitLookup)
		.pause(2000)
		.waitForElementVisible(viewAlerts, 2000)
		.click(viewAlerts)
		.pause(2000)
		.waitForElementVisible(manageAlerts, 2000)
		.click(manageAlerts)

	},

	/**
	 *@description Saves the Role
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test05EditParentChildRole: Click Save" : function (browser) {
		var saveBtn = "//section[@class='btnSet']/a[text()='Save']";
		var error = "//li[contains(text(),'Role with the same Role Name already exists')]";
		browser
		.useXpath()
		.assert.visible(saveBtn)
		.click(saveBtn)
		.pause(1000)
		.assert.elementNotPresent(error)
		.pause(1000)
	},

	/**
	 *@description Verifies that the edit was successful
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test05EditParentChildRole: Verify Details" : function (browser) {
		var xpath = "//li/span[contains(text(),'" + editedChildRoleName + "')]";
		var roleName = "//dd[@id='detailRoleName']";
		var noUsers = "//ul[@id='userChildList']/li[text()='No users currently assigned.']";
		browser
		.useXpath()
		.pause(1000)
		.waitForElementVisible(xpath, 3000)
		.waitForElementVisible(roleName, 2000)
		.assert.visible(roleName)
		.assert.elementNotPresent("//ul[@id='detailPermissionList']/li[contains(text(),'Manage Reports')]")
		.assert.elementNotPresent("//ul[@id='detailPermissionList']/li[contains(text(),'Search for Transactions in Permit Lookup')]")
		.assert.elementNotPresent("//ul[@id='detailPermissionList']/li[contains(text(),'View Current Alerts')]")
		.assert.elementNotPresent("//ul[@id='detailPermissionList']/li[contains(text(),'Manage User Defined Alerts')]")
		.assert.visible(noUsers)
	},

	/**
	 *@description Logs out of Iris
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test05EditParentChildRole: Logout" : function (browser) {
		browser.logout();
	},

};
