/**
 * @summary Users: Edits a parent child user
 * @since 7.3.6
 * @version 1.1
 * @author Thomas C
 * @requires AddParentChildRole
 * @requires AddParentParentRole
 * @requires AddParentChildUser
 * @requires EditParentChildRole
 * @requires EditParentParentRole
 * @requires EditParentChildUser
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ParentUserName");
var password = data("Password");
var password2 = data("Password2");
var firstName = "SmokeEdited";
var lastName = "TestEdited";
var email = "EDITED" + data("SmokeEmail");
var childEmail = "qa1child";
var childRole = "Administrator";
var parentRole = "Corporate Administrator";
var fullName = firstName + " " + lastName;

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 *@description Logs the user into Iris
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test09DeleteParentChildUser: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	 *@description Navigates to Users
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test09DeleteParentChildUser: Navigate to Users " : function (browser) {
		browser
		.navigateTo("Settings")
		.navigateTo("Users")
	},

	/**
	 *@description Clicks on Option menu
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test09DeleteParentChildUser: Click Options Menu" : function (browser) {
		var optionsMenu = "//li[@class='actv-true']/span[contains(text(),'" + fullName + "')]/../a[@title='Option Menu']";
		browser
		.useXpath()
		.waitForElementVisible(optionsMenu, 2000)
		.click(optionsMenu)
	},

	/**
	 *@description Clicks on Delete menu
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test09DeleteParentChildUser: Click Delete Button" : function (browser) {
		var editButton = "//li[@class='actv-true']//span[contains(text(),'" + fullName + "')]/following::a[text()='Delete'][1]";
		browser
		.useXpath()
		.assert.visible(editButton)
		.click(editButton)
	},

	/**
	 *@description Confirms deletion of user
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test09DeleteParentChildUser: Confirm Delete" : function (browser) {
		var user = "//span[text()='" + fullName + "']";
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='innerBorder']/article[contains(text(),'Are you sure you would like to delete this User?')]", 2000)
		.click("//span[text()='Delete']")
		.waitForElementNotVisible(user, 2000)
		.assert.elementNotPresent(user)
	},

	/**
	 *@description Logs out of Iris
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test09DeleteParentChildUser: Logout" : function (browser) {
		browser.logout();
	},

};
