/**
 * @summary Users: Edits a parent parent role
 * @since 7.3.6
 * @version 1.1
 * @author Thomas C
 * @requires test03AddParentParentRole
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ParentUserName");
var password = data("Password");
var childRole = data("ChildRole");
var parentRole = data("ParentRole");
var editedChildRoleName = data("EditedChildRole");
var editedParentRoleName = data("EditedParentRole");

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 *@description Logs the user into Iris
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test06EditParentParentRole: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	 *@description Navigates to Roles
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test06EditParentParentRole: Navigate to Roles" : function (browser) {
		browser
		.navigateTo("Settings")
		.navigateTo("Users")
		.navigateTo("Roles")
	},

	/**
	 *@description Opens options menu
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test06EditParentParentRole: Click Options Button" : function (browser) {
		var optionsButton = "//li/span[contains(text(),'" + parentRole + "')]/../a[@title='Option Menu']";
		browser
		.useXpath()
		.waitForElementVisible(optionsButton, 2000)
		.click(optionsButton)
	},

	/**
	 *@description Opens the edit menu
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test06EditParentParentRole: Click Edit" : function (browser) {
		var editButton = "//li[contains(span, '" + parentRole + "')]/following::a[text()='Edit'][1]"
			browser
			.useXpath()
			.waitForElementVisible(editButton, 2000)
			.click(editButton)
	},

	/**
	 *@description Changes the Role Name
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test06EditParentParentRole: Edit Parent Role Name" : function (browser) {
		var roleNameInput = "//input[@id='formRoleName']";
		browser
		.useXpath()
		.waitForElementVisible(roleNameInput, 1000)
		.clearValue(roleNameInput)
		.setValue(roleNameInput, editedParentRoleName)
	},

	/**
	 *@description Removes some permissions from the role
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test06EditParentParentRole: Edit Parent Role Permissions" : function (browser) {

		var permissionHeader = "//section[@id='childPermissionHeader']";
		var manageReports = "//ul[@id='parentPermissionList']/li[@class='Child']//label[contains(text(), 'Manage Reports')]"

			browser
			.useXpath()
			browser
			.waitForElementVisible(manageReports, 2000)
			.click(manageReports)
			.pause(2000)
	},

	/**
	 *@description Saves the Role
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test06EditParentParentRole: Click Save" : function (browser) {
		var saveBtn = "//section[@class='btnSet']/a[text()='Save']";
		var error = "//li[contains(text(),'Role with the same Role Name already exists')]";
		browser
		.useXpath()
		.assert.visible(saveBtn)
		.click(saveBtn)
		.pause(1000)
		.assert.elementNotPresent(error)
		.pause(1000)
	},

	/**
	 *@description Verifies that the edit was successful
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test06EditParentParentRole: Verify Details" : function (browser) {
		var xpath = "//li/span[contains(text(),'" + editedParentRoleName + "')]";
		var roleName = "//dd[@id='detailRoleName']";
		var noUsers = "//ul[@id='userChildList']/li[text()='No users currently assigned.']";
		browser
		.useXpath()
		.pause(2000)

		.waitForElementVisible(xpath, 3000)
		.waitForElementVisible(roleName, 2000)
		.assert.visible(roleName)
		.assert.elementNotPresent("//ul[@id='detailPermissionList']/li[contains(text(),'Manage Reports')]")
		.pause(1000)
		.assert.visible(noUsers)
	},

	/**
	 *@description Logs out of Iris
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test06EditParentParentRole: Logout" : function (browser) {
		browser.logout();
	},

};
