/*
Prerequisite Scripts:
test1AddPassCard.js
*/

var data = require('../../data.js');

var expectedCardInfo = ["123456789123456", "Blackboard External Srvr", "testing"];
var cardElemenets = ["dtlCardNumber", "dtlCardType", "dtlComment"];

var devurl = data("URL");
var username = data("ChildUserName");
var password = data("Password");


module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)')
	},
	
	"Navigate to Accounts" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("img[alt='Accounts']", 2000)
		.click("img[alt='Accounts']")
	},
	
	"Navigate to Passcards" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[contains(text(),'Passcards')]", 2000)
		.click("//a[contains(text(),'Passcards')]")
		.pause(1000)
	},
	
	"Choose and Delete a Passcard" : function (browser) {
		browser
		.useXpath()
		.click("//p[contains(text(),'123456789123456')]/../../a")
		.pause(1000)
		.click("//p[contains(text(),'123456789123456')]/../../following-sibling::section[1]/section/a[@class='delete']")
		.pause(1000)
		.click("//span[contains(text(), 'Ok')]")
		.pause(3000)
		.assert.elementNotPresent("//p[contains(text(),'123456789123456')]")
	},
	
	
	"Logout" : function (browser) {
		browser.logout();
	},
};