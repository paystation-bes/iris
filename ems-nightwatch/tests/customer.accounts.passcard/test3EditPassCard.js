/*
Prerequisite Scripts:
test1AddPassCard.js
*/

var data = require('../../data.js');

var expectedCardInfo = ["123456789123456", "Blackboard External Srvr", "testing"];
var cardElemenets = ["dtlCardNumber", "dtlCardType", "dtlComment"];

var devurl = data("URL");
var username = data("ChildUserName");
var password = data("Password");


module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)')
	},
	
	"Navigate to Accounts" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("img[alt='Accounts']", 2000)
		.click("img[alt='Accounts']")
	},
	
	"Navigate to Passcards" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[contains(text(),'Passcards')]", 2000)
		.click("//a[contains(text(),'Passcards')]")
		.pause(1000)
	},
	
	"Choose and Edit a Passcard" : function (browser) {
		browser
		.useXpath()
		.click("(//a[contains(@href, '#')])[12]")
		.useCss()
		.click("section.menuOptions.innerBorder > a.edit")
		.pause(1000)
	},
	
	"Save a Passcard" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//textarea[@id='formComment']", 2000)
		.setValue("//textarea[@id='formComment']", "testing")
		.useCss()
		.click(".linkButtonFtr.textButton.save")
		.pause(2000)
		for (i=0; i<expectedCardInfo.length; i++) {
			browser.assert.containsText("dd[name='" + cardElemenets[i] + "']", expectedCardInfo[i]);
		}
        browser.pause(1000);
	},
	
	"Logout" : function (browser) {
		browser.logout();
	},
};