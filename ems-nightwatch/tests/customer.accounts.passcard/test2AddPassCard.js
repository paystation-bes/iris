/*
Prerequisite Scripts:
AddCardType.js
 */

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ChildUserName");
var password = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	"Entering Accounts" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//nav[@id='main']//a[@title='Accounts']", 3000)
		.click("//nav[@id='main']//a[@title='Accounts']")
		.pause(1000);
	},
	
	"Click on Passcards" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[contains(text(),'Passcards')]", 3000)
		.click("//a[contains(text(),'Passcards')]")
		.pause(1000);
	},
	
	"Click Add Pass Card" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@id='opBtn_cardMenu']", 3000)
		.click("//a[@id='opBtn_cardMenu']")
		.waitForElementVisible("//a[contains(text(),'Add')]", 3000)
		.click("//a[contains(text(),'Add')]")
		.pause(1000);
	},
	
	"Entering PassCard Info" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("id('formCardNumber')", 3000)
		.click("id('formCardNumber')")
		.setValue("id('formCardNumber')", '123456789123456')
		.click("//input[@id='formCardType']")
		.waitForElementVisible("//input[@id='formCardType']", 3000)
		.click("//a[contains(text(),'Blackboard External Srvr')]")
		.pause(1000);
		},
	
	"Save" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("(//a[contains(text(),'Save')])[2]", 2000)
		.click("(//a[contains(text(),'Save')])[2]")
		.pause(1000)
		.waitForElementVisible("//p[contains(.,'123456789123456')]",3000)
		.pause(1000);
		},
		
			"Logout" : function (browser) {
		browser.logout();
	},
};