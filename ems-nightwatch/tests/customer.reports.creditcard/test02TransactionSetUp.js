/**
* @summary Reports: Credit Card Payment Mode: Send Transactions including new payment types
* @since 7.5
* @version 1.0
* @author Lonney McD
* @see EMS-10440
* @requires: test01MerchantSetUp
* @required by: test03ReportGenerateCSV
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ChildUserName");
var password = data("Password");

var PSSerial = data("PaystationCommAddress3");
var CardNumber = data("CardNumber");
	
module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
     * @description Logs the user into Iris
     * @param browser - The web browser object
     * @returns void
     **/
	"Test02TransactionSetUp: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password");
	},
	
	/**
	 *@description Places CC (swipe) transaction
	 *@param browser - The web browser object
	 *@returns Pre-auth, Post-auth
	 **/
	"Test02TransactionSetUp: Custom Command places a CC (swipe) transaction to pay station" : function (browser) 
	{
		
		 browser.sendCCPreAuthRFID("4.00", CardNumber, PSSerial, "0", function (result) {
			var CCTrans = {
				licenceNum : 'AAA111',
				stallNum : '',
				type : 'Regular',
				originalAmount : '400',
				chargedAmount: '4.00',
				cardPaid : '4.00',
				cashPaid : '0.00',
				cardAuthorizationId: result.authID,
				emsPreAuthId: result.emsPreAuthID,
			}
			browser.pause(1000);
			browser.sendCashCCPostAuth([CCTrans], PSSerial);
		});
		
	},	
	
	/**
	 *@description Places CC (tap) transaction
	 *@param browser - The web browser object
	 *@returns Pre-auth, Post-auth
	 **/
	"Test02TransactionSetUp: Custom Command places a CC (tap) transaction to pay station" : function (browser) 
	{
		
		 browser.sendCCPreAuthRFID("4.00", CardNumber, PSSerial, "1", function (result) {
			var CCTrans = {
				licenceNum : 'AAA222',
				stallNum : '',
				type : 'Regular',
				originalAmount : '400',
				chargedAmount: '4.00',
				cardPaid : '4.00',
				cashPaid : '0.00',
				cardAuthorizationId: result.authID,
				emsPreAuthId: result.emsPreAuthID,
			}
			
			browser.sendCashCCPostAuth([CCTrans], PSSerial);
		});
		
	},	
	
	/**
	 *@description Places Cash/CC (swipe) transaction
	 *@param browser - The web browser object
	 *@returns Pre-auth, Post-auth
	 **/
	"Test02TransactionSetUp: Custom Command places a Cash/CC (swipe) transaction to pay station" : function (browser)
	{
		
		 browser.sendCCPreAuthRFID("4.00", CardNumber, PSSerial, "0", function (result) {
			var CCTrans = {
				stallNum : '1',
				type : 'Regular',
				originalAmount : '400',
				chargedAmount: '4.00',
				cashPaid : '2.00',
				cardPaid : '2.00',
				cardAuthorizationId: result.authID,
				emsPreAuthId: result.emsPreAuthID,
			}
   
			browser.sendCashCCPostAuth([CCTrans], PSSerial);
		});
		
	},
	
	/**
	 *@description Places Cash/CC (tap) transaction
	 *@param browser - The web browser object
	 *@returns Pre-auth, Post-auth
	 **/
	"Test02TransactionSetUp: Custom Command places a Cash/CC (tap) transaction to pay station" : function (browser)
	{
		
		 browser.sendCCPreAuthRFID("4.00", CardNumber, PSSerial, "1", function (result) {
			var CCTrans = {
				stallNum : '2',
				type : 'Regular',
				originalAmount : '400',
				chargedAmount: '4.00',
				cashPaid : '2.00',
				cardPaid : '2.00',
				cardAuthorizationId: result.authID,
				emsPreAuthId: result.emsPreAuthID,
			}
   
			browser.sendCashCCPostAuth([CCTrans], PSSerial);
		});
		
	},
	/**
     * @description Logout
     * @param browser - The web browser object
     * @returns void
     **/
	"Test02TransactionSetUp: Logout" : function (browser) {
		browser
		.logout();
	},

};	
