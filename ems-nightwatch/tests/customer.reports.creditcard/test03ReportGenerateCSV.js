/**
* @summary Reports: Credit Card Payment Mode: Create and review Transaction All - CSV for new payment types
* @since 7.5
* @version 1.0
* @author Lonney McD, Paul B
* @see EMS-10440
* @requires: test01MerchantSetUp, test02TransactionSetUp
* @required by: 
**/

var data = require('../../data.js');
require('assert');
var devurl = data("URL");
var username = data("ChildUserName");
var password = data("Password");
var auditInfoToken = "";
var unzippedName = "";

module.exports = {

	tags: ['featuretesttag', 'completetesttag'],
	
	/**
     * @description Logs the user into Iris
     * @param browser - The web browser object
     * @returns void
     **/
	"Test03ReportGenerateCSV: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password");
	},
	
	/**
     * @description Click on Reports in the Sidebar
     * @param browser - The web browser object
     * @returns void
     **/
	"Test03ReportGenerateCSV: Click on Reports in the Sidebar" : function (browser) {
		browser
		.pause(3000)
		.navigateTo("Reports")
		.pause(3000);
	},
	
	/**
     * @description Click on Create Report
     * @param browser - The web browser object
     * @returns void
     **/
	"Test03ReportGenerateCSV: Click on Create Report" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@id='btnAddScheduleReport']", 2000)
		.click("//a[@id='btnAddScheduleReport']")
		.pause(1000);
	},
	
	/**
     * @description Click on the dropdown for Report Type
     * @param browser - The web browser object
     * @returns void
     **/
	"Test03ReportGenerateCSV: Click on the dropdown for Report Type" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//input[@id='reportTypeID']", 2000)
		.click("//input[@id='reportTypeID']")
		.pause(1000);
	},
	
	/**
     * @description Choose Transaction - All
     * @param browser - The web browser object
     * @returns void
     **/
	"Test03ReportGenerateCSV: Choose Transaction - All" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//li[@class='ui-menu-item']/a[text()='Transaction - All']", 2000)
		.click("//li[@class='ui-menu-item']/a[text()='Transaction - All']")
		.pause(1000);
	},
	
	/**
     * @description Click on the Next Step button
     * @param browser - The web browser object
     * @returns void
     **/
	"Test03ReportGenerateCSV: Click on the Next Step button" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@id='btnStep2Select']", 2000)
		.click("//a[@id='btnStep2Select']")
		.pause(1000);
	},
	
	/**
     * @description Click the Details radio button
     * @param browser - The web browser object
     * @returns void
     **/
	"Test03ReportGenerateCSV: Click the Details radio button" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@id='isSummary:false']", 2000)
		.click("//a[@id='isSummary:false']")
		.pause(1000);
	},
	
	/**
     * @description Click the CSV radio button
     * @param browser - The web browser object
     * @returns void
     **/
	"Test03ReportGenerateCSV: Click the CSV radio button" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@id='isPDF:false']", 2000)
		.click("//a[@id='isPDF:false']")
		.pause(1000);
	},
	
	/**
     * @description Click the NEXT STEP button
     * @param browser - The web browser object
     * @returns void
     **/
	"Test03ReportGenerateCSV: Click the NEXT STEP button" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@id='btnStep3Select']", 2000)
		.click("//a[@id='btnStep3Select']")
		.pause(1000);
	},
	
	/**
     * @description Click the SAVE button
     * @param browser - The web browser object
     * @returns void
     **/
	"Test03ReportGenerateCSV: Click the SAVE button" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@class='linkButtonFtr textButton save']", 2000)
		.click("//a[@class='linkButtonFtr textButton save']")
		.pause(2000)
		.waitForReport("Transaction - All");
	},
	
	/**
     * @description Download the ZIP
     * @param browser - The web browser object
     * @returns zip file of report
     **/
	"Test03ReportGenerateCSV: Download the ZIP" : function (browser) {
		browser
		.downloadZip("reports", devurl, "EMS10440CSVReport", __dirname, "Transaction - All");
	},
	
		
	/**
     * @description Unzip the CSV and take the content
     * @param browser - The web browser object
     * @returns content of report as array
     **/
	"ReportExportCSV: Unzip the CSV and take the content" : function (browser) {
		browser
		.unzipAndParseCSV("EMS10440CSVReport.zip", __dirname, function (fileName, result) {
			unzippedName = fileName;
			console.log("unzippedName" + unzippedName);
			console.log("result" + result);
			var rows = result;
			var creditCardSwipe = false;
			var creditCardTap = false;
			var cashCCSwipe = false;
			var cashCCTap = false;
			
			//Comparison code here 
			for (var i=0; i<rows.length; i++){
				for (var k=0; k<rows[i].length; k++){
					if (rows[i][k]=="CC (Swipe)"){
						creditCardSwipe = true;
					}
					
					if (rows[i][k]=="CC (Tap)"){
						creditCardTap = true;
					}
					if (rows[i][k]=="Cash/CC (Swipe)"){
						cashCCSwipe = true;
					}
					if (rows[i][k]=="Cash/CC (Tap)"){
						cashCCTap = true;
					}
				}
			}
			browser.assert.strictEqual(creditCardSwipe,true,"CC (Swipe) found " + creditCardSwipe);
			browser.assert.strictEqual(creditCardTap,true,"CC (Tap) found " + creditCardTap);
			browser.assert.strictEqual(cashCCSwipe,true,"Cash/CC (Swipe) found " + cashCCSwipe);
			browser.assert.strictEqual(cashCCTap,true,"Cash/CC (Tap) found " + cashCCTap);

			browser.deleteFile(unzippedName, __dirname);
			browser.deleteFile("EMS10440CSVReport.zip", __dirname);
		});
		
	},
	
	/**
     * @description Logout
     * @param browser - The web browser object
     * @returns void
     **/
	"Test03ReportGenerateCSV: Logout" : function (browser) {
		browser
		.logout();
	},
};
