/**
* @summary Reports: Credit Card Payment Mode: Create and review Transaction All - PDF for new payment types
* @since 7.5
* @version 1.0
* @author Lonney McD, Paul B
* @see EMS-10440
* @requires: test01MerchantSetUp, test02TransactionSetUp
* @required by: 
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ChildUserName");
var password = data("Password");

var auditInfoToken = "";

module.exports = {

	tags: ['featuretesttag', 'completetesttag'],
	
	/**
     * @description Logs the user into Iris
     * @param browser - The web browser object
     * @returns void
     **/
	"Test04ReportGeneratePDF: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
	},
	
	/**
     * @description Click on Reports in the Sidebar
     * @param browser - The web browser object
     * @returns void
     **/
	"Test04ReportGeneratePDF: Click on Reports in the Sidebar" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//nav[@id='main']/section[@class='topSection']/a[@title='Reports']/img", 2000)
		.click("//nav[@id='main']/section[@class='topSection']/a[@title='Reports']/img")
		.pause(1000)
	},
	
	/**
     * @description Click on Create Report
     * @param browser - The web browser object
     * @returns void
     **/
	"Test04ReportGeneratePDF: Click on Create Report" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@id='btnAddScheduleReport']", 2000)
		.click("//a[@id='btnAddScheduleReport']")
		.pause(1000)
	},
	
	/**
     * @description Click on the dropdown for Report Type
     * @param browser - The web browser object
     * @returns void
     **/
	"Test04ReportGeneratePDF: Click on the dropdown for Report Type" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//input[@id='reportTypeID']", 2000)
		.click("//input[@id='reportTypeID']")
		.pause(1000)
	},
	
	/**
     * @description Choose Transaction - All
     * @param browser - The web browser object
     * @returns void
     **/
	"Test04ReportGeneratePDF: Choose Transaction - All" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//li[@class='ui-menu-item']/a[text()='Transaction - All']", 2000)
		.click("//li[@class='ui-menu-item']/a[text()='Transaction - All']")
		.pause(1000)
	},
	
	/**
     * @description Click on the Next Step button
     * @param browser - The web browser object
     * @returns void
     **/
	"Test04ReportGeneratePDF: Click on the Next Step button" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@id='btnStep2Select']", 2000)
		.click("//a[@id='btnStep2Select']")
		.pause(1000)
	},
	
	/**
     * @description Click the Details radio button
     * @param browser - The web browser object
     * @returns void
     **/
	"Test04ReportGeneratePDF: Click the Details radio button" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@id='isSummary:false']", 2000)
		.click("//a[@id='isSummary:false']")
		.pause(1000)
	},
	
	/**
     * @description Click the PDF radio button
     * @param browser - The web browser object
     * @returns void
     **/
	"Test04ReportGeneratePDF: Click the PDF radio button" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@id='isPDF:true']", 2000)
		.click("//a[@id='isPDF:true']")
		.pause(1000)
	},
	
	/**
     * @description Click the NEXT STEP button
     * @param browser - The web browser object
     * @returns void
     **/
	"Test04ReportGeneratePDF: Click the NEXT STEP button" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@id='btnStep3Select']", 2000)
		.click("//a[@id='btnStep3Select']")
		.pause(1000)
	},
	
	/**
     * @description Click the SAVE button
     * @param browser - The web browser object
     * @returns void
     **/
	"Test04ReportGeneratePDF: Click the SAVE button" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@class='linkButtonFtr textButton save']", 2000)
		.click("//a[@class='linkButtonFtr textButton save']")
		.pause(2000)
		.waitForReport("Transaction - All")
	},
	
	/**
     * @description Download the PDF
     * @param browser - The web browser object
     * @returns void
     **/
	"Test04ReportGeneratePDF: Download the PDF" : function (browser) {
		browser
		.downloadPDF("reports", devurl, "EMS10440PDFReport", __dirname, "Transaction - All");
	},

	/**
     * @description Assert that the downloaded file contains new Payment Types
     * @param browser - The web browser object
     * @returns void
     **/
	"Test04ReportGeneratePDF: Assert that the downloaded file contains new Payment Types" : function (browser) {
		browser
		.assertPDFTextPresent("EMS10440PDFReport.pdf", __dirname, ["CC (Swipe)"])
		.assertPDFTextPresent("EMS10440PDFReport.pdf", __dirname, ["CC (Tap)"])
		.assertPDFTextPresent("EMS10440PDFReport.pdf", __dirname, ["Cash/CC (Swipe)"])
		.assertPDFTextPresent("EMS10440PDFReport.pdf", __dirname, ["CC (Tap)"])
	},

	/**
     * @description Delete the PDF file for cleanup
     * @param browser - The web browser object
     * @returns void
     **/
	"Test04ReportGeneratePDF: Delete the PDF file for cleanup" : function (browser) {
		browser
		.deleteFile("EMS10440PDFReport.pdf", __dirname)
	},
	
	/**
     * @description Logout
     * @param browser - The web browser object
     * @returns void
     **/
	"Test04ReportGeneratePDF: Logout" : function (browser) {
		browser
		.logout()
	},
};
