/**
* @summary Reports: Credit Card Payment Mode: Set up Merchant Account and Pay Station
* @since 7.5
* @version 1.0
* @author Lonney McD
* @see EMS-10440
* @requires: 
* @required by: test02TransactionSetUp, test03ReportGenerateCSV
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var pass = data("Password");
var defaultPass= data("DefaultPassword");
var custName = data("ChildCustomer");
var PSSerial = data("PaystationCommAddress3");
var CardNumber = data("CardNumber");


module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	 *@description Logs into System Admin account
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01MerchantSetUp: Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.systemAdminLogin(username, pass, defaultPass)
			.assert.title('Digital Iris - System Administration');
	},		
			
	/**
	 *@description Selects QA1Child Customer
	 *@param browser - The web browser object
	 *@returns void
	 **/	
		
	"test01MerchantSetUp: Search for and select Customer" : function (browser)
	{browser
			.useXpath()
			.waitForElementVisible("//input[@id='search']", 2000)
			.setValue("//input[@id='search']",custName)
			.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'"+ custName + "')]", 2000)
			.click("//a[@class='ui-corner-all'][contains(text(),'" + custName + "')]")
			.click("//a[@id='btnGo']")
			.pause(1000);
	},
	
	/**
	 *@description Verifies QA1Child Customer
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01MerchantSetUp: Verify Customer Details" : function (browser)
	{
		browser
			.useXpath()
			.assert.visible("//h1[@class][contains(text(),'"+custName+"')]")
			.pause(1000)
	},
	
	/**
	 *@description Opens 'Add Merchant Account' dialog
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01MerchantSetUp: Opens 'Add Merchant Account' dialog" : function (browser)
	{
		browser
			.useCss()
			.waitForElementVisible("#btnAddMrchntAccnt", 4000)
			.click("#btnAddMrchntAccnt")
	},
	
	/**
	 *@description Sets Merchant Account Criteria
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01MerchantSetUp: enters values for all merchant account fields" : function (browser) 
	{
			browser
			.useCss()
			.waitForElementVisible("#account", 2000)
			.setValue("#account", "Paymentech")
			.assert.visible("#statusCheck")
			.click("#statusCheck")
			.assert.visible("#processorsExpand")
			.click("#processorsExpand")
			.pause(1000)
			
			.useXpath()
			.assert.visible("//a[contains(text(),'Paymentech')]")
			.click("//a[contains(text(),'Paymentech')]")
			.pause(2000)
			
			.useCss()
			.waitForElementVisible("#field1", 2000)
			.setValue("#field1", "700000002018")
			.assert.visible("#field2")
			.setValue("#field2", "001")
			.assert.visible("#field3")
			.setValue("#field3", "digitech1")
			.assert.visible("#field4")
			.setValue("#field4", "digpaytec01")
			.assert.visible("#field5")
			.setValue("#field5", "0002")		
			.useXpath()
			.assert.visible("//span[@class='ui-button-text'][contains(text(), 'Add Account')]")
			.click("//span[@class='ui-button-text'][contains(text(), 'Add Account')]")
			.pause(4000)
			
	},	
	
	
	/**
	 *@description Navigates to Paystation page
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01MerchantSetUp: Navigates to Pay Station page" : function (browser)
	{
		browser
			.useXpath()
			browser.navigateTo("Pay Stations");
			browser.pause(5000);			
	},
	
	/**
	 *@description Find first pay station
	 *@param browser - The web browser object
	 *@returns Paystation number
	 **/
	 "test01MerchantSetUp: Get the first Pay Station number" : function (browser) 
	{
		browser
			.useXpath()
			.getText("(//ul[@id='paystationList']//span[@class='textLine'])[1]", function(result) {
                PSNumber = result.value;
				console.log("Using Pay Station #: " + PSNumber);
            });
	},

	/**
	 *@description Selects to edit pay station
	 *@param browser - The web browser object
	 *@returns void
	 **/
	 "test01MerchantSetUp: Select edit button for Pay Station" : function (browser) 
	{
		browser
			.useXpath()
			.assert.visible("(//ul[@id='paystationList']//a[@class='menuListButton edit'])[1]")
			.click("(//ul[@id='paystationList']//a[@class='menuListButton edit'])[1]")
			.pause(2000)
			.useCss()
			.assert.visible("#editHeading")
	},

	/**
	 *@description Selects merchant account for pay station
	 *@param browser - The web browser object
	 *@returns void
	 **/
	 "test01MerchantSetUp: Selects Merchant Account for Pay Station, and saves update" : function (browser) 
	{
		browser
			.useCss()
			.assert.visible("#formCreditCardAccountExpand")
			.click("#formCreditCardAccountExpand")
			
			.useXpath()
			.assert.visible("//a[contains(text(),'Paymentech')]")
			.click("//a[contains(text(),'Paymentech')]")
			
			.useCss()
			.assert.visible(".linkButtonFtr.textButton.save")
			.click(".linkButtonFtr.textButton.save")
			.pause(4000)
	},

	/**
	 *@description Logs user out and closes browser
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01MerchantSetUp: Logout" : function (browser)
	{
		browser.logout();
	}
	
};	
