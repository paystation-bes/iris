/**
* @summary System Admin: Customer: Elavon KPI: EMS-9623
* @since 7.4.1
* @version 1.0
* @author Kianoush Nesvaderani
* @requires 
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");

var x = Math.floor((Math.random() * 20) + 1);


module.exports = {
	tags : ['completetesttag', 'featuretesttag'],
	/**
	*@description Logs the user into Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - System Administration');
	},
	
	/**
	*@description Navigates to Customer Account
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigates to Customer Account" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("#search", 3000)
		.setValue("#search", "oranj")
		.waitForElementVisible("span.info", 3000)
		.click("span.info")
		.click("#btnGo");
	},
	
	/**
	*@description Deactivates the Pay Station
	*@param browser - The web browser object
	*@returns void
	**/
	"Deactivate the Pay Station" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[contains(text(),'Pay Stations')]", 5000)
		.click("//a[contains(text(),'Pay Stations')]")
		.pause(3000)
		.click("//a[@id='500011116655']")
		.pause(3000)
		.useCss()
		.click("#multiEditBtn")
		.waitForElementVisible("#formCreditCardAccountExpand", 5000)
		.click("#formCreditCardAccountExpand")
		.useXpath()
		.click("//li/a[contains(text(),'Choose')]")
		.useCss()
		.click("#btnPayStationDeactivate")
		.click(".linkButtonFtr.textButton.save")
		.pause(5000);
	},
	
	/**
	*@description Deletes the Merch Account
	*@param browser - The web browser object
	*@returns void
	**/
	"Delete the Merch Account" : function (browser) {
		browser
		.useXpath()
		.click("//a[contains(text(),'Customer Details')]")
		.waitForElementVisible("//li/a", 5000)
		.click("//li/a")
		.useCss()
		.waitForElementVisible("a.delete", 5000)
		.click("a.delete")
		.useXpath()
		.click("//button/span[contains(text(),'Delete')]");
	},
	
	/**
	*@description Logs the user out of Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Logout" : function (browser) {
		browser.logout();
	},
};
