/**
* @summary System Admin: Customer: Elavon KPI: EMS-9623
* @since 7.4.1
* @version 1.0
* @author Kianoush Nesvaderani
* @requires 
**/

var data = require('../../data.js');

var devurl = data("URL");


module.exports = {
	tags : ['completetesttag', 'featuretesttag'],
	/**
	*@description Sends a pre-auth and post-auth to a Pay Station
	*@param browser - The web browser object
	*@returns void
	**/
	"Send Pre-Auth AND Post-Auth using Elavon" : function (browser) {
		browser.sendCCPreAuth("4.00", "4485660000000007", "500011116655", function (result) {
			var CCTrans = {
				licenceNum : '',
				stallNum : '1',
				type : 'Regular',
				originalAmount : '400',
				chargedAmount: '4.00',
				cardPaid : '4.00',
				cardAuthorizationId: result.authID,
				emsPreAuthId: result.emsPreAuthID,
			}
   
			browser.sendCCPostAuth([CCTrans], "500011116655");
		});
	},
	
	/**
	*@description Verifies the transaction shows up on KPI monitoring page
	*@param browser - The web browser object
	*@returns void
	**/
	"Assert the transaction update on KPI monitoring page" : function (browser) {
		browser
		.url(devurl + "/monitoring/KPI?name=")
		.pause(5000)
		.assert.containsText("pre", ".request.elavon.viaconex=1")
		.assert.containsText("pre", ".request.elavon.converge")
		.assert.containsText("pre", ".cardRetry.elavon.converge")
		.assert.containsText("pre", ".cardRetry.elavon.viaconex")
		.assert.containsText("pre", ".responseTime.elavon.converge")
		.assert.containsText("pre", ".responseTime.elavon.viaconex");
	},
	
	/**
	*@description Logs the user out of Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Logout" : function (browser) {
		browser.end();
	},
};
