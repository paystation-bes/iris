/**
* @summary System Admin: Customer: Elavon KPI: EMS-9623
* @since 7.4.1
* @version 1.0
* @author Kianoush Nesvaderani
* @requires 
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");

var x = Math.floor((Math.random() * 20) + 1);


module.exports = {
	tags : ['completetesttag', 'featuretesttag'],
	/**
	*@description Logs the user into Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - System Administration');
	},
	
	/**
	*@description Navigates to Customer Account
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigates to Customer Account" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("#search", 3000)
		.setValue("#search", "oranj")
		.waitForElementVisible("span.info", 3000)
		.click("span.info")
		.click("#btnGo");
	},
	
	/**
	*@description Adds a Merchant Account
	*@param browser - The web browser object
	*@returns void
	**/
	"Add a new merch account" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("#btnAddMrchntAccnt", 5000)
		.click("#btnAddMrchntAccnt")
		.waitForElementVisible("#account", 5000)
		.setValue("#account", "Elavon KPI")
		.click("#processorsExpand")
		.useXpath()
		.click("//a[contains(text(),'Elavon')]")
		.useCss()
		.click("#statusCheck")
		.setValue("#field1", "0017340009997772229556")
		.setValue("#field2", "0017340009997772229556")
		.setValue("#field3", x)
		.useXpath()
		.click("//button/span[contains(text(),'Add Account')]");
	},
	
	/**
	*@description Associates the Pay Station with the Merchant Account
	*@param browser - The web browser object
	*@returns void
	**/
	"Associates a pay station with the new merch account" : function (browser) {
		browser
		.useXpath()
		.pause(5000)
		.click("//a[contains(text(),'Pay Stations')]")
		.pause(3000)
		.click("//a[@id='500011116655']")
		.pause(3000)
		.useCss()
		.click("#multiEditBtn")
		.waitForElementVisible("#formCreditCardAccountExpand", 5000)
		.click("#formCreditCardAccountExpand")
		.useXpath()
		.click("//li/a[contains(text(),'Elavon KPI')]")
		.useCss()
		.click("#btnPayStationActivate")
		.click(".linkButtonFtr.textButton.save");
	},
	
	/**
	*@description Logs the user out of Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Logout" : function (browser) {
		browser.logout();
	},
};
