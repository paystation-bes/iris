/**
* @summary Settings: Pay Stations: EMV XML Update: EMS-10323
* @since 7.5
* @version 1.0
* @author Kianoush Nesvaderani
* @requires 
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");


module.exports = {
	tags : ['smoketag', 'completetesttag'],
	/**
	*@description Sends the EMV XML and Logs the user into Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Send EMV and Login" : function (browser) {
		browser
		.sendEMV('Update', '2', '500000070001', '{"FirmwareVersion4":"7.53","ViolationStatus":"GBOK"}')
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - System Administration');
	},
	
	/**
	*@description Navigates to Customer
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigates to Customer" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("#search", 5000)
		.click("#search")
		.setValue("#search", "oranj")
		.waitForElementVisible("span.info", 5000)
		.click("span.info")
		.click("#btnGo");
	},
	
	/**
	*@description Navigates to the Pay Station
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigates to Pay Station" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[contains(text(),'Pay Stations')]", 5000)
		.click("//a[contains(text(),'Pay Stations')]")
		.waitForElementVisible("//span[contains(text(),'500000070001')]", 5000)
		.click("//span[contains(text(),'500000070001')]")
		.useCss()
		.waitForElementVisible("#infoBtn > a.tab", 5000)
		.click("#infoBtn > a.tab")
		.waitForElementVisible("#emvTelemetry-Lbl", 5000)
		.click("#emvTelemetry-Lbl");
	},
	
	/**
	*@description Verifies the EMV link is updated
	*@param browser - The web browser object
	*@returns void
	**/
	"Assert the EMV Link" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("#emvTelemetry-Lst > dd.detailValue", 5000)
		.assert.containsText("#emvTelemetry-Lst > dd.detailValue", "7.53");
	},
	
	/**
	*@description Logs the user out of Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Logout" : function (browser) {
		browser.logout();
	},
	
};
