/**
* @summary Settings: Pay Stations: EMV Link: EMS-10316
* @since 7.5
* @version 1.0
* @author Kianoush Nesvaderani
* @requires 
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");


module.exports = {
	tags : ['smoketag', 'completetesttag'],
	/**
	*@description Logs the user into Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	/**
	*@description Navigates to the Pay Station
	*@param browser - The web browser object
	*@returns void
	**/
	"Navigates to Pay Station" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[contains(text(),'Settings')]", 5000)
		.click("//a[contains(text(),'Settings')]")
		.waitForElementVisible("//a[contains(text(),'Pay Stations')]", 5000)
		.click("//a[contains(text(),'Pay Stations')]")
		.waitForElementVisible("//span[contains(text(),'500000070001')]", 5000)
		.click("//span[contains(text(),'500000070001')]")
		.useCss()
		.waitForElementVisible("#infoBtn > a.tab", 5000)
		.click("#infoBtn > a.tab")
		.waitForElementVisible("#emvTelemetry-Lbl", 5000)
		.click("#emvTelemetry-Lbl");
	},
	
	/**
	*@description Verifies the EMV link is present
	*@param browser - The web browser object
	*@returns void
	**/
	"Assert the EMV Link" : function (browser) {
		browser
		.useCss()
		.waitForElementVisible("#emvTelemetry-Lst > dt.detailLabel", 5000)
		.assert.containsText("#emvTelemetry-Lst > dt.detailLabel", "Serial Number");
	},
	
	/**
	*@description Logs the user out of Iris
	*@param browser - The web browser object
	*@returns void
	**/
	"Logout" : function (browser) {
		browser.logout();
	},
};
