var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
var password2 = data("Password2");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";

module.exports = {
	tags: ['smoketag', 'completetesttag'],

	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	/*
	* Click on Settings
	*/	
	"Click on Settings" : function (browser)
	{
		browser
			.useXpath()
			.click("//a[contains(text(),'Settings')]")
			.pause(1000)
			.assert.title("Digital Iris - Settings - Global : Settings")
			.waitForFlex()


	},

	"Click on 'Users' Tab" : function (browser)
	{
		browser
			.useXpath()
			.assert.visible("//a[@class='tab' and text()='Users']")
			.click("//a[@class='tab' and text()='Users']")
			.pause(1000)
	},

	"Click Option Menu and Delete button for New User" : function (browser)
	{
		browser
		.useXpath()
		.assert.visible("//span[contains(text(),'AutoEditName AutomationLastName')]/../a[@title='Option Menu']")
		.click("//span[contains(text(),'AutoEditName AutomationLastName')]/../a[@title='Option Menu']")
		.pause(1000)
		.assert.visible("//span[contains(text(),'AutoEditName AutomationLastName')]/../following-sibling::section//a[@title='Delete']")
		.click("//span[contains(text(),'AutoEditName AutomationLastName')]/../following-sibling::section//a[@title='Delete']")
	},

	"Confirm Deletion" : function(browser)
	{
		browser
		.useCss()
		.assert.visible(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")
		.click(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")
		.pause(5000)
	},

	"Verify New User has been deleted from UserList" : function(browser)
	{
		browser
		.useXpath()
		.assert.elementNotPresent("//ul[@id='userList']/li/span[contains(text(),'AutoEditName AutomationLastName')]")
	},


	"Logout" : function (browser)
	{
		browser.logout();
	}

};
