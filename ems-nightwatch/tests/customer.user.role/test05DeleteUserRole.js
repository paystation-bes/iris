var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/*
	* Logs into Iris with System Admin Account
	*/
	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	/*
	* Click on Settings
	*/	
	"Click on Settings" : function (browser)
	{
		browser
			.useXpath()
			.click("//a[contains(text(),'Settings')]")
			.pause(1000)
			.assert.title("Digital Iris - Settings - Global : Settings")
			.waitForFlex()


	},

	"Click on 'Users' Tab" : function (browser)
	{
		browser
			.useXpath()
			.assert.visible("//a[@class='tab' and text()='Users']")
			.click("//a[@class='tab' and text()='Users']")
			.pause(1000)
	},

	"Click on 'Roles' Tab" : function(browser)
	{
		browser
		.useCss()
		.assert.visible(".tab[title='Roles']")
		.click(".tab[title='Roles']")
		.pause(3000)
		.assert.title("Digital Iris - Settings - Users : Roles")
		.pause(3000)
	},

	"Click Option and Delete buttons for Auto Role Reports" : function(browser)
	{
		browser
		.useXpath()
		.assert.visible("//ul[@id='roleList']/li/span[contains(text(),'Auto Only Reports')]/../a[@title='Option Menu']")
		.click("//ul[@id='roleList']/li/span[contains(text(),'Auto Only Reports')]/../a[@title='Option Menu']")
		.pause(1000)
		.assert.visible("//ul[@id='roleList']/li/span[contains(text(),'Auto Only Reports')]/../following-sibling::section[1]//a[@class='delete']")
		.click("//ul[@id='roleList']/li/span[contains(text(),'Auto Only Reports')]/../following-sibling::section[1]//a[@class='delete']")
		.pause(3000)
	},

	"Confirm Delete" : function(browser)
	{
		browser
		.useCss()
		.assert.visible(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.ui-state-focus.active")
		.click(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.ui-state-focus.active")
		.pause(6000)
	},

	"Verify Deletion of User Role" : function (browser)
	{
		browser
		.useXpath()
		.assert.elementNotPresent("//ul[@id='roleList']/li/span[contains(text(),'Auto Only Reports')]")

	},

	"Logout" : function (browser)
	{
		browser.logout();
	}

};


