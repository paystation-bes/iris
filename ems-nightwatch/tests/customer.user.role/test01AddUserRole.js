var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/*
	* Logs into Iris with System Admin Account
	*/
	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	/*
	* Click on Settings
	*/	
	"Click on Settings" : function (browser)
	{
		browser
			.useXpath()
			.click("//a[contains(text(),'Settings')]")
			.pause(1000)
			.assert.title("Digital Iris - Settings - Global : Settings")
			.waitForFlex()


	},

	"Click on 'Users' Tab" : function (browser)
	{
		browser
			.useXpath()
			.assert.visible("//a[@class='tab' and text()='Users']")
			.click("//a[@class='tab' and text()='Users']")
			.pause(1000)
	},

	"Click on 'Roles' Tab" : function(browser)
	{
		browser
		.useCss()
		.assert.visible(".tab[title='Roles']")
		.click(".tab[title='Roles']")
		.pause(3000)
		.assert.title("Digital Iris - Settings - Users : Roles")
	},

	"Click on Add New User Role" : function(browser)
	{
		browser
		.useCss()
		.assert.visible("#btnAddRole")
		.click("#btnAddRole")
		.pause(3000)
		.assert.visible("#roleDetails>header>#addHeading")

	},

	"Input Role Name" : function(browser)
	{
		browser
		.useCss()
		.assert.visible("#formRoleName")
		.click("#formRoleName")
		.setValue("#formRoleName", "Auto Only Reports")

	},

	"Set Permissions to only Manage Reports and Click Save" : function(browser)
	{
		browser
		.useXpath()
		.assert.visible("//label[contains(text(),'Manage Reports')]/a")
		.click("//label[contains(text(),'Manage Reports')]/a")
		.pause(1000)

		.useCss()
		.assert.elementPresent(".linkButtonFtr.textButton.save")
		.click(".linkButtonFtr.textButton.save")
		.pause(5000)

	},

	"Verify New Role in list" : function(browser)
	{
		browser
		.useXpath()
		.assert.elementPresent("//ul[@id='roleList']/li/span[contains(text(),'Auto Only Reports')]")

	},

	"Logout" : function(browser)
	{
		browser.logout();
	}


};