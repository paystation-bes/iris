var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/*
	* Logs into Iris with System Admin Account
	*/
	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	/*
	* Click on Settings
	*/	
	"Click on Settings" : function (browser)
	{
		browser
			.useXpath()
			.click("//a[contains(text(),'Settings')]")
			.pause(1000)
			.assert.title("Digital Iris - Settings - Global : Settings")
			.waitForFlex()


	},

	"Click on 'Users' Tab" : function (browser)
	{
		browser
			.useXpath()
			.assert.visible("//a[@class='tab' and text()='Users']")
			.click("//a[@class='tab' and text()='Users']")
			.pause(1000)
	},
	
	"Click '+'" : function (browser)
	{
		browser
			.useCss()
			.assert.visible("#btnAddUser")
			.click("#btnAddUser")
			.pause(3000)
			.assert.visible("#addHeading")
	},
	
	"Enter First Name" : function (browser)
	{
		browser
			.useCss()
			.assert.visible("#formUserFirstName")
			.setValue("#formUserFirstName", 'AutomationFirstName')
			.pause(1000)
	},
	
	"Enter Last Name" : function (browser)
	{
		browser
			.useCss()
			.assert.visible("#formUserLastName")
			.setValue("#formUserLastName", 'AutomationLastName')
			.pause(1000)
	},

	
	"Enter Invalid Email Address" : function (browser)
	{
		browser
			browser
			.useCss()
			.assert.visible("#formUserName")
			.setValue("#formUserName", 'Auto')
			.pause(1000)
	},
	
	"Set Invalid Temporary Password" : function (browser)
	{
		browser
			browser
			.useCss()
			.assert.visible("#newPassword")
			.setValue("#newPassword", 'pass')
			.pause(1000)
	},
	
	
	"Attempt to Save User" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='linkButtonFtr textButton save' and text()='Save']", 1000)
			.click("//a[@class='linkButtonFtr textButton save' and text()='Save']")
			.pause(5000)
	},

	"Verify Error Message" : function (browser)
	{
		browser
		.useXpath()
		.assert.elementPresent("//section[@id='messageResponseAlertBox']//li/strong[contains(text(),'Email Address')]")
		.assert.elementPresent("//section[@id='messageResponseAlertBox']//li/strong[contains(text(),'Confirm Password')]")
		.assert.elementPresent("//section[@id='messageResponseAlertBox']//li/strong[contains(text(),'New Password')]")
		.assert.elementPresent("//section[@id='messageResponseAlertBox']//li/strong[contains(text(),'Role')]")
		.assert.elementPresent("//section[@id='messageResponseAlertBox']//li[contains(text(),'Password is not complex.')]")

		.useCss()
		.assert.elementPresent(".close")
		.click(".close")
		.pause(3000)

	},

	"Set Valid Email Address" : function (browser)
	{
		browser
			.useCss()
			.assert.visible("#formUserName")
			.clearValue("#formUserName")
			.setValue("#formUserName", 'Automation@oranj')
			.pause(1000)
	},

	"Set Valid Password" : function(browser)
	{
			browser
			.useCss()
			.assert.visible("#newPassword")
			.clearValue("#newPassword")
			.setValue("#newPassword", 'Password$2')
			.pause(1000)
	},

	"Confirm Valid Password" : function(browser)
	{
		browser
		.useCss()
		.setValue("#c_newPassword", "Password$2")


	},

	"Select Role and click Save" : function(browser)
	{
		browser
		.useXpath()
		.assert.visible("//ul[@id='userAllRoles']/li[contains(text(),'Auto Only Reports')]")
		.click("//ul[@id='userAllRoles']/li[contains(text(),'Auto Only Reports')]")

		.useCss()
		.assert.elementPresent(".linkButtonFtr.textButton.save")
		.click(".linkButtonFtr.textButton.save")
		.pause(5000)
	},

	"Verify New User in UserList" : function (browser)
	{
		browser
		.useXpath()
		.assert.elementPresent("//ul[@id='userList']/li/span[contains(text(),'AutomationFirstName AutomationLastName')]")
		.click("//ul[@id='userList']/li/span[contains(text(),'AutomationFirstName AutomationLastName')]")
		.pause(3000)
	},

	"Verify Role Assignment for New User" : function (browser)
	{
		browser
		.useXpath()
		.assert.elementPresent("//ul[@id='roleChildList']/li[contains(text(), 'Auto Only Reports')]")
	},

	"Logout" : function (browser) 
	{
		browser.logout();
	},
	
	"Attempt to Login with User un-enabled" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login("Automation@oranj", "Password$2")
			.assert.title('Digital Iris - Login.')
			
	},
};