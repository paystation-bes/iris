var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");
var password2 = data("Password2");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";

module.exports = {
	tags: ['smoketag', 'completetesttag'],

	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	/*
	* Click on Settings
	*/	
	"Click on Settings" : function (browser)
	{
		browser
			.useXpath()
			.click("//a[contains(text(),'Settings')]")
			.pause(1000)
			.assert.title("Digital Iris - Settings - Global : Settings")
			.waitForFlex()


	},

	"Click on 'Users' Tab" : function (browser)
	{
		browser
			.useXpath()
			.assert.visible("//a[@class='tab' and text()='Users']")
			.click("//a[@class='tab' and text()='Users']")
			.pause(3000)
	},

	"Click Option Menu and Edit button for New User" : function (browser)
	{
		browser
		.useXpath()
		.assert.visible("//span[contains(text(),'AutomationFirstName AutomationLastName')]/../a[@title='Option Menu']")
		.click("//span[contains(text(),'AutomationFirstName AutomationLastName')]/../a[@title='Option Menu']")
		.pause(1000)
		.assert.visible("//span[contains(text(),'AutomationFirstName AutomationLastName')]/../following-sibling::section//a[@title='Edit']")
		.click("//span[contains(text(),'AutomationFirstName AutomationLastName')]/../following-sibling::section//a[@title='Edit']")
		.pause(5000)
		.useCss()
		.assert.visible("#editHeading")
	},

	"Change First Name" : function (browser)
	{
		browser
		.useCss()
		.assert.visible("#formUserFirstName")
		.click("#formUserFirstName")
		.clearValue("#formUserFirstName")
		.setValue("#formUserFirstName", "AutoEditName")

	},

	"Enable User and click Save" : function(browser)
	{
		browser
		.useCss()
		.assert.visible("#statusCheck")
		.click("#statusCheck")
		.pause(1000)
		.assert.elementPresent(".linkButtonFtr.textButton.save")
		.click(".linkButtonFtr.textButton.save")
		.pause(3000)
	},

	"Verify Name Change in UserList" : function(browser)
	{
		browser
		.useXpath()
		.assert.elementPresent("//ul[@id='userList']/li/span[contains(text(),'AutoEditName AutomationLastName')]")
	},


	"Logout" : function (browser) 
	{
		browser.logout();
	},

	"Login with New User" : function (browser)
	{
		browser
		.url(devurl)
		.windowMaximize('current')
		.login("Automation@oranj", password2)
		.pause(2000)
		.useXpath()

		.waitForElementPresent("//input[@id='newPassword']", 100000)
		.setValue("//input[@id='newPassword' and @class='left toolTip reqField']", password)
		.pause(1000)

		.waitForElementVisible("//input[@id='c_newPassword' and @class='left reqField']", 10000)
		.setValue("//input[@id='c_newPassword' and @class='left reqField']", password)
		.pause(1000)

		.waitForElementVisible("//input[@id='changePassword' and @class='linkButtonFtr textButton']", 10000)
		.click("//input[@id='changePassword' and @class='linkButtonFtr textButton']")
		.pause(1000)

		.assert.title('Digital Iris - Settings - Global : System Notifications');
	},

	"Verify that User can only Manage Reports" : function (browser)
	{
		browser
		.useCss()
		.assert.elementPresent("a[title='Reports']")
		.assert.elementPresent("a[title='Settings']")
		.assert.elementNotPresent("a[title='Dashboard']")
		.assert.elementNotPresent("a[title='Maintenance']")	
		.assert.elementNotPresent("a[title='Collections']")	
		.assert.elementNotPresent("a[title='Accounts']")	
		.assert.elementNotPresent("a[title='Card Management']")		
	},

	"Logout 2" : function(browser)
	{
		browser.logout();
	}
		
};