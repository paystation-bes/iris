/**
* @summary Web Services - Digital API: XChange - Enable and Assert Digital API: XChange 
* @since 7.4.3
* @version 1.0
* @author Paul Breland
* @see US1195/EMS-9621
* @requires Test2CreateEditDeleteTransactionDataWSDL.js to be run first
**/

var data = require('../../data.js');
var devurl = data("URL");
var systemAdminUser = data("SysAdminUserName");
var childUser = data("ChildUserName");
var password = data("Password");

module.exports = {
	tags: ['featuretesttag', 'completetesttag'],

	"Login to System Admin view" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(systemAdminUser, password)
			.assert.title('Digital Iris - System Administration');
	},	
	
	
	"Click on Customer/Pay Station Field" : function (browser)
	{
		browser
			.useXpath()
			.pause(2000)
			.waitForElementVisible("//input[@id='search' and @class='autoText acWidthSearch stndrdChrctr hiddenLabel ui-autocomplete-input' and @type='text']", 2000)
			.click("//input[@id='search' and @class='autoText acWidthSearch stndrdChrctr hiddenLabel ui-autocomplete-input' and @type='text']")
			.pause(1000);
	},
	
	"Search for Customer qa1child" : function (browser)
	{
		browser
			.useXpath()
			.setValue("//input[@id='search' and @class='autoText acWidthSearch stndrdChrctr hiddenLabel ui-autocomplete-input' and @type='text']", 'qa1child')
			.pause(2000)
			.waitForElementVisible("//a[@id='btnGo' and @class='linkButtonFtr textButton search']", 1000)
			.click("//a[@id='btnGo' and @class='linkButtonFtr textButton search']") 
			.pause(1000);
	},
	
	"Click the pencil button to edit Customer Details" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@id='btnEditCustomer']", 2000)
			.click("//a[@id='btnEditCustomer']")
			.pause(1000);
	},

	"Check if Digital API: XChange is checked and if so, click to uncheck it" : function (browser)
	{
		browser
			.useXpath()
			.getAttribute("//label[contains(., 'Digital API: XChange')]/a", "class", function(result){
				console.log(result.value);
				if (result.value.trim() === "checkBox checked") {
					browser
					.useXpath()
					.click("//label[contains(., 'Digital API: XChange')]/a")
					.pause(1000)
					.assert.elementNotPresent("//label[contains(., 'Digital API: XChange')]/a[contains(@class, 'checked')]");
				};
			})
			.pause(1000);
	},
	
	"Click the UPDATE CUSTOMER button to save the change" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//button/span[contains(., 'Update Customer')]", 2000)
			.click("//button/span[contains(., 'Update Customer')]")
			.pause(2000);
	},

	"Assert Digital API: XChange is grayed out in the Services list (tooltip will say Available rather than Activated)" : function (browser)
	{
		browser
			.useXpath()
			.assert.visible("//li[contains(@title, 'Digital API: XChange is Available')]", 2000)
			.pause(1000);
	},
		
	"Click on 'Licenses' Tab" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='tab' and @title='Licenses']", 2000)
			.click("//a[@class='tab' and @title='Licenses']")
			.pause(1000);
	},

	"Assert Digital API: XChange section is not displayed within the Digital API tab" : function (browser)
	{
		browser
			.useXpath()
			.assert.elementNotPresent("//h2[contains(., 'Digital API - XChange')]", 2000)
			.pause(1000);
	},

	"Logout of System Admin view" : function (browser)

	{
		browser
			.logout()
			.pause(1000);
	},

	"Login to Customer view" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(childUser, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},	
	
	"Click the Settings left side menu option" : function (browser)
	{
		browser
			.useXpath()
			.navigateTo("Settings")
			.pause(1000);
	},		

	"Assert Digital API: XChange section is not displayed under Digital API/Partner Integrations" : function (browser)
	{
		browser
			.useXpath()
			.assert.elementNotPresent("//h3[contains(., 'Digital API - XChange')]", 2000)
			.pause(1000);
	},

	"Logout of Customer view" : function (browser)

	{
		browser.logout();
	},

	
};	