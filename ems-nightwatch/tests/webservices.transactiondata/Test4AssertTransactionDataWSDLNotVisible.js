/**
* @summary Web Services - Digital API: XChange - Assert TransactionData not listed on the Services page 
* @since 7.4.3
* @version 1.0
* @author Paul Breland
* @see US1195/EMS-9621
* @requires n/a
**/

var data = require('../../data.js');
var devurl = data("URL");

module.exports = {
	tags: ['featuretesttag', 'completetesttag'],

	"Go to the Services page" : function (browser) 
	{
		browser	
			.url(devurl + "/services")
			.windowMaximize('current');
	},	

	"Assert TransactionData is not present on the page" : function (browser)
	{
		browser
			.useXpath()
			.assert.elementNotPresent("//body[contains(., 'TransactionData')]", 2000)
			.pause(1000)
			.end();
	},

};	