/**
* @summary Web Services - Create, Edit and Delete TransactionData WSDL 
* @since 7.4.3
* @version 1.0
* @author Paul Breland
* @see US1195/EMS-9621
* @requires Test1EnableAndAssertDigitalAPIXChange.js to be run first
**/

var data = require('../../data.js');
var devurl = data("URL");
var systemAdminUser = data("SysAdminUserName");
var childUser = data("ChildUserName");
var password = data("Password");

var transactionDataToken = "";

module.exports = {
	tags: ['featuretesttag', 'completetesttag'],

	"Login to System Admin to create the TransactionData WSDL" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(systemAdminUser, password)
			.assert.title('Digital Iris - System Administration');
	},	
	
	"Click on Customer/Pay Station Field (create WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.pause(2000)
			.waitForElementVisible("//input[@id='search' and @class='autoText acWidthSearch stndrdChrctr hiddenLabel ui-autocomplete-input' and @type='text']", 2000)
			.click("//input[@id='search' and @class='autoText acWidthSearch stndrdChrctr hiddenLabel ui-autocomplete-input' and @type='text']")
			.pause(1000);
	},
	
	"Search for Customer qa1child (create WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.setValue("//input[@id='search' and @class='autoText acWidthSearch stndrdChrctr hiddenLabel ui-autocomplete-input' and @type='text']", 'qa1child')
			.pause(2000)
			.waitForElementVisible("//a[@id='btnGo' and @class='linkButtonFtr textButton search']", 1000)
			.click("//a[@id='btnGo' and @class='linkButtonFtr textButton search']") 
			.pause(1000);
	},
	

	"Click on 'Licenses' Tab (create WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='tab' and @title='Licenses']", 2000)
			.click("//a[@class='tab' and @title='Licenses']")
			.pause(1000);
	},
	
	"Click on 'Digital API' Sub Tab (create WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@title='Digital API']", 2000)
			.click("//a[@title='Digital API']")
			.pause(1000);
	},
	
	"Click on the + button for Add Digital API - XChange (create WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@title='Add Digital API - XChange']", 2000)
			.click("//a[@title='Add Digital API - XChange']")
			.pause(1000);
	},
	
	"Click on the dropdown box for EndPoint Type" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//input[@id='soapType']", 2000)
			.click("//input[@id='soapType']")
			.pause(1000);
	},
	
	"Click on TransactionData for EndPoint Type" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//li[@class='ui-menu-item']/a[text()='TransactionData']", 2000)
			.click("//li[@class='ui-menu-item']/a[text()='TransactionData']")
			.pause(1000);
	},	
	
	"Click on GENERATE TOKEN (create WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@id='generateToken']", 2000)
			.click("//a[@id='generateToken']")
			.pause(1000);
	},		
	
	"Click on ADD ACCOUNT (create WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//span[text()='Add Account']", 2000)
			.click("//span[text()='Add Account']")
			.pause(1000);
	},
	
	"Grab TransactionData Token (create WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.getText("//li[contains(., 'TransactionData')]/div[@class='col2 fixedWidthFont']", function(result){
				transactionDataToken = result.value;
				console.log(transactionDataToken);
			})
			.pause(1000);
	},	

	"Logout from System Admin (create WSDL)" : function (browser)
	
	{
		browser.logout()
		.pause(1000);
	},

	"Login to Customer view (create WSDL))" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(childUser, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},	
	
	"Click the Settings left side menu option (create WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.navigateTo("Settings")
			.pause(1000);
	},	

	"Assert Digital API: XChange in the Services list and in the Digital API/Partner Integrations section (create WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.assert.visible("//li[contains(., 'Digital API: XChange')]")
			.assert.visible("//h3[contains(., 'Digital API - XChange')]")
			.pause(1000);
	},

	"Assert TransactionData is listed under API Name and the correct TransactionData token is displayed under Token (create WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.assert.visible("//td[contains(., 'TransactionData')]")
			.assert.visible("//td[contains(., '" + transactionDataToken + "')]")
			.pause(1000);
	},

	"Logout from Customer view (create WSDL)" : function (browser)
	
	{
		browser.logout();
	},

	"Login to System Admin view (edit WSDL)" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(systemAdminUser, password)
			.assert.title('Digital Iris - System Administration');
	},
	
	"Click on Customer/Pay Station Field (edit WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.pause(2000)
			.waitForElementVisible("//input[@id='search' and @class='autoText acWidthSearch stndrdChrctr hiddenLabel ui-autocomplete-input' and @type='text']", 2000)
			.click("//input[@id='search' and @class='autoText acWidthSearch stndrdChrctr hiddenLabel ui-autocomplete-input' and @type='text']")
			.pause(1000);
	},
	
	"Search for Customer qa1child (edit WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.setValue("//input[@id='search' and @class='autoText acWidthSearch stndrdChrctr hiddenLabel ui-autocomplete-input' and @type='text']", 'qa1child')
			.pause(2000)
			.waitForElementVisible("//a[@id='btnGo' and @class='linkButtonFtr textButton search']", 1000)
			.click("//a[@id='btnGo' and @class='linkButtonFtr textButton search']") 
			.pause(1000);
	},
	

	"Click on 'Licenses' Tab (edit WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='tab' and @title='Licenses']", 2000)
			.click("//a[@class='tab' and @title='Licenses']")
			.pause(1000);
	},
	
	"Click on 'Digital API' Sub Tab (edit WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@title='Digital API']", 2000)
			.click("//a[@title='Digital API']")
			.pause(1000);
	},
	
	"Click the Option Menu beside the TransactionData entry (edit WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//ul[@id='soapXChangeList']//a[@title='Option Menu']", 2000)
			.click("//ul[@id='soapXChangeList']//a[@title='Option Menu']")
			.pause(1000);
	},

	"Click the Edit button in the Option Menu (edit WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//ul[@id='soapXChangeList']//a[@title='Edit']", 2000)
			.click("//ul[@id='soapXChangeList']//a[@title='Edit']")
			.pause(1000);
	},
	
	"Click on GENERATE TOKEN (edit WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@id='generateToken']", 2000)
			.click("//a[@id='generateToken']")
			.pause(1000);
	},		
	
	"Click on ADD ACCOUNT (edit WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//span[text()='Edit Account']", 2000)
			.click("//span[text()='Edit Account']")
			.pause(1000);
	},
	
	"Grab TransactionData Token (edit WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.getText("//li[contains(., 'TransactionData')]/div[@class='col2 fixedWidthFont']", function(result){
				transactionDataToken = result.value;
				console.log(transactionDataToken);
			})
			.pause(1000);
	},	


	"Assert that the updated TransactionData token is displayed under API Name (edit WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.assert.visible("//li//p[contains(., '" + transactionDataToken + "')]")
			.pause(1000);
	},

	"Logout from System Admin (edit WSDL)" : function (browser)
	
	{
		browser.logout()
		.pause(1000);
	},

	"Login to Customer view (edit WSDL)" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(childUser, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},	
	
	"Click the Settings left side menu option (edit WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.navigateTo("Settings")
			.pause(1000);
	},	


	"Assert that the updated TransactionData token is displayed under Token (edit WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.assert.visible("//td[contains(., '" + transactionDataToken + "')]")
			.pause(1000);
	},

	"Logout from Customer view (edit WSDL)" : function (browser)
	
	{
		browser.logout();
	},

	"Login to System Admin view (delete WSDL)" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(systemAdminUser, password)
			.assert.title('Digital Iris - System Administration');
	},
	
	"Click on Customer/Pay Station Field (delete WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.pause(2000)
			.waitForElementVisible("//input[@id='search' and @class='autoText acWidthSearch stndrdChrctr hiddenLabel ui-autocomplete-input' and @type='text']", 2000)
			.click("//input[@id='search' and @class='autoText acWidthSearch stndrdChrctr hiddenLabel ui-autocomplete-input' and @type='text']")
			.pause(1000);
	},
	
	"Search for Customer qa1child (delete WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.setValue("//input[@id='search' and @class='autoText acWidthSearch stndrdChrctr hiddenLabel ui-autocomplete-input' and @type='text']", 'qa1child')
			.pause(2000)
			.waitForElementVisible("//a[@id='btnGo' and @class='linkButtonFtr textButton search']", 1000)
			.click("//a[@id='btnGo' and @class='linkButtonFtr textButton search']") 
			.pause(1000);
	},
	

	"Click on 'Licenses' Tab (delete WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='tab' and @title='Licenses']", 2000)
			.click("//a[@class='tab' and @title='Licenses']")
			.pause(1000);
	},
	
	"Click on 'Digital API' Sub Tab (delete WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@title='Digital API']", 2000)
			.click("//a[@title='Digital API']")
			.pause(1000);
	},
	
	"Click the Option Menu beside the TransactionData entry (delete WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//ul[@id='soapXChangeList']//a[@title='Option Menu']", 2000)
			.click("//ul[@id='soapXChangeList']//a[@title='Option Menu']")
			.pause(1000);
	},

	"Click the Delete button in the Option Menu (delete WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//ul[@id='soapXChangeList']//a[@title='Delete']", 2000)
			.click("//ul[@id='soapXChangeList']//a[@title='Delete']")
			.pause(1000);
	},
	
	"Click the DELETE button on the confirmation pop up (delete WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//span[contains(., 'Delete')]", 2000)
			.click("//span[contains(., 'Delete')]")
			.pause(1000);
	},		

	"Assert TransactionData is no longer listed under API Name and the TransactionData token is no longer displayed under Token (delete WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.assert.elementNotPresent("//li//p[contains(., 'TransactionData')]")
			.assert.elementNotPresent("//li//p[contains(., '" + transactionDataToken + "')]")
			.pause(1000);
	},
	
	"Logout from System Admin (delete WSDL)" : function (browser)

	{
		browser
		.logout()
		.pause(1000);
	},


	"Login to Customer view (delete WSDL)" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(childUser, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},		
	
	"Click the Settings left side menu option (delete WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.navigateTo("Settings")
			.pause(1000);
	},	

	"Assert TransactionData is no longer listed under API Name and the TransactionData token is no longer displayed under Token (delete WSDL)" : function (browser)
	{
		browser
			.useXpath()
			.assert.elementNotPresent("//td[contains(., 'TransactionData')]")
			.assert.elementNotPresent("//td[contains(., " + transactionDataToken + ")]")
			.pause(1000);
	},
	
	"Logout (delete WSDL)" : function (browser)
	
	{
		browser.logout();
	},	

	
};	