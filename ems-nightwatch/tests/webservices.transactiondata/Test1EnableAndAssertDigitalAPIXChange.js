/**
* @summary Web Services - Digital API: XChange - Enable and Assert Digital API: XChange 
* @since 7.4.3
* @version 1.0
* @author Paul Breland
* @see US1195/EMS-9621
* @requires n/a
**/

var data = require('../../data.js');
var devurl = data("URL");
var systemAdminUser = data("SysAdminUserName");
var password = data("Password");

module.exports = {
	tags: ['featuretesttag', 'completetesttag'],

	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(systemAdminUser, password)
			.assert.title('Digital Iris - System Administration');
	},	
	
	"Click on Customer/Pay Station Field" : function (browser)
	{
		browser
			.useXpath()
			.pause(2000)
			.waitForElementVisible("//input[@id='search' and @class='autoText acWidthSearch stndrdChrctr hiddenLabel ui-autocomplete-input' and @type='text']", 2000)
			.click("//input[@id='search' and @class='autoText acWidthSearch stndrdChrctr hiddenLabel ui-autocomplete-input' and @type='text']")
			.pause(1000);
	},
	
	"Search for Customer qa1child" : function (browser)
	{
		browser
			.useXpath()
			.setValue("//input[@id='search' and @class='autoText acWidthSearch stndrdChrctr hiddenLabel ui-autocomplete-input' and @type='text']", 'qa1child')
			.pause(2000)
			.waitForElementVisible("//a[@id='btnGo' and @class='linkButtonFtr textButton search']", 1000)
			.click("//a[@id='btnGo' and @class='linkButtonFtr textButton search']") 
			.pause(1000);
	},
	
	"Assert Digital API: XChange in the Services list" : function (browser)
	{
		browser
			.useXpath()
			.assert.visible("//li[contains(., 'Digital API: XChange')]", 2000)
			.pause(1000);
	},
	
	"Click the pencil button to edit Customer Details" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@id='btnEditCustomer']", 2000)
			.click("//a[@id='btnEditCustomer']")
			.pause(1000);
	},

	"Assert Digital API: XChange in the Services list of the UPDATE CUSTOMER pop up" : function (browser)
	{
		browser
			.useXpath()
			.assert.visible("//label[contains(., 'Digital API: XChange')]", 2000)
			.pause(1000);
	},
	
	"Check if Digital API: XChange is checked and click if not checked" : function (browser)
	{
		browser
			.useXpath()
			.getAttribute("//label[contains(., 'Digital API: XChange')]/a", "class", function(result){
				console.log(result.value);
				if (result.value.trim() === "checkBox") {
					browser
					.useXpath()
					.click("//label[contains(., 'Digital API: XChange')]/a")
					.pause(1000)
					.assert.visible("//label[contains(., 'Digital API: XChange')]/a[contains(@class, 'checked')]");
				};
			})
			.pause(1000);
	},
	
	"Click the UPDATE CUSTOMER button to save the change" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//button/span[contains(., 'Update Customer')]", 2000)
			.click("//button/span[contains(., 'Update Customer')]")
			.pause(1000);
	},

	"Logout" : function (browser)
	
	{
		browser.logout();
	},

};	