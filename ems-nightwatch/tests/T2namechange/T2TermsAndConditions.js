var data = require('../../data.js');

var devurl = data("URL");
var username = data("ChildUserName");
var password = data("Password");


module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
		
	"Click the Terms of Service link at the bottom of  the page" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[text()='Terms of Service']", 4000)
			.click("//a[text()='Terms of Service']")
			.pause(1000)
	},

	"Assert 'T2 Systems Canada Inc.' present within the Terms of Service text" : function (browser)
	
	{
		browser
			.useXpath()
			.pause(1000)
			.waitForElementVisible("//section[@id='legalDisclaimer']", 4000)
			.assert.containsText("//section[@id='legalDisclaimer']/p", "T2 SYSTEMS CANADA INC.")
			.pause(1000)
	},
	
	"Logout" : function (browser)
	
	{
		browser.logout();
	},
	
};