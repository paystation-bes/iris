var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");


module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Assert T2 copyright text on the main page" : function (browser) 
	{
		browser
			.useXpath()
			.url(devurl)
			.windowMaximize('current')
			.pause(2000)
			.waitForElementVisible("//section[@id='copyright']", 2000)
			.assert.visible("//section[@id='copyright']")
			.assert.visible("//body[@id='index']//section[contains(text(),'T2')]")

	},
	
	"Login" : function (browser) 
	{
		browser
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
	
	
	
	"Assert T2 copyright text is visible after login" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='copyright']", 2000)
			.assert.visible("//section[@id='copyright']")
			.assert.visible("//body[@id='sysAdmin']//section[contains(text(),'T2')]")
			.pause(1000)
	},

	"Logout" : function (browser)
	
	{
		browser.logout();
	},
	
};