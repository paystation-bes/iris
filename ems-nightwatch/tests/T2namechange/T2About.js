var data = require('../../data.js');

var devurl = data("URL");
var username = data("ChildUserName");
var password = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
		
	"Click the About link at the bottom of  the page" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[text()='About']", 4000)
			.click("//a[text()='About']")
			.pause(1000)
	},

	"Assert 'T2 Systems Canada Inc.' present and 'Digital Payment' not present within the About text" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//article/p[contains (text(),'T2 Systems Canada Inc.')]", 4000)
			.assert.visible("//article/p[contains (text(),'T2 Systems Canada Inc.')]")
			.assert.elementNotPresent("//article/p[contains (text(),'Digital Payment')]")
			.pause(1000)
	},
	
	"Logout" : function (browser)
	
	{
		browser.logout();
	},
	
};