/**
 * @summary Customer: Reports: Reports should have 'Today' as default date selection
 * @since 7.3.4
 * @version 1.0
 * @author Nadine B
 * @see EMS-6318
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ChildUserName");
var password = data("Password");

var reportTypes = [];
var textID = "";
var numberID = 0;
var usedIDs = [];
var count = 0;

module.exports = {
		tags: ['smoketag', 'completetesttag'],
		/**
		*@description Logs the user into Iris
		*@param browser - The web browser object
		*@returns void
		**/
		"testDefaultReportDateSelection: Log in": function(browser){
			browser
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.pause(3000)
			.assert.title("Digital Iris - Main (Dashboard)");
		},
		
		/**
		*@description Goes to Report tab and clicks on 'CREATE REPORT'
		*@param browser - The web browser object
		*@returns void
		**/
		"testDefaultReportDateSelection: Create reports": function(browser) {
			browser
			.useXpath()
			.waitForElementVisible("//a[@title='Reports']", 2000)
			.click("//a[@title='Reports']")
			.assert.title("Digital Iris - Reports")
			.pause(1000) 
			.waitForElementVisible("//*[@id='btnAddScheduleReport']", 2000)
			.click("//*[@id='btnAddScheduleReport']")
			.pause(500) 
		},
		
		/**
		*@description Gets the ID of the first report in the scroll-list
		*@param browser - The web browser object
		*@returns void
		**/
		"testDefaultReportDateSelection: Get reports info": function(browser) {
			browser
			.useXpath()
			.click("//*[@id='reportTypeIDExpand']")
			.pause(500)

			//Extract the first report ID
			.getAttribute("//*[@id='ui-id-1']/li[@class='ui-menu-item']/a", "id", function(objID) {
				browser.pause(500)
				//Get the text portion of ID - static
				textID = objID.value.substring(0,6);
				//Get the number portion of the ID - changes depending on the report type
				numberID += parseInt(objID.value.substring(6,objID.value.length));
			});
		},
		
		/**
		*@description Generates IDs for the remaining reports
		*@param browser - The web browser object
		*@returns void
		**/
		"testDefaultReportDateSelection: Count reports and generate their element tags from IDs": function(browser) {
			browser
			.useXpath()
			//Gets all result elements and count how many there are
			.elements("xpath","//li[@class='ui-menu-item']", function(result) {
				count += result.value.length;
				console.log("The number of elements is: " + count);
				
				//Use the first report ID to generate the rest of the IDs and save them
				for(j = 0; j < count; j++){
					var nextID = numberID + j;
					usedIDs.push(nextID);
				}
				
				//Extract all report types
				for(n = 0; n < count; n++){
					browser.getText("//*[@id='ui-id-1']/li[@class='ui-menu-item']/a[@id='" + textID + usedIDs[n] + "']", function(result) {
					   reportTypes.push(result.value)
					
					 });
				}

			});

		},
	
		/**
		*@description Runs the report script on each report type
		*@param browser - The web browser object
		*@returns void
		**/
		"testDefaultReportDateSelection: Run reports": function(browser) {
			for ( i = 0; i < reportTypes.length; i++){
				browser.runReports(reportTypes[i]);
			}
		},
		
		/**
		*@description Logs the user out of Iris
		*@param browser - The web browser object
		*@returns void
		**/
		"testDefaultReportDateSelection: Log out" : function(browser) {
			browser.logout();
		}

};
