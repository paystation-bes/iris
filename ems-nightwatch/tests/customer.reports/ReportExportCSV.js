/**
 * @summary Customer Admin - Reports - CSV Export
 * @since 7.4.2
 * @version 1.0
 * @author Paul Breland
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ChildUserName");
var password = data("Password");

var auditInfoToken = "";
var ZipName = "";

module.exports = {

	tags: ['featuretesttag', 'completetesttag'],
	
	/**
     * @description Logs the user into Iris
     * @param browser - The web browser object
     * @returns void
     **/
	"ReportExportCSV: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
	},
	
	/**
     * @description Click on Reports in the Sidebar
     * @param browser - The web browser object
     * @returns void
     **/
	"ReportExportCSV: Click on Reports in the Sidebar" : function (browser) {
		browser
		.pause(3000)
		.navigateTo("Reports")
		.pause(3000)
	},
	
	/**
     * @description Click on Create Report
     * @param browser - The web browser object
     * @returns void
     **/
	"ReportExportCSV: Click on Create Report" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@id='btnAddScheduleReport']", 2000)
		.click("//a[@id='btnAddScheduleReport']")
		.pause(1000)
	},
	
	/**
     * @description Click on the dropdown for Report Type
     * @param browser - The web browser object
     * @returns void
     **/
	"ReportExportCSV: Click on the dropdown for Report Type" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//input[@id='reportTypeID']", 2000)
		.click("//input[@id='reportTypeID']")
		.pause(1000)
	},
	
	/**
     * @description Choose Transaction - All
     * @param browser - The web browser object
     * @returns void
     **/
	"ReportExportCSV: Choose Transaction - All" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//li[@class='ui-menu-item']/a[text()='Transaction - All']", 2000)
		.click("//li[@class='ui-menu-item']/a[text()='Transaction - All']")
		.pause(1000)
	},
	
	/**
     * @description Click on the Next Step button
     * @param browser - The web browser object
     * @returns void
     **/
	"ReportExportCSV: Click on the Next Step button" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@id='btnStep2Select']", 2000)
		.click("//a[@id='btnStep2Select']")
		.pause(1000)
	},
	
	/**
     * @description Click the Details radio button
     * @param browser - The web browser object
     * @returns void
     **/
	"ReportExportCSV: Click the Details radio button" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@id='isSummary:false']", 2000)
		.click("//a[@id='isSummary:false']")
		.pause(1000)
	},
	
	/**
     * @description Click the Transaction Date/Time dropdown
     * @param browser - The web browser object
     * @returns void
     **/
	"ReportExportCSV: Click the Transaction Date/Time dropdown" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//input[@id='formPrimaryDateType']", 2000)
		.click("//input[@id='formPrimaryDateType']")
		.pause(1000)
	},
	
	/**
     * @description Choose All
     * @param browser - The web browser object
     * @returns void
     **/
	"ReportExportCSV: Choose All" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//li[@class='ui-menu-item']/a[text()='All']", 2000)
		.click("//li[@class='ui-menu-item']/a[text()='All']")
		.pause(1000)
	},
	
	/**
     * @description Click the CSV radio button
     * @param browser - The web browser object
     * @returns void
     **/
	"ReportExportCSV: Click the CSV radio button" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@id='isPDF:false']", 2000)
		.click("//a[@id='isPDF:false']")
		.pause(1000)
	},
	
	/**
     * @description Click the NEXT STEP button
     * @param browser - The web browser object
     * @returns void
     **/
	"ReportExportCSV: Click the NEXT STEP button" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@id='btnStep3Select']", 2000)
		.click("//a[@id='btnStep3Select']")
		.pause(1000)
	},
	
	/**
     * @description Click the SAVE button
     * @param browser - The web browser object
     * @returns void
     **/
	"ReportExportCSV: Click the SAVE button" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@class='linkButtonFtr textButton save']", 2000)
		.click("//a[@class='linkButtonFtr textButton save']")
		.pause(2000)
		.waitForReport("Transaction - All")
	},
	
	/**
     * @description Download the ZIP
     * @param browser - The web browser object
     * @returns void
     **/
	"ReportExportCSV: Download the ZIP" : function (browser) {
		browser
		.downloadZip("reports", devurl, "US987CSVReport", __dirname, "Transaction - All");
	},
	
	/**
     * @description Unzip the CSV and take the content
     * @param browser - The web browser object
     * @returns void
     **/
	"ReportExportCSV: Unzip the CSV and take the content" : function (browser) {
		browser
		.unzip("US987CSVReport.zip", __dirname, function (result) {
			ZipName = result[0];
			console.log(result[0]);
		})

	},
	
	/**
     * @description Parse the CSV file
     * @param browser - The web browser object
     * @returns void
     **/
	"ReportExportCSV: Parse the CSV file" : function (browser) {
		browser
		.parseCSV(ZipName, __dirname, function (result) {
			var OK = "OK";
			console.log(result.length + " rows counted");
			if (result.length < 800000) {
				OK = "Not OK";
			}

			if (result.length > 1000001) {
				OK = "Not OK";
			}
			browser.assert.equal(OK, "OK", "Confirmed number of rows greater than 800,000 and less than 1,000,001")
		})
	},
	
	/**
     * @description Delete the CSV file for cleanup
     * @param browser - The web browser object
     * @returns void
     **/
	"ReportExportCSV: Delete the CSV file for cleanup" : function (browser) {
		browser
		.deleteFile(ZipName, __dirname)
		.deleteFile("US987CSVReport.zip", __dirname)
	},
	
	/**
     * @description Logout
     * @param browser - The web browser object
     * @returns void
     **/
	"ReportExportCSV: Logout" : function (browser) {
		browser
		.logout()
	},
};
