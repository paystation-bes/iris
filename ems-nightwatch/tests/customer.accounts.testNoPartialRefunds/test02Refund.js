/**
 * @summary Asserts that user cannot edit Amount to Refund
 * @Author Billy Hoang
 **/

var data = require('../../data.js');

// Login Info
var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");

// Testing Info
var cardNumber = data("CardNumber");
var slicedCardNumber = "XXXXXXXXXXXX" + cardNumber.slice(12,16);
var cardExpiry = data("CardExpiry");
var refundAmount;
var searchDate = "Today";


module.exports = {
	tags: ['smoketag', 'completetesttag'],
	
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
		"test02Refund: Login" : function (browser) 
	{
		console.log("User: " + username + "/" + password);
		browser	
			.url(devurl)
			.windowMaximize('current')
			.initialLogin(username, password, "password")
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	
	/**
	 * @description Navigate to Card Refunds
	 * @param browser - The web browser object
	 * @returns void
	 **/
		"test02Refund: Navigate to Card Refunds" : function (browser) 
	{
		browser
			.click("img[alt='Card Management']")
			.pause(1000)
			.assert.title("Digital Iris - Card Management : Banned Cards")
			.click(".tab[title='Card Refunds']")
			.pause(1000)
			.assert.title("Digital Iris - Card Management : Card Refunds");
	},
	
	
	/**
	 * @description Input required information to Refund
	 * @param browser - The web browser object
	 * @returns void
	 **/
		"test02Refund: Input required information to Refund" : function (browser)
		
	{
			browser
			.waitForElementVisible("#searchCardNumber", 1000)
			.setValue("#searchCardNumber", cardNumber)
			
			.assert.visible("#searchCardExpiry")
			.setValue("#searchCardExpiry", cardExpiry)

			.assert.visible("#searchTxDateTypeExpand")
			.click("#searchTxDateTypeExpand")
			
			.useXpath()
			.assert.visible("//a[contains(text(),'" + searchDate + "')]")
			.click("//a[contains(text(),'" + searchDate + "')]");
	},
	
	
	/**
	 * @description Click the Search button
	 * @param browser - The web browser object
	 * @returns void
	 **/
		"test02Refund: Click the Search" : function(browser)
	{	
			browser
			.useXpath()
			.assert.visible("//a[@class='linkButtonFtr search right']")
			.click("//a[@class='linkButtonFtr search right']")
			.pause(3000);
	},
	
	/**
	 * @description Look for the sent transaction from test01
	 * @param browser - The web browser object
	 * @returns void
	 **/
		"test02Refund: Look for transaction" : function(browser)
	{
			browser
			.useXpath()
			.assert.visible("(//div[@class='col1']/p[contains(text(),slicedCardNumber)])[1]");
	},
			
	/**
	 * @description Click the first Refund button for the first transaction
	 * @param browser - The web browser object
	 * @returns void
	 **/		
		"test02Refund: Click Refund" : function(browser)
	{	
			browser
			.useXpath()
			.assert.visible("(//a[@class='menuListButton refund txMenu'])[1]")
			.click("//li/a")
			.pause(2000);
	},
	
	/**
	 * @description Get Refund amount
	 * @param browser - The web browser object
	 * @returns void
	 **/
		"test02Refund: Get Refund Amount" : function(browser)
	{	
			browser
			.useCss()
			.getText("section.clear > span", function(result) {
                refundAmount = result.value;
				console.log("Amount to be refunded: " + refundAmount);
            });
			
	},

	/**
	 * @description Assert that Amount to Refund is not editable
	 * @param browser - The web browser object
	 * @returns void
	 **/
 		"test02Refund: Assert that Amount to Refund is not editable" : function (browser)
	{
	
			browser	
			.useXpath()
			.assert.elementNotPresent("//input[@id='formRefundAmount']")
			
			.useCss()
			.assert.elementPresent(".clear>span")
			.assert.containsText(".clear>span", refundAmount) 
	}, 

	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
		"test02Refund: Logout" : function (browser) 
	{
		browser.logout();
	},  
	
};	