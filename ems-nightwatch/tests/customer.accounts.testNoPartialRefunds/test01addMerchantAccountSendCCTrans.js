/**
 * @summary
 * Add merchant account (Paymentech) to customer oranj, and assign
 * it to the first pay station, also make a $4.00 CC transaction to that Pay Station
 * @Author Billy Hoang
 **/
 
 var data = require('../../data.js');

// Login Info
var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");

// Testing Info
var PSNumber;
var cardNumber = data("CardNumber");

module.exports = {
	tags: ['smoketag', 'completetesttag'],	


	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
		"test01addMerchantAccountSendCCTrans: Login to System Admin" : function (browser) 
	{
		console.log("User: " + username + "/" + password);
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
	/**
	 * @description Search for customer Oranj
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test01addMerchantAccountSendCCTrans: Search for oranj (Oranj Parent)" : function (browser) 
	{
		browser
			.useCss()
			.waitForElementVisible("#search", 2000)
			.setValue("#search", "oranj")
			.pause(2000)
			.assert.visible("#btnGo")
			.click("#btnGo")

	},

	/**
	 * @description Click on Add Merchant Account Button
	 * @param browser - The web browser object
	 * @returns void
	 **/
		"test01addMerchantAccountSendCCTrans: Click on Add Merchant Account Button" : function (browser) 
	{
		browser
			.useCss()
			.waitForElementVisible("#btnAddMrchntAccnt", 4000)
			.click("#btnAddMrchntAccnt")
	},

	/**
	 * @description Fill out required information for Merchant Account and submit
	 * @param browser - The web browser object
	 * @returns void
	 **/
		"test01addMerchantAccountSendCCTrans: Fill out Merchant Account form and submit" : function (browser) 
	{
			browser
			.useCss()
			.waitForElementVisible("#account", 2000)
			.setValue("#account", "Paymentech")
			.assert.visible("#statusCheck")
			.click("#statusCheck")
			.assert.visible("#processorsExpand")
			.click("#processorsExpand")
			.pause(1000)
			
			.useXpath()
			.assert.visible("//a[contains(text(),'Paymentech')]")
			.click("//a[contains(text(),'Paymentech')]")
			.pause(2000)
			
			.useCss()
			.waitForElementVisible("#field1", 2000)
			.setValue("#field1", "700000002018")
			.assert.visible("#field2")
			.setValue("#field2", "001")
			.assert.visible("#field3")
			.setValue("#field3", "digitech1")
			.assert.visible("#field4")
			.setValue("#field4", "digpaytec01")
			.assert.visible("#field5")
			.setValue("#field5", "0002")		
			
			.useXpath()
			.assert.visible("(//button[@type='button'])[2]")
			.click("(//button[@type='button'])[2]")
			.pause(4000)
			
	},

	/**
	 * @description Click on Pay Stations tab
	 * @param browser - The web browser object
	 * @returns void
	 **/
		"test01addMerchantAccountSendCCTrans: Click on the Pay Stations tab" : function (browser) 
	{
			browser
			.useCss()
			.assert.visible(".tab[title='Pay Stations']")
			.click(".tab[title='Pay Stations']")
			.pause(2000)
			.assert.title("Digital Iris - System Administration - oranj : Pay Station Details")
	},

	/**
	 * @description Get first Pay Station number
	 * @param browser - The web browser object
	 * @returns void
	 **/
 			"test01addMerchantAccountSendCCTrans: Get the first Pay Station number" : function(browser)
	{	
			browser
			.useXpath()
			.getText("(//ul[@id='paystationList']//span[@class='textLine'])[1]", function(result) {
                PSNumber = result.value.slice(0,12);
				console.log("Using Pay Station #: " + PSNumber);
            });
			
	}, 

	/**
	 * @description Click on the edit button of the first Pay Station
	 * @param browser - The web browser object
	 * @returns void
	 **/
		"test01addMerchantAccountSendCCTrans: Edit first Pay Station" : function (browser) 
	{
			browser
			.useXpath()
			.assert.visible("(//ul[@id='paystationList']//a[@class='menuListButton edit'])[1]")
			.click("(//ul[@id='paystationList']//a[@class='menuListButton edit'])[1]")
			.pause(2000)
			.useCss()
			.assert.visible("#editHeading")
	},

	/**
	 * @description Add Merchant Account to Pay Station
	 * @param browser - The web browser object
	 * @returns void
	 **/
		"test01addMerchantAccountSendCCTrans: Add Merchant Account to Pay Station" : function (browser)
	{
			browser
			.useCss()
			.assert.visible("#formCreditCardAccountExpand")
			.click("#formCreditCardAccountExpand")
			.pause(2000)
			
			.useXpath()
			.assert.visible("//a[contains(text(),'Paymentech')]")
			.click("//a[contains(text(),'Paymentech')]")
			
			.useCss()
			.assert.visible(".linkButtonFtr.textButton.save")
			.click(".linkButtonFtr.textButton.save")
			.pause(4000)
	},

	/**
	 * @description Navigate to the Pay Station's information and check merchant account
	 * @param browser - The web browser object
	 * @returns void
	 **/
		"test01addMerchantAccountSendCCTrans: Assert Merchant Account is added to Pay Station" : function (browser)	
	{
			browser
			.useCss()
			.assert.visible(".tab[title='Information']", 2000)
			.click(".tab[title='Information']")
			.pause(2000)
			.useXpath()
			.assert.visible("//section[@id='psCCMerchantAccountLine']/dd[contains(text(),'Paymentech')]")
	},
	
	/**
	 * @description Sends a Credit Card transaction to the Pay Station
	 * @param browser - The web browser object
	 * @returns void
	 **/
		"test01addMerchantAccountSendCCTrans: SendCCTrans" : function (browser) 
	{
		
		 browser.sendCCPreAuth("4.00", cardNumber, PSNumber, function (result) {
			var CCTrans = {
				licenceNum : '496BCW',
				stallNum : '1',
				type : 'Regular',
				originalAmount : '400',
				chargedAmount: '4.00',
				cardPaid : '4.00',
				cardAuthorizationId: result.authID,
				emsPreAuthId: result.emsPreAuthID,
			}
   
			browser.sendCCPostAuth([CCTrans], PSNumber);
		});
		
	},
	
	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test01addMerchantAccountSendCCTrans: Logout" : function (browser) 
	{
		browser.logout();
	},  
	
	
 };
 
 
 
 
 
 
 
 