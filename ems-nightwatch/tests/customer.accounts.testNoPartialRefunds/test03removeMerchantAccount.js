/**
 * @summary Remove Merchant Account from customer oranj
 * @Author Billy Hoang
 * 
 **/
 
 var data = require('../../data.js');
 
// Login Info
var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag'],

	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
		"test03removeMerchantAccount: Login to System Admin" : function (browser) 
	{
		console.log("User: " + username + "/" + password);
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},

	/**
	 * @description Search for customer oranj
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test03removeMerchantAccount: Search for oranj (Oranj Parent)" : function (browser) 
	{
		browser
			.useCss()
			.waitForElementVisible("#search", 2000)
			.setValue("#search", "oranj")
			.pause(2000)
			.assert.visible("#btnGo")
			.click("#btnGo")
	},

	/**
	 * @description Delete Paymentech Merchant Account
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test03removeMerchantAccount: Delete Paymentech Merchant Account" : function (browser){
		
			browser
			.useXpath()
			.assert.visible("//ul[@id='merchantAccountList']/li/div[@class='col1 ']/p[contains(text(),'Paymentech')]/../../a")
			.click("//ul[@id='merchantAccountList']/li/div[@class='col1 ']/p[contains(text(),'Paymentech')]/../../a")
			.pause(3000)
			
			.useCss()
			.assert.visible(".delete")
			.click(".delete")
			.pause(3000)

			.useXpath()
			// Need better path
			.assert.visible("(//button[@type='button'])[2]")
			.click("(//button[@type='button'])[2]")
			.pause(3000)
			
			.assert.elementNotPresent("//ul[@id='merchantAccountList']/li/div[@class='col1 ']/p[contains(text(),'Paymentech')]")
	},
	
};