/**
* @summary Customer Admin - Settings - Pay Station Details - Print Collection Reports
* @since 7.4.2
* @version 1.0
* @author Paul Breland
* 
**/

var data = require('../../data.js');
var devurl = data("URL");
var username = data("ChildUserName");
var password = data("Password");


module.exports = {
	    tags: ['featuretesttag', 'completetesttag'],

	"Send Audit Report to ensure at least one Collection Report entry is present" : function (browser) 
	{
		browser.sendAudit("500000080003")
	},

	"Login as customer" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - Main (Dashboard)');
	},
			
	"Click on Settings in the Sidebar" : function (browser) 
	{
		browser
			.pause(3000)
			.navigateTo("Settings")
			.pause(2000)
	},
	
	"Click the Pay Stations tab" : function (browser) 
	{
		browser
			.navigateTo("Pay Stations")
			.pause(2000)
	},

	"Click on the Pay Station from which to print the Collection Reports" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//span[contains(text(),'500000080003')]", 2000)
			.click("//span[contains(text(),'500000080003')]")
			.pause(1000)
	},
	
	"Click on the Reports tab within Pay Station Details" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//div[@id='reportsBtn']/a[@title='Reports']", 2000)
			.click("//div[@id='reportsBtn']/a[@title='Reports']")
			.pause(1000)
	},

	"Click on the Collection Reports tab within Reports" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@id='collectionReportBtn']", 2000)
			.click("//a[@id='collectionReportBtn']")
			.pause(1000)
	},	

	"Click on the first entry in Collection Reports" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//li/div[@class='col1']", 2000)
			.click("//li/div[@class='col1']")
			.pause(1000)
	},	

	"Assert that the Export to PDF button is visible" : function (browser)
	{
		browser
			.useXpath()
			.assert.visible("//span[contains(text(),'Export to PDF')]")
			.pause(1000)
	},	

	"Download the PDF" : function (browser)
	{
		browser
			.downloadPDF("collections", devurl, "PSCollectionReport", __dirname, "All")
	},		

	"Assert that the downloaded file is indeed a PDF" : function (browser)
	{
		browser
			.assertPDFTextPresent("PSCollectionReport.pdf", __dirname, ["Collection Report"])
	},		

	"Delete the PDF" : function (browser)
	{
		browser
			.deleteFile("PSCollectionReport.pdf", __dirname)
	},

	"Logout" : function (browser)
	
	{
		browser.logout();
	},	
	
};		