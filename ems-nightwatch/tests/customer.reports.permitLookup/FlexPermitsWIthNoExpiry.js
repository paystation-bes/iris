/**
 * @summary Flex Permits: Ensure that Flex permits with no expiry are correctly displayed
 * @since 7.4.1
 * @version 1.0
 * @author Thomas c
 * @see US728
 **/
var data = require('../../data.js');

var devurl = data("URL");
var serialNum = data("PaystationCommAddress");
//using FLexChildName to match other FLEX scripts - tested with admin@oranj on branch
var username = 'admin@oranj'//data("FlexChildName");
var password = data("Password");
var licenceNumber = 'AutoTest';
var cashTrans = {
    licenceNum: licenceNumber,
    stallNum: '',
    type: 'Regular',
    originalAmount: '400',
    chargedAmount: '4.00',
    cashPaid: '4.00',
    isRefundSlip: 'false'
};

/**
 *@description Recursivly searches for the license number
 *@param browser - The web browser object
 *@returns void
 **/
keepSearching = function(browser) {
	var dateType = '//input[@id="searchTxDateType"]';
	var searchPlate = '//input[@id="searchPlate"]';
	var thisWeek = 'This Week';
	var notAvailable = 'Not available';
	var firstResult = '//ul[@id="transReceiptListContainer"]//li';
    browser
        .navigateTo("Reports")
        .navigateTo("Permit Lookup")
        .clearValue(dateType)
        .setValue(dateType, thisWeek, function() {
                browser.setValue(searchPlate, licenceNumber, function() {
                    browser.click('//a[@title="Search"]', function() {
                        //this pause is needed as the search always returns 'not available' until the results populate
                        browser.pause(10000, function() {
                            browser.getText(firstResult, function(result) {
                                if (result.value.indexOf(notAvailable) > -1) {
                                        keepSearching(browser);
                                }
                            });
                        });
                    });
                });
        });
},

module.exports = {
tags: ['featuretesttag', 'completetesttag'],
	

    
	/**
     *@description Sends a cash transaction
     *@param browser - The web browser object
     *@returns void
     **/
	before : function(browser){
		browser
            .sendCashTrans([cashTrans])
            .pause(4000);
	},
	

    /**
     *@description Logs the user into Iris
     *@param browser - The web browser object
     *@returns void
     **/
    "FlexPermitsWithNoExpiry: Login": function(browser) {
        browser
            .url(devurl)
            .windowMaximize('current')
            .login(username, password)
            .assert.title('Digital Iris - Main (Dashboard)');
    },

    /**
     *@description Repeatedly search for permit until it appears in system
     *@param browser - The web browser object
     *@returns void
     **/
    "FlexPermitsWithNoExpiry: Searching for new permit": function(browser) {
        keepSearching(browser);
        browser.pause(2000);
    },

    /**
     *@description Repeatedly search for permit until it appears in system
     *@param browser - The web browser object
     *@returns void
     **/
    "Assert new permit is present": function(browser) {
        browser.assert.visible('//li[contains(., "' + licenceNumber + '")]');
    },

    /**
     *@description Asserts details of permit with expiry
     *@param browser - The web browser object
     *@returns void
     **/
    "FlexPermitsWithNoExpiry:  Select Permit with expiry": function(browser) {
        browser
            .useXpath()
            .click('//li[contains(., "' + serialNum + '")]')
            .waitForElementVisible('//dd[@id="expiryDate_Value"]', 10000)
            .assert.elementNotPresent('//dd[@id="expiryDate_Value" and contains(., "Does not expire")]')
            .assert.elementNotPresent('//span[@class="dateLine" and contains(text(),"Does not expire")]')
            .pause(2000);
    },

    /**
     *@description Searches for Permit with no expiry
     *@param browser - The web browser object
     *@returns void
     **/
    "FlexPermitsWithNoExpiry:  Search for permit no expiry": function(browser) {
        browser
            .clearValue('//input[@id="searchPlate"]')
            .navigateTo('Search')
            .pause(2000);
    },

    /**
     *@description Asserts details of permit with no Expiry also asserts that it is on top of list
     *@param browser - The web browser object
     *@returns void
     **/
    "FlexPermitsWithNoExpiry:  Select Permit with No expiry": function(browser) {
        browser
            .useXpath()
            .waitForElementVisible('//span[contains(., "Does not expire")]', 5000)
            .click('//ul[@id="transReceiptListContainer"]//li')
            .waitForElementVisible('//dd[@id="expiryDate_Value" and contains(., "Does not expire")]', 5000)
            .assert.containsText('//dd[@id="expiryDate_Value"]', 'Does not expire')
            .assert.containsText('//span[@class="dateLine"]', 'Does not expire')
            .pause(2000);
    },

    /**
     *@description Logs the user out of Iris
     *@param browser - The web browser object
     *@returns void
     **/
    "FlexPermitsWithNoExpiry: Logout": function(browser) {
        browser.logout();
    },
};