/**
 * @summary Child Customer: Accounts: Customer Accounts: Edit Account
 * @since 7.3.4
 * @version 1.0
 * @author Mandy F
 * @see US914
 * @requires test1AddCustomerAccount.js
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ChildUserName2");
var password = data("Password");

// generate random number between 1 and 100 to append to customer account data 
// to act as a new account each time script runs
var num = Math.floor((Math.random() * 100) + 1); 

// new customer account data for edit
var firstName = "Edited First Name";
var lastName = "Edited Last Name";
var email = "edited@automation.com" + num;
var description = "Edited customer account information " + num;

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test3EditCustomerAccount: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
		.assert.title('Digital Iris - Main (Dashboard)')
	},

	/**
	* @description Enters into the Customer Accounts page
	* @param browser - The web browser object
	* @returns void
	**/
	"test3EditCustomerAccount: Entering Customer Accounts" : function (browser) {
		var accountsLink = "//nav[@id='main']//a[@title='Accounts']";
		var custAccLink = "//a[@title='Customer Accounts']";
		browser
		.useXpath()
		.waitForElementVisible(accountsLink, 3000)
		.click(accountsLink)
		.waitForElementVisible(custAccLink, 3000)
		.click(custAccLink)
		.pause(1000);
	},

	/**
	 * @description Opens the edit screen for the first customer account on the list
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test3EditCustomerAccount: Open Customer Account Edit Screen" : function (browser) {
		var firstAcc = "//ul[@id='consumersListContainer']//li[1]";
		var firstAccOptions = firstAcc + "//a[@title='Option Menu']";
		var firstAccEditBtn = "//section[@class='menuOptions innerBorder']//a[@title='Edit']";
		browser
		.useXpath()
		.waitForElementVisible(firstAccOptions, 1000)
		.click(firstAccOptions)
		.waitForElementVisible(firstAccEditBtn, 1000)
		.click(firstAccEditBtn)
		.pause(1000);
	},
	
	/**
	 * @description Fill in edited information for the customer
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test3EditCustomerAccount: Edit Customer Information" : function (browser) {
		var firstNameForm = "//input[@id='formFirstName']";
		var lastNameForm = "//input[@id='formLastName']";
		var emailForm = "//input[@id='formEmail']";
		var descriptionForm = "//textarea[@id='formDescription']";
		var saveButton = "//a[@class='linkButtonFtr textButton save']";
		browser
		.useXpath()
		.waitForElementVisible(firstNameForm, 1000)
		.click(firstNameForm)
		.clearValue(firstNameForm)
		.setValue(firstNameForm, firstName)
		.waitForElementVisible(lastNameForm, 1000)
		.click(lastNameForm)
		.clearValue(lastNameForm)
		.setValue(lastNameForm, lastName)
		.waitForElementVisible(emailForm, 1000)
		.click(emailForm)
		.clearValue(emailForm)
		.setValue(emailForm, email)
		.waitForElementVisible(descriptionForm, 1000)
		.click(descriptionForm)
		.clearValue(descriptionForm)
		.setValue(descriptionForm, description)
		.waitForElementVisible(saveButton, 3000)
		.click(saveButton)
		.pause(3000);
	},
	
	/**
	 * @description Verifies the edited customer account information
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test3EditCustomerAccount: Verify Customer Account Information" : function (browser) {
		browser
		.useXpath()
		// assert first name is visible in details pop up
		.assert.visible("//dd[contains(text(), '" + firstName + "')]")
		// assert last name is visible in details pop up
		.assert.visible("//dd[contains(text(), '" + lastName + "')]")
		// assert email is visible in details pop up
		.assert.visible("//dd[contains(text(), '" + email + "')]")
		// assert description is visible in details pop up
		.assert.visible("//dd[contains(text(), '" + description + "')]")
		.pause(1000);
	},

	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test3EditCustomerAccount: Logout" : function (browser) {
		browser.logout();
	},
};
