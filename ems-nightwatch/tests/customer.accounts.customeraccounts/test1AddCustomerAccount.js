/**
 * @summary Child Customer: Accounts: Customer Accounts: Add Account
 * @since 7.3.4
 * @version 1.0
 * @author Mandy F
 * @see US914
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ChildUserName2");
var password = data("Password");

// generate random number between 1 and 100 to append to customer account data 
// to act as a new account each time script runs
var num = Math.floor((Math.random() * 100) + 1); 

// customer account data
var firstName = "QA";
var lastName = "Automation";
var description = "QA automation account " + num;
var email = "qaautomation@test.com" + num;

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test1AddCustomerAccount: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
		.assert.title('Digital Iris - Main (Dashboard)')
	},

	/**
	* @description Enters into the Customer Accounts page
	* @param browser - The web browser object
	* @returns void
	**/
	"test1AddCustomerAccount: Entering Customer Accounts" : function (browser) {
		var accountsLink = "//nav[@id='main']//a[@title='Accounts']";
		var custAccLink = "//a[@title='Customer Accounts']";
		browser
		.useXpath()
		.waitForElementVisible(accountsLink, 1000)
		.click(accountsLink)
		.waitForElementVisible(custAccLink, 1000)
		.click(custAccLink)
		.pause(1000);
	},
	
	/**
	* @description Open add customer account screen
	* @param browser - The web browser object
	* @returns void
	**/
	"test1AddCustomerAccount: Open Add Customer Account" : function (browser) {
		var addAccountBtn = "//a[@id='opBtn_addConsumer']";
		browser
		.useXpath()
		.waitForElementVisible(addAccountBtn, 1000)
		.click(addAccountBtn)
		.pause(1000);
	},

	/**
	* @description Fills in the information to add customer account
	* @param browser - The web browser object
	* @returns void
	**/
	"test1AddCustomerAccount: Add Customer Account Information" : function (browser) {
		var firstNameForm = "//input[@id='formFirstName']";
		var lastNameForm = "//input[@id='formLastName']";
		var emailForm = "//input[@id='formEmail']";
		var descriptionForm = "//textarea[@id='formDescription']";
		var saveButton = "//a[@class='linkButtonFtr textButton save']";
		browser
		.useXpath()
		.waitForElementVisible(firstNameForm, 1000)
		.click(firstNameForm)
		.setValue(firstNameForm, firstName)
		.waitForElementVisible(lastNameForm, 1000)
		.click(lastNameForm)
		.setValue(lastNameForm, lastName)
		.waitForElementVisible(emailForm, 1000)
		.click(emailForm)
		.setValue(emailForm, email)
		.waitForElementVisible(descriptionForm, 1000)
		.click(descriptionForm)
		.setValue(descriptionForm, description)
		.waitForElementVisible(saveButton, 2000)
		.click(saveButton)
		.pause(3000);
	},

	/**
	 * @description Opens the new customer account information details
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test1AddCustomerAccount: Open Customer Account Information Details" : function (browser) {
		var customerAcc = "//li[@description='" + description + "']/div[1]/p";
		browser
		.useXpath()
		.waitForElementVisible(customerAcc, 3000)
		.click(customerAcc)
		.pause(5000);
	},
	
	/**
	 * @description Verifies the new customer account information
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test1AddCustomerAccount: Verify Customer Account Information" : function (browser) {
		browser
		.useXpath()
		// assert first name is visible in details pop up
		.assert.visible("//dd[contains(text(), '" + firstName + "')]")
		// assert last name is visible in details pop up
		.assert.visible("//dd[contains(text(), '" + lastName + "')]")
		//assert email is visible in details pop up
		.assert.visible("//dd[contains(text(), '" + email + "')]")
		// assert description is visible in details pop up
		.assert.visible("//dd[contains(text(), '" + description + "')]")
		.pause(1000);
	},

	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test1AddCustomerAccount: Logout" : function (browser) {
		browser.logout();
	},
};
