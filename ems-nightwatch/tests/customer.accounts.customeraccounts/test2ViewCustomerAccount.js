/**
 * @summary Child Customer: Accounts: Customer Accounts: View Account
 * @since 7.3.4
 * @version 1.0
 * @author Mandy F
 * @see US914
 * @requires test1AddCustomerAccount.js
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ChildUserName2");
var password = data("Password");

// customer account data that need to be saved for verifying info
var firstName = new String();
var lastName = new String();
var email = new String();
var description = new String();

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test2ViewCustomerAccount: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
		.assert.title('Digital Iris - Main (Dashboard)')
	},

	/**
	* @description Enters into the Customer Accounts page
	* @param browser - The web browser object
	* @returns void
	**/
	"test2ViewCustomerAccount: Entering Customer Accounts" : function (browser) {
		var accountsLink = "//nav[@id='main']//a[@title='Accounts']";
		var custAccLink = "//a[@title='Customer Accounts']";
		browser
		.useXpath()
		.waitForElementVisible(accountsLink, 3000)
		.click(accountsLink)
		.waitForElementVisible(custAccLink, 3000)
		.click(custAccLink)
		.pause(1000);
	},

	/**
	 * @description Opens the first customer account details on the list
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test2ViewCustomerAccount: Open Customer Account Information Details" : function (browser) {
		var firstAcc = "//ul[@id='consumersListContainer']//li[1]";
		var firstAccName = firstAcc + "//div[@class='col1']//p";
		var firstAccLastName = firstAcc + "//div[@class='col2']//p";
		var firstAccEmail = firstAcc + "//div[@class='col3']//p";
		var listing = "//ul[@id='consumersListContainer']//li[1]/div[1]/p";
		browser
		.useXpath()
		.waitForElementVisible(firstAcc, 3000)
		// obtain first name of the first account and save in global variable
		.getText(firstAccName, function (result) {
			firstName = result.value.trim();
		})
		// obtain last name of the first account and save in global variable
		.getText(firstAccLastName, function (result) {
			lastName = result.value.trim();
		})
		// obtain email of the first account and save in global variable
		.getText(firstAccEmail, function (result) {
			email = result.value.trim();
		})
		// obtain description of the first account and save in global variable
		.getAttribute(firstAcc, "description", function (result) {
			description = result.value;
		})
		.click(listing)
		.pause(2000);
	},
	
	/**
	 * @description Verifies the customer account information
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test2ViewCustomerAccount: Verify Customer Account Information" : function (browser) {
		browser
		.useXpath()
		// assert first name is visible in details pop up
		.assert.visible("//dd[contains(text(), '" + firstName + "')]")
		// assert last name is visible in details pop up
		.assert.visible("//dd[contains(text(), '" + lastName + "')]")
		// assert email is visible in details pop up
		.assert.visible("//dd[contains(text(), '" + email + "')]")
		// assert description is visible in details pop up
		.assert.visible("//dd[contains(text(), '" + description + "')]")
		.pause(1000);
	},

	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test2ViewCustomerAccount: Logout" : function (browser) {
		browser.logout();
	},
};
