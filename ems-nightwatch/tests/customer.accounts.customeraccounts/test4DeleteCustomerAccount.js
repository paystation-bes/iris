/**
 * @summary Child Customer: Accounts: Customer Accounts: Delete Account
 * @since 7.3.4
 * @version 1.0
 * @author Mandy F
 * @see US914
 * @requires test1AddCustomerAccount.js
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ChildUserName2");
var password = data("Password");

// save customer account email for verifying delete in the end
var email = new String();

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test4DeleteCustomerAccount: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(username, password, "password")
		.assert.title('Digital Iris - Main (Dashboard)')
	},

	/**
	* @description Enters into the Customer Accounts page
	* @param browser - The web browser object
	* @returns void
	**/
	"test4DeleteCustomerAccount: Entering Customer Accounts" : function (browser) {
		var accountsLink = "//nav[@id='main']//a[@title='Accounts']";
		var custAccLink = "//a[@title='Customer Accounts']";
		browser
		.useXpath()
		.waitForElementVisible(accountsLink, 3000)
		.click(accountsLink)
		.waitForElementVisible(custAccLink, 3000)
		.click(custAccLink)
		.pause(1000);
	},
	
	/**
	 * @description Opens the option menu for the first customer account on the list
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test4DeleteCustomerAccount: Open Customer Account Options" : function (browser) {
		var firstAcc = "//ul[@id='consumersListContainer']//li[1]";
		var firstAccEmail = firstAcc + "//div[@class='col3']//p";
		var firstAccOptions = firstAcc + "//a[@title='Option Menu']";
		browser
		.useXpath()
		// obtain email of the first account and save in global variable
		.getText(firstAccEmail, function (result) {
			email = result.value;
		})
		.waitForElementVisible(firstAccOptions, 1000)
		.click(firstAccOptions)
		.pause(1000);
	},
	
	/**
	 * @description Delete the first customer account on the list
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test4DeleteCustomerAccount: Delete Customer Account" : function (browser) {
		var firstAcc = "//ul[@id='consumersListContainer']//li[1]";
		var firstAccDeleteBtn = "//section[@class='menuOptions innerBorder']//a[@title='Delete']";
		var deleteOkBtn = "//button[@type='button']//span[contains(text(), 'Ok')]";
		browser
		.useXpath()
		.waitForElementVisible(firstAccDeleteBtn, 1000)
		.click(firstAccDeleteBtn)
		.waitForElementVisible(deleteOkBtn, 1000)
		.click(deleteOkBtn)
		.pause(1000);
	},
	
	/**
	 * @description Verifies the customer account is deleted
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test4DeleteCustomerAccount: Verify Deleted Customer Account Information" : function (browser) {
		browser
		.useXpath()
		// assert email is no longer present (first and last name may have duplication and email doesn't)
		.assert.elementNotPresent("//p[contains(text(), '" + email + "')]")
		.pause(1000);
	},

	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test4DeleteCustomerAccount: Logout" : function (browser) {
		browser.logout();
	},
};
