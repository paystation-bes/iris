/**
* @summary Customer View: Settings: Locations: Edit Location: check UI for Facilities and Properties
* @since 7.3.5
* @version 1.0
* @author Paul Breland
* @see US723
* @requires n/a
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("FlexChildName");
var password = data("Password");


module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.initialLogin(username, password, "password")
			.assert.title('Digital Iris - Main (Dashboard)');
	},		
			
			
	"Click Settings on the left sidebar menu" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//nav[@id='main']/section[@class='bottomSection']/a/img", 4000)
			.click("//nav[@id='main']/section[@class='bottomSection']/a/img")
			.pause(1000)
	},

	"Click the Locations tab on the Settings page" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@title='Locations']", 4000)
			.click("//a[@title='Locations']")
			.pause(1000)
	},
	
	"Click the Add (+) Button to create a new Location" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@id='btnAddLocation']/img", 4000)
			.click("//a[@id='btnAddLocation']/img")
			.pause(1000)
	},
	
	"Assert the Facilities section header along with All Facilities and Selected Facilities subheaders" : function (browser)
	
	{
		browser
			.useXpath()
			.assert.visible("//section[@id='formFacilityChildren']/h3[contains(text(),'Facilities')]")
			.assert.visible("//section[contains(text(),'All Facilities:')]")
			.assert.visible("//section[contains(text(),'Selected Facilities:')]")
			.pause(1000)
	},
	
	"Assert the Properties section header along with All Properties and Selected Properties subheaders" : function (browser)
	
	{
		browser
			.useXpath()
			.assert.visible("//section[@id='formPropertyChildren']/h3[contains(text(),'Properties')]")
			.assert.visible("//section[contains(text(),'All Properties:')]")
			.assert.visible("//section[contains(text(),'Selected Properties:')]")
			.pause(1000)
	},

		"Assert the presence of Facilities and Properties within their respective lists" : function (browser)
	
	{
		browser
			.useXpath()
			.assert.visible("//ul[@id='locFacFullList']/li[contains(text(),'700 PIERCE LOT')]")
			.assert.visible("//ul[@id='locPropFullList']/li[contains(text(),'Route 20')]")			
			.pause(1000)
	},	
	
	"Click the Cancel Button to leave the Edit Location form" : function (browser)
	
	{
		browser
			//.useXpath()
			//.waitForElementVisible("//a[@class='linkButton textButton cancel']", 4000)
			//.click("//a[@class='linkButton textButton cancel']")
			.useCss()
			.waitForElementVisible("#locationForm>.btnSet>.linkButton.textButton.cancel", 3000)
			.click("#locationForm>.btnSet>.linkButton.textButton.cancel")
			.pause(1000)
	},
	
	"Logout" : function (browser)
	
	{
		browser.logout();
	},
	
};