/**
* @summary Customer View: Settings: Locations: Create Test Location 1 (FacProp1)
* @since 7.3.5
* @version 1.0
* @author Paul Breland
* @see US723
* @requires n/a
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("FlexChildName");
var password = data("Password");
var tempPassword = data("DefaultPassword");


module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.initialLogin(username, password, tempPassword)
			.assert.title('Digital Iris - Main (Dashboard)');
	},		
			
		
	"Click Settings on the left sidebar menu" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//nav[@id='main']/section[@class='bottomSection']/a/img", 4000)
			.click("//nav[@id='main']/section[@class='bottomSection']/a/img")
			.pause(1000)
	},

	"Click the Locations tab on the Settings page" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@title='Locations']", 4000)
			.click("//a[@title='Locations']")
			.pause(1000)
	},
	
	"Click the Add (+) Button to create a new Location" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@id='btnAddLocation']/img", 4000)
			.click("//a[@id='btnAddLocation']/img")
			.pause(1000)
	},

	"Fill in Name, Operating Mode and Capacity" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//input[@id='formLocationName']", 2000)
			.setValue("//input[@id='formLocationName']", 'FacProp1')
			.assert.visible("//input[@id='formOperatingMode']")
			.setValue("//input[@id='formOperatingMode']", 'Multiple')
			.assert.visible("//input[@id='formCapacity']")
			.setValue("//input[@id='formCapacity']", '100')
			.pause(1000)
	},

	"Click the Save Button to create the Location to test the Facilities and Properties scripts" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='linkButtonFtr textButton save']", 1000)
			.click("//a[@class='linkButtonFtr textButton save']")
			.pause(6000)
	},
	
	
	"Logout" : function (browser)
	
	{
		browser.logout();
	},
	
};