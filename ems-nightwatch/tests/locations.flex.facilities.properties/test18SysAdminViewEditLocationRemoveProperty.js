/**
* @summary System Admin View: Settings: Locations: Edit Location: ensure a Property can be removed from a Location
* @since 7.3.5
* @version 1.0
* @author Paul Breland
* @see US723
* @requires test01CustomerViewCreateFacilitiesPropertiesTestLocation1.js
* @requires test02CustomerViewCreateFacilitiesPropertiesTestLocation2.js
* @requires test16SysAdminViewEditLocationProperty.js
* @requires test17SysAdminViewEditLocationProperty1Location.js
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");


module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},		
	
	"Input the Customer" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//input[@id='search']", 2000)
			.setValue("//input[@id='search']", 'Oranj')

			.pause(1000)
	},

	"Click Go" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@id='btnGo']", 4000)
			.click("//a[@id='btnGo']")
			.pause(1000)
	},
	
	"Click the Locations tab" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@title='Locations']", 4000)
			.click("//a[@title='Locations']")
			.pause(1000)
	},

	
	"Click the Option Menu for the Location FacProp2" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("(//li[@class='Child'])[contains(@title,'FacProp2')]//a[@title='Option Menu']", 4000)
			.click("(//li[@class='Child'])[contains(@title,'FacProp2')]//a[@title='Option Menu']")
			
	},	
	
	"Click Edit on the Option Menu for the Location FacProp2" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("(//li[@class='Child'])[contains(@title,'FacProp2')]/following-sibling::section//a[@title='Edit']", 4000)
			.click("(//li[@class='Child'])[contains(@title,'FacProp2')]/following-sibling::section//a[@title='Edit']")
			.pause(1000)
	},	
	
	"Click the Checkbox beside the Route 20 Property" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//ul[@id='locPropSelectedList']/li[contains(text(),'Route 20')]", 4000)
			.click("//ul[@id='locPropSelectedList']/li[contains(text(),'Route 20')]")
			.pause(1000)
	},	
	
	"Assert that the Route 20 Property is no longer in the Selected Properties list" : function (browser) 
	{
		browser
			.useXpath()
			.assert.elementNotPresent("//ul[@id='locPropSelectedList']/li[@class='selected' and contains(text(),'Route 20')]")
			.pause(1000)
	},	
		
	
	"Click the Save Button to save the removing of the Route 20 Property from the Location FacProp2" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='linkButtonFtr textButton save']", 1000)
			.click("//a[@class='linkButtonFtr textButton save']")
			.pause(10000)
	},

	
	"Assert the Route 20 Property is NOT listed in Location Details of FacProp2" : function (browser) 
	{
		browser
			.useXpath()
			.pause(3000)
			.assert.visible("//section[@id='locationDetails']//h2[contains(text(),'FacProp2')]")
			.pause(1000)
			.assert.elementNotPresent("//ul[@id='propChildList']/li[contains(text(),'Route 20')]")
			.pause(1000)
	},	
	
	"Logout" : function (browser)
	
	{
		browser.logout();
	},
	
};