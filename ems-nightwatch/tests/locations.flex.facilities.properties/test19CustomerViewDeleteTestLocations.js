/**
* @summary Customer View: Settings: Locations: Delete Test Locations
* @since 7.3.5
* @version 1.0
* @author Paul Breland
* @see US723
* @requires test01CustomerViewCreateFacilitiesPropertiesTestLocation1.js
* @requires test02CustomerViewCreateFacilitiesPropertiesTestLocation2.js
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("FlexChildName");
var password = data("Password");


module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.initialLogin(username, password, "password")
			.assert.title('Digital Iris - Main (Dashboard)');
	},		
			
		
	"Click Settings on the left sidebar menu" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//nav[@id='main']/section[@class='bottomSection']/a/img", 4000)
			.click("//nav[@id='main']/section[@class='bottomSection']/a/img")
			.pause(1000)
	},

	"Click the Locations tab on the Settings page" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@title='Locations']", 4000)
			.click("//a[@title='Locations']")
			.pause(1000)
	},
	
	"Click the Option Menu for the Location FacProp1" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("(//li[@class='Child'])[contains(@title,'FacProp1')]//a[@title='Option Menu']", 4000)
			.click("(//li[@class='Child'])[contains(@title,'FacProp1')]//a[@title='Option Menu']")
			
	},	
	
	"Click Delete on the Option Menu for the Location FacProp1" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[contains(text(),'FacProp1')]/../following-sibling::section[1]/section/a[@class='delete btnDeleteLocation']", 4000)
			.click("//section[contains(text(),'FacProp1')]/../following-sibling::section[1]/section/a[@class='delete btnDeleteLocation']")
			.pause(1000)
	},	

	"Click Delete in the popup to confirm deleting Location FacProp1" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//button/span[contains(.,'Delete')]", 4000)
			.click("//button/span[contains(.,'Delete')]")
			.pause(1000)
	},		
	
	"Click the Option Menu for the Location FacProp2" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("(//li[@class='Child'])[contains(@title,'FacProp2')]//a[@title='Option Menu']", 4000)
			.click("(//li[@class='Child'])[contains(@title,'FacProp2')]//a[@title='Option Menu']")
			
	},	
	
	"Click Delete on the Option Menu for the Location FacProp2" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("(//li[@class='Child'])[contains(@title,'FacProp2')]/following-sibling::section//a[@title='Delete']", 4000)
			.click("(//li[@class='Child'])[contains(@title,'FacProp2')]/following-sibling::section//a[@title='Delete']")
			.pause(1000)
	},	

	"Click Delete in the popup to confirm deleting Location FacProp2" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//button/span[contains(.,'Delete')]", 4000)
			.click("//button/span[contains(.,'Delete')]")
			.pause(3000)
	},		
	
	"Assert Locations FacProp1 and FacProp2 are no longer present" : function (browser) 
	{
		browser
			.useXpath()
			.assert.elementNotPresent("//li[contains(.,'FacProp1')]")
			.pause(2000)
			.assert.elementNotPresent("//li[contains(.,'FacProp2')]")
	},			
	
	"Logout" : function (browser)
	
	{
		browser.logout();
	},
	
};