/**
* @summary Customer View: Settings: Locations: Edit Location: ensure a Facility can only be assigned to one Location at a time
* @since 7.3.5
* @version 1.0
* @author Paul Breland
* @see US723
* @requires test01CustomerViewCreateFacilitiesPropertiesTestLocation1.js
* @requires test02CustomerViewCreateFacilitiesPropertiesTestLocation2.js
* @requires test07CustomerViewEditLocationFacility.js
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("FlexChildName");
var password = data("Password");


module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.initialLogin(username, password, "password")
			.assert.title('Digital Iris - Main (Dashboard)');
	},		
			
			
	"Click Settings on the left sidebar menu" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//nav[@id='main']/section[@class='bottomSection']/a/img", 4000)
			.click("//nav[@id='main']/section[@class='bottomSection']/a/img")
			.pause(1000)
	},

	"Click the Locations tab on the Settings page" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@title='Locations']", 4000)
			.click("//a[@title='Locations']")
			.pause(1000)
	},
	
	"Click the Option Menu for the Location FacProp2" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("(//li[@class='Child'])[contains(@title,'FacProp2')]//a[@title='Option Menu']", 4000)
			.click("(//li[@class='Child'])[contains(@title,'FacProp2')]//a[@title='Option Menu']")
			
	},	
	
	"Click Edit on the Option Menu for the Location FacProp2" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("(//li[@class='Child'])[contains(@title,'FacProp2')]/following-sibling::section//a[@title='Edit']", 4000)
			.click("(//li[@class='Child'])[contains(@title,'FacProp2')]/following-sibling::section//a[@title='Edit']")
			.pause(1000)
	},	
	
	"Click the Checkbox beside the 700 PIERCE LOT Facility" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//ul[@id='locFacFullList']/li[contains(text(),'700 PIERCE LOT')]", 4000)
			.click("//ul[@id='locFacFullList']/li[contains(text(),'700 PIERCE LOT')]")
			.pause(1000)
	},	
	
	"Assert the 700 PIERCE LOT Facility is now in the Selected Facilities list" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//ul[@id='locFacSelectedList']/li[@class='active  selected' and contains(text(),'700 PIERCE LOT')]", 4000)
			.assert.visible("//ul[@id='locFacSelectedList']/li[@class='active  selected' and contains(text(),'700 PIERCE LOT')]")
			.pause(1000)
	},	
		
	
	"Click the Save Button to save the assigning of the 700 PIERCE LOT Facility to the Location FacProp2" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='linkButtonFtr textButton save']", 1000)
			.click("//a[@class='linkButtonFtr textButton save']")
			.pause(6000)
	},
	
	"Assert the 700 PIERCE LOT Facility is now listed in Location Details of FacProp2" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//ul[@id='facChildList']/li[contains(text(),'700 PIERCE LOT')]", 4000)
			.assert.visible("//ul[@id='facChildList']/li[contains(text(),'700 PIERCE LOT')]")
			.assert.visible("//section[@id='locationDetails']//h2[contains(text(),'FacProp2')]")
			.pause(3000)
	},	

	"Click the Location FacProp1 to bring up Location Details for it" : function (browser)
	
	{
		browser
			.useXpath()
			.pause(1000)
			.waitForElementVisible("(//li[@class='Child'])[contains(@title,'FacProp1')]", 1000)
			.click("(//li[@class='Child'])[contains(@title,'FacProp1')]")
			.pause(5000)
	},
	
	"Assert the 700 PIERCE LOT Facility is NOT listed in Location Details of FacProp1" : function (browser) 
	{
		browser
			.useXpath()
			.pause(1000)
			.assert.visible("//section[@id='locationDetails']//h2[contains(text(),'FacProp1')]")
			.assert.elementNotPresent("//ul[@id='facChildList']/li[contains(text(),'700 PIERCE LOT')]")
			.pause(1000)
	},	
	
	"Logout" : function (browser)
	
	{
		browser.logout();
	},
	
};