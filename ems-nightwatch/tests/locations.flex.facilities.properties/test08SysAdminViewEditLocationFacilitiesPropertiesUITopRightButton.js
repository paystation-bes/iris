/**
* @summary System Admin View: Settings: Locations: Edit Location: check UI for Facilities and Properties via the top right button on Location info
* @since 7.3.5
* @version 1.0
* @author Paul Breland
* @see US723
* @requires test01CustomerViewCreateFacilitiesPropertiesTestLocation1.js
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");


module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},		
			
		
	"Input the Customer" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//input[@id='search']", 2000)
			.setValue("//input[@id='search']", 'Oranj')

			.pause(5000)
	},

	"Click Go" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@id='btnGo']", 4000)
			.click("//a[@id='btnGo']")
			.pause(5000)
	},
	
	"Click the Locations tab" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@title='Locations']", 4000)
			.click("//a[@title='Locations']")
			.pause(5000)
	},

	"Click the Location FacProp1 to display its information" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("(//li[@class='Child'])[contains(@title,'FacProp1')]", 4000)
			.click("(//li[@class='Child'])[contains(@title,'FacProp1')]")
			.pause(5000)
	},

	"Click the Option Menu at the top right of the Location information window for Location FacProp1" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@id='opBtn_pageContent']/img", 4000)
			.click("//a[@id='opBtn_pageContent']/img")
			.pause(5000)
	},

	"Click Edit in the Option Menu" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='menu_pageContent']/section[@class='menuOptions innerBorder']/a[@class='edit btnEditLocation']", 4000)
			.click("//section[@id='menu_pageContent']/section[@class='menuOptions innerBorder']/a[@class='edit btnEditLocation']")
			.pause(5000)
	},
	
	"Assert the Facilities section header along with All Facilities and Selected Facilities subheaders" : function (browser)
	
	{
		browser
			.useXpath()
			.assert.visible("//section[@id='formFacilityChildren']/h3[contains(text(),'Facilities')]")
			.assert.visible("//section[contains(text(),'All Facilities:')]")
			.assert.visible("//section[contains(text(),'Selected Facilities:')]")
			.pause(5000)
	},
	
	"Assert the Properties section header along with All Properties and Selected Properties subheaders" : function (browser)
	
	{
		browser
			.useXpath()
			.assert.visible("//section[@id='formPropertyChildren']/h3[contains(text(),'Properties')]")
			.assert.visible("//section[contains(text(),'All Properties:')]")
			.assert.visible("//section[contains(text(),'Selected Properties:')]")
			.assert.visible("//ul[@id='locFacFullList']/li[contains(text(),'700 PIERCE LOT')]")
			.pause(5000)
	},

		"Assert the presence of Facilities and Properties within their respective lists" : function (browser)
	
	{
		browser
			.useXpath()
			.assert.visible("//ul[@id='locFacFullList']/li[contains(text(),'700 PIERCE LOT')]")
			.assert.visible("//ul[@id='locPropFullList']/li[contains(text(),'Route 20')]")			
			.pause(5000)
	},
	
	"Click the Cancel Button to leave the Edit Location form" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//article[@id='locationForm']/section[@class='btnSet']/a[@class='linkButton textButton cancel']", 4000)
			.click("//article[@id='locationForm']/section[@class='btnSet']/a[@class='linkButton textButton cancel']")
			.pause(5000)
	},
	
	"Logout" : function (browser)
	
	{
		browser.logout();
	},
	
};