/**
* @summary System Admin View: Settings: Locations: Edit Location: ensure a Facility can only be assigned to one Location at a time
* @since 7.3.5
* @version 1.0
* @author Paul Breland
* @see US723
* @requires test01CustomerViewCreateFacilitiesPropertiesTestLocation1.js
* @requires test02CustomerViewCreateFacilitiesPropertiesTestLocation2.js
* @requires test13SysAdminViewEditLocationFacility.js
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");


module.exports = {
	tags: ['smoketag', 'completetesttag'],
	"Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},		
	
	"Input the Customer" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//input[@id='search']", 2000)
			.setValue("//input[@id='search']", 'Oranj')
			.pause(4000)
	},

	"Click Go" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@id='btnGo']", 4000)
			.click("//a[@id='btnGo']")
			.pause(3000)
	},
	
	"Click the Locations tab" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@title='Locations']", 4000)
			.click("//a[@title='Locations']")
			.pause(3000)
	},

	
	"Click the Option Menu for the Location FacProp2" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("(//li[@class='Child'])[contains(@title,'FacProp2')]//a[@title='Option Menu']", 4000)
			.click("(//li[@class='Child'])[contains(@title,'FacProp2')]//a[@title='Option Menu']")
			
	},	
	
	"Click Edit on the Option Menu for the Location FacProp2" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("(//li[@class='Child'])[contains(@title,'FacProp2')]/following-sibling::section//a[@title='Edit']", 4000)
			.click("(//li[@class='Child'])[contains(@title,'FacProp2')]/following-sibling::section//a[@title='Edit']")
			.pause(3000)
	},	
	
	"Click the Checkbox beside the 700 PIERCE LOT Facility" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//ul[@id='locFacFullList']/li[contains(text(),'700 PIERCE LOT')]", 4000)
			.click("//ul[@id='locFacFullList']/li[contains(text(),'700 PIERCE LOT')]")
			.pause(3000)
	},	
	
	"Assert the 700 PIERCE LOT Facility is now in the Selected Facilities list" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//ul[@id='locFacSelectedList']/li[@class='active  selected' and contains(text(),'700 PIERCE LOT')]", 4000)
			.assert.visible("//ul[@id='locFacSelectedList']/li[@class='active  selected' and contains(text(),'700 PIERCE LOT')]")
			.pause(3000)
	},	
		
	
	"Click the Save Button to save the assigning of the 700 PIERCE LOT Facility to the Location FacProp2" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='linkButtonFtr textButton save']", 3000)
			.click("//a[@class='linkButtonFtr textButton save']")
			.pause(10000)
	},
	
	"Assert the 700 PIERCE LOT Facility is now listed in Location Details of FacProp2" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//ul[@id='facChildList']/li[contains(text(),'700 PIERCE LOT')]", 4000)
			.assert.visible("//ul[@id='facChildList']/li[contains(text(),'700 PIERCE LOT')]")
			.pause(1000)
			.assert.visible("//section[@id='locationDetails']//h2[contains(text(),'FacProp2')]")
			.pause(3000)
	},	

	"Click the Location FacProp1 to bring up Location Details for it" : function (browser)
	
	{
		browser
			.useXpath()
			.waitForElementVisible("(//li[@class='Child'])[contains(@title,'FacProp1')]", 3000)
			.click("(//li[@class='Child'])[contains(@title,'FacProp1')]")
			.pause(3000)
	},
	
	"Assert the 700 PIERCE LOT Facility is NOT listed in Location Details of FacProp1" : function (browser) 
	{
		browser
			.useXpath()
			.assert.visible("//section[@id='locationDetails']//h2[contains(text(),'FacProp1')]")
			.assert.elementNotPresent("//ul[@id='facChildList']/li[contains(text(),'700 PIERCE LOT')]")
			.pause(3000)
	},	
	
	"Logout" : function (browser)
	
	{
		browser.logout();
	},
	
};