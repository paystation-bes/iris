/**
 * @summary Enable/Disable a Customer and check if user can log into Customer Account when enabled/disabled
 * @since 7.5
 * @version 1.0
 * @author Johnson N
 **/
 
 
var data = require('../../data.js');

var devurl = data("URL");
var sysAdminUsername = data("SysAdminUserName");
var custAdminUsername = data("AdminUserName");
var password = data("Password");
var defaultpassword = data("DefaultPassword");
var customerName = "oranj"; 

module.exports = {
	
	"Initial login to Customer Admin": function(browser){
		console.log("User: " +custAdminUsername + "/" + password);
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(custAdminUsername, password, defaultpassword)
		.pause(2000);
		
	},
	
	"Logout of Customer Admin 1": function(browser){
		browser
		.useXpath()
        .waitForElementVisible("//a[@title='Logout']", 1000)
        .click("//a[@title='Logout']")
        .pause(1000)
		.assert.title("Digital Iris - Login.");
	},
	
	"Login to System Admin 1": function (browser) 
	{
		console.log("User: " +sysAdminUsername + "/" + password);
		browser	
		.systemAdminLogin(sysAdminUsername, password, defaultpassword);
	},
	
	"Search for oranj (Oranj Parent) 1" : function (browser) 
	{
		browser
			.useCss()
			.waitForElementVisible("#search", 2000)
			.setValue("#search", customerName)
			.pause(2000)
			.assert.visible("#btnGo")
			.click("#btnGo");

	},
	
	"Click Edit Customer button 1" : function(browser){
		browser
		.useCss()
		.waitForElementVisible("#btnEditCustomer", 2000)
		.click("#btnEditCustomer");
		
	},
	
	"Disable Customer" : function(browser){
		browser
		.useCss()
		.waitForElementVisible("#editCustomerFormArea", 2000)
		.useXpath()
		.assert.visible(".//*[@id='statusOptionArea']/label[contains(text(), 'Disabled')]")
		.click(".//*[@id='statusOptionArea']/label[contains(text(), 'Disabled')]")
		.assert.visible("//span[contains(text(), 'Update Customer') and @class='ui-button-text']")
		.click("//span[contains(text(), 'Update Customer') and @class='ui-button-text']");
	},
	
		"Logout of System Admin 1": function(browser){
		browser
		.useXpath()
        .waitForElementVisible("//a[@title='Logout']", 1000)
        .click("//a[@title='Logout']")
        .pause(1000)
		.assert.title("Digital Iris - Login.");
	},
	
	"Attempt to re-login to Customer Admin and verify Attention! popup": function(browser){
		browser
		.login(custAdminUsername, password)
		.pause(2000)
		.useCss()
		.assert.visible("#messageResponseAlertBox")
		.useXpath()
		.assert.visible("//*[@id='messageResponseAlertBox']/section/article[contains(text(), 'Sorry, access is denied.')]")
		.click("//*[@id='messageResponseAlertBox']/section/a[@title='Close']");
	},
	
	
		"Login to System Admin 2": function (browser) 
	{
		console.log("User: " +sysAdminUsername + "/" + password);
		browser	
		.pause(1000)
		.systemAdminLogin(sysAdminUsername, password, defaultpassword);
	},
	
	"Search for oranj (Oranj Parent) 2" : function (browser) 
	{
		browser
			.useCss()
			.waitForElementVisible("#search", 2000)
			.setValue("#search", customerName)
			.pause(2000)
			.assert.visible("#btnGo")
			.click("#btnGo");

	},
	
	"Click Edit Customer button 2" : function(browser){
		browser
		.useCss()
		.waitForElementVisible("#btnEditCustomer", 2000)
		.click("#btnEditCustomer");
		
	},
	
	"Enable Customer" : function(browser){
		browser
		.useCss()
		.waitForElementVisible("#editCustomerFormArea", 2000)
		.useXpath()
		.assert.visible(".//*[@id='statusOptionArea']/label[contains(text(), 'Enabled')]")
		.click(".//*[@id='statusOptionArea']/label[contains(text(), 'Enabled')]")
		.assert.visible("//span[contains(text(), 'Update Customer') and @class='ui-button-text']")
		.click("//span[contains(text(), 'Update Customer') and @class='ui-button-text']");
	},
	
	
	"Logout of System Admin 2": function(browser){
		browser
		.useXpath()
        .waitForElementVisible("//a[@title='Logout']", 1000)
        .click("//a[@title='Logout']")
        .pause(1000)
		.assert.title("Digital Iris - Login.");
	},
	
	"Login to Customer Admin" : function(browser){
		browser
		.initialLogin(custAdminUsername, password, defaultpassword)
		.pause(2000);
	},
	
		"Logout" : function (browser) 
		{
			browser.logout();
		},  
	
	
	
};