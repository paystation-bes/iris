/**
 * @summary Create Paystation for Customer on System Admin
 * @since 7.5
 * @version 1.0
 * @author Johnson N
 **/
 
var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var defaultpassword = data("DefaultPassword");
var customerName = "oranj";
var paystationSerialNumberToCreate = "500000070051";
 
 module.exports = {
	 
	 		"Login to System Admin" : function (browser) 
	{
		console.log("User: " + username + "/" + password);
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
		"Search for oranj (Oranj Parent)" : function (browser) 
	{
		browser
			.useCss()
			.waitForElementVisible("#search", 2000)
			.setValue("#search", customerName)
			.pause(2000)
			.assert.visible("#btnGo")
			.click("#btnGo");

	},
	
		"Navigate to Pay Stations Tab" : function (browser){
			browser
			.useXpath()
			.waitForElementVisible(".//a[@title='Pay Stations']", 3000)
			.click(".//a[@title='Pay Stations']");
		},
		
		
		
		
		"Click Add Paystation(s) button" : function (browser){
			browser
			.useCss()
			.waitForElementVisible("#btnAddPayStation", 2000)
			.click("#btnAddPayStation")
			.pause(1000)
			.assert.visible("#formSerialNumbers");
		},
		
		"Enter Paystation Serial Number(s) into Add Paystation form": function(browser){
			browser
			.useCss()
			.setValue("#formSerialNumbers", paystationSerialNumberToCreate)
			.useXpath()
			.click("//span[contains(text(), 'Add Pay Station') and @class='ui-button-text'] ");
		
		
		},
		"Verify Pay Station is now on list" : function(browser){
			browser
			.useXpath()
			.pause(3000)
			.assert.visible("//span[contains(text(),'" + paystationSerialNumberToCreate +"')]");
		},
		
		
			"Logout" : function (browser) 
		{
			browser.logout();
		},  
	
 };