/**
 * @summary Create All Possible Paystation types for Customer on System Admin
 * @since 7.5
 * @version 1.0
 * @author Johnson N
 **/
 
 var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var defaultpassword = data("DefaultPassword");
var customerName = "oranj";
//Note: a V1 Luke Paystation requires that the paystation serial number begins with 1
var v1LUKEPaystationSerial = "100000000055";
//Note: a SHELBY Paystation requires that the paystation serial number begins with 2
var SHELBYPaystationSerial = "200000000020";
//Note: a Radius LUKE Paystation requires that the paystation serial number begins with 3
var radiusLUKEPaystationSerial = "300000000020";
//Note: a LUKE II Paystation requires that the paystation serial number begins with 5
var LUKEIIPaystationSerial = "500000000030";
//Note: a FRODO Paystation requires that the paystation serial number begins with 6
var FRODOPaystationSerial = "600000000020";
//Note: a Test/Demo Paystation requires that the paystation serial number begins with 8
var testDemoPaystationSerial = "800000000020";
//Note: a Virtual Paystation requires that the paystation serial number begins with 9
var virtualPaystationSerial = "900000000020";
module.exports = {
	 
	 		"Login to System Admin" : function (browser) {
		console.log("User: " + username + "/" + password);
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
		"Search for oranj (Oranj Parent)" : function (browser){
		browser
			.useCss()
			.waitForElementVisible("#search", 2000)
			.setValue("#search", customerName)
			.pause(2000)
			.assert.visible("#btnGo")
			.click("#btnGo");
			},
	
		"Navigate to Pay Stations Tab" : function (browser){
			browser
			.useXpath()
			.waitForElementVisible(".//a[@title='Pay Stations']", 3000)
			.click(".//a[@title='Pay Stations']");
		},
		
		
		//Create V1 Luke Paystation
		
		"Click Add Paystation(s) button 1" : function (browser){
			browser
			.useCss()
			.waitForElementVisible("#btnAddPayStation", 2000)
			.click("#btnAddPayStation")
			.pause(1000)
			.assert.visible("#formSerialNumbers");
		},
		
		"Enter V1 LUKE Paystation Serial Number(s) into Add Paystation form": function(browser){
			browser
			.useCss()
			.setValue("#formSerialNumbers", v1LUKEPaystationSerial)
			.useXpath()
			.click("//span[contains(text(), 'Add Pay Station') and @class='ui-button-text'] ");
		
		
		},
		"Verify V1 Luke Pay Station is now on list" : function(browser){
			browser
			.useXpath()
			.pause(3000)
			.assert.visible("//span[contains(text(),'" + v1LUKEPaystationSerial +"')]");
		},
		
		"Verify V1 LUKE Pay Station Type" : function(browser){
			browser
			.useXpath()
			.click("//span[contains(text(),'" + v1LUKEPaystationSerial +"')]")
			.useCss()
			.waitForElementVisible("#infoBtn > a.tab", 5000)
			.click("#infoBtn > a.tab")
			.useXpath()
			.waitForElementVisible("//*[@id='infoArea']", 2000)
			.assert.containsText(".//*[@id='infoArea']/dl/section[@id='psTypeLine']/dd[@id = 'psType']", "V1 LUKE");
		},
		
		
		//Create SHELBY Paystation
		
				"Click Add Paystation(s) button 2" : function (browser){
			browser
			.useCss()
			.waitForElementVisible("#btnAddPayStation", 2000)
			.click("#btnAddPayStation")
			.pause(1000)
			.assert.visible("#formSerialNumbers");
		},
		
		"Enter SHELBY Paystation Serial Number(s) into Add Paystation form": function(browser){
			browser
			.useCss()
			.setValue("#formSerialNumbers", SHELBYPaystationSerial)
			.useXpath()
			.click("//span[contains(text(), 'Add Pay Station') and @class='ui-button-text'] ");
		
		
		},
		"Verify SHELBY Pay Station is now on list" : function(browser){
			browser
			.useXpath()
			.pause(3000)
			.assert.visible("//span[contains(text(),'" + SHELBYPaystationSerial +"')]");
		},
		
		"Verify SHELBY Pay Station Type" : function(browser){
			browser
			.useXpath()
			.click("//span[contains(text(),'" + SHELBYPaystationSerial +"')]")
			.useCss()
			.waitForElementVisible("#infoBtn > a.tab", 5000)
			.click("#infoBtn > a.tab")
			.useXpath()
			.waitForElementVisible("//*[@id='infoArea']", 2000)
			.assert.containsText(".//*[@id='infoArea']/dl/section[@id='psTypeLine']/dd[@id = 'psType']", "SHELBY");
		},
		
		
		//Create Radius LUKE Paystation
		
				"Click Add Paystation(s) button 3" : function (browser){
			browser
			.useCss()
			.waitForElementVisible("#btnAddPayStation", 2000)
			.click("#btnAddPayStation")
			.pause(1000)
			.assert.visible("#formSerialNumbers");
		},
		
		"Enter Radius LUKE Paystation Serial Number(s) into Add Paystation form": function(browser){
			browser
			.useCss()
			.setValue("#formSerialNumbers", radiusLUKEPaystationSerial)
			.useXpath()
			.click("//span[contains(text(), 'Add Pay Station') and @class='ui-button-text'] ");
		
		
		},
		"Verify Radius LUKE Pay Station is now on list" : function(browser){
			browser
			.useXpath()
			.pause(3000)
			.assert.visible("//span[contains(text(),'" + radiusLUKEPaystationSerial +"')]");
		},
		
		"Verify Radius LUKE Pay Station Type" : function(browser){
			browser
			.useXpath()
			.click("//span[contains(text(),'" + radiusLUKEPaystationSerial +"')]")
			.useCss()
			.waitForElementVisible("#infoBtn > a.tab", 5000)
			.click("#infoBtn > a.tab")
			.useXpath()
			.waitForElementVisible("//*[@id='infoArea']", 2000)
			.assert.containsText(".//*[@id='infoArea']/dl/section[@id='psTypeLine']/dd[@id = 'psType']", "Radius LUKE");
		},
		
		
		//Add LUKE II Paystation
		
				"Click Add Paystation(s) button 4" : function (browser){
			browser
			.useCss()
			.waitForElementVisible("#btnAddPayStation", 2000)
			.click("#btnAddPayStation")
			.pause(1000)
			.assert.visible("#formSerialNumbers");
		},
		
		"Enter LUKE II Paystation Serial Number(s) into Add Paystation form": function(browser){
			browser
			.useCss()
			.setValue("#formSerialNumbers", LUKEIIPaystationSerial)
			.useXpath()
			.click("//span[contains(text(), 'Add Pay Station') and @class='ui-button-text'] ");
		
		},
		
		
		"Verify LUKE II Pay Station is now on list" : function(browser){
			browser
			.useXpath()
			.pause(3000)
			.assert.visible("//span[contains(text(),'" + LUKEIIPaystationSerial +"')]");
		},
		
		"Verify LUKE II Pay Station Type" : function(browser){
			browser
			.useXpath()
			.click("//span[contains(text(),'" + LUKEIIPaystationSerial +"')]")
			.useCss()
			.waitForElementVisible("#infoBtn > a.tab", 5000)
			.click("#infoBtn > a.tab")
			.useXpath()
			.waitForElementVisible("//*[@id='infoArea']", 2000)
			.assert.containsText(".//*[@id='infoArea']/dl/section[@id='psTypeLine']/dd[@id = 'psType']", "LUKE II");
		},
		
		//Create FRODO Paystation
		
				"Click Add Paystation(s) button 5" : function (browser){
			browser
			.useCss()
			.waitForElementVisible("#btnAddPayStation", 2000)
			.click("#btnAddPayStation")
			.pause(1000)
			.assert.visible("#formSerialNumbers");
		},
		
		"Enter FRODO Paystation Serial Number(s) into Add Paystation form": function(browser){
			browser
			.useCss()
			.setValue("#formSerialNumbers", FRODOPaystationSerial)
			.useXpath()
			.click("//span[contains(text(), 'Add Pay Station') and @class='ui-button-text'] ");
		
		
		},
		"Verify FRODO Pay Station is now on list" : function(browser){
			browser
			.useXpath()
			.pause(3000)
			.assert.visible("//span[contains(text(),'" + FRODOPaystationSerial +"')]");
		},
		
		"Verify FRODO Pay Station Type" : function(browser){
			browser
			.useXpath()
			.click("//span[contains(text(),'" + FRODOPaystationSerial +"')]")
			.useCss()
			.waitForElementVisible("#infoBtn > a.tab", 5000)
			.click("#infoBtn > a.tab")
			.useXpath()
			.waitForElementVisible("//*[@id='infoArea']", 2000)
			.assert.containsText(".//*[@id='infoArea']/dl/section[@id='psTypeLine']/dd[@id = 'psType']", "FRODO");
		},
		
		
		//Create Test/Demo Paystation
		
				"Click Add Paystation(s) button 6" : function (browser){
			browser
			.useCss()
			.waitForElementVisible("#btnAddPayStation", 2000)
			.click("#btnAddPayStation")
			.pause(1000)
			.assert.visible("#formSerialNumbers");
		},
		
		"Enter Test/Demo Paystation Serial Number(s) into Add Paystation form": function(browser){
			browser
			.useCss()
			.setValue("#formSerialNumbers", testDemoPaystationSerial)
			.useXpath()
			.click("//span[contains(text(), 'Add Pay Station') and @class='ui-button-text'] ");
		
		
		},
		"Verify Test/Demo Pay Station is now on list" : function(browser){
			browser
			.useXpath()
			.pause(3000)
			.assert.visible("//span[contains(text(),'" + testDemoPaystationSerial +"')]");
		},
		
		"Verify Test/Demo Pay Station Type" : function(browser){
			browser
			.useXpath()
			.click("//span[contains(text(),'" + testDemoPaystationSerial +"')]")
			.useCss()
			.waitForElementVisible("#infoBtn > a.tab", 5000)
			.click("#infoBtn > a.tab")
			.useXpath()
			.waitForElementVisible("//*[@id='infoArea']", 2000)
			.assert.containsText(".//*[@id='infoArea']/dl/section[@id='psTypeLine']/dd[@id = 'psType']", "Test/Demo");
		},
		
		//Create Virtual Paystation
			"Click Add Paystation(s) button 7" : function (browser){
			browser
			.useCss()
			.waitForElementVisible("#btnAddPayStation", 2000)
			.click("#btnAddPayStation")
			.pause(1000)
			.assert.visible("#formSerialNumbers");
		},
		
		"Enter Virtual Paystation Serial Number(s) into Add Paystation form": function(browser){
			browser
			.useCss()
			.setValue("#formSerialNumbers", virtualPaystationSerial)
			.useXpath()
			.click("//span[contains(text(), 'Add Pay Station') and @class='ui-button-text'] ");
		
		
		},
		"Verify Virtual Pay Station is now on list" : function(browser){
			browser
			.useXpath()
			.pause(3000)
			.assert.visible("//span[contains(text(),'" + virtualPaystationSerial +"')]");
		},
		
		"Verify Virtual Pay Station Type" : function(browser){
			browser
			.useXpath()
			.click("//span[contains(text(),'" + virtualPaystationSerial +"')]")
			.useCss()
			.waitForElementVisible("#infoBtn > a.tab", 5000)
			.click("#infoBtn > a.tab")
			.useXpath()
			.waitForElementVisible("//*[@id='infoArea']", 2000)
			.assert.containsText(".//*[@id='infoArea']/dl/section[@id='psTypeLine']/dd[@id = 'psType']", "Virtual");
		},
		
			"Logout" : function (browser) 
		{
			browser.logout();
		},  
	
 };