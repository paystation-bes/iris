/**
 * @summary Centralized Child User Management: Verify that Parent with no child role 
 * @since 7.3.4
 * @version 1.2
 * @author Nadine B, Thomas C
 * @see US693: TC890
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ParentUserName");
var password = data("Password");

/* NOTE: Please don't add children to this user */

var firstName = "ParentUser" + Math.floor(Math.random() * 100) + 1;
var lastName = "WithNoChildren"+ Math.floor(Math.random() * 100) + 1;
var email = "idonthavechildren"+ Math.floor(Math.random() * 100) + 1+"@qaAutomation1";
var password2 = data("Password2");
var parentRole = " Corporate Administrator";
var fullName = firstName + " " + lastName;

var count = 0;

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test16VerifyAddingParentWithNoChildrenUserDataAndSwitchbar: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	 * @description Navigates to Users
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test16VerifyAddingParentWithNoChildrenUserDataAndSwitchbar: Navigate to Users" : function (browser) {
		browser
		.navigateTo("Settings")
		.navigateTo("Users")
		.pause(1000)
	},

	/**
	 * @description Clicks on the Add User button
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test16VerifyAddingParentWithNoChildrenUserDataAndSwitchbar: Click on '+' button" : function (browser) {
		var addUser = "//a[@id='btnAddUser']";
		browser
		.useXpath()
		.waitForElementVisible(addUser, 2000)
		.click(addUser)
	},

	/**
	 * @description Enters the Name, Email and Password for the user
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test16VerifyAddingParentWithNoChildrenUserDataAndSwitchbar: Enter Details" : function (browser) {
		browser
		.useXpath()
		.enterUserDetials(firstName, lastName, email, password2, true)
		.pause(2000)
	},

	/**
	 * @description Selects parent roles for the user
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test16VerifyAddingParentWithNoChildrenUserDataAndSwitchbar: Select Roles" : function (browser) {

		var parentXpath = "//ul[@id='userAllRoles']/li[@class='active ' and text()='" + parentRole + "']";

		var parentXpath = "//ul[@id='userAllRoles']/li[contains(text(),'" + parentRole + "')]";
		var selectedParentXpath = "//ul[@id='userSelectedRoles']/li[@class='active  selected' and contains(text(),'" + parentRole + "')]"
			browser
			.useXpath()
			.pause(2000)
			.assert.visible(parentXpath)
			.click(parentXpath)
			.pause(2000)
			.waitForElementVisible(selectedParentXpath, 5000)
	},

	/**
	 * @description Saves the new User
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test07AddUserBothRoles: Click Save" : function (browser) {
		var saveBtn = "//a[@class='linkButtonFtr textButton save']";
		var error = "//li[contains(text(),'same Email Address already exists')";
		browser
		.useXpath()
		.assert.visible(saveBtn)
		.click(saveBtn)
		.assert.elementNotPresent(error)
		},

	/**
	 * @description Verifies the added users details are correct
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test16VerifyAddingParentWithNoChildrenUserDataAndSwitchbar: Verify Details" : function (browser) {
		var xpath = "//span[contains(text(),'" + fullName + "')]";
		var name = "//dd[@id='detailUserDisplayName' and contains(text(),'" + fullName + "')]";
		var status = "//dd[@id='detailUserStatus' and contains(text(),'Enabled')]";
		var uname = "//dd[@id='detailUserName' and contains(text(),'" + email + "')]";
		var selectedParentRole = "//ul[@id='roleChildList']/li[contains(text(),'" + parentRole.trim() + "')]";
		browser
		.useXpath()
		.pause(1000)
		.click(xpath)
		.waitForElementVisible(name, 1000)
		.assert.visible(status)
		.assert.visible(uname)
		.assert.visible(selectedParentRole)
	},

	/**
	 * @description Logs out of the admin user
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test16VerifyAddingParentWithNoChildrenUserDataAndSwitchbar: Logout admin" : function (browser) {
		browser.logout();
	},

	/**
	 * @description Logs in as the new user
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test16VerifyAddingParentWithNoChildrenUserDataAndSwitchbar: Login as newly created parent with no children" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(email, password, password2)
	},

	/**
	 * @description That the new user is a parent user and does not have the switch bar
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test16VerifyAddingParentWithNoChildrenUserDataAndSwitchbar: Verify New User Data" : function (browser) {
		browser
		.useXpath()
		.navigateTo("Settings")
		.assertAllParentSections()
		.assert.elementNotPresent("//*[@id='parentTitle']")
		.assert.elementNotPresent("//*[@id='parentChildSwitchFrmArea']")
	},

	/**
	 * @description Logs out
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test16VerifyAddingParentWithNoChildrenUserDataAndSwitchbar: Logout this parent" : function (browser) {
		browser.logout();
	},

};
