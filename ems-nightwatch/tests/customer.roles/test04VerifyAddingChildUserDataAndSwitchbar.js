/**
 * @summary Centralized Child User Management: Add user with Only Child role and access to Child Company
 * @since 7.3.5
 * @version 1.1
 * @author Nadine B, Thomas C
 * @see US693: TC891
 */

var data = require('../../data.js');

var devurl = data("URL");
var password = data("Password");
var username = data("ParentUserName");
var firstName = "SomeUser" + Math.floor((Math.random() * 100) + 1);
var lastName = "LastName" + Math.floor((Math.random() * 100) + 1);
var email = "someuser" + Math.floor((Math.random() * 100) + 1)
	 + "@qaAutomation1";
var password2 = data("Password2");
var childEmail1 = "qa1child";
var childEmail2 = "qa1child2";
var childRole = " Administrator";
var parentRole = " Corporate Administrator";
var fullName = firstName + " " + lastName;
var count = 0;

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test04VerifyAddingChildUserDataAndSwitchbar: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	 * @description Navigates to Users
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test04VerifyAddingChildUserDataAndSwitchbar: Navigate to Users" : function (browser) {
		browser
		.useXpath()
		.navigateTo("Settings")
		.navigateTo("Users")
	},

	/**
	 * @description Clicks on the Add User button
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test04VerifyAddingChildUserDataAndSwitchbar: Click on '+' button" : function (browser) {
		var addUserBtn = "//a[@id='btnAddUser']";
		browser
		.useXpath()
		.waitForElementVisible(addUserBtn, 2000)
		.click(addUserBtn)
	},

	/**
	 * @description Enters the Name, Email and Password for the user
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test04VerifyAddingChildUserDataAndSwitchbar: Enter Details" : function (browser) {
		browser.enterUserDetials(firstName, lastName, email, password2, true);
	},

	/**
	 * @description Select user role
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test04VerifyAddingChildUserDataAndSwitchbar: Select Child Role Only" : function (
		browser) {
		var childXpath = "//ul[@id='userAllRoles']/li[@class='active childRole' and contains(text(),'" + childRole + "')]";
		var selectedChildXpath = "//ul[@id='userSelectedRoles']/li[@class='active childRole selected' and contains(text(),'" + childRole + "')]"
			browser
			.useXpath()
			.assert.visible(childXpath)
			.click(childXpath)
			.waitForElementVisible(selectedChildXpath, 1000)
	},

	/**
	 * @description Select child Company
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test04VerifyAddingChildUserDataAndSwitchbar: Select Child Company" : function (browser) {
		var childCompany1 = "//li[@class='active' and contains(text(),'" + childEmail1 + "')]";
		var selectedChildCompany1 = "//ul[@id='companySelectedRoles']/li[@class='active selected' and contains(text(),'" + childEmail1 + "')]";
		var childCompany2 = "//li[@class='active' and contains(text(),'" + childEmail2 + "')]";
		var selectedChildCompany2 = "//ul[@id='companySelectedRoles']/li[@class='active selected' and contains(text(),'" + childEmail2 + "')]";

		browser
		.useXpath()
		.waitForElementVisible("//ul[@id='companyAllRoles']", 1000)
		.waitForElementVisible(childCompany1, 1000)
		.click(childCompany1)
		.waitForElementVisible(selectedChildCompany1, 1000)
		.waitForElementVisible("//ul[@id='companyAllRoles']", 1000)
		.waitForElementVisible(childCompany2, 1000)
		.click(childCompany2)
		.waitForElementVisible(selectedChildCompany2, 1000)
	},

	/**
	 * @description Saves selections
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test04VerifyAddingChildUserDataAndSwitchbar: Click Save" : function (browser) {
		var saveBtn = "//a[@class='linkButtonFtr textButton save']";
		var error = "//li[contains(text(),'same Email Address')]";
		var xpath = "//span[text()='" + fullName + "']";
		browser
		.useXpath()
		.assert.visible(saveBtn)
		.click(saveBtn)
		.assert.elementNotPresent(error)
		.waitForElementVisible(xpath, 3000)
	},

	/**
	 * @description Verify selections
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test04VerifyAddingChildUserDataAndSwitchbar: Verify Details" : function (browser) {
		var xpath = "//span[contains(text(),'" + fullName + "')]";
		var name = "//dd[@id='detailUserDisplayName' and contains(text(),'" + fullName + "')]";
		var status = "//dd[@id='detailUserStatus' and contains(text(),'Enabled')]";
		var uname = "//dd[@id='detailUserName' and contains(text(),'" + email + "')]";
		var selectedChildRole = "//ul[@id='roleChildList']/li[contains(text(),'" + childRole.trim() + "')]";
		browser
		.useXpath()
		.pause(1000)
		.click(xpath)
		.waitForElementVisible(name, 1000)
		.assert.visible(status)
		.assert.visible(uname)
		.assert.visible(selectedChildRole);
	},

	/**
	 * @description Logs the user out of Iris
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test04VerifyAddingChildUserDataAndSwitchbar: Logout" : function (browser) {
		browser.logout();
	},

	/**
	 * @description Logs the new user into Iris
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test04VerifyAddingChildUserDataAndSwitchbar: Login as new user" : function (browser) {
		console.log(email + password + password2)

		browser
		.url(devurl)
		.windowMaximize('current')
		.initialLogin(email, password, password2)
	},

	/**
	 * @description Verifies user data
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test04VerifyAddingChildUserDataAndSwitchbar: Verify Child Data" : function (browser) {
		var childSwitchBar = "//*[@id='parentChildSwitchFrmArea']";
		var childEmail = "//h2[@id='childTitle' and contains(text(),'" + childEmail1 + "')]";
		browser
		.useXpath()
		.pause(1000)
		.assert.visible("//a[@title='Settings']")
		.assert.visible("//a[@title='Reports']")
		.assert.visible("//a[@title='Maintenance']")
		.assert.visible("//a[@title='Collections']")
		.assert.visible("//a[@title='Settings']")
		.pause(1000)
		.waitForElementVisible(childEmail, 10000)
		.assert.visible(childSwitchBar)
	},

	/**
	 * @description Verifies Switchbar size
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test04VerifyAddingChildUserDataAndSwitchbar: Assert the Switchbar only contains 3 things (2 children and 'Select Company')" : function (browser) {
		var switchBarExpand = "//*[@id='childCoSwitchACExpand']";
		browser
		.useXpath()
		.assert.visible(switchBarExpand)
		.click(switchBarExpand)
		.elements("xpath", "//li[@class='ui-menu-item']", function (result) {
			browser.assert.equal(result.value.length, "03", "The Switchbar has the correct amount of items")
		});
	},

	/**
	 * @description Logs the new user out of Iris
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test04VerifyAddingChildUserDataAndSwitchbar: Logout new user" : function (browser) {
		browser.logout();
	},

};
