/**
 * @summary Centralized Child User Management: Switch between two children
 * @since 7.3.5
 * @version 1.1
 * @author Nadine B, Thomas C
 * @see US693
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ParentUserName");
var password = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test11ParentSwitchBetweenTwoChildren: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title("Digital Iris - Main (Dashboard)")
		.useXpath()
		.waitForElementVisible("//*[@id='parentTitle']", 3000)
		.getText("//*[@id='parentTitle']", function (result) {
			browser.assert.equal(result.value, "qa1parent")
		});
	},

	/**
	 * @description Switches to the qa1child child account
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test11ParentSwitchBetweenTwoChildren: Switch to qa1child Child branch" : function (browser) {
		var switchBar = "//section[@id='parentChildSwitchFrmArea']";
		var expandSwitchBar = "//input[@id='childCoSwitchAC']";
		var qa1ChildSB = "//a[text()='qa1child']";
		var switchBtn = "//a[@id='custSwitchBtn']";

		browser
		.useXpath()
		.waitForElementVisible(switchBar, 2000)
		.click(expandSwitchBar)
		.waitForElementVisible(qa1ChildSB, 2000)
		.click(qa1ChildSB)
		.assert.visible(switchBtn)
		.click(switchBtn)
		.pause(2000)
	},

	/**
	 * @description Verifies that child has correct areas
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test12ParentSwitchBetweenTwoChildren: Verify Parent switched to qa1child in the Switchbar and qa1child menus are present" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementVisible("//h2[@id='childTitle' and text()='qa1child']", 10000)
		.assertAllSections();
	},

	/**
	 * @description Navigates to settings and verifies all sections exist
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test12ParentSwitchBetweenTwoChildren: Click Settings" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.navigateTo("Settings")
		.assertAllSettingsTabs();
	},

	/**
	 * @description Switches to the qa1child child account
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test11ParentSwitchBetweenTwoChildren: Switch to qa1child2 Child branch" : function (browser) {
		var switchBar = "//section[@id='parentChildSwitchFrmArea']";
		var expandSwitchBar = "//input[@id='childCoSwitchAC']";
		var qa1ChildSB = "//a[text()='qa1child2']";
		var switchBtn = "//a[@id='custSwitchBtn']";

		browser
		.useXpath()
		.waitForElementVisible(switchBar, 2000)
		.click(expandSwitchBar)
		.waitForElementVisible(qa1ChildSB, 2000)
		.click(qa1ChildSB)
		.assert.visible(switchBtn)
		.click(switchBtn)
		.pause(2000)
	},

	/**
	 * @description Verifies that Switch was successful
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test12ParentSwitchBetweenTwoChildren: Verify Parent switched to qa1child2 in the Switchbar and qa1child2 menus are present" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementVisible("//h2[@id='childTitle' and text()='qa1child2']", 10000)
		.assertAllSections();
	},

	/**
	 * @description Navigates to settings and verifies all sections exist
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test12ParentSwitchBetweenTwoChildren: Click Settings" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.navigateTo("Settings")
		.assertAllSettingsTabs();
	},

	/**
	 * @description Switches back to parent company
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test12ParentSwitchBetweenTwoChildren: Switch back to Parent Company" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//input[@id='childCoSwitchAC']", 10000)
		.click("//input[@id='childCoSwitchAC']")
		.waitForElementVisible("//ul/li[@class='ui-menu-item']/a[text()='qa1parent']", 10000)
		.click("//ul/li[@class='ui-menu-item']/a[text()='qa1parent']")
		.assert.visible("//a[@id='custSwitchBtn']")
		.click("//a[@id='custSwitchBtn']")
	},

	/**
	 * @description Verifies that switch was successful
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test12ParentSwitchBetweenTwoChildren: Verify the Switchbar and Parent menus are present" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementVisible("//*[@id='parentTitle']", 3000)
		.getText("//*[@id='parentTitle']", function (result) {
			browser.assert.equal(result.value, "qa1parent")
		});
		browser.assertAllParentSections();
		},

	/**
	 * @description Logs out
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test12ParentSwitchBetweenTwoChildren: Logout" : function (browser) {
		browser.logout();
	},

};
