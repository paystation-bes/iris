/**
 * @summary Centralized Child User Management: Child Acc Can Add User with Role Created in Parent Acc
 * @since 7.3.5
 * @version 1.0
 * @author Mandy F
 * @see US693: TC975
 * @requires test08ChildRoleAppearsInChildAcc.js
 **/

var data = require('../../data.js');

var devurl = data("URL");
var childUsername = data("ChildUserName");
var password = data("Password");

// save the parent name in a variable to pick a parent created child role
var parentName = new String();
// random number generator between 1 and 1000 for a different user each time script runs
var randomNum = Math.floor((Math.random() * 1000) + 1);
// data for a user
var firstName = "Random";
var lastName = "Account" + randomNum;
var emailAddress = "ran@acc" + randomNum;
var tempPW = data("Password2");

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test09ChildRoleAppearsInChildAcc: Login to Child Account" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(childUsername, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	 * @description Enters into Settings: Users: User Account page
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test09ChildRoleAppearsInChildAcc: Entering Child Account Settings: Users: User Account" : function (browser) {
		var settingsTab = "//nav[@id='main']//a[@title='Settings']";
		var usersTab = "//a[@title='Users']";
		var userAccTab = "//a[@title='User Account']";
		var parentAccName = "//em";
		browser
		.useXpath()
		.waitForElementVisible(settingsTab, 2000)
		.click(settingsTab)
		.waitForElementVisible(usersTab, 2000)
		.click(usersTab)
		.waitForElementVisible(userAccTab, 2000)
		.click(userAccTab)
		// get parent name to pick a role created by parent later
		.getText(parentAccName, function (result) {
			parentName = result.value;
		})
		.pause(1000);
	},

	/**
	 * @description Opens the add users form
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test09ChildRoleAppearsInChildAcc: Open Add Users Form" : function (browser) {
		var addOption = "//a[@id='btnAddUser']";
		browser
		.useXpath()
		.waitForElementVisible(addOption, 1000)
		.click(addOption)
		.pause(1000);
	},

	/**
	 * @description Fills in the information to add a user
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test09ChildRoleAppearsInChildAcc: Add User Information" : function (browser) {
		var firstNameForm = "//input[@id='formUserFirstName']";
		var lastNameForm = "//input[@id='formUserLastName']";
		var status = "//label[@class='checkboxLabel switch detailValue']";
		var emailAddressForm = "//input[@id='formUserName']";
		var tempPWForm = "//input[@id='newPassword']";
		var tempPWConfirm = "//input[@id='c_newPassword']";
		var roles = "//ul[@id='userAllRoles']//em[contains(text(), '" + parentName + "')]";
		var saveBtn = "//a[@class='linkButtonFtr textButton save']";
		browser
		.useXpath()
		.waitForElementVisible(firstNameForm, 1000)
		.setValue(firstNameForm, firstName)
		.waitForElementVisible(lastNameForm, 1000)
		.setValue(lastNameForm, lastName)
		.waitForElementVisible(status, 1000)
		.click(status)
		.waitForElementVisible(emailAddressForm, 1000)
		.setValue(emailAddressForm, emailAddress)
		.waitForElementVisible(tempPWForm, 1000)
		.setValue(tempPWForm, tempPW)
		.waitForElementVisible(tempPWConfirm, 1000)
		.setValue(tempPWConfirm, tempPW)
		.waitForElementVisible(roles, 1000)
		.click(roles)
		.waitForElementVisible(saveBtn, 1000)
		.click(saveBtn)
		.pause(2000);
	},

	/**
	 * @description Verify user account created
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test09ChildRoleAppearsInChildAcc: Verify User Account Information" : function (browser) {
		var userAcc = "//span[contains(text(), '" + firstName + " " + lastName + "')]";
		var userAccDisplayName = "//dd[contains(text(), '" + firstName + " " + lastName + "')]";
		var status = "//dd[contains(text(), 'Enabled')]";
		var emailAddressConfirm = "//dd[contains(text(), '" + emailAddress + "')]";
		var roleName = "//ul[@id='roleChildList']//li[contains(text(), '" + parentName + "')]";
		browser
		.useXpath()
		.waitForElementVisible(userAcc, 1000)
		.click(userAcc)
		.pause(1000)
		.assert.visible(userAccDisplayName)
		.assert.visible(status)
		.assert.visible(emailAddressConfirm)
		.assert.visible(roleName)
		.pause(1000);
	},

	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test09ChildRoleAppearsInChildAcc: Logout" : function (browser) {
		browser.logout();
	},
};
