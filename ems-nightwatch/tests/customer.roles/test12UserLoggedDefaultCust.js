/**
 * @summary Centralized Child User Management: User Created in Parent Account is Logged into Correct Default Customers
 * @since 7.3.5
 * @version 1.0
 * @author Mandy F
 * @see US693: TC984 & TC985
 **/

var data = require('../../data.js');

var devurl = data("URL");
var parentUsername = data("ParentUserName");
var childUsername1 = data("ChildUserName");
var childUsername2 = data("ChildUserName3");
var password = data("Password");

// random number generator between 1 and 1000 for a different role name each time script runs
var randomNum = Math.floor((Math.random() * 1000) + 1);
// data for a user
var firstName = "Random";
var lastName = "Account" + randomNum;
var emailAddress = "ran@acc" + randomNum;
var tempPW = "Password$2";
// save child company names
var childName1 = new String();
var childName2 = new String();

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12UserLoggedDefaultCust: Login to Parent Account" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(parentUsername, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	 * @description Enters into Settings: Users: User Accounts page
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12UserLoggedDefaultCust: Entering Parent Account Settings: Users: User Accounts" : function (browser) {
		var settingsTab = "//nav[@id='main']//a[@title='Settings']";
		var usersTab = "//a[@title='Users']";
		var userAccountsTab = "//a[@title='User Account']";
		browser
		.useXpath()
		.waitForElementVisible(settingsTab, 2000)
		.click(settingsTab)
		.waitForElementVisible(usersTab, 2000)
		.click(usersTab)
		.waitForElementVisible(userAccountsTab, 2000)
		.click(userAccountsTab)
		.pause(1000);
	},

	/**
	 * @description Opens the add users form
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12UserLoggedDefaultCust: Open Add Users Form" : function (browser) {
		var addOption = "//a[@id='btnAddUser']";
		browser
		.useXpath()
		.waitForElementVisible(addOption, 1000)
		.click(addOption)
		.pause(1000);
	},

	/**
	 * @description Fills in the information to add a user
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12UserLoggedDefaultCust: Add User Information" : function (browser) {
		var firstNameForm = "//input[@id='formUserFirstName']";
		var lastNameForm = "//input[@id='formUserLastName']";
		var status = "//label[@class='checkboxLabel switch detailValue']";
		var emailAddressForm = "//input[@id='formUserName']";
		var tempPWForm = "//input[@id='newPassword']";
		var tempPWConfirm = "//input[@id='c_newPassword']";
		var roles = "//ul[@id='userAllRoles']//em[contains(text(), 'Child')]";
		var selectAllChildCompanies = "//label[@id='companySelectionLabel']";
		var defaultChildName1 = "(//ul[@id='companySelectedRoles']//li)[1]";
		var defaultChildName2 = "(//ul[@id='companySelectedRoles']//li)[2]";
		var expandChildOptions = "//a[@id='defaultCustExpand']";
		var firstChildCompany = "(//li[@role='presentation'])[2]";
		var saveBtn = "//a[@class='linkButtonFtr textButton save']";
		browser
		.useXpath()
		.waitForElementVisible(firstNameForm, 1000)
		.setValue(firstNameForm, firstName)
		.waitForElementVisible(lastNameForm, 1000)
		.setValue(lastNameForm, lastName)
		.waitForElementVisible(status, 1000)
		.click(status)
		.waitForElementVisible(emailAddressForm, 1000)
		.setValue(emailAddressForm, emailAddress)
		.waitForElementVisible(tempPWForm, 1000)
		.setValue(tempPWForm, tempPW)
		.waitForElementVisible(tempPWConfirm, 1000)
		.setValue(tempPWConfirm, tempPW)
		.waitForElementVisible(roles, 1000)
		.click(roles)
		.waitForElementVisible(selectAllChildCompanies, 1000)
		.click(selectAllChildCompanies)
		.getText(defaultChildName1, function (result) {
			childName1 = result.value.trim();
		})
		.getText(defaultChildName2, function (result) {
			childName2 = result.value.trim();
		})
		.waitForElementVisible(expandChildOptions, 1000)
		.click(expandChildOptions)
		.waitForElementVisible(firstChildCompany, 1000)
		.click(firstChildCompany)
		.waitForElementVisible(saveBtn, 1000)
		.click(saveBtn)
		.pause(2000);
	},

	/**
	 * @description Verify user account created
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12UserLoggedDefaultCust: Verify User Account Information" : function (browser) {
		var userAcc = "//span[contains(text(), '" + firstName + " " + lastName + "')]";
		var userAccDisplayName = "//dd[contains(text(), '" + firstName + " " + lastName + "')]";
		var status = "//dd[contains(text(), 'Enabled')]";
		var emailAddressConfirm = "//dd[contains(text(), '" + emailAddress + "')]";
		var roleName = "//ul[@id='roleChildList']//li[contains(text(), 'Child')]";
		var childCompany1 = "//ul[@id='compChildList']//li[contains(text(), '" + childName1 + "')]";
		var childCompany2 = "//ul[@id='compChildList']//li[contains(text(), '" + childName2 + "')]";
		browser
		.useXpath()
		.waitForElementVisible(userAcc, 1000)
		.click(userAcc)
		.pause(1000)
		.assert.visible(userAccDisplayName)
		.assert.visible(status)
		.assert.visible(emailAddressConfirm)
		.assert.visible(roleName)
		.assert.visible(childCompany1)
		.assert.visible(childCompany2)
		.pause(1000);
	},

	/**
	 * @description Logs the parent user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12UserLoggedDefaultCust: Log Parent Out of Iris" : function (browser) {
		var logoutBtn = "//a[@title='Logout']";
		browser
		.useXpath()
		.waitForElementVisible(logoutBtn, 1000)
		.click(logoutBtn)
		.pause(1000);
	},

	/**
	 * @description Logs the new user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12UserLoggedDefaultCust: Login to New User Account" : function (browser) {
		browser
		.useXpath()
		.login(emailAddress, tempPW)
		.pause(2000);
	},

	/**
	 * @description Change password of new user
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12UserLoggedDefaultCust: Change PW to New User Account" : function (browser) {
		var newPW = "//input[@id='newPassword']";
		var confirmPW = "//input[@id='c_newPassword']";
		var changePWBtn = "//input[@type='submit']";
		browser
		.useXpath()
		.waitForElementVisible(newPW, 1000)
		.setValue(newPW, password)
		.waitForElementVisible(confirmPW, 1000)
		.setValue(confirmPW, password)
		.waitForElementVisible(changePWBtn, 1000)
		.click(changePWBtn)
		.pause(2000);
	},

	/**
	 * @description Verifies user is logged into default customer
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12UserLoggedDefaultCust: Verify User in Default Customer Dashboard" : function (browser) {
		var defaultCustName = "(//h2[@id='childTitle'])[text()='" + childName1 + "']";
		var notDefaultCust = "(//h2[@id='childTitle'])[text()='" + childName2 + "']";
		var dashboardTab = "//nav[@id='main']//a[@title='Dashboard']";
		var reportsTab = "//nav[@id='main']//a[@title='Reports']";
		var maintenanceTab = "//nav[@id='main']//a[@title='Maintenance']";
		var collectionsTab = "//nav[@id='main']//a[@title='Collections']";
		var accountsTab = "//nav[@id='main']//a[@title='Accounts']";
		var cardManagementTab = "//nav[@id='main']//a[@title='Card Management']";
		var settingsTab = "//nav[@id='main']//a[@title='Settings']";
		browser
		.useXpath()
		.pause(1000)
		.assert.title('Digital Iris - Main (Dashboard)')
		.assert.visible(defaultCustName)
		.assert.visible(dashboardTab)
		.assert.visible(reportsTab)
		.assert.visible(maintenanceTab)
		.assert.visible(collectionsTab)
		.assert.visible(accountsTab)
		.assert.visible(cardManagementTab)
		.assert.visible(settingsTab)
		.assert.elementNotPresent(notDefaultCust)
		.pause(1000)
	},

	/**
	 * @description Logs the new user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12UserLoggedDefaultCust: Log New User Out of Iris" : function (browser) {
		var logoutBtn = "//a[@title='Logout']";
		browser
		.useXpath()
		.waitForElementVisible(logoutBtn, 1000)
		.click(logoutBtn)
		.pause(3000);
	},

	/**
	 * @description Logs the parent back into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12UserLoggedDefaultCust: Login Back Into Parent Account" : function (browser) {
		browser
		.useXpath()
		.login(parentUsername, password)
		.pause(2000);
	},

	/**
	 * @description Enters into Settings: Users: User Accounts page
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12UserLoggedDefaultCust: Entering Back to Parent Account Settings: Users: User Accounts" : function (browser) {
		var settingsTab = "//nav[@id='main']//a[@title='Settings']";
		var usersTab = "//a[@title='Users']";
		var userAccountsTab = "//a[@title='User Account']";
		browser
		.useXpath()
		.waitForElementVisible(settingsTab, 2000)
		.click(settingsTab)
		.waitForElementVisible(usersTab, 2000)
		.click(usersTab)
		.waitForElementVisible(userAccountsTab, 2000)
		.click(userAccountsTab)
		.pause(1000);
	},

	/**
	 * @description Opens edit menu for user
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12UserLoggedDefaultCust: Open Edit Menu" : function (browser) {
		var detailsPage = "//span[contains(text(), '" + firstName + " " + lastName + "')]"
			var optionMenu = "//section[@id='userDetails']//a[@title='Option Menu']";
		var editOption = "//section[@id='userDetails']//a[@title='Edit']";
		browser
		.useXpath()
		.waitForElementVisible(detailsPage, 2000)
		.click(detailsPage)
		.waitForElementVisible(optionMenu, 2000)
		.click(optionMenu)
		.waitForElementVisible(editOption, 2000)
		.click(editOption)
		.pause(1000);
	},

	/**
	 * @description Change default customer for user
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12UserLoggedDefaultCust: Change Default Customer" : function (browser) {
		var expandChildOptions = "//a[@id='defaultCustExpand']";
		var secondChildCompany = "(//li[@role='presentation'])[3]"
			var saveBtn = "//a[@class='linkButtonFtr textButton save']";
		browser
		.useXpath()
		.waitForElementVisible(expandChildOptions, 1000)
		.click(expandChildOptions)
		.waitForElementVisible(secondChildCompany, 1000)
		.click(secondChildCompany)
		.waitForElementVisible(saveBtn, 1000)
		.click(saveBtn)
		.pause(2000);
	},

	/**
	 * @description Logs the parent user out of Iris again
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12UserLoggedDefaultCust: Log Parent Out of Iris Again" : function (browser) {
		var logoutBtn = "//a[@title='Logout']";
		browser
		.useXpath()
		.waitForElementVisible(logoutBtn, 1000)
		.click(logoutBtn)
		.pause(1000);
	},

	/**
	 * @description Logs the new user into Iris again
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12UserLoggedDefaultCust: Login to New User Account Again" : function (browser) {
		browser
		.useXpath()
		.login(emailAddress, password)
		.pause(2000);
	},

	/**
	 * @description Verifies user is logged into changed default customer
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12UserLoggedDefaultCust: Verify User in Changed Default Customer Dashboard" : function (browser) {
		var defaultCustName = "(//h2[@id='childTitle'])[text()='" + childName2 + "']";
		var notDefaultCust = "(//h2[@id='childTitle'])[text()='" + childName1 + "']";
		var dashboardTab = "//nav[@id='main']//a[@title='Dashboard']";
		var reportsTab = "//nav[@id='main']//a[@title='Reports']";
		var maintenanceTab = "//nav[@id='main']//a[@title='Maintenance']";
		var collectionsTab = "//nav[@id='main']//a[@title='Collections']";
		var accountsTab = "//nav[@id='main']//a[@title='Accounts']";
		var cardManagementTab = "//nav[@id='main']//a[@title='Card Management']";
		var settingsTab = "//nav[@id='main']//a[@title='Settings']";
		browser
		.useXpath()
		.pause(1000)
		.assert.title('Digital Iris - Main (Dashboard)')
		.assert.visible(defaultCustName)
		.assert.visible(dashboardTab)
		.assert.visible(reportsTab)
		.assert.visible(maintenanceTab)
		.assert.visible(collectionsTab)
		.assert.visible(accountsTab)
		.assert.visible(cardManagementTab)
		.assert.visible(settingsTab)
		.assert.elementNotPresent(notDefaultCust)
		.pause(1000)
	},

	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test12UserLoggedDefaultCust: Logout" : function (browser) {
		browser.logout();
	},
};
