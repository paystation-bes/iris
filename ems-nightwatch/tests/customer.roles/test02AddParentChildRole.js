/**
 * @summary Centralized Child User Management: Add Child Role from Parent Account
 * @since 7.3.5
 * @version 1.1
 * @author Nadine B, Thomas C
 * @see US693: TC856, TC862
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ParentUserName");
var password = data("Password");
var randomInt = data("RandIntLessThan")
	var roleName = "ChildRole" + Math.floor((Math.random() * 100) + 1);

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/

	"test02AddParentChildRole: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	 * @description Navigates to Roles
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test02AddParentChildRole: Navigate to Roles" : function (browser) {
		browser
		.useXpath()
		.navigateTo("Settings")
		.navigateTo("Users")
		.navigateTo("Roles")
	},

	/**
	 * @description Initiates an Add role operation
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test02AddParentChildRole: Click on '+' Button" : function (browser) {
		var addRoleBtn = "//a[@id='btnAddRole']";
		browser
		.useXpath()
		.waitForElementVisible(addRoleBtn, 1000)
		.click(addRoleBtn)
		.pause(2000)
		//Add role panel
		.waitForElementVisible("//h2[contains(text(),'Add Role')]", 5000)
	},

	/**
	 * @description Enters a role name
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test02AddParentChildRole: Enter Role Name" : function (browser) {
		var roleNameIn = "//input[@id='formRoleName']";
		browser
		.useXpath()
		.waitForElementVisible(roleNameIn, 1000)
		.setValue(roleNameIn, roleName)
	},

	/**
	 * @description Selects a Parent role type
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test03AddParentParentRole: Click Parent Role Radio Button" : function (browser) {
		var roleRadial = "//a[@id='roleType:3']";
		browser
		.useXpath()
		.assert.visible(roleRadial)
		.click(roleRadial)
		.pause(2000)
	},

	/**
	 * @description Processes permissions
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test03AddParentParentRole: Choose All Roles" : function (browser) {
		var allRoles = "//ul[@id='childPermissionList']/li[@class='Child']";
		browser.useXpath()
		//Child here does not refer to Parent or Child company
		browser.elements('xpath', allRoles, function (result) {
			console.log(result.value.length)
			for (var i = 1; i <= result.value.length; i++) {
				browser
				//assert that the role is visible
				.assert.visible(allRoles + "[" + i + "]/label")
				//clicking the label activates the checkbox
				.click(allRoles + "[" + i + "]/label");
			}
		});
	},

	/**
	 * @description Saves selections
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test03AddParentParentRole: Click Save" : function (browser) {
		var saveBtn = "//section[@class='btnSet']/a[text()='Save']";
		var error = "//li[contains(text(), 'same Role Name')]";
		browser
		.useXpath()
		.assert.visible(saveBtn)
		.click(saveBtn)
		.pause(1000)
		.assert.elementNotPresent(error)
		.pause(1000)
	},

	/**
	 * @description Verifies the details are saved correctly
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test03AddParentParentRole: Verify Details" : function (browser) {
		var addedRole = "//span[contains(text(), '" + roleName + "')]";
		var type = addedRole + "//em[contains(text(),'Child')]";
		browser
		.useXpath()
		.pause(2000)
		.waitForElementVisible(addedRole, 3000)
		.waitForElementVisible(type, 3000)
		.click(addedRole)
		.waitForElementVisible("//dd[@id='detailRoleName']", 3000)
		.assert.visible("//dd[@id='detailRoleType']")

		browser.elements('xpath', "//ul[@id='detailPermissionList']/li", function (result) {
			for (var n = 1; n <= result.value.length; n++) {
				var paths = "//ul[@id='detailPermissionList']/li[" + n + "]";
				browser.assert.visible(paths)
			}
		})

		browser.assert.visible("//ul[@id='userChildList']/li[text()='No users currently assigned.']")
	},

	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test02AddParentChildRole: Logout" : function (browser) {
		browser.logout();
	},
};
