/**
 * @summary Centralized Child User Management: Add User with Parent and Child
 *          Roles
 * @since 7.3.5
 * @version 1.0
 * @author Thomas C
 * @see US693
 */

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ParentUserName");
var password = data("Password");
var password2 = data("Password2");

var firstName = "firstName" + Math.floor(Math.random() * 100) + 1;
var lastName = "lastName" + Math.floor(Math.random() * 100) + 1;
var email = firstName + "@qaAutomation1";
var childEmail = "qa1child";
var childRole = "Administrator";
var parentRole = "Corporate Administrator";
var fullName = firstName + " " + lastName;

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test07AddUserBothRoles: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	 * @description Navigates to Settings
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test07AddUserBothRoles: Click on Settings" : function (browser) {
		var settings = "//section[@class='bottomSection']/a[@title='Settings']";
		browser
		.useXpath()
		.waitForElementVisible(settings, 2000)
		.click(settings)
		.pause(1000)
	},

	/**
	 * @description Navigates to Users tab
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test07AddUserBothRoles: Click on Users Tab" : function (browser) {
		var users = "//a[@title='Users']";
		browser
		.useXpath()
		.waitForElementVisible(users, 1000)
		.click(users)
	},

	/**
	 * @description Clicks on the Add User button
	 * @param browser -  The web browser object
	 * @returns void
	 */
	"test07AddUserBothRoles: Click on Add User button" : function (browser) {
		var addUserBtn = "//a[@id='btnAddUser']";
		browser
		.useXpath()
		.waitForElementVisible(addUserBtn, 2000)
		.click(addUserBtn)
	},

	/**
	 * @description Enters the Name, Email and Password for the user
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test07AddUserBothRoles: Enter User Details" : function (browser) {
		browser.enterUserDetials(firstName, lastName, email, password2, true);
	},

	/**
	 * @description Selects parent and child roles for the user
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test07AddUserBothRoles: Select Roles" : function (browser) {
		var childXpath = "//li[contains(text(),'" + childRole + "')]";
		var selectedChildXpath = "//ul[@id='userSelectedRoles']//li[@class='active childRole selected']";
		var parentXpath = "//li[contains(text(),'" + parentRole + "')]";
		var selectedParentXpath = "//ul[@id='userSelectedRoles']//li[@class='active  selected']";
		browser
		.useXpath()
		.assert.visible(childXpath)
		.click(childXpath)
		.pause(500)
		.waitForElementVisible(selectedChildXpath, 2000)
		.assert.visible(parentXpath)
		.click(parentXpath)
		.pause(2000)
		.waitForElementVisible(selectedParentXpath, 2000)
	},

	/**
	 * @description Selects the child accounts that the user can access
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test07AddUserBothRoles: Select Child Account" : function (browser) {
		var childCompany = "//li[@custname='" + childEmail + "']";
		var childCompanySelectedXpath = "//ul[@id='companySelectedRoles']" + childCompany;
		browser
		.useXpath()
		.waitForElementVisible("//ul[@id='companyAllRoles']", 1000)
		.waitForElementVisible(childCompany, 1000)
		.click(childCompany)
		.pause(500)
		.waitForElementVisible(childCompanySelectedXpath, 1000)
		.pause(500)
	},

	/**
	 * @description Saves the new User
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test07AddUserBothRoles: Click Save" : function (browser) {
		var saveBtn = "//a[@class='linkButtonFtr textButton save']";
		var error = "//li[contains(text(),'same Email Address already exists')";
		browser
		.useXpath()
		.assert.visible(saveBtn)
		.click(saveBtn)
		.assert.elementNotPresent(error)
	},

	/**
	 * @description Verifies that the new user has been saved
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test07AddUserBothRoles: Verify Details" : function (browser) {
		var xpath = "//span[contains(text(),'" + fullName + "')]";
		var name = "//dd[@id='detailUserDisplayName' and text()='" + fullName + "']";
		var uname = "//dd[@id='detailUserName' and text()='" + email + "']";
		var selectedChildRole = "//li[contains(text(),'" + childRole + "')]";
		var selectedParentRole = "//li[contains(text(),'" + parentRole + "')]";
		browser
		.useXpath()
		.pause(1000)
		.click(xpath)
		.waitForElementVisible(name, 1000)
		.assert.visible(uname)
		.assert.visible(selectedChildRole)
		.assert.visible(selectedParentRole)
	},

	/**
	 * @description Logs out of the admin user
	 * @param browser - The web browser object
	 * @returns void
	 */
	"test07AddUserBothRoles: Logout of admin" : function (browser) {
		browser
		.useXpath()
		.click("//a[@title='Logout']")
		.pause(2000);
	},

	/**
	 * @description Logs into the newly created user
	 * @param browser -  The web browser object
	 * @returns void
	 */
	"test07AddUserBothRoles: Sign in as new user" : function (browser) {
		browser.initialLogin(email, password, password2);
	},

	/**
	 * @description Switches to the child account
	 * @param browser -  The web browser object
	 * @returns void
	 */
	"test07AddUserBothRoles: Switch to qa1child Child branch" : function (browser) {
		var switchBar = "//*[@id='parentChildSwitchFrmArea']";
		var qa1ChildSB = "//h2[text()='qa1child']";
		var switchBtn = "//a[@id='custSwitchBtn']";
		
		browser
		.useXpath()
		.waitForElementVisible(switchBar, 10000)
		.click(switchBar)
		.waitForElementVisible(qa1ChildSB, 10000)
		.click(qa1ChildSB)
		.assert.visible(switchBtn)
		.click(switchBtn)
		.pause(2000)
	},

	/**
	 * @description Logs out
	 * @param browser -  The web browser object
	 * @returns void
	 */
	"test07AddUserBothRoles: test07AddUserBothRoles: Logout" : function (browser) {
		browser.logout();
	},

};
