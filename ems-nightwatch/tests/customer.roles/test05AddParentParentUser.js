/**
 * @summary Centralized Child User Management: Add User to Parent Customer account with Parent Role
 * @since 7.3.5
 * @version 1.2
 * @author Nadine B, Thomas C
 * @see US693: TC880
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ParentUserName");
var password = data("Password");

var firstName = "ParentUser" + Math.floor((Math.random() * 100) + 1);
var lastName = "LastName" + Math.floor((Math.random() * 100) + 1);
var email = "parentuser"+ Math.floor((Math.random() * 100) + 1)+"@qaAutomation1";
var password2 = "Password$2";
var childEmail = " qa1child";
var parentRole = " Corporate Administrator";
var fullName = firstName + " " + lastName;

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test05AddParentParentUser: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	 * @description Navigates to Users
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test05AddParentParentUser: Navigate to Users" : function (browser) {
		browser
		.useXpath()
		.navigateTo("Settings")
		.navigateTo("Users")
	},

	/**
	 * @description Clicks on the Add User button
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test05AddParentParentUser: Click on '+' button" : function (browser) {
		var addUserBtn = "//a[@id='btnAddUser']";
		browser
		.useXpath()
		.waitForElementVisible(addUserBtn, 2000)
		.click(addUserBtn)
	},

	/**
	 * @description Enters the Name, Email and Password for the user
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test05AddParentParentUser: Enter Details" : function (browser) {
		browser.enterUserDetials(firstName, lastName, email, password2, true);
	},

	/**
	 * @description Selects user roles
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test05AddParentParentUser: Select Roles" : function (browser) {
		var parentXpath = "//ul[@id='userAllRoles']/li[@class='active ' and text()='" + parentRole + "']";
		var parentXpath = "//ul[@id='userAllRoles']/li[contains(text(),'" + parentRole + "')]";
		var selectedParentXpath = "//ul[@id='userSelectedRoles']/li[@class='active  selected' and contains(text(),'" + parentRole + "')]"
			browser
			.useXpath()
			.pause(2000)
			.assert.visible(parentXpath)
			.click(parentXpath)
			.pause(2000)
			.waitForElementVisible(selectedParentXpath, 5000)
	},

	/**
	 * @description Saves selections
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test05AddParentParentUser: Click Save" : function (browser) {
		var saveBtn = "//a[@class='linkButtonFtr textButton save']";
		var error = "//li[contains(text(),'same Email Address')]";
		var xpath = "//span[text()='" + fullName + "']";
		browser
		.useXpath()
		.assert.visible(saveBtn)
		.click(saveBtn)
		.assert.elementNotPresent(error)
		.waitForElementVisible(xpath, 3000)
	},

	/**
	 * @description Verifies the result details
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test05AddParentParentUser: Verify Details" : function (browser) {
		var xpath = "//span[text()='" + fullName + "']";
		var name = "//dd[@id='detailUserDisplayName' and text()='" + fullName + "']";
		var status = "//dd[@id='detailUserStatus' and text()='Enabled']";
		var uname = "//dd[@id='detailUserName' and text()='" + email + "']";
		var selectedParentRole = "//ul[@id='roleChildList']/li[contains(text(),'" + parentRole.trim() + "')]";
		browser
		.useXpath()
		.pause(1000)
		.click(xpath)
		.waitForElementVisible(name, 2000)
		.assert.visible(status)
		.assert.visible(uname)
		.assert.visible(selectedParentRole)
	},

	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test05AddParentParentUser: Logout" : function (browser) {
		browser.logout();
	},
};
