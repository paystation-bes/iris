/**
 * @summary Centralized Child User Management: Switch From Child Only Area of Site to Parent Account
 * @since 7.3.5
 * @version 1.1
 * @author Mandy F, Thomas C
 * @see US693: TC983
 **/

var data = require('../../data.js');

var devurl = data("URL");
var parentUsername = data("ParentUserName");
var password = data("Password");

// save the child name and parent name
var childName = new String();
var parentName = new String();

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test10SwitchChildOnlyToParent: Login to Parent Account" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(parentUsername, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	 * @description Switches into child company
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test10SwitchChildOnlyToParent: Switch to Child Company" : function (browser) {
		var dropDownList = "//a[@id='childCoSwitchACExpand']";
		var childCompany = "(//li[@role='presentation']//a)[3]";
		var switchBtn = "//a[@id='custSwitchBtn']";
		browser
		.useXpath()
		.waitForElementVisible(dropDownList, 1000)
		.click(dropDownList)
		.getText(childCompany, function (result) {
			childName = result.value;
		})
		.waitForElementVisible(childCompany, 1000)
		.click(childCompany)
		.waitForElementVisible(switchBtn, 1000)
		.click(switchBtn)
		.pause(3000);
	},

	/**
	 * @description Verifies child data is present
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test10SwitchChildOnlyToParent: Verify Child Data" : function (browser) {
		var childNameHeader = "//h2[@id='childTitle']";
		var dashboardTab = "//nav[@id='main']//a[@title='Dashboard']";
		var reportsTab = "//nav[@id='main']//a[@title='Reports']";
		var maintenanceTab = "//nav[@id='main']//a[@title='Maintenance']";
		var collectionsTab = "//nav[@id='main']//a[@title='Collections']";
		var accountsTab = "//nav[@id='main']//a[@title='Accounts']";
		var cardManagementTab = "//nav[@id='main']//a[@title='Card Management']";
		var settingsTab = "//nav[@id='main']//a[@title='Settings']";
		browser
		.useXpath()
		.pause(1000)
		.assert.title('Digital Iris - Main (Dashboard)')
		.assert.visible(childNameHeader)
		.assert.visible(dashboardTab)
		.assert.visible(reportsTab)
		.assert.visible(maintenanceTab)
		.assert.visible(collectionsTab)
		.assert.visible(accountsTab)
		.assert.visible(cardManagementTab)
		.assert.visible(settingsTab)
		.pause(2000)
	},

	/**
	 * @description Goes into a page (Card Management) that parent account doesn't have
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test10SwitchChildOnlyToParent: Go To Card Management" : function (browser) {
		var cardManagementTab = "//nav[@id='main']//a[@title='Card Management']";
		var cardManagementHeader = "//h1[contains(text(), 'Card Management')]";
		browser
		.useXpath()
		.waitForElementVisible(cardManagementTab, 2000)
		.click(cardManagementTab)
		.assert.visible(cardManagementHeader)
		.pause(1000);
	},

	/**
	 * @description Switch back into parent account
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test10SwitchChildOnlyToParent: Switch Back to Parent Account" : function (browser) {
		var dropDownList = "//a[@id='childCoSwitchACExpand']";
		var parentCompany = "(//li[@role='presentation']//a)[2]";
		var switchBtn = "//a[@id='custSwitchBtn']";
		browser
		.useXpath()
		.waitForElementVisible(dropDownList, 1000)
		.click(dropDownList)
		.getText(parentCompany, function (result) {
			parentName = result.value;
		})
		.waitForElementVisible(parentCompany, 1000)
		.click(parentCompany)
		.waitForElementVisible(switchBtn, 1000)
		.click(switchBtn)
		.pause(3000);
	},

	/**
	 * @description Verifies parent is back onto dashboard
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test10SwitchChildOnlyToParent: Verify Parent in Dashboard" : function (browser) {
		var parentNameHeader = "//h2[@id='parentTitle']";
		var dashboardTab = "//nav[@id='main']//a[@title='Dashboard']";
		var reportsTab = "//nav[@id='main']//a[@title='Reports']";
		var maintenanceTab = "//nav[@id='main']//a[@title='Maintenance']";
		var collectionsTab = "//nav[@id='main']//a[@title='Collections']";
		var accountsTab = "//nav[@id='main']//a[@title='Accounts']";
		var cardManagementTab = "//nav[@id='main']//a[@title='Card Management']";
		var settingsTab = "//nav[@id='main']//a[@title='Settings']";
		var childNameGone = "//h2[contains(text(), '" + childName + "')]";
		browser
		.useXpath()
		.pause(1000)
		.assert.title('Digital Iris - Main (Dashboard)')
		.assert.visible(parentNameHeader)
		.assert.visible(dashboardTab)
		.assert.visible(reportsTab)
		.assert.visible(settingsTab)
		.assert.elementNotPresent(childNameGone)
		.assert.elementNotPresent(maintenanceTab)
		.assert.elementNotPresent(collectionsTab)
		.assert.elementNotPresent(accountsTab)
		.assert.elementNotPresent(cardManagementTab)
		.pause(1000)
	},

	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test10SwitchChildOnlyToParent: Logout" : function (browser) {
		browser.logout();
	},
};
