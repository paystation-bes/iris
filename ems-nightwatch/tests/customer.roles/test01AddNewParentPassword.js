/**
 * @summary Centralized Child User Management: Add Parent Account From System Admin
 * @since 7.3.5
 * @version 1.0
 * @author Thomas C, Nadine B
 * @see US693:
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");

var id = Math.floor((Math.random() * 1000) + 1);
var roleName = "qa" + id + " parent";
var userName = "qa" + id + "@parent";
var tempPassword = data("Password2");
var newPassword = data("Password");

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test01AddNewParentPassword: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
	},

	/**
	 * @description Creates a parent user
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test01AddNewParentPassword: Create a parent user" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("id('btnAddNewCustomer')", 2000)
		.click("id('btnAddNewCustomer')")
		.waitForElementVisible("id('parentFlagCheck')", 2000)
		.click("id('parentFlagCheck')")
		.setValue("id('formCustomerName')", roleName)
		.click("id('accountStatus:enabled')")
		.setValue("id('formUserName')", userName)
		.setValue("id('newPassword')", tempPassword)
		.setValue("id('c_newPassword')", tempPassword)
		.click("(//button[@type='button'])[2]")
		.pause(2000)
		.waitForElementVisible("//a[@title='Logout']", 2000)
		.click("//a[@title='Logout']")
		.pause(1000);
	},

	/**
	 * @description Opens a browser and logs in with a newly created parent
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test01AddNewParentPassword: Login with new parent" : function (browser) {
		browser
		.login(userName, tempPassword);
	},

	/**
	 * @description Processes Terms and Conditions screen
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test01AddNewParentPassword: Accept terms and conditions" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("id('serviceAgreementArea')", 2000)
		.click("id('saDetails')")
		.keys(browser.Keys.END)
		.keys(browser.Keys.NULL)
		.setValue("id('name')", 'QA Parent')
		.setValue("id('title')", 'QA')
		.setValue("id('organization')", 'T2')
		.click("id('saAccept')")
		.pause(2000);
	},

	/**
	 * @description Assigns a new user password
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test01AddNewParentPassword: Set the new password" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("id('newPassword')", 2000)
		.setValue("id('newPassword')", newPassword)
		.setValue("id('c_newPassword')", newPassword)
		.click("id('changePassword')")
		.pause(1000)
		.assert.title('Digital Iris - Main (Dashboard)')
	},

	/**
	 *@description Logs the user out
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01AddNewParentPassword: Logout" : function (browser) {
		browser.logout();
	},
};
