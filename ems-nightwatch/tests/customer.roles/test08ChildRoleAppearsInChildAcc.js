/**
 * @summary Centralized Child User Management: Child Role Created in Parent Account Can Be Viewed But Not Edited in All Child Account
 * @since 7.3.5
 * @version 1.2
 * @author Mandy F, Thomas C
 * @see US693: TC974
 **/

var data = require('../../data.js');

var devurl = data("URL");
var parentUsername = data("ParentUserName");
var childUsername = data("ChildUserName");
var password = data("Password");
var password2 = data("Password2")
	// save the parent name in a variable to verify it is same parent in child account
	var parentName = new String();
// random number generator between 1 and 1000 for a different role name each time script runs
var randomNum = Math.floor((Math.random() * 1000) + 1);
// name of the child role and parent role
var childRoleName = "All Child Permissions" + randomNum;
var parentRoleName = "All Parent Permissions" + randomNum;

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test08ChildRoleAppearsInChildAcc: Login to Parent Account" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(parentUsername, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	 * @description Get parent name for verification in child account later
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test08ChildRoleAppearsInChildAcc: Get Parent Name" : function (browser) {
		var parentHeader = "//section[@id='parentRow']//h2[@id='parentTitle']";
		browser
		.useXpath()
		.waitForElementVisible(parentHeader, 1000)
		.getText(parentHeader, function (result) {
			parentName = result.value;
		});
	},

	/**
	 * @description Enters into Settings: Users: Roles page
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test08ChildRoleAppearsInChildAcc: Entering Parent Account Settings: Users: Roles" : function (browser) {
		browser
		.navigateTo("Settings")
		.navigateTo("Users")
		.navigateTo("Roles")
	},

	/**
	 * @description Opens the add role form
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test08ChildRoleAppearsInChildAcc: Open Add Role Form" : function (browser) {
		var addOption = "//a[@id='btnAddRole']";
		browser
		.useXpath()
		.waitForElementVisible(addOption, 1000)
		.click(addOption)
		.pause(1000);
	},

	/**
	 * @description Fills in the information to add a child role
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test08ChildRoleAppearsInChildAcc: Add Child Role Information" : function (browser) {
		var childRoleNameForm = "//input[@id='formRoleName']";
		var childTypeButton = "//section[@id='roleSwitchForm']//label[@id='childSwitch']";
		browser
		.useXpath()
		.waitForElementVisible(childRoleNameForm, 1000)
		.setValue(childRoleNameForm, childRoleName)
		.waitForElementVisible(childTypeButton, 1000)
		.click(childTypeButton)
		.pause(1000);
	},

	/**
	 * @description Chooses all the child permissions
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test08ChildRoleAppearsInChildAcc: Choose All Child Permissions" : function (browser) {
		var permissions = "(//ul[@id='childPermissionList']/li[@class='Child']/label)";
		browser.elements('xpath', permissions, function (result) {
			for (var i = 1; i <= result.value.length; i++) {
				browser
				.click(permissions + "[" + i + "]")
				.pause(250);
			}
		});

	},

	/**
	 * @description Saves the information to add a child role
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test08ChildRoleAppearsInChildAcc: Save Child Role Information" : function (browser) {
		var saveBtn = "//a[@class='linkButtonFtr textButton save']";
		browser
		.useXpath()
		.waitForElementVisible(saveBtn, 1000)
		.click(saveBtn)
		.pause(2000);
	},

	/**
	 * @description Verifies child role is created
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test08ChildRoleAppearsInChildAcc: Verify Child Role Information In Parent Account" : function (browser) {
		var childRole = "//span[contains(text(), '" + childRoleName + "')]";
		var childRoleType = "//dd[contains(text(), 'Child')]";
		browser
		.useXpath()
		.assert.visible(childRole)
		.click(childRole)
		.pause(1000)
		.assert.visible(childRoleType)
		.pause(1000);
	},

	/**
	 * @description Opens the add role form again
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test08ChildRoleAppearsInChildAcc: Open Add Role Form Again" : function (browser) {
		var addOption = "//a[@id='btnAddRole']";
		browser
		.useXpath()
		.waitForElementVisible(addOption, 1000)
		.click(addOption)
		.pause(1000);
	},

	/**
	 * @description Fills in the information to add a parent role
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test08ChildRoleAppearsInChildAcc: Add Parent Role Information" : function (browser) {
		var parentRoleNameForm = "//input[@id='formRoleName']";
		var parentTypeButton = "//section[@id='roleSwitchForm']//label[@id='parentSwitch']";
		browser
		.useXpath()
		.waitForElementVisible(parentRoleNameForm, 1000)
		.setValue(parentRoleNameForm, parentRoleName)
		.waitForElementVisible(parentTypeButton, 1000)
		.click(parentTypeButton)
		.pause(1000);
	},

	/**
	 * @description Chooses all the parent permissions
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test08ChildRoleAppearsInChildAcc: Choose All Parent Permissions" : function (browser) {
		var permissions = "(//ul[@id='parentPermissionList']/li[@class='Child']/label)";
		browser.elements('xpath', permissions, function (result) {
			for (var i = 1; i <= result.value.length; i++) {
				browser
				.click(permissions + "[" + i + "]")
				.pause(250);
			}
		});
	},

	/**
	 * @description Saves the information to add a parent role
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test08ChildRoleAppearsInChildAcc: Save Parent Role Information" : function (browser) {
		var saveBtn = "//a[@class='linkButtonFtr textButton save']";
		browser
		.useXpath()
		.waitForElementVisible(saveBtn, 1000)
		.click(saveBtn)
		.pause(2000);
	},

	/**
	 * @description Logs the parent user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test08ChildRoleAppearsInChildAcc: Log Parent Out of Iris" : function (browser) {
		var logoutBtn = "//a[@title='Logout']";
		browser
		.useXpath()
		.waitForElementVisible(logoutBtn, 1000)
		.click(logoutBtn)
		.pause(3000);
	},

	/**
	 * @description Logs the child user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test08ChildRoleAppearsInChildAcc: Login to Child Account" : function (browser) {
		browser
		.useXpath()
		.login(childUsername, password)
		.pause(1000);
	},

	/**
	 * @description Enters into Settings: Users: Roles page
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test08ChildRoleAppearsInChildAcc: Entering Child Account Settings: Users: Roles" : function (browser) {
		browser
		.navigateTo("Settings")
		.navigateTo("Users")
		.navigateTo("Roles")
		.pause(1000);
	},

	/**
	 * @description Verify child role information in child account
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test08ChildRoleAppearsInChildAcc: Verify Child Role Information In Child Account" : function (browser) {
		var childRole = "//ul[@id='roleList']//span[contains(text(), '" + childRoleName + "')]";
		var parentRole = "//ul[@id='roleList']//span[contains(text(), '" + parentRoleName + "')]"
			var childRoleParentName = childRole + "//em[contains(text(), '" + parentName + "')]";
		var childRoleDetails = "//dd[contains(text(), '" + childRoleName + "')]";
		browser
		.useXpath()
		.assert.visible(childRole)
		.assert.visible(childRoleParentName)
		// assert that you cannot edit role in a child account
		.assert.elementNotPresent(childRole + "//..//a[@title='Option Menu']")
		// assert parent role created earlier is not present in child account
		.assert.elementNotPresent(parentRole)
		.click(childRole)
		.pause(2000)
		.assert.visible(childRoleDetails)
		.pause(2000);
	},

	/**
	 * @description Logs the user out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test08ChildRoleAppearsInChildAcc: Logout" : function (browser) {
		browser.logout();
	},
};
