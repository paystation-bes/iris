/**
 * @summary Centralized Child User Management: Add a User with Parent and Child Roles but No Child Company - Verify that this is not possible and Alert message is correct
 * @since 7.3.5
 * @version 1.2
 * @author Nadine B, Thomas C
 * @see US693: TC892
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ParentUserName");
var password = data("Password");

var firstName = "UserBothParentAndChild" + Math.floor((Math.random() * 100) + 1);
var lastName = "ButNoChildCompany" + Math.floor((Math.random() * 100) + 1);
var email = "userbothparentandchild" + Math.floor((Math.random() * 100) + 1) + "@qaAutomation1";
var password2 = "Password$2";
var childRole = " Administrator";
var parentRole = " Corporate Administrator";
var fullName = firstName + " " + lastName;

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test06AddParenChildUserNoChildCompany: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	 * @description Navigates to Users
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test06AddParenChildUserNoChildCompany: Navigate to Users" : function (browser) {
		browser
		.useXpath()
		.navigateTo("Settings")
		.navigateTo("Users")
	},

	
	/**
	 * @description Clicks on the Add User button
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test06AddParenChildUserNoChildCompany: Click on '+' button" : function (browser) {
		var addUserBtn = "//a[@id='btnAddUser']";
		browser
		.useXpath()
		.waitForElementVisible(addUserBtn, 2000)
		.click(addUserBtn)
	},
	
	/**
	 * @description Enters the Name, Email and Password for the user
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test06AddParenChildUserNoChildCompany: Enter Details" : function (browser) {
		var firstNameIn ="//input[@id='formUserFirstName']";
		var lastNameIn="//input[@id='formUserLastName']";
		var emailIn ="//input[@id='formUserName']";
		var newPassIn="//input[@id='newPassword']";
		var confirmPassIn="//input[@id='c_newPassword']";
		browser
		.useXpath()
		.waitForElementVisible(firstNameIn, 4000)
		.setValue(firstNameIn, firstName)
		.pause(250)
		.setValue(lastNameIn, lastName)
		.pause(250)
		.setValue(emailIn, email)
		.pause(250)
		.setValue(newPassIn, password2)
		.pause(250)
		.setValue(confirmPassIn, password2)
		.pause(250)
		//enabled check box
		.click("//a[@id='statusCheck']")
	},
	
	/**
	 * @description Selects parent and child roles for the user
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test06AddParenChildUserNoChildCompany: Select Roles" : function (browser) {
		var childXpath = "//ul[@id='userAllRoles']/li[@class='active childRole' and contains(text(),'" + childRole + "')]";
		var selectedChildXpath = "//ul[@id='userSelectedRoles']/li[@class='active childRole selected' and contains(text(),'" + childRole + "')]"
			var parentXpath = "//ul[@id='userAllRoles']/li[@class='active ' and contains(text(),'" + parentRole + "')]";
		var selectedParentXpath = "//ul[@id='userSelectedRoles']/li[@class='active  selected' and contains(text(),'" + parentRole + "')]"
			browser
			.useXpath()
			.assert.visible(childXpath)
			.click(childXpath)
			.waitForElementVisible(selectedChildXpath, 1000)
			.assert.visible(parentXpath)
			.click(parentXpath)
			.waitForElementVisible(selectedParentXpath, 1000)
	},
	
	/**
	 * @description Clicks save and verify that this user cannot be saved
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test06AddParenChildUserNoChildCompany: Click Save" : function (browser) {
		var saveBtn = "//a[@class='linkButtonFtr textButton save']";
		var error = "//li[contains(text(),'same Email Address')]";
		var expectedAlert = "//*[@id='messageResponseAlertBox']//li[contains(text(), 'At least one child company should be selected.')]";
		var attentionCloseBtn = "//*[@id='messageResponseAlertBox']/section/a[@title='Close']";
		var cancelButton = "//section[@class='btnSet']/a[contains(text(), 'Cancel')]";
		var confirmCancelBtn = "//div/button/span[@class='ui-button-text' and contains(text(), 'Yes, cancel now')]";
		browser
		.useXpath()
		.assert.visible(saveBtn)
		.click(saveBtn)
		.assert.elementNotPresent(error)
		.assert.visible(expectedAlert)
		.waitForElementVisible(attentionCloseBtn, 1000)
		.click(attentionCloseBtn)
		.pause(2000)
		.waitForElementVisible(cancelButton, 3000)
		.click(cancelButton)
		.pause(2000)
		.waitForElementVisible(confirmCancelBtn, 3000)
		.click(confirmCancelBtn)
		.pause(3000)
	},
	
	/**
	 * @description Verifies the added users details are correct
	 * @param browser -
	 *                The web browser object
	 * @returns void
	 */
	"test06AddParenChildUserNoChildCompany: Logout" : function (browser) {
		browser.logout()
	},

};
