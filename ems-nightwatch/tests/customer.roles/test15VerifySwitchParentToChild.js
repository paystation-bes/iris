/**
 * @summary Centralized Child User Management: Switch From Child Only Area of Site to Parent Account
 * @since 7.3.5
 * @version 1.1
 * @author Nadine B, Thomas C
 * @see US693: TC879
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("ParentUserName");
var password = data("Password");
var switchButton = "//a[@id='custSwitchBtn']";
module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 * @description Logs the user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test15VerifySwitchParentToChild: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title("Digital Iris - Main (Dashboard)")
		.useXpath()
		.waitForElementVisible("//*[@id='parentTitle']", 3000)
		//Check that the parent title is correct 
		.getText("//*[@id='parentTitle']", function (result) {
			browser.assert.equal(result.value, "qa1parent")
		});
	},
	
	/**
	 * @description Switches to a child user using the Company Selection Bar
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test15VerifySwitchParentToChild: Switch to Child Company" : function (browser) {
		var qa1ChildSwitchBar = "//ul/li[@class='ui-menu-item']/a[text()='qa1child']";
		var switchBar = "//*[@id='parentChildSwitchFrmArea']";
		var switchBarExpand = "//*[@id='childCoSwitchACExpand']";
		browser
		.useXpath()
		//Switch bar
		.waitForElementVisible(switchBar, 10000)
		//Expand switch bar 
		.click(switchBarExpand)
		.waitForElementVisible(qa1ChildSwitchBar, 10000)
		.click(qa1ChildSwitchBar)
		.assert.visible(switchButton)
		.click(switchButton)
		.pause(2000)
	},

	/**
	 * @description Verifies that switching to the child company was successful 
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test15VerifySwitchParentToChild: Verify Child switched in the Switchbar and Child menus are present" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementVisible("//h2[@id='childTitle' and text()='qa1child']", 10000)
		.assert.visible("//a[@title='Settings']")
		.verify.visible("//a[@title='Reports']")
		.verify.visible("//a[@title='Maintenance']")
		.verify.visible("//a[@title='Collections']")
		.verify.visible("//a[@title='Card Management']")
		.verify.visible("//a[@title='Settings']")
	},

	/**
	 * @description Navigates to settings in the child company
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test15VerifySwitchParentToChild: Click Settings" : function (browser) {
		var settings ="//nav[@id='main']//a[@title='Settings']";
		browser
		.useXpath()
		.pause(1000)
		//Settings
		.waitForElementVisible(settings, 10000)
		.click(settings)
		.pause(1000)
		.waitForElementVisible("//a[@title='Alerts']", 10000)
		.pause(1000)
		.waitForElementVisible("//a[@title='Pay Stations']", 10000)
		.assert.visible("//a[@title='Global']")
		.assert.visible("//a[@title='Rates']")
		.assert.visible("//a[@title='Mobile Devices']")
		.assert.visible("//a[@title='Routes']")
		.assert.visible("//a[@title='Alerts']")
		.assert.visible("//a[@title='Users']")
		.assert.visible("//a[@title='Card Settings']")
	},

	/**
	 * @description Switches from the child company: Settings page back to the parent company
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test15VerifySwitchParentToChild: Switch back to Parent Company" : function (browser) {
		var childCompanySwitchBar = "//input[@id='childCoSwitchAC']";
		var qa1Parent = "//ul/li[@class='ui-menu-item']/a[text()='qa1parent']";
		browser
		.useXpath()
		.waitForElementVisible(childCompanySwitchBar, 10000)
		.click(childCompanySwitchBar)
		.waitForElementVisible(qa1Parent, 10000)
		.click(qa1Parent)
		.assert.visible(switchButton)
		.click(switchButton)
	},

	/**
	 * @description Verifies that the user is successfully switched back to the parent company 
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test15VerifySwitchParentToChild: Verify the Switchbar and Parent menus are present" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementVisible("//*[@id='parentTitle']", 3000)
		.getText("//*[@id='parentTitle']", function (result) {
			browser.assert.equal(result.value, "qa1parent")
		});
		browser
		.waitForElementVisible("//a[@title='Settings']", 10000)
		.assert.visible("//a[@title='Reports']")
		.assert.visible("//nav[@id='main']//a[@title='Settings']")
		.assert.elementNotPresent("//a[@title='Maintenance']")
		.assert.elementNotPresent("//a[@title='Collections']")
		.assert.elementNotPresent("//nav[@id='main']//a[@title='Card Management']")

	},

	/**
	 * @description Logs the user out 
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test15VerifySwitchParentToChild: Logout" : function (browser) {
		browser.logout();
	},

};
