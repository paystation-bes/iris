var data = require('../../data.js');

var devurl = data("URL");
var username = data("ParentUserName");
var password = data("Password");

var roleName = "Parent Role " + Math.floor(Math.random() * 100) + 1;

module.exports = {
	tags: ['smoketag', 'completetesttag'],
	/**
	 *@description Logs the user into Iris
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test14NoPermissionsParentRole: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	 *@description Navigates to Settings
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test14NoPermissionsParentRole: Click on Settings in the Sidebar" : function (browser) {
		var settings = "//section[@class='bottomSection']/a[@title='Settings']";
		browser
		.useXpath()
		.waitForElementVisible(settings, 2000)
		.click(settings)
		.pause(1000)
	},

	/**
	 *@description Navigates to Users tab
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test14NoPermissionsParentRole: Click on Users Tab" : function (browser) {
		var users = "//a[@title='Users']";
		browser
		.useXpath()
		.waitForElementVisible(users, 1000)
		.click(users)
	},

	/**
	 *@description Navigates to the Roles tab
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test14NoPermissionsParentRole: Click on Roles Sub-Tab" : function (browser) {
		var roles = "//a[text()='Roles']";
		var rolesTitle = "//h2[text()='Roles']";
		browser
		.useXpath()
		.pause(1000)
		.waitForElementVisible(roles, 2000)
		.click(roles)
		.pause(1000)
		.waitForElementVisible(rolesTitle, 2000)
	},

	/**
	 *@description Clicks on the Add Role button
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test14NoPermissionsParentRole: Click on '+' Button" : function (browser) {
		addRoleBtn = "//a[@id='btnAddRole']";
		addRoleTitle = "//h2[text()='Add Role']";
		browser
		.useXpath()
		.waitForElementVisible(addRoleBtn, 1000)
		.click(addRoleBtn)
		.waitForElementVisible(addRoleTitle, 1000)
	},

	/**
	 *@description Enters the Role Name
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test14NoPermissionsParentRole: Enter Role Name" : function (browser) {
		var roleNameInput = "//input[@id='formRoleName']";
		browser
		.useXpath()
		.waitForElementVisible(roleNameInput, 1000)
		.setValue(roleNameInput, roleName)
	},

	/**
	 *@description Clicks on the Parent Role radial button
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test14NoPermissionsParentRole: Click Parent Role Radio Button" : function (browser) {
		var childRole = "//a[@id='roleType:3']";
		browser
		.useXpath()
		.assert.visible(childRole)
		.click(childRole)
	},

	/**
	 *@description Clicks on the Save button without choosing role
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test14NoPermissionsParentRole: Choose No Roles and Click Save" : function (browser) {
		endKey = "\uE010";
		saveBtn = "//section[@class='btnSet']/a[text()='Save']";
		cancelBtn = "//section[@class='btnSet']/a[text()='Cancel']";
		permissionAlert = "//li[contains(.,'Permission selection is required')]";
		closeAlert = "//a[@title='Close']";
		confirmCancel = "//span[text()='Yes, cancel now']";

		browser
		.useXpath()
		.keys(endKey)
		.assert.visible(saveBtn)
		.click(saveBtn)
		.waitForElementVisible(permissionAlert, 2000)
		.assert.visible(permissionAlert)
		.waitForElementVisible(closeAlert, 2000)
		.click(closeAlert)
		.pause(2000)
	},

	/**
	 *@description Clicks on the Cancel button
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test14NoPermissionsParentRole: Cancel Add Role" : function (browser) {
		endKey = "\uE010";
		saveBtn = "//section[@class='btnSet']/a[text()='Save']";
		cancelBtn = "//section[@class='btnSet']/a[text()='Cancel']";
		permissionAlert = "//li[contains(.,'Permission selection is required')]";
		closeAlert = "//a[@title='Close']";
		confirmCancel = "//span[text()='Yes, cancel now']";

		browser
		.useXpath()
		.waitForElementVisible(cancelBtn, 2000)
		.click(cancelBtn)
		.pause(1000)
		.waitForElementVisible(confirmCancel, 2000)
		.click(confirmCancel)
		.pause(2000)
	},

	/**
	 *@description Assert that the role has not been added
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test14NoPermissionsParentRole: Assert Role not added" : function (browser) {
		browser
		.useXpath()
		.assert.elementNotPresent("//li[contains(.," + roleName + ")]")
	},

	/**
	 *@description Logs the user out
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test14NoPermissionsParentRole: Logout" : function (browser) {
		browser.logout();
	}
};
