/**
 * @summary
 * Edit a System Admin User Account
 * 
 * @Author Billy Hoang
 **/
 
 var data = require('../../data.js');

// Login Info
var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");

var name = "AutomatedSysAdmin";
var newname = "AutomatedSysAdmin2";



module.exports = {
	tags: ['smoketag', 'completetesttag'],	


	/**
	 * @description Logs the user into Iris with a System Admin Account
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test01AddSysAdmin: Login to System Admin" : function (browser) 
	{
		console.log("User: " + username + "/" + password);
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password, "password");
	},

	/**
	 * @description Navigate to User Administration
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test01AddSysAdmin: Navigate to User Administration" : function (browser) 
	{
		browser	
			.click("[title='User Administration']")
			.pause(2000)
			.assert.title('Digital Iris - System Administration - User Administration : Admin User Accounts');
	},

		/**
	 * @description Edit System Admin Account
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test01AddSysAdmin: Edit System Admin Account" : function (browser) 
	{
		browser
		// Click Edit
			.useXpath()
			.click("//ul[@id='userList']/li/span[contains(text(), '" + name + " "+ name + "')]/following-sibling::a")
			.pause(2000)
			.click("//ul[@id='userList']/li/span[contains(text(), '" + name + " "+ name + "')]/../following-sibling::section[1]/section/a[@class='edit']")


		//Change Display Name
			.useCss()
			.click("#statusCheck")
			.clearValue("#formUserFirstName")
			.clearValue("#formUserLastName")
			.setValue("#formUserFirstName", newname)
			.setValue("#formUserLastName", newname)
			.click(".linkButtonFtr.textButton.save")
			.pause(2000);	
	},

		/**
	 * @description Assert that new System Admin has been added
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test01AddSysAdmin: Assert New Sys Admin" : function (browser) 
	{
		browser	
			.useXpath()
			.assert.visible("//ul[@id='userList']/li/span[contains(text(), '" + newname + " "+ newname + "')]");
	},

};
