/**
 * @summary Edit an PayStationInfo Digital API License
 * @since 7.5
 * @version 1.0
 * @author Johnson N
 **/
 
 var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var defaultpassword = data("DefaultPassword");
var customerName = "oranj";
var oldToken;
 
 module.exports = {
		tags: ['smoketag', 'completetesttag'],
	 		"Login to System Admin" : function (browser) 
	{
		console.log("User: " + username + "/" + password);
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
		"Search for oranj (Oranj Parent)" : function (browser) 
	{
		browser
			.useCss()
			.waitForElementVisible("#search", 2000)
			.setValue("#search", customerName)
			.pause(2000)
			.assert.visible("#btnGo")
			.click("#btnGo");

	},
	
		"Navigate to Licenses Tab" : function (browser){
			browser
			.useXpath()
			.waitForElementVisible(".//a[@title='Licenses']", 3000)
			.click(".//a[@title='Licenses']");
		},
		
		"Get PayStationInfo's Token": function(browser){
			browser
			.pause(2000)
			.useXpath()
			.assert.visible(".//*[@id='soapList']/li/div/p[contains(text(), 'PayStationInfo')]")
			.getText(".//*[@id='soapList']/li/div/p[contains(text(), 'PayStationInfo')]/../following-sibling::div[1]/p", function(result){
				oldToken = result.value;
				console.log("Old Token is: " + oldToken);
			});
		
		},
		
		"Click Edit Button for PayStationInfo API" : function(browser){
			browser
			.useXpath()
			.waitForElementVisible(".//*[@id='soapList']/li/div/p[contains(text(), 'PayStationInfo')]/../following-sibling::a", 2000)
			.click(".//*[@id='soapList']/li/div/p[contains(text(), 'PayStationInfo')]/../following-sibling::a")
			.waitForElementVisible(".//*[@id='soapList']/li/div/p[contains(text(), 'PayStationInfo')]/../../following-sibling::section[1]/section/a[@title='Edit']", 3000)
			.click(".//*[@id='soapList']/li/div/p[contains(text(), 'PayStationInfo')]/../../following-sibling::section[1]/section/a[@title='Edit']");
			
			
		},
		
		"Edit PayStationInfo Token" : function(browser){
			browser
			.useCss()
			.waitForElementVisible("#soapAccntFormArea", 2000)
			.assert.visible("#generateToken")
			.click("#generateToken")
			.useXpath()
			.click("//span[contains(text(), 'Edit Account') and @class='ui-button-text']");
			
		},
		
		"Verify Token is changed" : function(browser){
			browser
			.pause(1000)
			.useXpath()
			.expect.element(".//*[@id='soapList']/li/div/p[contains(text(), 'PayStationInfo')]/../following-sibling::div[1]/p").text.to.not.equal(oldToken);
			
		},
			
			
			
		
		
				"Logout" : function (browser) 
		{
			browser.logout();
		}, 
		

		
		
		
		
	
 };
