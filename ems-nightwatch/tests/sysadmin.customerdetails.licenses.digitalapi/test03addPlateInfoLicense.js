/**
 * @summary Add an PlateInfo Digital API License
 * @since 7.5
 * @version 1.0
 * @author Johnson N
 **/
 
 var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var defaultpassword = data("DefaultPassword");
var customerName = "oranj";
var generatedToken;
 
 module.exports = {
		tags: ['smoketag', 'completetesttag'],
	 		"Login to System Admin" : function (browser) 
	{
		console.log("User: " + username + "/" + password);
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
		"Search for oranj (Oranj Parent)" : function (browser) 
	{
		browser
			.useCss()
			.waitForElementVisible("#search", 2000)
			.setValue("#search", customerName)
			.pause(2000)
			.assert.visible("#btnGo")
			.click("#btnGo");

	},
	
		"Navigate to Licenses Tab" : function (browser){
			browser
			.useXpath()
			.waitForElementVisible(".//a[@title='Licenses']", 3000)
			.click(".//a[@title='Licenses']");
		},
		
		"Click the Add Digital API - Read button": function(browser){
			browser
			.useCss()
			.waitForElementVisible("#btnAddSOAP", 2000)
			.click("#btnAddSOAP")
			.pause(1000)
			.assert.visible("#soapAccntFormArea");
		},
		
		"Select PlateInfo Endpoint Type and Generate Token": function(browser){
			browser
			.useCss()
			.waitForElementVisible("#soapTypeExpand", 1000)
			.click("#soapTypeExpand")
			.useXpath()
			.waitForElementVisible(".//a[contains(text(), 'PlateInfo')]", 2000)
			.click(".//a[contains(text(), 'PlateInfo')]")
			.useCss()
			.waitForElementVisible("#generateToken", 1000)
			.click("#generateToken")
			.useXpath() 
			.click("//span[contains(text(), 'Add Account') and @class='ui-button-text']");
				
		},
		
		"Verify PlateInfo Digital API license is created": function(browser){
			browser
			.useXpath()
			.pause(2000)
			.assert.visible(".//*[@id='soapList']/li/div/p[contains(text(), 'PlateInfo')]");
			
		},
		
		
				"Logout" : function (browser) 
		{
			browser.logout();
		}, 
		

		
		
		
		
	
 };
