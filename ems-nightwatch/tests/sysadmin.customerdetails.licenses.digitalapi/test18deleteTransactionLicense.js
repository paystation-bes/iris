/**
 * @summary Delete an TransactionInfo Digital API License
 * @since 7.5
 * @version 1.0
 * @author Johnson N
 **/
 
 var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
var defaultpassword = data("DefaultPassword");
var customerName = "oranj";
 
 module.exports = {
		tags: ['smoketag', 'completetesttag'],
	 		"Login to System Admin" : function (browser) 
	{
		console.log("User: " + username + "/" + password);
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
		"Search for oranj (Oranj Parent)" : function (browser) 
	{
		browser
			.useCss()
			.waitForElementVisible("#search", 2000)
			.setValue("#search", customerName)
			.pause(2000)
			.assert.visible("#btnGo")
			.click("#btnGo");

	},
	
		"Navigate to Licenses Tab" : function (browser){
			browser
			.useXpath()
			.waitForElementVisible(".//a[@title='Licenses']", 3000)
			.click(".//a[@title='Licenses']");
		},
		
		"Click Delete Button for TransactionInfo API" : function(browser){
			browser
			.useXpath()
			.waitForElementVisible(".//*[@id='soapList']/li/div/p[contains(text(), 'TransactionInfo')]/../following-sibling::a", 2000)
			.click(".//*[@id='soapList']/li/div/p[contains(text(), 'TransactionInfo')]/../following-sibling::a")
			.waitForElementVisible(".//*[@id='soapList']/li/div/p[contains(text(), 'TransactionInfo')]/../../following-sibling::section[1]/section/a[@title='Delete']", 3000)
			.click(".//*[@id='soapList']/li/div/p[contains(text(), 'TransactionInfo')]/../../following-sibling::section[1]/section/a[@title='Delete']");
			
			
		},
		
		"Delete TransactionInfo License" : function(browser){
			browser
			.useCss()
			.waitForElementVisible("#messageResponseAlertBox", 2000)
			.useXpath()
			.assert.visible("//span[contains(text(), 'Delete') and @class='ui-button-text']", 2000)
			.click("//span[contains(text(), 'Delete') and @class='ui-button-text']");
			
		},
		
		"Verify TransactionInfo License is deleted" : function(browser){
			browser
			.pause(3000)
			.useXpath()
			.assert.elementNotPresent(".//*[@id='soapList']/li/div/p[contains(text(), 'TransactionInfo')]");
			
		},
			
			
			
		
		
				"Logout" : function (browser) 
		{
			browser.logout();
		}, 
		

		
		
		
		
	
 };