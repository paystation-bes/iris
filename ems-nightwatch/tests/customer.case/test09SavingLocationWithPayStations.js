/**
 * @summary Integrate with Counts in the Cloud: Tests saving location with pay stations
 * @since 7.4.1
 * @version 1.0
 * @author Nadine B
 * @see US898: TC1245, TC1246 & TC1249
 **/

var data = require('../../data.js');

var devurl = data("URL");

var adminUsername = data("SysAdminUserName");
var customerUsername = data("AdminUserName");
var customer = "oranj";
var customerName = customer;
var password = data("Password");

var randomNumber = Math.floor((Math.random() * 100) + 1);
var formName = "//*[@id='formLocationName']";
var formCapacity = "//*[@id='formCapacity']";
var locationName = "LocationWithOutCase" + randomNumber;
var operationModeButton = "//*[@id='formOperatingModeExpand']";
var locOperationMode = "//li[@class='ui-menu-item']/a[contains(text(), 'Multiple')]";
var locCapacity = 100;
var longBeachElement1 = "133 Long Beach";
var longBeachElement2 = "328 Pacific";

var autoCountSelection1 = "//ul[@id='locLotFullList']/li[contains(text(), '" + longBeachElement1 + "')]";
var autoCountSelection2 = "//ul[@id='locLotFullList']/li[contains(text(), '" + longBeachElement2 + "')]";

var save = "//*[@id='locationForm']/section[@class='btnSet']/a[contains(@class, 'Save')]";

var settingsTab = "//a[contains(@title, 'Settings')]";
var integrationsTab = "//a[contains(@title, 'Integrations')]";
var licensesTab = "//a[contains(@title, 'Licenses')]";
var locationsTab = "//a[contains(@title, 'Locations')]";
var locationDetailsTab = "//a[contains(@title, 'Location Details')]";
var locationsHeader = "//h2[contains(text(), 'Locations')]";

var count = 0;
var autoCountLots = "//*[@id='formLotChildren']/h3[contains(text(), 'Lots')]";

var selectedLongBeachElement1 = "//*[@id='locLotSelectedList']/li[contains(text(), '" + longBeachElement1 + "')]";
var selectedLongBeachElement2 = "//*[@id='locLotSelectedList']/li[contains(text(), '" + longBeachElement2 + "')]";

var availableLongBeachElement1 = "//*[@id='locLotFullList']/li[contains(text(), '" + longBeachElement1 + "')]";
var availableLongBeachElement2 = "//*[@id='locLotFullList']/li[contains(text(), '" + longBeachElement2 + "')]";

var payStation = "500000070001";
var payStationCheckBox = "//ul[contains(@id, 'locPSFullList')]/li[contains(text(), '" + payStation + "')]/a[contains(@class, 'checkBox')]";
var selectedPayStation = "//ul[contains(@id, 'locPSSelectedList')]/li[contains(text(), '500000070001') and contains(@class, 'selected')]";

var saveLocation = "//*[@id='locationForm']/section/a[contains(@class, 'save') and contains(text(), 'Save')]";

module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	 *@description Logs the system admin user into Iris, enables CASE for customer 'oranj' and assigns the account 'Texas' to them
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test09SavingLocationWithPayStations: Enable CASE for customer 'oranj' and assign CASE account 'Texas'" : function (browser) {
		browser
		.changeCustomersCasePermissions(true, adminUsername, password, customer, customerName, true, false)
		.pause(1000);
	},

	/**
	 *@description Signs into customer account 'oranj'
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test09SavingLocationWithPayStations: Sign into CASE Enabled Customer account" : function (browser) {
		browser.logoutAndLogin(customerUsername, password);
	},

	/**
	 *@description Navigate to Location Details
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test09SavingLocationWithPayStations: (CASE Enabled) Go to Settings > Locations > Location Details" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.waitForElementPresent(settingsTab, 4000)
		.click(settingsTab)

		.waitForElementPresent(locationsTab, 4000)
		.click(locationsTab)

		.waitForElementPresent(locationsTab, 4000)
		.click(locationsTab)

		.waitForElementPresent(locationDetailsTab, 4000)
		.click(locationDetailsTab)
		.pause(1000);
	},

	/**
	 *@description Add a new location - LocationWithOutCase
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test09SavingLocationWithPayStations: (CASE Enabled) Add a new location - fill out the form" : function (browser) {
		browser
		.useXpath()
		.click("//*[@id='btnAddLocation']")

		.pause(2000)
		.waitForElementPresent(formName, 4000)
		.pause(1000)
		.setValue(formName, locationName)
		.pause(1000)
		.click(operationModeButton)
		.pause(1000)
		.click(locOperationMode)
		.waitForElementPresent(formCapacity, 4000)
		.pause(1000)
		.setValue(formCapacity, locCapacity)
		.pause(1000)

		.click(payStationCheckBox)
		.pause(500)
		.assert.elementPresent(selectedPayStation)

		.waitForElementPresent(autoCountLots, 4000)

		.pause(1000)
		.assert.elementPresent(availableLongBeachElement1)
		.pause(1000)
		.assert.elementPresent(availableLongBeachElement2)

		.pause(1000)
		.click(availableLongBeachElement1)
		.pause(1000)
		.click(availableLongBeachElement2)
		.pause(1000)
		.assert.visible(selectedLongBeachElement1)
		.assert.visible(selectedLongBeachElement2)

		.pause(1000)
		.click(saveLocation)
		.pause(1000);

	},

	/**
	 *@description Verifies that the location data is saved
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test09SavingLocationWithPayStations: Verify that the AutoCount location data is saved properly" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementPresent("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + locationName + "')]", 4000)
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + "Unassigned" + "')]")
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + locationName + "')]")

		.pause(2000)
		.assert.elementPresent("//*[@id='psChildList']/li/a[contains(text(), '" + payStation + "')]")

		.assert.elementPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]")
		.pause(500)
		.assert.elementPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement2 + "')]")

		.assert.visible("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]")
		.pause(500)
		.assert.visible("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement2 + "')]")
		.pause(1000)
	},

	/**
	 *@description Logs the user out
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test09SavingLocationWithPayStations: Log out" : function (browser) {
		browser.logout();
	}

};
