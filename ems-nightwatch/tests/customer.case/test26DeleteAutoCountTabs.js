/**
* @summary Customer Admin: Dashboard: Cleanup- delete AutoCount Widget Tabs
* @since 7.4.1
* @version 1.0
* @author Lonney McD
* @see 
* @requires test02AccountAssignedToChild, test17AddCaseOnlyLocation, test19WidgetAddSave
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var pass = data("Password");
var defaultPass= data("DefaultPassword");


module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	 *@description Logs into customer account (oranj)
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test26DeleteAutoCountTabs : Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.initialLogin(username, pass, defaultPass)
			.assert.title('Digital Iris - Main (Dashboard)');
	},		

	/**
	 *@description Selects the Finance tab to make sure that Counted Occupancy and Counted/Paid Occ. tabs are not selected
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test26DeleteAutoCountTabs : Select Finance tab" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementVisible("//a[@class='tab'][@title='Finance']", 1000)
		.click("//a[@class='tab'][@title='Finance']")
		.pause(2000)
	},

	/**
	 *@description Switches Dash to edit mode
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test26DeleteAutoCountTabs : Switch Dash to edit mode" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='headerTools']/a[@title='Edit']", 1000, false)
			.click("//section[@id='headerTools']/a[@title='Edit']")
			.pause(2000)
			.assert.elementPresent("//a[@title='Add Widget']")
			;
	},

	/**
	 *@description Select to delete Counted Occupancy tab
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test26DeleteAutoCountTabs : Select to delete Counted Occupancy tab" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@class]/input[@value='Counted Occupancy']/../a[@title='Delete Tab']", 1000, false)
			.click("//section[@class]/input[@value='Counted Occupancy']/../a[@title='Delete Tab']")
			.pause(2000)
			;
	},

	/**
	 *@description Click Delete Tab button to Confirm deletion of Counted Occupancy Tab
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test26DeleteAutoCountTabs : Confirm deletion of Counted Occupancy Tab" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//button[@type='button']//span[text() = 'Delete tab']", 1000)
		.click("//button[@type='button']//span[text() = 'Delete tab']")
		.pause(2000);
	},

	/**
	 *@description Select to delete Counted/Paid Occ. tab
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test26DeleteAutoCountTabs : Select to delete Counted/Paid Occ. tab" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@class]/input[@value='Counted/Paid Occ.']/../a[@title='Delete Tab']", 1000, false)
			.click("//section[@class]/input[@value='Counted/Paid Occ.']/../a[@title='Delete Tab']")
			.pause(2000)
			;
	},

	/**
	 *@description Click Delete Tab button to Confirm deletion of Counted/Paid Occ. Tab
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test26DeleteAutoCountTabs : Confirm deletion of Counted/Paid Occ. Tab" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//button[@type='button']//span[text() = 'Delete tab']", 1000)
		.click("//button[@type='button']//span[text() = 'Delete tab']")
		.pause(2000);
	},

	/**
	 *@description Save Dashboard
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test26DeleteAutoCountTabs: Save dashboard" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@id='headerTools']//a[@title='Save']", 1000)
		.click("//section[@id='headerTools']//a[@title='Save']")
		.pause(3000)
	},

	/**
	 *@description Verify AutoCount tabs have been deleted, but that previously existing tabs still remain still 
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test26DeleteAutoCountTabs : Verify AutoCount tabs have been deleted" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//a[@class='tab'][@title='Finance']", 1000)
		.waitForElementVisible("//a[@class='tab'][@title='Operations']", 1000)
		.waitForElementVisible("//a[@class='tab'][@title='Map']", 1000)
		.waitForElementVisible("//a[@class='tab'][@title='Parker Metrics']", 1000)
		.waitForElementVisible("//a[@class='tab'][@title='Card Processing']", 1000)
		.assert.elementNotPresent("//section[@class='tabSection']/input[@value='Compare Occupancy']")
		.assert.elementNotPresent("//section[@class='tabSection']/input[@value='Counted/Paid Occ.']")
		.pause(1000)
		;
	},
	
	/**
	 *@description Logout
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test26DeleteAutoCountTabs  : Logout" : function (browser)

	{
		browser.logout();
	},
};