/**
 * @summary Integrate with Counts in the Cloud: Test that Parent accounts have no Licenses tab in System Admin view
 * @since 7.4.1
 * @version 1.0
 * @author Mandy F
 * @see US898: EMS-9309
 **/

var data = require('../../data.js');

var devurl = data("URL");
var adminUsername = data("SysAdminUserName");
var password = data("Password");
var customerName = "Oranj Parent";

module.exports = {
	tags: ['completetesttag', 'featuretesttag'],
	
	/**
	 * @description Logs the system admin user into Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test16ParentNoLicenses: Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')		
		.login(adminUsername, password)
		.assert.title('Digital Iris - System Administration');
	},

	/**
	 * @description Search for a parent customer
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test16ParentNoLicenses: Search Parent Customer" : function (browser) {
		var searchBar = "//input[@id='search']";
		var searchResultParent = "//a[contains(., 'Oranj Parent')]";
		var goBtn = "//a[@id='btnGo']";
		browser
		.useXpath()
		.waitForElementVisible(searchBar, 2000)
		.setValue(searchBar, customerName)
		.waitForElementVisible(searchResultParent, 2000)
		.click(searchResultParent)
		.waitForElementVisible(goBtn, 2000)
		.click(goBtn)
		.pause(3000);
	},

	/**
	 * @description Assert that the Parent customer has both CASE and FLEX permissions on
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test16ParentNoLicenses: Assert Permissions On" : function (browser) {
		var editBtn = "//a[@id='btnEditCustomer']";
		var FLEXSub = "//label[contains(., 'FLEX Integration')]";
		var CASESub = "//label[contains(., 'AutoCount Integration')]";
		var updateBtn = "//button[contains(., 'Update Customer')]";
		browser
		.useXpath()
		.waitForElementVisible(editBtn, 2000)
		.click(editBtn)
		.waitForElementVisible(FLEXSub, 2000)
		.getAttribute(FLEXSub + "//a", "class", function (result) {
			if (result.value != "checkBox checked") {
				browser.click(FLEXSub);
			}
		})
		.getAttribute(CASESub + "//a", "class", function (result) {
			if (result.value != "checkBox checked") {
				browser.click(CASESub);
			}
		})
		.pause(2000)
		.waitForElementVisible(updateBtn, 2000)
		.click(updateBtn)
		.pause(2000);
	},

	/**
	 * @description Assert that the parent customer doesn't have the Licenses tab
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test16ParentNoLicenses: Assert No Licenses Tab" : function (browser) {
		var navArea = "(//section[@id='secondNavArea']//a[@class='tab'])";
		browser
		.useXpath()
		.waitForElementVisible(navArea, 2000)
		.assert.elementNotPresent(navArea + "[contains(., 'Licenses')]")
		.pause(1000);
	},

	/**
	 * @description Log out of Iris
	 * @param browser - The web browser object
	 * @returns void
	 **/
	"test16ParentNoLicenses: Log out of Iris" : function (browser) {
		browser.logout();
	}

};
