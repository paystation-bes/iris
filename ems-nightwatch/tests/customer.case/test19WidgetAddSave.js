/**
* @summary Customer Admin: Dashboard: Create Two new AutoCount Widget tabs, and assign Widget set to tabs
* @since 7.4.1
* @version 1.0
* @author Lonney McD
* @see EMS9524
* @requires test02AccountAssignedToChild, test17AddCaseOnlyLocation
* @required by: test26DeleteAutoCountTabs, test25WidgetListWithSubscription, test24WidgetListWithoutSubscription, test23WidgetNoParent, test22WidgetVerifyDataPresent, test21WidgetDeleteSave, test20WidgetEditSave
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var pass = data("Password");
var defaultPass= data("DefaultPassword");


module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	 *@description Logs into customer account (oranj)
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test19WidgetAddSave: Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.initialLogin(username, pass, defaultPass)
			.assert.title('Digital Iris - Main (Dashboard)');
	},		
			
	/**
	 *@description Counted Occupancy: Switches Dash to edit mode
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test19WidgetAddSave: Counted Occupancy: Switch Dash to edit mode" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='headerTools']/a[@title='Edit']", 1000, false)
			.click("//section[@id='headerTools']/a[@title='Edit']")
			.pause(2000)
			.assert.visible("//a[@title='Add Widget']")
			;
	},
	
	/**
	 *@description Counted Occupancy: Click the Add Tab Button and Name Tab
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test19WidgetAddSave: Counted Occupancy: Click the Add Tab Button and Name Tab" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@title='New Tab']", 1000, false)
			.click("//a[@title='New Tab']")
			.pause(2000)
			.assert.visible("//section[@class='tabSection current new']")
			.waitForElementVisible("//section[@class='tabSection current new']/input[@class='tabInput']", 1000, false)
			.keys("Counted Occupancy")
			.pause(500)
			.keys(browser.Keys.ENTER)
			.pause(3000)
			.assert.containsText("//section[@class='tabSection current new']/a[@class='tab']", "Counted Occupancy")
			;
	},
		
	/**
	 *@description Counted Occupancy: Click the Add Widget Button 1
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test19WidgetAddSave: Counted Occupancy: Click the Add Widget Button 1" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']", 1000, false)
			.click("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']")
			.pause(3000)
			.assert.visible("//div[@aria-describedby='widgetList' and @role='dialog']")
			.assert.visible("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']/span[text()='Add widget']")
			.assert.visible("//button/span[text()='Cancel']")
			.pause(1000)
			;
	},
	
	/**
	 *@description Counted Occupancy: Expand Counted Occupancy Section of Widget List 1
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test19WidgetAddSave: Counted Occupancy: Expand Counted Occupancy Section of Widget List 1" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widgetListContainer']//h3[text() = 'Counted Occupancy']", 1000)
		.click("//section[@class='widgetListContainer']//h3[text() = 'Counted Occupancy']")
		.pause(1000)
		;
	},	

	/**
	 *@description Counted Occupancy: Select Counted Occupancy by location Widget and apply to dash 1
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test19WidgetAddSave: Counted Occupancy: Select Counted Occupancy by location Widget and apply to dash 1" : function (browser)
	{
		browser
		.useXpath()
		.assert.elementPresent("//section[@class='widgetListContainer']//li//header[text() = 'Counted Occupancy by Hour']")
		.click("//section[@class='widgetListContainer']//li//header[text() = 'Counted Occupancy by Hour']")
		.pause(1000)
		.waitForElementVisible("//button//span[text() = 'Add widget']", 1000)
		.click("//button//span[text() = 'Add widget']")
		.pause(1000)
	},
	
	/**
	 *@description Counted Occupancy: Click the Add Widget Button 2
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test19WidgetAddSave: Counted Occupancy: Click the Add Widget Button 2" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']", 1000, false)
			.click("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']")
			.pause(3000)
			.assert.visible("//div[@aria-describedby='widgetList' and @role='dialog']")
			.assert.visible("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']/span[text()='Add widget']")
			.assert.visible("//button/span[text()='Cancel']")
			.pause(1000)
			;
	},
	
	/**
	 *@description Counted Occupancy: Expand Counted Occupancy Section of Widget List 2
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test19WidgetAddSave: Counted Occupancy: Expand Counted Occupancy Section of Widget List 2" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widgetListContainer']//h3[text() = 'Counted Occupancy']", 1000)
		.click("//section[@class='widgetListContainer']//h3[text() = 'Counted Occupancy']")
		.pause(1000)
		;
	},	

	/**
	 *@description Counted Occupancy: Select Counted Occupancy by Location Widget and apply to dash 2
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test19WidgetAddSave: Counted Occupancy: Select Counted Occupancy by Location Widget and apply to dash 2" : function (browser)
	{
		browser
		.useXpath()
		.assert.elementPresent("//section[@class='widgetListContainer']//li//header[text() = 'Counted Occupancy by Location']")
		.click("//section[@class='widgetListContainer']//li//header[text() = 'Counted Occupancy by Location']")
		.pause(1000)
		.waitForElementVisible("//button//span[text() = 'Add widget']", 1000)
		.click("//button//span[text() = 'Add widget']")
		.pause(1000)
	},
	
	/**
	 *@description Counted Occupancy: Save Edits to Dash
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test19WidgetAddSave: Counted Occupancy: Save Edits to Dash" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@id='headerTools']//a[@title='Save']", 1000)
		.click("//section[@id='headerTools']//a[@title='Save']")
		.pause(3000)
	},
	
	/**
	 *@description Counted Occupancy: Verify widgets saved to Dash
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test19WidgetAddSave: Counted Occupancy: Verify Widget Add Success" : function (browser) {
		browser
		.useXpath()
		.assert.visible("(//section[@class='widget']//span[@class='wdgtName'])[text()='Counted Occupancy by Hour']")
		.assert.visible("(//section[@class='widget']//span[@class='wdgtName'])[text()='Counted Occupancy by Location']")
		.pause(1000)
	},
	
	/**
	 *@description Counted/Paid Occupancy: Switches Dash to edit mode
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test19WidgetAddSave: Counted/Paid Occupancy: Switch Dash to edit mode" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='headerTools']/a[@title='Edit']", 1000, false)
			.click("//section[@id='headerTools']/a[@title='Edit']")
			.pause(2000)
			.assert.visible("//a[@title='Add Widget']")
			;
	},
	
	/**
	 *@description Counted/Paid Occupancy: Click the Add Tab Button and Name Tab
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test19WidgetAddSave: Counted/Paid Occupancy: Click the Add Tab Button and Name Tab" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@title='New Tab']", 1000, false)
			.click("//a[@title='New Tab']")
			.pause(2000)
			.assert.visible("//section[@class='tabSection current new']")
			.waitForElementVisible("//section[@class='tabSection current new']/input[@class='tabInput']", 1000, false)
			.keys("Counted/Paid Occ.")
			.pause(500)
			.keys(browser.Keys.ENTER)
			.pause(3000)
			.assert.containsText("//section[@class='tabSection current new']/a[@class='tab']", "Counted/Paid Occ.")
			;
	},
	
	/**
	 *@description Counted/Paid Occupancy: Click the Add Widget Button 1
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test19WidgetAddSave: Counted/Paid Occupancy: Click the Add Widget Button 1" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']", 1000, false)
			.click("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']")
			.pause(3000)
			.assert.visible("//div[@aria-describedby='widgetList' and @role='dialog']")
			.assert.visible("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']/span[text()='Add widget']")
			.assert.visible("//button/span[text()='Cancel']")
			.pause(1000)
			;
	},
	
	/**
	 *@description Counted/Paid Occupancy: Expand Counted/Paid Occupancy Section of Widget List 1
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test19WidgetAddSave: Counted/Paid Occupancy: Expand Counted/Paid Occupancy Section of Widget List 1" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widgetListContainer']//h3[text() = 'Counted/Paid Occupancy']", 1000)
		.click("//section[@class='widgetListContainer']//h3[text() = 'Counted/Paid Occupancy']")
		.pause(1000)
		;
	},	

	/**
	 *@description Counted/Paid Occupancy: Select Counted/Paid Occupancy by location Widget and apply to dash 1
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test19WidgetAddSave: Counted/Paid Occupancy: Select Counted/Paid Occupancy by location Widget and apply to dash 1" : function (browser)
	{
		browser
		.useXpath()
		.assert.elementPresent("//section[@class='widgetListContainer']//li//header[text() = 'Counted/Paid Occ. by Hour']")
		.click("//section[@class='widgetListContainer']//li//header[text() = 'Counted/Paid Occ. by Hour']")
		.pause(1000)
		.waitForElementVisible("//button//span[text() = 'Add widget']", 1000)
		.click("//button//span[text() = 'Add widget']")
		.pause(1000)
	},
	
	/**
	 *@description Counted/Paid Occupancy: Click the Add Widget Button 2
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test19WidgetAddSave: Counted/Paid Occupancy: Click the Add Widget Button 2" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']", 1000, false)
			.click("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']")
			.pause(3000)
			.assert.visible("//div[@aria-describedby='widgetList' and @role='dialog']")
			.assert.visible("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']/span[text()='Add widget']")
			.assert.visible("//button/span[text()='Cancel']")
			.pause(1000)
			;
	},
	
	/**
	 *@description Counted/Paid Occupancy: Expand Counted/Paid Occupancy Section of Widget List 2
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test19WidgetAddSave: Counted/Paid Occupancy: Expand Counted/Paid Occupancy Section of Widget List 2" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widgetListContainer']//h3[text() = 'Counted/Paid Occupancy']", 1000)
		.click("//section[@class='widgetListContainer']//h3[text() = 'Counted/Paid Occupancy']")
		.pause(1000)
		;
	},	

	/**
	 *@description Counted/Paid Occupancy: Select Counted/Paid Occupancy by Location Widget and apply to dash 2
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test19WidgetAddSave: Counted/Paid Occupancy: Select Counted/Paid Occupancy by Location Widget and apply to dash 2" : function (browser)
	{
		browser
		.useXpath()
		.assert.elementPresent("//section[@class='widgetListContainer']//li//header[text() = 'Counted/Paid Occ. by Location']")
		.click("//section[@class='widgetListContainer']//li//header[text() = 'Counted/Paid Occ. by Location']")
		.pause(1000)
		.waitForElementVisible("//button//span[text() = 'Add widget']", 1000)
		.click("//button//span[text() = 'Add widget']")
		.pause(1000)
	},
	
	/**
	 *@description Counted/Paid Occupancy: Save Edits to Dash
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test19WidgetAddSave: Counted/Paid Occupancy: Save Edits to Dash" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@id='headerTools']//a[@title='Save']", 1000)
		.click("//section[@id='headerTools']//a[@title='Save']")
		.pause(3000)
	},
	
	/**
	 *@description Counted/Paid Occupancy: Verify widgets saved to Dash
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test19WidgetAddSave: Counted/Paid Occupancy: Verify Widget Add Success" : function (browser) 
	{
		browser
		.useXpath()
		.assert.visible("(//section[@class='widget']//span[@class='wdgtName'])[text()='Counted/Paid Occ. by Hour']")
		.assert.visible("(//section[@class='widget']//span[@class='wdgtName'])[text()='Counted/Paid Occ. by Location']")
		.pause(1000)
	},	

	/**
	 *@description Logout
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test19WidgetAddSave: Logout" : function (browser)

	{
		browser.logout();
	},
	
};