/**
 * @summary Integrate with Counts in the Cloud: Tests that same AutoCount account can be assigned to different customer
 * @since 7.4.1
 * @version 1.0
 * @author Nadine B
 * @see US898: TC1241
 **/

var data = require('../../data.js');

var devurl = data("URL");

var adminUsername = data("SysAdminUserName");
var customerUsername = data("AdminUserName");
var customer = "oranj";
var customerName = customer;
var password = data("Password");

var randomNumber = Math.floor((Math.random() * 100) + 1);
var newCustomerName = "Customer WithCaseOn" + randomNumber;
var newCustomerParent = "Oranj Parent";
var newCustomerTimeZone = "Canada/Pacific";
var newCustomerUsername = "customer_with_case_on" + randomNumber + "@email";
var newCustomer = "customerWithCaseOn";
var newCustomerPassword = "Password$2";

var caseAccountBeginning = "Texas";
var caseAccount = "Texas A&M University";
var caseAccountSearchResults = "//*[@id='ui-id-5']/span[@class='info'][contains(text(),'";
var caseAccountSearch = "//li[@class='ui-menu-item']/a[@class='ui-corner-all' and contains(text(), '" + caseAccount + "')]";
var customerSearchResults = "//*[@id='ui-id-2']/span[@class='info'][contains(text(),'";
var caseCredentialsGlobalSettings = "//*[@id='globalPreferences']/section/h3[contains(text(), 'Auto')]/strong[contains(text(), 'Count Credentials')]";
var caseCredentialsAccountName = "//dd[@id='autoCountCustName' and contains(text(), '" + caseAccount + "')]";
var caseCredentialsAccountNameUnassigned = "//dd[@id='autoCountCustName' and contains(text(), 'No Customer')]";

var autoCountIntegration = ".//*[@id='autoCountIntegration']/section/header/h2[contains(text(), 'Auto')]/strong[contains(text(), 'Count Credentials')]";
var autoCountLots = "//*[@id='formLotChildren']/h3[contains(text(), 'Lots')]";

var settingsTab = "//a[contains(@title, 'Settings')]";
var integrationsTab = "//a[contains(@title, 'Integrations')]";
var licensesTab = "//a[contains(@title, 'Licenses')]";
var locationsTab = "//a[contains(@title, 'Locations')]";
var locationDetailsTab = "//a[contains(@title, 'Location Details')]";
var locationsHeader = "//h2[contains(text(), 'Locations')]";
var mobileLicensesTab = "//a[contains(@title, 'Mobile Licenses')]";

module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	 *@description Logs the system admin user into Iris and verifies that customer 'oranj' has an assigned CASE account
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test03SameAccountDifferentCustomer: Verify Customer's CASE account is assigned" : function (browser) {
		browser
		.searchForCustomer(true, adminUsername, password, customer, customerName)
		.useXpath()
		// Go to Licenses > Integrations
		.waitForElementVisible(licensesTab, 2000)
		.click(licensesTab)

		.pause(1000)
		.waitForElementVisible(integrationsTab, 4000)
		.click(integrationsTab)

		.pause(2000)
		.assert.elementPresent(autoCountIntegration)

		// Assert the assigned CASE account
		.assert.elementPresent(caseCredentialsAccountName)

		.pause(1000);

	},

	/**
	 *@description Views CASE lots associated with the customer location 'Airport'
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test03SameAccountDifferentCustomer: View CASE Lots associated with the first location 'Airport' for this Customer" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.click(locationsTab)
		.pause(1000)

		.waitForElementPresent("//a[@class='menuListButton menu']", 4000)
		.click("//a[@class='menuListButton menu']")
		.pause(1000)
		.waitForElementPresent("//a[@class='edit btnEditLocation']", 4000)
		.click("//a[@class='edit btnEditLocation']")
		.pause(2000)
		.execute('scrollTo(0,3000)')
		.pause(1000)
		.assert.elementPresent(autoCountLots)
		.elements("xpath", "//ul[@id='locLotFullList']/li", function (result) {
			browser.assert.equal(result.value.length, 3);
		});

	},

	/**
	 *@description Create a new customer with CASE subscription
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test03SameAccountDifferentCustomer: Create a new Customer and verify new Customer's CASE Subscription is enabled" : function (browser) {
		browser
		.useXpath()
		.click("//*[@id='main']/a[@title='Home']")
		.waitForElementVisible("//*[@id='btnAddNewCustomer']", 4000)
		.click("//*[@id='btnAddNewCustomer']")

		browser
		.pause(1000)
		.waitForElementVisible("//*[@id='formCustomerName']", 4000)
		.setValue("//*[@id='formCustomerName']", newCustomerName)
		.pause(500)
		.waitForElementVisible("//*[@id='prntCustomerAC']", 4000)
		.setValue("//*[@id='prntCustomerAC']", newCustomerParent)
		.pause(1000)
		.waitForElementVisible("//*[@id='selectTimeZone']", 4000)
		.setValue("//*[@id='selectTimeZone']", newCustomerTimeZone)
		.pause(500)
		.waitForElementVisible("//li[@class='ui-menu-item']/a[@class='ui-corner-all' and contains(text(), '" + newCustomerTimeZone + "')]", 4000)
		.click("//li[@class='ui-menu-item']/a[@class='ui-corner-all' and contains(text(), '" + newCustomerTimeZone + "')]")
		.pause(2000)
		.click("//label[contains(@class, 'radioLabel') and contains(text(), 'Enabled')]")
		.pause(500)
		.waitForElementVisible("//*[@id='formUserName']", 4000)
		.setValue("id('formUserName')", newCustomerUsername)
		.pause(500)
		.waitForElementVisible("//*[@id='newPassword']", 4000)
		.setValue("id('newPassword')", newCustomerPassword)
		.pause(500)
		.waitForElementVisible("//*[@id='c_newPassword']", 4000)
		.setValue("id('c_newPassword')", newCustomerPassword)

		//Get all subscriptions
		var count = 0;
		browser.elements("xpath", "//*[@for='subscriptionListHidden']/a", function (result) {

			count += result.value.length;

			//Extract all subscription types
			for (n = 0; n < count; n++) {
				browser
				.click("(//*[@for='subscriptionListHidden']/a)[" + (n + 1) + "]");
			}
		});

		browser
		.pause(1000)
		.click("//button/span[contains(text(), 'Add Customer')]")
		.pause(2000);

	},

	/**
	 *@description Finds the newly created customer
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test03SameAccountDifferentCustomer: Find the new Customer" : function (browser) {
		browser
		.useXpath()
		.searchForCustomer(false, adminUsername, password, newCustomerName, newCustomerName);
	},

	/**
	 *@description Assigns 'Texas' CASE account to the new customer
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test03SameAccountDifferentCustomer: Assign the same CASE account to the new Customer" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.waitForElementVisible(licensesTab, 4000)
		.click(licensesTab)

		.pause(2000)
		.waitForElementVisible(integrationsTab, 4000)
		.click(integrationsTab)

		.pause(2000)
		.click("//*[@id='btnEditAutoCountCredentials']")
		.pause(1000)
		.waitForElementVisible("//input[@id='autoCountSearch']", 4000)

		//Clear the previous value from the search box
		.getValue("xpath", "//input[@id='autoCountSearch']", function (result) {
			if (result.value.length > 0) {
				browser.clearValue("//input[@id='autoCountSearch']")
			}
		});

		browser
		.pause(2000)
		.setValue("//input[@id='autoCountSearch']", caseAccountBeginning)
		.pause(1000)
		.waitForElementVisible(caseAccountSearch, 4000)
		.click(caseAccountSearch)
		.click("//*[@id='autoCountSaveBtn']")
		.pause(2000)

		.assert.elementPresent(caseCredentialsAccountName)
		.pause(2000);

	},

	/**
	 *@description Finds the original customer
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test03SameAccountDifferentCustomer: Go back to the original Customer" : function (browser) {
		browser
		.useXpath()
		.click("//*[@id='main']/a[@title='Home']")

		.searchForCustomer(false, adminUsername, password, customer, customerName);

	},

	/**
	 *@description Navigates to Integration tab
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test03SameAccountDifferentCustomer: Go to original Customer's Licenses > Integrations" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible(licensesTab, 2000)
		.click(licensesTab)

		.pause(2000)
		.waitForElementVisible(integrationsTab, 4000)
		.click(integrationsTab);
	},

	/**
	 *@description Verifies that 'Texas' isn't assigned to this customer any more
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test03SameAccountDifferentCustomer: Verify that the CASE Account 'Texas' isn't assigned to it any more" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible(licensesTab, 2000)
		.click(licensesTab)

		.pause(2000)
		.waitForElementVisible(integrationsTab, 4000)
		.click(integrationsTab)

		//Verify that CASE account isn't here anymore
		.pause(2000)
		.assert.elementNotPresent(caseCredentialsAccountName)
		.assert.elementPresent(caseCredentialsAccountNameUnassigned)
		.pause(2000);
	},

	/**
	 *@description Logs out of System admin account and logs into customer's account
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test03SameAccountDifferentCustomer: Log out of System admin and log into Customer's account" : function (browser) {
		browser.logoutAndLogin(customerUsername, password);
	},

	/**
	 *@description Verifies that CASE account location data is absent
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test03SameAccountDifferentCustomer: Verify that the CASE account location data is not present" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.waitForElementPresent(settingsTab, 2000)
		.click(settingsTab)

		.waitForElementPresent(locationsTab, 2000)
		.click(locationsTab)

		.waitForElementPresent(locationsTab, 2000)
		.click(locationsTab)

		.waitForElementPresent(locationDetailsTab, 1000)
		.click(locationDetailsTab)

	},

	/**
	 *@description Verifies that Lots' selections are not present any more since 'Texas' is not customer's CASE account any more
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test03SameAccountDifferentCustomer: Verify that the CASE selections from the 'Texas' account are not present in the options" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.click("//a[@class='menuListButton menu']")
		.pause(2000)
		.click("//a[@class='edit btnEditLocation']")
		.pause(1000)
		.assert.elementPresent(autoCountLots)

		//Customer's CASE property settings are gone now
		.execute('scrollTo(0,3000)')

		.assert.elementPresent("//*[@id='locLotFullList']//li[contains(., 'No lots')]");

	},

	/**
	 *@description Logs the user out
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test03SameAccountDifferentCustomer: Log out" : function (browser) {
		browser.logout();
	}

};
