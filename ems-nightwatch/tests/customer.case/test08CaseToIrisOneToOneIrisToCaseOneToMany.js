/**
 * @summary Integrate with Counts in the Cloud: Tests for One-to-Many and One-to-One mappings with AutoCount and locations
 * @since 7.4.1
 * @version 1.0
 * @author Nadine B
 * @see US898: TC1247 (EMS-6502), TC1248 (EMS-6501)
 **/

var data = require('../../data.js');

var devurl = data("URL");

var adminUsername = data("SysAdminUserName");
var customerUsername = data("AdminUserName");
var customer = "oranj";
var customerName = customer;
var password = data("Password");

var randomNumber = Math.floor((Math.random() * 100) + 1);
var formName = "//*[@id='formLocationName']";
var formCapacity = "//*[@id='formCapacity']";
var originalLocationName = "Location" + randomNumber;
var newLocation = "LocationWithSameCASE" + randomNumber;
var operationModeButton = "//*[@id='formOperatingModeExpand']";
var locOperationMode = "//li[@class='ui-menu-item']/a[contains(text(), 'Multiple')]";
var locCapacity = 100;
var longBeachElement1 = "133 Long Beach";
var longBeachElement2 = "328 Pacific";

var autoCountSelection1 = "//ul[@id='locLotFullList']/li[contains(text(), '" + longBeachElement1 + "')]";
var autoCountSelection2 = "//ul[@id='locLotFullList']/li[contains(text(), '" + longBeachElement2 + "')]";

var editLocation = "//ul/li[contains(@class, 'Child') and contains(@title, '" + originalLocationName + "')]/a[contains(@class, 'menuListButton')]";
var optionButton = "//*[@id='opBtn_pageContent']";
var editButton = "//*[@id='menu_pageContent']/section/a[contains(@title, 'Edit')]";
var save = "//*[@id='locationForm']/section[@class='btnSet']/a[contains(@class, 'Save')]";

var settingsTab = "//a[contains(@title, 'Settings')]";
var integrationsTab = "//a[contains(@title, 'Integrations')]";
var licensesTab = "//a[contains(@title, 'Licenses')]";
var locationsTab = "//a[contains(@title, 'Locations')]";
var locationDetailsTab = "//a[contains(@title, 'Location Details')]";
var locationsHeader = "//h2[contains(text(), 'Locations')]";
var mobileLicensesTab = "//a[contains(@title, 'Mobile Licenses')]";

var autoCountLots = "//*[@id='formLotChildren']/h3[contains(text(), 'Lots')]";

var selectedLongBeachElement1 = "//*[@id='locLotSelectedList']/li[contains(text(), '" + longBeachElement1 + "')]";
var selectedLongBeachElement2 = "//*[@id='locLotSelectedList']/li[contains(text(), '" + longBeachElement2 + "')]";

var availableLongBeachElement1 = "//*[@id='locLotFullList']/li[contains(text(), '" + longBeachElement1 + "')]";
var availableLongBeachElement2 = "//*[@id='locLotFullList']/li[contains(text(), '" + longBeachElement2 + "')]";

var saveLocation = "//*[@id='locationForm']/section/a[contains(@class, 'save') and contains(text(), 'Save')]";

module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	 *@description Logs the system admin user into Iris, enables CASE for customer 'oranj'
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test08CaseToIrisOneToOneIrisToCaseOneToMany: Enable CASE for customer 'oranj' (assumes 'City of Long Beach' is its CASE account)" : function (browser) {
		browser
		.changeCustomersCasePermissions(true, adminUsername, password, customer, customerName, true, false)
		.pause(1000);
	},

	/**
	 *@description Signs into customer account 'oranj'
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test08CaseToIrisOneToOneIrisToCaseOneToMany: Sign into CASE Enabled Customer account" : function (browser) {
		browser.logoutAndLogin(customerUsername, password);
	},

	/**
	 *@description Navigate to Location Details
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test08CaseToIrisOneToOneIrisToCaseOneToMany: (CASE Enabled) Go to Settings > Locations > Location Details" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.waitForElementPresent(settingsTab, 4000)
		.click(settingsTab)

		.waitForElementPresent(locationsTab, 4000)
		.click(locationsTab)

		.waitForElementPresent(locationsTab, 4000)
		.click(locationsTab)

		.waitForElementPresent(locationDetailsTab, 4000)
		.click(locationDetailsTab)
		.pause(1000);
	},

	/**
	 *@description Adds a location with two CASE lots
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test08CaseToIrisOneToOneIrisToCaseOneToMany: Add a new location with the two CASE options - fill out the form" : function (browser) {
		browser
		.useXpath()
		.click("//*[@id='btnAddLocation']")

		.pause(2000)
		.waitForElementPresent(formName, 4000)
		.pause(1000)
		.setValue(formName, originalLocationName)
		.pause(1000)
		.click(operationModeButton)
		.pause(1000)
		.click(locOperationMode)
		.waitForElementPresent(formCapacity, 4000)
		.pause(1000)
		.setValue(formCapacity, locCapacity)
		.pause(1000)
		.waitForElementPresent(autoCountLots, 4000)

		.pause(1000)
		.assert.elementPresent(availableLongBeachElement1)
		.pause(1000)
		.assert.elementPresent(availableLongBeachElement2)

		.pause(1000)
		.click(availableLongBeachElement1)
		.pause(1000)
		.click(availableLongBeachElement2)
		.pause(1000)
		.assert.visible(selectedLongBeachElement1)
		.assert.visible(selectedLongBeachElement2)

		.click(saveLocation)
		.pause(2000);

	},

	/**
	 *@description Verifies the CASE lots are saved properly
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test08CaseToIrisOneToOneIrisToCaseOneToMany: Verify that AutoCount location data is saved" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementPresent("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + originalLocationName + "')]", 4000)
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + originalLocationName + "')]")
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + originalLocationName + "')]")

		.assert.elementPresent(autoCountLots, 4000)

		.assert.elementPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]")
		.assert.elementPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement2 + "')]")

		.assert.visible("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]")
		.assert.visible("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement2 + "')]")
	},

	/**
	 *@description Adds a new location with same first CASE lot
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test08CaseToIrisOneToOneIrisToCaseOneToMany: Add another location with same first AutoCount property - fill out the form" : function (browser) {
		browser
		.useXpath()
		.click("//*[@id='btnAddLocation']")

		.pause(2000)
		.waitForElementPresent(formName, 4000)
		.pause(1000)
		.setValue(formName, newLocation)
		.pause(1000)
		.click(operationModeButton)
		.pause(1000)
		.click(locOperationMode)
		.waitForElementPresent(formCapacity, 4000)
		.pause(1000)
		.setValue(formCapacity, locCapacity)
		.pause(1000)
		.waitForElementPresent(autoCountLots, 4000)

		.pause(1000)
		.assert.elementPresent(availableLongBeachElement1)

		.pause(1000)
		.click(availableLongBeachElement1)

		.pause(1000)
		.assert.visible(selectedLongBeachElement1)
		.assert.hidden(selectedLongBeachElement2)

		.click(saveLocation)
		.pause(2000);

	},

	/**
	 *@description Verifies the one CASE lot was added properly
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test08CaseToIrisOneToOneIrisToCaseOneToMany: Verify that the new AutoCount location data is saved properly (only one selection)" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementPresent("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + newLocation + "')]", 4000)
		.pause(1000)
		.execute('scrollTo(0,3000)')
		.pause(500)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + "Unassigned" + "')]")
		.pause(1000)
		.execute('scrollTo(0,3000)')
		.pause(500)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + newLocation + "')]")

		.assert.elementPresent(autoCountLots, 4000)
		.pause(1000)
		.assert.elementPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]")
		.assert.elementNotPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement2 + "')]")
		.pause(1000)
		.assert.visible("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]")
	},

	/**
	 *@description Verifies that the original location is missing the lot added to the new one
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test08CaseToIrisOneToOneIrisToCaseOneToMany: Verify that the original AutoCount location data is missing the one assigned to the newer location" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementPresent("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + originalLocationName + "')]", 4000)
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + newLocation + "')]")
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + originalLocationName + "')]")

		.assert.elementPresent(autoCountLots, 4000)
		.pause(1000)
		.assert.elementNotPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]")
		.assert.elementPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement2 + "')]")
		.pause(1000)
		.assert.visible("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement2 + "')]")

	},

	/**
	 *@description Adds the second lot to the new location
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test08CaseToIrisOneToOneIrisToCaseOneToMany: Click on 'Edit' button for the new location and add everything that original location has" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementPresent("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + newLocation + "')]", 4000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + newLocation + "')]")
		.pause(2000)
		.click(optionButton)
		.pause(1000)
		.click(editButton)
		.pause(2000)
		.waitForElementPresent(autoCountLots, 4000)

		.pause(1000)
		.assert.elementPresent(availableLongBeachElement2)
		.pause(1000)

		.pause(1000)
		.click(availableLongBeachElement2)

		.pause(1000)
		.assert.visible(selectedLongBeachElement2)
		.click(saveLocation)
	},

	/**
	 *@description Verifies both lots are added properly
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test08CaseToIrisOneToOneIrisToCaseOneToMany: Verify that the new AutoCount location data is saved (second selection added)" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementPresent("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + newLocation + "')]", 4000)
		.pause(1000)
		.execute('scrollTo(0,3000)')
		.pause(500)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + "Unassigned" + "')]")
		.pause(1000)
		.execute('scrollTo(0,3000)')
		.pause(500)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + newLocation + "')]")

		.assert.elementPresent(autoCountLots, 4000)

		.pause(1000)
		.assert.elementPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]")
		.assert.elementPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement2 + "')]")
		.pause(1000)
		.assert.visible("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]")
		.assert.visible("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement2 + "')]")

	},

	/**
	 *@description Verifies the original location is missing both of the lots
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test08CaseToIrisOneToOneIrisToCaseOneToMany: Verify the original location data is missing both of the locations assigned to the newer location" : function (browser) {
		browser
		.useXpath()
		.pause(2000)

		.waitForElementPresent("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + originalLocationName + "')]", 4000)
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + "Unassigned" + "')]")
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + originalLocationName + "')]")

		.assert.elementPresent(autoCountLots, 4000)

		.pause(1000)
		.assert.elementNotPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]")
		.assert.elementNotPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement2 + "')]")
		.pause(1000)

	},

	/**
	 *@description Logs the user out
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test08CaseToIrisOneToOneIrisToCaseOneToMany: Log out" : function (browser) {
		browser.logout();

	},
};
