/**
 * @summary Integrate with Counts in the Cloud: Tests that AutoCount account can be assigned to customer
 * @since 7.4.1
 * @version 1.0
 * @author Nadine B
 * @see US898: TC1236, TC1237
 * @required by: test26DeleteAutoCountTabs, test25WidgetListWithSubscription, test24WidgetListWithoutSubscription, test23WidgetNoParent, test22WidgetVerifyDataPresent, test21WidgetDeleteSave, test20WidgetEditSave, test19WidgetAddSave, test18WidgetNameDescription, test17AddCaseOnlyLocation 
 **/

var data = require('../../data.js');

var devurl = data("URL");

var adminUsername = data("SysAdminUserName");
var customerUsername = data("AdminUserName");
var customer = "oranj";
var password = data("Password");

var caseAccountSearchResults = "//*[@id='ui-id-5']/span[@class='info'][contains(text(),'";
var caseAccountBeginning = "Texas";
var caseAccount = "Texas A&M University";
var caseCredentialsGlobalSettings = "//*[@id='globalPreferences']/section/h3[contains(text(), 'Auto')]/strong[contains(text(), 'Count Credentials')]";
var caseCredentialsAccountName = "//dd[@id='autoCountCustName' and contains(text(), '" + caseAccount + "')]";
var caseAccountSearch = "//li[@class='ui-menu-item']/a[@class='ui-corner-all' and contains(text(), '" + caseAccount + "')]";

var settingsTab = "//a[contains(@title, 'Settings')]";
var integrationsTab = "//a[contains(@title, 'Integrations')]";
var licensesTab = "//a[contains(@title, 'Licenses')]";
var mobileLicensesTab = "//a[contains(@title, 'Mobile Licenses')]";

module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	 *@description Logs the system admin user into Iris and verifies that the customer 'oranj' has CASE subscription
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test02AccountAssignedToChild: Verify new Customer's CASE Subscription is enabled" : function (browser) {
		browser.changeCustomersCasePermissions(true, adminUsername, password, customer, customer, true, false);
	},

	/**
	 *@description Navigate to Integrations
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test02AccountAssignedToChild: Go to Licenses > Integrations" : function (browser) {
		browser
		.pause(1000)
		.useXpath()
		.waitForElementVisible(licensesTab, 2000)
		.click(licensesTab)

		.pause(2000)
		.waitForElementVisible(integrationsTab, 4000)
		.click(integrationsTab);
	},

	/**
	 *@description Assigns CASE account to customer 'oranj'
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test02AccountAssignedToChild: Assign a CASE account to the Customer" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.waitForElementVisible(licensesTab, 2000)
		.click(licensesTab)

		.pause(2000)
		.waitForElementVisible(integrationsTab, 4000)
		.click(integrationsTab)

		.pause(2000)
		.click("//*[@id='btnEditAutoCountCredentials']")
		.pause(1000)
		.waitForElementVisible("//input[@id='autoCountSearch']", 4000)

		//Clear the previous value from the search box
		.getValue("xpath", "//input[@id='autoCountSearch']", function (result) {
			if (result.value.length > 0) {
				browser.clearValue("//input[@id='autoCountSearch']")
			}
		});

		browser
		.pause(2000)
		.setValue("//input[@id='autoCountSearch']", caseAccountBeginning)
		.pause(1000)
		.waitForElementVisible(caseAccountSearch, 4000)
		.click(caseAccountSearch)
		.click("//*[@id='autoCountSaveBtn']")
		.pause(2000)

		.assert.elementPresent(caseCredentialsAccountName)
		.pause(1000);

	},

	/**
	 *@description Switches tabs to verify that CASE account remains assigned
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test02AccountAssignedToChild: Verify that the CASE account name is present even after switching the tabs" : function (browser) {
		browser
		.pause(1000)
		.useXpath()
		.click(mobileLicensesTab)
		.pause(1000)
		.click(licensesTab)
		.pause(1000)
		.waitForElementVisible(integrationsTab, 4000)
		.click(integrationsTab)
		.pause(2000)
		.assert.elementPresent(caseCredentialsAccountName)
		.pause(1000);
	},

	/**
	 *@description Logs out of System admin and logs into customer account
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test02AccountAssignedToChild: Log out of System Admin account and log into Customer account" : function (browser) {
		browser
		.logoutAndLogin(customerUsername, password)
		.pause(1000);
	},

	/**
	 *@description Navigates to Global Settings and verifies CASE account assignment is present
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test02AccountAssignedToChild: Go to Settings > CASE credentials present in the Global Settings" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementVisible(settingsTab, 4000)
		.click(settingsTab)
		.pause(3000)
		.assert.elementPresent(caseCredentialsAccountName);

	},

	/**
	 *@description Logs out
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test02AccountAssignedToChild: Log out" : function (browser) {
		browser.logout();
	}

};
