/**
* @summary Customer Admin: Settings: Locations: Create New Location with only CASE lot assigned
* @since 7.3.5
* @version 1.0
* @author Lonney McD
* @see N/A
* @requires test02AccountAssignedToChild
* @required by: test26DeleteAutoCountTabs, test25WidgetListWithSubscription, test24WidgetListWithoutSubscription, test23WidgetNoParent, test22WidgetVerifyDataPresent, test21WidgetDeleteSave, test20WidgetEditSave, test19WidgetAddSave, test18WidgetNameDescription 
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var pass = data("Password");
var defaultPass= data("DefaultPassword");


module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	 *@description Logs into customer account (oranj)
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test17AddCaseOnlyLocation: Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.initialLogin(username, pass, defaultPass)
			.assert.title('Digital Iris - Main (Dashboard)');
	},		
			
	/**
	 *@description navigates to Settings> Locations
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test17AddCaseOnlyLocation: Navigate to Settings> Locations page" : function (browser) 
	{
		browser.navigateTo("Settings");
		browser.navigateTo("Locations");
		browser.pause(5000);
	},
	
	/**
	 *@description Click the Add (+) Button to create a new Location
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test17AddCaseOnlyLocation: Click the Add (+) Button to create a new Location" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@id='btnAddLocation']/img", 4000)
			.click("//a[@id='btnAddLocation']/img")
			.pause(1000)
	},
	
	/**
	 *@description Fill in Name, Operating Mode and Capacity
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test17AddCaseOnlyLocation: Fill in Name, Operating Mode and Capacity" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//input[@id='formLocationName']", 2000)
			.setValue("//input[@id='formLocationName']", 'CASE location')
			.assert.visible("//input[@id='formOperatingMode']")
			.setValue("//input[@id='formOperatingMode']", 'Multiple')
			.assert.visible("//input[@id='formCapacity']")
			.setValue("//input[@id='formCapacity']", '100')
			.pause(1000)
	},
	
	/**
	 *@description Select AutoCount lot TAMU Lot 40
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test17AddCaseOnlyLocation: Click the Checkbox beside the TAMU Lot 40 AutoCount Mapping Lot" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//ul[@id='locLotFullList']/li[contains(text(),'TAMU Lot 40')]", 4000)
			.click("//ul[@id='locLotFullList']/li[contains(text(),'TAMU Lot 40')]")
			.pause(1000)
	},	
	
	/**
	 *@description Click Save button
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test17AddCaseOnlyLocation: Click the Save Button to create the Location to test AutoCount location 1" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='linkButtonFtr textButton save']", 1000)
			.click("//a[@class='linkButtonFtr textButton save']")
			.waitForElementVisible("(//li[@class='Child'])[contains(@title,'CASE location ')]", 6000)
			.pause(3000)
	},
	
	/**
	 *@description Verify CASE location is saved
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test17AddCaseOnlyLocation: Verify the CASE Location AutoCount lot is displayed 1" : function (browser)
	{
		browser
			.useXpath()
			.click("(//li[@class='Child'])[contains(@title,'Airport')]")
			.pause(2000)
			.click("(//li[@class='Child'])[contains(@title,'CASE location')]")
			.pause(2000)
			.assert.elementPresent("//ul[@id='lotChildList']/li[contains(text(),'TAMU Lot 40')]")
			.pause(1000)
	},
	
	/**
	 *@description Click Option Menu for Airport location
	 *@param browser - The web browser object
	 *@returns void
	 **/
	 "test17AddCaseOnlyLocation: Click the Option Menu for the Location Airport" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("(//li[@class='Child'])[contains(@title,'Airport')]//a[@title='Option Menu']", 4000)
			.click("(//li[@class='Child'])[contains(@title,'Airport')]//a[@title='Option Menu']")
	},
	
	/**
	 *@description Click edit button in Airport option menu
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test17AddCaseOnlyLocation: Click Edit on the Option Menu for the Location Airport" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("(//li[@class='Child'])[contains(@title,'Airport')]/following-sibling::section//a[@title='Edit']", 4000)
			.click("(//li[@class='Child'])[contains(@title,'Airport')]/following-sibling::section//a[@title='Edit']")
			.pause(1000)
	},	
	
	/**
	 *@description Select AutoCount lot TAMU Lot 30
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test17AddCaseOnlyLocation: Click the Checkbox beside the TAMU Lot 30 AutoCount Mapping Lot" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//ul[@id='locLotFullList']/li[contains(text(),'TAMU Lot 30')]", 4000)
			.click("//ul[@id='locLotFullList']/li[contains(text(),'TAMU Lot 30')]")
			.pause(1000)
	},	
	
	/**
	 *@description Click Save button
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test17AddCaseOnlyLocation: Click the Save Button to create the Location to test AutoCount location 2" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='linkButtonFtr textButton save']", 1000)
			.click("//a[@class='linkButtonFtr textButton save']")
			.pause(3000)
	},
	
	/**
	 *@description Verify CASE location is saved
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test17AddCaseOnlyLocation: Verify the CASE Location AutoCount lot is displayed 2" : function (browser)
	{
		browser
			.useXpath()
			.click("(//li[@class='Child'])[contains(@title,'CASE location')]")
			.click("(//li[@class='Child'])[contains(@title,'Airport')]")
			.waitForElementVisible("//ul[@id='lotChildList']/li[contains(text(),'TAMU Lot 30')]",2000)
			.pause(1000)
	},
	
	"test17AddCaseOnlyLocation: Logout" : function (browser)
	
	{
		browser.logout();
	},
	
};