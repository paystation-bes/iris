/**
* @summary Customer Admin: Dashboard: Verify AutoCount widget names and descriptions are correct in the widget list
* @since 7.4.1
* @version 1.0
* @author Lonney McD
* @see EMS9523
* @requires test02AccountAssignedToChild, test17AddCaseOnlyLocation
* @required N/A
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var pass = data("Password");
var defaultPass= data("DefaultPassword");


module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	 *@description Logs into customer account (oranj)
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test18WidgetNameDescription: Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.initialLogin(username, pass, defaultPass)
			.assert.title('Digital Iris - Main (Dashboard)');
	},		
			
	/**
	 *@description Switches Dash to edit mode
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test18WidgetNameDescription: Switch Dash to edit mode 1" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='headerTools']/a[@title='Edit']", 1000, false)
			.click("//section[@id='headerTools']/a[@title='Edit']")
			.pause(2000)
			.assert.visible("//a[@title='Add Widget']")
			;
	},
	
	/**
	 *@description Click the Add Widget Button 1
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test18WidgetNameDescription: Click the Add Widget Button 1" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']", 1000, false)
			.click("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']")
			.pause(3000)
			.assert.visible("//div[@aria-describedby='widgetList' and @role='dialog']")
			.assert.visible("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']/span[text()='Add widget']")
			.assert.visible("//button/span[text()='Cancel']")
			.pause(1000)
			;
	},
	
	/**
	 *@description Expand Counted Occupancy Section of Widget List
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test18WidgetNameDescription: Expand Counted Occupancy Section of Widget List" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widgetListContainer']//h3[text() = 'Counted Occupancy']", 1000)
		.click("//section[@class='widgetListContainer']//h3[text() = 'Counted Occupancy']")
		.pause(1000)
		;
	},	

	/**
	 *@description Verify Counted Occupancy Widget Names and Descriptions
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test18WidgetNameDescription: Verify Counted Occupancy Widget Names and Descriptions" : function (browser) 
	{
		browser
		.useXpath()
		.assert.elementPresent("//section[@class='widgetListContainer']//h3[text() = 'Counted Occupancy']")
		.assert.elementPresent("//section[@class='widgetListContainer']//li//header[text() = 'Counted Occupancy by Hour']")
		.assert.elementPresent("//section[@class='widgetListContainer']//li//article[text() = 'Counted occupancy for the last 24 hours grouped by hour']")
		.assert.elementPresent("//section[@class='widgetListContainer']//li//header[text() = 'Counted Occupancy by Location']")
		.assert.elementPresent("//section[@class='widgetListContainer']//li//article[text() = 'Counted occupancy grouped by location']")
		.pause(1000)
		;
	},
	
	/**
	 *@description Expand Counted/Paid Occupancy Section of Widget List
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test18WidgetNameDescription: Expand Counted/Paid Occupancy Section of Widget List" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widgetListContainer']//h3[text() = 'Counted/Paid Occupancy']", 1000)
		.click("//section[@class='widgetListContainer']//h3[text() = 'Counted/Paid Occupancy']")
		.pause(1000)
		;
	},	

	/**
	 *@description Verify Counted/Paid Occupancy Widget Names and Descriptions
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test18WidgetNameDescription: Verify Counted/Paid Occupancy Widget Names and Descriptions" : function (browser) 
	{
		browser
		.useXpath()
		.assert.visible("//section[@class='widgetListContainer']//h3[text() = 'Counted/Paid Occupancy']")
		.assert.visible("//section[@class='widgetListContainer']//li//header[text() = 'Counted/Paid Occ. by Hour']")
		.assert.visible("//section[@class='widgetListContainer']//li//article[text() = 'Counted/paid occupancy for the last 24 hours grouped by hour']")
		.assert.visible("//section[@class='widgetListContainer']//li//header[text() = 'Counted/Paid Occ. by Location']")
		.assert.visible("//section[@class='widgetListContainer']//li//article[text() = 'Counted/paid occupancy for now grouped by location']")
		.pause(1000)
		;
	},
	
	/**
	 *@description Logout
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test18WidgetNameDescription: Logout" : function (browser)
	
	{
		browser.logout();
	},
	
};