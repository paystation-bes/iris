/**
* @summary Customer Admin: Dashboard: Delete all AutoCount widget, apply Changes, and save to dashboard
* @since 7.4.1
* @version 1.0
* @author Lonney McD
* @see EMS9526
* @requires test02AccountAssignedToChild, test17AddCaseOnlyLocation, test19WidgetAddSave, test20WidgetEditSave
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var pass = data("Password");
var defaultPass= data("DefaultPassword");


module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	 *@description Logs into customer account (oranj)
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.initialLogin(username, pass, defaultPass)
			.assert.title('Digital Iris - Main (Dashboard)');
	},		

	/**
	 *@description Selects Counted Occupancy tab
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test21WidgetDeleteSave: Select Counted Occupancy tab" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='tab'][contains(text(), 'Counted Occupancy')]", 1000, false)
			.click("//a[@class='tab'][contains(text(), 'Counted Occupancy')]")
			.pause(2000)
			;
	},
		
	/**
	 *@description Switches Dash to edit mode
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test21WidgetDeleteSave: Switch Dash to edit mode" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='headerTools']/a[@title='Edit']", 1000, false)
			.click("//section[@id='headerTools']/a[@title='Edit']")
			.pause(2000)
			.assert.elementPresent("//a[@title='Add Widget']")
			;
	},
	
	/**
	 *@description Click option button for Counted Occupancy by Hour widget
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Click option button for Counted Occupancy by Hour widget" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widgetObject']//span[text()= 'Counted Occupancy by Hour Edit']/..//a[@title='Option Menu']", 1000)
		.click("//section[@class='widgetObject']//span[text()= 'Counted Occupancy by Hour Edit']/..//a[@title='Option Menu']")
		.pause(1000);
	},
		
	/**
	 *@description Click the Options > Delete Button
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Click the Options > Delete Button for Counted Occupancy by Hour" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widget']//span[@class='wdgtName'][text()='Counted Occupancy by Hour Edit']//..//..//..//a[contains(@class,'delete')]", 2000)
		.click("//section[@class='widget']//span[@class='wdgtName'][text()='Counted Occupancy by Hour Edit']//..//..//..//a[contains(@class,'delete')]")
		.pause(1000);
	},
	
	/**
	 *@description Click Delete Widget on Confirmation pop-up
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Confirm Delete Counted Occupancy by Hour Widget" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementVisible("//button[@type='button']//span[text() = 'Delete widget']", 1000)
		.click("//button[@type='button']//span[text() = 'Delete widget']")
		.pause(1000);
	},
	
	/**
	 *@description Click option button for Counted Occupancy by Loc widget
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Click option button for Counted Occupancy by Loc widget" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widgetObject']//span[text()= 'Counted Occupancy by Loc Edit']/..//a[@title='Option Menu']", 1000)
		.click("//section[@class='widgetObject']//span[text()= 'Counted Occupancy by Loc Edit']/..//a[@title='Option Menu']")
		.pause(1000);
	},
		
	/**
	 *@description Click the Options > Delete Button
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Click the Options > Delete Button for Counted Occupancy by Loc" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widget']//span[@class='wdgtName'][text()='Counted Occupancy by Loc Edit']//..//..//..//a[contains(@class,'delete')]", 2000)
		.click("//section[@class='widget']//span[@class='wdgtName'][text()='Counted Occupancy by Loc Edit']//..//..//..//a[contains(@class,'delete')]")
		.pause(1000);
	},
	
	/**
	 *@description Click Delete Widget on Confirmation pop-up
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Confirm Delete Counted Occupancy by Loc Widget" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementVisible("//button[@type='button']//span[text() = 'Delete widget']", 1000)
		.click("//button[@type='button']//span[text() = 'Delete widget']")
		.pause(1000);
	},	
	
	/**
	 *@description Save Dashboard
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Save dashboard 1" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@id='headerTools']//a[@title='Save']", 1000)
		.click("//section[@id='headerTools']//a[@title='Save']")
		.pause(3000)
	},

	/**
	 *@description Confirm that widgets has been deleted
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Confirm Deletion 1" : function (browser)
	{
		browser
		.useXpath()
		.assert.elementNotPresent("//section[@class='widgetObject']//span[text()= 'Counted Occupancy by Hour Edit']")
		.assert.elementNotPresent("//section[@class='widgetObject']//span[text()= 'Counted Occupancy by Loc Edit']")
		.pause(1000)
		;
	},
	
	/**
	 *@description Counted Occupancy: RE-ADD: Switches Dash to edit mode
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test21WidgetDeleteSave: Counted Occupancy: RE-ADD: Switch Dash to edit mode" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='headerTools']/a[@title='Edit']", 1000, false)
			.click("//section[@id='headerTools']/a[@title='Edit']")
			.pause(2000)
			.assert.visible("//a[@title='Add Widget']")
			;
	},
	
	/**
	 *@description Counted Occupancy: RE-ADD: Click the Add Widget Button 1
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Counted Occupancy: RE-ADD: Click the Add Widget Button 1" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']", 1000, false)
			.click("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']")
			.pause(3000)
			.assert.visible("//div[@aria-describedby='widgetList' and @role='dialog']")
			.assert.visible("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']/span[text()='Add widget']")
			.assert.visible("//button/span[text()='Cancel']")
			.pause(1000)
			;
	},
	
	/**
	 *@description Counted Occupancy: RE-ADD: Expand Counted Occupancy Section of Widget List 1
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Counted Occupancy: RE-ADD: Expand Counted Occupancy Section of Widget List 1" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widgetListContainer']//h3[text() = 'Counted Occupancy']", 1000)
		.click("//section[@class='widgetListContainer']//h3[text() = 'Counted Occupancy']")
		.pause(1000)
		;
	},	

	/**
	 *@description Counted Occupancy: RE-ADD: Select Counted Occupancy by location Widget and apply to dash 1
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Counted Occupancy: RE-ADD: Select Counted Occupancy by location Widget and apply to dash 1" : function (browser)
	{
		browser
		.useXpath()
		.assert.elementPresent("//section[@class='widgetListContainer']//li//header[text() = 'Counted Occupancy by Location']")
		.click("//section[@class='widgetListContainer']//li//header[text() = 'Counted Occupancy by Location']")
		.pause(1000)
		.waitForElementVisible("//button//span[text() = 'Add widget']", 1000)
		.click("//button//span[text() = 'Add widget']")
		.pause(1000)
	},

	/**
	 *@description Counted Occupancy: RE-ADD: Click the Add Widget Button 2
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Counted Occupancy: RE-ADD: Click the Add Widget Button 2" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']", 1000, false)
			.click("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']")
			.pause(3000)
			.assert.visible("//div[@aria-describedby='widgetList' and @role='dialog']")
			.assert.visible("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']/span[text()='Add widget']")
			.assert.visible("//button/span[text()='Cancel']")
			.pause(1000)
			;
	},
	
	/**
	 *@description Counted Occupancy: RE-ADD: Expand Counted Occupancy Section of Widget List 2
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Counted Occupancy: RE-ADD: Expand Counted Occupancy Section of Widget List 2" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widgetListContainer']//h3[text() = 'Counted Occupancy']", 1000)
		.click("//section[@class='widgetListContainer']//h3[text() = 'Counted Occupancy']")
		.pause(1000)
		;
	},	

	/**
	 *@description Counted Occupancy: RE-ADD: Select Counted Occupancy by hour Widget and apply to dash 2
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Counted Occupancy: RE-ADD: Select Counted Occupancy by hour Widget and apply to dash 2" : function (browser)
	{
		browser
		.useXpath()
		.assert.elementPresent("//section[@class='widgetListContainer']//li//header[text() = 'Counted Occupancy by Hour']")
		.click("//section[@class='widgetListContainer']//li//header[text() = 'Counted Occupancy by Hour']")
		.pause(1000)
		.waitForElementVisible("//button//span[text() = 'Add widget']", 1000)
		.click("//button//span[text() = 'Add widget']")
		.pause(1000)
	},
	
	/**
	 *@description Counted Occupancy: RE-ADD: Save Edits to Dash
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Counted Occupancy: RE-ADD: Save Edits to Dash" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@id='headerTools']//a[@title='Save']", 1000)
		.click("//section[@id='headerTools']//a[@title='Save']")
		.pause(3000)
	},
	
	/**
	 *@description Counted Occupancy: RE-ADD: Verify widgets saved to Dash
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Counted Occupancy: RE-ADD: Verify Widget Add Success" : function (browser) {
		browser
		.useXpath()
		.assert.visible("(//section[@class='widget']//span[@class='wdgtName'])[text()='Counted Occupancy by Hour']")
		.assert.visible("(//section[@class='widget']//span[@class='wdgtName'])[text()='Counted Occupancy by Location']")
		.pause(1000)
	},	
	
	/**
	 *@description Selects Counted/Paid Occupancy tab
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test21WidgetDeleteSave: Select Counted/Paid Occupancy tab" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='tab'][contains(text(), 'Counted/Paid Occ.')]", 1000, false)
			.click("//a[@class='tab'][contains(text(), 'Counted/Paid Occ.')]")
			.pause(2000)
			;
	},
		
	/**
	 *@description Switches Dash to edit mode
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test21WidgetDeleteSave: Switch Dash to edit mode 2" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='headerTools']/a[@title='Edit']", 1000, false)
			.click("//section[@id='headerTools']/a[@title='Edit']")
			.pause(2000)
			.assert.elementPresent("//a[@title='Add Widget']")
			;
	},
	
	/**
	 *@description Click option button for Counted/Paid Occ. by Hour widget
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Click option button for Counted/Paid Occ. by Hour widget" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widgetObject']//span[text()= 'Counted/Paid Occ. by Hour Edit']/..//a[@title='Option Menu']", 1000)
		.click("//section[@class='widgetObject']//span[text()= 'Counted/Paid Occ. by Hour Edit']/..//a[@title='Option Menu']")
		.pause(1000);
	},
		
	/**
	 *@description Click the Options > Delete Button
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Click the Options > Delete Button for Counted/Paid Occ. by Hour" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widget']//span[@class='wdgtName'][text()='Counted/Paid Occ. by Hour Edit']//..//..//..//a[contains(@class,'delete')]", 2000)
		.click("//section[@class='widget']//span[@class='wdgtName'][text()='Counted/Paid Occ. by Hour Edit']//..//..//..//a[contains(@class,'delete')]")
		.pause(1000);
	},
	
	/**
	 *@description Click Delete Widget on Confirmation pop-up
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Confirm Delete Counted/Paid Occ. by Hour Widget" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementVisible("//button[@type='button']//span[text() = 'Delete widget']", 1000)
		.click("//button[@type='button']//span[text() = 'Delete widget']")
		.pause(1000);
	},
	
	/**
	 *@description Click option button for Counted/Paid Occ. by Loc widget
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Click option button for Counted/Paid Occ. by Loc widget" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widgetObject']//span[text()= 'Counted/Paid Occ. by Loc Edit']/..//a[@title='Option Menu']", 1000)
		.click("//section[@class='widgetObject']//span[text()= 'Counted/Paid Occ. by Loc Edit']/..//a[@title='Option Menu']")
		.pause(1000);
	},
		
	/**
	 *@description Click the Options > Delete Button
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Click the Options > Delete Button for Counted/Paid Occ. by Loc" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widget']//span[@class='wdgtName'][text()='Counted/Paid Occ. by Loc Edit']//..//..//..//a[contains(@class,'delete')]", 2000)
		.click("//section[@class='widget']//span[@class='wdgtName'][text()='Counted/Paid Occ. by Loc Edit']//..//..//..//a[contains(@class,'delete')]")
		.pause(1000);
	},
	
	/**
	 *@description Click Delete Widget on Confirmation pop-up
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Confirm Delete Counted/Paid Occ. by Loc Widget" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementVisible("//button[@type='button']//span[text() = 'Delete widget']", 1000)
		.click("//button[@type='button']//span[text() = 'Delete widget']")
		.pause(1000);
	},	
	
	/**
	 *@description Save Dashboard
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Save dashboard 2" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@id='headerTools']//a[@title='Save']", 1000)
		.click("//section[@id='headerTools']//a[@title='Save']")
		.pause(3000)
	},

	/**
	 *@description Confirm that widgets has been deleted
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Confirm Deletion 2" : function (browser)
	{
		browser
		.useXpath()
		.assert.elementNotPresent("//section[@class='widgetObject']//span[text()= 'Counted/Paid Occ. by Hour Edit']")
		.assert.elementNotPresent("//section[@class='widgetObject']//span[text()= 'Counted/Paid Occ. by Loc Edit']")
		.pause(1000)
		;
	},
	
	/**
	 *@description Counted/Paid Occ.: RE-ADD: Switches Dash to edit mode
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test21WidgetDeleteSave: Counted/Paid Occ.: RE-ADD: Switch Dash to edit mode 1" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='headerTools']/a[@title='Edit']", 1000, false)
			.click("//section[@id='headerTools']/a[@title='Edit']")
			.pause(2000)
			.assert.visible("//a[@title='Add Widget']")
			;
	},
	
	/**
	 *@description Counted/Paid Occ.: RE-ADD: Click the Add Widget Button 1
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Counted/Paid Occ.: RE-ADD: Click the Add Widget Button 1" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']", 1000, false)
			.click("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']")
			.pause(3000)
			.assert.visible("//div[@aria-describedby='widgetList' and @role='dialog']")
			.assert.visible("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']/span[text()='Add widget']")
			.assert.visible("//button/span[text()='Cancel']")
			.pause(1000)
			;
	},
	
	/**
	 *@description Counted/Paid Occ.: RE-ADD: Expand Counted/Paid Occ. Section of Widget List 1
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Counted/Paid Occ.: RE-ADD: Expand Counted/Paid Occ. Section of Widget List 1" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widgetListContainer']//h3[text() = 'Counted/Paid Occupancy']", 1000)
		.click("//section[@class='widgetListContainer']//h3[text() = 'Counted/Paid Occupancy']")
		.pause(1000)
		;
	},	

	/**
	 *@description Counted/Paid Occ.: RE-ADD: Select Counted/Paid Occ. by location Widget and apply to dash 1
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Counted/Paid Occ.: RE-ADD: Select Counted/Paid Occ. by location Widget and apply to dash 1" : function (browser)
	{
		browser
		.useXpath()
		.assert.elementPresent("//section[@class='widgetListContainer']//li//header[text() = 'Counted/Paid Occ. by Location']")
		.click("//section[@class='widgetListContainer']//li//header[text() = 'Counted/Paid Occ. by Location']")
		.pause(1000)
		.waitForElementVisible("//button//span[text() = 'Add widget']", 1000)
		.click("//button//span[text() = 'Add widget']")
		.pause(1000)
	},

	/**
	 *@description Counted/Paid Occ.: RE-ADD: Click the Add Widget Button 2
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Counted/Paid Occ.: RE-ADD: Click the Add Widget Button 2" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']", 1000, false)
			.click("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']")
			.pause(3000)
			.assert.visible("//div[@aria-describedby='widgetList' and @role='dialog']")
			.assert.visible("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']/span[text()='Add widget']")
			.assert.visible("//button/span[text()='Cancel']")
			.pause(1000)
			;
	},
	
	/**
	 *@description Counted/Paid Occ.: RE-ADD: Expand Counted/Paid Occ. Section of Widget List 2
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Counted/Paid Occ.: RE-ADD: Expand Counted/Paid Occ. Section of Widget List 2" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widgetListContainer']//h3[text() = 'Counted/Paid Occupancy']", 1000)
		.click("//section[@class='widgetListContainer']//h3[text() = 'Counted/Paid Occupancy']")
		.pause(1000)
		;
	},	

	/**
	 *@description Counted/Paid Occ.: RE-ADD: Select Counted/Paid Occ. by hour Widget and apply to dash 2
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Counted/Paid Occ.: RE-ADD: Select Counted/Paid Occ. by hour Widget and apply to dash 2" : function (browser)
	{
		browser
		.useXpath()
		.assert.elementPresent("//section[@class='widgetListContainer']//li//header[text() = 'Counted/Paid Occ. by Hour']")
		.click("//section[@class='widgetListContainer']//li//header[text() = 'Counted/Paid Occ. by Hour']")
		.pause(1000)
		.waitForElementVisible("//button//span[text() = 'Add widget']", 1000)
		.click("//button//span[text() = 'Add widget']")
		.pause(1000)
	},
	
	/**
	 *@description Counted/Paid Occ.: RE-ADD: Save Edits to Dash
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Counted/Paid Occ.: RE-ADD: Save Edits to Dash" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@id='headerTools']//a[@title='Save']", 1000)
		.click("//section[@id='headerTools']//a[@title='Save']")
		.pause(3000)
	},
	
	/**
	 *@description Counted/Paid Occ.: RE-ADD: Verify widgets saved to Dash
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Counted/Paid Occ.: RE-ADD: Verify Widget Add Success" : function (browser) {
		browser
		.useXpath()
		.assert.visible("(//section[@class='widget']//span[@class='wdgtName'])[text()='Counted/Paid Occ. by Hour']")
		.assert.visible("(//section[@class='widget']//span[@class='wdgtName'])[text()='Counted/Paid Occ. by Location']")
		.pause(1000)
	},	
	
	/**
	 *@description Logout
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test21WidgetDeleteSave: Logout" : function (browser)

	{
		browser.logout();
	},
};