/**
* @summary Customer Admin: Dashboard: Verify that data is being displayed on the AutoCount widgets
* @since 7.4.1
* @version 1.0
* @author Lonney McD
* @see EMS-9529 & EMS-9537
* @requires test02AccountAssignedToChild, test17AddCaseOnlyLocation, test19WidgetAddSave
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var pass = data("Password");
var defaultPass= data("DefaultPassword");


module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	 *@description Logs into customer account (oranj)
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test22WidgetVerifyDataPresent : Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.initialLogin(username, pass, defaultPass)
			.assert.title('Digital Iris - Main (Dashboard)');
	},		

	/**
	 *@description Selects Counted Occupancy tab
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test22WidgetVerifyDataPresent : Select Counted Occupancy tab" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='tab'][contains(text(), 'Counted Occupancy')]", 1000, false)
			.click("//a[@class='tab'][contains(text(), 'Counted Occupancy')]")
			.pause(2000)
			;
	},
		
	/**
	 *@description Checks that there are no instances of 'There is currently no data returned for this widget.' Present (widgets are displaying data)
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test22WidgetVerifyDataPresent : Verify data is present in Counted Occupancy Widgets" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@class='widget']//span[@class='wdgtName'][text()='Counted Occupancy by Hour']", 2000)
			.waitForElementVisible("//section[@class='widget']//span[@class='wdgtName'][text()='Counted Occupancy by Location']", 2000)
			.assert.hidden("//section[@id]/section/span[text()= 'There is currently no data returned for this widget.']")
			.pause(1000)
			;
	},
	
	/**
	 *@description Selects Counted/Paid Occ. tab
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test22WidgetVerifyDataPresent : Select Counted/Paid Occ. tab" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='tab'][contains(text(), 'Counted/Paid Occ.')]", 1000, false)
			.click("//a[@class='tab'][contains(text(), 'Counted/Paid Occ.')]")
			.pause(2000)
			;
	},
		
	/**
	 *@description Checks that there are no instances of 'There is currently no data returned for this widget.' Present (widgets are displaying data)
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test22WidgetVerifyDataPresent : Verify data is present in Counted/Paid Occ. Widgets" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@class='widget']//span[@class='wdgtName'][text()='Counted/Paid Occ. by Hour']", 2000)
			.waitForElementVisible("//section[@class='widget']//span[@class='wdgtName'][text()='Counted/Paid Occ. by Location']", 2000)
			.assert.hidden("//section[@id]/section/span[text()= 'There is currently no data returned for this widget.']")
			.pause(1000)
			;
	},
	
	/**
	 *@description Logout
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test22WidgetVerifyDataPresent  : Logout" : function (browser)

	{
		browser.logout();
	},
};