/**
 * @summary Integrate with Counts in the Cloud: Tests for no AutoCount in parent locations
 * @since 7.4.1
 * @version 1.0
 * @author Nadine B
 * @see US898: EMS-7188
 **/

var data = require('../../data.js');

var devurl = data("URL");

var adminUsername = data("SysAdminUserName");
var customerUsername = data("AdminUserName");
var customer = "oranj";
var customerName = customer;
var password = data("Password");

var randomNumber = Math.floor((Math.random() * 100) + 1);
var formName = "//*[@id='formLocationName']";
var formCapacity = "//*[@id='formCapacity']";
var locName = "LocSysAdminParent" + randomNumber;
var locName2 = "LocCustomerParent" + randomNumber;

var saveLocation = "//*[@id='locationForm']/section/a[contains(@class, 'save') and contains(text(), 'Save')]";

var autoCountLots = "//*[@id='formLotChildren']/h3[contains(text(), 'Lots')]";

var settingsTab = "//a[contains(@title, 'Settings')]";
var integrationsTab = "//a[contains(@title, 'Integrations')]";
var licensesTab = "//a[contains(@title, 'Licenses')]";
var locationsTab = "//a[contains(@title, 'Locations')]";
var locationDetailsTab = "//a[contains(@title, 'Location Details')]";
var locationsHeader = "//h2[contains(text(), 'Locations')]";

module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	 *@description Logs the system admin user into Iris and verify that CASE is enabled for 'oranj'
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test11CaseParentLocation: Log into System admin account and verify that CASE is enabled for the Customer 'oranj'" : function (browser) {
		browser.changeCustomersCasePermissions(true, adminUsername, password, customer, customerName, true, false);
	},

	/**
	 *@description Navigates to Location Details
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test11CaseParentLocation: Go to Locations > Location Details" : function (browser) {
		browser
		.useXpath()
		.pause(1000)

		.waitForElementPresent(locationsTab, 4000)
		.click(locationsTab)

		.waitForElementPresent(locationsTab, 4000)
		.click(locationsTab)
		.pause(1000);
	},

	/**
	 *@description Adds a new parent location and verifies AutoCount options are hidden
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test11CaseParentLocation: Add a new parent location - fill out the form - verify that the AutoCount options are not there" : function (browser) {
		browser
		.useXpath()
		.click("//*[@id='btnAddLocation']")
		.pause(1000)
		.waitForElementPresent(formName, 4000)
		.pause(1000)
		.setValue(formName, locName)
		.pause(1000)
		.click("//*[@id='parentFlagCheck']")
		.pause(1000)
		.assert.elementPresent("//ul[@id='locLocFullList']/li[contains(text(), 'Airport')]/a[contains(@class, 'checkBox')]")
		.click("//ul[@id='locLocFullList']/li[contains(text(), 'Airport')]/a[contains(@class, 'checkBox')]")

		.assert.hidden(autoCountLots)

		.click(saveLocation)
		.pause(2000)

	},

	/**
	 *@description Logs out of System Admin and into 'oranj' account
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test11CaseParentLocation: Log out of System Admin account and sign into CASE Customer account" : function (browser) {
		browser.logoutAndLogin(customerUsername, password);
	},

	/**
	 *@description Navigates to Location Details
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test11CaseParentLocation: Go to Settings > Locations > Location Details" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementPresent(settingsTab, 4000)
		.click(settingsTab)

		.waitForElementPresent(locationsTab, 4000)
		.click(locationsTab)

		.waitForElementPresent(locationsTab, 4000)
		.click(locationsTab)

		.waitForElementPresent(locationDetailsTab, 4000)
		.click(locationDetailsTab)
		.pause(1000);
	},

	/**
	 *@description Adds another new parent location and verifies AutoCount options are hidden
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test11CaseParentLocation: Add another new location - fill out the form + assert AutoCount option is hidden" : function (browser) {
		browser
		.useXpath()
		.click("//*[@id='btnAddLocation']")
		.pause(1000)
		.waitForElementPresent(formName, 4000)
		.pause(1000)
		.setValue(formName, locName2)
		.pause(1000)
		.click("//*[@id='parentFlagCheck']")
		.pause(1000)
		.assert.elementPresent("//ul[@id='locLocFullList']/li[contains(text(), 'Downtown')]/a[contains(@class, 'checkBox')]")
		.click("//ul[@id='locLocFullList']/li[contains(text(), 'Airport')]/a[contains(@class, 'checkBox')]")

		.assert.hidden(autoCountLots)

		.click(saveLocation)
		.pause(2000)

	},

	/**
	 *@description Logs the user out
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test11CaseParentLocation: logout" : function (browser) {
		browser.logout();
	}

};
