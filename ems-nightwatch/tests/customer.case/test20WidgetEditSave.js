/**
* @summary Customer Admin: Dashboard: Edit all AutoCount widget, apply Changes, and save to dashboard
* @since 7.4.1
* @version 1.0
* @author Lonney McD
* @see EMS9527
* @requires test02AccountAssignedToChild, test17AddCaseOnlyLocation, test19WidgetAddSave
* @required by: test21WidgetDeleteSave
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var pass = data("Password");
var defaultPass= data("DefaultPassword");


module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	 *@description Logs into customer account (oranj)
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave: Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.initialLogin(username, pass, defaultPass)
			.assert.title('Digital Iris - Main (Dashboard)');
	},		

	/**
	 *@description Selects Counted Occupancy tab
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test20WidgetEditSave: Select Counted Occupancy tab" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='tab'][contains(text(), 'Counted Occupancy')]", 1000, false)
			.click("//a[@class='tab'][contains(text(), 'Counted Occupancy')]")
			.pause(2000)
			;
	},
		
	/**
	 *@description Switches Dash to edit mode
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test20WidgetEditSave: Switch Dash to edit mode" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='headerTools']/a[@title='Edit']", 1000, false)
			.click("//section[@id='headerTools']/a[@title='Edit']")
			.pause(2000)
			.assert.elementPresent("//a[@title='Add Widget']")
			;
	},
	
	/**
	 *@description Click option button for Counted Occupancy widget
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave: Click option button for Counted Occupancy widget" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widgetObject']//span[text()= 'Counted Occupancy by Hour']/..//a[@title='Option Menu']", 1000)
		.click("//section[@class='widgetObject']//span[text()= 'Counted Occupancy by Hour']/..//a[@title='Option Menu']")
		.pause(1000);
	},
		
	/**
	 *@description Click the Options > Settings Button
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave: Click the Options > Settings Button" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widget']//span[@class='wdgtName'][text()='Counted Occupancy by Hour']//..//..//..//a[contains(@class,'settings')]", 2000)
		.click("//section[@class='widget']//span[@class='wdgtName'][text()='Counted Occupancy by Hour']//..//..//..//a[contains(@class,'settings')]")
		.pause(1000);
	},
	
	/**
	 *@description Changes Name, then Clicks Area Chart, then Column Chart
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave: Change Name & Edit chart type" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementVisible("//input[@id='widgetName']", 1000)
		.clearValue("//input[@id='widgetName']")
		.setValue("//input[@id='widgetName']", 'Counted Occupancy by Hour Edit')
		.pause(1000)
		.waitForElementVisible("//section[@id]/img[contains(@src,'chartThumb_Area')]", 1000)
		.click("//section[@id]/img[contains(@src,'chartThumb_Area')]")
		.waitForElementVisible("//section[@id]/img[contains(@src,'chartThumb_Column')]", 1000)
		.click("//section[@id]/img[contains(@src,'chartThumb_Column')]")
		.pause(1000)
		;
	},	

	/**
	 *@description Apply Changes to Widget
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave: Apply Changes to Widget" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//button[@type='button']//span[text() = 'Apply changes']", 1000)
		.click("//button[@type='button']//span[text() = 'Apply changes']")
		.pause(3000)
	},
	
	/**
	 *@description Click option button for Counted Occupancy by Location widget
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave: Click option button for Counted Occupancy by Location widget" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widgetObject']//span[text()= 'Counted Occupancy by Location']/..//a[@title='Option Menu']", 1000)
		.click("//section[@class='widgetObject']//span[text()= 'Counted Occupancy by Location']/..//a[@title='Option Menu']")
		.pause(1000);
	},
		
	/**
	 *@description Click the Options > Settings Button
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave: Click the Options > Settings Button for Counted Occupancy by Location" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widget']//span[@class='wdgtName'][text()='Counted Occupancy by Location']//..//..//..//a[contains(@class,'settings')]", 2000)
		.click("//section[@class='widget']//span[@class='wdgtName'][text()='Counted Occupancy by Location']//..//..//..//a[contains(@class,'settings')]")
		.pause(1000);
	},
	
	/**
	 *@description Changes Name, then Clicks Area Chart, then Column Chart
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave: Change Counted Occupancy by Location Name & Edit chart type" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementVisible("//input[@id='widgetName']", 1000)
		.clearValue("//input[@id='widgetName']")
		.setValue("//input[@id='widgetName']", 'Counted Occupancy by Loc Edit')
		.pause(1000)
		.waitForElementVisible("//section[@id]/img[contains(@src,'chartThumb_Area')]", 1000)
		.click("//section[@id]/img[contains(@src,'chartThumb_Area')]")
		.waitForElementVisible("//section[@id]/img[contains(@src,'chartThumb_Column')]", 1000)
		.click("//section[@id]/img[contains(@src,'chartThumb_Column')]")
		.pause(1000)
		;
	},	

	/**
	 *@description Apply Changes to Widget
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave: Apply Changes to Counted Occupancy by Location" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//button[@type='button']//span[text() = 'Apply changes']", 1000)
		.click("//button[@type='button']//span[text() = 'Apply changes']")
		.pause(3000)
	},

	/**
	 *@description Save Dashboard
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave: Save dashboard 1" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@id='headerTools']//a[@title='Save']", 1000)
		.click("//section[@id='headerTools']//a[@title='Save']")
		.pause(3000)
	},
	
	/**
	 *@description Verify Counted Occupancy widgets has been changed
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave: Verify Counted Occupancy widgets changes on saved dashboard" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widgetObject']//span[text()= 'Counted Occupancy by Hour Edit']", 1000)
		.assert.elementPresent("//section[@class='widgetObject']//span[text()= 'Counted Occupancy by Hour Edit']")
		.waitForElementVisible("//section[@class='widgetObject']//span[text()= 'Counted Occupancy by Loc Edit']", 1000)
		.assert.elementPresent("//section[@class='widgetObject']//span[text()= 'Counted Occupancy by Loc Edit']")
		.pause(1000)
		;
	},	
		
	/**
	 *@description Selects Counted/Paid Occ. tab
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test20WidgetEditSave: Select Counted/Paid Occ. tab" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//a[@class='tab'][contains(text(), 'Counted/Paid Occ.')]", 1000, false)
			.click("//a[@class='tab'][contains(text(), 'Counted/Paid Occ.')]")
			.pause(2000)
			;
	},		
	
	/**
	 *@description Switches Dash to edit mode 2
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test20WidgetEditSave: Switch Dash to edit mode 2" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='headerTools']/a[@title='Edit']", 1000, false)
			.click("//section[@id='headerTools']/a[@title='Edit']")
			.pause(2000)
			.assert.elementPresent("//a[@title='Add Widget']")
			;
	},
	
	/**
	 *@description Click option button for Counted/Paid Occupancy by Location widget
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave: Click option button for Counted/Paid Occupancy by Location widget" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widgetObject']//span[text()= 'Counted/Paid Occ. by Location']/..//a[@title='Option Menu']", 1000)
		.click("//section[@class='widgetObject']//span[text()= 'Counted/Paid Occ. by Location']/..//a[@title='Option Menu']")
		.pause(1000);
	},
		
	/**
	 *@description Click the Options > Settings Button
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave: Click the Options > Settings Button for Counted/Paid Occ. by Location" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widget']//span[@class='wdgtName'][text()='Counted/Paid Occ. by Location']//..//..//..//a[contains(@class,'settings')]", 2000)
		.click("//section[@class='widget']//span[@class='wdgtName'][text()='Counted/Paid Occ. by Location']//..//..//..//a[contains(@class,'settings')]")
		.pause(1000);
	},
	
	/**
	 *@description Changes Name, then Clicks Area Chart, then Column Chart
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave: Change Counted/Paid Occ. by Location Name & Edit chart type" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementVisible("//input[@id='widgetName']", 1000)
		.clearValue("//input[@id='widgetName']")
		.setValue("//input[@id='widgetName']", 'Counted/Paid Occ. by Loc Edit')
		.pause(1000)
		.waitForElementVisible("//section[@id]/img[contains(@src,'chartThumb_Area')]", 1000)
		.click("//section[@id]/img[contains(@src,'chartThumb_Area')]")
		.waitForElementVisible("//section[@id]/img[contains(@src,'chartThumb_Column')]", 1000)
		.click("//section[@id]/img[contains(@src,'chartThumb_Column')]")
		.pause(1000)
		;
	},	

	/**
	 *@description Apply Changes to Widget
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave: Apply Changes to Counted/Paid Occ. by Location" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//button[@type='button']//span[text() = 'Apply changes']", 1000)
		.click("//button[@type='button']//span[text() = 'Apply changes']")
		.pause(3000)
	},

	/**
	 *@description Click option button for Counted/Paid Occ. by Hour widget
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave: Click option button for Counted/Paid Occ. by Hour widget" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widgetObject']//span[text()= 'Counted/Paid Occ. by Hour']/..//a[@title='Option Menu']", 1000)
		.click("//section[@class='widgetObject']//span[text()= 'Counted/Paid Occ. by Hour']/..//a[@title='Option Menu']")
		.pause(1000);
	},
		
	/**
	 *@description Click the Options > Settings Button
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave: Click the Options > Settings Button for Counted/Paid Occ. by Hour" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widget']//span[@class='wdgtName'][text()='Counted/Paid Occ. by Hour']//..//..//..//a[contains(@class,'settings')]", 2000)
		.click("//section[@class='widget']//span[@class='wdgtName'][text()='Counted/Paid Occ. by Hour']//..//..//..//a[contains(@class,'settings')]")
		.pause(1000);
	},
	
	/**
	 *@description Changes Name, then Clicks Area Chart, then Column Chart
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave: Change Counted/Paid Occ. by Hour Name & Edit chart type" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementVisible("//input[@id='widgetName']", 1000)
		.clearValue("//input[@id='widgetName']")
		.setValue("//input[@id='widgetName']", 'Counted/Paid Occ. by Hour Edit')
		.pause(1000)
		.waitForElementVisible("//section[@id]/img[contains(@src,'chartThumb_Area')]", 1000)
		.click("//section[@id]/img[contains(@src,'chartThumb_Area')]")
		.waitForElementVisible("//section[@id]/img[contains(@src,'chartThumb_Column')]", 1000)
		.click("//section[@id]/img[contains(@src,'chartThumb_Column')]")
		.pause(1000)
		;
	},	

	/**
	 *@description Apply Changes to Widget
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave: Apply Changes to Counted/Paid Occ. by Hour" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//button[@type='button']//span[text() = 'Apply changes']", 1000)
		.click("//button[@type='button']//span[text() = 'Apply changes']")
		.pause(3000)
	},
	
	/**
	 *@description Save Dashboard
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave: Save dashboard" : function (browser)
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@id='headerTools']//a[@title='Save']", 1000)
		.click("//section[@id='headerTools']//a[@title='Save']")
		.pause(3000)
	},
	
	/**
	 *@description Verify name has been changed
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave: Verify Counted/Paid Occupancy widgets changes on saved dashboard" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementVisible("//section[@class='widgetObject']//span[text()= 'Counted/Paid Occ. by Hour Edit']", 1000)
		.assert.elementPresent("//section[@class='widgetObject']//span[text()= 'Counted/Paid Occ. by Hour Edit']")
		.waitForElementVisible("//section[@class='widgetObject']//span[text()= 'Counted/Paid Occ. by Loc Edit']", 1000)
		.assert.elementPresent("//section[@class='widgetObject']//span[text()= 'Counted/Paid Occ. by Loc Edit']")
		.pause(1000)
		;
	},	

	/**
	 *@description Logout
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test20WidgetEditSave : Logout" : function (browser)

	{
		browser.logout();
	},
};