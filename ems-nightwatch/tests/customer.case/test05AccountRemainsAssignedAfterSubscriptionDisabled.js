/**
 * @summary Integrate with Counts in the Cloud: Tests that after AutoCount subscription is disabled, AutoCount account remains assigned
 * @since 7.4.1
 * @version 1.0
 * @author Nadine B
 * @see US898: TC1243 (EMS-6600)
 **/

var data = require('../../data.js');

var devurl = data("URL");

var adminUsername = data("SysAdminUserName");
var customerUsername = data("AdminUserName");
var customer = "oranj";
var customerName = customer;
var password = data("Password");

var customerSearchResults = "//*[@id='ui-id-2']/span[@class='info'][contains(text(),'";

var autoCountIntegration = ".//*[@id='autoCountIntegration']/section/header/h2[contains(text(), 'Auto')]/strong[contains(text(), 'Count Credentials')]";
var caseAccount1 = "Texas";
var caseAccount2Beginning = "City of Long";
var caseAccount2 = "City of Long Beach";

var caseAccountSearchResults = "//*[@id='ui-id-5']/span[@class='info'][contains(text(),'";
var caseAccountBeginning = "Texas";
var caseAccount = "Texas A&M University";
var caseCredentialsGlobalSettings = "//*[@id='globalPreferences']/section/h3[contains(text(), 'Auto')]/strong[contains(text(), 'Count Credentials')]";
var caseCredentialsAccountName1 = "//dd[@id='autoCountCustName' and contains(text(), '" + caseAccount1 + "')]";
var caseCredentialsAccountName2 = "//dd[@id='autoCountCustName' and contains(text(), '" + caseAccount2 + "')]";
var caseAccountSearch1 = "//li[@class='ui-menu-item']/a[@class='ui-corner-all' and contains(text(), '" + caseAccount1 + "')]";
var caseAccountSearch2 = "//li[@class='ui-menu-item']/a[@class='ui-corner-all' and contains(text(), '" + caseAccount2 + "')]";

var settingsTab = "//a[contains(@title, 'Settings')]";
var integrationsTab = "//a[contains(@title, 'Integrations')]";
var licensesTab = "//a[contains(@title, 'Licenses')]";
var locationsTab = "//a[contains(@title, 'Locations')]";
var locationDetailsTab = "//a[contains(@title, 'Location Details')]";
var locationsHeader = "//h2[contains(text(), 'Locations')]";
var mobileLicensesTab = "//a[contains(@title, 'Mobile Licenses')]";

var texasElement1 = "TAMU Lot 30";
var texasElement2 = "TAMU Lot 40";
var texasElement3 = "TAMU Lot 72";
var longBeachElement1 = "133 Long Beach";
var longBeachElement2 = "328 Pacific";

var autoCountIntegration = ".//*[@id='autoCountIntegration']/section/header/h2[contains(text(), 'Auto')]/strong[contains(text(), 'Count Credentials')]";
var autoCountLots = "//*[@id='formLotChildren']/h3[contains(text(), 'Lots')]";
var lotList = "//*[@id='locLotFullList']/li[contains(text(), '";

module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	 *@description Logs the system admin user into Iris and disables CASE for 'oranj'
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test05AccountRemainsAssignedAfterSubscriptionDisabled: Verify new Customer's CASE Subscription is disabled" : function (browser) {
		browser.changeCustomersCasePermissions(true, adminUsername, password, customer, customerName, false, false);
	},

	"test05AccountRemainsAssignedAfterSubscriptionDisabled: Go to Licenses > Integrations" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.waitForElementVisible(licensesTab, 4000)
		.click(licensesTab)

		.pause(2000)
		.waitForElementVisible(integrationsTab, 4000)
		.click(integrationsTab)

		.pause(2000)
	},

	/**
	 *@description Verifies that CASE account option is not present
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test05AccountRemainsAssignedAfterSubscriptionDisabled: Verify that the option to assign CASE Account is not available" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.assert.elementNotPresent(autoCountIntegration)
		.pause(1500);
	},

	/**
	 *@description Log out of System admin and log in to customer account
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test05AccountRemainsAssignedAfterSubscriptionDisabled: Log out of System admin and log in to Customer's account" : function (browser) {
		browser.logoutAndLogin(customerUsername, password);
	},

	/**
	 *@description Navigate to Location Details
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test05AccountRemainsAssignedAfterSubscriptionDisabled: Customer (CASE disabled) - Go to Settings > Locations > Location Details" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.waitForElementPresent(settingsTab, 2000)
		.click(settingsTab)

		.waitForElementPresent(locationsTab, 2000)
		.click(locationsTab)

		.waitForElementPresent(locationsTab, 2000)
		.click(locationsTab)

		.waitForElementPresent(locationDetailsTab, 1000)
		.click(locationDetailsTab)
		.pause(1000);
	},

	/**
	 *@description Verifies the AutoCount option doesn't show up for location edit
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test05AccountRemainsAssignedAfterSubscriptionDisabled: Verify that Location Edit form doesn't show the option to edit AutoCount properties" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementPresent("//a[@class='menuListButton menu']", 4000)
		.click("//a[@class='menuListButton menu']")
		.pause(1000)
		.waitForElementPresent("//a[@class='edit btnEditLocation']", 4000)
		.click("//a[@class='edit btnEditLocation']")
		.pause(2000)
		.execute('scrollTo(0,3000)')
		.pause(1000)
		.assert.hidden(autoCountLots)
		.pause(1000)

	},

	/**
	 *@description Logs the user out
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test05AccountRemainsAssignedAfterSubscriptionDisabled: Log out of the new Customer account" : function (browser) {
		browser.logout();
	}

};
