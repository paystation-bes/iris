/**
* @summary Customer Admin: Dashboard: Verify that AutoCount Widgets are not available for Child Account, when AutoCount subscription is disabled
* @since 7.4.1
* @version 1.0
* @author Lonney McD
* @see EMS-9546
* @requires test02AccountAssignedToChild, test17AddCaseOnlyLocation, test19WidgetAddSave
* @required by: test25WidgetListWithSubscription
**/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("AdminUserName");
var sysadminname = data("SysAdminUserName");
var password = data("Password");
var pass = data("Password");
var defaultPass= data("DefaultPassword");


module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	* @description Logs the system admin user into Iris
	* @param browser - The web browser object
	* @returns void
	**/
	"test24WidgetListWithoutSubscription: Login System Admin" : function (browser) 
	{
	//requires that initial login be done in SysAdmin account before script is run
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(sysadminname, password)
		.assert.title('Digital Iris - System Administration');
	},
	
	/**
	* @description Searches for the Child customer
	* @param browser - The web browser object
	* @returns void
	**/	
	"test24WidgetListWithoutSubscription: Search for and select Child Customer" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//input[@id='search']", 2000)
		.setValue("//input[@id='search']", 'oranj')
		.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'oranj')]", 2000)
		.click("//a[@class='ui-corner-all'][contains(text(),'oranj')]")
		.click("//a[@id='btnGo']")
		.pause(1000);
	},
	
	/**
	* @description Opens the edit customer pop up
	* @param browser - The web browser object
	* @returns void
	**/
	"test24WidgetListWithoutSubscription: Click Edit Customer 1" : function (browser) 
	{
		browser
		.useXpath()
		.click("//a[@id='btnEditCustomer']")
		.pause(2000);
	},
	
	/**
	* @description Checks that AutoCount is disabled
	* @param browser - The web browser object
	* @returns void
	**/
	"test24WidgetListWithoutSubscription: Set AutoCount Disabled" : function (browser) {
		browser
		.useXpath()
		browser.elements("xpath", "//label[contains(.,'AutoCount Integration')]/a[@class='checkBox checked']", function (result) 
		{if (result.value.length > 0) 
			{console.log("clicking")
			browser.click("//a[@id='subscriptionList:1800']")
			}
		});
	},

	/**
	* @description Click the Update Customer button
	* @param browser - The web browser object
	* @returns void
	**/	
	"test24WidgetListWithoutSubscription: Click Update Customer" : function (browser) {
		browser
		.useXpath()
		.click("//span[@class='ui-button-text'][contains(text(),'Update Customer')]")
		.pause(3000);
	},
	
	/**
	* @description Verify AutoCount disabled
	* @param browser - The web browser object
	* @returns void
	**/	
	"test24WidgetListWithoutSubscription: Verify AutoCount disabled" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//section[@id='globalPreferences']/section/ul/li[@title=' AutoCount Integration is Available']", 2000)
		.pause(1000);
	},
	
	/**
	* @description Logs out of System Admin
	* @param browser - The web browser object
	* @returns void
	**/
	"test24WidgetListWithoutSubscription: Logout1" : function (browser) {
		browser.logout();
	},

	
	/**
	 *@description Logs into Child Account (Oranj)
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test24WidgetListWithoutSubscription: Login" : function (browser) 
	{
		browser	
			.url(devurl)
			.windowMaximize('current')
			.initialLogin(username, pass, defaultPass)
			.assert.title('Digital Iris - Main (Dashboard)');
	},		
			
	/**
	 *@description Switches Dash to edit mode
	 *@param browser - The web browser object
	 *@returns void
	 **/	
	"test24WidgetListWithoutSubscription: Switch Dash to edit mode 1" : function (browser) 
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@id='headerTools']/a[@title='Edit']", 1000, false)
			.click("//section[@id='headerTools']/a[@title='Edit']")
			.pause(2000)
			.assert.visible("//a[@title='Add Widget']")
			;
	},
	
	/**
	 *@description Click the Add Widget Button 1
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test24WidgetListWithoutSubscription: Click the Add Widget Button 1" : function (browser)
	{
		browser
			.useXpath()
			.waitForElementVisible("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']", 1000, false)
			.click("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']")
			.pause(3000)
			.assert.visible("//div[@aria-describedby='widgetList' and @role='dialog']")
			.assert.visible("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']/span[text()='Add widget']")
			.assert.visible("//button/span[text()='Cancel']")
			.pause(1000)
			;
	},
	
	/**
	 *@description Verify Counted Occupancy Section of Widget List is not present
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test24WidgetListWithoutSubscription: Verify Counted Occupancy Section of Widget List not present" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementNotPresent("//section[@class='widgetListContainer']//h3[text() = 'Counted Occupancy']", 2000)
		.pause(1000)
		;
	},	

	/**
	 *@description Verify Counted Occupancy Widget Names and Descriptions not present
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test24WidgetListWithoutSubscription: Verify Counted Occupancy Widget Names and Descriptions not present" : function (browser) 
	{
		browser
		.useXpath()
		.assert.elementNotPresent("//section[@class='widgetListContainer']//h3[text() = 'Counted Occupancy']")
		.assert.elementNotPresent("//section[@class='widgetListContainer']//li//header[text() = 'Counted Occupancy by Hour']")
		.assert.elementNotPresent("//section[@class='widgetListContainer']//li//article[text() = 'Counted occupancy for the last 24 hours grouped by hour']")
		.assert.elementNotPresent("//section[@class='widgetListContainer']//li//header[text() = 'Counted Occupancy by Location']")
		.assert.elementNotPresent("//section[@class='widgetListContainer']//li//article[text() = 'Counted occupancy for now grouped by location']")
		.pause(1000)
		;
	},		
	
	/**
	 *@description Verify Counted/Paid Occupancy Section of Widget List is not present
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test24WidgetListWithoutSubscription: Verify Counted/Paid Occupancy Section of Widget List not present" : function (browser) 
	{
		browser
		.useXpath()
		.waitForElementNotPresent("//section[@class='widgetListContainer']//h3[text() = 'Counted/Paid Occupancy']", 2000)
		.pause(1000)
		;
	},	

	/**
	 *@description Verify Counted/Paid Occupancy Widget Names and Descriptions not present
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test24WidgetListWithoutSubscription: Verify Counted/Paid Occupancy Widget Names and Descriptions not present" : function (browser) 
	{
		browser
		.useXpath()
		.assert.elementNotPresent("//section[@class='widgetListContainer']//h3[text() = 'Counted/Paid Occupancy']")
		.assert.elementNotPresent("//section[@class='widgetListContainer']//li//header[text() = 'Counted/Paid Occ. by Hour']")
		.assert.elementNotPresent("//section[@class='widgetListContainer']//li//article[text() = 'Counted/paid occupancy for the last 24 hours grouped by hour']")
		.assert.elementNotPresent("//section[@class='widgetListContainer']//li//header[text() = 'Counted/Paid Occ. by Location']")
		.assert.elementNotPresent("//section[@class='widgetListContainer']//li//article[text() = 'Counted/paid occupancy for now grouped by location']")
		.pause(1000)
		;
	},
	
	/**
	 *@description Logout from Parent Account
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test24WidgetListWithoutSubscription: Logout from Parent Account" : function (browser)
	
	{
		browser.logout();
	},
};