/**
 * @summary Integrate with Counts in the Cloud: Tests that customer view cannot assign AutoCount account
 * @since 7.4.1
 * @version 1.0
 * @author Nadine B
 * @see US898: TC1244 (EMS-6503)
 **/

var data = require('../../data.js');

var devurl = data("URL");

var adminUsername = data("SysAdminUserName");
var customerUsername = data("AdminUserName");
var customer = "oranj";
var customerName = customer;
var password = data("Password");

var caseCheckbox = "//*[@id='subscriptionList:1800' and contains(@class,'checkBox')]";
var caseCheckboxChecked = "//*[@id='subscriptionList:1800' and contains(@class,'checkBox') and contains(@class,'checked') ]";
var count = 0;

module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	 *@description Logs the system admin user into Iris and disables CASE for customer 'oranj'
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test06CustomerCannotAssignAccount: Sign into System admin account and disable CASE for Customer" : function (browser) {
		browser.changeCustomersCasePermissions(true, adminUsername, password, customer, customerName, false, false);
	},

	/**
	 *@description Logs out of System admin and log into customer account
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test06CustomerCannotAssignAccount: Log out of System Admin account and log into Customer account" : function (browser) {
		browser.logoutAndLogin(customerUsername, password);
	},

	/**
	 *@description Count number of 'Edit' buttons in Settings
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test06CustomerCannotAssignAccount: Go to Settings and count the number of 'Edit' buttons" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.waitForElementPresent("//a[@title='Settings']", 2000)
		.click("//a[@title='Settings']")

		.waitForElementPresent("//*[@id='globalPreferences']/header/h2", 4000)
		.pause(1000)

		//Count all the 'Edit' buttons
		.elements("xpath", "//*[contains(@class, 'linkButtonIcn edit')]", function (result) {
			count += result.value.length;
			console.log("*** (CASE Disabled) The number of 'Edit' buttons = " + result.value.length);
		});

	},

	/**
	 *@description Log out of customer account and log into System admin
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test06CustomerCannotAssignAccount: Log out of Customer account" : function (browser) {
		browser
		.pause(2000)
		.logoutAndLogin(adminUsername, password);
	},

	/**
	 *@description Enable CASE for customer 'oranj'
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test06CustomerCannotAssignAccount: Enable CASE for Customer" : function (browser) {
		browser.changeCustomersCasePermissions(false, adminUsername, password, customer, customerName, true, false);
	},

	/**
	 *@description Logs out of System admin and logs back into customer account
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test06CustomerCannotAssignAccount: Log out of System Admin account and log into Customer account (CASE enabled)" : function (browser) {
		browser.logoutAndLogin(customerUsername, password);
	},

	/**
	 *@description Navigates to Settings
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test06CustomerCannotAssignAccount: Go to Settings" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.waitForElementPresent("//a[@title='Settings']", 2000)
		.click("//a[@title='Settings']")

	},

	/**
	 *@description Verify AutoCount information cannot be edited (i.e. the number of 'Edit' buttons is the same)
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test06CustomerCannotAssignAccount: Observe Global Preferences and verify that AutoCount information cannot be edited" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.waitForElementPresent("//a[@title='Settings']", 2000)
		.click("//a[@title='Settings']")

		.waitForElementPresent("//*[@id='globalPreferences']/header/h2", 4000)
		.pause(1000)

		//AutoCount is there
		.assert.elementPresent("//*[@id='globalPreferences']/section/h3[contains(text(), 'Auto')]/strong[contains(text(), 'Count Credentials')]")

		//No 'Edit' button for AutoCount (Count all the 'Edit' buttons again and compare)
		.elements("xpath", "//*[contains(@class, 'linkButtonIcn edit')]", function (result) {
			browser.assert.equal(count, result.value.length);
			console.log("*** (CASE Enabled) The number of 'Edit' buttons = " + result.value.length);
		});

	},

	/**
	 *@description Logs the user out
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test06CustomerCannotAssignAccount: Log out" : function (browser) {
		browser.logout();
	}

};
