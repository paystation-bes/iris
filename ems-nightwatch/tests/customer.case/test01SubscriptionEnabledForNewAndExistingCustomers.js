/**
 * @summary Integrate with Counts in the Cloud: Test that AutoCount subscription can be enabled
 * @since 7.4.1
 * @version 1.0
 * @author Nadine B
 * @see US898: TC1232
 **/

var data = require('../../data.js');

var devurl = data("URL");

var adminUsername = data("SysAdminUserName");
var customerUsername = data("AdminUserName");
var customer = "oranj";
var customerName = "Oranj Parent";
var password = data("Password");

var randomNumber = Math.floor((Math.random() * 100) + 1);
var newCustomerName = "Customer" + randomNumber;
var newCustomerParent = "Oranj Parent";
var newCustomerTimeZone = "Canada/Pacific";
var newCustomerUsername = "customer" + randomNumber + "@email";
var newCustomer = "customer" + randomNumber;
var newCustomerPassword = "Password$2";

var autoCountOptionMenu = "//h2[contains(@class, 'autoCountHeading') and contains(text(), 'Auto')]/strong[contains(text(), 'Count')]";
var autoCountLots = "//*[@id='formLotChildren']/h3[contains(text(), 'Lots')]";

var settingsTab = "//a[contains(@title, 'Settings')]";
var integrationsTab = "//a[contains(@title, 'Integrations')]";
var licensesTab = "//a[contains(@title, 'Licenses')]";
var locationsTab = "//a[contains(@title, 'Locations')]";
var locationDetailsTab = "//a[contains(@title, 'Location Details')]";
var locationsHeader = "//h2[contains(text(), 'Locations')]";

module.exports = {
	tags : ['completetesttag', 'featuretesttag'],
	/**
	 *@description Logs the system admin user into Iris
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01SubscriptionEnabledForNewAndExistingCustomers: Enable Customer's CASE Subscription" : function (browser) {
		browser.changeCustomersCasePermissions(true, adminUsername, password, customer, customer, true, false);
	},

	/**
	 *@description Navigates to Integrations tab
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01SubscriptionEnabledForNewAndExistingCustomers: Go to Licenses > Integrations" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@title='Licenses']", 2000)
		.click("//a[@title='Licenses']")

		.pause(2000)
		.waitForElementVisible("//a[@title='Integrations']", 4000)
		.click("//a[@title='Integrations']");
	},

	/**
	 *@description Verifies CASE Account option
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01SubscriptionEnabledForNewAndExistingCustomers: Verify that the option to assign CASE Account is available" : function (browser) {
		browser
		.useXpath()
		.pause(3000)
		.assert.elementPresent(".//*[@id='autoCountIntegration']/section/header/h2[contains(text(), 'Auto')]/strong[contains(text(), 'Count Credentials')]")
		.pause(2000);
	},

	/**
	 *@description Logs out of system admin, logs into customer account
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01SubscriptionEnabledForNewAndExistingCustomers: Log out of System Admin account and log into Customer account" : function (browser) {
		browser.logoutAndLogin(customerUsername, password);
	},

	/**
	 *@description Navigates to Location Details
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01SubscriptionEnabledForNewAndExistingCustomers: Go to Settings > Locations > Location Details" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.waitForElementPresent(settingsTab, 4000)
		.click(settingsTab)

		.waitForElementPresent(locationsTab, 4000)
		.click(locationsTab)

		.waitForElementPresent(locationsTab, 4000)
		.click(locationsTab)

		.waitForElementPresent(locationDetailsTab, 4000)
		.click(locationDetailsTab)
		.pause(1000);
	},

	/**
	 *@description Verify CASE option is available
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01SubscriptionEnabledForNewAndExistingCustomers: Add a new location and verify that the CASE location section is available" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.waitForElementPresent("//*[@id='btnAddLocation']", 2000)
		.click("//*[@id='btnAddLocation']")

		.pause(1000)
		.execute('scrollTo(0,3000)')
		.assert.elementPresent(autoCountOptionMenu);

	},

	/**
	 *@description Logs out of customer account, logs into system admin
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01SubscriptionEnabledForNewAndExistingCustomers: Log out of Customer account and log into System admin account" : function (browser) {
		browser.logoutAndLogin(adminUsername, password);
	},

	/**
	 *@description Create new customer with CASE enabled
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01SubscriptionEnabledForNewAndExistingCustomers: Create a new customer with CASE AutoCount enabled" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//*[@id='btnAddNewCustomer']", 4000)
		.click("//*[@id='btnAddNewCustomer']")

		browser
		.pause(1000)
		.setValue("//*[@id='formCustomerName']", newCustomerName)
		.pause(500)
		.setValue("//*[@id='prntCustomerAC']", newCustomerParent)
		.pause(1000)
		.click("//label[contains(@class, 'radioLabel') and contains(text(), 'Enabled')]")
		.pause(500)
		.setValue("id('formUserName')", newCustomerUsername)
		.pause(500)
		.setValue("id('newPassword')", newCustomerPassword)
		.pause(500)
		.setValue("id('c_newPassword')", newCustomerPassword)

		//Get all subscriptions
		var count = 0;
		browser.elements("xpath", "//*[@for='subscriptionListHidden']/a", function (result) {

			count += result.value.length;

			//Extract all subscription types
			for (n = 0; n < count; n++) {
				browser
				.click("(//*[@for='subscriptionListHidden']/a)[" + (n + 1) + "]");
			}
		});

		browser
		.pause(1000)
		.click("//button/span[contains(text(), 'Add Customer')]")
		.pause(2000);

	},

	/**
	 *@description Log out of system admin, log in as customer
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01SubscriptionEnabledForNewAndExistingCustomers: Log out of System admin and log into the new Customer account + Set Password" : function (browser) {

		browser
		.logoutAndLogin(newCustomerUsername, newCustomerPassword)
		.pause(1000)
		.useXpath()
		.waitForElementVisible("//*[@id='newPassword']", 4000)
		.setValue("//*[@id='newPassword']", password)
		.pause(500)
		.setValue("//*[@id='c_newPassword']", password)
		.pause(500)
		.click("//*[@id='changePassword']")
		.pause(1000)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	/**
	 *@description Navigate to Location Details
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01SubscriptionEnabledForNewAndExistingCustomers: New Customer - Go to Settings > Locations > Location Details" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.waitForElementPresent(settingsTab, 4000)
		.click(settingsTab)

		.waitForElementPresent(locationsTab, 4000)
		.click(locationsTab)

		.waitForElementPresent(locationsTab, 4000)
		.click(locationsTab)

		.waitForElementPresent(locationDetailsTab, 4000)
		.click(locationDetailsTab)
		.pause(1000);
	},

	/**
	 *@description Add new location + verify CASE option
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01SubscriptionEnabledForNewAndExistingCustomers: New Customer - Add a new location and verify that the CASE location section is present" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.waitForElementPresent("//*[@id='btnAddLocation']", 2000)
		.click("//*[@id='btnAddLocation']")
		.pause(1000)
		.execute('scrollTo(0,3000)')
		.assert.elementPresent(autoCountOptionMenu);

	},

	/**
	 *@description Disabled customer's CASE subscription
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01SubscriptionEnabledForNewAndExistingCustomers: Disable new Customer's CASE Subscription" : function (browser) {
		browser
		.logoutAndLogin(adminUsername, password)
		.changeCustomersCasePermissions(false, adminUsername, password, newCustomer, newCustomerName, false, false);
	},

	/**
	 *@description Navigate to Credentials
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01SubscriptionEnabledForNewAndExistingCustomers: New Customer (CASE disabled): Go to Licenses > Credentials" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@title='Licenses']", 2000)
		.click("//a[@title='Licenses']")

		.pause(2000)
		.waitForElementVisible("//a[@title='Integrations']", 4000)
		.click("//a[@title='Integrations']");
	},

	/**
	 *@description Verifies CASE option is unavailable
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01SubscriptionEnabledForNewAndExistingCustomers: New Customer (CASE disabled): Verify that the option to assign CASE Account is not available" : function (browser) {
		browser
		.useXpath()
		.pause(2000)

		.assert.elementNotPresent(autoCountLots)
		.pause(1500);
	},

	/**
	 *@description Logs out of system admin, logs in as customer
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01SubscriptionEnabledForNewAndExistingCustomers: Log out of System admin and log into the new Customer account (CASE disabled)" : function (browser) {
		browser.logoutAndLogin(newCustomerUsername, password)
	},

	/**
	 *@description Navigates to Location Details
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01SubscriptionEnabledForNewAndExistingCustomers: New Customer (CASE disabled) - Go to Settings > Locations > Location Details" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.waitForElementPresent(settingsTab, 4000)
		.click(settingsTab)

		.waitForElementPresent(locationsTab, 4000)
		.click(locationsTab)

		.waitForElementPresent(locationsTab, 4000)
		.click(locationsTab)

		.waitForElementPresent(locationDetailsTab, 4000)
		.click(locationDetailsTab)
		.pause(1000);

	},

	/**
	 *@description Adds a new location and verifies CASE option is abscent
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01SubscriptionEnabledForNewAndExistingCustomers: New Customer (CASE disabled) - Add a new location and verify that the CASE location section is not present" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.waitForElementPresent("//*[@id='btnAddLocation']", 2000)
		.click("//*[@id='btnAddLocation']")
		.pause(1000);

		browser
		.pause(1000)
		.execute('scrollTo(0,3000)')

		.assert.hidden(autoCountOptionMenu);

	},

	/**
	 *@description Logs the user out
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test01SubscriptionEnabledForNewAndExistingCustomers: Log out of the new Customer account" : function (browser) {
		browser.logout();
	}

};
