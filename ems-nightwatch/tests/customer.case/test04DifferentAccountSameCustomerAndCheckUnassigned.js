/**
 * @summary Integrate with Counts in the Cloud: Tests that different AutoCount can be assigned to customer
 * @since 7.4.1
 * @version 1.0
 * @author Nadine B
 * @see US898: TC1242, EMS-7185
 **/

var data = require('../../data.js');

var devurl = data("URL");

var adminUsername = data("SysAdminUserName");
var customerUsername = data("AdminUserName");
var customer = "oranj";
var customerName = customer;
var password = data("Password");

var customerSearchResults = "//*[@id='ui-id-2']/span[@class='info'][contains(text(),'";

var autoCountIntegration = ".//*[@id='autoCountIntegration']/section/header/h2[contains(text(), 'Auto')]/strong[contains(text(), 'Count Credentials')]";
var caseAccount1 = "Texas";
var caseAccount2Beginning = "City of Long";
var caseAccount2 = "City of Long Beach";

var caseAccountSearchResults = "//*[@id='ui-id-5']/span[@class='info'][contains(text(),'";
var caseAccountBeginning = "Texas";
var caseAccount = "Texas A&M University";
var caseCredentialsGlobalSettings = "//*[@id='globalPreferences']/section/h3[contains(text(), 'Auto')]/strong[contains(text(), 'Count Credentials')]";
var caseCredentialsAccountName1 = "//dd[@id='autoCountCustName' and contains(text(), '" + caseAccount1 + "')]";
var caseCredentialsAccountName2 = "//dd[@id='autoCountCustName' and contains(text(), '" + caseAccount2 + "')]";
var caseAccountSearch1 = "//li[@class='ui-menu-item']/a[@class='ui-corner-all' and contains(text(), '" + caseAccount1 + "')]";
var caseAccountSearch2 = "//li[@class='ui-menu-item']/a[@class='ui-corner-all' and contains(text(), '" + caseAccount2 + "')]";

var settingsTab = "//a[contains(@title, 'Settings')]";
var integrationsTab = "//a[contains(@title, 'Integrations')]";
var licensesTab = "//a[contains(@title, 'Licenses')]";
var locationsTab = "//a[contains(@title, 'Locations')]";
var locationDetailsTab = "//a[contains(@title, 'Location Details')]";
var locationsHeader = "//h2[contains(text(), 'Locations')]";
var mobileLicensesTab = "//a[contains(@title, 'Mobile Licenses')]";

var texasElement1 = "TAMU Lot 30";
var texasElement2 = "TAMU Lot 40";
var texasElement3 = "TAMU Lot 72";
var longBeachElement1 = "133 Long Beach";
var longBeachElement2 = "328 Pacific";

var autoCountIntegration = ".//*[@id='autoCountIntegration']/section/header/h2[contains(text(), 'Auto')]/strong[contains(text(), 'Count Credentials')]";
var autoCountLots = "//*[@id='formLotChildren']/h3[contains(text(), 'Lots')]";
var lotList = "//*[@id='locLotFullList']/li[contains(text(), '";

module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	 *@description Logs the system admin user into Iris and searches for customer 'oranj'
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test04DifferentAccountSameCustomerAndCheckUnassigned: Search for customer 'oranj'" : function (browser) {
		browser
		.useXpath()
		.searchForCustomer(true, adminUsername, password, customer, customerName)
		.pause(1000);
	},

	/**
	 *@description Assigns 'Texas' CASE account for customer 'oranj'
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test04DifferentAccountSameCustomerAndCheckUnassigned: Assign a 'Texas' CASE account to the Customer" : function (browser) {

		browser
		.useXpath()
		.pause(2000)
		.waitForElementVisible(licensesTab, 4000)
		.click(licensesTab)

		.pause(2000)
		.waitForElementVisible(integrationsTab, 4000)
		.click(integrationsTab)

		.pause(2000)
		.click("//*[@id='btnEditAutoCountCredentials']")
		.pause(1000)
		.waitForElementVisible("//input[@id='autoCountSearch']", 4000)

		//Clear the previous value from the search box
		.getValue("xpath", "//input[@id='autoCountSearch']", function (result) {
			if (result.value.length > 0) {
				browser.clearValue("//input[@id='autoCountSearch']")
			}
		});

		browser
		.pause(2000)
		.setValue("//input[@id='autoCountSearch']", caseAccount1)
		.pause(1000)
		.waitForElementVisible(caseAccountSearch1, 4000)
		.click(caseAccountSearch1)
		.click("//*[@id='autoCountSaveBtn']")
		.pause(2000)

		.assert.elementPresent(caseCredentialsAccountName1)
		.pause(1000);

	},

	/**
	 *@description Verifies the correct CASE lots are present
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test04DifferentAccountSameCustomerAndCheckUnassigned: Verify CASE Locations for 'Texas'" : function (browser) {

		browser
		.useXpath()
		.pause(1000)
		.click(locationsTab)
		.pause(1000)

		.waitForElementPresent("//a[@class='menuListButton menu']", 4000)
		.click("//a[@class='menuListButton menu']")
		.pause(1000)
		.waitForElementPresent("//a[@class='edit btnEditLocation']", 4000)
		.click("//a[@class='edit btnEditLocation']")
		.pause(2000)
		.execute('scrollTo(0,3000)')
		.pause(1000)
		.assert.elementPresent(autoCountLots)
		.elements("xpath", "//ul[@id='locLotFullList']/li", function (result) {
			browser.assert.equal(result.value.length, 3);
		})
		browser
		.useXpath()
		.assert.elementPresent(lotList + texasElement1 + "')]")
		.assert.elementPresent(lotList + texasElement2 + "')]")
		.assert.elementPresent(lotList + texasElement3 + "')]")
		.execute('scrollTo(3000,0)')

	},

	/**
	 *@description Assigns 'City of Long Beach' account to 'oranj'
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test04DifferentAccountSameCustomerAndCheckUnassigned: Assign a different CASE account to the Customer" : function (browser) {

		browser
		.useXpath()
		.pause(2000)
		.waitForElementVisible(licensesTab, 4000)
		.click(licensesTab)

		.pause(2000)
		.waitForElementVisible(integrationsTab, 4000)
		.click(integrationsTab)

		.pause(2000)
		.click("//*[@id='btnEditAutoCountCredentials']")
		.pause(1000)
		.waitForElementVisible("//input[@id='autoCountSearch']", 4000)

		//Clear the previous value from the search box
		.getValue("xpath", "//input[@id='autoCountSearch']", function (result) {
			if (result.value.length > 0) {
				browser.clearValue("//input[@id='autoCountSearch']")
			}
		});

		browser
		.pause(2000)
		.setValue("//input[@id='autoCountSearch']", caseAccount2Beginning)
		.pause(1000)
		.waitForElementVisible(caseAccountSearch2, 4000)
		.click(caseAccountSearch2)
		.click("//*[@id='autoCountSaveBtn']")
		.pause(2000)

		.assert.elementPresent(caseCredentialsAccountName2)
		.pause(2000);

	},

	/**
	 *@description Logs the system admin user out of Iris and signs in as customer
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test04DifferentAccountSameCustomerAndCheckUnassigned: Log out of System admin and log in to Customer account" : function (browser) {
		browser.logoutAndLogin(customerUsername, password);
	},

	/**
	 *@description Navigates to Location Details
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test04DifferentAccountSameCustomerAndCheckUnassigned: Go to Settings > Locations > Location Details" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.waitForElementPresent(settingsTab, 2000)
		.click(settingsTab)
		.pause(1000)
		.waitForElementPresent(locationsTab, 2000)
		.click(locationsTab)
		.pause(1000)
		.waitForElementPresent(locationsTab, 2000)
		.click(locationsTab)
		.pause(1000)
		.waitForElementPresent(locationDetailsTab, 1000)
		.click(locationDetailsTab)
		.pause(1000);
	},

	/**
	 *@description Verifies the correct lots are present for 'City of Long Beach' account
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test04DifferentAccountSameCustomerAndCheckUnassigned: Verify that the CASE selections from the 'City of Long Beach' account are present in the options" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.click(locationsTab)
		.pause(1000)

		.waitForElementPresent("//a[@class='menuListButton menu']", 4000)
		.click("//a[@class='menuListButton menu']")
		.pause(1000)
		.waitForElementPresent("//a[@class='edit btnEditLocation']", 4000)
		.click("//a[@class='edit btnEditLocation']")
		.pause(2000)
		.execute('scrollTo(0,3000)')
		.pause(1000)
		.assert.elementPresent(autoCountLots)
		.elements("xpath", "//ul[@id='locLotFullList']/li", function (result) {
			browser.assert.equal(result.value.length, 2);
		})
		browser
		.useXpath()
		.assert.elementNotPresent(lotList + texasElement1 + "')]")
		.assert.elementNotPresent(lotList + texasElement2 + "')]")
		.assert.elementNotPresent(lotList + texasElement3 + "')]")

		.assert.elementPresent(lotList + longBeachElement1 + "')]")
		.assert.elementPresent(lotList + longBeachElement2 + "')]");

	},

	/**
	 *@description Verifies that 'Unassigned' location doesn't have AutoCount options
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test04DifferentAccountSameCustomerAndCheckUnassigned: Verify that the 'Unassigned' location has no options for AutoCount properties mapping" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.execute('scrollTo(0,0)')
		.waitForElementPresent("//li[contains(@class, 'Child') and contains(@title, 'Unassigned')]/a", 4000)
		.click("//li[contains(@class, 'Child') and contains(@title, 'Unassigned')]/a")
		.pause(2000, function() {
			browser.assert.hidden(autoCountLots);
		});
	},

	/**
	 *@description Logs the user out
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test04DifferentAccountSameCustomerAndCheckUnassigned: Log out" : function (browser) {
		browser.logout();
	}

};
