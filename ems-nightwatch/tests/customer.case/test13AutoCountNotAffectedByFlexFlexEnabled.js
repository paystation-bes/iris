/**
 * @summary System Admin: Customer Details: FLEX Subscription does not affect AutoCount subscription- Flex Enabled/Counts disabled
 * @since 7.4.1
 * @version 1.0
 * @author Lonney McD
 * @see US898: EMS-9085
 **/

var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var username2 = data("AdminUserName");
var password = data("Password");

module.exports = {
	tags: ['completetesttag', 'featuretesttag'],
	
	/**
	* @description Logs the system admin user into Iris
	* @param browser - The web browser object
	* @returns void
	**/
	"test13AutoCountNotAffectedByFlexFlexEnabled: Login System Admin 1" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - System Administration');
	},
	
	/**
	* @description Searches for the child customer
	* @param browser - The web browser object
	* @returns void
	**/
	"test13AutoCountNotAffectedByFlexFlexEnabled: Search and Select Customer 1" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//input[@id='search']", 2000)
		.setValue("//input[@id='search']", 'oranj')
		.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'oranj')]", 2000)
		.click("//a[@class='ui-corner-all'][contains(text(),'oranj')]")
		.click("//a[@id='btnGo']")
		.pause(1000);
	},

	/**
	* @description Opens the edit customer pop up
	* @param browser - The web browser object
	* @returns void
	**/
	"test13AutoCountNotAffectedByFlexFlexEnabled: Click Edit Customer 1" : function (browser) {
		browser
		.useXpath()
		.click("//a[@id='btnEditCustomer']");
	},

	/**
	* @description Checks that FLEX is enabled and AutoCount is disabled
	* @param browser - The web browser object
	* @returns void
	**/	
	"test13AutoCountNotAffectedByFlexFlexEnabled: Set Flex Enabled, and AutoCount Disabled" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//*[@id='parentFlag-edit']/label", 2000)
		browser.elements("xpath", "//label[contains(.,'FLEX Integration')]/a[@class='checkBox ']", function (result) {
			if (result.value.length > 0) {
				console.log("clicking")
				browser.click("//a[@id='subscriptionList:1700']")
			}
		});
		browser.elements("xpath", "//label[contains(.,'AutoCount Integration')]/a[@class='checkBox checked']", function (result) {
			if (result.value.length > 0) {
				console.log("clicking")
				browser.click("//a[@id='subscriptionList:1800']")
			}
		});
	},

	/**
	* @description Click the Update Customer button
	* @param browser - The web browser object
	* @returns void
	**/	
	"test13AutoCountNotAffectedByFlexFlexEnabled: Click Update Customer 1" : function (browser) {
		browser
		.useXpath()
		.click("//span[@class='ui-button-text'][contains(text(),'Update Customer')]")
		.pause(3000);
	},

	/**
	* @description Opens the edit customer pop up again
	* @param browser - The web browser object
	* @returns void
	**/
	"test13AutoCountNotAffectedByFlexFlexEnabled: Click Edit Customer 2" : function (browser) {
		browser
		.useXpath()
		.click("//a[@id='btnEditCustomer']");
	},

	/**
	* @description Verifies that FLEX is enabled and AutoCount is disabled
	* @param browser - The web browser object
	* @returns void
	**/
	"test13AutoCountNotAffectedByFlexFlexEnabled: Verify Flex and AutoCount Subscriptions" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//label[contains(.,'FLEX Integration')]/a[@id='subscriptionList:1700']", 2000)
		.assert.elementPresent("//label[contains(.,'FLEX Integration')]/a[@class='checkBox checked']")
		.assert.elementPresent("//label[contains(.,'AutoCount Integration')]/a[@class='checkBox ']");
	},
	
	/**
	* @description Clicks the update customer button
	* @param browser - The web browser object
	* @returns void
	**/
	"test13AutoCountNotAffectedByFlexFlexEnabled: Click Update Customer 2" : function (browser) {
		browser
		.useXpath()
		.click("//span[@class='ui-button-text'][contains(text(),'Update Customer')]")
		.pause(3000);
	},

	/**
	* @description Goes to the Licenses: Integrations tab
	* @param browser - The web browser object
	* @returns void
	**/	
	"test13AutoCountNotAffectedByFlexFlexEnabled: Navigate to Licenses> Integrations" : function (browser) {
		browser.navigateTo("Licenses");
		browser.navigateTo("Integrations");
		browser.pause(5000);
	},
	
	/**
	* @description Verifies that FLEX is visible on Integrations tab and AutoCount is not
	* @param browser - The web browser object
	* @returns void
	**/
	"test13AutoCountNotAffectedByFlexFlexEnabled: Verify Flex present and AutoCount not present on Integrations page" : function (browser) {
		browser
		.useXpath()
		.assert.elementPresent("//section[@id='flexIntegration']//h2[contains(text(),'Flex Server Credentials')]")
		.assert.elementNotPresent("//section[@id='autoCountIntegration']//h2[contains(text(),'Auto')]");
	},

	/**
	* @description Goes to the Locations tab
	* @param browser - The web browser object
	* @returns void
	**/	
	"test13AutoCountNotAffectedByFlexFlexEnabled: Navigate to Locations" : function (browser) {
		browser.navigateTo("Locations");
	},
	
	/**
	* @description Opens the add location form
	* @param browser - The web browser object
	* @returns void
	**/
	"test13AutoCountNotAffectedByFlexFlexEnabled: Add Location 1" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@id='btnAddLocation']/img", 4000)
		.click("//a[@id='btnAddLocation']/img")
		.pause(1000)
	},

	/**
	* @description Verifies that FLEX mappings are visible in the form and AutoCount is not
	* @param browser - The web browser object
	* @returns void
	**/	
	"test13AutoCountNotAffectedByFlexFlexEnabled: Verify Flex Facilities and Properties visible and AutoCount hidden on Add Location form 1" : function (browser) {
		browser
		.useCss()
		.assert.elementPresent("#locPropFullList")
		.assert.visible("#locPropFullList")
		.assert.visible("#locFacFullList")
		.assert.hidden("#locLotFullList");
	},
	
	/**
	* @description Logs out of System Admin
	* @param browser - The web browser object
	* @returns void
	**/
	"test13AutoCountNotAffectedByFlexFlexEnabled: Logout1" : function (browser) {
		browser.logout();
	},
	
	/**
	* @description Logs into a customer admin account
	* @param browser - The web browser object
	* @returns void
	**/
	"test13AutoCountNotAffectedByFlexFlexEnabled: Login Customer Admin 1" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username2, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	/**
	* @description Goes to the Settings: Locations tab
	* @param browser - The web browser object
	* @returns void
	**/
	"test13AutoCountNotAffectedByFlexFlexEnabled: Navigate to Settings> Locations" : function (browser) {
		browser.navigateTo("Settings");
		browser.navigateTo("Locations");
	},
	
	/**
	* @description Opens the add location form
	* @param browser - The web browser object
	* @returns void
	**/
	"test13AutoCountNotAffectedByFlexFlexEnabled: Add Location 2" : function (browser) {
		browser
		.useXpath()
		.waitForElementVisible("//a[@id='btnAddLocation']/img", 4000)
		.click("//a[@id='btnAddLocation']/img")
		.pause(1000)
	},
	
	/**
	* @description Verifies that FLEX mappings are visible in the form and AutoCount is not
	* @param browser - The web browser object
	* @returns void
	**/
	"test13AutoCountNotAffectedByFlexFlexEnabled: Verify Flex Facilities and Properties visible and AutoCount hidden on Add Location form 2" : function (browser) {
		browser
		.useCss()
		.assert.elementPresent("#locPropFullList")
		.assert.visible("#locPropFullList")
		.assert.visible("#locFacFullList")
		.assert.hidden("#locLotFullList");
	},
	
	/**
	* @description Logs out of the customer admin
	* @param browser - The web browser object
	* @returns void
	**/
	"test13AutoCountNotAffectedByFlexFlexEnabled: Logout2" : function (browser) {
		browser.logout();
	},
};
