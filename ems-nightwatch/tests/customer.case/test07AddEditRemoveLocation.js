/**
 * @summary Integrate with Counts in the Cloud: Tests for all functionality with AutoCount and locations
 * @since 7.4.1
 * @version 1.0
 * @author Nadine B
 * @see US898: TC1245, TC1246 & TC1249, EMS-7189
 **/

var data = require('../../data.js');

var devurl = data("URL");

var adminUsername = data("SysAdminUserName");
var customerUsername = data("AdminUserName");
var customer = "oranj";
var customerName = customer;
var password = data("Password");

var randomNumber = Math.floor((Math.random() * 100) + 1);
var formName = "//*[@id='formLocationName']";
var formCapacity = "//*[@id='formCapacity']";
var originalLocationName = "Location" + randomNumber;
var locName2 = "LocationWithOutCase" + randomNumber;
var operationModeButton = "//*[@id='formOperatingModeExpand']";
var locOperationMode = "//li[@class='ui-menu-item']/a[contains(text(), 'Multiple')]";
var locCapacity = 100;
var longBeachElement1 = "133 Long Beach";
var longBeachElement2 = "328 Pacific";

var autoCountSelection1 = "//ul[@id='locLotFullList']/li[contains(text(), '" + longBeachElement1 + "')]";
var autoCountSelection2 = "//ul[@id='locLotFullList']/li[contains(text(), '" + longBeachElement2 + "')]";

var editLocation = "//ul/li[contains(@class, 'Child') and contains(@title, '" + originalLocationName + "')]/a[contains(@class, 'menuListButton')]";
var optionButton = "//*[@id='opBtn_pageContent']";
var editButton = "//*[@id='menu_pageContent']/section/a[contains(@title, 'Edit')]";
var save = "//*[@id='locationForm']/section[@class='btnSet']/a[contains(@class, 'Save')]";

var autoCountIntegration = ".//*[@id='autoCountIntegration']/section/header/h2[contains(text(), 'Auto')]/strong[contains(text(), 'Count Credentials')]";
var caseAccount1 = "Texas";
var caseAccount2Beginning = "City of Long";
var caseAccount2 = "City of Long Beach";

var caseAccountSearchResults = "//*[@id='ui-id-5']/span[@class='info'][contains(text(),'";
var caseAccountBeginning = "Texas";
var caseAccount = "Texas A&M University";
var caseCredentialsGlobalSettings = "//*[@id='globalPreferences']/section/h3[contains(text(), 'Auto')]/strong[contains(text(), 'Count Credentials')]";
var caseCredentialsAccountName1 = "//dd[@id='autoCountCustName' and contains(text(), '" + caseAccount1 + "')]";
var caseCredentialsAccountName2 = "//dd[@id='autoCountCustName' and contains(text(), '" + caseAccount2 + "')]";
var caseAccountSearch1 = "//li[@class='ui-menu-item']/a[@class='ui-corner-all' and contains(text(), '" + caseAccount1 + "')]";
var caseAccountSearch2 = "//li[@class='ui-menu-item']/a[@class='ui-corner-all' and contains(text(), '" + caseAccount2 + "')]";

var settingsTab = "//a[contains(@title, 'Settings')]";
var integrationsTab = "//a[contains(@title, 'Integrations')]";
var licensesTab = "//a[contains(@title, 'Licenses')]";
var locationsTab = "//a[contains(@title, 'Locations')]";
var locationDetailsTab = "//a[contains(@title, 'Location Details')]";
var locationsHeader = "//h2[contains(text(), 'Locations')]";
var mobileLicensesTab = "//a[contains(@title, 'Mobile Licenses')]";

var autoCountOption = "//*[@id='locationForm']/h2[contains(text(), 'Auto')]/strong[contains(text(), 'Count')]";
var count = 0;
var autoCountLots = "//*[@id='formLotChildren']/h3[contains(text(), 'Lots')]";

var selectedLongBeachElement1 = "//*[@id='locLotSelectedList']/li[contains(text(), '" + longBeachElement1 + "')]";
var selectedLongBeachElement2 = "//*[@id='locLotSelectedList']/li[contains(text(), '" + longBeachElement2 + "')]";

var availableLongBeachElement1 = "//*[@id='locLotFullList']/li[contains(text(), '" + longBeachElement1 + "')]";
var availableLongBeachElement2 = "//*[@id='locLotFullList']/li[contains(text(), '" + longBeachElement2 + "')]";

var saveLocation = "//*[@id='locationForm']/section/a[contains(@class, 'save') and contains(text(), 'Save')]";

module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	 *@description Logs the system admin user into Iris, enables CASE for customer 'oranj' and assigns the account 'Texas' to them
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test07AddEditRemoveLocation: Enable CASE for customer 'oranj' and assign CASE account 'Texas'" : function (browser) {
		browser
		.changeCustomersCasePermissions(true, adminUsername, password, customer, customerName, true, false)
		.pause(1000);
	},

	/**
	 *@description Signs into customer account 'oranj'
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test07AddEditRemoveLocation: Sign into CASE Enabled Customer account" : function (browser) {
		browser.logoutAndLogin(customerUsername, password);
	},

	/**
	 *@description Navigate to Location Details
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test07AddEditRemoveLocation: (CASE Enabled) Go to Settings > Locations > Location Details" : function (browser) {
		browser
		.useXpath()
		.pause(2000)
		.waitForElementPresent(settingsTab, 4000)
		.click(settingsTab)

		.waitForElementPresent(locationsTab, 4000)
		.click(locationsTab)

		.waitForElementPresent(locationsTab, 4000)
		.click(locationsTab)

		.waitForElementPresent(locationDetailsTab, 4000)
		.click(locationDetailsTab)
		.pause(1000);
	},

	/**
	 *@description Add a new location - LocationWithOutCase
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test07AddEditRemoveLocation: (CASE Enabled) Add a new location - fill out the form" : function (browser) {
		browser
		.useXpath()
		.click("//*[@id='btnAddLocation']")

		.pause(2000)
		.waitForElementPresent(formName, 4000)
		.pause(1000)
		.setValue(formName, locName2)
		.pause(1000)
		.click(operationModeButton)
		.pause(1000)
		.click(locOperationMode)
		.waitForElementPresent(formCapacity, 4000)
		.pause(1000)
		.setValue(formCapacity, locCapacity)
		.pause(1000)
		.waitForElementPresent(autoCountLots, 4000)

		.pause(1000)
		.assert.hidden(selectedLongBeachElement1)
		.pause(1000)
		.assert.hidden(selectedLongBeachElement2)

		.pause(1000)
		.click(saveLocation)
		.pause(2000);

	},

	/**
	 *@description Verify location data is saved (no CASE locations)
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test07AddEditRemoveLocation: (CASE Enabled) Verify that AutoCount location data is saved" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), 'Unassigned')]")
		.pause(1000)
		.waitForElementPresent("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + locName2 + "')]", 4000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + locName2 + "')]")

		.pause(1000)
		.execute('scrollTo(0,3000)')
		.assert.hidden(autoCountLots)
		.execute('scrollTo(3000,0)');

	},

	/**
	 *@description Add another location (with CASE locations)
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test07AddEditRemoveLocation: (CASE Enabled) Add another new location - fill out the form" : function (browser) {
		browser
		.useXpath()
		.click("//*[@id='btnAddLocation']")

		.pause(2000)
		.waitForElementPresent(formName, 4000)
		.pause(1000)
		.setValue(formName, originalLocationName)
		.pause(1000)
		.click(operationModeButton)
		.pause(1000)
		.click(locOperationMode)
		.waitForElementPresent(formCapacity, 4000)
		.pause(1000)
		.setValue(formCapacity, locCapacity)
		.pause(1000)
		.waitForElementPresent(autoCountLots, 4000)

		.pause(1000)
		.assert.elementPresent(availableLongBeachElement1)
		.pause(1000)
		.assert.elementPresent(availableLongBeachElement2)

		.pause(1000)
		.click(availableLongBeachElement1)
		.pause(1000)
		.click(availableLongBeachElement2)
		.pause(1000)
		.assert.visible(selectedLongBeachElement1)
		.assert.visible(selectedLongBeachElement2)

		.click(saveLocation)
		.pause(2000);

	},

	/**
	 *@description Verify newly added location data (with CASE locations)
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test07AddEditRemoveLocation: Verify that newly added AutoCount location data is saved" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementPresent("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + originalLocationName + "')]", 4000)
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + originalLocationName + "')]")
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + originalLocationName + "')]")

		.assert.elementPresent(autoCountLots, 4000)

		.assert.elementPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]")
		.assert.elementPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement2 + "')]")

		.assert.visible("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]")
		.assert.visible("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement2 + "')]")

	},

	/**
	 *@description Clicks 'Edit' and removes '133 Long Beach'
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test07AddEditRemoveLocation: Click on 'Edit' button and remove '133 Long Beach' from AutoCount selections" : function (browser) {
		browser
		.useXpath()
		.pause(1000)

		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + locName2 + "')]")
		.pause(2000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + originalLocationName + "')]")
		.pause(2000)
		.click(optionButton)
		.pause(1000)
		.click(editButton)
		.pause(2000)
		.waitForElementPresent(selectedLongBeachElement1, 4000)
		.click(selectedLongBeachElement1)
		.pause(1000)

		.click(saveLocation)
	},

	/**
	 *@description Verifies '133 Long Beach' is removed
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test07AddEditRemoveLocation: Verify that AutoCount location removed does not appear" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementPresent("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + originalLocationName + "')]", 4000)
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + locName2 + "')]")
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + originalLocationName + "')]")
		.pause(1000)
		.assert.elementPresent(autoCountLots)
		.pause(1000)
		.assert.elementNotPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]")
	},

	/**
	 *@description Logs the user out
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test07AddEditRemoveLocation: Log out" : function (browser) {
		browser.logout();
	}
};
