/**
 * @summary Integrate with Counts in the Cloud: Tests for AutoCount accessibility in system admin view
 * @since 7.4.1
 * @version 1.0
 * @author Nadine B
 * @see US898: TC1254 (EMS-6602)
 **/

var data = require('../../data.js');

var devurl = data("URL");

var adminUsername = data("SysAdminUserName");
var customerUsername = data("AdminUserName");
var customer = "oranj";
var customerName = customer;
var password = data("Password");

var randomNumber = Math.floor((Math.random() * 100) + 1);
var formName = "//*[@id='formLocationName']";
var formCapacity = "//*[@id='formCapacity']";
var locationName = "test10Location" + randomNumber;
var newLocation = "test10NewLocation" + randomNumber;
var operationModeButton = "//*[@id='formOperatingModeExpand']";
var locOperationMode = "//li[@class='ui-menu-item']/a[contains(text(), 'Multiple')]";
var locCapacity = 100;
var longBeachElement1 = "133 Long Beach";
var longBeachElement2 = "328 Pacific";

var autoCountSelection1 = "//ul[@id='locLotFullList']/li[contains(text(), '" + longBeachElement1 + "')]";
var autoCountSelection2 = "//ul[@id='locLotFullList']/li[contains(text(), '" + longBeachElement2 + "')]";

var editLocation = "//ul/li[contains(@class, 'Child') and contains(@title, '" + locationName + "')]/a[contains(@class, 'menuListButton')]";
var optionButton = "//*[@id='opBtn_pageContent']";
var editButton = "//*[@id='menu_pageContent']/section/a[contains(@title, 'Edit')]";
var save = "//*[@id='locationForm']/section[@class='btnSet']/a[contains(@class, 'Save')]";

var autoCountIntegration = ".//*[@id='autoCountIntegration']/section/header/h2[contains(text(), 'Auto')]/strong[contains(text(), 'Count Credentials')]";
var caseAccount1 = "Texas";
var caseAccount2Beginning = "City of Long";
var caseAccount2 = "City of Long Beach";

var caseAccountSearchResults = "//*[@id='ui-id-5']/span[@class='info'][contains(text(),'";
var caseAccountBeginning = "Texas";
var caseAccount = "Texas A&M University";
var caseCredentialsGlobalSettings = "//*[@id='globalPreferences']/section/h3[contains(text(), 'Auto')]/strong[contains(text(), 'Count Credentials')]";
var caseCredentialsAccountName1 = "//dd[@id='autoCountCustName' and contains(text(), '" + caseAccount1 + "')]";
var caseCredentialsAccountName2 = "//dd[@id='autoCountCustName' and contains(text(), '" + caseAccount2 + "')]";
var caseAccountSearch1 = "//li[@class='ui-menu-item']/a[@class='ui-corner-all' and contains(text(), '" + caseAccount1 + "')]";
var caseAccountSearch2 = "//li[@class='ui-menu-item']/a[@class='ui-corner-all' and contains(text(), '" + caseAccount2 + "')]";

var settingsTab = "//a[contains(@title, 'Settings')]";
var integrationsTab = "//a[contains(@title, 'Integrations')]";
var licensesTab = "//a[contains(@title, 'Licenses')]";
var locationsTab = "//a[contains(@title, 'Locations')]";
var locationDetailsTab = "//a[contains(@title, 'Location Details')]";
var locationsHeader = "//h2[contains(text(), 'Locations')]";
var mobileLicensesTab = "//a[contains(@title, 'Mobile Licenses')]";

var autoCountOption = "//*[@id='locationForm']/h2[contains(text(), 'Auto')]/strong[contains(text(), 'Count')]";
var count = 0;
var autoCountLots = "//*[@id='formLotChildren']/h3[contains(text(), 'Lots')]";

var selectedLongBeachElement1 = "//*[@id='locLotSelectedList']/li[contains(text(), '" + longBeachElement1 + "')]";
var selectedLongBeachElement2 = "//*[@id='locLotSelectedList']/li[contains(text(), '" + longBeachElement2 + "')]";

var availableLongBeachElement1 = "//*[@id='locLotFullList']/li[contains(text(), '" + longBeachElement1 + "')]";
var availableLongBeachElement2 = "//*[@id='locLotFullList']/li[contains(text(), '" + longBeachElement2 + "')]";

var payStation = "500000070001";
var payStationCheckBox = "//ul[contains(@id, 'locPSFullList')]/li[contains(text(), '" + payStation + "')]/a[contains(@class, 'checkBox')]";
var selectedPayStation = "//ul[contains(@id, 'locPSSelectedList')]/li[contains(text(), '500000070001') and contains(@class, 'selected')]";

var saveLocation = "//*[@id='locationForm']/section/a[contains(@class, 'save') and contains(text(), 'Save')]";

module.exports = {
	tags : ['completetesttag', 'featuretesttag'],

	/**
	 *@description Logs the system admin user into Iris and enables customer's CASE subscription
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10ChildCustomerAccessibility: Enable Customer's CASE Subscription" : function (browser) {
		browser.changeCustomersCasePermissions(true, adminUsername, password, customer, customerName, true, false);
	},

	/**
	 *@description Navigates to Location Details
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10ChildCustomerAccessibility: Go to Locations > Location Details" : function (browser) {
		browser
		.useXpath()
		.pause(2000)

		.waitForElementPresent(locationsTab, 4000)
		.click(locationsTab)

		.waitForElementPresent(locationsTab, 4000)
		.click(locationsTab)

		.pause(1000);
	},

	/**
	 *@description Adds a new location - no AutoCount lots selected
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10ChildCustomerAccessibility: Add a new location but do not select any AutoCount options " : function (browser) {
		browser
		.useXpath()
		.click("//*[@id='btnAddLocation']")

		.pause(2000)
		.waitForElementPresent(formName, 4000)
		.pause(1000)
		.setValue(formName, locationName)
		.pause(1000)
		.click(operationModeButton)
		.pause(1000)
		.click(locOperationMode)
		.waitForElementPresent(formCapacity, 4000)
		.pause(1000)
		.setValue(formCapacity, locCapacity)
		.pause(1000)

		.click(payStationCheckBox)
		.pause(500)
		.assert.elementPresent(selectedPayStation)

		.waitForElementPresent(autoCountLots, 4000)

		.pause(1000)
		.assert.elementPresent(availableLongBeachElement1)
		.pause(1000)
		.assert.elementPresent(availableLongBeachElement2)

		.pause(1000)
		.click(saveLocation)
		.pause(1000);

	},

	/**
	 *@description Verifies that AutoCount lot data is not present
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10ChildCustomerAccessibility: Verify that AutoCount location data is not present" : function (browser) {

		browser
		.useXpath()
		.pause(1000)
		.waitForElementPresent("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + locationName + "')]", 4000)
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + locationName + "')]")
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + locationName + "')]")
		.pause(1000)

		.assert.elementPresent("//*[@id='psChildList']/li/a[contains(text(), '" + payStation + "')]")
		.assert.elementNotPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]")
		.assert.elementNotPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement2 + "')]");

	},

	/**
	 *@description Edits the location - select one AutoCount lot
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10ChildCustomerAccessibility: Click on 'Edit' button and make AutoCount selections" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementPresent("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + locationName + "')]", 4000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + locationName + "')]")
		.pause(1000)
		.execute('scrollTo(3000, 0)')
		.pause(1000)
		.click(optionButton)
		.pause(1000)
		.click(editButton)
		.pause(2000)
		.waitForElementPresent(autoCountLots, 4000)
		.pause(1000)
		.assert.elementPresent(availableLongBeachElement1)
		.click(availableLongBeachElement1)
		.pause(1000)
		.click(saveLocation);

	},

	/**
	 *@description Verifies that the AutoCount location data is present
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10ChildCustomerAccessibility: Verify that AutoCount location data is present" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementPresent("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + locationName + "')]", 4000)
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + "Unassigned" + "')]")
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + locationName + "')]")
		.pause(2000)
		.assert.elementPresent("//*[@id='psChildList']/li/a[contains(text(), '" + payStation + "')]")
		.pause(1000)
		.assert.elementPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]")
		.pause(1000)
		.assert.visible("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]");

	},

	/**
	 *@description Edits the location to make a second AutoCount lot selection
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10ChildCustomerAccessibility: Click on 'Edit' button again and make more AutoCount selections" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.execute('scrollTo(3000, 0)')
		.pause(1000)
		.click(optionButton)
		.pause(1000)
		.click(editButton)
		.pause(2000)
		.waitForElementPresent(autoCountLots, 4000)

		.assert.elementPresent(availableLongBeachElement2)
		.click(availableLongBeachElement2)
		.pause(1000)
		.click(saveLocation);

	},

	/**
	 *@description Verifies that both AutoCount lot selections are present
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10ChildCustomerAccessibility: Verify that the two AutoCount location data is present" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementPresent("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + locationName + "')]", 4000)
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + "Unassigned" + "')]")
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + locationName + "')]")
		.pause(1000)
		.assert.elementPresent("//*[@id='psChildList']/li/a[contains(text(), '" + payStation + "')]")
		.assert.elementPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]")
		.assert.elementPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement2 + "')]")

		.assert.visible("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]")
		.assert.visible("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement2 + "')]")

	},

	/**
	 *@description Edits the location - removes one AutoCount selection
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10ChildCustomerAccessibility: Click on 'Edit' button again and remove one AutoCount selection" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.click(optionButton)
		.pause(1000)
		.click(editButton)
		.pause(1000)
		.waitForElementPresent(autoCountLots, 4000)

		.assert.elementPresent(selectedLongBeachElement2)
		.click(selectedLongBeachElement2)
		.pause(1000)
		.click(saveLocation);

	},

	/**
	 *@description Verifies that one AutoCount lot was removed
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10ChildCustomerAccessibility: Verify that the AutoCount lot was successfully removed" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementPresent("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + locationName + "')]", 4000)
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + "Unassigned" + "')]")
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + locationName + "')]")
		.pause(1000)
		.assert.elementPresent("//*[@id='psChildList']/li/a[contains(text(), '" + payStation + "')]")
		.assert.elementPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]")
		.assert.elementNotPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement2 + "')]")

		.assert.visible("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]");

	},

	/**
	 *@description Adds a new lcoation with the same AutoCount lot as the one that's still assigned to the original location
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10ChildCustomerAccessibility: Add a new location with same AutoCount options " : function (browser) {
		browser
		.useXpath()
		.click("//*[@id='btnAddLocation']")

		.pause(2000)
		.waitForElementPresent(formName, 4000)
		.pause(1000)
		.setValue(formName, newLocation)
		.pause(1000)
		.click(operationModeButton)
		.pause(1000)
		.click(locOperationMode)
		.waitForElementPresent(formCapacity, 4000)
		.pause(1000)
		.setValue(formCapacity, locCapacity)
		.pause(1000)
		.waitForElementPresent(autoCountLots, 4000)

		.pause(1000)
		.assert.elementPresent(availableLongBeachElement1)

		.pause(1000)
		.click(availableLongBeachElement1)

		.pause(1000)
		.assert.visible(selectedLongBeachElement1)

		.click(saveLocation)
		.pause(2000);

	},

	/**
	 *@description Verify that the same one element is added to the new location successfully
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10ChildCustomerAccessibility: Verify that new locations's AutoCount data is present" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementPresent("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + newLocation + "')]", 4000)
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + newLocation + "')]")
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + newLocation + "')]")

		.assert.elementPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]")

		.assert.visible("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]");

	},

	/**
	 *@description Verify that the old location doesn't contain any of the lot data anymore
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10ChildCustomerAccessibility: Verifies that the old locations's AutoCount data is not there anymore" : function (browser) {
		browser
		.useXpath()
		.pause(1000)
		.waitForElementPresent("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + locationName + "')]", 4000)
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + locationName + "')]")
		.pause(1000)
		.click("//li[contains(@class, 'Child')]/section[@class='locName' and contains(text(), '" + locationName + "')]")

		.assert.elementPresent("//*[@id='psChildList']/li/a[contains(text(), '" + payStation + "')]")
		.assert.elementNotPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement1 + "')]")
		.assert.elementNotPresent("//*[@id='lotChildList']/li[contains(text(), '" + longBeachElement2 + "')]")

		.pause(1000);
	},

	/**
	 *@description Logs the user out
	 *@param browser - The web browser object
	 *@returns void
	 **/
	"test10ChildCustomerAccessibility: log out" : function (browser) {
		browser.logout();
	}

};
