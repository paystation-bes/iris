var data = require('../../data.js');

var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");
GLOBAL.widgetId = "";
GLOBAL.sectionId = "";
var payStationNum;

module.exports = {
	tags: ['smoketag', 'completetesttag'],

	"Login to System Admin" : function (browser) 
	{
		console.log("User: " + username + "/" + password);
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},

	"Click Add new Customer" : function (browser)
	{
		browser
		.useCss()
		.assert.visible("#btnAddNewCustomer")
		.pause(1000)
		.click("#btnAddNewCustomer")

		.useXpath()
		.pause(3000)
		.assert.visible("//span[contains(text(),'New Customer')]")
	},

	"Select new time zone" : function (browser)
	{
		browser
		.useCss()
		.assert.visible("#prntCustomerACExpand")
		.pause(1000)
		.click("#prntCustomerACExpand")
		.pause(3000)

		.useXpath()
		.assert.visible("//ul/li/a[contains(text(),'bulbasaur')]")
		.pause(1000)
		.click("//ul/li/a[contains(text(),'bulbasaur')]")
	}



};