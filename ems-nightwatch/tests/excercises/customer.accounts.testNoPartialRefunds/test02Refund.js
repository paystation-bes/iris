/**
 * @summary Asserts that user cannot edit Amount to Refund
 * @Author Billy Hoang
 **/

var data = require('../../data.js');

// Login Info
var devurl = data("URL");
var username = data("AdminUserName");
var password = data("Password");

// Testing Info
var cardNumber = data("CardNumber");
var slicedCardNumber = "XXXXXXXXXXXX" + cardNumber.slice(12,16);
var cardExpiry = data("CardExpiry");
var refundAmount;

// SearchDate can be one of:
// Today, Yesterday, Last 24 Hours, This Week, Last Week, Last 7 Days, This Month, Last Month, Last 30 Days
var searchDate = "Today";



module.exports = {
	tags: ['smoketag', 'completetesttag'],
	
	
		"Login" : function (browser) 
	{
		console.log("User: " + username + "/" + password);
		browser	
			.url(devurl)
			.windowMaximize('current')
			.initialLogin(username, password, "password")
			.assert.title('Digital Iris - Main (Dashboard)');
	},
	
	
	
		"Navigate to Card Refunds" : function (browser) 
	{
		browser
			.click("img[alt='Card Management']")
			.pause(1000)
			.assert.title("Digital Iris - Card Management : Banned Cards")
			.click(".tab[title='Card Refunds']")
			.pause(1000)
			.assert.title("Digital Iris - Card Management : Card Refunds");
	},
	
	
	
		"Input required information to Refund" : function (browser)
		
	{
			browser
			.waitForElementVisible("#searchCardNumber", 1000)
			.setValue("#searchCardNumber", cardNumber)
			
			.assert.visible("#searchCardExpiry")
			.setValue("#searchCardExpiry", cardExpiry)

			.assert.visible("#searchTxDateTypeExpand")
			.click("#searchTxDateTypeExpand")
			
			.useXpath()
			.assert.visible("//a[contains(text(),'" + searchDate + "')]")
			.click("//a[contains(text(),'" + searchDate + "')]");
	},
	
	
	
		"Click the Search button" : function(browser)
	{	
			browser
			.useXpath()
			.assert.visible("//a[@class='linkButtonFtr search right']")
			.click("//a[@class='linkButtonFtr search right']")
			.pause(3000);
	},
	
	
		"Look for the first transaction listed, and get the amount to be refunded" : function(browser)
	{
			browser
			.useXpath()
			.assert.visible("(//div[@class='col1']/p[contains(text(),slicedCardNumber)])[1]");
	},
			
			
		"Click the first Refund button for the first transaction" : function(browser)
	{	
			browser
			.useXpath()
			.assert.visible("(//a[@class='menuListButton refund txMenu'])[1]")
			.click("//li/a")
			.pause(2000);
	},
	
	
			"Get Refund Amount" : function(browser)
	{	
			browser
			.useCss()
			.getText("section.clear > span", function(result) {
                refundAmount = result.value;
				console.log("Amount to be refunded: " + refundAmount);
            });
			
	},
	
 		"Assert that Amount to Refund is not editable" : function (browser)
	{
	
			browser	
			.useXPath()
			.assert.elementNotPresent("//input[@id='formRefundAmount']")
			
			.useCss()
			.assert.elementPresent(".clear>span")
			.assert.containsText(".clear>span", refundAmount) 
	}, 


		"Logout" : function (browser) 
	{
		browser.logout();
	},  
	
};	