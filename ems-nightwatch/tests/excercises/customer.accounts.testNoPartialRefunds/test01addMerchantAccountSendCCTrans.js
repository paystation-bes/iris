/**
 * @summary
 * Add merchant account (Paymentech) to customer oranj, and assign
 * it to the first pay station, also make a $4.00 CC transaction to that Pay Station
 * @Author Billy Hoang
 **/
 
 var data = require('../../data.js');

// Login Info
var devurl = data("URL");
var username = data("SysAdminUserName");
var password = data("Password");

// Testing Info
var PSNumber;
//var cardNumber = data("CardNumber");
var cardNumber = "4275330012345675";

module.exports = {
	
	
//----------------- ADD MERCHANT ACCOUNT TO ORANJ -----------------//


		"Login to System Admin" : function (browser) 
	{
		console.log("User: " + username + "/" + password);
		browser	
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.assert.title('Digital Iris - System Administration');
	},
	
	"Search for oranj (Oranj Parent)" : function (browser) 
	{
		browser
			.useCss()
			.waitForElementVisible("#search", 2000)
			.setValue("#search", "oranj")
			.pause(2000)
			.assert.visible("#btnGo")
			.click("#btnGo")

	},
	
		"Click on Add Merchant Account Button" : function (browser) 
	{
		browser
			.useCss()
			.waitForElementVisible("#btnAddMrchntAccnt", 4000)
			.click("#btnAddMrchntAccnt")
	},
	
		"Fill out required information for Merchant Account and submit" : function (browser) 
	{
			browser
			.useCss()
			.waitForElementVisible("#account", 2000)
			.setValue("#account", "Paymentech")
			.assert.visible("#statusCheck")
			.click("#statusCheck")
			.assert.visible("#processorsExpand")
			.click("#processorsExpand")
			.pause(1000)
			
			.useXpath()
			.assert.visible("//a[contains(text(),'Paymentech')]")
			.click("//a[contains(text(),'Paymentech')]")
			.pause(2000)
			
			.useCss()
			.waitForElementVisible("#field1", 2000)
			.setValue("#field1", "700000002018")
			.assert.visible("#field2")
			.setValue("#field2", "001")
			.assert.visible("#field3")
			.setValue("#field3", "digitech1")
			.assert.visible("#field4")
			.setValue("#field4", "digpaytec01")
			.assert.visible("#field5")
			.setValue("#field5", "0002")		
			
			// CLICK THE ADD ACCOUNT BUTTON:
			
			// DOES NOT WORK
			/* .assert.visible(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus")
			.click(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only.active.ui-state-focus") */
			
			// DOES NOT WORK
			/* .Xpath()
			.assert.visible("//span[contains(text(), 'Add Account')]/..")
			.click("//span[contains(text(), 'Add Account')]/..")
			.pause(3000) */
			
			// NEED A BETTER PATH
			.useXpath()
			.assert.visible("(//button[@type='button'])[2]")
			.click("(//button[@type='button'])[2]")
			.pause(4000)
			
	},
	
	
//----------------- ASSIGN MERCHANT ACCOUNT TO FIRST PAY STATION -----------------//
	
		"Click on the Pay Stations tab" : function (browser) 
	{
			browser
			.useCss()
			.assert.visible(".tab[title='Pay Stations']")
			.click(".tab[title='Pay Stations']")
			.pause(2000)
			.assert.title("Digital Iris - System Administration - oranj : Pay Station Details")
	},
	
	
		// 	NOT SURE IF WORKING YET (USE HARD CODED PSNUMBER FOR NOW
		
/* 			"Get the first Pay Station number" : function(browser)
	{	
			browser
			.useCss()
			.getText("(//ul[@id='paystationList']//span[@class='textLine'])[1]", function(result) {
                PSNumber = result.value;
				console.log("Using Pay Station #: " + PSNumber);
            });
			
	}, */
	
		"Click on the edit button of the first pay station" : function (browser) 
	{
			browser
			.useXpath()
			.assert.visible("(//ul[@id='paystationList']//a[@class='menuListButton edit'])[1]")
			.click("(//ul[@id='paystationList']//a[@class='menuListButton edit'])[1]")
			.pause(2000)
			.useCss()
			.assert.visible("#editHeading")
	},
	
		"Add Merchant Account to Pay Station" : function (browser)
	{
			browser
			.useCss()
			.assert.visible("#formCreditCardAccountExpand")
			.click("#formCreditCardAccountExpand")
			
			.useXpath()
			.assert.visible("//a[contains(text(),'Paymentech')]")
			.click("//a[contains(text(),'Paymentech')]")
			
			.useCss()
			.assert.visible(".linkButtonFtr.textButton.save")
			.click(".linkButtonFtr.textButton.save")
			.pause(4000)
	},
	
		"Navigate to the Pay Station's information and check merchant account" : function (browser)	
	{
			browser
			.useCss()
			.assert.visible(".tab[title='Information']", 2000)
			.click(".tab[title='Information']")
			.pause(2000)
			.useXpath()
			.assert.visible("//section[@id='psCCMerchantAccountLine']/dd[contains(text(),'Paymentech')]")
	},
	
	
//----------------- SEND CC TRANSACTION TO PAY STATION -----------------//


		"SendCCTrans" : function (browser) 
	{
		
		 browser.sendCCPreAuth("4.00", CardNumber, "500000080001", function (result) {
			var CCTrans = {
				licenceNum : '496BCW',
				stallNum : '1',
				type : 'Regular',
				originalAmount : '400',
				chargedAmount: '4.00',
				cardPaid : '4.00',
				cardAuthorizationId: result.authID,
				emsPreAuthId: result.emsPreAuthID,
			}
   
			browser.sendCCPostAuth([CCTrans], "500000080001");
		});
		
	},
	
	
//----------------- END -----------------//


	"Logout" : function (browser) 
	{
		browser.logout();
	},  
	
	
};
 
 
 
 
 
 
 
 