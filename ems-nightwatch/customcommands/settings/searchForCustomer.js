/**
 * @summary Generate MEID for 18 digit CCID of a CDMA modem types
 * @since 7.3.6
 * @version 1.0
 * @author Nadine B
 * @see US-955
 **/

exports.command = function(newSession, adminUsername, password, customer, customerName, callback) {
	
	var data = require('../../data.js');

	var devurl = data("URL");
	
    var client = this;
    var customerSearchResults = "//li[@class='ui-menu-item']/a[@class='ui-corner-all' and contains(text(), '";
    var flexCheckbox = "//*[@id='subscriptionList:1700']";
    var flexCheckboxChecked = "//*[@id='subscriptionList:1700' and contains(@class, 'checked')]";
    var updateCustomer = "//div[@class='ui-dialog-buttonset']/button/span[@class='ui-button-text'][contains(text(), 'Update Customer')]";
    
    
    //Log in as System Admin

    if(newSession){
	client
	.url(devurl)
	.windowMaximize('current')
	.pause(2000)
	.login(adminUsername, password)

    }
    client
    .useXpath()
	.pause(1000)
	.assert.title('Digital Iris - System Administration')

	//Searches up the Customer Admin

//	client
	
	.waitForElementVisible("//*[@id='search']", 2000)
	.click("//*[@id='search']")
	.setValue("//*[@id='search']", customer)
	.waitForElementVisible("//*[@id='ui-id-2']/span[@class='info']", 2000)
	.waitForElementVisible(customerSearchResults + customerName + "')]", 4500)
	.click(customerSearchResults + customerName + "')]")
	.click("//*[@id='btnGo']")

	
	return client;
};