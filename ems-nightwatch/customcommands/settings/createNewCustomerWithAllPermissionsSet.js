/**
 * @summary Creates a customer with specified configurations
 * @since 7.3.4
 * @version 1.0
 * @author Nadine B
 * @see US-898
 **/

//Sample calls: 
//browser.createNewCustomerWithPermissions("Customer", false, "Oranj Parent", "Canada/Pacific", "Enabled", "customer@email", "Password$2", ["Alerts", "Coupons", "FLEX Integration"]);
//browser.createNewCustomerWithPermissions("Customer", true, "", "Canada/Pacific", "Enabled", "customer@email", "Password$2", ["ALL"]);

exports.command = function(customerName, isParent, parentName, timeZone, username, password, callback) {
	
	var data = require('../../data.js');

	var devurl = data("URL");
	
    var client = this;
    client
    .useXpath()
	.waitForElementVisible("id('btnAddNewCustomer')", 2000)
	.click("id('btnAddNewCustomer')")
	
	if(isParent == true){
		client
		.useXpath()
		.waitForElementVisible("id('parentFlagCheck')", 2000)
		.click("id('parentFlagCheck')")
	}else{
		client
		.useXpath()
		.click("//*[@id='prntCustomerAC']")
		.setValue("//*[@id='prntCustomerAC']", parentName)
	}
	
    client
	.setValue("id('formCustomerName')", customerName)
	.click("id('accountStatus:enabled')")
	.setValue("id('formUserName')", username)
	.setValue("id('newPassword')", password)
	.setValue("id('c_newPassword')", password)
	
	//Get all subscriptions
	var count = 0;
//	var containsAutoCount = false;
	client.elements("//*[@for='subscriptionListHidden']", function(result){
		
//		count += result.value.length;
//		console.log("The number of elements is: " + count);
		
		//Extract all subscription types
		for(n = 0; n < count; n++){
			client.click("//*[@for='subscriptionListHidden']/a");
			
//			.getText("//*[@for='subscriptionListHidden']/a", function(subscriptions) {
//				   if (subscriptions.value.indexOf("AutoCount") > -1){
//					   containsAutoCount = true;
//					   console.log("AutoCount = " + containsAutoCount);
//				   }
//				
//				 });
		}
		
		
	});

	client
	.pause(1000)
//	.assert.elementPresent(caseCheckbox)
//	.assert.elementNotPresent(caseCheckboxChecked)
	.click("//button/span[contains(text(), 'Add Customer')]")
	.pause(1000)
	
    return client;
    
};