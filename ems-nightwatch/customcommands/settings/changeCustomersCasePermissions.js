/**
 * @summary Integrate with Counts in the Cloud
 * @since 7.3.4
 * @version 1.0
 * @author Nadine B
 * @see US-898: TC1232
 **/

exports.command = function(newSession, adminUsername, password, customer, customerName, caseOn, logoutAfter, callback) {
	
	var data = require('../../data.js');

	var devurl = data("URL");
    var client = this;
    var customerSearchResults = "//a[@class='ui-corner-all' and contains(text(), '" + customerName + "')]";
    var caseCheckbox = "//*[@id='subscriptionList:1800' and contains(@class,'checkBox')]";
    var caseCheckboxChecked = "//*[@id='subscriptionList:1800' and contains(@class,'checkBox') and contains(@class,'checked') ]";
    var updateCustomer = "//div[@class='ui-dialog-buttonset']/button/span[@class='ui-button-text'][contains(text(), 'Update Customer')]";
    
    
    //Log in as System Admin

    if(newSession){
	
    	client
    	.url(devurl)
    	.windowMaximize('current')
    	.login(adminUsername, password)
    	.assert.title('Digital Iris - System Administration');
  
    	
    	  }
//    client
//    	.login(adminUsername, password)
//    	.assert.title('Digital Iris - System Administration');
//  

	//Searches up the Customer Admin

	client
	.useXpath()
	.waitForElementVisible("//*[@id='search']", 2000)
	.click("//*[@id='search']")
	.setValue("//*[@id='search']", customer)
	.waitForElementVisible("//*[@id='ui-id-2']/span[@class='info']", 2000)
	.waitForElementVisible(customerSearchResults, 4500)
	.click(customerSearchResults)
	.click("//*[@id='btnGo']")

	
	//Edit Customer's CASE permissions depending on intent - caseOn
	
	client
	.useXpath()
	.pause(1000)
	.waitForElementVisible("//*[@id='btnEditCustomer']", 2000)
	.click("//*[@id='btnEditCustomer']")
		

	.pause(1000)
	.waitForElementVisible(caseCheckbox, 4000)

	//Enable CASE
	
	if(caseOn == true){
		client.elements('xpath', caseCheckboxChecked, function(result){
		console.log(result.value)
		
			if(result.value.length > 0) {
				client
				.click(caseCheckbox)
				.pause(1000)
				client.click(caseCheckbox)
				.pause(1000)
			}else{
				client
				.click(caseCheckbox)
				.pause(1000)
			}
		})
		
		client
		.useXpath()
		.pause(2000)
		.assert.elementPresent(caseCheckboxChecked)
		
	}else{
		
		//Disable CASE
		
		client.elements('xpath', caseCheckboxChecked, function(result){
			console.log(result.value)
			if(result.value.length > 0) {
				client.click(caseCheckbox)
				.pause(1000)
			}
		})
		
		client
		.useXpath()
		.pause(2000)
		.assert.elementPresent(caseCheckbox)
		.assert.elementNotPresent(caseCheckboxChecked)
			
			
	}	

	
	//Save Customer changes

	client
	.useXpath()
	.pause(1000)
	.click(updateCustomer)
	.pause(2000)

	
	//Log out
	if (logoutAfter == true) {
		client
		.waitForElementVisible("//a[@title='Logout']", 3000)
		.click("//a[@title='Logout']")
		.pause(1000)
		return client;
		
	}else{
		return client;
	}
	

};