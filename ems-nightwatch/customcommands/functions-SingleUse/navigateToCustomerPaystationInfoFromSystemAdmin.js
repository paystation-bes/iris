/**
 * @summary Generate MEID for 18 digit CCID of a CDMA modem types
 * @since 7.3.6
 * @version 1.0
 * @author Nadine B
 * @see US-955
 **/

exports.command = function(adminUsername, password, customer, customerName, modemType, callback) {
	var client = this;
	var data = require('../../data.js');

	var devurl = data("URL");
	var paystationCommAddress = data("PaystationCommAddress");
	

		/**
		*@description Find customer
		*@param browser - The web browser object
		*@returns void
		**/
		console.log("navigateToCustomerPaystationInfoFromSystemAdmin: " + modemType);
			client
			.pause(2000)
			
			.searchForCustomer(true, adminUsername, password, customer, customer);
		
		
		/**
		*@description Navigate to Pay Station Details
		*@param browser - The web browser object
		*@returns void
		**/

			client
			.useXpath()
			.pause(2000)
			
			.waitForElementVisible("//*[@id='secondNavArea']/nav/div/a[@title='Pay Stations']", 10000)
			.pause(1000)
			.click("//*[@id='secondNavArea']/nav/div/a[@title='Pay Stations']")
			.pause(1000);
		
		
		/**
		*@description Assert the paystation is there and click on it
		*@param browser - The web browser object
		*@returns void
		**/
	
			client
			.pause(1000)
			.useXpath()
			.waitForElementVisible("//section/span[@class='container']/span[contains(text(), '" + paystationCommAddress + "')]", 4000)
			
			.click("//section/span[@class='container']/span[contains(text(), '" + paystationCommAddress + "')]")
			.pause(1000);

		
		/**
		*@description Navigates to Information panel
		*@param browser - The web browser object
		*@returns void
		**/
		
			client
			.pause(1000)
			.useXpath()
			.waitForElementVisible("//*[@id='infoBtn']/a", 4000)
			
			.click("//*[@id='infoBtn']/a")
			.pause(1000);
		
		return client;
};