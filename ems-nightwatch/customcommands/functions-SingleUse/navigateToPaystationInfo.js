/**
 * @summary Generate MEID for 18 digit CCID of a CDMA modem types
 * @since 7.3.6
 * @version 1.0
 * @author Nadine B
 * @see US-955
 **/

exports.command = function(username, password, modemType, callback) {
	var client = this;
	var data = require('../../data.js');

	var devurl = data("URL");
	var paystationCommAddress = data("PaystationCommAddress");
	
		/**
		*@description Logs the user into Iris
		*@param browser - The web browser object
		*@returns void
		**/
		console.log("navigateToPaystationInfo: " + modemType) // +  " - Log in as customer")
			client
			.url(devurl)
			.windowMaximize('current')
			.login(username, password)
			.pause(3000)
			.assert.title("Digital Iris - Main (Dashboard)");

		/**
		*@description Navigate to Settings menu
		*@param browser - The web browser object
		*@returns void
		**/
	
			client
			.pause(2000)
			.useXpath()
			.waitForElementVisible("//section[@class='bottomSection']/a[@title='Settings']", 4000)

			.click("//section[@class='bottomSection']/a[@title='Settings']")
			.pause(1000);
		
		/**
		*@description Navigate to Pay Stations tab
		*@param browser - The web browser object
		*@returns void
		**/
		
			client
			.pause(1000)
			.useXpath()
			.waitForElementVisible("//*[@id='secondNavArea']/nav/div/a[@title='Pay Stations']", 10000)
			.pause(1000)
			.waitForElementVisible("//*[@id='flexTestButtonResult']/section[contains(@class, 'success')]", 10000)
			.pause(1000)
			.click("//*[@id='secondNavArea']/nav/div/a[@title='Pay Stations']")
			.pause(1000);
		
		/**
		*@description Assert the paystation is there and click on it
		*@param browser - The web browser object
		*@returns void
		**/
	
			client
			.pause(1000)
			.useXpath()
			.waitForElementVisible("//section/span[@class='container' and contains(text(), '" + paystationCommAddress + "')]", 4000)
			
			.click("//section/span[@class='container' and contains(text(), '" + paystationCommAddress + "')]")
			.pause(1000);	
		
		/**
		*@description Navigates to Information panel
		*@param browser - The web browser object
		*@returns void
		**/
	
			client
			.pause(1000)
			.useXpath()
			.waitForElementVisible("//*[@id='infoBtn']/a", 4000)
			
			.click("//*[@id='infoBtn']/a")
			.pause(1000);

		
		return client;
		
};