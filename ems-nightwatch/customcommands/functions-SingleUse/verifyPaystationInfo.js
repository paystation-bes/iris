/**
 * @summary Generate MEID for 18 digit CCID of a CDMA modem types
 * @since 7.3.6
 * @version 1.0
 * @author Nadine B
 * @see US-955
 **/

exports.command = function(modemType, ccid, callback) {
	var client = this;
	var data = require('../../data.js');

	var devurl = data("URL");
	var paystationCommAddress = data("PaystationCommAddress");
	
	console.log("verifyPaystationInfo: " + modemType);

	if(ccid.length != 18){
		
		client
		.useXpath()
		.pause(1000)
		.assert.elementNotPresent("//section/dt[contains(text(), 'MEID')]")
		.assert.elementPresent("//section/dt[contains(text(), 'CCID')]")
		.getText("//section/dt[contains(text(), 'CCID')]/../dd[@class='detailValue']", function(result){
			client.assert.equal(result.value, ccid)
		})
		.assert.elementPresent("//section/dt[contains(text(), 'Modem Type')]")
		.assert.elementPresent("//section/dt[contains(text(), 'Carrier')]")
		
	}else{
		
		if(modemType == 'CDMA'){	
			
			client
			.useXpath()
			.pause(1000)
		.getText("//section/dt[contains(text(), 'Carrier')]/../dd[@class='detailValue']", function(result){
			
			client.assert.equal(result.value, 'TELUS')
		})
		.getText("//section/dt[contains(text(), 'Modem Type')]/../dd[@class='detailValue']", function(result){
			
			client.assert.equal(result.value, modemType)
		})
			
		.generateMEID(ccid);
		
		}else if(modemType == 'Ethernet' || modemType == 'WiFi'){
			client
			.useXpath()
			.pause(1000)
			.getText("//section/dt[contains(text(), 'Modem Type')]/../dd[@class='detailValue']", function(result){
			
				client.assert.equal(result.value, modemType)
	
			})
			client
			.assert.elementNotPresent("//section/dt[contains(text(), 'MEID')]")
			.pause(2000);
			
		}else if(modemType == 'GSM' || modemType == 'CellularModem'){
			client
			.useXpath()
			.pause(1000)
			.getText("//section/dt[contains(text(), 'Modem Type')]/../dd[@class='detailValue']", function(result){
				
				client.assert.equal(result.value, modemType)
	
			})
			.getText("//section/dt[contains(text(), 'CCID')]/../dd[@class='detailValue']", function(result){
				
				client.assert.equal(result.value, ccid)
	
			})
			.getText("//section/dt[contains(text(), 'Carrier')]/../dd[@class='detailValue']", function(result){
				
				client.assert.equal(result.value, 'TELUS')
	
			})
			client
			.assert.elementNotPresent("//section/dt[contains(text(), 'MEID')]")
			
		}else{ 
			client
			.useXpath()
			.pause(1000)
			.assert.elementNotPresent("//section/dt[contains(text(), 'MEID')]")
			.assert.elementPresent("//section/dt[contains(text(), 'CCID')]")
			.assert.elementPresent("//section/dt[contains(text(), 'Modem Type')]")
			.assert.elementPresent("//section/dt[contains(text(), 'Carrier')]")
		}
	}
};