/**
 * @summary Generate MEID for 18 digit CCID of a CDMA modem types
 * @since 7.3.6
 * @version 1.0
 * @author Nadine B
 * @see US-955
 **/

exports.command = function(ccid, callback) {
	var client = this;
	var data = require('../../data.js');
	
	var newCcid = ccid.toString();
	var first10Digits = newCcid.substring(0, 10);
	var last8Digits = newCcid.substring(10);
	
	client.assert.equal(first10Digits.length, 10)
	client.assert.equal(last8Digits.length, 8)


	var firstPart = parseInt(first10Digits)
	
	var first10Hex = firstPart.toString(16);

	var lastPart = parseInt(last8Digits)
	var last8Hex = lastPart.toString(16);

	var firstPart = parseInt(first10Digits)
	
	console.log("")
	
	console.log("first10Hex = " + first10Hex)
	console.log("last8Hex = " + last8Hex)
	
	
	var meid = first10Hex + last8Hex
	console.log("MEID = " + meid)
	
	
	client
	.pause(1000)
	.useXpath()
	.waitForElementVisible("//section/dt[@class='detailLabel' and contains(text(), 'MEID')]", 4000)
	
	
	.getText("//section/dt[contains(text(), 'MEID')]/../dd[@class='detailValue']", function(result){

		client.assert.equal(result.value, meid.toUpperCase())
	
	})
	
	client.pause(1000);
			
	return client;
};