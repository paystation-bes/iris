/**
 * @summary Gets Public Key and encrypts card data - uses encryptCard.jar
 * @since 7.3.6
 * @version 1.1
 * @author Thomas C, Mandy F
 **/
var data = require('../../data.js');
var request;
var exec = require('child_process').exec, child;

try {
    request = require('../../node_modules/request');
} catch (error) {
    request = require('request');
}
var protocol = 'https://';
var devurl = data("URL");
var requestURL = devurl + "/download?type=publicKey&commaddress=BOSS";
var serverStartIndex = devurl.indexOf(protocol) + protocol.length;
var serverEndIndex = devurl.indexOf('.digitalpaytech');
var server = ' ' + devurl.substring(serverStartIndex, serverEndIndex) + ' ';
var portStartIndex = devurl.lastIndexOf(':') + ':'.length;
var port = ' ' + devurl.substring(portStartIndex) + ' ';
var encryptedCard = '';

exports.command = function(cardData, callback) {
    var self = this;
    var target = 'java -jar encryptCard.jar' + server + port + ' ' + cardData;
    client = this;
    var exec = require('child_process').exec,
        child;
    child = exec(target,
        function(error, stdout, stderr) {
            console.log(stdout);
			if (callback !== null) {
				callback(stdout.trim());
			}
			
			if (error !== null) {
                console.log('exec error: ' + error);
            }
			
        });
    this.pause(2000);


    return this;

};