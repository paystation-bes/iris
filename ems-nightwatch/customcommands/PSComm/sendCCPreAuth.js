/**
 * @summary Sends the CC PreAuth to Iris and asserts the response
 * @since 7.4.1
 * @version 1.1
 * @author Thomas C, Mandy F
 * @param chargedAmount - amount to be charged
 * @param cardData - credit card number NOT ENCRYPTED
 * @param serialNum - PS serial number
 * @param callback - optional callback function to return authorization IDs for Post-Auth
 * @returns client
 **/

var data = require('../../data.js');
var id;
var request;
try {
	request = require('../../node_modules/request');
} catch (error) {
	request = require('request');
}
var exec = require('child_process').exec, child;

var paystationCommAddress = data("PaystationCommAddress");
var devurl = data("URL");

var protocol = 'https://';
var serverStartIndex = devurl.indexOf(protocol) + protocol.length;
var serverEndIndex = devurl.indexOf('.digitalpaytech');
var server = ' ' + devurl.substring(serverStartIndex, serverEndIndex) + ' ';
var portStartIndex = devurl.lastIndexOf(':') + ':'.length;
var port = ' ' + devurl.substring(portStartIndex) + ' ';
var encryptedCard = '';

var authIDResponse = '';
var emsPreAuthIDResponse = '';

exports.command = function (chargedAmount, cardData, serialNum, callback) {
	client = this;
	if (serialNum != null) {
		paystationCommAddress = serialNum;
	}
	var statusCode;
	var date = new Date();
	//Gen random messageNum <= 1000
	var messageNum = Math.floor(Math.random() * 100) + 1;
	//converts date to GMT
	var gmtDate = new Date(date.valueOf() + date.getTimezoneOffset() * 60000);
	var expiryDate = new Date(date.valueOf() + 60000 * 30 + date.getTimezoneOffset() * 60000);
	var gmtDateString = gmtDate.getFullYear() + ":" + ('0' + (gmtDate.getMonth() + 1)).slice(-2) + ":" + ('0' + gmtDate.getDate()).slice(-2) + ':' + ('0' + gmtDate.getHours()).slice(-2) + ":" + ('0' + gmtDate.getMinutes()).slice(-2) + ":" + ('0' + gmtDate.getSeconds()).slice(-2) + ":GMT";
	var gmtExpDateString = expiryDate.getFullYear() + ":" + ('0' + (expiryDate.getMonth() + 1)).slice(-2) + ":" + ('0' + expiryDate.getDate()).slice(-2) + ':' + ('0' + expiryDate.getHours()).slice(-2) + ":" + ('0' + expiryDate.getMinutes()).slice(-2) + ":" + ('0' + expiryDate.getSeconds()).slice(-2) + ":GMT";
	var body = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><methodCall><methodName>XMLRPCRequestHandler.processXML</methodName><params><param><value><?xml version=\"1.0\"?><Paystation_2 Password=\"zaq123$ESZxsw234%RDX\" UserId=\"emsadmin\" Version=\"0.0.1\"><Message4EMS>";
	var footer = "</Message4EMS></Paystation_2></value></param></params></methodCall>";
	var target = 'java -jar encryptCard.jar' + server + port + ' ' + cardData;
	client = this;
	console.log(target)
	var exec = require('child_process').exec,
	child;
	child = exec(target,

			function (error, stdout, stderr) {

			if (error !== null) {
				console.log('exec error: ' + error);
			}

			body += '<AuthorizeCardRequest MessageNumber="' + messageNum + '">';
			body += '<PaystationCommAddress>' + paystationCommAddress + '</PaystationCommAddress>';
			body += '<CardData>' + stdout.trim() + '</CardData>';
			body += '<ChargeAmount>' + chargedAmount + '</ChargeAmount>';
			body += '<RefId>10</RefId>';
			body += '</AuthorizeCardRequest>';
			body += footer;

			var postRequest = {
				uri : devurl + '/XMLRPC_PS2',
				method : "POST",
				agent : false,
				timeout : 10000,
				headers : {
					'content-type' : 'text/xml',
					'content-length' : Buffer.byteLength(body, [''])
				}
			};

			console.log(body);

			var req = request.post(postRequest, function (err, httpResponse, body) {
					if(err){
						throw err;
					}
					client.assert.equal(httpResponse.statusCode, 200, "Status Code Correct");
					var response = body.replace("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><methodResponse><params><param><value><base64>", "");
					response = response.replace("</base64></value></param></params></methodResponse>", "");
					result = new Buffer(response, 'base64').toString('ascii');
					console.log(result);					
					client.assert.equal(result.indexOf("Authorized") > -1, true, "Message accepted");
					startAuth = result.indexOf('<AuthorizationId>') + '<AuthorizationId>'.length;
					endAuth = result.indexOf('</AuthorizationId>');
					startEMSAuth = result.indexOf('<EmsPreAuthId>') + '<EmsPreAuthId>'.length;
					endEMSAuth = result.indexOf('</EmsPreAuthId>');
					authIDResponse = result.substring(startAuth, endAuth);
					emsPreAuthIDResponse = result.substring(startEMSAuth, endEMSAuth);
					
					console.log("authID: " + authIDResponse);
					console.log("emsPreAuthID: " + emsPreAuthIDResponse);
				
					var preAuthResponse = {
						authID: authIDResponse,
						emsPreAuthID: emsPreAuthIDResponse,
					}
					
					if (callback != null) {
						callback(preAuthResponse);
					}		
				});

			client.pause(5000);
			req.write(body);
			req.end();

		});

	client.pause(5000);
	return client;
};
