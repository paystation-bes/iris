/**
 * @summary Sends the event to Iris and asserts the response
 * @since 7.4.3
 * @version 1.0
 * @author Nadine B
 * @param payStation = the name of the pay station as it appears in Iris
 * @returns client
 **/

var request;
try {
	request = require('../../node_modules/request');
} catch (error) {
	request = require('request');
}

var payStationList = ".//*[@id='paystationList']";
var payStationElement = payStationList + "/span/li/section/span[contains(text(), '";
var payStationDetails = ".//*[@id='detailHeading' and contains(text(), 'Pay Station Details')]";
var recentActivity = ".//*[@id='alertsBtn']/a[contains(text(), 'Recent Activity')]";
//var allActiveRecentAlerts = "//ul[@id='crntAlertList']/span/li/section[contains(@class, 'activeAlert')]/div[contains(@class, 'col1')]/p";
var allActiveRecentAlerts = "//ul[@id='crntAlertList']//div[contains(@class, 'col4')]/p[contains(., 'Active')]";
var activeAlertBegin = "//ul[@id='crntAlertList']/span/li[";
var activeAlertEnd = "]/section/div[contains(@class, 'col1')]/p";

exports.command = function (payStation) {
	client = this;
	
	client.getAttribute(payStationElement + payStation + "')]/..", "class", function(result) {
		console.log("*** Processing " + payStation);
		var pos = payStation;
		
		if (result.value != 'severityNull'){
			//if an alert exists for the pay station
			client
			.useXpath()
			.pause(1000)
			.click(payStationElement + pos + "')]")
			.waitForElementPresent(payStationDetails, 4000)
			.waitForElementPresent(recentActivity, 4000)
			.click(recentActivity)
			
			.pause(1000)
			.waitForElementPresent(allActiveRecentAlerts, 4000)
			//grab all alerts for this pay station
			.elements("xpath", allActiveRecentAlerts, function(alerts){
				console.log("The number of active alerts is: " + alerts.value.length);
				
				//extract alert information
				console.log("Alerts: " + alerts.value);
				
				var j = 0;
				for(j = 1; j <= alerts.value.length; j++) {
					client
					.pause(1000)
					client.getText(activeAlertBegin + j + activeAlertEnd, function(alert) {
						console.log(" ACTIVE: " + alert.value);
						
						client.clearAllActiveAlerts(payStation, alert.value);
	
					});
					
				}
				
			});
			
		}
		
	  });
	
	return client;
};