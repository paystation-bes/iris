  /**
 * @summary Sends a Collection Event to a Pay Station
 * @since 7.3.6
 * @version 1.0
 * @author Thomas C, Billy H
 * @param amounts - array of amounts to be collected (ie. [1,2,3,4,5]), paystationCommAddress - Serial number of the Pay Station you want to send to
 * @returns client
 **/
var data = require('../../data.js');

var request;
try {
  request = require('../../node_modules/request');
} catch (error) {
  request = require('request');
}
var devurl = data("URL");

exports.command = function (paystationCommAddress) {
  var client = this;
  var statusCode;
  var date = new Date();
  //Gen random messageNum <= 1000
  var messageNum = Math.floor(Math.random() * 100) + 1;
  //converts date to GMT
  var gmtDate = new Date(date.valueOf() + date.getTimezoneOffset() * 60000);
  var gmtDateString = gmtDate.getFullYear() + ":" + ('0' + (gmtDate.getMonth() + 1)).slice(-2) + ":" + ('0' + gmtDate.getDate()).slice(-2) + ':' + ('0' + gmtDate.getHours()).slice(-2) + ":" + ('0' + gmtDate.getMinutes()).slice(-2) + ":" + ('0' + gmtDate.getSeconds()).slice(-2) + ":GMT";
  //var gmtDateString2 = gmtDate.getFullYear() + ":" + ('0' + (gmtDate.getMonth() + 1)).slice(-2) + ":" + ('0' + gmtDate.getDate()).slice(-2) + ':' + ('0' + gmtDate.getHours()).slice(-2) + ":" + ('0' + gmtDate.getMinutes()).slice(-2) + ":" + ('0' + gmtDate.getSeconds()).slice(-2) + ":GMT";

  var body = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><methodCall><methodName>XMLRPCRequestHandler.processXML</methodName><params><param><value><?xml version=\"1.0\"?><Paystation_2 Password=\"zaq123$ESZxsw234%RDX\" UserId=\"emsadmin\" Version=\"6.3 build 0013\"><Message4EMS>";
  var footer = "</Message4EMS></Paystation_2></value></param></params></methodCall>";



    body += "<Audit MessageNumber=\"69\"> <PaystationCommAddress>" + paystationCommAddress + "</PaystationCommAddress> <ReportNumber>69</ReportNumber> <LotNumber>SOAP Parking</LotNumber> <MachineNumber>7</MachineNumber> <StartDate>" + gmtDateString + "</StartDate> <EndDate>" + gmtDateString + "</EndDate> <PatrollerTicketsSold>1</PatrollerTicketsSold> <TicketsSold>6</TicketsSold> <CoinAmount>10.00</CoinAmount> <OnesAmount>3.00</OnesAmount> <TwoesAmount>2.00</TwoesAmount> <FivesAmount>15.00</FivesAmount> <TensAmount>30.00</TensAmount> <TwentiesAmount>20.00</TwentiesAmount> <FiftiesAmount>100.00</FiftiesAmount> <SmartCardRechargeAmount>0</SmartCardRechargeAmount> <CitationsPaid>0</CitationsPaid> <CitationAmount>0</CitationAmount> <ChangeIssued>2</ChangeIssued> <RefundIssued>0.00</RefundIssued> <ExcessPayment>0.00</ExcessPayment> <NextTicketNumber>33</NextTicketNumber> <AttendantTicketsSold>0</AttendantTicketsSold> <AttentantTicketsAmount>0</AttentantTicketsAmount> <AttendantDepositAmount>0</AttendantDepositAmount> <AcceptedFloatAmount>0</AcceptedFloatAmount> <CurrentTubeStatus>0_0:0_0:0_0:0_0</CurrentTubeStatus> <OverfillAmount>0</OverfillAmount> <ReplenishedAmount>0</ReplenishedAmount> <HopperInfo>25:100_75:100_7175:40800_0:0</HopperInfo> <TestDispensed>0:0:0</TestDispensed> <CardAmounts> <CardType>Other</CardType> <CardAmount>2.75</CardAmount> <CardType>Visa</CardType> <CardAmount>2.25</CardAmount> <CardType>Amex</CardType> <CardAmount>1.23</CardAmount> </CardAmounts> </Audit>";

    body += footer;

      var postRequest = {
    uri : devurl + '/XMLRPC_PS2',
    method : "POST",
    agent : false,
    timeout : 10000,
    headers : {
      'content-type' : 'text/xml',
      'content-length' : Buffer.byteLength(body, [''])
    }
  };

  var req = request.post(postRequest, function (err, httpResponse, body) {
    var result ="";
    
      if(err || (!body)){
        console.log("Error while sending collection event...");
        console.log(err);
      }
      else{
        var response = body.replace("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><methodResponse><params><param><value><base64>", "");
        response = response.replace("</base64></value></param></params></methodResponse>", "");
        result = new Buffer(response, 'base64').toString('ascii');
        console.log("Response : " + result)
      }
        client.assert.equal(httpResponse.statusCode, 200, "Status Code Correct");
        client.assert.equal(result.indexOf("Acknowledge") > -1, true, "Message accepted");

      
    });

  client.pause(2000)
  req.write(body);
  req.end();

};