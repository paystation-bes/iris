/**
 * @summary Sends a single phase CC transaction to Iris and asserts the response
 * @since 7.4.3
 * @version 1.0
 * @author Mandy F
 * @param transArray - an array of event objects {type:type, action: action}
 * @param serialNum - PS serial number
 * @param secretKey - secret key from getSPToken()
 * @returns client
 **/

var data = require('../../data.js');

var crypto = require('crypto');
var url = require('url');
var request;
try {
	request = require('../../node_modules/request');
} catch (error) {
	request = require('request');
}
var devurl = data("URL");
var paystationCommAddress = data("PaystationCommAddress");

function generateSPTokenSig(url, serialNumber, timeStamp, psKey, requestXML) {
	var hmacSig = "";
	var serialNum = serialNumber;
	var sigVer = "1";
	var timeStamp = encodeURIComponent(timeStamp);
	var version = "6.4.0";
	var requestXMLTrans = requestXML;

	hmacSig += "POST";
	hmacSig += url;
	hmacSig += "SerialNumber=";
	hmacSig += serialNum;
	hmacSig += "&SignatureVersion=";
	hmacSig += sigVer;
	hmacSig += "&Timestamp=";
	hmacSig += timeStamp;
	hmacSig += "&Version=";
	hmacSig += version;
	hmacSig += requestXMLTrans;

	var hmac = crypto.createHmac("SHA256", new Buffer(psKey, "base64")).update(hmacSig).digest("base64");
	var encodedHmac = encodeURIComponent(hmac);

	return encodedHmac;
}

exports.command = function (transArray, serialNum, psKey) {
	client = this;

	if (serialNum != null) {
		paystationCommAddress = serialNum;
	}
	var statusCode;
	var date = new Date();
	//Gen random messageNum <= 1000
	var messageNum = Math.floor(Math.random() * 100) + 1;
	//converts date to GMT
	var gmtDate = new Date(date.valueOf() + date.getTimezoneOffset() * 60000);
	var expiryDate = new Date(date.valueOf() + 60000 * 30 + date.getTimezoneOffset() * 60000);
	var sigDateString = gmtDate.getFullYear() + "-" + ('0' + (gmtDate.getMonth() + 1)).slice(-2) + "-" + ('0' + gmtDate.getDate()).slice(-2) + 'T' + ('0' + gmtDate.getHours()).slice(-2) + ":" + ('0' + gmtDate.getMinutes()).slice(-2) + ":" + ('0' + gmtDate.getSeconds()).slice(-2);
	var gmtDateString = gmtDate.getFullYear() + ":" + ('0' + (gmtDate.getMonth() + 1)).slice(-2) + ":" + ('0' + gmtDate.getDate()).slice(-2) + ':' + ('0' + gmtDate.getHours()).slice(-2) + ":" + ('0' + gmtDate.getMinutes()).slice(-2) + ":" + ('0' + gmtDate.getSeconds()).slice(-2) + ":GMT";
	var gmtExpDateString = expiryDate.getFullYear() + ":" + ('0' + (expiryDate.getMonth() + 1)).slice(-2) + ":" + ('0' + expiryDate.getDate()).slice(-2) + ':' + ('0' + expiryDate.getHours()).slice(-2) + ":" + ('0' + expiryDate.getMinutes()).slice(-2) + ":" + ('0' + expiryDate.getSeconds()).slice(-2) + ":GMT";

	var body = "";
	for (i = 0; i < transArray.length; i++) {
		if (transArray[i].purchaseDate != null) {
			gmtDateString = transArray[i].purchaseDate;
		}
		if (transArray[i].expiryDate != null) {
			gmtExpDateString = transArray[i].expiryDate;
		}

		body += "<Transaction>";
		body += "<MessageNumber>" + (messageNum + i) + "</MessageNumber>";
		body += "<PaystationCommAddress>" + paystationCommAddress + "</PaystationCommAddress>";
		body += "<Number>" + (messageNum + i) + "</Number>";
		body += "<LotNumber></LotNumber>";
		body += "<StallNumber>" + transArray[i].stallNum + "</StallNumber>";
		body += "<LicensePlateNo>" + transArray[i].licenceNum + "</LicensePlateNo>";
		body += "<AddTimeNum>27002</AddTimeNum>";
		body += "<Type>" + transArray[i].type + "</Type>";
		body += "<PurchasedDate>" + gmtDateString + "</PurchasedDate>";
		body += "<ParkingTimePurchased>30</ParkingTimePurchased>";
		body += "<OriginalAmount>" + transArray[i].originalAmount + "</OriginalAmount>";
		body += "<ChargedAmount>" + transArray[i].chargedAmount + "</ChargedAmount>";
		body += "<CashPaid></CashPaid>";
		body += "<CardPaid>" + transArray[i].cardPaid + "</CardPaid>";
		body += "<PaidByRFID>0</PaidByRFID>";
		body += "<ChangeDispensed>0</ChangeDispensed>";
		body += "<IsRefundSlip>0</IsRefundSlip>";
		body += "<CardData>" + transArray[i].cardData + "</CardData>";
		body += "<CouponNumber></CouponNumber>";
		body += "<ExpiryDate>" + gmtExpDateString + "</ExpiryDate>";
		body += "<RateName>Automation Rate</RateName>";
		body += "<RateId>1</RateId>";
		body += "<RateValue>100</RateValue>";
		body += "<TaxType>N/A</TaxType>";
		body += "</Transaction>";
	}

	var hostnameURL = url.parse(devurl).hostname;
	var signature = generateSPTokenSig(hostnameURL, paystationCommAddress, sigDateString, psKey, body);

	var postRequest = {
		uri : devurl + "/PayStation/Transaction?Signature=" + signature + "&SignatureVersion=1&Timestamp=" + sigDateString + "&SerialNumber=" + paystationCommAddress + "&Version=6.4.0",
		method : "POST",
		agent : false,
		timeout : 1000,
		headers : {
			'content-type' : 'application/xml',
			'content-length' : Buffer.byteLength(body, [''])
		}
	};

	console.log("Sending: " + body);

	var req = request.post(postRequest, function (err, httpResponse, body) {
			var response = body.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", "");
			result = new Buffer(response, 'utf8').toString('ascii');
			console.log("Response : " + result);
			client.assert.equal(httpResponse.statusCode, 200, "Status Code Correct");
			client.assert.equal(result.indexOf("Success") > -1, true, "Message accepted");
		});

	client.pause(2000);
	req.write(body);
	req.end();
};
