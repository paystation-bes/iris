/**
 * @summary Gets the Single Phase Token from Iris and asserts the response
 * @since 7.4.3
 * @version 1.0
 * @author Mandy F
 * @param serialNum - PS serial number
 * @param callback - callback function to receive the token
 * @returns client
 **/

var data = require('../../data.js');

var crypto = require('crypto');
var url = require('url');
var request;
try {
	request = require('../../node_modules/request');
} catch (error) {
	request = require('request');
}
var devurl = data("URL");
var paystationCommAddress = data("PaystationCommAddress");

function generateTokenSignature(url, serialNumber, timeStamp, requestXML) {
	var hmacSig = "";
	var serialNum = serialNumber;
	var sigVer = "1";
	var timeStamp = encodeURIComponent(timeStamp);
	var version = "6.4.0";
	var requestXMLTrans = requestXML;
	
	hmacSig += "POST";
	hmacSig += url;
	hmacSig += "/PayStation/GetToken";
	hmacSig += "SerialNumber=";
	hmacSig += serialNum;
	hmacSig += "&SignatureVersion=";
	hmacSig += sigVer;
	hmacSig += "&Timestamp=";
	hmacSig += timeStamp;
	hmacSig += "&Version=";
	hmacSig += version;
	hmacSig += requestXMLTrans;
	
	var hmac = crypto.createHmac("SHA256", "zaq123$ESZxsw234%RDX").update(hmacSig).digest("base64");
	var encodedHmac = encodeURIComponent(hmac);
	
	return encodedHmac;
}

exports.command = function (serialNum, callback) {
	client = this;
	
	if (serialNum != null) {
		paystationCommAddress = serialNum;
	}
	var statusCode;
	var date = new Date();
	//Gen random messageNum <= 1000
	var messageNum = Math.floor(Math.random() * 100) + 1;
	//converts date to GMT
	var gmtDate = new Date(date.valueOf() + date.getTimezoneOffset() * 60000);
	var gmtDateString = gmtDate.getFullYear() + "-" + ('0' + (gmtDate.getMonth() + 1)).slice(-2) + "-" + ('0' + gmtDate.getDate()).slice(-2) + 'T' + ('0' + gmtDate.getHours()).slice(-2) + ":" + ('0' + gmtDate.getMinutes()).slice(-2) + ":" + ('0' + gmtDate.getSeconds()).slice(-2);
	
	var body = "";
	body += "<Token MessageNumber=\"" + messageNum + "\">";
	body += "<PaystationCommAddress>" + paystationCommAddress + "</PaystationCommAddress>";
	body += "<MacAddress>61:23:56:64:25:53</MacAddress>";
	body += "</Token>";
	
	var hostnameURL = url.parse(devurl).hostname;
	var signature = generateTokenSignature(hostnameURL, paystationCommAddress, gmtDateString, body);
	
	var postRequest = {
		uri : devurl + "/PayStation/GetToken?Signature=" + signature + "&SignatureVersion=1&Timestamp=" + gmtDateString + "&SerialNumber=" + paystationCommAddress + "&Version=6.4.0",
		method : "POST",
		agent : false,
		timeout : 1000,
		headers : {
			'content-type' : 'application/xml',
			'content-length' : Buffer.byteLength(body, [''])
		}
	};

	console.log("Sending: " + body);

	var req = request.post(postRequest, function (err, httpResponse, body) {
			var response = body.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", "");
			result = new Buffer(response, 'utf8').toString('ascii');
			console.log("Response : " + result);
			client.assert.equal(httpResponse.statusCode, 200, "Status Code Correct");
			client.assert.equal(result.indexOf("Success") > -1, true, "Message accepted");
			var startPsSecretKey = result.indexOf('<SecretKey>') + '<SecretKey>'.length;
            var endPsSecretKey = result.indexOf('</SecretKey>');
			psSecretKey = result.substring(startPsSecretKey, endPsSecretKey);
			
			if (callback != null) {
				callback(psSecretKey);
			}
		});

	client.pause(2000);
	req.write(body);
	req.end();
};
