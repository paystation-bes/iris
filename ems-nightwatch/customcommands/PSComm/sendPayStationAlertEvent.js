/**
 * @summary Sends the event to Iris and asserts the response
 * @since 7.3.6
 * @version 1.0
 * @author Nadine B
 * @param eventArray - an array of event objects {type:type, action: action}, payStationCommAddress - pay station number
 * @returns client
 **/
var data = require('../../data.js');

var request;
try {
	request = require('../../node_modules/request');
} catch (error) {
	request = require('request');
}


exports.command = function (devurl, timestamp, eventArray) {
	client = this;
	var statusCode;
	var date = new Date();
	
	//Gen random messageNum <= 1000
	var messageNum = Math.floor(Math.random() * 100) + 1;
	var gmtDateString;
	
	if (timestamp == '') {
		//converts date to GMT
		var gmtDate = new Date(date.valueOf() + date.getTimezoneOffset() * 60000);
		var gmtDateString = gmtDate.getFullYear() + ":" + ('0' + (gmtDate.getMonth() + 1)).slice(-2) + ":" + ('0' + gmtDate.getDate()).slice(-2) + ':' + ('0' + gmtDate.getHours()).slice(-2) + ":" + ('0' + gmtDate.getMinutes()).slice(-2) + ":" + ('0' + gmtDate.getSeconds()).slice(-2) + ":GMT";

	} else {
		var gmtDateString = timestamp;
	}
	console.log("EVENT DATE: " + gmtDateString);
	
	var body = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><methodCall><methodName>XMLRPCRequestHandler.processXML</methodName><params><param><value><?xml version=\"1.0\"?><Paystation_2 Password=\"zaq123$ESZxsw234%RDX\" UserId=\"emsadmin\" Version=\"6.3 build 0013\"><Message4EMS>";
	var footer = "</Message4EMS></Paystation_2></value></param></params></methodCall>";

	var alertIntend = "";
	var eventMsgNum = "";
	
	for (i = 0; i < eventArray.length; i++) {
		console.log(eventArray[i].information);
		console.log("Inside sendPayStationAlertEvent, sending event " + eventArray[i].action + " for Pay Station " + eventArray[i].pos);
		if(eventArray[i].alertType == "Event"){
			alertIntend += eventArray[i].alertType;
			eventMsgNum += messageNum + i;
			body += "<Event MessageNumber=\"" + (messageNum + i) + "\"><PaystationCommAddress>" + eventArray[i].pos + "</PaystationCommAddress><TimeStamp>" + gmtDateString + "</TimeStamp><Type>" + eventArray[i].type + "</Type><Action>" + eventArray[i].action + "</Action><Information>" + eventArray[i].information + "</Information></Event>";
			
		}else if(eventArray[i].alertType == "HeartBeat"){
			alertIntend += eventArray[i].alertType;
			eventMsgNum += "HB";
			body += "<" + alertIntend + " MessageNumber=\"" + eventMsgNum + "\"><PaystationCommAddress>" + eventArray[i].pos + "</PaystationCommAddress><TimeStamp>" + gmtDateString + "</TimeStamp></" + alertIntend +">";
		}		
		
	}

	body += footer;
	
	var postRequest = {
		uri : devurl + '/XMLRPC_PS2',
		method : "POST",
		agent : false,
		timeout : 1000,
		headers : {
			'content-type' : 'text/xml',
			'content-length' : Buffer.byteLength(body, [''])
		}
	};

	var req = request.post(postRequest, function (err, httpResponse, body) {
				if(!err){
				var response = body.replace("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><methodResponse><params><param><value><base64>", "");
				response = response.replace("</base64></value></param></params></methodResponse>", "");
				result = new Buffer(response, 'base64').toString('ascii');
				console.log("Response : " + result)
				client.assert.equal(httpResponse.statusCode, 200, "Status Code Correct");
				client.assert.equal(result.indexOf("Acknowledge") > -1, true, "Message accepted");
				}else{
					console.log(err);
				}
			
		});

	client.pause(2000)
	req.write(body);
	req.end();

};
