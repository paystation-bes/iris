/**
 * @summary Sends a CC post-auth transaction to Iris and asserts the response
 * @since 7.4.2
 * @version 1.0
 * @author Mandy F
 * @param transArray - an array of event objects {type:type, action: action}
 * @param serialNum - PS serial number
 * @returns client
 **/
 
var data = require('../../data.js');

var request;
try {
	request = require('../../node_modules/request');
} catch (error) {
	request = require('request');
}
var devurl = data("URL");
var paystationCommAddress = data("PaystationCommAddress");

exports.command = function (transArray, serialNum) {
	client = this;
	if (serialNum != null) {
		paystationCommAddress = serialNum;
	}
	var statusCode;
	var date = new Date();
	//Gen random messageNum <= 1000
	var messageNum = Math.floor(Math.random() * 100) + 1;
	//converts date to GMT
	var gmtDate = new Date(date.valueOf() + date.getTimezoneOffset() * 60000);
	var expiryDate = new Date(date.valueOf() + 60000 * 30 + date.getTimezoneOffset() * 60000);
	var gmtDateString = gmtDate.getFullYear() + ":" + ('0' + (gmtDate.getMonth() + 1)).slice(-2) + ":" + ('0' + gmtDate.getDate()).slice(-2) + ':' + ('0' + gmtDate.getHours()).slice(-2) + ":" + ('0' + gmtDate.getMinutes()).slice(-2) + ":" + ('0' + gmtDate.getSeconds()).slice(-2) + ":GMT";
	var gmtExpDateString = expiryDate.getFullYear() + ":" + ('0' + (expiryDate.getMonth() + 1)).slice(-2) + ":" + ('0' + expiryDate.getDate()).slice(-2) + ':' + ('0' + expiryDate.getHours()).slice(-2) + ":" + ('0' + expiryDate.getMinutes()).slice(-2) + ":" + ('0' + expiryDate.getSeconds()).slice(-2) + ":GMT";

	var body = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><methodCall><methodName>XMLRPCRequestHandler.processXML</methodName><params><param><value><?xml version=\"1.0\"?><Paystation_2 Password=\"zaq123$ESZxsw234%RDX\" UserId=\"emsadmin\" Version=\"6.3 build 0013\"><Message4EMS>";
	var footer = "</Message4EMS></Paystation_2></value></param></params></methodCall>";

	for (i = 0; i < transArray.length; i++) {

		if (transArray[i].purchaseDate != null) {
			gmtDateString = transArray[i].purchaseDate;
		}

		if (transArray[i].expiryDate != null) {
			gmtExpDateString = transArray[i].expiryDate;
		}

		body += "<Transaction MessageNumber=\"" + (messageNum + i) + "\">";
		body += "<PaystationCommAddress>" + paystationCommAddress + "</PaystationCommAddress>";
		body += "<Number>" + (messageNum + i) + "</Number>";
		body += "<LicensePlateNo>" + transArray[i].licenceNum + "</LicensePlateNo>";
		body += "<LotNumber>Automation Lot</LotNumber>";
		body += "<StallNumber>" + transArray[i].stallNum + "</StallNumber>";
		body += "<AddTimeNum>270000</AddTimeNum>";
		body += "<Type>" + transArray[i].type + "</Type>";
		body += "<PurchasedDate>" + gmtDateString + "</PurchasedDate>";
		body += "<ParkingTimePurchased>30</ParkingTimePurchased>";
		body += "<OriginalAmount>" + transArray[i].originalAmount + "</OriginalAmount>";
		body += "<ChargedAmount>" + transArray[i].chargedAmount + "</ChargedAmount>";
		body += "<CouponNumber></CouponNumber>";
		body += "<CashPaid>" + transArray[i].cashPaid + "</CashPaid>";
		body += "<CardPaid>" + transArray[i].cardPaid + "</CardPaid>";
		body += "<CardAuthorizationId>" + transArray[i].cardAuthorizationId + "</CardAuthorizationId>";
		body += "<EmsPreAuthId>" + transArray[i].emsPreAuthId + "</EmsPreAuthId>";
		body += "<ExpiryDate>" + gmtExpDateString + "</ExpiryDate>";
	    body += "<Tax1Rate>7.24</Tax1Rate>";
        body += "<Tax1Value>1</Tax1Value>";
        body += "<Tax1Name>GST</Tax1Name>";
        body += "<Tax2Rate>5.00</Tax2Rate>";
        body += "<Tax2Value>1.00</Tax2Value>";
        body += "<Tax2Name>PST</Tax2Name>";
		body += "<LicensePlateNo></LicensePlateNo>";
		body += "<RateName>Automation Rate</RateName>";
		body += "<RateID>1</RateID>";
		body += "<RateValue>400</RateValue>";
		body += "<TaxType>N/A</TaxType>";
		body += "</Transaction>";
	}

	body += footer;

	var postRequest = {
		uri : devurl + '/XMLRPC_PS2',
		method : "POST",
		agent : false,
		timeout : 10000,
		headers : {
			'content-type' : 'text/xml',
			'content-length' : Buffer.byteLength(body, [''])
		}
	};
	
	console.log(body);
	
	var req = request.post(postRequest, function (err, httpResponse, body) {
		var result = "";
		// Checking for body not null/undefined here is redundant anyway.
		if (err || (!body)) {
			console.log("Error while trying to send event...");
			console.log(err);
		} else {
			var response = body.replace("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><methodResponse><params><param><value><base64>", "");
			response = response.replace("</base64></value></param></params></methodResponse>", "");
			result = new Buffer(response, 'base64').toString('ascii');
			
			console.log("Response : " + result);
		}
		
		client.assert.equal((httpResponse && httpResponse.statusCode) ? httpResponse.statusCode : -1, 200, "Status Code Correct");
		client.assert.equal(result.indexOf("Acknowledge") > -1, true, "Message accepted");
	});

	client.pause(2000)
	req.write(body);
	req.end();

};
