/**
 * @summary Create a new alert for a specified pay station
 * @since 7.4.3
 * @version 1.0
 * @author Nadine B
 * @see US-9617
 **/

exports.command = function(devurl, timestamp, payStation, alertIntent, eventType, actionEvent, info, callback) {
	var client = this;
	var data = require('../../data.js');
	
	var AlertEvent = {
		pos : payStation,
		alertType: alertIntent,
		type : eventType,
		action : actionEvent,
		information: info
	}

	console.log("Inside createNewPayStationAlert, about to send event");
	client.sendPayStationAlertEvent(devurl, timestamp, [AlertEvent]);
	
	return client;
};