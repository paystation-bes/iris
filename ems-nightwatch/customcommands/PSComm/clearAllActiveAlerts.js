/**
 * @summary Clears all active alerts for a pay station
 * @since 7.4.3
 * @version 1.0
 * @author Nadine B
 * @param payStation = name of the pay station as it appears in Iris, alert = name of the alert as it appears in Iris
 * @returns client
 */
var data = require('../../data.js');

var request;
try {
	request = require('../../node_modules/request');
} catch (error) {
	request = require('request');
}

var devurl = data("URL");

exports.command = function(payStation, alert, callback) {
	client = this;
	var type = '';
	var action = '';
	var info = '';
	
	console.log("*About to clear alerts for " + alert + "!");
	
	if (alert.indexOf("Removed") > -1) {
		action = 'Inserted';
		
	} else if (alert.indexOf("Not Present") > -1) {
		action = 'Present';
		
	} else if (alert.indexOf("Error") > -1) {
		
		if(alert.indexOf("Cutter") > -1) {
			action = 'CutterErrorCleared';
			
		} else if(alert.indexOf("Head") > -1) {
			action = 'HeadErrorCleared';
			
		} else if(alert.indexOf("Temperature") > -1) {
			action = 'TemperatureErrorCleared';
			
		} else if(alert.indexOf("Voltage") > -1) {
			action = 'VoltageErrorCleared';
		}
		
	} else if (alert.indexOf("Shock") > -1) {
		type = 'Paystation';
		action = 'ShockOff';
		
	} else if (alert.indexOf("Alarm") > -1) {
		type = 'Paystation';
		action = 'AlarmOff';
		
	} else if (alert.indexOf("Door") > -1) {
		action = 'DoorClosed';
		
	} else if (alert.indexOf("Service") > -1) {
		action = 'ServiceModeDisabled';
		
	} else if (alert.indexOf("Failure") > -1) {
		action = 'PaystationFailureCleared';
		
	} else if (alert.indexOf("Public Key") > -1){
		info = 'true';
		action = 'PublicKeyUpdate';
		
	} else if (alert.indexOf("Low") > -1) {
		
		if (alert.indexOf("Changer") > -1) {
			action = 'Normal';
			
		} else if (alert.indexOf("Power") > -1) {
			action = 'LowPowerShutdownOff';
			
		} else if (alert.indexOf("Paper") > -1) {
			action = 'PaperLowCleared';
			
		}
		
	} else if (alert.indexOf("Out") > -1) {
		action = 'PaperOutCleared';
		
	}else if (alert.indexOf("Jam") > -1) {
		
		if (alert.indexOf("Paper") > -1) {
			action = 'PaperJamCleared';
		} else {
			action = 'JamCleared';
		}
		
	} else if (alert.indexOf("Full") > -1) {
		action = 'FullCleared';
		
	} else if (alert.indexOf("Empty") > -1) {
		action = 'EmptyCleared';
		
	} else if (alert.indexOf("UnableToStack") > -1) {
		action = 'UnableToStackCleared';
	}
	
	
	if (alert.indexOf("Bill") > -1) {
		
		if (alert.indexOf("Acceptor") > -1) {
			type = 'BillAcceptor';
			
		} else if (alert.indexOf("Stacker") > -1) {
			type = 'BillStacker';
			
		} else if (alert.indexOf("Canister") > -1) {
			type = 'BillCanister';
			
		}
		
	} else if (alert.indexOf("Coin") > -1) {
		
		if (alert.indexOf("Acceptor") > -1) {
			type = 'CoinAcceptor';
			
		} else if (alert.indexOf("Hopper") > -1) {
			type = 'CoinHopper';
			
		} else if (alert.indexOf("Changer") > -1) {
			type = 'CoinChanger';
			
		} else if (alert.indexOf("Escrow") > -1) {
			type = 'CoinEscrow';
			
		} else if (alert.indexOf("Canister") > -1) {
			type = 'CoinCanister';
			
		}
		
	} else if (alert.indexOf("Card") > -1) {
		
		if (alert.indexOf("RFID") > -1) {
			type = 'RfidCardReader';
			
		} else {
			type = 'CardReader';
		}
		
	} else if (alert.indexOf("Pay Station") > -1) {
		type = 'Paystation';
		
	} else if (alert.indexOf("Printer") > -1) {
		type = 'Printer';
		
	} else if (alert.indexOf("Maintenance") > -1) {
		type = 'Paystation';
		info = 'Maintenance';
		
	} else if (alert.indexOf("Vault") > -1) {
		type = 'Paystation';
		info = 'CashVault';
		
	}
	
	
	if (alert.indexOf("Pedestal") > -1) {
		type = 'Paystation';
		client.createNewPayStationAlert(devurl, '', payStation, 'Event', type, action, 'Upper');
		client.createNewPayStationAlert(devurl, '', payStation, 'Event', type, action, 'Lower');
		
	} else {
		client.createNewPayStationAlert(devurl, '', payStation, 'Event', type, action, info);
		
	}
	
};