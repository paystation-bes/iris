/**
 * @summary Sends an Audit to Iris and asserts the response
 * @since 7.3.6
 * @version 1.1
 * @author Thomas C, Mandy F
 **/
var data = require('../../data.js');

var request;
try {
	request = require('../../node_modules/request');
} catch (error) {
	request = require('request');
}
var devurl = data("URL");
var paystationCommAddress = data("PaystationCommAddress");

exports.command = function (auditInfo, serialNum) {
	client = this;

	var statusCode;
	var date = new Date();
	//Gen random messageNum <= 1000
	var messageNum = Math.floor(Math.random() * 100) + 1;
	//converts date to GMT
	var gmtDate = new Date(date.valueOf() + date.getTimezoneOffset() * 60000);
	var gmtDateString = gmtDate.getFullYear() + ":" + ('0' + (gmtDate.getMonth() + 1)).slice(-2) + ":" + ('0' + gmtDate.getDate()).slice(-2) + ':' + ('0' + gmtDate.getHours()).slice(-2) + ":" + ('0' + gmtDate.getMinutes()).slice(-2) + ":" + ('0' + gmtDate.getSeconds()).slice(-2) + ":GMT";
	// default amounts for audit
	var gmtExpDateString = gmtDateString;
	var ticketsSoldAudit = "5";
	var coinAmountAudit = "10000.00";
	var onesAmountAudit = "1000.00";
	var twoesAmountAudit = "1000.00";
	var fivesAmountAudit = "2000.00";
	var tensAmountAudit = "5000.00";
	var twentiesAmountAudit = "1000.00";
	var fiftiesAmountAudit = "2000.00";
	var changeIssuedAudit = "2.00";
	var refundIssuedAudit = "3.00";
	var excessPaymentAudit = "0.00";
	var otherCardAmountAudit = "2.2";
	var visaCardAmountAudit = "1.23";
	var amexCardAmountAudit = "1.23";

	if (arguments.length == 1) {
		if (typeof arguments[0] == "string") {
			paystationCommAddress = arguments[0];
		}

		if (typeof arguments[0] == "object") {
			if (arguments[0].ticketsSold != null) {
				ticketsSoldAudit = arguments[0].ticketsSold;
			}

			if (arguments[0].coinAmount != null) {
				coinAmountAudit = arguments[0].coinAmount;
			}

			if (arguments[0].onesAmount != null) {
				onesAmountAudit = arguments[0].onesAmount;
			}

			if (arguments[0].twoesAmount != null) {
				twoesAmountAudit = arguments[0].twoesAmount;
			}

			if (arguments[0].fivesAmount != null) {
				fivesAmountAudit = arguments[0].fivesAmount;
			}

			if (arguments[0].tensAmount != null) {
				tensAmountAudit = arguments[0].tensAmount;
			}

			if (arguments[0].twentiesAmount != null) {
				twentiesAmountAudit = arguments[0].twentiesAmount;
			}

			if (arguments[0].fiftiesAmount != null) {
				fiftiesAmountAudit = arguments[0].fiftiesAmount;
			}

			if (arguments[0].changeIssued != null) {
				changeIssuedAudit = arguments[0].changeIssued;
			}

			if (arguments[0].refundIssued != null) {
				refundIssuedAudit = arguments[0].refundIssued;
			}

			if (arguments[0].excessPayment != null) {
				excessPaymentAudit = arguments[0].excessPayment;
			}

			if (arguments[0].otherCardAmount != null) {
				otherCardAmountAudit = arguments[0].otherCardAmount;
			}

			if (arguments[0].visaCardAmount != null) {
				visaCardAmountAudit = arguments[0].visaCardAmount;
			}

			if (arguments[0].amexCardAmount != null) {
				amexCardAmountAudit = arguments[0].amexCardAmount;
			}

			if (arguments[0].purchaseDate != null) {
				gmtDateString = arguments[0].purchaseDate;
			}

			if (arguments[0].expiryDate != null) {
				gmtExpDateString = arguments[0].expiryDate;
			}
		}
	} else {
		if (serialNum != null) {
			paystationCommAddress = serialNum;
		}

		if (auditInfo != null) {
			if (auditInfo.ticketsSold != null) {
				ticketsSoldAudit = auditInfo.ticketsSold;
			}

			if (auditInfo.coinAmount != null) {
				coinAmountAudit = auditInfo.coinAmount;
			}

			if (auditInfo.onesAmount != null) {
				onesAmountAudit = auditInfo.onesAmount;
			}

			if (auditInfo.twoesAmount != null) {
				twoesAmountAudit = auditInfo.twoesAmount;
			}

			if (auditInfo.fivesAmount != null) {
				fivesAmountAudit = auditInfo.fivesAmount;
			}

			if (auditInfo.tensAmount != null) {
				tensAmountAudit = auditInfo.tensAmount;
			}

			if (auditInfo.twentiesAmount != null) {
				twentiesAmountAudit = auditInfo.twentiesAmount;
			}

			if (auditInfo.fiftiesAmount != null) {
				fiftiesAmountAudit = auditInfo.fiftiesAmount;
			}

			if (auditInfo.changeIssued != null) {
				changeIssuedAudit = auditInfo.changeIssued;
			}

			if (auditInfo.refundIssued != null) {
				refundIssuedAudit = auditInfo.refundIssued;
			}

			if (auditInfo.excessPayment != null) {
				excessPaymentAudit = auditInfo.excessPayment;
			}

			if (auditInfo.otherCardAmount != null) {
				otherCardAmountAudit = auditInfo.otherCardAmount;
			}

			if (auditInfo.visaCardAmount != null) {
				visaCardAmountAudit = auditInfo.visaCardAmount;
			}

			if (auditInfo.amexCardAmount != null) {
				amexCardAmountAudit = auditInfo.amexCardAmount;
			}

			if (auditInfo.purchaseDate != null) {
				gmtDateString = auditInfo.purchaseDate;
			}

			if (auditInfo.expiryDate != null) {
				gmtExpDateString = auditInfo.expiryDate;
			}
		}
	}

	var body = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><methodCall><methodName>XMLRPCRequestHandler.processXML</methodName><params><param><value><?xml version=\"1.0\"?><Paystation_2 Password=\"zaq123$ESZxsw234%RDX\" UserId=\"emsadmin\" Version=\"6.3 build 0013\"><Message4EMS>";
	var footer = "</Message4EMS></Paystation_2></value></param></params></methodCall>";

	body += "<Audit MessageNumber=\"" + (messageNum) + "\">";
	body += "<PaystationCommAddress>" + paystationCommAddress + "</PaystationCommAddress>";
	body += "<ReportNumber>" + (messageNum) + "</ReportNumber>";
	body += "<LotNumber>Automation Parking</LotNumber>";
	body += "<MachineNumber>4</MachineNumber>";
	body += "<StartDate>" + gmtDateString + "</StartDate>";
	body += "<EndDate>" + gmtExpDateString + "</EndDate>";
	body += "<PatrollerTicketsSold>0</PatrollerTicketsSold>";
	body += "<TicketsSold>" + ticketsSoldAudit + "</TicketsSold>";
	body += "<CoinAmount>" + coinAmountAudit + "</CoinAmount>";
	body += "<OnesAmount>" + onesAmountAudit + "</OnesAmount>";
	body += "<TwoesAmount>" + twoesAmountAudit + "</TwoesAmount>";
	body += "<FivesAmount>" + fivesAmountAudit + "</FivesAmount>";
	body += "<TensAmount>" + tensAmountAudit + "</TensAmount>";
	body += "<TwentiesAmount>" + twentiesAmountAudit + "</TwentiesAmount>";
	body += "<FiftiesAmount>" + fiftiesAmountAudit + "</FiftiesAmount>";
	body += "<SmartCardRechargeAmount>0</SmartCardRechargeAmount>";
	body += "<CitationsPaid>0</CitationsPaid>";
	body += "<CitationAmount>0</CitationAmount>";
	body += "<ChangeIssued>" + changeIssuedAudit + "</ChangeIssued>";
	body += "<RefundIssued>" + refundIssuedAudit + "</RefundIssued>";
	body += "<ExcessPayment>" + excessPaymentAudit + "</ExcessPayment>";
	body += "<NextTicketNumber>33</NextTicketNumber>";
	body += "<AttendantTicketsSold>0</AttendantTicketsSold>";
	body += "<AttentantTicketsAmount>0</AttentantTicketsAmount>";
	body += "<AttendantDepositAmount>0</AttendantDepositAmount>";
	body += "<AcceptedFloatAmount>0</AcceptedFloatAmount>";
	body += "<CurrentTubeStatus>0_0:0_0:0_0:0_0</CurrentTubeStatus>";
	body += "<OverfillAmount>0</OverfillAmount>";
	body += "<ReplenishedAmount>0</ReplenishedAmount>";
	body += "<HopperInfo>0:0_0:0_0:0_0:0</HopperInfo>";
	body += "<TestDispensed>0:0:0</TestDispensed> <CardAmounts>";
	body += "<CardType>Other</CardType>";
	body += "<CardAmount>" + otherCardAmountAudit + "</CardAmount>";
	body += "<CardType>Visa</CardType>";
	body += "<CardAmount>" + visaCardAmountAudit + "</CardAmount>";
	body += "<CardType>Amex</CardType>";
	body += "<CardAmount>" + amexCardAmountAudit + "</CardAmount>";
	body += "</CardAmounts>";
	body += "</Audit>";

	body += footer;

	console.log(body);

	var postRequest = {
		uri : devurl + '/XMLRPC_PS2',
		method : "POST",
		agent : false,
		timeout : 1000,
		headers : {
			'content-type' : 'text/xml',
			'content-length' : Buffer.byteLength(body, [''])
		}
	};

	var req = request.post(postRequest, function (err, httpResponse, body) {

			var response = body.replace("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><methodResponse><params><param><value><base64>", "");
			response = response.replace("</base64></value></param></params></methodResponse>", "");
			result = new Buffer(response, 'base64').toString('ascii');
			console.log("Response : " + result);
			client.assert.equal(httpResponse.statusCode, 200, "Status Code Correct");
			client.assert.equal(result.indexOf("Acknowledge") > -1, true, "Message accepted");

		});

	client.pause(2000);
	req.write(body);
	req.end();

};
