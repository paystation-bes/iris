/**
 * @summary Sends a Collection Event to a Pay Station
 * @since 7.3.6
 * @version 1.0
 * @author Thomas C, Billy H
 * @param amounts - array of amounts to be collected (ie. [1,2,3,4,5]), paystaionCommAddress - Serial number of the Pay Station you want to send to
 * @returns client
 * @returns client
 **/
var data = require('../../data.js');

var request;
try {
	request = require('../../node_modules/request');
} catch (error) {
	request = require('request');
}
var devurl = data("URL");

exports.command = function (amounts, paystationCommAddress) {
	var client = this;
	var statusCode;
	var date = new Date();
	//Gen random messageNum <= 1000
	var messageNum = Math.floor(Math.random() * 100) + 1;
	//converts date to GMT
	var gmtDate = new Date(date.valueOf() + date.getTimezoneOffset() * 60000);
	var gmtDateString = gmtDate.getFullYear() + ":" + ('0' + (gmtDate.getMonth() + 1)).slice(-2) + ":" + ('0' + gmtDate.getDate()).slice(-2) + ':' + ('0' + gmtDate.getHours()).slice(-2) + ":" + ('0' + gmtDate.getMinutes()).slice(-2) + ":" + ('0' + gmtDate.getSeconds()).slice(-2) + ":GMT";
	//var gmtDateString2 = gmtDate.getFullYear() + ":" + ('0' + (gmtDate.getMonth() + 1)).slice(-2) + ":" + ('0' + gmtDate.getDate()).slice(-2) + ':' + ('0' + gmtDate.getHours()).slice(-2) + ":" + ('0' + gmtDate.getMinutes()).slice(-2) + ":" + ('0' + gmtDate.getSeconds()).slice(-2) + ":GMT";

	var body = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><methodCall><methodName>XMLRPCRequestHandler.processXML</methodName><params><param><value><?xml version=\"1.0\"?><Paystation_2 Password=\"zaq123$ESZxsw234%RDX\" UserId=\"emsadmin\" Version=\"6.3 build 0013\"><Message4EMS>";
	var footer = "</Message4EMS></Paystation_2></value></param></params></methodCall>";

	body += "<Collection MessageNumber=\"" + messageNum + "\"><Type>Bill</Type><PaystationCommAddress>" + paystationCommAddress + "</PaystationCommAddress><ReportNumber>" + messageNum + "</ReportNumber><LotNumber>Collection Lot</LotNumber><MachineNumber>Collection Machine</MachineNumber><StartDate>" + gmtDateString + "</StartDate><EndDate>" + gmtDateString + "</EndDate><StartTransNumber>1</StartTransNumber><EndTransNumber>10</EndTransNumber><TicketCount>10</TicketCount><BillCount01>" + amounts[0].toString() + "</BillCount01><BillCount02>" + amounts[1].toString() + "</BillCount02><BillCount05>" + amounts[2].toString() + "</BillCount05><BillCount10>" + amounts[3].toString() + "</BillCount10><BillCount20>" + amounts[4].toString() + "</BillCount20><BillCount50>" + amounts[5].toString() + "</BillCount50></Collection>";

	body += footer;
	
	var postRequest = {
		uri : devurl + '/XMLRPC_PS2',
		method : "POST",
		agent : false,
		timeout : 10000,
		headers : {
			'content-type' : 'text/xml',
			'content-length' : Buffer.byteLength(body, [''])
		}
	};

	var req = request.post(postRequest, function (err, httpResponse, body) {
		var result = "";
		// Checking for body not null/undefined here is redundant anyway.
		if (err || (!body)) {
			console.log("Error while trying to send event...");
			console.log(err);
		} else {
			var response = body.replace("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><methodResponse><params><param><value><base64>", "");
			response = response.replace("</base64></value></param></params></methodResponse>", "");
			result = new Buffer(response, 'base64').toString('ascii');
			
			console.log("Response : " + result);
		}
		
		client.assert.equal((httpResponse && httpResponse.statusCode) ? httpResponse.statusCode : -1, 200, "Status Code Correct");
		client.assert.equal(result.indexOf("Acknowledge") > -1, true, "Message accepted");
	});

	client.pause(2000)
	req.write(body);
	req.end();

};
