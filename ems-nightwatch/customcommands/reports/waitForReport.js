/**
 * @summary Custom Command: Wait for report to finish generating
 * @since 7.4.2
 * @version 1.0
 * @author Mandy F
 */

/**
 * @description Waits for report to finish generating
 * @param report name
 * @callback callback function to be called when the command finishes (OPTIONAL)
 * @returns client
 */
exports.command = function (name, callback) {
	var client = this;
	
	var count = 0;

	var reloadBtn = "//section[@class='groupBox menuBox']//h2[text() = 'Pending and Incomplete Reports']//..//a[@title='Reload']";
	var reportSubmitted = "//ul[@id='queuedReportsList']//p[contains(., '" + name + "')]//..//..//p[text() = 'Submitted']";
	var reportRunning = "//ul[@id='queuedReportsList']//p[contains(., '" + name + "')]//..//..//p[text() = 'Running']";
	var reportComplete = "//ul[@id='reportsList']//p[contains(., '" + name + "')]//..//..//p[text() = 'just now']";

	// call retry() function for the first time
	retry();

	// retry() function defined explicitly to call it again recursively
	function retry() {
		// just in case we are stuck in an infinity loop, this will break the recursive call after 30 refreshes (~ 5 mins)
		// and will fail the whole script because it could mean reports are not generating
		if (count > 30) {
			client.assert.equal("Retried too many times", "Can keep retrying");
		}
		count++;
		console.log(count);
		
		client
		.useXpath()
		.elements("xpath", reportSubmitted, function (result) {
			// if report is still in 'submitted' state, wait 10 seconds then refresh
			if (result.value.length > 0) {
				console.log("Wait 10 seconds before retrying...");
				client
				.useXpath()
				.pause(10000)
				.waitForElementVisible(reloadBtn, 1000)
				.click(reloadBtn)
				.pause(1000);
				// retry() called recursively
				retry();
				// else report is no longer in submitted state
			} else {
				client
				.useXpath()
				.elements("xpath", reportRunning, function (result) {
					// if report is now in 'running' state, wait 5 seconds and refresh
					if (result.value.length > 0) {
						console.log("Wait 5 seconds before retrying...");
						client
						.useXpath()
						.pause(5000)
						.waitForElementVisible(reloadBtn, 1000)
						.click(reloadBtn)
						.pause(1000);
						// retry() called recursively again
						retry();
						// else report is no longer in 'running' or 'submitted' state, assert it just completed
						// report could be in 'failed' state, in that case, whole script will fail because assertion failed
					} else {
						client
						.useXpath()
						.assert.visible(reportComplete)
						.pause(1000);

						// optional callback function, doesn't return any values
						if (callback != null) {
							callback();
						}
					}
				});
			}
		});
	}

	return client;
};
