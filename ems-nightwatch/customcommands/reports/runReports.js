/**
 * @summary Customer: Reports: Reports should have 'Today' as default date selection
 * @since 7.3.4
 * @version 1.0
 * @author Nadine B
 * @see EMS-6318
 * @description Runs the report script: finds a report and clicks next step, then varifies that Transaction Date/Time field is set to 'Today'
 * @param client - The web browser object, callback - optional
 * @returns client
**/
exports.command = function(reportName, callback) {
    var client = this;
	
    console.log("Report name is: " + reportName);
	//"Expand: Click on transaction type arrow"
    client
		.pause(1000)
		.click("//*[@id='reportTypeIDExpand']")
		.pause(500) 
		
	//"Select transaction type"
		.setValue("//*[@id='reportTypeID']",reportName)
		.waitForElementVisible("//a[@class='ui-corner-all'][contains(text(),'" + reportName + "')]", 4500)
		.pause(500)
		.click("//a[@class='ui-corner-all'][contains(text(),'" + reportName + "')]")
		.pause(1000) 
		
	//"Click next step"
		.click("//*[@id='btnStep2Select']")
		.pause(1000) 
		
	//"Assert that Transaction Date/Time shows Today"
		.getValue("//*[@id='formPrimaryDateType']", function(result) {
			console.log("Date is: " + result.value);
			if(result.value == ""){
				client.assert.equal(undefined);
			}else{
				client.assert.equal(result.value, "Today");
			}
		   client.pause(1000)
		 });

		client
			.click("//*[@id='btnCancelReportType']")
			.pause(700) 
			
	//"Confirm cancelation"
			.click("//*[@id='settings']/div[3]/div[2]/div/button[1]")
			.pause(1000);
		
	return client;
};