exports.command = function(tabName, callback) {
    var client = this;
    
    client
        .useXpath()
        .waitForElementVisible("//a[@title='New Tab']", 1000)
        .click("//a[@title='New Tab']")
        .pause(1000)
        .waitForElementVisible("//section[@class='tabSection current new']/input[@class='tabInput']", 1000)
        .keys(tabName)
        .pause(2000);
        
    return this;
};
