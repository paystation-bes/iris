exports.command = function(callback) {
    var client = this;
    
    client
        .useXpath()
        .waitForElementVisible("//a[@title='Save']", 1000)
        .click("//a[@title='Save']")
        .pause(1000)
        .refresh()
        .pause(3000);
        
    return this;
};
