exports.command = function(widget, callback) {
    var client = this;
    
    client
        .useXpath()
        // Clicks the Add Widget button
        .waitForElementVisible("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']", 1000)
        .click("//section[@style='display: block;']/section/section/section/a[@title='Add Widget']")
        .pause(3000)
        // Gets the currently selected widget list.
        .waitForElementPresent("//section[@class='widgetListContainer']/ul[@style='display: block;']", 1000)
        .getAttribute("//section[@class='widgetListContainer']/ul[@style='display: block;']", "id", function(result) {
            var selectedWidgetList = "";
            var desiredWidgetList = "";
            console.log("Selected Widget List: " + result.value);
            selectedWidgetList = result.value;
            client
                // Gets the widget list where the desired widget can be found.
                .waitForElementPresent("//ul[@class='widgetSelection']/li/header[text()='" + widget + "']", 1000)
                .getAttribute("//ul[@class='widgetSelection']/li/header[text()='" + widget + "']/../..", "id", function(result) {
                    console.log("Desired Widget List: " + result.value);
                    desiredWidgetList = result.value;
                    CompareWidgetIds(selectedWidgetList, desiredWidgetList);
                })
        })
    
    return this;
    
    // Compares the two widget list ids and clicks the widget list if it is not already selected
    function CompareWidgetIds(selectedWidgetList, desiredWidgetList) {
        if (selectedWidgetList != desiredWidgetList) {
            var widgetListNumber = "";
            // Remove text from widget list ID so you are left with just the number
            widgetListNumber = desiredWidgetList.replace("metricList", "");
            console.log("Widget List Number: " + widgetListNumber);
            ClickWidgetList(widgetListNumber);
        }
        SelectAndAddWidget();
    }
    
    // Clicks the specified widget list
    function ClickWidgetList(widgetListNumber) {
        client
            .waitForElementPresent("//section[@class='widgetListContainer']/h3[@id='metric" + widgetListNumber + "']", 1000)
            .click("//section[@class='widgetListContainer']/h3[@id='metric" + widgetListNumber + "']")
            .pause(1000);
    }
    
    // Selects and Adds the widget
    function SelectAndAddWidget() {
        client
            .waitForElementPresent("//header[text()='" + widget + "']", 1000)
            .click("//header[text()='" + widget + "']")
            .pause(1000)
            .waitForElementVisible("//button/span[text()='Add widget']", 1000)
            .click("//button/span[text()='Add widget']")
            .pause(1000)
    }
};
