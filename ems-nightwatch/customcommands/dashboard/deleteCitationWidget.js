/**
 * @summary Customer Admin: Citation widgets added and appear
 * @since 7.3.4
 * @version 1.0
 * @author Nadine B
 * @see US-897
 **/

exports.command = function(widgetName, callback) {
    var client = this;
    var widgetObject = "//section[@class='widgetObject']/section[@class='widget']/header/span[contains(text(),";
    var widgetObjectName = "//section[@class='widget']/header/span[@class='wdgtName' and contains(text(),";
    var optionMenu = "')]/../a[@class='hdrButtonIcn menu editOnly' and contains(@title, 'Option Menu')]";
    var deleteWidget = "']/section/section[@class='editMenuOptions innerBorder']/a[@class='delete deleteWdgt']";
    var confirmDelete = "//div[@class='ui-dialog-buttonset']/button/span[@class='ui-button-text' and contains(text(), 'Delete widget')]";
    var editDashboardButton = "//*[@id='headerTools']/a[@class='linkButtonFtr edit']";
    
	client
	.useXpath()
	
	.waitForElementPresent(widgetObject + " '" + widgetName + "')]", 4000)

	.waitForElementPresent(widgetObjectName + " '" + widgetName + optionMenu, 4000)
	.click(widgetObjectName + " '" + widgetName + optionMenu)
	
	.getAttribute(widgetObjectName + " '" + widgetName + "')]/../../..", "id", function(result) {
		client
		.pause(1000)
		.waitForElementVisible("//section[@id='" + result.value + deleteWidget, 4000)
		.click("//section[@id='" + result.value + deleteWidget)
		
		.waitForElementVisible(confirmDelete, 4000)
		.click(confirmDelete)
		.pause(2000)
		
		
	})

	.assert.elementNotPresent(widgetObject + " '" + widgetName + "')]")

	.pause(1000)
	
	return client;
};