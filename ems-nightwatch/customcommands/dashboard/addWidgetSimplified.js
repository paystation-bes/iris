/**
 * @summary Customer Admin: widgets added and appear
 * @since 7.4.2
 * @version 1.0
 * @author Johnson N
 * @param widgetName - The name of the widget the User wants to add
 **/
 
 
exports.command = function(widgetName, callback) {
    var client = this;
    var addWidgetButton = "//section[@class='layout5 layout']/section[@class='sectionTools'][1]/section/a[@class='linkButtonIcn addWidget']";
	var showAllButton = "//section[@class='widgetListContainer']/a[@class='linkButton textButton showAllWdgts']";
	client
		.useXpath()
		.pause(1000)
		.waitForElementVisible(addWidgetButton, 2000)
		.click(addWidgetButton)
		
		
		.pause(1000)
		.waitForElementVisible(showAllButton, 2000)
		.click(showAllButton)
			
		.pause(1000)
		.click("//section[@class='widgetListContainer']/ul/li[header[contains(text(), '" + widgetName + "')]]")
			
		.pause(500)
		.click("//div[@class='ui-dialog-buttonset']/button/span[contains(text(), 'Add widget')]")
        .pause(1000);
	
	return client;
};