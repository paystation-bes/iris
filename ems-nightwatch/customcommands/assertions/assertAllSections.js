/**
 * @summary Asserts that all Settings tabs are present
 * @since 7.3.5
 * @version 1.0
 * @author Thomas C
 **/
exports.command = function () {
	var client = this;
	client.useXpath()
	.assert.visible("//a[@title='Settings']")
	.assert.visible("//a[@title='Reports']")
	.assert.visible("//a[@title='Maintenance']")
	.assert.visible("//a[@title='Collections']")
	.assert.visible("//a[@title='Card Management']")
	.assert.visible("//a[@title='Accounts']")
	.assert.visible("//a[@title='Dashboard']")
	return client;
};
