var data = require('../../data.js');

var devurl = data("URL");
var username = data("ParentUserName");
var password = data("Password");

module.exports = {
	"Login" : function (browser) {
		browser
		.url(devurl)
		.windowMaximize('current')
		.login(username, password)
		.assert.title('Digital Iris - Main (Dashboard)');
	},

	"Navigate to Roles" : function (browser) {
		browser
		.navigateTo("Settings")
		.navigateTo("Users")
		.navigateTo("Roles")
		.pause(3000)
	},

	"Click Delete Button" : function (browser) {
		var optionsMenu = "//a[@title='Option Menu']";
		var elements = browser.elements('xpath', optionsMenu, function(result){
						
			for (i = 1; i < result.value.length; i++) {
				browser
				.waitForElementVisible(optionsMenu, 1000)
				.click(optionsMenu)
				.waitForElementVisible("//a[text()='Delete']", 2000)
				.click("//a[text()='Delete']")
				.waitForElementVisible(" //span[text()='Delete']", 2000)
				.click("//span[text()='Delete']")
				.pause(2000);
				}
		})
			
	},

	"Verify that default roles are present" : function(browser){
		browser
		.pause(5000)
		.elements('xpath', "//ul[@id='roleList']//li", function(result){
			browser.assert.equal(2, result.value.length, "All Roles Deleted");
	})
	},
	
	"Logout" : function (browser) {
		browser.logout();
	},

};
