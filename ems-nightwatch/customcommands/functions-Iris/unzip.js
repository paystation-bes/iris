/**
 * @summary Custom Command: Unzip File
 * @since 7.4.1
 * @version 1.0
 * @author Mandy F
 */

/**
 * @description Unzips a file
 * @param fileName - the name of the zip file
 * @param directory - the directory of the zip file (usually __dirname)
 * @returns client
 */
 
exports.command = function (fileName, directory) {
	var client = this;
	var unzip = require('unzip');
	var fs = require('fs');
	
	client.pause(500, function () {
		fs.createReadStream(directory + "\\" + fileName).pipe(unzip.Extract({ path: directory }));
		console.log("File Successfully Unzipped!");
	});

	return client;
};
