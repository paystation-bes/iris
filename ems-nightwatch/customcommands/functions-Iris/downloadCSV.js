/**
 * @summary Custom Command: Download CSV *NOTE* Doesn't actually test functionality of the 'export' buttons
 * @since 7.3.6
 * @version 1.1
 * @author Mandy F
 */

/**
 * @description Downloads file from the site for the export options
 * @param option - the part of the site you want to export for: Coupons, Passcards, Widget, Pay Station: Recent Actvity, or Reports
 * @param devURL - the URL/branch you are currently on
 * @param fileName - the name of the file WITHOUT extension name to save as 
 * @param directory - directory of where the file is to be saved
 * @param id - either name of the widget to export, serial number of pay station, or report name (OPTIONAL)
 * @returns client
 */
exports.command = function (option, devURL, fileName, directory, id) {
	var fs = require('fs');
	var request = require('request');
	var client = this;
	var url = new String();
	var selectedID = new String();
	var cookieName = new String();
	var cookieValue = new String();
	var optionEdited = option.toLowerCase().trim();
	var dest = directory + "\\" + fileName + ".csv";
	var file = fs.createWriteStream(dest);

	client.getCookie("JSESSIONID", function (result) {
		cookieName = result.name;
		cookieValue = result.value;
	})
	client.pause(1000, function () {
		// export CSV URL for widget
		if ((optionEdited === "widget") || (optionEdited === "widgets")) {
			client.assert.title('Digital Iris - Main (Dashboard)');
			client.useXpath().getAttribute("(//span[@class='wdgtName'])[text() = '" + id + "']//..//..//..", "id", function (result) {				
				selectedID = result.value.trim().substr(9);
			});
			client.pause(1000, function () {
				url = devURL + "/secure/dashboard/widgetCSV.html?widgetID=" + selectedID + "&";
			});

		// export CSV URL for pay station: recent activity
		} else if ((optionEdited  === "pay station") || (optionEdited  === "pay stations") || (optionEdited  === "paystation") || (optionEdited  === "paystations")) {
			client.assert.title('Digital Iris - Settings - Pay Stations : Pay Station List');
			client.useXpath().getAttribute("(//span[@class='container'])[text() = '" + id + "']//..//..", "id", function (result) {				
				selectedID = result.value.trim().substr(3);
			});
			client.pause(1000, function () {
				url = devURL + "/secure/settings/locations/payStationList/alertsHistory.html?payStationID=" + selectedID + "&";
			});		
			
		// export CSV URL for SYSTEM ADMIN pay station: recent activity
		} else if ((optionEdited  === "system admin pay station") || (optionEdited  === "system admin pay stations") || (optionEdited  === "system admin paystation") || (optionEdited  === "system admin paystations")) {
			client.useXpath().getAttribute("(//span[@class='container']//span[@class='textLine'])[contains(., '" + id + "')]//..//..//..", "id", function (result) {				
				selectedID = result.value.trim().substr(3);
			});
			client.url(function (result) {
				var startCustID = result.value.indexOf('customerID=') + 'customerID='.length;
				var custID = result.value.substring(startCustID);
				url = devURL + "/systemAdmin/workspace/payStations/payStationDetails/alertsHistory.html?payStationID=" + selectedID + "&&customerID=" + custID;
			});
			
		// export CSV URL for reports
        } else if ((optionEdited === "reports") || (optionEdited === "report")) {
            client.assert.title('Digital Iris - Reports');
            client.useXpath().getAttribute("(//ul[@id='reportsList']//p[contains(text(), '" + id + "')]//..//..)[1]", "id", function (result) {              
                selectedID = result.value.trim().substr(7);
            });
            client.pause(1000, function () {
                url = devURL + "/secure/reporting/viewReport.html?&repositoryID=" + selectedID;
            });	
			
		// export PDF URL for SYSTEM ADMIN reports
		} else if ((optionEdited  === "system admin report") || (optionEdited  === "system admin reports")) {
			client.useXpath().getAttribute("(//ul[@id='reportsList']//p[contains(text(), '" + id + "')]//..//..)[1]", "id", function (result) {				
				selectedID = result.value.trim().substr(7);
			});
			client.pause(1000, function () {
				url = devURL + "/systemAdmin/reporting/viewReport.html?&repositoryID=" + selectedID;
			});
		
		// export CSV URL for reports
		} else if ((optionEdited === "reports") || (optionEdited === "report")) {
			client.assert.title('Digital Iris - Reports');
			client.useXpath().getAttribute("(//ul[@id='reportsList']//p[contains(text(), '" + id + "')]//..//..)[1]", "id", function (result) {				
				selectedID = result.value.trim().substr(7);
			});
			client.pause(1000, function () {
				url = devURL + "/secure/reporting/viewReport.html?&repositoryID=" + selectedID; 
			});
			
		// export CSV URL for coupons
		} else if ((optionEdited  === "coupons") || (optionEdited  === "coupon")) {
			client.assert.title('Digital Iris - Accounts : Coupons');
			url = devURL + "/secure/accounts/coupons/exportCoupons.html?";
		
		// export CSV URL for passcards
		} else if ((optionEdited  === "passcards") || (optionEdited  === "passcard")) {
			client.assert.title('Digital Iris - Accounts : Passcards');
			url = devURL + "/secure/accounts/customerCards/exportCards.html?";
		}
		
		// using the URL from earlier to get the downloaded file and then put it in desired file
		client.pause(1000, function () {
			var options = {
				url : url,
				method : 'GET',
				headers : {
					'Cookie' : cookieName + "=" + cookieValue,
				},
			};
			request(options, function (error, response, body) {
				if (!error && response.statusCode === 200) {
					console.log("File Download Success!") 
				} else {
					console.log("File Download Error: " + response.statusCode)
				}
			}).pipe(file);
			client.pause(2000);
		})
	});

	return client;
};
