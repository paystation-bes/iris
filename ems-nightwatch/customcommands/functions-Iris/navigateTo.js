/**
 * @summary Navigates to the given title 
 * @since 7.3.5
 * @version 1.0
 * @author Thomas C
 **/
exports.command = function (target) {
	var client = this;
	var navTo;
	if (target === "Settings") {
		navTo = "//section[@class='bottomSection']/a[@title='Settings']";
	} else {
		navTo = "//a[@title='" + target + "']";
	}
	client.useXpath();
	client.pause(1000)
	client.waitForElementVisible(navTo, 2000);
	client.click(navTo);
	client.pause(1000);
	return client;
};
