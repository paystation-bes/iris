/**
 * @summary Custom Command: Compare two CSV files
 * @since 7.3.6
 * @version 1.1
 * @author Mandy F
 */

/**
 * @description Compares two CSV files to see if they are the same
 * @param firstCSV - filename of the first CSV
 * @param secondCSV - filename of the second CSV
 * @param directory - directory of the files
 * @param callback - the callback function to return a boolean on whether the CSV matches (true) or doesn't match (false) (OPTIONAL)
 * @returns client
 */
exports.command = function (firstCSV, secondCSV, directory, callback) {
	var client = this;
	var firstCSVFile = firstCSV + ".csv";
	var secondCSVFile = secondCSV + ".csv";
	var csv1 = [];
	var csv2 = [];
	
	var compare = false;
	
	// parse the first CSV
	client.parseCSV(firstCSVFile, directory, function (result) {
		csv1 = result;
		
		// parse the second CSV
		client.parseCSV(secondCSVFile, directory, function (result) {
			csv2 = result;
			
			// compare then make assertion and return boolean
			client.pause(1, function (result) {
				client.assert.equal(csv1.sort().toString(), csv2.sort().toString(), "Same!");
				if (csv1.sort().toString() === csv2.sort().toString()) {
					compare = true;
				}
				client.pause(1, function(result) {
					if (callback != null) {
						callback(compare);
					}
				});
			});
		});
	});

	return client;
};
