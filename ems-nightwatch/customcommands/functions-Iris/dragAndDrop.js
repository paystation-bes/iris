/**
 * @summary Custom Command: Drag and Drop 
 * @since 7.3.4
 * @version 1.0
 * @author Thomas C,
 */


/**
	 * @description Drag and Drop PS onto map
	 * @param source - element to be selected and dragged
	 * @param sourceOffsetX - pixels from top left corner of source element
	 * @param sourceOffsetY -pixels from top left corner of source element
	 * @param target - element to be dropped onto 
	 * @param targetOffsetX - pixels from top left corner of source element
	 * @param targetOffsetY -pixels from top left corner of source element
	 * @returns client
	 */
exports.command = function(source, sourceOffsetX,sourceOffsetY, target, targetOffsetX, targetOffsetY) {
	var client = this;
	client.moveToElement(source, sourceOffsetX, sourceOffsetY, function() {
		client.pause(500);
		client.mouseButtonDown(0);
		client.moveToElement(target, targetOffsetX, targetOffsetY);
		client.mouseButtonUp(0);
		});
	
	return client;
};
