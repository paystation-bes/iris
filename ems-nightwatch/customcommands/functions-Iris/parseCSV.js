/**
 * @summary Custom Command: Parse CSV
 * @since 7.3.5
 * @version 1.1
 * @author Mandy F
 */

/**
 * @description Parses CSV files into an array according to the rows (elements)
 * @param fileName - the name of the file to be uploaded with file extension included
 * @param directory - the directory of the file (usually __dirname)
 * @param callback - the callback function to return the CSV as an array of arrays of strings
 * @returns client
 */
 
exports.command = function (fileName, directory, callback) {
	var client = this;
	var fs = require('fs');
	var Baby = require('babyparse');
			
	fs.readFile(directory + "//" + fileName, function (err, data) {
		if (err) {
			throw err;
		}
		
		var textData = data.toString();
		var rows = Baby.parse(textData);
		
		client.pause(500, function () {
			callback(rows.data);
		});
	});
	
	return client;
};
