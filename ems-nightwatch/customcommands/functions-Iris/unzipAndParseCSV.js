/**
 * @summary Custom Command: Unzip File
 * @since 7.5.0
 * @version 1.0
 * @author Thomas C
 */

/**
 * @description Unzips a file and parses the CSV
 * @param fileName - the name of the zip file
 * @param directory - the directory of the zip file (usually __dirname)
 * @param callback - a callback function
 * @returns client
 */
 
exports.command = function (zipFileName, directory, callback) {
	var client = this;
	var unzip = require('unzip');
	var fs = require('fs');
	var Baby = require('babyparse');
	var fileName = "";
	
	client.pause(500, function () {

		fs.createReadStream(directory + "\\" + zipFileName).pipe(unzip.Parse()).on('entry', function (entry) {
		    fileName = entry.path;
		    entry.pipe(fs.createWriteStream(directory +'\\' +  fileName)).on('close', function(){
				console.log("File Successfully Unzipped: " + fileName);
				fs.readFile(directory + "\\" + fileName, function (err, data) {
					if (err) {
						console.log('Parse Error:')
						client.assert.fail(err, '', err, '');
					}
					console.log("File Successfully Parsed: " + fileName);
					var textData = data.toString();
					var rows = Baby.parse(textData);
					callback(fileName, rows.data);
				});
			});
		  });
	});

	return client;
};
