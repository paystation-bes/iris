/**
 * @summary Generate MEID for 18 digit CCID of a CDMA modem types
 * @since 7.3.6
 * @version 1.0
 * @author Nadine B
 * @see US-955
 **/

exports.command = function(modemType, ccid, carrier, callback) {
	var client = this;
	var data = require('../../data.js');
	
	var ModemCCID = {
		type : 'Modem',
		action : 'CCID',
		information: ccid
	}
	var ModemCarrier = {
		type : 'Modem',
		action : 'Carrier',
		information: carrier
	}
	var ModemAPN = {
		type : 'Modem',
		action : 'APN',
		information: ''
	}
	var ModemType = {
		type : 'Modem',
		action : 'Type',
		information: modemType
	}
			
	client
	//Event objects are packed into an array
	.sendEvent([ModemCCID, ModemCarrier, ModemAPN, ModemType])
	
	return client;
};