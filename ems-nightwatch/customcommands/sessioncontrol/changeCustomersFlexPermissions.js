/**
 * @summary Customer Admin: Citation widgets added and appear
 * @since 7.3.4
 * @version 1.0
 * @author Nadine B
 * @see US-897
 **/

exports.command = function(adminUsername, password, customer, customerName, flexOn, callback) {
	var data = require('../../data.js');

	var devurl = data("URL");
	
    var client = this;
    var customerSearchResults = "//*[@id='ui-id-2']/span[@class='info'][contains(text(),'";
    var flexCheckbox = "//*[@id='subscriptionList:1700']";
    var flexCheckboxChecked = "//*[@id='subscriptionList:1700' and contains(@class, 'checked')]";
    var updateCustomer = "//div[@class='ui-dialog-buttonset']/button/span[@class='ui-button-text'][contains(text(), 'Update Customer')]";
    
    
    //Log in as System Admin

	client
	.url(devurl)
	.windowMaximize('current')
	.login(adminUsername, password)
	.assert.title('Digital Iris - System Administration');

	//Searches up the Customer Admin

	client
	.useXpath()
	.waitForElementVisible("//*[@id='search']", 2000)
	.click("//*[@id='search']")
	.setValue("//*[@id='search']", customer)
	.waitForElementVisible("//*[@id='ui-id-2']/span[@class='info']", 2000)
	.waitForElementVisible(customerSearchResults + customerName + "')]", 4500)
	.click(customerSearchResults + customerName + "')]")
	.click("//*[@id='btnGo']")

	
	//Edit Customer's FLEX permissions depending on intent - flexOn
	
	client
	.useXpath()
	.pause(1000)
	.waitForElementVisible("//*[@id='btnEditCustomer']", 2000)
	.click("//*[@id='btnEditCustomer']")
		

	.pause(1000)
	.waitForElementVisible("//*[@id='sub-1700']", 2000)

	//Enable FLEX
	
	if(flexOn == true){
		client.elements('xpath', flexCheckboxChecked, function(result){
		console.log(result.value)
		
			if(result.value.length > 0) {
				client
				.click(flexCheckbox)
				.pause(1000)
				client.click(flexCheckbox)
				.pause(1000)
			}else{
				client
				.click(flexCheckbox)
				.pause(1000)
			}
		})
		
		client
		.useXpath()
		.pause(2000)
		.assert.elementPresent(flexCheckboxChecked)
		
	}else{
		
		//Disable FLEX
		
		client.elements('xpath', flexCheckboxChecked, function(result){
			console.log(result.value)
			if(result.value.length > 0) {
				client.click(flexCheckbox)
				.pause(1000)
			}
		})
		
		client
		.useXpath()
		.pause(2000)
		.assert.elementPresent(flexCheckbox)
		.assert.elementNotPresent(flexCheckboxChecked)
			
			
	}	

	
	//Save Customer changes

	client
	.useXpath()
	.pause(1000)
	.click(updateCustomer)
	.pause(1000)

	
	//Log out

	client
	.waitForElementVisible("//a[@title='Logout']", 3000)
    .click("//a[@title='Logout']")
    .pause(1000)

	
	return client;
};