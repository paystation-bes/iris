exports.command = function(username, password, callback) {
    var client = this;
	client
		.useCss()
        .waitForElementVisible('input[id=username]', 1000)
        .setValue('input[id=username]', username)
        .waitForElementVisible('input[id=password]', 1000)
        .setValue('input[id=password]', password)
        .waitForElementVisible('input[name=submit]', 1000)
        .click('#submit')
        .pause(1000);
	return client;
};
