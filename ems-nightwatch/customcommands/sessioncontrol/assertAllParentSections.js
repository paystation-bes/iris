/**
 * @summary Asserts that correct left hand sections are present for parent account
 * @since 7.3.5
 * @version 1.0
 * @author Thomas C
 **/
exports.command = function () {
	var client = this;
	client.useXpath()
	.assert.elementNotPresent("//a[@title='Maintenance']")
	.assert.elementNotPresent("//a[@title='Collections']")
	.assert.elementNotPresent("//a[@title='Card Management']")
	.assert.visible("//a[@title='Settings']")
	.assert.visible("//a[@title='Reports']")
	.assert.visible("//a[@title='Dashboard']")
	return client;
};
