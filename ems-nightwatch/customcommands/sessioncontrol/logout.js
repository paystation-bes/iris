exports.command = function(callback) {
    var client = this;
    client
        .useXpath()
        .waitForElementVisible("//a[@title='Logout']", 1000)
        .click("//a[@title='Logout']")
        .pause(1000)
        .end();
    return client;

};
