/**
 * @summary Asserts that all left navigation links are present
 * @since 7.3.5
 * @version 1.0
 * @author Thomas C
 * @param target - title of the page to navigate to 
 **/
exports.command = function (target) {
	var client = this;
	client
	.assert.visible("//a[@title='Global']")
	.assert.visible("//a[@title='Rates']")
	.assert.visible("//a[@title='Locations']")
	.assert.visible("//a[@title='Pay Stations']")
	.assert.visible("//a[@title='Mobile Devices']")
	.assert.visible("//a[@title='Routes']")
	.assert.visible("//a[@title='Alerts']")
	.assert.visible("//a[@title='Users']")
	.assert.visible("//a[@title='Card Settings']")
	return client;
};
