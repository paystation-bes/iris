/**
 * @summary Customer Admin: Citation widgets added and appear
 * @since 7.3.4
 * @version 1.0
 * @author Nadine B
 * @see US-897
 **/

exports.command = function(widgetXpath, widgetName, callback) {
    var client = this;
    var addWidgetButton = "//section[@class='layout5 layout']/section[@class='sectionTools'][1]/section/a[@class='linkButtonIcn addWidget']";
    var citationWidgetOption = "//section[@class='widgetListContainer']/h3[contains(text(), 'Citations')]";
 
	client
		.useXpath()
		.pause(1000)
		.waitForElementVisible(addWidgetButton, 2000)
		.click(addWidgetButton)
			
		.pause(1000)
		.waitForElementVisible(citationWidgetOption, 2000)
		.click(citationWidgetOption)
			
		.pause(1000)
		.click(widgetXpath + " '" + widgetName + "')]")
			
		.pause(500)
		.click("//div[@class='ui-dialog-buttonset']/button/span[contains(text(), 'Add widget')]")
        .pause(1000);
	
	return client;
};