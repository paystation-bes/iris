/**
 * @summary Custom Command: Upload file
 * @since 7.3.4
 * @version 1.0
 * @author Mandy F
 */

/**
 * @description Uploads files onto the site for the import options
 * @param fileName - the name of the file to be uploaded with file extension included
 * @param directory - the directory of the file (usually __dirname)
 * @returns client
 */
exports.command = function (fileName, directory) {
	var client = this;

	client
	.useXpath()
	.waitForElementVisible("//input[@type='file']", 1000)
	.setValue("//input[@type='file']", directory + "\\" + fileName)
	.pause(2000);

	return client;
};
