/**
 * @summary Generate MEID for 18 digit CCID of a CDMA modem types
 * @since 7.3.6
 * @version 1.0
 * @author Nadine B
 * @see US-955
 **/

exports.command = function(adminUsername, password, customer, customerName, username, modemType, ccid, callback) {
	var client = this;
	var data = require('../../data.js');

	var devurl = data("URL");
	var paystationCommAddress = data("PaystationCommAddress");
	

	/**
	 * @description Sends all required events for this script
	 * @param browser - The web browser object
	 * @returns void
	 **/
	console.log("runPaystationEvent: " + modemType) 
		
	client.createPaystationEvent(modemType, ccid, 'TELUS');

	
	/**
	 * @description Log into customer's account and go to Paystation Information tab
	 * @param browser - The web browser object
	 * @returns void
	 **/

	client.navigateToPaystationInfo(username, password, modemType);

	
	/**
	*@description Assert modem information is present and calculate MEID and verify that MEID matches the one displayed
	*@param browser - The web browser object
	*@returns void
	**/
	
	client.verifyPaystationInfo(modemType, ccid);
	
	
	/**
	*@description Log out
	*@param browser - The web browser object
	*@returns void
	**/

	client
	.useXpath()
    .waitForElementVisible("//a[@title='Logout']", 1000)
    .click("//a[@title='Logout']")
    .pause(1000)

	
	/**
	*@description Navigate to Paystation Information tab for a customer from System Admin side
	*@param browser - The web browser object
	*@returns void
	**/

	client.navigateToCustomerPaystationInfoFromSystemAdmin(adminUsername, password, customer, customerName, modemType);
	
	client.verifyPaystationInfo(modemType, ccid);	
	
	
	/**
	*@description Log out
	*@param browser - The web browser object
	*@returns void
	**/

	client
	.useXpath()
    .waitForElementVisible("//a[@title='Logout']", 1000)
    .click("//a[@title='Logout']")
    .pause(1000)

	
	
	return client;
};