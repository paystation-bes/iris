/**
 * @description Enters the Name, Email and Password for the user
 * @param firstName - The first name of the user
 * @param lastName - The last name of the user
 * @param email - The email name of the user
 * @param password2 - The default password
 * @param enabled - Is the account activated - boolean
 * @returns void
 */

exports.command = function (firstName, lastName, email, password2, enabled) {
	var client = this;
	var firstNameIn = "//input[@id='formUserFirstName']";
	var lastNameIn = "//input[@id='formUserLastName']";
	var emailIn = "//input[@id='formUserName']";
	var newPassIn = "//input[@id='newPassword']";
	var confirmPassIn = "//input[@id='c_newPassword']";

	client
	.useXpath()
	.waitForElementVisible(firstNameIn, 4000)
	.setValue(firstNameIn, firstName)
	.pause(250)
	.setValue(lastNameIn, lastName)
	.pause(250)
	.setValue(emailIn, email)
	.pause(250)
	.setValue(newPassIn, password2)
	.pause(250)
	.setValue(confirmPassIn, password2)
	.pause(250)
	//enabled check box
	if (enabled) {
		client.click("//a[@id='statusCheck']");
	}
	return client;
};
