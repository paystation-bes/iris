/**
 * @summary Sends an Audit to Iris and asserts the response
 * @since 7.3.6
 * @version 1.0
 * @author Thomas C,
 **/
var data = require('../../data.js');

var request;
try {
	request = require('../../node_modules/request');
} catch (error) {
	request = require('request');
}
var devurl = "https://qa3.digitalpaytech.com";
var paystationCommAddress = data("PaystationCommAddress");


exports.command = function () {
	client = this;
	var statusCode;
	var date = new Date();
	//Gen random messageNum <= 1000
	var messageNum = Math.floor(Math.random() * 100) + 1;
	//converts date to GMT
	var gmtDate = new Date(date.valueOf() + date.getTimezoneOffset() * 60000);
	var gmtDateString = gmtDate.getFullYear() + ":" + ('0' + (gmtDate.getMonth() + 1)).slice(-2) + ":" + ('0' + gmtDate.getDate()).slice(-2) + ':' + ('0' + gmtDate.getHours()).slice(-2) + ":" + ('0' + gmtDate.getMinutes()).slice(-2) + ":" + ('0' + gmtDate.getSeconds()).slice(-2) + ":GMT";

	var body = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><methodCall><methodName>XMLRPCRequestHandler.processXML</methodName><params><param><value><?xml version=\"1.0\"?><Paystation_2 Password=\"zaq123$ESZxsw234%RDX\" UserId=\"emsadmin\" Version=\"6.3 build 0013\"><Message4EMS>";
	var footer = "</Message4EMS></Paystation_2></value></param></params></methodCall>";

	body += "<Audit MessageNumber=\"" + (messageNum) + "\">";
	body += "<PaystationCommAddress>" + paystationCommAddress + "</PaystationCommAddress>";
	body += "<ReportNumber>" + (messageNum) + "</ReportNumber>";
	body += "<LotNumber>Student Parking</LotNumber>";
	body += "<MachineNumber>4</MachineNumber>";
	body += "<StartDate>"+ gmtDateString +"</StartDate>";
	body += "<EndDate>" + gmtDateString + "</EndDate>";
	body += "<PatrollerTicketsSold>0</PatrollerTicketsSold>";
	body += "<TicketsSold>5</TicketsSold>";
	body += "<CoinAmount>10000.00</CoinAmount>";
	body += "<OnesAmount>1000.00</OnesAmount>";
	body += "<TwoesAmount>1000.00</TwoesAmount>";
	body += "<FivesAmount>2000.00</FivesAmount>";
	body += "<TensAmount>5000.00</TensAmount>";
	body += "<TwentiesAmount>1000.00</TwentiesAmount>";
	body += "<FiftiesAmount>2000.00</FiftiesAmount>";
	body += "<SmartCardRechargeAmount>0</SmartCardRechargeAmount>";
	body += "<CitationsPaid>0</CitationsPaid>";
	body += "<CitationAmount>0</CitationAmount>";
	body += "<ChangeIssued>2.00</ChangeIssued>";
	body += "<RefundIssued>3.00</RefundIssued>";
	body += "<ExcessPayment>0.00</ExcessPayment>";
	body += "<NextTicketNumber>33</NextTicketNumber>";
	body += "<AttendantTicketsSold>0</AttendantTicketsSold>";
	body += "<AttentantTicketsAmount>0</AttentantTicketsAmount>";
	body += "<AttendantDepositAmount>0</AttendantDepositAmount>";
	body += "<AcceptedFloatAmount>0</AcceptedFloatAmount>";
	body += "<CurrentTubeStatus>0_0:0_0:0_0:0_0</CurrentTubeStatus>";
	body += "<OverfillAmount>0</OverfillAmount>";
	body += "<ReplenishedAmount>0</ReplenishedAmount>";
	body += "<HopperInfo>0:0_0:0_0:0_0:0</HopperInfo>";
	body += "<TestDispensed>0:0:0</TestDispensed> <CardAmounts>";
	body += "<CardType>Other</CardType>";
	body += "<CardAmount>2.2</CardAmount>";
	body += "<CardType>Visa</CardType>";
	body += "<CardAmount>1.23</CardAmount>";
	body += "<CardType>Amex</CardType>";
	body += "<CardAmount>1.23</CardAmount>";
	body += "</CardAmounts>";
	body += "</Audit>";

	body += footer;

	console.log(body);
	
	var postRequest = {
		uri : devurl + '/XMLRPC_PS2',
		method : "POST",
		agent : false,
		timeout : 1000,
		headers : {
			'content-type' : 'text/xml',
			'content-length' : Buffer.byteLength(body, [''])
		}
	};

	var req = request.post(postRequest, function (err, httpResponse, body) {
			
				var response = body.replace("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><methodResponse><params><param><value><base64>", "");
				response = response.replace("</base64></value></param></params></methodResponse>", "");
				result = new Buffer(response, 'base64').toString('ascii');
				console.log("Response : " + result)
				client.assert.equal(httpResponse.statusCode, 200, "Status Code Correct");
				client.assert.equal(result.indexOf("Acknowledge") > -1, true, "Message accepted");

			
		});

	client.pause(2000)
	req.write(body);
	req.end();

};
