exports.command = function() {
	var overlaySelector = 'div.ui-widget-overlay';
	return this
		.useCss()
        .pause(1000)
        .waitForElementNotPresent(overlaySelector, 10000);
};