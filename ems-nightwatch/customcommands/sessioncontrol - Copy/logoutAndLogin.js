/**
 * @summary Integrate with Counts in the Cloud
 * @since 7.3.4
 * @version 1.0
 * @author Nadine B
 * @see US-898
 **/

exports.command = function(username, password, callback) {
    var client = this;
	var devurl = "https://dev.digitalpaytech.com:8447/";
//	var devurl = "http://localhost:80/";
	
    client
        .useXpath()
        .waitForElementVisible("//a[@title='Logout']", 1000)
        .click("//a[@title='Logout']")
        .pause(1000)
        
        .login(username, password);
    
    return client;

};