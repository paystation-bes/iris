exports.command = function(cssSelector) {
	return this
		.useCss()
		.waitForElementVisible(cssSelector, 7000)
		.execute("document.querySelector('" + cssSelector + "').click()")
		.pause(500);
};