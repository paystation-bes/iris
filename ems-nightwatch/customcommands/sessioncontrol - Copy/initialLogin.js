/**
 * @summary Attempts to Log the user into Iris, if needed logs in with default password and changes the password
 * @since 7.3.5
 * @version 1.1
 * @author Thomas C,
 * @param username - The username to log into
 * @param pass - The password to try first, if not logged in logs in with defaultPass and changes password to pass value
 * @param defaultPass - The  default password
 * @returns void
 **/
exports.command = function (username, pass, defaultPass) {
	var client = this;
	client
	.login(username, pass)
	.pause(1000)
	.getTitle(function (result) {
		if (result != "Digital Iris - Main (Dashboard)") {
			client
			.back()
			
			.login(username, defaultPass)
			.pause(2000)

			.useXpath()

			.waitForElementPresent("//input[@id='newPassword']", 100000)
			.setValue("//input[@id='newPassword' and @class='left toolTip reqField']", pass)
			.pause(1000)

			.waitForElementVisible("//input[@id='c_newPassword' and @class='left reqField']", 10000)
			.setValue("//input[@id='c_newPassword' and @class='left reqField']", pass)
			.pause(1000)

			.waitForElementVisible("//input[@id='changePassword' and @class='linkButtonFtr textButton']", 10000)
			.click("//input[@id='changePassword' and @class='linkButtonFtr textButton']")
			.pause(1000)

			.assert.title('Digital Iris - Main (Dashboard)');
		}
	});

	return client;
};
