exports.command = function() {
	var overlaySelector = 'div.ui-widget-overlay';
	return this
		.useCss()
        .waitForElementPresent("section.flexTestIcon.success,section.flexTestIcon.failed", 1000000)
        .waitForElementNotPresent(overlaySelector, 10000);
};