package com.digitalpaytech.util;

import java.util.Map;
import java.util.HashMap;

public final class TelemetryUtil {
    
    private static final Map<Integer, String> UPGRADE_PROGRESS_MAP;
    
    static {
        UPGRADE_PROGRESS_MAP = new HashMap<Integer, String>(StandardConstants.CONSTANT_12);
        // UPGRADE_PROGRESS_INIT = 0
        UPGRADE_PROGRESS_MAP.put(StandardConstants.CONSTANT_0, "upgrade.progress.init");
        // UPGRADE_PROGRESS_SENT_UPGRADE_NOTIFICATION = 1
        UPGRADE_PROGRESS_MAP.put(StandardConstants.CONSTANT_1, "upgrade.progress.sent.upgrade.notification");
        // UPGRADE_PROGRESS_RECEIVED_UPGRADE_DETAILS = 2
        UPGRADE_PROGRESS_MAP.put(StandardConstants.CONSTANT_2, "upgrade.progress.received.upgrade.details");
        // UPGRADE_PROGRESS_RESCHEDULE = 3
        UPGRADE_PROGRESS_MAP.put(StandardConstants.CONSTANT_3, "upgrade.progress.reschedule");
        // UPGRADE_PROGRESS_RECEIVED_DOWNLOAD_STARTED = 4
        UPGRADE_PROGRESS_MAP.put(StandardConstants.CONSTANT_4, "upgrade.progress.received.download.started");
        // UPGRADE_PROGRESS_RECEIVED_DOWNLOAD_COMPLETE = 5
        UPGRADE_PROGRESS_MAP.put(StandardConstants.CONSTANT_5, "upgrade.progress.received.download.complete");
        // UPGRADE_PROGRESS_RECEIVED_UPGRADE_COMPLETE = 6
        UPGRADE_PROGRESS_MAP.put(StandardConstants.CONSTANT_6, "upgrade.progress.received.upgrade.complete");
        // UPGRADE_PROGRESS_ERROR = 7
        UPGRADE_PROGRESS_MAP.put(StandardConstants.CONSTANT_7, "upgrade.progress.error");
        // UPGRADE_PROGRESS_CANCELLED_PENDING = 8
        UPGRADE_PROGRESS_MAP.put(StandardConstants.CONSTANT_8, "upgrade.progress.cancelled.pending");
        // UPGRADE_PROGRESS_CANCELLED_REMOTE = 9
        UPGRADE_PROGRESS_MAP.put(StandardConstants.CONSTANT_9, "upgrade.progress.cancelled.remote");
        // UPGRADE_PROGRESS_CANCELLED_MANUAL = 10
        UPGRADE_PROGRESS_MAP.put(StandardConstants.CONSTANT_10, "upgrade.progress.cancelled.manual");        
        // UPGRADE_PROGRESS_NOT_STARTED = 999
        UPGRADE_PROGRESS_MAP.put(StandardConstants.CONSTANT_999, "upgrade.progress.not.started");
    }
    
    private TelemetryUtil() {
    }
    
    public static String getUpgradeProgressMessageKey(final int upgradeProgressKey) {
        return UPGRADE_PROGRESS_MAP.get(upgradeProgressKey);
    }
}
