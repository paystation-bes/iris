package com.digitalpaytech.util;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

public abstract class CommonValidator<MESSAGE_CONTAINER> {
    private static final int BASE_REGEX_MESSAGE_PARAMS_CNT = 3;
    
    public abstract String getMessage(String messageKey, Object... args);
    
    protected abstract void addError(MESSAGE_CONTAINER messageContainer, String fieldName, String message);
    
    /**
     * Validate whether the value is within the set of allowed values.
     * This method also travel through Object structure as deep as the specified "depth".
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (javascript representation).
     * @param fieldLabelKey
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @param depth
     *            The depth of Object structure this method will visit.
     * @param disallowedValues
     *            Optional parameter that lists id values that are invalid
     * @return The "value" Object. In case that value is a String, it will be trimmed. Returns null if validation fail.
     */
    @SuppressWarnings("unchecked")
    private <T> Collection<T> validateAllValuesRequired(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabelKey,
        final Collection<T> values, final int depth, final Object... disallowedValues) {
        Collection<T> result = values;
        if (result != null) {
            final Iterator<T> itr = values.iterator();
            if (depth <= 0) {
                if (!itr.hasNext()) {
                    result = null;
                }
            } else {
                final int newDepth = depth - 1;
                while ((result != null) && (itr.hasNext())) {
                    Object buffer = itr.next();
                    if (buffer instanceof Collection) {
                        buffer = validateAllValuesRequired(messageContainer, fieldName, fieldLabelKey, (Collection<Object>) buffer, newDepth,
                                                           disallowedValues);
                    } else {
                        buffer = validateRequired(messageContainer, fieldName, fieldLabelKey, buffer, disallowedValues);
                    }
                    
                    if (buffer == null) {
                        result = null;
                    }
                }
            }
        }
        
        return result;
    }
    
    /**
     * Validate whether the value is within the set of allowed values.
     * This method simply invoke {@link CommonValidator#validateRequired(Object, String, String, Object, int, Object...)} with depth = 0.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (javascript representation).
     * @param fieldLabelKey
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @param disallowedValues
     *            Optional parameter that lists id values that are invalid
     * @return The "value" Object. In case that value is a String, it will be trimmed. Returns null if validation fail.
     */
    @SuppressWarnings("unchecked")
    protected final <T> T validateRequired(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabelKey,
        final T value, final Object... disallowedValues) {
        T result = value;
        if (result != null) {
            if (result instanceof Collection) {
                return (T) validateAllValuesRequired(messageContainer, fieldName, fieldLabelKey, (Collection<Object>) value, 0, disallowedValues);
            }
            
            if (result instanceof String) {
                String buffer = (String) result;
                buffer = buffer.trim();
                if (buffer.length() > 0) {
                    result = (T) buffer;
                } else {
                    result = null;
                }
            }
            
            int i = disallowedValues.length;
            while ((result != null) && (--i >= 0)) {
                if (value.equals(disallowedValues[i])) {
                    result = null;
                }
            }
        }
        
        if (result == null) {
            addError(messageContainer, fieldName, this.getMessage(ErrorConstants.REQUIRED, this.getMessage(fieldLabelKey)));
        }
        
        return result;
    }
    
    /**
     * Validate whether the input field is a positive integer
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabelKey
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @return The int value. Returns -1 if validation fail.
     */
    protected final int validatePositiveInteger(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabelKey,
        final String value) {
        return validateIntegerLimits(messageContainer, fieldName, fieldLabelKey, value, 0, Integer.MAX_VALUE);
    }
    
    /**
     * Validate whether the data field has been filled by testing for null and empty string (trimmed).
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabelKey
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @return The int value. Returns -1 if validation fail.
     */
    protected final int validateIntegerLimits(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabelKey,
        final String value, final int min, final int max) {
        if (StringUtils.isBlank(value)) {
            addError(messageContainer, fieldName, this.getMessage(ErrorConstants.REQUIRED, this.getMessage(fieldLabelKey)));
            return -1;
        }
        
        int intValue = -1;
        try {
            intValue = Integer.parseInt(value);
        } catch (NumberFormatException nfe) {
            if (!value.matches(WebCoreConstants.REGEX_INTEGER)) {
                addError(messageContainer, fieldName, this.getMessage(ErrorConstants.INVALID_NUMERIC, this.getMessage(fieldLabelKey)));
            } else {
                addError(messageContainer, fieldName, this.getMessage(ErrorConstants.OVERFLOW_MAX, this.getMessage(fieldLabelKey), max));
            }
            
            return -1;
        }
        
        return validateIntegerLimits(messageContainer, fieldName, fieldLabelKey, intValue, min, max);
    }
    
    /**
     * Validate whether the data field has been filled by testing for null and empty string (trimmed).
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabelKey
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @return The int value. Returns -1 if validation fail.
     */
    protected final int validateIntegerLimits(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabelKey,
        final int value, final int min, final int max) {
        if (value < min) {
            addError(messageContainer, fieldName, this.getMessage(ErrorConstants.UNDERFLOW_MIN, this.getMessage(fieldLabelKey), min));
        } else if (value > max) {
            addError(messageContainer, fieldName, this.getMessage(ErrorConstants.OVERFLOW_MAX, this.getMessage(fieldLabelKey), max));
        }
        
        return value;
    }
    
    protected final Date validateDateLimits(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabelKey,
        final Date value, final Date min, final Date max, final String minLabel, final String maxLabel) {
        Date result = value;
        if (result != null) {
            if (max == null) {
                if ((min != null) && (result.before(min))) {
                    result = null;
                    addError(messageContainer, fieldName,
                             this.getMessage(ErrorConstants.UNDERFLOW_DATE_MIN, this.getMessage(fieldLabelKey), minLabel));
                }
            } else if (min == null) {
                if (result.after(max)) {
                    result = null;
                    addError(messageContainer, fieldName, this.getMessage(ErrorConstants.OVERFLOW_DATE_MAX, this.getMessage(fieldLabelKey), maxLabel));
                }
            } else {
                if (result.before(min) || result.after(max)) {
                    result = null;
                    addError(messageContainer, fieldName,
                             this.getMessage(ErrorConstants.OUT_OF_RANGE, this.getMessage(fieldLabelKey), minLabel, maxLabel));
                }
            }
        }
        
        return result;
    }
    
    /**
     * Validate whether the data field fits within the specified limits
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabelKey
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @param min
     *            The minimum value in the comparison range
     * @param max
     *            The maximum value in the comparison range
     * 
     * @return The float value. Returns -1 if validation fail.
     */
    protected final double validateDoubleLimits(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabelKey,
        final String value, final double min, final double max) {
        if (StringUtils.isBlank(value)) {
            addError(messageContainer, fieldName, this.getMessage(ErrorConstants.REQUIRED, this.getMessage(fieldLabelKey)));
            return -1;
        }
        
        double dblValue = -1;
        try {
            dblValue = Double.parseDouble(value);
        } catch (NumberFormatException nfe) {
            addError(messageContainer, fieldName, this.getMessage(ErrorConstants.INVALID_NUMERIC, this.getMessage(fieldLabelKey)));
            return -1;
        }
        
        return validateDoubleLimits(messageContainer, fieldName, fieldLabelKey, dblValue, min, max);
    }
    
    /**
     * Validate whether the data field fits within the specified limits
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabelKey
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @param min
     *            The minimum value in the comparison range
     * @param max
     *            The maximum value in the comparison range
     * @return The double value. Returns -1 if validation fail.
     */
    protected final double validateDoubleLimits(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabelKey,
        final double value, final double min, final double max) {
        if (value < min) {
            addError(messageContainer, fieldName, this.getMessage(ErrorConstants.UNDERFLOW_MIN, this.getMessage(fieldLabelKey), min));
        } else if (value > max) {
            addError(messageContainer, fieldName, this.getMessage(ErrorConstants.OVERFLOW_MAX, this.getMessage(fieldLabelKey), max));
        }
        
        return value;
    }
    
    /**
     * Validate whether the data field is in correct date format defined by {@link WebCoreConstants#DATE_UI_FORMAT}.
     * If the data field is in correct format, the Date Object represent the field value will be returned.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabelKey
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @return {@link java.util.Date} Object created from it string representation. Returns null if validation fail.
     */
    protected final Date validateDate(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabelKey,
        final String value, final TimeZone timeZone) {
        Date result = null;
        if (value != null) {
            final String dateStr = value.trim();
            if (dateStr.length() > 0) {
                if (!dateStr.matches(WebCoreConstants.REGEX_DATE)) {
                    addError(messageContainer,
                             fieldName,
                             this.getMessage(ErrorConstants.INVALID_DATE, this.getMessage(fieldLabelKey),
                                             this.getMessage(ErrorConstants.PATTERN_DATE)));
                } else {
                    final PooledResource<DateFormat> format = DateUtil.takeDateFormat(WebCoreConstants.DATE_UI_FORMAT, timeZone);
                    try {
                        result = format.get().parse(value);
                        //                        result = new Date((long) (Math.floor(result.getTime() / 86400000L) * 86400000L));
                    } catch (ParseException pe) {
                        addError(messageContainer,
                                 fieldName,
                                 this.getMessage(ErrorConstants.INVALID_DATE, this.getMessage(fieldLabelKey),
                                                 this.getMessage(ErrorConstants.PATTERN_DATE)));
                    } finally {
                        format.close();
                    }
                }
            }
        }
        
        return result;
    }
    
    /**
     * Validate whether the data field is in correct time format defined by {@link WebCoreConstants#TIME_UI_FORMAT}.
     * If the data field is in correct format, the Date Object represent the field value will be returned.
     * 
     * *Note that this Date Object will only containing the time-part of it. The date-part will be 1st January 1970.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabelKey
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @param timeZone
     *            TimeZone for this validation. This timeZone will also be used to calculate the correct timestamp from the time string.
     * @return {@link java.util.Date} Object, with the date-part set to 1st January 1970, created from it string representation. Returns null if validation
     *         fail.
     */
    protected Date validateTime(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabelKey, final String value,
        final TimeZone timeZone) {
        Date result = null;
        if (value != null) {
            final String timeStr = value.trim();
            if (timeStr.length() > 0) {
                if (!timeStr.matches(WebCoreConstants.REGEX_TIME)) {
                    addError(messageContainer,
                             fieldName,
                             this.getMessage(ErrorConstants.INVALID_TIME, this.getMessage(fieldLabelKey),
                                             this.getMessage(ErrorConstants.PATTERN_TIME)));
                } else {
                    final PooledResource<DateFormat> format = DateUtil.takeDateFormat(WebCoreConstants.TIME_UI_FORMAT, timeZone);
                    try {
                        result = format.get().parse(timeStr);
                    } catch (ParseException pe) {
                        addError(messageContainer,
                                 fieldName,
                                 this.getMessage(ErrorConstants.INVALID_TIME, this.getMessage(fieldLabelKey),
                                                 this.getMessage(ErrorConstants.PATTERN_TIME)));
                    } finally {
                        format.close();
                    }
                }
            }
        }
        
        return result;
    }
    
    /**
     * Validate whether the data field is in correct date-time format defined. The date-part format is defined by {@link WebCoreConstants#DATE_UI_FORMAT}.
     * The time-part format is defined by {@link WebCoreConstants#TIME_UI_FORMAT}.
     * If the data field is in correct format, the Date Object represent the field value will be returned.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param dateFieldName
     *            Name of the validating date-part field (java representation).
     * @param dateFieldLabel
     *            Message Key for retrieving the actual date-part field name that displayed to the user.
     * @param dateValue
     *            The value of the date-part field.
     * @param timeFieldName
     *            Name of the validating time-part field (java representation).
     * @param timeFieldLabel
     *            Message Key for retrieving the actual time-part field name that displayed to the user.
     * @param timeValue
     *            The value of the time-part field.
     * @param timeZone
     *            TimeZone for this validation. This timeZone will also be used to calculate the correct timestamp from the time string.
     * @return {@link java.util.Date} Object created from it string representation. Returns null if validation fail.
     */
    protected final Date validateDateTime(final MESSAGE_CONTAINER messageContainer, final String dateFieldName, final String dateFieldLabel,
        final String dateValue, final String timeFieldName, final String timeFieldLabel, final String timeValue, final TimeZone timeZone) {
        Date result = null;
        
        final String dateStr = (dateValue == null) ? "" : dateValue.trim();
        final String timeStr = (timeValue == null) ? "" : timeValue.trim();
        if ((dateStr.length() > 0) && (timeStr.length() > 0)) {
            boolean validDate = dateStr.matches(WebCoreConstants.REGEX_DATE);
            boolean validTime = timeStr.matches(WebCoreConstants.REGEX_TIME);
            if (validDate && validTime) {
                final PooledResource<DateFormat> format = DateUtil.takeDateFormat(WebCoreConstants.DATE_UI_FORMAT + WebCoreConstants.EMPTY_SPACE
                                                                                  + WebCoreConstants.TIME_UI_FORMAT, timeZone);
                try {
                    result = format.get().parse(dateStr + WebCoreConstants.EMPTY_SPACE + timeStr);
                } catch (ParseException pe) {
                    validDate = false;
                    validTime = false;
                } finally {
                    format.close();
                }
            }
            
            if (!validDate || !validTime) {
                addDateTimeValidationErrors(messageContainer, dateFieldName, dateFieldLabel, validDate, timeFieldName, timeFieldLabel, validTime);
            }
        }
        
        return result;
    }
    
    protected final Date validateReportsDateTime(final MESSAGE_CONTAINER messageContainer, final String dateFieldName, final String dateFieldLabel,
        final String dateValue, final String timeFieldName, final String timeFieldLabel, final String timeValue, final TimeZone timeZone) {
        Date result = null;
        
        final String dateStr = (dateValue == null) ? "" : dateValue.trim();
        final String timeStr = (timeValue == null) ? "" : timeValue.trim();
        if ((dateStr.length() > 0) && (timeStr.length() > 0)) {
            boolean validDate = dateStr.matches(WebCoreConstants.REGEX_DATE);
            boolean validTime = timeStr.matches(WebCoreConstants.REGEX_REPORTS_TIME);
            if (validDate && validTime) {
                final PooledResource<DateFormat> format = DateUtil.takeDateFormat(WebCoreConstants.DATE_UI_FORMAT + WebCoreConstants.EMPTY_SPACE
                                                                                  + WebCoreConstants.TIME_REPORTING_FORMAT, timeZone);
                try {
                    result = format.get().parse(dateStr + WebCoreConstants.EMPTY_SPACE + timeStr);
                } catch (ParseException pe) {
                    validDate = false;
                    validTime = false;
                } finally {
                    format.close();
                }
            }
            
            if (!validDate || !validTime) {
                addReportDateTimeValidationErrors(messageContainer, dateFieldName, dateFieldLabel, validDate, timeFieldName, timeFieldLabel, validTime);
            }
        }
        
        return result;
    }
    
    private void addReportDateTimeValidationErrors(final MESSAGE_CONTAINER messageContainer, final String dateFieldName, final String dateFieldLabel,
        final boolean validDate, final String timeFieldName, final String timeFieldLabel, final boolean validTime) {
        if (!dateFieldLabel.equals(timeFieldLabel)) {
            if (!validDate) {
                addError(messageContainer, dateFieldName,
                         this.getMessage(ErrorConstants.INVALID_DATE, this.getMessage(dateFieldLabel), this.getMessage(ErrorConstants.PATTERN_DATE)));
            }
            if (!validTime) {
                addError(messageContainer, timeFieldName,
                         this.getMessage(ErrorConstants.INVALID_TIME, this.getMessage(timeFieldLabel), this.getMessage(ErrorConstants.PATTERN_REPORT_TIME)));
            }
        } else {
            String invalidField = dateFieldName;
            if (validDate) {
                invalidField = timeFieldName;
            } else if (!validTime) {
                invalidField = invalidField + StandardConstants.STRING_COMMA + timeFieldName;
            }
            
            addError(messageContainer,
                     invalidField,
                     this.getMessage(ErrorConstants.INVALID_DATETIME, this.getMessage(dateFieldLabel),
                                     this.getMessage(ErrorConstants.PATTERN_DATETIME)));
        }
    }
    
    private void addDateTimeValidationErrors(final MESSAGE_CONTAINER messageContainer, final String dateFieldName, final String dateFieldLabel,
        final boolean validDate, final String timeFieldName, final String timeFieldLabel, final boolean validTime) {
        if (!dateFieldLabel.equals(timeFieldLabel)) {
            if (!validDate) {
                addError(messageContainer, dateFieldName,
                         this.getMessage(ErrorConstants.INVALID_DATE, this.getMessage(dateFieldLabel), this.getMessage(ErrorConstants.PATTERN_DATE)));
            }
            if (!validTime) {
                addError(messageContainer, timeFieldName,
                         this.getMessage(ErrorConstants.INVALID_TIME, this.getMessage(timeFieldLabel), this.getMessage(ErrorConstants.PATTERN_TIME)));
            }
        } else {
            String invalidField = dateFieldName;
            if (validDate) {
                invalidField = timeFieldName;
            } else if (!validTime) {
                invalidField = invalidField + StandardConstants.STRING_COMMA + timeFieldName;
            }
            
            addError(messageContainer,
                     invalidField,
                     this.getMessage(ErrorConstants.INVALID_DATETIME, this.getMessage(dateFieldLabel),
                                     this.getMessage(ErrorConstants.PATTERN_DATETIME)));
        }
    }
    
    /**
     * Validate whether the data String has length with in the limit of "minLength" and "maxLength".
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabelKey
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @param minLength
     *            minimum length of the field's value.
     * @param maxLength
     *            maximum length of the field's value.
     * @return Trimmed String. Returns null if validation fail.
     */
    protected final String validateStringLength(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabelKey,
        final String value, final int minLength, final int maxLength) {
        int actualMinLength;
        int actualMaxLength;
        if ((maxLength > 0) && (minLength > maxLength)) {
            actualMinLength = maxLength;
            actualMaxLength = minLength;
        } else {
            actualMinLength = minLength;
            actualMaxLength = maxLength;
        }
        
        String result = null;
        if (actualMinLength > 0) {
            if (actualMaxLength <= 0) {
                result = exactStringLength(messageContainer, fieldName, fieldLabelKey, value, actualMinLength,
                                           ErrorConstants.LENGTH_MISMATCHED_STRING);
            } else {
                result = rangeStringLength(messageContainer, fieldName, fieldLabelKey, value, actualMinLength, actualMaxLength,
                                           ErrorConstants.OUT_OF_BOUND_STRING);
            }
        }
        
        return result;
    }
    
    /**
     * Validate whether the number has length with in the limit of "minLength" and "maxLength".
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabelKey
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @param minLength
     *            minimum length of the field's value.
     * @param maxLength
     *            maximum length of the field's value.
     * @return Trimmed String. Returns null if validation fail.
     */
    protected final String validateNumberStringLength(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabelKey,
        final String value, final int minLength, final int maxLength) {
        int actualMinLength;
        int actualMaxLength;
        if ((maxLength > 0) && (minLength > maxLength)) {
            actualMinLength = maxLength;
            actualMaxLength = minLength;
        } else {
            actualMinLength = minLength;
            actualMaxLength = maxLength;
        }
        
        String result = null;
        if (actualMinLength > 0) {
            if (actualMaxLength <= 0) {
                result = exactStringLength(messageContainer, fieldName, fieldLabelKey, value, actualMinLength,
                                           ErrorConstants.LENGTH_MISMATCHED_DIGITS);
            } else {
                result = rangeStringLength(messageContainer, fieldName, fieldLabelKey, value, actualMinLength, actualMaxLength,
                                           ErrorConstants.OUT_OF_BOUND_DIGITS);
            }
        }
        
        return result;
    }
    
    private String exactStringLength(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabelKey,
        final String value, final int length, final String messageKey) {
        String result = StringUtils.stripToNull(value);
        if ((result != null) && (result.length() != length)) {
            addError(messageContainer, fieldName, this.getMessage(messageKey, this.getMessage(fieldLabelKey), length));
            result = null;
        }
        
        return result;
    }
    
    private String rangeStringLength(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabelKey,
        final String value, final int minLength, final int maxLength, final String messageKey) {
        String result = StringUtils.stripToNull(value);
        if ((result != null) && ((result.length() < minLength) || (result.length() > maxLength))) {
            addError(messageContainer, fieldName, this.getMessage(messageKey, this.getMessage(fieldLabelKey), minLength, maxLength));
            result = null;
        }
        
        return result;
    }
    
    /**
     * Validate whether the data String has the exact length.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabelKey
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @param length
     *            expecting length of the field's value.
     * @return Trimmed String. Returns null if validation fail.
     */
    protected final String validateStringLength(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabelKey,
        final String value, final int length) {
        return validateStringLength(messageContainer, fieldName, fieldLabelKey, value, length, -1);
    }
    
    /**
     * Validate whether every value in the collection is within the specified limits for length.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabelKey
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param values
     *            The values of the collection.
     * @param disallowedValues
     *            Optional parameter that lists id values that are invalid
     * @return The "values" Collection<String>. In case that value is a String, it will be trimmed. Returns null if validation fail.
     */
    protected final Collection<String> validateAllLengths(final MESSAGE_CONTAINER messageContainer, final String fieldName,
        final String fieldLabelKey, final Collection<String> values, final int minLength, final int maxLength) {
        final Collection<String> valuesCol = new ArrayList<String>();
        for (String value : values) {
            final String result = validateStringLength(messageContainer, fieldName, fieldLabelKey, value, minLength, maxLength);
            if (result == null) {
                return null;
            } else {
                valuesCol.add(result);
            }
        }
        
        return values;
    }
    
    /**
     * Validate whether the "lesser-field" is really less than or equals to "greater-field".
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param lesserFieldName
     *            Name of the validating lesser-field (java representation), the field that its' value should be less than greater-field.
     * @param lesserFieldLabel
     *            Message Key for retrieving the actual lesser-field name that displayed to the user.
     * @param lesserFieldVal
     *            The value of the lesser-field.
     * @param greaterFieldName
     *            Name of the validating greater-field (java representation), the field that its' value should be greater than lesser-field.
     * @param greaterFieldLabel
     *            Message Key for retrieving the actual greater-field name that displayed to the user.
     * @param greaterFieldVal
     *            The value of the greater-field.
     * @return int which represents comparison value (see {@link Comparable#compareTo(Object)}). But, if any of the value is null, -1 will be returned.
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected final <T extends Comparable> int validateLesserOrEqualsValue(final MESSAGE_CONTAINER messageContainer, final String lesserFieldName,
        final String lesserFieldLabel, final T lesserFieldVal, final String greaterFieldName, final String greaterFieldLabel, final T greaterFieldVal) {
        int result = -1;
        if ((lesserFieldVal != null) && (greaterFieldVal != null)) {
            result = lesserFieldVal.compareTo(greaterFieldVal);
            if (result > 0) {
                addError(messageContainer, greaterFieldName + StandardConstants.STRING_COMMA + lesserFieldName,
                         this.getMessage(ErrorConstants.NOT_LESSER_NOR_EQUALS, this.getMessage(lesserFieldLabel), this.getMessage(greaterFieldLabel)));
            }
        }
        
        return result;
    }
    
    /**
     * Validate whether the "lesser-field" is really less than "greater-field".
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param lesserFieldName
     *            Name of the validating lesser-field (java representation), the field that its' value should be less than greater-field.
     * @param lesserFieldLabel
     *            Message Key for retrieving the actual lesser-field name that displayed to the user.
     * @param lesserFieldVal
     *            The value of the lesser-field.
     * @param greaterFieldName
     *            Name of the validating greater-field (java representation), the field that its' value should be greater than lesser-field.
     * @param greaterFieldLabel
     *            Message Key for retrieving the actual greater-field name that displayed to the user.
     * @param greaterFieldVal
     *            The value of the greater-field.
     * @return int which represents comparison value (see {@link Comparable#compareTo(Object)}). But, if any of the value is null, -1 will be returned.
     */
    protected final <T extends Comparable<T>> int validateLesserValue(final MESSAGE_CONTAINER messageContainer, final String lesserFieldName,
        final String lesserFieldLabel, final T lesserFieldVal, final String greaterFieldName, final String greaterFieldLabel, final T greaterFieldVal) {
        int result = -1;
        if ((lesserFieldVal != null) && (greaterFieldVal != null)) {
            result = lesserFieldVal.compareTo(greaterFieldVal);
            if (result >= 0) {
                addError(messageContainer, greaterFieldName + StandardConstants.STRING_COMMA + lesserFieldName,
                         this.getMessage(ErrorConstants.NOT_LESSER, this.getMessage(lesserFieldLabel), this.getMessage(greaterFieldLabel)));
            }
        }
        
        return result;
    }
    
    /**
     * Validate the field with Regular Expression pattern. If the field is valid this method returns the trimmed input String.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabel
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @param regex
     *            Regular Expression String which will be used to validate the field.
     * @param errorMsgKey
     *            Message key to be displayed when validation fail.
     * @param regExPatternLabel
     *            Label for user friendly string representing the acceptable characters. This String will be included in the message displayed to User.
     * @return Trimmed String. Return null if validation fail.
     */
    protected final String validateRegEx(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value, final String regex, final String errorMsgKey, final String regExPatternLabel, final Object... extraMessageParams) {
        String result = StringUtils.stripToNull(value);
        if ((result != null) && (!result.matches(regex))) {
            result = null;
            
            final Object[] messageParams = new Object[BASE_REGEX_MESSAGE_PARAMS_CNT + extraMessageParams.length];
            messageParams[0] = this.getMessage(fieldLabel);
            messageParams[1] = regExPatternLabel != null && !"".equals(regExPatternLabel.trim()) ? this.getMessage(regExPatternLabel) : "";
            messageParams[2] = StringEscapeUtils.escapeHtml(value);
            if (extraMessageParams.length > 0) {
                System.arraycopy(extraMessageParams, 0, messageParams, BASE_REGEX_MESSAGE_PARAMS_CNT, extraMessageParams.length);
            }
            
            addError(messageContainer, fieldName, this.getMessage(errorMsgKey, messageParams));
        }
        
        return result;
    }
    
    /**
     * Validate List of String (values of a field) with Regular Expression pattern. This method is similar to
     * {@link #validateRegEx(WebSecurityForm, String, String, String, String, String, String)}.
     * The only difference is that this one works on List of String.
     * 
     * Note that validation will instantly stop after an invalid value has been found.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabel
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param valuesList
     *            List of the values of the field.
     * @param regex
     *            Regular Expression String which will be used to validate the field.
     * @param errorMsgKey
     *            Message key to be displayed when validation fail.
     * @param regExPatternLabel
     *            Label for user friendly string representing the acceptable characters. This String will be included in the message displayed to User.
     * @return List of trimmed String. REturn null if validation fail.
     */
    protected final List<String> validateRegEx(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final Collection<String> valuesList, final String regex, final String errorMsgKey, final String regExPatternLabel,
        final Object... extraMessageParams) {
        List<String> result = null;
        if ((valuesList != null) && (valuesList.size() > 0)) {
            result = new ArrayList<String>(valuesList.size());
            final Pattern p = Pattern.compile(regex);
            final Iterator<String> valueItr = valuesList.iterator();
            while ((result != null) && valueItr.hasNext()) {
                final String value = valueItr.next().trim();
                if (value.length() > 0) {
                    if (p.matcher(value).matches()) {
                        result.add(value);
                    } else {
                        result = null;
                        
                        final Object[] messageParams = new Object[BASE_REGEX_MESSAGE_PARAMS_CNT + extraMessageParams.length];
                        messageParams[0] = this.getMessage(fieldLabel);
                        messageParams[1] = regExPatternLabel != null && !"".equals(regExPatternLabel.trim()) ? this.getMessage(regExPatternLabel)
                                : "";
                        messageParams[2] = StringEscapeUtils.escapeHtml(value);
                        if (extraMessageParams.length > 0) {
                            System.arraycopy(extraMessageParams, 0, messageParams, BASE_REGEX_MESSAGE_PARAMS_CNT, extraMessageParams.length);
                        }
                        
                        addError(messageContainer, fieldName, this.getMessage(errorMsgKey, messageParams));
                    }
                }
            }
        }
        
        return result;
    }
    
    /**
     * Validate the field value whether it is Alpha Numeric or not.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabel
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @return Trimmed String. Return null if validation fail.
     */
    protected final String validateAlphaNumeric(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value) {
        return validateRegEx(messageContainer, fieldName, fieldLabel, value, WebCoreConstants.REGEX_ALPHANUMERIC, ErrorConstants.MISMATCHED_STRING,
                             ErrorConstants.PATTERN_ALPHANUMERIC);
    }
    
    /**
     * Validate the field value whether it is Alpha Numeric with space or not.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabel
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @return Trimmed String. Return null if validation fail.
     */
    protected final String validateAlphaNumericWithSpace(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value) {
        return validateRegEx(messageContainer, fieldName, fieldLabel, value, WebCoreConstants.REGEX_ALPHANUMERIC_WITH_SPACE,
                             ErrorConstants.MISMATCHED_STRING, ErrorConstants.PATTERN_ALPHANUMERIC_SPACE);
    }
    
    /**
     * Validate the field value whether it is Alpha Numeric with wildcard character or not.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabel
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @param numberOfDigits
     *            The max number of decimal digits
     * @return Trimmed String. Return null if validation fail.
     */
    protected final String validateAlphaNumericSearch(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value) {
        return validateRegEx(messageContainer, fieldName, fieldLabel, value, WebCoreConstants.REGEX_ALPHANUMERIC_WITH_STAR,
                             ErrorConstants.MISMATCHED_STRING, ErrorConstants.PATTERN_ALPHANUMERIC_STAR);
    }
    
    /**
     * Validate the field value whether it is Alpha Numeric with wildcard character and + or not.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabel
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @param numberOfDigits
     *            The max number of decimal digits
     * @return Trimmed String. Return null if validation fail.
     */
    protected final String validateAlphaNumericSearchWithPlus(final MESSAGE_CONTAINER messageContainer, final String fieldName,
        final String fieldLabel, final String value) {
        return validateRegEx(messageContainer, fieldName, fieldLabel, value, WebCoreConstants.REGEX_ALPHANUMERIC_WITH_STAR_AND_PLUS,
                             ErrorConstants.MISMATCHED_STRING, ErrorConstants.PATTERN_ALPHANUMERIC_STAR_PLUS);
    }
    
    /**
     * Validate the field value whether it is Number or not.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabel
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @return Trimmed String. Return null if validation fail.
     */
    protected final String validateNumber(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value) {
        return validateRegEx(messageContainer, fieldName, fieldLabel, value, WebCoreConstants.REGEX_INTEGER, ErrorConstants.MISMATCHED_NUMBER,
                             ErrorConstants.PATTERN_NUMERIC);
    }
    
    protected final Integer validateNumber(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final Integer value) {
        if (value == null) {
            this.addError(messageContainer,
                          fieldName,
                          this.getMessage(ErrorConstants.MISMATCHED_NUMBER, this.getMessage(fieldLabel),
                                          this.getMessage(ErrorConstants.PATTERN_NUMERIC)));
        }
        
        return value;
    }
    
    protected final String validateCustomerCardNumber(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value) {
        return validateRegEx(messageContainer, fieldName, fieldLabel, value, WebCoreConstants.REGEX_INTEGER_WITH_EQUALS,
                             ErrorConstants.MISMATCHED_STRING, ErrorConstants.PATTERN_NUMERIC_EQUALS);
    }
    
    /**
     * Validate whether a possibly floating point string with maximum 2 digits is a number or not.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabel
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @return Trimmed String. Return null if validation fail.
     */
    protected final String validateDollarAmt(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value) {
        return validateRegEx(messageContainer, fieldName, fieldLabel, value, WebCoreConstants.REGEX_TWO_DIGIT_FLOAT,
                             ErrorConstants.MISMATCHED_CURRENCY, ErrorConstants.PATTERN_CURRENCY);
    }
    
    protected final BigDecimal validateDollarAmt(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final BigDecimal value) {
        if ((value == null) || (value.scale() < WebCoreConstants.MIN_CURRENCY_SCALE) || (value.scale() > WebCoreConstants.MAX_CURRENCY_SCALE)) {
            this.addError(messageContainer,
                          fieldName,
                          this.getMessage(ErrorConstants.MISMATCHED_STRING, this.getMessage(fieldLabel),
                                          this.getMessage(ErrorConstants.PATTERN_CURRENCY)));
        }
        
        return value;
    }
    
    /**
     * Validate whether the possibly floating point value string with 0 to N digits is a number or not.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabel
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @param numberOfDigits
     *            The value of the field.
     * @return Trimmed String. Return null if validation fail.
     */
    protected final String validateFloat(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value, final Integer numberOfDigits) {
        final String regExStr = "^[0-9]+(\\.[0-9]{0," + numberOfDigits + "})?$";
        return validateRegEx(messageContainer, fieldName, fieldLabel, value, regExStr, ErrorConstants.MISMATCHED_FLOAT,
                             ErrorConstants.PATTERN_NUMERIC, numberOfDigits);
    }
    
    /**
     * Validate whether the possibly floating point value string is a number or not.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabel
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @return Trimmed String. Return null if validation fail.
     */
    protected final String validateFloat(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel, final String value) {
        return validateRegEx(messageContainer, fieldName, fieldLabel, value, WebCoreConstants.REGEX_FLOAT, ErrorConstants.MISMATCHED_FLOAT,
                             ErrorConstants.PATTERN_NUMERIC);
    }
    
    protected final Number validateFloat(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel, final Number value) {
        if (value == null) {
            this.addError(messageContainer,
                          fieldName,
                          this.getMessage(ErrorConstants.MISMATCHED_FLOAT, this.getMessage(fieldLabel),
                                          this.getMessage(ErrorConstants.PATTERN_NUMERIC)));
        }
        
        return value;
    }
    
    protected final String validateLocationText(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value) {
        return validateRegEx(messageContainer, fieldName, fieldLabel, value, WebCoreConstants.REGEX_LOCATION_TEXT, ErrorConstants.MISMATCHED_STRING,
                             ErrorConstants.PATTERN_LOCATION);
    }
    
    protected final String validateCouponText(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value) {
        return validateRegEx(messageContainer, fieldName, fieldLabel, value, WebCoreConstants.REGEX_COUPON_TEXT, ErrorConstants.MISMATCHED_STRING,
                             ErrorConstants.PATTERN_LOCATION);
    }
    
    protected final String validateCouponSearchText(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value) {
        return validateRegEx(messageContainer, fieldName, fieldLabel, value, WebCoreConstants.REGEX_COUPON_SEARCH_TEXT,
                             ErrorConstants.MISMATCHED_STRING, ErrorConstants.PATTERN_COUPON_SEARCH);
    }
    
    protected final String validateCouponWithDiscountSearchText(final MESSAGE_CONTAINER messageContainer, final String fieldName,
        final String fieldLabel, final String value) {
        return validateRegEx(messageContainer, fieldName, fieldLabel, value, WebCoreConstants.REGEX_COUPON_WITH_DISCOUNT_SEARCH_TEXT,
                             ErrorConstants.MISMATCHED_STRING, ErrorConstants.PATTERN_COUPON_SEARCH_DISCOUNT);
    }
    
    protected final String validateDiscountText(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value) {
        String result = StringUtils.stripToNull(value);
        if (result != null) {
            Number discountAmt = null;
            try {
                discountAmt = getDiscountAmountWithPrefixedSign(result);
                if (discountAmt == null) {
                    discountAmt = getDiscountAmountWithSuffixedSign(result);
                }
                if (discountAmt == null) {
                    discountAmt = new Double(result);
                }
            } catch (NumberFormatException nfe) {
                // DO NOTHING.
            }
            
            if ((discountAmt == null)
                || (((discountAmt instanceof Integer) && isInvalidPercentageDiscount(discountAmt.intValue())) || ((discountAmt instanceof Double) && isInvalidDoolarDiscount(discountAmt
                        .doubleValue())))) {
                result = null;
                
                final Object[] messageParams = new Object[BASE_REGEX_MESSAGE_PARAMS_CNT];
                messageParams[0] = this.getMessage(fieldLabel);
                messageParams[1] = this.getMessage(ErrorConstants.MISMATCHED_DISCOUNT);
                messageParams[2] = StringEscapeUtils.escapeHtml(value);
                
                addError(messageContainer, fieldName, this.getMessage(ErrorConstants.MISMATCHED_DISCOUNT, messageParams));
            }
        }
        
        return result;
    }
    
    private Number getDiscountAmountWithPrefixedSign(final String discountAmt) {
        return (discountAmt.length() <= 1) ? null : getDiscountAmount(discountAmt.charAt(0), discountAmt.substring(1).trim());
    }
    
    private Number getDiscountAmountWithSuffixedSign(final String discountAmt) {
        final int endIdx = discountAmt.length() - 1;
        return (endIdx > 0) ? getDiscountAmount(discountAmt.charAt(endIdx), discountAmt.substring(0, endIdx).trim()) : null;
    }
    
    private Number getDiscountAmount(final char sign, final String discountAmt) {
        Number result = null;
        if (sign == '$') {
            result = new Double(discountAmt);
        } else if (sign == '%') {
            result = new Integer(discountAmt);
        }
        
        return result;
    }
    
    private boolean isInvalidDoolarDiscount(final double discountAmt) {
        return discountAmt < WebCoreConstants.MIN_DISCOUNT_DOLLARS;
    }
    
    private boolean isInvalidPercentageDiscount(final int discountAmt) {
        return (discountAmt < WebCoreConstants.MIN_DISCOUNT_PERCENTAGE) || (discountAmt > WebCoreConstants.MAX_DISCOUNT_PERCENTAGE);
    }
    
    protected final String validateStandardText(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value) {
        return validateRegEx(messageContainer, fieldName, fieldLabel, value, WebCoreConstants.REGEX_STANDARD_TEXT, ErrorConstants.MISMATCHED_STRING,
                             ErrorConstants.PATTERN_STANDARD_TEXT);
    }
    
    protected final String validateExtraStandardText(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value) {
        return validateRegEx(messageContainer, fieldName, fieldLabel, value, WebCoreConstants.REGEX_EXTRA_STANDARD_TEXT,
                             ErrorConstants.MISMATCHED_STRING, ErrorConstants.PATTERN_STANDARD_TEXT_EXTRA);
    }
    
    protected final String validatePrintableText(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value) {
        return validateRegEx(messageContainer, fieldName, fieldLabel, value, WebCoreConstants.REGEX_PRINTABLE_TEXT, ErrorConstants.MISMATCHED_STRING,
                             ErrorConstants.PATTERN_PRINTABLE_TEXT);
    }
    
    protected final String validatePayStationText(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value) {
        return validateRegEx(messageContainer, fieldName, fieldLabel, value, WebCoreConstants.REGEX_PAYSTATION_TEXT,
                             ErrorConstants.MISMATCHED_STRING, ErrorConstants.PATTERN_PAY_STATION);
    }
    
    protected final List<String> validatePayStationText(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final List<String> value) {
        return validateRegEx(messageContainer, fieldName, fieldLabel, value, WebCoreConstants.REGEX_PAYSTATION_TEXT,
                             ErrorConstants.MISMATCHED_STRING, ErrorConstants.PATTERN_PAY_STATION);
    }
    
    /**
     * Validate the field value whether it is a valid description for the project.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabel
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @return Trimmed String. Return null if validation fail.
     */
    protected final String validateExternalDescription(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value, final Integer minLength) {
        final String retVal1 = validateStringLength(messageContainer, fieldName, fieldLabel, value, minLength,
                                                    WebCoreConstants.DESCRIPTION_EXTERNAL_LENGTH);
        final String retVal2 = validatePrintableText(messageContainer, fieldName, fieldLabel, value);
        if (retVal1 == null || retVal2 == null) {
            return null;
        } else {
            return retVal2;
        }
    }
    
    /**
     * Validate the field value whether it is a valid email address.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabel
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @return Trimmed String. Return null if validation fail.
     */
    protected final String validateEmailAddress(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value) {
        return validateRegEx(messageContainer, fieldName, fieldLabel, value, WebCoreConstants.REGEX_EMAIL, ErrorConstants.MISMATCHED_EMAIL, "");
    }
    
    /**
     * Validate the List values of the field whether they are all valid email address or not.
     * 
     * Note that validation will instantly stop after an invalid value has been found.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabel
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param valuesList
     *            List of the values of the field.
     * @return List of trimmed String. REturn null if validation fail.
     */
    protected final List<String> validateEmailAddresses(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final Collection<String> valuesList) {
        return validateRegEx(messageContainer, fieldName, fieldLabel, valuesList, WebCoreConstants.REGEX_EMAIL, ErrorConstants.MISMATCHED_EMAIL,
                             ErrorConstants.PATTERN_EMAIL);
    }
    
    /**
     * Validate the field value whether it is a valid URL.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabel
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @return Trimmed String. Return null if validation fail.
     */
    protected final String validateURL(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel, final String value) {
        return validateRegEx(messageContainer, fieldName, fieldLabel, value, WebCoreConstants.REGEX_URL, ErrorConstants.MISMATCHED_URL_NO_PROTOCOL,
                             "");
    }
    
    /**
     * Validate whether the value is valid range string. The range string is a string containing sets of range.
     * This method also validate that the begin and end of the range is located with in "min" and "max" value.
     * For example, valid range string is: "0-9, 1-5", "0, 50-100"
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabel
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @param min
     *            min value (inclusive) of the range.
     * @param max
     *            max value (inclusive) of the range
     * @return true if the value pass validation.
     */
    protected final boolean validateIntegerRangeString(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value, final int min, final int max) {
        boolean result = false;
        if (value != null) {
            final String[] groups = value.split(StandardConstants.STRING_COMMA);
            
            result = true;
            int idx = groups.length;
            while (result && (--idx >= 0)) {
                final String[] ranges = groups[idx].split(StandardConstants.STRING_DASH);
                if (ranges.length == 1) {
                    result = validateSingleIntegerRangeString(messageContainer, fieldName, fieldLabel, ranges[0].trim(), min, max);
                } else if (ranges.length == 2) {
                    result = validateSingleIntegerRangeString(messageContainer, fieldName, fieldLabel, ranges[0].trim(), ranges[1].trim(), min, max);
                } else {
                    addError(messageContainer, fieldName, this.getMessage(ErrorConstants.INVALID, this.getMessage(fieldLabel)));
                    result = false;
                }
            }
        }
        
        return result;
    }
    
    private boolean validateSingleIntegerRangeString(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value, final int min, final int max) {
        boolean result = true;
        if (validateNumber(messageContainer, fieldName, fieldLabel, value) != null) {
            result = validateIntegerLimits(messageContainer, fieldName, fieldLabel, value, min, max) >= 0;
        } else {
            result = false;
        }
        
        return result;
    }
    
    private boolean validateSingleIntegerRangeString(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String start, String end, final int min, final int max) {
        boolean result = true;
        if ((validateNumber(messageContainer, fieldName, fieldLabel, start) != null)
            && (validateNumber(messageContainer, fieldName, fieldLabel, end) != null)) {
            result = validateIntegerLimits(messageContainer, fieldName, fieldLabel, end, min, max) >= 0;
            if (result && (Integer.parseInt(start) > Integer.parseInt(end))) {
                addError(messageContainer, fieldName, this.getMessage(ErrorConstants.INVALID_RANGE, this.getMessage(fieldLabel)));
            }
        } else {
            result = false;
        }
        
        return result;
    }
    
    /**
     * Validate the field value whether it is valid boolean string ("", "yes", "no, "on", "off", "true", "false"). The actual implementation is in
     * WebCoreUtil.getBoolean.
     * For null and "", the value is considered to be false.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabel
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @return Boolean value. Return null if validation fail.
     */
    protected final Boolean validateBooleanString(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value) {
        final Boolean result = WebCoreUtil.getBoolean(value);
        if (result == null) {
            addError(messageContainer, fieldName,
                     this.getMessage(ErrorConstants.INVALID_BOOLEAN, this.getMessage(fieldLabel), StringEscapeUtils.escapeHtml(value)));
        }
        
        return result;
    }
    
    /**
     * Transform the comma-separated-string sent from UI (in List Object).
     * Note that only first element will be used because all the data from UI are going to be in just the first element
     * 
     * @param raw
     *            raw List Object containing comma-separated-string
     * @return List of every items in comma-separated-string or null if there are nothing in the string
     */
    protected final List<String> transformCommaSeparatedList(final List<String> raw) {
        return ((raw == null) || (raw.size() <= 0)) ? null : transformCommaSeparatedList(raw.get(0));
    }
    
    /**
     * Transform the comma-separated-string sent from UI (in String).
     * 
     * @param raw
     *            comma-separated-string
     * @return List of every items in comma-separated-string or null if there are nothing in the string
     */
    protected final List<String> transformCommaSeparatedList(final String raw) {
        List<String> result = null;
        if (raw != null) {
            final String[] items = raw.split(StandardConstants.STRING_COMMA);
            
            result = new ArrayList<String>(items.length);
            for (int i = -1; ++i < items.length;) {
                if (items[i] != null) {
                    items[i] = items[i].trim();
                    if (items[i].length() > 0) {
                        result.add(items[i]);
                    }
                }
            }
            
            if (result.size() <= 0) {
                result = null;
            }
        }
        
        return result;
    }
    
    /**
     * Calculate time difference in minutes between start and end hours and minutes.
     * 
     * @param startHrMin
     *            startHrMin[0] is the hour, e.g. 03, 21. startHrMin[1] is the minute, e.g. 00, 30.
     * @param endHrMin
     *            endHrMin[0] is the hour, e.g. 06, 24. endHrMin[1] is the minute, e.g. 00, 30.
     * @return int The differences in minutes.
     */
    protected final int getTimeDiffInMinute(final String[] startHrMin, final String[] endHrMin) {
        final int startTime = Integer.parseInt(startHrMin[0].trim()) * StandardConstants.HOURS_IN_MINS_1 + Integer.parseInt(startHrMin[1].trim());
        final int endTime = Integer.parseInt(endHrMin[0].trim()) * StandardConstants.HOURS_IN_MINS_1 + Integer.parseInt(endHrMin[1].trim());
        return endTime - startTime;
    }
    
    /**
     * Validate the field value whether it is using defined format (e.g. HH:mm) and value is correct.
     * 
     * @param messageContainer
     *            Object that hold all the messages.
     * @param fieldName
     *            Name of the validating field (java representation).
     * @param fieldLabel
     *            Message Key for retrieving the actual field name that displayed to the user.
     * @param value
     *            The value of the field.
     * @return boolean false if validation failed.
     */
    protected final boolean validateTimeFormat(final MESSAGE_CONTAINER messageContainer, final String fieldName, final String fieldLabel,
        final String value) {
        
        final DateFormat timeFmt = new SimpleDateFormat(DateUtil.TIME_UI_FORMAT);
        try {
            timeFmt.parse(value);
            return true;
        } catch (ParseException pe) {
            addError(messageContainer,
                     fieldName,
                     this.getMessage(ErrorConstants.INVALID_TIME, this.getMessage(fieldLabel),
                                     this.getMessage(ErrorConstants.PATTERN_TIME)));

        }
        return false;
    }
}
