package com.digitalpaytech.util;

public final class KPIConstants {
    
    // Names of name-value pairs
    
    public static final String DIGITALAPI_RESPONSE_TIME_AUDITINFO = "digitalapi.responseTime.auditInfo";
    public static final String DIGITALAPI_RESPONSE_TIME_PAYSTATIONINFO = "digitalapi.responseTime.paystationInfo";
    public static final String DIGITALAPI_RESPONSE_TIME_TRANSACTIONINFO = "digitalapi.responseTime.transactionInfo";
    public static final String DIGITALAPI_RESPONSE_TIME_PLATEINFO = "digitalapi.responseTime.plateInfo";
    public static final String DIGITALAPI_RESPONSE_TIME_STALLINFO = "digitalapi.responseTime.stallInfo";
    public static final String DIGITALAPI_RESPONSE_TIME_COUPON = "digitalapi.responseTime.coupon";
    
    public static final String DIGITALAPI_REQUEST_COUPON_FILESIZE = "digitalapi.request.coupon.file.size";
    
    public static final String DIGITALAPI_REQUEST_AUDITINFO = "digitalapi.request.auditInfo";
    public static final String DIGITALAPI_REQUEST_PAYSTATIONINFO = "digitalapi.request.paystationInfo";
    public static final String DIGITALAPI_REQUEST_PLATEINFO = "digitalapi.request.plateInfo";
    public static final String DIGITALAPI_REQUEST_STALLINFO = "digitalapi.request.stallInfo";
    public static final String DIGITALAPI_REQUEST_TRANSACTIONINFO = "digitalapi.request.transactionInfo";
    public static final String DIGITALAPI_REQUEST_COUPON = "digitalapi.request.coupon";
    public static final String DIGITALAPI_REQUEST_TOTAL = "digitalapi.request.total";
    public static final String DIGITALAPI_REQUEST_SUCCESS_TOTAL = "digitalapi.request.success.total";
    public static final String DIGITALAPI_REQUEST_SUCCESS_AUDITINFO = "digitalapi.request.success.auditInfo";
    public static final String DIGITALAPI_REQUEST_SUCCESS_PAYSTATIONINFO = "digitalapi.request.success.paystationInfo";
    public static final String DIGITALAPI_REQUEST_SUCCESS_PLATEINFO = "digitalapi.request.success.plateInfo";
    public static final String DIGITALAPI_REQUEST_SUCCESS_STALLINFO = "digitalapi.request.success.stallInfo";
    public static final String DIGITALAPI_REQUEST_SUCCESS_TRANSACTIONINFO = "digitalapi.request.success.transactionInfo";
    public static final String DIGITALAPI_REQUEST_SUCCESS_COUPON = "digitalapi.request.success.coupon";
    public static final String DIGITALAPI_REQUEST_FAIL_TOTAL = "digitalapi.request.fail.total";
    public static final String DIGITALAPI_REQUEST_FAIL_AUDITINFO = "digitalapi.request.fail.auditInfo";
    public static final String DIGITALAPI_REQUEST_FAIL_PAYSTATIONINFO = "digitalapi.request.fail.paystationInfo";
    public static final String DIGITALAPI_REQUEST_FAIL_PLATEINFO = "digitalapi.request.fail.plateInfo";
    public static final String DIGITALAPI_REQUEST_FAIL_STALLINFO = "digitalapi.request.fail.stallInfo";
    public static final String DIGITALAPI_REQUEST_FAIL_TRANSACTIONINFO = "digitalapi.request.fail.transactionInfo";
    public static final String DIGITALAPI_REQUEST_FAIL_COUPON = "digitalapi.request.fail.coupon";
    
    public static final String DIGITALAPI_COUPON_RECORD = "digitalapi.coupon.record";
    
    public static final String BOSS_UPLOAD_SUCCESS_LOTSETTING = "boss.upload.success.lotSetting";
    public static final String BOSS_UPLOAD_SUCCESS_FILE_UPLOAD = "boss.upload.success.fileUpload";
    public static final String BOSS_UPLOAD_SUCCESS_PAYSTATION_QUERY = "boss.upload.success.paystationQuery";
    public static final String BOSS_UPLOAD_SUCCESS_OFFLINE_TRANSACTION = "boss.upload.success.offlineTransaction";
    public static final String BOSS_UPLOAD_FAIL_LOTSETTING = "boss.upload.fail.lotSetting";
    public static final String BOSS_UPLOAD_FAIL_FILE_UPLOAD = "boss.upload.fail.fileUpload";
    public static final String BOSS_UPLOAD_FAIL_PAYSTATION_QUERY = "boss.upload.fail.paystationQuery";
    public static final String BOSS_UPLOAD_FAIL_OFFLINE_TRANSACTION = "boss.upload.fail.fileUpload";
    
    public static final String BOSS_DOWNLOAD_SUCCESS_LOTSETTING = "boss.download.success.lotSetting";
    public static final String BOSS_DOWNLOAD_SUCCESS_PUBLICKEY_REQUEST = "boss.download.success.publicKeyRequest";
    public static final String BOSS_DOWNLOAD_SUCCESS_CREDITCARD = "boss.download.success.creditCard";
    public static final String BOSS_DOWNLOAD_SUCCESS_BADSMARTCARD = "boss.download.success.badSmartCard";
    public static final String BOSS_DOWNLOAD_FAIL_LOTSETTING = "boss.download.fail.lotSetting";
    public static final String BOSS_DOWNLOAD_FAIL_PUBLICKEY_REQUEST = "boss.download.fail.publicKeyRequest";
    public static final String BOSS_DOWNLOAD_FAIL_CREDITCARD = "boss.download.fail.creditCard";
    public static final String BOSS_DOWNLOAD_FAIL_BADSMARTCARD = "boss.download.fail.smartCard";
    
    public static final String BOSS_UPLOAD_SUCCESS_TOTAL = "boss.upload.success.total";
    public static final String BOSS_DOWNLOAD_SUCCESS_TOTAL = "boss.download.success.total";
    public static final String BOSS_UPLOAD_FAIL_TOTAL = "boss.upload.fail.total";
    public static final String BOSS_DOWNLOAD_FAIL_TOTAL = "boss.download.fail.total";
    public static final String BOSS_UPLOAD_TOTAL = "boss.upload.total";
    public static final String BOSS_DOWNLOAD_TOTAL = "boss.download.total";
    
    public static final String UI_DASHBOARD_QUERY_TOTAL = "ui.dashboard.query.total";
    public static final String UI_DASHBOARD_QUERY_SUCCESS = "ui.dashboard.query.success";
    public static final String UI_DASHBOARD_QUERY_FAIL = "ui.dashboard.query.fail";
    public static final String UI_DASHBOARD_ALERTS = "ui.dashboard.alerts";
    public static final String UI_DASHBOARD_REVENUE = "ui.dashboard.revenue";
    public static final String UI_DASHBOARD_COLLECTIONS = "ui.dashboard.collections";
    public static final String UI_DASHBOARD_SETTLEDCARD = "ui.dashboard.settledCard";
    public static final String UI_DASHBOARD_PURCHASES = "ui.dashboard.purchases";
    public static final String UI_DASHBOARD_PAIDOCCUPANCY = "ui.dashboard.paidOccupancy";
    public static final String UI_DASHBOARD_TURNOVER = "ui.dashboard.turnover";
    public static final String UI_DASHBOARD_UTILIZATION = "ui.dashboard.utilization";
    public static final String UI_DASHBOARD_MAP = "ui.dashboard.map";
    
    public static final String UI_REQUEST_INFORMATION = "ui.request.information";
    public static final String UI_REQUEST_SUCCESS = "ui.request.success";
    public static final String UI_REQUEST_REDIRECT = "ui.request.redirect";
    public static final String UI_REQUEST_CLIENT_ERROR = "ui.request.clientError";
    public static final String UI_REQUEST_SERVER_ERROR = "ui.request.serverError";
    
    public static final String UI_REQUEST_449 = "ui.request.449";
    public static final String UI_REQUEST_480 = "ui.request.480";
    public static final String UI_REQUEST_481 = "ui.request.481";
    public static final String UI_REQUEST_482 = "ui.request.482";
    public static final String UI_REQUEST_483 = "ui.request.483";
    public static final String UI_REQUEST_509 = "ui.request.509";
    
    public static final String UNCAUGHT_EXCEPTION_INFORMATION = "uncaughtException.information";
    public static final String UNCAUGHT_EXCEPTION_SUCCESS = "uncaughtException.success";
    public static final String UNCAUGHT_EXCEPTION_REDIRECT = "uncaughtException.redirect";
    public static final String UNCAUGHT_EXCEPTION_CLIENT_ERROR = "uncaughtException.clientServer";
    public static final String UNCAUGHT_EXCEPTION_SERVER_ERROR = "uncaughtException.serverError";
    
    public static final String CARD_PROCESSING_REQUEST_CONCORD = "cardprocessing.request.concord";
    public static final String CARD_PROCESSING_REQUEST_ALLIANCE = "cardprocessing.request.alliance";
    public static final String CARD_PROCESSING_REQUEST_AUTHORIZENET = "cardprocessing.request.authorizeNet";
    public static final String CARD_PROCESSING_REQUEST_BLACKBOARD = "cardprocessing.request.blackBoard";
    public static final String CARD_PROCESSING_REQUEST_DATAWIRE = "cardprocessing.request.datawire";
    public static final String CARD_PROCESSING_REQUEST_FIRSTDATANASHVILLE = "cardprocessing.request.firstDataNashville";
    public static final String CARD_PROCESSING_REQUEST_FIRSTHORIZON = "cardprocessing.request.firstHorizon";
    public static final String CARD_PROCESSING_REQUEST_HEARTLAND = "cardprocessing.request.heartland";
    public static final String CARD_PROCESSING_REQUEST_HEARTLAND_SPPLUS = "cardprocessing.request.heartland.spplus";
    public static final String CARD_PROCESSING_REQUEST_LINK2GOV = "cardprocessing.request.link2Gov";
    public static final String CARD_PROCESSING_REQUEST_MONERIS = "cardprocessing.request.moneris";
    public static final String CARD_PROCESSING_REQUEST_NUVISION = "cardprocessing.request.nuVision";
    public static final String CARD_PROCESSING_REQUEST_PARADATA = "cardprocessing.request.paradata";
    public static final String CARD_PROCESSING_REQUEST_PAYMENTECH = "cardprocessing.request.paymentech";
    public static final String CARD_PROCESSING_REQUEST_TOTALCARD = "cardprocessing.request.totalCard";
    public static final String CARD_PROCESSING_REQUEST_CBORDGOLD = "cardprocessing.request.cbord.csgold";
    public static final String CARD_PROCESSING_REQUEST_CBORDODYSSEY = "cardprocessing.request.cbord.odyssey";
    public static final String CARD_PROCESSING_REQUEST_ELAVON_VIACONEX = "cardprocessing.request.elavon.viaconex";
    public static final String CARD_PROCESSING_REQUEST_ELAVON_CONVERGE = "cardprocessing.request.elavon.converge";
    public static final String CARD_PROCESSING_REQUEST_TD_MERCHANT_SERVICES = "cardprocessing.request.TDMerchantServices";
    public static final String CARD_PROCESSING_REQUEST_CREDITCALL = "cardprocessing.request.creditcall";
    
    public static final String CARD_PROCESSING_RESPONSE_TIME_CONCORD = "cardprocessing.responseTime.concord";
    public static final String CARD_PROCESSING_RESPONSE_TIME_ALLIANCE = "cardprocessing.responseTime.alliance";
    public static final String CARD_PROCESSING_RESPONSE_TIME_AUTHORIZENET = "cardprocessing.responseTime.authorizeNet";
    public static final String CARD_PROCESSING_RESPONSE_TIME_BLACKBOARD = "cardprocessing.responseTime.blackBoard";
    public static final String CARD_PROCESSING_RESPONSE_TIME_DATAWIRE = "cardprocessing.responseTime.datawire";
    public static final String CARD_PROCESSING_RESPONSE_TIME_FIRSTDATANASHVILLE = "cardprocessing.responseTime.firstDataNashville";
    public static final String CARD_PROCESSING_RESPONSE_TIME_FIRSTHORIZON = "cardprocessing.responseTime.firstHorizon";
    public static final String CARD_PROCESSING_RESPONSE_TIME_HEARTLAND = "cardprocessing.responseTime.heartland";
    public static final String CARD_PROCESSING_RESPONSE_TIME_HEARTLAND_SPPLUS = "cardprocessing.responseTime.heartland.spplus";
    public static final String CARD_PROCESSING_RESPONSE_TIME_LINK2GOV = "cardprocessing.responseTime.link2Gov";
    public static final String CARD_PROCESSING_RESPONSE_TIME_MONERIS = "cardprocessing.responseTime.moneris";
    public static final String CARD_PROCESSING_RESPONSE_TIME_NUVISION = "cardprocessing.responseTime.nuVision";
    public static final String CARD_PROCESSING_RESPONSE_TIME_PARADATA = "cardprocessing.responseTime.paradata";
    public static final String CARD_PROCESSING_RESPONSE_TIME_PAYMENTECH = "cardprocessing.responseTime.paymentech";
    public static final String CARD_PROCESSING_RESPONSE_TIME_TOTALCARD = "cardprocessing.responseTime.totalCard";
    public static final String CARD_PROCESSING_RESPONSE_TIME_CBORDGOLD = "cardprocessing.responseTime.cbord.csgold";
    public static final String CARD_PROCESSING_RESPONSE_TIME_CBORDODYSSEY = "cardprocessing.responseTime.cbord.odyssey";
    public static final String CARD_PROCESSING_RESPONSE_TIME_ELAVON_VIACONEX = "cardprocessing.responseTime.elavon.viaconex";
    public static final String CARD_PROCESSING_RESPONSE_TIME_ELAVON_CONVERGE = "cardprocessing.responseTime.elavon.converge";
    public static final String CARD_PROCESSING_RESPONSE_TIME_TD_MERCHANT_SERVICES = "cardprocessing.responseTime.TDMerchantServices";
    public static final String CARD_PROCESSING_RESPONSE_TIME_CREDITCALL = "cardprocessing.responseTime.creditcall";
    
    public static final String CARD_PROCESSING_QUEUE_SIZE_SETTLEMENT = "cardprocessing.queueSize.settlement";
    public static final String CARD_PROCESSING_QUEUE_SIZE_STOREANDFORWARD = "cardprocessing.queueSize.storeandforward";
    public static final String CARD_PROCESSING_QUEUE_SIZE_REVERSAL = "cardprocessing.queueSize.reversal";
    public static final String CARD_PROCESSING_QUEUE_SIZE_CARDRETRY = "cardprocessing.queueSize.cardRetry.";
    public static final String CARD_PROCESSING_QUEUE_SIZE_TOTAL = "cardprocessing.queueSize.cardRetry.total";
    
    public static final String CARD_PROCESSING_QUEUE_EXHAUSTING_STOREANDFORWARD = "cardprocessing.queue.exhausting.storeandforward";
    
    public static final String BACKGROUND_PROCESSES_SMSALERT_SENT = "backgroundprocesses.smsalert.sent";
    public static final String BACKGROUND_PROCESSES_SMSALERT_RECEIVE = "backgroundprocesses.smsalert.receive";
    public static final String BACKGROUND_PROCESSES_ALERT_EMAIL_RESPONSE_TIME = "backgroundprocesses.alert.email.responseTime";
    public static final String BACKGROUND_PROCESSES_ALERT_TOTAL = "backgroundprocesses.alert.total";
    public static final String BACKGROUND_PROCESSES_ALERT_COMMUNICATION = "backgroundprocesses.alert.communication";
    public static final String BACKGROUND_PROCESSES_ALERT_COLLECTION = "backgroundprocesses.alert.collection";
    public static final String BACKGROUND_PROCESSES_ALERT_PAYSTATION = "backgroundprocesses.alert.paystation";
    public static final String BACKGROUND_PROCESSES_RECALC = "backgroundprocesses.recalc";
    public static final String BACKGROUND_PROCESSES_RECALC_FAIL = "backgroundprocesses.recalc.fail";
    public static final String BACKGROUND_PROCESSES_REPORT_READY = "backgroundprocesses.report.ready";
    public static final String BACKGROUND_PROCESSES_REPORT_SCHEDULED = "backgroundprocesses.report.scheduled";
    public static final String BACKGROUND_PROCESSES_REPORT_LAUNCHED = "backgroundprocesses.report.launched";
    public static final String BACKGROUND_PROCESSES_REPORT_COMPLETED = "backgroundprocesses.report.completed";
    public static final String BACKGROUND_PROCESSES_REPORT_CANCELLED = "backgroundprocesses.report.cancelled";
    public static final String BACKGROUND_PROCESSES_REPORT_FAILED = "backgroundprocesses.report.failed";
    public static final String BACKGROUND_PROCESSES_REPORT_RESPONSE_TIME = "backgroundprocesses.report.responseTime";
    public static final String BACKGROUND_PROCESSES_SENSOR_DATA_TO_PROCESS = "backgroundprocesses.sensorData.toProcess";
    public static final String BACKGROUND_PROCESSES_EXTENSIBLE_PERMIT = "backgroundprocesses.extensiblePermit";
    public static final String BACKGROUND_PROCESSES_SMSALERT_TO_PROCESS = "backgroundprocesses.smsalert.toProcess";
    
    public static final String UI_LOGGED_IN = "ui.loggedIn";
    
    public static final String INCREMENTAL_MIGRATION_TRANSACTION_SIZE = "incrementalmigration.transaction.size";
    public static final String INCREMENTAL_MIGRATION_CUSTOMER_TO_PROCESS = "incrementalmigration.customer.toProcess";
    public static final String INCREMENTAL_MIGRATION_RECORD_ERROR = "incrementalmigration.record.error";
    
    public static final String XMLRPC_REQUEST_THROUGHPUT = "xmlrpc.request.throughput";
    
    public static final String XMLRPC_REQUEST_TOTAL = "xmlrpc.request.total";
    public static final String XMLRPC_REQUEST_SUCCESS_TOTAL = "xmlrpc.request.success.total";
    public static final String XMLRPC_REQUEST_SUCCESS_AUDIT = "xmlrpc.request.success.audit";
    public static final String XMLRPC_REQUEST_SUCCESS_TRANSACTION = "xmlrpc.request.success.transaction";
    public static final String XMLRPC_REQUEST_SUCCESS_AUTHORIZEREQUEST = "xmlrpc.request.success.authorizeRequest";
    public static final String XMLRPC_REQUEST_SUCCESS_EVENT = "xmlrpc.request.success.event";
    public static final String XMLRPC_REQUEST_SUCCESS_HEARTBEAT = "xmlrpc.request.success.heartBeat";
    public static final String XMLRPC_REQUEST_SUCCESS_LOTSETTING_SUCCESS = "xmlrpc.request.success.lotSettingSuccess";
    public static final String XMLRPC_REQUEST_SUCCESS_PLATE_RPT_REQ = "xmlrpc.request.success.plateRptReq";
    public static final String XMLRPC_REQUEST_SUCCESS_AUTHORIZE_CARD_REQUEST = "xmlrpc.request.success.authorizeCardRequest";
    public static final String XMLRPC_REQUEST_SUCCESS_STALLINFO_REQUEST = "xmlrpc.request.success.stallInfoRequest";
    public static final String XMLRPC_REQUEST_SUCCESS_STALLREPORT_REQUEST = "xmlrpc.request.success.stallReportRequest";
    
    public static final String XMLRPC_REQUEST_FAIL_TOTAL = "xmlrpc.request.fail.total";
    public static final String XMLRPC_REQUEST_FAIL_AUDIT = "xmlrpc.request.fail.audit";
    public static final String XMLRPC_REQUEST_FAIL_TRANSACTION = "xmlrpc.request.fail.transaction";
    public static final String XMLRPC_REQUEST_FAIL_AUTHORIZE_REQUEST = "xmlrpc.request.fail.authorizeRequest";
    public static final String XMLRPC_REQUEST_FAIL_EVENT = "xmlrpc.request.fail.event";
    public static final String XMLRPC_REQUEST_FAIL_HEARTBEAT = "xmlrpc.request.fail.heartBeat";
    public static final String XMLRPC_REQUEST_FAIL_LOTSETTING_SUCCESS = "xmlrpc.request.fail.lotSettingSuccess";
    public static final String XMLRPC_REQUEST_FAIL_PLATE_RPT_REQ = "xmlrpc.request.fail.plateRptReq";
    public static final String XMLRPC_REQUEST_FAIL_AUTHORIZE_CARD_REQUEST = "xmlrpc.request.fail.authorizeCardRequest";
    public static final String XMLRPC_REQUEST_FAIL_STALLINFO_REQUEST = "xmlrpc.request.fail.stallInfoRequest";
    public static final String XMLRPC_REQUEST_FAIL_STALLREPORT_REQUEST = "xmlrpc.request.fail.stallReportRequest";
    
    public static final String XMLRPC_REQUEST_TRANSACTION_SUCCESS_STOREANDFORWARD = "xmlrpc.request.transaction.success.storeAndForward";
    public static final String XMLRPC_REQUEST_TRANSACTION_SUCCESS_CASHCREDIT_SWIPE = "xmlrpc.request.transaction.success.payment.cashcredit.swipe";
    public static final String XMLRPC_REQUEST_TRANSACTION_SUCCESS_CASHCREDIT_CL = "xmlrpc.request.transaction.success.payment.cashcredit.contactless";
    public static final String XMLRPC_REQUEST_TRANSACTION_SUCCESS_CASHCREDIT_CH = "xmlrpc.request.transaction.success.payment.cashcredit.chip";
    public static final String XMLRPC_REQUEST_TRANSACTION_SUCCESS_CASHVALUE = "xmlrpc.request.transaction.success.payment.cashValue";
    public static final String XMLRPC_REQUEST_TRANSACTION_SUCCESS_VALUE = "xmlrpc.request.transaction.success.payment.value";
    public static final String XMLRPC_REQUEST_TRANSACTION_SUCCESS_CREDIT_SWIPE = "xmlrpc.request.transaction.success.payment.credit.swipe";
    public static final String XMLRPC_REQUEST_TRANSACTION_SUCCESS_CREDIT_CL = "xmlrpc.request.transaction.success.payment.credit.contactless";
    public static final String XMLRPC_REQUEST_TRANSACTION_SUCCESS_CREDIT_CH = "xmlrpc.request.transaction.success.payment.credit.chip";
    public static final String XMLRPC_REQUEST_TRANSACTION_SUCCESS_CASHSMART = "xmlrpc.request.transaction.success.payment.cashSmart";
    public static final String XMLRPC_REQUEST_TRANSACTION_SUCCESS_SMART = "xmlrpc.request.transaction.success.payment.smart";
    public static final String XMLRPC_REQUEST_TRANSACTION_SUCCESS_UNKNOWN = "xmlrpc.request.transaction.success.payment.unknown";
    public static final String XMLRPC_REQUEST_TRANSACTION_SUCCESS_CASH = "xmlrpc.request.transaction.success.payment.cash";
    public static final String XMLRPC_REQUEST_TRANSACTION_SUCCESS_NA = "xmlrpc.request.transaction.success.permit.na";
    public static final String XMLRPC_REQUEST_TRANSACTION_SUCCESS_PAYANDDISPLAY = "xmlrpc.request.transaction.success.permit.payAndDisplay";
    public static final String XMLRPC_REQUEST_TRANSACTION_SUCCESS_PAYBYPLATE = "xmlrpc.request.transaction.success.permit.payByPlate";
    public static final String XMLRPC_REQUEST_TRANSACTION_SUCCESS_PAYBYSPACE = "xmlrpc.request.transaction.success.permit.payBySpace";
    
    public static final String XMLRPC_REQUEST_TRANSACTION_FAIL_STOREANDFORWARD = "xmlrpc.request.transaction.fail.storeAndForward";
    public static final String XMLRPC_REQUEST_TRANSACTION_FAIL_CASHCREDIT_SWIPE = "xmlrpc.request.transaction.fail.payment.cashcredit.swipe";
    public static final String XMLRPC_REQUEST_TRANSACTION_FAIL_CASHCREDIT_CL = "xmlrpc.request.transaction.fail.payment.cashcredit.contactless";
    public static final String XMLRPC_REQUEST_TRANSACTION_FAIL_CASHCREDIT_CH = "xmlrpc.request.transaction.fail.payment.cashcredit.chip";
    public static final String XMLRPC_REQUEST_TRANSACTION_FAIL_CASHVALUE = "xmlrpc.request.transaction.fail.payment.cashValue";
    public static final String XMLRPC_REQUEST_TRANSACTION_FAIL_VALUE = "xmlrpc.request.transaction.fail.payment.value";
    public static final String XMLRPC_REQUEST_TRANSACTION_FAIL_CREDIT_SWIPE = "xmlrpc.request.transaction.fail.payment.credit.swipe";
    public static final String XMLRPC_REQUEST_TRANSACTION_FAIL_CREDIT_CL = "xmlrpc.request.transaction.fail.payment.credit.contactless";
    public static final String XMLRPC_REQUEST_TRANSACTION_FAIL_CREDIT_CH = "xmlrpc.request.transaction.fail.payment.credit.chip";
    public static final String XMLRPC_REQUEST_TRANSACTION_FAIL_CASHSMART = "xmlrpc.request.transaction.fail.payment.cashSmart";
    public static final String XMLRPC_REQUEST_TRANSACTION_FAIL_SMART = "xmlrpc.request.transaction.fail.payment.smart";
    public static final String XMLRPC_REQUEST_TRANSACTION_FAIL_UNKNOWN = "xmlrpc.request.transaction.fail.payment.unknown";
    public static final String XMLRPC_REQUEST_TRANSACTION_FAIL_CASH = "xmlrpc.request.transaction.fail.payment.cash";
    public static final String XMLRPC_REQUEST_TRANSACTION_FAIL_NA = "xmlrpc.request.transaction.fail.permit.na";
    public static final String XMLRPC_REQUEST_TRANSACTION_FAIL_PAYANDDISPLAY = "xmlrpc.request.transaction.fail.permit.payAndDisplay";
    public static final String XMLRPC_REQUEST_TRANSACTION_FAIL_PAYBYPLATE = "xmlrpc.request.transaction.fail.permit.payByPlate";
    public static final String XMLRPC_REQUEST_TRANSACTION_FAIL_PAYBYSPACE = "xmlrpc.request.transaction.fail.permit.payBySpace";
    
    public static final String PAYSTATION_REST_REQUEST_FAIL_CANCEL_TRANSACTION = "paystation.rest.request.fail.cancelTransaction";
    public static final String PAYSTATION_REST_REQUEST_FAIL_HEARTBEAT = "paystation.rest.request.fail.heartBeat";
    public static final String PAYSTATION_REST_REQUEST_FAIL_TOKEN = "paystation.rest.request.fail.token";
    public static final String PAYSTATION_REST_REQUEST_FAIL_TRANSACTION = "paystation.rest.request.fail.transaction";
    public static final String PAYSTATION_REST_REQUEST_FAIL_TOTAL = "paystation.rest.request.fail.total";
    public static final String PAYSTATION_REST_REQUEST_SUCCESS_CANCEL_TRANSACTION = "paystation.rest.request.success.cancelTransaction";
    public static final String PAYSTATION_REST_REQUEST_SUCCESS_HEARTBEAT = "paystation.rest.request.success.heartBeat";
    public static final String PAYSTATION_REST_REQUEST_SUCCESS_TOKEN = "paystation.rest.request.success.token";
    public static final String PAYSTATION_REST_REQUEST_SUCCESS_TRANSACTION = "paystation.rest.request.success.transaction";
    public static final String PAYSTATION_REST_REQUEST_SUCCESS_TOTAL = "paystation.rest.request.success.total";
    public static final String PAYSTATION_REST_REQUEST_THROUGHPUT = "paystation.rest.request.throughput";
    public static final String PAYSTATION_REST_REQUEST_TOTAL = "paystation.rest.request.total";
    
    public static final String PAYSTATION_REST_REQUEST_TRANS_FAIL_PAYMENT_CASH = "paystation.rest.request.transaction.fail.payment.cash";
    public static final String PAYSTATION_REST_REQUEST_TRANS_FAIL_PAYMENT_CASHSMART = "paystation.rest.request.transaction.fail.payment.cashSmart";
    public static final String PAYSTATION_REST_REQUEST_TRANS_FAIL_PAYMENT_CASHVALUE = "paystation.rest.request.transaction.fail.payment.cashValue";
    public static final String PAYSTATION_REST_REQUEST_TRANS_FAIL_PAYMENT_CASHCREDIT = "paystation.rest.request.transaction.fail.payment.cashCredit";
    public static final String PAYSTATION_REST_REQUEST_TRANS_FAIL_PAYMENT_CREDIT = "paystation.rest.request.transaction.fail.payment.credit";
    public static final String PAYSTATION_REST_REQUEST_TRANS_FAIL_PAYMENT_SMART = "paystation.rest.request.transaction.fail.payment.smart";
    public static final String PAYSTATION_REST_REQUEST_TRANS_FAIL_PAYMENT_UNKNOWN = "paystation.rest.request.transaction.fail.payment.unknown";
    public static final String PAYSTATION_REST_REQUEST_TRANS_FAIL_PAYMENT_VALUE = "paystation.rest.request.transaction.fail.payment.value";
    public static final String PAYSTATION_REST_REQUEST_TRANS_FAIL_PERMIT_NA = "paystation.rest.request.transaction.fail.permit.na";
    public static final String PAYSTATION_REST_REQUEST_TRANS_FAIL_PERMIT_PAYANDDISPLAY = "paystation.rest.request.transaction.fail.permit.payAndDisplay";
    public static final String PAYSTATION_REST_REQUEST_TRANS_FAIL_PERMIT_PAYBYPLATE = "paystation.rest.request.transaction.fail.permit.payByPlate";
    public static final String PAYSTATION_REST_REQUEST_TRANS_FAIL_PERMIT_PAYBYSPACE = "paystation.rest.request.transaction.fail.permit.payBySpace";
    public static final String PAYSTATION_REST_REQUEST_TRANS_FAIL_STOREANDFORWARD = "paystation.rest.request.transaction.fail.storeAndForward";
    
    public static final String PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PAYMENT_CASH = "paystation.rest.request.transaction.success.payment.cash";
    public static final String PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PAYMENT_CASHSMART = "paystation.rest.request.transaction.success.payment.cashSmart";
    public static final String PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PAYMENT_CASHVALUE = "paystation.rest.request.transaction.success.payment.cashValue";
    public static final String PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PAYMENT_CASHCREDIT = "paystation.rest.request.transaction.success.payment.cashCredit";
    public static final String PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PAYMENT_CREDIT = "paystation.rest.request.transaction.success.payment.credit";
    public static final String PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PAYMENT_SMART = "paystation.rest.request.transaction.success.payment.smart";
    public static final String PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PAYMENT_UNKNOWN = "paystation.rest.request.transaction.success.payment.unknown";
    public static final String PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PAYMENT_VALUE = "paystation.rest.request.transaction.success.payment.value";
    public static final String PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PERMIT_NA = "paystation.rest.request.transaction.success.permit.na";
    public static final String PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PERMIT_PAYANDDISPLAY = "paystation.rest.request.transaction.success.permit.payAndDisplay";
    public static final String PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PERMIT_PAYBYPLATE = "paystation.rest.request.transaction.success.permit.payByPlate";
    public static final String PAYSTATION_REST_REQUEST_TRANS_SUCCESS_PERMIT_PAYBYSPACE = "paystation.rest.request.transaction.success.permit.payBySpace";
    public static final String PAYSTATION_REST_REQUEST_TRANS_SUCCESS_STOREANDFORWARD = "paystation.rest.request.transaction.success.storeAndForward";
    
    public static final String PAYSTATION_REST_REQUEST_FAIL_PARAMETER_ERROR = "paystation.rest.request.fail.parameter.error";
    public static final String PAYSTATION_REST_REQUEST_FAIL_SIGNATURE_ERROR = "paystation.rest.request.fail.signature.error";
    public static final String PAYSTATION_REST_REQUEST_FAIL_SYSTEM_ERROR = "paystation.rest.request.fail.system.error";
    
    public static final String UI_FILE_UPLOAD_TOTAL = "ui.file.upload.total";
    public static final String UI_FILE_UPLOAD_SUCCESS_TOTAL = "ui.file.upload.success.total";
    public static final String UI_FILE_UPLOAD_SUCCESS_COUPON = "ui.file.upload.success.coupon";
    public static final String UI_FILE_UPLOAD_SUCCESS_PASSCARD = "ui.file.upload.success.passcard";
    public static final String UI_FILE_UPLOAD_SUCCESS_BANNEDCARD = "ui.file.upload.success.bannedCard";
    public static final String UI_FILE_UPLOAD_FAIL_TOTAL = "ui.file.upload.fail.total";
    public static final String UI_FILE_UPLOAD_FAIL_COUPON = "ui.file.upload.fail.coupon";
    public static final String UI_FILE_UPLOAD_FAIL_PASSCARD = "ui.file.upload.fail.passcard";
    public static final String UI_FILE_UPLOAD_FAIL_BANNEDCARD = "ui.file.upload.fail.bannedCard";
    
    public static final String UI_FILE_DOWNLOAD_TOTAL = "ui.file.download.total";
    public static final String UI_FILE_DOWNLOAD_SUCCESS_TOTAL = "ui.file.download.success.total";
    public static final String UI_FILE_DOWNLOAD_SUCCESS_COUPON = "ui.file.download.success.coupon";
    public static final String UI_FILE_DOWNLOAD_SUCCESS_PASSCARD = "ui.file.download.success.passcard";
    public static final String UI_FILE_DOWNLOAD_SUCCESS_REPORT = "ui.file.download.success.report";
    public static final String UI_FILE_DOWNLOAD_SUCCESS_WIDGET = "ui.file.download.success.widget";
    public static final String UI_FILE_DOWNLOAD_FAIL_TOTAL = "ui.file.download.fail.total";
    public static final String UI_FILE_DOWNLOAD_FAIL_COUPON = "ui.file.download.fail.coupon";
    public static final String UI_FILE_DOWNLOAD_FAIL_PASSCARD = "ui.file.download.fail.passcard";
    public static final String UI_FILE_DOWNLOAD_FAIL_REPORT = "ui.file.download.fail.report";
    public static final String UI_FILE_DOWNLOAD_FAIL_WIDGET = "ui.file.download.fail.widget";
    public static final String UI_FILE_UPLOAD_SIZE = "ui.file.upload..size";
    
    public static final String UI_DASHBOARD_RESPONSE_TIME_ALERTS = "ui.dashboard.responseTime.alerts";
    public static final String UI_DASHBOARD_RESPONSE_TIME_REVENUE = "ui.dashboard.responseTime.revenue";
    public static final String UI_DASHBOARD_RESPONSE_TIME_COLLECTIONS = "ui.dashboard.responseTime.collections";
    public static final String UI_DASHBOARD_RESPONSE_TIME_SETTLEDCARD = "ui.dashboard.responseTime.settledCard";
    public static final String UI_DASHBOARD_RESPONSE_TIME_PURCHASES = "ui.dashboard.responseTime.purchases";
    public static final String UI_DASHBOARD_RESPONSE_TIME_PAIDOCCUPANCY = "ui.dashboard.responseTime.paidOccupancy";
    public static final String UI_DASHBOARD_RESPONSE_TIME_TURNOVER = "ui.dashboard.responseTime.turnover";
    public static final String UI_DASHBOARD_RESPONSE_TIME_UTILIZATION = "ui.dashboard.responseTime.utilization";
    public static final String UI_DASHBOARD_RESPONSE_TIME_MAP = "ui.dashboard.responseTime.map";
    
    public static final String INTERNAL_KEY_INDEX = "key.internal.index";
    public static final String INTERNAL_KEY_VALID_DAYS = "key.internal.valid.days";
    public static final String INTERNAL_KEY_SPARE_COUNT = "key.internal.spare.count";
    public static final String EXTERNAL_KEY_INDEX = "key.external.index";
    public static final String EXTERNAL_KEY_VALID_DAYS = "key.external.valid.days";
    public static final String EXTERNAL_KEY_SPARE_COUNT = "key.external.spare.count";
    
    // Build Properties
    public static final String BUILD_VERSION = "build.version";
    public static final String BUILD_NUMBER = "build.number";
    public static final String BUILD_DATE = "build.date";
    public static final String BUILD_DEPLOYMENT_MODE = "build.deployment.mode";
    
    // URIs KPIListenerServiceImpl
    
    public static final String CONCORD = "ConcordProcessor";
    public static final String ALLIANCE = "AllianceProcessor";
    public static final String AUTHORIZENET = "AuthorizeNetProcessor";
    public static final String BLACKBOARD = "BlackBoardProcessor";
    public static final String DATAWIRE = "DatawireProcessor";
    public static final String FIRSTDATANASHVILLE = "FirstDataNashvilleProcessor";
    public static final String FIRSTHORIZON = "FirstHorizonProcessor";
    public static final String HEARTLAND = "HeartlandProcessor";
    public static final String HEARTLAND_SPPLUS = "HeartlandSPPlusProcessor";
    public static final String LINK2GOV = "Link2GovProcessor";
    public static final String MONERIS = "MonerisProcessor";
    public static final String NUVISION = "NuVisionProcessor";
    public static final String PARADATA = "ParadataProcessor";
    public static final String PAYMENTECH = "PaymentechProcessor";
    public static final String TOTALCARD = "TotalCardProcessor";
    public static final String CBORDGOLD = "CBordGoldProcessor";
    public static final String CBORDODYSSEY = "CBordOdysseyProcessor";
    public static final String ELAVON_VIACONEX = "ElavonViaConexProcessor";
    public static final String ELAVON_CONVERGE = "ElavonProcessor";
    public static final String TD_MERCHANT = "TDMerchantProcessor";
    public static final String CREDITCALL = "CreditcallProcessor";
    public static final String SMSPERMITALERTJOBSERVICEIMPL = "SmsPermitAlertJobServiceImpl";
    public static final String SENDSMS = "sendSMS";
    public static final String SENDCONFIRMATIONSMS = "sendConfirmationSMS";
    public static final String EMAILNOTIFICATIONTHREAD = "EmailNotificationThread";
    
    // URIs KPIFilter
    public static final String COUPON_UPLOAD = "/secure/accounts/coupons/importCoupons.html";
    public static final String PASSCARD_UPLOAD = "/secure/accounts/customerCards/importCards.html";
    public static final String BANNEDCARD_UPLOAD = "/secure/cardManagement/importBannedCard.html";
    public static final String COUPON_DOWNLOAD = "/secure/accounts/coupons/exportCoupons.html";
    public static final String PASSCAR_DDOWNLOAD = "/secure/accounts/customerCards/exportCards.html";
    public static final String REPORT_DOWNLOAD = "/secure/reporting/viewReport.html";
    public static final String WIDGET_DOWNLOAD = "/secure/dashboard/widgetCSV.html";
    public static final String XMLRPC_PS2 = "/XMLRPC_PS2";
    public static final String PAYSTATION_REST = "/PayStation";
    public static final String WIDGET = "/secure/dashboard/widget.html";
    public static final String REST_COUPONS = "/REST/Coupons";
    public static final String UPLOAD = "/upload";
    public static final String DOWNLOAD = "/download";
    
    // Request Types KPIFilter
    public static final String AUDIT = "Audit";
    public static final String TRANSACTION = "Transaction";
    public static final String AUTHORIZE_REQUEST = "AuthorizeRequest";
    public static final String EVENT = "Event";
    public static final String HEARTBEAT = "HeartBeat";
    public static final String LOTSETTING_SUCCESS = "LotSettingSuccess";
    public static final String PLATE_RPT_REQ = "PlateRptReq";
    public static final String AUTHORIZE_CARD_REQUEST = "AuthorizeCardRequest";
    public static final String STALLINFO_REQUEST = "StallInfoRequest";
    public static final String STALLREPORT_REQUEST = "StallReportRequest";
    
    // Transaction Types KPIFilter
    public static final String PAYMENT_TYPE = "paymentType";
    public static final String IS_STOREFORWARD = "isStoreForward";
    public static final String PERMIT_ISSUE_TYPE = "permitIssueType";
    
    // BOSS Types KPIFilter
    public static final String TYPE_PARAMETER = "type";
    
    public static final String LOT_SETTING = "lotsetting";
    public static final String FILE_UPLOAD = "FileUpload";
    public static final String PAYMENTSTATION_QUERY = "paymentstationquery";
    public static final String OFFLINE_TRANSACTION = "OffLineXMLTransaction";
    
    public static final String TYPE_LOT_SETTING = "lotsetting";
    public static final String TYPE_PUBLIC_KEY_REQUEST = "publicKey";
    public static final String TYPE_BAD_CREDITCARD = "badcreditcard";
    public static final String TYPE_BAD_SMARTCARD = "badsmartcard";
    
    // Attributes KPIFilter
    public static final String XMLRPC_REQUEST = "XMLRPC_REQUEST";
    public static final String XMLRPC_STATUS_CODE = "XMLRPCSTATUSCODE";
    
    // Checks for failed xmlrpc requests, can be found in Handlers
    public static final String ERROR_INVALID_PAYSTATION = "Pay station is not provisioned";
    public static final String INVALID_STALL_RANGE_STRING = "InvalidStallRange";
    public static final String INVALID_REQUEST_STRING = "InvalidRequest";
    public static final String MISSING_REPORT_DATE = "Missing ReportDate";
    public static final String START_POSITION_TOO_BIG = "StartPosition should be between 0 and 99999999";
    public static final String RECORD_RETURN_TOO_BIG = "Max returned records should be between 0 and  5000";
    
    public static final String[] ORIGINAL_NAMES = { "elavon", "elavon (Converge)" };
    public static final String[] NEW_NAMES = { "elavon.viaconex", "elavon.converge" };
    
    private KPIConstants() {
        
    }
}
