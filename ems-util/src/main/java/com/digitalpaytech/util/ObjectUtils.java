package com.digitalpaytech.util;

public final class ObjectUtils {
    private ObjectUtils() {
        
    }
    
    public static boolean nullableEquals(final Object o1, final Object o2) {
        boolean result = false;
        if (o1 == null) {
            if (o2 == null) {
                result = true;
            }
        } else {
            if (o2 != null) {
                result = o1.equals(o2);
            }
        }
        
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public static int nullableCompare(final Object o1, final Object o2) {
        int result = -1;
        if (o1 == null) {
            if (o2 == null) {
                result = 0;
            }
        } else {
            if (o2 == null) {
                result = 1;
            } else {
                result = ((Comparable<Object>) o1).compareTo(o2);
            }
        }
        
        return result;
    }
}
