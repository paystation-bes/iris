package com.digitalpaytech.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

/**
 * Log all un-handled Exception with in Spring context
 * 
 * @author wittawatv
 *
 */
public class SpringExceptionLogger implements HandlerExceptionResolver, Ordered {
	private static Logger log = Logger.getLogger(SpringExceptionLogger.class);
	
	/**
	 * Put this handler in the front of Exception Handlers Stack.
	 * 
	 */
	@Override
	public int getOrder() {
		return Integer.MIN_VALUE;
	}

	/**
	 * Simply log the Exception's stacktrace and return "null" to let other Handlers get the chance to handle the Exception.
	 * 
	 */
	@Override
	public ModelAndView resolveException(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex) {
		log.error("Unhandled Exception Thrown...", ex);
		
		return null;
	}

}
