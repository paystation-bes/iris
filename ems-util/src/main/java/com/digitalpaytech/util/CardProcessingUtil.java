package com.digitalpaytech.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.digitalpaytech.domain.MerchantAccount;
import com.digitalpaytech.domain.PaymentCard;
import com.digitalpaytech.domain.Processor;
import com.digitalpaytech.domain.ProcessorTransaction;

public final class CardProcessingUtil {
    private static final String ALPHANUMERIC_CHARS = "ijAE23F1GHopqIJ45KLhNOPstQYZ6BCxyD7vw8mn9abcUVWXdkRSTlruzMefg";
    private static final int BASE_61 = 61;
    
    // Copied from EMS 6.3.11 com.digitalpioneer.util.CheckSum.CRC16
    
    /**
     * scrambler lookup table for fast computation.
     */
    private static final int[] CRC_TABLE = { 0x0, 0x8005, 0x800f, 0xa, 0x801b, 0x1e, 0x14, 0x8011, 0x8033, 0x36, 0x3c, 0x8039, 0x28, 0x802d, 0x8027,
        0x22, 0x8063, 0x66, 0x6c, 0x8069, 0x78, 0x807d, 0x8077, 0x72, 0x50, 0x8055, 0x805f, 0x5a, 0x804b, 0x4e, 0x44, 0x8041, 0x80c3, 0xc6, 0xcc,
        0x80c9, 0xd8, 0x80dd, 0x80d7, 0xd2, 0xf0, 0x80f5, 0x80ff, 0xfa, 0x80eb, 0xee, 0xe4, 0x80e1, 0xa0, 0x80a5, 0x80af, 0xaa, 0x80bb, 0xbe, 0xb4,
        0x80b1, 0x8093, 0x96, 0x9c, 0x8099, 0x88, 0x808d, 0x8087, 0x82, 0x8183, 0x186, 0x18c, 0x8189, 0x198, 0x819d, 0x8197, 0x192, 0x1b0, 0x81b5,
        0x81bf, 0x1ba, 0x81ab, 0x1ae, 0x1a4, 0x81a1, 0x1e0, 0x81e5, 0x81ef, 0x1ea, 0x81fb, 0x1fe, 0x1f4, 0x81f1, 0x81d3, 0x1d6, 0x1dc, 0x81d9, 0x1c8,
        0x81cd, 0x81c7, 0x1c2, 0x140, 0x8145, 0x814f, 0x14a, 0x815b, 0x15e, 0x154, 0x8151, 0x8173, 0x176, 0x17c, 0x8179, 0x168, 0x816d, 0x8167, 0x162,
        0x8123, 0x126, 0x12c, 0x8129, 0x138, 0x813d, 0x8137, 0x132, 0x110, 0x8115, 0x811f, 0x11a, 0x810b, 0x10e, 0x104, 0x8101, 0x8303, 0x306, 0x30c,
        0x8309, 0x318, 0x831d, 0x8317, 0x312, 0x330, 0x8335, 0x833f, 0x33a, 0x832b, 0x32e, 0x324, 0x8321, 0x360, 0x8365, 0x836f, 0x36a, 0x837b, 0x37e,
        0x374, 0x8371, 0x8353, 0x356, 0x35c, 0x8359, 0x348, 0x834d, 0x8347, 0x342, 0x3c0, 0x83c5, 0x83cf, 0x3ca, 0x83db, 0x3de, 0x3d4, 0x83d1, 0x83f3,
        0x3f6, 0x3fc, 0x83f9, 0x3e8, 0x83ed, 0x83e7, 0x3e2, 0x83a3, 0x3a6, 0x3ac, 0x83a9, 0x3b8, 0x83bd, 0x83b7, 0x3b2, 0x390, 0x8395, 0x839f, 0x39a,
        0x838b, 0x38e, 0x384, 0x8381, 0x280, 0x8285, 0x828f, 0x28a, 0x829b, 0x29e, 0x294, 0x8291, 0x82b3, 0x2b6, 0x2bc, 0x82b9, 0x2a8, 0x82ad, 0x82a7,
        0x2a2, 0x82e3, 0x2e6, 0x2ec, 0x82e9, 0x2f8, 0x82fd, 0x82f7, 0x2f2, 0x2d0, 0x82d5, 0x82df, 0x2da, 0x82cb, 0x2ce, 0x2c4, 0x82c1, 0x8243, 0x246,
        0x24c, 0x8249, 0x258, 0x825d, 0x8257, 0x252, 0x270, 0x8275, 0x827f, 0x27a, 0x826b, 0x26e, 0x264, 0x8261, 0x220, 0x8225, 0x822f, 0x22a, 0x823b,
        0x23e, 0x234, 0x8231, 0x8213, 0x216, 0x21c, 0x8219, 0x208, 0x820d, 0x8207, 0x202, };
    
    private static final Logger LOG = Logger.getLogger(CardProcessingUtil.class);
    private static List<String> exampleCardNumbersList;
    private static Map<String, Integer> creditCardMap;
    
    static {
        loadExampleCardNumbers();
        loadCreditCardMap();
    }
    
    private CardProcessingUtil() {
    }
    
    private static void loadCreditCardMap() {
        creditCardMap = new HashMap<String, Integer>();
        creditCardMap.put(CardProcessingConstants.NAME_AMEX, CardProcessingConstants.CC_TYPE_ID_AMEX);
        creditCardMap.put(CardProcessingConstants.NAME_VISA, CardProcessingConstants.CC_TYPE_ID_VISA);
        creditCardMap.put(CardProcessingConstants.NAME_MASTERCARD, CardProcessingConstants.CC_TYPE_ID_MASTERCARD);
        creditCardMap.put(CardProcessingConstants.NAME_DISCOVER, CardProcessingConstants.CC_TYPE_ID_DISCOVER);
        creditCardMap.put(CardProcessingConstants.NAME_DINERS, CardProcessingConstants.CC_TYPE_ID_DINERS);
        creditCardMap.put(CardProcessingConstants.NAME_DINERS_WO_CLUB, CardProcessingConstants.CC_TYPE_ID_DINERS);
        creditCardMap.put(CardProcessingConstants.NAME_CREDIT_CARD, CardProcessingConstants.CC_TYPE_ID_CREDIT_CARD);
        creditCardMap.put(CardProcessingConstants.NAME_JCB, CardProcessingConstants.CC_TYPE_ID_JCB);
        creditCardMap.put(CardProcessingConstants.NAME_UNION_PAY, CardProcessingConstants.CC_TYPE_ID_UNION_PAY);
    }
    
    // Copies from EMS 6.3.11 com.digitalpioneer.appservice.card.Impl.CardAppServiceImpl
    /*
     * 5105105105105100 - MasterCard
     * 5555555555554444 - MasterCard
     * 36121212121212 - MasterCard
     * 4222222222222 - VISA
     * 4111111111111111 - VISA
     * 4012888888881881 - VISA
     * 378282246310005 - American Express
     * 371449635398431 - American Express
     * 378734493671000 - Amex Corporate
     * 38520000023237 - Dinners Club
     * 30569309025904 - Dinners Club
     * 6011111111111117 - Discover
     * 6011000990139424 - Discover
     * 3530111333300000 - JCB
     * 3566002020360505 - JCB
     * 201400000000009 - Enroute
     */
    public static void loadExampleCardNumbers() {
        BufferedReader br = null;
        exampleCardNumbersList = new ArrayList<String>();
        try {
            br = new BufferedReader(new InputStreamReader(CardProcessingUtil.class.getResourceAsStream("/example_card_numbers")));
            String s;
            while ((s = br.readLine()) != null) {
                exampleCardNumbersList.add(s.trim());
            }
        } catch (IOException ioe) {
            final String msg = "CardProcessingUtil, cannot load 'example_card_numbers.txt'";
            LOG.error(msg, ioe);
            throw new RuntimeException(msg, ioe);
            
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    LOG.error(e);
                }
            }
        }
    }
    
    public static List<String> getExampleCardNumbers() {
        return exampleCardNumbersList;
    }
    
    public static Integer getCreditCardTypeId(final String customerCardTypeName) {
        return creditCardMap.get(customerCardTypeName);
    }
    
    /*
     * Copies from EMS 6.3.12 com.digitalpioneer.mvc.validation.CardTypeValidator, checkTrack2RegEx.
     */
    public static boolean isValidPattern(final String track2Pattern) {
        try {
            Pattern.compile(track2Pattern);
            
        } catch (PatternSyntaxException pse) {
            LOG.error(pse.getMessage());
            return false;
        }
        return true;
    }
    
    /*
     * Copies the following 3 methods from EMS 6.3.11 com.digitalpioneer.appservice.card.Impl.CardAppServiceImpl
     * Create a list of sample card numbers in 'example_card_numbers' and make sure the pattern does not
     * verify these cards. We don't want any overlap of patterns.
     */
    public static boolean isExistTrack2Pattern(final String pattern) {
        String tmpCardNumber = null;
        final Iterator<String> it = exampleCardNumbersList.iterator();
        while (it.hasNext()) {
            tmpCardNumber = it.next();
            if (checkCustomAccountNumber(tmpCardNumber, pattern)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * @return boolean Return 'true' if the input accountNumber pattern matches with list of existing track 2 patterns.
     */
    private static boolean checkCustomAccountNumber(final String accountNumber, final String track2pattern) {
        final String accountPattern = extractAccountNumber(track2pattern, CardProcessingConstants.TRACK_2_DELIMITER);
        return checkRegEx(accountPattern, accountNumber);
    }
    
    private static boolean checkRegEx(final String regex, final String value) {
        try {
            final Pattern pattern = Pattern.compile("^" + regex + "$");
            final Matcher matcher = pattern.matcher(value.trim());
            
            return matcher.find();
        } catch (PatternSyntaxException pse) {
            LOG.warn(pse);
        }
        return false;
    }
    
    /**
     * Copies from EMS 6.3.11 com.digitalpioneer.domain.PreAuth
     * This method divides by 100 and return dollar amount.
     * e.g. 1 -> 0.01
     * 100 -> 1.00
     * 
     * @param amount
     *            In cents, 100 based.
     * @return BigDecimal Dollar amount.
     */
    public static BigDecimal getCentsAmountInDollars(final int amount) {
        return new BigDecimal(amount).divide(new BigDecimal(StandardConstants.DOLLAR_AMOUNT_IN_CENTS_1)).setScale(2, BigDecimal.ROUND_HALF_UP);
    }
    
    /**
     * This method multiples by 100 and return based 100 cents amount.
     * e.g. 1 -> 100
     * 100 -> 10000
     * 
     * @param amount
     *            In dollars.
     * @return BigDecimal Cents amount.
     */
    public static BigDecimal getDollarsAmountInCents(final String amount) {
        return WebCoreUtil.convertToBase100Value(amount);
    }
    
    /**
     * This method multiples by 100 and return based 100 cents amount.
     * e.g. 1 -> 100
     * 100 -> 10000
     * 
     * @param amount
     *            In dollars.
     * @return BigDecimal Cents amount.
     */
    public static BigDecimal getDollarsAmountInCents(final int amount) {
        return WebCoreUtil.convertToBase100Value(String.valueOf(amount));
    }
    
    // Copies from EMS 6.3.11 com.digitalpioneer.util.Track2Card
    public static String extractAccountNumber(final String track2Data, final char delimiter) {
        final int iPos = track2Data.indexOf(delimiter);
        if (iPos > 0) {
            return track2Data.substring(0, iPos);
        }
        return track2Data;
    }
    
    // Copies from EMS 6.3.11 com.digitalpioneer.util.Track2Card
    public static String getLast4DigitsOfAccountNumber(final String track2Data, final char delimiter) {
        final String accountNumber = extractAccountNumber(track2Data, delimiter);
        if (accountNumber.length() > WebCoreConstants.MAX_NUMBERS_LAST_DIGITS) {
            return accountNumber.substring(accountNumber.length() - WebCoreConstants.MAX_NUMBERS_LAST_DIGITS, accountNumber.length());
        }
        return accountNumber;
    }
    
    public static String getFirst6DigitsOfAccountNumber(final String track2Data, final char delimiter) {
        final String accountNumber = extractAccountNumber(track2Data, delimiter);
        if (accountNumber.length() > WebCoreConstants.MAX_NUMBERS_FIRST_DIGITS) {
            return accountNumber.substring(0, WebCoreConstants.MAX_NUMBERS_FIRST_DIGITS);
        }
        return accountNumber;
    }
    
    public static String getFirst6Last4DigitsOfAccountNumber(final String track2Data, final char delimiter) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append(getFirst6DigitsOfAccountNumber(track2Data, delimiter));
        bdr.append("******").append(getLast4DigitsOfAccountNumber(track2Data, delimiter));
        return bdr.toString();
    }
    
    public static String createAccountNumber(final String cardNumber, final String cardExpiryMMYY) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append(cardNumber).append("=").append(cardExpiryMMYY.substring(2)).append(cardExpiryMMYY.substring(0, 2));
        return bdr.toString();
    }
    
    public static String covertLast4DigitsOfCardNumberToString(final int last4DigitsOfCardNumber) {
        final int length = String.valueOf(last4DigitsOfCardNumber).length();
        if (length > 0) {
            final StringBuilder builder = new StringBuilder();
            for (int i = WebCoreConstants.MAX_NUMBERS_LAST_DIGITS; i > length; i--) {
                builder.append(WebCoreConstants.ZERO);
            }
            return builder.append(last4DigitsOfCardNumber).toString();
        } else {
            return "";
        }
    }
    
    // Copied from EMS 6.3.11 com.digitalpioneer.util.CheckSum.LRC
    public static byte[] computeLRC(final byte[] b) {
        //loop, calculating CRC for each byte of the string 
        byte lrc = 0x0000;
        for (int i = 0; i < b.length; i++) {
            lrc ^= b[i];
        }
        final byte[] bb = new byte[1];
        bb[0] = lrc;
        return bb;
    }
    
    // Copied from EMS 6.3.11 com.digitalpioneer.util.HexUtil
    /**
     * Replace binary data with hex notation inside square brackets.
     * 
     * @param str
     * @return
     */
    public static String replaceBinary(final String str) {
        final char[] chars = new char[str.length()];
        str.getChars(0, str.length(), chars, 0);
        final StringBuffer result = new StringBuffer();
        for (char b : chars) {
            if (b >= WebCoreConstants.PRINTABLE_CHAR_START && b <= WebCoreConstants.PRINTABLE_CHAR_END) {
                result.append(b);
            } else {
                // unprintable char
                result.append("[");
                result.append(Integer.toHexString(b));
                result.append("]");
            }
        }
        return result.toString();
    }
    
    /**
     * Convert the whole string into hex string. (eg. "12AB" -> 31324142)
     * 
     * @param str
     * @return
     */
    public static String toHexString(final String str) {
        final char[] chars = new char[str.length()];
        str.getChars(0, str.length(), chars, 0);
        final StringBuffer result = new StringBuffer();
        for (char b : chars) {
            String hex = Integer.toHexString(b);
            if (hex.length() < 2) {
                hex = WebCoreConstants.ZERO + hex;
            }
            result.append(hex);
        }
        return result.toString();
    }
    
    public static String toHexString(final byte[] bytes) {
        final StringBuilder result = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            result.append(Integer
                    .toString((bytes[i] & WebCoreConstants.BITCROP_BYTE) + WebCoreConstants.BITCROP_17TH, WebCoreConstants.BITCOUNT_SHORT)
                    .substring(1));
        }
        return result.toString();
        
    }
    
    public static byte[] computeCRC16(final byte[] b) {
        //loop, calculating CRC for each byte of the string 
        int crc = 0x0000;
        int temp;
        for (int i = 0; i < b.length; i++) {
            
            temp = ((int) b[i]) & WebCoreConstants.BITCROP_BYTE;
            temp = temp ^ (crc >> WebCoreConstants.BITCOUNT_BYTE);
            crc = (crc << WebCoreConstants.BITCOUNT_BYTE) ^ CRC_TABLE[temp];
            crc &= WebCoreConstants.BITCROP_SHORT;
        }
        
        final byte[] result = new byte[2];
        result[1] = (byte) (crc & WebCoreConstants.BITCROP_BYTE);
        result[0] = (byte) ((crc & WebCoreConstants.BITCROP_SECOND_BYTE) >> WebCoreConstants.BITCOUNT_BYTE);
        return result;
        //return (new Integer(crc).toString().getBytes());
    }
    
    public static int computeIntCRC(final byte[] b) {
        //loop, calculating CRC for each byte of the string 
        int crc = 0x0000;
        int temp;
        for (int i = 0; i < b.length; i++) {
            temp = ((int) b[i]) & WebCoreConstants.BITCROP_BYTE;
            temp = temp ^ (crc >> WebCoreConstants.BITCOUNT_BYTE);
            crc = (crc << WebCoreConstants.BITCOUNT_BYTE) ^ CRC_TABLE[temp];
            crc &= WebCoreConstants.BITCROP_SHORT;
        }
        
        return crc;
    }
    
    public static int computeIntCRC(final String string) {
        return computeIntCRC(string.getBytes());
    }
    
    // Copied from EMS 6 com.digitalpioneer.domain.Reversal
    public static int getAmountInCents(final float amountInDollars) {
        return Math.round(StandardConstants.DOLLAR_AMOUNT_IN_CENTS_FLOAT_1 * amountInDollars);
    }
    
    /**
     * @param merchantAccount
     *            e.g. field 1 = Terminal ID - 00024242
     *            field 2 = Merchant ID - 849
     * 
     * @return String e.g. 00024242-849
     */
    public static String createRedisKey(final MerchantAccount merchantAccount) {
        final StringBuilder bdr = new StringBuilder();
        bdr.append(merchantAccount.getField1()).append(StandardConstants.STRING_DASH).append(merchantAccount.getField2());
        return bdr.toString();
    }
    
    /**
     * The MD5 message-digest algorithm is a widely used cryptographic hash function producing a 128-bit (16-byte) hash value,
     * typically expressed as a 32 digit hexadecimal number. MD5 has been utilized in a wide variety of security applications. It
     * is also commonly used to check data integrity.
     * 
     * @param fullPathFileName
     *            e.g. /opt/tomcat/3233-400400.xml.hpz
     * @return String compute MD5 message digest. Returns null if encounters a problem.
     */
    public static String createMD5Checksum(final String fullPathFileName) throws IOException, NoSuchAlgorithmException {
        return createChecksum("MD5", fullPathFileName);
    }
    
    public static String createChecksum(final String algorithm, final String fullPathFileName) throws IOException, NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance(algorithm);
        
        InputStream fis = null;
        String checkSum = null;
        try {
            fis = new FileInputStream(fullPathFileName);
            final byte[] bytesArr = new byte[StandardConstants.ONE_KILOBYTE];
            
            int nread = 0;
            while ((nread = fis.read(bytesArr)) != -1) {
                md.update(bytesArr, 0, nread);
            }
            
            final byte[] mdbytes = md.digest();
            // Convert the byte to hex format.
            final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < mdbytes.length; i++) {
                sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            checkSum = sb.toString();
        } catch (FileNotFoundException fnfe) {
            throw new IOException();
            //LOG.error(fullPathFileName + " not found.", fnfe);
        } catch (IOException ioe) {
            throw new IOException();
            //LOG.error(fullPathFileName + " causes IO Exception.", ioe);
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException ioe) {
                    LOG.error(ioe);
                }
            }
        }
        return checkSum;
    }
    
    /**
     * Create an unique alphanumeric 4 characters that's generated from input PointOfSale Id.
     * It uses Base 61 hash algorithm and no random access, same PointOfSale Id would always results same codes.
     * If generated code is less than 4 characters, append 0 (zero) until 4 characters.
     * 
     * e.g. PointOfSale Id = 88116, code = WvO , append 0 = WvO0
     * PointOfSale Id = 238327, code = zzz , append 0 = zzz0
     * PointOfSale Id = 238328, code = BAAA
     *
     * Heartland POS 8583 V 15.2 Oct-2015 Spec:
     * IID: Unique Account Input Device Identifier – This entry is sent in all financial messages
     * (1100, 1200, 1220, 1300 and 1420) to uniquely identify a device where account data is
     * acquired (swiped, scanned, or manually entered). The format for this entry is four
     * alphanumeric characters left-justified and space-filled (anp4), and is required for all MCCs.
     * Example: If a location has three inside POS devices, eight outside pumps and one automated
     * car wash, then it will have 12 unique IIDs, corresponding to one IID for each
     * payment location.
     *
     * The implementation is from:
     * http://coddicted.com/design-a-tiny-url-or-url-shortener/
     * http://www.geeksforgeeks.org/how-to-design-a-tiny-url-or-url-shortener/
     * 
     * @param Integer
     *            PointOfSale Id
     * @return String 4 characters hash code
     */
    public static String generateAlphanumericHash(final Integer pointOfSaleId) {
        final StringBuilder hashBdr = new StringBuilder();
        Integer tmpId = pointOfSaleId;
        while (tmpId > 0) {
            hashBdr.append(ALPHANUMERIC_CHARS.charAt(tmpId % BASE_61));
            tmpId /= BASE_61;
        }
        final String hash = hashBdr.reverse().toString();
        if (hash.length() < StandardConstants.CONSTANT_4) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append(hash);
            // Append space when the result is too short.
            while (bdr.length() < StandardConstants.CONSTANT_4) {
                bdr.append(WebCoreConstants.VALIDATION_MIN_LENGTH_0);
            }
            return bdr.toString();
        }
        if (hash.length() > StandardConstants.CONSTANT_4) {
            return hash.substring(hash.length() - StandardConstants.CONSTANT_4, hash.length());
        }
        return hash;
    }
    
    /**
     * Verify if input processId is in CardProcessingConstants.SINGLE_PHASE_PROCESSOR_IDS array.
     * 
     * @param processorId
     * @return boolean true if input processorId is in CardProcessingConstants.SINGLE_PHASE_PROCESSOR_IDS.
     */
    public static boolean isSinglePhaseProcessor(final int processorId) {
        for (int i = 0; i < CardProcessingConstants.SINGLE_PHASE_PROCESSOR_IDS.length; i++) {
            if (processorId == CardProcessingConstants.SINGLE_PHASE_PROCESSOR_IDS[i]) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * e.g. AuthorizationNumber = 3589FS:112 return true
     * AuthorizationNumber = 3589FS return false
     * AuthorizationNumber = 3589FS: return false
     * 
     * @param authorizationNumber
     *            It should contain authorization number + ':' + PreAuthId
     * @return boolean Return true only if authorization number is on the left, a colon and a PreAuthId.
     */
    public static boolean hasAuthorizationNumberAndPreAuthId(final String authorizationNumber) {
        if (authorizationNumber == null || authorizationNumber.indexOf(StandardConstants.STRING_COLON) == -1) {
            return false;
        }
        final String[] arr = authorizationNumber.split(StandardConstants.STRING_COLON);
        if (arr.length != StandardConstants.CONSTANT_2 || StringUtils.isBlank(arr[0]) || StringUtils.isBlank(arr[1])) {
            return false;
        }
        return true;
    }
    
    public static boolean isNotNullAndIsLink(final MerchantAccount merchantAccount) {
        return merchantAccount != null && merchantAccount.getIsLink() ? true : false;
    }
    
    public static boolean isBlankOrZero(final String value) {
        if (StringUtils.isBlank(value) || value.equals(StandardConstants.STRING_ZERO)) {
            return true;
        }
        return false;
    }
    
    public static String createCardProcessorPausedExceptionMessage(final int processorId, final String merchantAccountName) {
        final StringBuilder bdr = new StringBuilder(52);
        bdr.append("Processor id: ").append(processorId).append(" is paused. Merchant account name is: ").append(merchantAccountName);
        return bdr.toString();
    }
    
    public static ProcessorTransaction buildRefundProcTrans(final ProcessorTransaction origPtd) {
        final ProcessorTransaction refundPtd = new ProcessorTransaction(origPtd);
        refundPtd.setId((Long) null);
        refundPtd.setAmount(origPtd.getAmount());
        refundPtd.setIsApproved(false);
        refundPtd.setAuthorizationNumber(null);
        refundPtd.setProcessingDate(null);
        refundPtd.setProcessorTransactionId(null);
        refundPtd.setReferenceNumber(StandardConstants.STRING_ZERO);
        refundPtd.setPaymentCards(null);
        return refundPtd;
    }
    
    public static String getFirstCardTypeOrNA(final Collection<PaymentCard> paymentCards) {
        if (paymentCards != null && !paymentCards.isEmpty()) {
            return new ArrayList<PaymentCard>(paymentCards).get(StandardConstants.CONSTANT_0).getCreditCardType().getName();
        }
        return WebCoreConstants.N_A_STRING;
    }
    
    public static boolean isCreditcallRequest( final String processorName, final Processor... creditcallProcessors) {
        if (StringUtils.isBlank(processorName) || creditcallProcessors == null || creditcallProcessors.length == 0) {
            return false;
        }
        return Arrays.stream(creditcallProcessors).anyMatch(creditcallProcessor -> creditcallProcessor != null 
                && creditcallProcessor.getName().equalsIgnoreCase(processorName));
    }
    
    /**
     * @param code e.g. "accepted" or "declined"
     * @param message String message returned from the processor.
     * @param acceptedMessages, e.g. "TransactionAlreadySettled", "Transaction Already Settled", 
     *                               "TransactionAlreadyVoided", "Transaction Already Voided",
     *                               "TransactionAlreadyRefunded", "Transaction Already Refunded"
     * @return boolean Return true if code = "accepted" or code = "declined" but message contains "Transaction Already Settled/Refunded/Voided".
     */
    public static String getAcceptedIfAcceptedOrAlreadyDone(final String code, final String message, final String... acceptedMessages) {
        if (code.equalsIgnoreCase(StandardConstants.STRING_STATUS_CODE_DECLINED)
                && StringUtils.isNotBlank(message)
                && Arrays.asList(acceptedMessages).contains(message.toLowerCase())) {
            return StandardConstants.STRING_STATUS_CODE_ACCEPTED;
        }
        return code;
    }
    
    /**
     * Check in String cardEaseDataJson if it contains 'card type':'fswipe'.
     * @param messageHelper MessageHelper for retrieving data from messages.properties.
     * @param cardEaseDataJson String data within <CardEaseData>...</CardEaseData>. It could be null.
     */
    public static boolean hasFSwipeCardType(final MessageHelper messageHelper, final String cardEaseDataJson) {
        if (StringUtils.isBlank(cardEaseDataJson) || messageHelper == null) {
            return false;
        }
        final String[] jsonArr = cardEaseDataJson.toLowerCase().split(StandardConstants.STRING_COMMA);
        final List<String> list = Arrays.asList(jsonArr);
        
        final Optional<String> optResult = list.stream()
            .filter(data -> data.contains(messageHelper.getMessage("label.cardeasedata.cardtype"))
                            && data.contains(messageHelper.getMessage("label.cardeasedata.fswipe")))
            .findFirst();
        
        if (optResult.isPresent()) {
            return true;
        }
        return false;
    }
    
}
