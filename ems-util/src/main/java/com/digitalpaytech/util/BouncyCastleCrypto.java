package com.digitalpaytech.util;

import com.digitalpaytech.exception.CryptoException;

public final class BouncyCastleCrypto extends Crypto {
    private static final String JCE_PROVIDER = "BC";
    private static final String KEYSTORE_TYPE = "JCEKS";
    private static final String KEYSTORE_PROVIDER = "SunJCE";
    
    protected BouncyCastleCrypto(final String keystorePath) throws CryptoException {
        super(keystorePath);
        initializeKeystore(KEYSTORE_TYPE, KEYSTORE_PROVIDER);
    }
    
    public String getJceProvider() {
        return JCE_PROVIDER;
    }
}
