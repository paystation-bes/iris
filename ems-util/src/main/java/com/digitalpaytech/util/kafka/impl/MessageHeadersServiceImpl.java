package com.digitalpaytech.util.kafka.impl;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.kafka.AbstractMessageProducer;
import com.digitalpaytech.util.kafka.MessageHeadersService;
import com.digitalpaytech.util.kafka.MessageValues;

@Service
public class MessageHeadersServiceImpl implements MessageHeadersService {
    private static final Logger LOG = Logger.getLogger(MessageHeadersServiceImpl.class);
    
    @Override
    public final byte[] embedHeaders(final MessageValues original, final String... headers) throws UnsupportedEncodingException {
        final byte[][] headerValues = new byte[headers.length][];
        int n = 0;
        int headerCount = 0;
        int headersLength = 0;
        for (String header : headers) {
            final Object value = original.get(header) == null ? null : original.get(header);
            if (value != null) {
                headerValues[n] = ((String) value).getBytes(StandardConstants.HTTP_UTF_8_ENCODE_CHARSET);
                headerCount++;
                headersLength += header.length() + headerValues[n++].length;
            } else {
                headerValues[n++] = null;
            }
        }
        // 0xff, n(1), [ [lenHdr(1), hdr, lenValue(4), value] ... ]
        final byte[] newPayload 
            = new byte[((byte[]) original.getPayload()).length + headersLength + headerCount * StandardConstants.CONSTANT_5 + StandardConstants.CONSTANT_2];
        final ByteBuffer byteBuffer = ByteBuffer.wrap(newPayload);
        // signal new format
        byteBuffer.put((byte) 0xff); 
        byteBuffer.put((byte) headerCount);
        for (int i = 0; i < headers.length; i++) {
            if (headerValues[i] != null) {
                byteBuffer.put((byte) headers[i].length());
                byteBuffer.put(headers[i].getBytes(StandardConstants.HTTP_UTF_8_ENCODE_CHARSET));
                byteBuffer.putInt(headerValues[i].length);
                byteBuffer.put(headerValues[i]);
            }
        }

        byteBuffer.put((byte[]) original.getPayload());
        return byteBuffer.array();
    }

    /*
     * e.g. �contentType   ?"text/plain"?originalContentType    "application/json;charset=UTF-8"
     *      ->
     *      ["contentType", "text/plain"]
     */
    private void addNameValuePair(final String name, final StringBuilder headers, final Map<String, String> map) {
        final int begin = headers.indexOf(name);
        final int beginDoubleQ = headers.indexOf(StandardConstants.STRING_DOUBLE_QUOTES, begin);
        // '+ 1' is to advance 1 char and it's for the begin and end. 
        final int endDoubleQ = headers.indexOf(StandardConstants.STRING_DOUBLE_QUOTES, beginDoubleQ + 1) + 1;
        map.put(name, headers.substring(beginDoubleQ, endDoubleQ));
    }
    
    
    @Override
    public final MessageValues extractHeaders(final String messageWithHeaders) throws UnsupportedEncodingException {
        if (messageWithHeaders.indexOf(AbstractMessageProducer.CONTENT_TYPE) != StandardConstants.INT_MINUS_ONE 
                && messageWithHeaders.indexOf(AbstractMessageProducer.ORIGINAL_CONTENT_TYPE) != StandardConstants.INT_MINUS_ONE
                && messageWithHeaders.indexOf(StandardConstants.STRING_CURLY_BRACE_OPEN) != StandardConstants.INT_MINUS_ONE
                && messageWithHeaders.indexOf(StandardConstants.STRING_CURLY_BRACE_CLOSE) != StandardConstants.INT_MINUS_ONE) {
            
            final int idx = messageWithHeaders.indexOf(StandardConstants.STRING_CURLY_BRACE_OPEN);
            final String[] headersAndPayload = new String[] { messageWithHeaders.substring(0, idx), 
                                                              messageWithHeaders.substring(idx, messageWithHeaders.length()), };
            final StringBuilder bdr = new StringBuilder();
            bdr.append(headersAndPayload[0]);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Headers string: " + bdr.toString());
                LOG.debug("Payload: " + headersAndPayload[1]);
            }
            
            final Map<String, String> headersMap = new ConcurrentHashMap<String, String>();
            addNameValuePair(AbstractMessageProducer.CONTENT_TYPE, bdr, headersMap);
            addNameValuePair(AbstractMessageProducer.ORIGINAL_CONTENT_TYPE, bdr, headersMap);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Do we have headers? " + headersMap);
            }
            
            final String payload = headersAndPayload[1];
            
            final MessageValues mv = new MessageValues(headersMap);
            mv.setPayload(payload);
            return mv;
        }

        final MessageValues mv = new MessageValues(new ConcurrentHashMap<>());
        mv.setPayload(messageWithHeaders);
        return mv;
    }    
}
