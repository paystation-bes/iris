package com.digitalpaytech.util.kafka;

import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.json.JSON;

@Component("abstractMessageProducer")
public abstract class AbstractMessageProducer {
    
    public static final String CONTENT_TYPE = "contentType";
    public static final String ORIGINAL_CONTENT_TYPE = "originalContentType";
    
    private static final Logger LOG = Logger.getLogger(AbstractMessageProducer.class);
    
    private static final String UNABLE_TO_SEND_MESSAGE = "Unable to send message: ";
    private static final String ERROR_SENT_MSG_TO_TOPIC_MSG = "Failed to send message to topic: {0}";
    private static final String SUCCESSFUL_SENT_MSG_MSG = "Sent message to topic {0} on partition {1} offset {2}";
    private static final String DOUBLE_QUOTE = "\"";
    private static final String TOPIC = "topic";
    
    private JSON json;
    private Producer<String, String> producer;
    private Producer<String, byte[]> byteArrayProducer;
    
    @Autowired
    private MessageHeadersService messageHeadersService;
    
    protected final Producer<String, String> producer() {
        return this.producer;
    }
    
    public final void setJson(final JSON json) {
        this.json = json;
    }
    
    public final Producer<String, String> getProducer() {
        return this.producer;
    }
    
    public final void setProducer(final Producer<String, String> producer) {
        this.producer = producer;
    }
    
    public final Producer<String, byte[]> getByteArrayProducer() {
        return this.byteArrayProducer;
    }
    
    public final void setByteArrayProducer(final Producer<String, byte[]> byteArrayProducer) {
        this.byteArrayProducer = byteArrayProducer;
    }
    
    public final void setMessageHeadersService(final MessageHeadersService messageHeadersService) {
        this.messageHeadersService = messageHeadersService;
    }
    
    @PostConstruct
    protected void init() {
        this.json = new JSON();
    }
    
    @PreDestroy
    protected final void close() {
        if (this.producer != null) {
            this.producer.close();
        }
        if (this.byteArrayProducer != null) {
            this.byteArrayProducer.close();
        }
    }
    
    public abstract Properties getTopicProperties();
    
    @Async("kafkaProducerExecutor")
    public <T> Future<Boolean> sendWithByteArray(final String topicType, final String key, final T data)
        throws InvalidTopicTypeException, JsonException {
        return send(getTopicProperties(), topicType, key, this.json.serialize(data).getBytes());
    }
    
    @Async("kafkaProducerExecutor")
    public <T> Future<Boolean> send(final String topicType, final String key, final T data) throws InvalidTopicTypeException, JsonException {
        return send(getTopicProperties(), topicType, key, this.json.serialize(data));
    }
    
    private Future<Boolean> send(final Properties topicsProps, final String topicType, final String key, final String data)
        throws InvalidTopicTypeException {
        final CompletableFuture<Boolean> result = new CompletableFuture<>();
        validate(topicsProps, result, topicType);
        
        final String topic = topicsProps.getProperty(topicType);
        // TODO remove
        final Instant start = Instant.now();
        
        this.producer.send(new ProducerRecord<>(topic, key, data), new Callback() {
            @Override
            public void onCompletion(final RecordMetadata metadata, final Exception exception) {
                if (exception != null) {
                    LOG.error(UNABLE_TO_SEND_MESSAGE + data, exception);
                    result.completeExceptionally(new MessageProducingException(MessageFormat.format(ERROR_SENT_MSG_TO_TOPIC_MSG, topic), exception));
                } else {
                    
                    // TODO remove
                    final Duration timeToProduce = Duration.between(start, Instant.now());
                    
                    if (timeToProduce.getSeconds() > 0) {
                        LOG.error("FINISH producing message to " + topic + " after " + Duration.between(start, Instant.now()).getSeconds()
                                  + " seconds ");
                    }
                    if (LOG.isDebugEnabled()) {
                        LOG.debug(MessageFormat.format(SUCCESSFUL_SENT_MSG_MSG, topic, metadata.partition(), metadata.offset()));
                    }
                    result.complete(true);
                }
                
            }
        });
        
        return result;
    }
    
    private Future<Boolean> send(final Properties topicsProps, final String topicType, final String key, final byte[] data)
        throws InvalidTopicTypeException {
        final CompletableFuture<Boolean> result = new CompletableFuture<>();
        validate(topicsProps, result, topicType);
        
        final String topic = topicsProps.getProperty(topicType);
        byte[] withHeaders = null;
        try {
            final MessageValues mv = getHeadersAndPayload(topicsProps, removeAfterTopic(topicType), data);
            withHeaders = this.messageHeadersService.embedHeaders(mv, mv.getHeaders());
            
        } catch (UnsupportedEncodingException uee) {
            throw new InvalidTopicTypeException(uee);
        }
        // TODO remove
        final Instant start = Instant.now();
        
        this.byteArrayProducer.send(new ProducerRecord<>(topic, key, withHeaders), new Callback() {
            @Override
            public void onCompletion(final RecordMetadata metadata, final Exception exception) {
                if (exception != null) {
                    LOG.error(UNABLE_TO_SEND_MESSAGE + new String(data), exception);
                    result.completeExceptionally(new MessageProducingException(MessageFormat.format(ERROR_SENT_MSG_TO_TOPIC_MSG, topic), exception));
                } else {
                    // TODO remove
                    final Duration timeToProduce = Duration.between(start, Instant.now());
                    
                    if (timeToProduce.getSeconds() > 0) {
                        LOG.error("FINISH producing message to " + topic + " after " + Duration.between(start, Instant.now()).getSeconds()
                                  + " seconds ");
                    }
                    
                    result.complete(true);
                    
                    if (LOG.isDebugEnabled()) {
                        LOG.debug(MessageFormat.format(SUCCESSFUL_SENT_MSG_MSG, topic, metadata.partition(), metadata.offset()));
                    }
                }
            }
        });
        
        return result;
    }
    
    protected void validate(final Properties topicProperties, final CompletableFuture<Boolean> result, final String topicType) {
        if (topicProperties == null) {
            result.completeExceptionally(new MessageProducingException(
                    MessageFormat.format("Topic configuration could not be found for : {0}", this.getClass().getName())));
        }
        
        final String topic = topicProperties.getProperty(topicType);
        if (topic == null) {
            throw new InvalidTopicTypeException(topicType);
        }
    }
    
    private String removeAfterTopic(final String topicType) {
        /*
         * e.g. kafka.mitreid.usermessage.topic.name -> kafka.mitreid.usermessage.topic
         * kafka.alert.processing.topic.customer.alert.type.name -> kafka.alert.processing.topic
         * kafka.alert.processing.topic.recent.events.group -> kafka.alert.processing.topic
         */
        final int idx = topicType.indexOf(TOPIC) + TOPIC.length();
        if (idx != StandardConstants.INT_MINUS_ONE) {
            return topicType.substring(0, idx);
        }
        return topicType;
    }
    
    private MessageValues getHeadersAndPayload(final Properties topicsProps, final String topicType, final Object payload) {
        final String contentType = topicsProps.getProperty(topicType + StandardConstants.STRING_DOT + CONTENT_TYPE);
        final String originalContentType = topicsProps.getProperty(topicType + StandardConstants.STRING_DOT + ORIGINAL_CONTENT_TYPE);
        
        final Map<String, String> headers = new ConcurrentHashMap<>(10);
        headers.put(CONTENT_TYPE, DOUBLE_QUOTE + contentType + DOUBLE_QUOTE);
        headers.put(ORIGINAL_CONTENT_TYPE, DOUBLE_QUOTE + originalContentType + DOUBLE_QUOTE);
        
        final MessageValues msgVal = new MessageValues(headers);
        msgVal.setPayload(payload);
        return msgVal;
    }
    
}
