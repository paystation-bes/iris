package com.digitalpaytech.util.kafka;

import java.io.UnsupportedEncodingException;

public interface MessageHeadersService {
    byte[] embedHeaders(MessageValues original, String... headers) throws UnsupportedEncodingException;
    
    MessageValues extractHeaders(String messageWithHeader) throws UnsupportedEncodingException;
}
