package com.digitalpaytech.util.kafka.impl;

import java.util.Properties;

import javax.annotation.Resource;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Component;

import com.digitalpaytech.util.kafka.AbstractMessageProducer;
import com.digitalpaytech.util.kafka.KafkaKeyConstants;

@Component("pciLinkMessageProducer")
public class PciLinkMessageProducer extends AbstractMessageProducer {
    
    @Resource(name = KafkaKeyConstants.PCI_LINK_PROPERTIES)
    private Properties producerConf;
    
    @Resource(name = KafkaKeyConstants.PCI_LINK_TOPIC_NAMES_PROPERTIES)
    private Properties topicProperties;
    
    @Override
    public final Properties getTopicProperties() {
        return this.topicProperties;
    }
    
    protected final void init() {
        super.init();
        if (super.getProducer() == null) {
            final StringSerializer serializer = new StringSerializer();
            super.setProducer(new KafkaProducer<>(this.producerConf, serializer, serializer));
        }
        if (super.getByteArrayProducer() == null) {
            super.setByteArrayProducer(new KafkaProducer<>(this.producerConf, new StringSerializer(), new ByteArraySerializer()));
        }
    }
    
}
