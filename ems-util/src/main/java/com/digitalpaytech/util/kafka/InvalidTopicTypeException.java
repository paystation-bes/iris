package com.digitalpaytech.util.kafka;

public class InvalidTopicTypeException extends RuntimeException {
    private static final long serialVersionUID = -406599866050646062L;

    public InvalidTopicTypeException(final String topicType) {
        super("Un-recognize topic type " + topicType);
    }
    
    public InvalidTopicTypeException(final String message, final Throwable t) {
        super(message, t);
    }
    
    public InvalidTopicTypeException(final Throwable t) {
        super(t);
    }
}
