package com.digitalpaytech.util.kafka.impl;

import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.digitalpaytech.exception.JsonException;
import com.digitalpaytech.util.StandardConstants;
import com.digitalpaytech.util.json.JSON;
import com.digitalpaytech.util.json.JSONConfigurationBean;
import com.digitalpaytech.util.kafka.MessageConsumerFactory;
import com.digitalpaytech.util.kafka.MessageHeadersService;
import com.digitalpaytech.util.kafka.MessageValues;

@Component
public class MessageConsumerFactoryImpl implements MessageConsumerFactory {
    private static final Logger LOG = Logger.getLogger(MessageConsumerFactory.class);
    
    @Autowired
    private MessageHeadersService messageHeadersService;
    
    private JSON json;
    
    @PostConstruct
    private void init() {
        final JSONConfigurationBean jsonConfig = new JSONConfigurationBean(true, false, false);
        this.json = new JSON(jsonConfig);
    }
    
    @Override
    public KafkaConsumer<String, String> buildConsumer(final Properties properties, final Collection<String> topics, final String consumerGroupId) {
        
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, consumerGroupId);
        final KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(properties);
        
        consumer.subscribe(topics);
        if (LOG.isDebugEnabled()) {
            LOG.debug("New kafka consumer subscribed to :" + String.join(StandardConstants.STRING_COMMA, topics) + "with consumerGroupId: "
                      + consumerGroupId);
        }
        return consumer;
        
    }
    
    @Override
    public final <T> T deserialize(final String message, final Class<T> type) throws JsonException {
        return this.json.deserialize(message, type);
    }
    
    /**
     * Return a message where headers, that were originally embedded into the payload, have been promoted back to actual headers.
     * The new payload is now the original payload.
     *
     * @param messageWithHeaders
     *            the message to extract headers.
     */
    @Override
    public final <T> T deserializeMessageWithEmbeddedHeaders(final String messageWithHeaders, final Class<T> type)
        throws UnsupportedEncodingException, JsonException {
        
        final MessageValues mv = this.messageHeadersService.extractHeaders(messageWithHeaders);
        return deserialize((String) mv.getPayload(), type);
    }
    
    public final void setJSON(final JSON aJson) {
        this.json = aJson;
    }
}
