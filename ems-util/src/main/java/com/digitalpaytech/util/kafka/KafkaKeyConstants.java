package com.digitalpaytech.util.kafka;

public final class KafkaKeyConstants {
    public static final String OTA_DEVICE_NOTIFICATION_TOPIC_NAME = "kafka.ota.device.notification.topic.name";
    public static final String OTA_DEVICE_NOTIFICATION_CONSUMER_GROUP = "kafka.ota.device.notification.topic.group";
    public static final String OTA_DEVICE_CANCEL_PENDING_NOTIFICATION_TOPIC_NAME = "kafka.ota.device.cancel.pending.notification.topic.name";
    public static final String OTA_DEVICE_CANCEL_PENDING_NOTIFICATION_CONSUMER_GROUP = "kafka.ota.device.cancel.pending.notification.topic.group";
    
    
    
    // Property files
    public static final String DELIMETER = "delimeter";
    public static final String IRIS_PROPERTIES = "KafkaIrisProperties";
    public static final String LINK_PROPERTIES = "KafkaLinkProperties";
    public static final String PCI_LINK_PROPERTIES = "KafkaPciLinkProperties";
    public static final String TOPIC_NAMES_PROPERTIES = "KafkaTopicNamesProperties";
    public static final String PCI_LINK_TOPIC_NAMES_PROPERTIES = "KafkaPciLinkTopicNamesProperties";
    
    // Topic names
    public static final String FMS_KAFKA_UPGRADE_TOPIC_NAME = "kafka.device.upgrade.topic.name";
    public static final String FMS_KAFKA_UPGRADE_CONSUMER_GROUP = "kafka.device.upgrade.topic.group";
    
    public static final String CORE_CPS_SNF_CHARGE_RESULTS_TOPIC_NAME = "kafka.core.cps.snf.charge.results.topic.name";
    public static final String CORE_CPS_SNF_CHARGE_RESULTS_CONSUMER_GROUP = "kafka.core.cps.snf.charge.results.topic.group";

    public static final String CORE_CPS_CAPTURE_CHARGE_RESULTS_TOPIC_NAME = "kafka.core.cps.capture.charge.results.topic.name";
    public static final String CORE_CPS_CAPTURE_CHARGE_RESULTS_CONSUMER_GROUP = "kafka.core.cps.capture.charge.results.topic.group";
    
    public static final String MERCHANT_SERVICE_UPDATE_TOPIC_NAME_KEY = "kafka.merchant.service.merchant.account.update.topic.name";
    public static final String MERCHANT_SERVICE_UPDATE_CONSUMER_GROUP = "kafka.merchant.service.merchant.account.update.topic.group";
    
    public static final String OFFLINE_REFUND_RESULT_TOPIC_NAME_KEY = "kafka.offline.refund.result.topic.name";
    public static final String OFFLINE_REFUND_RESULT_TOPIC_CONSUMER_GROUP = "kafka.offline.refund.result.topic.group";
    
    public static final String ALERT_PROCESSING_RECENT_EVENTS_TOPIC_NAME_KEY = "kafka.alert.processing.topic.recent.events.name";
    public static final String ALERT_PROCESSING_RECENT_EVENTS_CONSUMER_GROUP = "kafka.alert.processing.topic.recent.events.group";
    
    public static final String ALERT_PROCESSING_PROCESSING_EVENTS_TOPIC_NAME_KEY = "kafka.alert.processing.topic.processing.events.name";
    public static final String ALERT_PROCESSING_PROCESSING_EVENTS_CONSUMER_GROUP = "kafka.alert.processing.topic.processing.events.group";
    
    public static final String ALERT_PROCESSING_HISTORICAL_EVENTS_TOPIC_NAME_KEY = "kafka.alert.processing.topic.historical.events.name";
    public static final String ALERT_PROCESSING_HISTORICAL_EVENTS_CONSUMER_GROUP = "kafka.alert.processing.topic.historical.events.group";
    
    public static final String ALERT_PROCESSING_NOTIFICATION_EVENTS_TOPIC_NAME_KEY = "kafka.alert.processing.topic.notification.events.name";
    public static final String ALERT_PROCESSING_NOTIFICATION_EVENTS_CONSUMER_GROUP = "kafka.alert.processing.topic.notification.events.group";
    
    public static final String ALERT_PROCESSING_CUSTOMER_ALERT_TYPE_TOPIC_NAME_KEY = "kafka.alert.processing.topic.customer.alert.type.name";
    public static final String ALERT_PROCESSING_CUSTOMER_ALERT_TYPE_CONSUMER_GROUP = "kafka.alert.processing.topic.customer.alert.type.group";
    
    public static final String BIN_RANGE_NOTIFICATION_TOPIC_NAME_KEY = "kafka.bin.range.notification.topic.name";
    public static final String BIN_RANGE_NOTIFICATION_TOPIC_CONSUMER_GROUP = "kafka.bin.range.notification.topic.group";
    
    public static final String LCDMS_KAFKA_DEVICE_NOTIFY_TOPIC_NAME = "kafka.ldcms.device.notify.topic.name";
    public static final String LCDMS_KAFKA_DEVICE_NOTIFY_CONSUMER_GROUP = "kafka.ldcms.device.notify.topic.group";

    public static final String ELAVON_BATCH_CLOSE_TOPIC_NAME = "kafka.elavon.batch.close.topic.name";
    public static final String ELAVON_BATCH_CLOSE_CONSUMER_GROUP = "kafka.elavon.batch.close.topic.group";

    
    // Producer only topics
    public static final String LCDMS_KAFKA_CREATE_TOPIC_NAME = "kafka.ldcms.device.update.topic.name";
    public static final String LINK_KAFKA_POS_TERMINAL_ASSIGNMENTS_TOPIC_NAME = "kafka.pos.terminal.assignments.topic.name";
    public static final String LINK_KAFKA_LIMITED_PREFERRED_RATE_INCREMENT_TOPIC_NAME = "kafka.limited.preferred.rate.increment.topic.name";
    public static final String FMS_KAFKA_UPGRADE_ACK_TOPIC_NAME = "kafka.device.upgrade.ack.topic.name";

    public static final String LINK_KAFKA_MITREID_USERMESSAGE_TOPIC_NAME = "kafka.mitreid.usermessage.topic.name";

    public static final String LINK_BAD_CARD_SERVICE_ADD = "kafka.bad.card.service.add.topic.name";
    public static final String LINK_BAD_CARD_SERVICE_DELETE = "kafka.bad.card.service.delete.topic.name";

    
    private KafkaKeyConstants() {
    }
}
