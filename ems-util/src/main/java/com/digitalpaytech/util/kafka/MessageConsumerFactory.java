package com.digitalpaytech.util.kafka;

import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Properties;

import org.apache.kafka.clients.consumer.KafkaConsumer;

import com.digitalpaytech.exception.JsonException;

public interface MessageConsumerFactory {
    
    KafkaConsumer<String, String> buildConsumer(Properties properties, Collection<String> topics, String consumerGroupId);
    
    <T> T deserialize(String message, Class<T> type) throws JsonException;
    
    <T> T deserializeMessageWithEmbeddedHeaders(String messageWithHeader, Class<T> type) throws UnsupportedEncodingException, JsonException;
    
}
