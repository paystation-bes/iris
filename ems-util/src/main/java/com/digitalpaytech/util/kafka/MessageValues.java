package com.digitalpaytech.util.kafka;

import java.util.Map;
import java.util.LinkedHashMap;

public class MessageValues {
    private Map<String, String> headers = new LinkedHashMap<>();
    private Object payload;
    
    public MessageValues(final Map<String, String> headers) {
        this.headers.putAll(headers);
    }
    
    public final String[] getHeaders() {
        return this.headers.keySet().toArray(new String[this.headers.size()]);
    }
    
    public final Map<String, String> getHeadersMap() {
        return this.headers;
    }
    
    public final Object getPayload() {
        return this.payload;
    }
    
    public final void setPayload(final Object payload) {
        this.payload = payload;
    }
    
    public final Object get(final Object key) {
        return this.headers.get(key);
    }
}
