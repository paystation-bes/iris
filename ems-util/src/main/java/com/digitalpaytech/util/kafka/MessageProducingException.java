package com.digitalpaytech.util.kafka;

public class MessageProducingException extends Exception {
    private static final long serialVersionUID = -6476169541172782428L;

    public MessageProducingException(final String message) {
        super(message);
    }
    
    public MessageProducingException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
