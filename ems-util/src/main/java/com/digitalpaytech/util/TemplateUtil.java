package com.digitalpaytech.util;

import java.io.InputStream;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;
import org.apache.velocity.tools.ToolManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TemplateUtil {
	private static Logger log = Logger.getLogger(TemplateUtil.class);
	
	private VelocityEngine velocity;
	private ToolManager velocityTools;
	
	@Autowired
	private MessageHelper messages;
	
	@PostConstruct
	public void init() {
		this.velocity = new VelocityEngine();
		
		Properties veloProps = null;
		
		InputStream veloConfig = this.getClass().getResourceAsStream("/velocity.properties");
		if (veloConfig == null) {
			log.warn("'template.properties' not found, default configurations will be used");
		}
		else {
			veloProps = new Properties();
			try {
				veloProps.load(veloConfig);
			}
			catch (Exception e) {
				log.error("Error while loading 'template.properties'", e);
				veloProps = null;
			}
		}
		
		if (veloProps == null) {
			this.velocity.init();
		}
		else {
			this.velocity.init(veloProps);
		}
		
		log.info("Template engine loaded !");
		
		this.velocityTools = new ToolManager();
		this.velocityTools.configure("/velocitytools.xml");
	}

	public String applyTemplate(String templatePath, Map<String, Object> context) {
		Template template = this.velocity.getTemplate(templatePath);
		Context ctx = this.velocityTools.createContext(context);
		for(String key : context.keySet()) {
			ctx.put(key, context.get(key));
		}
		
		ctx.put("msgtool", this.messages);
		
		StringWriter writer = new StringWriter();
		template.merge(ctx, writer);
		
		return writer.toString();
	}
}
