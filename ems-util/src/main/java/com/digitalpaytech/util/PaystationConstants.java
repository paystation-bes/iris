package com.digitalpaytech.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public final class PaystationConstants {
    
    public static final int FOUR_DIGITS = 4;
    
    public static final int MESSAGE_TYPE_TOKEN = 1;
    public static final int MESSAGE_TYPE_TRANSACTION = 2;
    public static final int MESSAGE_TYPE_CANCELTRANSACTION = 3;
    
    public static final int STALL_TYPE_ALL = 0;
    public static final int STALL_TYPE_VALID = 1;
    public static final int STALL_TYPE_EXPIRED = 2;
    
    public static final int PAYSTATION_STALL_QUERY_BY_LOCATION_LATEST_EXPIRY_TIME = 1;
    public static final int PAYSTATION_STALL_QUERY_BY_CUSTOMER_LATEST_EXPIRY_TIME = 2;
    public static final int PAYSTATION_STALL_QUERY_BY_LOCATION_EXPIRY_TIME_OF_MOST_RECENT_PERMIT = 3;
    public static final int PAYSTATION_STALL_QUERY_BY_CUSTOMER_EXPIRY_TIME_OF_MOST_RECENT_PERMIT = 4;
    
    public static final int PAYSTATION_JURISDICTION_PREFERRED_DISABLED = 0;
    public static final int PAYSTATION_JURISDICTION_PREFERRED_BY_CUSTOMER = 1;
    public static final int PAYSTATION_JURISDICTION_PREFERRED_BY_LOCATION = 2;
    
    public static final int PAYSTATION_JURISDICTION_LIMITED_DISABLED = 0;
    public static final int PAYSTATION_JURISDICTION_LIMITED_BY_CUSTOMER = 1;
    public static final int PAYSTATION_JURISDICTION_LIMITED_BY_LOCATION = 2;
    
    public static final Date INVALID_EXPIRED_STALL = (new GregorianCalendar(2000, Calendar.JANUARY, 01, 00, 00, 00)).getTime();
    
    public static final int PAYSTATION_STATUS_TYPE_OK = 0;
    public static final int PAYSTATION_STATUS_TYPE_SERIALNUMBER_EMPTY = 1;
    public static final int PAYSTATION_STATUS_TYPE_REQUEST_EMPTY = 2;
    public static final int PAYSTATION_STATUS_TYPE_COMMADDRESS_EMPTY = 3;
    public static final int PAYSTATION_STATUS_TYPE_DOES_NOT_MATCH = 4;
    public static final int PAYSTATION_STATUS_TYPE_DOES_NOT_EXIST = 5;
    public static final int PAYSTATION_STATUS_TYPE_IS_DEACTIVATED = 6;
    public static final int PAYSTATION_STATUS_TYPE_IS_INVALID = 7;
    public static final int PAYSTATION_STATUS_TYPE_INVALID_SIGNATURE_VERSION = 8;
    public static final int PAYSTATION_STATUS_TYPE_INVALID_PARAMETER = 9;
    public static final int PAYSTATION_STATUS_TYPE_TIMESTAMP_FAILURE = 10;
    public static final int PAYSTATION_STATUS_TYPE_SIGNATURE_DOES_NOT_MATCH = 11;
    public static final int PAYSTATION_STATUS_TYPE_CREDIT_CARD_BAD_CARD = 12;
    public static final int PAYSTATION_STATUS_TYPE_CREDIT_CARD_DENIED = 13;
    public static final int PAYSTATION_STATUS_TYPE_CREDIT_CARD_INVALID_CARD = 14;
    public static final int PAYSTATION_STATUS_TYPE_INVALID_MERCHANTACCOUNT = 15;
    public static final int PAYSTATION_STATUS_TYPE_ENCRYPTION_ERROR = 16;
    public static final int PAYSTATION_STATUS_TYPE_UNSUBSCRIBED_SERVICE_ERROR = 17;
    public static final int PAYSTATION_STATUS_TYPE_CANNOT_CANCEL_ERROR = 18;
    public static final int PAYSTATION_STATUS_TYPE_UNKNOWN_ERROR = 19;
    public static final int PAYSTATION_STATUS_TYPE_PROCESSOR_PAUSED = 20;
    
    public static final String RESPONSE_MESSAGE_SUCCESS = "Success";
    public static final String RESPONSE_MESSAGE_SUCCESSNOREFUND = "SuccessNoRefund";
    public static final String RESPONSE_MESSAGE_PARAMETER_ERROR = "ParameterError";
    public static final String RESPONSE_MESSAGE_SIGNATURE_ERROR = "SignatureError";
    public static final String RESPONSE_MESSAGE_ENCRYPTION_ERROR = "EncryptionError";
    public static final String RESPONSE_MESSAGE_DUPLICATE_ERROR = "Duplicate";
    public static final String RESPONSE_MESSAGE_DENIED_ERROR = "Denied";
    public static final String RESPONSE_MESSAGE_BADCARD_ERROR = "BadCard";
    public static final String RESPONSE_MESSAGE_INVALIDCARD_ERROR = "InvalidCard";
    public static final String RESPONSE_MESSAGE_OFFLINE_ERROR = "Offline";
    public static final String RESPONSE_MESSAGE_UNSUBSCRIBEDSERVICE_ERROR = "UnsubscribedService";
    public static final String RESPONSE_MESSAGE_EMS_UNAVAILABLE = "EMSUnavailable";
    public static final String RESPONSE_MESSAGE_INVALID_MERCHANTACCOUNT_ERROR = "InvalidMerchant";
    public static final String RESPONSE_MESSAGE_PURCHASE_NOT_FOUND_ERROR = "NotFound";
    public static final String RESPONSE_MESSAGE_CANNOT_CANCEL_ERROR = "CanNotCancel";
    public static final String RESPONSE_MESSAGE_NO_CONFIGURATION_ERROR = "NoConfiguration";
    
    public static final String ACCEPTABLE_SIGNATURE_VERSION = "1";
    
    // request encoding
    public static final String HTTP_REST_ENCODE_CHARSET = "UTF-8";
    public static final String AMPERSAND = "&";
    public static final String EQUAL_SIGN = "=";
    
    public static final String REGEX_MACADDRESS = "^([0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}$";
    
    public static final String PAYSTATION_PATH_TOKEN_URI = "/PayStation/GetToken";
    public static final String PAYSTATION_PATH_TRANSACTION_URI = "/PayStation/Transaction";
    public static final String PAYSTATION_PATH_CANCELTRANSACTION_URI = "/PayStation/CancelTransaction";
    public static final String PAYSTATION_PATH_HEARTBEAT_URI = "/PayStation/HeartBeat";
    
    public static final int FAILURE_INVALID_PARAMETER = 400;
    public static final int FAILURE_SIGNATURE_ERROR = 401;
    public static final int FAILURE_INTERNAL_ERROR = 500;
    
    private PaystationConstants() {
        
    }
    
}
