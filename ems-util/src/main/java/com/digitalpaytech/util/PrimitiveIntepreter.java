package com.digitalpaytech.util;

public abstract class PrimitiveIntepreter<T> {
	public static final int MASK_BYTE = 0XFF;
	public static final long MASK_LONG_BYTE = 0XFFL;
	
	public abstract byte[] decode(T data);
	public abstract T encode(byte[] data, int offset);
	
	public T encode(byte[] data) {
		return encode(data, 0);
	}
}
