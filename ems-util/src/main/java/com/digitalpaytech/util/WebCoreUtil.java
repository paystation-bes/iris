package com.digitalpaytech.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.net.UnknownServiceException;
import java.security.SecureRandom;
import java.sql.BatchUpdateException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.netflix.hystrix.exception.HystrixRuntimeException;

/**
 * WebCoreUtil
 * 
 * @author Brian Kim
 * 
 */
public final class WebCoreUtil {
    
    private static final Logger LOG = Logger.getLogger(WebCoreUtil.class);
    
    private static final String CONNECTION_REFUSED = "Connection refused";
    
    public static enum LOWER_UPPER_CASES {
        LOWERCASE, UPPERCASE
    };
    
    public static final BigDecimal HUNDRED = new BigDecimal(100);
    
    private WebCoreUtil() {
    }
    
    public static StringBuilder escapeFileName(final String input) {
        final StringBuilder result = new StringBuilder((int) (input.length() * 1.2));
        int idx = -1;
        while (++idx < input.length()) {
            final char curr = input.charAt(idx);
            final int charCode = (int) curr;
            if (charCode == 92) {
                result.append("%5C");
            } else if (((charCode >= 65) && (charCode <= 122)) || ((charCode >= 48) && (charCode <= 57))) {
                result.append(curr);
            } else {
                switch (curr) {
                    case '%':
                        result.append("%25");
                        break;
                    case '/':
                        result.append("%2F");
                        break;
                    case ':':
                        result.append("%3A");
                        break;
                    case '*':
                        result.append("%2A");
                        break;
                    case '?':
                        result.append("%3F");
                        break;
                    case '"':
                        result.append("%22");
                        break;
                    case '|':
                        result.append("%7C");
                        break;
                    case '<':
                        result.append("%3C");
                        break;
                    case '>':
                        result.append("%3E");
                        break;
                    default:
                        result.append(curr);
                        break;
                }
            }
        }
        
        return result;
    }
    
    public static Integer verifyRandomIdAndReturnActual(final HttpServletResponse response, final RandomKeyMapping keyMapping, final String randomId,
        final String[] errorMessages) {
        if (!DashboardUtil.validateRandomId(randomId)) {
            LOG.warn(errorMessages[0]);
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }
        
        final Object actualId = keyMapping.getKey(randomId);
        
        if (actualId == null || !DashboardUtil.validateDBIntegerPrimaryKey(actualId.toString())) {
            LOG.warn(errorMessages[1]);
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }
        return Integer.parseInt(actualId.toString());
    }
    
    public static Integer verifyRandomIdAndReturnActualWithoutError(final HttpServletResponse response, final RandomKeyMapping keyMapping,
        final String randomId, final String[] errorMessages) {
        if (!DashboardUtil.validateRandomId(randomId)) {
            return null;
        }
        
        final Object actualId = keyMapping.getKey(randomId);
        
        return Integer.parseInt(actualId.toString());
    }
    
    public static Long verifyRandomIdAndReturnActualLong(final HttpServletResponse response, final RandomKeyMapping keyMapping,
        final String randomId, final String[] errorMessages) {
        if (!DashboardUtil.validateRandomId(randomId)) {
            LOG.warn(errorMessages[0]);
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }
        
        final Object actualId = keyMapping.getKey(randomId);
        
        if (actualId == null || !DashboardUtil.validateDBIntegerPrimaryKey(actualId.toString())) {
            LOG.warn(errorMessages[1]);
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return null;
        }
        return Long.parseLong(actualId.toString());
    }
    
    public static String encodeHTMLTag(final String value) {
        String returnValue = value;
        if (value != null && value.length() > 0) {
            final StringBuffer sb = new StringBuffer(value);
            substituteChar("<", "&lt;", sb, 0);
            substituteChar(">", "&gt;", sb, 0);
            
            returnValue = sb.toString();
        }
        
        return returnValue;
    }
    
    private static void substituteChar(final String wildCardValue, final String replaceWildCardValue, final StringBuffer sb, final int startIndex) {
        final int index = sb.indexOf(wildCardValue, startIndex);
        if (index >= 0) {
            sb.replace(index, index + wildCardValue.length(), replaceWildCardValue);
            
            // calculate new start position
            final int newIndex = index + replaceWildCardValue.length();
            if (newIndex < sb.length()) {
                substituteChar(wildCardValue, replaceWildCardValue, sb, newIndex);
            }
        }
    }
    
    public static Float convertCelsiusToFahrenheit(final Float celsius) {
        return Float.parseFloat(WebCoreConstants.SENSOR_TEMPERATURE_FORMATTER.format((1.8f * celsius) + 32));
    }
    
    /**
     * @param b
     *            boolean
     * @return return 1 if 'true', 0 if 'false'.
     */
    public static Integer convertBooleanToInteger(final boolean b) {
        return BooleanUtils.toInteger(b);
    }
    
    /**
     * Converts base 100 int value to float String.
     * e.g. 515 -> 5.15
     */
    public static String convertBase100ValueToFloatString(final int intValue) {
        return String.valueOf(new BigDecimal(intValue).divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP));
    }
    
    /**
     * Divide base 100 int value by divisor.
     * e.g. 500 / 100 = 5
     *      325 / 100 = 3
     *      450 / 5   = 90
     */
    public static int divideBase100ValueBy100(final String base100Value) {
        return new BigDecimal(base100Value).divide(new BigDecimal(StandardConstants.CONSTANT_100)).intValue();
    }    
    
    /**
     * Converts String value to base 100 int value.
     * e.g. 5.15 -> 515, 7 -> 700
     * 
     * @return Return 0 if value is null or ""
     * @throws NumberFormatException
     *             If value String is not a number.
     */
    public static int convertToBase100IntValue(final String value) throws NumberFormatException {
        if (StringUtils.isBlank(value)) {
            return 0;
        }
        return convertToBase100Value(value, 0).intValue();
    }
    
    /**
     * Converts String value to base 100 short value.
     * e.g. 5.15 -> 515, 7 -> 700
     * 
     * @return Return 0 if value is null or "".
     * @throws NumberFormatException
     *             If value String is not a number.
     */
    public static short convertToBase100ShortValue(final String value) throws NumberFormatException {
        if (StringUtils.isBlank(value)) {
            return 0;
        }
        return convertToBase100Value(value, 0).shortValue();
    }
    
    public static int convertToBase100Value(final int dollars) {
        return dollars * 100;
    }
    
    public static BigDecimal convertToBase100Value(final String value) throws NumberFormatException {
        return convertToBase100Value(value, 0);
    }
    
    public static BigDecimal convertToBase100Value(final String value, final int scale) {
        // These lines are dedicated to trim out excessive mantissa to help BigDecimal perform faster...
        int lastIdx = value.length();
        final int dotIdx = value.indexOf(".");
        if (dotIdx > 0) {
            lastIdx = dotIdx + 4 + scale;
            if (lastIdx > value.length()) {
                lastIdx = value.length();
            }
        }
        
        return HUNDRED.multiply(new BigDecimal(value.substring(0, lastIdx))).setScale(scale, RoundingMode.HALF_UP);
    }
    
    public static Integer convertToBase100IntValue(final BigDecimal value, final int scale) {
        if (value == null) {
            return 0;
        }
        
        return HUNDRED.multiply(value).setScale(scale, RoundingMode.HALF_UP).intValue();
    }
    
    public static BigDecimal convertFromBase100Value(final int cents) {
        return convertFromBase100Value(new Integer(cents));
    }
    
    public static BigDecimal convertFromBase100Value(final Number cents) {
        return new BigDecimal(formatBase100Value(cents));
    }
    
    public static BigDecimal convertFromBase100Value(final String cents) {
        return new BigDecimal(formatBase100Value(cents));
    }
    
    public static String formatBase100Value(final Number cents) {
        return formatBase100Value(cents.toString());
    }
    
    public static String formatBase100Value(final String centsString) {
        final StringBuilder result = new StringBuilder();
        String buffer = centsString;
        if (buffer.charAt(0) == '-') {
            result.append('-');
            buffer = buffer.substring(1);
        }
        
        if (buffer.length() > 2) {
            final int mantissaIdx = buffer.length() - 2;
            result.append(buffer.substring(0, mantissaIdx)).append(".").append(buffer.substring(mantissaIdx));
        } else {
            result.append("0.");
            if (buffer.length() < 2) {
                result.append("0");
            }
            
            result.append(buffer);
        }
        
        return result.toString();
    }
    
    /**
     * Combines words by removing spaces and makes result lowercase.
     * 
     * @param string
     *            e.g. "Time Limited"
     * @param delimiter
     *            e.g. space ( ), comma (,)
     * @return String e.g. "Time Limited" -> "timelimited", "Table1, Table2" -> "table1table2"
     */
    public static String combineWordsAndChangeCase(final String delimiter, final LOWER_UPPER_CASES selectedCase, final String string) {
        final String[] words = string.split(delimiter);
        final StringBuilder bdr = new StringBuilder();
        for (String s : words) {
            bdr.append(s.trim());
        }
        if (selectedCase == LOWER_UPPER_CASES.LOWERCASE) {
            return bdr.toString().toLowerCase();
        }
        return bdr.toString().toUpperCase();
    }
    
    public static boolean checkIfExistingTransactionException(final Throwable e) {
        Throwable curr = e;
        Throwable cause = e.getCause();
        while (cause != null && curr != cause) {
            if (cause instanceof MySQLIntegrityConstraintViolationException) {
                final String cause_msg = cause.getMessage();
                return cause_msg.indexOf("idx_purchase_multi_uq") > -1;
            }
            
            curr = cause;
            cause = curr.getCause();
        }
        return false;
    }
    
    public static boolean checkIfDuplicateException(final Throwable e) {
        Throwable curr = e;
        Throwable cause = e.getCause();
        while (cause != null && curr != cause) {
            if (cause instanceof BatchUpdateException) {
                final String cause_msg = cause.getMessage();
                return (cause_msg != null)
                       && ((cause.getMessage().indexOf("Duplicate entry") > -1 || e.getMessage().indexOf("Duplicate entry") > -1));
            }
            
            curr = cause;
            cause = curr.getCause();
        }
        return false;
    }
    
    /**
     * Converts Object into byte array.
     * 
     * @param obj
     *            Object instance
     * @return byte array
     * @throws IOException
     *             When it fails to write object, this exception will be thrown.
     */
    public static byte[] convertObjToBinary(final Object obj) throws IOException {
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        final ObjectOutputStream oos = new ObjectOutputStream(bos);
        try {
            oos.writeObject(obj);
        } catch (IOException e) {
            throw e;
        } finally {
            oos.flush();
            oos.close();
            bos.close();
        }
        
        final byte[] data = bos.toByteArray();
        return data;
    }
    
    /**
     * Create the physical file in the specified path.
     * 
     * @param filePath
     *            directory for file to be located.
     * @param fileContents
     *            byte array containing object information.
     * @throws IOException
     * @throws IOException
     *             When it fails to write the file, this exception will be thrown.
     */
    public static void createFile(final String directoryPath, final String fileName, final byte[] fileContents) throws IOException {
        final StringBuffer path = new StringBuffer(directoryPath);
        if (!path.toString().endsWith("/")) {
            path.append("/");
        }
        
        final File file = new File(directoryPath);
        if (!file.isDirectory()) {
            file.mkdir();
        }
        
        path.append(fileName);
        
        final FileOutputStream outStream = new FileOutputStream(path.toString());
        outStream.write(fileContents);
        
        outStream.flush();
        outStream.close();
    }
    
    /**
     * @param value
     *            Could be 'true/false' or '0/1'.
     * @return boolean Return 'false' if value is null, empty or invalid data.
     */
    public static boolean convertToBoolean(final String value) {
        if (StringUtils.isBlank(value)) {
            return false;
        }
        if (value.length() == 1 && Character.isDigit(value.charAt(0))) {
            return convertToBoolean(Integer.parseInt(value));
        }
        return Boolean.valueOf(value);
    }
    
    public static boolean convertToBoolean(final int value) {
        if (value != 0 && value != 1) {
            return false;
        }
        // BooleanUtils.toBoolean(int value, int trueValue, int falseValue)
        return BooleanUtils.toBoolean(value, 1, 0);
    }
    
    /**
     * Converts file to byte array to create the binary file.
     * 
     * @param file
     *            File to be converted to byte array.s
     * @return byte array
     * @throws IOException
     *             when it fails to read file, this exception will be thrown.
     */
    public static byte[] convertFileToBinary(final File file) throws IOException {
        final byte[] fileByte = new byte[(int) file.length()];
        
        final FileInputStream fileInputStream = new FileInputStream(file);
        fileInputStream.read(fileByte);
        
        return fileByte;
    }
    
    /**
     * Returns all files in the specified directory name
     * 
     * @param directoryPath
     *            Directory name containing all files.
     * @return File array
     */
    public static File[] getAllFiles(final String directoryPath) {
        final File file = new File(directoryPath);
        return file.listFiles();
    }
    
    public static String encodeBase64(final String value) {
        final String base64Value = new String(Base64.encodeBase64(value.getBytes()));
        return base64Value;
    }
    
    public static String decodeBase64(final String encodedValue) {
        return new String(Base64.decodeBase64(encodedValue));
    }
    
    public static boolean checkIfAllDigits(final String input) {
        for (int i = 0; i < input.length(); i++) {
            if (!Character.isDigit(input.charAt(i))) {
                return (false);
            }
        }
        return (true);
    }
    
    /**
     * e.g.
     * charData = 123456789=0000, charsToEscape = { '=' }, return true
     * charData = 123456789-0000, charsToEscape = { '=' }, return false
     * charData = 123=456789=0000, charsToEscape = { '=' }, return true
     * charData = 123-456789=0000, charsToEscape = { '=', '-' }, return true
     * 
     * @param charsToEscape
     *            char array contains characters that should be ignored.
     * @param cardData
     *            card data / account number
     * @return boolean true if all characters are numeric after escape/ignore characters defined in 'charsToEscape'
     */
    public static boolean escapeCharsAndCheckIfAllDigits(final char[] charsToEscape, final String cardData) {
        // If card data length is > 40 (max track2 length is 37), it's encrypted.
        if (cardData.length() > 40) {
            return false;
        }
        char c;
        final StringBuilder bdr = new StringBuilder();
        for (int i = 0; i < cardData.length(); i++) {
            c = cardData.charAt(i);
            if (!isInArray(c, charsToEscape)) {
                bdr.append(c);
            }
        }
        if (WebCoreUtil.checkIfAllDigits(bdr.toString())) {
            return true;
        }
        return false;
    }
    
    private static boolean isInArray(final char c, final char[] chars) {
        for (int i = 0; i < chars.length; i++) {
            if (c == chars[i]) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean isInArray(final int number, final int[] array) {
        for (int i : array) {
            if (number == i) {
                return true;
            }
        }
        return false;
    }
    
    // ----------------------------------------------------------------------------------------
    // Copied from EMS 6.3.12 com.digitalpioneer.util.csv.CsvProcessor
    public static int getInt(final String value) {
        if (isEmpty(value)) {
            return (-1);
        } else if (!isInteger(value, false)) {
            return (-1);
        }
        
        try {
            return (Integer.parseInt(value));
        } catch (Exception e) {
            return (-1);
        }
    }
    
    public static boolean isInteger(final String value) {
        return (isInteger(value, false));
    }
    
    public static boolean isInteger(final String value, final boolean allowWildcards) {
        if (isEmpty(value)) {
            return (false);
        }
        
        if (value.length() > 8) {
            return (false);
        }
        
        for (int i = 0; i < value.length(); i++) {
            final char character = value.charAt(i);
            if (!Character.isDigit(character)) {
                return (false);
            }
        }
        return (true);
    }
    
    public static int getIntZeroForEmpty(final String value) {
        if (isEmpty(value)) {
            return (0);
        } else {
            return getInt(value);
        }
    }
    
    public static boolean getBoolean(final Object obj) {
        return (getBoolean((String) obj));
    }
    
    public static boolean getBoolean(final String value) {
        if (isEmpty(value)) {
            return (false);
        }
        
        return (value.equalsIgnoreCase("on") || value.equalsIgnoreCase("true") || value.equalsIgnoreCase("yes") || value.equalsIgnoreCase("y"));
    }
    
    public static boolean isValidBooleanValue(final String value) {
        if (!isEmpty(value) && !value.equalsIgnoreCase("on") && !value.equalsIgnoreCase(Boolean.TRUE.toString()) && !value.equalsIgnoreCase("yes")
            && !value.equalsIgnoreCase("y") && !value.equalsIgnoreCase("off") && !value.equalsIgnoreCase(Boolean.FALSE.toString())
            && !value.equalsIgnoreCase("no") && !value.equalsIgnoreCase("n")) {
            return false;
        }
        return true;
    }
    
    public static boolean isEmpty(final String value) {
        return (value == null || value.trim().length() == 0);
    }
    
    public static int getDollarValue(final String value) {
        if (isEmpty(value)) {
            return (0);
        }
        
        int val = 0;
        try {
            val = new BigDecimal(value).multiply(new BigDecimal(100)).intValue();
        } catch (Exception e) {
            return -1;
        }
        return val;
    }
    
    public static String somethingOrNull(final String input) {
        String result = null;
        if (input != null) {
            result = input.trim();
            if (result.length() <= 0) {
                result = null;
            }
        }
        
        return result;
    }
    
    public static String appendHoursMinutesIfNecessary(final String date, final String hours, final String minutes) {
        if (date.indexOf(WebCoreConstants.DATETIME_SYMBOL) == -1) {
            final StringBuilder bdr = new StringBuilder();
            bdr.append(date.trim()).append(WebCoreConstants.DATETIME_SYMBOL).append(hours).append(WebCoreConstants.COLON);
            bdr.append(minutes).append(WebCoreConstants.COLON).append("00");
            return bdr.toString();
        }
        return date;
    }
    
    // The stall range format was follow the following syntax:
    // x, y, z.... or x-y, z...
    // eg. 1, 3, 34-45, 50-54, 60
    public static boolean isValidStallRange(final String value) {
        final String[] stall_groups = value.split(",");
        for (int i = 0; i < stall_groups.length; i++) {
            final String[] stall_values = stall_groups[i].split("-");
            boolean is_valid_value = false;
            switch (stall_values.length) {
                case 1:
                    final String stall_value = stall_values[0].trim();
                    is_valid_value = (stall_value.length() < 6) && isInteger(stall_value) && !stall_value.equals("0");
                    break;
                
                case 2:
                    final String stall_value_low = stall_values[0].trim();
                    final String stall_value_high = stall_values[1].trim();
                    
                    final int stall_num_low = getInt(stall_value_low);
                    final int stall_num_high = getInt(stall_value_high);
                    
                    is_valid_value = (stall_num_low > 0 && stall_num_high > stall_num_low);
                    break;
                default:
                    break;
            }
            if (!is_valid_value) {
                return (false);
            }
        }
        
        // All values are integer and we pass
        return (true);
    }
    
    /**
     * e.g. "6.2 build 0621" -> "620621"
     * "6.4 build 0026" -> "640026"
     */
    public static int removeNonNumericCharsAndConvert(String str) {
        StringBuilder bdr = new StringBuilder();
        char[] chars = str.toCharArray();
        for (char c : chars) {
            if (Character.isDigit(c)) {
                bdr = bdr.append(c);
            }
        }
        return Integer.parseInt(bdr.toString());
    }
    
    /**
     * Prefix '0' until total characters reaches the max.
     * e.g. 5 -> 5, 05, 005, 0005, 00005, 000005, 0000005, 00000005, 0000000005...
     */
    public static String prefixZeroesForSql(String str, int max) {
        int length = str.length();
        int numZeros = max - length;
        
        StringBuilder zeroBdr = new StringBuilder();
        zeroBdr.append(WebCoreConstants.ZERO);
        
        StringBuilder bdr = new StringBuilder();
        bdr.append(WebCoreConstants.SINGLE_QUOTE).append(str).append(WebCoreConstants.SINGLE_QUOTE)
                .append(WebCoreConstants.PAYSTATION_CONTACT_URL_SEPARATOR);
        
        for (int i = 0; i < numZeros; i++) {
            bdr = bdr.append(prefixZero(zeroBdr.toString(), str));
            zeroBdr = zeroBdr.append(WebCoreConstants.ZERO);
        }
        
        String newStr = bdr.toString();
        if (newStr.endsWith(WebCoreConstants.PAYSTATION_CONTACT_URL_SEPARATOR)) {
            newStr = newStr.substring(0, newStr.length() - 1);
        }
        return newStr;
    }
    
    /**
     * e.g. str = 5, maxLength = 4,
     * 5 -> 0005
     * 05 -> 0005
     * 0005 -> 00005
     * 2525 -> 2525
     * 
     * @param short numeric value
     * @param max
     *            max number of characters
     * @return String number prefixed with zeroes if necessary.
     */
    public static String prefixZeroes(final short sh, final int max) {
        return prefixStrings(String.valueOf(sh), WebCoreConstants.ZERO, max);
    }
    
    public static String prefixStrings(final String str, final String mask, final int max) {
        StringBuilder bdr = new StringBuilder();
        for (int i = 0; i < max - str.length(); i++) {
            bdr = bdr.append(mask);
        }
        bdr.append(str);
        return bdr.toString();
    }

    public static String createWithCreditCardMask(final short last4Digits, final String mask) {
        final String last4DigitsString = prefixZeroes(last4Digits, StandardConstants.CONSTANT_4);
        return prefixStrings(last4DigitsString, mask, WebCoreConstants.VALIDATION_MAX_LENGTH_16); 
    }
    
    public static String createStarredCardNumber(final String cardNumber, final boolean isWithStars) {
        final StringBuilder starredCardNumber = new StringBuilder();
        if (isWithStars) {
            starredCardNumber.append(StandardConstants.STRING_FOUR_STARS);
            starredCardNumber.append(StandardConstants.STRING_EMPTY_SPACE);
        }
        if (cardNumber.length() < WebCoreConstants.MAX_NUMBERS_LAST_DIGITS) {
            starredCardNumber.append(StandardConstants.STRING_FOUR_ZEROS.substring(cardNumber.length()));
            starredCardNumber.append(cardNumber);
        } else {
            starredCardNumber.append(cardNumber.substring(cardNumber.length() - WebCoreConstants.MAX_NUMBERS_LAST_DIGITS));
        }
        return starredCardNumber.toString();
    }
    
    private static String prefixZero(String chars, String str) {
        StringBuilder bdr = new StringBuilder();
        bdr.append(WebCoreConstants.SINGLE_QUOTE).append(chars).append(str).append(WebCoreConstants.SINGLE_QUOTE)
                .append(WebCoreConstants.PAYSTATION_CONTACT_URL_SEPARATOR);
        return bdr.toString();
    }
    
    public static boolean isPsPre641(String version) {
        try {
            if (removeNonNumericCharsAndConvert(version) < WebCoreConstants.PS_VERSION_6_4_1) {
                return true;
            }
            return false;
        } catch (NumberFormatException nfe) {
            LOG.error("Return false, cannot retrieve or convert version number to integer: " + version, nfe);
            return false;
        }
    }
    
    /**
     * e.g. string = null, return ""
     * string = "", return ""
     * string = "   ", return ""
     * string = "  DPT  ", return "  DPT  "
     */
    public static String returnEmptyIfBlank(String string) {
        if (StringUtils.isNotBlank(string)) {
            return string;
        }
        return "";
    }
    
    public static boolean hasNumericNotZeroValue(String data) {
        if (!isInteger(data)) {
            return false;
        }
        int i = Integer.parseInt(data);
        if (i > 0) {
            return true;
        }
        return false;
    }
    
    /**
     * e.g. trueValue = "1"
     * "0" - not active, cleared, return false
     * "1" - active, return true;
     * 
     * @param number
     *            0 or 1
     * @param trueValue
     *            a number represents true.
     * @return boolean return true if input equals trueValue otherwise return false.
     */
    public static boolean convertNumericValueToBoolean(String number, String trueValue) {
        if (StringUtils.isNotBlank(number) && number.equals(trueValue)) {
            return true;
        }
        return false;
    }
    
    public static Object[] createObjectArray(Object... objects) {
        Object[] array = new Object[objects.length];
        for (int i = 0; i < objects.length; i++) {
            array[i] = objects[i];
        }
        return array;
    }
    
    public static String cleanupIframeJSON(String jsonString) {
        return jsonString.replaceAll("<\\\\/", "</");
    }
    
    /**
     * If it's not credit card (1), smart card (2) or passcard card type id (3) , return true.
     * 
     * @param cardTypeId
     *            CardType id.
     * @return boolean true if it's unknown card type.
     */
    public static boolean isUnknowCardType(int cardTypeId) {
        int[] ids = new int[] { WebCoreConstants.CREDIT_CARD, WebCoreConstants.SMART_CARD, WebCoreConstants.VALUE_CARD };
        for (int id : ids) {
            if (cardTypeId == id) {
                return false;
            }
        }
        return true;
    }
    
    // Deletes all files and subdirectories under dir.
    // Returns true if all deletions were successful.
    // If a deletion fails, the method stops attempting to delete and returns
    // false.
    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // The directory is now empty so delete it
        return dir.delete();
    }
    
    /**
     * Delete the track data file.
     * 
     * @param file
     *            File instance to be deleted.
     */
    public static boolean deleteFile(File file) {
        boolean isSucceed = false;
        
        if (!file.exists()) {
            LOG.error("Delete track data file: no such file or directory: " + file.getPath());
        }
        
        if (!file.canWrite()) {
            LOG.error("Delete track data file: write protected: " + file.getPath());
        }
        
        /* If it is a directory, make sure it is empty */
        if (file.isDirectory()) {
            String[] files = file.list();
            if (files.length > 0) {
                LOG.error("Delete track data file: directory not empty: " + file.getPath());
            }
        }
        
        /* Delete the actual file. */
        System.gc();
        isSucceed = file.delete();
        
        if (!isSucceed) {
            LOG.error("Delete track data file: Deletion of track data failed." + file.getPath());
        }
        return isSucceed;
    }
    
    /**
     * The logic is from O'Reilly Java I/O book.
     * http://books.google.ca/books?id=
     * 42etT_9-_9MC&pg=PT352&lpg=PT352&dq=oreilly+Java+I/O+SecureDelete&source=bl&ots=xy5J0FkMfE&sig=X8snue-Mr9T6xltSXLGSWffY_h0&hl=en&sa=X&ei=cEBLUbaoLomdiQLQkoC4Cw&ved=
     * 0
     * C
     * D
     * c
     * Q
     * 6
     * A
     * E
     * w
     * A
     * g
     * 
     * "The logic maps the entire file to be erased into memory. It then writes zeroes into the file, then ones, then random data produced by
     * a java.security.SecureRandom object. After each run, the buffer is forced to make sure the data is actually written to the disk. Otherwise,
     * only the last pass might be committed. Failing to force the data might leave magnetic patterns an adversary could analyze, even if the
     * actual file contents were the same."
     * 
     * @param file
     *            File to be deleted.
     * @return boolean Return 'false' if file still exists.
     */
    public static boolean secureDelete(File file) {
        if (file.exists()) {
            long length = file.length();
            SecureRandom random = new SecureRandom();
            RandomAccessFile raf = null;
            try {
                raf = new RandomAccessFile(file, "rws");
                raf.seek(0);
                raf.getFilePointer();
                byte[] data = new byte[64];
                int pos = 0;
                while (pos < length) {
                    random.nextBytes(data);
                    raf.write(data);
                    pos += data.length;
                }
                raf.close();
                file.delete();
            } catch (IOException ioe) {
                StringBuilder bdr = new StringBuilder();
                bdr.append("Cannot execute 'secureDelete' for file name: ").append(file.getName())
                        .append(". Use regular java.io.File delete instead.");
                LOG.error(bdr.toString(), ioe);
                file.delete();
            }
            
            if (LOG.isInfoEnabled()) {
                boolean deleted = false;
                StringBuilder bdr = new StringBuilder();
                bdr.append("Is file: ").append(file.getName()).append(" deleted? ");
                if (!file.exists()) {
                    deleted = true;
                }
                bdr.append(deleted);
                LOG.info(bdr.toString());
            }
            
            // Return true if file is deleted.
            if (!file.exists()) {
                return true;
            } else {
                return false;
            }
        }
        // File doesn't exist.
        StringBuilder bdr = new StringBuilder();
        bdr.append("File: ").append(file.getAbsolutePath()).append(" doesn't exist.");
        LOG.warn(bdr.toString());
        return true;
    }
    
    public static String formatToDollars(String amount) {
        double amountDollars = Double.parseDouble(amount);
        return String.format("%.2f", amountDollars);
    }
    
    public static <T> boolean isCollectionEmpty(final Collection<T> collection) {
        return collection == null || collection.isEmpty();
    }

    
    public static String concatErrors(final List<String> fileErrors, final int maxLength) {
        String concatErrors = null;
        
        if (fileErrors != null) {
            final StringBuilder bldr = new StringBuilder();
            
            for (String fileError : fileErrors) {
                if (bldr.length() + fileError.length() <= maxLength) {
                    bldr.append(fileError).append(StandardConstants.CHAR_SEMICOLON);
                } else {
                    break;
                }
            }
            if (bldr.length() > 0) {
                concatErrors = bldr.toString();
            }
        }
        return concatErrors;
    }    
    
    public static String returnNAIfBlank(final String string) {
        return StringUtils.isBlank(string) ? WebCoreConstants.N_A_STRING : string;
    }

    public static boolean isNetworkException(final Exception e) {
        if ((ExceptionUtils.getRootCause(e) instanceof ConnectException
                && ExceptionUtils.getRootCauseMessage(e).contains(CONNECTION_REFUSED))
            || ExceptionUtils.getRootCause(e) instanceof UnknownHostException) {
            return true;
        }
        return false;
    }
    
    public static boolean isNetworkCommunicationException(final Exception e) {
        boolean isHystrixException = e instanceof HystrixRuntimeException;
        if (!isHystrixException) {
            return false;
        }
        final Throwable rootCause = ExceptionUtils.getRootCause(e);
        if (rootCause == null) {
            return false;
        }
        
        return Stream.of(MalformedURLException.class, SocketException.class, UnknownHostException.class, UnknownServiceException.class)
                .anyMatch(cls -> cls.isInstance(rootCause));        
    }
    
    public static boolean causedByClientExceptionOrNetworkCommunicationException(final Exception e) {
        if(causeByClientException(e)) {
            LOG.debug("is Client Exception");
            return true;
        }
        if(isNetworkCommunicationException(e)) {
            LOG.debug("is Network Exception");
            return true;
        }
        return false;
    }
    
    public static boolean causeByClientException(final Exception e) {
        if (e instanceof HystrixRuntimeException) {
            StringWriter sWrite = null;
            PrintWriter pWrite = null;
            try {
                sWrite = new StringWriter();
                pWrite = new PrintWriter(sWrite);
                e.printStackTrace(pWrite);
                if (sWrite.toString().toLowerCase().contains(WebCoreConstants.CLIENT_EXCEPTION_MESSAGE_NO_AVAILABLE_SERVER)
                        || sWrite.toString().toLowerCase().contains(WebCoreConstants.CLIENT_EXCEPTION_MESSAGE_NUM_RETRIES_EXCEEDED_MAX)) {
                    return true;
                }
            } finally {
                try {
                    if (sWrite != null) {
                        sWrite.close();
                    }
                    if (pWrite != null) {
                        pWrite.close();
                    }
                } catch (IOException ioe) {
                    LOG.error(ioe);
                }
            }
        }
        return false;
    }
    
    /*
     * In Iris Processor table for Link records they ends with ".link" but in Merchant-Service they don't.
     * @return String e.g. processor.creditcall.link -> processor.creditcall
     * processor.creditcall -> processor.creditcall
     */
    public static String getProcessorNameInMerchantService(final String dotLink, final String processorName) {
        return processorName.endsWith(dotLink) ? processorName.substring(StandardConstants.CONSTANT_0, processorName.lastIndexOf(dotLink))
                : processorName;
    }
    
    
    public static boolean isUUID(final String token) {
        if (StringUtils.isBlank(token)) {
            return false;
        }
        return CardProcessingConstants.UUID_PATTERN.matcher(token).matches();
    }
}
