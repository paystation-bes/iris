package com.digitalpaytech.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.KeyStore.Entry;
import java.security.KeyStore.PasswordProtection;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.KeyStore.SecretKeyEntry;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

import org.apache.log4j.Logger;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.x509.SubjectKeyIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.x509.X509Extension;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import com.digitalpaytech.exception.CryptoException;

public abstract class Crypto {
    
    private KeyStore keystore_;
    
    public final static int SECRET_KEY_OBJECT_ID = 0;
    public final static int PRIVATE_KEY_OBJECT_ID = 1;
    public final static int PUBLIC_KEY_OBJECT_ID = 2;
    
    private static final String RSA_ALGORITHM_NAME = "RSA";
    private static final String AES_ALGORITHM_NAME = "AES";
    
    private final String DNAME = "CN=Digital Payment, OU=Digital Payment, O=Digital Payment, L=Burnaby, ST=BC, C=CA";
    
    private final Logger logger = Logger.getLogger(Crypto.class);
    
    private final static char[] KEY_PASSWORD_CHARS = "testpassword".toCharArray();
    private final static PasswordProtection KEY_PASSWORD = new PasswordProtection(KEY_PASSWORD_CHARS);
    private final static char[] KEYSTORE_PASSWORD_CHARS = "l;kja*dfj@aRsBXkqePr?9xCv[9ewq.8ds(UjOsafW2I1sdfZsdfk]laG".toCharArray();
    
    private String keystorePath_ = null;
    
    protected Crypto(String keystorePath) {
        keystorePath_ = keystorePath;
    }
    
    /**
     * Initializes the keystore.
     * 
     * If the keystore itself has not been created, this function will throw an
     * exception.
     * 
     * @param cryptoPath
     *            - The path to where the keystore is/will be located.
     * @throws CryptoException
     *             - if the keystore cannot be initialized.
     */
    protected void initializeKeystore(String keystoreType, String keystoreProvider) throws CryptoException {
        File keystore_file = new File(keystorePath_);
        
        // If keystore file already exists , load the data
        if (keystore_file.exists()) {
            try {
                keystore_ = KeyStore.getInstance(keystoreType, keystoreProvider);
                keystore_.load(new FileInputStream(keystore_file), KEYSTORE_PASSWORD_CHARS);
            } catch (Exception e) {
                throw new CryptoException("Unable to initialize keystore.", e);
            }
        }
        
        // If keystore file does not exist, throw exception
        else {
            /*
             * //if need generate initial key bundle uncomment this part try {
             * keystore_ = KeyStore.getInstance(keystoreType, keystoreProvider);
             * keystore_.load(null, null); createRsaKey("000001", 2048);
             * createAesKey("002001", 256); return; }catch (Exception e) { throw
             * new CryptoException("Unable to initialize keystore.", e); }
             */
            throw new CryptoException("Keystore: " + keystorePath_ + " does not exist.");
        }
    }
    
    public Key getKey(String keyName, int keyType) throws CryptoException {
        Entry entry = null;
        Key key = null;
        
        // Get keystore entry
        try {
            entry = keystore_.getEntry(keyName, KEY_PASSWORD);
        } catch (GeneralSecurityException e) {
            throw new CryptoException("Unable to retrieve key: " + keyName, e);
        }
        
        // Get key from keystore entry
        if (entry == null) {
            throw new CryptoException("Unable to retrieve key: " + keyName);
        } else if (keyType == SECRET_KEY_OBJECT_ID) {
            SecretKeyEntry key_entry = (SecretKeyEntry) entry;
            key = key_entry.getSecretKey();
        } else if (keyType == PRIVATE_KEY_OBJECT_ID) {
            PrivateKeyEntry key_entry = (PrivateKeyEntry) entry;
            key = key_entry.getPrivateKey();
        } else if (keyType == PUBLIC_KEY_OBJECT_ID) {
            PrivateKeyEntry key_entry = (PrivateKeyEntry) entry;
            key = key_entry.getCertificate().getPublicKey();
        } else {
            throw new CryptoException("Unknown Key Object Type: " + keyType);
        }
        
        return (key);
    }
    
    public abstract String getJceProvider();
    
    public void createRsaKey(String alias, int keySize, String signatureAlgorithm) throws CryptoException {
        KeyPairGenerator key_gen = null;
        try {
            key_gen = KeyPairGenerator.getInstance(RSA_ALGORITHM_NAME, getJceProvider());
            key_gen.initialize(keySize);
            
            KeyPair key_pair = key_gen.generateKeyPair();
            
            X509Certificate[] chain = new X509Certificate[1];
            chain[0] = getSelfCertificate(key_pair, signatureAlgorithm);
            
            // store in keystore
            keystore_.setKeyEntry(alias, key_pair.getPrivate(), KEY_PASSWORD_CHARS, chain);
            keystore_.store(new FileOutputStream(keystorePath_), KEYSTORE_PASSWORD_CHARS);
        } catch (Exception e) {
            throw new CryptoException("Unable to generate new RSA key", e);
        }
    }
    
    public boolean checkAndGenerateSigningKey(List<String> supportedAlgorithms, int keySize) throws CryptoException {
        boolean keysAboutToExpire = false;
        
        KeyPairGenerator keyGen = null;
        for (String algorithm : supportedAlgorithms) {
            try {
                Entry entry = keystore_.getEntry("DPTSIGNINGKEY" + algorithm, KEY_PASSWORD);
                if (entry != null) {
                    PrivateKeyEntry privateKeyEntry = (PrivateKeyEntry) entry;
                    X509Certificate certificate = (X509Certificate) privateKeyEntry.getCertificate();
                    Date expireDate = certificate.getNotAfter();
                    Date currentDate = new Date();
                    int diffInDays = (int) ((expireDate.getTime() - currentDate.getTime()) / (1000 * 60 * 60 * 24));
                    if (diffInDays < 365) {
                        keysAboutToExpire = true;
                    }
                    logger.info("Signing Key Entry Found for algorithm " + algorithm + " skipping generation");
                    
                    continue;
                }
                logger.info("Signing Key Entry not Found generating for algorithm" + algorithm);
                logger.info("Getting KeyPair Generator instance for RSA for provider" + getJceProvider());
                keyGen = KeyPairGenerator.getInstance("RSA", getJceProvider());
                logger.info("Got KeyGen refrence" + keyGen.getAlgorithm());
                keyGen.initialize(keySize);
                logger.info("Initialized key size");
                KeyPair keyPair = keyGen.generateKeyPair();
                logger.info("Generated key Pair");
                X509Certificate[] chain = new X509Certificate[1];
                logger.info("Creating x509 certificate for singing algorithm" + algorithm);
                chain[0] = getSigningCertificate(keyPair, algorithm);
                logger.info("++++Generated+++ Certificate for algorithm +++ " + algorithm + "+++ Serial Number +++" + chain[0].getSerialNumber()
                            + "+++ Public key +++" + chain[0].getPublicKey());
                keystore_.setKeyEntry("DPTSIGNINGKEY" + algorithm, keyPair.getPrivate(), KEY_PASSWORD_CHARS, chain);
                keystore_.store(new FileOutputStream(keystorePath_), KEYSTORE_PASSWORD_CHARS);
                logger.info("Saved in keystore successfully with key alias name" + "DPTSIGNINGKEY" + algorithm);
                
            } catch (Exception e) {
                throw new CryptoException("Unable to generate Signing Key", e);
            }
        }
        return keysAboutToExpire;
        
    }
    
    public void createAesKey(String alias, int keySize) throws CryptoException {
        try {
            KeyGenerator kgen = KeyGenerator.getInstance(AES_ALGORITHM_NAME);
            kgen.init(keySize);
            SecretKey skey = kgen.generateKey();
            SecretKeyEntry skey_entry = new SecretKeyEntry(skey);
            keystore_.setEntry(alias, skey_entry, KEY_PASSWORD);
            keystore_.store(new FileOutputStream(keystorePath_), KEYSTORE_PASSWORD_CHARS);
        } catch (Exception e) {
            throw new CryptoException("Unable to generate new AES key", e);
        }
    }
    
    public boolean isExistKey(String alias) {
        try {
            Entry entry = keystore_.getEntry(alias, KEY_PASSWORD);
            if (entry != null)
                return true;
        } catch (GeneralSecurityException e) {
        }
        return false;
    }
    
    public void deleteKey(String keyName) throws CryptoException {
        try {
            keystore_.deleteEntry(keyName);
            keystore_.store(new FileOutputStream(keystorePath_), KEYSTORE_PASSWORD_CHARS);
        } catch (Exception e) {
            throw new CryptoException("Unable to delete key: " + keyName, e);
        }
    }
    
    private X509Certificate getSelfCertificate(KeyPair keyPair, String signatureAlgorithm) throws CertificateException, InvalidKeyException,
            SignatureException, NoSuchAlgorithmException, NoSuchProviderException, OperatorCreationException {
        try {
            X500Name x500Name = new X500Name(this.DNAME);
            long validity = (long) 1 * 24 * 60 * 60;
            Date firstDate = new Date();
            Date lastDate = new Date();
            lastDate.setTime(lastDate.getTime() + validity * 1000);
            X509v3CertificateBuilder v3CertGen = new JcaX509v3CertificateBuilder(x500Name, BigInteger.valueOf((firstDate.getTime() / 1000)), firstDate,
                    lastDate, x500Name, keyPair.getPublic());
            
            v3CertGen.addExtension(X509Extension.subjectKeyIdentifier, false, createSubjectKeyId(keyPair.getPublic()));
            
            v3CertGen.addExtension(X509Extension.authorityKeyIdentifier, false, createAuthorityKeyId(keyPair.getPublic()));
            
            return new JcaX509CertificateConverter().setProvider(getJceProvider()).getCertificate(v3CertGen.build(new JcaContentSignerBuilder(
                                                                                                          signatureAlgorithm).setProvider(getJceProvider())
                                                                                                          .build(keyPair.getPrivate())));
        } catch (IOException e) {
            throw new CertificateEncodingException("getSelfCert: " + e.getMessage());
        }
    }
    
    private X509Certificate getSigningCertificate(KeyPair keyPair, String signatureAlgorithm) throws CertificateException, InvalidKeyException,
            SignatureException, NoSuchAlgorithmException, NoSuchProviderException, OperatorCreationException {
        try {
            X500Name x500Name = new X500Name(this.DNAME);
            long validity = (long) 10 * 365 * 24 * 60 * 60;
            logger.info("Setting validity to 10 years");
            Date firstDate = new Date();
            Date lastDate = new Date();
            lastDate.setTime(lastDate.getTime() + validity * 1000);
            X509v3CertificateBuilder v3CertGen = new JcaX509v3CertificateBuilder(x500Name, BigInteger.valueOf((firstDate.getTime() / 1000)), firstDate,
                    lastDate, x500Name, keyPair.getPublic());
            logger.info("Generated X509Certificate builder");
            v3CertGen.addExtension(X509Extension.subjectKeyIdentifier, false, createSubjectKeyId(keyPair.getPublic()));
            
            v3CertGen.addExtension(X509Extension.authorityKeyIdentifier, false, createAuthorityKeyId(keyPair.getPublic()));
            
            logger.info("Generating Jca509 certificate for provider" + getJceProvider());
            return new JcaX509CertificateConverter().setProvider(getJceProvider()).getCertificate(v3CertGen.build(new JcaContentSignerBuilder(
                                                                                                          signatureAlgorithm).setProvider(getJceProvider())
                                                                                                          .build(keyPair.getPrivate())));
        } catch (IOException e) {
            throw new CertificateEncodingException("getSelfCert: " + e.getMessage());
        }
    }
    
    private AuthorityKeyIdentifier createAuthorityKeyId(PublicKey pub) throws IOException {
        
        return new AuthorityKeyIdentifier(createSubjectPublicKeyInfo(pub));
    }
    
    private SubjectKeyIdentifier createSubjectKeyId(PublicKey pub) throws IOException {
        
        return new SubjectKeyIdentifier(createSubjectPublicKeyInfo(pub));
    }
    
    private SubjectPublicKeyInfo createSubjectPublicKeyInfo(PublicKey pub) throws IOException {
        ByteArrayInputStream bIn = new ByteArrayInputStream(pub.getEncoded());
        return new SubjectPublicKeyInfo((ASN1Sequence) new ASN1InputStream(bIn).readObject());
    }
}