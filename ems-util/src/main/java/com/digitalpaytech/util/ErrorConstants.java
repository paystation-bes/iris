package com.digitalpaytech.util;

public final class ErrorConstants {
    public static final String ERROR_STATUS = "errorStatus";
    
    public static final String MS_COMM_FAILURE = "error.microservice.communication";
    
    public static final String NOT_UNIFI = "error.customer.unifi.id";
    
    public static final String REQUIRED = "error.common.required";
    public static final String OUT_OF_RANGE = "error.common.invalid.range";
    
    public static final String INVALID = "error.common.invalid";
    public static final String INVALID_NUMERIC = "error.invalid.numeric.only";
    public static final String INVALID_DATE = "error.common.invalid.date";
    public static final String INVALID_TIME = "error.common.invalid.time";
    public static final String INVALID_DATETIME = "error.common.invalid.datetime";
    public static final String INVALID_RANGE = "error.common.invalid.range.order";
    public static final String INVALID_BOOLEAN = "error.common.invalid.boolean";
    
    public static final String MISMATCHED_STRING = "error.common.invalid.special.character.name";
    public static final String MISMATCHED_NUMBER = "error.common.invalid.number";
    public static final String MISMATCHED_CURRENCY = "error.common.invalid.dollarAmtOnly2Dig";
    public static final String MISMATCHED_FLOAT = "error.common.invalid.float";
    public static final String MISMATCHED_DISCOUNT = "error.common.invalid.discount";
    public static final String MISMATCHED_EMAIL = "error.common.invalid.email";
    public static final String MISMATCHED_URL_NO_PROTOCOL = "error.common.invalid.nonProtocolURL";
    
    public static final String OVERFLOW_MAX = "error.common.more.than.max";
    public static final String OVERFLOW_DATE_MAX = "error.common.date.more.than.max";
    
    public static final String UNDERFLOW_MIN = "error.common.less.than.min";
    public static final String UNDERFLOW_DATE_MIN = "error.common.date.less.than.min";
    
    public static final String OUT_OF_BOUND_STRING = "error.common.invalid.lengths";
    public static final String OUT_OF_BOUND_DIGITS = "error.common.invalid.lengths.digits";
    
    public static final String LENGTH_MISMATCHED_STRING = "error.common.invalid.length";
    public static final String LENGTH_MISMATCHED_DIGITS = "error.common.invalid.length.digits";
    
    public static final String NOT_LESSER = "error.common.invalid.not.lesser";
    public static final String NOT_LESSER_NOR_EQUALS = "error.common.invalid.not.lesser.nor.equals";
    
    public static final String PATTERN_DATE = "error.pattern.date";
    public static final String PATTERN_TIME = "error.pattern.time";
    public static final String PATTERN_REPORT_TIME = "error.pattern.report.time";
    public static final String PATTERN_DATETIME = "error.pattern.datetime";
    public static final String PATTERN_ALPHANUMERIC = "error.regex.alphanumeric";
    public static final String PATTERN_ALPHANUMERIC_SPACE = "error.regex.alphanumeric.with.space";
    public static final String PATTERN_ALPHANUMERIC_STAR = "error.regex.alphanumeric.with.star";
    public static final String PATTERN_ALPHANUMERIC_STAR_PLUS = "error.regex.alphanumeric.with.star.plus";
    public static final String PATTERN_NUMERIC = "error.regex.numeric";
    public static final String PATTERN_NUMERIC_EQUALS = "error.regex.numeric.with.equals";
    public static final String PATTERN_CURRENCY = "error.regex.currency";
    public static final String PATTERN_LOCATION = "error.regex.text.location";
    public static final String PATTERN_PAY_STATION = "error.regex.text.payStation";
    public static final String PATTERN_COUPON_SEARCH = "error.regex.text.coupon.search";
    public static final String PATTERN_COUPON_SEARCH_DISCOUNT = "error.regex.text.coupon.with.discount.search";
    public static final String PATTERN_EMAIL = "error.regex.emailaddress";
    public static final String PATTERN_STANDARD_TEXT = "error.regex.text.standard";
    public static final String PATTERN_STANDARD_TEXT_EXTRA = "error.regex.text.extraStandard";
    public static final String PATTERN_PRINTABLE_TEXT = "error.regex.text.printable";
    
    public static final String REQUESTED_ITEM_DELETED = "error.common.requested.item.deleted";
    
    public static final String COMMON_IMPORT = "error.common.import";
    public static final String DUPLICATED_IMPORT_RECORD = "error.common.import.duplicated.record";
    public static final String CSV_INSUFFICENT_COLUMNS = "error.common.csv.import.insufficient.column";
    public static final String IMPORT_SIZE_LIMIT_EXCEEDED = "error.common.import.exceeding.size.limit";
    
    public static final String ALERT_CANNOT_BE_DELAYED = "error.alert.cannot.be.delayed";
    public static final String ALERT_DELAY_BY_MINUTES_ZERO = "error.alert.delay.by.minutes.zero";
    
    private ErrorConstants() {
        
    }
}
