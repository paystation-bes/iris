package com.digitalpaytech.util;

import java.io.Closeable;

public interface PooledResource<T> extends Closeable {
    T get();
    
    void close();
}
