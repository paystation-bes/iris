package com.digitalpaytech.util;

public class SensorUtil {
    public static String getBatteryChargingStateDisplayed(float inputCurrent, float systemLoad)
    {
        String chargeState = "";
        if (inputCurrent != WebCoreConstants.SENSOR_VALUE_UNSET && systemLoad != WebCoreConstants.SENSOR_VALUE_UNSET)
        {
            if (isBatteryCharging(inputCurrent, systemLoad))
            {
                chargeState = "(" + WebCoreConstants.SENSOR_CHARGE_STATE_CHARGING + ")";
            }
        }
        return chargeState;
    }
    
    public static boolean isBatteryCharging(float inputCurrent, float systemLoad)
    {
        return (inputCurrent > systemLoad);
    }

}
