package com.digitalpaytech.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * WebCoreConstants
 * 
 * @author Brian Kim
 * 
 */
public final class WebCoreConstants {
    public static final String GMT = "GMT";
    public static final String SERVER_TIMEZONE_GMT = GMT;
    public static final String SYSTEM_ADMIN_UI_TIMEZONE = "Canada/Pacific";
    public static final TimeZone SYSTEM_ADMIN_TIMEZONE = TimeZone.getTimeZone(WebCoreConstants.SYSTEM_ADMIN_UI_TIMEZONE);
    
    public static final String ERROR_FEATURENOTENABLED = StandardConstants.STRING_MINUS_ONE;
    public static final String ERROR_SESSIONINVALID = "-2";
    public static final String ERROR_FEATUREINVALID = "-3";
    public static final String ERROR_INSUFFICIENTPERMISSION = "-4";
    public static final String ERROR_ACCESSDENIED = "-5";
    
    public static final String HTTP_HEADER_REFERER = "referer";
    
    public static final String HTTP_GET = "GET";
    public static final String HTTP_POST = "POST";
    public static final String HTTP_GET_QUOTATION = "?";
    public static final String ROOT_PATH = "/";
    public static final String HTTP_STRING = "http";
    public static final String HTTPS_STRING = "https";
    
    public static final short SHORT_RECORD_NOT_FOUND = -1;
    public static final int INT_RECORD_NOT_FOUND = -1;
    public static final Object RECORD_NOT_FOUND = -1;
    
    public static final String DATETIME_SYMBOL = "T";
    
    // TODO - Change property names to match with http codes
    
    public static final String HEADER_CUSTOM_STATUS_CODE = "CustomStatusCode";
    //'Bandwidth Limit Exceeded' - returned when widget dynamic SQL reaches the data limit specified in emsproperties
    public static final int HTTP_STATUS_CODE_509 = 209;
    //'Retry With' - returned if widget dynamic SQL returns no data
    public static final int HTTP_STATUS_CODE_449 = 249;
    
    //non-standard response code - returned if trying to delete a location which has child locations
    public static final int HTTP_STATUS_CODE_480 = 280;
    //non-standard response code - returned if trying to delete a location which has pay stations
    public static final int HTTP_STATUS_CODE_481 = 281;
    //non-standard response code - returned if trying to delete a location which has active permits
    public static final int HTTP_STATUS_CODE_482 = 282;
    //non-standard response code - returned if trying to cancel a queued report that is launched or completed 
    public static final int HTTP_STATUS_CODE_483 = 283;
    
    //standard response code - returned if server can't find what was requested
    public static final int HTTP_STATUS_CODE_404 = 404;
    //standard response code - conflict
    public static final int HTTP_STATUS_CODE_409 = 409;
    //standard response code - returned if unexpected condition was encountered
    public static final int HTTP_STATUS_CODE_500 = 500;
    
    public static final String SESSION_FORCECHANGEPWD = "ForceChangePWD";
    public static final String REPORTS_TEMP_PATH = "/WEB-INF/tempReports";
    
    public static final String HEADER_CONTENT_TYPE = "Content-Type";
    public static final String CONTENT_TYPE_APPLICATION_XML = "application/xml";
    public static final String CONTENT_TYPE_APPLICATION_JSON = "application/json";
    
    // for Primary key lookups
    public static final String RANDOM_KEY_MAPPING = "randomKeyMapping";
    public static final String BEAN_PROPERTY_PK = "primaryKey";
    public static final String RANDOM_ID_LOOK_UP_NAME = "randomId";
    public static final String ID_LOOK_UP_NAME = "id";
    
    // Random Key min length (max is 16 for Long Integer)
    public static final int RANDOMKEY_MIN_HEX_LENGTH = 26;
    public static final int RANDOMKEY_MAX_HEX_LENGTH = 26;
    
    /* session key for temporary dashboard data */
    public static final String SESSION_TAB_TREEMAP = "tabTreeMap";
    public static final String SESSION_SECTION_IDS_TO_BE_DELETED = "sectionIdsToBeDeleted";
    public static final String SESSION_WIDGET_IDS_TO_BE_DELETED = "widgetIdsToBeDeleted";
    public static final String SESSION_SUBSET_MEMBER_IDS_TO_BE_DELETED = "SubsetMemberIdsToBeDeleted";
    public static final String SESSION_TAB_HEADINGS = "tabHeadings";
    public static final String SESSION_WIDGET_SETTINGS_OPTIONS = "widgetSettingsOptionsForCustomer";
    public static final String SESSION_WIDGET_ID_CURRENT = "widgetIdCurrent";
    public static final String SESSION_CHILD_LOCATIONS = "childLocations";
    
    /* temporary Id prefix for dashboard */
    public static final String TEMP_TAB_ID_PREFIX = "TempT";
    public static final String TEMP_SECTION_ID_PREFIX = "TempS";
    public static final String TEMP_WIDGET_ID_PREFIX = "TempW";
    
    // temporary Rate Profile Id 
    public static final String TEMP_RATE_PROFILE_ID_PREFIX = "TempRP";
    public static final String TEMP_RATE_RATE_PROFILE_ID_PREFIX = StandardConstants.STRING_DASH;
    public static final String SESSION_RATE_PROFILE_ATTRIBUTE_NAME = "currentRateProfile";
    public static final String SESSION_RATE_RATE_PROFILE_MAP_ATTRIBUTE_NAME = "currentRateRateProfileMap";
    
    public static final int DEFAULT_SECTION_LAYOUT_ID = 2;
    public static final int DEFAULT_NUMBER_OF_COLUMN = 3;
    public static final int DEFAULT_PAST_NUMBER_OF_MONTHS = 12;
    public static final String DEFAULT_RATE_NAME = "Unknown Rate";
    public static final String DEFAULT_PAYSTATIONSETTING_NAME = "Unknown Lot Setting";
    public static final String DEFAULT_LOCATION = "Unassigned";
    public static final String DEFAULT_TAB_NAME = "Untitled";
    
    public static final String NOT_MAINTAINED_PAYSTATION_SETTING = "No Settings";
    
    public static final String RESPONSE_TRUE = Boolean.TRUE.toString();
    public static final String RESPONSE_FALSE = Boolean.FALSE.toString();
    public static final String UNKNOWN = "Unknown";
    public static final String NO = "NO";
    public static final String YES = "YES";
    public static final String PAYSTATION_CONTACT_URL_SEPARATOR = StandardConstants.STRING_COMMA;
    public static final String NEXT_LINE_SYMBOL = "\r\n";
    public static final String NIX_NEXT_LINE_SYMBOL = "\n";
    public static final String EQUAL_SIGN = "=";
    
    /* Dashboard widget tier type status */
    public static final String ACTIVE = "active";
    public static final String IN_ACTIVE = "inactive";
    public static final String DISABLED = "disabled";
    public static final String SELECTED = "selected";
    
    /* Customer property types */
    public static final int CUSTOMER_PROPERTY_TYPE_TIMEZONE = 1;
    public static final int CUSTOMER_PROPERTY_TYPE_QUERY_SPACES_BY = 2;
    public static final int CUSTOMER_PROPERTY_TYPE_MAX_USER_ACCOUNTS = 3;
    public static final int CUSTOMER_PROPERTY_TYPE_CREDIT_CARD_OFFLINE_RETRY = 4;
    public static final int CUSTOMER_PROPERTY_TYPE_MAX_OFFLINE_RETRY = 5;
    public static final int CUSTOMER_PROPERTY_TYPE_SMS_WARNING_PERIOD = 6;
    public static final int CUSTOMER_PROPERTY_TYPE_DISPLAY_SEPARATE_COLLECTIONS_AS = 7;
    public static final int CUSTOMER_PROPERTY_TYPE_HIDDEN_PAYSTATIONS_REPORTED = 8;
    public static final int CUSTOMER_PROPERTY_TYPE_GPS_LATITUDE = 9;
    public static final int CUSTOMER_PROPERTY_TYPE_GPS_LONGITUDE = 10;
    public static final int CUSTOMER_PROPERTY_TYPE_JURISDICTION_TYPE_PREFERRED = 11;
    public static final int CUSTOMER_PROPERTY_TYPE_JURISDICTION_TYPE_LIMITED = 12;
    
    public static final String GLOBAL_CONFIGURATION = "GlobalConfiguration";
    
    /* regular expressions for validation. */
    public static final String REGEX_LOCATION_TEXT = "^[a-zA-Z0-9 .,]*$";
    public static final String REGEX_COUPON_TEXT = "^[a-zA-Z0-9 .,\\-]*$";
    public static final String REGEX_COUPON_SEARCH_TEXT = "^[a-zA-Z0-9 .,?*]*$";
    public static final String REGEX_COUPON_WITH_DISCOUNT_SEARCH_TEXT = "^[a-zA-Z0-9 .,?$%*]*$";
    public static final String REGEX_STANDARD_TEXT = "^[a-zA-Z0-9 .,:;#@'\\-|/\\\\{}()]*$";
    public static final String REGEX_PAYSTATION_TEXT = "^[a-zA-Z0-9 .,:;#@'\\-_/\\\\{}()]*$";
    public static final String REGEX_EXTRA_STANDARD_TEXT = "^[a-zA-Z0-9 .,:#@$'\\-=_|/\\\\{}()]*$";
    //The angle bracket is removed to prevent XSS.
    public static final String REGEX_PRINTABLE_TEXT = "^[a-zA-Z0-9 .,:;~!#@$%&'`\"\\^*+\\-=?_|/\\\\{}()\\[\\]\\r\\n]*$";
    
    public static final String REGEX_TIME_ZONE = "^[a-zA-Z /\\-()]*$";
    public static final String REGEX_ALPHANUMERIC = "^[0-9a-zA-Z]+$";
    public static final String REGEX_ALPHANUMERIC_WITH_SPACE = "^[0-9a-zA-Z ]+$";
    public static final String REGEX_ALPHANUMERIC_WITH_STAR = "^[0-9a-zA-Z\\*]+$";
    public static final String REGEX_ALPHANUMERIC_WITH_STAR_AND_PLUS = "^[0-9a-zA-Z\\*\\+]+$";
    public static final String REGEX_ALPHANUMERIC_LINK2GOV = "^[a-zA-Z0-9 .,-_#@/:|\\\\{}()]*$";
    public static final String REGEX_INTEGER = "^[0-9]+$";
    public static final String REGEX_INTEGER_WITH_EQUALS = "^[0-9=]+$";
    public static final String REGEX_FLOAT = "^[0-9]+\\.?[0-9]*$";
    public static final String REGEX_TWO_DIGIT_FLOAT = "^\\d*(\\.\\d{1,2})?$";
    public static final String REGEX_PAN_EXP_DIGIT_COUNT = "^\\d{15,17}=\\d{4}";
    
    public static final String REGEX_EMAIL_OLD = "^[\\w\\.-_]{1,}\\@([\\da-zA-Z-_]{1,}\\.){1,}[\\da-zA-Z-]+$";
    public static final String REGEX_EMAIL_PART_LOCAL = "([a-zA-Z0-9!#$%&'*+-\\/=?^_`{}|~.]+)|(\"([^\"\\\\]|(\\\\)|(\\\\\"))+\")";
    public static final String REGEX_EMAIL_PART_DOMAIN_IPV4 = "\\[[0-9]{1,3}(\\.[0-9]{1,3}){3}\\]";
    public static final String REGEX_EMAIL_PART_DOMAIN_IPV6 = "\\[IPv6:[0-9a-fA-F]{1,4}(:[0-9a-fA-F]{1,4}){7}\\]";
    public static final String REGEX_EMAIL_PART_DOMAIN = "[a-zA-Z0-9_-]+\\.[a-zA-Z0-9_-]+)*";
    public static final String REGEX_EMAIL = "^(" + REGEX_EMAIL_PART_LOCAL + ")(\\." + REGEX_EMAIL_PART_LOCAL + ")*@((" + REGEX_EMAIL_PART_DOMAIN_IPV4
                                             + ")|(" + REGEX_EMAIL_PART_DOMAIN + ")$";
    public static final String REGEX_EMAIL_DPT = ".+@digitalpaytech.com$";
    public static final String REGEX_EMAIL_DPT_OR_T2 = "(.+@digitalpaytech.com$)|(.+@t2systems.com$)";
    
    /* regular expressions. */
    public static final String REGEX_NON_ALPHANUMERIC = "[^a-zA-Z0-9]+";
    public static final String REGEX_WHITE_SPACE_ONLY = "^\\s*$";
    
    public static final String REGEX_CSV_FILES = "[0-9a-zA-Z| |.|,|\\-|=|_|+|/|:|<|>]*";
    
    // MM/dd/yyyy
    public static final String REGEX_DATE = "(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])/((19|20)\\d\\d)";
    // 24hour time (e.g. 23:54)
    public static final String REGEX_TIME = "([01]?[0-9]|2[0-4]):[0-5][0-9]";
    public static final String REGEX_REPORTS_TIME = "([01]?[0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9])";
    public static final String REGEX_URL = "^[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]$";
    public static final String REGEX_URL_WITH_TYPE = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]$";
    
    public static final String REGEX_DOLLAR_VALUE_FRONT = "^[$]\\s*(?=.*[0-9])\\d*(?:\\.\\d{1,2})?$";
    public static final String REGEX_DOLLAR_VALUE_END = "^(?=.*[0-9])\\d*(?:\\.\\d{1,2})?\\s*[$]$";
    public static final String REGEX_PERCENTAGE_VALUE_FRONT = "^[%]\\s*(100|[0-9]{1,2})$";
    public static final String REGEX_PERCENTAGE_VALUE_END = "^(100|[0-9]{1,2})\\s*[%]$";
    
    /* User Status Type */
    public static final int USER_STATUS_TYPE_DISABLED = 0;
    public static final int USER_STATUS_TYPE_ENABLED = 1;
    public static final int USER_STATUS_TYPE_DELETED = 2;
    
    /* Role Status Type */
    public static final int ROLE_STATUS_TYPE_DISABLED = 0;
    public static final int ROLE_STATUS_TYPE_ENABLED = 1;
    public static final int ROLE_STATUS_TYPE_DELETED = 2;
    
    /* Merchant Status Type */
    public static final int MERCHANT_STATUS_TYPE_DISABLED_ID = 0;
    public static final int MERCHANT_STATUS_TYPE_ENABLED_ID = 1;
    public static final int MERCHANT_STATUS_TYPE_DELETED_ID = 2;
    public static final int MERCHANT_STATUS_TYPE_MIGRATED_ID = 3;
    
    /* Customer Status Type */
    public static final String CUSTOMER_STATUS_TYPE_DISABLED_LABEL = DISABLED;
    public static final int CUSTOMER_STATUS_TYPE_DISABLED = 0;
    public static final String CUSTOMER_STATUS_TYPE_ENABLED_LABEL = "enabled";
    public static final int CUSTOMER_STATUS_TYPE_ENABLED = 1;
    public static final String CUSTOMER_STATUS_TYPE_TRIAL_LABEL = "trial";
    public static final int CUSTOMER_STATUS_TYPE_TRIAL = 2;
    public static final String CUSTOMER_STATUS_TYPE_DELETED_LABEL = "deleted";
    public static final int CUSTOMER_STATUS_TYPE_DELETED = 3;
    public static final String[] CUSTOMER_STATUS_TYPE_LABELS = new String[] { CUSTOMER_STATUS_TYPE_DISABLED_LABEL, CUSTOMER_STATUS_TYPE_ENABLED_LABEL,
        CUSTOMER_STATUS_TYPE_TRIAL_LABEL, CUSTOMER_STATUS_TYPE_DELETED_LABEL, };
    public static final int[] CUSTOMER_STATUS_TYPES =
            new int[] { CUSTOMER_STATUS_TYPE_DISABLED, CUSTOMER_STATUS_TYPE_ENABLED, CUSTOMER_STATUS_TYPE_TRIAL, CUSTOMER_STATUS_TYPE_DELETED, };
    
    /* Customer Type */
    public static final int CUSTOMER_TYPE_DPT = 1;
    public static final int CUSTOMER_TYPE_PARENT = 2;
    public static final int CUSTOMER_TYPE_CHILD = 3;
    
    /* validation length */
    public static final int VALIDATION_MIN_LENGTH_0 = 0;
    public static final int VALIDATION_MIN_LENGTH_1 = 1;
    public static final int VALIDATION_MIN_LENGTH_4 = 4;
    public static final int VALIDATION_MIN_LENGTH_8 = 8;
    public static final int VALIDATION_MIN_LENGTH_10 = 10;
    public static final int VALIDATION_MAX_LENGTH_15 = 15;
    public static final int VALIDATION_MAX_LENGTH_16 = 16;
    public static final int VALIDATION_MAX_LENGTH_20 = 20;
    public static final int VALIDATION_MAX_LENGTH_25 = 25;
    public static final int VALIDATION_MAX_LENGTH_30 = 30;
    public static final int VALIDATION_MAX_LENGTH_32 = 32;
    public static final int VALIDATION_MAX_LENGTH_40 = 40;
    public static final int VALIDATION_MAX_LENGTH_50 = 50;
    public static final int VALIDATION_MAX_LENGTH_60 = 60;
    public static final int VALIDATION_MAX_LENGTH_100 = 100;
    public static final int VALIDATION_MAX_LENGTH_255 = 255;
    public static final int VALIDATION_MAX_LENGTH_512 = 512;
    public static final int DESCRIPTION_EXTERNAL_LENGTH = 80;
    
    public static final int VALIDATION_WSENDPOINT_TOKEN_LENGTH = 32;
    
    /* CardType ids */
    public static final int N_A = 0;
    public static final int CREDIT_CARD = 1;
    public static final int SMART_CARD = 2;
    public static final int VALUE_CARD = 3;
    
    /* Event Severity Types */
    public static final int SEVERITY_CLEAR = 0;
    public static final int SEVERITY_MINOR = 1;
    public static final int SEVERITY_MAJOR = 2;
    public static final int SEVERITY_CRITICAL = 3;
    public static final Integer[] SEVERITY_LEVELS = { 0, 1, 2, 3 };
    
    /* Event Alert Threshold Type */
    public static final int ALERT_THRESHOLD_TYPE_LAST_SEEN_INTERVAL_HOUR = 1;
    public static final int ALERT_THRESHOLD_TYPE_RUNNING_TOTAL_DOLLAR = 2;
    public static final int ALERT_THRESHOLD_TYPE_COIN_CANISTER_COUNT = 6;
    public static final int ALERT_THRESHOLD_TYPE_COIN_CANISTER_DOLLARS = 7;
    public static final int ALERT_THRESHOLD_TYPE_BILL_STACKER_COUNT = 8;
    public static final int ALERT_THRESHOLD_TYPE_BILL_STACKER_DOLLARS = 9;
    public static final int ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_COUNT = 10;
    public static final int ALERT_THRESHOLD_TYPE_UNSETTLED_CREDIT_CARD_DOLLARS = 11;
    public static final int ALERT_THRESHOLD_TYPE_PAYSTATION = 12;
    public static final int ALERT_THRESHOLD_TYPE_OVERDUE_COLLECTION = 13;
    
    /* Event Alert Type */
    // Changed to use ALERT_THRESHOLD_TYPE    
    //    public final static short ALERT_TYPE_LAST_SEE_INTERVAL = 1;
    //    public final static short ALERT_TYPE_RUNNING_TOTAL = 2;
    //    public final static short ALERT_TYPE_COIN_CANISTER = 3;
    //    public final static short ALERT_TYPE_BILL_STACKER = 4;
    //    public final static short ALERT_TYPE_UNSETTLED_CREDIT = 5;
    //    public final static short ALERT_TYPE_PAYSTATION = 6;
    
    /* Event Alert Class Type */
    public static final byte ALERT_CLASS_TYPE_COMMUNICATION = 1;
    public static final byte ALERT_CLASS_TYPE_COLLECTION = 2;
    public static final byte ALERT_CLASS_TYPE_PAY_STATION_ALERT = 3;
    
    /* Extend-By-Phone */
    public static final int SUNDAY = 1;
    public static final int MONDAY = 2;
    public static final int TUESDAY = 3;
    public static final int WEDNESDAY = 4;
    public static final int THURSDAY = 5;
    public static final int FRIDAY = 6;
    public static final int SATURDAY = 7;
    public static final String CURRENT_RATES_DAY_OF_WEEK = "currentRatesDayOfWeek";
    public static final String CURRENT_PARKING_PERMISSIONS_DAY_OF_WEEK = "currentPermissionsDayOfWeek";
    public static final String COLON = ":";
    public static final String DOLLAR_SIGN = "$";
    public static final String EMPTY_SPACE = " ";
    public static final String LOC_RANDOM_ID = "locRandomIdStr";
    
    public static final double EBP_RATE_LIMIT_MIN = 0.00d;
    public static final double EBP_RATE_LIMIT_MAX = 99.99d;
    public static final double EBP_SERVICE_FEE_LIMIT_MIN = 0.00d;
    public static final double EBP_SERVICE_FEE_LIMIT_MAX = 99.99d;
    public static final int EBP_EXTENTION_MINS_LIMIT_MIN = 1;
    public static final int EBP_EXTENTION_MINS_LIMIT_MAX = 720;
    
    /* Sensor Log Type */
    public static final String EVENT_TYPE_SENSOR = "Sensor";
    public static final String EVENT_TYPE_PAYSTATION = "Paystation";
    public static final String EVENT_TYPE_COIN_ACCEPTOR = "CoinAcceptor";
    public static final String EVENT_TYPE_BILL_ACCEPTOR = "BillAcceptor";
    public static final String EVENT_TYPE_CARD_READER = "CardReader";
    public static final String EVENT_TYPE_MODEM = "Modem";
    public static final String EVENT_TYPE_RFIDCARDREADER = "RfidCardReader";
    
    public static final String EVENT_TYPE_PRINTER = "Printer";
    public static final String EVENT_TYPE_BILL_STACKER = "BillStacker";
    public static final String EVENT_TYPE_BILL_CANISTER = "BillCanister";
    public static final String EVENT_TYPE_COIN_CHANGER = "CoinChanger";
    public static final String EVENT_TYPE_COIN_CANISTER = "CoinCanister";
    public static final String EVENT_TYPE_COIN_ESCROW = "CoinEscrow";
    public static final String EVENT_TYPE_COIN_HOPPER = "CoinHopper";
    public static final String EVENT_TYPE_PCM = "PCM";
    public static final String EVENT_TYPE_UID = "UID";
    public static final String EVENT_TYPE_TICKET_FOOTER_SUCCESS = "TicketFooterSuccess";
    
    public static final String EVENT_DEVICE_TYPE_DOOR_STRING = "Door";
    public static final String EVENT_DEVICE_TYPE_DOOR_LOWER_STRING = "DoorLower";
    public static final String EVENT_DEVICE_TYPE_DOOR_UPPER_STRING = "DoorUpper";
    public static final String EVENT_DEVICE_TYPE_DOOR_MAINTENANCE_STRING = "DoorMaintenance";
    public static final String EVENT_DEVICE_TYPE_DOOR_CASH_VAULT_STRING = "DoorCashVault";
    
    public static final String EVENT_DEVICE_TYPE_SHOCK_ALARM_STRING = "ShockAlarm";
    public static final String EVENT_DEVICE_TYPE_BILL_CANISTER_STRING = "BillCanister";
    
    public static final int SENSOR_INFO_BATTERY_VOLTAGE = 0;
    public static final int SENSOR_INFO_BATTERY_2_VOLTAGE = 1;
    public static final int SENSOR_INFO_CONTROLLER_TEMPERATURE = 2;
    public static final int SENSOR_INFO_AMBIENT_TEMPERATURE = 3;
    public static final int SENSOR_INFO_RELATIVE_HUMIDITY = 4;
    public static final int SENSOR_INFO_INPUT_CURRENT = 5;
    public static final int SENSOR_INFO_SYSTEM_LOAD = 6;
    public static final String ACTION_CONTROLLER_TEMPERATURE = "ControllerTemperature";
    public static final String ACTION_AMBIENT_TEMPERATURE = "AmbientTemperature";
    public static final String ACTION_RELATIVE_HUMIDITY = "RelativeHumidity";
    public static final String ACTION_BATTERY_VOLTAGE = "BatteryVoltage";
    public static final String ACTION_VOLTAGE = "Voltage";
    public static final String ACTION_INPUT_CURRENT = "InputPower";
    public static final String ACTION_SYSTEM_LOAD = "OutputPower";
    
    public static final String INFO_DOOR_UPPER = "Upper";
    public static final String INFO_DOOR_LOWER = "Lower";
    public static final String INFO_MAINTENANCE = "Maintenance";
    public static final String INFO_CASH_VAULT = "CashVault";
    
    // Radius sensor values which are corresponding to PosServiceState column names.
    public static final String AMBIENT_TEMPERATURE = "AmbientTemperature";
    public static final String CONTROLLER_TEMPERATURE = "ControllerTemperature";
    public static final String INPUT_CURRENT = "InputCurrent";
    public static final String SYSTEM_LOAD = "SystemLoad";
    public static final String RELATIVE_HUMIDITY = "RelativeHumidity";
    public static final String BATTERY_1_VOLTAGE = "Battery1Voltage";
    public static final String BATTERY_2_VOLTAGE = "Battery2Voltage";
    public static final String IS_LOW_POWER_SHUTDOWN_ON = "IsLowPowerShutdownOn";
    public static final String BATTERY_1_LEVEL = "Battery1Level";
    public static final String BATTERY_2_LEVEL = "Battery2Level";
    public static final String VOLTAGE_LOW_STRING = "VoltageLow";
    public static final String IS_BILL_ACCEPTOR = "IsBillAcceptor";
    public static final String IS_COIN_ACCEPTOR = "IsCoinAcceptor";
    public static final String IS_CARD_READER = "IsCardReader";
    public static final float BATTERY_LOW_VOLTAGE_THRESHOLD = 11.9f;
    public static final float BATTERY_NORMAL_VOLTAGE_THRESHOLD = 12.5f;
    public static final float BATTERY_VOLTAGE_THROWOUT_THRESHOLD = 6.00f;
    public static final float BATTERY_N_A_VOLTAGE_DEFAULT = 32767.0f;
    public static final float BATTERY_N_A_VOLTAGE_ZERO = 0.0f;
    
    public static final int LEVEL_EMPTY = 0;
    public static final int LEVEL_LOW = 1;
    public static final int LEVEL_NORMAL = 2;
    public static final int LEVEL_FULL = 3;
    public static final int LEVEL_JAM = 4;
    
    // Processor Groups
    public static final String DIRECT = "processor.group.direct";
    public static final String GATEWAY = "processor.group.gateway";
    public static final String VALUECARD = "processor.group.valuecard";
    public static final String EMV = "processor.group.emv";
    
    public static final String EMPTY_STRING = "";
    public static final Date EMPTY_DATE = new Date(0);
    
    public static final String PROCESS_TRANSACTION_TEST_CHARGE_FAILED = "processTransactionTestChargeFailed";
    
    /* Terminal Account */
    public static final String TERMINAL_ENABLED = "Enabled";
    public static final String TERMINAL_DISABLED = "Disabled";
    public static final String TERMINAL_DELETED = "Deleted";
    public static final String TERMINAL_INVALID = "INVALID";
    
    /* Common date format as defined in the SRS */
    public static final String DATE_FORMAT = "MMM dd, yyyy h:mm a";
    public static final String DATE_NO_TIME_FORMAT = "MMM dd, yyyy";
    public static final String DATETIME_FORMAT = "MMM dd, yyyy h:mm:ss a";
    public static final String DATE_UI_FORMAT = "MM/dd/yyyy";
    public static final String TIME_UI_FORMAT = "H:mm";
    public static final String TIME_REPORTING_FORMAT = "H:mm:ss";
    public static final String TIME_UI_FORMAT_TODAY = "h:mm a";
    
    public static final int POS_TYPE_DIGITAL_API = 2;
    
    public static final int ROUTE_TYPE_COLLECTIONS = 1;
    public static final int ROUTE_TYPE_MAINTENANCE = 2;
    public static final int ROUTE_TYPE_OTHER = 3;
    
    public static final int MOBILE_LICENSE_STATUS_AVAILABLE = 1;
    public static final int MOBILE_LICENSE_STATUS_PROVISIONED = 2;
    
    public static final int USER_ACCOUNT_MOBILE_STATUS_ACTIVE = 1;
    public static final int USER_ACCOUNT_MOBILE_STATUS_LOGGED_IN = 2;
    public static final int USER_ACCOUNT_MOBILE_STATUS_LOGGED_OUT = 3;
    
    //    public final static String USER_ACCOUNT_MOBILE_STATUS_ACTIVE_STRING = "active";
    //    public final static String USER_ACCOUNT_MOBILE_STATUS_LOGGED_IN_STRING = "logged in";
    //    public final static String USER_ACCOUNT_MOBILE_STATUS_LOGGED_OUT_STRING = "logged out";
    
    public static final int MOBILE_APP_TYPE_COLLECT = 1;
    //Not used as of 7.1
    public static final int MOBILE_APP_TYPE_MAINTAIN = 2;
    public static final int MOBILE_APP_TYPE_PATROL = 3;
    
    // These are not congruent with any database entries.
    // These are used to provide a natural way to sort mobile device information presented to the UI
    public static final int MOBILE_DEVICE_STATUS_PROVISIONED = 1;
    public static final int MOBILE_DEVICE_STATUS_INACTIVE = 2;
    public static final int MOBILE_DEVICE_STATUS_BLOCKED = 3;
    
    //    public final static String MOBILE_DEVICE_STATUS_PROVISIONED_STRING = "Provisioned";
    //    public final static String MOBILE_DEVICE_STATUS_INACTIVE_STRING = "Inactive";
    //    public final static String MOBILE_DEVICE_STATUS_LOCKED_STRING = "Blocked";
    
    public static final int MOBILE_APP_ACTIVITY_TYPE_PROVISION = 1;
    public static final int MOBILE_APP_ACTIVITY_TYPE_LOGIN = 2;
    public static final int MOBILE_APP_ACTIVITY_TYPE_LOGOUT = 3;
    public static final int MOBILE_APP_ACTIVITY_TYPE_BLOCK = 4;
    public static final int MOBILE_APP_ACTIVITY_TYPE_UNBLOCK = 5;
    public static final int MOBILE_APP_ACTIVITY_TYPE_RELEASE = 6;
    
    /* Number of records returned each time additional records are requested when scrolling down an infinite scroll list */
    public static final int INFINITE_SCROLL_RESULT_COUNT = 25;
    public static final int MAX_RESULT_COUNT = 100;
    public static final int PAY_STATIONS_PER_PAGE = 150;
    public static final int FLEX_LOCATION_FACILITIES_PER_PAGE = 150;
    public static final int FLEX_LOCATION_PROPERTIES_PER_PAGE = 150;
    public static final int CASE_LOCATION_LOTS_PER_PAGE = 150;
    
    /* List of possible modules for the pay station details page */
    public static final int EVENT_DEVICE_TYPE_BATTERY = 0;
    public static final int EVENT_DEVICE_TYPE_BATTERY2 = 1;
    public static final int EVENT_DEVICE_TYPE_BILL_ACCEPTOR = 2;
    public static final int EVENT_DEVICE_TYPE_BILL_STACKER = 3;
    public static final int EVENT_DEVICE_TYPE_CARD_READER = 4;
    public static final int EVENT_DEVICE_TYPE_COIN_ACCEPTOR = 5;
    public static final int EVENT_DEVICE_TYPE_COIN_CHANGER = 6;
    public static final int EVENT_DEVICE_TYPE_COIN_HOPPER1 = 7;
    public static final int EVENT_DEVICE_TYPE_COIN_HOPPER2 = 8;
    public static final int EVENT_DEVICE_TYPE_DOOR = 9;
    public static final int EVENT_DEVICE_TYPE_DOOR_LOWER = 10;
    public static final int EVENT_DEVICE_TYPE_DOOR_UPPER = 11;
    public static final int EVENT_DEVICE_TYPE_PAY_STATION = 12;
    public static final int EVENT_DEVICE_TYPE_PRINTER = 13;
    public static final int EVENT_DEVICE_TYPE_SHOCK_ALARM = 14;
    public static final int EVENT_DEVICE_TYPE_PCM = 15;
    public static final int EVENT_DEVICE_TYPE_COIN_CANISTER = 16;
    public static final int EVENT_DEVICE_TYPE_MAINTENANCE_DOOR = 17;
    public static final int EVENT_DEVICE_TYPE_CASH_VAULT_DOOR = 18;
    public static final int EVENT_DEVICE_TYPE_COIN_ESCROW = 19;
    public static final int EVENT_DEVICE_TYPE_ALERT = 20;
    public static final int EVENT_DEVICE_TYPE_MODEM = 21;
    public static final int EVENT_DEVICE_TYPE_RFID_CARD_READER = 22;
    
    /* Event Action Type */
    public static final int EVENT_ACTION_TYPE_ALERTED = 1;
    public static final int EVENT_ACTION_TYPE_CLEARED = 2;
    
    public static final String EVENT_ACTION_TYPE_ALERTED_STRING = "Alerted";
    public static final String EVENT_ACTION_TYPE_CLEARED_STRING = "Cleared";
    
    /* Collection types used for filtering recent collections in the collections center */
    public static final int COLLECTION_TYPE_ALL = 0;
    public static final int COLLECTION_TYPE_BILL = 1;
    public static final int COLLECTION_TYPE_COIN = 2;
    public static final int COLLECTION_TYPE_CARD = 3;
    
    /* Payment types */
    public static final int PAYMENT_TYPE_CASH = 1;
    public static final int PAYMENT_TYPE_CREDIT_CARD_SWIPE = 2;
    public static final int PAYMENT_TYPE_PATROLLER_VALUE = 3;
    public static final int PAYMENT_TYPE_SMART_CARD = 4;
    public static final int PAYMENT_TYPE_CASH_CREDIT_SWIPE = 5;
    public static final int PAYMENT_TYPE_CASH_SMART = 6;
    public static final int PAYMENT_TYPE_VALUE_CARD = 7;
    public static final int PAYMENT_TYPE_CASH_VALUE = 9;
    public static final int PAYMENT_TYPE_UNKNOWN = 10;
    public static final int PAYMENT_TYPE_CREDIT_CARD_CONTACTLESS = 11;
    public static final int PAYMENT_TYPE_CASH_CREDIT_CL = 12;
    public static final int PAYMENT_TYPE_CREDIT_CARD_CHIP = 13;
    public static final int PAYMENT_TYPE_CASH_CREDIT_CH = 14;
    public static final int PAYMENT_TYPE_CREDIT_CARD_EXTERNAL = 15;
    public static final int PAYMENT_TYPE_CREDIT_CARD_FSWIPE = 16;
    public static final int PAYMENT_TYPE_CASH_CREDIT_CARD_FSWIPE = 17;
    
    /* Pos date types; used in sys admin for inserting new rows into PosDate table to update PosStatus */
    public static final int POS_DATE_TYPE_PROVISIONED = 1;
    public static final int POS_DATE_TYPE_LOCKED = 2;
    public static final int POS_DATE_TYPE_DECOMMISSIONED = 3;
    public static final int POS_DATE_TYPE_DELETED = 4;
    public static final int POS_DATE_TYPE_VISIBLE = 5;
    public static final int POS_DATE_TYPE_BILLABLE_MONTHLY_ON_ACTIVATION = 6;
    public static final int POS_DATE_TYPE_DIGITAL_CONNECT = 7;
    public static final int POS_DATE_TYPE_TEST_ACTIVATED = 8;
    public static final int POS_DATE_TYPE_ACTIVATED = 9;
    public static final int POS_DATE_TYPE_BILLABLE_MONTHLY_FOR_EMS = 10;
    public static final int POS_DATE_TYPE_WAS_MOVED_TO = 201;
    public static final int POS_DATE_TYPE_WAS_MOVED_AWAY = 202;
    public static final int POS_DATE_TYPE_COMMENT_ADDED = 203;
    
    public static final char[] ALLOWED_NON_ALPHANUM_CHARACTERS = { ' ', '-', '_', '`', ',', '/' };
    
    public static final int SUBSCRIPTION_TYPE_STANDARD_REPORT = 100;
    public static final int SUBSCRIPTION_TYPE_ALERTS = 200;
    public static final int SUBSCRIPTION_TYPE_REALTIME_CC_PROCESSING = 300;
    public static final int SUBSCRIPTION_TYPE_BATCH_CC_PROCESSING = 400;
    public static final int SUBSCRIPTION_TYPE_REALTIME_CAMPUS_CARD_PROCESSING = 500;
    public static final int SUBSCRIPTION_TYPE_COUPONS = 600;
    public static final int SUBSCRIPTION_TYPE_PASSCARDS = 700;
    public static final int SUBSCRIPTION_TYPE_SMART_CARDS = 800;
    public static final int SUBSCRIPTION_TYPE_EXTEND_BY_PHONE = 900;
    public static final int SUBSCRIPTION_TYPE_DIGITAL_API_READ = 1000;
    public static final int SUBSCRIPTION_TYPE_DIGITAL_API_WRITE = 1100;
    public static final int SUBSCRIPTION_TYPE_3RD_PARTY_PAY_BY_CELL = 1200;
    public static final int SUBSCRIPTION_TYPE_DIGITAL_COLLECT = 1300;
    //Not used in 7.1
    public static final int SUBSCRIPTION_TYPE_DIGITAL_MAINTAIN = 1400;
    //Not used in 7.1
    public static final int SUBSCRIPTION_TYPE_DIGITAL_PATROL = 1500;
    public static final int SUBSCRIPTION_TYPE_ONLINE_PAYSTATION_CONFIGURATION = 1600;
    public static final int SUBSCRIPTION_TYPE_FLEX_INTEGRATION = 1700;
    public static final int SUBSCRIPTION_TYPE_CASE_INTEGRATION = 1800;
    public static final int SUBSCRIPTION_TYPE_DIGITAL_API_XCHANGE = 1900;
    public static final int SUBSCRIPTION_TYPE_ONLINE_RATE_CONFIGURATION = 2000;
    
    /* Locations */
    public static final String LOCATION_TREE = "locationTree";
    
    /* Authorization Type */
    public static final int AUTH_TYPE_NONE = 0;
    public static final int AUTH_TYPE_INTERNAL_LIST = 1;
    public static final int AUTH_TYPE_EXTERNAL_SERVER = 2;
    
    /* Events & Alerts */
    public static final String EVENTS_ALERTS_PROPERTIES = "/events_alerts.properties";
    
    public static final int OLDEST_DAY_FOR_HISTORY_ALERT = -90;
    public static final int OLDEST_DAY_FOR_HISTORY_ALERT_MAP = -1;
    
    /* Pay station types */
    public static final int PAY_STATION_TYPE_NA = 0;
    public static final int PAY_STATION_TYPE_LUKE = 1;
    public static final int PAY_STATION_TYPE_SHELBY = 2;
    public static final int PAY_STATION_TYPE_LUKE_RADIUS = 3;
    public static final int PAY_STATION_TYPE_LUKE_2 = 5;
    public static final int PAY_STATION_TYPE_LUKE_B = 6;
    public static final int PAY_STATION_TYPE_TEST_DEMO = 8;
    public static final int PAY_STATION_TYPE_TEST_VIRTUAL = 9;
    public static final int PAY_STATION_TYPE_TEST_INTELLEPAY = 255;
    
    /*
     * Point of sale types
     * public final static int POINT_OF_SALE_TYPE_UKNOWN = 0;
     * public final static int POINT_OF_SALE_TYPE_DPT_PAY_STATION = 1;
     * public final static int POINT_OF_SALE_TYPE_DIGITAL_API = 2;
     */
    
    /* Modem Types */
    public static final int MODEM_TYPE_NO_MODEM = 0;
    public static final int MODEM_TYPE_UNKNOWN = 1;
    public static final int MODEM_TYPE_CDMA = 2;
    public static final int MODEM_TYPE_ETHERNET = 3;
    public static final int MODEM_TYPE_GSM = 4;
    public static final int MODEM_TYPE_WIFI = 5;
    
    /* XML-RPC */
    public static final String RFID_DATA = "1";
    public static final String CHIP_DATA = "1";
    public static final String MAPPING_PROPERTIES_FILE_NAME = "mapping.properties";
    public static final String STATIC_CONTENT_PATHS_PROPERTIES_FILE_NAME = "/StaticContentPaths.properties";
    public static final String PATH_UI_ACCESS_CONF = "/ui-access.properties";
    
    // New Ps2 Format
    public static final String DEFAULT_NEW_PS2_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    
    //    public final static int PROCESSOR_TRANSACTION_TYPE_PREAUTH_ID = 1;
    //    public final static int PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_NO_REFUNDS_ID = 2;
    //    public final static int PROCESSOR_TRANSACTION_TYPE_SETTLE_WITH_REFUNDS_ID = 6;
    //    public final static int PROCESSOR_TRANSACTION_TYPE_UNCLOSEABLE_ID = 8;
    //    public final static int PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_NO_REFUNDS_ID = 3;
    //    public final static int PROCESSOR_TRANSACTION_TYPE_CHARGE_WITH_REFUNDS_ID = 7;
    //    public final static int PROCESSOR_TRANSACTION_TYPE_SINGLE_TX_CHARGE_WITH_NO_REFUND_ID = 21;
    //    public final static int PROCESSOR_TRANSACTION_TYPE_SINGLE_TX_REFUND = 22;
    //    public final static int PROCESSOR_TRANSACTION_TYPE_SINGLE_TX_CHARGE_WITH_REFUND_ID = 23;
    //    public final static int PROCESSOR_TRANSACTION_TYPE_CANCELLED_ID = 99;
    
    public static final int TRANSACTION_TYPE_SC_RECHARGE_ID = 4;
    public static final int TRANSACTION_TYPE_CANCELLED_ID = 5;
    public static final int TRANSACTION_TYPE_EBP_ADDTIME = 16;
    public static final int TRANSACTION_TYPE_EBP_REGULAR = 17;
    public static final int CUSTOMER_CARD_TYPE_CARD_TYPE_SMART_CARD_ID = 2;
    
    public static final String BLANK = "";
    
    /* Transaction Types */
    public static final String TEST_NAME = "Test";
    public static final String RATE_NAME_TEST_TRANSACTION = "Test Transaction";
    
    /* action services */
    public static final String ACTION_SERVICE_COMMUNICATION = "CommunicationAlert";
    public static final String ACTION_SERVICE_COLLECTION = "CollectionAlert";
    public static final String ACTION_SERVICE_SMS_PERMIT = "SmsPermitAlert";
    public static final String ACTION_SERVICE_OVERDUE_COLLECTION = "OverdueCollection";
    public static final String ACTION_SERVICE_SEPARATE_COLLECTION_COIN_COUNT = "SeparateCollectionCoinCount";
    public static final String ACTION_SERVICE_SEPARATE_COLLECTION_COIN_DOLLARS = "SeparateCollectionCoinDollars";
    public static final String ACTION_SERVICE_SEPARATE_COLLECTION_BILL_COUNT = "SeparateCollectionBillCount";
    public static final String ACTION_SERVICE_SEPARATE_COLLECTION_BILL_DOLLARS = "SeparateCollectionBillDollars";
    public static final String ACTION_SERVICE_SEPARATE_COLLECTION_CC_COUNT = "SeparateCollectionCreditCardCount";
    public static final String ACTION_SERVICE_SEPARATE_COLLECTION_CC_DOLLARS = "SeparateCollectionCreditCardDollars";
    public static final String ACTION_SERVICE_PERIODIC_RUNNING_TOTAL_UPDATE = "PeriodicRunningTotalUpdate";
    public static final String ACTION_SERVICE_PAYSTATION_EVENT = "PaystationEventAlert";
    public static final String ACTION_SERVICE_PURCHASE_EMAILADDRESS = "PurchaseEmailAddress";
    public static final String ACTION_SERVICE_DUPLICATE_SERIAL_NUMBER = "DuplicateSerialNumber";
    
    /* transaction push */
    public static final String VERRUS_STRING = "Verrus";
    public static final String STREETLINE_TXN_PUSH_STRING = "StreetlineTxnPush";
    public static final String STREETLINE_STATUS_PUSH_STRING = "StreetlineStatusPush";
    public static final int NOTAX = 0;
    public static final int POS_SPACE_QUERY_BY_LOT_SETTING_NAME = 0;
    public static final int POS_SPACE_QUERY_BY_LOCATION_ID = 1;
    public static final int POS_SPACE_QUERY_BY_CUSTOMER = 2;
    
    public static final String CATALINE_HOME_PARAMETER = "catalina.home";
    /* Save track data */
    public static final String DEFAULT_TRACKDATAFILE_LOCATION = System.getProperty(CATALINE_HOME_PARAMETER) + "/trackdatafiles/";
    public static final String SAVE_TRACKDATA_SUFFIX = "cc_";
    
    /* Archive old data */
    public static final String DEFAULT_ARCHIVE_LOCATION = System.getProperty(CATALINE_HOME_PARAMETER) + "/archives/";
    public static final String DEFAULT_TEMPFILE_LOCATION = System.getProperty(CATALINE_HOME_PARAMETER) + "/emstempfiles/";
    public static final String DEFAULT_FILE_UPLOAD_LOCATION = System.getProperty(CATALINE_HOME_PARAMETER) + "/fileUploads/";
    public static final String DEFAULT_TRANSACTION_UPLOAD_LOCATION = System.getProperty(CATALINE_HOME_PARAMETER) + "/transactions/";
    public static final String DEFAULT_TRANSACTION_UPLOAD_QUARANTINE_LOCATION =
            System.getProperty(CATALINE_HOME_PARAMETER) + "/transactions/quarantines/";
    public static final String DEFAULT_TRANSACTION_FILES_ERROR_LOCATION = System.getProperty(CATALINE_HOME_PARAMETER) + "/zipHpzErrors/";
    public static final String FILE_NAME_PREFIX_BAD_CARDS = "BadCards";
    public static final String ARCHIVE_FILE_NAME_PREFIX_FOR_POS_SENSOR_INFO = "POSSensorInfo_Archive_";
    public static final String ARCHIVE_FILE_NAME_PREFIX_FOR_POS_BATTERY_INFO = "POSBatteryInfo_Archive_";
    public static final String CSV_FILE_NAME_EXTENSION = ".csv";
    public static final int PAYSTATION_ID_RETRIEVE_BATCH_SIZE = 5000;
    
    /* Query Spaces By */
    public static final String QUERY_SPACES_BY_LOT = "1";
    public static final String QUERY_SPACES_BY_LOCATION = "2";
    public static final String QUERY_SPACES_BY_CUSTOMER = "3";
    public static final String QUERY_SPACES_BY_LOT_STRING = "Lot";
    public static final String QUERY_SPACES_BY_LOCATION_STRING = "Location";
    public static final String QUERY_SPACES_BY_CUSTOMER_STRING = "Customer";
    
    /* LocationPOSTree types */
    public static final int LOCATIONPOS_TYPE_ALL = 0;
    public static final int LOCATIONPOS_TYPE_PARENT_LOCATION = 1;
    public static final int LOCATIONPOS_TYPE_CHILD_LOCATION = 2;
    public static final int LOCATIONPOS_TYPE_PAY_STATION = 3;
    public static final int LOCATIONPOS_TYPE_ROUTE = 4;
    
    /* WebService End Points */
    public static final int END_POINT_ID_STALL_INFO = 1;
    public static final int END_POINT_ID_PAY_STATION_INFO = 2;
    public static final int END_POINT_ID_TRANSCTION_INFO = 3;
    public static final int END_POINT_ID_PLATE_INFO = 4;
    public static final int END_POINT_ID_AUDIT_INFO = 5;
    public static final int END_POINT_ID_TRANSACTION_DATA = 6;
    
    /* Extensible Rate and Permission */
    //One day
    public static final int DEFAULT_MAX_EXTENDABLE_TIME_IN_MINUTES = 24 * 60;
    public static final int PERMISSION_TYPE_TIME_LIMITED_PARKING = 1;
    public static final int PERMISSION_TYPE_UNLIMITED_PARKING = 2;
    public static final int PERMISSION_TYPE_RESTRICTED_PARKING = 3;
    public static final int PERMISSION_TYPE_SPECIAL_PARKING = 4;
    
    /* CardRetryTransactionType */
    public static final int CARD_RETRY_TRANSACTION_TYPE_BATCHED = 0;
    public static final int CARD_RETRY_TRANSACTION_TYPE_IMPORTED = 1;
    
    public static final String ALL_STRING = "All";
    public static final String N_A_STRING = "N/A";
    
    public static final String COMMA = ",";
    
    /* form default input value */
    public static final String NOT_APPLICABLE = N_A_STRING;
    
    /* Event Action types in EMS 6 */
    public static final String EVENT_ACTION_ALARM_OFF_STRING = "AlarmOff";
    public static final String EVENT_ACTION_ALARM_ON_STRING = "AlarmOn";
    public static final String EVENT_ACTION_BILLS_ACCEPTED_STRING = "BillsAccepted";
    public static final String EVENT_ACTION_CLOSED_STRING = "Closed";
    public static final String EVENT_ACTION_COINS_ACCEPTED_STRING = "CoinsAccepted";
    public static final String EVENT_ACTION_COINS_DISPENSED_STRING = "CoinsDispensed";
    public static final String EVENT_ACTION_PAYSTATION_COINCHANGER_LEVEL_LOW_STRING = "CoinChangerLow";
    public static final String EVENT_ACTION_PAYSTATION_COINCHANGER_LEVEL_HIGH_STRING = "CoinChangerHigh";
    public static final String EVENT_ACTION_PAYSTATION_COINCHANGER_JAM_STRING = "CoinChangerJam";
    public static final String EVENT_ACTION_PAYSTATION_COINCHANGER_JAM_CLEARED_STRING = "CoinChangerJamCleared";
    public static final String EVENT_ACTION_CUTTER_ERROR_STRING = "CutterError";
    public static final String EVENT_ACTION_CUTTER_ERROR_CLEARED_STRING = "CutterErrorCleared";
    public static final String EVENT_ACTION_DISABLED_STRING = "Disabled";
    public static final String EVENT_ACTION_DOOR_OPENED_STRING = "DoorOpened";
    public static final String EVENT_ACTION_DOOR_CLOSED_STRING = "DoorClosed";
    public static final String EVENT_ACTION_EMPTY_STRING = "Empty";
    public static final String EVENT_ACTION_EMPTY_CLEARED_STRING = "EmptyCleared";
    public static final String EVENT_ACTION_LOW_STRING = "Low";
    public static final String EVENT_ACTION_NORMAL_STRING = "Normal";
    public static final String EVENT_ACTION_ENABLED_STRING = "Enabled";
    public static final String EVENT_ACTION_FAILED_STRING = "Failed";
    public static final String EVENT_ACTION_FULL_STRING = "Full";
    public static final String EVENT_ACTION_FULL_CLEARED_STRING = "FullCleared";
    public static final String EVENT_ACTION_HEAD_ERROR_STRING = "HeadError";
    public static final String EVENT_ACTION_HEAD_ERROR_CLEARED_STRING = "HeadErrorCleared";
    public static final String EVENT_ACTION_JAM_STRING = "Jam";
    public static final String EVENT_ACTION_JAM_CLEARED_STRING = "JamCleared";
    public static final String EVENT_ACTION_LEVER_DISENGAGED_STRING = "LeverDisengaged";
    public static final String EVENT_ACTION_LEVER_DISENGAGED_CLEARED_STRING = "LeverDisengagedCleared";
    public static final String EVENT_ACTION_NOT_PRESENT_STRING = "NotPresent";
    public static final String EVENT_ACTION_OPENED_STRING = "Opened";
    public static final String EVENT_ACTION_PAPER_NORMAL_STRING = "PaperNormal";
    public static final String EVENT_ACTION_PAPER_LOW_STRING = "PaperLow";
    public static final String EVENT_ACTION_PAPER_LOW_CLEARED_STRING = "PaperLowCleared";
    public static final String EVENT_ACTION_PAPER_OUT_STRING = "PaperOut";
    public static final String EVENT_ACTION_PAPER_OUT_CLEARED_STRING = "PaperOutCleared";
    public static final String EVENT_ACTION_PAPER_JAM_STRING = "PaperJam";
    public static final String EVENT_ACTION_PAPER_JAM_CLEARED_STRING = "PaperJamCleared";
    public static final String EVENT_ACTION_PAPER_TICKET_DID_NOT_PRINT_STRING = "TicketDidNotPrint";
    public static final String EVENT_ACTION_PAPER_TICKET_DID_NOT_PRINT_CLEARED_STRING = "TicketDidNotPrintCleared";
    public static final String EVENT_ACTION_PAPER_PRINTING_FAILURE_STRING = "PrintingFailure";
    public static final String EVENT_ACTION_PAPER_PRINTING_FAILURE_CLEARED_STRING = "PrintingFailureCleared";
    public static final String EVENT_ACTION_TICKET_NOT_TAKEN_STRING = "TicketNotTaken";
    public static final String EVENT_ACTION_TICKET_NOT_TAKEN_CLEARED_STRING = "TicketNotTakenCleared";
    public static final String EVENT_ACTION_PAYSTATION_FAILURE_STRING = "PaystationFailure";
    public static final String EVENT_ACTION_PAYSTATION_FAILURE_CLEARED_STRING = "PaystationFailureCleared";
    public static final String EVENT_ACTION_PRESENT_STRING = "Present";
    public static final String EVENT_ACTION_RESET_STRING = "Reset";
    public static final String EVENT_ACTION_SERVICE_MODE_DISABLED_STRING = "ServiceModeDisabled";
    public static final String EVENT_ACTION_SERVICE_MODE_ENABLED_STRING = "ServiceModeEnabled";
    public static final String EVENT_ACTION_TEMPERATURE_ERROR_STRING = "TemperatureError";
    public static final String EVENT_ACTION_TEMPERATURE_ERROR_CLEARED_STRING = "TemperatureErrorCleared";
    public static final String EVENT_ACTION_VOLTAGE_STRING = "Voltage";
    public static final String EVENT_ACTION_VOLTAGE_ERROR_STRING = "VoltageError";
    public static final String EVENT_ACTION_VOLTAGE_ERROR_CLEARED_STRING = "VoltageErrorCleared";
    public static final String EVENT_ACTION_UNABLE_TO_STACK_STRING = "UnableToStack";
    public static final String EVENT_ACTION_UNABLE_TO_STACK_CLEARED_STRING = "UnableToStackCleared";
    public static final String EVENT_ACTION_UPGRADE_STRING = "Upgrade";
    public static final String EVENT_ACTION_LOW_POWER_SHUTDOWN_OFF_STRING = "LowPowerShutdownOff";
    public static final String EVENT_ACTION_LOW_POWER_SHUTDOWN_ON_STRING = "LowPowerShutdownOn";
    public static final String EVENT_ACTION_SHOCK_OFF_STRING = "ShockOff";
    public static final String EVENT_ACTION_SHOCK_ON_STRING = "ShockOn";
    public static final String EVENT_ACTION_WIRELESS_SIGNAL_STRENGTH_STRING = "WirelessSignalStrength";
    public static final String EVENT_ACTION_REMOVED_STRING = "Removed";
    public static final String EVENT_ACTION_INSERTED_STRING = "Inserted";
    public static final String EVENT_ACTION_TYPE_STRING = "Type";
    public static final String EVENT_ACTION_CCID_STRING = "CCID";
    public static final String EVENT_ACTION_MODEM_HARDWARE_MANUFACTURE_STRING = "HardwareManufacturer";
    public static final String EVENT_ACTION_MODEM_HARDWARE_MODEL_STRING = "HardwareModel";
    public static final String EVENT_ACTION_MODEM_HARDWARE_FIRMWARE_STRING = "HardwareFirmware";
    public static final String EVENT_ACTION_CARRIER_STRING = "Carrier";
    public static final String EVENT_ACTION_APN_STRING = "APN";
    public static final String EVENT_ACTION_VOLTAGE_LOW_STRING = "VoltageLow";
    public static final String EVENT_ACTION_BATTERY_1_STRING = "Battery";
    public static final String EVENT_ACTION_BATTERY_2_STRING = "Battery2";
    public static final String EVENT_ACTION_PUBLIC_KEY_UPDATE_STRING = "PublicKeyUpdate";
    public static final String EVENT_ACTION_PUBLIC_KEY_UPDATE_OFF_STRING = "PublicKeyUpdateOff";
    public static final String EVENT_ACTION_UID_CHANGED_STRING = "UIDChanged";
    public static final String EVENT_ACTION_UID_ALERT_STRING = "DuplicateSerialNumber";
    public static final String EVENT_ACTION_UID_CLEARED_STRING = "DuplicateSerialNumberCleared";
    public static final String EVENT_ACTION_CLOCK_CHANGED_STRING = "ClockChanged";
    
    public static final int EVENT_STATUS_TYPE_PRESENT = 0;
    public static final int EVENT_STATUS_TYPE_JAM = 1;
    public static final int EVENT_STATUS_TYPE_UNABLE_TO_STACK = 2;
    public static final int EVENT_STATUS_TYPE_LEVEL = 3;
    public static final int EVENT_STATUS_TYPE_OPEN = 4;
    public static final int EVENT_STATUS_TYPE_LOW_POWER_SHUTDOWN = 5;
    public static final int EVENT_STATUS_TYPE_PUBLIC_KEY_UPDATE = 6;
    public static final int EVENT_STATUS_TYPE_SERVICE_MODE = 7;
    public static final int EVENT_STATUS_TYPE_UPGRADE = 8;
    public static final int EVENT_STATUS_TYPE_TICKET_FOOTER_SUCCESS = 9;
    public static final int EVENT_STATUS_TYPE_CUTTER_ERROR = 10;
    public static final int EVENT_STATUS_TYPE_HEAD_ERROR = 11;
    public static final int EVENT_STATUS_TYPE_LEVER_DISENGAGED = 12;
    public static final int EVENT_STATUS_TYPE_PAPER_STATUS = 13;
    public static final int EVENT_STATUS_TYPE_TEMPERATURE_ERROR = 14;
    public static final int EVENT_STATUS_TYPE_VOLTAGE_ERROR = 15;
    public static final int EVENT_STATUS_TYPE_ON = 16;
    public static final int EVENT_STATUS_TYPE_WIRELESS_SIGNAL_STRENGTH = 17;
    public static final int EVENT_STATUS_TYPE_REMOVE = 18;
    public static final int EVENT_STATUS_TYPE_ALERT = 19;
    public static final int EVENT_STATUS_TYPE_CCID = 20;
    public static final int EVENT_STATUS_TYPE_CARRIER = 21;
    public static final int EVENT_STATUS_TYPE_APN = 22;
    public static final int EVENT_STATUS_TYPE_CUSTOMER_DEFINED_ALERT = 23;
    public static final int EVENT_STATUS_TYPE_UID = 27;
    
    /* Event Definition ids in EMS 7 */
    public static final int EVENT_DEFINITION_BATTERY_LEVEL_NORMAL = 1;
    public static final int EVENT_DEFINITION_BATTERY_LEVEL_LOW = 2;
    public static final int EVENT_DEFINITION_BATTERY_2_LEVEL_NORMAL = 3;
    public static final int EVENT_DEFINITION_BATTERY_2_LEVEL_LOW = 4;
    public static final int EVENT_DEFINITION_BILLACCEPTOR_JAM_ON = 7;
    public static final int EVENT_DEFINITION_BILLACCEPTOR_JAM_OFF = 8;
    public static final int EVENT_DEFINITION_BILLACCEPTOR_UNABLETOSTACK_ON = 9;
    public static final int EVENT_DEFINITION_BILLACCEPTOR_UNABLETOSTACK_OFF = 10;
    public static final int EVENT_DEFINITION_BILLSTACKER_LEVEL_FULL = 13;
    public static final int EVENT_DEFINITION_BILLSTACKER_LEVEL_NORMAL = 14;
    public static final int EVENT_DEFINITION_COINACCEPTOR_JAM_ON = 21;
    public static final int EVENT_DEFINITION_COINACCEPTOR_JAM_OFF = 22;
    public static final int EVENT_DEFINITION_COINCHANGER_JAM_ON = 25;
    public static final int EVENT_DEFINITION_COINCHANGER_JAM_OFF = 26;
    public static final int EVENT_DEFINITION_COINCHANGER_LEVEL_NORMAL = 27;
    public static final int EVENT_DEFINITION_COINCHANGER_LEVEL_LOW = 28;
    public static final int EVENT_DEFINITION_COINCHANGER_LEVEL_EMPTY = 29;
    public static final int EVENT_DEFINITION_COINHOPPER1_LEVEL_NORMAL = 32;
    public static final int EVENT_DEFINITION_COINHOPPER1_LEVEL_EMPTY = 33;
    public static final int EVENT_DEFINITION_COINHOPPER2_LEVEL_NORMAL = 36;
    public static final int EVENT_DEFINITION_COINHOPPER2_LEVEL_EMPTY = 37;
    public static final int EVENT_DEFINITION_PAYSTATION_LOWPOWERSHUTDOWN_ON = 44;
    public static final int EVENT_DEFINITION_PAYSTATION_LOWPOWERSHUTDOWN_OFF = 45;
    public static final int EVENT_DEFINITION_PRINTER_PRESENT_ON = 52;
    public static final int EVENT_DEFINITION_PRINTER_PRESENT_OFF = 53;
    public static final int EVENT_DEFINITION_PRINTER_LEVERDISENGAGED_ON = 58;
    public static final int EVENT_DEFINITION_PRINTER_LEVERDISENGAGED_OFF = 59;
    public static final int EVENT_DEFINITION_PRINTER_PAPERSTATUS_NORMAL = 60;
    public static final int EVENT_DEFINITION_PRINTER_PAPERSTATUS_LOW = 61;
    public static final int EVENT_DEFINITION_PRINTER_PAPERSTATUS_EMPTY = 62;
    public static final int EVENT_DEFINITION_PRINTER_PAPERSTATUS_CLEARED = 63;
    public static final int EVENT_DEFINITION_SHOCKALARM_ON = 68;
    public static final int EVENT_DEFINITION_SHOCKALARM_OFF = 69;
    public static final int EVENT_DEFINITION_COINESCROW_JAM_ON = 82;
    public static final int EVENT_DEFINITION_COINESCROW_JAM_OFF = 83;
    public static final int EVENT_DEFINITION_BILLSTACKER_FULL_CLEARED = 90;
    public static final int EVENT_DEFINITION_PRINTER_STATUS_JAM_ON = 101;
    public static final int EVENT_DEFINITION_PRINTER_STATUS_JAM_OFF = 102;
    public static final int EVENT_DEFINITION_TICKET_NOT_TAKEN_ON = 103;
    public static final int EVENT_DEFINITION_TICKET_NOT_TAKEN_OFF = 104;
    public static final int EVENT_DEFINITION_TICKET_DID_NOT_PRINT_ON = 105;
    public static final int EVENT_DEFINITION_TICKET_DID_NOT_PRINT_OFF = 106;
    public static final int EVENT_DEFINITION_PRINTING_FAILURE_ON = 107;
    public static final int EVENT_DEFINITION_PRINTING_FAILURE_OFF = 108;
    public static final int EVENT_DEFINITION_UID_CHANGED = 109;
    public static final int EVENT_DEFINITION_UID_ALERT = 110;
    public static final int EVENT_DEFINITION_UID_CLEARED = 111;
    public static final int EVENT_DEFINITION_CLOCK_CHANGED = 112;
    
    public static final byte EVENT_TYPE_BATTERY_LEVEL = 1;
    public static final byte EVENT_TYPE_BILLACCEPTOR_PRESENT = 2;
    public static final byte EVENT_TYPE_BILLACCEPTOR_JAM = 3;
    public static final byte EVENT_TYPE_BILLACCEPTOR_LEVEL = 4;
    public static final byte EVENT_TYPE_BILLSTACKER_PRESENT = 5;
    public static final byte EVENT_TYPE_BILLSTACKER_LEVEL = 6;
    public static final byte EVENT_TYPE_BILLCANISTER_REMOVE = 7;
    public static final byte EVENT_TYPE_CARDREADER_PRESENT = 8;
    public static final byte EVENT_TYPE_COINACCEPTOR_PRESENT = 9;
    public static final byte EVENT_TYPE_COINACCEPTOR_JAM = 10;
    public static final byte EVENT_TYPE_COINCHANGER_PRESENT = 11;
    public static final byte EVENT_TYPE_COINCHANGER_JAM = 12;
    public static final byte EVENT_TYPE_COINCHANGER_LEVEL = 13;
    public static final byte EVENT_TYPE_COINHOPPER1_PRESENT = 14;
    public static final byte EVENT_TYPE_COINHOPPER1_LEVEL = 15;
    public static final byte EVENT_TYPE_COINHOPPER2_PRESENT = 16;
    public static final byte EVENT_TYPE_COINHOPPER2_LEVEL = 17;
    public static final byte EVENT_TYPE_DOOR_OPEN = 18;
    public static final byte EVENT_TYPE_DOORLOWER_OPEN = 19;
    public static final byte EVENT_TYPE_DOORUPPER_OPEN = 20;
    public static final byte EVENT_TYPE_LOWER_POWER_SHUTDOWN = 21;
    public static final byte EVENT_TYPE_PAYSTATION_PUBLICKEY = 22;
    public static final byte EVENT_TYPE_PAYSTATION_SERVICEMODE = 23;
    public static final byte EVENT_TYPE_PAYSTATION_UPGRADE = 24;
    public static final byte EVENT_TYPE_PAYSTATION_TICKETFOOTER = 25;
    public static final byte EVENT_TYPE_PRINTER_PRESENT = 26;
    public static final byte EVENT_TYPE_PRINTER_CUTTERERROR = 27;
    public static final byte EVENT_TYPE_PRINTER_HEADERROR = 28;
    public static final byte EVENT_TYPE_PRINTER_LEVELDISENGAGED = 29;
    public static final byte EVENT_TYPE_PRINTER_PAPERSTATUS = 30;
    public static final byte EVENT_TYPE_PRINTER_TEMPERATUREERROR = 31;
    public static final byte EVENT_TYPE_PRINTER_VOLTAGEERROR = 32;
    public static final byte EVENT_TYPE_SHOCK_ALARM = 33;
    public static final byte EVENT_TYPE_PCM_UPGRADER = 34;
    public static final byte EVENT_TYPE_PCM_WIRELESSSIGNAL = 35;
    public static final byte EVENT_TYPE_COINCANISTER_PRESENT = 36;
    public static final byte EVENT_TYPE_COINCANISTER_REMOVED = 37;
    public static final byte EVENT_TYPE_MAINTENANCEDOOR_OPEN = 38;
    public static final byte EVENT_TYPE_CASHVAULTDOOR_OPEN = 39;
    public static final byte EVENT_TYPE_COINESCROW_PRESENT = 40;
    public static final byte EVENT_TYPE_COINESCROW_JAM = 41;
    public static final byte EVENT_TYPE_MODEM_TYPE = 42;
    public static final byte EVENT_TYPE_MODEM_CCID = 43;
    public static final byte EVENT_TYPE_MODEM_NETWORKCARRIER = 44;
    public static final byte EVENT_TYPE_MODEM_APN = 45;
    public static final byte EVENT_TYPE_CUSTOMERDEFINED_ALERT = 46;
    public static final byte EVENT_TYPE_PAYSTATION_COINCHANGER_LEVEL = 47;
    public static final byte EVENT_TYPE_PAYSTATION_COINCHANGER_JAM = 48;
    public static final byte EVENT_TYPE_RFIDCARDREADER_PRESENT = 49;
    public static final byte EVENT_TYPE_PRINTER_PAPERJAM = 50;
    public static final byte EVENT_TYPE_PAYSTATION_TICKETNOTTAKEN = 51;
    public static final byte EVENT_TYPE_PRINTER_TICKETDIDNOTPRINT = 52;
    public static final byte EVENT_TYPE_PRINTER_PRINTINGFAILURE = 53;
    public static final byte EVENT_TYPE_PAYSTATION_UID_CHANGED = 54;
    public static final byte EVENT_TYPE_DUPLICATE_SERIALNUMBER = 55;
    public static final byte EVENT_TYPE_PAYSTATION_CLOCK_CHANGED = 56;
    
    public static final String BATTERY = "Battery";
    public static final String BATTERY_LOW = "Low";
    public static final String BATTERY_NORMAL = "Normal";
    public static final String BATTERY_MAJOR = "Major";
    public static final String BATTERY_NA = N_A_STRING;
    
    public static final int EVENT_ACTION_TYPE_NOTHING = 0;
    public static final int EVENT_ACTION_TYPE_SET = 1;
    public static final int EVENT_ACTION_TYPE_CLEAR = 2;
    public static final int EVENT_ACTION_TYPE_FULL = 3;
    public static final int EVENT_ACTION_TYPE_NORMAL = 4;
    public static final int EVENT_ACTION_TYPE_LOW = 5;
    public static final int EVENT_ACTION_TYPE_EMPTY = 6;
    public static final int EVENT_ACTION_TYPE_HIGH = 7;
    public static final int EVENT_ACTION_TYPE_INFO = 8;
    
    /* SmsMessageType */
    public static final int TRANSACTION_RECEIVED_FROM_PAY_STATION = 1;
    public static final int EXPIRY_ALERT_SENT_OPTION_TO_EXTEND = 2;
    public static final int EXPIRY_ALERT_SENT_NO_OPTION_TO_EXTEND = 3;
    public static final int USER_RESPONSE_RECEIVED = 4;
    public static final int INVALID_USER_RESPONSE_RECEIVED = 5;
    public static final int FAILURE_MESSAGE_SENT_TO_USER = 6;
    public static final int EXTENSION_CONFIRMATION_SENT = 7;
    public static final int SMS_SEND_FAILURE = 8;
    public static final int CREDIT_CARD_DECLINED = 9;
    public static final int UNABLE_TO_PROCESS_CREDIT_CARD = 10;
    public static final int TIME_AUTO_EXTENDED = 11;
    public static final int MESSAGE_FROM_PHONE_NUMBER_NOT_ASSOCIATED_WITH_ACTIVE_PARKING_SESSION = 12;
    public static final int END_CONVERSATION_FAILURE = 13;
    public static final int MESSAGE_FROM_PHONE_NUMBER_NOT_ASSOCIATED_WITH_UNEXPIRED_PARKING_SESSION = 14;
    
    /* Extend By Phone */
    public static final String AREA_CODE_NORTH_AMERICA = "+1";
    public static final int SMSFAILEDRESPONSE_MAX_STRING_LENGTH = 255;
    public static final int SMS_WARNING_PERIOD_MIN = 2;
    public static final int SMS_WARNING_PERIOD_MAX = 60;
    
    public static final int EXTENSIBLE_RATE_TYPE_HOURLY_RATE = 1;
    public static final int EXTENSIBLE_TYPE_SPECIAL_RATE = 2;
    public static final String THIRD_PARTY_ACCOUNT_TYPE_UPSIDEWIRELESS = "Upside Wireless";
    
    /* Transactioninfo WS types */
    public static final int GETTRANSACTIONINFOBYSTALL_TYPE = 1;
    public static final int GETTRANSACTIONINFOBYPURCHASEDDATE_TYPE = 2;
    public static final int GETTRANSACTIONINFOBYSETTLEMENTDATE_TYPE = 3;
    public static final int GETTRANSACTIONINFOBYSERIALNUMBER_TYPE = 4;
    public static final int GETTRANSACTIONINFOBYREGION_TYPE = 5;
    public static final int GETTRANSACTIONINFOBYSETTING_TYPE = 6;
    public static final int GETTRANSACTIONINFOBYGROUP_TYPE = 7;
    
    /* Transactiondata WS types */
    public static final int GETTRANSACTIONDATABYUPDATEDATE_TYPE = 8;
    
    /* Copied from EMS 6.3.11 com.digitalpioneer.domain.DomainConstants. */
    public static final int MYSQL_MAX_UNSIGNED_MEDIUM_INT_VALUE = 16777215;
    public static final int MYSQL_MIN_SIGNED_MEDIUM_INT_VALUE = -8388608;
    public static final int MYSQL_MAX_SIGNED_MEDIUM_INT_VALUE = 8388607;
    public static final int MYSQL_MIN_UNSIGNED_TINY_INT_VALUE = 0;
    public static final int MYSQL_MAX_UNSIGNED_TINY_INT_VALUE = 255;
    public static final int MYSQL_MIN_UNSIGNED_SMALL_INT_VALUE = 0;
    public static final int MYSQL_MAX_UNSIGNED_SMALL_INT_VALUE = 65535;
    
    public static final double MAX_WIDGET_TARGETVALUE = 999999999.99;
    public static final int MAX_LOCATION_CAPACITY = 100000;
    public static final int MAX_LOCATION_REVENUE = 1000000;
    
    public static final int MIN_PAYSTATION_ID = 1;
    public static final int MAX_PAYSATION_ID = MYSQL_MAX_UNSIGNED_MEDIUM_INT_VALUE;
    
    public static final int MIN_MERCHANT_ACCOUNT_ID = 1;
    public static final int MAX_MERCHANT_ACCOUNT_ID = MYSQL_MAX_UNSIGNED_MEDIUM_INT_VALUE;
    
    public static final int MIN_TICKET_NUMBER = 0;
    public static final int MAX_TICKET_NUMBER = Integer.MAX_VALUE;
    
    public static final int MIN_TYPE_ID = 0;
    public static final int MAX_TYPE_ID = MYSQL_MAX_UNSIGNED_TINY_INT_VALUE;
    
    public static final int MIN_AMOUNT_IN_CENTS = 0;
    public static final int MAX_AMOUNT_IN_CENTS = MYSQL_MAX_UNSIGNED_MEDIUM_INT_VALUE;
    
    public static final int MIN_CARD_TYPE_LENGTH = 1;
    public static final int MAX_CARD_TYPE_LENGTH = 20;
    
    public static final int MIN_LAST_4_DIGITS_OF_CREDIT_CARD_NUMBER = 0;
    public static final int MAX_LAST_4_DIGITS_OF_CREDIT_CARD_NUMBER = 9999;
    
    public static final int MIN_CREDIT_CARD_EXPIRY_YYMM = 0001;
    public static final int MAX_CREDIT_CARD_EXPIRY_YYMM = 9912;
    
    public static final int MIN_CARD_PROCESSING_CHECKSUM_VALUE = 0;
    public static final int MAX_CARD_PROCESSING_CHECKSUM_VALUE = MYSQL_MIN_UNSIGNED_SMALL_INT_VALUE;
    
    public static final int MIN_CARD_PROCESSING_PROCESSOR_TRANSACTION_ID_LENGTH = 0;
    public static final int MAX_CARD_PROCESSING_PROCESSOR_TRANSACTION_ID_LENGTH = 30;
    
    public static final int MIN_CARD_PROCESSING_AUTHORIZATION_NUMBER_LENGTH = 0;
    public static final int MAX_CARD_PROCESSING_AUTHORIZATION_NUMBER_LENGTH = 30;
    
    public static final int MIN_CARD_PROCESSING_REFERENCE_NUMBER_LENGTH = 0;
    public static final int MAX_CARD_PROCESSING_REFERENCE_NUMBER_LENGTH = 30;
    
    public static final int SHA_1_HASH_LENGTH = 28;
    public static final int EXACT_CARD_PROCESSING_HASH_LENGTH = SHA_1_HASH_LENGTH;
    public static final int EXACT_BAD_CARD_HASH_LENGTH = SHA_1_HASH_LENGTH;
    
    public static final int MIN_CARD_PROCESSING_NUM_RETRIES = 0;
    public static final int MAX_CARD_PROCESSING_NUM_RETRIES = Short.MAX_VALUE;
    
    public static final short ALERTTHRESHOLDEXCEEDDISPLAY_COUNT = 1;
    public static final short ALERTTHRESHOLDEXCEEDDISPLAY_DOLLAR = 2;
    
    public static final short ALERTTYPE_ID_LASTSEENINTERVAL = 1;
    public static final short ALERTTYPE_ID_RUNNINGTOTAL = 2;
    public static final short ALERTTYPE_ID_COINCANNISTER = 3;
    public static final short ALERTTYPE_ID_BILLSTACKER = 4;
    public static final short ALERTTYPE_ID_UNSETTLEDCREDITCARD = 5;
    public static final short ALERTTYPE_ID_PAYSTATIONALERT = 6;
    public static final short ALERTTYPE_ID_LASTSEENCOLLECTION = 7;
    
    public static final String FORMAT_YYYYMMDD = "yyyyMMdd";
    public static final String REGULAR_EXPRESSION_FORMAT = "([a-zA-Z0-9]*)+";
    
    // Coupon Input fields
    public static final int COUPON_DISCOUNT_MIN = 0;
    public static final int COUPON_DISCOUNT_MAX = 100;
    public static final double COUPON_DOLLARS_DISCOUNT_MAX = 999.99;
    public static final int COUPON_LENGTH_MIN = 1;
    public static final int COUPON_LENGTH_MAX = 10;
    public static final short COUPON_USES_MIN = 1;
    public static final short COUPON_USES_MAX = 9999;
    public static final short COUPON_USES_UNLIMITED = Short.MAX_VALUE;
    // In bytes
    public static final int COUPON_IMPORT_FILE_SIZE_MAX = 51200;
    public static final int COUPON_DESCRIPTION_LENGTH_MAX = 34;
    public static final int COUPON_STALL_RANGES_LENGTH_MAX = 255;
    public static final Date COUPON_DATE_MIN = (new GregorianCalendar(StandardConstants.CONSTANT_2002, 0, 1, 0, 0, 0)).getTime();
    public static final String COUPON_DATE_MIN_STRING = "2002-01-01";
    public static final Date COUPON_DATE_MAX = (new GregorianCalendar(StandardConstants.CONSTANT_2100, 0, 1, StandardConstants.LAST_HOUR_OF_THE_DAY,
            StandardConstants.LAST_MIN_SECOND, StandardConstants.LAST_MIN_SECOND)).getTime();
    public static final String COUPON_DATE_MAX_STRING = "2099-12-31";
    public static final int COUPON_MAX_LOCATION_LENGTH = 50;
    public static final int COUPON_MAX_SPACE_RANGE = 255;
    public static final int COUPON_MAX_MULTI_COUPON_RETRIEVAL = 1000;
    public static final int COUPON_MAX_MULTI_COUPON_DELETION = 1000;
    public static final int COUPON_MAX_MULTI_COUPON_CREATION = 1000;
    public static final int COUPON_MAX_MULTI_COUPON_UPDATE = 1000;
    public static final String COUPON_CODE_PAY_AND_DISPLAY = "PND";
    public static final String COUPON_CODE_PAY_BY_SPACE = "PBS";
    public static final String COUPON_CODE_PAY_BY_LICENSE_PLATE = "PBL";
    public static final String[] COUPON_OPERATING_MODES = { COUPON_CODE_PAY_AND_DISPLAY, COUPON_CODE_PAY_BY_SPACE, COUPON_CODE_PAY_BY_LICENSE_PLATE };
    public static final String COUPON_N_A_START = "2000-01-01T00:00:00";
    public static final String COUPON_N_A_END = "3000-01-01T23:59:59";
    public static final String COUPON_N_A_START_UI = "2000-01-01 00:00:00";
    public static final String COUPON_N_A_END_UI = "3000-01-01 23:59:59";
    public static final int LOCATION_MIN_ID = 1;
    public static final String DISCOUNT_TYPE_PERCENT = "P";
    public static final String DISCOUNT_TYPE_DOLLARS = "D";
    public static final String COUPON_CREATE_UPDATE = "cu";
    public static final String COUPON_DEL_ALL = "da";
    public static final String COUPON_DISCOUNT_TYPE_DOLLAR = "Amount";
    public static final String COUPON_DISCOUNT_TYPE_PERCENT = "Percent";
    
    public static final int STALL_NUM_MIN = 1;
    public static final int STALL_NUM_MAX = 99999;
    public static final char[] ONLINE_COUPON_IDENTIFIERS = new char[] { 'E', 'e' };
    public static final char[] OFFLINE_COUPON_IDENTIFIERS = new char[] { 'B', 'b' };
    public static final char OFFLINE_COUPON_IDENTIFIER = 'B';
    public static final String OFFLINE_COUPON_PLUS = "+";
    
    public static final Date NO_START_DATE = (new GregorianCalendar(StandardConstants.CONSTANT_2000, 0, 1, 0, 0, 0)).getTime();
    public static final Date NO_END_DATE = (new GregorianCalendar(StandardConstants.CONSTANT_3000, 0, 1, StandardConstants.LAST_HOUR_OF_THE_DAY,
            StandardConstants.LAST_MIN_SECOND, StandardConstants.LAST_MIN_SECOND)).getTime();
    public static final String UNLIMITED = "Unlimited";
    public static final int COUPON_INVALID = -1;
    public static final int COUPON_INACTIVE = -2;
    public static final int COUPON_EXPIRED = -3;
    
    public static final Integer ITEMS_PER_PAGE = 25;
    public static String MAPPING_URL;
    
    // CUSTOMER CARD
    public static final int CUSTOM_CARD_GRACE_PERIOD_MAX = 999;
    public static final int CUSTOM_CARD_MAX_NUM_OF_USES = Short.MAX_VALUE;
    
    // CardSettings Validator
    public static final String VALID_TRACK_2_PATTNER_CHARACTERS = "0123456789\\d{}[]=";
    public static final int BAD_CARD_MIN_LIMIT = 1;
    public static final int BAD_CARD_MAX_LIMIT = 99;
    public static final int RETRY_MIN_LIMIT = 3;
    public static final int RETRY_MAX_LIMIT = 99;
    
    public static final String REQKEY_UPLOAD_EXCEPTION = "fileUploadException";
    
    public static final String PERCENT_SIGN = "%";
    public static final String SQL_WILDCARD_PERCENT = PERCENT_SIGN;
    
    public static final String SENSOR_CHARGE_STATE_CHARGING = "Charging";
    public static final String SENSOR_CHARGE_STATE_DISCHARGING = "Discharging";
    public static final String SENSOR_UNIT_VOLTAGE = "V";
    public static final String SENSOR_UNIT_MILLIAMPS = "Milliamps";
    public static final String SENSOR_UNIT_FAHRENHEIT = "F";
    public static final String SENSOR_UNIT_CELSIUS = "C";
    public static final String SENSOR_UNIT_HUMIDITY = PERCENT_SIGN;
    
    public static final float SENSOR_VALUE_UNSET = Short.MAX_VALUE;
    
    public static final NumberFormat SENSOR_BATTERY_FORMATTER = new DecimalFormat("00.0");
    public static final NumberFormat SENSOR_TEMPERATURE_FORMATTER = new DecimalFormat("00.0");
    public static final NumberFormat SENSOR_HUMIDITY_FORMATTER = new DecimalFormat("00");
    public static final NumberFormat SENSOR_POWER_FORMATTER = new DecimalFormat("0000");
    
    public static final String SESSIONKEY_LOGIN_PAGE = "loginPage";
    
    public static final int PS_VERSION_6_4_1 = 640100;
    public static final String ZERO = "0";
    public static final char SINGLE_QUOTE = '\'';
    
    // PDA
    public static final int SEARCH_BY_VALID_STALL = 1;
    public static final int SEARCH_BY_VALID_STALL_RANGE = 2;
    public static final int SEARCH_BY_EXPIRED_STALL = 3;
    public static final int SEARCH_BY_EXPIRED_STALL_RANGE = 4;
    public static final int SEARCH_BY_VALID_PLATE = 5;
    public static final int SEARCH_BY_EXPIRED_PLATE = 6;
    public static final int SEARCH_BY_PLATE = 7;
    public static final String PAYSTATION = "paystation";
    public static final String LOCATION = "location";
    public static final String CUSTOMER = "customer";
    public static final int EXPIRED_PLATE_MINUTES_MIN = 1;
    public static final int EXPIRED_PLATE_MINUTES_MAX = 999;
    
    public static final String PAY_STATION = "PayStation";
    public static final int NUM_RECORDS_FOR_EVENT_HISTORY_EXPORT = 5000;
    
    public static final String BADCARD_SOURCE_IMPORT = "import";
    public static final String BADCARD_SOURCE_MANUAL = "manual";
    public static final String BADCARD_CSV_PROCESSOR_TYPE_CREDIT_CARD = "Credit Card";
    
    public static final int CARD_TYPE_NA = 0;
    public static final int CARD_TYPE_CREDIT_CARD = 1;
    public static final int CARD_TYPE_SMART_CARD = 2;
    public static final int CARD_TYPE_VALUE_CARD = 3;
    public static final int MAX_NUMBERS_LAST_DIGITS = 4;
    public static final int MAX_NUMBERS_FIRST_DIGITS = 6;
    
    public static final byte CUSTOMER_MIGRATION_VALIDATION_STATUS_TYPE_NA = 0;
    public static final byte CUSTOMER_MIGRATION_VALIDATION_STATUS_TYPE_BLOCKED = 1;
    public static final byte CUSTOMER_MIGRATION_VALIDATION_STATUS_TYPE_WARNED = 2;
    public static final byte CUSTOMER_MIGRATION_VALIDATION_STATUS_TYPE_PASSED = 3;
    
    public static final byte CUSTOMER_MIGRATION_VALIDATION_TYPE_EMS6_DBCONNECTION = 1;
    public static final byte CUSTOMER_MIGRATION_VALIDATION_TYPE_EMS6SLAVE_DBCONNECTION = 2;
    public static final byte CUSTOMER_MIGRATION_VALIDATION_TYPE_ETL_UPTODATE = 3;
    public static final byte CUSTOMER_MIGRATION_VALIDATION_TYPE_CARD_RETRY_RUNNING = 4;
    public static final byte CUSTOMER_MIGRATION_VALIDATION_TYPE_PAY_STATION_VERSION = 5;
    public static final byte CUSTOMER_MIGRATION_VALIDATION_TYPE_REST_ACCOUNT_CHECK = 6;
    public static final byte CUSTOMER_MIGRATION_VALIDATION_TYPE_PAY_STATION_COUNT = 7;
    public static final byte CUSTOMER_MIGRATION_VALIDATION_TYPE_COUPON_COUNT = 8;
    public static final byte CUSTOMER_MIGRATION_VALIDATION_TYPE_UNUSED_MERCHANT_ACCOUNT = 9;
    public static final byte CUSTOMER_MIGRATION_VALIDATION_TYPE_LOCATION_WITH_DASH = 10;
    public static final byte CUSTOMER_MIGRATION_VALIDATION_TYPE_LONG_RATE_NAME = 11;
    public static final byte CUSTOMER_MIGRATION_VALIDATION_TYPE_MERCHANT_ACCOUNT_REFERENCE = 12;
    public static final byte CUSTOMER_MIGRATION_VALIDATION_TYPE_REVERSAL = 13;
    public static final byte CUSTOMER_MIGRATION_VALIDATION_TYPE_CARD_TRANSACTION = 14;
    
    public static final byte CUSTOMER_MIGRATION_STATUS_TYPE_REQUESTED = 0;
    public static final byte CUSTOMER_MIGRATION_STATUS_TYPE_SCHEDULED = 1;
    public static final byte CUSTOMER_MIGRATION_STATUS_TYPE_SUSPENDED = 2;
    public static final byte CUSTOMER_MIGRATION_STATUS_TYPE_CANCELLED = 3;
    public static final byte CUSTOMER_MIGRATION_STATUS_TYPE_QUEUED = 4;
    public static final byte CUSTOMER_MIGRATION_STATUS_TYPE_EMS6_DISABLED = 5;
    public static final byte CUSTOMER_MIGRATION_STATUS_TYPE_WAITING_DATA_MIGRATION = 6;
    public static final byte CUSTOMER_MIGRATION_STATUS_TYPE_DATA_MIGRATION_CONFIRMED = 7;
    public static final byte CUSTOMER_MIGRATION_STATUS_TYPE_COMPLETED = 8;
    public static final byte CUSTOMER_MIGRATION_STATUS_TYPE_COMMUNICATION_CHECKED = 9;
    public static final byte CUSTOMER_MIGRATION_STATUS_TYPE_ALL_PAYSTATIONS_MIGRATED = 10;
    public static final byte CUSTOMER_MIGRATION_STATUS_TYPE_BOARDED = 90;
    public static final byte CUSTOMER_MIGRATION_STATUS_TYPE_NOT_BOARDED = 99;
    
    public static final byte CUSTOMER_MIGRATION_FAILURE_TYPE_UNKNOWN = 0;
    public static final byte CUSTOMER_MIGRATION_FAILURE_TYPE_UNPROCESSED_REVERSAL = 1;
    public static final byte CUSTOMER_MIGRATION_FAILURE_TYPE_UNPROCESSED_CARD = 2;
    public static final byte CUSTOMER_MIGRATION_FAILURE_TYPE_PRE_MIGRATION_VALIDATIONS = 3;
    public static final byte CUSTOMER_MIGRATION_FAILURE_TYPE_IN_MIGRATION_VALIDATIONS = 4;
    
    public static final int NEW_TAB_ID = -1;
    
    public static final int PAY_STATION_CREATION_ERROR_GENERAL = 0;
    public static final int PAY_STATION_CREATION_ERROR_UNKNOWN_TYPE = -1;
    
    public static final int BATCH_SIZE = 5000;
    public static final String URI_BATCH_PROCESS_STATUS = "/secure/batchProcessStatus.html";
    
    // URL Reroute
    public static final String DOMAIN_NOTIFICATION = "url";
    public static final String IP_NOTIFICATION = "ip";
    public static final String DIGITAL_CONNECT_IP_NOTIFICATION = "connIp";
    
    public static final String VERSION = "version";
    public static final String HASHED_VERSION = "staticDir";
    public static final String BUILD_NUMBER = "buildNumber";
    public static final String BUILD_DATE = "builddate";
    public static final String HASHED_BUILD_DATE = "buildDateHash";
    public static final String DEPLOYMENT_MODE = "deploymentMode";
    
    public static final int PAY_STATION_DETAIL_LIST_MAX_TREND_DAYS = 7;
    public static final String SENSOR_TREND_TARGET_VOLTAGE = "11.6";
    public static final String SENSOR_TREND_TARGET_HUMIDITY = "95";
    public static final String SENSOR_TREND_TARGET_TEMPERATURE_LOW_C = "-40";
    public static final String SENSOR_TREND_TARGET_TEMPERATURE_HIGH_C = "60";
    public static final String SENSOR_TREND_TARGET_TEMPERATURE_LOW_F = "-40";
    public static final String SENSOR_TREND_TARGET_TEMPERATURE_HIGH_F = "140";
    
    // PermitIssueTypes
    public static final int PERMIT_ISSUE_TYPE_NA = 0;
    public static final int PERMIT_ISSUE_TYPE_PND = 1;
    public static final int PERMIT_ISSUE_TYPE_PBL = 2;
    public static final int PERMIT_ISSUE_TYPE_PBS = 3;
    
    public static final String JSON_ERROR_STATUS_LABEL = "errorStatus";
    public static final String POSTTOKEN_STRING = "postToken";
    
    // Hash type
    public static final int HASH_TYPE_WS_MISSING = 0;
    
    public static final byte RATE_TYPE_FLAT = 0;
    public static final byte RATE_TYPE_DAILY = 1;
    public static final byte RATE_TYPE_HOURLY = 2;
    public static final byte RATE_TYPE_INCREMENTAL = 3;
    public static final byte RATE_TYPE_MONTHLY = 4;
    public static final byte RATE_TYPE_PARKING_RESTRICTION = 5;
    public static final byte RATE_TYPE_HOLIDAY = 6;
    
    public static final byte DEFAULT_DISPLAY_PRIORITY = 50;
    
    public static final int PAGED_LIST_MAXUPDATETIME_LONG_LENGTH = 10;
    
    public static final int ALERT_THRESHOLD_100 = 100;
    public static final int ALERT_THRESHOLD_75 = 75;
    public static final float ALERT_THRESHOLD_0_75 = 0.75f;
    
    public static final String NOT_USED = "NOTUSED";
    
    public static final char PRINTABLE_CHAR_START = 0x20;
    public static final char PRINTABLE_CHAR_END = 0x7E;
    
    public static final int BITCROP_BYTE = 0XFF;
    public static final int BITCROP_SHORT = 0XFFFF;
    public static final int BITCROP_SECOND_BYTE = 0XFF00;
    public static final int BITCROP_17TH = 0X100;
    
    public static final int BITCOUNT_BYTE = 8;
    public static final int BITCOUNT_SHORT = 16;
    
    public static final int CHAR_BUFFER_CAPACITY = 128;
    
    public static final int MAX_CURRENCY_SCALE = 2;
    public static final int MIN_CURRENCY_SCALE = -2;
    
    public static final int MIN_DISCOUNT_DOLLARS = 0;
    public static final int MIN_DISCOUNT_PERCENTAGE = 0;
    public static final int MAX_DISCOUNT_PERCENTAGE = 100;
    
    public static final int ONE_SECOND_IN_MILLISECONDS = 1000;
    public static final int ONE_DAY_IN_MILLISECONDS = 86400000;
    
    public static final String CUSTOMER_TIME_ZONE_NO_SELECTION = StandardConstants.STRING_MINUS_ONE;
    
    public static final int QUARTERS_HOURS_PER_DAY = 96;
    
    public static final Locale DEFAULT_LOCALE = Locale.ENGLISH;
    
    public static final String DATE_FORMAT_WITH_TIMEZONE_XSTREAM = "yyyy-MM-dd'T'HH:mm:ssXXX";
    
    public static final String DUPLICATE_CLOSE_BATCH_TRANSACTION = "DuplicateCloseBatchTX";
    
    public static final Integer XSTREAM_PERMIT = 1;
    public static final Integer XSTREAM_CITATION_WIDGET_TYPE = 2;
    public static final Integer XSTREAM_CITATION_WIDGET_MAP_TYPE = 3;
    public static final Integer XSTREAM_CONTRAVENTION_TYPE = 4;
    public static final Integer XSTREAM_PERMIT_DETAIL = 5;
    public static final Integer XSTREAM_FACILITY = 6;
    public static final Integer XSTREAM_PROPERTY = 7;
    public static final Integer XSTREAM_FLEX_INFO = 8;
    public static final Integer XSTREAM_CITATION_WIDGET_RAW_TYPE = 9;
    
    public static final byte QUEUE_TYPE_RECENT_EVENT = 0b00000001;
    public static final byte QUEUE_TYPE_PROCESSING_EVENT = 0b00000010;
    public static final byte QUEUE_TYPE_HISTORICAL_EVENT = 0b00000100;
    public static final byte QUEUE_TYPE_NOTIFICATION_EMAIL = 0b00001000;
    public static final byte QUEUE_TYPE_CUSTOMER_ALERT_TYPE = 0b00010000;
    
    public static final String HTTP_SUFFIX = "://";
    
    public static final int LENGTH_PS_SERIAL_NUMBER = 12;
    
    public static final String ONLINE_PAYSTATION_CONFIGURATION_STATUS_ALL = "all";
    
    // JSON schema validation files
    public static final String GEO_PARKING_AREA_JSON_SCHEMA = "/geo_parking_area.schema.json";
    
    public static final String ZERO_SECONDS = "00";
    
    public static final String UI_MESSAGES = "messages";

    public static final String CLIENT_EXCEPTION_MESSAGE_NO_AVAILABLE_SERVER = "load balancer does not have available server for client";
    public static final String CLIENT_EXCEPTION_MESSAGE_NUM_RETRIES_EXCEEDED_MAX = "number of retries on next server exceeded max";
    
    public static final String LINK_ACTION_TYPE_ADD = "add";
    public static final String LINK_ACTION_TYPE_UPDATE = "update";
    public static final String LINK_ACTION_TYPE_DELETE = "delete";
    public static final String LINK_EMAIL_PS_SUFFIX = "@paystation";
    public static final String LINK_PROCESSOR_NAME_SUFFIX = ".link";
    public static final String LINK_PROCESSOR_NAME_SUFFIX_KEY = "link.processor.suffix";
    
    private WebCoreConstants() {
        
    }
}
