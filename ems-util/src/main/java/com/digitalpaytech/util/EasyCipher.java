package com.digitalpaytech.util;

import java.io.Serializable;

import com.digitalpaytech.exception.CryptoException;

public interface EasyCipher extends Serializable {
	public byte[] encrypt(byte[] plain) throws CryptoException;
	public byte[] decrypt(byte[] encrypted) throws CryptoException;
	public void ensureInit();
}