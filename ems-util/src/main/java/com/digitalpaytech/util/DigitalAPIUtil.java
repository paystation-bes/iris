package com.digitalpaytech.util;

import org.apache.log4j.Logger;

public class DigitalAPIUtil
{
	private static final Logger logger = Logger.getLogger(DigitalAPIUtil.class);

   private static final String EMS_REGULAR_EXP = "[0-9a-zA-Z| |.|,|\\-|=|_|\\#|\\@|\\(|\\)|/|:|\\||\\{|\\}|\\\\]*";

	public static boolean isInteger(String value)
	{
		if (value == null || value.trim().length() == 0)
			return (false);

		if (value.length() > 8)
			return (false);

		for (int i = 0; i < value.length(); i++)
		{
			char character = value.charAt(i);
			if (!Character.isDigit(character))
				return (false);
		}
		return (true);
	}


	public static boolean validateStringWithRegEx(String value, String regEx)
	{
		if ((value != null) && !(value.equals("")))
		{
			if (!value.matches(regEx))
			{
				return false;
			}
		}
		return true;
	}

	// Validate information using regular expressions.
	public static boolean validateString(String value)
	{
		return validateStringWithRegEx(value, EMS_REGULAR_EXP);
	}

}
