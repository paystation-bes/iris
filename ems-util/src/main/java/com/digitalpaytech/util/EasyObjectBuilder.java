package com.digitalpaytech.util;

import java.io.Serializable;

public interface EasyObjectBuilder<O> extends Serializable{
	O create(Object configurations);
}
