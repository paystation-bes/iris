package com.digitalpaytech.util.transport;

public interface Transporter {
    <T> String serialize(Class<T> rootClass, T rootObj);
    
    <T> T deserialize(Class<T> rootClass, String serializedObj);
}
