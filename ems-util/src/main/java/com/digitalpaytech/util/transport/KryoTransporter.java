package com.digitalpaytech.util.transport;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.apache.shiro.codec.Base64;

import com.digitalpaytech.util.EasyObjectBuilder;
import com.digitalpaytech.util.EasyPool;
import com.digitalpaytech.util.PooledResource;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.UnsafeInput;
import com.esotericsoftware.kryo.io.UnsafeOutput;

public final class KryoTransporter implements Transporter {
    protected static final int DEFAULT_CONCURRENCY = 5;
    
    private EasyPool<Kryo> kryoPool;
    
    public KryoTransporter(final int concurrency) {
        this.kryoPool = new EasyPool<Kryo>(new EasyObjectBuilder<Kryo>() {
            @Override
            public Kryo create(final Object configurations) {
                return new Kryo();
            }
        }, null, concurrency);
    }
    
    @Override
    public <T> String serialize(final Class<T> rootClass, final T rootObj) {
        String result = null;
        if (rootObj != null) {
            final ByteArrayOutputStream baos = new ByteArrayOutputStream(8192);
            final UnsafeOutput out = new UnsafeOutput(baos);
            
            final PooledResource<Kryo> kryo = this.kryoPool.take();
            kryo.get().writeObject(out, rootObj);
            kryo.close();
            out.close();
            
            result = new String(Base64.encode(baos.toByteArray()));
        }
        
        return result;
    }
    
    @Override
    public <T> T deserialize(final Class<T> rootClass, final String serializedObj) {
        T result = null;
        byte[] objBytes = null;
        if (serializedObj != null) {
            objBytes = Base64.decode(serializedObj);
        }
        
        if ((objBytes != null) && (objBytes.length > 0)) {
            final UnsafeInput in = new UnsafeInput(new ByteArrayInputStream(objBytes));
            
            final PooledResource<Kryo> kryo = this.kryoPool.take();
            result = kryo.get().readObject(in, rootClass);
            kryo.close();
            in.close();
        }
        
        return result;
    }
    
}
