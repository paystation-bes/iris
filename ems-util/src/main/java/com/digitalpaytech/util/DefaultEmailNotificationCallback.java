package com.digitalpaytech.util;

import javax.mail.internet.InternetAddress;

import org.apache.log4j.Logger;

public class DefaultEmailNotificationCallback implements EmailNotificationCallback {
	Logger logger = Logger.getLogger(DefaultEmailNotificationCallback.class);
	EmailNotification notification;

	public DefaultEmailNotificationCallback(EmailNotification notification) {
		super();
		this.notification = notification;
	}

	public void onFailure(Throwable e) {
		logger.error("Send notification failed!", e);
	}

	public void onSuccess() {
		StringBuffer msg = new StringBuffer();
		msg.append("Successfully sent message to recipients:\n");
		InternetAddress[] addrs = notification.getToAddrs();
		if (addrs != null) {
			for (InternetAddress addr : addrs) {
				msg.append("\t" + addr.getAddress() + "\n");
			}
		}
		addrs = notification.getCcAddrs();
		if (addrs != null) {
			for (InternetAddress addr : addrs) {
				msg.append("\t" + addr.getAddress() + "\n");
			}
		}
		addrs = notification.getBccAddrs();
		if (addrs != null) {
			for (InternetAddress addr : addrs) {
				msg.append("\t" + addr.getAddress() + "\n");
			}
		}
		logger.debug(msg.toString());
	}
}