package com.digitalpaytech.util;

import java.text.MessageFormat;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;

import org.apache.log4j.Logger;

/**
 * This class is responsible for shutting down all {@link EMSThread} properly. Every time {@link EMSThread} is instantiated, it will register itself with an instance of this class.
 * The {@link EMSThreadManager#shutdown()} need to be called before JVM shutdown. In case of WebApplication, a ServletContextListener should call this method when the ServletContext gets destroyed.
 * 
 * 
 * The reason for using this approach to properly shutdown/implement Threads is to minimize changes that need to be made to existing EMS7 codes (prevent ourselves from introducing new business-logic bugs).
 * 
 * @author wittawatv
 *
 */
public class EMSThreadManager {
    /* Singleton Declaration */
    /**
     * Singleton factory method.
     * 
     * @return An instance of EMSThreadManager.
     */
    public static EMSThreadManager getInstance() {
        return SingletonHolder.INSTANCE;
    }
    
    private static class SingletonHolder {
        private static final EMSThreadManager INSTANCE = new EMSThreadManager();
    }
    
    /* The actual class itself */
    private static final Logger logger = Logger.getLogger(EMSThreadManager.class);
    private static final String FMT_THREAD_IDENTITY = "Thread \"{0}\" [{1}]";
    
    private boolean shutdown = false;
    private Semaphore shutdownLock = new Semaphore(1, true);
    private Queue<EMSThread> registeredThreads = new ConcurrentLinkedQueue<EMSThread>();
    
    protected EMSThreadManager() {
        
    }
    
    /**
     * Register the EMSThread with this EMSThreadManager. You don't need to manually call this method because every instance of EMSThread is registered to a Manager.
     * 
     * @param thread
     */
    public void register(EMSThread thread) {
        if(this.shutdown) {
            throw new IllegalStateException("EMSThreadManager is about to shutdown ! Could not register new Thread");
        }
        
        this.registeredThreads.offer(thread);
        if(logger.isInfoEnabled()) {
            logger.info("Registerd: " + printThreadIdentity(thread));
        }
    }
    
    /**
     * Shutdown all the threads registered with this EMSThreadManager by set their "shutdown" flag to true. Then interrupt all of them.
     * 
     */
    public void shutdown() {
        this.shutdown = true;
        
        EMSThread[] threads;
        
        logger.info("Acquiring shutdown lock to hold all current threads for shutdown...");
        shutdownLock.acquireUninterruptibly();
        try {
            threads = this.registeredThreads.toArray(new EMSThread[0]);
            this.registeredThreads.clear();
        }
        finally {
            shutdownLock.release();
            logger.info("...Released shutdown lock.");
        }
        
        for(int i = threads.length; --i >= 0; ) {
            if(threads[i].isShutdown()) {
                if(logger.isDebugEnabled()) {
                    logger.debug(printThreadIdentity(threads[i]) + " has already been flagged for shutdown!");
                }
            }
            else {
                threads[i].shutdown();
                threads[i].interrupt();
                if(logger.isInfoEnabled()) {
                    logger.info("Shutdown: " + printThreadIdentity(threads[i]));
                }
            }
        }
    }
    
    private String printThreadIdentity(EMSThread thread) {
        return MessageFormat.format(FMT_THREAD_IDENTITY, thread.getName(), thread.getId());
    }
}
