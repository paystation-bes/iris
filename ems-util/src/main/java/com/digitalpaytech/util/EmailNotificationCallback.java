package com.digitalpaytech.util;

public interface EmailNotificationCallback
{
	public void onSuccess();
	public void onFailure(Throwable e);
}

