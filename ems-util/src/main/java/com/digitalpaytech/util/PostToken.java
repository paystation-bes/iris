package com.digitalpaytech.util;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Random;

import org.apache.commons.validator.GenericValidator;

public class PostToken implements Serializable {
	private static final long serialVersionUID = -4423706506611303100L;
	
	private String token;
	
	public PostToken() {
		this((String) null);
	}
	
	public PostToken(String token) {
		if(token == null) {
			reset();
		}
		else {
			this.token = token;
		}
	}
	
	public void reset() {
//		this.token = WebSecurityUtil.getRandomLongAsHexString(WebSecurityConstants.RANDOMKEY_MIN_HEX_LENGTH, WebSecurityConstants.RANDOMKEY_MAX_HEX_LENGTH).toUpperCase();
		long number;
        String hex;
        Random random = new Random(Calendar.getInstance().getTimeInMillis());
        do {
            number = random.nextLong();
            hex = Long.toHexString(number);
        } while (!GenericValidator.isInRange(hex.length(), WebSecurityConstants.RANDOMKEY_MIN_HEX_LENGTH, WebSecurityConstants.RANDOMKEY_MAX_HEX_LENGTH));
        
        this.token = hex;
	}
	
	public boolean matches(String other) {
		boolean result = true;
		if(this.token != null) {
			result = this.token.equals(other);
		}
		
		return result;
	}

	@Override
	public String toString() {
		return token;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PostToken other = (PostToken) obj;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		return true;
	}
}
