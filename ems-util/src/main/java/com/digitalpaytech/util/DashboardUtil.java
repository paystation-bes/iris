package com.digitalpaytech.util;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.web.servlet.SimpleCookie;

import java.util.Iterator;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.digitalpaytech.domain.Tab;
import com.digitalpaytech.domain.Section;
import com.digitalpaytech.domain.Widget;

/**
 * DashboardUtil contains utility static methods for Dashboard & Widget.
 * 
 * @author Allen Liang
 */

public class DashboardUtil {

	private static final int TEMP_ID_START_ID = 10;
	private static int tempId = TEMP_ID_START_ID;
	private static int maxTemporaryId = 999999;
	
	/**
	 * Temporary id will be increment by 1 and prefix with tempIdPrefix variable, e.g. TempT10, TempS11, TempW12.
	 * The id will be reset after reaching the max, 999999.
	 * @param tempIdPrefix e.g. WebCoreConstants.TEMP_TAB_ID_PREFIX, WebCoreConstants.TEMP_SECTION_ID_PREFIX or WebCoreConstants.TEMP_WIDGET_ID_PREFIX.
	 * @return String temporary id.
	 */
	public static String getTemporaryId(String tempIdPrefix) {
		if (tempId == maxTemporaryId) {
			tempId = TEMP_ID_START_ID;
		}
		StringBuilder bdr = new StringBuilder();
		bdr.append(tempIdPrefix).append(++tempId);
		return bdr.toString();
	}

	
	public static void resetTemporaryId(String password) {
		if (StringUtils.isNotBlank(password) && password.equals(WebSecurityConstants.COMPLEX_PASSWORD_EXP)) {
			tempId = TEMP_ID_START_ID;
		}
	}
	
	public static boolean validateRandomId(String randomId) {
		return randomId != null && !randomId.equals("") && randomId.matches("[0-9A-Za-z]+$");
	}

	
	public static boolean isSameRandomId(String randomId1, String randomId2) {
		if (StringUtils.isNotBlank(randomId1) && StringUtils.isNotBlank(randomId2) && randomId1.equals(randomId2)) {
			return true;
		}
		return false;
	}	
	
	/**
	 * Checks if input 'number' is an integer.
	 * @param number String format.
	 * @return boolean true if it's an integer.
	 */
	public static boolean isInteger(String number) {
		try {
			Integer.parseInt(number);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}
	
	/**
	 * Checks if input 'number' is an integer and greater than -1.
	 * @param number String format.
	 * @return boolean true if it's an integer and greater than -1.
	 */
	public static boolean isPositiveInteger(String number) {
		if (isInteger(number)) {
			int i = Integer.parseInt(number);
			if (i > -1) {
				return true;
			}
		}
		return false;
	}
	
	
	public static boolean validateDBIntegerPrimaryKey(String id) {
		return id != null && !id.equals("") && id.matches(WebCoreConstants.REGEX_INTEGER);
	}
	
	
	/**
	 * Sets randomId in Tab, Sections and Widgets objects.
	 * @param request HttpServletRequest with HttpSession.
	 * @param tab Full fetched Tab object
	 * @return Tab All randomIds are generated.
	 */
	public static Tab setRandomIds(HttpServletRequest request, Tab tab) {
		// Sets randomId in Tab.
		tab = setTabRandomId(request, tab);
		
		// Sets randomIds in Sections & Widgets.
		RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
		Iterator<Widget> widIter;
		Iterator<Section> secIter = tab.getSections().iterator();
		while (secIter.hasNext()) {
			Section sec = secIter.next();
			sec.setRandomId(keyMapping.getRandomString(sec, WebCoreConstants.ID_LOOK_UP_NAME));
			tab.addSectionToMap(sec);
			
			widIter = sec.getWidgets().iterator();
			while (widIter.hasNext()) {
				Widget wid = widIter.next();
				wid.setRandomId(keyMapping.getRandomString(wid, WebCoreConstants.ID_LOOK_UP_NAME));
				sec.addWidgetToMap(wid);
			}
		}
		return tab;
	}
	
	
	/**
	 * Sets randomId in Tab object only.
	 * @param request HttpServletRequest with HttpSession.
	 * @param tab Tab object.
	 * @return Tab Tab randomId is generated.
	 */
	public static Tab setTabRandomId(HttpServletRequest request, Tab tab) {
		RandomKeyMapping keyMapping = SessionTool.getInstance(request).getKeyMapping();
		if (tab.getId() == null || tab.getId().intValue() == 0 || tab.getId().intValue() == -1) {
			tab.setRandomId(DashboardUtil.getTemporaryId(WebCoreConstants.TEMP_TAB_ID_PREFIX));
			tab.setRandomId(keyMapping.getRandomString(tab, WebCoreConstants.RANDOM_ID_LOOK_UP_NAME));

		} else {
			tab.setRandomId(keyMapping.getRandomString(tab, WebCoreConstants.ID_LOOK_UP_NAME));
		}
		return tab;
	}
	
	public static void setCurrentTabCookie(HttpServletRequest request, HttpServletResponse response, int currentTabIndex) {
		String cookieName = SessionUtil.getCurrentUserNameEncoded(request) + ".currentTabIndex";		
		SimpleCookie currentTabCookie = new SimpleCookie(cookieName);
		currentTabCookie.setValue(Integer.toString(currentTabIndex));
		currentTabCookie.setSecure(true);
		currentTabCookie.setHttpOnly(true);		
		currentTabCookie.saveTo(request, response);
	}
	
	public static int getCurrentTabFromCookie(HttpServletRequest request) {
		int result = -1;
		
		String cookieName = SessionUtil.getCurrentUserNameEncoded(request) + ".currentTabIndex";
		
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			int i = cookies.length;
			while((result == -1) && (--i >= 0)) {
				if (StringUtils.isNotBlank(cookies[i].getName()) && cookies[i].getName().equalsIgnoreCase(cookieName)) {
					try {
						result = Integer.parseInt(cookies[i].getValue());
					}
					catch(NumberFormatException nfe) {
						result = 0;
					}
				}
			}
		}
		
		return result;
	}
	
	private DashboardUtil() {
	}
}
