package com.digitalpaytech.util;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.util.ClassUtils;

public final class ExecutionBean {
    private Object instance;
    private String methodName;
    
    private Method[] methods;
    private MethodHandle[] methodHandles;
    
    public ExecutionBean() {
        
    }
    
    public ExecutionBean(final Object instance, final String methodName) {
        this.instance = instance;
        this.methodName = methodName;
    }
    
    @PostConstruct
    public void init() throws IllegalAccessException, NoSuchMethodException {
        if (this.instance == null) {
            throw new IllegalStateException("Object instance must be specified");
        }
        if (this.methodName == null) {
            throw new IllegalStateException("Method's name must be specified");
        }
        
        final Method[] candidates = this.instance.getClass().getMethods();
        
        final List<Method> matches = new ArrayList<Method>();
        int i = candidates.length;
        while (--i >= 0) {
            if (this.methodName.equals(candidates[i].getName())) {
                matches.add(candidates[i]);
            }
        }
        
        if (matches.size() <= 0) {
            throw new NoSuchMethodException("Could not locate Method " + this.instance.getClass().getName() + "." + this.methodName);
        } else {
            this.methods = matches.toArray(new Method[matches.size()]);
            this.methodHandles = new MethodHandle[matches.size()];
            
            final MethodHandles.Lookup lookup = MethodHandles.lookup();
            i = this.methods.length;
            while (--i >= 0) {
                this.methodHandles[i] = MethodHandles.insertArguments(lookup.unreflect(this.methods[i]), 0, this.instance);
            }
        }
    }
    
    // InvocationTargetException should be thrown instead of Throwable. This is because the functionality of this method is to execute another method.
    @SuppressWarnings({ "checkstyle:illegalcatch" })
    public Object execute(final Object... params) throws NoSuchMethodException, InvocationTargetException {
        if ((this.methods == null) || (this.methods.length <= 0)) {
            throw new IllegalStateException("Please initialize ExecutionBean by calling method 'init()' before executing it!");
        }
        
        MethodHandle handle = null;
        
        int i = this.methods.length;
        while ((handle == null) && (--i >= 0)) {
            if (paramTypesMatch(this.methods[i].getParameterTypes(), params)) {
                handle = this.methodHandles[i];
            }
        }
        
        if (handle == null) {
            final StringBuilder msg = new StringBuilder();
            msg.append("Could not locate suitable method for specified parameters [");
            for (Object p : params) {
                if (p == null) {
                    msg.append("null");
                } else {
                    msg.append(p.getClass().getName());
                }
                
                msg.append(",");
            }
            
            msg.append("]");
            
            throw new NoSuchMethodException(msg.toString());
        }
        
        Object result = null;
        try {
            result = handle.invokeWithArguments(params);
        } catch (Throwable t) {
            throw new InvocationTargetException(t);
        }
        
        return result;
    }

    public Object getInstance() {
        return this.instance;
    }

    public void setInstance(final Object instance) {
        preventMutation();
        this.instance = instance;
    }

    public String getMethodName() {
        return this.methodName;
    }

    public void setMethodName(final String methodName) {
        preventMutation();
        this.methodName = methodName;
    }
    
    private boolean paramTypesMatch(final Class<?>[] paramTypes, final Object[] paramValues) {
        boolean matched = paramTypes.length == paramValues.length;
        
        int i = paramTypes.length;
        while (matched && (--i >= 0)) {
            matched = ClassUtils.isAssignableValue(paramTypes[i], paramValues[i]);
        }
        
        return matched;
    }
    
    private void preventMutation() {
        if (this.methodHandles != null) {
            throw new IllegalStateException("This Object is immutable after 'init()'");
        }
    }
}
