package com.digitalpaytech.util.csv;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CsvProcessingError implements Serializable {
    private static final long serialVersionUID = -3898081999892508372L;
    
    private int lineNumber;
    private String errorMessage;
    private boolean shouldContinue;
    
    public CsvProcessingError() {
        this((String) null, 0, true);
    }
    
    public CsvProcessingError(String errorMessage) {
        this(errorMessage, 0, true);
    }
    
    public CsvProcessingError(String errorMessage, boolean shouldContinue) {
        this(errorMessage, 0, shouldContinue);
    }
    
    public CsvProcessingError(String errorMessage, int lineNumber) {
    	this(errorMessage, lineNumber, true);
    }
    
    public CsvProcessingError(String errorMessage, int lineNumber, boolean shouldContinue) {
    	this.errorMessage = errorMessage;
    	this.lineNumber = lineNumber;
    	this.shouldContinue = shouldContinue;
    }
    
    public int getLineNumber() {
        return lineNumber;
    }
    
    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }
    
    public String getErrorMessage() {
        return errorMessage;
    }
    
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    
    public boolean isShouldContinue() {
        return shouldContinue;
    }
    
    public void setShouldContinue(boolean shouldContinue) {
        this.shouldContinue = shouldContinue;
    }
    
    public static Map<String, StringBuilder> groupErrors(List<CsvProcessingError> errors) {
    	LinkedHashMap<String, StringBuilder> errorsGroup = new LinkedHashMap<String, StringBuilder>(errors.size() * 2);
        for (CsvProcessingError error : errors) {
            StringBuilder linesStr = errorsGroup.get(error.getErrorMessage());
            if (linesStr == null) {
                linesStr = new StringBuilder();
                errorsGroup.put(error.getErrorMessage(), linesStr);
                
                linesStr.append(error.getLineNumber());
            } else {
                linesStr.append(", ").append(error.getLineNumber());
            }
        }
        
        return errorsGroup;
    }
}
