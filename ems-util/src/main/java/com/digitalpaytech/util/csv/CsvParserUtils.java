package com.digitalpaytech.util.csv;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.digitalpaytech.exception.InvalidCSVDataException;
import com.digitalpaytech.util.WebCoreUtil;

public final class CsvParserUtils {
    private static final Pattern PATTERN_TOKEN = Pattern.compile("(^|,)((\"(([^\"]|(\"\"))*)\")|[^,]*)(?=,|$)");
    private static final int TOKEN_GROUP_QUOTED = 4;
    private static final int TOKEN_GROUP_NORMAL = 2;
    
    private CsvParserUtils() {
    }
    
    /**
     * Verify the headers
     * 
     * @param tokens
     *            Headers read from CSV file.
     * @param headers
     *            Expected headers.
     * @param columns
     *            Number of columns.
     * @throws InvalidCSVDataException
     */
    public static void verifyHeaders(final List<CsvProcessingError> errors, final String[] tokens, final String[] headers, final int columns) {
        // Incorrect number of columns
        if (tokens.length != columns) {
            errors.add(new CsvProcessingError("Invalid Header Data - Incorrect # of columns", 1, false));
        } else {
            for (int i = 0; i < columns; i++) {
                if (!headers[i].equalsIgnoreCase(tokens[i].trim())) {
                    errors.add(new CsvProcessingError("Invalid Header Data - Header: " + i + " is: '" + WebCoreUtil.encodeHTMLTag(tokens[i])
                                                      + "', but should be '" + headers[i] + "'",
                            1, false));
                }
            }
        }
    }
    
    public static String[] split(final String line) {
        final ArrayList<String> result = new ArrayList<String>();
        final Matcher m = PATTERN_TOKEN.matcher(line);
        while (m.find()) {
            String buffer = m.group(TOKEN_GROUP_QUOTED);
            if (buffer == null) {
                buffer = m.group(TOKEN_GROUP_NORMAL);
            }
            if (buffer == null) {
                throw new IllegalStateException("Invalid Regular Expression Pattern or Matched Token Group for Token Pattern");
            }
            
            result.add(buffer);
        }
        
        return result.toArray(new String[result.size()]);
    }
    
}
